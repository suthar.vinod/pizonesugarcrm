<?php
// created: 2022-05-05 10:55:36
$extensionOrderMap = array (
  'custom/Extension/application/Ext/EntryPointRegistry/updateHistoricData.php' => 
  array (
    'md5' => '9c12f353fad5ce0c9ad9062d477f8d7a',
    'mtime' => 1537423819,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/deleteAdditionalRecordsInWP.php' => 
  array (
    'md5' => '570c030a76b464f4080c4ca1331686ae',
    'mtime' => 1547532603,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyCompanyContactToContact.php' => 
  array (
    'md5' => '91c3b800769311f0f51706591e718a66',
    'mtime' => 1547532603,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/deleteAdditionalRecordsInSA.php' => 
  array (
    'md5' => 'b3cea3764beb644e6a270a5e3da4cff0',
    'mtime' => 1547532603,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyCompanyAndCompanyContactToRespectiveFields.php' => 
  array (
    'md5' => '6ab25f345faf3996ef8913f4217f7447',
    'mtime' => 1547532603,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyStudyDirectorToStudyDirectorScriptField.php' => 
  array (
    'md5' => '79e924585e4b973d4a2d8b2b34cc8648',
    'mtime' => 1548740930,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/insert_update_field_metadata.php' => 
  array (
    'md5' => 'e5a874f80ac97e8af4ae72afd1dfda8b',
    'mtime' => 1558160563,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/setCategoryToSponsor.php' => 
  array (
    'md5' => '25ecad9cba63e143b5f84dcae977a9d3',
    'mtime' => 1558161180,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/rename_obser_employee_since_fJan.php' => 
  array (
    'md5' => '172c9957779330b2fbdfcb567f7ebc60',
    'mtime' => 1558162994,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/rename_obser_since_fJan.php' => 
  array (
    'md5' => '6b65a3cb301869adde96aff1ef219013',
    'mtime' => 1558162994,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/migrateOldFieldsToNew.php' => 
  array (
    'md5' => '1342ef1693cf33813b2c9215ae5b37e4',
    'mtime' => 1558162994,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/migrateField.php' => 
  array (
    'md5' => '89ade6fcbe4f1e2b5b5f759b8174a6cf',
    'mtime' => 1558162994,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/migrateDateDiscovered.php' => 
  array (
    'md5' => '15044a0c8486f6cadeb3a8b9a21f2b87',
    'mtime' => 1558162994,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/attachAnimalsToRoom.php' => 
  array (
    'md5' => '28acf26032e19cdbab3e9f8c889e60c3',
    'mtime' => 1558162994,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/deleteEntriesInContactsDE.php' => 
  array (
    'md5' => '4ea039ce969a5388150a1913ca3ed7b3',
    'mtime' => 1558162994,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/insert_update_field_metadata_sprint2.php' => 
  array (
    'md5' => 'cd2b1bc8fe590b0b6846f2297f26b725',
    'mtime' => 1558163236,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyPrimaryApsOptToPrimaryApsOptRelate.php' => 
  array (
    'md5' => '8b063f39f4121f40c3d46bce9ec66d8d',
    'mtime' => 1558164137,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyDocumentStatusToStatusId.php' => 
  array (
    'md5' => '07584c09a4f180665003838adf16be24',
    'mtime' => 1558164137,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyDocumentTypeToCategoryId.php' => 
  array (
    'md5' => 'd5da01f2e28477f0377d68b660aacc76',
    'mtime' => 1558164137,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyLeadAuditorToLeadAuditorRelate.php' => 
  array (
    'md5' => 'c4404da6281175c45aaec36e9f008cac',
    'mtime' => 1558164137,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyStudyCoordinatorToStudyCoordinatorRelate.php' => 
  array (
    'md5' => 'b5a50e85bad9a877f5a2533fc69e7f1d',
    'mtime' => 1558164137,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyObserEmployeeFromEmployeeToContactsModule.php' => 
  array (
    'md5' => '0247649dca59b43e18405a72000b0437',
    'mtime' => 1558164137,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyShippedDestroyedDateFieldInShippedDestroyedText.php' => 
  array (
    'md5' => 'c699e993305e5c874948ad15269b8b88',
    'mtime' => 1558164137,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyPathologistAndStudyVeterinarianToAuthorField.php' => 
  array (
    'md5' => 'db14e3c788e2c90ceb30366295d95638',
    'mtime' => 1558164137,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyPrincipalInvestigatorToPrincipalInvestigatorRelate.php' => 
  array (
    'md5' => 'b262f2b76976b6104131bd4be77ca38e',
    'mtime' => 1558164137,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/setConfigVar.php' => 
  array (
    'md5' => 'd8342f7ed22e582c4411ce955aac96cc',
    'mtime' => 1560235139,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/updateLastServiceDateInEF.php' => 
  array (
    'md5' => 'a227c502a34f91de44949aece3458c60',
    'mtime' => 1560921340,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/myCustomDataMigrate.php' => 
  array (
    'md5' => '7122b736e13f5babda9badb5460696ec',
    'mtime' => 1561368263,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/addHomeInSpecialQueryModule.php' => 
  array (
    'md5' => '0b2a5c7755b7e3d676aa9b5a3eab66f5',
    'mtime' => 1561368263,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyTestSystemVendorToRelateField.php' => 
  array (
    'md5' => '75f89f9af2f84d35e77e9158e489a846',
    'mtime' => 1562047160,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/copyTestSystemDeceasedToCheckbox.php' => 
  array (
    'md5' => '85c12b134b5a63cbd0900bea852e2e6a',
    'mtime' => 1562047160,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/fixRegulatoryResponseName.php' => 
  array (
    'md5' => 'b083d4892d8c405fbcef264217b20cff',
    'mtime' => 1564999216,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/populateIntegerFieldInObservationModule.php' => 
  array (
    'md5' => 'c6e27352bd9c59bb7d4d0b2964af8d28',
    'mtime' => 1572932741,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/migrateFieldData.php' => 
  array (
    'md5' => '2d394b6caad1e459ec71ae1c8b1ea3d1',
    'mtime' => 1580119886,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/wpeSequenceCorrection.php' => 
  array (
    'md5' => 'cc04ac758b6b830606cba5a7034272d0',
    'mtime' => 1580967373,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/getEquipmentDataEP.php' => 
  array (
    'md5' => '4ec57f79033ac5a2d956e1b90cbb6f2e',
    'mtime' => 1583729804,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/getReportData.php' => 
  array (
    'md5' => 'b00db77a4ef0bb2dd4d91fa8ba7e765d',
    'mtime' => 1592297023,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/getGenReportData.php' => 
  array (
    'md5' => '2150adc882ac18d2b90fafd22c23e56e',
    'mtime' => 1592297023,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/getTaskDesignData.php' => 
  array (
    'md5' => 'de742394bf74b27e5b5da24ae1dd00bb',
    'mtime' => 1628929653,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/getGdDropdownofCurrentWP.php' => 
  array (
    'md5' => '528a0e59b37f397c12db2d7e152a14a7',
    'mtime' => 1636443126,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/getInventoryItem.php' => 
  array (
    'md5' => '4ac33b7fe12f4d37567c58129a0405c7',
    'mtime' => 1636498684,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/CurrentWPGetGDlist.php' => 
  array (
    'md5' => '8b46df141afdf1e919a0255bbd9a595c',
    'mtime' => 1639550468,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/EntryPointRegistry/getGdDropdownofWP.php' => 
  array (
    'md5' => '94bc0c11f2e0803ffbf3022cafc641a0',
    'mtime' => 1651748134,
    'is_override' => false,
  ),
);