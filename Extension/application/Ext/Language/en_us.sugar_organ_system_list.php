<?php
 // created: 2022-02-17 07:17:17

$app_list_strings['organ_system_list']=array (
  '' => '',
  'Cardiovascular' => 'Cardiovascular',
  'Clin Path' => 'Clin Path',
  'GI' => 'Gastrointestinal',
  'Integumentary' => 'Integumentary',
  'Lymphatic' => 'Lymphatic',
  'Musculoskeletal' => 'Musculoskeletal',
  'Neurologic' => 'Neurologic',
  'Not Applicable' => 'Not Applicable',
  'Ophthalmic' => 'Ophthalmic',
  'Respiratory' => 'Respiratory',
  'Surgical' => 'Surgical',
  'Systemic' => 'Systemic',
  'Urogential' => 'Urogenital',
  'Unknown ADR' => 'Unknown-ADR (Obsolete)',
);