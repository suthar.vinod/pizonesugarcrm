<?php
 // created: 2018-11-14 22:20:28

$app_list_strings['departments_list']=array (
  '' => '',
  'AS' => 'AS',
  'DVM' => 'DVM',
  'Facility' => 'Facility',
  'ISR' => 'ISR',
  'IVP' => 'IVT',
  'LA AC' => 'LA AC',
  'LA OP' => 'LA OP',
  'PS' => 'PS',
  'SA AC' => 'SA AC',
  'SA OP' => 'SA OP',
  'SCI' => 'SCI',
  'SP' => 'SP',
);