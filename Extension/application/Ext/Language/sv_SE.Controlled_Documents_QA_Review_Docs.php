<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sv_SE.Controlled_Documents_QA_Review_Docs.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sv_SE.Controlled_Documents_QA_Review_Docs.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Marknad';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Kunskapsbas';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Säljsida';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Marknadsföringsmaterial';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Produktbroschyrer';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'Frågor och svar';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Aktiv';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Utkast';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'Frågor och svar';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Utgången';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Under granskning';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Avvaktar';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Marknad';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Kunskapsbas';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Säljsida';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Marknadsföringsmaterial';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Produktbroschyrer';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'Frågor och svar';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Aktiv';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Utkast';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'Frågor och svar';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Utgången';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Under granskning';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Avvaktar';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Marknad';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Kunskapsbas';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Säljsida';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Marknadsföringsmaterial';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Produktbroschyrer';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'Frågor och svar';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Aktiv';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Utkast';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'Frågor och svar';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Utgången';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Under granskning';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Avvaktar';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';

?>
