<?php
 // created: 2021-11-10 00:50:52

$app_list_strings['im_location_type_list']=array (
  '' => '',
  'Cabinet' => 'Cabinet',
  'Equipment' => 'Equipment',
  'Known' => 'Known',
  'Room' => 'Room',
  'RoomCabinet' => 'Room/Cabinet',
  'RoomShelf' => 'Room/Shelf',
  'Shelf' => 'Shelf',
  'Unknown' => 'Unknown',
);