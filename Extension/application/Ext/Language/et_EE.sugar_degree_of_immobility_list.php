<?php
 // created: 2021-12-14 09:08:40

$app_list_strings['degree_of_immobility_list']=array (
  '' => '',
  'Minimal' => 'Minimal',
  'Partial' => 'Partial',
  'Complete' => 'Complete',
  'Minimal Partial' => 'Minimal & Partial',
);