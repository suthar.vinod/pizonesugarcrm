<?php
 // created: 2018-09-24 19:06:01

$app_list_strings['company_division_list']=array (
  'Biocompatibility' => 'Biocompatibility',
  'ISR' => 'ISR',
  'Pharmacology' => 'Pharmacology',
  'Analytical Services' => 'Analytical Services',
  'Pathology Services' => 'Pathology Services',
  'Toxicology' => 'Toxicology',
  'Choose Functional Area' => 'Choose Functional Area',
  'Bioskills' => 'Bioskills',
  'Regulatory' => 'Regulatory',
  '' => '',
  'Standard Biocompatibility' => 'Standard Biocompatibility',
);