<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Beklenmeyen Arama',
  'Existing Customer' => 'Mevcut Müşteri',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Çalışan',
  'Conference' => 'Konferans',
  'Trade Show' => 'Ticari Etkinlik',
  'Web Site' => 'Web Sitesi',
  'Email' => 'E-Posta',
  'Campaign' => 'Kampanya',
  'Support Portal User Registration' => 'Destek Portali Kullanıcı Tanımlaması',
  'Other' => 'Diğer',
  'Referral' => 'Referral',
);