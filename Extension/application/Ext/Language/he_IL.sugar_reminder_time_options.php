<?php
 // created: 2017-06-16 19:19:41

$app_list_strings['reminder_time_options']=array (
  -1 => 'אין',
  60 => 'לפני דקה',
  300 => 'לפני 5 דקות',
  600 => 'לפני 10 דקות',
  900 => 'לפני 15 דקות',
  1800 => 'לפני 30 דקות',
  3600 => 'לפני שעה',
  7200 => 'לפני שעתיים',
  10800 => 'לפני 3 שעות',
  18000 => 'לפני 5 שעות',
  86400 => 'לפני  יום',
  604800 => '7 days prior',
);