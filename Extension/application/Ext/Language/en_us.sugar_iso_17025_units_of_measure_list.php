<?php
 // created: 2020-10-26 18:17:53

$app_list_strings['iso_17025_units_of_measure_list']=array (
  '' => '',
  'CO2' => 'CO2',
  'Dimension' => 'Dimension',
  'Length' => 'Length',
  'Mass' => 'Mass',
  'Relative Humidity' => 'Relative Humidity',
  'RPM' => 'RPM',
  'Temperature' => 'Temperature',
  'Time' => 'Time',
  'Voltage' => 'Voltage',
  'Volume' => 'Volume',
);