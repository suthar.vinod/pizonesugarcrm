<?php
 // created: 2019-04-05 11:15:00

$app_list_strings['document_type_list']=array (
  'Study Design Document' => 'Study Design Document/Synopsis',
  'Study Quote' => 'Study Quote',
  'Quote Revision' => 'Quote Revision',
  'Signed Quote' => 'Signed Quote',
  'PO' => 'PO',
  'Change Order' => 'Change Order',
  '' => '',
  'Signed Change Order' => 'Signed Change Order',
  'Signed Quote Revision' => 'Signed Quote Revision',
  'Signed SOW' => 'Signed SOW',
  'SOW' => 'SOW',
);