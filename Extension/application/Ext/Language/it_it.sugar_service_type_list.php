<?php
 // created: 2021-01-12 11:40:53

$app_list_strings['service_type_list']=array (
  '' => '',
  'Analytical' => 'Analytical',
  'Bioskills' => 'Bioskills',
  'Clinical Pathology' => 'Clinical Pathology',
  'Histology' => 'Histology',
  'In Life Service' => 'In Life Service',
  'ISR' => 'ISR',
  'IVT' => 'IVT',
  'LAIL' => 'LAIL',
  'Pathology' => 'Pathology',
  'Phamacology' => 'Phamacology',
  'QA' => 'QA',
  'SAIL' => 'SAIL',
  'Sample Prep' => 'Sample Prep',
  'Scientific' => 'Scientific',
  'Toxicology' => 'Toxicology',
  'Standard Biocompatibility' => 'Standard Biocompatibility',
);