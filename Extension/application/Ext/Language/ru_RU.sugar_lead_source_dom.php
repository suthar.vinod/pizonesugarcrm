<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Холодный звонок',
  'Existing Customer' => 'Существующий клиент',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Сотрудник',
  'Conference' => 'Конференция',
  'Trade Show' => 'Специализированная выставка',
  'Web Site' => 'Веб-сайт',
  'Email' => 'E-mail',
  'Campaign' => 'Кампания',
  'Support Portal User Registration' => 'Пользовательская регистрация в Портал Поддержки',
  'Other' => 'Другой',
  'Referral' => 'Referral',
);