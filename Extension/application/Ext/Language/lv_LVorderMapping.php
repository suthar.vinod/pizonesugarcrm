<?php
// created: 2022-12-01 09:56:33
$extensionOrderMap = array (
  'custom/Extension/application/Ext/Language/lv_LV.sugar_projects_priority_options.php' => 
  array (
    'md5' => '2a142aa0ae9fad957013074807bb26cc',
    'mtime' => 1452047630,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_workproductcategory.php' => 
  array (
    'md5' => 'b5f93224504603b07d84026fbbfbe528',
    'mtime' => 1452052489,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_work_product_type_list.php' => 
  array (
    'md5' => '14b2494dff772c9326337af11f89812c',
    'mtime' => 1452052927,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_task_name_list.php' => 
  array (
    'md5' => 'e7603d13824c3ec504477a62f528fd69',
    'mtime' => 1452121526,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_altered_bid_terms_list.php' => 
  array (
    'md5' => 'face5b77e2fca4f1518a077c88eb9fd3',
    'mtime' => 1452178583,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_lead_source_dom.php' => 
  array (
    'md5' => '100e9373052315f646fb9f6897422ee4',
    'mtime' => 1452182133,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_job_function_list.php' => 
  array (
    'md5' => 'd49a5ab716551c62a8517d65b1b156a7',
    'mtime' => 1452189041,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_specialty_list.php' => 
  array (
    'md5' => '1f5acc7da380c96bd8be17c522ea8ab0',
    'mtime' => 1452195118,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_sales_activity_stage_list.php' => 
  array (
    'md5' => '4abef208afefd9ed290c68274ec7688f',
    'mtime' => 1452725054,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_SalesActivityCategory.php' => 
  array (
    'md5' => 'aaab1b58db71fa5321d5eeba63cd6d7b',
    'mtime' => 1453342834,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Sales_Management.php' => 
  array (
    'md5' => 'd87287af43830186a2f9bdfbcd2133a3',
    'mtime' => 1453686685,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_meeting_type_dom.php' => 
  array (
    'md5' => 'f571506fa6c37bd8b03860fac16e6f61',
    'mtime' => 1453846602,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_document_template_type_dom.php' => 
  array (
    'md5' => '2f5dd233e8c7a42e13ca2307d1464e53',
    'mtime' => 1456171518,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_document_status_dom.php' => 
  array (
    'md5' => 'cf9b42fbde5384f4614c032d9299f90d',
    'mtime' => 1456258071,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_internal_department_list.php' => 
  array (
    'md5' => 'a83506733c4ed9314d22add407b5d06b',
    'mtime' => 1456769658,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_pathologist_list.php' => 
  array (
    'md5' => '69ad5db420aa21c79f64452e671f0394',
    'mtime' => 1456870468,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_task_status_dom.php' => 
  array (
    'md5' => 'dbb3ec1ee041a9bf215a352aa87048c8',
    'mtime' => 1457114760,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_regulatory.php' => 
  array (
    'md5' => '1871d867332d68fec26dd75440120e04',
    'mtime' => 1457381929,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_Division_list.php' => 
  array (
    'md5' => 'dee82145cecf4aa9d4eb4519ddad4f82',
    'mtime' => 1457382288,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_Department_list.php' => 
  array (
    'md5' => '31f6972e4b271d68d06052a07b1b4d0a',
    'mtime' => 1457382378,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_bc_study_outcome_type_list.php' => 
  array (
    'md5' => '94175c1ca59f8bfd91e781a4949965fe',
    'mtime' => 1457452485,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_sales_activity_status_list.php' => 
  array (
    'md5' => '93fc3cc26bb910c52b5d133003ddb5d6',
    'mtime' => 1459199629,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_sales_activity_type_list.php' => 
  array (
    'md5' => 'bca703bf68d3593dddd7faad159b5b90',
    'mtime' => 1459705103,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_study_compliance_list.php' => 
  array (
    'md5' => '1444ea453cfbe3881627819a92b4cb42',
    'mtime' => 1459810259,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_Yes_No.php' => 
  array (
    'md5' => '8f273394d1ab01b082ab813ea86f9626',
    'mtime' => 1460154304,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_department_manager_approval_list.php' => 
  array (
    'md5' => '9e8acae486d6def4853a6a5765ce300a',
    'mtime' => 1460411892,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_study.php' => 
  array (
    'md5' => '0fcbb9d184bc97d18b32b85f045a28a5',
    'mtime' => 1460471107,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_invoicing.php' => 
  array (
    'md5' => '81f6d041e1ba0b95d1d8b3d37ee53a55',
    'mtime' => 1460473173,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_data_storage_phase_list.php' => 
  array (
    'md5' => 'e1542534d520b40f5d6c149bb2fd86a4',
    'mtime' => 1460997696,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_study_pathologist_list.php' => 
  array (
    'md5' => '0884c5c5b093ac81fca13e1f319b90ca',
    'mtime' => 1461083310,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_expanded_study_result_list.php' => 
  array (
    'md5' => '6cc4fad755a40b92675c007818ea0a76',
    'mtime' => 1461159065,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_Med_Device_Category_and_Contact.php' => 
  array (
    'md5' => '6dae8a54821df866b44dd15e296805ef',
    'mtime' => 1461684029,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_patient_contact_duration_list.php' => 
  array (
    'md5' => '4e9738b5d229c9ad9814d258f49d6779',
    'mtime' => 1461686352,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_followup_status_list.php' => 
  array (
    'md5' => '2939f7bdf4c6fd3106aa0b536448fe80',
    'mtime' => 1463797916,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_quote_status_list.php' => 
  array (
    'md5' => '2c7a56fbe34565e5b34901349e4256e3',
    'mtime' => 1470759099,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_animal_model_list.php' => 
  array (
    'md5' => '3e7d6e4f4b8809442b111b85ed62e10e',
    'mtime' => 1471644013,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_device_type_list.php' => 
  array (
    'md5' => 'f795368914224bbbfa380af93c734b31',
    'mtime' => 1471644581,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_bc_study_article_received_list.php' => 
  array (
    'md5' => '14e9df022dc3a9fb9e86d2d866f51d77',
    'mtime' => 1477920881,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_deliverable_owner_list.php' => 
  array (
    'md5' => 'a44d8899a20671eacc3c74dba02147d1',
    'mtime' => 1479760637,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_pathologist_workload_list.php' => 
  array (
    'md5' => 'c0483574ada7547bf889142b6b3cda09',
    'mtime' => 1480601831,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_quote_review_priority_list.php' => 
  array (
    'md5' => '2e8cf421e60255fa64715801e04035d4',
    'mtime' => 1482852180,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_signed_quote_c_list.php' => 
  array (
    'md5' => 'f6db0a309d6a33b0e0e90288cae6a4f1',
    'mtime' => 1485188416,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_estimated_hours_list.php' => 
  array (
    'md5' => '42de106c07f8db00ca57069664b120db',
    'mtime' => 1486678993,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_primary_aps_operator_list.php' => 
  array (
    'md5' => '98695d68ccbc1dd1df29f5c0797b030e',
    'mtime' => 1489504759,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_product_type_list.php' => 
  array (
    'md5' => 'b998299d4785fb33e66f80ef283cc4ca',
    'mtime' => 1489607384,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_Product_Contact_Category.php' => 
  array (
    'md5' => 'bd8f39538a616f32649df80af4a9d365',
    'mtime' => 1489607480,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_contact_duration_list.php' => 
  array (
    'md5' => '534a86bc48ec4325aed58901d78f286a',
    'mtime' => 1489608634,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_contact_type_list.php' => 
  array (
    'md5' => '4853941e5fb784453f21c0faa7b22c12',
    'mtime' => 1489609949,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_Cytotoxicity_Testing.php' => 
  array (
    'md5' => '8207a2eb1097b98ab89a80fc1c1ea13b',
    'mtime' => 1489611457,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_activity_list.php' => 
  array (
    'md5' => '01467d4cad56fe30f043e6035b6af638',
    'mtime' => 1490044255,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_invoice_installment_percent_list.php' => 
  array (
    'md5' => '38ee736b51c6d31d309e80d1c6febf73',
    'mtime' => 1492111064,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_business_develop_activity_list.php' => 
  array (
    'md5' => 'bdbeb51c083819478eaafa39e397ab89',
    'mtime' => 1493742615,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_quality_assurance_activity_list.php' => 
  array (
    'md5' => '32a567c9d15080364feea0419899ebfd',
    'mtime' => 1493744560,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_regulatory_activity_list.php' => 
  array (
    'md5' => 'a04c275eb96f43d321fad96a8ef15c7b',
    'mtime' => 1493745906,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Activity_Notes.php' => 
  array (
    'md5' => '62223d8a8f36ca353346083f6e31562c',
    'mtime' => 1494540242,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_activity_personnel_list.php' => 
  array (
    'md5' => '8e6d9a3510c6bfd6a3378f7c25c90ffc',
    'mtime' => 1494618165,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_new_deliverable_c_list.php' => 
  array (
    'md5' => '63f1ecb8f811c4b2f2a3985435da6d08',
    'mtime' => 1497042102,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_pathology_activity_list.php' => 
  array (
    'md5' => 'adae62066e604bfd65370a761643053c',
    'mtime' => 1497042653,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_reminder_time_options.php' => 
  array (
    'md5' => '0a849d42b1d09f515df393dc364bee1c',
    'mtime' => 1497640781,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_analytical_equipment_list_list.php' => 
  array (
    'md5' => '476b7cf01ddd2946c177fcca4a9ba4c8',
    'mtime' => 1497647886,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.HR_Management.php' => 
  array (
    'md5' => '454d0eda9761ea62acaccf05aea43237',
    'mtime' => 1500561436,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_job_category_list.php' => 
  array (
    'md5' => '4d946120b67aafcbbff368864a98ff97',
    'mtime' => 1500569094,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.AnimalsModuleCustomizations.php' => 
  array (
    'md5' => '9793e9a924b0ac18896ad060ff12b03d',
    'mtime' => 1505227918,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.WPEsModuleCustomizations.php' => 
  array (
    'md5' => 'b1ff62523a0b8a1002f42a049d0ac11f',
    'mtime' => 1505228202,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.customa1a_critical_phase_inspectio_activities_1_calls.php' => 
  array (
    'md5' => '0b71ac7c23f44d73c72fe5a5a3b8f243',
    'mtime' => 1505315712,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_sales_focus_list.php' => 
  array (
    'md5' => 'c1c2f1b0879e9a835ea6f2020dbe3eac',
    'mtime' => 1516229438,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_primary_activity_list.php' => 
  array (
    'md5' => '021d0624fec6e4244f5f06ae5cfec165',
    'mtime' => 1516304517,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_visit_status_list.php' => 
  array (
    'md5' => 'd401a8b0129ac04e6429d5c7e1521857',
    'mtime' => 1516654576,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_error_occured_on_weekend_list.php' => 
  array (
    'md5' => '55818505dcbf65805abe6d04e5b4a583',
    'mtime' => 1517331009,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_vaccine_a_list.php' => 
  array (
    'md5' => '497f371167a4f6c8cb4581d7da46fb7d',
    'mtime' => 1519154513,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_vaccine_b_c_list.php' => 
  array (
    'md5' => 'ad62950e7ce92a0a2c1d6c5dd1008458',
    'mtime' => 1519154638,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_vaccine_c_list.php' => 
  array (
    'md5' => '5fb84d42ffa6ea2ddb53f007132756fd',
    'mtime' => 1519154722,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_deceased_list.php' => 
  array (
    'md5' => '380060a90282d9579ec2f9e615db3e33',
    'mtime' => 1523539881,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_reason_for_amendment_list.php' => 
  array (
    'md5' => 'e42cc2f3f71ceaf49df0e8326cd706a6',
    'mtime' => 1523624792,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_lead_quality_list.php' => 
  array (
    'md5' => 'acd6199303f2d3fcc16f95d542b3fe96',
    'mtime' => 1524503796,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_test_control_article_checkin_list.php' => 
  array (
    'md5' => 'bdb388c521a1c536ecf0f732ea900fe0',
    'mtime' => 1525111547,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_sales_activity_source_list.php' => 
  array (
    'md5' => '179616572e78d78e3596bf3cb6f5f1a2',
    'mtime' => 1525809086,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_imaging_modality_list.php' => 
  array (
    'md5' => 'b7bc2daa33f671d0b1b7cc0e22bda2f3',
    'mtime' => 1526920700,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_anatomical_location_list.php' => 
  array (
    'md5' => '6f8c9fd69dbb07fcf0b47058758f05d8',
    'mtime' => 1526928146,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_weight_range_list.php' => 
  array (
    'md5' => '819f89f1c75b9a4ef84a0385841e12f8',
    'mtime' => 1526932340,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_portal_account_activated_list.php' => 
  array (
    'md5' => '57d6d4cb8869ef6faac8347254a535c4',
    'mtime' => 1528812837,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_procedure_room_type_list.php' => 
  array (
    'md5' => 'db083384918673b66f1b9c80ee614d31',
    'mtime' => 1530039142,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_required_equipment_list.php' => 
  array (
    'md5' => '43165d78a32d53ae74e8a1f562d1bd2c',
    'mtime' => 1530039672,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_regulatory_region_list.php' => 
  array (
    'md5' => 'e45ce4abe367eb831259bfea005b962b',
    'mtime' => 1530040209,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_required_reports_list.php' => 
  array (
    'md5' => 'c27e5baf68b23654b63591d4d9f2fbdf',
    'mtime' => 1530040489,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_cpi_finding_category_list.php' => 
  array (
    'md5' => 'd42b1bed4a98e1d3b80af0ed99435e32',
    'mtime' => 1536257578,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_timeline_type_list.php' => 
  array (
    'md5' => 'f33df31c3cdf17ad476ffb65534e3227',
    'mtime' => 1536868175,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_chronic_study_list.php' => 
  array (
    'md5' => 'dd1ca71f86ff91d22da776e6633e0ee9',
    'mtime' => 1536931758,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_duration_greater_than_4_week_list.php' => 
  array (
    'md5' => 'f8b29aaf9b2868496b29c4139e3e49b0',
    'mtime' => 1536931934,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_view_all_company_projects_list.php' => 
  array (
    'md5' => '45269dff400f08c5a747f7aae75a2c31',
    'mtime' => 1537205764,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_is_manager_list.php' => 
  array (
    'md5' => 'f487c070beba518734aaa2a9aadae729',
    'mtime' => 1537423814,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_responsible_personnel_list.php' => 
  array (
    'md5' => 'a7b05b60089d9042a0fdd275308fd3ed',
    'mtime' => 1537423814,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_deviation_type_list.php' => 
  array (
    'md5' => '15d852c505af294e9594878d4fc217bf',
    'mtime' => 1537423814,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_company_division_list.php' => 
  array (
    'md5' => 'b464421d66b59e81709d51dbc4d62334',
    'mtime' => 1537815961,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_employee_status_dom.php' => 
  array (
    'md5' => '06d2b5e6f566b637d0d40103ce0c2267',
    'mtime' => 1537879807,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_functional_area_list.php' => 
  array (
    'md5' => '270bc58303e76978bac2c9a2ae8b1738',
    'mtime' => 1537890068,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_reason_for_lost_quote.php' => 
  array (
    'md5' => 'dcb6b62915d21af86b099dc13eb0c11a',
    'mtime' => 1540321904,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_departments_list.php' => 
  array (
    'md5' => 'e033a435486397950aefc0eb9b54bca6',
    'mtime' => 1542234027,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_instrument_list.php' => 
  array (
    'md5' => '5ca0e5d66fa89e5ecd91d69928ec7184',
    'mtime' => 1542292724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_lead_auditor_list.php' => 
  array (
    'md5' => '992b3a02edb8c8b75ec35b92cb0c9658',
    'mtime' => 1542726469,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_analytical_activity_list.php' => 
  array (
    'md5' => '1dc1194dbdf12b522767bd84d1903459',
    'mtime' => 1542822935,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_required.php' => 
  array (
    'md5' => '5261ef660cce5d26989df8a929030922',
    'mtime' => 1543443670,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_necropsy_type_list.php' => 
  array (
    'md5' => 'bcac54b14ecb0621ab84f6b67f594151',
    'mtime' => 1543445005,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_slide_size_type_list.php' => 
  array (
    'md5' => '45002d32cd1667f6797e337362712002',
    'mtime' => 1543512869,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_histopath_processing_type_list.php' => 
  array (
    'md5' => 'c3e8e580b7358a07781661772e248e56',
    'mtime' => 1543513310,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.customm01_sales_activities_1_calls.php' => 
  array (
    'md5' => '4472136f04cf86a2df7c3ceee6c74011',
    'mtime' => 1544482922,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_spa.php' => 
  array (
    'md5' => '527b5b4a3cd436c70af79a763b002db8',
    'mtime' => 1548360275,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_vendor_list.php' => 
  array (
    'md5' => '6e684f1178f10958f47f7c7ab47eb123',
    'mtime' => 1548430343,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_document_status_list.php' => 
  array (
    'md5' => 'b46255f6beec635762155bb2a5ec0f07',
    'mtime' => 1548720407,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Company_Documents.php' => 
  array (
    'md5' => 'a8c8d81605c730baad8a7ba24459c36e',
    'mtime' => 1549482610,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Tradeshow_Documents.php' => 
  array (
    'md5' => 'c87a1ad66b915ad6cfe3725de1b63e57',
    'mtime' => 1550100147,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_meeting_status_dom.php' => 
  array (
    'md5' => 'c699ad472e463764ec5afb8713f17035',
    'mtime' => 1552478385,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_document_type_list.php' => 
  array (
    'md5' => '8f9b2d87b9fd2d3211dac3c0209c8678',
    'mtime' => 1554462900,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_study_coordinator_list.php' => 
  array (
    'md5' => '9274b32c94fe7e21b8f7ed9ac751a537',
    'mtime' => 1554977192,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_room_list.php' => 
  array (
    'md5' => '43cf2ba1ff9d872fcd158f398a458445',
    'mtime' => 1554977710,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_archiving_type_of_contact_list.php' => 
  array (
    'md5' => '4453deaf2ab56edbb1085418d216fbae',
    'mtime' => 1555330454,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_work_product_compliance_list.php' => 
  array (
    'md5' => '2289691d36e0df311303b67eb011e65f',
    'mtime' => 1555332295,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_department_id_list.php' => 
  array (
    'md5' => '35b4d514aaaba078818d23ce99382ba9',
    'mtime' => 1556105258,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_veterinarian_list.php' => 
  array (
    'md5' => 'f2ac1bd6ede05ff9f7e87f652c12d142',
    'mtime' => 1556799522,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_study_director_list.php' => 
  array (
    'md5' => 'b4b4f01eafebf264b1563f6417485bd2',
    'mtime' => 1557259182,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_reason_for_transfer_list.php' => 
  array (
    'md5' => '07abc3b0de00d396661cfc866c2db8f0',
    'mtime' => 1558336725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_category_id_list.php' => 
  array (
    'md5' => '1c80c35e10fd99ce87b26dda8920ba69',
    'mtime' => 1558336725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_vendor_level_list.php' => 
  array (
    'md5' => '34a4554f5587bd34b8548901529a2ba1',
    'mtime' => 1558336725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_vendor_status_list.php' => 
  array (
    'md5' => 'afa46c5adbc1a3e9e39b2cda8eb07607',
    'mtime' => 1558336725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_method_of_qualification_list.php' => 
  array (
    'md5' => '17c7374d8310c829e2fefdaa236bdd39',
    'mtime' => 1558336725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_iso_17025_units_of_measure_list.php' => 
  array (
    'md5' => '6002d290bf10692ec87f4824171f57a0',
    'mtime' => 1558336725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_company_type.php' => 
  array (
    'md5' => 'e6fc835be9ad1c202d6bcff3783d3453',
    'mtime' => 1558336725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Equipment_Facility_Records.php' => 
  array (
    'md5' => '44361e0c9075920a44b2c3ef70c6860d',
    'mtime' => 1558336725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_company_category_list.php' => 
  array (
    'md5' => '40e42946437941e5685571cde4f9e627',
    'mtime' => 1558336725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_current_vendor_status_list.php' => 
  array (
    'md5' => '7472704958ed225b0ce546797e4d7ab5',
    'mtime' => 1558336725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_company_status_list.php' => 
  array (
    'md5' => '095cada856abe308c3a5333f9ccf1c19',
    'mtime' => 1558336725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Equipment_Facility_Document.php' => 
  array (
    'md5' => 'e2c1620fbcb03a29e6d2a136b1b2894b',
    'mtime' => 1558336725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_usda_category_list.php' => 
  array (
    'md5' => '5c6f5cce8e745e305ed9ee36bd0b3fe1',
    'mtime' => 1560773447,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_iacuc.php' => 
  array (
    'md5' => '3a79b87f863b3aac9eca45c1ffd32e60',
    'mtime' => 1560883572,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_equipment_and_facility_document_type_list.php' => 
  array (
    'md5' => 'f2064a8da1018cffeb9fe934a1a824a9',
    'mtime' => 1560959354,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_address_type_list.php' => 
  array (
    'md5' => 'a9172576345c4eb05d85da9ea92fe13f',
    'mtime' => 1562155655,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Regulatory_Response_Document.php' => 
  array (
    'md5' => '4498d40977cf6ed2fa8eec92f7613a43',
    'mtime' => 1562673649,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_vendor_type_list.php' => 
  array (
    'md5' => '1ded1d1d057d7808be481b205922aa8f',
    'mtime' => 1562760118,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_binder_location_list.php' => 
  array (
    'md5' => 'dc39e13b5ea767c6a83ba34f154b0943',
    'mtime' => 1563884066,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_response_status_list.php' => 
  array (
    'md5' => '3a828d1b6c49d734b26ab5ca6ec6c1a4',
    'mtime' => 1564487119,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_anticipated_study_start_timeline_list.php' => 
  array (
    'md5' => 'f9c7305993493880524aa87aa4bb3d6c',
    'mtime' => 1564573393,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_win_probability_list.php' => 
  array (
    'md5' => '4a9ef533d04c82acb6dbaf526453e608',
    'mtime' => 1564678867,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.customm03_work_product_activities_1_calls.php' => 
  array (
    'md5' => '5efe15a18cef6883a0ea7672f6836862',
    'mtime' => 1565006593,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_USDA_Exemption.php' => 
  array (
    'md5' => 'def239b2f6ba52500c7cd923b2ae7564',
    'mtime' => 1565264751,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Contact_Documents.php' => 
  array (
    'md5' => 'cce2f340604a1d8e5b4b2a94030009ad',
    'mtime' => 1565868337,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_contact_document_list.php' => 
  array (
    'md5' => '1cd7aee9dca55f4bf5bf4e123d004fef',
    'mtime' => 1565868674,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Email_Documents.php' => 
  array (
    'md5' => '83ea0c6c306761eb397b740162b7176b',
    'mtime' => 1566905596,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_classification_list.php' => 
  array (
    'md5' => '2eee1e2398e2d1fc8dd9dbc4b50f0ee6',
    'mtime' => 1568634472,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_equipment_and_facilities_status_list.php' => 
  array (
    'md5' => '05b16ffc800ff749b0cf1d9522971169',
    'mtime' => 1568636131,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Email_Template.php' => 
  array (
    'md5' => 'cde6bfd1a7ae5c39ee0b3967b9672b15',
    'mtime' => 1568721884,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Test_System_Documents.php' => 
  array (
    'md5' => 'f0f1c46fe1a10df0eded11183bdade3a',
    'mtime' => 1569846203,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_sales_activity_quote_req_list.php' => 
  array (
    'md5' => 'cb52324e4eb40589ff35de3b06dbef5c',
    'mtime' => 1569846868,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_scientific_activity_list.php' => 
  array (
    'md5' => 'fa09f43d2c88ae91fef5e859780597fd',
    'mtime' => 1570104475,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_aaalac_exemptions_list.php' => 
  array (
    'md5' => 'e3693963e777ad9a4924d96b5bcb2cf7',
    'mtime' => 1573049432,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_vc_related_to_list.php' => 
  array (
    'md5' => 'effe05ec619a843c71b66cc236299854',
    'mtime' => 1573130839,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_error_classification_c_list.php' => 
  array (
    'md5' => '1a4c0f9b8f5ada9cbff0275c0c37374c',
    'mtime' => 1575293178,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_corrective_action_options_list.php' => 
  array (
    'md5' => '3c3d730062aabb921a44bee578a2a5a5',
    'mtime' => 1575636900,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_test_system_document_category_list.php' => 
  array (
    'md5' => '10cbe36aa95e787cfe02346f919a8884',
    'mtime' => 1576068581,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_normal_see_gas_list.php' => 
  array (
    'md5' => '4263a512191fbe5ca783330a8dece432',
    'mtime' => 1579256142,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_oral_cavity_list_not_exam.php' => 
  array (
    'md5' => '0d1a1a98404f3a6e34d4a84ba7e2c757',
    'mtime' => 1579256213,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_oral_cavity_list_normal.php' => 
  array (
    'md5' => '4a05ef7101f3966fc9a4588eef9f5282',
    'mtime' => 1579256280,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_normal_sln_list.php' => 
  array (
    'md5' => '081e1a6be44456a477b5e41823bcf51f',
    'mtime' => 1579256926,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_shipping_company_list.php' => 
  array (
    'md5' => 'b34997b8668ba20ccd5311a4ada9457c',
    'mtime' => 1580822232,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_shipping_delivery_list.php' => 
  array (
    'md5' => '0b655bfba4e2203a8d10ac9e5ce03dc6',
    'mtime' => 1580822454,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_shipping_conditions_list.php' => 
  array (
    'md5' => '7758a25636cb39a3a18b5238eba25c0c',
    'mtime' => 1580822753,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_material_category_list.php' => 
  array (
    'md5' => 'a0e6acc12e2b74024a7b3ccd840309a6',
    'mtime' => 1580823130,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_hazardous_contents_list.php' => 
  array (
    'md5' => 'f660f9ed0ca15066007d251c961e3db0',
    'mtime' => 1580995825,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_deliverable_type_list.php' => 
  array (
    'md5' => '13d100f3905aecc88c6ddbaed5de0592',
    'mtime' => 1584359304,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_quoteco_status_list.php' => 
  array (
    'md5' => 'e26a38587956b03c67ee1cf2cd37d7c4',
    'mtime' => 1591867032,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_equipment_and_facility_records_type_list.php' => 
  array (
    'md5' => 'ba43338028827aa41bcbbf1ab67d3554',
    'mtime' => 1592298285,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_why_it_happened_c_list.php' => 
  array (
    'md5' => '2ae03a3f99ecdd04ea96e1e28267b513',
    'mtime' => 1592366429,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_work_type_list.php' => 
  array (
    'md5' => '649ec21682f368d5ec59b971c18370b7',
    'mtime' => 1593079084,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_product_status_list.php' => 
  array (
    'md5' => '3000e1b6c2fff4a669c185c0c2b06740',
    'mtime' => 1597910822,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_product_notes_multiselect_list.php' => 
  array (
    'md5' => '8acc407731f054e27db8eb874ee1813c',
    'mtime' => 1597911014,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.GDP_Examples.php' => 
  array (
    'md5' => '289dbadb1f2a1614df3bd959844cd55e',
    'mtime' => 1598514455,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_prd_level_list.php' => 
  array (
    'md5' => '4cb9583c0abe00c018c4bbfb70947394',
    'mtime' => 1599557913,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Product_Document.php' => 
  array (
    'md5' => 'c4941a44cd466b54441679ac9b9d3199',
    'mtime' => 1599726233,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_purchase_unit_list.php' => 
  array (
    'md5' => 'a62a495ab4e40a65e6fd956d6149a656',
    'mtime' => 1600948057,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_ef_status_list.php' => 
  array (
    'md5' => 'd4f105a10a0094a384e0ff1f470037e4',
    'mtime' => 1601966186,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_disposition_type_deliverable_list.php' => 
  array (
    'md5' => '67c3b0006baad30bad5df3a04aba6a89',
    'mtime' => 1605600869,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_expense_or_capitalize_list.php' => 
  array (
    'md5' => '255fca2fba9e7d23813b23b91e41dd09',
    'mtime' => 1607419798,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_high_level_root_cause_action_list.php' => 
  array (
    'md5' => '790faec8e72c3647b3ba1d8246329b2c',
    'mtime' => 1608007851,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_impactful_list.php' => 
  array (
    'md5' => '98499e7b808c92f707ffb2cb011e89bd',
    'mtime' => 1608008475,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_management_assessment_train_list.php' => 
  array (
    'md5' => '0386ba1296f2c980a97e3c9c5c5e19fa',
    'mtime' => 1608008667,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_iacuc_deficiency_class_list.php' => 
  array (
    'md5' => '67a25013d6423f5f8062d2fee48af08c',
    'mtime' => 1608009011,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_wps_outcome_wpe_activities_list.php' => 
  array (
    'md5' => 'ad3a962bd30d889f60db83a11900a339',
    'mtime' => 1608013590,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_ed_type_list.php' => 
  array (
    'md5' => '05455fa6e107f13646c662f75866d8ac',
    'mtime' => 1608033933,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_enrollment_status_list.php' => 
  array (
    'md5' => 'f0f6c51b53b5d9e886460ae9e67d1afc',
    'mtime' => 1608035942,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_species_sp_list.php' => 
  array (
    'md5' => 'd86c23f8c188a29dd1f8447b53b888e3',
    'mtime' => 1610451348,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_service_type_list.php' => 
  array (
    'md5' => 'a34e9093162568f0141cfc314913ccb5',
    'mtime' => 1610451653,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_service_format_list.php' => 
  array (
    'md5' => '4f6a3b7030819a0612a5ffee94c8f85a',
    'mtime' => 1611825082,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.Controlled_Documents_QA_Review_Docs.php' => 
  array (
    'md5' => 'cffdec4d7d041c4ec3475cbeea0dea4c',
    'mtime' => 1613028945,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_stability_considerations_list.php' => 
  array (
    'md5' => 'c9779e463c570a9f80f89edceb37c831',
    'mtime' => 1613626782,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_paraffin_plastic_list.php' => 
  array (
    'md5' => '212a5837f4f8d768c2db59f6e1e0288c',
    'mtime' => 1617700752,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_test_system_list.php' => 
  array (
    'md5' => 'f50503a3e3c0a796efdc601ea1afbe38',
    'mtime' => 1621501185,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_deviation_rate_basis_list.php' => 
  array (
    'md5' => '0461180eec8162daa27408006da95b4e',
    'mtime' => 1624956270,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_final_report_timeline_type_list.php' => 
  array (
    'md5' => 'a5d5ce3af0abaa2fb140def1ee8c3863',
    'mtime' => 1626936093,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_red_yellow_green_list.php' => 
  array (
    'md5' => 'db12fe8852f510bc527770916fa7db97',
    'mtime' => 1626944982,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_category_list.php' => 
  array (
    'md5' => '0564405d1691352deb6efc9c39525c7e',
    'mtime' => 1628146974,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_minutes_per_slide_list.php' => 
  array (
    'md5' => '2ab7537350af515229d9cdca6a089745',
    'mtime' => 1628758936,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_procedure_room_list.php' => 
  array (
    'md5' => '4b289350b8bc34f6aff4330ac660f0bd',
    'mtime' => 1628836477,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_action_needed_list.php' => 
  array (
    'md5' => 'c84fb72d52e85a0b78316452c4e392d8',
    'mtime' => 1634194533,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_grant_submission_sa_2_list.php' => 
  array (
    'md5' => 'a4534a916c87f4811a2b643851f8fcdd',
    'mtime' => 1635239007,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_reason_for_expansion_list.php' => 
  array (
    'md5' => 'dd4c097f05c413501f2317ae5ab54d0d',
    'mtime' => 1635239205,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_gd_study_article_type_list.php' => 
  array (
    'md5' => '6d55696dd6f3a56f5bd3cda0abee321c',
    'mtime' => 1635411073,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_stability_considerations_ii_list.php' => 
  array (
    'md5' => 'ad9b8c8458864f66d5dca777f0cf4ba7',
    'mtime' => 1636456211,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_yes_no_na_list.php' => 
  array (
    'md5' => 'd83379a10641df327385f33bac235c77',
    'mtime' => 1636456602,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_inventory_item_category_list.php' => 
  array (
    'md5' => '27ae35574a0a8202f88d3738a69207b4',
    'mtime' => 1636503980,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_inventory_item_owner_list.php' => 
  array (
    'md5' => 'd04bb2355f56b01c689227f708cc30df',
    'mtime' => 1636504316,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_im_location_type_list.php' => 
  array (
    'md5' => 'e72d8decc706873b85440c38ca55c083',
    'mtime' => 1636505452,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_inventory_item_status_list.php' => 
  array (
    'md5' => 'd3f22e19450f8c637db93a5a92a353f8',
    'mtime' => 1636506083,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_timepoint_type_list.php' => 
  array (
    'md5' => '7662e986cfed828767a3824228350dc2',
    'mtime' => 1636510895,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_inventory_item_storage_condition_list.php' => 
  array (
    'md5' => 'fde3f4edc2bdfabd82fdc85cda2d670f',
    'mtime' => 1636511655,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_yes_no_list.php' => 
  array (
    'md5' => 'c3491d7d18584403363ae09f4f16e5d5',
    'mtime' => 1636553499,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_aaalac_and_usda_exemptions_list.php' => 
  array (
    'md5' => 'f5b96a1f7abec488bede882f619a4b13',
    'mtime' => 1637230763,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_tsd_wt_type_list.php' => 
  array (
    'md5' => 'f8cfe32af4d3847d6d957a93034405d8',
    'mtime' => 1638275421,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_td_phase_list.php' => 
  array (
    'md5' => 'a47ade26c1baa8fb11ac1b4d248d6486',
    'mtime' => 1638437574,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_procedure_type_com_list.php' => 
  array (
    'md5' => '86b3c6431f45f1f19303fee5795e40ae',
    'mtime' => 1638867775,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_TSD_purpose_list.php' => 
  array (
    'md5' => '5b02edf6a3be0f09b8c28cba4f676ca7',
    'mtime' => 1638869661,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_housing_requirements_list.php' => 
  array (
    'md5' => '027580f6dd3edb33837a3d24baf97001',
    'mtime' => 1638870931,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_number_list.php' => 
  array (
    'md5' => '93e10b8e1086249743319603ca01b76c',
    'mtime' => 1638873166,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_sales_study_article_status_list.php' => 
  array (
    'md5' => 'b48fd3df87f5aa130b09f9a3d2a6d2a0',
    'mtime' => 1638880076,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_degree_of_immobility_list.php' => 
  array (
    'md5' => '813a6540271976e55c9279558f984284',
    'mtime' => 1639472920,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_type_of_acclimation_list.php' => 
  array (
    'md5' => '0f00b3f936960d72405c1a82b4157fc3',
    'mtime' => 1639472981,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_inspection_results_list.php' => 
  array (
    'md5' => '4c2e5f7c1b8c09060da1ff8d85eed5dc',
    'mtime' => 1640075559,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_or_status_list.php' => 
  array (
    'md5' => 'd2963100a7dce372a1a526763fa86828',
    'mtime' => 1640676035,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_tpr_reference_list.php' => 
  array (
    'md5' => 'de511cdd6102af33746812a5799dd028',
    'mtime' => 1641537753,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_inventory_item_type_list.php' => 
  array (
    'md5' => 'cb48ed5546be52136e7bff288117395b',
    'mtime' => 1642489495,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_universal_inventory_management_type_list.php' => 
  array (
    'md5' => 'b35a102f4b5c9f39ee2ab772379181fa',
    'mtime' => 1642490314,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_type_specimen_list.php' => 
  array (
    'md5' => '2b1f82dea0be3cc8653e4b25753e1066',
    'mtime' => 1643102231,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_phase_of_inspection_list.php' => 
  array (
    'md5' => '7dcdf249f964f6eaa31accaa3d09860f',
    'mtime' => 1643700660,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_room_type_list.php' => 
  array (
    'md5' => '93765903903df12258f3764d068ddab0',
    'mtime' => 1643701129,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.CAPA_Files.php' => 
  array (
    'md5' => '1be7fe526b9cb72bf8b670bab241b18c',
    'mtime' => 1643872463,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_ca_andor_pa_dd_list.php' => 
  array (
    'md5' => '8bb0fa8705e483fca18e1df1b3e607e3',
    'mtime' => 1643875795,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_frequency_score_list.php' => 
  array (
    'md5' => '715cf917588dcbdba2e6edf65402cd10',
    'mtime' => 1643876686,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_severity_score_list.php' => 
  array (
    'md5' => '122766d9d6ac931c08e85959665e31b2',
    'mtime' => 1643876846,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_GD_status_list.php' => 
  array (
    'md5' => 'a7643889df411a8eeb6f1fa755081850',
    'mtime' => 1643881434,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_task_type_list.php' => 
  array (
    'md5' => '31d1af5f2cec0e7a148d5cf75993ff3f',
    'mtime' => 1643882359,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_ot_category_list.php' => 
  array (
    'md5' => '013f9234fb94b5661877400bea8cc3e2',
    'mtime' => 1644306090,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_ot_type_list.php' => 
  array (
    'md5' => '5b6229b22b4be37bb2ac0cc6847bbc77',
    'mtime' => 1644306166,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_organ_system_list.php' => 
  array (
    'md5' => '5f3e8b86777e1e3a522f8832ec806b8b',
    'mtime' => 1645082236,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_po_status_list.php' => 
  array (
    'md5' => '18ace421745b1324d13c3790bba55e40',
    'mtime' => 1645513654,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_deliverable_status_list.php' => 
  array (
    'md5' => 'ba66a7dfe940af73fc50d23ec8418268',
    'mtime' => 1645515388,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_ii_related_to_c_list.php' => 
  array (
    'md5' => '2464bf1068b64a975b98b2a7d5809e62',
    'mtime' => 1645691005,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_specimen_type_list.php' => 
  array (
    'md5' => '842770c23b8e4d57ea9c48c90f813c7d',
    'mtime' => 1645710876,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_specimen_type_no_other_c_list.php' => 
  array (
    'md5' => 'd27d8633b73526d1624cd54a31b4e5cf',
    'mtime' => 1645710968,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_integration_work_stream_list.php' => 
  array (
    'md5' => '6a6e2282d9aaeeb9dde5923862651075',
    'mtime' => 1647329016,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_plan_actual_list.php' => 
  array (
    'md5' => '672eb54fd9fe603def38182b55891f65',
    'mtime' => 1647329467,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_td_audit_phase_list.php' => 
  array (
    'md5' => '3e215929d7a447c141aaafd1410d7206',
    'mtime' => 1648111997,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_sale_document_category_list.php' => 
  array (
    'md5' => 'e2dd698a8b9ddd47c5f607fd3f8187f4',
    'mtime' => 1648727216,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_product_category_list.php' => 
  array (
    'md5' => '42fe3a34247aec56f83b85559ed0a49d',
    'mtime' => 1648728291,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_Breed_List.php' => 
  array (
    'md5' => 'ec5636383f2ea1a448fc7762b18b1d78',
    'mtime' => 1649143292,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_ii_test_type_list.php' => 
  array (
    'md5' => '61019cdcc55e76582570a4faaa202f36',
    'mtime' => 1649754237,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_task_procedure_list.php' => 
  array (
    'md5' => '3a946c112c1ee3e2ce8757c1567d2926',
    'mtime' => 1650530004,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_bid_status_list.php' => 
  array (
    'md5' => '43891b9255aa22456e07efef8418a533',
    'mtime' => 1650956950,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_biod_dose_route_list.php' => 
  array (
    'md5' => 'b24baf82a97917fb03c2909584cfafdf',
    'mtime' => 1650957456,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_inventory_management_type_list.php' => 
  array (
    'md5' => 'f56a9fd88ef1fc95d5ec22906a58fcc3',
    'mtime' => 1652944904,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_wpc_type_list.php' => 
  array (
    'md5' => '934b0126055d992d74fc1db1ab7c0a85',
    'mtime' => 1653375918,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_work_product_status_list.php' => 
  array (
    'md5' => '9191897a3c9d6e31e6dd99363b277cab',
    'mtime' => 1653376538,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_aaalac_requirements_list.php' => 
  array (
    'md5' => 'ca05385774a09b5ebf9f5dccf423ff31',
    'mtime' => 1654579535,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_usda_exemptions_list.php' => 
  array (
    'md5' => 'd0b814955b77707e419fa5d93a00487f',
    'mtime' => 1654579774,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_department_list.php' => 
  array (
    'md5' => '7a53c4deb2ee501b84720e4f78d5b6ff',
    'mtime' => 1655358263,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_location_list.php' => 
  array (
    'md5' => '924c33e8f1331adf7f6ba8f28dbf215f',
    'mtime' => 1655792800,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_extraction_conditions_list.php' => 
  array (
    'md5' => '96d9330fb9082c0e1856fd46f036a6b2',
    'mtime' => 1657618161,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_payment_terms_list.php' => 
  array (
    'md5' => 'd8deb19b447f4df96a04802271fdca6b',
    'mtime' => 1658990483,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_error_category_list.php' => 
  array (
    'md5' => '0206ddd998916183628b2e113b07a017',
    'mtime' => 1664862473,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_deliverable_list.php' => 
  array (
    'md5' => '6dcf4529577294cc7c53910e26326b84',
    'mtime' => 1666701722,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_equipment_required_list.php' => 
  array (
    'md5' => 'be630d09d0e2e50248613a0a45f4f62e',
    'mtime' => 1666849962,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_completion_status_list.php' => 
  array (
    'md5' => 'bb83180b122c372f53ee544d791c5e4d',
    'mtime' => 1667886252,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_standard_task_list.php' => 
  array (
    'md5' => '7d5c3dbc1584dcdc9c4746c2a541b0b4',
    'mtime' => 1667886842,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_diagnosis_list.php' => 
  array (
    'md5' => '7fbb47646102b365642c6010a44073a5',
    'mtime' => 1667898727,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_condition_list.php' => 
  array (
    'md5' => 'c16023685016e0923186769ac62ce242',
    'mtime' => 1667898807,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_amendment_request_list.php' => 
  array (
    'md5' => '923df6cf660536b9ed457c9eceebcabe',
    'mtime' => 1669873030,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/lv_LV.sugar_error_type_list.php' => 
  array (
    'md5' => '5a8780b186f79a75055d566b7a50e4c0',
    'mtime' => 1669887997,
    'is_override' => false,
  ),
);