<?php
 // created: 2022-01-18 07:18:34

$app_list_strings['universal_inventory_management_type_list']=array (
  '' => '',
  'Accessory Article' => 'Accessory Article',
  'Accessory Product' => 'Accessory Product',
  'Autoclave supplies' => 'Autoclave supplies',
  'Balloon Catheter' => 'Balloon Catheter',
  'Bandaging' => 'Bandaging',
  'Block' => 'Block',
  'Blood draw supply' => 'Blood draw supply',
  'Cannula' => 'Cannula',
  'Catheter' => 'Catheter',
  'Chemical' => 'Chemical',
  'Cleaning Agent' => 'Cleaning Agent',
  'Cleaning Supplies' => 'Cleaning Supplies',
  'Contrast' => 'Contrast',
  'Control Article' => 'Control Article',
  'Culture' => 'Culture',
  'Data Book' => 'Data Book',
  'Data Sheet' => 'Data Sheet',
  'Drug' => 'Drug',
  'EDTA Plasma' => 'EDTA Plasma',
  'Endotracheal TubeSupplies' => 'Endotracheal Tube/Supplies',
  'Enrichment Toy' => 'Enrichment Toy',
  'Equipment' => 'Equipment',
  'Equipment Facility Record' => 'Equipment / Facility Record',
  'ETO supplies' => 'ETO supplies',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Feeding supplies' => 'Feeding supplies',
  'Graft' => 'Graft',
  'Hard drive' => 'Hard drive',
  'Hard Drive' => 'Hard Drive',
  'Inflation Device' => 'Inflation Device',
  'NA Heparin Plasma' => 'NA Heparin Plasma',
  'NaCit Plasma' => 'NaCit Plasma',
  'Office Supply' => 'Office Supply',
  'Other' => 'Other',
  'Protocol Book' => 'Protocol Book',
  'Reagent' => 'Reagent',
  'Serum' => 'Serum',
  'Sheath' => 'Sheath',
  'Shipping supplies' => 'Shipping supplies',
  'Slide' => 'Slide',
  'Solution' => 'Solution',
  'Stent' => 'Stent',
  'Surgical Instrument' => 'Surgical Instrument',
  'Surgical site prep' => 'Surgical site prep',
  'Suture' => 'Suture',
  'Test Article' => 'Test Article',
  'Tissue' => 'Tissue',
  'Tubing' => 'Tubing',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
  'Wire' => 'Wire',
  'Balloons' => 'Balloons',
);