<?php
 // created: 2022-06-07 05:29:33

$app_list_strings['usda_exemptions_list']=array (
  '' => '',
  'Conscious Restraint' => 'Conscious Restraint',
  'Canine Exercise Restrictions' => 'Canine Exercise Restrictions',
  'FoodWater Restriction' => 'Food/Water Restriction (aside from approved fasting)',
  'Multiple Major Survival Surgeries on a Single Protocol' => 'Multiple Major Survival Surgeries on a Single Protocol',
  'Survival Surgeries on Multiple Protocols' => 'Survival Surgeries on Multiple Protocols',
  'None' => 'None',
);