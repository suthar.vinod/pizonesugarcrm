<?php
 // created: 2016-03-28 21:13:49

$app_list_strings['sales_activity_status_list']=array (
  'select' => 'Select...',
  'open' => 'Open',
  'won' => 'Won',
  'closed' => 'Closed',
  'duplicate' => 'Duplicate',
);