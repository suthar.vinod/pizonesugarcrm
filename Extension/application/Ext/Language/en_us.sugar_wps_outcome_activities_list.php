<?php
 // created: 2021-02-17 16:54:48

$app_list_strings['wps_outcome_activities_list']=array (
  '' => '',
  'Invasive Procedure' => 'Invasive Procedure',
  'Study Article Exposure' => 'Study Article Exposure',
  'Study Specific Data Recorded' => 'Study Specific Data Recorded',
  'Only Other Protocol Activities' => 'Other Protocol Activities',
  'Performed Per Protocol' => 'Performed Per Protocol',
  'Preventative Health Care' => 'Preventative Health Care',
  'Vet Order' => 'Vet Order',
);