<?php
 // created: 2019-10-03 12:07:55

$app_list_strings['scientific_activity_list']=array (
  '' => '',
  'Protocol Development' => 'Protocol Development',
  'Operations Oversight' => 'Operations Oversight',
  'Data Entry_Analysis' => 'Data Entry & Analysis',
  'Reporting' => 'Reporting',
  'Procedure_Assignment' => 'Procedure Assignment',
  'Conference Attendance' => 'Conference Attendance',
  'Out of Office' => 'Out of Office',
);