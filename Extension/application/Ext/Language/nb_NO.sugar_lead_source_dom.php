<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Cold call',
  'Existing Customer' => 'Eksisterende kunde',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Medarbeider',
  'Conference' => 'Konferanse',
  'Trade Show' => 'Varemesse',
  'Web Site' => 'Webside',
  'Email' => 'Vilkårlig e-postadresse',
  'Campaign' => 'Kampanje',
  'Support Portal User Registration' => 'Support Portal Brukerregistrering',
  'Other' => 'Andre',
  'Referral' => 'Referral',
);