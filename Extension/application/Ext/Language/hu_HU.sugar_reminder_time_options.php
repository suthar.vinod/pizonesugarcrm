<?php
 // created: 2017-06-16 19:19:41

$app_list_strings['reminder_time_options']=array (
  -1 => 'Egyik sem',
  60 => '1 perccel ezelőtt',
  300 => '5 perccel ezelőtt',
  600 => '10 perccel ezelőtt',
  900 => '15 perccel ezelőtt',
  1800 => '30 perccel ezelőtt',
  3600 => '1 órával ezelőtt',
  7200 => '2 órával ezelőtt',
  10800 => '3 órával ezelőtt',
  18000 => '5 órával ezelőtt',
  86400 => '1 nappal ezelőtt',
  604800 => '7 days prior',
);