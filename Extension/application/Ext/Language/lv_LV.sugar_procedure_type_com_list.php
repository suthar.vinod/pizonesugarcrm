<?php
 // created: 2021-12-07 09:02:55

$app_list_strings['procedure_type_com_list']=array (
  '' => '',
  'Initial' => 'Initial',
  'Follow up' => 'Follow-up',
  'Termination' => 'Termination',
  'Acute' => 'Acute',
  'Screening' => 'Screening',
);