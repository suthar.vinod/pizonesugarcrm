<?php
 // created: 2019-12-06 12:55:00

$app_list_strings['corrective_action_options_list']=array (
  '' => '',
  'None' => 'None',
  'Other' => 'Other',
  'Protocol Amendment' => 'Protocol Amendment',
  'Protocol Specific Training' => 'Protocol Specific Training',
  'SOP Update' => 'SOP Update',
  'Protocol Requirement Review' => 'Protocol Requirement Review',
  'Further Investigation of Event' => 'Further Investigation of Event',
  'Report Amendment' => 'Report Amendment',
);