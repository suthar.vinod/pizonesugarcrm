<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Cold Call',
  'Existing Customer' => 'Existing Customer',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Employee',
  'Conference' => 'Conference',
  'Trade Show' => 'Trade Show',
  'Web Site' => 'Web Site',
  'Email' => 'Any Email',
  'Campaign' => 'Campaign',
  'Support Portal User Registration' => 'Support Portal User Registration',
  'Other' => 'Other',
  'Referral' => 'Referral',
);