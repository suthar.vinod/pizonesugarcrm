<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'المكالمة التسويقية',
  'Existing Customer' => 'العميل الحالي',
  'Self Generated' => 'منشأة ذاتيًا',
  'Employee' => 'الموظف',
  'Conference' => 'المؤتمر',
  'Trade Show' => 'العرض التجاري',
  'Web Site' => 'موقع الويب',
  'Email' => 'البريد الإلكتروني',
  'Campaign' => 'الحملة',
  'Support Portal User Registration' => 'دعم تسجيل مستخدم البوابة',
  'Other' => 'أخرى',
  'Referral' => 'Referral',
);