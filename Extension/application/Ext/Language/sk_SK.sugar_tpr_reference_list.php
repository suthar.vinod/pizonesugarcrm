<?php
 // created: 2022-01-07 06:42:33

$app_list_strings['tpr_reference_list']=array (
  '' => '',
  'Clinical Observations Form' => 'F-S-IL-GN-OP-001-01 – Clinical Observations Form',
  'PARPPC Observations' => 'F-S-IL-GN-OP-001-02 – PAR-PPC Observations – Multiple Incision Form',
  'Anesthesia Recovery Monitoring' => 'F-S-IL-GN-OP-007-01 - Anesthesia Recovery Monitoring',
  'Animal Prep Form' => 'F-S-IL-GN-OP-013-01 – Animal Prep Form',
  'MultiAnimal Procedure Prep Form' => 'F-S-IL-GN-OP-013-02 – Multi-Animal Procedure Prep Form',
  'MultiAnimal Recovery Form' => 'F-S-IL-GN-OP-068-01 - Multi-Animal Recovery Form',
  'Physical Examination Form' => 'F-S-IL-GN-VT-001-01 – Physical Examination Form',
);