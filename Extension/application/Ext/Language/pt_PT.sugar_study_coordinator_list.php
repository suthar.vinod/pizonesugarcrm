<?php
 // created: 2019-04-11 10:06:32

$app_list_strings['study_coordinator_list']=array (
  'Emily Visedo' => 'Emily Visedo',
  'DeeAnn Deiss' => 'DeeAnn Deiss',
  'Liz Roby' => 'Liz Roby',
  'Tom Van Valkenburg' => 'Tom Van Valkenburg',
  'Choose One' => 'Choose One',
  'Ben Vos' => 'Ben Vos',
  'Emma Squires Sperling' => 'Emma Squires-Sperling',
  'Anton Crane' => 'Anton Crane',
  'Liz Clark' => 'Liz Clark',
  'Amy Puetz' => 'Amy Puetz',
  'Erica VanReeth' => 'Erica VanReeth',
  'Dan Kroeninger' => 'Dan Kroeninger',
  'Kerry Trotter' => 'Kerry Trotter',
  'Hannah Schmidt' => 'Hannah Schmidt',
  'Steven Mamer' => 'Steven Mamer',
  'Emma Reiss' => 'Emma Reiss',
  'Kailyn Singh' => 'Kailyn Singh',
  'Katelyn Mortensen' => 'Katelyn Mortensen',
);