<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Telemarketing',
  'Existing Customer' => 'Istniejący kontrahent',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Pracownik',
  'Conference' => 'Uczestnik konferencji',
  'Trade Show' => 'Uczestnik prezentacji',
  'Web Site' => 'Ze strony WWW',
  'Email' => 'E-mail',
  'Campaign' => 'Kampania',
  'Support Portal User Registration' => 'Rejestracja użytkownika portalu wsparcia',
  'Other' => 'Inne',
  'Referral' => 'Referral',
);