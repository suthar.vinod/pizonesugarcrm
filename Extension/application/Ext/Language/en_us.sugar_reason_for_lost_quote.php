<?php
 // created: 2020-05-13 17:08:00

$app_list_strings['reason_for_lost_quote']=array (
  '' => '',
  'no response from client' => 'No Response from Client',
  'lab proximity to company' => 'Lab Proximity to Company',
  'lower quote from competitive lab' => 'Lower Quote from Competitive Lab',
  'scheduling issue' => 'Scheduling Issue',
  'experience' => 'Lab Experience',
  'lack of required equipment' => 'Lack of Required Equipment',
  'lack of timely response' => 'Lack of Timely Response',
  'report timelines' => 'Report Timelines',
  'capacity' => 'Capacity',
  'WL' => 'WL',
  'Chose another lab' => 'Selected a Different Lab',
);