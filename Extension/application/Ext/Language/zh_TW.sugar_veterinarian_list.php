<?php
 // created: 2019-05-02 12:18:42

$app_list_strings['veterinarian_list']=array (
  'ebauer' => 'Emily Drake Bauer',
  'jvislisel' => 'Joe Vislisel',
  'apinto' => 'Alejandra Pinto',
  'None' => 'none',
  'Choose Veterinarian' => 'Choose Veterinarian...',
  'cnorman' => 'Claire Norman',
  'lszenay' => 'Lesley Szenay',
  'tpavek' => 'Todd Pavek',
);