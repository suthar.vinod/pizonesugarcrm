<?php
 // created: 2018-11-29 17:41:51

$app_list_strings['histopath_processing_type_list']=array (
  '' => '',
  'Paraffin' => 'Paraffin',
  'Plastic' => 'Plastic',
  'Frozen' => 'Frozen',
  'SEM' => 'SEM',
  'Faxitron' => 'Faxitron',
);