<?php
 // created: 2017-04-13 19:17:47

$app_list_strings['invoice_installment_percent_list']=array (
  'Choose One' => 'Choose One',
  'None' => 'None',
  10 => '10%',
  25 => '25%',
  40 => '40%',
  50 => '50%',
  60 => '60%',
  100 => '100%',
  '1x Monthly' => '1x Monthly',
  75 => '75%',
);