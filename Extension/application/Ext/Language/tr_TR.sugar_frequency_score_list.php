<?php
 // created: 2022-02-03 08:24:46

$app_list_strings['frequency_score_list']=array (
  '' => '',
  'NA' => 'NA',
  1 => '1 - Rarely',
  2 => '2 - Occasionally',
  3 => '3 - Frequently',
);