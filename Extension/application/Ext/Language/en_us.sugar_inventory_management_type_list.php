<?php
 // created: 2022-05-19 07:21:44

$app_list_strings['inventory_management_type_list']=array (
  '' => '',
  'Archive Offsite' => 'Archive Offsite',
  'Archive Onsite' => 'Archive Onsite',
  'Create Inventory Collection' => 'Create Inventory Collection',
  'Discard' => 'Discard',
  'Exhausted' => 'Exhausted',
  'External Transfer' => 'External Transfer',
  'Internal Transfer Analysis' => 'Internal Transfer - Analysis',
  'Internal Transfer' => 'Internal Transfer - Storage',
  'Missing' => 'Missing',
  'Obsolete' => 'Obsolete',
  'Separate Inventory Items' => 'Separate Inventory Collection Item(s)',
);