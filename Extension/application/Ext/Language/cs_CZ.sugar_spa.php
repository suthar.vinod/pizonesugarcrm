<?php
 // created: 2019-01-24 20:04:35

$app_list_strings['spa']=array (
  'Sponsor Submitted' => 'Sponsor Submitted',
  'APS Reviewed' => 'APS Reviewed',
  'Sponsor Finalized' => 'Sponsor Finalized',
  'APS Approved' => 'APS Approved',
  'Choose One' => 'Choose One',
  'Waiting on Payment' => 'Waiting on Payment',
);