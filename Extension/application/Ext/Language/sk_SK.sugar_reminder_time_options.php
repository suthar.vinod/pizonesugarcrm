<?php
 // created: 2017-06-16 19:19:43

$app_list_strings['reminder_time_options']=array (
  -1 => 'Žiadne',
  60 => 'Pred 1 minútou',
  300 => 'Pred 5 minútami',
  600 => 'Pred 10 minútami',
  900 => 'Pred 15 minútami',
  1800 => 'Pred 30 minútami',
  3600 => 'Pred 1 hodinou',
  7200 => 'Pred 2 hodinami',
  10800 => 'Pred 3 hodinami',
  18000 => 'Pred 5 hodinami',
  86400 => 'Pred 1 dňom',
  604800 => '7 days prior',
);