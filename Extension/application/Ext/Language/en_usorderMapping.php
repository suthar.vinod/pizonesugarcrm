<?php
// created: 2022-12-11 04:10:08
$extensionOrderMap = array (
  'custom/Extension/application/Ext/Language/en_us.sugar_projects_priority_options.php' => 
  array (
    'md5' => 'd71aabfa9b6161678b0bff832a9b0c68',
    'mtime' => 1452047630,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_workproductcategory.php' => 
  array (
    'md5' => '92dc53e909a09ca4255d52d9084291c8',
    'mtime' => 1452052490,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_work_product_type_list.php' => 
  array (
    'md5' => '965708149d8e8dbe63b0200f54059ab2',
    'mtime' => 1452052928,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_task_name_list.php' => 
  array (
    'md5' => 'e7603d13824c3ec504477a62f528fd69',
    'mtime' => 1452121526,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_altered_bid_terms_list.php' => 
  array (
    'md5' => '3678fdfca53b8701ecfe613a6a89b449',
    'mtime' => 1452178584,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_lead_source_dom.php' => 
  array (
    'md5' => 'fbe15560b17eff3e35f05ae67a8848e8',
    'mtime' => 1452182133,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_job_function_list.php' => 
  array (
    'md5' => '4c33d8c6ba9931fdc730275e79fc1268',
    'mtime' => 1452189041,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_specialty_list.php' => 
  array (
    'md5' => '9b926f5373c7cd8debd6b3eda97bc9bf',
    'mtime' => 1452195119,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Work_Product_Personnel.php' => 
  array (
    'md5' => '2f345df2ef9686fb27c8c048a4f4fd0d',
    'mtime' => 1452545762,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_sales_activity_stage_list.php' => 
  array (
    'md5' => '59277faaf0fcfdc4ca9f72264c531b73',
    'mtime' => 1452725055,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Sales.php' => 
  array (
    'md5' => '39c609339441a6781dfd01d09e098fa4',
    'mtime' => 1452952748,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_SalesActivityCategory.php' => 
  array (
    'md5' => '9dc355c4063ec7b71127f69ac2ce8bcf',
    'mtime' => 1453342834,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Sales_Activity_Division_Department.php' => 
  array (
    'md5' => '02b2db0410e6ba9fbf12c367d39dc56d',
    'mtime' => 1453346745,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Work_Products.php' => 
  array (
    'md5' => 'd54faf4795a6636f5680f3c28e46382b',
    'mtime' => 1453652556,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Common.php' => 
  array (
    'md5' => '48aeb19f8dbfd5c18a16106def843191',
    'mtime' => 1453669489,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Work_Product_Management.php' => 
  array (
    'md5' => '833738e099dd99132b49ec070b0ca463',
    'mtime' => 1453672484,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Sales_Management.php' => 
  array (
    'md5' => 'd87287af43830186a2f9bdfbcd2133a3',
    'mtime' => 1453686685,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Errors.php' => 
  array (
    'md5' => '3d4664994a5edfad949af7ae0575e6ac',
    'mtime' => 1455654752,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_document_template_type_dom.php' => 
  array (
    'md5' => '90417d7e2d7bc0de60cb3bb75aec06e1',
    'mtime' => 1456171519,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_document_status_dom.php' => 
  array (
    'md5' => '0e566a2297df24aabe12d8234ed7ce82',
    'mtime' => 1456258072,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_internal_department_list.php' => 
  array (
    'md5' => '38c3a69a60a0f58b527ca7a6f2c8dc5f',
    'mtime' => 1456769659,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_pathologist_list.php' => 
  array (
    'md5' => '515e49cb9b4ec72f8b0fcbda584338e0',
    'mtime' => 1456870469,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_task_status_dom.php' => 
  array (
    'md5' => 'b1860c0625b9827f9be8467de56699d8',
    'mtime' => 1457114761,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_regulatory.php' => 
  array (
    'md5' => '1871d867332d68fec26dd75440120e04',
    'mtime' => 1457381929,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_Division_list.php' => 
  array (
    'md5' => 'ca2e34cc9d908a2e9d4f93e267188e05',
    'mtime' => 1457382289,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_Department_list.php' => 
  array (
    'md5' => 'c4e4125f6586daae172b5def80cf325f',
    'mtime' => 1457382378,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_sales_activity_status_list.php' => 
  array (
    'md5' => '2cd00a8c949a480c818d26cc779b21e9',
    'mtime' => 1459199629,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_sales_activity_type_list.php' => 
  array (
    'md5' => 'bca703bf68d3593dddd7faad159b5b90',
    'mtime' => 1459705103,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_department_manager_approval_list.php' => 
  array (
    'md5' => 'd967fff8adb95735f319c797d3dd7493',
    'mtime' => 1460411892,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_study.php' => 
  array (
    'md5' => '29ef975c4826e4587719013867c36d2c',
    'mtime' => 1460471108,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_invoicing.php' => 
  array (
    'md5' => 'a68420c48380964d7bdfc0a32df73d04',
    'mtime' => 1460473174,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_study_pathologist_list.php' => 
  array (
    'md5' => '68581b61a5e9e2170ab7d8604523d4d3',
    'mtime' => 1461083311,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_Med_Device_Category_and_Contact.php' => 
  array (
    'md5' => '8643ff2ceb29661f1f0353abc3ba3fe9',
    'mtime' => 1461684030,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_patient_contact_duration_list.php' => 
  array (
    'md5' => '4e9738b5d229c9ad9814d258f49d6779',
    'mtime' => 1461686352,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_followup_status_list.php' => 
  array (
    'md5' => '2939f7bdf4c6fd3106aa0b536448fe80',
    'mtime' => 1463797916,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_quote_status_list.php' => 
  array (
    'md5' => '785add159c3fce04015a583ae10985e1',
    'mtime' => 1470759100,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_animal_model_list.php' => 
  array (
    'md5' => '4fca27887ad575995aff6b84eac6660f',
    'mtime' => 1471644014,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_device_type_list.php' => 
  array (
    'md5' => '4688ac6ea93ebf7bb0f28f5572750cfd',
    'mtime' => 1471644582,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_deliverable_owner_list.php' => 
  array (
    'md5' => 'b0936b148254cfb596badb4eb5f033f5',
    'mtime' => 1479760638,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_signed_quote_c_list.php' => 
  array (
    'md5' => '88c3a482f523828a2edb124cfd775616',
    'mtime' => 1485188418,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_estimated_hours_list.php' => 
  array (
    'md5' => '5552eaf0d01f176eba1c095f34884d87',
    'mtime' => 1486678995,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Time_Keeping.php' => 
  array (
    'md5' => '57bae22b86b2f434f829cc51673b14dc',
    'mtime' => 1487310199,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_primary_aps_operator_list.php' => 
  array (
    'md5' => '47374dfb8ec56fad865c95a6d4daa0de',
    'mtime' => 1489504759,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_product_type_list.php' => 
  array (
    'md5' => '6eba4ce913391b4b85dbbd33cad1a45c',
    'mtime' => 1489607385,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_Product_Contact_Category.php' => 
  array (
    'md5' => '5a190417c760407751be94e698af7a8b',
    'mtime' => 1489607481,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_contact_duration_list.php' => 
  array (
    'md5' => '41fb6665f6b7094cd894a3af595f650b',
    'mtime' => 1489608636,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_contact_type_list.php' => 
  array (
    'md5' => '6f6e4176083a95d4acc64335b9b4d145',
    'mtime' => 1489609950,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_Cytotoxicity_Testing.php' => 
  array (
    'md5' => '317c6c8e77879092525fd048fc84aca9',
    'mtime' => 1489611458,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_activity_list.php' => 
  array (
    'md5' => '177c66e0fc67831628024aafd965f3c0',
    'mtime' => 1490044257,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_business_develop_activity_list.php' => 
  array (
    'md5' => 'a9a02473e4d6488c484f550ec7353fab',
    'mtime' => 1493742619,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_quality_assurance_activity_list.php' => 
  array (
    'md5' => '7e4f609c40e136c78fb3c993abeba6aa',
    'mtime' => 1493744562,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_regulatory_activity_list.php' => 
  array (
    'md5' => 'd94540fbf61631732cf673b34a6be802',
    'mtime' => 1493745908,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Activity_Notes.php' => 
  array (
    'md5' => '3c730f3e4f2cc1f45a010d0c520f6397',
    'mtime' => 1494540242,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_activity_personnel_list.php' => 
  array (
    'md5' => '65a9305fe9d9d37fe9610d8aa69dbadf',
    'mtime' => 1494618166,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Quality_Assurance.php' => 
  array (
    'md5' => 'a356fde6f1cfd7637a8309d2e4c20a2f',
    'mtime' => 1496861714,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_new_deliverable_c_list.php' => 
  array (
    'md5' => 'b5bfb71479b4b6e9bfd8a68041b882cc',
    'mtime' => 1497042103,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_pathology_activity_list.php' => 
  array (
    'md5' => '3b052ce52206c48e9b043573fb849234',
    'mtime' => 1497042654,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_reminder_time_options.php' => 
  array (
    'md5' => '26c1f889f7a360b4392dae5ecd652f2d',
    'mtime' => 1497640783,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_analytical_equipment_list_list.php' => 
  array (
    'md5' => '8207bcffd5ff78525f4b152bc9b91170',
    'mtime' => 1497647888,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.HR_Management.php' => 
  array (
    'md5' => '843c43d50a8bce60c6354539ac63c21c',
    'mtime' => 1500561435,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_job_category_list.php' => 
  array (
    'md5' => '6b9875976d4541ddcb49002bf1a154ce',
    'mtime' => 1500569095,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.IntelliDocs.php' => 
  array (
    'md5' => '4064dda3524a4852494925bbc96874cc',
    'mtime' => 1504209538,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Animals.php' => 
  array (
    'md5' => '9bac07f5a7ba281e7b140501a9b619c2',
    'mtime' => 1505227124,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Work_Product_Enrollment.php' => 
  array (
    'md5' => '7becf5ac7eb3e92fde2313d9b2b6263c',
    'mtime' => 1505227331,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.AnimalsModuleCustomizations.php' => 
  array (
    'md5' => 'd0b264e489caec41dcba23e868b8ef2c',
    'mtime' => 1505227918,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.WPEsModuleCustomizations.php' => 
  array (
    'md5' => '0791c9701e081c9361c20ab3a7a9e55c',
    'mtime' => 1505228202,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.customa1a_critical_phase_inspectio_activities_1_calls.php' => 
  array (
    'md5' => 'f7d0fa92a056f2b3e07a839492c5bd2d',
    'mtime' => 1505315712,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Tradeshow_Management.php' => 
  array (
    'md5' => '65d8bafbe4f1382a5116ab86da16cdd4',
    'mtime' => 1516224779,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_sales_focus_list.php' => 
  array (
    'md5' => 'c1c2f1b0879e9a835ea6f2020dbe3eac',
    'mtime' => 1516229438,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_primary_activity_list.php' => 
  array (
    'md5' => 'c3c85c5a989eb57972bb72b134f1a4fe',
    'mtime' => 1516304518,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Visitor_Management.php' => 
  array (
    'md5' => '6a1466f7c23971366778c0a48c869be9',
    'mtime' => 1516653891,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_visit_status_list.php' => 
  array (
    'md5' => '38e8895be1d47cf02d7e0f22b0d56a19',
    'mtime' => 1516654577,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_error_occured_on_weekend_list.php' => 
  array (
    'md5' => '55818505dcbf65805abe6d04e5b4a583',
    'mtime' => 1517331009,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Equipment.php' => 
  array (
    'md5' => '46892509c6e4345d93adb8c3b64e8a67',
    'mtime' => 1517339643,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Error_Employees.php' => 
  array (
    'md5' => 'dbffa5d081a92af6c01bf5652e9798e5',
    'mtime' => 1517429749,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Error_Documents.php' => 
  array (
    'md5' => '0d9c0d2f8a5a753aa246986ee3941ac7',
    'mtime' => 1517430003,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Protocol_Amendments.php' => 
  array (
    'md5' => 'd18d8bdf0e997ca9ac58184e1061c00c',
    'mtime' => 1517840466,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Acquired_Companies.php' => 
  array (
    'md5' => '793870843700235d8794cb37f95da57c',
    'mtime' => 1518622334,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Error_Departments.php' => 
  array (
    'md5' => '983bd9e43e1e6077299a0100c6b02343',
    'mtime' => 1519307383,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_deceased_list.php' => 
  array (
    'md5' => '776fa65940c1b2022140e672a6709bab',
    'mtime' => 1523539882,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Tradeshow_Activities.php' => 
  array (
    'md5' => 'ada04d128eec9bdc1cf017da6a52011c',
    'mtime' => 1524502871,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_lead_quality_list.php' => 
  array (
    'md5' => 'acd6199303f2d3fcc16f95d542b3fe96',
    'mtime' => 1524503796,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_test_control_article_checkin_list.php' => 
  array (
    'md5' => 'bdb388c521a1c536ecf0f732ea900fe0',
    'mtime' => 1525111547,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_sales_activity_source_list.php' => 
  array (
    'md5' => '86831f31d9940acd48492a17c53fb3a7',
    'mtime' => 1525809087,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Anatomy_Database.php' => 
  array (
    'md5' => 'f17965242d85420ad2bc1c73c69de249',
    'mtime' => 1526919983,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_imaging_modality_list.php' => 
  array (
    'md5' => 'a003e5fe0b58784d055f0c7ae13d5186',
    'mtime' => 1526920701,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_anatomical_location_list.php' => 
  array (
    'md5' => '47c2edfa70baf6881a0b037a6e8897cb',
    'mtime' => 1526928147,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_weight_range_list.php' => 
  array (
    'md5' => '29eb64c5e887d64527b3aec4017e94d0',
    'mtime' => 1526932341,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Error_QC_Employees.php' => 
  array (
    'md5' => '2e4a039c12e6b67dfc6194af5b7614a6',
    'mtime' => 1527885675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_portal_account_activated_list.php' => 
  array (
    'md5' => '57d6d4cb8869ef6faac8347254a535c4',
    'mtime' => 1528812837,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_procedure_room_type_list.php' => 
  array (
    'md5' => 'db083384918673b66f1b9c80ee614d31',
    'mtime' => 1530039142,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_required_equipment_list.php' => 
  array (
    'md5' => '43165d78a32d53ae74e8a1f562d1bd2c',
    'mtime' => 1530039672,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_regulatory_region_list.php' => 
  array (
    'md5' => 'e45ce4abe367eb831259bfea005b962b',
    'mtime' => 1530040209,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_required_reports_list.php' => 
  array (
    'md5' => '687eaf5a2f4d78a3f09a67c64387be02',
    'mtime' => 1530040490,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_cpi_finding_category_list.php' => 
  array (
    'md5' => '407c11b42b04515e0618629b1bd4e7d6',
    'mtime' => 1536257579,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_timeline_type_list.php' => 
  array (
    'md5' => 'cbaf4149fc085ac8207fe116d8d8d1a7',
    'mtime' => 1536868176,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_chronic_study_list.php' => 
  array (
    'md5' => 'dd1ca71f86ff91d22da776e6633e0ee9',
    'mtime' => 1536931758,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_duration_greater_than_4_week_list.php' => 
  array (
    'md5' => '5cd0d0a0caad1e200c4de9227724f2a5',
    'mtime' => 1536931935,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_view_all_company_projects_list.php' => 
  array (
    'md5' => 'c6a109cb59cc6fd637b529b807f95816',
    'mtime' => 1537205764,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_deviation_type_list.php' => 
  array (
    'md5' => '2fc159626f7d2b37bf0cd59871d80c5b',
    'mtime' => 1537423816,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_responsible_personnel_list.php' => 
  array (
    'md5' => 'a7b05b60089d9042a0fdd275308fd3ed',
    'mtime' => 1537423816,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Deviation_Employees.php' => 
  array (
    'md5' => 'bdf0e971c33584d4b09e26616e76887a',
    'mtime' => 1537423816,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_is_manager_list.php' => 
  array (
    'md5' => 'fd3beeb78bb65afa3e7b77703718f50d',
    'mtime' => 1537423816,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_employee_status_dom.php' => 
  array (
    'md5' => '4fe3f83df7f6f8facf8eee1cd559fc67',
    'mtime' => 1537879807,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_functional_area_list.php' => 
  array (
    'md5' => '13177a99f255e21ee5b1832a2199aff2',
    'mtime' => 1537890069,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Study_Workflow.php' => 
  array (
    'md5' => '35d742649732d50f7ae49918f3526714',
    'mtime' => 1539103132,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_departments_list.php' => 
  array (
    'md5' => 'a6883f72279a63b9e127ce20f654150b',
    'mtime' => 1542234028,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_instrument_list.php' => 
  array (
    'md5' => 'f17c353ab3628176e09f12fe785f9196',
    'mtime' => 1542292725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_lead_auditor_list.php' => 
  array (
    'md5' => '26d16b6f1f6e011c46620667978d4ddd',
    'mtime' => 1542726469,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_analytical_activity_list.php' => 
  array (
    'md5' => '501de6796f49c63de7fad097d196b3c1',
    'mtime' => 1542822935,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_required.php' => 
  array (
    'md5' => '5261ef660cce5d26989df8a929030922',
    'mtime' => 1543443670,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_necropsy_type_list.php' => 
  array (
    'md5' => '9b39d160fd64d66f2d4c8a4f9824e7d3',
    'mtime' => 1543445006,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_slide_size_type_list.php' => 
  array (
    'md5' => 'f6b7c62ad68fd2066f2f4bba168308f6',
    'mtime' => 1543512870,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_histopath_processing_type_list.php' => 
  array (
    'md5' => '2026187fbae14b2c3aa931451bfdb052',
    'mtime' => 1543513311,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.customm01_sales_activities_1_calls.php' => 
  array (
    'md5' => '2ab393cfef65b2c92543a1fc071b99b1',
    'mtime' => 1544482921,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_vendor_list.php' => 
  array (
    'md5' => '3231f79a86fbdd3031e6941d7a8b66d2',
    'mtime' => 1548430343,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_document_status_list.php' => 
  array (
    'md5' => 'b46255f6beec635762155bb2a5ec0f07',
    'mtime' => 1548720407,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Company_Documents.php' => 
  array (
    'md5' => '852f40dc574e3a8ebf289158a9263fed',
    'mtime' => 1549482610,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Tradeshow_Documents.php' => 
  array (
    'md5' => '3c9a62025a01ce9d6868288842172876',
    'mtime' => 1550100147,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_meeting_status_dom.php' => 
  array (
    'md5' => 'e6c2b837fd937f0b81747295c0b7643b',
    'mtime' => 1552478385,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_document_type_list.php' => 
  array (
    'md5' => '5a0ef4d4dd3cdc5fdb781466a0d89871',
    'mtime' => 1554462900,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_study_coordinator_list.php' => 
  array (
    'md5' => '008fa77706c01bfb057776413817d730',
    'mtime' => 1554977192,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_room_list.php' => 
  array (
    'md5' => '76ae8906f0fdf18ac66dba6cd992c979',
    'mtime' => 1554977710,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_archiving_type_of_contact_list.php' => 
  array (
    'md5' => '554fc18b347a31630fc3b78dc47c7024',
    'mtime' => 1555330455,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_department_id_list.php' => 
  array (
    'md5' => '3cd45c3acefc3618b92cb5b05bc77e63',
    'mtime' => 1556105258,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_veterinarian_list.php' => 
  array (
    'md5' => '88eb7443d043cc54a06498a7e51de197',
    'mtime' => 1556799522,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_study_director_list.php' => 
  array (
    'md5' => '9a1052842f388c90c1a5e5b6e90310d7',
    'mtime' => 1557259182,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_category_list_m06_error.php' => 
  array (
    'md5' => '9408248633da0b9df5b7ed273f41a64e',
    'mtime' => 1558162994,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_method_of_qualification_list.php' => 
  array (
    'md5' => '17c7374d8310c829e2fefdaa236bdd39',
    'mtime' => 1558336724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_current_vendor_status_list.php' => 
  array (
    'md5' => '9a0288306a44e3ec41123094d06431ed',
    'mtime' => 1558336724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_company_status_list.php' => 
  array (
    'md5' => '2f6f450c6750f77da3a865c21c2fdf93',
    'mtime' => 1558336724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Equipment_Facility_Document.php' => 
  array (
    'md5' => '6415d3945b42fb84827c0ec1a5515dde',
    'mtime' => 1558336724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_vendor_status_list.php' => 
  array (
    'md5' => 'afa46c5adbc1a3e9e39b2cda8eb07607',
    'mtime' => 1558336724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_vendor_level_list.php' => 
  array (
    'md5' => '34a4554f5587bd34b8548901529a2ba1',
    'mtime' => 1558336724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Room_Transfer.php' => 
  array (
    'md5' => '425222ed36a480272273426f2a5fa778',
    'mtime' => 1558336724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Room.php' => 
  array (
    'md5' => '39e298597e7a8b3010bb856b40c741c7',
    'mtime' => 1558336724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Equipment_Facility_Service.php' => 
  array (
    'md5' => '189397a7c8b13de023d0c3de31de9609',
    'mtime' => 1558336724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Equipment_Facility_Records.php' => 
  array (
    'md5' => 'eb923b4956ef6069128efb1eb52980b5',
    'mtime' => 1558336724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_company_type.php' => 
  array (
    'md5' => '96b7a973c7b88b4ada79e60c98b114bd',
    'mtime' => 1558336724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_reason_for_transfer_list.php' => 
  array (
    'md5' => '07abc3b0de00d396661cfc866c2db8f0',
    'mtime' => 1558336724,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_usda_category_list.php' => 
  array (
    'md5' => '71667415d343c0384fd80ac8b194e567',
    'mtime' => 1560773447,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_iacuc.php' => 
  array (
    'md5' => 'da17cac66966b992549ddd8a41fb45b1',
    'mtime' => 1560883572,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_equipment_and_facility_document_type_list.php' => 
  array (
    'md5' => 'fd7ab162794b0c676d4d90e330d53468',
    'mtime' => 1560959354,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Company_Address.php' => 
  array (
    'md5' => '501b33ec7ebae2167046df8fcc4077a1',
    'mtime' => 1562155340,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_address_type_list.php' => 
  array (
    'md5' => 'a9172576345c4eb05d85da9ea92fe13f',
    'mtime' => 1562155655,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Regulatory_Response.php' => 
  array (
    'md5' => '774b2450d0a1ae547ca6f4980b676e46',
    'mtime' => 1562672576,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Regulatory_Response_Document.php' => 
  array (
    'md5' => 'de6080e87076ab1e329b01082fb68fbe',
    'mtime' => 1562673649,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_vendor_type_list.php' => 
  array (
    'md5' => '95040dc7760bd5474aaf8cc60895f60f',
    'mtime' => 1562760119,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_binder_location_list.php' => 
  array (
    'md5' => 'dc39e13b5ea767c6a83ba34f154b0943',
    'mtime' => 1563884066,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_response_status_list.php' => 
  array (
    'md5' => '644d60dc6f12dc18d8f6d4beff4439d2',
    'mtime' => 1564487119,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_anticipated_study_start_timeline_list.php' => 
  array (
    'md5' => 'f9c7305993493880524aa87aa4bb3d6c',
    'mtime' => 1564573393,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_win_probability_list.php' => 
  array (
    'md5' => '263a3fa777aeef621572bd07537adfac',
    'mtime' => 1564678867,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.customm03_work_product_activities_1_calls.php' => 
  array (
    'md5' => 'bbdf1dc2f4a640e60012a3d213d948a9',
    'mtime' => 1565006593,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_USDA_Exemption.php' => 
  array (
    'md5' => '926faea95c57b1354f32a56b4248d0ce',
    'mtime' => 1565264752,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Contact_Documents.php' => 
  array (
    'md5' => 'c4de709e0c13c0ab8c87e38c7e39e11d',
    'mtime' => 1565868337,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_contact_document_list.php' => 
  array (
    'md5' => '50ef13813113b107821e09c491920cf1',
    'mtime' => 1565868675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Email_Documents.php' => 
  array (
    'md5' => '4425b4bc5d7b3e4bf8be1a871d10333f',
    'mtime' => 1566905596,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_equipment_and_facilities_status_list.php' => 
  array (
    'md5' => '05b16ffc800ff749b0cf1d9522971169',
    'mtime' => 1568636131,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Email_Template.php' => 
  array (
    'md5' => '672596e4e18a49633ed01371b1b3743d',
    'mtime' => 1568721884,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Company_Name.php' => 
  array (
    'md5' => 'a5273b423dff0509f1fd7079d77cae51',
    'mtime' => 1569507010,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Test_System_Documents.php' => 
  array (
    'md5' => 'b0bd7273349195ea2012eae1114993d5',
    'mtime' => 1569846203,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_scientific_activity_list.php' => 
  array (
    'md5' => '8a23693076138d6dbd7eab9d85357f32',
    'mtime' => 1570104475,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_aaalac_exemptions_list.php' => 
  array (
    'md5' => '74d328c24333054b217984b1af101814',
    'mtime' => 1573049433,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_record_type_display_notes.php' => 
  array (
    'md5' => 'a374ef16705664c4721cd54502dc9b62',
    'mtime' => 1574252882,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Units.php' => 
  array (
    'md5' => '8bec88d04581d3ff394e8f8e0e551f20',
    'mtime' => 1574867211,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Species.php' => 
  array (
    'md5' => '76dedea304b8c9185b3fd689aca3daea',
    'mtime' => 1574870428,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_corrective_action_options_list.php' => 
  array (
    'md5' => '0e992f3c2b27afd2d812f6130cbd9252',
    'mtime' => 1575636901,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_test_system_document_category_list.php' => 
  array (
    'md5' => '10cbe36aa95e787cfe02346f919a8884',
    'mtime' => 1576068581,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Weight.php' => 
  array (
    'md5' => 'af97e4b5a15096869e1080265432e454',
    'mtime' => 1578401834,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Clinical_Observation.php' => 
  array (
    'md5' => 'ed88f5de2b5099c60a0b11655a253896',
    'mtime' => 1578402824,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Clinical_Issue_Exam.php' => 
  array (
    'md5' => 'e0597e604efcbce53d07d7182fdb7172',
    'mtime' => 1578403275,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.tech_list.php' => 
  array (
    'md5' => 'dfe767a61cb73d52877305e34bd9b983',
    'mtime' => 1578901598,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.vet_list.php' => 
  array (
    'md5' => '579ce3dca47f65c986e34cea70e88be8',
    'mtime' => 1578901598,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.examiner_type_list.php' => 
  array (
    'md5' => '7a14202c05ced370deb97bd7f1ba1922',
    'mtime' => 1578901598,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.CO_dependency_lang.php' => 
  array (
    'md5' => '7026d1a0c6b58c973ad5ad5bef2c6c46',
    'mtime' => 1579064816,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Service_Vendor.php' => 
  array (
    'md5' => 'c631a9dc55a671ca2ceac7eb619327a9',
    'mtime' => 1579093040,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_normal_see_gas_list.php' => 
  array (
    'md5' => 'dd15051eb38b3f921045be6de4938732',
    'mtime' => 1579256143,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_oral_cavity_list_not_exam.php' => 
  array (
    'md5' => '0d1a1a98404f3a6e34d4a84ba7e2c757',
    'mtime' => 1579256213,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_oral_cavity_list_normal.php' => 
  array (
    'md5' => '4a05ef7101f3966fc9a4588eef9f5282',
    'mtime' => 1579256280,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_normal_sln_list.php' => 
  array (
    'md5' => '48f34d67a2cb4ea6d80c9262535b6a63',
    'mtime' => 1579256927,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_co_gait_abnormal_list.php' => 
  array (
    'md5' => '53c2ea6b91faf784fa7a524778b0e479',
    'mtime' => 1580117759,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_co_gait_normal_list.php' => 
  array (
    'md5' => 'dc9220fb33a6b91c2080c2e2aba0e5ec',
    'mtime' => 1580117759,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_co_appetite_list.php' => 
  array (
    'md5' => '0aa1b50445ed4e8d81c672cc2cad7a1d',
    'mtime' => 1580117759,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_co_mentation_dd_list.php' => 
  array (
    'md5' => 'a3315cd076228bd264e308011cc00981',
    'mtime' => 1580117759,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_co_feces_dd_list.php' => 
  array (
    'md5' => '5bab59485c560d05302b200163d4767b',
    'mtime' => 1580117759,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_mentation_bar.php' => 
  array (
    'md5' => '20411666d723ce30192649ed8db130d0',
    'mtime' => 1580117759,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_normal_list.php' => 
  array (
    'md5' => 'be5b61deb9f707b8125003cb00456367',
    'mtime' => 1580117759,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.notes_data_lang.php' => 
  array (
    'md5' => 'ea5fb33fb1cb66643baec7c47b34513d',
    'mtime' => 1580361521,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_shipping_delivery_list.php' => 
  array (
    'md5' => '6c6d634d8347bb412f9f8e2c59dd0bd8',
    'mtime' => 1580822454,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_shipping_conditions_list.php' => 
  array (
    'md5' => '7758a25636cb39a3a18b5238eba25c0c',
    'mtime' => 1580822753,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_material_category_list.php' => 
  array (
    'md5' => 'a0e6acc12e2b74024a7b3ccd840309a6',
    'mtime' => 1580823130,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_hazardous_contents_list.php' => 
  array (
    'md5' => 'f8b938190c4e19adad3cc8280e2ec24d',
    'mtime' => 1580995825,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_deliverable_type_list.php' => 
  array (
    'md5' => '6092143d2e1e930f30f307dd1c0573f3',
    'mtime' => 1584359305,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_category_id_list.php' => 
  array (
    'md5' => 'd2ed7b5a616b4df73132739bcfbdaec7',
    'mtime' => 1588329835,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_shipping_company_list.php' => 
  array (
    'md5' => '76a5d3ae62d889cca137a5fbc7363065',
    'mtime' => 1590050379,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_quoteco_status_list.php' => 
  array (
    'md5' => 'af229743868908ec9b6699a436af1a5e',
    'mtime' => 1591867730,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_sales_activity_quote_req_list.php' => 
  array (
    'md5' => '5974ed5679af2a6505eefbcda35f7e0e',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_vaccine_c_list.php' => 
  array (
    'md5' => '6d492b9a62e934d6cf1aac9876ac76d0',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_study_compliance_list.php' => 
  array (
    'md5' => 'eac7216cfedcd41a2a86d6b036e21d01',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_pathologist_workload_list.php' => 
  array (
    'md5' => '5f1b9b4404883f5cf2c3d5bff486f385',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_company_division_list.php' => 
  array (
    'md5' => '2cd76b3eaee2f15018566cfa820efbba',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_sex_list.php' => 
  array (
    'md5' => '22e789680f61b72c8e010c8e7d4fda8c',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_expanded_study_result_list.php' => 
  array (
    'md5' => 'fd07ef4161d3ae67c210dd5c5fe561fe',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_bc_study_article_received_list.php' => 
  array (
    'md5' => '312f3469e9dc8fc5c92e1614242f2bf4',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_reason_for_amendment_list.php' => 
  array (
    'md5' => '0e11fddcb3e8d2d5a7ac2d8ebc58eeab',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_invoice_installment_percent_list.php' => 
  array (
    'md5' => '559e5cc029889c36b0ce7aa862416913',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_vaccine_a_list.php' => 
  array (
    'md5' => '3c765ce448a92f50e8efc3e6bcbac2c0',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_data_storage_phase_list.php' => 
  array (
    'md5' => 'ced3e8f62145a09c4a3b48ab28770de6',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_work_product_compliance_list.php' => 
  array (
    'md5' => '88dd443dab79399545d8f4cdef02efea',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_reason_for_lost_quote.php' => 
  array (
    'md5' => '2d4179d67edf44b9bb123c54a7d253c0',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_quote_review_priority_list.php' => 
  array (
    'md5' => '7d4e3f0734378e1ce0b77b554f7cb17c',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_vaccine_b_c_list.php' => 
  array (
    'md5' => 'c7b7cdf8b0edec5563b892ff930f20f1',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_Yes_No.php' => 
  array (
    'md5' => 'd22f51d23a32d1e8dbd13f86b3c00abd',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_yes_no_radio_list.php' => 
  array (
    'md5' => '39b783cef50ff76292d8cce3eb28eb1d',
    'mtime' => 1591869676,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_meeting_type_dom.php' => 
  array (
    'md5' => '6ba4cece9590b3e38eec05b2ef8f529e',
    'mtime' => 1591882184,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_equipment_and_facility_records_type_list.php' => 
  array (
    'md5' => 'cb03d2b862f5cd0f6c3b2e2cbbf367af',
    'mtime' => 1592298285,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_work_type_list.php' => 
  array (
    'md5' => '649ec21682f368d5ec59b971c18370b7',
    'mtime' => 1593079084,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_spa.php' => 
  array (
    'md5' => '6456e6312b6f26ad0c066b7493161a7c',
    'mtime' => 1595928926,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_company_category_list.php' => 
  array (
    'md5' => '7ae42a394d1f63429c5ab3b52071ba25',
    'mtime' => 1597131429,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_product_status_list.php' => 
  array (
    'md5' => '2c4d3f5e2e468ed9bd6cf831d95e2b6f',
    'mtime' => 1597911209,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_product_notes_multiselect_list.php' => 
  array (
    'md5' => '3e851d210f2ca4db719a02303506c77a',
    'mtime' => 1597911209,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.GDP_Examples.php' => 
  array (
    'md5' => '1899e097157161fb43a0ad3f5e12d359',
    'mtime' => 1598514455,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_product_allowed_storage_list.php' => 
  array (
    'md5' => '9ea19f0eba984849ee299eee3574b6d2',
    'mtime' => 1599557377,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_prd_level_list.php' => 
  array (
    'md5' => '4cb9583c0abe00c018c4bbfb70947394',
    'mtime' => 1599557913,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Product_Document.php' => 
  array (
    'md5' => '7535989515a73f17e830c18678045e32',
    'mtime' => 1599726233,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Product.php' => 
  array (
    'md5' => 'b7936e6b8bdbcc03d4fe22a758b80abb',
    'mtime' => 1600774111,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_service_vendor_type_list.php' => 
  array (
    'md5' => 'b2facfc5355db09ec4497d4cb103205b',
    'mtime' => 1603792538,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_iso_17025_units_of_measure_list.php' => 
  array (
    'md5' => '5ae4f4d06d341e158a6b2021b471d7b0',
    'mtime' => 1603961988,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_disposition_type_deliverable_list.php' => 
  array (
    'md5' => 'a371840332407e0bf5a0581d6baf4555',
    'mtime' => 1605600870,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_why_it_happened_c_list.php' => 
  array (
    'md5' => '6082ebd66c80b206fcdad53e5521f6de',
    'mtime' => 1605785489,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_classification_list.php' => 
  array (
    'md5' => '289ed1653e7c380a78a89881284c6833',
    'mtime' => 1606987892,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_expense_or_capitalize_list.php' => 
  array (
    'md5' => '255fca2fba9e7d23813b23b91e41dd09',
    'mtime' => 1607419798,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_ef_status_list.php' => 
  array (
    'md5' => '653bf57ccb1648ad09abf7dd187aa787',
    'mtime' => 1607422029,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_management_assessment_train_list.php' => 
  array (
    'md5' => 'c748a9bf19c392b6c7f6058f8ef0df36',
    'mtime' => 1608019283,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_impactful_list.php' => 
  array (
    'md5' => 'b49b073fd55f36eaf1e4dc30f46d78fe',
    'mtime' => 1608019283,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_ed_type_list.php' => 
  array (
    'md5' => '29c2ba97d957f4634b2305935e2c0eb0',
    'mtime' => 1608033933,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_enrollment_status_list.php' => 
  array (
    'md5' => '009b562033fd5f156df41e20ceefa2ec',
    'mtime' => 1608035942,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_high_level_root_cause_action_list.php' => 
  array (
    'md5' => '389c52c12b3cbe498610bb60f7f10544',
    'mtime' => 1609234926,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Service_Pricing.php' => 
  array (
    'md5' => 'a80bdc5b55f21f174047eee6fb62f771',
    'mtime' => 1610449348,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_species_sp_list.php' => 
  array (
    'md5' => 'd86c23f8c188a29dd1f8447b53b888e3',
    'mtime' => 1610451348,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_error_classification_c_list.php' => 
  array (
    'md5' => '37a302eabc163cf6d4b76401c8499e7e',
    'mtime' => 1611219158,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_iacuc_deficiency_class_list.php' => 
  array (
    'md5' => '889585169e18e0a7e172db182a441c8c',
    'mtime' => 1611651663,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_service_format_list.php' => 
  array (
    'md5' => 'e2443296f5753cd6620a8d376e5e1bc8',
    'mtime' => 1611825082,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_service_type_list.php' => 
  array (
    'md5' => '2bc340230680c960ae4e369e4109f9b0',
    'mtime' => 1612430765,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Controlled_Document_QA_Reviews.php' => 
  array (
    'md5' => '1f9a3e45eaefabf917f1b8e98b697c9e',
    'mtime' => 1613027483,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Controlled_Documents_QA_Review_Docs.php' => 
  array (
    'md5' => '3778098fb3024785160363f25f184de6',
    'mtime' => 1613028944,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Test_System_Design_1.php' => 
  array (
    'md5' => '72964883f11b41fee636925a22f36c59',
    'mtime' => 1613030709,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_stability_considerations_list.php' => 
  array (
    'md5' => 'c56c7ec11436bb145fa38ecfcaa292bf',
    'mtime' => 1613639016,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Historical_USDA_ID.php' => 
  array (
    'md5' => 'e42eaa9640b0ac7c86c9b974c211da72',
    'mtime' => 1614076598,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Account_Number.php' => 
  array (
    'md5' => 'db8773fb6d134cf139860f835afcaf7d',
    'mtime' => 1614241780,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_wps_outcome_wpe_activities_list.php' => 
  array (
    'md5' => '4a040ed7dc33b5d8a10fb0ef6ba47d48',
    'mtime' => 1614847337,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_wps_outcome_activities_list.php' => 
  array (
    'md5' => '2f6208ed31e3e1ebe9eaa7eadf9f058a',
    'mtime' => 1614847337,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_paraffin_plastic_list.php' => 
  array (
    'md5' => '212a5837f4f8d768c2db59f6e1e0288c',
    'mtime' => 1617700752,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Work_Product_Group.php' => 
  array (
    'md5' => 'dc02afc5e73dd48f41dc78d19b302cef',
    'mtime' => 1618909104,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Species_Census.php' => 
  array (
    'md5' => '60f4d118177c4cadafe5ca6e018722c3',
    'mtime' => 1619677784,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_moduleListSingular.php' => 
  array (
    'md5' => 'b79491f8ef74f927994b77be9a710f29',
    'mtime' => 1622105007,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Controlled_Document_Utilization.php' => 
  array (
    'md5' => 'df2b1cc61feb96eedf4666c85dd4166a',
    'mtime' => 1624953153,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_moduleList.php' => 
  array (
    'md5' => '40f62cefae3e413768b72242faa6e25a',
    'mtime' => 1624954094,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_deviation_rate_basis_list.php' => 
  array (
    'md5' => '798ce3a9ca6eb66ce106165dfcaaed59',
    'mtime' => 1624959874,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_final_report_timeline_type_list.php' => 
  array (
    'md5' => '592a921532f7df945df42544a680e0a1',
    'mtime' => 1626936094,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_vc_related_to_list.php' => 
  array (
    'md5' => '9ae7e1dd589aeb317ee6c731c0d35706',
    'mtime' => 1626938656,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_red_yellow_green_list.php' => 
  array (
    'md5' => 'db12fe8852f510bc527770916fa7db97',
    'mtime' => 1626944982,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_category_list.php' => 
  array (
    'md5' => '82ead6133158bd67c99427f5fe970e9f',
    'mtime' => 1628146975,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_minutes_per_slide_list.php' => 
  array (
    'md5' => '2ab7537350af515229d9cdca6a089745',
    'mtime' => 1628758936,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_procedure_room_list.php' => 
  array (
    'md5' => '988843c639850930a62feddc322dae73',
    'mtime' => 1628836478,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Task_Design.php' => 
  array (
    'md5' => 'dd4d3c24e4d3941598f5cbdb98476fb5',
    'mtime' => 1628918992,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Group_Design.php' => 
  array (
    'md5' => 'af008e80d490564d986c6278d3a80a8c',
    'mtime' => 1628919535,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_reason_for_weight_list.php' => 
  array (
    'md5' => '0b5541e41684f2e993772a0207cd29c4',
    'mtime' => 1629356838,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_test_system_list.php' => 
  array (
    'md5' => '3d69ed371f69c665924ca2b8d3c1d453',
    'mtime' => 1631609364,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_action_needed_list.php' => 
  array (
    'md5' => 'c84fb72d52e85a0b78316452c4e392d8',
    'mtime' => 1634194533,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Order_Request_Item.php' => 
  array (
    'md5' => '431869c392f2f59033a0dceca5d2c0ee',
    'mtime' => 1634629957,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Order_Request.php' => 
  array (
    'md5' => 'cea53285885897be08b4910569185a70',
    'mtime' => 1634630210,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Purchase_Order_Item.php' => 
  array (
    'md5' => '6b69307c0584799fc204ca91a9a15e53',
    'mtime' => 1634632312,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Purchase_Order.php' => 
  array (
    'md5' => 'c103a9ffd339864362a244c113792c79',
    'mtime' => 1634632714,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Received_Items.php' => 
  array (
    'md5' => 'c766212d5e70bc67ddccd9f0311534d4',
    'mtime' => 1634632943,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_purchase_unit_list.php' => 
  array (
    'md5' => '5a38dea3fd4769f964fdf3c8a6fc1923',
    'mtime' => 1634685613,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_oi_status_list.php' => 
  array (
    'md5' => '62137b034b271a29610fd1bb75b94266',
    'mtime' => 1634685613,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_ORI_status_list.php' => 
  array (
    'md5' => 'a5448362299e64adb59f075b258f5a28',
    'mtime' => 1634685613,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_poi_ori_related_to_list.php' => 
  array (
    'md5' => '83db74d864c03db491db9ca7d3006ecb',
    'mtime' => 1634685613,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_POI_owner_list.php' => 
  array (
    'md5' => '75aad8377b3d014e741b28fc424b9c77',
    'mtime' => 1634685613,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_grant_submission_sa_2_list.php' => 
  array (
    'md5' => 'a4534a916c87f4811a2b643851f8fcdd',
    'mtime' => 1635239007,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_reason_for_expansion_list.php' => 
  array (
    'md5' => '3d141561568bdc0d2f2475ec0535024b',
    'mtime' => 1635239206,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_bc_study_outcome_type_list.php' => 
  array (
    'md5' => 'dfe2c70a88160dd575f64c24a5ffa5e0',
    'mtime' => 1635239662,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_gd_study_article_type_list.php' => 
  array (
    'md5' => '620a5c1fa19baed3ad0ae8faff7a0689',
    'mtime' => 1635411073,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Inventory_Collection.php' => 
  array (
    'md5' => '8841ade135e8353776601fcab94940e1',
    'mtime' => 1636448384,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Inventory_Item.php' => 
  array (
    'md5' => 'e0bc91b948aa8726fac2fcc9ee315e6e',
    'mtime' => 1636448670,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Inventory_Management.php' => 
  array (
    'md5' => '620ea10184b001fc2b7dd3bdfb7b5d49',
    'mtime' => 1636449123,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_ im_specimen_category_list.php' => 
  array (
    'md5' => 'c86db510b94826d39983fe1246903bed',
    'mtime' => 1636449392,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_ im_record_category_list.php' => 
  array (
    'md5' => 'c8f18d5dea5d0f14506a7bd32c7f4bbd',
    'mtime' => 1636449392,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_im_record_category_list.php' => 
  array (
    'md5' => '0647d95c41b9732bb73c006bb0258c9b',
    'mtime' => 1636449392,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_ im_study_article_category_list.php' => 
  array (
    'md5' => 'ed7efbcc20176810d743254189c1e000',
    'mtime' => 1636449392,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_known_unknown_list.php' => 
  array (
    'md5' => '42269c08326ff88026fe5cb10c0de934',
    'mtime' => 1636449392,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_Inventory_Item.php' => 
  array (
    'md5' => 'bdae9f7df0eb2087a73cafef1f82a9b1',
    'mtime' => 1636449392,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_im_study_article_category_list.php' => 
  array (
    'md5' => '90d146122527eed402966bf2461cbda6',
    'mtime' => 1636449392,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_im_specimen_category_list.php' => 
  array (
    'md5' => 'bf61be1fa298328512c8fc20e88c60e4',
    'mtime' => 1636449392,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_inventory_item_subtype_list.php' => 
  array (
    'md5' => '69dbd3feb954afe7d87a6908774875bf',
    'mtime' => 1636449392,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_stability_considerations_ii_list.php' => 
  array (
    'md5' => '6233ff3f6403089e79f7c53933f52164',
    'mtime' => 1636456212,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_yes_no_na_list.php' => 
  array (
    'md5' => 'd036f3e271f66989d6a89af868f0add5',
    'mtime' => 1636456603,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_inventory_item_category_list.php' => 
  array (
    'md5' => '27ae35574a0a8202f88d3738a69207b4',
    'mtime' => 1636503980,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_inventory_item_owner_list.php' => 
  array (
    'md5' => 'd04bb2355f56b01c689227f708cc30df',
    'mtime' => 1636504316,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_im_location_type_list.php' => 
  array (
    'md5' => '64ab63e4158d9a8b62a085745d6ed5e7',
    'mtime' => 1636505452,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_inventory_item_status_list.php' => 
  array (
    'md5' => '4ba20d77a325559cca1168e203d0b062',
    'mtime' => 1636506083,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_timepoint_type_list.php' => 
  array (
    'md5' => 'f9551c5695dc68d6b5a00f8beeb6f750',
    'mtime' => 1636510895,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_inventory_item_storage_condition_list.php' => 
  array (
    'md5' => 'b72f00abf8c7594188e861692583b26a',
    'mtime' => 1636511655,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_yes_no_list.php' => 
  array (
    'md5' => '5da4f6a51f326c5dbf29e77afd26489e',
    'mtime' => 1636553500,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_aaalac_and_usda_exemptions_list.php' => 
  array (
    'md5' => '4b7a7f19acb0d40819ad11dff9c0983d',
    'mtime' => 1637230763,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_tsd_wt_type_list.php' => 
  array (
    'md5' => '364978c6cbdeb9e451fce9dffce4e27a',
    'mtime' => 1638275422,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_procedure_type_com_list.php' => 
  array (
    'md5' => 'bc386178360ecb79cd1241058419e735',
    'mtime' => 1638867776,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_TSD_purpose_list.php' => 
  array (
    'md5' => '00e6bd5667e0f45d8653b205f63a482e',
    'mtime' => 1638869662,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_housing_requirements_list.php' => 
  array (
    'md5' => '9ab0c4f8b4588c01c898a9fb2206c072',
    'mtime' => 1638870932,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_number_list.php' => 
  array (
    'md5' => 'f8dbd555dcb9b397710cd3039639655c',
    'mtime' => 1638873167,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.NAMSA_Subcontracting_Companies.php' => 
  array (
    'md5' => 'd36755c098759f8b9ea53e88992ef5f0',
    'mtime' => 1638876878,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.NAMSA_Test_Codes.php' => 
  array (
    'md5' => 'fe76d38c0833323a6b004ad5042f6137',
    'mtime' => 1638877932,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_sales_study_article_status_list.php' => 
  array (
    'md5' => 'd86c5d0615e17d5c80aa10cb9595b7d8',
    'mtime' => 1638880077,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_degree_of_immobility_list.php' => 
  array (
    'md5' => '721ca9ac5f562cccac6affc00137fad3',
    'mtime' => 1639472921,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_type_of_acclimation_list.php' => 
  array (
    'md5' => '0f00b3f936960d72405c1a82b4157fc3',
    'mtime' => 1639472981,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_inspection_results_list.php' => 
  array (
    'md5' => 'f595b83d8e8063fc8787f0ea320e6685',
    'mtime' => 1640075560,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_or_status_list.php' => 
  array (
    'md5' => 'bd00d9ad18964f7a02113dec6e04a871',
    'mtime' => 1640676036,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_tpr_reference_list.php' => 
  array (
    'md5' => 'de511cdd6102af33746812a5799dd028',
    'mtime' => 1641537753,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_inventory_item_type_list.php' => 
  array (
    'md5' => '05990f09b31a56ee6e418d7d04f45678',
    'mtime' => 1642489496,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_universal_inventory_management_type_list.php' => 
  array (
    'md5' => 'b0271b6cf653d1882ecd14046a442e51',
    'mtime' => 1642490315,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_type_specimen_list.php' => 
  array (
    'md5' => '2b1f82dea0be3cc8653e4b25753e1066',
    'mtime' => 1643102231,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_phase_of_inspection_list.php' => 
  array (
    'md5' => '88d74b28588e13bbfd95474bf43b851d',
    'mtime' => 1643700661,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_room_type_list.php' => 
  array (
    'md5' => 'c87a011f94c853b34114d948755c5f12',
    'mtime' => 1643701129,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Operator_Tracking.php' => 
  array (
    'md5' => 'f01950ec928956c43b0924a934be275b',
    'mtime' => 1643701242,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.CAPA_Files.php' => 
  array (
    'md5' => '19739ac37570ac77bbb28bd13e5ed3a0',
    'mtime' => 1643872463,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.CAPA.php' => 
  array (
    'md5' => '3e8e68ff3e8e2e4f770d7319aa2f3a6b',
    'mtime' => 1643872848,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_ca_andor_pa_dd_list.php' => 
  array (
    'md5' => '52662a38f3a5e7c1a9897ed620f310f7',
    'mtime' => 1643875796,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_frequency_score_list.php' => 
  array (
    'md5' => '715cf917588dcbdba2e6edf65402cd10',
    'mtime' => 1643876686,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_severity_score_list.php' => 
  array (
    'md5' => '122766d9d6ac931c08e85959665e31b2',
    'mtime' => 1643876846,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_GD_status_list.php' => 
  array (
    'md5' => '3aeceeab61135077a039ab809fea2f01',
    'mtime' => 1643881435,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_task_type_list.php' => 
  array (
    'md5' => 'b27e1513c65ab5e40e5d971943359b73',
    'mtime' => 1643882360,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_ot_category_list.php' => 
  array (
    'md5' => '1f68f911b0acbecb50e3cf20de51de3d',
    'mtime' => 1644306090,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_ot_type_list.php' => 
  array (
    'md5' => '91d43268370adbcf4ce33b23f8cfe9df',
    'mtime' => 1644306167,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.addModuleLink.php' => 
  array (
    'md5' => 'ce8b88cac6b9ec7b894e944f40edbc74',
    'mtime' => 1645080444,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_organ_system_list.php' => 
  array (
    'md5' => '21fed1b16f387af196d074fa82f9de52',
    'mtime' => 1645082237,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_po_status_list.php' => 
  array (
    'md5' => '18ace421745b1324d13c3790bba55e40',
    'mtime' => 1645513654,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_deliverable_status_list.php' => 
  array (
    'md5' => 'bc6e39732ce2901e1e0168b438186a4c',
    'mtime' => 1645515388,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_ii_related_to_c_list.php' => 
  array (
    'md5' => '4045b6835164de02fb1e2f3a9b1a64f0',
    'mtime' => 1645691005,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_specimen_type_list.php' => 
  array (
    'md5' => 'ecce24deeba3979031df8f613d0884c4',
    'mtime' => 1645710876,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_specimen_type_no_other_c_list.php' => 
  array (
    'md5' => 'd50bb012a993f6d0f4c490e7a9f59edd',
    'mtime' => 1645710969,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_integration_work_stream_list.php' => 
  array (
    'md5' => 'abd024ec7263bfe269bbae4a6688f6d7',
    'mtime' => 1647329017,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_plan_actual_list.php' => 
  array (
    'md5' => '71ecc0b315d30faa009ff3a564ba39bc',
    'mtime' => 1647329467,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_td_audit_phase_list.php' => 
  array (
    'md5' => '3e215929d7a447c141aaafd1410d7206',
    'mtime' => 1648111997,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_sale_document_category_list.php' => 
  array (
    'md5' => '5aea99e95b67b91db4b5b996f4b4468e',
    'mtime' => 1648727216,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_pro_subtype_list.php' => 
  array (
    'md5' => 'e9271930b25d792dc9a942e95d488d31',
    'mtime' => 1648727579,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_product_category_list.php' => 
  array (
    'md5' => '67fb781ae88c81c2df206c3910e468a7',
    'mtime' => 1648728291,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_Breed_List.php' => 
  array (
    'md5' => 'df4364ed624374d2eb215e0ec4d94954',
    'mtime' => 1649143293,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_ii_test_type_list.php' => 
  array (
    'md5' => '16ea75bceb30163d72bd800c6d95e201',
    'mtime' => 1649754237,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_task_procedure_list.php' => 
  array (
    'md5' => '0be81a68a5b16f1ae4a20bd12c21298e',
    'mtime' => 1650530005,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.Batch_ID.php' => 
  array (
    'md5' => 'aa571ea4c1f74bc75e2404e328a03d03',
    'mtime' => 1650955018,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_bid_status_list.php' => 
  array (
    'md5' => '43891b9255aa22456e07efef8418a533',
    'mtime' => 1650956950,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_biod_dose_route_list.php' => 
  array (
    'md5' => '4cec842f949a89ebf1efbc44f62f3aae',
    'mtime' => 1650957457,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_td_phase_list.php' => 
  array (
    'md5' => '208613b320a7266b4ea446f30e812622',
    'mtime' => 1651741043,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_inventory_management_type_list.php' => 
  array (
    'md5' => '9c8c892b955686c4ae00548872b97b65',
    'mtime' => 1652944904,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_wpc_type_list.php' => 
  array (
    'md5' => '934b0126055d992d74fc1db1ab7c0a85',
    'mtime' => 1653375918,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_work_product_status_list.php' => 
  array (
    'md5' => '5c9816b701e98f5bae79afa3807ab3f6',
    'mtime' => 1653376538,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_aaalac_requirements_list.php' => 
  array (
    'md5' => '08d641ec94938875cb0510b7fb5fd0c5',
    'mtime' => 1654579536,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_usda_exemptions_list.php' => 
  array (
    'md5' => 'd0b814955b77707e419fa5d93a00487f',
    'mtime' => 1654579774,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_department_list.php' => 
  array (
    'md5' => '15f58dd1d899d44a6c1fe26277197dfb',
    'mtime' => 1655358264,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_location_list.php' => 
  array (
    'md5' => 'b446f40cba1210d0bac40531d0d9890f',
    'mtime' => 1655792800,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_extraction_conditions_list.php' => 
  array (
    'md5' => '5e52eddbf6c0c862e784f2997f792fdf',
    'mtime' => 1657618162,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_payment_terms_list.php' => 
  array (
    'md5' => '82f41d2aa88f6280ae63e2db70795da2',
    'mtime' => 1658990483,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_error_category_list.php' => 
  array (
    'md5' => 'b5c3762a88c21173e4195df7616bcc5f',
    'mtime' => 1664862473,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_deliverable_list.php' => 
  array (
    'md5' => 'dce45e6e4d181772d11d08617d1a963f',
    'mtime' => 1666701722,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_equipment_required_list.php' => 
  array (
    'md5' => '5fd6f04e59fb91b59fed726534f44488',
    'mtime' => 1666849962,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_completion_status_list.php' => 
  array (
    'md5' => 'bb83180b122c372f53ee544d791c5e4d',
    'mtime' => 1667886252,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_standard_task_list.php' => 
  array (
    'md5' => 'cc661351a36383877e014eea327ff396',
    'mtime' => 1667886843,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_diagnosis_list.php' => 
  array (
    'md5' => 'a1d9e4d20e83b90289d92394458b3844',
    'mtime' => 1667898727,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_condition_list.php' => 
  array (
    'md5' => '97387617fe387608a42a3ef23d07163c',
    'mtime' => 1667898808,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_amendment_request_list.php' => 
  array (
    'md5' => '92e7cff89a118bfea6238718868b154a',
    'mtime' => 1669873031,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/en_us.sugar_error_type_list.php' => 
  array (
    'md5' => '62726c76ad532c8159eb8e59e2584a60',
    'mtime' => 1669887998,
    'is_override' => false,
  ),
);