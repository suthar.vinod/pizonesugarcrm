<?php
 // created: 2022-07-28 06:41:23

$app_list_strings['payment_terms_list']=array (
  'aps account' => 'APS Account - Net 30',
  'new client' => 'New Client - per bid',
  'pre_pay' => 'Pre-pay',
  'check_with_finance' => 'Check with Finance',
  '' => '',
  'Net 30' => 'Net 30',
  'Net 45' => 'Net 45',
  'Net 60' => 'Net 60',
  'Net 77' => 'Net 77',
  'Per MSA' => 'Per MSA',
  'Net 90' => 'Net 90',
);