<?php
 // created: 2022-03-31 11:46:56

$app_list_strings['sale_document_category_list']=array (
  '' => '',
  'Change Order' => 'Change Order',
  'Interinstitutional Assurance' => 'Interinstitutional Assurance',
  'PO' => 'PO',
  'Pricelist Submission' => 'Pricelist Submission',
  'Publication' => 'Publication',
  'Study Quote' => 'Quote',
  'Signed Change Order' => 'Signed Change Order',
  'Signed Quote' => 'Signed Quote',
  'Signed SOW' => 'Signed SOW',
  'SOW' => 'SOW',
  'Study Design Document' => 'Study Design',
  'Subcontractor Quote' => 'Subcontractor Quote',
  'Quote Revision' => 'Quote Revision (Obsolete)',
  'Signed Quote Revision' => 'Signed Quote Revision/PO (Obsolete)',
);