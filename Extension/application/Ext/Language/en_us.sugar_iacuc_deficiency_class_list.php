<?php
 // created: 2021-01-22 14:26:42

$app_list_strings['iacuc_deficiency_class_list']=array (
  '' => '',
  'NA' => 'NA',
  'A' => 'A (Acceptable)',
  'M' => 'M (Minor)',
  'S' => 'S (Significant)',
  'C' => 'C (Change in Program)',
);