<?php
 // created: 2016-04-18 16:41:36

$app_list_strings['data_storage_phase_list']=array (
  'Carousel' => 'Carousel',
  'Reporting' => 'Reporting',
  'Complete and In Review' => 'Complete and In Review',
  'Scanned' => 'Scanned',
  'Archived' => 'Archived',
  'Shipped or Returned' => 'Shipped or Returned',
  'Destroyed' => 'Destroyed',
  'Study Not Performed' => 'Study Not Performed',
  'Choose One' => 'Choose one',
);