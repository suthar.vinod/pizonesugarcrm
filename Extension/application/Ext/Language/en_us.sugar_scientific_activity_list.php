<?php
 // created: 2019-10-03 12:07:55

$app_list_strings['scientific_activity_list']=array (
  '' => '',
  'Conference Attendance' => 'Conference Attendance',
  'Data Entry_Analysis' => 'Data Entry & Analysis',
  'Out of Office' => 'Out of Office',
  'Procedure_Assignment' => 'Procedure Assignment',
  'Protocol Development' => 'Protocol Development',
  'Reporting' => 'Reporting',
  'Operations Oversight' => 'Study Management',
);