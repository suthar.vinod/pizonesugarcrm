<?php
 // created: 2016-04-04 22:50:59

$app_list_strings['study_compliance_list']=array (
  'Select One...' => 'Select One...',
  'non_GLP' => 'non-GLP',
  'GLP' => 'GLP',
  'ISO' => 'ISO',
  'USP' => 'USP',
  'JMHLW' => 'JMHLW',
  'Not Applicable' => 'Not Applicable',
);