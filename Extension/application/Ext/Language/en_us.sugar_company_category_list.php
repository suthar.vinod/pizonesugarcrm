<?php
 // created: 2020-07-31 13:57:20

$app_list_strings['company_category_list']=array (
  '' => '',
  'Sponsor' => 'Sponsor',
  'Sponsor and Vendor' => 'Sponsor and Vendor',
  'Vendor' => 'Vendor',
  'Manufacturer' => 'Manufacturer',
  'Sponsor and Manufacturer' => 'Sponsor and Manufacturer',
  'Vendor and Manufacturer' => 'Vendor and Manufacturer',
  'SponsorVendor Manufacturer' => 'Sponsor, Vendor, and Manufacturer',
);