<?php
 // created: 2016-04-20 13:31:05

$app_list_strings['expanded_study_result_list']=array (
  'Choose One' => 'Choose One',
  'Not Applicable' => 'Not Applicable',
  'Passed Study' => 'Passed Study',
  'Failed Study' => 'Failed Study',
  'Completed Study' => 'Completed Study',
);