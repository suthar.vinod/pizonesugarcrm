<?php
 // created: 2020-11-30 19:27:32

$app_list_strings['classification_list']=array (
  '' => '',
  'Data Generator With Electronic Records' => 'Data Generator With Electronic Records',
  'Data Generator Without Electronic Records' => 'Data Generator Without Electronic Records',
  'Facility Task' => 'Facility Task',
  'Non Data Generator' => 'Non Data Generator',
  'Non GLP Equipment' => 'Non GLP Equipment',
);