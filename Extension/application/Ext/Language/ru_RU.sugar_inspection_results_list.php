<?php
 // created: 2021-12-21 08:32:39

$app_list_strings['inspection_results_list']=array (
  '' => '',
  'No Findings' => 'No Findings',
  'Findings' => 'Findings',
  'Observations' => 'Observations',
  'Findings_Observations' => 'Findings & Observations',
);