<?php
// created: 2022-12-01 09:56:44
$extensionOrderMap = array (
  'custom/Extension/application/Ext/Language/zh_TW.sugar_bc_study_article_received_list.php' => 
  array (
    'md5' => '97744f6e3c04d0d242075bdeb59ae583',
    'mtime' => 1477920884,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_deliverable_owner_list.php' => 
  array (
    'md5' => '59d6673b20c7064ef15a3111323bfdc9',
    'mtime' => 1479760638,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_pathologist_workload_list.php' => 
  array (
    'md5' => 'cbf3ddab6375ea713f201d2a03dde957',
    'mtime' => 1480601833,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_quote_review_priority_list.php' => 
  array (
    'md5' => '2e8cf421e60255fa64715801e04035d4',
    'mtime' => 1482852180,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_signed_quote_c_list.php' => 
  array (
    'md5' => 'f15c653bbb5a8926fddc380aeb16038c',
    'mtime' => 1485188418,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_estimated_hours_list.php' => 
  array (
    'md5' => '5552eaf0d01f176eba1c095f34884d87',
    'mtime' => 1486678995,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_primary_aps_operator_list.php' => 
  array (
    'md5' => '98695d68ccbc1dd1df29f5c0797b030e',
    'mtime' => 1489504759,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_product_type_list.php' => 
  array (
    'md5' => '51abd2edd086c53e853a5ba0c7a9c961',
    'mtime' => 1489607385,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_Product_Contact_Category.php' => 
  array (
    'md5' => '5a190417c760407751be94e698af7a8b',
    'mtime' => 1489607481,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_contact_duration_list.php' => 
  array (
    'md5' => 'bdb459b6992cc892b7fc45c0e9885278',
    'mtime' => 1489608635,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_contact_type_list.php' => 
  array (
    'md5' => '77267f8345650aae45595405839f21b7',
    'mtime' => 1489609950,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_Cytotoxicity_Testing.php' => 
  array (
    'md5' => '317c6c8e77879092525fd048fc84aca9',
    'mtime' => 1489611458,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_activity_list.php' => 
  array (
    'md5' => 'e651a2a46a6ca934b52958bb16ec924b',
    'mtime' => 1490044257,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_invoice_installment_percent_list.php' => 
  array (
    'md5' => '3d125feadc077737986abdb2adc5f157',
    'mtime' => 1492111066,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_business_develop_activity_list.php' => 
  array (
    'md5' => '4d2620e609b8b5c4e4ae43f8739865e8',
    'mtime' => 1493742618,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_quality_assurance_activity_list.php' => 
  array (
    'md5' => '7e4f609c40e136c78fb3c993abeba6aa',
    'mtime' => 1493744562,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_regulatory_activity_list.php' => 
  array (
    'md5' => 'd94540fbf61631732cf673b34a6be802',
    'mtime' => 1493745908,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.Activity_Notes.php' => 
  array (
    'md5' => '18f3af20fd0e07f5d568559a18e2343e',
    'mtime' => 1494540242,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_activity_personnel_list.php' => 
  array (
    'md5' => '3299f9842efe8e315c902b21b84fc006',
    'mtime' => 1494618166,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_new_deliverable_c_list.php' => 
  array (
    'md5' => 'b5bfb71479b4b6e9bfd8a68041b882cc',
    'mtime' => 1497042103,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_pathology_activity_list.php' => 
  array (
    'md5' => '46e3059213f75a3e758d6aa061b57989',
    'mtime' => 1497042654,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_reminder_time_options.php' => 
  array (
    'md5' => '1100302964c700f874f6b73e1244e519',
    'mtime' => 1497640783,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_analytical_equipment_list_list.php' => 
  array (
    'md5' => '8207bcffd5ff78525f4b152bc9b91170',
    'mtime' => 1497647888,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.HR_Management.php' => 
  array (
    'md5' => 'ea06ebbb83938f5655d2c2ef8e6375b0',
    'mtime' => 1500561436,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_job_category_list.php' => 
  array (
    'md5' => '4d946120b67aafcbbff368864a98ff97',
    'mtime' => 1500569094,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.AnimalsModuleCustomizations.php' => 
  array (
    'md5' => '932369d355c0dbac9e0dab5b8caefaa3',
    'mtime' => 1505227917,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.WPEsModuleCustomizations.php' => 
  array (
    'md5' => '4c8b1003de81a6108fb164aedf056930',
    'mtime' => 1505228202,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.customa1a_critical_phase_inspectio_activities_1_calls.php' => 
  array (
    'md5' => '8a3436935d35501b7bb15bc22229feda',
    'mtime' => 1505315712,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_sales_focus_list.php' => 
  array (
    'md5' => 'c1c2f1b0879e9a835ea6f2020dbe3eac',
    'mtime' => 1516229438,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_primary_activity_list.php' => 
  array (
    'md5' => '1563d466028238623138767b33c54ee9',
    'mtime' => 1516304518,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_visit_status_list.php' => 
  array (
    'md5' => '38e8895be1d47cf02d7e0f22b0d56a19',
    'mtime' => 1516654577,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_error_occured_on_weekend_list.php' => 
  array (
    'md5' => '55818505dcbf65805abe6d04e5b4a583',
    'mtime' => 1517331009,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_vaccine_a_list.php' => 
  array (
    'md5' => '497f371167a4f6c8cb4581d7da46fb7d',
    'mtime' => 1519154513,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_vaccine_b_c_list.php' => 
  array (
    'md5' => 'a5351f42e34acd304e964628e5cac882',
    'mtime' => 1519154639,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_vaccine_c_list.php' => 
  array (
    'md5' => 'dd65340daae44b3af96761c1f140630c',
    'mtime' => 1519154723,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_deceased_list.php' => 
  array (
    'md5' => '776fa65940c1b2022140e672a6709bab',
    'mtime' => 1523539882,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_reason_for_amendment_list.php' => 
  array (
    'md5' => 'e42cc2f3f71ceaf49df0e8326cd706a6',
    'mtime' => 1523624792,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_lead_quality_list.php' => 
  array (
    'md5' => 'acd6199303f2d3fcc16f95d542b3fe96',
    'mtime' => 1524503796,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_test_control_article_checkin_list.php' => 
  array (
    'md5' => 'bdb388c521a1c536ecf0f732ea900fe0',
    'mtime' => 1525111547,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_sales_activity_source_list.php' => 
  array (
    'md5' => '4b062c3b6a1f1cf6d16d51d39c395ff9',
    'mtime' => 1525809087,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_imaging_modality_list.php' => 
  array (
    'md5' => 'a003e5fe0b58784d055f0c7ae13d5186',
    'mtime' => 1526920701,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_anatomical_location_list.php' => 
  array (
    'md5' => 'be4daba7ce492e42b9d504a232cecbba',
    'mtime' => 1526928147,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_weight_range_list.php' => 
  array (
    'md5' => '16715961e95be8e6e558106c978a73ab',
    'mtime' => 1526932341,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_portal_account_activated_list.php' => 
  array (
    'md5' => '57d6d4cb8869ef6faac8347254a535c4',
    'mtime' => 1528812837,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_procedure_room_type_list.php' => 
  array (
    'md5' => 'db083384918673b66f1b9c80ee614d31',
    'mtime' => 1530039142,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_required_equipment_list.php' => 
  array (
    'md5' => '43165d78a32d53ae74e8a1f562d1bd2c',
    'mtime' => 1530039672,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_regulatory_region_list.php' => 
  array (
    'md5' => 'e45ce4abe367eb831259bfea005b962b',
    'mtime' => 1530040209,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_required_reports_list.php' => 
  array (
    'md5' => '687eaf5a2f4d78a3f09a67c64387be02',
    'mtime' => 1530040490,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_cpi_finding_category_list.php' => 
  array (
    'md5' => '407c11b42b04515e0618629b1bd4e7d6',
    'mtime' => 1536257579,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_timeline_type_list.php' => 
  array (
    'md5' => '46f02a6de94adfda27cbe7af95593ee3',
    'mtime' => 1536868176,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_chronic_study_list.php' => 
  array (
    'md5' => 'dd1ca71f86ff91d22da776e6633e0ee9',
    'mtime' => 1536931758,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_duration_greater_than_4_week_list.php' => 
  array (
    'md5' => '5cd0d0a0caad1e200c4de9227724f2a5',
    'mtime' => 1536931935,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_view_all_company_projects_list.php' => 
  array (
    'md5' => '45269dff400f08c5a747f7aae75a2c31',
    'mtime' => 1537205764,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_deviation_type_list.php' => 
  array (
    'md5' => '15d852c505af294e9594878d4fc217bf',
    'mtime' => 1537423811,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_responsible_personnel_list.php' => 
  array (
    'md5' => 'a7b05b60089d9042a0fdd275308fd3ed',
    'mtime' => 1537423811,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_is_manager_list.php' => 
  array (
    'md5' => 'f487c070beba518734aaa2a9aadae729',
    'mtime' => 1537423811,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_company_division_list.php' => 
  array (
    'md5' => '92d090ef452a19b045d49c706eac0c36',
    'mtime' => 1537815962,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_employee_status_dom.php' => 
  array (
    'md5' => '882aaa65fddb7645655dbf243a656433',
    'mtime' => 1537879807,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_functional_area_list.php' => 
  array (
    'md5' => '0648ade7eaa200d8cf147742d516b563',
    'mtime' => 1537890069,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_reason_for_lost_quote.php' => 
  array (
    'md5' => '68092e38d72c1f8012739a30acedbf1c',
    'mtime' => 1540321905,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_departments_list.php' => 
  array (
    'md5' => '18fc963a8be42d85dfaa99cd6703d495',
    'mtime' => 1542234028,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_instrument_list.php' => 
  array (
    'md5' => '4660e53a1ecbac44d000a9fc2855651e',
    'mtime' => 1542292725,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_lead_auditor_list.php' => 
  array (
    'md5' => '992b3a02edb8c8b75ec35b92cb0c9658',
    'mtime' => 1542726469,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_analytical_activity_list.php' => 
  array (
    'md5' => '1dc1194dbdf12b522767bd84d1903459',
    'mtime' => 1542822935,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_required.php' => 
  array (
    'md5' => '5261ef660cce5d26989df8a929030922',
    'mtime' => 1543443670,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_necropsy_type_list.php' => 
  array (
    'md5' => '9b39d160fd64d66f2d4c8a4f9824e7d3',
    'mtime' => 1543445006,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_slide_size_type_list.php' => 
  array (
    'md5' => 'f6b7c62ad68fd2066f2f4bba168308f6',
    'mtime' => 1543512870,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_histopath_processing_type_list.php' => 
  array (
    'md5' => 'c3e8e580b7358a07781661772e248e56',
    'mtime' => 1543513310,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.customm01_sales_activities_1_calls.php' => 
  array (
    'md5' => '78dd79ccd2f4294677c96edc2a53af3f',
    'mtime' => 1544482922,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_spa.php' => 
  array (
    'md5' => '2bd8964b5008351986a5f212f8037e71',
    'mtime' => 1548360276,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_vendor_list.php' => 
  array (
    'md5' => '6e684f1178f10958f47f7c7ab47eb123',
    'mtime' => 1548430343,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_document_status_list.php' => 
  array (
    'md5' => 'b46255f6beec635762155bb2a5ec0f07',
    'mtime' => 1548720407,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.Company_Documents.php' => 
  array (
    'md5' => 'f1fc0cce5df7e09f625b073d84a3baba',
    'mtime' => 1549482610,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.Tradeshow_Documents.php' => 
  array (
    'md5' => '3dacdd718fbc5d008d38d28fa7520a2d',
    'mtime' => 1550100147,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_meeting_status_dom.php' => 
  array (
    'md5' => '42dac6c04fd5724811df459842c56e89',
    'mtime' => 1552478385,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_document_type_list.php' => 
  array (
    'md5' => '61b2844e35d9ff16b086d54c4b1ab742',
    'mtime' => 1554462900,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_study_coordinator_list.php' => 
  array (
    'md5' => '9274b32c94fe7e21b8f7ed9ac751a537',
    'mtime' => 1554977192,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_room_list.php' => 
  array (
    'md5' => '43cf2ba1ff9d872fcd158f398a458445',
    'mtime' => 1554977710,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_archiving_type_of_contact_list.php' => 
  array (
    'md5' => '554fc18b347a31630fc3b78dc47c7024',
    'mtime' => 1555330455,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_work_product_compliance_list.php' => 
  array (
    'md5' => '2289691d36e0df311303b67eb011e65f',
    'mtime' => 1555332295,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_department_id_list.php' => 
  array (
    'md5' => '35b4d514aaaba078818d23ce99382ba9',
    'mtime' => 1556105258,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_veterinarian_list.php' => 
  array (
    'md5' => '3dcc4b5c523a8c57a2cd9a9181db65df',
    'mtime' => 1556799522,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_study_director_list.php' => 
  array (
    'md5' => 'bf2fa8e1d8d02c99a847eada86aae59f',
    'mtime' => 1557259182,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.Equipment_Facility_Document.php' => 
  array (
    'md5' => '8c4f3b21a07c922135eac769cfa0db27',
    'mtime' => 1558336726,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.Equipment_Facility_Records.php' => 
  array (
    'md5' => 'c9d9a6ab41aa2c9d079c7c0bf442f6cd',
    'mtime' => 1558336726,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_category_id_list.php' => 
  array (
    'md5' => '1c80c35e10fd99ce87b26dda8920ba69',
    'mtime' => 1558336726,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_company_category_list.php' => 
  array (
    'md5' => '40e42946437941e5685571cde4f9e627',
    'mtime' => 1558336726,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_company_status_list.php' => 
  array (
    'md5' => '095cada856abe308c3a5333f9ccf1c19',
    'mtime' => 1558336726,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_company_type.php' => 
  array (
    'md5' => 'ae315a2d09c89ea813afd8afa060c8b7',
    'mtime' => 1558336726,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_method_of_qualification_list.php' => 
  array (
    'md5' => '17c7374d8310c829e2fefdaa236bdd39',
    'mtime' => 1558336726,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_iso_17025_units_of_measure_list.php' => 
  array (
    'md5' => '93deeb17517b75898c0fe0a339a0f155',
    'mtime' => 1558336726,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_current_vendor_status_list.php' => 
  array (
    'md5' => '9a0288306a44e3ec41123094d06431ed',
    'mtime' => 1558336726,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_reason_for_transfer_list.php' => 
  array (
    'md5' => '07abc3b0de00d396661cfc866c2db8f0',
    'mtime' => 1558336726,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_vendor_level_list.php' => 
  array (
    'md5' => '34a4554f5587bd34b8548901529a2ba1',
    'mtime' => 1558336726,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_vendor_status_list.php' => 
  array (
    'md5' => 'afa46c5adbc1a3e9e39b2cda8eb07607',
    'mtime' => 1558336726,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_usda_category_list.php' => 
  array (
    'md5' => '5c6f5cce8e745e305ed9ee36bd0b3fe1',
    'mtime' => 1560773447,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_iacuc.php' => 
  array (
    'md5' => '3a79b87f863b3aac9eca45c1ffd32e60',
    'mtime' => 1560883572,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_equipment_and_facility_document_type_list.php' => 
  array (
    'md5' => 'f2064a8da1018cffeb9fe934a1a824a9',
    'mtime' => 1560959354,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_address_type_list.php' => 
  array (
    'md5' => 'a9172576345c4eb05d85da9ea92fe13f',
    'mtime' => 1562155655,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.Regulatory_Response_Document.php' => 
  array (
    'md5' => '466a6bdb1127dc9c367168b8443648a9',
    'mtime' => 1562673649,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_vendor_type_list.php' => 
  array (
    'md5' => '95040dc7760bd5474aaf8cc60895f60f',
    'mtime' => 1562760119,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_binder_location_list.php' => 
  array (
    'md5' => 'dc39e13b5ea767c6a83ba34f154b0943',
    'mtime' => 1563884066,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_response_status_list.php' => 
  array (
    'md5' => '3a828d1b6c49d734b26ab5ca6ec6c1a4',
    'mtime' => 1564487119,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_anticipated_study_start_timeline_list.php' => 
  array (
    'md5' => 'f9c7305993493880524aa87aa4bb3d6c',
    'mtime' => 1564573393,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_win_probability_list.php' => 
  array (
    'md5' => '4a9ef533d04c82acb6dbaf526453e608',
    'mtime' => 1564678867,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.customm03_work_product_activities_1_calls.php' => 
  array (
    'md5' => 'bc28b099dea8e9608185c55f91dd976e',
    'mtime' => 1565006593,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_USDA_Exemption.php' => 
  array (
    'md5' => '090add4809cda97376589096f54edbc8',
    'mtime' => 1565264752,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.Contact_Documents.php' => 
  array (
    'md5' => '33a615e060ad5b89db4c8fe6cd084fc8',
    'mtime' => 1565868337,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_contact_document_list.php' => 
  array (
    'md5' => '50ef13813113b107821e09c491920cf1',
    'mtime' => 1565868675,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.Email_Documents.php' => 
  array (
    'md5' => '2b4c83b5b9221207fd0e0d73bbd0a1c5',
    'mtime' => 1566905596,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_classification_list.php' => 
  array (
    'md5' => '2eee1e2398e2d1fc8dd9dbc4b50f0ee6',
    'mtime' => 1568634472,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_equipment_and_facilities_status_list.php' => 
  array (
    'md5' => '05b16ffc800ff749b0cf1d9522971169',
    'mtime' => 1568636131,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.Email_Template.php' => 
  array (
    'md5' => '8b9f200ac3448e5d90ff19df29581f59',
    'mtime' => 1568721884,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.Test_System_Documents.php' => 
  array (
    'md5' => '711dd1f1ea5c24abeb5167c07b358ccd',
    'mtime' => 1569846203,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_sales_activity_quote_req_list.php' => 
  array (
    'md5' => '6c405b477db768b23dbe92147b7b647e',
    'mtime' => 1569846869,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_scientific_activity_list.php' => 
  array (
    'md5' => 'fa09f43d2c88ae91fef5e859780597fd',
    'mtime' => 1570104475,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_aaalac_exemptions_list.php' => 
  array (
    'md5' => 'fe9f9df2417c4f155b7abde7a7aeedf0',
    'mtime' => 1573049433,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_vc_related_to_list.php' => 
  array (
    'md5' => '98ac63d0412f8de8232fb9835e852c6a',
    'mtime' => 1573130840,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_error_classification_c_list.php' => 
  array (
    'md5' => '1a4c0f9b8f5ada9cbff0275c0c37374c',
    'mtime' => 1575293178,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_corrective_action_options_list.php' => 
  array (
    'md5' => 'd64918c149f60970d067cc769a6679a6',
    'mtime' => 1575636901,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_test_system_document_category_list.php' => 
  array (
    'md5' => '10cbe36aa95e787cfe02346f919a8884',
    'mtime' => 1576068581,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_normal_see_gas_list.php' => 
  array (
    'md5' => 'dd15051eb38b3f921045be6de4938732',
    'mtime' => 1579256143,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_oral_cavity_list_not_exam.php' => 
  array (
    'md5' => '0d1a1a98404f3a6e34d4a84ba7e2c757',
    'mtime' => 1579256213,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_oral_cavity_list_normal.php' => 
  array (
    'md5' => '4a05ef7101f3966fc9a4588eef9f5282',
    'mtime' => 1579256280,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_normal_sln_list.php' => 
  array (
    'md5' => '48f34d67a2cb4ea6d80c9262535b6a63',
    'mtime' => 1579256927,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_shipping_company_list.php' => 
  array (
    'md5' => 'b34997b8668ba20ccd5311a4ada9457c',
    'mtime' => 1580822232,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_shipping_delivery_list.php' => 
  array (
    'md5' => '0b655bfba4e2203a8d10ac9e5ce03dc6',
    'mtime' => 1580822454,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_shipping_conditions_list.php' => 
  array (
    'md5' => '7758a25636cb39a3a18b5238eba25c0c',
    'mtime' => 1580822753,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_material_category_list.php' => 
  array (
    'md5' => 'a0e6acc12e2b74024a7b3ccd840309a6',
    'mtime' => 1580823130,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_hazardous_contents_list.php' => 
  array (
    'md5' => 'f660f9ed0ca15066007d251c961e3db0',
    'mtime' => 1580995825,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_deliverable_type_list.php' => 
  array (
    'md5' => '18e5199333fc5e74dec747a6c0bc1084',
    'mtime' => 1584359305,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_quoteco_status_list.php' => 
  array (
    'md5' => 'e26a38587956b03c67ee1cf2cd37d7c4',
    'mtime' => 1591867032,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_equipment_and_facility_records_type_list.php' => 
  array (
    'md5' => 'ba43338028827aa41bcbbf1ab67d3554',
    'mtime' => 1592298285,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_why_it_happened_c_list.php' => 
  array (
    'md5' => 'aad2c646193c025d5d4e30821bea394f',
    'mtime' => 1592366430,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_work_type_list.php' => 
  array (
    'md5' => '649ec21682f368d5ec59b971c18370b7',
    'mtime' => 1593079084,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_product_status_list.php' => 
  array (
    'md5' => 'a93f34f6e88f5c7a416030e413b888b7',
    'mtime' => 1597910823,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_product_notes_multiselect_list.php' => 
  array (
    'md5' => '8acc407731f054e27db8eb874ee1813c',
    'mtime' => 1597911014,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.GDP_Examples.php' => 
  array (
    'md5' => '3c3ea8a4f4c49c7f4e2b68f8e8563455',
    'mtime' => 1598514455,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_prd_level_list.php' => 
  array (
    'md5' => '4cb9583c0abe00c018c4bbfb70947394',
    'mtime' => 1599557913,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.Product_Document.php' => 
  array (
    'md5' => 'bb9cb32b109cd5b443b152b4b1cd271a',
    'mtime' => 1599726233,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_purchase_unit_list.php' => 
  array (
    'md5' => 'a62a495ab4e40a65e6fd956d6149a656',
    'mtime' => 1600948057,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_ef_status_list.php' => 
  array (
    'md5' => 'd4f105a10a0094a384e0ff1f470037e4',
    'mtime' => 1601966186,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_disposition_type_deliverable_list.php' => 
  array (
    'md5' => '5ae6d1a195c1500cb0ac1fac50411dfc',
    'mtime' => 1605600870,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_expense_or_capitalize_list.php' => 
  array (
    'md5' => '255fca2fba9e7d23813b23b91e41dd09',
    'mtime' => 1607419798,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_high_level_root_cause_action_list.php' => 
  array (
    'md5' => 'c7467f26a8d7b0a2d5812e0bf32ff202',
    'mtime' => 1608007852,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_impactful_list.php' => 
  array (
    'md5' => '98499e7b808c92f707ffb2cb011e89bd',
    'mtime' => 1608008475,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_management_assessment_train_list.php' => 
  array (
    'md5' => '0386ba1296f2c980a97e3c9c5c5e19fa',
    'mtime' => 1608008667,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_iacuc_deficiency_class_list.php' => 
  array (
    'md5' => '67a25013d6423f5f8062d2fee48af08c',
    'mtime' => 1608009011,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_wps_outcome_wpe_activities_list.php' => 
  array (
    'md5' => 'a7216479996afc426b6cb80b15a48766',
    'mtime' => 1608013591,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_ed_type_list.php' => 
  array (
    'md5' => '05455fa6e107f13646c662f75866d8ac',
    'mtime' => 1608033933,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_enrollment_status_list.php' => 
  array (
    'md5' => 'f0f6c51b53b5d9e886460ae9e67d1afc',
    'mtime' => 1608035942,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_species_sp_list.php' => 
  array (
    'md5' => 'd86c23f8c188a29dd1f8447b53b888e3',
    'mtime' => 1610451348,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_service_type_list.php' => 
  array (
    'md5' => 'a34e9093162568f0141cfc314913ccb5',
    'mtime' => 1610451653,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_service_format_list.php' => 
  array (
    'md5' => '4f6a3b7030819a0612a5ffee94c8f85a',
    'mtime' => 1611825082,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.Controlled_Documents_QA_Review_Docs.php' => 
  array (
    'md5' => 'f043208884577f590e5fdd48148a3e2c',
    'mtime' => 1613028945,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_stability_considerations_list.php' => 
  array (
    'md5' => 'c9779e463c570a9f80f89edceb37c831',
    'mtime' => 1613626782,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_paraffin_plastic_list.php' => 
  array (
    'md5' => '212a5837f4f8d768c2db59f6e1e0288c',
    'mtime' => 1617700752,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_test_system_list.php' => 
  array (
    'md5' => '777767ae472aeffd4a3a5c180fcdcaaa',
    'mtime' => 1621501185,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_deviation_rate_basis_list.php' => 
  array (
    'md5' => '98f15460210f306d3b0b7e4f9aaa2910',
    'mtime' => 1624956271,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_final_report_timeline_type_list.php' => 
  array (
    'md5' => 'a5d5ce3af0abaa2fb140def1ee8c3863',
    'mtime' => 1626936093,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_red_yellow_green_list.php' => 
  array (
    'md5' => 'db12fe8852f510bc527770916fa7db97',
    'mtime' => 1626944982,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_category_list.php' => 
  array (
    'md5' => 'b5570e8d93aecbeb8b420c253d4169b1',
    'mtime' => 1628146975,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_minutes_per_slide_list.php' => 
  array (
    'md5' => '2ab7537350af515229d9cdca6a089745',
    'mtime' => 1628758936,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_procedure_room_list.php' => 
  array (
    'md5' => '1c30bc72935ff65d36d044980780e4da',
    'mtime' => 1628836478,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_action_needed_list.php' => 
  array (
    'md5' => 'c84fb72d52e85a0b78316452c4e392d8',
    'mtime' => 1634194533,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_grant_submission_sa_2_list.php' => 
  array (
    'md5' => 'a4534a916c87f4811a2b643851f8fcdd',
    'mtime' => 1635239007,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_reason_for_expansion_list.php' => 
  array (
    'md5' => 'dd4c097f05c413501f2317ae5ab54d0d',
    'mtime' => 1635239205,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_gd_study_article_type_list.php' => 
  array (
    'md5' => '6d55696dd6f3a56f5bd3cda0abee321c',
    'mtime' => 1635411073,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_stability_considerations_ii_list.php' => 
  array (
    'md5' => '6233ff3f6403089e79f7c53933f52164',
    'mtime' => 1636456212,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_yes_no_na_list.php' => 
  array (
    'md5' => 'd036f3e271f66989d6a89af868f0add5',
    'mtime' => 1636456603,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_inventory_item_category_list.php' => 
  array (
    'md5' => '27ae35574a0a8202f88d3738a69207b4',
    'mtime' => 1636503980,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_inventory_item_owner_list.php' => 
  array (
    'md5' => 'd04bb2355f56b01c689227f708cc30df',
    'mtime' => 1636504316,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_im_location_type_list.php' => 
  array (
    'md5' => 'e72d8decc706873b85440c38ca55c083',
    'mtime' => 1636505452,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_inventory_item_status_list.php' => 
  array (
    'md5' => 'd3f22e19450f8c637db93a5a92a353f8',
    'mtime' => 1636506083,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_timepoint_type_list.php' => 
  array (
    'md5' => '7662e986cfed828767a3824228350dc2',
    'mtime' => 1636510895,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_inventory_item_storage_condition_list.php' => 
  array (
    'md5' => 'fde3f4edc2bdfabd82fdc85cda2d670f',
    'mtime' => 1636511655,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_yes_no_list.php' => 
  array (
    'md5' => '5da4f6a51f326c5dbf29e77afd26489e',
    'mtime' => 1636553500,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_aaalac_and_usda_exemptions_list.php' => 
  array (
    'md5' => 'f5b96a1f7abec488bede882f619a4b13',
    'mtime' => 1637230763,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_tsd_wt_type_list.php' => 
  array (
    'md5' => 'de875780f22b420ca6ad07c1c8d6114d',
    'mtime' => 1638275422,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_td_phase_list.php' => 
  array (
    'md5' => 'ed9c160c1b3642f3b9dfb226f9e9845f',
    'mtime' => 1638437575,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_procedure_type_com_list.php' => 
  array (
    'md5' => 'c46a0156bbf56669b0f9813df244021c',
    'mtime' => 1638867776,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_TSD_purpose_list.php' => 
  array (
    'md5' => '00e6bd5667e0f45d8653b205f63a482e',
    'mtime' => 1638869662,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_housing_requirements_list.php' => 
  array (
    'md5' => '9ab0c4f8b4588c01c898a9fb2206c072',
    'mtime' => 1638870932,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_number_list.php' => 
  array (
    'md5' => 'a18f5dcc9c7be46b17b07a6764b67a95',
    'mtime' => 1638873167,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_sales_study_article_status_list.php' => 
  array (
    'md5' => 'd86c5d0615e17d5c80aa10cb9595b7d8',
    'mtime' => 1638880077,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_degree_of_immobility_list.php' => 
  array (
    'md5' => '813a6540271976e55c9279558f984284',
    'mtime' => 1639472920,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_type_of_acclimation_list.php' => 
  array (
    'md5' => '0f00b3f936960d72405c1a82b4157fc3',
    'mtime' => 1639472981,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_inspection_results_list.php' => 
  array (
    'md5' => '472e38bc5ca1b6901fc8116fc9b00b69',
    'mtime' => 1640075560,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_or_status_list.php' => 
  array (
    'md5' => 'd2963100a7dce372a1a526763fa86828',
    'mtime' => 1640676035,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_tpr_reference_list.php' => 
  array (
    'md5' => 'de511cdd6102af33746812a5799dd028',
    'mtime' => 1641537753,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_inventory_item_type_list.php' => 
  array (
    'md5' => '034f3a19be4d03ab236cf84c14c560bd',
    'mtime' => 1642489496,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_universal_inventory_management_type_list.php' => 
  array (
    'md5' => 'b0271b6cf653d1882ecd14046a442e51',
    'mtime' => 1642490315,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_type_specimen_list.php' => 
  array (
    'md5' => '2b1f82dea0be3cc8653e4b25753e1066',
    'mtime' => 1643102231,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_phase_of_inspection_list.php' => 
  array (
    'md5' => 'eee185bc24aea3b5b32dfb9d1c05cab6',
    'mtime' => 1643700661,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_room_type_list.php' => 
  array (
    'md5' => '93765903903df12258f3764d068ddab0',
    'mtime' => 1643701129,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.CAPA_Files.php' => 
  array (
    'md5' => '0d0129b6cfebf6a53657c764140469f6',
    'mtime' => 1643872463,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_ca_andor_pa_dd_list.php' => 
  array (
    'md5' => '52662a38f3a5e7c1a9897ed620f310f7',
    'mtime' => 1643875796,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_frequency_score_list.php' => 
  array (
    'md5' => '715cf917588dcbdba2e6edf65402cd10',
    'mtime' => 1643876686,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_severity_score_list.php' => 
  array (
    'md5' => '122766d9d6ac931c08e85959665e31b2',
    'mtime' => 1643876846,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_GD_status_list.php' => 
  array (
    'md5' => '3aeceeab61135077a039ab809fea2f01',
    'mtime' => 1643881435,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_task_type_list.php' => 
  array (
    'md5' => '603e68386c6cad4de3f870d11864677c',
    'mtime' => 1643882360,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_ot_category_list.php' => 
  array (
    'md5' => '013f9234fb94b5661877400bea8cc3e2',
    'mtime' => 1644306090,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_ot_type_list.php' => 
  array (
    'md5' => '5b6229b22b4be37bb2ac0cc6847bbc77',
    'mtime' => 1644306166,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_organ_system_list.php' => 
  array (
    'md5' => 'bd1f776001f4131a7c2caa8c9a21286e',
    'mtime' => 1645082237,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_po_status_list.php' => 
  array (
    'md5' => '18ace421745b1324d13c3790bba55e40',
    'mtime' => 1645513654,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_deliverable_status_list.php' => 
  array (
    'md5' => 'f8ec8407ea20604b00b8b36cbfd45329',
    'mtime' => 1645515388,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_ii_related_to_c_list.php' => 
  array (
    'md5' => '2464bf1068b64a975b98b2a7d5809e62',
    'mtime' => 1645691005,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_specimen_type_list.php' => 
  array (
    'md5' => '842770c23b8e4d57ea9c48c90f813c7d',
    'mtime' => 1645710876,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_specimen_type_no_other_c_list.php' => 
  array (
    'md5' => 'd27d8633b73526d1624cd54a31b4e5cf',
    'mtime' => 1645710968,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_integration_work_stream_list.php' => 
  array (
    'md5' => '8fc1eb9e7049c6c60d2c87e9310720dd',
    'mtime' => 1647329017,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_plan_actual_list.php' => 
  array (
    'md5' => '672eb54fd9fe603def38182b55891f65',
    'mtime' => 1647329467,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_td_audit_phase_list.php' => 
  array (
    'md5' => '3e215929d7a447c141aaafd1410d7206',
    'mtime' => 1648111997,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_sale_document_category_list.php' => 
  array (
    'md5' => 'e2dd698a8b9ddd47c5f607fd3f8187f4',
    'mtime' => 1648727216,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_product_category_list.php' => 
  array (
    'md5' => '42fe3a34247aec56f83b85559ed0a49d',
    'mtime' => 1648728291,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_Breed_List.php' => 
  array (
    'md5' => '0507cdc02f0ee263620332db55a9e1f1',
    'mtime' => 1649143293,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_ii_test_type_list.php' => 
  array (
    'md5' => '61019cdcc55e76582570a4faaa202f36',
    'mtime' => 1649754237,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_task_procedure_list.php' => 
  array (
    'md5' => '3a946c112c1ee3e2ce8757c1567d2926',
    'mtime' => 1650530004,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_bid_status_list.php' => 
  array (
    'md5' => '43891b9255aa22456e07efef8418a533',
    'mtime' => 1650956950,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_biod_dose_route_list.php' => 
  array (
    'md5' => '4cec842f949a89ebf1efbc44f62f3aae',
    'mtime' => 1650957457,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_inventory_management_type_list.php' => 
  array (
    'md5' => 'f56a9fd88ef1fc95d5ec22906a58fcc3',
    'mtime' => 1652944904,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_wpc_type_list.php' => 
  array (
    'md5' => '934b0126055d992d74fc1db1ab7c0a85',
    'mtime' => 1653375918,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_work_product_status_list.php' => 
  array (
    'md5' => '0f4b4e25e72f5cfa1dc2c0b74298b06c',
    'mtime' => 1653376538,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_aaalac_requirements_list.php' => 
  array (
    'md5' => 'ca05385774a09b5ebf9f5dccf423ff31',
    'mtime' => 1654579535,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_usda_exemptions_list.php' => 
  array (
    'md5' => 'd0b814955b77707e419fa5d93a00487f',
    'mtime' => 1654579774,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_department_list.php' => 
  array (
    'md5' => '15f58dd1d899d44a6c1fe26277197dfb',
    'mtime' => 1655358264,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_location_list.php' => 
  array (
    'md5' => '924c33e8f1331adf7f6ba8f28dbf215f',
    'mtime' => 1655792800,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_extraction_conditions_list.php' => 
  array (
    'md5' => '5e52eddbf6c0c862e784f2997f792fdf',
    'mtime' => 1657618162,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_payment_terms_list.php' => 
  array (
    'md5' => 'd37f132ea490fcd6d99c271825cf237c',
    'mtime' => 1658990483,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_error_category_list.php' => 
  array (
    'md5' => '0206ddd998916183628b2e113b07a017',
    'mtime' => 1664862473,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_deliverable_list.php' => 
  array (
    'md5' => '55e23375a115c1b9d17c1dafaa3fb8f5',
    'mtime' => 1666701722,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_equipment_required_list.php' => 
  array (
    'md5' => 'be630d09d0e2e50248613a0a45f4f62e',
    'mtime' => 1666849962,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_completion_status_list.php' => 
  array (
    'md5' => 'bb83180b122c372f53ee544d791c5e4d',
    'mtime' => 1667886252,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_standard_task_list.php' => 
  array (
    'md5' => 'bd0ec8fd46d0e5183b8dce4ad001e13a',
    'mtime' => 1667886843,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_diagnosis_list.php' => 
  array (
    'md5' => '7fbb47646102b365642c6010a44073a5',
    'mtime' => 1667898727,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_condition_list.php' => 
  array (
    'md5' => '36bf613fcd61c3e37131928fdec7d04c',
    'mtime' => 1667898808,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_amendment_request_list.php' => 
  array (
    'md5' => '4915a6b89802010df2a0c058a83f1d94',
    'mtime' => 1669873031,
    'is_override' => false,
  ),
  'custom/Extension/application/Ext/Language/zh_TW.sugar_error_type_list.php' => 
  array (
    'md5' => '5391c9b8734e53d9e7178336004a4179',
    'mtime' => 1669887998,
    'is_override' => false,
  ),
);