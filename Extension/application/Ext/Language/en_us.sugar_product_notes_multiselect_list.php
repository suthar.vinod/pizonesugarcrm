<?php
 // created: 2020-08-13 15:52:10

$app_list_strings['product_notes_multiselect_list']=array (
  '' => '',
  'Cannot Procure' => 'Cannot Procure',
  'Long Lead Time' => 'Long Lead Time',
  'Often Backordered' => 'Often Backordered',
  'Short Supply' => 'Short Supply',
);