<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Chamada Espontânea',
  'Existing Customer' => 'Cliente Existente',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Colaborador',
  'Conference' => 'Conferência',
  'Trade Show' => 'Feira/Evento',
  'Web Site' => 'Site de Internet',
  'Email' => 'E-mail Qualquer',
  'Campaign' => 'Campanha',
  'Support Portal User Registration' => 'Registro para usuários do Portal de Suporte',
  'Other' => 'Outro',
  'Referral' => 'Referral',
);