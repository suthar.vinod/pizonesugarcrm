<?php
 // created: 2021-12-07 09:02:56

$app_list_strings['procedure_type_com_list']=array (
  '' => '',
  'Acute' => 'Acute',
  'Initial' => 'Initial',
  'Follow up' => 'Follow up',
  'Screening' => 'Screening',
  'Termination' => 'Termination',
);