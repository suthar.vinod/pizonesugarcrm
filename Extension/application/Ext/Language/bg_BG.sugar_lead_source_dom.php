<?php
 // created: 2016-01-07 15:55:32

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Телефонен разговор',
  'Existing Customer' => 'Съществуващ клиент',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Служители',
  'Conference' => 'Конференция',
  'Trade Show' => 'Търговско изложение',
  'Web Site' => 'Сайт на компанията',
  'Email' => 'Електронна поща',
  'Campaign' => 'Кампания',
  'Support Portal User Registration' => 'Регистрация на потребители в Портал',
  'Other' => 'Други',
  'Referral' => 'Referral',
);