<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Ψυχρό Τηλεφώνημα',
  'Existing Customer' => 'Δυνητικό Πελάτη',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Εργαζόμενος',
  'Conference' => 'Συνέδριο',
  'Trade Show' => 'Εμπορική Έκθεση',
  'Web Site' => 'Ιστοσελίδα',
  'Email' => 'Οποιοδήποτε Email',
  'Campaign' => 'Εκστρατεία',
  'Support Portal User Registration' => 'Εγγραφή Χρήστη στο Portal',
  'Other' => 'Άλλο:',
  'Referral' => 'Referral',
);