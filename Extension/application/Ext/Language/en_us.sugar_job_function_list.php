<?php
 // created: 2016-01-07 17:50:41

$app_list_strings['job_function_list']=array (
  'blank' => '',
  'research and development' => 'R & D',
  'preclinical' => 'Preclinical',
  'consultant' => 'Consultant',
  'regulatory' => 'Regulatory',
  'clinical' => 'Clinical',
  'management' => 'Management',
  'executive management' => 'Executive Management',
);