<?php
 // created: 2017-03-15 20:10:34

$app_list_strings['contact_duration_list']=array (
  'Limited' => 'Limited',
  'Prolonged_30' => 'Prolonged, ',
  'Permanent' => 'Permanent, >30 days',
  'Unknown' => 'Unknown',
  'Choose one...' => 'Choose One...',
);