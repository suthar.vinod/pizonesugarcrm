<?php
 // created: 2020-02-04 13:20:54

$app_list_strings['shipping_delivery_list']=array (
  '' => '',
  'Direct' => 'Direct',
  'Same Day' => 'Same Day',
  '1 HR' => '1 HR',
  '3 HR' => '3 HR',
  'International Priority' => 'International Priority',
  'First Priority Overnight' => 'First Priority Overnight',
  'Priority Overnight' => 'Priority Overnight',
  '2 Day' => '2 Day',
  'Ground' => 'Ground',
  '2 HR' => '2 HR',
);