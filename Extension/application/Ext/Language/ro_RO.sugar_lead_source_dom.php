<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Prospectare',
  'Existing Customer' => 'Client existent',
  'Self Generated' => 'Autogenerat',
  'Employee' => 'Angajat',
  'Conference' => 'Conferinta',
  'Trade Show' => 'Expozitie',
  'Web Site' => 'Website',
  'Email' => 'Oricare Email:',
  'Campaign' => 'Campanie',
  'Support Portal User Registration' => 'Inregistrare utilizator pe portal suport',
  'Other' => 'Altul',
  'Referral' => 'Referral',
);