<?php
 // created: 2021-11-10 02:21:35

$app_list_strings['timepoint_type_list']=array (
  '' => '',
  'AcuteBaseline' => 'Acute/Baseline',
  'AcuteTermination' => 'Acute/Termination',
  'Ad Hoc' => 'Ad Hoc',
  'ChronicBaseline' => 'Chronic/Baseline',
  'ChronicTreatment' => 'Chronic/Treatment',
  'Defined' => 'Defined',
  'Follow up' => 'Follow-up',
  'Model CreationBaseline' => 'Model Creation/Baseline',
  'None' => 'None',
  'Receipt' => 'Receipt',
  'Termination' => 'Termination',
  'Vet Order' => 'Vet Order',
  'Work Product Schedule' => 'Work Product Schedule',
);