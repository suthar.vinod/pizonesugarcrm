<?php
 // created: 2022-02-17 07:17:17

$app_list_strings['organ_system_list']=array (
  '' => '',
  'Cardiovascular' => 'Cardiovascular',
  'GI' => 'GI',
  'Integumentary' => 'Integumentary',
  'Musculoskeletal' => 'Musculoskeletal',
  'Neurologic' => 'Neurologic',
  'Ophthalmic' => 'Ophthalmic',
  'Respiratory' => 'Respiratory',
  'Surgical' => 'Surgical',
  'Systemic' => 'Systemic',
  'Unknown ADR' => 'Unknown-ADR',
  'Urogential' => 'Urogential',
  'Lymphatic' => 'Lymphatic',
  'Clin Path' => 'Clin Path',
  'Not Applicable' => 'Not Applicable',
);