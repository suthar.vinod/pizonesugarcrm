<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Пропозиція товарів або послуг за телефоном',
  'Existing Customer' => 'Існуючий клієнт',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Співробітник',
  'Conference' => 'Конференція',
  'Trade Show' => 'Комерційний перегляд',
  'Web Site' => 'Веб-сторінка',
  'Email' => 'Email',
  'Campaign' => 'Маркетингова кампанія',
  'Support Portal User Registration' => 'Реєстрація користувача на Support Portal',
  'Other' => 'Інше',
  'Referral' => 'Referral',
);