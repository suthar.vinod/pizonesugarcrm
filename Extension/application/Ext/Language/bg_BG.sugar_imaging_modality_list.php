<?php
 // created: 2018-05-21 16:38:20

$app_list_strings['imaging_modality_list']=array (
  '' => '',
  'Angiography' => 'Angiography',
  'CT' => 'CT',
  'Ex_Vivo' => 'Ex-Vivo',
  'IVUS' => 'IVUS',
  'MRI' => 'MRI',
  'OCT' => 'OCT',
  'Ultrasound' => 'Ultrasound',
);