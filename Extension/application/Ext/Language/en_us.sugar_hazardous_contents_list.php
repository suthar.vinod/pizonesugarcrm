<?php
 // created: 2020-02-06 13:30:25

$app_list_strings['hazardous_contents_list']=array (
  '' => '',
  'Biological Substance Category B' => 'Biological Substance – Category B',
  'Ethanol ETOH' => 'Ethanol (ETOH)',
  'Glutaraldehyde' => 'Glutaraldehyde',
  'Hexane' => 'Hexane',
  'Methanol' => 'Methanol',
  'NA' => 'NA',
  'Other' => 'Other',
);