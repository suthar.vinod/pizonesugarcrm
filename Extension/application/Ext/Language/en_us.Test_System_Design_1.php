<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['TSD1_Test_System_Design_1'] = 'Test System Designs';
$app_list_strings['moduleListSingular']['TSD1_Test_System_Design_1'] = 'Test System Design';
$app_list_strings['number_list']['01'] = '01';
$app_list_strings['number_list']['02'] = '02';
$app_list_strings['number_list']['03'] = '03';
$app_list_strings['number_list']['04'] = '04';
$app_list_strings['number_list']['05'] = '05';
$app_list_strings['number_list']['06'] = '06';
$app_list_strings['number_list']['07'] = '07';
$app_list_strings['number_list']['08'] = '08';
$app_list_strings['number_list']['09'] = '09';
$app_list_strings['number_list'][10] = '10';
$app_list_strings['number_list'][''] = '';
$app_list_strings['Breed_List']['Alpine'] = 'Alpine';
$app_list_strings['Breed_List']['Athymic Nude'] = 'Athymic Nude';
$app_list_strings['Breed_List']['BALBc'] = 'BALB/c';
$app_list_strings['Breed_List']['Beagle'] = 'Beagle';
$app_list_strings['Breed_List']['C3H HeJ'] = 'C3H/HeJ';
$app_list_strings['Breed_List']['C57'] = 'C57';
$app_list_strings['Breed_List']['CD Hairless'] = 'CD Hairless';
$app_list_strings['Breed_List']['CD1'] = 'CD1';
$app_list_strings['Breed_List']['CF1'] = 'CF1';
$app_list_strings['Breed_List']['Cross Breed'] = 'Cross Breed';
$app_list_strings['Breed_List']['Dorset'] = 'Dorset';
$app_list_strings['Breed_List']['Dutch Belted'] = 'Dutch Belted';
$app_list_strings['Breed_List']['Freisen'] = 'Freisen';
$app_list_strings['Breed_List']['Golden Syrian'] = 'Golden Syrian';
$app_list_strings['Breed_List']['Gottingen'] = 'Gottingen';
$app_list_strings['Breed_List']['Hanford'] = 'Hanford';
$app_list_strings['Breed_List']['Hartley'] = 'Hartley';
$app_list_strings['Breed_List']['Holstein'] = 'Holstein';
$app_list_strings['Breed_List']['Jersey'] = 'Jersey';
$app_list_strings['Breed_List']['Lamancha'] = 'Lamancha';
$app_list_strings['Breed_List']['LDLR'] = 'LDLR Yucatan';
$app_list_strings['Breed_List']['Micro Yucatan'] = 'Micro-Yucatan';
$app_list_strings['Breed_List']['Mongrel'] = 'Mongrel';
$app_list_strings['Breed_List']['Myotonic'] = 'Myotonic';
$app_list_strings['Breed_List']['ND4'] = 'ND4';
$app_list_strings['Breed_List']['New Zealand White'] = 'New Zealand White';
$app_list_strings['Breed_List']['Ossabaw'] = 'Ossabaw';
$app_list_strings['Breed_List']['Polypay'] = 'Polypay';
$app_list_strings['Breed_List']['Saanen'] = 'Saanen';
$app_list_strings['Breed_List']['Sinclair'] = 'Sinclair';
$app_list_strings['Breed_List']['SKH1 Hairless'] = 'SKH1 Hairless';
$app_list_strings['Breed_List']['Sprague Dawley'] = 'Sprague Dawley';
$app_list_strings['Breed_List']['Suffolk'] = 'Suffolk';
$app_list_strings['Breed_List']['Suffolk X'] = 'Suffolk X';
$app_list_strings['Breed_List']['Toggenburg'] = 'Toggenburg';
$app_list_strings['Breed_List']['Watanabe'] = 'Watanabe';
$app_list_strings['Breed_List']['Wistar'] = 'Wistar';
$app_list_strings['Breed_List']['Yorkshire X'] = 'Yorkshire X';
$app_list_strings['Breed_List']['Yucatan'] = 'Yucatan';
$app_list_strings['Breed_List'][''] = '';
$app_list_strings['strain_list']['E coli WP2 uvrA'] = 'E.coli WP2-uvrA';
$app_list_strings['strain_list']['Salmonella TA97a'] = 'Salmonella- TA97a';
$app_list_strings['strain_list']['Salmonella TA98'] = 'Salmonella- TA98';
$app_list_strings['strain_list']['Salmonella TA100'] = 'Salmonella- TA100';
$app_list_strings['strain_list']['Salmonella TA1535'] = 'Salmonella- TA1535';
$app_list_strings['strain_list']['Salmonella TA102'] = 'Salmonella- TA102';
$app_list_strings['strain_list']['Salmonella TA1537'] = 'Salmonella- TA1537';
$app_list_strings['strain_list'][''] = '';
$app_list_strings['tsd_sex_list']['M F CM'] = 'M, F, CM';
$app_list_strings['tsd_sex_list']['CM or F'] = 'CM or F';
$app_list_strings['tsd_sex_list']['M or F'] = 'M or F';
$app_list_strings['tsd_sex_list']['M'] = 'M';
$app_list_strings['tsd_sex_list']['F'] = 'F';
$app_list_strings['tsd_sex_list']['CM'] = 'CM';
$app_list_strings['tsd_sex_list']['CM or M'] = 'CM or M';
$app_list_strings['tsd_sex_list']['Equal MF'] = 'Equal M/F';
$app_list_strings['tsd_sex_list']['Equal MFCM'] = 'Equal M/F/CM';
$app_list_strings['tsd_sex_list']['Equal CMF'] = 'Equal CM/F';
$app_list_strings['tsd_sex_list']['Equal CMM'] = 'Equal CM/M';
$app_list_strings['tsd_sex_list']['Qty Specific Required'] = 'Qty. Specific Required';
$app_list_strings['tsd_sex_list'][''] = '';
$app_list_strings['tsd1_sex_list']['CM'] = 'CM';
$app_list_strings['tsd1_sex_list']['CM or F'] = 'CM or F';
$app_list_strings['tsd1_sex_list']['CM or M'] = 'CM or M';
$app_list_strings['tsd1_sex_list']['Equal CMF'] = 'Equal CM/F';
$app_list_strings['tsd1_sex_list']['Equal CMM'] = 'Equal CM/M';
$app_list_strings['tsd1_sex_list']['Equal MF'] = 'Equal M/F';
$app_list_strings['tsd1_sex_list']['Equal MFCM'] = 'Equal M/F/CM';
$app_list_strings['tsd1_sex_list']['F'] = 'F';
$app_list_strings['tsd1_sex_list']['M'] = 'M';
$app_list_strings['tsd1_sex_list']['M or F'] = 'M or F';
$app_list_strings['tsd1_sex_list']['M F CM'] = 'M, F, CM';
$app_list_strings['tsd1_sex_list']['Quantity Specific'] = 'Quantity Specific';
$app_list_strings['tsd1_sex_list'][''] = '';
$app_list_strings['animal_source_type_list']['Multiple Acceptable'] = 'Multiple Acceptable';
$app_list_strings['animal_source_type_list']['Single'] = 'Single';
$app_list_strings['animal_source_type_list']['Specific'] = 'Specific';
$app_list_strings['animal_source_type_list'][''] = '';
$app_list_strings['min_age_type_list']['Actual'] = 'Actual';
$app_list_strings['min_age_type_list']['2 months'] = '> 2 months';
$app_list_strings['min_age_type_list']['9 months'] = '> 9 months';
$app_list_strings['min_age_type_list']['Age appropriate to weight'] = 'Age appropriate to weight';
$app_list_strings['min_age_type_list'][''] = '';
$app_list_strings['max_age_type_list']['Actual'] = 'Actual';
$app_list_strings['max_age_type_list']['36 months'] = '36 months';
$app_list_strings['max_age_type_list']['Age appropriate to weight'] = 'Age appropriate to weight';
$app_list_strings['max_age_type_list'][''] = '';
$app_list_strings['tsd_wt_type_list']['Actual'] = 'Actual';
$app_list_strings['tsd_wt_type_list']['Actual or having suitable anatomy'] = 'Actual or having suitable anatomy';
$app_list_strings['tsd_wt_type_list'][''] = '';
