<?php
 // created: 2022-11-08 09:12:07

$app_list_strings['diagnosis_list']=array (
  '' => '',
  'VSD' => 'VSD',
  'Valve Regurg' => 'Valve Regurg',
  'Unknown' => 'Unknown',
  'Congenital' => 'Congenital',
  'Incidental' => 'Incidental',
  'Heart block' => 'Heart block',
  'A Fib' => 'A Fib',
  'Tachycardia' => 'Tachycardia',
  'Bradycardia' => 'Bradycardia',
  'Muffled heart sounds' => 'Muffled heart sounds',
  'Post Op' => 'Post-Op',
  'Undefined' => 'Undefined',
  'Hemorrhage' => 'Hemorrhage',
  'Cardiac disease' => 'Cardiac disease',
  'Cardiac' => 'Cardiac',
  'Pulmonary' => 'Pulmonary',
  'Brachyspira' => 'Brachyspira',
  'Lawsonia' => 'Lawsonia',
  'Salmonella' => 'Salmonella',
  'E coli' => 'E coli',
  'Porcine Parvovirus' => 'Porcine Parvovirus',
  'Giardia' => 'Giardia',
  'Coccidia' => 'Coccidia',
  'Monesia' => 'Monesia',
  'Stress' => 'Stress',
  'Cryptosporidium' => 'Cryptosporidium',
  'Dietary intolerance' => 'Dietary intolerance',
  'Hemorrhagic gastroenteritis' => 'Hemorrhagic gastroenteritis',
  'Dental Disease' => 'Dental Disease',
  'Tartar' => 'Tartar',
  'Gingivitis' => 'Gingivitis',
  'Broken tooth' => 'Broken tooth',
  'Foreign Body' => 'Foreign Body',
  'Volvulus' => 'Volvulus',
  'Primary GI' => 'Primary GI',
  'Secondary GI' => 'Secondary GI',
  'Dietary indiscretion' => 'Dietary indiscretion',
  'Pancreatitis' => 'Pancreatitis',
  'Hepatic disease' => 'Hepatic disease',
  'Choke' => 'Choke',
  'Peritoneal effusion' => 'Peritoneal effusion',
  'Bloat' => 'Bloat',
  'Organomegaly' => 'Organomegaly',
  'Ulcer' => 'Ulcer',
  'Parasitism' => 'Parasitism',
  'Roundworm' => 'Roundworm',
  'Strongile' => 'Strongile',
  'NSAID' => 'NSAID',
  'Rumen' => 'Rumen',
  'Monogastric' => 'Monogastric',
  'GI disease' => 'GI disease',
  'Urinary disease' => 'Urinary disease',
  'Colitis' => 'Colitis',
  'Oral disease' => 'Oral disease',
  'Oral injury' => 'Oral injury',
  'Nausea' => 'Nausea',
  'Neurologic' => 'Neurologic',
  'Abscess' => 'Abscess',
  'Acquired' => 'Acquired',
  'Allergic' => 'Allergic',
  'Anemia' => 'Anemia',
  'Anesthesia recovery' => 'Anesthesia recovery',
  'Animal Altercation' => 'Animal Altercation',
  'Aspiration' => 'Aspiration',
  'Barbaring' => 'Barbaring',
  'Cardiogenic' => 'Cardiogenic',
  'Carpal Valgus' => 'Carpal Valgus',
  'Carpal Varus' => 'Carpal Varus',
  'Cataract' => 'Cataract',
  'Cellulitis' => 'Cellulitis',
  'Central nervous' => 'Central nervous',
  'Cherry Eye' => 'Cherry Eye',
  'Chronic inflammation' => 'Chronic inflammation',
  'CLA' => 'CLA',
  'Coagulopathy' => 'Coagulopathy',
  'Congestion' => 'Congestion',
  'Conjunctivitis' => 'Conjunctivitis',
  'Corneal Edema' => 'Corneal Edema',
  'Corneal Ulcer' => 'Corneal Ulcer',
  'Cystitis' => 'Cystitis',
  'Dermatitis' => 'Dermatitis',
  'Disuse' => 'Disuse',
  'Dystrophy' => 'Dystrophy',
  'Edema' => 'Edema',
  'Environment' => 'Environment',
  'Epistaxis' => 'Epistaxis',
  'Estrus' => 'Estrus',
  'Extracranial' => 'Extracranial',
  'Fleas' => 'Fleas',
  'Fracture' => 'Fracture',
  'Full thickness dehiscence' => 'Full thickness dehiscence',
  'Hematoma' => 'Hematoma',
  'Hematomaseroma' => 'Hematoma/seroma',
  'Hematuria' => 'Hematuria',
  'Hemoglobinuria' => 'Hemoglobinuria',
  'Hoof Crack' => 'Hoof Crack',
  'Hyphema' => 'Hyphema',
  'Hypoglycemia' => 'Hypoglycemia',
  'Hypotension' => 'Hypotension',
  'Idiopathic' => 'Idiopathic',
  'Incisional Swelling' => 'Incisional Swelling',
  'Infection' => 'Infection',
  'Inflammation' => 'Inflammation',
  'Injection site' => 'Injection site',
  'Injection Site Abscess' => 'Injection Site Abscess',
  'Injury' => 'Injury',
  'Interdigital Cyst' => 'Interdigital Cyst',
  'Intracranial' => 'Intracranial',
  'Intubation' => 'Intubation',
  'Joint disease' => 'Joint disease',
  'Joint injury' => 'Joint injury',
  'Laminitis' => 'Laminitis',
  'Lice' => 'Lice',
  'Malignant hyperthermia' => 'Malignant hyperthermia',
  'Mange' => 'Mange',
  'Mastitis' => 'Mastitis',
  'Medication' => 'Medication',
  'Melenoma' => 'Melenoma',
  'Mites' => 'Mites',
  'Mycoplasma' => 'Mycoplasma',
  'Neoplasia' => 'Neoplasia',
  'Neoplasia undefined' => 'Neoplasia-undefined',
  'Neovascularization Pannus' => 'Neovascularization/ Pannus',
  'Non cardiogenic' => 'Non-cardiogenic',
  'Orf' => 'Orf',
  'Orthopedic injury' => 'Orthopedic injury',
  'Otitis externa' => 'Otitis externa',
  'Otitis interna' => 'Otitis interna',
  'Pain' => 'Pain',
  'Parasitic' => 'Parasitic',
  'ParesisParalysis' => 'Paresis/Paralysis',
  'Peripheral nervous' => 'Peripheral nervous',
  'Physiologic' => 'Physiologic',
  'Physiological' => 'Physiological',
  'Pneumonia' => 'Pneumonia',
  'Porphyrinuria' => 'Porphyrinuria',
  'Post clipping' => 'Post-clipping',
  'Post renal' => 'Post-renal',
  'Pre renal' => 'Pre-renal',
  'Pregnancy' => 'Pregnancy',
  'Pseudomonas' => 'Pseudomonas',
  'Pulmonary disease' => 'Pulmonary disease',
  'Purpura' => 'Purpura',
  'Pyelonephritis' => 'Pyelonephritis',
  'Pyoderma' => 'Pyoderma',
  'Pyuria' => 'Pyuria',
  'Recent Weaning' => 'Recent Weaning',
  'Renal' => 'Renal',
  'Reverse sneezing' => 'Reverse sneezing',
  'Ring worm' => 'Ring worm',
  'Rubbing' => 'Rubbing',
  'Sarcoma' => 'Sarcoma',
  'Self Mutilation' => 'Self Mutilation',
  'Seroma' => 'Seroma',
  'Skin only' => 'Skin only',
  'Soft Tissue Injury' => 'Soft Tissue Injury',
  'Staph other' => 'Staph - other',
  'Staph aureus' => 'Staph aureus',
  'Strep' => 'Strep',
  'Strep suis' => 'Strep suis',
  'Study Related' => 'Study Related',
  'Suture Reaction' => 'Suture Reaction',
  'Tender Foot' => 'Tender Foot',
  'Ticks' => 'Ticks',
  'Transfusion' => 'Transfusion',
  'Trauma' => 'Trauma',
  'Trueperella' => 'Trueperella',
  'Upper Respiratory Infection' => 'Upper Respiratory Infection',
  'Urinary obstruction' => 'Urinary obstruction',
  'Urolithasis' => 'Urolithasis',
  'Urticaria' => 'Urticaria',
  'Vaccine' => 'Vaccine',
  'Vaccine site' => 'Vaccine site',
  'Vaginitis' => 'Vaginitis',
  'Vasculitis' => 'Vasculitis',
  'Yucatan' => 'Yucatan',
  'Cyniclomyces guttulatus' => 'Cyniclomyces guttulatus',
  'Normal' => 'Normal',
  'Actinomyces hyovaginalis' => 'Actinomyces hyovaginalis',
  'Adenovirus' => 'Adenovirus',
  'Anticoagulation therapy' => 'Anticoagulation therapy',
  'Arthritis' => 'Arthritis',
  'BehaviorTemperament' => 'Behavior/Temperament',
  'Bordetella bronchiseptica' => 'Bordetella bronchiseptica',
  'Burn' => 'Burn',
  'Clawhoof avulsion' => 'Claw/hoof avulsion',
  'Device related' => 'Device related',
  'Diabetes' => 'Diabetes',
  'Ear Tag infections' => 'Ear Tag infections',
  'Endocarditis' => 'Endocarditis',
  'Erysipelas' => 'Erysipelas',
  'Fusobacterium' => 'Fusobacterium',
  'Gastric reflux' => 'Gastric reflux',
  'GI prep' => 'GI prep',
  'Glaesserella parasuis' => 'Glaesserella parasuis',
  'Hygroma' => 'Hygroma',
  'Incisional hernia' => 'Incisional hernia',
  'Influenza' => 'Influenza',
  'Inguinal hernia' => 'Inguinal hernia',
  'Keratoconjunctivitis Sicca KCS' => 'Keratoconjunctivitis Sicca (KCS)',
  'Laryngitis' => 'Laryngitis',
  'Medication Overdose' => 'Medication Overdose',
  'Microphthalmia' => 'Microphthalmia',
  'Myodegeneration' => 'Myodegeneration',
  'OPPV' => 'OPPV',
  'Pasteurella multocida' => 'Pasteurella multocida',
  'Patellar Luxation' => 'Patellar Luxation',
  'Peritonitis' => 'Peritonitis',
  'Perivascular Administration' => 'Perivascular Administration',
  'Pleural Effusion' => 'Pleural Effusion',
  'Pododermatitis' => 'Pododermatitis',
  'Porcine circovirus PCV' => 'Porcine circovirus (PCV)',
  'Rodent Urological Syndrome' => 'Rodent Urological Syndrome',
  'Saccharomycosis' => 'Saccharomycosis',
  'Septic Arthritis' => 'Septic Arthritis',
  'Shearing Injury' => 'Shearing Injury',
  'T pyogenes' => 'T. pyogenes',
  'Umbilical hernia' => 'Umbilical hernia',
  'Enterococcus gallinarum' => 'Enterococcus gallinarum',
  'Staphylococcus chromogenes' => 'Staphylococcus chromogenes',
  'Aortic valve regurgitation' => 'Aortic valve regurgitation',
  'Atrial Fibrillation' => 'Atrial Fibrillation',
  'BALT hyperplasia' => 'BALT hyperplasia',
  'Central Venous Line Infection' => 'Central Venous Line Infection',
  'Clostridium perfringens' => 'Clostridium perfringens',
  'Coccidiosis' => 'Coccidiosis',
  'Endometritis' => 'Endometritis',
  'GI procedure prep' => 'GI procedure prep',
  'Horners' => 'Horner\'s Syndrome',
  'Ileus' => 'Ileus',
  'Klebsiella pneumoniae' => 'Klebsiella pneumoniae',
  'Mitral valve regurgitation' => 'Mitral valve regurgitation',
  'Musculoskeletal' => 'Musculoskeletal',
  'Normal for PostOperative Recovery' => 'Normal for Post-Operative Recovery',
  'Overgrown Hooves' => 'Overgrown Hooves',
  'Pericarditis' => 'Pericarditis',
  'Pulmonic valve regurgitation' => 'Pulmonic valve regurgitation',
  'Rotavirus' => 'Rotavirus',
  'Toxicity' => 'Toxicity',
  'Feed transition' => 'Feed transition',
  'Mannheimia haemolytica' => 'Mannheimia haemolytica',
  'Sialocele' => 'Sialocele',
);