<?php
 // created: 2017-06-16 19:19:40

$app_list_strings['reminder_time_options']=array (
  -1 => 'Ingen',
  60 => '1 minut før',
  300 => '5 minutter før',
  600 => '10 minutter før',
  900 => '15 minutter før',
  1800 => '30 minutter før',
  3600 => '1 time før',
  7200 => '2 timer før',
  10800 => '3 timer før',
  18000 => '5 timer før',
  86400 => '1 dag før',
  604800 => '7 days prior',
);