<?php
 // created: 2016-03-07 20:24:49

$app_list_strings['Division_list']=array (
  'Blank' => 'Blank',
  'Analytical_Services' => 'Analytical Services',
  'Corporate' => 'Corporate',
  'InLife_Services' => 'In-Life Services',
  'InVitro_Services' => 'In-Vitro Services',
  'Pathology_Services' => 'Pathology_Services',
  'Regulatory_Services' => 'Regulatory Services',
  'Choose Division' => 'Choose Division',
);