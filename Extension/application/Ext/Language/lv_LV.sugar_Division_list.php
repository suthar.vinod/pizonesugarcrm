<?php
 // created: 2016-03-07 20:24:48

$app_list_strings['Division_list']=array (
  'Corporate' => 'Corporate',
  'InLife_Services' => 'In-Life Services',
  'InVitro_Services' => 'In-Vitro Services',
  'Pathology_Services' => 'Pathology_Services',
  'Analytical_Services' => 'Analytical Services',
  'Regulatory_Services' => 'Regulatory Services',
  'Blank' => 'Blank',
  'Choose Division' => 'Choose Division',
);