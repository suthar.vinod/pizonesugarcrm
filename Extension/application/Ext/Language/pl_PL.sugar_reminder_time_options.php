<?php
 // created: 2017-06-16 19:19:42

$app_list_strings['reminder_time_options']=array (
  -1 => 'Brak',
  60 => '1 minutę wcześniej',
  300 => '5 minut wcześniej',
  600 => '10 minut wcześniej',
  900 => '15 minut wcześniej',
  1800 => '30 minut wcześniej',
  3600 => '1 godzinę wcześniej',
  7200 => '2 godziny wcześniej',
  10800 => '3 godziny wcześniej',
  18000 => '5 godzin wcześniej',
  86400 => '1 dzień wcześniej',
  604800 => '7 days prior',
);