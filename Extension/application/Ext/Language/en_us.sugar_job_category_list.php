<?php
 // created: 2017-07-20 16:44:55

$app_list_strings['job_category_list']=array (
  '' => '',
  'Analytical' => 'Analytical',
  'Animal Care' => 'Animal Care',
  'Business Development' => 'Business Development',
  'Histology' => 'Histology',
  'IVT' => 'IVT',
  'Other' => 'Other',
  'Pathologist' => 'Pathologist',
  'Pharmacology' => 'Pharmacology',
  'Quality Assurance' => 'Quality Assurance',
  'Regulatory Services' => 'Regulatory Services',
  'Research Tech_SA' => 'Research Tech - SA',
  'Research Tech_LA' => 'Research Tech - LA',
  'Scientist' => 'Scientist',
  'Surgical Tech' => 'Surgical Tech',
  'Toxicology' => 'Toxicology',
  'Veterinarian' => 'Veterinarian',
);