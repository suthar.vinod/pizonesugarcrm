<?php
 // created: 2022-02-24 13:54:36

$app_list_strings['specimen_type_list']=array (
  '' => '',
  'Balloons' => 'Balloons',
  'Culture' => 'Culture',
  'EDTA Plasma' => 'EDTA Plasma',
  'Fecal' => 'Fecal',
  'NaCit Plasma' => 'NaCit Plasma',
  'Na Heparin Plasma' => 'Na Heparin Plasma',
  'Other' => 'Other',
  'Serum' => 'Serum',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
);