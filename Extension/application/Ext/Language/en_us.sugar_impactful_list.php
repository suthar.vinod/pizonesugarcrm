<?php
 // created: 2020-10-15 11:02:53

$app_list_strings['impactful_list']=array (
  '' => '',
  'This Deviation was impactful' => 'This Deviation was impactful.',
  'This Deviation was NOT impactful' => 'This Deviation was NOT impactful.',
  'Pending needs further assessment before finalizing' => 'Pending, needs further assessment before finalizing.',
  'This Adverse Event was impactful' => 'This Adverse Event was impactful.',
  'This Adverse Event was NOT impactful' => 'This Adverse Event was NOT impactful.',
);