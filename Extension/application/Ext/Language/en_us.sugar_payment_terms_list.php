<?php
 // created: 2022-07-28 06:41:23

$app_list_strings['payment_terms_list']=array (
  '' => '',
  'check_with_finance' => 'Check with Finance',
  'aps account' => 'APS Account - Net 30',
  'Net 30' => 'Net 30',
  'Net 45' => 'Net 45',
  'Net 60' => 'Net 60',
  'Net 77' => 'Net 77',
  'Net 90' => 'Net 90',
  'new client' => 'New Client - per bid',
  'Per MSA' => 'Per MSA',
  'pre_pay' => 'Pre-pay',
);