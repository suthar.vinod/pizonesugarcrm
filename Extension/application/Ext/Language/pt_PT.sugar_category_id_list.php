<?php
 // created: 2019-02-20 13:34:00

$app_list_strings['category_id_list']=array (
  '' => '',
  'MSA' => 'MSA',
  'MSA Amendment' => 'MSA Amendment',
  'NDA_CDA' => 'NDA / CDA',
  'Quality Agreement' => 'Quality Agreement',
  'Material Transfer Agreement' => 'Material Transfer Agreement',
  'Qualification' => 'Qualification',
  'Supplemental' => 'Supplemental',
);