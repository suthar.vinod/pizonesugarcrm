<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'טלמרקטינג',
  'Existing Customer' => 'לקוח קיים',
  'Self Generated' => 'Self Generated',
  'Employee' => 'עובד',
  'Conference' => 'ועידה',
  'Trade Show' => 'תערוכה',
  'Web Site' => 'אתר אינטרנט',
  'Email' => 'Any Email',
  'Campaign' => 'קמפיין',
  'Support Portal User Registration' => 'תמיכה בהרשמת משתמש לפורטל',
  'Other' => 'Other',
  'Referral' => 'Referral',
);