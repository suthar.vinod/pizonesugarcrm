<?php
 // created: 2020-02-06 13:30:24

$app_list_strings['hazardous_contents_list']=array (
  '' => '',
  'Ethanol ETOH' => 'Ethanol (ETOH)',
  'Glutaraldehyde' => 'Glutaraldehyde',
  'Hexane' => 'Hexane',
  'Methanol' => 'Methanol',
  'NA' => 'NA',
  'Other' => 'Other',
  'Biological Substance Category B' => 'Biological Substance – Category B',
);