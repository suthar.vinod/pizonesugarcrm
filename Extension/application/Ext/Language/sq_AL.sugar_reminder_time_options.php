<?php
 // created: 2017-06-16 19:19:43

$app_list_strings['reminder_time_options']=array (
  -1 => 'Asnjëra',
  60 => '1 minutë përpara',
  300 => '5 minuta përpara',
  600 => '10 minuta përpara',
  900 => '15 minuta përpara',
  1800 => '30 minuta përpara',
  3600 => '1 orë përpara',
  7200 => '2 orë përpara',
  10800 => '3 orë përpara',
  18000 => '5 orë përpara',
  86400 => '1 ditë përpara',
  604800 => '7 days prior',
);