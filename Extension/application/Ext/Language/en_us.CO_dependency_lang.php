<?php

$app_list_strings["rt_taken_list"] = array(
    'Taken' => 'Taken'
);

$app_list_strings["rt_taken_both_list"] = array(
    '' => '',
    'Taken' => 'Taken',
    'Not Taken' => 'Not Taken'
);

$app_list_strings["rt_not_taken_list"] = array(
    'Not Taken' => 'Not Taken'
);
