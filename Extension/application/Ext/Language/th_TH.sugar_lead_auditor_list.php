<?php
 // created: 2018-11-20 15:07:49

$app_list_strings['lead_auditor_list']=array (
  'External Contractor' => 'External Contractor',
  'BC Auditor Group' => 'BC Auditor Group',
  'Brenda Bailey' => 'Brenda Bailey',
  'Emily Markuson' => 'Emily Markuson',
  'Kevin Catalano' => 'Kevin Catalano',
  'Kristen Varas' => 'Kristen Varas',
  'Nicole Klee' => 'Nicole Klee',
  'Tammy Fossum' => 'Tammy Fossum',
  'None' => 'None',
  'Choose Auditor' => 'Choose Auditor...',
  'Erica VanReeth' => 'Erica VanReeth',
  'Dani Zehowski' => 'Dani Zehowski',
  'Karen Bastyr' => 'Karen Bastyr',
  'Nick McCune' => 'Nick McCune',
  'Katie Jenkins' => 'Katie Jenkins',
  'Kris Ruppelius' => 'Kris Ruppelius',
  'Stephanie Beane' => 'Stephanie Beane',
  'Amy Malloy' => 'Amy Malloy',
);