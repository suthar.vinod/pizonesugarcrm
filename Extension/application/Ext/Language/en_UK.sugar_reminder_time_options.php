<?php
 // created: 2017-06-16 19:19:42

$app_list_strings['reminder_time_options']=array (
  -1 => 'None',
  60 => '1 minute prior',
  300 => '5 minutes prior',
  600 => '10 minutes prior',
  900 => '15 minutes prior',
  1800 => '30 minutes prior',
  3600 => '1 hour prior',
  7200 => '2 hours prior',
  10800 => '3 hours prior',
  18000 => '5 hours prior',
  86400 => '1 day prior',
  604800 => '7 days prior',
);