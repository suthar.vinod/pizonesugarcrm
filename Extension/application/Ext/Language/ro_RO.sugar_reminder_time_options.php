<?php
 // created: 2017-06-16 19:19:42

$app_list_strings['reminder_time_options']=array (
  -1 => 'Niciunul',
  60 => 'Cu 1 minut înainte',
  300 => 'Cu 5 minute înainte',
  600 => 'Cu 10 minute înainte',
  900 => 'Cu 15 minute înainte',
  1800 => 'Cu 30 minute înainte',
  3600 => 'Cu 1 oră înainte',
  7200 => 'Cu 2 ore înainte',
  10800 => 'Cu 3 ore înainte',
  18000 => 'Cu 5 ore înainte',
  86400 => 'Cu 1 zi înainte',
  604800 => '7 days prior',
);