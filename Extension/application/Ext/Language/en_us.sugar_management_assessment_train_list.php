<?php
 // created: 2020-10-16 05:38:21

$app_list_strings['management_assessment_train_list']=array (
  '' => '',
  'Major Change or New' => 'Employee performed activities under this Controlled Document while overdue; Controlled Document Revision = Major Change or New',
  'Revision Change or Minor Change' => 'Employee performed activities under this Controlled Document while overdue; Controlled Document Revision = Revision Change Only or Minor Change',
  'Employee did NOT perform SOP tasks while overdue' => 'Employee did NOT undertake activities under this Controlled Document while overdue',
  'The Controlled Document is a standalone document No Controlled Document activities are associated' => 'The Controlled Document is a standalone document. No Controlled Document activities are associated with the document.',
  'Overdue refresher training' => 'Employee performed activities under this Controlled Document when their Refresher Training was overdue',
);