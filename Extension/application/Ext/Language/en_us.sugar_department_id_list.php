<?php
 // created: 2019-04-24 11:27:38

$app_list_strings['department_id_list']=array (
  '' => '',
  'AS' => 'AS',
  'DVM' => 'DVM',
  'Facility' => 'Facility',
  'HS' => 'HS',
  'ISR' => 'ISR',
  'IVT' => 'IVT',
  'LA AC' => 'LA AC',
  'LA OP' => 'LA OP',
  'PH OP' => 'PH OP',
  'PS' => 'PS',
  'SA AC' => 'SA AC',
  'SA OP' => 'SA OP',
  'SCI' => 'SCI',
  'SP' => 'SP',
);