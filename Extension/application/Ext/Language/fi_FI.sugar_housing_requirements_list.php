<?php
 // created: 2021-12-07 09:55:32

$app_list_strings['housing_requirements_list']=array (
  '' => '',
  'Animals require their own room' => 'Animals require their own room',
  'Electronics present in room' => 'Electronics present in room',
  'Additional space in room required' => 'Additional space in room required',
  'Other' => 'Other',
);