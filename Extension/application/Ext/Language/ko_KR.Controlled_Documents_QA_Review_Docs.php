<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.Controlled_Documents_QA_Review_Docs.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.Controlled_Documents_QA_Review_Docs.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = '마케팅';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = '지식 기반';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = '영업';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = '홍보 자료';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = '브로슈어';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = '자주묻는질문';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = '작동중';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = '임시 보관';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = '자주묻는질문';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = '기간 만료됨';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = '검토중';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = '보류중';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = '마케팅';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = '지식 기반';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = '영업';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = '홍보 자료';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = '브로슈어';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = '자주묻는질문';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = '작동중';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = '임시 보관';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = '자주묻는질문';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = '기간 만료됨';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = '검토중';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = '보류중';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = '마케팅';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = '지식 기반';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = '영업';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = '홍보 자료';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = '브로슈어';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = '자주묻는질문';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = '작동중';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = '임시 보관';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = '자주묻는질문';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = '기간 만료됨';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = '검토중';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = '보류중';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';

?>
