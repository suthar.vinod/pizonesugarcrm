<?php
 // created: 2022-02-22 07:36:28

$app_list_strings['deliverable_status_list']=array (
  '' => '',
  'Pending method development' => 'Pending method development',
  'In Progress' => 'In Progress',
  'Waiting on Sponsor' => 'Out to Sponsor, Waiting on Redlines',
  'Waiting_on_Subcontracted_Report' => 'Waiting on Subcontracted Report',
  'Waiting On Sponsor Ready to Audit' => 'Out to Sponsor, Ready to Audit',
  'Completed' => 'Completed',
  'Overdue' => 'Overdue',
  'Sent to Study DirectorPrinciple Investigator' => 'Sent to Study Director/Principle Investigator',
  'Sponsor Retracted' => 'Sponsor Retracted',
  'None' => 'None',
  'Not Performed' => 'Not Performed',
  'Completed Account on Hold' => 'Completed, Account on Hold',
  'Returned to Sponsor' => 'Returned to Sponsor',
  'Sent to Third Party' => 'Sent to Third Party',
  'Discarded' => 'Discarded',
  'Out to Study Director Waiting on Redlines' => 'Out to Study Director, Waiting on Redlines',
  'In Data Review' => 'In Data Review',
  'Ready for Pathologist' => 'Ready for Pathologist',
  'Ready for Study Director' => 'Ready for Study Director',
);