<?php
 // created: 2019-12-06 12:55:01

$app_list_strings['corrective_action_options_list']=array (
  '' => '',
  'SOP Update' => 'APS Controlled Document Revision',
  'Further Investigation of Event' => 'Further Investigation of Event',
  'None' => 'None',
  'Other' => 'Other',
  'Protocol Amendment' => 'Protocol Amendment',
  'Protocol Requirement Review' => 'Protocol Requirement Review',
  'Report Amendment' => 'Report Amendment',
  'Protocol Specific Training' => 'Task Retraining',
);