<?php
 // created: 2022-04-26 07:17:36

$app_list_strings['biod_dose_route_list']=array (
  '' => '',
  'IV' => 'IV',
  'IP' => 'IP',
  'IV IP' => 'IV & IP',
  'Other' => 'Other',
);