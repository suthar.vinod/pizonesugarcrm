<?php
 // created: 2016-03-28 21:13:48

$app_list_strings['sales_activity_status_list']=array (
  'open' => 'Open',
  'select' => 'Select...',
  'closed' => 'Closed',
  'won' => 'Won',
  'duplicate' => 'Duplicate',
);