<?php
 // created: 2022-10-27 05:52:42

$app_list_strings['equipment_required_list']=array (
  '12 lead' => '12-lead',
  'Bard' => 'Bard',
  'CT' => 'CT',
  'Fluroscopy' => 'Fluroscopy',
  'PowerLab' => 'PowerLab',
  'Ultrasound APS' => 'Ultrasound (APS)',
  'Ultrasound Jim B' => 'Ultrasound (Jim B.)',
  '' => '',
  'Angio' => 'Angio',
  'Any special Gas Requirements' => 'Any special Gas Requirements',
  'Automated Nociception Analyzer' => 'Automated Nociception Analyzer',
  'Bypass' => 'Bypass',
  'C Arm' => 'C-Arm (if in an OR)',
  'Cardiac Output' => 'Cardiac Output',
  'Digital Randall Selitto Device dRS' => 'Digital Randall-Selitto Device (dRS)',
  'Electronic Von Frey eVF' => 'Electronic Von Frey (eVF)',
  'Endoscopic Tower' => 'Endoscopic Tower',
  'Flow Probe' => 'Flow Probe',
  'Hargreaves Hg' => 'Hargreaves (Hg)',
  'Hot Cold Plate' => 'Hot-Cold Plate',
  'IABP' => 'IABP',
  'IVUS' => 'IVUS',
  'OCT' => 'OCT',
  'Other' => 'Other',
  'Pacemaker handheld' => 'Pacemaker - handheld',
  'Pacemaker Micropacer' => 'Pacemaker - Micropacer',
  'Rotarod' => 'Rotarod',
  'UVB' => 'UVB',
  'Velocity' => 'Velocity',
  'Von Frey vF' => 'Von Frey (vF)',
);