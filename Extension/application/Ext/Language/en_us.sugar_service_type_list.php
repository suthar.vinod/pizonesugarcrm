<?php
 // created: 2021-01-28 15:02:15

$app_list_strings['service_type_list']=array (
  '' => '',
  'Analytical' => 'Analytical',
  'Bioskills' => 'Bioskills',
  'Clinical Pathology' => 'Clinical Pathology',
  'Histology' => 'Histology',
  'In Life Service' => 'In Life Service',
  'ISR' => 'ISR',
  'IVT' => 'IVT',
  'LAIL' => 'LAIL',
  'Operations' => 'Operations',
  'Pathology' => 'Pathology',
  'Phamacology' => 'Phamacology',
  'QA' => 'QA',
  'SAIL' => 'SAIL',
  'Sample Prep' => 'Sample Prep',
  'Scientific' => 'Scientific',
  'Standard Biocompatibility' => 'Standard Biocompatibility',
  'Toxicology' => 'Toxicology',
);