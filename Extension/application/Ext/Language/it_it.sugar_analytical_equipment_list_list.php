<?php
 // created: 2017-06-16 21:18:05

$app_list_strings['analytical_equipment_list_list']=array (
  '' => '',
  'ISQ LT' => 'GCMS – ISQ LT',
  'TSQ Quantiva 1' => 'LCMS – TSQ Quantiva 1',
  'TSQ Quantiva 2' => 'LCMS – TSQ Quantiva 2',
  'QE Focus' => 'LCMS – QE Focus',
  'Nicolet iS10' => 'FTIR – Nicolet iS10',
  'ASE350' => 'ASE – ASE350',
  'Agilent 7500 CS' => 'ICPMS – Agilent 7500 CS',
  'Precellys Evolution' => 'Homogenizer – Precellys Evolution',
  'Cryolys' => 'Homogenizer Chiller – Cryolys',
);