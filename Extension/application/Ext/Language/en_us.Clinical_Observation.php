<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['CO_Clinical_Observation'] = 'Clinical Observations';
$app_list_strings['moduleListSingular']['CO_Clinical_Observation'] = 'Clinical Observation';
$app_list_strings['yes_no_list']['No'] = 'No';
$app_list_strings['yes_no_list']['Yes'] = 'Yes';
$app_list_strings['yes_no_list'][''] = '';
$app_list_strings['observation_list']['Normal'] = 'Normal';
$app_list_strings['observation_list']['See Observation Text'] = 'See Observation Text';
$app_list_strings['observation_list'][''] = '';
$app_list_strings['housing_list']['Group'] = 'Group';
$app_list_strings['housing_list']['Single'] = 'Single';
$app_list_strings['housing_list'][''] = '';
$app_list_strings['Protocol_Ad_Hoc_Type_list']['Ad Hoc'] = 'Ad Hoc';
$app_list_strings['Protocol_Ad_Hoc_Type_list']['Protocol'] = 'Protocol';
$app_list_strings['Protocol_Ad_Hoc_Type_list'][''] = '';
$app_list_strings['taken_not_taken_list']['Not Taken'] = 'Not Taken';
$app_list_strings['taken_not_taken_list']['Taken'] = 'Taken';
$app_list_strings['taken_not_taken_list'][''] = '';
$app_list_strings['yes_list']['Yes'] = 'Yes';
$app_list_strings['yes_list'][''] = '';
$app_list_strings['clin_obs_type_list']['Ad Hoc'] = 'Ad Hoc';
$app_list_strings['clin_obs_type_list']['Morbidity and Mortality'] = 'Morbidity and Mortality';
$app_list_strings['clin_obs_type_list']['Pain Assessment'] = 'Pain Assessment';
$app_list_strings['clin_obs_type_list']['Post Operative AM'] = 'Post Operative Day AM';
$app_list_strings['clin_obs_type_list']['Post Operative Day 1 AM'] = 'Post Operative Day 1 AM';
$app_list_strings['clin_obs_type_list']['Post Operative PM'] = 'Post Operative PM';
$app_list_strings['clin_obs_type_list']['Standard'] = 'Standard';
$app_list_strings['clin_obs_type_list'][''] = '';
$app_list_strings['observation_selection_list']['Cough'] = 'Cough';
$app_list_strings['observation_selection_list']['Diarrhea'] = 'Diarrhea';
$app_list_strings['observation_selection_list']['Inappetence'] = 'Inappetence';
$app_list_strings['observation_selection_list']['Lameness'] = 'Lameness';
$app_list_strings['observation_selection_list']['LesionWound'] = 'Lesion/Wound';
$app_list_strings['observation_selection_list']['Mentation'] = 'Mentation';
$app_list_strings['observation_selection_list']['Nasal Discharge'] = 'Nasal Discharge';
$app_list_strings['observation_selection_list'][''] = '';
$app_list_strings['post_operative_pain_level_list'][1] = '1';
$app_list_strings['post_operative_pain_level_list'][2] = '2';
$app_list_strings['post_operative_pain_level_list'][3] = '3';
$app_list_strings['post_operative_pain_level_list'][''] = '';
$app_list_strings['post_operative_pain_level_list']['zero'] = '0';
$app_list_strings['pain_level_observation_list']['Droopy Ears'] = 'Droopy Ears';
$app_list_strings['pain_level_observation_list']['Hunched'] = 'Hunched';
$app_list_strings['pain_level_observation_list']['Inappetence'] = 'Inappetence';
$app_list_strings['pain_level_observation_list']['Mentation'] = 'Mentation';
$app_list_strings['pain_level_observation_list']['Teeth Grinding'] = 'Teeth Grinding';
$app_list_strings['pain_level_observation_list']['Vocalization'] = 'Vocalization';
$app_list_strings['pain_level_observation_list'][''] = '';
$app_list_strings['co_appetite_list']['Anorexic'] = 'Anorexic (all feed remaining)';
$app_list_strings['co_appetite_list']['Mild Inappetent'] = 'Mild Inappetent (~1/4-1/2 feed remaining)';
$app_list_strings['co_appetite_list']['Moderate Inappetent'] = 'Moderate Inappetent (~1/2-3/4 feed remaining)';
$app_list_strings['co_appetite_list']['Normal'] = 'Normal';
$app_list_strings['co_appetite_list']['Not Assessed'] = 'Not Assessed (Group Housed)';
$app_list_strings['co_appetite_list'][''] = '';
$app_list_strings['fecal_assessment_list']['Absent'] = 'Absent';
$app_list_strings['fecal_assessment_list']['Diarrhea'] = 'Diarrhea';
$app_list_strings['fecal_assessment_list']['Normal'] = 'Normal';
$app_list_strings['fecal_assessment_list']['Not Assessed'] = 'Not Assessed (Group Housed)';
$app_list_strings['fecal_assessment_list']['Scant'] = 'Scant';
$app_list_strings['fecal_assessment_list']['Soft'] = 'Soft';
$app_list_strings['fecal_assessment_list'][''] = '';
$app_list_strings['mentation_list']['BAR'] = 'BAR';
$app_list_strings['mentation_list']['LethargicDepressed'] = 'Lethargic/Depressed';
$app_list_strings['mentation_list']['QAR'] = 'QAR';
$app_list_strings['mentation_list']['Unresponsive'] = 'Unresponsive';
$app_list_strings['mentation_list'][''] = '';
$app_list_strings['normal_abnormal_list']['Normal'] = 'Normal';
$app_list_strings['normal_abnormal_list']['Abnormal'] = 'Abnormal';
$app_list_strings['normal_abnormal_list'][''] = '';
$app_list_strings['lameness_location_list']['LH'] = 'LH';
$app_list_strings['lameness_location_list']['LF'] = 'LF';
$app_list_strings['lameness_location_list']['RH'] = 'RH';
$app_list_strings['lameness_location_list']['RF'] = 'RF';
$app_list_strings['lameness_location_list']['Bilateral hindlimb'] = 'Bilateral hindlimb';
$app_list_strings['lameness_location_list']['Bilateral forelimb'] = 'Bilateral forelimb';
$app_list_strings['lameness_location_list'][''] = '';
$app_list_strings['co_lameness_severity_list']['Mild'] = 'Mild (toe tapping)';
$app_list_strings['co_lameness_severity_list']['Moderate'] = 'Moderate (limping/raised at standing)';
$app_list_strings['co_lameness_severity_list']['Severe'] = 'Severe (non weight bearing)';
$app_list_strings['co_lameness_severity_list'][''] = '';
