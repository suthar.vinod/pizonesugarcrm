<?php
 // created: 2019-02-20 13:42:59

$app_list_strings['vendor_status_list']=array (
  '' => '',
  'Approved' => 'Approved',
  'Disapproved' => 'Disapproved',
  'Pending Approval' => 'Pending Approval',
  'Retired' => 'Retired',
);