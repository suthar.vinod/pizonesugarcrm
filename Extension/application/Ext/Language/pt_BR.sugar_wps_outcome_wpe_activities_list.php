<?php
 // created: 2020-12-15 06:26:30

$app_list_strings['wps_outcome_wpe_activities_list']=array (
  '' => '',
  'Invasive Procedure' => 'Invasive Procedure',
  'Study Article Exposure' => 'Study Article Exposure',
  'Study Specific Data Recorded' => 'Study Specific Data Recorded',
  'Other Activities' => 'Other Activities',
);