<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Soitto',
  'Existing Customer' => 'Olemassa oleva asiakas',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Työntekijä',
  'Conference' => 'Konferenssi',
  'Trade Show' => 'Messut',
  'Web Site' => 'Internet-sivusto',
  'Email' => 'Mikä tahansa sähköposti',
  'Campaign' => 'Kampanja',
  'Support Portal User Registration' => 'Tukiportaalin käyttäjärekisteröinti',
  'Other' => 'Muu',
  'Referral' => 'Referral',
);