<?php
 // created: 2022-02-03 09:59:19

$app_list_strings['task_type_list']=array (
  'Lead Follow Up' => 'Lead Follow Up',
  'Document Creation' => 'Document Creation',
  'Document Review' => 'Document Review',
  'Quote Follow Up' => 'Quote Follow Up',
  'Sponsor Follow Up' => 'Sponsor Follow Up',
  'Financial' => 'Financial',
  'Work Product' => 'Work Product',
  'Procedure Scheduling' => 'Procedure Scheduling',
  'Quote Review' => 'Quote Review',
  'Sponsor Communication' => 'Sponsor Communication',
  'Sponsor Quote Review' => 'Sponsor Quote Review',
  'Histopathology' => 'Histopathology',
  'Invoice' => 'Invoice',
  'Maintenance Request' => 'Maintenance Request',
  'Vet Check' => 'Vet Check',
  'Send Necropsy Findings' => 'Send Necropsy Findings',
  'Tradeshow' => 'Tradeshow',
  '' => '',
  'Schedule Matrix Review' => 'Schedule Matrix Review',
  'Custom' => 'Custom',
  'Standard' => 'Custom',
);