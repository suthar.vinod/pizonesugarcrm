<?php
 // created: 2020-08-28 18:21:09

$app_list_strings['purchase_unit_list']=array (
  '' => '',
  'Each' => 'Each',
  'Bottle' => 'Bottle',
  'Box' => 'Box',
  'Case' => 'Case',
  'Pallet' => 'Pallet',
  'Roll' => 'Roll',
  'Sleeve' => 'Sleeve',
);