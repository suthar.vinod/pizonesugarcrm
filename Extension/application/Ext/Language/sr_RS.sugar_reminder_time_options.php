<?php
 // created: 2017-06-16 19:19:42

$app_list_strings['reminder_time_options']=array (
  -1 => 'Nijedna',
  60 => 'Pre jednog minuta',
  300 => 'Pre pet minuta',
  600 => 'Pre 10 minuta',
  900 => 'Pre 15 minuta',
  1800 => 'Pre 30 minuta',
  3600 => 'Pre jednog časa',
  7200 => 'Pre dva časa',
  10800 => 'Pre tri časa',
  18000 => 'Pre pet časova',
  86400 => 'Pre jednog dana',
  604800 => '7 days prior',
);