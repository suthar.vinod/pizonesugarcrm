<?php
 // created: 2016-08-19 22:00:14

$app_list_strings['animal_model_list']=array (
  'Choose one...' => 'Choose one...',
  'naive_porcine' => 'Naive Porcine',
  'naive_canine' => 'Naive Canine',
  'naive_ovine' => 'Naive Ovine',
  'naive_rabbit' => 'Naive Lagamorph',
  'myocaridal_infarction' => 'Myocardial Infarction',
  'rabbit_elastase_aneurysm' => 'Rabbit Elastase Aneurysm ',
  'canine_aneurysm' => 'Canine Aneurysm',
);