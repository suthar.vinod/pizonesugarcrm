<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['et_email_template_category_dom']['Marketing'] = '市场营销';
$app_list_strings['et_email_template_category_dom']['Knowledege Base'] = '知识库';
$app_list_strings['et_email_template_category_dom']['Sales'] = '销售';
$app_list_strings['et_email_template_category_dom'][''] = '';
$app_list_strings['et_email_template_subcategory_dom']['Marketing Collateral'] = '营销资料';
$app_list_strings['et_email_template_subcategory_dom']['Product Brochures'] = '产品手册';
$app_list_strings['et_email_template_subcategory_dom']['FAQ'] = '常见问题';
$app_list_strings['et_email_template_subcategory_dom'][''] = '';
$app_list_strings['et_email_template_status_dom']['Active'] = '启用';
$app_list_strings['et_email_template_status_dom']['Draft'] = '草稿';
$app_list_strings['et_email_template_status_dom']['FAQ'] = '常见问题';
$app_list_strings['et_email_template_status_dom']['Expired'] = '失效';
$app_list_strings['et_email_template_status_dom']['Under Review'] = '审查中';
$app_list_strings['et_email_template_status_dom']['Pending'] = '未决定';
$app_list_strings['moduleList']['ET_Email_Template'] = 'Email Templates';
$app_list_strings['moduleListSingular']['ET_Email_Template'] = 'Email Template';
$app_list_strings['department_list']['Analytical Services'] = 'Analytical';
$app_list_strings['department_list']['Business Development'] = 'Business Development';
$app_list_strings['department_list']['Facilities'] = 'Facilities';
$app_list_strings['department_list']['Finance'] = 'Finance';
$app_list_strings['department_list']['Histology Services'] = 'Histology Services';
$app_list_strings['department_list']['Human_Resources'] = 'Human Resources';
$app_list_strings['department_list']['In life Large Animal Care'] = 'In-life Large Animal Care';
$app_list_strings['department_list']['In life Large Animal Research'] = 'In-life Large Animal Research';
$app_list_strings['department_list']['In life Small Animal Care'] = 'In-life Small Animal Care';
$app_list_strings['department_list']['In life Small Animal Research'] = 'In-life Small Animal Research';
$app_list_strings['department_list']['Information_Technology'] = 'Information Technology';
$app_list_strings['department_list']['Interventional Surgical Research'] = 'Interventional Surgical Research';
$app_list_strings['department_list']['Lab Services'] = 'Lab Services';
$app_list_strings['department_list']['Operations Support'] = 'Operations Support';
$app_list_strings['department_list']['Pathology Services'] = 'Pathology Services';
$app_list_strings['department_list']['Pharmacology'] = 'Pharmacology Services';
$app_list_strings['department_list']['Process Improvement'] = 'Process Improvement';
$app_list_strings['department_list']['Quality Assurance Unit'] = 'Quality Assurance Unit';
$app_list_strings['department_list']['Regulatory Services'] = 'Regulatory Services';
$app_list_strings['department_list']['Scientific'] = 'Scientific';
$app_list_strings['department_list']['Software Development'] = 'Software Development';
$app_list_strings['department_list']['Toxicology'] = 'Toxicology Services';
$app_list_strings['department_list']['Veterinary Services'] = 'Veterinary Services';
$app_list_strings['department_list'][''] = '';
