<?php
 // created: 2021-12-21 08:32:40

$app_list_strings['inspection_results_list']=array (
  '' => '',
  'No Findings' => 'No Findings',
  'Findings' => 'Findings',
  'Observations' => 'Observations (Obsolete)',
  'Findings_Observations' => 'Findings & Observations (Obsolete)',
);