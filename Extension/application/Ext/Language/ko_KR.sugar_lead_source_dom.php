<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => '방문판매',
  'Existing Customer' => '기존고객',
  'Self Generated' => 'Self Generated',
  'Employee' => '직원',
  'Conference' => '컨퍼런스',
  'Trade Show' => '표시회',
  'Web Site' => '웹사이트',
  'Email' => '다른 이메일:',
  'Campaign' => '캠페인',
  'Support Portal User Registration' => '포탈 사용자 등록',
  'Other' => '기타',
  'Referral' => 'Referral',
);