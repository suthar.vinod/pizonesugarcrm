<?php
 // created: 2022-02-03 09:59:20

$app_list_strings['task_type_list']=array (
  'Lead Follow Up' => 'Lead Follow Up',
  'Document Creation' => 'Document Creation',
  'Document Review' => 'Document Review',
  'Quote Review' => 'Quote Review',
  'Sponsor Communication' => 'Sponsor Communication',
  'Sponsor Quote Review' => 'Sponsor Quote Review',
  'Quote Follow Up' => 'Quote Follow Up',
  'Sponsor Follow Up' => 'Sponsor Follow Up',
  'Invoice' => 'Invoice',
  'Histopathology' => 'Histopathology',
  'Financial' => 'Financial',
  'Work Product' => 'Work Product',
  'Procedure Scheduling' => 'Procedure Scheduling',
  'Maintenance Request' => 'Maintenance Request',
  'Vet Check' => 'Vet Check',
  'Send Necropsy Findings' => 'Send Necropsy Findings',
  'Tradeshow' => 'Tradeshow',
  '' => '',
  'Schedule Matrix Review' => 'Schedule Matrix Review',
  'Custom' => 'Custom',
  'Standard' => 'Custom',
);