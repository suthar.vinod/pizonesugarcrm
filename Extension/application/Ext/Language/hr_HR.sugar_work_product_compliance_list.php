<?php
 // created: 2019-04-15 12:44:55

$app_list_strings['work_product_compliance_list']=array (
  'Not Applicable' => 'Not Applicable',
  'nonGLP' => 'nonGLP',
  'GLP' => 'GLP',
  'Withdrawn' => 'Withdrawn',
  'GLP_Amended_NonGLP' => 'GLP, Amended nonGLP',
  'Choose Compliance' => 'Choose Compliance',
  'GLP Not Performed' => 'GLP, Not Performed',
  'GLP Discontinued' => 'GLP, Discontinued',
  '' => '',
);