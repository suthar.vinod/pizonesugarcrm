<?php
 // created: 2017-06-16 19:19:42

$app_list_strings['reminder_time_options']=array (
  -1 => 'Nenhum',
  60 => '1 minuto de antecedência',
  300 => '5 minutos de antecedência',
  600 => '10 minutos de antecedência',
  900 => '15 minutos de antecedência',
  1800 => '30 minutos de antecedência',
  3600 => '1 hora de antecedência',
  7200 => '2 horas de antecedência',
  10800 => '3 horas de antecedência',
  18000 => '5 horas de antecedência',
  86400 => '1 dia de antecedência',
  604800 => '7 days prior',
);