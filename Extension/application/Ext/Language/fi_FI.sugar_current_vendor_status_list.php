<?php
 // created: 2019-02-20 18:15:36

$app_list_strings['current_vendor_status_list']=array (
  '' => '',
  'Approved' => 'Approved',
  'Disapproved' => 'Disapproved',
  'Pending Approval' => 'Pending Approval',
  'Retired' => 'Retired',
);