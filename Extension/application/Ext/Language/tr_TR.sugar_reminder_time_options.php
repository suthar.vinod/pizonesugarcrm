<?php
 // created: 2017-06-16 19:19:42

$app_list_strings['reminder_time_options']=array (
  -1 => 'Yok',
  60 => '1 dakika önce',
  300 => '5 dakika önce',
  600 => '10 dakika önce',
  900 => '15 dakika önce',
  1800 => '30 dakika önce',
  3600 => '1 saat önce',
  7200 => '2 saat önce',
  10800 => '3 saat önce',
  18000 => '5 saat önce',
  86400 => '1 gün önce',
  604800 => '7 days prior',
);