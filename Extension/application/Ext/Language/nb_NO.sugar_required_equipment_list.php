<?php
 // created: 2018-06-26 19:01:12

$app_list_strings['required_equipment_list']=array (
  '' => '',
  'IVUS' => 'IVUS',
  'OCT' => 'OCT',
  'PowerLab' => 'PowerLab',
  'Endoscopy_Tower' => 'Endoscopy Tower',
  'Portable_CARM' => 'Portable C-Arm',
  'Ultrasound' => 'Ultrasound',
  'Velocity' => 'Velocity',
  'Bard' => 'Bard',
  'RF Generator' => 'RF Generator',
  'EP Combination' => 'EP Combination',
  'Cardiac CT' => 'Cardiac CT',
  'Other CT' => 'Other CT',
  'Hourly Holding' => 'Hourly Holding',
);