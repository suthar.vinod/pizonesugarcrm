<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['IM_Inventory_Management'] = 'Inventory Management';
$app_list_strings['moduleListSingular']['IM_Inventory_Management'] = 'Inventory Management';
$app_list_strings['related_to_list']['Inventory Collection'] = 'Inventory Collection';
$app_list_strings['related_to_list']['Inventory Item'] = 'Inventory Item';
$app_list_strings['related_to_list'][''] = '';
$app_list_strings['inventory_management_type_list']['Archive Offsite'] = 'Archive Offsite';
$app_list_strings['inventory_management_type_list']['Archive Onsite'] = 'Archive Onsite';
$app_list_strings['inventory_management_type_list']['Create Inventory Collection'] = 'Create Inventory Collection';
$app_list_strings['inventory_management_type_list']['Discard'] = 'Discard';
$app_list_strings['inventory_management_type_list']['External Transfer'] = 'External Transfer';
$app_list_strings['inventory_management_type_list']['Internal Transfer'] = 'Internal Transfer';
$app_list_strings['inventory_management_type_list']['Obsolete'] = 'Obsolete';
$app_list_strings['inventory_management_type_list']['Processing'] = 'Processing';
$app_list_strings['inventory_management_type_list']['Separate Inventory Items'] = 'Separate Inventory Collection Item(s)';
$app_list_strings['inventory_management_type_list'][''] = '';
$app_list_strings['im_condition_list']['Ambient Temperature'] = 'Ambient Temperature';
$app_list_strings['im_condition_list']['Room Temperature'] = 'Room Temperature';
$app_list_strings['im_condition_list']['Refrigerated'] = 'Refrigerated';
$app_list_strings['im_condition_list']['On Ice'] = 'On Ice';
$app_list_strings['im_condition_list']['Frozen on Dry Ice'] = 'Frozen on Dry Ice';
$app_list_strings['im_condition_list']['Frozen in Dry IceAcetone'] = 'Frozen in Dry Ice/Acetone';
$app_list_strings['im_condition_list']['Frozen in Liquid Nitrogen'] = 'Frozen in Liquid Nitrogen';
$app_list_strings['im_condition_list']['Frozen'] = 'Frozen';
$app_list_strings['im_condition_list']['Ultra Frozen'] = 'Ultra Frozen';
$app_list_strings['im_condition_list'][''] = '';
$app_list_strings['processing_type_list'][1] = '1';
$app_list_strings['processing_type_list'][''] = '';
$app_list_strings['processing_transfer_condition_list']['Ambient Temperature'] = 'Ambient Temperature';
$app_list_strings['processing_transfer_condition_list']['Room Temperature'] = 'Room Temperature';
$app_list_strings['processing_transfer_condition_list']['Refrigerated'] = 'Refrigerated';
$app_list_strings['processing_transfer_condition_list']['On Ice'] = 'On Ice';
$app_list_strings['processing_transfer_condition_list']['Frozen on Dry Ice'] = 'Frozen on Dry Ice';
$app_list_strings['processing_transfer_condition_list']['Frozen in Dry Ice Acetone'] = 'Frozen in Dry Ice / Acetone';
$app_list_strings['processing_transfer_condition_list'][''] = '';
$app_list_strings['end_condition_acceptable_list']['No'] = 'No';
$app_list_strings['end_condition_acceptable_list']['Yes'] = 'Yes';
$app_list_strings['end_condition_acceptable_list']['Yes with Comments'] = 'Yes with Comments';
$app_list_strings['end_condition_acceptable_list'][''] = '';
$app_list_strings['im_location_type_list']['Equipment'] = 'Equipment';
$app_list_strings['im_location_type_list']['Room'] = 'Room';
$app_list_strings['im_location_type_list']['RoomCabinet'] = 'Room/Cabinet';
$app_list_strings['im_location_type_list']['RoomShelf'] = 'Room/Shelf';
$app_list_strings['im_location_type_list']['Cabinet'] = 'Cabinet';
$app_list_strings['im_location_type_list']['Shelf'] = 'Shelf';
$app_list_strings['im_location_type_list'][''] = '';
$app_list_strings['location_list'][780] = '780 86th Ave.';
$app_list_strings['location_list'][8945] = '8945 Evergreen Blvd.';
$app_list_strings['location_list'][8960] = '8960 Evergreen Blvd.';
$app_list_strings['location_list'][9055] = '9055 Evergreen Blvd.';
$app_list_strings['location_list'][''] = '';
$app_list_strings['location_shelf_list'][1] = '1';
$app_list_strings['location_shelf_list'][''] = '';
$app_list_strings['location_cabinet_list'][1] = '1';
$app_list_strings['location_cabinet_list'][''] = '';
$app_list_strings['inventory_item_category_list']['Product'] = 'Product';
$app_list_strings['inventory_item_category_list']['Record'] = 'Record';
$app_list_strings['inventory_item_category_list']['Solution'] = 'Solution';
$app_list_strings['inventory_item_category_list']['Specimen'] = 'Specimen';
$app_list_strings['inventory_item_category_list']['Study Article'] = 'Study Article';
$app_list_strings['inventory_item_category_list'][''] = '';
$app_list_strings['im_category_list']['Product'] = 'Product';
$app_list_strings['im_category_list']['Record'] = 'Record';
$app_list_strings['im_category_list']['Specimen'] = 'Specimen';
$app_list_strings['im_category_list']['Study Article'] = 'Study Article';
$app_list_strings['im_category_list'][''] = '';
$app_list_strings['im_type_list']['Record'] = 'Record';
$app_list_strings['im_type_list']['Specimen'] = 'Specimen';
$app_list_strings['im_type_list']['Study Article'] = 'Study Article';
$app_list_strings['im_type_list'][''] = '';
$app_list_strings['inventory_item_type_list']['Block'] = 'Block';
$app_list_strings['inventory_item_type_list']['Control Article'] = 'Control Article';
$app_list_strings['inventory_item_type_list']['Culture'] = 'Culture';
$app_list_strings['inventory_item_type_list']['Data Book'] = 'Data Book';
$app_list_strings['inventory_item_type_list']['Drug'] = 'Drug';
$app_list_strings['inventory_item_type_list']['Equipment Facility Record'] = 'Equipment / Facility Record';
$app_list_strings['inventory_item_type_list']['Extract'] = 'Extract';
$app_list_strings['inventory_item_type_list']['Fecal'] = 'Fecal';
$app_list_strings['inventory_item_type_list']['Hard drive'] = 'Hard drive';
$app_list_strings['inventory_item_type_list']['Plasma'] = 'Plasma';
$app_list_strings['inventory_item_type_list']['Protocol Book'] = 'Protocol Book';
$app_list_strings['inventory_item_type_list']['Serum'] = 'Serum';
$app_list_strings['inventory_item_type_list']['Slide'] = 'Slide';
$app_list_strings['inventory_item_type_list']['Test Article'] = 'Test Article';
$app_list_strings['inventory_item_type_list']['Tissue'] = 'Tissue';
$app_list_strings['inventory_item_type_list']['Urine'] = 'Urine';
$app_list_strings['inventory_item_type_list']['Whole Blood'] = 'Whole Blood';
$app_list_strings['inventory_item_type_list']['Accessory Article'] = 'Accessory Article';
$app_list_strings['inventory_item_type_list']['Data Sheet'] = 'Data Sheet';
$app_list_strings['inventory_item_type_list']['Hard Drive'] = 'Hard Drive';
$app_list_strings['inventory_item_type_list'][''] = '';
$app_list_strings['processing_method_list'][1] = '1';
$app_list_strings['processing_method_list'][''] = '';
$app_list_strings['complete_incomplete_list']['Complete'] = 'Complete';
$app_list_strings['complete_incomplete_list']['Incomplete'] = 'Incomplete';
$app_list_strings['complete_incomplete_list'][''] = '';
$app_list_strings['inventory_item_storage_medium_list']['4 Gluteraldehyde'] = '4% Gluteraldehyde';
$app_list_strings['inventory_item_storage_medium_list']['4 Paraformaldehyde'] = '4% Paraformaldehyde';
$app_list_strings['inventory_item_storage_medium_list']['10 Neutral Buffered Formalin'] = '10% Neutral Buffered Formalin';
$app_list_strings['inventory_item_storage_medium_list']['Davidsons Fixative'] = 'Davidson\'s Fixative';
$app_list_strings['inventory_item_storage_medium_list']['None'] = 'None';
$app_list_strings['inventory_item_storage_medium_list']['Normal Saline'] = 'Normal Saline';
$app_list_strings['inventory_item_storage_medium_list']['Water'] = 'Water';
$app_list_strings['inventory_item_storage_medium_list'][''] = '';
$app_list_strings['sales_wp_list']['Internal Use'] = 'Internal Use';
$app_list_strings['sales_wp_list']['Sales'] = 'Sales';
$app_list_strings['sales_wp_list']['Work Product'] = 'Work Product';
$app_list_strings['sales_wp_list'][''] = '';
$app_list_strings['item_collection_list']['Inventory Item'] = 'Inventory Item';
$app_list_strings['item_collection_list']['Inventory Collection'] = 'Inventory Collection';
$app_list_strings['item_collection_list'][''] = '';
$app_list_strings['composition_list']['Inventory Item'] = 'Inventory Item';
$app_list_strings['composition_list']['Inventory Collection'] = 'Inventory Collection';
$app_list_strings['composition_list'][''] = '';
$app_list_strings['inventory_item_storage_condition_list']['Ambient Temperature'] = 'Ambient Temperature';
$app_list_strings['inventory_item_storage_condition_list']['Frozen'] = 'Frozen';
$app_list_strings['inventory_item_storage_condition_list']['Refrigerated'] = 'Refrigerated';
$app_list_strings['inventory_item_storage_condition_list']['Room Temperature'] = 'Room Temperature';
$app_list_strings['inventory_item_storage_condition_list']['Ultra Frozen in Freezer'] = 'Ultra Frozen in Freezer';
$app_list_strings['inventory_item_storage_condition_list']['Ultra Frozen in Liquid Nitrogen'] = 'Ultra Frozen in Liquid Nitrogen';
$app_list_strings['inventory_item_storage_condition_list'][''] = '';
