<?php
 // created: 2019-04-15 12:14:14

$app_list_strings['archiving_type_of_contact_list']=array (
  '' => '',
  '1st Contact Email' => '1st Contact - Email',
  '1st Contact Phone' => '1st Contact - Phone',
  '2nd Contact Email' => '2nd Contact - Email',
  '2nd Contact Phone' => '2nd Contact - Phone',
  '3rd Contact Email' => '3rd Contact - Email',
  '3rd Contact Phone' => '3rd Contact - Phone',
);