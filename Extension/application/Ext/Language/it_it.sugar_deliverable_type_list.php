<?php
 // created: 2020-03-16 11:48:24

$app_list_strings['deliverable_type_list']=array (
  '' => '',
  'Hay' => 'Hay',
  'Slide' => 'Slide',
  'Tissue' => 'Tissue',
  'BloodPlasmaSerum' => 'Blood/Plasma/Serum',
  'Paraffin Blocks' => 'Paraffin Blocks',
  'Study Materials' => 'Study Materials',
  'Feed' => 'Feed',
);