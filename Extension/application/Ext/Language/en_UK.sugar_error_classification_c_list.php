<?php
 // created: 2019-12-02 13:26:18

$app_list_strings['error_classification_c_list']=array (
  'Error' => 'Error',
  'Non Impactful Deviation' => 'Non-Impactful Deviation',
  'Impactful Deviation' => 'Impactful Deviation',
  'Choose One' => 'Choose One',
  'Withdrawn Error' => 'Withdrawn Error',
  'Observation' => 'Observation',
  'Unforeseen Circumstance Unanticipated Response' => 'Unforeseen Circumstance/Unanticipated Response (UCUR)',
  'Adverse Event' => 'Adverse Event',
  'Pending' => 'Pending',
  'Duplicate' => 'Duplicate',
  'Notification' => 'Notification',
  '' => '',
  'Opportunity for Improvement' => 'Opportunity for Improvement',
  'Job well done' => 'Job well done',
  'A' => 'A',
  'M' => 'M',
  'S' => 'S',
  'C' => 'C',
  'NA' => 'NA',
);