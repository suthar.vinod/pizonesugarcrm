<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['OT_Operator_Tracking'] = 'Operator Tracking';
$app_list_strings['moduleListSingular']['OT_Operator_Tracking'] = 'Operator Tracking';
$app_list_strings['operator_role_list']['Primary'] = 'Primary';
$app_list_strings['operator_role_list']['Secondary'] = 'Secondary';
$app_list_strings['operator_role_list']['Observer'] = 'Observer';
$app_list_strings['operator_role_list'][''] = '';
$app_list_strings['ot_category_list'][1] = '1';
$app_list_strings['ot_category_list'][''] = '';
$app_list_strings['ot_type_list'][1] = '1';
$app_list_strings['ot_type_list'][''] = '';
