<?php
 // created: 2022-02-01 07:38:48

$app_list_strings['room_type_list']=array (
  '' => '',
  'Animal Housing' => 'Animal Housing',
  'Archive' => 'Archive',
  'Cath Lab' => 'Cath Lab',
  'IT' => 'IT',
  'Lab' => 'Lab',
  'OR' => 'OR',
  'Shop' => 'Shop',
  'Storage' => 'Storage',
  'Various Locations' => 'Various Locations',
  'Warehouse' => 'Warehouse',
  'CT' => 'CT',
  'Necropsy' => 'Necropsy',
  'Wetlab' => 'Wetlab',
  'Behavior' => 'Behavior',
);