<?php
 // created: 2017-06-16 19:19:41

$app_list_strings['reminder_time_options']=array (
  -1 => 'Nėra',
  60 => 'Prieš 1 minutę',
  300 => 'Prieš 5 minutes',
  600 => 'Prieš 10 minučių',
  900 => 'Prieš 15 minučių',
  1800 => 'Prieš 30 minučių',
  3600 => 'Prieš 1 valandą',
  7200 => 'Prieš 2 valandas',
  10800 => 'Prieš 3 valandas',
  18000 => 'Prieš 5 valandas',
  86400 => 'Prieš 1 dieną',
  604800 => '7 days prior',
);