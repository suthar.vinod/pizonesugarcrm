<?php
 // created: 2018-11-15 14:38:45

$app_list_strings['instrument_list']=array (
  'LCMS' => 'LC/MS',
  'GCMS' => 'GC/MS',
  'ICPMS' => 'ICP-MS',
  'FTIR' => 'FTIR',
  '' => '',
  'LCMS GCMS ICPMS' => 'LC/MS, GC/MS, ICP/MS',
  'LCMS GCMS ICPMS FTIR' => 'LC/MS, GC/MS, ICP/MS, FTIR',
  'LCUV' => 'LC/UV',
);