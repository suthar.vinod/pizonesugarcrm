<?php

//create the links label
$app_strings['LNK_REPORTS_C'] = 'All Notification for a Study';
$app_strings['LNK_ALLNOTIFICATIONFORSTUDY'] = 'All Notification for a Study';
$app_strings['LNK_ALLDEVIATIONSFORSTUDY'] = 'All Deviation For Study';
$app_strings['LNK_ALLADVERSEEVENTSFORSTUDY'] = 'All Adverse Events for a Study';
$app_strings['LNK_MOSTRECENTWPAFORTESTSYSTEM'] = 'WPA Report';
$app_strings['LNK_CUSTOMCOMREPORT'] = 'Pyrogen Animal Use';
$app_strings['LNK_CUSTOMTSWEIGHT'] = 'Custom Test System Weight';
$app_strings['LNK_CUSTOMTSCAPACITY'] = 'Custom Capacity Report';
$app_strings['LNK_CUSTOMTSWPAALLOCATION'] = 'Ops Support Assignments Review';
$app_strings['LNK_CUSTOMTSDEVIATIONPERANIMAL'] = 'Deviations per Animal by Department';
$app_strings['LNK_CUSTOMTSPATHOLOGYWPD'] = 'Custom report for Pathology WPDs';
$app_strings['LNK_CUSTOMOUTTOSPONSORWPD'] = 'WPDs Out to Sponsor for 30+ Days';
$app_strings['LNK_DAILYSCHEDULINGNEEDS'] = 'Daily Scheduling Needs - LA';
$app_strings['LNK_DAILYSCHEDULINGNEEDSSA'] = 'Daily Scheduling Needs - SA';