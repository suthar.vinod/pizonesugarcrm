<?php
 // created: 2020-05-13 18:10:38

$app_list_strings['work_product_compliance_list']=array (
  'Not Applicable' => 'Not Applicable',
  'NonGLP Discovery' => 'NonGLP - Discovery',
  'NonGLP Structured' => 'NonGLP - Structured',
  'GLP' => 'GLP',
  'GLP Not Performed' => 'GLP, Not Performed',
  'GLP Discontinued' => 'GLP, Discontinued',
  'Withdrawn' => 'Obsoleted, valid 08-13-14 to 04-27-18 GLP, Withdrawn',
  'GLP_Amended_NonGLP' => 'Obsoleted, valid 08-13-14 to 04-27-18 GLP, Amended nonGLP',
  'nonGLP' => 'nonGLP',
  '' => '',
);