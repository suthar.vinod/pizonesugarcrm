<?php
 // created: 2022-02-01 07:38:49

$app_list_strings['room_type_list']=array (
  '' => '',
  'Animal Housing' => 'Animal Housing',
  'Archive' => 'Archive',
  'Behavior' => 'Behavior',
  'Cath Lab' => 'Cath Lab',
  'CT' => 'CT',
  'IT' => 'IT',
  'Lab' => 'Lab',
  'Necropsy' => 'Necropsy',
  'OR' => 'OR',
  'Shop' => 'Shop',
  'Storage' => 'Storage',
  'Various Locations' => 'Various Locations',
  'Warehouse' => 'Warehouse',
  'Wetlab' => 'Wetlab',
);