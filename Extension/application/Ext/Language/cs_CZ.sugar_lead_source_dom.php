<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Volání naslepo',
  'Existing Customer' => 'Existujíci zákazník',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Zaměstnanec',
  'Conference' => 'Konference',
  'Trade Show' => 'Veletrh',
  'Web Site' => 'Webová stránka',
  'Email' => 'Email',
  'Campaign' => 'Kampaň',
  'Support Portal User Registration' => 'Registrace uživatele portálu zákaznické podpory',
  'Other' => 'Jiné',
  'Referral' => 'Referral',
);