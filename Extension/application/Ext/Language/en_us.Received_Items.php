<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['RI_Received_Items'] = 'Received Items';
$app_list_strings['moduleListSingular']['RI_Received_Items'] = 'Received Item';
$app_list_strings['ri_type_2_list']['ORI'] = 'ORI';
$app_list_strings['ri_type_2_list']['POI'] = 'POI';
$app_list_strings['ri_type_2_list'][''] = '';
