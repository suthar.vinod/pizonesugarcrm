<?php
 // created: 2020-12-15 12:39:02

$app_list_strings['enrollment_status_list']=array (
  '' => '',
  'On Study' => 'On Study',
  'On Study Transferred' => 'Transferred',
  'Not Used' => 'Not Used',
  'Assigned Backup' => '(Obsolete) Assigned Backup',
  'Assigned On Study' => '(Obsolete) Assigned On Study',
  'Non naive Stock' => '(Obsolete) Non-naive Stock',
  'Enrolled Not Used' => '(Obsolete) Enrolled – Not Used',
  'On Study Backup' => '(Obsolete) On Study Backup',
  'Choose One' => '(Obsolete) Stock',
);