<?php
 // created: 2022-02-03 08:27:26

$app_list_strings['severity_score_list']=array (
  '' => '',
  'NA' => 'NA',
  1 => '1 - Minor',
  3 => '3 - Significant',
  5 => '5 - Critical',
);