<?php
 // created: 2016-04-03 17:38:22

$app_list_strings['sales_activity_type_list']=array (
  'Within 1 month' => 'Within 1 month',
  'Within 1 to 3 months' => 'Within 1 to 3 months',
  'Within 3 to 6 months' => 'Within 3 to 6 months',
  'Within 6 to 12 months' => 'Within 6 to 12 months',
  'Greater than 12 months' => 'Greater than 12 months',
);