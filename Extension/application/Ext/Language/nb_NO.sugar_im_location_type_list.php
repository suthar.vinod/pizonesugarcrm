<?php
 // created: 2021-11-10 00:50:52

$app_list_strings['im_location_type_list']=array (
  'Equipment' => 'Equipment',
  'Room' => 'Room',
  'RoomCabinet' => 'Room/Cabinet',
  'RoomShelf' => 'Room/Shelf',
  'Cabinet' => 'Cabinet',
  'Shelf' => 'Shelf',
  '' => '',
  'Known' => 'Known',
  'Unknown' => 'Unknown',
);