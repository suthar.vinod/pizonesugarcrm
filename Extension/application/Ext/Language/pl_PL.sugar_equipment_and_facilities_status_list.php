<?php
 // created: 2019-09-16 12:15:31

$app_list_strings['equipment_and_facilities_status_list']=array (
  '' => '',
  'Active' => 'Active',
  'Inactive' => 'Inactive',
  'Out for Service' => 'Out for Service',
  'Retired' => 'Retired',
  'Returned to Sponsor' => 'Returned to Sponsor',
);