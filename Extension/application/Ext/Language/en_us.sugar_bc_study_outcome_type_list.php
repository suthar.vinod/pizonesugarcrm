<?php
 // created: 2021-10-20 04:32:50

$app_list_strings['bc_study_outcome_type_list']=array (
  '' => '',
  'Completed Study' => 'Completed Study',
  'Passed Study' => 'Passed Study',
  'Failed Study' => 'Failed Study',
  'Expanded Study' => 'Expanded Study',
  'Discontinued Study' => 'Discontinued Study',
  'None' => 'None (Obsoleted 26Oct2021)',
  'Aborted Study' => 'Aborted Study (Obsoleted 26Oct2021)',
  'CAB Study' => 'CAB Study (Obsoleted 26Oct2021)',
);