<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/da_DK.Controlled_Documents_QA_Review_Docs.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/da_DK.Controlled_Documents_QA_Review_Docs.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Markedsføringsside';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Videnbase';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Salgsside';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Markedsføringsmateriale';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Produktbrochurer';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'Ofte stillede spørgsmål';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Aktiv';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Kladde';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'Ofte stillede spørgsmål';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Udløbet';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Til gennemsyn';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Afventer';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Markedsføringsside';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Videnbase';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Salgsside';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Markedsføringsmateriale';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Produktbrochurer';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'Ofte stillede spørgsmål';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Aktiv';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Kladde';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'Ofte stillede spørgsmål';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Udløbet';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Til gennemsyn';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Afventer';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Markedsføringsside';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Videnbase';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Salgsside';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Markedsføringsmateriale';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Produktbrochurer';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'Ofte stillede spørgsmål';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Aktiv';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Kladde';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'Ofte stillede spørgsmål';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Udløbet';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Til gennemsyn';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Afventer';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';

?>
