<?php
 // created: 2017-05-02 17:02:40

$app_list_strings['quality_assurance_activity_list']=array (
  '' => '',
  'Protocol Audit' => 'Protocol Audit',
  'Implant Procedure Audit' => 'Implant Procedure Audit',
  'Follow_Up Procedure Audit' => 'Follow-Up Procedure Audit',
  'Term Procedure Audit' => 'Term Procedure Audit',
  'Data Audit' => 'Data Audit',
  'Report Audit' => 'Report Audit',
  'Audit Findings Reporting' => 'Audit Findings Reporting',
);