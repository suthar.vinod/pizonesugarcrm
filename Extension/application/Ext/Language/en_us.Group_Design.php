<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Group_Design.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Group_Design.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['GD_Group_Design'] = 'Group Designs';
$app_list_strings['moduleListSingular']['GD_Group_Design'] = 'Group Design';
$app_list_strings['record_status_list']['Active'] = 'Active';
$app_list_strings['record_status_list']['Archived'] = 'Archived';
$app_list_strings['record_status_list'][''] = '';
$app_list_strings['number_list']['01'] = '01';
$app_list_strings['number_list']['02'] = '02';
$app_list_strings['number_list']['03'] = '03';
$app_list_strings['number_list']['04'] = '04';
$app_list_strings['number_list']['05'] = '05';
$app_list_strings['number_list']['06'] = '06';
$app_list_strings['number_list']['07'] = '07';
$app_list_strings['number_list']['08'] = '08';
$app_list_strings['number_list']['09'] = '09';
$app_list_strings['number_list'][10] = '10';
$app_list_strings['number_list'][''] = '';
$app_list_strings['study_article_type_list']['Test'] = 'Test';
$app_list_strings['study_article_type_list']['Control'] = 'Control';
$app_list_strings['study_article_type_list']['Test Control'] = 'Test & Control';
$app_list_strings['study_article_type_list']['Sham'] = 'Sham';
$app_list_strings['study_article_type_list'][''] = '';
$app_list_strings['quantity_type_list']['Exact'] = 'Exact';
$app_list_strings['quantity_type_list']['Max'] = 'Max';
$app_list_strings['quantity_type_list'][''] = '';
$app_list_strings['gd_study_article_type_list']['Control'] = 'Control';
$app_list_strings['gd_study_article_type_list']['Sham'] = 'Sham';
$app_list_strings['gd_study_article_type_list']['Test'] = 'Test';
$app_list_strings['gd_study_article_type_list']['Test Control'] = 'Test & Control';
$app_list_strings['gd_study_article_type_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['GD_Group_Design'] = 'Group Designs';
$app_list_strings['moduleListSingular']['GD_Group_Design'] = 'Group Design';
$app_list_strings['record_status_list']['Active'] = 'Active';
$app_list_strings['record_status_list']['Archived'] = 'Archived';
$app_list_strings['record_status_list'][''] = '';
$app_list_strings['number_list']['01'] = '01';
$app_list_strings['number_list']['02'] = '02';
$app_list_strings['number_list']['03'] = '03';
$app_list_strings['number_list']['04'] = '04';
$app_list_strings['number_list']['05'] = '05';
$app_list_strings['number_list']['06'] = '06';
$app_list_strings['number_list']['07'] = '07';
$app_list_strings['number_list']['08'] = '08';
$app_list_strings['number_list']['09'] = '09';
$app_list_strings['number_list'][10] = '10';
$app_list_strings['number_list'][''] = '';
$app_list_strings['study_article_type_list']['Test'] = 'Test';
$app_list_strings['study_article_type_list']['Control'] = 'Control';
$app_list_strings['study_article_type_list']['Test Control'] = 'Test & Control';
$app_list_strings['study_article_type_list']['Sham'] = 'Sham';
$app_list_strings['study_article_type_list'][''] = '';
$app_list_strings['quantity_type_list']['Exact'] = 'Exact';
$app_list_strings['quantity_type_list']['Max'] = 'Max';
$app_list_strings['quantity_type_list'][''] = '';
$app_list_strings['gd_study_article_type_list']['Control'] = 'Control';
$app_list_strings['gd_study_article_type_list']['Sham'] = 'Sham';
$app_list_strings['gd_study_article_type_list']['Test'] = 'Test';
$app_list_strings['gd_study_article_type_list']['Test Control'] = 'Test & Control';
$app_list_strings['gd_study_article_type_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['GD_Group_Design'] = 'Group Designs';
$app_list_strings['moduleListSingular']['GD_Group_Design'] = 'Group Design';
$app_list_strings['record_status_list']['Active'] = 'Active';
$app_list_strings['record_status_list']['Archived'] = 'Archived';
$app_list_strings['record_status_list'][''] = '';
$app_list_strings['number_list']['01'] = '01';
$app_list_strings['number_list']['02'] = '02';
$app_list_strings['number_list']['03'] = '03';
$app_list_strings['number_list']['04'] = '04';
$app_list_strings['number_list']['05'] = '05';
$app_list_strings['number_list']['06'] = '06';
$app_list_strings['number_list']['07'] = '07';
$app_list_strings['number_list']['08'] = '08';
$app_list_strings['number_list']['09'] = '09';
$app_list_strings['number_list'][10] = '10';
$app_list_strings['number_list'][''] = '';
$app_list_strings['study_article_type_list']['Test'] = 'Test';
$app_list_strings['study_article_type_list']['Control'] = 'Control';
$app_list_strings['study_article_type_list']['Test Control'] = 'Test & Control';
$app_list_strings['study_article_type_list']['Sham'] = 'Sham';
$app_list_strings['study_article_type_list'][''] = '';
$app_list_strings['quantity_type_list']['Exact'] = 'Exact';
$app_list_strings['quantity_type_list']['Max'] = 'Max';
$app_list_strings['quantity_type_list'][''] = '';
$app_list_strings['gd_study_article_type_list']['Control'] = 'Control';
$app_list_strings['gd_study_article_type_list']['Sham'] = 'Sham';
$app_list_strings['gd_study_article_type_list']['Test'] = 'Test';
$app_list_strings['gd_study_article_type_list']['Test Control'] = 'Test & Control';
$app_list_strings['gd_study_article_type_list'][''] = '';

?>
