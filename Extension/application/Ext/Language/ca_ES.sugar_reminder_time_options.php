<?php
 // created: 2017-06-16 19:19:42

$app_list_strings['reminder_time_options']=array (
  -1 => 'Cap',
  60 => '1 minut abans',
  300 => '5 minuts abans',
  600 => '10 minuts abans',
  900 => '15 minuts abans',
  1800 => '30 minuts abans',
  3600 => '1 hora abans',
  7200 => '2 hores abans',
  10800 => '3 hores abans',
  18000 => '5 hores abans',
  86400 => '1 dia abans',
  604800 => '7 days prior',
);