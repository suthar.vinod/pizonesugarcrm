<?php
 // created: 2017-05-02 16:30:15

$app_list_strings['business_develop_activity_list']=array (
  '' => '',
  'Conference Call' => 'Conference Call',
  'Client_Visit_Onsite' => 'Onsite Client Visit',
  'Client_Visit_Offsite' => 'Offsite Client Visit',
  'Tradeshow' => 'Tradeshow',
  'Procedure Coverage' => 'Procedure Coverage',
  'Other' => 'Other',
);