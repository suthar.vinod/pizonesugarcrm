<?php
 // created: 2017-06-16 19:19:41

$app_list_strings['reminder_time_options']=array (
  -1 => 'Neviens',
  60 => '1 minūti pirms',
  300 => '5 minūtes pirms',
  600 => '10 minūtes pirms',
  900 => '15 minūtes pirms',
  1800 => '30 minūtes pirms',
  3600 => '1 stundu pirms',
  7200 => '2 stundas pirms',
  10800 => '3 stundas pirms',
  18000 => '5 stundas pirms',
  86400 => '1 dienu pirms',
  604800 => '7 days prior',
);