<?php
 // created: 2018-11-14 22:20:27

$app_list_strings['departments_list']=array (
  '' => '',
  'SA AC' => 'SA AC',
  'LA AC' => 'LA AC',
  'SP' => 'SP',
  'IVP' => 'IVP',
  'SA OP' => 'SA OP',
  'DVM' => 'DVM',
  'LA OP' => 'LA OP',
  'ISR' => 'ISR',
  'PS' => 'PS',
  'SCI' => 'SCI',
  'AS' => 'AS',
  'Facility' => 'Facility',
);