<?php
 // created: 2017-06-16 19:19:40

$app_list_strings['reminder_time_options']=array (
  -1 => 'Žádný',
  60 => 'Před 1 minutou',
  300 => '5 minut předem',
  600 => '10 minut předem',
  900 => '15 minut předem',
  1800 => '30 minut předem',
  3600 => 'Před 1 hodinou',
  7200 => '2 hodiny předem',
  10800 => '3 hodiny předem',
  18000 => '5 hodin předem',
  86400 => 'Před 1 dnem',
  604800 => '7 days prior',
);