<?php
 // created: 2018-09-25 15:41:09

$app_list_strings['functional_area_list']=array (
  '' => '',
  'Analytical Services' => 'Analytical',
  'Animal Holding' => 'Animal Holding',
  'Consulting Services' => 'Consulting Services',
  'Cytotoxicity' => 'Cytotoxicity',
  'Genotoxicity' => 'Genotoxicity',
  'Hemocompatibility' => 'Hemocompatibility',
  'Implantation' => 'Implantation',
  'Interventional Surgical' => 'Interventional and Surgical',
  'Irritation' => 'Irritation',
  'Miscellaneous Testing' => 'Miscellaneous Testing',
  'Pathology' => 'Pathology',
  'Pharmacology' => 'Pharmacology',
  'Sensitization' => 'Sensitization',
  'Systemic Toxicity' => 'Systemic Toxicity',
  'Toxicity' => 'Toxicity',
  'Regulatory' => 'Regulatory',
  'Training' => 'Training',
);