<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['ABC12_Work_Product_Activities'] = 'Work Product Activities';
$app_list_strings['moduleListSingular']['ABC12_Work_Product_Activities'] = 'Work Product Activities';
$app_list_strings['Department_list']['Select'] = 'Select';
$app_list_strings['Department_list']['Analytical'] = 'Analytical';
$app_list_strings['Department_list']['Biocompatibility'] = 'Biocompatibility';
$app_list_strings['Department_list']['Gross Pathology'] = 'Gross Pathology';
$app_list_strings['Department_list']['Histology'] = 'Histology';
$app_list_strings['Department_list']['Interventional Surgical'] = 'Interventional / Surgical';
$app_list_strings['Department_list']['Microbiology'] = 'Microbiology';
$app_list_strings['Department_list']['Pharmacology'] = 'Pharmacology';
$app_list_strings['Department_list']['Toxicology'] = 'Toxicology';
$app_list_strings['Department_list']['Cytotoxicity'] = 'Cytotoxicity';
$app_list_strings['Department_list']['Hemocompatibility'] = 'Hemocompatibility';
$app_list_strings['Department_list']['Genotoxicity'] = 'Genotoxicity';
$app_list_strings['Department_list']['Sensitization'] = 'Sensitization';
$app_list_strings['Department_list']['Irritation'] = 'Irritation';
$app_list_strings['Department_list']['Systemic Toxicity'] = 'Systemic Toxicity';
$app_list_strings['Department_list']['Intramuscular'] = 'Intramuscular';
$app_list_strings['Department_list']['Choose Department'] = 'Choose Department';
$app_list_strings['activity_list']['Choose One'] = 'Choose One';
$app_list_strings['activity_list']['Literature Research'] = 'Literature Research';
$app_list_strings['activity_list']['Protocol Development'] = 'Protocol Development';
$app_list_strings['activity_list']['Study Article Reconciliation'] = 'Study Article Reconciliation';
$app_list_strings['activity_list']['Procedure Oversight'] = 'Procedure Oversight';
$app_list_strings['activity_list']['Data Entry'] = 'Data Entry';
$app_list_strings['activity_list']['Report Editing'] = 'Report Editing ';
$app_list_strings['activity_list']['Report Drafting'] = 'Report Drafting';
$app_list_strings['activity_list']['Report Finalization'] = 'Report Finalization';
$app_list_strings['activity_list']['Slide Evaluation'] = 'Slide Evaluation';
$app_list_strings['activity_list']['Conference Call'] = 'Sponsor Conference Call';
$app_list_strings['activity_list']['Internal Meeting'] = 'Internal Meeting';
$app_list_strings['activity_list']['Sponsor Meeting'] = 'Sponsor Meeting';
$app_list_strings['activity_list']['Protocol Audit'] = 'Protocol Audit';
$app_list_strings['activity_list']['Critical Phase Audit'] = 'Critical Phase Audit';
$app_list_strings['activity_list']['Report Audit'] = 'Report Audit';
$app_list_strings['activity_list']['Inspection Report Drafting'] = 'Inspection Report Drafting';
$app_list_strings['activity_list']['Necropsy Oversight'] = 'Necropsy Oversight';
$app_list_strings['activity_list']['Sample Preparation'] = 'Sample Preparation';
