<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Aktyvūs pardavimai',
  'Existing Customer' => 'Esamas klientas',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Darbuotojas',
  'Conference' => 'Konferencija',
  'Trade Show' => 'Paroda',
  'Web Site' => 'Tinklapis',
  'Email' => 'El. paštas',
  'Campaign' => 'Campaign',
  'Support Portal User Registration' => 'Aptarnavimo portalo vartotojo registracija',
  'Other' => 'Kita',
  'Referral' => 'Referral',
);