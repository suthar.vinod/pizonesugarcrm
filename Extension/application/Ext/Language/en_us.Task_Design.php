<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['TaskD_Task_Design'] = 'Task Designs';
$app_list_strings['moduleListSingular']['TaskD_Task_Design'] = 'Task Design';
$app_list_strings['yes_no_list']['Yes'] = 'Yes';
$app_list_strings['yes_no_list']['No'] = 'No';
$app_list_strings['yes_no_list'][''] = '';
$app_list_strings['task_type_list']['Custom'] = 'Custom';
$app_list_strings['task_type_list']['Standard'] = 'Custom';
$app_list_strings['task_type_list'][''] = '';
$app_list_strings['standard_task_list']['Anesthetized Blood Draw'] = 'Anesthetized Blood Draw';
$app_list_strings['standard_task_list']['Awake Blood Draw'] = 'Awake Blood Draw';
$app_list_strings['standard_task_list']['Bandage Change'] = 'Bandage Change';
$app_list_strings['standard_task_list']['Battery ChangeCharge'] = 'Battery Change/Charge';
$app_list_strings['standard_task_list']['Body Weight'] = 'Body Weight';
$app_list_strings['standard_task_list']['CT'] = 'CT';
$app_list_strings['standard_task_list']['Diet Mods'] = 'Diet Mods';
$app_list_strings['standard_task_list']['EKGECG'] = 'EKG/ECG';
$app_list_strings['standard_task_list']['Gait Exam'] = 'Gait Exam';
$app_list_strings['standard_task_list']['Neruo Exam'] = 'Neruo Exam';
$app_list_strings['standard_task_list']['Physical Exam'] = 'Physical Exam';
$app_list_strings['standard_task_list']['Ultrasound'] = 'Ultrasound';
$app_list_strings['standard_task_list']['Urinalysis'] = 'Urinalysis';
$app_list_strings['standard_task_list'][''] = '';
$app_list_strings['equipment_required_list']['12 lead'] = '12-lead';
$app_list_strings['equipment_required_list']['Bard'] = 'Bard';
$app_list_strings['equipment_required_list']['CT'] = 'CT';
$app_list_strings['equipment_required_list']['EKGECG'] = 'EKG/ECG';
$app_list_strings['equipment_required_list']['Fluroscopy'] = 'Fluroscopy';
$app_list_strings['equipment_required_list']['PowerLab'] = 'PowerLab';
$app_list_strings['equipment_required_list']['Ultrasound APS'] = 'Ultrasound (APS)';
$app_list_strings['equipment_required_list']['Ultrasound Jim B'] = 'Ultrasound (Jim B.)';
$app_list_strings['equipment_required_list'][''] = '';
$app_list_strings['taskdesign_task_type_list']['Custom'] = 'Custom';
$app_list_strings['taskdesign_task_type_list']['Standard'] = 'Standard';
$app_list_strings['taskdesign_task_type_list'][''] = '';
$app_list_strings['plan_actual_list']['Plan'] = 'Plan';
$app_list_strings['plan_actual_list']['Actual'] = 'Actual';
$app_list_strings['plan_actual_list'][''] = '';
$app_list_strings['procedure_activity_list']['Activity'] = 'Activity';
$app_list_strings['procedure_activity_list']['Procedure'] = 'Procedure';
$app_list_strings['procedure_activity_list'][''] = '';
$app_list_strings['td_tier_relative_list']['NA'] = 'N/A';
$app_list_strings['td_tier_relative_list']['1st Tier'] = '1st Tier';
$app_list_strings['td_tier_relative_list']['2nd Tier'] = '2nd Tier';
$app_list_strings['td_tier_relative_list'][''] = '';
$app_list_strings['task_procedure_list']['Task'] = 'Task';
$app_list_strings['task_procedure_list']['Procedure'] = 'Procedure';
$app_list_strings['task_procedure_list'][''] = '';
