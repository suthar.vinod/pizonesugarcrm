<?php
 // created: 2020-09-03 12:23:17

$app_list_strings['product_allowed_storage_list']=array (
  '' => '',
  'Ambient Temperature' => 'Ambient Temperature',
  'Room Temperature' => 'Room Temperature',
  'Refrigerated' => 'Refrigerated',
  'Frozen' => 'Frozen',
  'Ultra Frozen in Liquid Nitrogen' => 'Ultra Frozen in Liquid Nitrogen',
  'Ultra Frozen in Freezer' => 'Ultra Frozen in Freezer',
  'To Be Determined' => 'To Be Determined',
);