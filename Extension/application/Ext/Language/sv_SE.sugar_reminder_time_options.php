<?php
 // created: 2017-06-16 19:19:42

$app_list_strings['reminder_time_options']=array (
  -1 => 'Ingen',
  60 => '1 minut före',
  300 => '5 minuter före',
  600 => '10 minuter före',
  900 => '15 minuter före',
  1800 => '30 minuter före',
  3600 => '1 timme före',
  7200 => '2 timmar före',
  10800 => '3 timmar före',
  18000 => '5 timmar före',
  86400 => '1 dag före',
  604800 => '7 days prior',
);