<?php
 // created: 2022-05-24 07:15:38

$app_list_strings['work_product_status_list']=array (
  'Pending' => 'Pending',
  'In Development' => 'In Development',
  'Testing' => 'Testing',
  'Archived' => 'Complete',
  'Withdrawn' => 'Study Not Performed',
  'Reporting' => 'Reporting',
  'Transferred to NW' => 'Transferred to NW',
);