<?php
 // created: 2020-05-13 17:15:47

$app_list_strings['study_compliance_list']=array (
  '' => '',
  'NonGLP Discovery' => 'NonGLP - Discovery',
  'NonGLP Structured' => 'NonGLP - Structured',
  'GLP' => 'GLP',
  'ISO' => 'ISO',
  'USP' => 'USP',
  'JMHLW' => 'JMHLW',
  'Not Applicable' => 'Not Applicable',
  'non_GLP' => 'non-GLP',
);