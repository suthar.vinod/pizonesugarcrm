<?php
 // created: 2020-05-13 16:50:34

$app_list_strings['invoice_installment_percent_list']=array (
  '' => '',
  'None' => 'None',
  10 => '10%',
  25 => '25%',
  40 => '40%',
  50 => '50%',
  60 => '60%',
  75 => '75%',
  100 => '100%',
  '1x Monthly' => '1x Monthly',
);