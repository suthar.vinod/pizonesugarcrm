<?php
 // created: 2017-04-13 19:17:43

$app_list_strings['invoice_installment_percent_list']=array (
  'Choose One' => 'Choose One',
  'None' => 'None',
  25 => '25',
  50 => '50',
  100 => '100',
  '1x Monthly' => '1x Monthly',
  40 => '40%',
  60 => '60%',
  10 => '10%',
  75 => '75%',
);