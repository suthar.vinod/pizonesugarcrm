<?php
 // created: 2016-04-19 16:28:30

$app_list_strings['study_pathologist_list']=array (
  'Choose Pathologist...' => 'Choose Pathologist...',
  'Igor Polyakov' => 'Igor Polyakov',
  'Lynette Phillips' => 'Lynette Phillips',
  'Adrienne Schucker' => 'Adrienne Schucker',
  'None' => 'None',
  'External Personnel' => 'External Personnel',
  'Muhammad Ahsan' => 'Muhammad Ahsan',
);