<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.WPEsModuleCustomizations.php
 
$app_list_strings['usda_category_list'] = array (
  'B' => 'B',
  'C' => 'C',
  'D' => 'D',
  'E' => 'E',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['enrollment_status_list'] = array (
  'Choose One' => 'Choose One',
  'Assigned Backup' => 'Assigned Backup',
  'Assigned On Study' => 'Assigned On Study',
  'Non naive Stock' => 'Non-naive Stock',
  'Not Used' => 'Not Used',
  'On Study' => 'On Study',
  'On Study Backup' => 'On Study Backup',
  'On Study Transferred' => 'On Study Transferred',
  'Stock' => 'Stock',
  'Deceased' => 'Deceased',
);
?>
