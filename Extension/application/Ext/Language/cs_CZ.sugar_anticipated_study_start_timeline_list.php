<?php
 // created: 2019-07-31 11:43:12

$app_list_strings['anticipated_study_start_timeline_list']=array (
  '' => '',
  '3 4 weeks' => '3-4 weeks',
  '1 2 months' => '1-2 months',
  '2 3 months' => '2-3 months',
  '3 6 months' => '3-6 months',
  '6 12 months' => '6-12 months',
  '12 months' => '12+ months',
);