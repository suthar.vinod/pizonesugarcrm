<?php
 // created: 2021-12-14 09:09:41

$app_list_strings['type_of_acclimation_list']=array (
  '' => '',
  'JacketHarness' => 'Jacket/Harness',
  'E collar' => 'E-collar',
  'C collar' => 'C-collar',
  'Manual restraint' => 'Manual restraint',
  'Leash' => 'Leash',
  'Other' => 'Other',
  'Crossties' => 'Crossties',
  'Sling' => 'Sling',
  'Imaging Cart' => 'Imaging Cart',
  'Pyrogen testingRabbit restrainer' => 'Pyrogen testing/Rabbit restrainer',
  'Crossties Jacket' => 'Crossties & Jacket',
);