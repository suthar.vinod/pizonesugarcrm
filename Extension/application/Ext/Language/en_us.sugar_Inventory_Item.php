<?php 
$app_list_strings['unique_id_type_list'] = array (
  'Internal' => 'Internal Only',
  'External' => 'External & Internal',
  '' => '',
);$app_list_strings['inventory_item_category_list'] = array (
  'Product' => 'Product',
  'Record' => 'Record',
  'Solution' => 'Solution',
  'Specimen' => 'Specimen',
  '' => '',
);$app_list_strings['inventory_item_type_list'] = array (
  'Accessory Product' => 'Accessory Product',
  'Block' => 'Block',
  'Chemical' => 'Chemical',
  'Cleaning Agent' => 'Cleaning Agent',
  'Control Article' => 'Control Article',
  'Culture' => 'Culture',
  'Data Book' => 'Data Book',
  'Data Sheet' => 'Data Sheet',
  'Drug' => 'Drug',
  'Equipment Facility Record' => 'Equipment / Facility Record',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Hard drive' => 'Hard drive',
  'Plasma' => 'Plasma',
  'Protocol Book' => 'Protocol Book',
  'Reagent' => 'Reagent',
  'Serum' => 'Serum',
  'Slide' => 'Slide',
  'Test Article' => 'Test Article',
  'Tissue' => 'Tissue',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
  '' => '',
);$app_list_strings['inventory_item_subtype_list'] = array (
  'Sample Prep BAS logs for equipmentrooms' => 'Sample Prep BAS logs for equipment/rooms',
  'Stock Animal Weights' => 'Stock Animal Weights',
  'Analytical Calibration Check for Adjustable Pipettes' => 'Analytical - Calibration Check for Adjustable Pipettes',
  '8960 Room Logs' => '8960 Room Logs',
  'OR3 OR4 OR5 Daily Maintenance Checklist' => 'OR3 OR4 OR5 Daily Maintenance Checklist',
  'CL1 CL2 CL3 CL4 Daily Maintenance Checklist' => 'CL1 CL2 CL3 CL4 Daily Maintenance Checklist',
  '8945 Prep Daily Maintenance Checklist' => '8945 Prep Daily Maintenance Checklist',
  'Cycle Log' => 'Cycle Log',
  'Cycle Printer Output' => 'Cycle Printer Output',
  'Biological Sample Test Log' => 'Biological Sample Test Log',
  'Siemens Fluroroscopy Patient and Leonardo Workstation Log' => 'Siemens Fluroroscopy Patient and Leonardo Workstation Log',
  'Change of DICOM Metadata Notification' => 'Change of DICOM Metadata Notification',
  'Baxter COM2 Cardiac Output Computer Data Collection' => 'Baxter COM-2 Cardiac Output Computer Data Collection',
  'Verification of the Cardiopulmonary Bypass Perfusion Systems Roller Head' => 'Verification of the Cardiopulmonary Bypass Perfusion Systems Roller Head',
  '8945 8960 Animal Room Temp Reports' => '8945 & 8960 Animal Room Temp Reports',
  '8945 8960 9055 Data Storage Temp Reports' => '8945, 8960, 9055 Data Storage Temp Reports',
  '8945 80 Freezer Store Room Refrigerator Temp Report' => '8945 -80 Freezer & Store Room Refrigerator Temp Report',
  '9055 Controlled Receiving Temp Report' => '9055 Controlled Receiving Temp Report',
  'Hood Monitoring Logs' => 'Hood Monitoring Logs',
  'Sample Prep Equipment Monitoring Logs' => 'Sample Prep Equipment Monitoring Logs',
  'Sample Prep RefrigeratorFreezer Logs' => 'Sample Prep Refrigerator/Freezer Logs',
  'Sample Prep Microbiological Mointoring Forms' => 'Sample Prep Microbiological Mointoring Forms',
  'Sterilizer Receipts' => 'Sterilizer Receipts',
  'Cagewash Tracking' => 'Cagewash Tracking',
  '8960 Feed Receipt' => '8960 Feed Receipt',
  '8960 Environmental Monitoring' => '8960 Environmental Monitoring',
  'Reagent Usage Log' => 'Reagent Usage Log',
  'Solution Preparation Log' => 'Solution Preparation Log',
  'Pathology Refrigerator Freezer Temperature Log' => 'Pathology Refrigerator & Freezer Temperature Log',
  'Histology Oven Temperature' => 'Histology Oven Temperature',
  'Cryostat Temperature Log' => 'Cryostat Temperature Log',
  'Hazardous Waste Inspection Log' => 'Hazardous Waste Inspection Log',
  'Tissue Processor Log' => 'Tissue Processor Log',
  'SpeedVac Use Log' => 'SpeedVac Use Log',
  'Sterilizer Cycle Log' => 'Sterilizer Cycle Log',
  'Depyrogenator Cycle Log' => 'Depyrogenator Cycle Log',
  'Dishwasher Cycle Log' => 'Dishwasher Cycle Log',
  'Depyrogenator Cycle Printouts' => 'Depyrogenator Cycle Printouts',
  'BalanceScale Logs' => 'Balance/Scale Logs',
  'CaliperGage Block Logs' => 'Caliper/Gage Block Logs',
  'FTIR Sample Analysis Form' => 'FTIR Sample Analysis Form',
  'Analytical Refrigerator and Freezer Temperature Log' => 'Analytical Refrigerator and Freezer Temperature Log',
  'Analytical Routine Equipment Maintenance Log' => 'Analytical Routine Equipment Maintenance Log',
  'Analtyical Facilty Equipment Log' => 'Analtyical Facilty Equipment Log',
  'Controlled Substances Log' => 'Controlled Substances Log',
  '8945 TempHumidity Logs feed room fridgefreezers Animal Care' => '8945 Temp/Humidity Logs (feed room & fridge/freezers - Animal Care)',
  '8945 Micro monitoring logs' => '8945 Micro monitoring logs',
  'Feed Receipt Logs' => 'Feed Receipt Logs',
  'MaintenanceTemp Records 8960' => 'Maintenance/Temp Records - 8960',
  'Hemavet' => 'Hemavet',
  '8945 Recovery Fridge' => '8945 Recovery Fridge',
  '8945 Ancillary Equipment Sanitation' => '8945 Ancillary Equipment Sanitation',
  '8960 Microbiological Monitoring' => '8960 Microbiological Monitoring',
  'Formalin Recycling Form' => 'Formalin Recycling Form',
  'Aldehyde Solution Neutralizing Form' => 'Aldehyde Solution Neutralizing Form',
  'Perfusion Tank Log' => 'Perfusion Tank Log',
  'Balance and Scale Validation Logs' => 'Balance and Scale Validation Logs',
  'Freezer Inventory Log' => 'Freezer Inventory Log',
  'Calibration Records 8960' => 'Calibration Records - 8960',
  'Biologicals for Autoclave 8960' => 'Biologicals for Autoclave - 8960',
  'CP Slides IVT' => 'CP Slides (IVT)',
  'IVT Equipment Monitoring Logs' => 'IVT Equipment Monitoring Logs',
  'IVT Montitoring System Temp Graphs' => 'IVT Montitoring System Temp Graphs',
  '8945 8960 RV Filter Change Logs' => '8945, 8960, RV Filter Change Logs',
  '8945 8960 9055 Generator Operation Checklist' => '8945, 8960, 9055 Generator Operation Checklist',
  'Animal Receipt Logs' => 'Animal Receipt Logs',
  '8960 Weekly Maintenance Shower Eyewash' => '8960 Weekly Maintenance (Shower & Eyewash)',
  'Analytical Services pH Calibration Log' => 'Analytical Services pH Calibration Log',
  'Analytical Services Solution Log Book Stock Solutions' => 'Analytical Services Solution Log Book- Stock Solutions',
  'Analytical Services Solution Log Book Stock Dilutions' => 'Analytical Services Solution Log Book- Stock Dilutions',
  'Analytical Services Solution Log Book Standard Curves' => 'Analytical Services Solution Log Book- Standard Curves',
  'Analytical Services Solution Log Book Buffers Mobile Phases Other' => 'Analytical Services Solution Log Book- Buffers, Mobile Phases, Other',
  'Analytical Services Solution Preparation' => 'Analytical Services Solution Preparation',
  'Analytical Services Standard Curve Spiking Solution Preparation' => 'Analytical Services Standard Curve Spiking Solution Preparation',
  'LAL Standard Curves' => 'LAL Standard Curves',
  'CascadeBFTII QC' => 'Cascade/BFTII QC',
  'Spectrophotometer Weekly Verification' => 'Spectrophotometer Weekly Verification',
  'Solution Preparation Documents' => 'Solution Preparation Documents',
  'Cell SplittingMedia Change Forms Mouse Lymphoma Balb L929 V79 THP Chok1 other cell lines' => 'Cell Splitting/Media Change Forms - Mouse Lymphoma, Balb, L929, V79, THP, Chok1, other cell lines',
  'Cell Plating Records' => 'Cell Plating Records',
  'Serum Heat Inactivation' => 'Serum Heat Inactivation',
  'ProtoCOL3 Colony Counter Validation Records' => 'ProtoCOL3 Colony Counter Validation Records',
  'Cell Revival Records' => 'Cell Revival Records',
  'Aliquot Creation Logs' => 'Aliquot Creation Logs',
  'Flow Cytometer CST maintenance' => 'Flow Cytometer CST / maintenance',
  'Moxi' => 'Moxi',
  'Blood Loop Controls' => 'Blood Loop Controls',
  'iSTAT Validation' => 'iSTAT Validation',
  'Scale Validation' => 'Scale Validation',
  'Glove Box' => 'Glove Box',
  'Hazardous Waste Log' => 'Hazardous Waste Log',
  'Osmometer Log' => 'Osmometer Log',
  'pH Log' => 'pH Log',
  'Equipment Routine Maintenance' => 'Equipment Routine Maintenance',
  '' => '',
);$app_list_strings['inventory_item_status_list'] = array (
  'Planned Inventory' => 'Planned Inventory',
  'Onsite Inventory' => 'Onsite Inventory',
  'Onsite Inventory Expired' => 'Onsite Inventory (Expired)',
  'Offsite Inventory' => 'Offsite Inventory',
  'Offsite Inventory Expired' => 'Offsite Inventory (Expired)',
  'Onsite Archived' => 'Onsite Archived',
  'Onsite Archived Expired' => 'Onsite Archived (Expired)',
  'Offsite Archived' => 'Offsite Archived',
  'Offsite Archived Expired' => 'Offsite Archived (Expired)',
  'Destroyed' => 'Destroyed',
  '' => '',
);$app_list_strings['related_to_list'] = array (
  'Internal Use' => 'Internal Use',
  'Multiple Test Systems' => 'Multiple Test Systems',
  'Sales' => 'Sales',
  'Single Test System' => 'Single Test System',
  'Work Product' => 'Work Product',
  '' => '',
);$app_list_strings['inventory_item_sterilization_list'] = array (
  'ETO Sterilized' => 'ETO Sterilized',
  'Gamma Sterilized' => 'Gamma Sterilized',
  'Non sterile' => 'Non-sterile',
  'Steam Sterilized' => 'Steam Sterilized',
  '' => '',
);$app_list_strings['timepoint_type_list'] = array (
  'Ad Hoc' => 'Ad Hoc',
  'None' => 'None',
  'Work Product Schedule' => 'Work Product Schedule',
  '' => '',
);$app_list_strings['processing_method_list'] = array (
  1 => '1',
  '' => '',
);$app_list_strings['expiration_test_article_list'] = array (
  'Known' => 'Known',
  'Pending' => 'Pending',
  'Unknown' => 'Unknown',
  '' => '',
);$app_list_strings['inventory_item_storage_condition_list'] = array (
  '30 45' => '30 - 45°C',
  'Ambient' => 'Ambient',
  'Room Temperature' => 'Room Temperature',
  'Refrigerate' => 'Refrigerate',
  'Frozen' => 'Frozen',
  'Ultra Frozen' => 'Ultra Frozen',
  '' => '',
);$app_list_strings['inventory_item_storage_medium_list'] = array (
  '4 Gluteraldehyde' => '4% Gluteraldehyde',
  '4 Paraformaldehyde' => '4% Paraformaldehyde',
  '10 Neutral Buffered Formalin' => '10% Neutral Buffered Formalin',
  'Davidsons Fixative' => 'Davidson\'s Fixative',
  'None' => 'None',
  'Normal Saline' => 'Normal Saline',
  'Water' => 'Water',
  '' => '',
);$app_list_strings['im_location_type_list'] = array (
  'Equipment' => 'Equipment',
  'Room' => 'Room',
  'RoomCabinet' => 'Room/Cabinet',
  'RoomShelf' => 'Room/Shelf',
  'Cabinet' => 'Cabinet',
  'Shelf' => 'Shelf',
  '' => '',
);$app_list_strings['location_list'] = array (
  780 => '780 86th Ave.',
  8945 => '8945 Evergreen Blvd.',
  8960 => '8960 Evergreen Blvd.',
  9055 => '9055 Evergreen Blvd.',
  '' => '',
);$app_list_strings['location_shelf_list'] = array (
  1 => '1',
  '' => '',
);$app_list_strings['location_cabinet_list'] = array (
  1 => '1',
  '' => '',
);$app_list_strings['inventory_item_owner_list'] = array (
  'APS Owned' => 'APS Owned',
  'APS Supplied' => 'APS Supplied',
  'Client' => 'Client',
  'Client Owned' => 'Client Owned',
  '' => '',
);