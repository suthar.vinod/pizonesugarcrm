<?php
 // created: 2022-03-15 07:23:37

$app_list_strings['integration_work_stream_list']=array (
  '' => '',
  'Accounting Finance Tax' => 'Accounting/Finance/Tax',
  'Commercial Plan' => 'Commercial Plan',
  'Communications Plans' => 'Communications Plans',
  'Cultural Integration Plan' => 'Cultural Integration Plan',
  'Human Resources' => 'Human Resources',
  'Integration Strategy Governance' => 'Integration Strategy & Governance',
  'IT' => 'IT',
  'Metrics for Success' => 'Metrics for Success',
  'Operations Biosafety Programs' => 'Operations: Biosafety Programs',
  'Operations Brooklyn Park' => 'Operations: Brooklyn Park',
  'Operations Coon Rapids' => 'Operations: Coon Rapids',
  'Operations Coon Rapids Build Out' => 'Operations: Coon Rapids Build Out',
  'Procurement Supply Chain' => 'Procurement / Supply Chain',
  'Quality' => 'Quality',
  'Sugar Software' => 'Sugar Software',
  'Building Buildouts' => 'Building Buildouts',
  'eQMS' => 'eQMS',
  'Financial Force ERP' => 'Financial Force ERP',
  'Labware' => 'Labware',
  'NAMSA 360 Single Portal' => 'NAMSA 360 (Single Portal)',
  'PSA  Financial Force' => 'PSA – Financial Force',
);