<?php
 // created: 2016-01-07 19:31:59

$app_list_strings['specialty_list']=array (
  'blank' => '',
  'cardiovascular' => 'Cardiovascular',
  'neurovascular' => 'Neurovascular',
  'orthopaedic' => 'Orthopaedic',
  'general pharmacology' => 'General Pharmacology',
  'oem' => 'OEM',
  'diabetes' => 'Diabetes',
  'urogenital' => 'Urogenital',
);