<?php
 // created: 2018-06-26 19:14:49

$app_list_strings['required_reports_list']=array (
  '' => '',
  'Animal Health' => 'Animal Health',
  'Gross Pathology' => 'Gross Pathology',
  'Gross and Histopathology' => 'Gross and Histopathology',
  'Analytical Report' => 'Analytical Report',
  'In_Life Report' => 'In-Life Report',
  'Final Report' => 'Final Report',
);