<?php
 // created: 2021-12-28 07:20:36

$app_list_strings['or_status_list']=array (
  '' => '',
  'Open' => 'Open',
  'Complete' => 'Complete',
  'Partially Submitted' => 'Partially Submitted',
  'Fully Submitted' => 'Fully Submitted',
);