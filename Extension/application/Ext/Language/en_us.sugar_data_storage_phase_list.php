<?php
 // created: 2020-05-13 17:25:01

$app_list_strings['data_storage_phase_list']=array (
  '' => '',
  'Carousel' => 'Carousel',
  'Reporting' => 'Reporting',
  'Complete and In Review' => 'Complete and In Review',
  'Scanned' => 'Scanned',
  'Archived' => 'Archived',
  'Shipped or Returned' => 'Shipped or Returned',
  'Destroyed' => 'Destroyed',
  'Study Not Performed' => 'Study Not Performed',
);