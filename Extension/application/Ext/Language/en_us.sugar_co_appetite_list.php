<?php
 // created: 2019-12-21 13:58:54

$app_list_strings['appetite_dd_list']=array (
  '' => '',
  'Normal' => 'Normal',
  'Mild Inappetent' => 'Mild Inappetent (~1/4-1/2 feed remaining)',
  'Moderate Inappetent' => 'Moderate Inappetent (~1/2-3/4 feed remaining)',
  'Anorexic' => 'Anorexic (all feed remaining)',
  'Not Assessed' => 'Not Assessed (Group Housed)',
);
