<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Prvi poziv',
  'Existing Customer' => 'Postojeći klijent',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Zaposleni',
  'Conference' => 'Konferencija',
  'Trade Show' => 'Sajam',
  'Web Site' => 'Web Sajt',
  'Email' => 'Bilo koji Email:',
  'Campaign' => 'Kampanja',
  'Support Portal User Registration' => 'Registracija portala za podršku',
  'Other' => 'Ostalo',
  'Referral' => 'Referral',
);