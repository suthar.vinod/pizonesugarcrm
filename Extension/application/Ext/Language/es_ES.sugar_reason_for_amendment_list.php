<?php
 // created: 2018-04-13 13:06:32

$app_list_strings['reason_for_amendment_list']=array (
  'Typographical Error' => 'Typographical Error',
  'Data Entry Error' => 'Data Entry Error',
  'Error in Final Report PDF' => 'Error in Final Report PDF',
  'Sponsor Request' => 'Sponsor Request',
  'Sponsor Request for Additional Analysis' => 'Sponsor Request for Additional Analysis',
  'Choose one' => 'Choose One',
  'Audit Finding' => 'Audit Finding',
);