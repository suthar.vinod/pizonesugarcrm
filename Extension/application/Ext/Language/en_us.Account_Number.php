<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['AN_Account_Number'] = 'Account Numbers';
$app_list_strings['moduleListSingular']['AN_Account_Number'] = 'Account Number';
$app_list_strings['account_type_list']['Ship to'] = 'Ship to';
$app_list_strings['account_type_list']['Order'] = 'Order';
$app_list_strings['account_type_list'][''] = '';
