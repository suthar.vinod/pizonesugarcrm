<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sq_AL.Controlled_Documents_QA_Review_Docs.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sq_AL.Controlled_Documents_QA_Review_Docs.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'marketingu';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'baza e njohurisë';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Shitjet';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'marketingu kolateral';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Broshurë e produkteve';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'Pyetje të shpeshta';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'aktive';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Skicë';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'Pyetje të shpeshta';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Skaduar';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Në shqyrtim';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Pezull';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'marketingu';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'baza e njohurisë';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Shitjet';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'marketingu kolateral';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Broshurë e produkteve';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'Pyetje të shpeshta';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'aktive';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Skicë';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'Pyetje të shpeshta';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Skaduar';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Në shqyrtim';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Pezull';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'marketingu';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'baza e njohurisë';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Shitjet';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'marketingu kolateral';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Broshurë e produkteve';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'Pyetje të shpeshta';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'aktive';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Skicë';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'Pyetje të shpeshta';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Skaduar';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Në shqyrtim';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Pezull';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';

?>
