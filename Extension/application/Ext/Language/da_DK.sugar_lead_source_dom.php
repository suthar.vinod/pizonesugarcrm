<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Uopfordret opkald',
  'Existing Customer' => 'Eksisterende kunde',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Medarbejder',
  'Conference' => 'Konference',
  'Trade Show' => 'Messe',
  'Web Site' => 'Websted',
  'Email' => 'E-mail',
  'Campaign' => 'Kampagne',
  'Support Portal User Registration' => 'Support portal bruger registrering',
  'Other' => 'Andet',
  'Referral' => 'Referral',
);