<?php
 // created: 2021-11-10 02:21:35

$app_list_strings['timepoint_type_list']=array (
  'Ad Hoc' => 'Ad Hoc',
  'None' => 'None',
  'Work Product Schedule' => 'Work Product Schedule',
  '' => '',
  'AcuteBaseline' => 'Acute/Baseline',
  'AcuteTermination' => 'Acute/Termination',
  'ChronicBaseline' => 'Chronic/Baseline',
  'ChronicTreatment' => 'Chronic/Treatment',
  'Defined' => 'Defined',
  'Follow up' => 'Follow-up',
  'Model CreationBaseline' => 'Model Creation/Baseline',
  'Receipt' => 'Receipt',
  'Termination' => 'Termination',
  'Vet Order' => 'Vet Order',
);