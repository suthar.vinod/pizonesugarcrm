<?php
 // created: 2016-04-26 15:20:28

$app_list_strings['Med_Device_Category_and_Contact']=array (
  'Surface device mucosal membrane' => 'Surface device - Mucosal Membrane',
  'Surface device breached or compromised surface' => 'Surface device - Breach/Compromised Surface',
  'Ext communicating device blood path indirect' => 'Ext communicating device - Blood path, indirect',
  'Ext communicating device tissue bone dentin' => 'Ext communicating device - Tissue/Bone/Dentin',
  'Ext communicating device circulating blood' => 'Ext communicating device - Circulating Blood',
  'Implant device tissue bone' => 'Implant device - Tissue/bone',
  'Implant device blood' => 'Implant device - Blood',
);