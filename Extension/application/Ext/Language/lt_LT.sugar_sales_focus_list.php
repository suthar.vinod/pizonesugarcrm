<?php
 // created: 2018-01-17 22:50:38

$app_list_strings['sales_focus_list']=array (
  '' => '',
  'Full APS Program' => 'Full APS Program',
  'Biocompatibility' => 'Biocompatibility',
  'Analytical' => 'Analytical',
  'Biocompatibility_Analytical' => 'Biocompatibility/Analytical',
  'Bioskills' => 'Bioskills',
  'ISR' => 'ISR',
  'Pharmacology' => 'Pharmacology',
  'Regulatory' => 'Regulatory',
  'Toxicology' => 'Toxicology',
);