<?php
 // created: 2017-03-14 15:19:18

$app_list_strings['primary_aps_operator_list']=array (
  'Michael Jorgenson' => 'Michael Jorgenson',
  'Allan Camrud' => 'Allan Camrud',
  'Mark Beckel' => 'Mark Beckel',
  'Tyler LaMont' => 'Tyler LaMont',
  'Joseph Vislisel' => 'Joseph Vislisel',
  'Christina Gross' => 'Christina Gross',
  'Elizabeth Carter' => 'Elizabeth Carter',
  'Chris Lafean' => 'Chris Lafean',
  'Choose One' => 'Choose One',
);