<?php
 // created: 2021-11-10 01:01:23

$app_list_strings['inventory_item_status_list']=array (
  'Planned Inventory' => 'Planned Inventory',
  'Onsite Inventory' => 'Onsite Inventory',
  'Onsite Inventory Expired' => 'Onsite Inventory (Expired)',
  'Offsite Inventory' => 'Offsite Inventory',
  'Offsite Inventory Expired' => 'Offsite Inventory (Expired)',
  'Onsite Archived' => 'Onsite Archived',
  'Onsite Archived Expired' => 'Onsite Archived (Expired)',
  'Offsite Archived' => 'Offsite Archived',
  'Offsite Archived Expired' => 'Offsite Archived (Expired)',
  '' => '',
  'Discarded' => 'Discarded',
  'Exhausted' => 'Exhausted',
  'Invoiced' => 'Invoiced',
  'Obsolete' => 'Obsolete',
);