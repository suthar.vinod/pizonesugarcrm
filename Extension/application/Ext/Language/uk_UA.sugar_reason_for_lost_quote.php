<?php
 // created: 2018-10-23 19:11:45

$app_list_strings['reason_for_lost_quote']=array (
  'Choose One' => 'Choose One...',
  'lab proximity to company' => 'Lab Proximity to Company',
  'lower quote from competitive lab' => 'Lower Quote from Competitive Lab',
  'scheduling issue' => 'Scheduling Issue',
  'experience' => 'Staff Experience',
  'lack of required equipment' => 'Lack of Required Equipment',
  'report timelines' => 'Report Timelines',
  'capacity' => 'Capacity',
  'lack of timely response' => 'Lack of Timely Response',
  'no response from client' => 'No Response from Client',
  'Chose another lab' => 'Selected a Different Lab',
  'WL' => 'WL',
);