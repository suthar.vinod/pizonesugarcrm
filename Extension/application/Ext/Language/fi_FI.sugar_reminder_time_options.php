<?php
 // created: 2017-06-16 19:19:43

$app_list_strings['reminder_time_options']=array (
  -1 => 'Ei mitään',
  60 => 'Minuutti ennen',
  300 => '5 minuuttia ennen',
  600 => '10 minuuttia ennen',
  900 => '15 minuuttia ennen',
  1800 => '30 minuuttia ennen',
  3600 => 'Tunti ennen',
  7200 => '2 tuntia ennen',
  10800 => '3 tuntia ennen',
  18000 => '5 tuntia ennen',
  86400 => 'Päivä ennen',
  604800 => '7 days prior',
);