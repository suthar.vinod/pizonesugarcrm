<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['an01_activity_notes_category_dom']['Marketing'] = 'Mārketings';
$app_list_strings['an01_activity_notes_category_dom']['Knowledege Base'] = 'Zināšanu bāze';
$app_list_strings['an01_activity_notes_category_dom']['Sales'] = 'Pārdošana';
$app_list_strings['an01_activity_notes_category_dom'][''] = '';
$app_list_strings['an01_activity_notes_subcategory_dom']['Marketing Collateral'] = 'Mārketinga materiāli';
$app_list_strings['an01_activity_notes_subcategory_dom']['Product Brochures'] = 'Produktu brošūras';
$app_list_strings['an01_activity_notes_subcategory_dom']['FAQ'] = 'BUJ';
$app_list_strings['an01_activity_notes_subcategory_dom'][''] = '';
$app_list_strings['an01_activity_notes_status_dom']['Active'] = 'Aktīvs';
$app_list_strings['an01_activity_notes_status_dom']['Draft'] = 'Uzmetums';
$app_list_strings['an01_activity_notes_status_dom']['FAQ'] = 'BUJ';
$app_list_strings['an01_activity_notes_status_dom']['Expired'] = 'Izbeidzies';
$app_list_strings['an01_activity_notes_status_dom']['Under Review'] = 'Caurskatē';
$app_list_strings['an01_activity_notes_status_dom']['Pending'] = 'Nepabeigta';
$app_list_strings['moduleList']['AN01_Activity_Notes'] = 'Activity Notes';
$app_list_strings['moduleListSingular']['AN01_Activity_Notes'] = 'Activity Note';
