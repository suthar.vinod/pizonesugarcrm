<?php
 // created: 2017-06-16 19:19:43

$app_list_strings['reminder_time_options']=array (
  -1 => 'Ükski',
  60 => '1 minut enne',
  300 => '5 minutit enne',
  600 => '10 minutit enne',
  900 => '15 minutit enne',
  1800 => '30 minutit enne',
  3600 => '1 tund enne',
  7200 => '2 tundi enne',
  10800 => '3 tundi enne',
  18000 => '5 tundi enne',
  86400 => '1 päev enne',
  604800 => '7 days prior',
);