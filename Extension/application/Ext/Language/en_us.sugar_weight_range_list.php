<?php
 // created: 2018-05-21 19:52:21

$app_list_strings['weight_range_list']=array (
  '' => '',
  '0_10' => '0 - 10 kg',
  '11_20' => '11 - 20 kg',
  '20_30' => '20 - 30 kg',
  '30_45' => '30 - 45 kg',
  '45_60' => '45 - 60 kg',
  '60_80' => '60 - 80 kg',
  '80_100' => '80 - 100 kg',
  '100_plus' => '100+ kg',
);