<?php
 // created: 2017-06-16 19:19:41

$app_list_strings['reminder_time_options']=array (
  -1 => 'Ingen',
  60 => '1 minutt Før',
  300 => '5 minutter Før',
  600 => '10 Minutter Før',
  900 => '15 Minutter Før',
  1800 => '30 Minutter Før',
  3600 => '1 Time Før',
  7200 => '2 Timer Før',
  10800 => '3 timer Før',
  18000 => '5 Timer Før',
  86400 => '1 Dag Før',
  604800 => '7 days prior',
);