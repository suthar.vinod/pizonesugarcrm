<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => '勧誘電話',
  'Existing Customer' => '既存顧客',
  'Self Generated' => 'Self Generated',
  'Employee' => '社員',
  'Conference' => 'カンファレンス',
  'Trade Show' => '展示会',
  'Web Site' => 'Webサイト',
  'Email' => 'Eメール',
  'Campaign' => 'キャンペーン',
  'Support Portal User Registration' => 'サポートポータルユーザの登録',
  'Other' => 'その他',
  'Referral' => 'Referral',
);