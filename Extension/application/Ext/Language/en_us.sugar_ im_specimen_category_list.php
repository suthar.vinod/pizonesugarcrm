<?php
 // created: 2020-09-30 09:34:11

$app_list_strings[' im_specimen_category_list']=array (
  '' => '',
  'Block' => 'Block',
  'Culture' => 'Culture',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Plasma' => 'Plasma',
  'Serum' => 'Serum',
  'Slide' => 'Slide',
  'Tissue' => 'Tissue',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
);