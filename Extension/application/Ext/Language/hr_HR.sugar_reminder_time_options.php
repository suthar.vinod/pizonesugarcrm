<?php
 // created: 2017-06-16 19:19:43

$app_list_strings['reminder_time_options']=array (
  -1 => 'Nema',
  60 => '1 minutu prije',
  300 => '5 minuta prije',
  600 => '10 minuta prije',
  900 => '15 minuta prije',
  1800 => '30 minuta prije',
  3600 => '1 sat prije',
  7200 => '2 sata prije',
  10800 => '3 sata prije',
  18000 => '5 sati prije',
  86400 => '1 dan prije',
  604800 => '7 days prior',
);