<?php
 // created: 2019-08-01 17:01:07

$app_list_strings['win_probability_list']=array (
  100 => '100%',
  25 => '25%',
  50 => '50%',
  75 => '75%',
  'zeropercent' => '0%',
  '' => '',
);