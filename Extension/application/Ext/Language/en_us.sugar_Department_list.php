<?php
 // created: 2016-03-07 20:26:18

$app_list_strings['Department_list']=array (
  'Select' => 'Select',
  'Analytical' => 'Analytical',
  'Biocompatibility' => 'Biocompatibility',
  'Gross Pathology' => 'Gross Pathology',
  'Histology' => 'Histology',
  'Interventional Surgical' => 'Interventional / Surgical',
  'Microbiology' => 'Microbiology',
  'Pharmacology' => 'Pharmacology',
  'Toxicology' => 'Toxicology',
  'Cytotoxicity' => 'Cytotoxicity',
  'Hemocompatibility' => 'Hemocompatibility',
  'Genotoxicity' => 'Genotoxicity',
  'Sensitization' => 'Sensitization',
  'Irritation' => 'Irritation',
  'Systemic Toxicity' => 'Systemic Toxicity',
  'Intramuscular' => 'Intramuscular',
  'Choose Department' => 'Choose Department',
);