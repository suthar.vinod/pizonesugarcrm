<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['U_Units'] = 'Units';
$app_list_strings['moduleListSingular']['U_Units'] = 'Unit';
$app_list_strings['unit_type_list']['Area'] = 'Area';
$app_list_strings['unit_type_list']['Length'] = 'Length';
$app_list_strings['unit_type_list']['Percent'] = 'Percent';
$app_list_strings['unit_type_list']['Time'] = 'Time';
$app_list_strings['unit_type_list']['Volume'] = 'Volume';
$app_list_strings['unit_type_list']['Volume per Time'] = 'Volume per Time';
$app_list_strings['unit_type_list']['Volume per Weight'] = 'Volume per Weight';
$app_list_strings['unit_type_list']['Volume per Weight per Time'] = 'Volume per Weight per Time';
$app_list_strings['unit_type_list']['Weight'] = 'Weight';
$app_list_strings['unit_type_list']['Weight per Time'] = 'Weight per Time';
$app_list_strings['unit_type_list']['Weight per Volume'] = 'Weight per Volume';
$app_list_strings['unit_type_list']['Weight per Volume per Time'] = 'Weight per Volume per Time';
$app_list_strings['unit_type_list']['Weight per Weight'] = 'Weight per Weight';
$app_list_strings['unit_type_list']['Weight per Weight per Time'] = 'Weight per Weight per Time';
$app_list_strings['unit_type_list'][''] = '';
