<?php
 // created: 2018-09-24 19:06:02

$app_list_strings['company_division_list']=array (
  'Biocompatibility' => 'Biocompatibility',
  'ISR' => 'ISR',
  'Pharmacology' => 'Pharmacology',
  'Toxicology' => 'Toxicology',
  'Analytical Services' => 'Analytical Services',
  'Pathology Services' => 'Pathology Services',
  'Bioskills' => 'Bioskills',
  'Choose Functional Area' => 'Choose Functional Area',
  'Regulatory' => 'Regulatory',
  '' => '',
  'Standard Biocompatibility' => 'Standard Biocompatibility',
);