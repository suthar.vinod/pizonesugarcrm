<?php
 // created: 2020-12-22 14:35:20

$app_list_strings['high_level_root_cause_action_list']=array (
  '' => '',
  'Isolated Process Error' => 'Isolated Process',
  'Isolated Human Error' => 'Isolated Human',
  'Repeat Process Error' => 'Repeat Process',
  'Repeat Human Error' => 'Repeat Human',
);