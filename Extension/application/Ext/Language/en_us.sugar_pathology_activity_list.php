<?php
 // created: 2017-06-09 21:10:54

$app_list_strings['pathology_activity_list']=array (
  '' => '',
  'Necropsy' => 'Necropsy',
  'Faxitron' => 'Faxitron',
  'Tissue Receipt' => 'Tissue Receipt',
  'Tissue Trimming' => 'Tissue Trimming',
  'Tissue Shipping' => 'Tissue Shipping',
  'Paraffin Embedding' => 'Paraffin Embedding',
  'Plastic Embedding' => 'Plastic Embedding',
  'Slide Completion' => 'Slide Completion',
  'Slide Shipping' => 'Slide Shipping',
  'SEM Imaging' => 'SEM Imaging',
  'Histomorphometry' => 'Histomorphometry',
  'Slide Review' => 'Slide Review',
  'Gross Necropsy Report Writing' => 'Gross Necropsy Report Writing',
  'Pathology Report Writing' => 'Pathology Report Writing',
  'Out of Office' => 'Out of Office',
);