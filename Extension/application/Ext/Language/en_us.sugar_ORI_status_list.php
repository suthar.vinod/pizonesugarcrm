<?php
 // created: 2021-09-02 17:04:32

$app_list_strings['ORI_status_list']=array (
  '' => '',
  'Backordered' => 'Backordered',
  'Inventory' => 'Fully Received',
  'Ordered' => 'Ordered',
  'Partially Received' => 'Partially Received',
  'Requested' => 'Requested',
);