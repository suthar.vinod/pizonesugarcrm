<?php
 // created: 2022-02-03 09:59:20

$app_list_strings['task_type_list']=array (
  '' => '',
  'Document Creation' => 'Document Creation',
  'Document Review' => 'Document Review',
  'Financial' => 'Financial',
  'Histopathology' => 'Histopathology',
  'Invoice' => 'Invoice',
  'Lead Follow Up' => 'Lead Follow Up',
  'Maintenance Request' => 'Maintenance Request',
  'Procedure Scheduling' => 'Procedure Scheduling',
  'Quote Follow Up' => 'Quote Follow Up',
  'Quote Review' => 'Quote Review',
  'Schedule Matrix Review' => 'Schedule Matrix Review',
  'Send Necropsy Findings' => 'Send Necropsy Findings',
  'Sponsor Communication' => 'Sponsor Communication',
  'Sponsor Follow Up' => 'Sponsor Follow Up',
  'Sponsor Quote Review' => 'Sponsor Quote Review',
  'Tradeshow' => 'Tradeshow',
  'Vet Check' => 'Vet Check',
  'Work Product' => 'Work Product',
  'Custom' => 'Custom',
  'Standard' => 'Custom',
);