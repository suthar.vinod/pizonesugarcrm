<?php
 // created: 2021-11-18 10:19:23

$app_list_strings['aaalac_and_usda_exemptions_list']=array (
  '' => '',
  'Conscious Restraint' => 'Conscious Restraint',
  'Exercise Restriction dogs' => 'Exercise Restriction (dogs)',
  'FoodWater Restriction' => 'Food/Water Restriction',
  'Multiple Major Surgeries 1protocol' => 'Multiple Major Surgeries (1/protocol)',
  'Multiple Survival Surgeries' => 'Multiple Survival Surgeries',
  'Neuromuscular Blocker' => 'Neuromuscular Blocker',
  'Personnel Training' => 'Personnel Training',
  'Survival Surgery' => 'Survival Surgery',
  'Toxicity Study' => 'Toxicity Study',
);