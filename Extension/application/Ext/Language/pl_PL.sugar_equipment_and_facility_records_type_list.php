<?php
 // created: 2020-06-16 09:04:45

$app_list_strings['equipment_and_facility_records_type_list']=array (
  '' => '',
  'Calibration' => 'Calibration',
  'Chain of Custody' => 'Chain of Custody',
  'Initial In Service' => 'Initial In-Service',
  'Inspection' => 'Inspection',
  'Preventative Maintenance' => 'Preventative Maintenance',
  'Repair' => 'Repair',
  'SoftwareFirmware Change' => 'Software/Firmware Change',
  'Cleaning' => 'Cleaning',
  'Equipment Qualification' => 'Equipment Qualification',
  'Service Contract' => 'Service Contract',
  'Verification' => 'Verification',
  'Part 11 Gap Analysis' => 'Part 11 Gap Analysis',
  'Routine Maintenance' => 'Routine Maintenance',
  'Initial In Service 2' => 'Initial in Service',
);