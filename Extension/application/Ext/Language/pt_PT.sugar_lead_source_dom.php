<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Chamada Espontânea',
  'Existing Customer' => 'Cliente Existente',
  'Self Generated' => 'Gerado pelo próprio',
  'Employee' => 'Colaborador',
  'Conference' => 'Conferência',
  'Trade Show' => 'Feira/Evento',
  'Web Site' => 'Site de Internet',
  'Email' => 'Outro E-mail:',
  'Campaign' => 'Campanha',
  'Support Portal User Registration' => 'Registo de Utilizador de Suporte ao Portal',
  'Other' => 'Outro:',
  'Referral' => 'Referral',
);