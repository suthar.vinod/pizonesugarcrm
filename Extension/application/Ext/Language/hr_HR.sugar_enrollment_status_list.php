<?php
 // created: 2020-12-15 12:39:02

$app_list_strings['enrollment_status_list']=array (
  'Choose One' => 'Choose One',
  'Assigned Backup' => 'Assigned Backup',
  'Assigned On Study' => 'Assigned On Study',
  'Non naive Stock' => 'Non-naive Stock',
  'Not Used' => 'Not Used',
  'On Study' => 'On Study',
  'On Study Backup' => 'On Study Backup',
  'On Study Transferred' => 'On Study Transferred',
  'Enrolled Not Used' => 'Enrolled – Not Used',
  '' => '',
);