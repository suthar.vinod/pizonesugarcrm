<?php
 // created: 2018-05-08 19:51:27

$app_list_strings['sales_activity_source_list']=array (
  '' => '',
  'Current Sponsor' => 'Current Sponsor',
  'In Coming Cold Call' => 'In Coming Cold Call',
  'Trade Show' => 'Trade Show',
  'Referral' => 'Referral',
  'Website' => 'APS Website',
  'Third Party Website' => 'Third Party Website',
  'Surpass' => 'Surpass',
  'Cold Call by APS' => 'Cold Call by APS',
  'Advertisement' => 'Advertisement',
);