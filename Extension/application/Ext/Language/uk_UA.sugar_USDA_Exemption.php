<?php
 // created: 2019-08-08 11:45:52

$app_list_strings['USDA_Exemption']=array (
  'Conscious_Restraint' => 'Conscious Restraint',
  'Exercise_Restriction' => 'Exercise Restriction (dogs)',
  'Food_Water_Restriction' => 'Food/Water Restriction',
  'Multiple Major_1' => 'Multiple Major (1 protocol)',
  'Neuromuscular_Blocker' => 'Neuromuscular Blocker',
  'Training' => 'Training',
  'Choose_One' => 'Choose One',
  'Toxicity_Study' => 'Toxicity Study',
  'None' => 'None',
);