<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Kalt Akquise',
  'Existing Customer' => 'Bestehender Kunde',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Mitarbeiter',
  'Conference' => 'Konferenz',
  'Trade Show' => 'Messe',
  'Web Site' => 'Web Seite',
  'Email' => 'Irgendeine E-Mail',
  'Campaign' => 'Kampagne',
  'Support Portal User Registration' => 'Support Portal Benutzer Registration',
  'Other' => 'Andere',
  'Referral' => 'Referral',
);