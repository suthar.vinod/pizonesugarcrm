<?php
 // created: 2022-02-08 07:41:30

$app_list_strings['ot_category_list']=array (
  '' => '',
  'Cardiopulmonary Bypass' => 'Cardiopulmonary Bypass',
  'CatheterWire' => 'Catheter/Wire',
  'Circulatory Assist' => 'Circulatory Assist',
  'Electrophysiology' => 'Electrophysiology',
  'Embolization' => 'Embolization',
  'Endoscopy' => 'Endoscopy',
  'General Cardiac' => 'General Cardiac',
  'Hemostasis' => 'Hemostasis',
  'Integumentary' => 'Integumentary',
  'Leads' => 'Leads',
  'Muscoskeletal' => 'Muscoskeletal',
  'Non Cardiac Ablation' => 'Non Cardiac Ablation',
  'Other' => 'Other',
  'StentsBalloons' => 'Stents/Balloons',
  'Transplant' => 'Transplant',
  'Valve' => 'Valve',
  'Vascular Anastomosis' => 'Vascular Anastomosis',
);