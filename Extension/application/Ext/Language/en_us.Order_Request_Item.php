<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['ORI_Order_Request_Item'] = 'Order Request Items';
$app_list_strings['moduleListSingular']['ORI_Order_Request_Item'] = 'Order Request Item';
$app_list_strings['inventory_item_owner_list']['APS'] = 'APS';
$app_list_strings['inventory_item_owner_list']['APS Supplied Client Owned'] = 'APS Supplied, Client Owned';
$app_list_strings['inventory_item_owner_list'][''] = '';
$app_list_strings['ORI_type_list']['In System Vendor Non Specific'] = 'In System, Vendor Non-Specific';
$app_list_strings['ORI_type_list']['In System Vendor Specific'] = 'In System, Vendor Specific';
$app_list_strings['ORI_type_list']['Not In System Vendor Non Specific'] = 'Not In System, Vendor Non-Specific';
$app_list_strings['ORI_type_list']['Not In System Vendor Specific'] = 'Not In System, Vendor Specific';
$app_list_strings['ORI_type_list'][''] = '';
$app_list_strings['product_category_list']['Balloon Catheter'] = 'Balloon Catheter';
$app_list_strings['product_category_list']['Cannula'] = 'Cannula';
$app_list_strings['product_category_list']['Catheter'] = 'Catheter';
$app_list_strings['product_category_list']['Chemical'] = 'Chemical';
$app_list_strings['product_category_list']['Cleaning Agent'] = 'Cleaning Agent';
$app_list_strings['product_category_list']['Contrast'] = 'Contrast';
$app_list_strings['product_category_list']['Drug'] = 'Drug';
$app_list_strings['product_category_list']['Graft'] = 'Graft';
$app_list_strings['product_category_list']['Inflation Device'] = 'Inflation Device';
$app_list_strings['product_category_list']['Reagent'] = 'Reagent';
$app_list_strings['product_category_list']['Sheath'] = 'Sheath';
$app_list_strings['product_category_list']['Stent'] = 'Stent';
$app_list_strings['product_category_list']['Suture'] = 'Suture';
$app_list_strings['product_category_list']['Wire'] = 'Wire';
$app_list_strings['product_category_list'][''] = '';
$app_list_strings['purchase_unit_list']['Each'] = 'Each';
$app_list_strings['purchase_unit_list']['Bottle'] = 'Bottle';
$app_list_strings['purchase_unit_list']['Box'] = 'Box';
$app_list_strings['purchase_unit_list']['Case'] = 'Case';
$app_list_strings['purchase_unit_list'][''] = '';
$app_list_strings['ORI_status_list']['Backordered'] = 'Backordered';
$app_list_strings['ORI_status_list']['Inventory'] = 'Inventory';
$app_list_strings['ORI_status_list']['Ordered'] = 'Ordered';
$app_list_strings['ORI_status_list']['Received'] = 'Received';
$app_list_strings['ORI_status_list']['Requested'] = 'Requested';
$app_list_strings['ORI_status_list'][''] = '';
$app_list_strings['poi_ori_related_to_list']['Internal Use'] = 'Internal Use';
$app_list_strings['poi_ori_related_to_list']['Sales'] = 'Sales';
$app_list_strings['poi_ori_related_to_list']['Work Product'] = 'Work Product';
$app_list_strings['poi_ori_related_to_list'][''] = '';
$app_list_strings['POI_owner_list']['APS Owned'] = 'APS Owned';
$app_list_strings['POI_owner_list']['APS Supplied Client Owned'] = 'APS Supplied, Client Owned';
$app_list_strings['POI_owner_list'][''] = '';
$app_list_strings['pro_subtype_list']['Access Catheter'] = 'Access Catheter';
$app_list_strings['pro_subtype_list']['Angioplasty Balloon'] = 'Angioplasty Balloon';
$app_list_strings['pro_subtype_list']['Aortic Root Cannula'] = 'Aortic Root Cannula';
$app_list_strings['pro_subtype_list']['Arterial Cannula'] = 'Arterial Cannula';
$app_list_strings['pro_subtype_list']['Bare Metal Stent'] = 'Bare Metal Stent';
$app_list_strings['pro_subtype_list']['Barium Contrast'] = 'Barium Contrast';
$app_list_strings['pro_subtype_list']['Central Venous Catheter'] = 'Central Venous Catheter';
$app_list_strings['pro_subtype_list']['Coated'] = 'Coated';
$app_list_strings['pro_subtype_list']['Cutting Balloon'] = 'Cutting Balloon';
$app_list_strings['pro_subtype_list']['Diagnostic Catheter'] = 'Diagnostic Catheter';
$app_list_strings['pro_subtype_list']['Drug Coated Balloon'] = 'Drug Coated Balloon';
$app_list_strings['pro_subtype_list']['Drug Coated Stent'] = 'Drug Coated Stent';
$app_list_strings['pro_subtype_list']['EP Catheter'] = 'EP Catheter';
$app_list_strings['pro_subtype_list']['ePTFE'] = 'ePTFE';
$app_list_strings['pro_subtype_list']['Ethibond'] = 'Ethibond';
$app_list_strings['pro_subtype_list']['Ethilon'] = 'Ethilon';
$app_list_strings['pro_subtype_list']['Exchange Wire'] = 'Exchange Wire';
$app_list_strings['pro_subtype_list']['Extension Catheter'] = 'Extension Catheter';
$app_list_strings['pro_subtype_list']['Foley Catheter'] = 'Foley Catheter';
$app_list_strings['pro_subtype_list']['Guide Catheter'] = 'Guide Catheter';
$app_list_strings['pro_subtype_list']['Guide Wire'] = 'Guide Wire';
$app_list_strings['pro_subtype_list']['High Pressure'] = 'High Pressure';
$app_list_strings['pro_subtype_list']['Injectable'] = 'Injectable';
$app_list_strings['pro_subtype_list']['Introducer Sheath'] = 'Introducer Sheath';
$app_list_strings['pro_subtype_list']['Iodinated Contrast'] = 'Iodinated Contrast';
$app_list_strings['pro_subtype_list']['IV Catheter'] = 'IV Catheter';
$app_list_strings['pro_subtype_list']['Knitted'] = 'Knitted';
$app_list_strings['pro_subtype_list']['Mapping Catheter'] = 'Mapping Catheter';
$app_list_strings['pro_subtype_list']['Marking Wire'] = 'Marking Wire';
$app_list_strings['pro_subtype_list']['Micro Catheter'] = 'Micro Catheter';
$app_list_strings['pro_subtype_list']['Mila Catheter'] = 'Mila Catheter';
$app_list_strings['pro_subtype_list']['Monocryl'] = 'Monocryl';
$app_list_strings['pro_subtype_list']['Oral'] = 'Oral';
$app_list_strings['pro_subtype_list']['OTW over the wire'] = 'OTW (over the wire)';
$app_list_strings['pro_subtype_list']['Patches'] = 'Patches';
$app_list_strings['pro_subtype_list']['PDS'] = 'PDS';
$app_list_strings['pro_subtype_list']['Pediatric Cannula'] = 'Pediatric Cannula';
$app_list_strings['pro_subtype_list']['Pledget'] = 'Pledget';
$app_list_strings['pro_subtype_list']['POBA Balloon'] = 'POBA Balloon';
$app_list_strings['pro_subtype_list']['Pressure Catheter'] = 'Pressure Catheter';
$app_list_strings['pro_subtype_list']['Prolene'] = 'Prolene';
$app_list_strings['pro_subtype_list']['PTA Balloon'] = 'PTA Balloon';
$app_list_strings['pro_subtype_list']['Silk'] = 'Silk';
$app_list_strings['pro_subtype_list']['Steerable Sheath'] = 'Steerable Sheath';
$app_list_strings['pro_subtype_list']['Support Catheter'] = 'Support Catheter';
$app_list_strings['pro_subtype_list']['Support Wire'] = 'Support Wire';
$app_list_strings['pro_subtype_list']['Ti Cron'] = 'Ti-Cron';
$app_list_strings['pro_subtype_list']['Transseptal Catheter'] = 'Transseptal Catheter';
$app_list_strings['pro_subtype_list']['Venous Cannula'] = 'Venous Cannula';
$app_list_strings['pro_subtype_list']['Vessel Cannula'] = 'Vessel Cannula';
$app_list_strings['pro_subtype_list']['Vicryl'] = 'Vicryl';
$app_list_strings['pro_subtype_list']['Woven'] = 'Woven';
$app_list_strings['pro_subtype_list'][''] = '';
$app_list_strings['inventory_item_storage_condition_list']['30 45'] = '30 - 45°C';
$app_list_strings['inventory_item_storage_condition_list']['Ambient'] = 'Ambient';
$app_list_strings['inventory_item_storage_condition_list']['Room Temperature'] = 'Room Temperature';
$app_list_strings['inventory_item_storage_condition_list']['Refrigerate'] = 'Refrigerate';
$app_list_strings['inventory_item_storage_condition_list']['Frozen'] = 'Frozen';
$app_list_strings['inventory_item_storage_condition_list']['Ultra Frozen'] = 'Ultra Frozen';
$app_list_strings['inventory_item_storage_condition_list'][''] = '';
