<?php
 // created: 2019-03-13 11:59:45

$app_list_strings['meeting_status_dom']=array (
  'Planned' => 'スケジュール済み',
  'Held' => '保留中',
  'Not Held' => 'キャンセル済み',
  'Completed' => 'Completed',
  'Obsolete' => 'Obsolete',
);