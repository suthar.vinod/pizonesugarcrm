<?php 
$app_list_strings['category_id_list'] = array (
  '' => '',
  'Material Transfer Agreement' => 'Material Transfer Agreement',
  'MSA' => 'MSA',
  'MSA Amendment' => 'MSA Amendment',
  'NDA_CDA' => 'NDA / CDA',
  'Qualification' => 'Qualification',
  'Quality Agreement' => 'Quality Agreement',
  'Supplemental' => 'Supplemental',
  'Supplier Survey' => 'Supplier Survey',
);