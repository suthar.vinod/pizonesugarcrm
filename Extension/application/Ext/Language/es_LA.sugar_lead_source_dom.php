<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Llamada en Frío',
  'Existing Customer' => 'Cliente Existente',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Empleado',
  'Conference' => 'Conferencia',
  'Trade Show' => 'Exposición',
  'Web Site' => 'Sitio Web',
  'Email' => 'Correo Electrónico',
  'Campaign' => 'Campaña',
  'Support Portal User Registration' => 'Registro de Usuario del Portal de Soporte',
  'Other' => 'Otro',
  'Referral' => 'Referral',
);