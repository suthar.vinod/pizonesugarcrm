<?php
 // created: 2017-06-16 19:19:41

$app_list_strings['reminder_time_options']=array (
  -1 => 'Κανένα',
  60 => '1 λεπτό πριν',
  300 => '5 λεπτά πριν',
  600 => '10 λεπτά πριν',
  900 => '15 λεπτά πριν',
  1800 => '30 λεπτά πριν',
  3600 => '1 ώρα πριν',
  7200 => '2 ώρες πριν',
  10800 => '3 ώρες πριν',
  18000 => '5 ώρες πριν',
  86400 => '1 ημέρα πριν',
  604800 => '7 days prior',
);