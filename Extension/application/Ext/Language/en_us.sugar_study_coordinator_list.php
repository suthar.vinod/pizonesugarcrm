<?php
 // created: 2019-04-11 10:06:32

$app_list_strings['study_coordinator_list']=array (
  'Choose One' => 'Choose One',
  'Amy Puetz' => 'Amy Puetz',
  'Anton Crane' => 'Anton Crane',
  'Ben Vos' => 'Ben Vos',
  'Dan Kroeninger' => 'Dan Kroeninger',
  'DeeAnn Deiss' => 'DeeAnn Deiss',
  'Emily Visedo' => 'Emily Visedo',
  'Emma Reiss' => 'Emma Reiss',
  'Emma Squires Sperling' => 'Emma Squires-Sperling',
  'Erica VanReeth' => 'Erica VanReeth',
  'Hannah Schmidt' => 'Hannah Schmidt',
  'Kailyn Singh' => 'Kailyn Singh',
  'Katelyn Mortensen' => 'Katelyn Mortensen',
  'Kerry Trotter' => 'Kerry Trotter',
  'Liz Clark' => 'Liz Clark',
  'Liz Roby' => 'Liz Roby',
  'Steven Mamer' => 'Steven Mamer',
  'Tom Van Valkenburg' => 'Tom Van Valkenburg',
);