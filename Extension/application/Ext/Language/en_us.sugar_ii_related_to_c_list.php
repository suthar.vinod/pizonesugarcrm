<?php
 // created: 2022-02-24 08:23:25

$app_list_strings['ii_related_to_c_list']=array (
  '' => '',
  'Internal Use' => 'Internal Use',
  'Multiple Test Systems' => 'Test System(s)',
  'Order Request Item' => 'Order Request Item',
  'Purchase Order Item' => 'Purchase Order Item',
  'Sales' => 'Sales',
  'Work Product' => 'Work Product',
  'Single Test System' => 'Single Test System (Obsolete)',
);