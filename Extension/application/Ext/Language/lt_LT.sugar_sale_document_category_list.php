<?php
 // created: 2022-03-31 11:46:56

$app_list_strings['sale_document_category_list']=array (
  '' => '',
  'Study Design Document' => 'Study Design',
  'Study Quote' => 'Quote',
  'Quote Revision' => 'Quote Revision',
  'Signed Quote' => 'Signed Quote',
  'Signed Quote Revision' => 'Signed Quote Revision',
  'PO' => 'PO',
  'Change Order' => 'Change Order',
  'Signed Change Order' => 'Signed Change Order',
  'SOW' => 'SOW',
  'Signed SOW' => 'Signed SOW',
  'Interinstitutional Assurance' => 'Interinstitutional Assurance',
  'Pricelist Submission' => 'Pricelist Submission',
  'Publication' => 'Publication',
  'Subcontractor Quote' => 'Subcontractor Quote',
);