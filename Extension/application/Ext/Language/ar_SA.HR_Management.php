<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['hr01_resumes_category_dom']['Marketing'] = 'التسويق';
$app_list_strings['hr01_resumes_category_dom']['Knowledege Base'] = 'قاعدة المعارف';
$app_list_strings['hr01_resumes_category_dom']['Sales'] = 'المبيعات';
$app_list_strings['hr01_resumes_category_dom'][''] = '';
$app_list_strings['hr01_resumes_subcategory_dom']['Marketing Collateral'] = 'وسائط التسويق';
$app_list_strings['hr01_resumes_subcategory_dom']['Product Brochures'] = 'منشورات المنتج';
$app_list_strings['hr01_resumes_subcategory_dom']['FAQ'] = 'الأسئلة المتداولة';
$app_list_strings['hr01_resumes_subcategory_dom'][''] = '';
$app_list_strings['hr01_resumes_status_dom']['Active'] = 'نشط';
$app_list_strings['hr01_resumes_status_dom']['Draft'] = 'المسودة';
$app_list_strings['hr01_resumes_status_dom']['FAQ'] = 'الأسئلة المتداولة';
$app_list_strings['hr01_resumes_status_dom']['Expired'] = 'منتهي الصلاحية';
$app_list_strings['hr01_resumes_status_dom']['Under Review'] = 'قيد المراجعة';
$app_list_strings['hr01_resumes_status_dom']['Pending'] = 'معلق';
$app_list_strings['moduleList']['HR01_Resumes'] = 'Resumes';
$app_list_strings['moduleListSingular']['HR01_Resumes'] = 'Resume';
