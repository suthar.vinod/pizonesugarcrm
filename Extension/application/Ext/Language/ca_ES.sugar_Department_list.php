<?php
 // created: 2016-03-07 20:26:18

$app_list_strings['Department_list']=array (
  'Biocompatibility' => 'Biocompatibility',
  'Interventional Surgical' => 'Interventional / Surgical',
  'Pharmacology' => 'Pharmacology',
  'Analytical' => 'Analytical',
  'Toxicology' => 'Toxicology',
  'Microbiology' => 'Microbiology',
  'Gross Pathology' => 'Gross Pathology',
  'Histology' => 'Histology',
  'Select' => 'Select',
  'Cytotoxicity' => 'Cytotoxicity',
  'Hemocompatibility' => 'Hemocompatibility',
  'Genotoxicity' => 'Genotoxicity',
  'Sensitization' => 'Sensitization',
  'Irritation' => 'Irritation',
  'Systemic Toxicity' => 'Systemic Toxicity',
  'Intramuscular' => 'Intramuscular',
  'Choose Department' => 'Choose Department',
);