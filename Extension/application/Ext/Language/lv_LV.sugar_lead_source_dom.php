<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Darījuma zvans',
  'Existing Customer' => 'Esošs klients',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Darbinieks',
  'Conference' => 'Konference',
  'Trade Show' => 'Nozares izstāde',
  'Web Site' => 'Mājas lapa',
  'Email' => 'Jebkāds e-pasts',
  'Campaign' => 'Kampaņa',
  'Support Portal User Registration' => 'Atbalsta portāla lietotāja reģistrācija',
  'Other' => 'Cits',
  'Referral' => 'Referral',
);