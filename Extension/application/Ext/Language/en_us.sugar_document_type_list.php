<?php
 // created: 2019-04-05 11:15:00

$app_list_strings['document_type_list']=array (
  '' => '',
  'Change Order' => 'Change Order',
  'PO' => 'PO',
  'Study Quote' => 'Quote',
  'Quote Revision' => 'Quote Revision',
  'Signed Change Order' => 'Signed Change Order',
  'Signed Quote' => 'Signed Quote',
  'Signed Quote Revision' => 'Signed Quote Revision',
  'Signed SOW' => 'Signed SOW',
  'SOW' => 'SOW',
  'Study Design Document' => 'Study Design',
);