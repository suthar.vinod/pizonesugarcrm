<?php
 // created: 2021-08-05 07:02:55

$app_list_strings['category_list']=array (
  '' => '',
  'Critical Phase Inspection' => 'Critical Phase Inspection',
  'Daily QC' => 'Daily QC',
  'Data Book QC' => 'Data Book QC',
  'Deceased Animal' => 'Deceased Animal (Obsolete)',
  'Equipment Maintenance Request' => 'Equipment Maintenance Request',
  'External Audit' => 'External Audit',
  'Feedback' => 'External Feedback',
  'Maintenance Request' => 'Facility Maintenance Request',
  'Gross Pathology' => 'Gross Pathology',
  'IACUC Deficiency' => 'IACUC Deficiency',
  'Internal Feedback' => 'Internal Feedback',
  'Process Audit' => 'Process Audit',
  'Real time study conduct' => 'Real time study conduct',
  'Rejected Animal' => 'Rejected/Unused Animal (Obsolete)',
  'Retrospective Data QC' => 'Retrospective Data QC',
  'Study Specific Charge' => 'Study Specific Charge',
  'Test Failure' => 'Test Failure',
  'Training' => 'Training',
  'Vet Check' => 'Vet Check',
  'Weekly Sweep' => 'Weekly Sweep',
  'Work Product Schedule Outcome' => 'Work Product Outcome',
);