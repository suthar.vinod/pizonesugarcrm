<?php
 // created: 2022-05-24 07:15:38

$app_list_strings['work_product_status_list']=array (
  'Pending' => 'Pending',
  'In Development' => 'In Development',
  'Testing' => 'Testing',
  'Reporting' => 'Reporting',
  'Archived' => 'Complete',
  'Transferred to NW' => 'Transferred to NW',
  'Withdrawn' => 'Study Not Performed',
);