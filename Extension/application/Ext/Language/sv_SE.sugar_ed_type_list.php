<?php
 // created: 2020-12-15 12:05:33

$app_list_strings['ed_type_list']=array (
  'Animal Enrollment' => 'Animal Enrollment',
  'Animal Health Status to SD' => 'Animal Health Status to SD',
  'Early Term Early Death' => 'Early Term/Early Death',
  'Test Article Failures' => 'Test Article Failures',
  'Unblinding of Study Personnel' => 'Unblinding of Study Personnel',
  '' => '',
  'Business Development' => 'Business Development',
  'External Feedback' => 'External Feedback',
  'External Feedback Follow Up' => 'External Feedback Follow Up',
  'Necropsy Findings' => 'Necropsy Findings',
  'Vet Findings' => 'Vet Findings',
  'SD Assessment Confirmation' => 'SD Assessment Confirmation',
  'COM email' => 'COM email',
  'Client Correspondence' => 'Client Correspondence',
  'Interinstitutional Assurance' => 'Interinstitutional Assurance',
  'Other' => 'Other',
  'SD COM Notification and Acknowledgment' => 'SD COM Notification and Acknowledgment',
);