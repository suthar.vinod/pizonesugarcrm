<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['PO_Purchase_Order'] = 'Purchase Order';
$app_list_strings['moduleListSingular']['PO_Purchase_Order'] = 'Purchase Orders';
$app_list_strings['po_equipment_list']['Not Required'] = 'Not Required';
$app_list_strings['po_equipment_list']['Required Not Received'] = 'Required Not Received';
$app_list_strings['po_equipment_list']['Required and Received'] = 'Required and Received';
$app_list_strings['po_equipment_list'][''] = '';
