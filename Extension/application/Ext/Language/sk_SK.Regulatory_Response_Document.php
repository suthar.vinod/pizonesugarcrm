<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['rrd_regulatory_response_doc_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['rrd_regulatory_response_doc_category_dom']['Knowledege Base'] = 'Báza znalostí';
$app_list_strings['rrd_regulatory_response_doc_category_dom']['Sales'] = 'Predaje';
$app_list_strings['rrd_regulatory_response_doc_category_dom'][''] = '';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom']['Marketing Collateral'] = 'Marketingové materiály';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom']['Product Brochures'] = 'Produktové brožúry';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom']['FAQ'] = 'Často kladené otázky';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom'][''] = '';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Active'] = 'Aktívny';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Draft'] = 'Návrh';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['FAQ'] = 'Často kladené otázky';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Expired'] = 'Platnosť skončila';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Under Review'] = 'Kontrolovaný';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Pending'] = 'Prebieha';
$app_list_strings['moduleList']['RRD_Regulatory_Response_Doc'] = 'Regulatory Response Documents';
$app_list_strings['moduleListSingular']['RRD_Regulatory_Response_Doc'] = 'Regulatory Response Document';
