<?php
 // created: 2021-08-05 07:02:55

$app_list_strings['category_list']=array (
  'Critical Phase Inspection' => 'Critical Phase Inspection',
  'Deceased Animal' => 'Deceased Animal',
  'Maintenance Request' => 'Maintenance Request',
  'Rejected Animal' => 'Rejected Animal',
  'Vet Check' => 'Vet Check',
  '' => '',
  'Daily QC' => 'Daily QC',
  'Data Book QC' => 'Data Book QC',
  'Real time study conduct' => 'Real time study conduct',
  'Weekly Sweep' => 'Weekly Sweep',
  'Feedback' => 'Feedback',
  'External Audit' => 'External Audit',
  'Process Audit' => 'Process Audit',
  'Internal Feedback' => 'Internal Feedback',
  'Gross Pathology' => 'Gross Pathology',
  'IACUC Deficiency' => 'IACUC Deficiency',
  'Training' => 'Training',
  'Retrospective Data QC' => 'Retrospective Data QC',
  'Equipment Maintenance Request' => 'Equipment Maintenance Request',
  'Test Failure' => 'Test Failure',
  'Work Product Schedule Outcome' => 'Work Product Outcome',
  'Study Specific Charge' => 'Study Specific Charge',
);