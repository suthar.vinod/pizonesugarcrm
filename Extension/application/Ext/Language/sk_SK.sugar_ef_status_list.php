<?php
 // created: 2020-10-06 06:36:26

$app_list_strings['ef_status_list']=array (
  '' => '',
  'Active' => 'Active',
  'Active GLP compliant' => 'Active, GLP compliant',
  'Active GLP non compliant' => 'Active, GLP non-compliant',
  'Inactive' => 'Inactive',
  'Out for Service' => 'Out for Service',
  'Quarantined' => 'Quarantined',
  'Retired' => 'Retired',
);