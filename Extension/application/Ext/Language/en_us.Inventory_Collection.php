<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['IC_Inventory_Collection'] = 'Inventory Collections';
$app_list_strings['moduleListSingular']['IC_Inventory_Collection'] = 'Inventory Collection';
$app_list_strings['ic_related_to_list']['Internal Use'] = 'Internal Use';
$app_list_strings['ic_related_to_list']['Work Product'] = 'Work Product';
$app_list_strings['ic_related_to_list'][''] = '';
$app_list_strings['inventory_item_storage_medium_list']['4 Gluteraldehyde'] = '4% Gluteraldehyde';
$app_list_strings['inventory_item_storage_medium_list']['4 Paraformaldehyde'] = '4% Paraformaldehyde';
$app_list_strings['inventory_item_storage_medium_list']['10 Neutral Buffered Formalin'] = '10% Neutral Buffered Formalin';
$app_list_strings['inventory_item_storage_medium_list']['Davidsons Fixative'] = 'Davidson\'s Fixative';
$app_list_strings['inventory_item_storage_medium_list']['None'] = 'None';
$app_list_strings['inventory_item_storage_medium_list']['Normal Saline'] = 'Normal Saline';
$app_list_strings['inventory_item_storage_medium_list']['Water'] = 'Water';
$app_list_strings['inventory_item_storage_medium_list'][''] = '';
$app_list_strings['ic_current_storage_list']['Ambient'] = 'Ambient';
$app_list_strings['ic_current_storage_list']['Frozen'] = 'Frozen';
$app_list_strings['ic_current_storage_list']['Refrigerate'] = 'Refrigerate';
$app_list_strings['ic_current_storage_list']['Room Temperature'] = 'Room Temperature';
$app_list_strings['ic_current_storage_list']['Ultra Frozen'] = 'Ultra Frozen';
$app_list_strings['ic_current_storage_list'][''] = '';
$app_list_strings['location_list'][780] = '780 86th Ave.';
$app_list_strings['location_list'][8945] = '8945 Evergreen Blvd.';
$app_list_strings['location_list'][8960] = '8960 Evergreen Blvd.';
$app_list_strings['location_list'][9055] = '9055 Evergreen Blvd.';
$app_list_strings['location_list'][''] = '';
$app_list_strings['location_shelf_list'][1] = '1';
$app_list_strings['location_shelf_list'][''] = '';
$app_list_strings['location_cabinet_list'][1] = '1';
$app_list_strings['location_cabinet_list'][''] = '';
