<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Existing Customer' => 'Existing Customer',
  'Web Site' => 'Web Site',
  'Cold Call' => 'Cold Call',
  'Self Generated' => 'Self Generated',
  'Referral' => 'Referral',
  'Conference' => 'Conference',
  'Trade Show' => 'Trade Show',
  'Campaign' => 'Campaign',
  'Support Portal User Registration' => 'FlightLog User Registration',
  'Email' => 'Email',
  'Employee' => 'Employee',
  'Other' => 'Other',
);