<?php
 // created: 2021-12-07 09:34:22

$app_list_strings['TSD_purpose_list']=array (
  '' => '',
  'Protect item or device from animal no additional weight added' => 'Protect item or device from animal (no additional weight added)',
  'Hold device or item additional weight added' => 'Hold device or item (additional weight added)',
  'Connected to external items' => 'Connected to external items (machines, computers, devices, etc.)',
  'Restrict animals movement only while working with the animal' => 'Restrict animal’s movement only while working with the animal',
  'Complete awake imaging where the animal is required to remain calm and still' => 'Complete awake imaging where the animal is required to remain calm and still',
  'Complete awake imaging where animal is required to be in a specific position for imaging' => 'Complete awake imaging where animal is required to be in a specific position for imaging',
  'Perform procedure on animal while in sling' => 'Perform procedure on animal while in sling',
  'Gain access to limbs or specific area' => 'Gain access to limbs or specific area',
);