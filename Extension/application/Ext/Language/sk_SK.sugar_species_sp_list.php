<?php
 // created: 2021-01-12 11:35:48

$app_list_strings['species_sp_list']=array (
  '' => '',
  'Bovine' => 'Bovine',
  'Canine' => 'Canine',
  'Caprine' => 'Caprine',
  'Guinea Pig' => 'Guinea Pig',
  'Hamster' => 'Hamster',
  'Lagomorph' => 'Lagomorph',
  'Murine' => 'Murine',
  'Ovine' => 'Ovine',
  'Porcine' => 'Porcine',
  'Rat' => 'Rat',
);