<?php
 // created: 2019-03-13 11:59:45

$app_list_strings['meeting_status_dom']=array (
  'Not Held' => 'Canceled',
  'Completed' => 'Completed',
  'Held' => 'Held',
  'Obsolete' => 'Obsolete',
  'Planned' => 'Scheduled',
);