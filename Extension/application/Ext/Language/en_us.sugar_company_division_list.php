<?php
 // created: 2020-05-13 16:34:56

$app_list_strings['company_division_list']=array (
  '' => '',
  'Analytical Services' => 'Analytical Services',
  'Bioskills' => 'Bioskills',
  'Biocompatibility' => 'Custom Biocompatibility',
  'Standard Biocompatibility' => 'Standard Biocompatibility',
  'Histology Services' => 'Histology Services',
  'ISR' => 'ISR',
  'Pathology Services' => 'Pathology Services',
  'Pharmacology' => 'Pharmacology',
  'Regulatory' => 'Regulatory',
  'Toxicology' => 'Toxicology',
);