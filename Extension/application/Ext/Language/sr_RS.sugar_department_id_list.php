<?php
 // created: 2019-04-24 11:27:38

$app_list_strings['department_id_list']=array (
  'SA AC' => 'SA AC',
  'LA AC' => 'LA AC',
  'SP' => 'SP',
  'IVT' => 'IVT',
  'SA OP' => 'SA OP',
  'DVM' => 'DVM',
  'LA OP' => 'LA OP',
  'ISR' => 'ISR',
  'PS' => 'PS',
  'SCI' => 'SCI',
  'AS' => 'AS',
  'Facility' => 'Facility',
  '' => '',
  'PH OP' => 'PH OP',
  'HS' => 'HS',
);