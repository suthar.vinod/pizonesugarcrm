<?php
 // created: 2020-07-24 12:57:13

$app_list_strings['spa']=array (
  'Not Started' => 'Not Started',
  'Waiting on Payment' => 'Waiting on Payment',
  'Sponsor Submitted' => 'Sponsor Submitted',
  'Waiting on Sponsor' => 'Waiting on Sponsor',
  'APS Approved' => 'APS Approved',
  'APS Reviewed' => 'APS Reviewed (Obsolete)',
  'Sponsor Finalized' => 'Sponsor Finalized (Obsolete)',
  '' => '',
);