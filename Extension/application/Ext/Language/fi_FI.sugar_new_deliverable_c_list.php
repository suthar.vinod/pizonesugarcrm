<?php
 // created: 2017-06-09 21:01:43

$app_list_strings['new_deliverable_c_list']=array (
  '' => '',
  'Analytical Data Tables' => 'Analytical Data Tables',
  'Analytical Report' => 'Analytical Report',
  'Analytical Validation Report' => 'Analytical Validation Report',
  'Animal Health_Clin Path Report' => 'Animal Health Report',
  'Final Report' => 'Final Report',
  'Gross Pathology Report' => 'Gross Pathology Report',
  'Histopathology Report' => 'Histopathology Report',
  'In_Life Report' => 'In-Life Report',
  'Interim Analytical Report' => 'Interim Analytical Report',
  'Interim Animal Health Report' => 'Interim Animal Health Report',
  'Interim Gross Path Report' => 'Interim Gross Path Report',
  'Interim Histopathology Report' => 'Interim Histopathology Report',
  'Interim In_Life Report' => 'Interim In-Life Report',
  'Pharmacology Data Tables' => 'Pharmacology Data Tables',
  'Pharmacology Report' => 'Pharmacology Report',
  'SEM Report' => 'SEM Report',
  'Slide Shipping' => 'Slide Shipping',
  'Tissue Shipping' => 'Tissue Shipping',
);