<?php
 // created: 2022-01-25 09:17:11

$app_list_strings['type_specimen_list']=array (
  '' => '',
  'Balloons' => 'Balloons',
  'Block' => 'Block',
  'Culture' => 'Culture',
  'EDTA Plasma' => 'EDTA Plasma',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Na Heparin Plasma' => 'Na Heparin Plasma',
  'NaCit Plasma' => 'NaCit Plasma',
  'Other' => 'Other',
  'Serum' => 'Serum',
  'Slide' => 'Slide',
  'Tissue' => 'Tissue',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
);