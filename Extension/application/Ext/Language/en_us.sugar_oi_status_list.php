<?php
 // created: 2021-09-02 17:02:44

$app_list_strings['oi_status_list']=array (
  '' => '',
  'Backordered' => 'Backordered',
  'Inventory' => 'Fully Received',
  'Ordered' => 'Ordered',
  'Partially Received' => 'Partially Received',
  'Pending' => 'Pending',
);