<?php
 // created: 2017-03-15 20:57:37

$app_list_strings['Cytotoxicity_Testing']=array (
  'MEM Elution' => 'MEM Elution - CY40',
  'MEM Serial Dilution' => 'MEM Serial Dilution - CY41',
  'Liquid Cytotoxicity' => 'Liquid Cytotoxicity - CY10',
  'Bacterial Endotoxin' => 'Bacterial Endotoxin - CY50',
  'Bacterial Endotoxin_Lot Release' => 'Bacterial Endotoxin, Lot Release - CY51',
  'Agar Overlay' => 'Agar Overlay, Solid - CY01',
  'Agar Overlay_Liquid' => 'Agar Overlay, Liquid - CY02',
  'Direct Contact' => 'Direct Contact - CY20',
  'Neutral Red Uptake' => 'Neutral Red Uptake - CY30',
  'MTT' => 'MTT - CY70',
  'Colony Formation' => 'Colony Formation - CY60',
  'Choose one...' => 'Choose One...',
);