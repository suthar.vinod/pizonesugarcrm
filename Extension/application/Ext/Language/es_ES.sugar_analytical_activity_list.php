<?php
 // created: 2018-11-21 17:55:35

$app_list_strings['analytical_activity_list']=array (
  '' => '',
  'Method Development' => 'Method Development',
  'Protocol Development' => 'Protocol Development',
  'Sample Prep' => 'Sample Prep',
  'Sample Analysis' => 'Sample Analysis',
  'Analytical Report Writing' => 'Analytical Report Writing',
  'Out of Office' => 'Out of Office',
  'Data Analysis' => 'Data Analysis',
  'Instrument Maintenance' => 'Instrument Maintenance',
  'Data Delivery To Toxicologist' => 'Data Delivery To Toxicologist',
  'Exhaustive exaggerated extraction start' => 'Exhaustive/Exaggerated Extraction Start',
  'Simulated Use Extraction Start' => 'Simulated Use Extraction Start',
);