<?php
 // created: 2021-11-18 10:19:23

$app_list_strings['aaalac_and_usda_exemptions_list']=array (
  '' => '',
  'Conscious Restraint' => 'Conscious Restraint (USDA and AAALAC)',
  'Exercise Restriction dogs' => ' Exercise Restriction (dogs) (USDA and AAALAC)',
  'FoodWater Restriction' => 'Food/Water Restriction (aside from approved fasting) (USDA and AAALAC)',
  'Multiple Major Surgeries 1protocol' => 'Multiple Major Survival Surgery (1/protocol) (USDA and AAALAC)',
  'Multiple Survival Surgeries' => 'Multiple Survival Surgery (AAALAC)',
  'Neuromuscular Blocker' => ' Neuromuscular Blocker/Paralytics (AAALAC)',
  'Personnel Training' => ' Personnel Training (AAALAC)',
  'Survival Surgery' => 'Survival Surgery (AAALAC)',
  'Toxicity Study' => 'Toxicity Study (AAALAC)',
);