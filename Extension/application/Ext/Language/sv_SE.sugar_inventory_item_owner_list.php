<?php
 // created: 2021-11-10 00:31:56

$app_list_strings['inventory_item_owner_list']=array (
  '' => '',
  'APS Owned' => 'APS Owned',
  'APS Supplied' => 'APS Supplied',
  'Client Owned' => 'Client Owned',
  'APS Supplied Client Owned' => 'APS Supplied, Client Owned',
);