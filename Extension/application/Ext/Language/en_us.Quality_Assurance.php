<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['moduleList']['A1A_CPI_Findings'] = 'CPI Findings';
$app_list_strings['moduleListSingular']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['moduleListSingular']['A1A_CPI_Findings'] = 'CPI Findings';
$app_list_strings['phase_of_inspection_list']['Choose One'] = 'Choose One';
$app_list_strings['phase_of_inspection_list']['Sample Preparation'] = 'Sample Preparation';
$app_list_strings['phase_of_inspection_list']['Addition of Article to Test System'] = 'Addition of Article to Test System';
$app_list_strings['phase_of_inspection_list']['Evaluation'] = 'Evaluation';
$app_list_strings['phase_of_inspection_list']['Analytical'] = 'Analytical';
$app_list_strings['phase_of_inspection_list']['Clinical Pathology'] = 'Clinical Pathology';
$app_list_strings['phase_of_inspection_list']['Contributing Scientist Report'] = 'Contributing Scientist Report';
$app_list_strings['phase_of_inspection_list']['Initial Procedure'] = 'Initial Procedure';
$app_list_strings['phase_of_inspection_list']['In Life'] = 'In-Life';
$app_list_strings['phase_of_inspection_list']['Followup Procedure'] = 'Follow-up Procedure';
$app_list_strings['phase_of_inspection_list']['Terminal Procedure'] = 'Terminal Procedure';
$app_list_strings['phase_of_inspection_list']['Necropsy'] = 'Necropsy';
$app_list_strings['phase_of_inspection_list']['Tissue Trimming'] = 'Tissue Trimming';
$app_list_strings['phase_of_inspection_list']['Histology'] = 'Histology';
$app_list_strings['phase_of_inspection_list']['Data Review'] = 'Data Review';
$app_list_strings['phase_of_inspection_list']['Interim Report'] = 'Interim Report';
$app_list_strings['phase_of_inspection_list']['Animal Health Report'] = 'Animal Health Report';
$app_list_strings['phase_of_inspection_list']['Pathology Report'] = 'Pathology Report';
$app_list_strings['phase_of_inspection_list']['Final Report'] = 'Final Report';
$app_list_strings['phase_of_inspection_list']['Final Report Amendment'] = 'Final Report Amendment';
$app_list_strings['cpi_finding_category_list']['Choose One'] = 'Choose One';
$app_list_strings['cpi_finding_category_list']['Documentation Error'] = 'Documentation Error';
$app_list_strings['cpi_finding_category_list']['Missed Task General '] = 'Missed Task - General ';
$app_list_strings['cpi_finding_category_list']['Missed Task Protocol'] = 'Missed Task - Protocol';
$app_list_strings['cpi_finding_category_list']['Missed Task SOP'] = 'Missed Task SOP';
$app_list_strings['cpi_finding_category_list']['Equipment'] = 'Equipment';
$app_list_strings['cpi_finding_category_list']['Timed Event'] = 'Timed Event';
