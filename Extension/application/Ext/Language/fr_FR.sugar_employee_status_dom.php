<?php
 // created: 2018-09-25 12:50:07

$app_list_strings['employee_status_dom']=array (
  'Active' => 'Actif',
  'Terminated' => 'Inactif',
  'Leave of Absence' => 'Autorisation d&#39;absence',
  'Non APS Study Director' => 'Non APS Study Director',
);