<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Cold Call',
  'Existing Customer' => 'Bestaande klant',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Medewerker',
  'Conference' => 'Conferentie',
  'Trade Show' => 'Beurs',
  'Web Site' => 'Website',
  'Email' => 'E-mailadres',
  'Campaign' => 'Campagne',
  'Support Portal User Registration' => 'Support Portal Gebruikers Registratie',
  'Other' => 'Anders',
  'Referral' => 'Referral',
);