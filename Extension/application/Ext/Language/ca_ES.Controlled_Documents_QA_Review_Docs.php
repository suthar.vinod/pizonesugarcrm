<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ca_ES.Controlled_Documents_QA_Review_Docs.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ca_ES.Controlled_Documents_QA_Review_Docs.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Màrketing';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Página de Vendes';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Impresos de Màrqueting';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Fullets de Producte';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'PREGUNTES FREQÜENTS';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Actiu';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Esborrany';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'PREGUNTES FREQÜENTS';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Caducat';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'En Revisió';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Pendent';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Màrketing';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Página de Vendes';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Impresos de Màrqueting';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Fullets de Producte';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'PREGUNTES FREQÜENTS';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Actiu';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Esborrany';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'PREGUNTES FREQÜENTS';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Caducat';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'En Revisió';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Pendent';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Màrketing';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Página de Vendes';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Impresos de Màrqueting';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Fullets de Producte';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'PREGUNTES FREQÜENTS';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Actiu';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Esborrany';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'PREGUNTES FREQÜENTS';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Caducat';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'En Revisió';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Pendent';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';

?>
