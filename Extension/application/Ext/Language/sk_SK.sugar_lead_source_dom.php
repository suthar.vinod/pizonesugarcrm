<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Predajný telefonát',
  'Existing Customer' => 'Existujúci zákazník',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Zamestnanec',
  'Conference' => 'Konferencia',
  'Trade Show' => 'Veľtrh',
  'Web Site' => 'Webstránka',
  'Email' => 'Email:',
  'Campaign' => 'Kampaň',
  'Support Portal User Registration' => 'Registrácia užívateľa na portáli podpory',
  'Other' => 'Iné',
  'Referral' => 'Referral',
);