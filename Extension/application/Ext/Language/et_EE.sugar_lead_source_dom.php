<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Külm kõne',
  'Existing Customer' => 'Olemasolev klient',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Employee',
  'Conference' => 'Konverents',
  'Trade Show' => 'Trade show',
  'Web Site' => 'Veebisait',
  'Email' => 'Muu E-post:',
  'Campaign' => 'Campaign',
  'Support Portal User Registration' => 'Support Portal User Registration',
  'Other' => 'Teine',
  'Referral' => 'Referral',
);