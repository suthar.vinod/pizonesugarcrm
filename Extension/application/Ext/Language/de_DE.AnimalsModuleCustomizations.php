<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.AnimalsModuleCustomizations.php
 
$app_list_strings['vaccine_a_list'] = array (
  'Choose One' => 'Choose One',
  'Mycoplasma' => 'Mycoplasma',
  'Lawsonia' => 'Lawsonia',
  'Rabies' => 'Rabies',
  'Bordetella' => 'Bordetella',
  'DHPP' => 'DHPP',
  'CDT' => 'CDT',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['room_list'] = array (
  'Choose One' => 'Choose One',
  89452 => '8945-2',
  89453 => '8945-3',
  89454 => '8945-4',
  89455 => '8945-5',
  89456 => '8945-6',
  89457 => '8945-7',
  89458 => '8945-8',
  89459 => '8945-9',
  894510 => '8945-10',
  894511 => '8945-11',
  894512 => '8945-12',
  894513 => '8945-13',
  894514 => '8945-14',
  894515 => '8945-15',
  894516 => '8945-16',
  894519 => '8945-19',
  894520 => '8945-20',
  894521 => '8945-21',
  894522 => '8945-22',
  894523 => '8945-23',
  894524 => '8945-24',
  894525 => '8945-25',
  894526 => '8945-26',
  894527 => '8945-27',
  894528 => '8945-28',
  894529 => '8945-29',
  894530 => '8945-30',
  894531 => '8945-31',
  894532 => '8945-32',
  894533 => '8945-33',
  894534 => '8945-34',
  78035 => '780-35',
  78036 => '780-36',
  78037 => '780-37',
  78038 => '780-38',
  78039 => '780-39',
  89601 => '8960-1',
  89602 => '8960-2',
  89603 => '8960-3',
  89604 => '8960-4',
  89605 => '8960-5',
  89606 => '8960-6',
  89607 => '8960-7',
  89608 => '8960-8',
  89609 => '8960-9',
  896010 => '8960-10',
  896011 => '8960-11',
  896012 => '8960-12',
  896013 => '8960-13',
  896014 => '8960-14',
  896015 => '8960-15',
  896016 => '8960-16',
  896017 => '8960-17',
  896018 => '8960-18',
  896019 => '8960-19',
  896020 => '8960-20',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sex_list'] = array (
  'Choose One' => 'Choose One',
  'Female' => 'Female',
  'Castrated Male' => 'Castrated Male',
  'Male' => 'Male',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['enrollment_status_list'] = array (
  'Choose One' => 'Choose One',
  'Assigned Backup' => 'Assigned Backup',
  'Assigned On Study' => 'Assigned On Study',
  'Non naive Stock' => 'Non-naive Stock',
  'Not Used' => 'Not Used',
  'On Study' => 'On Study',
  'On Study Backup' => 'On Study Backup',
  'On Study Transferred' => 'On Study Transferred',
  'Stock' => 'Stock',
  'Deceased' => 'Deceased',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['species_list'] = array (
  'Canine' => 'Canine',
  'Caprine' => 'Caprine',
  'Guinea Pig' => 'Guinea Pig',
  'Hamster' => 'Hamster',
  'Lagomorph' => 'Lagamorph',
  'Mouse' => 'Mouse',
  'Rat' => 'Rat',
  'Choose One' => 'Choose One',
  'Porcine' => 'Porcine',
  'Bovine' => 'Bovine',
  'Ovine' => 'Ovine',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['Breed_List'] = array (
  'Beagle' => 'Beagle',
  'CD1' => 'CD1',
  'CF1' => 'CF1',
  'Gottingen' => 'Gottingen',
  'Hartley' => 'Hartley',
  'Holstein' => 'Holstein',
  'Mongrel' => 'Mongrel',
  'ND4' => 'ND4',
  'New Zealand White' => 'New Zealand White',
  'Choose One' => 'Choose One',
  'Athymic Nude' => 'Athymic Nude',
  'Cross Breed' => 'Cross Breed',
  'Golden Syrian' => 'Golden Syrian',
  'Polypay' => 'Polypay',
  'Sinclair' => 'Sinclair',
  'Sprague Dawley' => 'Sprague Dawley',
  'Suffolk X' => 'Suffolk X',
  'Watanabe' => 'Watanabe',
  'Yorkshire X' => 'Yorkshire X',
  'Yucatan' => 'Yucatan',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['vendor_list'] = array (
  'Choose One' => 'Choose One',
  'APS' => 'APS',
  'Bakkom Rabbitry' => 'Bakkom Rabbitry',
  'Charles River' => 'Charles River',
  'Elm Hill' => 'Elm Hill',
  'Envigo' => 'Envigo',
  'Lonestar Laboratory Swine' => 'Lonestar Laboratory Swine',
  'Manthei Hogs' => 'Manthei Hogs',
  'Marshall BioResources' => 'Marshall BioResources',
  'Neaton Polupays' => 'Neaton Polupays',
  'Oak Hill Genetics' => 'Oak Hill Genetics',
  'Purdue University' => 'Purdue University',
  'Rigland Farms' => 'Rigland Farms',
  'S and S Farms' => 'S & S Farms',
  'Sinclair BioResources' => 'Sinclair BioResources',
  'Twin Valley Kennels' => 'Twin Valley Kennels',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['usda_category_list'] = array (
  'B' => 'B',
  'C' => 'C',
  'D' => 'D',
  'E' => 'E',
);
?>
