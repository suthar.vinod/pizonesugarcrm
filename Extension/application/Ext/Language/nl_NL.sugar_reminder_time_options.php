<?php
 // created: 2017-06-16 19:19:42

$app_list_strings['reminder_time_options']=array (
  -1 => 'Geen',
  60 => '1 minuut ervoor',
  300 => '5 minuten ervoor',
  600 => '10 minuten ervoor',
  900 => '15 minuten ervoor',
  1800 => '30 minuten ervoor',
  3600 => '1 uur ervoor',
  7200 => '2 uur ervoor',
  10800 => '3 uur ervoor',
  18000 => '5 uur ervoor',
  86400 => '1 dag ervoor',
  604800 => '7 days prior',
);