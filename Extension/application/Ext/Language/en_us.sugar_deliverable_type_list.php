<?php
 // created: 2020-03-16 11:48:25

$app_list_strings['deliverable_type_list']=array (
  '' => '',
  'BloodPlasmaSerum' => 'Blood/Plasma/Serum',
  'Feed' => 'Feed',
  'Hay' => 'Hay',
  'Paraffin Blocks' => 'Paraffin Blocks',
  'Slide' => 'Slide',
  'Study Materials' => 'Study Materials',
  'Tissue' => 'Tissue',
);