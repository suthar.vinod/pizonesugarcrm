<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Hideghívás',
  'Existing Customer' => 'Meglévő kliens',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Munkavállaló',
  'Conference' => 'Konferencia',
  'Trade Show' => 'Kiállítás',
  'Web Site' => 'Weboldal',
  'Email' => 'E-mail:',
  'Campaign' => 'Kampány',
  'Support Portal User Registration' => 'Támogatás portál felhasználói regisztráció',
  'Other' => 'Egyéb',
  'Referral' => 'Referral',
);