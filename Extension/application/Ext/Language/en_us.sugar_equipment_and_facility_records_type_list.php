<?php
 // created: 2020-06-16 09:04:45

$app_list_strings['equipment_and_facility_records_type_list']=array (
  '' => '',
  'Calibration' => 'Calibration',
  'Chain of Custody' => 'Chain of Custody',
  'Cleaning' => 'Cleaning',
  'Initial In Service' => 'Completed In-Service',
  'Equipment Qualification' => 'Performance Verification',
  'Initial In Service 2' => 'Initial in Service',
  'Inspection' => 'Inspection',
  'Part 11 Gap Analysis' => 'Part 11 Gap Analysis',
  'Preventative Maintenance' => 'Preventative Maintenance',
  'Repair' => 'Repair',
  'Routine Maintenance' => 'Routine Maintenance',
  'Service Contract' => 'Service Contract',
  'SoftwareFirmware Change' => 'Software/Firmware Change',
  'Verification' => 'Verification',
);