<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Chiamata a Freddo',
  'Existing Customer' => 'Cliente Esistente',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Dipendente',
  'Conference' => 'Conferenza',
  'Trade Show' => 'Fiera',
  'Web Site' => 'Sito Web',
  'Email' => 'Email alternativo',
  'Campaign' => 'Campagna',
  'Support Portal User Registration' => 'Registrazione Utente al Portale di Supporto',
  'Other' => 'Altro',
  'Referral' => 'Referral',
);