<?php
 // created: 2017-06-16 19:19:41

$app_list_strings['reminder_time_options']=array (
  -1 => 'Nessuno',
  60 => '1 minuto prima',
  300 => '5 minuti prima',
  600 => '10 minuti prima',
  900 => '15 minuti prima',
  1800 => '30 minuti prima',
  3600 => '1 ora prima',
  7200 => '2 ore prima',
  10800 => '3 ore prima',
  18000 => '5 ore prima',
  86400 => '1 giorno prima',
  604800 => '7 days prior',
);