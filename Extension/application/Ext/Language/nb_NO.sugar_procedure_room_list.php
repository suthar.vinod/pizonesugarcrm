<?php
 // created: 2021-08-13 06:34:37

$app_list_strings['procedure_room_list']=array (
  1 => 'Cath Lab 1',
  2 => 'Cath Lab 2',
  3 => 'Cath Lab 3',
  4 => 'Cath Lab 4',
  5 => 'CT',
  6 => 'Histology Lab',
  7 => 'Necropsy',
  8 => 'OR1',
  9 => 'OR2',
  10 => 'OR3',
  11 => 'OR4',
  12 => 'OR5',
  13 => 'Sample Prep Lab',
  14 => 'IVT Lab',
  15 => 'Clin Path Lab',
  16 => 'Blood Loop Lab',
);