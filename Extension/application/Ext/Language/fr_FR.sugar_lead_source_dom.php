<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Appel entrant',
  'Existing Customer' => 'Client existant',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Employé',
  'Conference' => 'Conférence',
  'Trade Show' => 'Salon',
  'Web Site' => 'Site web',
  'Email' => 'Email Quelconque',
  'Campaign' => 'Campagne',
  'Support Portal User Registration' => 'Portail de support',
  'Other' => 'Autre',
  'Referral' => 'Referral',
);