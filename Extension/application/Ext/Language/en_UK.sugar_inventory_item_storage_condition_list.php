<?php
 // created: 2021-11-10 02:34:15

$app_list_strings['inventory_item_storage_condition_list']=array (
  '' => '',
  'Ambient Temperature' => 'Ambient Temperature',
  'Room Temperature' => 'Room Temperature',
  'Refrigerated' => 'Refrigerated',
  'Frozen' => 'Frozen',
  'Ultra Frozen in Liquid Nitrogen' => 'Ultra Frozen in Liquid Nitrogen',
  'Ultra Frozen in Freezer' => 'Ultra Frozen in Freezer',
  'To Be Determined' => 'To Be Determined',
);