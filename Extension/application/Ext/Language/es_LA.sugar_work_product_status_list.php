<?php
 // created: 2022-05-24 07:15:38

$app_list_strings['work_product_status_list']=array (
  'Archived' => 'Archived',
  'Testing' => 'Testing',
  'Withdrawn' => 'Withdrawn',
  'In Development' => 'In Development',
  'Pending' => 'Pending',
  'Reporting' => 'Reporting',
  'Transferred to NW' => 'Transferred to NW',
);