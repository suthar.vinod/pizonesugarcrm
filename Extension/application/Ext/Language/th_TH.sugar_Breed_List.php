<?php
 // created: 2022-04-05 07:21:33

$app_list_strings['Breed_List']=array (
  'Beagle' => 'Beagle',
  'CD1' => 'CD1',
  'CF1' => 'CF1',
  'Gottingen' => 'Gottingen',
  'Hartley' => 'Hartley',
  'Holstein' => 'Holstein',
  'Mongrel' => 'Mongrel',
  'ND4' => 'ND4',
  'New Zealand White' => 'New Zealand White',
  'Athymic Nude' => 'Athymic Nude',
  'Cross Breed' => 'Cross Breed',
  'Golden Syrian' => 'Golden Syrian',
  'Polypay' => 'Polypay',
  'Sinclair' => 'Sinclair',
  'Sprague Dawley' => 'Sprague Dawley',
  'Suffolk X' => 'Suffolk X',
  'Watanabe' => 'Watanabe',
  'Yorkshire X' => 'Yorkshire X',
  'Yucatan' => 'Yucatan',
  'C57' => 'C57',
  'Wistar' => 'Wistar',
  'CD Hairless' => 'CD Hairless',
  'SKH1 Hairless' => 'SKH1 Hairless',
  'Micro Yucatan' => 'Micro-Yucatan',
  'C3H HeJ' => 'C3H/HeJ',
  'BALBc' => 'BALB/c',
  'Lamancha' => 'Lamancha',
  'Freisen' => 'Freisen',
  'Myotonic' => 'Myotonic',
  'LDLR' => 'LDLR Yucatan',
  'Dutch Belted' => 'Dutch Belted',
  'Alpine' => 'Alpine',
  'Saanen' => 'Saanen',
  'Toggenburg' => 'Toggenburg',
  'Ossabaw' => 'Ossabaw',
  'Jersey' => 'Jersey',
  '' => '',
  'Dorset' => 'Dorset',
  'Dorset X' => 'Dorset X',
  'Hampshire' => 'Hampshire',
  'Hampshire X' => 'Hampshire X',
  'Hanford' => 'Hanford',
  'Other' => 'Other',
  'Polypay X' => 'Polypay X',
  'Suffolk' => 'Suffolk',
  'Landrace Cross' => 'Landrace Cross',
  'Lewis' => 'Lewis',
);