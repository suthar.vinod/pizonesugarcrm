<?php
 // created: 2017-06-16 19:19:41

$app_list_strings['reminder_time_options']=array (
  -1 => 'Ninguna',
  60 => '1 minuto antes',
  300 => '5 minutos antes',
  600 => '10 minutos antes',
  900 => '15 minutos antes',
  1800 => '30 minutos antes',
  3600 => '1 hora antes',
  7200 => '2 horas antes',
  10800 => '3 horas antes',
  18000 => '5 horas antes',
  86400 => '1 día antes',
  604800 => '7 days prior',
);