<?php
 // created: 2016-04-26 15:59:11

$app_list_strings['patient_contact_duration_list']=array (
  'Limited' => 'Limited (24 hours or less)',
  'Prolonged' => 'Prolonged (24 hours - 30 days)',
  'Permanent' => 'Permanent (greater than 30 days)',
);