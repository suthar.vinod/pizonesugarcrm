<?php
 // created: 2020-05-13 17:26:17

$app_list_strings['expanded_study_result_list']=array (
  '' => '',
  'Not Applicable' => 'Not Applicable',
  'Passed Study' => 'Passed Study',
  'Failed Study' => 'Failed Study',
  'Completed Study' => 'Completed Study',
);