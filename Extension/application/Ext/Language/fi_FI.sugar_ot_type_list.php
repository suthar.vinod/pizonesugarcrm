<?php
 // created: 2022-02-08 07:42:46

$app_list_strings['ot_type_list']=array (
  '' => '',
  'Ablation' => 'Ablation',
  'Mapping' => 'Mapping',
  'AblationMapping' => 'Ablation/Mapping',
  'Peripheral' => 'Peripheral',
  'Coronary' => 'Coronary',
  'Gastrointestinal' => 'Gastrointestinal',
  'Pulmonary' => 'Pulmonary',
  'Urinary' => 'Urinary',
  'Stent Graft' => 'Stent Graft',
  'Surgical Mitral' => 'Surgical Mitral',
  'Transcatheter Mitral' => 'Transcatheter Mitral',
  'Surgical Tricuspid' => 'Surgical Tricuspid',
  'Transcatheter Tricuspid' => 'Transcatheter Tricuspid',
  'Surgical Aortic' => 'Surgical Aortic',
  'Transcatheter Aortic' => 'Transcatheter Aortic',
  'Surgical Pulmonary' => 'Surgical Pulmonary',
  'Transcatheter Pulmonary' => 'Transcatheter Pulmonary',
  'Interpositional' => 'Interpositional',
  'AVF' => 'AVF',
  'AVG' => 'AVG',
  'CABG' => 'CABG',
  'Cebrebral' => 'Cebrebral',
  'Cardiac' => 'Cardiac',
  'Protection' => 'Protection',
  'Renal' => 'Renal',
  'Thigh' => 'Thigh',
  'Bladder' => 'Bladder',
  'Prostate' => 'Prostate',
  'Surgical LVAD' => 'Surgical LVAD',
  'Transcatheter LVAD' => 'Transcatheter LVAD',
  'Aortic' => 'Aortic',
  'ECMO' => 'ECMO',
  'Liver' => 'Liver',
  'Kidney' => 'Kidney',
  'Left Atrial Closure' => 'Left Atrial Closure',
  'Septal Treatment' => 'Septal Treatment',
  'Thrombectomy' => 'Thrombectomy',
  'Gastroscopy' => 'Gastroscopy',
  'Bronchoscopy' => 'Bronchoscopy',
  'Laprascopy' => 'Laprascopy',
  'Thorascopy' => 'Thorascopy',
  'Intercardiac' => 'Intercardiac',
  'Epicardial' => 'Epicardial',
  'Epidural' => 'Epidural',
  'Vascular Closure' => 'Vascular Closure',
  'Hemostatic Agents' => 'Hemostatic Agents',
  'Vessel Sealing Device' => 'Vessel Sealing Device',
  'Wound Creation' => 'Wound Creation',
  'Bandaging' => 'Bandaging',
  'Surface modulation' => 'Surface modulation',
  'Cardiac Evaluation' => 'Cardiac Evaluation',
  'Peripheral Evaluation' => 'Peripheral Evaluation',
  'TendonLigament Repair' => 'Tendon/Ligament Repair',
  'Orthopedics' => 'Orthopedics',
  'Cartilage Defect' => 'Cartilage Defect',
  'Joint injection' => 'Joint injection',
  'Other' => 'Other',
  'Aneurysm Elastase' => 'Aneurysm Elastase',
  'Aneurysm Surgical' => 'Aneurysm Surgical',
  'Standard' => 'Standard',
);