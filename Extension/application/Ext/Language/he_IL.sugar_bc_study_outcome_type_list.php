<?php
 // created: 2016-03-08 15:54:45

$app_list_strings['bc_study_outcome_type_list']=array (
  'Completed Study' => 'Completed Study',
  'Passed Study' => 'Passed Study',
  'Failed Study' => 'Failed Study',
  'Expanded Study' => 'Expanded Study',
  'Aborted Study' => 'Aborted Study',
  'CAB Study' => 'CAB Study',
  'None' => 'None',
  'Choose SOT' => 'Choose SOT',
);