<?php
 // created: 2016-08-19 22:09:40

$app_list_strings['device_type_list']=array (
  'Choose one...' => 'Choose one...',
  'ep_catheter' => 'EP Catheter',
  'coronary_stent' => 'Coronary Stent',
  'peripheral_stent' => 'Peripheral Stent',
  'aneurysm_filler' => 'Aneurysm Filler/Diverter',
  'heart_valve' => 'Heart Valve',
  'intravascular_catheter' => 'Intravascular Catheter',
  'neuromodulation' => 'Neuromodulation',
);