<?php
 // created: 2022-03-15 07:23:37

$app_list_strings['integration_work_stream_list']=array (
  '' => '',
  'Accounting Finance Tax' => 'Accounting/Finance/Tax',
  'Building Buildouts' => 'Building Buildouts',
  'Commercial Plan' => 'Commercial Plan',
  'Communications Plans' => 'Communications Plans',
  'Cultural Integration Plan' => 'Cultural Integration Plan',
  'eQMS' => 'eQMS',
  'Financial Force ERP' => 'Financial Force ERP',
  'Human Resources' => 'Human Resources',
  'Integration Strategy Governance' => 'Integration Strategy & Governance',
  'IT' => 'IT',
  'Labware' => 'Labware',
  'Metrics for Success' => 'Metrics for Success',
  'NAMSA 360 Single Portal' => 'NAMSA 360 (Single Portal)',
  'Operations Biosafety Programs' => 'Operations: Biosafety Programs',
  'Operations Brooklyn Park' => 'Operations: Brooklyn Park',
  'Operations Coon Rapids' => 'Operations: Coon Rapids',
  'Operations Coon Rapids Build Out' => 'Operations: Coon Rapids Build Out',
  'Procurement Supply Chain' => 'Procurement / Supply Chain',
  'PSA  Financial Force' => 'PSA – Financial Force',
  'Quality' => 'Quality',
  'Sugar Software' => 'Sugar Software',
);