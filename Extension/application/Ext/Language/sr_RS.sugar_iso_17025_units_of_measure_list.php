<?php
 // created: 2019-02-20 14:58:21

$app_list_strings['iso_17025_units_of_measure_list']=array (
  '' => '',
  'CO2' => 'CO2',
  'Dimension' => 'Dimension',
  'gm' => 'gm',
  'kg' => 'kg',
  'Length' => 'Length',
  'Mass' => 'Mass',
  'Not Applicable' => 'Not Applicable',
  'Relative Humidity' => 'Relative Humidity',
  'RPM' => 'RPM',
  'Temperature' => 'Temperature',
  'Time' => 'Time',
  'Voltage' => 'Voltage',
  'Volume' => 'Volume',
);