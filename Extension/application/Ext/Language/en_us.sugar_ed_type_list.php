<?php
 // created: 2020-12-15 12:05:33

$app_list_strings['ed_type_list']=array (
  '' => '',
  'Animal Enrollment' => 'Animal Enrollment',
  'Animal Health Status to SD' => 'Animal Health Status to SD',
  'Business Development' => 'Business Development',
  'Client Correspondence' => 'Client Correspondence',
  'COM email' => 'COM email',
  'Early Term Early Death' => 'Early Term/Early Death',
  'External Feedback' => 'External Feedback',
  'External Feedback Follow Up' => 'External Feedback Follow Up',
  'Interinstitutional Assurance' => 'Interinstitutional Assurance',
  'Necropsy Findings' => 'Necropsy Findings',
  'Other' => 'Other',
  'SD Assessment Confirmation' => 'SD Assessment Confirmation',
  'SD COM Notification and Acknowledgment' => 'SD COM Notification and Acknowledgment',
  'Test Article Failures' => 'Test Article Failures',
  'Unblinding of Study Personnel' => 'Unblinding of Study Personnel',
  'Vet Findings' => 'Vet Findings',
);