<?php
 // created: 2020-12-03 15:06:36

$app_list_strings['ef_status_list']=array (
  '' => '',
  'Active' => 'Active',
  'Active GLP compliant' => 'Active, GLP compliant',
  'Active GLP non compliant' => 'Active, GLP non-compliant',
  'Out for Service' => 'Out for Service',
  'Quarantined' => 'Quarantined',
  'Retired' => 'Retired',
);