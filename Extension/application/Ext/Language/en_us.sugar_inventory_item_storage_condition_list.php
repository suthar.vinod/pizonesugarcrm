<?php
 // created: 2021-11-10 02:34:15

$app_list_strings['inventory_item_storage_condition_list']=array (
  '' => '',
  'Ambient Temperature' => 'Ambient Temperature',
  'Frozen' => 'Frozen',
  'Refrigerated' => 'Refrigerated',
  'Room Temperature' => 'Room Temperature',
  'To Be Determined' => 'To Be Determined',
  'Ultra Frozen in Freezer' => 'Ultra Frozen in Freezer',
  'Ultra Frozen in Liquid Nitrogen' => 'Ultra Frozen in Liquid Nitrogen',
);