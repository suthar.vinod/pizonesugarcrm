<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.NAMSA_Test_Codes.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['TC_NAMSA_Test_Codes'] = 'NAMSA Test Codes';
$app_list_strings['moduleListSingular']['TC_NAMSA_Test_Codes'] = 'NAMSA Test Code';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['TC_NAMSA_Test_Codes'] = 'NAMSA Test Codes';
$app_list_strings['moduleListSingular']['TC_NAMSA_Test_Codes'] = 'NAMSA Test Code';

?>
