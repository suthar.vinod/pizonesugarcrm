<?php
 // created: 2018-11-21 17:55:35

$app_list_strings['analytical_activity_list']=array (
  '' => '',
  'Analytical Report Writing' => 'Analytical Report Writing',
  'Data Analysis' => 'Data Analysis',
  'Data Delivery To Toxicologist' => 'Data Delivery To Toxicologist',
  'Exhaustive exaggerated extraction start' => 'Exhaustive/Exaggerated Extraction Start',
  'Instrument Maintenance' => 'Instrument Maintenance',
  'Method Development' => 'Method Development',
  'Out of Office' => 'Out of Office',
  'Protocol Development' => 'Protocol Development',
  'Sample Analysis' => 'Sample Analysis',
  'Sample Prep' => 'Sample Prep',
  'Simulated Use Extraction Start' => 'Simulated Use Extraction Start',
);