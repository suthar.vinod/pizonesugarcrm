<?php
 // created: 2019-09-16 11:47:52

$app_list_strings['classification_list']=array (
  '' => '',
  'Non Data Generator' => 'Non Data Generator',
  'Data Generator With Electronic Records' => 'Data Generator With Electronic Records',
  'Data Generator Without Electronic Records' => 'Data Generator Without Electronic Records',
  'Non GLP Equipment' => 'Non GLP Equipment',
);