<?php
 // created: 2022-02-24 13:56:09

$app_list_strings['specimen_type_no_other_c_list']=array (
  '' => '',
  'Balloons' => 'Balloons',
  'Culture' => 'Culture',
  'EDTA Plasma' => 'EDTA Plasma',
  'Fecal' => 'Fecal',
  'NaCit Plasma' => 'NaCit Plasma',
  'Na Heparin Plasma' => 'Na Heparin Plasma',
  'Serum' => 'Serum',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
);