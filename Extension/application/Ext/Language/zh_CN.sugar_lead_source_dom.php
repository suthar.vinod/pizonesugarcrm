<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => '冷电',
  'Existing Customer' => '现有客户',
  'Self Generated' => 'Self Generated',
  'Employee' => '员工',
  'Conference' => '会议',
  'Trade Show' => '展览',
  'Web Site' => '网站',
  'Email' => '任何电子邮件',
  'Campaign' => '市场活动：',
  'Support Portal User Registration' => '支持门户用户注册',
  'Other' => '其它',
  'Referral' => 'Referral',
);