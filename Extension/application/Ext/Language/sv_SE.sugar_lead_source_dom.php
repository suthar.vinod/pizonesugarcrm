<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Cold call',
  'Existing Customer' => 'Existerande kund',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Anställd',
  'Conference' => 'Konferans',
  'Trade Show' => 'Mässa',
  'Web Site' => 'Webbsida',
  'Email' => 'Någon epost',
  'Campaign' => 'Kampanj',
  'Support Portal User Registration' => 'Portal Support Användaregistrering',
  'Other' => 'Annan',
  'Referral' => 'Referral',
);