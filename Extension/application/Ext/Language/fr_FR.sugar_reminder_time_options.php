<?php
 // created: 2017-06-16 19:19:41

$app_list_strings['reminder_time_options']=array (
  -1 => 'Aucun',
  60 => '1 minute avant',
  300 => '5 minutes avant',
  600 => '10 minutes avant',
  900 => '15 minutes avant',
  1800 => '30 minutes avant',
  3600 => '1 heure avant',
  7200 => '2 heures avant',
  10800 => '3 heures avant',
  18000 => '5 heures avant',
  86400 => '1 jour avant',
  604800 => '7 days prior',
);