<?php
 // created: 2019-08-01 17:01:07

$app_list_strings['win_probability_list']=array (
  '' => '',
  'zeropercent' => '0%',
  25 => '25%',
  50 => '50%',
  75 => '75%',
  100 => '100%',
);