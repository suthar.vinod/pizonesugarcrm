<?php
 // created: 2020-05-13 18:25:52

$app_list_strings['reason_for_amendment_list']=array (
  '' => '',
  'Typographical Error' => 'Typographical and/or Grammatical Error',
  'Data Entry Error' => 'Data Entry Error',
  'Error in Final Report PDF' => 'Final Report PDF Compilation Error ',
  'Sponsor Request' => 'Sponsor Request  for Information Update',
  'Sponsor Request for Additional Analysis' => 'Sponsor Request for Additional Analysis',
  'Audit Finding' => 'Audit Finding',
);