<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Thirje e ftohtë',
  'Existing Customer' => 'Konsumator ekzitues',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Punëtorë',
  'Conference' => 'Konferencë',
  'Trade Show' => 'Ekspozimi i tregtisë',
  'Web Site' => 'Ueb faqe',
  'Email' => 'Email',
  'Campaign' => 'Fushata',
  'Support Portal User Registration' => 'Mbështetje Regjistrimi për përdoruesin e portalit.',
  'Other' => 'Tjetër',
  'Referral' => 'Referral',
);