<?php
 // created: 2018-11-20 15:07:49

$app_list_strings['lead_auditor_list']=array (
  'Choose Auditor' => 'Choose Auditor...',
  'Brenda Bailey' => 'Brenda Bailey',
  'Tammy Fossum' => 'Tammy Fossum',
  'Emily Markuson' => 'Emily Markuson',
  'Kristen Varas' => 'Kristen Varas',
  'None' => 'None',
  'BC Auditor Group' => 'BC Auditor Group',
  'Kevin Catalano' => 'Kevin Catalano',
  'Nicole Klee' => 'Nicole Klee',
  'External Contractor' => 'External Contractor',
  'Erica VanReeth' => 'Erica VanReeth',
  'Dani Zehowski' => 'Dani Zehowski',
  'Karen Bastyr' => 'Karen Bastyr',
  'Nick McCune' => 'Nick McCune',
  'Katie Jenkins' => 'Katie Jenkins',
  'Kris Ruppelius' => 'Kris Ruppelius',
  'Stephanie Beane' => 'Stephanie Beane',
  'Amy Malloy' => 'Amy Malloy',
);