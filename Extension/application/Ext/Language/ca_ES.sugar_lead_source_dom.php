<?php
 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Trucada en Fred',
  'Existing Customer' => 'Client Existent',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Empleat',
  'Conference' => 'Conferència',
  'Trade Show' => 'Exposició',
  'Web Site' => 'Lloc Web',
  'Email' => 'Qualsevol Correu',
  'Campaign' => 'Campanya',
  'Support Portal User Registration' => 'Registre d&#39;usuaris de portal suport',
  'Other' => 'Un altre',
  'Referral' => 'Referral',
);