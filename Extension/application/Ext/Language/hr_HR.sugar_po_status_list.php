<?php
 // created: 2022-02-22 07:07:34

$app_list_strings['po_status_list']=array (
  '' => '',
  'Pending' => 'Pending',
  'Submitted' => 'Submitted',
  'Complete' => 'Complete',
  'Cancelled' => 'Cancelled',
);