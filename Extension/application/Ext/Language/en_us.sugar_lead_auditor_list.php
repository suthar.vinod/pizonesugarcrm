<?php
 // created: 2018-11-20 15:07:49

$app_list_strings['lead_auditor_list']=array (
  'Choose Auditor' => 'Choose Auditor...',
  'Amy Malloy' => 'Amy Malloy',
  'Dani Zehowski' => 'Dani Zehowski',
  'Emily Markuson' => 'Emily Markuson',
  'Kristen Varas' => 'Kristen Varas',
  'Nick McCune' => 'Nick McCune',
  'Katie Jenkins' => 'Katie Jenkins',
  'Nicole Klee' => 'Nicole Klee',
  'Stephanie Beane' => 'Stephanie Beane',
  'Erica VanReeth' => 'Erica VanReeth',
  'Karen Bastyr' => 'Karen Bastyr',
  'Kris Ruppelius' => 'Kris Ruppelius',
  'Tammy Fossum' => 'Tammy Fossum',
  'BC Auditor Group' => 'BC Auditor Group',
  'External Contractor' => 'External Contractor',
  'None' => 'None',
  'Kevin Catalano' => 'Kevin Catalano',
  'Brenda Bailey' => 'Brenda Bailey',
);