<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['W_Weight'] = 'Weights';
$app_list_strings['moduleListSingular']['W_Weight'] = 'Weight';
$app_list_strings['yes_no_list']['No'] = 'No';
$app_list_strings['yes_no_list']['Yes'] = 'Yes';
$app_list_strings['yes_no_list'][''] = '';
$app_list_strings['reason_for_weight_list']['Receipt'] = 'Receipt';
$app_list_strings['reason_for_weight_list']['Protocol Assignment'] = 'Protocol Assignment';
$app_list_strings['reason_for_weight_list']['Substance Administration'] = 'Substance Administration';
$app_list_strings['reason_for_weight_list']['Monthly Monitoring'] = 'Monthly Monitoring';
$app_list_strings['reason_for_weight_list']['Veterinarian Order'] = 'Veterinarian Order';
$app_list_strings['reason_for_weight_list']['Housing Assessment'] = 'Housing Assessment';
$app_list_strings['reason_for_weight_list']['Feed Assessment'] = 'Feed Assessment';
$app_list_strings['reason_for_weight_list']['Ad Hoc'] = 'Ad Hoc';
$app_list_strings['reason_for_weight_list'][''] = '';
$app_list_strings['no_change_change_list']['Changed'] = 'Changed';
$app_list_strings['no_change_change_list']['No Change Required'] = 'No Change Required';
$app_list_strings['no_change_change_list'][''] = '';
$app_list_strings['yes_list']['Yes'] = 'Yes';
$app_list_strings['yes_list'][''] = '';
