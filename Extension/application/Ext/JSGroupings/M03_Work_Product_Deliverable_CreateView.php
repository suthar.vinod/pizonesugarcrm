<?php

//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/Work_Product_DeliverableCreateView/M03_Work_Product_Deliverable_CreateView.js'] = 'include/javascript/sugar_grp7.min.js';
        }

        break;
    }
}