<?php

//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/RemoveWorkProductsSubpanelButtons.js']   = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/RemoveIMSubpanelButtons.js']             = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/ExtendInventoryManagementCreateView.js'] = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/AddFlagToFilterIMSelectionRecords.js']   = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/HideUnnecessarySubpanels.js']            = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/IIHideLinkDropdown.js']                  = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/RemoveIISubpanelButtons.js']             = 'include/javascript/sugar_grp7.min.js';
        }
        break;
    }
}
