<?php 
 //WARNING: The contents of this file are auto-generated
$beanList['idoc_signers'] = 'idoc_signers';
$beanFiles['idoc_signers'] = 'modules/idoc_signers/idoc_signers.php';
$modules_exempt_from_availability_check['idoc_signers'] = 'idoc_signers';
$report_include_modules['idoc_signers'] = 'idoc_signers';
$modInvisList[] = 'idoc_signers';
$beanList['idoc_events'] = 'idoc_events';
$beanFiles['idoc_events'] = 'modules/idoc_events/idoc_events.php';
$modules_exempt_from_availability_check['idoc_events'] = 'idoc_events';
$report_include_modules['idoc_events'] = 'idoc_events';
$modInvisList[] = 'idoc_events';
$beanList['idoc_documents'] = 'idoc_documents';
$beanFiles['idoc_documents'] = 'modules/idoc_documents/idoc_documents.php';
$modules_exempt_from_availability_check['idoc_documents'] = 'idoc_documents';
$report_include_modules['idoc_documents'] = 'idoc_documents';
$modInvisList[] = 'idoc_documents';
$beanList['idoc_templates'] = 'idoc_templates';
$beanFiles['idoc_templates'] = 'modules/idoc_templates/idoc_templates.php';
$modules_exempt_from_availability_check['idoc_templates'] = 'idoc_templates';
$report_include_modules['idoc_templates'] = 'idoc_templates';
$modInvisList[] = 'idoc_templates';

?>