<?php 
 //WARNING: The contents of this file are auto-generated
$beanList['M03_Additional_WP_Personnel'] = 'M03_Additional_WP_Personnel';
$beanFiles['M03_Additional_WP_Personnel'] = 'modules/M03_Additional_WP_Personnel/M03_Additional_WP_Personnel.php';
$moduleList[] = 'M03_Additional_WP_Personnel';
$beanList['M03_Work_Product_Facility'] = 'M03_Work_Product_Facility';
$beanFiles['M03_Work_Product_Facility'] = 'modules/M03_Work_Product_Facility/M03_Work_Product_Facility.php';
$moduleList[] = 'M03_Work_Product_Facility';
$beanList['M03_Work_Product_Code'] = 'M03_Work_Product_Code';
$beanFiles['M03_Work_Product_Code'] = 'modules/M03_Work_Product_Code/M03_Work_Product_Code.php';
$moduleList[] = 'M03_Work_Product_Code';
$beanList['M03_Additional_WP_IDs'] = 'M03_Additional_WP_IDs';
$beanFiles['M03_Additional_WP_IDs'] = 'modules/M03_Additional_WP_IDs/M03_Additional_WP_IDs.php';
$moduleList[] = 'M03_Additional_WP_IDs';
$beanList['M03_Work_Product'] = 'M03_Work_Product';
$beanFiles['M03_Work_Product'] = 'modules/M03_Work_Product/M03_Work_Product.php';
$moduleList[] = 'M03_Work_Product';
$beanList['M03_Work_Product_Deliverable'] = 'M03_Work_Product_Deliverable';
$beanFiles['M03_Work_Product_Deliverable'] = 'custom/modules/M03_Work_Product_Deliverable/M03_Work_Product_Deliverable.php';
$moduleList[] = 'M03_Work_Product_Deliverable';

?>