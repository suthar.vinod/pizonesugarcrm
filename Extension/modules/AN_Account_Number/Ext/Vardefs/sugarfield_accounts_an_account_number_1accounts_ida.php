<?php
 // created: 2021-11-02 17:55:38
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['name']='accounts_an_account_number_1accounts_ida';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['type']='id';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['source']='non-db';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['vname']='LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_AN_ACCOUNT_NUMBER_TITLE_ID';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['id_name']='accounts_an_account_number_1accounts_ida';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['link']='accounts_an_account_number_1';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['table']='accounts';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['module']='Accounts';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['rname']='id';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['reportable']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['side']='right';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['massupdate']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['duplicate_merge']='disabled';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['hideacl']=true;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['audited']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['importable']='true';

 ?>