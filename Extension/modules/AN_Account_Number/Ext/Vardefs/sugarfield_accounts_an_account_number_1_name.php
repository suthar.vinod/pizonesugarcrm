<?php
 // created: 2021-11-02 17:55:41
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['required']=true;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['audited']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['massupdate']=true;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['hidemassupdate']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['duplicate_merge']='enabled';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['duplicate_merge_dom_value']='1';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['merge_filter']='disabled';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['reportable']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['unified_search']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['calculated']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['vname']='LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_NAME_FIELD_TITLE';

 ?>