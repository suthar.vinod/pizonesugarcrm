<?php
 // created: 2021-12-14 09:03:29
$dictionary['AN_Account_Number']['fields']['account_number_email_address_c']['labelValue']='Account Number Email Address';
$dictionary['AN_Account_Number']['fields']['account_number_email_address_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['AN_Account_Number']['fields']['account_number_email_address_c']['enforced']='';
$dictionary['AN_Account_Number']['fields']['account_number_email_address_c']['dependency']='';
$dictionary['AN_Account_Number']['fields']['account_number_email_address_c']['required_formula']='';
$dictionary['AN_Account_Number']['fields']['account_number_email_address_c']['readonly_formula']='';

 ?>