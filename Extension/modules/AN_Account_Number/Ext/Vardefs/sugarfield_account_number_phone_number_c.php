<?php
 // created: 2021-12-14 08:58:08
$dictionary['AN_Account_Number']['fields']['account_number_phone_number_c']['labelValue']='Account Number Phone Number';
$dictionary['AN_Account_Number']['fields']['account_number_phone_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['AN_Account_Number']['fields']['account_number_phone_number_c']['enforced']='';
$dictionary['AN_Account_Number']['fields']['account_number_phone_number_c']['dependency']='';
$dictionary['AN_Account_Number']['fields']['account_number_phone_number_c']['required_formula']='';
$dictionary['AN_Account_Number']['fields']['account_number_phone_number_c']['readonly_formula']='';

 ?>