<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_AN_ACCOUNT_NUMBER_RECORD_DASHBOARD'] = 'Account Numbers Record Dashboard';
$mod_strings['LBL_ACCOUNT_NUMBER_ADDRESS_C_CA_COMPANY_ADDRESS_ID'] = 'Account Number Address (related Company Address ID)';
$mod_strings['LBL_ACCOUNT_NUMBER_ADDRESS'] = 'Account Number Address';
$mod_strings['LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_NAME_FIELD_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNT_NUMBER_PHONE_NUMBER'] = 'Account Number Phone Number';
$mod_strings['LBL_ACCOUNT_NUMBER_EMAIL_ADDRESS'] = 'Account Number Email Address';
