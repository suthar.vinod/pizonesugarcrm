<?php
 // created: 2021-12-07 12:25:29
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['importable']='false';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['duplicate_merge']='disabled';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['merge_filter']='disabled';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['unified_search']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['calculated']='true';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['formula']='$company_name';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['enforced']=true;

 ?>