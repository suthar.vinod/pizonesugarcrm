<?php
 // created: 2021-12-07 12:23:20
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['audited']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['massupdate']=true;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['hidemassupdate']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['duplicate_merge']='enabled';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['duplicate_merge_dom_value']='1';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['merge_filter']='disabled';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['reportable']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['unified_search']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['calculated']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['vname']='LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_NAME_FIELD_TITLE';

 ?>