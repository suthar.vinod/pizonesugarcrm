<?php
// created: 2021-12-07 12:21:22
$dictionary["NSC_NAMSA_Sub_Companies"]["fields"]["accounts_nsc_namsa_sub_companies_1"] = array (
  'name' => 'accounts_nsc_namsa_sub_companies_1',
  'type' => 'link',
  'relationship' => 'accounts_nsc_namsa_sub_companies_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE',
  'id_name' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["NSC_NAMSA_Sub_Companies"]["fields"]["accounts_nsc_namsa_sub_companies_1_name"] = array (
  'name' => 'accounts_nsc_namsa_sub_companies_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
  'link' => 'accounts_nsc_namsa_sub_companies_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["NSC_NAMSA_Sub_Companies"]["fields"]["accounts_nsc_namsa_sub_companies_1accounts_ida"] = array (
  'name' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE_ID',
  'id_name' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
  'link' => 'accounts_nsc_namsa_sub_companies_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
