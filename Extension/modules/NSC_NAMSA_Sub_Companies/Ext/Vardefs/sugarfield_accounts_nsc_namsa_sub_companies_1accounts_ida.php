<?php
 // created: 2021-12-07 12:23:18
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['name']='accounts_nsc_namsa_sub_companies_1accounts_ida';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['type']='id';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['source']='non-db';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['vname']='LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE_ID';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['id_name']='accounts_nsc_namsa_sub_companies_1accounts_ida';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['link']='accounts_nsc_namsa_sub_companies_1';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['table']='accounts';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['module']='Accounts';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['rname']='id';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['reportable']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['side']='right';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['massupdate']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['duplicate_merge']='disabled';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['hideacl']=true;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['audited']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['importable']='true';

 ?>