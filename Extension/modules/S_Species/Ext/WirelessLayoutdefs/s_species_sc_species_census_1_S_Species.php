<?php
 // created: 2021-04-29 06:58:32
$layout_defs["S_Species"]["subpanel_setup"]['s_species_sc_species_census_1'] = array (
  'order' => 100,
  'module' => 'SC_Species_Census',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_SC_SPECIES_CENSUS_TITLE',
  'get_subpanel_data' => 's_species_sc_species_census_1',
);
