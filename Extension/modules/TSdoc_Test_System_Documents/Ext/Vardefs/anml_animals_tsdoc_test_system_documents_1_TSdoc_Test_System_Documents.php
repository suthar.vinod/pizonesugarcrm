<?php
// created: 2019-09-30 12:27:41
$dictionary["TSdoc_Test_System_Documents"]["fields"]["anml_animals_tsdoc_test_system_documents_1"] = array (
  'name' => 'anml_animals_tsdoc_test_system_documents_1',
  'type' => 'link',
  'relationship' => 'anml_animals_tsdoc_test_system_documents_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_TSDOC_TEST_SYSTEM_DOCUMENTS_TITLE',
  'id_name' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["TSdoc_Test_System_Documents"]["fields"]["anml_animals_tsdoc_test_system_documents_1_name"] = array (
  'name' => 'anml_animals_tsdoc_test_system_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
  'link' => 'anml_animals_tsdoc_test_system_documents_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["TSdoc_Test_System_Documents"]["fields"]["anml_animals_tsdoc_test_system_documents_1anml_animals_ida"] = array (
  'name' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_TSDOC_TEST_SYSTEM_DOCUMENTS_TITLE_ID',
  'id_name' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
  'link' => 'anml_animals_tsdoc_test_system_documents_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
