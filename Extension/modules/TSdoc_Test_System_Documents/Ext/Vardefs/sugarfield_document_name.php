<?php
 // created: 2019-09-30 12:29:29
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['audited']=true;
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['massupdate']=false;
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['merge_filter']='disabled';
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['unified_search']=false;
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['calculated']=false;

 ?>