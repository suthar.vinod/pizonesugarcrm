<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number to delete the Company.';
$mod_strings['LBL_ACCOUNT_ID'] = 'Company ID:';
$mod_strings['LBL_OPPORTUNITY_ID'] = 'Opportunity ID:';
$mod_strings['LBL_MEETINGS_NOTES_FROM_MEETINGS_TITLE'] = 'Meetings';
$mod_strings['LBL_MEETINGS_NOTES_FROM_NOTES_TITLE'] = 'Meetings';
