<?php
// created: 2017-05-11 22:18:50
$dictionary["Meeting"]["fields"]["meetings_an01_activity_notes_1"] = array (
  'name' => 'meetings_an01_activity_notes_1',
  'type' => 'link',
  'relationship' => 'meetings_an01_activity_notes_1',
  'source' => 'non-db',
  'module' => 'AN01_Activity_Notes',
  'bean_name' => 'AN01_Activity_Notes',
  'vname' => 'LBL_MEETINGS_AN01_ACTIVITY_NOTES_1_FROM_MEETINGS_TITLE',
  'id_name' => 'meetings_an01_activity_notes_1meetings_ida',
  'link-type' => 'many',
  'side' => 'left',
);
