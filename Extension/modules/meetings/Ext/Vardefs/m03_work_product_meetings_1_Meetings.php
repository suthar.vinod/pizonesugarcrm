<?php
// created: 2017-05-02 19:55:40
$dictionary["Meeting"]["fields"]["m03_work_product_meetings_1"] = array (
  'name' => 'm03_work_product_meetings_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_meetings_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE',
  'id_name' => 'm03_work_product_meetings_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["Meeting"]["fields"]["m03_work_product_meetings_1_name"] = array (
  'name' => 'm03_work_product_meetings_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_meetings_1m03_work_product_ida',
  'link' => 'm03_work_product_meetings_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["Meeting"]["fields"]["m03_work_product_meetings_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_meetings_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE_ID',
  'id_name' => 'm03_work_product_meetings_1m03_work_product_ida',
  'link' => 'm03_work_product_meetings_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
