<?php
 // created: 2021-01-12 10:38:08
$dictionary['Meeting']['fields']['business_develop_activity_c']['labelValue']='Business Development Activity';
$dictionary['Meeting']['fields']['business_develop_activity_c']['dependency']='';
$dictionary['Meeting']['fields']['business_develop_activity_c']['required_formula']='';
$dictionary['Meeting']['fields']['business_develop_activity_c']['visibility_grid']=array (
  'trigger' => 'activity_personnel_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Business Development' => 
    array (
      0 => '',
      1 => 'Conference Call',
      2 => 'Client_Visit_Onsite',
      3 => 'Client_Visit_Offsite',
      4 => 'Tradeshow',
      5 => 'Procedure Coverage',
      6 => 'Other',
    ),
    'Scientific' => 
    array (
    ),
    'Quality Assurance' => 
    array (
    ),
    'Pathology' => 
    array (
    ),
    'Analytical' => 
    array (
    ),
    'Regulatory' => 
    array (
    ),
  ),
);

 ?>