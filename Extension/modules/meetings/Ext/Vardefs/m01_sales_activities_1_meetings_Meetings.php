<?php
// created: 2018-12-10 23:02:13
$dictionary["Meeting"]["fields"]["m01_sales_activities_1_meetings"] = array (
  'name' => 'm01_sales_activities_1_meetings',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_meetings',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_MEETINGS_FROM_M01_SALES_TITLE',
);
