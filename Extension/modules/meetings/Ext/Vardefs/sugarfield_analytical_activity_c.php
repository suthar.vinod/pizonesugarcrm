<?php
 // created: 2021-01-12 10:36:36
$dictionary['Meeting']['fields']['analytical_activity_c']['labelValue']='Analytical Activity';
$dictionary['Meeting']['fields']['analytical_activity_c']['dependency']='';
$dictionary['Meeting']['fields']['analytical_activity_c']['required_formula']='';
$dictionary['Meeting']['fields']['analytical_activity_c']['visibility_grid']=array (
  'trigger' => 'activity_personnel_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Business Development' => 
    array (
    ),
    'Scientific' => 
    array (
    ),
    'Quality Assurance' => 
    array (
    ),
    'Pathology' => 
    array (
    ),
    'Analytical' => 
    array (
      0 => '',
      1 => 'Analytical Report Writing',
      2 => 'Data Analysis',
      3 => 'Data Delivery To Toxicologist',
      4 => 'Exhaustive exaggerated extraction start',
      5 => 'Instrument Maintenance',
      6 => 'Method Development',
      7 => 'Protocol Development',
      8 => 'Sample Analysis',
      9 => 'Sample Prep',
      10 => 'Simulated Use Extraction Start',
      11 => 'Out of Office',
    ),
    'Regulatory' => 
    array (
    ),
  ),
);

 ?>