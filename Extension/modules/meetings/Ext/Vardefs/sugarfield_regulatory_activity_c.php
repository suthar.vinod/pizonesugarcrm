<?php
 // created: 2021-01-12 10:39:54
$dictionary['Meeting']['fields']['regulatory_activity_c']['labelValue']='Regulatory Activity';
$dictionary['Meeting']['fields']['regulatory_activity_c']['dependency']='';
$dictionary['Meeting']['fields']['regulatory_activity_c']['required_formula']='';
$dictionary['Meeting']['fields']['regulatory_activity_c']['visibility_grid']=array (
  'trigger' => 'activity_personnel_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Business Development' => 
    array (
    ),
    'Scientific' => 
    array (
    ),
    'Quality Assurance' => 
    array (
    ),
    'Pathology' => 
    array (
    ),
    'Analytical' => 
    array (
    ),
    'Regulatory' => 
    array (
      0 => '',
      1 => 'Regulatory Consulting',
      2 => 'FDA Response Writing',
    ),
  ),
);

 ?>