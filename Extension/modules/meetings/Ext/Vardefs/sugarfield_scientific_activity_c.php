<?php
 // created: 2021-01-26 17:00:44
$dictionary['Meeting']['fields']['scientific_activity_c']['labelValue']='Scientific Activity';
$dictionary['Meeting']['fields']['scientific_activity_c']['dependency']='';
$dictionary['Meeting']['fields']['scientific_activity_c']['required_formula']='';
$dictionary['Meeting']['fields']['scientific_activity_c']['visibility_grid']=array (
  'trigger' => 'activity_personnel_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Business Development' => 
    array (
    ),
    'Scientific' => 
    array (
      0 => '',
      1 => 'Protocol Development',
      2 => 'Reporting',
      3 => 'Operations Oversight',
    ),
    'Quality Assurance' => 
    array (
    ),
    'Pathology' => 
    array (
    ),
    'Analytical' => 
    array (
    ),
    'Regulatory' => 
    array (
    ),
  ),
);

 ?>