<?php
 // created: 2021-01-12 10:39:26
$dictionary['Meeting']['fields']['pathology_activity_c']['labelValue']='Pathology Activity';
$dictionary['Meeting']['fields']['pathology_activity_c']['dependency']='';
$dictionary['Meeting']['fields']['pathology_activity_c']['required_formula']='';
$dictionary['Meeting']['fields']['pathology_activity_c']['visibility_grid']=array (
  'trigger' => 'activity_personnel_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Business Development' => 
    array (
    ),
    'Scientific' => 
    array (
    ),
    'Quality Assurance' => 
    array (
    ),
    'Pathology' => 
    array (
      0 => '',
      1 => 'Necropsy',
      2 => 'Faxitron',
      3 => 'Tissue Receipt',
      4 => 'Tissue Trimming',
      5 => 'Tissue Shipping',
      6 => 'Paraffin Embedding',
      7 => 'Plastic Embedding',
      8 => 'Slide Completion',
      9 => 'Slide Shipping',
      10 => 'SEM Imaging',
      11 => 'Histomorphometry',
      12 => 'Slide Review',
      13 => 'Gross Necropsy Report Writing',
      14 => 'Pathology Report Writing',
      15 => 'Out of Office',
    ),
    'Analytical' => 
    array (
    ),
    'Regulatory' => 
    array (
    ),
  ),
);

 ?>