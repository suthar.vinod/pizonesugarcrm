<?php
 // created: 2021-01-12 10:37:30
$dictionary['Meeting']['fields']['analytical_equipment_list_c']['labelValue']='Analytical Equipment List';
$dictionary['Meeting']['fields']['analytical_equipment_list_c']['dependency']='';
$dictionary['Meeting']['fields']['analytical_equipment_list_c']['required_formula']='';
$dictionary['Meeting']['fields']['analytical_equipment_list_c']['visibility_grid']=array (
  'trigger' => 'analytical_activity_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Analytical Report Writing' => 
    array (
    ),
    'Data Analysis' => 
    array (
    ),
    'Instrument Maintenance' => 
    array (
      0 => '',
      1 => 'ISQ LT',
      2 => 'TSQ Quantiva 1',
      3 => 'TSQ Quantiva 2',
      4 => 'QE Focus',
      5 => 'Nicolet iS10',
      6 => 'ASE350',
      7 => 'Agilent 7500 CS',
      8 => 'Precellys Evolution',
      9 => 'Cryolys',
    ),
    'Method Development' => 
    array (
    ),
    'Protocol Development' => 
    array (
    ),
    'Sample Analysis' => 
    array (
    ),
    'Sample Prep' => 
    array (
    ),
    'Out of Office' => 
    array (
    ),
  ),
);

 ?>