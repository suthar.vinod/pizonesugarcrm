<?php
 // created: 2021-01-12 10:40:30
$dictionary['Meeting']['fields']['report_audit_page_total_c']['labelValue']='Report Audit Page Total';
$dictionary['Meeting']['fields']['report_audit_page_total_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['report_audit_page_total_c']['enforced']='';
$dictionary['Meeting']['fields']['report_audit_page_total_c']['dependency']='equal($quality_assurance_activity_c,"Report Audit")';
$dictionary['Meeting']['fields']['report_audit_page_total_c']['required_formula']='';

 ?>