<?php
 // created: 2022-02-01 07:34:15
$dictionary['Meeting']['fields']['quality_assurance_activity_c']['labelValue']='Quality Assurance Audit Activity';
$dictionary['Meeting']['fields']['quality_assurance_activity_c']['dependency']='';
$dictionary['Meeting']['fields']['quality_assurance_activity_c']['required_formula']='';
$dictionary['Meeting']['fields']['quality_assurance_activity_c']['readonly_formula']='';
$dictionary['Meeting']['fields']['quality_assurance_activity_c']['visibility_grid']=array (
  'trigger' => 'activity_personnel_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Business Development' => 
    array (
    ),
    'Scientific' => 
    array (
    ),
    'Quality Assurance' => 
    array (
      0 => '',
      1 => 'Application',
      2 => 'Analytical',
      3 => 'Animal Health Report',
      4 => 'Blood Collection',
      5 => 'Body Weights Observations',
      6 => 'Cell Processing',
      7 => 'Colony Counting',
      8 => 'Contributing Scientist Report',
      9 => 'Controlled Document Review',
      10 => 'Data Review',
      11 => 'Dose',
      12 => 'Evaluation',
      13 => 'Faxitron',
      14 => 'Final Report',
      15 => 'Followup Procedure',
      16 => 'Histology',
      17 => 'Implant',
      18 => 'In Life',
      19 => 'In_Life Report',
      20 => 'Interim M_M report',
      21 => 'Interim Pathology Report',
      22 => 'Interim Report',
      23 => 'Model Creation',
      24 => 'Morphometry',
      25 => 'Necropsy',
      26 => 'Pathology Report',
      27 => 'Protocol Amendment',
      28 => 'Protocol Review',
      29 => 'Final Report Amendment',
      30 => 'Sample Preparation',
      31 => 'Sample Receipt',
      32 => 'SEM Report',
      33 => 'Staining',
      34 => 'Terminal Procedure',
      35 => 'Tissue Trimming',
      36 => 'Training CPI',
      37 => 'Training Second Review',
      38 => 'Treatment',
      39 => 'Other_Non Study Specific',
      40 => 'Out of Office',
    ),
    'Pathology' => 
    array (
    ),
    'Analytical' => 
    array (
    ),
    'Regulatory' => 
    array (
    ),
  ),
);

 ?>