<?php
 // created: 2017-05-11 22:18:50
$layout_defs["Meetings"]["subpanel_setup"]['meetings_an01_activity_notes_1'] = array (
  'order' => 100,
  'module' => 'AN01_Activity_Notes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_MEETINGS_AN01_ACTIVITY_NOTES_1_FROM_AN01_ACTIVITY_NOTES_TITLE',
  'get_subpanel_data' => 'meetings_an01_activity_notes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
