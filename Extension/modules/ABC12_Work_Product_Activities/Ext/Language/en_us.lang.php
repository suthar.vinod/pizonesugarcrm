<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACTIVITY_TIME'] = 'Activity Time (Hours)';
$mod_strings['LNK_NEW_RECORD'] = 'Create Time Keeping';
$mod_strings['LNK_LIST'] = 'View Time Keeping';
$mod_strings['LBL_MODULE_NAME'] = 'Time Keeping';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Time Keeping';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Time Keeping';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Time Keeping vCard';
$mod_strings['LNK_IMPORT_ABC12_WORK_PRODUCT_ACTIVITIES'] = 'Import Time Keeping';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Time Keeping List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Time Keeping';
$mod_strings['LBL_ABC12_WORK_PRODUCT_ACTIVITIES_SUBPANEL_TITLE'] = 'Time Keeping';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Time Keeping';
$mod_strings['LBL_ACTIVITY'] = 'Activity';
$mod_strings['LBL_ACTIVITY_DATE'] = 'Activity Date';
$mod_strings['LBL_ACTIVITY_TIME_QUARTER_HOUR'] = 'Activity Time (By Quarter Hour)';
$mod_strings['LBL_ABC12_WORK_PRODUCT_ACTIVITIES_FOCUS_DRAWER_DASHBOARD'] = 'Time Keeping Focus Drawer';
