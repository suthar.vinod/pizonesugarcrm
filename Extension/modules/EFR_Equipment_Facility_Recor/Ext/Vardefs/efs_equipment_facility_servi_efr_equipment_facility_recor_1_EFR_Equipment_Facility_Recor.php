<?php
// created: 2019-02-25 15:18:39
$dictionary["EFR_Equipment_Facility_Recor"]["fields"]["efs_equipment_facility_servi_efr_equipment_facility_recor_1"] = array (
  'name' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'type' => 'link',
  'relationship' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'source' => 'non-db',
  'module' => 'EFS_Equipment_Facility_Servi',
  'bean_name' => 'EFS_Equipment_Facility_Servi',
  'side' => 'right',
  'vname' => 'LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE',
  'id_name' => 'efs_equipmd3d1y_servi_ida',
  'link-type' => 'one',
);
$dictionary["EFR_Equipment_Facility_Recor"]["fields"]["efs_equipment_facility_servi_efr_equipment_facility_recor_1_name"] = array (
  'name' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE',
  'save' => true,
  'id_name' => 'efs_equipmd3d1y_servi_ida',
  'link' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'table' => 'efs_equipment_facility_servi',
  'module' => 'EFS_Equipment_Facility_Servi',
  'rname' => 'name',
);
$dictionary["EFR_Equipment_Facility_Recor"]["fields"]["efs_equipmd3d1y_servi_ida"] = array (
  'name' => 'efs_equipmd3d1y_servi_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE_ID',
  'id_name' => 'efs_equipmd3d1y_servi_ida',
  'link' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'table' => 'efs_equipment_facility_servi',
  'module' => 'EFS_Equipment_Facility_Servi',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
