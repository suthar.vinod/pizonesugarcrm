<?php
 // created: 2019-03-01 18:33:43
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['audited']=true;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['massupdate']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['merge_filter']='disabled';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['unified_search']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['calculated']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['required']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['readonly']=true;

 ?>