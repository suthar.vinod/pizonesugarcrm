<?php
 // created: 2019-06-05 11:52:53
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['audited']=true;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['massupdate']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['comments']='Full text of the note';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['duplicate_merge']='enabled';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['merge_filter']='disabled';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['unified_search']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['calculated']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['rows']='6';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['cols']='80';

 ?>