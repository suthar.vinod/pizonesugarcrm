<?php
// created: 2019-02-20 15:09:02
$dictionary["EFR_Equipment_Facility_Recor"]["fields"]["equip_equipment_efr_equipment_facility_recor_2"] = array (
  'name' => 'equip_equipment_efr_equipment_facility_recor_2',
  'type' => 'link',
  'relationship' => 'equip_equipment_efr_equipment_facility_recor_2',
  'source' => 'non-db',
  'module' => 'Equip_Equipment',
  'bean_name' => 'Equip_Equipment',
  'side' => 'right',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE',
  'id_name' => 'equip_equi01e2uipment_ida',
  'link-type' => 'one',
);
$dictionary["EFR_Equipment_Facility_Recor"]["fields"]["equip_equipment_efr_equipment_facility_recor_2_name"] = array (
  'name' => 'equip_equipment_efr_equipment_facility_recor_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EQUIP_EQUIPMENT_TITLE',
  'save' => true,
  'id_name' => 'equip_equi01e2uipment_ida',
  'link' => 'equip_equipment_efr_equipment_facility_recor_2',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'name',
);
$dictionary["EFR_Equipment_Facility_Recor"]["fields"]["equip_equi01e2uipment_ida"] = array (
  'name' => 'equip_equi01e2uipment_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE_ID',
  'id_name' => 'equip_equi01e2uipment_ida',
  'link' => 'equip_equipment_efr_equipment_facility_recor_2',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
