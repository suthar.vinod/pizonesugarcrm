<?php
 // created: 2020-10-06 06:43:39
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['audited']=true;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['massupdate']=true;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['options']='equipment_and_facilities_status_list';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['duplicate_merge']='enabled';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['duplicate_merge_dom_value']='1';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['merge_filter']='disabled';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['unified_search']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['calculated']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['dependency']='or(equal(related($equip_equipment_efr_equipment_facility_recor_2,"classification_check_c"),"1"),and(equal(related($equip_equipment_efr_equipment_facility_recor_2,"department_c"),"Facilities"),not(equal(related($equip_equipment_efr_equipment_facility_recor_2,"qualification_required_c"),"1"))),greaterThan(strlen($status_id),0))';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['required']=true;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['reportable']=true;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['hidemassupdate']=false;

 ?>