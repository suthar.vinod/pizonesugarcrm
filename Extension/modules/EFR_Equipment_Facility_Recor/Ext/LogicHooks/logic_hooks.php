<?php

$hook_array['after_save'][] = Array(1,
    'Generating system id for E&FR',
    'custom/modules/EFR_Equipment_Facility_Recor/EFRecordCustomLogicHook.php',
    'EFRecordCustomLogicHook',
    'generateSystemId',);

$hook_array['after_relationship_add'][] = Array(1,
    'Update status and software fields in E&F and last service date in E&FS',
    'custom/modules/EFR_Equipment_Facility_Recor/EFRecordCustomLogicHook.php',
    'EFRecordCustomLogicHook',
    'generateSystemId',);

/*$hook_array['before_relationship_delete'][] = Array(1,
    'Update status and software fields in E&F and last service date in E&FS',
    'custom/modules/EFR_Equipment_Facility_Recor/EFRecordCustomLogicHook.php',
    'EFRecordCustomLogicHook',
    'generateSystemId',);

$hook_array['before_delete'][] = Array(1,
    'Update status and software fields in E&F and last service date in E&FS',
    'custom/modules/EFR_Equipment_Facility_Recor/EFRecordCustomLogicHook.php',
    'EFRecordCustomLogicHook',
    'generateSystemId',);*/
	
	
$hook_array['after_relationship_delete'][] = Array(1,
    'Update status and software fields in E&F and last service date in E&FS',
    'custom/modules/EFR_Equipment_Facility_Recor/EFRecordCustomLogicHook.php',
    'EFRecordCustomLogicHook',
    'setServiceDate',);		

?>