<?php
$viewdefs['SP_Service_Pricing']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterSPPrice',
    'name' => 'FilterFormat',
    'filter_definition' => array(
        array(
            'format' => array(
                '$in' => array('Each','Hourly','Task'),
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);