<?php
// created: 2021-01-12 11:07:14
$dictionary["SP_Service_Pricing"]["fields"]["sp_service_pricing_sp_service_pricing_1"] = array (
  'name' => 'sp_service_pricing_sp_service_pricing_1',
  'type' => 'link',
  'relationship' => 'sp_service_pricing_sp_service_pricing_1',
  'source' => 'non-db',
  'module' => 'SP_Service_Pricing',
  'bean_name' => 'SP_Service_Pricing',
  'vname' => 'LBL_SP_SERVICE_PRICING_SP_SERVICE_PRICING_1_FROM_SP_SERVICE_PRICING_L_TITLE',
  'id_name' => 'sp_service_pricing_sp_service_pricing_1sp_service_pricing_ida',
);
$dictionary["SP_Service_Pricing"]["fields"]["sp_service_pricing_sp_service_pricing_1"] = array (
  'name' => 'sp_service_pricing_sp_service_pricing_1',
  'type' => 'link',
  'relationship' => 'sp_service_pricing_sp_service_pricing_1',
  'source' => 'non-db',
  'module' => 'SP_Service_Pricing',
  'bean_name' => 'SP_Service_Pricing',
  'vname' => 'LBL_SP_SERVICE_PRICING_SP_SERVICE_PRICING_1_FROM_SP_SERVICE_PRICING_R_TITLE',
  'id_name' => 'sp_service_pricing_sp_service_pricing_1sp_service_pricing_ida',
);
