<?php
 // created: 2021-09-04 04:30:27
$dictionary['SP_Service_Pricing']['fields']['price']['required']=true;
$dictionary['SP_Service_Pricing']['fields']['price']['name']='price';
$dictionary['SP_Service_Pricing']['fields']['price']['vname']='LBL_PRICE';
$dictionary['SP_Service_Pricing']['fields']['price']['type']='currency';
$dictionary['SP_Service_Pricing']['fields']['price']['massupdate']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['hidemassupdate']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['no_default']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['comments']='';
$dictionary['SP_Service_Pricing']['fields']['price']['help']='';
$dictionary['SP_Service_Pricing']['fields']['price']['importable']='true';
$dictionary['SP_Service_Pricing']['fields']['price']['duplicate_merge']='enabled';
$dictionary['SP_Service_Pricing']['fields']['price']['duplicate_merge_dom_value']='1';
$dictionary['SP_Service_Pricing']['fields']['price']['audited']=true;
$dictionary['SP_Service_Pricing']['fields']['price']['reportable']=true;
$dictionary['SP_Service_Pricing']['fields']['price']['unified_search']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['merge_filter']='disabled';
$dictionary['SP_Service_Pricing']['fields']['price']['pii']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['default']='';
$dictionary['SP_Service_Pricing']['fields']['price']['calculated']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['len']=26;
$dictionary['SP_Service_Pricing']['fields']['price']['size']='20';
$dictionary['SP_Service_Pricing']['fields']['price']['enable_range_search']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['precision']=6;
$dictionary['SP_Service_Pricing']['fields']['price']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['SP_Service_Pricing']['fields']['price']['convertToBase']=true;
$dictionary['SP_Service_Pricing']['fields']['price']['showTransactionalAmount']=true;

 ?>