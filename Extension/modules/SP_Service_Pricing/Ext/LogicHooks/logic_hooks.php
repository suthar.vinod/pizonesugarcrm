<?php

   $hook_version = 1;
   $hook_array = Array();

   $hook_array['before_save'] = Array();
   $hook_array['before_save'][] = Array(
      1, 
      'Generate Internal Barcode', 
      'custom/modules/SP_Service_Pricing/Internal_barcode.php', 
      'Internal_barcode_class', 
      'UpdateInternal_barcodeHook'
   );
   
   $hook_array['before_save'][] = Array(
    10,
    'Add Price for all Related Service Price',
    'custom/modules/SP_Service_Pricing/checkSubpanelPrice.php',
    'checkSubpanelPrice',
    'checkPriceBeforeSave',
    );
    
    $hook_array['after_save'][] = Array(
        11,
        'Add Price for all Related Service Price',
        'custom/modules/SP_Service_Pricing/checkSubpanelPrice.php',
        'checkSubpanelPrice',
        'checkPriceAfterSave',
    );
	
	$hook_array['after_relationship_add'][] = Array(
		12,
		'Add Price for all Related Service Price',
		'custom/modules/SP_Service_Pricing/checkSubpanelPrice.php',
		'checkSubpanelPrice',
		'checkPrice1',
	); 
	
	$hook_array['after_relationship_delete'][] = Array(
		13,
		'Add Price for all Related Service Price',
		'custom/modules/SP_Service_Pricing/checkSubpanelPrice.php',
		'checkSubpanelPrice',
		'checkPrice1',
	); 

?>