<?php
 // created: 2016-10-25 03:27:04
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['len'] = '255';
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['audited'] = true;
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['massupdate'] = false;
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['unified_search'] = false;
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['full_text_search']['boost'] = 1.5500000000000000444089209850062616169452667236328125;
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['calculated'] = false;

