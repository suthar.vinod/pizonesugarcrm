<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'System ID (SA Quote)';
$mod_strings['LBL_QUOTE_DATE'] = 'Quote Date';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_QUOTE_AMOUNT'] = 'Quote Amount';
$mod_strings['LBL_M01_SALES_ACTIVITY_QUOTE_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote &amp; Study Documents';
$mod_strings['LNK_NEW_RECORD'] = 'Create Sales Activity Quote';
$mod_strings['LNK_LIST'] = 'View Inactive_Inactive) Sales Quotes';
$mod_strings['LBL_MODULE_NAME'] = 'Activity Quotes';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Inactive_(Inactive) Sales Quote';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Inactive_(Inactive) Sales Quote';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Inactive_(Inactive) Sales Quote vCard';
$mod_strings['LNK_IMPORT_M01_SALES_ACTIVITY_QUOTE'] = 'Import Sales Activity Quote';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Inactive_(Inactive) Sales Quotes List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Inactive_(Inactive) Sales Quote';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Inactive_Inactive_Inactive) Sales Quotes';
$mod_strings['LBL_M01_SALES_M01_SALES_ACTIVITY_QUOTE_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';
$mod_strings['LBL_M01_SALES_ACTIVITY_QUOTE_FOCUS_DRAWER_DASHBOARD'] = 'Activity Quotes Focus Drawer';
