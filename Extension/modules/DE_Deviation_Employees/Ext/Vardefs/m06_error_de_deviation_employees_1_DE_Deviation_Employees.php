<?php
// created: 2018-07-30 20:17:15
$dictionary["DE_Deviation_Employees"]["fields"]["m06_error_de_deviation_employees_1"] = array (
  'name' => 'm06_error_de_deviation_employees_1',
  'type' => 'link',
  'relationship' => 'm06_error_de_deviation_employees_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'side' => 'right',
  'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'id_name' => 'm06_error_de_deviation_employees_1m06_error_ida',
  'link-type' => 'one',
);
$dictionary["DE_Deviation_Employees"]["fields"]["m06_error_de_deviation_employees_1_name"] = array (
  'name' => 'm06_error_de_deviation_employees_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_M06_ERROR_TITLE',
  'save' => true,
  'id_name' => 'm06_error_de_deviation_employees_1m06_error_ida',
  'link' => 'm06_error_de_deviation_employees_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'name',
);
$dictionary["DE_Deviation_Employees"]["fields"]["m06_error_de_deviation_employees_1m06_error_ida"] = array (
  'name' => 'm06_error_de_deviation_employees_1m06_error_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE_ID',
  'id_name' => 'm06_error_de_deviation_employees_1m06_error_ida',
  'link' => 'm06_error_de_deviation_employees_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
