<?php
 // created: 2019-05-23 18:07:26
$dictionary['DE_Deviation_Employees']['fields']['name']['len']='255';
$dictionary['DE_Deviation_Employees']['fields']['name']['audited']=true;
$dictionary['DE_Deviation_Employees']['fields']['name']['massupdate']=false;
$dictionary['DE_Deviation_Employees']['fields']['name']['unified_search']=false;
$dictionary['DE_Deviation_Employees']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['DE_Deviation_Employees']['fields']['name']['calculated']=false;

 ?>