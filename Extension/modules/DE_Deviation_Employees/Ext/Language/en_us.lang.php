<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DEVIATION_TYPE'] = 'Type';
$mod_strings['LBL_APS_EMPLOYEE'] = 'APS Employee';
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_M06_ERROR_TITLE'] = 'Communication';
$mod_strings['LNK_NEW_RECORD'] = 'Create Responsible Personnel';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Responsible Personnel';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Responsible Personnel';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Responsible Personnel vCard';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Responsible Personnel List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Responsible Personnel';
$mod_strings['LNK_LIST'] = 'View Responsible Personnel';
$mod_strings['LBL_MODULE_NAME'] = 'Responsible Personnel';
$mod_strings['LNK_IMPORT_DE_DEVIATION_EMPLOYEES'] = 'Import Responsible Personnel';
$mod_strings['LBL_DE_DEVIATION_EMPLOYEES_SUBPANEL_TITLE'] = 'Responsible Personnel';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Responsible Personnel';
$mod_strings['LBL_DE_DEVIATION_EMPLOYEES_FOCUS_DRAWER_DASHBOARD'] = 'Responsible Personnel Focus Drawer';
$mod_strings['LBL_DE_DEVIATION_EMPLOYEES_RECORD_DASHBOARD'] = 'Responsible Personnel Record Dashboard';
