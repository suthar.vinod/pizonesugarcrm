<?php
 // created: 2021-10-19 09:22:27
$layout_defs["POI_Purchase_Order_Item"]["subpanel_setup"]['poi_purchase_order_item_ri_received_items_1'] = array (
  'order' => 100,
  'module' => 'RI_Received_Items',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'get_subpanel_data' => 'poi_purchase_order_item_ri_received_items_1',
);
