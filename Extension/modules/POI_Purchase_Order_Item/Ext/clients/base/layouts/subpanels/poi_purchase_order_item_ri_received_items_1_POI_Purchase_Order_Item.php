<?php
// created: 2021-08-04 19:45:33
$viewdefs['POI_Purchase_Order_Item']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'context' => 
  array (
    'link' => 'poi_purchase_order_item_ri_received_items_1',
  ),
);