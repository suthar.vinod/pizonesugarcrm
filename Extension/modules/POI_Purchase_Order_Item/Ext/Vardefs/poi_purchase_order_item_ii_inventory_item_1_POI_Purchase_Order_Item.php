<?php
// created: 2021-11-09 09:48:27
$dictionary["POI_Purchase_Order_Item"]["fields"]["poi_purchase_order_item_ii_inventory_item_1"] = array (
  'name' => 'poi_purchase_order_item_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'poi_purchase_order_item_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'id_name' => 'poi_purcha8945er_item_ida',
  'link-type' => 'many',
  'side' => 'left',
);
