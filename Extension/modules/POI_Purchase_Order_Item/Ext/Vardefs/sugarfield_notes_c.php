<?php
 // created: 2022-04-12 09:28:16
$dictionary['POI_Purchase_Order_Item']['fields']['notes_c']['labelValue']='Notes';
$dictionary['POI_Purchase_Order_Item']['fields']['notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['POI_Purchase_Order_Item']['fields']['notes_c']['enforced']='';
$dictionary['POI_Purchase_Order_Item']['fields']['notes_c']['dependency']='';
$dictionary['POI_Purchase_Order_Item']['fields']['notes_c']['required_formula']='';
$dictionary['POI_Purchase_Order_Item']['fields']['notes_c']['readonly_formula']='';

 ?>