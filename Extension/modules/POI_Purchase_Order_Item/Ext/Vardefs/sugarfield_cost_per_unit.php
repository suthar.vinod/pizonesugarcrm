<?php
 // created: 2021-07-30 04:58:50
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['required']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['name']='cost_per_unit';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['vname']='LBL_COST_PER_UNIT';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['type']='currency';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['massupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['no_default']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['comments']='';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['help']='';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['importable']='false';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['duplicate_merge']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['duplicate_merge_dom_value']='0';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['audited']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['reportable']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['unified_search']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['merge_filter']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['pii']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['default']='';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['calculated']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['len']=26;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['size']='20';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['enable_range_search']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['precision']=6;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['hidemassupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['convertToBase']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['showTransactionalAmount']=true;

 ?>