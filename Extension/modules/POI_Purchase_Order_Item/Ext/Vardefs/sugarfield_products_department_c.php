<?php
 // created: 2021-12-14 06:07:50
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['duplicate_merge_dom_value']=0;
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['labelValue']='Product\'s Department(s)';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['calculated']='true';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['formula']='related($prod_product_poi_purchase_order_item_1,"department")';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['enforced']='true';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['dependency']='';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['required_formula']='';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['readonly']='1';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['readonly_formula']='';

 ?>