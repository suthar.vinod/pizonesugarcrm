<?php
 // created: 2020-12-18 14:10:19
$dictionary['POI_Purchase_Order_Item']['fields']['order_date']['massupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['order_date']['hidemassupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['order_date']['importable']='false';
$dictionary['POI_Purchase_Order_Item']['fields']['order_date']['duplicate_merge']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['order_date']['duplicate_merge_dom_value']='0';
$dictionary['POI_Purchase_Order_Item']['fields']['order_date']['calculated']=false;

 ?>