<?php
 // created: 2021-08-10 21:23:15
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['duplicate_merge_dom_value']=0;
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['labelValue']='purchaserequesttotal';
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['calculated']='true';
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['formula']='multiply($cost_per_unit,$unit_quantity_ordered)';
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['enforced']='true';
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['dependency']='';
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['required_formula']='';
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['readonly_formula']='';

 ?>