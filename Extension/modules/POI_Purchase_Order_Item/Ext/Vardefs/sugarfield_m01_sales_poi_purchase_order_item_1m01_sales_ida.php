<?php
 // created: 2021-10-19 11:28:56
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['name']='m01_sales_poi_purchase_order_item_1m01_sales_ida';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['type']='id';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['source']='non-db';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['vname']='LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['id_name']='m01_sales_poi_purchase_order_item_1m01_sales_ida';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['link']='m01_sales_poi_purchase_order_item_1';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['table']='m01_sales';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['module']='M01_Sales';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['rname']='id';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['reportable']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['side']='right';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['massupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['duplicate_merge']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['hideacl']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['audited']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['importable']='true';

 ?>