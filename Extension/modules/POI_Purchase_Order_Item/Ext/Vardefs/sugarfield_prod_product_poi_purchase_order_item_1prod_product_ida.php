<?php
 // created: 2021-10-19 11:30:13
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['name']='prod_product_poi_purchase_order_item_1prod_product_ida';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['type']='id';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['source']='non-db';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['vname']='LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['id_name']='prod_product_poi_purchase_order_item_1prod_product_ida';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['link']='prod_product_poi_purchase_order_item_1';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['table']='prod_product';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['module']='Prod_Product';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['rname']='id';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['reportable']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['side']='right';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['massupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['duplicate_merge']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['hideacl']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['audited']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['importable']='true';

 ?>