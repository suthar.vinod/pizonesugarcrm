<?php
 // created: 2021-10-19 11:28:58
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['audited']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['massupdate']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['hidemassupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['duplicate_merge']='enabled';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['duplicate_merge_dom_value']='1';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['merge_filter']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['reportable']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['unified_search']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['calculated']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['vname']='LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_NAME_FIELD_TITLE';

 ?>