<?php
// created: 2020-07-31 12:39:49
$dictionary["POI_Purchase_Order_Item"]["fields"]["prod_product_poi_purchase_order_item_1"] = array (
  'name' => 'prod_product_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'prod_product_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'Prod_Product',
  'bean_name' => 'Prod_Product',
  'side' => 'right',
  'vname' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'id_name' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
  'link-type' => 'one',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["prod_product_poi_purchase_order_item_1_name"] = array (
  'name' => 'prod_product_poi_purchase_order_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_PROD_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
  'link' => 'prod_product_poi_purchase_order_item_1',
  'table' => 'prod_product',
  'module' => 'Prod_Product',
  'rname' => 'name',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["prod_product_poi_purchase_order_item_1prod_product_ida"] = array (
  'name' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID',
  'id_name' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
  'link' => 'prod_product_poi_purchase_order_item_1',
  'table' => 'prod_product',
  'module' => 'Prod_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
