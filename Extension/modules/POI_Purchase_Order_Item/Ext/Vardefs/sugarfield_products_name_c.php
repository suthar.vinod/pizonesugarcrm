<?php
 // created: 2022-02-22 08:13:28
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['duplicate_merge_dom_value']=0;
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['labelValue']='Product\'s Name';
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['calculated']='true';
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['formula']='related($prod_product_poi_purchase_order_item_1,"name")';
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['enforced']='true';
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['dependency']='';
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['required_formula']='';
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['readonly_formula']='';

 ?>