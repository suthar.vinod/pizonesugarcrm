<?php
// created: 2021-08-04 19:45:33
$dictionary["POI_Purchase_Order_Item"]["fields"]["poi_purchase_order_item_ri_received_items_1"] = array (
  'name' => 'poi_purchase_order_item_ri_received_items_1',
  'type' => 'link',
  'relationship' => 'poi_purchase_order_item_ri_received_items_1',
  'source' => 'non-db',
  'module' => 'RI_Received_Items',
  'bean_name' => 'RI_Received_Items',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'id_name' => 'poi_purcha665cer_item_ida',
  'link-type' => 'many',
  'side' => 'left',
);
