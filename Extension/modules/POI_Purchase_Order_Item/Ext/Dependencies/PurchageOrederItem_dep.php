<?php

$dependencies['POI_Purchase_Order_Item']['poi_visibility_dep'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('related_to'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_poi_purchase_order_item_1_name',
                'value' => 'equal($related_to, "Work Product")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm01_sales_poi_purchase_order_item_1_name',
                'value' => 'equal($related_to, "Sales")',
            ),
        ),
    ),
);

$dependencies['POI_Purchase_Order_Item']['poi_required_dep'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_poi_purchase_order_item_1_name', 'related_to', 'm01_sales_poi_purchase_order_item_1_name', 'prod_product_poi_purchase_order_item_1_name'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_poi_purchase_order_item_1_name',
                'label' => 'wp_required_label',
                'value' => 'equal($related_to, "Work Product")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm01_sales_poi_purchase_order_item_1_name',
                'label' => 'sales_required_label',
                'value' => 'equal($related_to, "Sales")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'prod_product_poi_purchase_order_item_1_name',
                'label' => 'product_required_label',
                'value' => 'true',
            ),
        ),
    ),
);
