<?php

$dependencies['POI_Purchase_Order_Item']['po_required_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('po_purchase_order_poi_purchase_order_item_1_name'),
    'onload'        => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions'       => array(
        array(
            'name'   => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'po_purchase_order_poi_purchase_order_item_1_name',
                'label'  => 'purchase_order_required_label',
                'value'  => 'true'
            ),
        ),
    ),
);