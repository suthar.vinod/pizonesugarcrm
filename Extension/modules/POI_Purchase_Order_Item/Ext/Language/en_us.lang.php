<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_PURCHASEREQUESTTOTAL'] = 'purchaserequesttotal';
$mod_strings['LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_NAME_FIELD_TITLE'] = 'Sales Activity';
$mod_strings['LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_M01_SALES_TITLE'] = 'Sales Activity';
$mod_strings['LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_NAME_FIELD_TITLE'] = 'Work Product';
$mod_strings['LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Product';
$mod_strings['LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_NAME_FIELD_TITLE'] = 'Product';
$mod_strings['LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_PROD_PRODUCT_TITLE'] = 'Product';
$mod_strings['LBL_PRODUCTS_DEPARTMENT'] = 'Product&#039;s Department(s)';
$mod_strings['LBL_PRODUCTS_NAME'] = 'Product&#039;s Name';
$mod_strings['LBL_NOTES'] = 'Notes';
