<?php
// created: 2019-09-26 14:13:30
$dictionary["CN_Company_Name"]["fields"]["accounts_cn_company_name_1"] = array (
  'name' => 'accounts_cn_company_name_1',
  'type' => 'link',
  'relationship' => 'accounts_cn_company_name_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_CN_COMPANY_NAME_1_FROM_CN_COMPANY_NAME_TITLE',
  'id_name' => 'accounts_cn_company_name_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["CN_Company_Name"]["fields"]["accounts_cn_company_name_1_name"] = array (
  'name' => 'accounts_cn_company_name_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_CN_COMPANY_NAME_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_cn_company_name_1accounts_ida',
  'link' => 'accounts_cn_company_name_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["CN_Company_Name"]["fields"]["accounts_cn_company_name_1accounts_ida"] = array (
  'name' => 'accounts_cn_company_name_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_CN_COMPANY_NAME_1_FROM_CN_COMPANY_NAME_TITLE_ID',
  'id_name' => 'accounts_cn_company_name_1accounts_ida',
  'link' => 'accounts_cn_company_name_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
