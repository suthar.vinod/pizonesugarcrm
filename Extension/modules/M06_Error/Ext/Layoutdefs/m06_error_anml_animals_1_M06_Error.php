<?php
 // created: 2018-01-31 16:00:43
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_anml_animals_1'] = array (
  'order' => 100,
  'module' => 'ANML_Animals',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M06_ERROR_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE',
  'get_subpanel_data' => 'm06_error_anml_animals_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
