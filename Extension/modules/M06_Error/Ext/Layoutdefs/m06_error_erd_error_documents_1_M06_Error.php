<?php
 // created: 2018-01-31 21:10:46
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_erd_error_documents_1'] = array (
  'order' => 100,
  'module' => 'Erd_Error_Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'm06_error_erd_error_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
