<?php
 // created: 2022-04-26 07:03:59
$layout_defs["M06_Error"]["subpanel_setup"]['bid_batch_id_m06_error_1'] = array (
  'order' => 100,
  'module' => 'BID_Batch_ID',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_BID_BATCH_ID_M06_ERROR_1_FROM_BID_BATCH_ID_TITLE',
  'get_subpanel_data' => 'bid_batch_id_m06_error_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
