<?php
 // created: 2020-06-12 17:17:45
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['labelValue']='QA Verification of Completion Duration';
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['calculated']='1';
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['formula']='ifElse(or(equal($actual_qa_reassessment_date_c,""),equal($target_qa_reassessment_date_c,"")),"",subtract(daysUntil($actual_qa_reassessment_date_c),daysUntil($target_qa_reassessment_date_c)))';
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['enforced']='1';
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['dependency']='';

 ?>