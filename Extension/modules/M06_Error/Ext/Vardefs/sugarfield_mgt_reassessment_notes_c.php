<?php
 // created: 2019-10-10 12:37:43
$dictionary['M06_Error']['fields']['mgt_reassessment_notes_c']['labelValue']='Management Reassessment';
$dictionary['M06_Error']['fields']['mgt_reassessment_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['mgt_reassessment_notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['mgt_reassessment_notes_c']['dependency']='equal($management_reassessment_req_c,true)';

 ?>