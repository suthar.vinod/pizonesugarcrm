<?php
 // created: 2020-05-06 18:09:02
$dictionary['M06_Error']['fields']['due_date_c']['labelValue']='Due Date';
$dictionary['M06_Error']['fields']['due_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['due_date_c']['required']=false;
$dictionary['M06_Error']['fields']['due_date_c']['dependency']='equal($error_category_c,"IACUC Deficiency")';

 ?>