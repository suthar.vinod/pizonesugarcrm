<?php
 // created: 2019-10-02 13:48:14
$dictionary['M06_Error']['fields']['maintenance_request_notes_c']['labelValue']='Facilities Assessment';
$dictionary['M06_Error']['fields']['maintenance_request_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['maintenance_request_notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['maintenance_request_notes_c']['dependency']='or(equal($error_category_c,"Maintenance Request"),equal($error_type_c,"Facility"))';

 ?>