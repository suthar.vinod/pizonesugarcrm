<?php

$dictionary["M06_Error"]["fields"]['employee_relate'] = array (
      'name' => 'employee_relate',
      'source' => 'non-db',
      'vname' => 'LBL_EMPLOYEE_RELATE',
      'type' => 'relate',
      'id_name' => 'employee_id',
      'module' => 'Employees',
      'rname' => 'name',
      'studio' => 'visible',
    );


$dictionary["M06_Error"]["fields"]['employee_id'] = array (
      'name' => 'employee_id',
      'type' => 'id',
      'vname' => 'LBL_EMPLOYEE_ID',
    );
