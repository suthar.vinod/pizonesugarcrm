<?php
 // created: 2022-01-11 06:55:19
$dictionary['M06_Error']['fields']['surgical_access_c']['labelValue']='Surgical Access';
$dictionary['M06_Error']['fields']['surgical_access_c']['dependency']='isInList($error_type_c,createList("Not Used","Not used shared BU","Rejected"))';
$dictionary['M06_Error']['fields']['surgical_access_c']['required_formula']='';
$dictionary['M06_Error']['fields']['surgical_access_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['surgical_access_c']['visibility_grid']='';

 ?>