<?php
 // created: 2019-10-02 13:49:11
$dictionary['M06_Error']['fields']['fac_reassessment_required_c']['labelValue']='Facilities Reassessment Required';
$dictionary['M06_Error']['fields']['fac_reassessment_required_c']['enforced']='';
$dictionary['M06_Error']['fields']['fac_reassessment_required_c']['dependency']='or(equal($error_category_c,"Maintenance Request"),equal($error_type_c,"Facility"))';

 ?>