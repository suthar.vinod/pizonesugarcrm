<?php
/**
 * Created by PhpStorm.
 * User: nabeel.alvi
 * Date: 7/23/18
 * Time: 5:23 PM
 */

$dictionary["M06_Error"]["fields"]["rp_review_email_dept"] = array(
    'name' => 'rp_review_email_dept',
    'label' => 'LBL_REVIEW_EMAIL_DEPT',
    'type' => 'text',
    'dbType' => 'blob',
    'module' => 'M06_Error',
    'audited' => false,
    'mass_update' => false,
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
    'source' => 'db'
);
?>
