<?php
 // created: 2020-01-24 13:13:32
$dictionary['M06_Error']['fields']['equipment_reassessment_c']['labelValue']='Equipment Reassessment';
$dictionary['M06_Error']['fields']['equipment_reassessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['equipment_reassessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['equipment_reassessment_c']['dependency']='equal($equipment_reassessment_req_c,true)';

 ?>