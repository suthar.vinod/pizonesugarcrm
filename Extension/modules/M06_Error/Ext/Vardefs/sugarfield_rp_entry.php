<?php
/**
 * Created by PhpStorm.
 * User: nabeel.alvi
 * Date: 7/20/18
 * Time: 6:58 PM
 */

$dictionary["M06_Error"]["fields"]["rp_entry"] = array(
    'name' => 'rp_entry',
    'label' => 'LBL_ENTRY',
    'type' => 'text',
    'dbType' => 'blob',
    'default_value' => '',
    'audited' => false,
    'mass_update' => false,
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
    'source' => 'db',
);
?>
