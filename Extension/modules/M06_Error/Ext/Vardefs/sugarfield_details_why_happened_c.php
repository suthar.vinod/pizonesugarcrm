<?php
 // created: 2020-06-17 04:03:19
$dictionary['M06_Error']['fields']['details_why_happened_c']['labelValue']='Details for Why it Happened';
$dictionary['M06_Error']['fields']['details_why_happened_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['details_why_happened_c']['enforced']='';
$dictionary['M06_Error']['fields']['details_why_happened_c']['dependency']='equal($why_it_happened_c,"Known")';

 ?>