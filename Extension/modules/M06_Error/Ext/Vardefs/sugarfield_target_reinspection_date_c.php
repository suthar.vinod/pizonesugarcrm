<?php
 // created: 2021-09-09 07:58:01
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['labelValue']='Target SD Assessment Date';
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['formula']='addDays($date_entered,7)';
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['dependency']='and(not(equal($actual_sd_ack_date_c,"")),not(equal($error_category_c,"Study Specific Charge")))';
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['required_formula']='';
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['readonly_formula']='';

 ?>