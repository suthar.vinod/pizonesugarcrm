<?php
 // created: 2020-02-21 13:12:28
$dictionary['M06_Error']['fields']['justification_for_no_capa_c']['labelValue']='Justification for no CAPA';
$dictionary['M06_Error']['fields']['justification_for_no_capa_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['justification_for_no_capa_c']['enforced']='';
$dictionary['M06_Error']['fields']['justification_for_no_capa_c']['dependency']='equal($capa_initiation_c,"No")';

 ?>