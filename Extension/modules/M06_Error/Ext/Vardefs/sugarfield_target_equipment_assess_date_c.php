<?php
 // created: 2020-01-24 13:02:04
$dictionary['M06_Error']['fields']['target_equipment_assess_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_equipment_assess_date_c']['labelValue']='Target Equipment Assessment Date';
$dictionary['M06_Error']['fields']['target_equipment_assess_date_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_equipment_assess_date_c']['formula']='addDays($date_entered,14)';
$dictionary['M06_Error']['fields']['target_equipment_assess_date_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_equipment_assess_date_c']['dependency']='equal($error_category_c,"Equipment Maintenance Request")';

 ?>