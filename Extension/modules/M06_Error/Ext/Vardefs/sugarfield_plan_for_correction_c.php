<?php
 // created: 2020-05-06 18:08:06
$dictionary['M06_Error']['fields']['plan_for_correction_c']['labelValue']='Plan for Correction';
$dictionary['M06_Error']['fields']['plan_for_correction_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['plan_for_correction_c']['enforced']='';
$dictionary['M06_Error']['fields']['plan_for_correction_c']['required']=false;
$dictionary['M06_Error']['fields']['plan_for_correction_c']['dependency']='equal($error_category_c,"IACUC Deficiency")';

 ?>