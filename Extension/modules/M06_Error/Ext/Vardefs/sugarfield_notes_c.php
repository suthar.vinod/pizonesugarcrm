<?php
 // created: 2020-06-04 15:09:20
$dictionary['M06_Error']['fields']['notes_c']['labelValue']='Pending Classification Notes';
$dictionary['M06_Error']['fields']['notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['notes_c']['dependency']='equal($error_classification_c,"Pending")';

 ?>