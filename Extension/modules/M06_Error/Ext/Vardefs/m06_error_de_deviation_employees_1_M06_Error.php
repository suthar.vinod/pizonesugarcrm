<?php
// created: 2018-07-30 20:17:15
$dictionary["M06_Error"]["fields"]["m06_error_de_deviation_employees_1"] = array (
  'name' => 'm06_error_de_deviation_employees_1',
  'type' => 'link',
  'relationship' => 'm06_error_de_deviation_employees_1',
  'source' => 'non-db',
  'module' => 'DE_Deviation_Employees',
  'bean_name' => 'DE_Deviation_Employees',
  'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_de_deviation_employees_1m06_error_ida',
  'link-type' => 'many',
  'side' => 'left',
);
