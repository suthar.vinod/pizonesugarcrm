<?php
 // created: 2019-10-10 12:39:26
$dictionary['M06_Error']['fields']['pi_reassessment_notes_c']['labelValue']='PI Reassessment';
$dictionary['M06_Error']['fields']['pi_reassessment_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['pi_reassessment_notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['pi_reassessment_notes_c']['dependency']='equal($pi_reassessment_required_c,true)';

 ?>