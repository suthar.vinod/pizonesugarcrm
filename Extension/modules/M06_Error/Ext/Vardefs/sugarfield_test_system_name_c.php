<?php
 // created: 2020-01-13 15:09:39
$dictionary['M06_Error']['fields']['test_system_name_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['test_system_name_c']['labelValue']='Test System Name';
$dictionary['M06_Error']['fields']['test_system_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['test_system_name_c']['calculated']='1';
$dictionary['M06_Error']['fields']['test_system_name_c']['formula']='related($m06_error_anml_animals_1,"name")';
$dictionary['M06_Error']['fields']['test_system_name_c']['enforced']='1';
$dictionary['M06_Error']['fields']['test_system_name_c']['dependency']='';

 ?>