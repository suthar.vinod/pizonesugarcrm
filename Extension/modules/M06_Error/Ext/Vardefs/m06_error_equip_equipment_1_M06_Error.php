<?php
// created: 2018-01-31 16:07:58
$dictionary["M06_Error"]["fields"]["m06_error_equip_equipment_1"] = array (
  'name' => 'm06_error_equip_equipment_1',
  'type' => 'link',
  'relationship' => 'm06_error_equip_equipment_1',
  'source' => 'non-db',
  'module' => 'Equip_Equipment',
  'bean_name' => 'Equip_Equipment',
  'vname' => 'LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'id_name' => 'm06_error_equip_equipment_1equip_equipment_idb',
);
