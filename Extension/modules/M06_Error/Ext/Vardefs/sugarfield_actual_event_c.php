<?php
 // created: 2021-08-05 06:53:27
$dictionary['M06_Error']['fields']['actual_event_c']['labelValue']='Actual Event';
$dictionary['M06_Error']['fields']['actual_event_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['actual_event_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_event_c']['dependency']='isInList($error_category_c,createList("Critical Phase Inspection","Daily QC","Data Book QC","Deceased Animal","External Audit","Feedback","Maintenance Request","Process Audit","Real time study conduct","Rejected Animal","Vet Check","Weekly Sweep","Internal Feedback","Gross Pathology","IACUC Deficiency","Equipment Maintenance Request","Retrospective Data QC","Training","Test Failure","Work Product Schedule Outcome","Study Specific Charge"))';
$dictionary['M06_Error']['fields']['actual_event_c']['required_formula']='isInList($error_category_c,createList("Critical Phase Inspection","Daily QC","Data Book QC","Deceased Animal","External Audit","Feedback","Maintenance Request","Process Audit","Real time study conduct","Rejected Animal","Vet Check","Weekly Sweep","Internal Feedback","Gross Pathology","IACUC Deficiency","Equipment Maintenance Request","Retrospective Data QC","Training","Test Failure","Study Specific Charge"))';
$dictionary['M06_Error']['fields']['actual_event_c']['readonly_formula']='';

 ?>