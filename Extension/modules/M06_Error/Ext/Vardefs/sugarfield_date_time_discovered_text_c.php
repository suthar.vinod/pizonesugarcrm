<?php
 // created: 2021-02-11 14:46:56
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['required']=true;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['name']='date_time_discovered_text_c';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['vname']='LBL_DATE_TIME_DISCOVERED';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['type']='varchar';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['massupdate']=false;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['no_default']=false;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['comments']='';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['help']='';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['importable']='false';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['duplicate_merge']='disabled';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['duplicate_merge_dom_value']='0';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['audited']=true;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['reportable']=true;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['unified_search']=false;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['merge_filter']='disabled';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['calculated']=false;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['len']='255';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['size']='20';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['source']='custom_fields';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['dependency']='or(equal($error_category_c,"Deceased Animal"),equal($error_type_c,"Found Deceased"))';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['hidemassupdate']=false;

 ?>