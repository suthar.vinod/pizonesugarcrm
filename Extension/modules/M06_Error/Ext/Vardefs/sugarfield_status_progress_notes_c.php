<?php
 // created: 2020-05-06 18:09:46
$dictionary['M06_Error']['fields']['status_progress_notes_c']['labelValue']='Status/Progress Notes';
$dictionary['M06_Error']['fields']['status_progress_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['status_progress_notes_c']['required']=false;
$dictionary['M06_Error']['fields']['status_progress_notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['status_progress_notes_c']['dependency']='equal($error_category_c,"IACUC Deficiency")';

 ?>