<?php
// created: 2018-01-31 16:00:43
$dictionary["M06_Error"]["fields"]["m06_error_anml_animals_1"] = array (
  'name' => 'm06_error_anml_animals_1',
  'type' => 'link',
  'relationship' => 'm06_error_anml_animals_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'vname' => 'LBL_M06_ERROR_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'm06_error_anml_animals_1anml_animals_idb',
);
