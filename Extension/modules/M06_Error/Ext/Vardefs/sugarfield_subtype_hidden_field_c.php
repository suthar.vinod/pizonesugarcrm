<?php
 // created: 2021-12-28 05:21:30
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['labelValue']='Subtype Hidden Field';
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['enforced']='';
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['dependency']='';
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['required_formula']='';
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['readonly']='1';
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['readonly_formula']='';

 ?>