<?php
 // created: 2022-01-11 06:52:01
$dictionary['M06_Error']['fields']['blood_collection_c']['labelValue']='Blood Collected';
$dictionary['M06_Error']['fields']['blood_collection_c']['dependency']='isInList($error_type_c,createList("Not Used","Not used shared BU","Rejected"))';
$dictionary['M06_Error']['fields']['blood_collection_c']['required_formula']='';
$dictionary['M06_Error']['fields']['blood_collection_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['blood_collection_c']['visibility_grid']='';

 ?>