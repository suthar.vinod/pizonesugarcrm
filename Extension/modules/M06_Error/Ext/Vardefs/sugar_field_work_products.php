<?php
/**
 * Created by PhpStorm.
 * User: nabeel.alvi
 * Date: 7/23/18
 * Time: 5:23 PM
 */

$dictionary["M06_Error"]["fields"]["work_products"] = array(
    'name' => 'work_products',
    'label' => 'LBL_ENTRY',
    'type' => 'text',
    'dbType' => 'blob',
    'default_value' => '',
    'audited' => false,
    'mass_update' => false,
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
    'source' => 'db',
);
?>
