<?php
 // created: 2020-12-18 13:30:08
$dictionary['M06_Error']['fields']['corrective_action_1_c']['labelValue']='Corrective Action Within the Study';
$dictionary['M06_Error']['fields']['corrective_action_1_c']['dependency']='';
$dictionary['M06_Error']['fields']['corrective_action_1_c']['required_formula']='';
$dictionary['M06_Error']['fields']['corrective_action_1_c']['visibility_grid']=array (
  'trigger' => 'error_classification_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Adverse Event' => 
    array (
      0 => 'Further Investigation of Event',
    ),
    'Error' => 
    array (
      0 => '',
      1 => 'SOP Update',
      2 => 'Further Investigation of Event',
      3 => 'None',
      4 => 'Other',
      5 => 'Protocol Amendment',
      6 => 'Protocol Requirement Review',
      7 => 'Report Amendment',
      8 => 'Protocol Specific Training',
    ),
    'Duplicate' => 
    array (
    ),
    'Impactful Deviation' => 
    array (
      0 => '',
      1 => 'SOP Update',
      2 => 'Further Investigation of Event',
      3 => 'None',
      4 => 'Other',
      5 => 'Protocol Amendment',
      6 => 'Protocol Requirement Review',
      7 => 'Report Amendment',
      8 => 'Protocol Specific Training',
    ),
    'Job well done' => 
    array (
    ),
    'Notification' => 
    array (
    ),
    'Observation' => 
    array (
    ),
    'Opportunity for Improvement' => 
    array (
    ),
    'Pending' => 
    array (
    ),
    'Non Impactful Deviation' => 
    array (
    ),
    'Unforeseen Circumstance Unanticipated Response' => 
    array (
    ),
    'Withdrawn Error' => 
    array (
    ),
    'Choose One' => 
    array (
    ),
    'A' => 
    array (
    ),
    'M' => 
    array (
    ),
    'S' => 
    array (
    ),
    'C' => 
    array (
    ),
    'NA' => 
    array (
    ),
    'Major Change or New' => 
    array (
    ),
    'Revision Change or Minor Change' => 
    array (
    ),
    'Employee did NOT perform SOP tasks while overdue' => 
    array (
    ),
    'The Controlled Document is a standalone document. No Controlled Document activities are associated' => 
    array (
    ),
    'Overdue refresher training' => 
    array (
    ),
    'Adverse Event Study Article Related' => 
    array (
      0 => 'Further Investigation of Event',
    ),
    'Adverse Event Procedure Related' => 
    array (
      0 => 'Further Investigation of Event',
    ),
    'Adverse Event Test System Model Related' => 
    array (
      0 => 'Further Investigation of Event',
    ),
    'Adverse Event Etiology Unknown' => 
    array (
      0 => 'Further Investigation of Event',
    ),
  ),
);

 ?>