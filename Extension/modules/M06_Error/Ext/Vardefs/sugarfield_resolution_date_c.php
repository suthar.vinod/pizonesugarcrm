<?php
 // created: 2020-07-07 05:52:30
$dictionary['M06_Error']['fields']['resolution_date_c']['labelValue']='Resolution Date';
$dictionary['M06_Error']['fields']['resolution_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['resolution_date_c']['dependency']='equal($error_type_c,"Resolution Notification")';
$dictionary['M06_Error']['fields']['resolution_date_c']['readonly']='true';
 ?>