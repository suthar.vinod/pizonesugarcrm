<?php
 // created: 2020-06-12 17:18:39
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['labelValue']='SD Assessment Duration';
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['calculated']='1';
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['formula']='ifElse(or(equal($reinspection_date_c,""),equal($target_reinspection_date_c,"")),"",subtract(daysUntil($reinspection_date_c),daysUntil($target_reinspection_date_c)))';
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['enforced']='1';
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['dependency']='';

 ?>