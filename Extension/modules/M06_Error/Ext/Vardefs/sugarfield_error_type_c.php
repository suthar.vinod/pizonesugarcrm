<?php
 // created: 2022-10-04 05:48:48
$dictionary['M06_Error']['fields']['error_type_c']['labelValue']='Type';
$dictionary['M06_Error']['fields']['error_type_c']['dependency']='';
$dictionary['M06_Error']['fields']['error_type_c']['required_formula']='';
$dictionary['M06_Error']['fields']['error_type_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['error_type_c']['visibility_grid']=array (
  'trigger' => 'error_category_c',
  'values' => 
  array (
    'Choose One' => 
    array (
    ),
    'Critical Phase Inspection' => 
    array (
      0 => '',
      1 => 'Sttudy',
      2 => 'Facility',
    ),
    'Deceased Animal' => 
    array (
      0 => '',
      1 => 'Found Dead',
      2 => 'Early Term',
    ),
    'Maintenance Request' => 
    array (
    ),
    'Observation' => 
    array (
      0 => 'Sttudy',
      1 => 'Facility',
    ),
    'Rejected Animal' => 
    array (
      0 => '',
      1 => 'Not Sedated Study Article NOT Administered Surgical Access NOT Performed',
      2 => 'Study Article Administered',
      3 => 'Study Article NOT Administered Surgical Access NOT Performed',
      4 => 'Study Article NOT Administered Surgical Access Performed',
      5 => 'Unused_Medication Administered',
      6 => 'Unused_No Medication',
    ),
    'Vet Check' => 
    array (
      0 => '',
      1 => 'Initial Issue',
      2 => 'Repeat Issue',
      3 => 'Resolution Notification',
      4 => 'Observation at Transfer',
      5 => 'Clinical Pathology Review',
    ),
    '' => 
    array (
    ),
    'Daily QC' => 
    array (
      0 => '',
      1 => 'Sttudy',
      2 => 'Facility',
    ),
    'Data Book QC' => 
    array (
      0 => '',
      1 => 'Sttudy',
      2 => 'Facility',
    ),
    'Real time study conduct' => 
    array (
      0 => '',
      1 => 'Sttudy',
      2 => 'Facility',
      3 => 'Re Challenge',
    ),
    'Weekly Sweep' => 
    array (
      0 => '',
      1 => 'Sttudy',
      2 => 'Facility',
    ),
    'Feedback' => 
    array (
      0 => '',
      1 => 'Complaint',
      2 => 'Kudos',
    ),
    'Process Audit' => 
    array (
      0 => '',
      1 => 'OFI',
      2 => 'Minor',
      3 => 'Major',
    ),
    'External Audit' => 
    array (
      0 => '',
      1 => 'Sttudy',
      2 => 'Facility',
    ),
    'Gross Pathology' => 
    array (
      0 => '',
      1 => 'Deceased Animal',
      2 => 'Herd Health',
      3 => 'Necropsy Findings Notification',
    ),
    'Internal Feedback' => 
    array (
      0 => '',
      1 => 'Operations',
      2 => 'Analytical Operations',
      3 => 'Inlife Operations',
      4 => 'Laboratory Operations',
      5 => 'Large Animal Operations',
      6 => 'Operations Support',
      7 => 'Pathology Operations',
      8 => 'Re_identification',
      9 => 'Remaining Departments',
      10 => 'Scientific',
      11 => 'Small Animal Operations',
      12 => 'SPA Bug',
    ),
    'IACUC Deficiency' => 
    array (
      0 => '',
      1 => 'Facility',
      2 => 'Program',
    ),
    'Equipment Maintenance Request' => 
    array (
    ),
    'Retrospective Data QC' => 
    array (
    ),
    'Training' => 
    array (
      0 => '',
      1 => 'Trainee Initial Training',
      2 => 'Trainee Refresher Training',
      3 => 'Trainer Initial Training',
      4 => 'Trainer Refresher Training',
      5 => 'Trainee',
      6 => 'Trainer',
    ),
    'Test Failure' => 
    array (
      0 => '',
      1 => 'Expanded Study',
      2 => 'Failed Study',
      3 => 'Aborted Study',
      4 => 'CAB',
      5 => 'Invalid Study',
    ),
    'Work Product Schedule Outcome' => 
    array (
      0 => '',
      1 => 'Early Death',
      2 => 'Early Term Outcome',
      3 => 'Failed Sham',
      4 => 'Failed Pyrogen',
      5 => 'Found Deceased',
      6 => 'Not Used',
      7 => 'Not used shared BU',
      8 => 'Passed Sham',
      9 => 'Passed Pyrogen',
      10 => 'Performed Per Protocol',
      11 => 'Procedure Death',
      12 => 'Rejected',
      13 => 'Sedation for Animal Health',
      14 => 'Standard Biocomp TS Selection',
      15 => 'Unsuccessful Procedure and Survived',
    ),
    'Study Specific Charge' => 
    array (
    ),
  ),
);

 ?>