<?php
 // created: 2019-05-22 13:08:35
$dictionary['M06_Error']['fields']['submitter_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['submitter_c']['required']=false;
$dictionary['M06_Error']['fields']['submitter_c']['name']='submitter_c';
$dictionary['M06_Error']['fields']['submitter_c']['vname']='LBL_SUBMITTER';
$dictionary['M06_Error']['fields']['submitter_c']['type']='varchar';
$dictionary['M06_Error']['fields']['submitter_c']['massupdate']=false;
$dictionary['M06_Error']['fields']['submitter_c']['no_default']=false;
$dictionary['M06_Error']['fields']['submitter_c']['comments']='';
$dictionary['M06_Error']['fields']['submitter_c']['help']='';
$dictionary['M06_Error']['fields']['submitter_c']['importable']='false';
$dictionary['M06_Error']['fields']['submitter_c']['duplicate_merge']='disabled';
$dictionary['M06_Error']['fields']['submitter_c']['duplicate_merge_dom_value']='0';
$dictionary['M06_Error']['fields']['submitter_c']['audited']=true;
$dictionary['M06_Error']['fields']['submitter_c']['reportable']=true;
$dictionary['M06_Error']['fields']['submitter_c']['unified_search']=false;
$dictionary['M06_Error']['fields']['submitter_c']['merge_filter']='disabled';
$dictionary['M06_Error']['fields']['submitter_c']['pii']=false;
$dictionary['M06_Error']['fields']['submitter_c']['calculated']=false;
$dictionary['M06_Error']['fields']['submitter_c']['len']='255';
$dictionary['M06_Error']['fields']['submitter_c']['size']='20';

 ?>