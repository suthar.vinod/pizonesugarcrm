<?php
 // created: 2019-10-10 12:01:00
$dictionary['M06_Error']['fields']['vet_reason_for_reassessment_c']['labelValue']='Vet Reason for Reassessment';
$dictionary['M06_Error']['fields']['vet_reason_for_reassessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['vet_reason_for_reassessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['vet_reason_for_reassessment_c']['dependency']='equal($vet_reassessment_required_c,true)';

 ?>