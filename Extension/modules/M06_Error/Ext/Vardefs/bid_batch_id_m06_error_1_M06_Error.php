<?php
// created: 2022-04-26 07:03:59
$dictionary["M06_Error"]["fields"]["bid_batch_id_m06_error_1"] = array (
  'name' => 'bid_batch_id_m06_error_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_m06_error_1',
  'source' => 'non-db',
  'module' => 'BID_Batch_ID',
  'bean_name' => 'BID_Batch_ID',
  'vname' => 'LBL_BID_BATCH_ID_M06_ERROR_1_FROM_BID_BATCH_ID_TITLE',
  'id_name' => 'bid_batch_id_m06_error_1bid_batch_id_ida',
);
