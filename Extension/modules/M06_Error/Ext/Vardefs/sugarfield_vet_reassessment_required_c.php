<?php
 // created: 2020-05-19 18:34:12
$dictionary['M06_Error']['fields']['vet_reassessment_required_c']['labelValue']='Vet Reassessment Required';
$dictionary['M06_Error']['fields']['vet_reassessment_required_c']['enforced']='';
$dictionary['M06_Error']['fields']['vet_reassessment_required_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal"))';

 ?>