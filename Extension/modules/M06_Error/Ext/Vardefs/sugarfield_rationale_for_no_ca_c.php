<?php
 // created: 2020-12-22 09:17:23
$dictionary['M06_Error']['fields']['rationale_for_no_ca_c']['labelValue']='Corrective Action Details';
$dictionary['M06_Error']['fields']['rationale_for_no_ca_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['rationale_for_no_ca_c']['enforced']='';
$dictionary['M06_Error']['fields']['rationale_for_no_ca_c']['dependency']='isInList($error_classification_c,createList("Error","Impactful Deviation","Adverse Event","Adverse Event Study Article Related","Adverse Event Procedure Related","Adverse Event Test System Model Related","Adverse Event Etiology Unknown"))';
$dictionary['M06_Error']['fields']['rationale_for_no_ca_c']['required_formula']='';

 ?>