<?php
 // created: 2019-06-27 14:20:12
$dictionary['M06_Error']['fields']['target_qa_reassessment_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_qa_reassessment_date_c']['labelValue']='Target QA Verification of Completion  Date';
$dictionary['M06_Error']['fields']['target_qa_reassessment_date_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_qa_reassessment_date_c']['formula']='addDays($date_entered,21)';
$dictionary['M06_Error']['fields']['target_qa_reassessment_date_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_qa_reassessment_date_c']['dependency']='and(equal($error_category_c,"Critical Phase Inspection"),not(equal($error_classification_c,"Duplicate")))';

 ?>