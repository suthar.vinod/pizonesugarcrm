<?php
 // created: 2022-01-11 07:01:37
$dictionary['M06_Error']['fields']['test_article_exposure_c']['labelValue']='Test Article Exposure';
$dictionary['M06_Error']['fields']['test_article_exposure_c']['dependency']='isInList($error_type_c,createList("Not Used","Not used shared BU","Rejected"))';
$dictionary['M06_Error']['fields']['test_article_exposure_c']['required_formula']='';
$dictionary['M06_Error']['fields']['test_article_exposure_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['test_article_exposure_c']['visibility_grid']='';

 ?>