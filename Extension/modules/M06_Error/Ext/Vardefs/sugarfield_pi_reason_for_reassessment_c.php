<?php
 // created: 2019-10-10 12:03:33
$dictionary['M06_Error']['fields']['pi_reason_for_reassessment_c']['labelValue']='PI Reason for Reassessment';
$dictionary['M06_Error']['fields']['pi_reason_for_reassessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['pi_reason_for_reassessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['pi_reason_for_reassessment_c']['dependency']='equal($pi_reassessment_required_c,true)';

 ?>