<?php
 // created: 2020-06-30 12:20:14
$dictionary['M06_Error']['fields']['recommendation_c']['labelValue']='Recommendation';
$dictionary['M06_Error']['fields']['recommendation_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['recommendation_c']['enforced']='';
$dictionary['M06_Error']['fields']['recommendation_c']['dependency']='isInList($error_category_c,createList("Critical Phase Inspection","Process Audit"))';

 ?>