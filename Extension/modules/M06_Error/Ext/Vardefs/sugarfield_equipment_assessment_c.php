<?php
 // created: 2020-08-25 06:51:23
$dictionary['M06_Error']['fields']['equipment_assessment_c']['labelValue']='Equipment Assessment';
$dictionary['M06_Error']['fields']['equipment_assessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['equipment_assessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['equipment_assessment_c']['dependency']='equal($error_category_c,"Equipment Maintenance Request")';

 ?>