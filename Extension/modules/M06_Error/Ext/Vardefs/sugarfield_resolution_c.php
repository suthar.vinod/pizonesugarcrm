<?php
 // created: 2021-09-09 08:01:55
$dictionary['M06_Error']['fields']['resolution_c']['labelValue']='SD Assessment';
$dictionary['M06_Error']['fields']['resolution_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['resolution_c']['enforced']='';
$dictionary['M06_Error']['fields']['resolution_c']['dependency']='and(not(equal($actual_sd_ack_date_c,"")),not(equal($error_category_c,"Study Specific Charge")))';
$dictionary['M06_Error']['fields']['resolution_c']['required_formula']='';
$dictionary['M06_Error']['fields']['resolution_c']['readonly_formula']='';

 ?>