<?php
 // created: 2021-06-10 11:44:55
$dictionary['M06_Error']['fields']['vet_assessment_date_c']['labelValue']='Vet Assessment Date';
$dictionary['M06_Error']['fields']['vet_assessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['vet_assessment_date_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal"))';
$dictionary['M06_Error']['fields']['vet_assessment_date_c']['required_formula']='';
$dictionary['M06_Error']['fields']['vet_assessment_date_c']['readonly_formula']='';

 ?>