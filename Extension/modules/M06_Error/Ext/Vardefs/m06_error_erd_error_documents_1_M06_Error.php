<?php
// created: 2018-01-31 21:10:46
$dictionary["M06_Error"]["fields"]["m06_error_erd_error_documents_1"] = array (
  'name' => 'm06_error_erd_error_documents_1',
  'type' => 'link',
  'relationship' => 'm06_error_erd_error_documents_1',
  'source' => 'non-db',
  'module' => 'Erd_Error_Documents',
  'bean_name' => 'Erd_Error_Documents',
  'vname' => 'LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'id_name' => 'm06_error_erd_error_documents_1erd_error_documents_idb',
);
