<?php
 // created: 2020-01-27 12:59:10
$dictionary['M06_Error']['fields']['sd_reassessment_required_c']['labelValue']='SD Reassessment Required?';
$dictionary['M06_Error']['fields']['sd_reassessment_required_c']['dependency']='or(not(equal($resolution_c,"")),not(equal($reinspection_date_c,"")))';
$dictionary['M06_Error']['fields']['sd_reassessment_required_c']['visibility_grid']='';

 ?>