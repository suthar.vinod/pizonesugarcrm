<?php
 // created: 2020-02-21 13:09:49
$dictionary['M06_Error']['fields']['feedback_assessment_notes_2_c']['labelValue']='PI Assessment';
$dictionary['M06_Error']['fields']['feedback_assessment_notes_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['feedback_assessment_notes_2_c']['enforced']='';
$dictionary['M06_Error']['fields']['feedback_assessment_notes_2_c']['dependency']='equal($error_type_c,"Complaint")';

 ?>