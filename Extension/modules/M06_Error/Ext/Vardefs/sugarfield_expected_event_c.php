<?php
 // created: 2021-01-25 14:07:00
$dictionary['M06_Error']['fields']['expected_event_c']['labelValue']='Expected Event';
$dictionary['M06_Error']['fields']['expected_event_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['expected_event_c']['enforced']='';
$dictionary['M06_Error']['fields']['expected_event_c']['dependency']='isInList($error_category_c,createList("Critical Phase Inspection","Daily QC","Data Book QC","Process Audit","Real time study conduct","Weekly Sweep","Retrospective Data QC","Training","Test Failure","Internal Feedback","IACUC Deficiency"))';
$dictionary['M06_Error']['fields']['expected_event_c']['required_formula']='isInList($error_category_c,createList("Critical Phase Inspection","Daily QC","Data Book QC","Process Audit","Real time study conduct","Weekly Sweep","Retrospective Data QC","Training","Test Failure","Internal Feedback"))';

 ?>