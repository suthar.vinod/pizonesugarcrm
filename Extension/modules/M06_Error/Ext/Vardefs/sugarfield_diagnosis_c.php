<?php
 // created: 2021-06-01 10:06:29
$dictionary['M06_Error']['fields']['diagnosis_c']['labelValue']='Diagnosis';
$dictionary['M06_Error']['fields']['diagnosis_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal","Work Product Schedule Outcome"))';
$dictionary['M06_Error']['fields']['diagnosis_c']['required_formula']='';
$dictionary['M06_Error']['fields']['diagnosis_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['diagnosis_c']['visibility_grid']='';

 ?>