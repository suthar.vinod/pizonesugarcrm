<?php
 // created: 2021-08-12 08:52:13
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['labelValue']='Manager Assessment Duration';
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['calculated']='true';
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['formula']='ifElse(equal($actual_management_assessment_c,""),"",subtract(daysUntil($actual_management_assessment_c),daysUntil($date_entered)))';
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['enforced']='true';
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['dependency']='';
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['required_formula']='';
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['readonly_formula']='';

 ?>