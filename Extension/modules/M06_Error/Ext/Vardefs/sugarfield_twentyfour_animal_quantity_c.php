<?php
 // created: 2022-10-04 05:54:48
$dictionary['M06_Error']['fields']['twentyfour_animal_quantity_c']['labelValue']='24hr: Quantity of animals that scored 1 or greater';
$dictionary['M06_Error']['fields']['twentyfour_animal_quantity_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['twentyfour_animal_quantity_c']['enforced']='';
$dictionary['M06_Error']['fields']['twentyfour_animal_quantity_c']['dependency']='and(equal($error_category_c,"Real time study conduct"),equal($error_type_c,"Re Challenge"))';
$dictionary['M06_Error']['fields']['twentyfour_animal_quantity_c']['required_formula']='and(equal($error_category_c,"Real time study conduct"),equal($error_type_c,"Re Challenge"))';
$dictionary['M06_Error']['fields']['twentyfour_animal_quantity_c']['readonly_formula']='';

 ?>