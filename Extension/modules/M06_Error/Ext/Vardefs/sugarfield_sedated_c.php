<?php
 // created: 2022-01-11 06:47:34
$dictionary['M06_Error']['fields']['sedated_c']['labelValue']='Sedated';
$dictionary['M06_Error']['fields']['sedated_c']['dependency']='isInList($error_type_c,createList("Not Used","Not used shared BU","Rejected"))';
$dictionary['M06_Error']['fields']['sedated_c']['required_formula']='';
$dictionary['M06_Error']['fields']['sedated_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['sedated_c']['visibility_grid']='';

 ?>