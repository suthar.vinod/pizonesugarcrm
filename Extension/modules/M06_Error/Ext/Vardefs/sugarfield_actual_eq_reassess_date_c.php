<?php
 // created: 2020-01-24 13:12:17
$dictionary['M06_Error']['fields']['actual_eq_reassess_date_c']['labelValue']='Actual Equipment Reassessment Date';
$dictionary['M06_Error']['fields']['actual_eq_reassess_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_eq_reassess_date_c']['dependency']='equal($equipment_reassessment_req_c,true)';

 ?>