<?php
 // created: 2019-06-13 15:40:29
$dictionary['M06_Error']['fields']['actual_qa_reassessment_date_c']['labelValue']='Actual QA Verification of Completion Date';
$dictionary['M06_Error']['fields']['actual_qa_reassessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_qa_reassessment_date_c']['dependency']='and(equal($error_category_c,"Critical Phase Inspection"),not(equal($error_classification_c,"Duplicate")))';

 ?>