<?php
 // created: 2022-09-08 05:53:47
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['labelValue']='Data Reviewed By';
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['enforced']='false';
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['dependency']='';
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['required_formula']='';
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['readonly']='1';
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['readonly_formula']='';

 ?>