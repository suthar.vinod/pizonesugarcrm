<?php
 // created: 2020-05-01 10:48:14
$dictionary['M06_Error']['fields']['study_director_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['study_director_c']['labelValue']='Study Director';
$dictionary['M06_Error']['fields']['study_director_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['study_director_c']['calculated']='1';
$dictionary['M06_Error']['fields']['study_director_c']['formula']='related($m06_error_m03_work_product_1,"assigned_to_for_coms_c")';
$dictionary['M06_Error']['fields']['study_director_c']['enforced']='1';
$dictionary['M06_Error']['fields']['study_director_c']['dependency']='and(not(equal($wp_name_c,"")),not(isInList($error_category_c,createList("Feedback","Internal Feedback"))))';

 ?>