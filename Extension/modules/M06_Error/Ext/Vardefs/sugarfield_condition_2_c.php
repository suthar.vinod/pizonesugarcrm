<?php
 // created: 2022-02-11 19:50:59
$dictionary['M06_Error']['fields']['condition_2_c']['labelValue']='Clinical Symptoms';
$dictionary['M06_Error']['fields']['condition_2_c']['dependency']='';
$dictionary['M06_Error']['fields']['condition_2_c']['required_formula']='';
$dictionary['M06_Error']['fields']['condition_2_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['condition_2_c']['visibility_grid']=array (
  'trigger' => 'organ_system_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Cardiovascular' => 
    array (
      0 => '',
      1 => 'Arrhythmia',
      2 => 'Bradycardia',
      3 => 'Heart block',
      4 => 'Heart murmur',
      5 => 'Hypoxia',
      6 => 'Muffled heart sounds',
      7 => 'Other',
      8 => 'Pericardial effusion',
      9 => 'Tachycardia',
      10 => 'Prolonged CRT Cyanosis',
    ),
    'GI' => 
    array (
      0 => '',
      1 => 'Abdominal distension',
      2 => 'Bloat',
      3 => 'Choke',
      4 => 'Constipation',
      5 => 'Diarrhea',
      6 => 'Gingivitis',
      7 => 'Hematemesis',
      8 => 'Hematochezia',
      9 => 'Inappetence',
      10 => 'Internal Parasites',
      11 => 'Melena',
      12 => 'Nausea',
      13 => 'Other',
      14 => 'Peritoneal effusionAscites',
      15 => 'Ptyalism',
      16 => 'Rectal Bleeding',
      17 => 'Rectal Prolapse',
      18 => 'Regurgitation',
      19 => 'Ulcer',
      20 => 'Vomiting',
      21 => 'Dental disease',
      22 => 'Icteric',
      23 => 'Ileus',
    ),
    'Integumentary' => 
    array (
      0 => '',
      1 => 'Abrasion',
      2 => 'Alopecia',
      3 => 'Blisters',
      4 => 'Bruising',
      5 => 'Discharge',
      6 => 'Ectoparasite',
      7 => 'Edema',
      8 => 'Erythema',
      9 => 'Hoof crack',
      10 => 'Hyperpigmentation',
      11 => 'Laceration',
      12 => 'Mass',
      13 => 'Nodule',
      14 => 'Other',
      15 => 'PapulesPustules',
      16 => 'Pruritis',
      17 => 'Pustules',
      18 => 'Rash',
      19 => 'Scabbing',
      20 => 'Swelling',
      21 => 'Urticaria',
      22 => 'Ulceration',
      23 => 'Wound',
      24 => 'Otitis Externa',
    ),
    'Musculoskeletal' => 
    array (
      0 => '',
      1 => 'Atrophy',
      2 => 'Conformation',
      3 => 'Joint Swelling',
      4 => 'Lame',
      5 => 'Other',
      6 => 'Poor Confirmation',
    ),
    'Neurologic' => 
    array (
      0 => '',
      1 => 'Ataxia',
      2 => 'Deafness',
      3 => 'Head Tilt',
      4 => 'Nystagmus',
      5 => 'Other',
      6 => 'Paresis',
      7 => 'Paralysis',
      8 => 'Seizure',
      9 => 'Syncope',
    ),
    'Ophthalmic' => 
    array (
      0 => '',
      1 => 'Blindness',
      2 => 'Blepharospasm',
      3 => 'Chemosis',
      4 => 'Conjunctivits',
      5 => 'Corneal Edema',
      6 => 'Corneal Opacity',
      7 => 'Discharge',
      8 => 'Ectropion',
      9 => 'Enophthalmos',
      10 => 'Entropion',
      11 => 'Horners',
      12 => 'Hyphema',
      13 => 'Microphthalmia',
      14 => 'NeovascularizationPannus',
      15 => 'Other',
      16 => 'Scleral Injection',
    ),
    'Respiratory' => 
    array (
      0 => '',
      1 => 'Apnea',
      2 => 'Coughing',
      3 => 'Dyspnea',
      4 => 'Hemoptysis',
      5 => 'Lung lesions',
      6 => 'Nasal discharge',
      7 => 'Other',
      8 => 'Pleural effusion',
      9 => 'Respiratory Stridor',
      10 => 'Tachypnea',
      11 => 'Respiratory distress',
    ),
    'Surgical' => 
    array (
      0 => '',
      1 => 'Bandage abnormality',
      2 => 'Bruising',
      3 => 'Catheter Site Swelling',
      4 => 'Dehiscence',
      5 => 'Discharge',
      6 => 'Hemorrhage',
      7 => 'Incisional swelling',
      8 => 'Other',
      9 => 'Recumbency',
      10 => 'Self mutilation',
      11 => 'Swelling',
      12 => 'Anesthesia',
      13 => 'Infection',
      14 => 'Intubation',
      15 => 'Mass',
      16 => 'Post op Swelling',
    ),
    'Systemic' => 
    array (
      0 => '',
      1 => 'Anaphylaxis',
      2 => 'Dehydration',
      3 => 'Hemorrhage',
      4 => 'Hypertension',
      5 => 'Hypoglycemia',
      6 => 'Hypotension',
      7 => 'Hypothermia',
      8 => 'Icterus',
      9 => 'Malaise',
      10 => 'Obesity',
      11 => 'Other',
      12 => 'Pallor',
      13 => 'PetechiaEcchyomoses',
      14 => 'Pyrexia',
      15 => 'Shock',
      16 => 'Underweight',
      17 => 'Weight loss',
      18 => 'Lymphadenopathy',
      19 => 'Icteric',
    ),
    'Unknown ADR' => 
    array (
      0 => '',
      1 => 'ADR',
      2 => 'Other',
    ),
    'Urogential' => 
    array (
      0 => '',
      1 => 'Bilirubinuria',
      2 => 'Discolored urine',
      3 => 'Hematuria',
      4 => 'Other',
      5 => 'Paraphimosis',
      6 => 'Porphyrinuria',
      7 => 'Preputial Swelling',
      8 => 'Pyuria',
      9 => 'StrainingDysuria',
      10 => 'Trauma',
      11 => 'Udder Enlargement',
      12 => 'Urinary obstruction',
      13 => 'Vulvar discharge',
      14 => 'Vulvar Swelling',
      15 => 'Azotemia',
    ),
    'Lymphatic' => 
    array (
      0 => '',
      1 => 'Abscess',
      2 => 'Other',
      3 => 'Lymphadenopathy',
      4 => 'Necropsy Finding',
    ),
    'Nervous System' => 
    array (
    ),
    'Integument' => 
    array (
    ),
    'Urogenital' => 
    array (
    ),
    'Clin Path' => 
    array (
    ),
    'Not Applicable' => 
    array (
      0 => '',
      1 => 'Normal Exam',
      2 => 'QAcclimation Activity',
      3 => 'Post Op Exam',
      4 => 'Routine Exam',
    ),
  ),
);

 ?>