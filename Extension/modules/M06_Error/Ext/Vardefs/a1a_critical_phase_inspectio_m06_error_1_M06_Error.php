<?php
// created: 2019-02-21 13:44:24
$dictionary["M06_Error"]["fields"]["a1a_critical_phase_inspectio_m06_error_1"] = array (
  'name' => 'a1a_critical_phase_inspectio_m06_error_1',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_m06_error_1',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'side' => 'right',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'a1a_critic9e89spectio_ida',
  'link-type' => 'one',
);
$dictionary["M06_Error"]["fields"]["a1a_critical_phase_inspectio_m06_error_1_name"] = array (
  'name' => 'a1a_critical_phase_inspectio_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'save' => true,
  'id_name' => 'a1a_critic9e89spectio_ida',
  'link' => 'a1a_critical_phase_inspectio_m06_error_1',
  'table' => 'a1a_critical_phase_inspectio',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["a1a_critic9e89spectio_ida"] = array (
  'name' => 'a1a_critic9e89spectio_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_M06_ERROR_TITLE_ID',
  'id_name' => 'a1a_critic9e89spectio_ida',
  'link' => 'a1a_critical_phase_inspectio_m06_error_1',
  'table' => 'a1a_critical_phase_inspectio',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
