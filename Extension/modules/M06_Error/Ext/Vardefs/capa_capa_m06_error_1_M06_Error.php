<?php
// created: 2022-02-03 07:30:32
$dictionary["M06_Error"]["fields"]["capa_capa_m06_error_1"] = array (
  'name' => 'capa_capa_m06_error_1',
  'type' => 'link',
  'relationship' => 'capa_capa_m06_error_1',
  'source' => 'non-db',
  'module' => 'CAPA_CAPA',
  'bean_name' => 'CAPA_CAPA',
  'side' => 'right',
  'vname' => 'LBL_CAPA_CAPA_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'capa_capa_m06_error_1capa_capa_ida',
  'link-type' => 'one',
);
$dictionary["M06_Error"]["fields"]["capa_capa_m06_error_1_name"] = array (
  'name' => 'capa_capa_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CAPA_CAPA_M06_ERROR_1_FROM_CAPA_CAPA_TITLE',
  'save' => true,
  'id_name' => 'capa_capa_m06_error_1capa_capa_ida',
  'link' => 'capa_capa_m06_error_1',
  'table' => 'capa_capa',
  'module' => 'CAPA_CAPA',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["capa_capa_m06_error_1capa_capa_ida"] = array (
  'name' => 'capa_capa_m06_error_1capa_capa_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CAPA_CAPA_M06_ERROR_1_FROM_M06_ERROR_TITLE_ID',
  'id_name' => 'capa_capa_m06_error_1capa_capa_ida',
  'link' => 'capa_capa_m06_error_1',
  'table' => 'capa_capa',
  'module' => 'CAPA_CAPA',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
