<?php
 // created: 2021-02-11 14:47:52
$dictionary['M06_Error']['fields']['date_time_discovered_c']['labelValue']='Date Time Discovered1';
$dictionary['M06_Error']['fields']['date_time_discovered_c']['enforced']='';
$dictionary['M06_Error']['fields']['date_time_discovered_c']['dependency']='or(equal($error_category_c,"Deceased Animal"),equal($error_type_c,"Found Deceased"))';
$dictionary['M06_Error']['fields']['date_time_discovered_c']['required_formula']='';

 ?>