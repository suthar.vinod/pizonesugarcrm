<?php
 // created: 2020-12-15 11:52:33
$dictionary['M06_Error']['fields']['time_to_discovery_days_c']['labelValue']='Time to Discovery (Days)';
$dictionary['M06_Error']['fields']['time_to_discovery_days_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['time_to_discovery_days_c']['formula']='ifElse(not(equal($date_error_documented_c,"")),abs(subtract(daysUntil($date_error_occurred_c),daysUntil($date_error_documented_c))),abs(subtract(daysUntil($date_error_occurred_c),daysUntil($date_time_discovered_c))))';
$dictionary['M06_Error']['fields']['time_to_discovery_days_c']['enforced']='false';
$dictionary['M06_Error']['fields']['time_to_discovery_days_c']['dependency']='';
$dictionary['M06_Error']['fields']['time_to_discovery_days_c']['required_formula']='';

 ?>