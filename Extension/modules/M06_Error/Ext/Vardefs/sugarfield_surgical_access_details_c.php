<?php
 // created: 2022-01-11 06:57:02
$dictionary['M06_Error']['fields']['surgical_access_details_c']['labelValue']='Surgical Access Details';
$dictionary['M06_Error']['fields']['surgical_access_details_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['surgical_access_details_c']['enforced']='';
$dictionary['M06_Error']['fields']['surgical_access_details_c']['dependency']='equal($surgical_access_c,"Yes")';
$dictionary['M06_Error']['fields']['surgical_access_details_c']['required_formula']='equal($surgical_access_c,"Yes")';
$dictionary['M06_Error']['fields']['surgical_access_details_c']['readonly_formula']='';

 ?>