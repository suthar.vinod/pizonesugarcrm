<?php
 // created: 2019-10-02 12:56:43
$dictionary['M06_Error']['fields']['target_pi_assessment_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_pi_assessment_date_c']['labelValue']='Target PI Assessment Date';
$dictionary['M06_Error']['fields']['target_pi_assessment_date_c']['calculated']='true';
$dictionary['M06_Error']['fields']['target_pi_assessment_date_c']['formula']='addDays($date_entered,14)';
$dictionary['M06_Error']['fields']['target_pi_assessment_date_c']['enforced']='true';
$dictionary['M06_Error']['fields']['target_pi_assessment_date_c']['dependency']='equal($error_type_c,"Complaint")';

 ?>