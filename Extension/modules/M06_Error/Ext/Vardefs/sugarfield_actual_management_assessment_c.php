<?php
 // created: 2021-12-21 08:38:52
$dictionary['M06_Error']['fields']['actual_management_assessment_c']['labelValue']='Actual Subtype Date';
$dictionary['M06_Error']['fields']['actual_management_assessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_management_assessment_c']['dependency']='not(equal($error_category_c,"IACUC Deficiency"))';
$dictionary['M06_Error']['fields']['actual_management_assessment_c']['required_formula']='';
$dictionary['M06_Error']['fields']['actual_management_assessment_c']['readonly_formula']='';

 ?>