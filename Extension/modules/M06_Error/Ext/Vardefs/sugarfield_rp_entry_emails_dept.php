<?php
/**
 * Created by PhpStorm.
 * User: nabeel.alvi
 * Date: 7/23/18
 * Time: 5:23 PM
 */

$dictionary["M06_Error"]["fields"]["rp_entry_email_dept"] = array(
    'name' => 'rp_entry_email_dept',
    'label' => 'LBL_ENTRY_EMAIL_DEPT',
    'type' => 'text',
    'dbType' => 'blob',
    'audited' => false,
    'mass_update' => false,
    'module' => 'M06_Error',
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
    'source' => 'db'
);
?>
