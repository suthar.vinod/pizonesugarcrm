<?php
$dictionary["M06_Error"]["fields"]["related_data"] = array(
    'name' => 'related_data',
    'label' => 'LBL_RELATED_DATA',
    'type' => 'text',
    'dbType' => 'blob',
    'default_value' => '',
    'audited' => false,
    'mass_update' => false,
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
    'source' => 'db',
);
?>
