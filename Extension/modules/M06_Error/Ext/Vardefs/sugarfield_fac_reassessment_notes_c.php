<?php
 // created: 2019-10-10 12:31:56
$dictionary['M06_Error']['fields']['fac_reassessment_notes_c']['labelValue']='Facilities Reassessment';
$dictionary['M06_Error']['fields']['fac_reassessment_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['fac_reassessment_notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['fac_reassessment_notes_c']['dependency']='equal($fac_reassessment_required_c,true)';

 ?>