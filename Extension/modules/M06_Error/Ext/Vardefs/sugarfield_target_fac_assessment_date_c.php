<?php
 // created: 2019-10-02 13:48:44
$dictionary['M06_Error']['fields']['target_fac_assessment_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_fac_assessment_date_c']['labelValue']='Target Facilities Assessment Date';
$dictionary['M06_Error']['fields']['target_fac_assessment_date_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_fac_assessment_date_c']['formula']='addDays($date_entered,14)';
$dictionary['M06_Error']['fields']['target_fac_assessment_date_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_fac_assessment_date_c']['dependency']='or(equal($error_category_c,"Maintenance Request"),equal($error_type_c,"Facility"))';

 ?>