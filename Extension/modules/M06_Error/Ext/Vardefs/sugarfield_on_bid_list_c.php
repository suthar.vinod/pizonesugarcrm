<?php
 // created: 2022-09-08 05:51:51
$dictionary['M06_Error']['fields']['on_bid_list_c']['labelValue']='On BID list';
$dictionary['M06_Error']['fields']['on_bid_list_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['on_bid_list_c']['enforced']='false';
$dictionary['M06_Error']['fields']['on_bid_list_c']['dependency']='';
$dictionary['M06_Error']['fields']['on_bid_list_c']['required_formula']='';
$dictionary['M06_Error']['fields']['on_bid_list_c']['readonly']='1';
$dictionary['M06_Error']['fields']['on_bid_list_c']['readonly_formula']='';

 ?>