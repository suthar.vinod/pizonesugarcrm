<?php
 // created: 2020-11-19 11:44:37
$dictionary['M06_Error']['fields']['why_it_happened_c']['labelValue']='Does Submitter know Why it Happened?';
$dictionary['M06_Error']['fields']['why_it_happened_c']['dependency']='isInList($error_category_c,createList("Daily QC","Data Book QC","Real time study conduct","Retrospective Data QC","Training","Weekly Sweep"))';
$dictionary['M06_Error']['fields']['why_it_happened_c']['required_formula']='';
$dictionary['M06_Error']['fields']['why_it_happened_c']['visibility_grid']='';

 ?>