<?php
 // created: 2021-05-25 07:19:13
$dictionary['M06_Error']['fields']['plan_comments_c']['labelValue']='Plan Comments';
$dictionary['M06_Error']['fields']['plan_comments_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['plan_comments_c']['enforced']='';
$dictionary['M06_Error']['fields']['plan_comments_c']['dependency']='';
$dictionary['M06_Error']['fields']['plan_comments_c']['required_formula']='';
$dictionary['M06_Error']['fields']['plan_comments_c']['readonly_formula']='';

 ?>