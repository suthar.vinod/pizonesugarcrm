<?php

$dictionary["M06_Error"]["fields"]["wordpress_flag"] = array(
    'name' => 'wordpress_flag',
    'label' => 'LBL_WORDPRESS_FLAG',
    'type' => 'bool',
    'module' => 'M06_Error',
    'default_value' => false,
    'audited' => false,
    'mass_update' => false,
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
);
?>
