<?php
 // created: 2020-11-02 17:44:57
$dictionary['M06_Error']['fields']['impactful_dd_c']['labelValue']='Impactful?';
$dictionary['M06_Error']['fields']['impactful_dd_c']['dependency']='';
$dictionary['M06_Error']['fields']['impactful_dd_c']['required_formula']='';
$dictionary['M06_Error']['fields']['impactful_dd_c']['visibility_grid']=array (
  'trigger' => 'error_classification_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Adverse Event' => 
    array (
      0 => '',
      1 => 'This Adverse Event was impactful',
      2 => 'This Adverse Event was NOT impactful',
      3 => 'Pending needs further assessment before finalizing',
    ),
    'Error' => 
    array (
      0 => '',
      1 => 'This Deviation was impactful',
      2 => 'This Deviation was NOT impactful',
      3 => 'Pending needs further assessment before finalizing',
    ),
    'Duplicate' => 
    array (
    ),
    'Job well done' => 
    array (
    ),
    'Notification' => 
    array (
    ),
    'Observation' => 
    array (
    ),
    'Opportunity for Improvement' => 
    array (
    ),
    'Pending' => 
    array (
    ),
    'Impactful Deviation' => 
    array (
    ),
    'Non Impactful Deviation' => 
    array (
    ),
    'Unforeseen Circumstance Unanticipated Response' => 
    array (
    ),
    'Withdrawn Error' => 
    array (
    ),
    'A' => 
    array (
    ),
    'M' => 
    array (
    ),
    'S' => 
    array (
    ),
    'C' => 
    array (
    ),
    'NA' => 
    array (
    ),
    'Major Change or New' => 
    array (
    ),
    'Revision Change or Minor Change' => 
    array (
    ),
    'Employee did NOT perform SOP tasks while overdue' => 
    array (
    ),
    'The Controlled Document is a standalone document. No Controlled Document activities are associated' => 
    array (
    ),
    'Overdue refresher training' => 
    array (
    ),
    'Adverse Event Study Article Related' => 
    array (
      0 => '',
      1 => 'Pending needs further assessment before finalizing',
      2 => 'This Adverse Event was impactful',
      3 => 'This Adverse Event was NOT impactful',
    ),
    'Adverse Event Procedure Related' => 
    array (
      0 => '',
      1 => 'Pending needs further assessment before finalizing',
      2 => 'This Adverse Event was impactful',
      3 => 'This Adverse Event was NOT impactful',
    ),
    'Adverse Event Test System Model Related' => 
    array (
      0 => '',
      1 => 'Pending needs further assessment before finalizing',
      2 => 'This Adverse Event was impactful',
      3 => 'This Adverse Event was NOT impactful',
    ),
    'Adverse Event Etiology Unknown' => 
    array (
      0 => '',
      1 => 'Pending needs further assessment before finalizing',
      2 => 'This Adverse Event was impactful',
      3 => 'This Adverse Event was NOT impactful',
    ),
  ),
);

 ?>