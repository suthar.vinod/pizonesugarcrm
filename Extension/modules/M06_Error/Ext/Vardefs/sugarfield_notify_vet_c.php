<?php
 // created: 2021-05-25 07:17:20
$dictionary['M06_Error']['fields']['notify_vet_c']['labelValue']='Notify a veterinarian if the following clinical signs are observed';
$dictionary['M06_Error']['fields']['notify_vet_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['notify_vet_c']['enforced']='';
$dictionary['M06_Error']['fields']['notify_vet_c']['dependency']='';
$dictionary['M06_Error']['fields']['notify_vet_c']['required_formula']='';
$dictionary['M06_Error']['fields']['notify_vet_c']['readonly_formula']='';

 ?>