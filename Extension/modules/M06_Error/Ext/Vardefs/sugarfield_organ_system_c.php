<?php
 // created: 2021-06-01 10:04:23
$dictionary['M06_Error']['fields']['organ_system_c']['labelValue']='Organ System';
$dictionary['M06_Error']['fields']['organ_system_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal","Work Product Schedule Outcome"))';
$dictionary['M06_Error']['fields']['organ_system_c']['required_formula']='';
$dictionary['M06_Error']['fields']['organ_system_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['organ_system_c']['visibility_grid']='';

 ?>