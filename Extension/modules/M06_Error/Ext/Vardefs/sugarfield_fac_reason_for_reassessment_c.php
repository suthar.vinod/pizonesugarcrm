<?php
 // created: 2019-10-10 12:01:59
$dictionary['M06_Error']['fields']['fac_reason_for_reassessment_c']['labelValue']='Facilities Reason for Reassessment';
$dictionary['M06_Error']['fields']['fac_reason_for_reassessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['fac_reason_for_reassessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['fac_reason_for_reassessment_c']['dependency']='equal($fac_reassessment_required_c,true)';

 ?>