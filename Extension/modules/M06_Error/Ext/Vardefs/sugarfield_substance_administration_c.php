<?php
 // created: 2022-01-11 06:49:22
$dictionary['M06_Error']['fields']['substance_administration_c']['labelValue']='Substance Administration';
$dictionary['M06_Error']['fields']['substance_administration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['substance_administration_c']['enforced']='';
$dictionary['M06_Error']['fields']['substance_administration_c']['dependency']='isInList($error_type_c,createList("Not Used","Not used shared BU","Rejected"))';
$dictionary['M06_Error']['fields']['substance_administration_c']['required_formula']='';
$dictionary['M06_Error']['fields']['substance_administration_c']['readonly_formula']='';

 ?>