<?php
 // created: 2020-05-19 18:31:04
$dictionary['M06_Error']['fields']['target_vet_assessment_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_vet_assessment_date_c']['labelValue']='Target Vet Assessment Date';
$dictionary['M06_Error']['fields']['target_vet_assessment_date_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_vet_assessment_date_c']['formula']='$date_entered';
$dictionary['M06_Error']['fields']['target_vet_assessment_date_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_vet_assessment_date_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal"))';

 ?>