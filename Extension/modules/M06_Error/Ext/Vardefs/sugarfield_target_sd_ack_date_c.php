<?php
 // created: 2020-12-15 08:26:51
$dictionary['M06_Error']['fields']['target_sd_ack_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_sd_ack_date_c']['labelValue']='Target SD Acknowledgement Date';
$dictionary['M06_Error']['fields']['target_sd_ack_date_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_sd_ack_date_c']['formula']='addDays($date_entered,3)';
$dictionary['M06_Error']['fields']['target_sd_ack_date_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_sd_ack_date_c']['dependency']='and(not(equal($wp_name_c,"")),not(isInList($error_category_c,createList("Feedback","Internal Feedback"))))';

 ?>