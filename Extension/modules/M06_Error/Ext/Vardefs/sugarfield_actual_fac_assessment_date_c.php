<?php
 // created: 2019-10-02 13:49:59
$dictionary['M06_Error']['fields']['actual_fac_assessment_date_c']['labelValue']='Actual Facilities Assessment Date';
$dictionary['M06_Error']['fields']['actual_fac_assessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_fac_assessment_date_c']['dependency']='or(equal($error_category_c,"Maintenance Request"),equal($error_type_c,"Facility"))';

 ?>