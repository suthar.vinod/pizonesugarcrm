<?php
 // created: 2020-01-16 18:01:03
$dictionary['M06_Error']['fields']['date_error_documented_c']['labelValue']='Date Discovered';
$dictionary['M06_Error']['fields']['date_error_documented_c']['enforced']='';
$dictionary['M06_Error']['fields']['date_error_documented_c']['dependency']='isInList($error_category_c,createList("Critical Phase Inspection","Daily QC","Data Book QC","External Audit","Maintenance Request","Process Audit","Real time study conduct","Weekly Sweep","Internal Feedback","IACUC Deficiency","Equipment Maintenance Request","Retrospective Data QC","Test Failure"))';

 ?>