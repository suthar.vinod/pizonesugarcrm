<?php
 // created: 2020-01-13 15:11:06
$dictionary['M06_Error']['fields']['compliance_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['compliance_c']['labelValue']='Compliance';
$dictionary['M06_Error']['fields']['compliance_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['compliance_c']['calculated']='1';
$dictionary['M06_Error']['fields']['compliance_c']['formula']='related($m06_error_m03_work_product_1,"work_product_compliance_c")';
$dictionary['M06_Error']['fields']['compliance_c']['enforced']='1';
$dictionary['M06_Error']['fields']['compliance_c']['dependency']='';

 ?>