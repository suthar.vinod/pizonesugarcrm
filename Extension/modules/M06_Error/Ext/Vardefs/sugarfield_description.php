<?php
 // created: 2020-06-04 15:10:16
$dictionary['M06_Error']['fields']['description']['audited']=true;
$dictionary['M06_Error']['fields']['description']['massupdate']=false;
$dictionary['M06_Error']['fields']['description']['comments']='Full text of the note';
$dictionary['M06_Error']['fields']['description']['duplicate_merge']='enabled';
$dictionary['M06_Error']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['M06_Error']['fields']['description']['merge_filter']='disabled';
$dictionary['M06_Error']['fields']['description']['unified_search']=false;
$dictionary['M06_Error']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['M06_Error']['fields']['description']['calculated']=false;
$dictionary['M06_Error']['fields']['description']['rows']='6';
$dictionary['M06_Error']['fields']['description']['cols']='80';

 ?>