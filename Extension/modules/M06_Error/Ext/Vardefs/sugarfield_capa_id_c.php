<?php
 // created: 2020-02-21 13:11:45
$dictionary['M06_Error']['fields']['capa_id_c']['labelValue']='CAPA ID';
$dictionary['M06_Error']['fields']['capa_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['capa_id_c']['enforced']='';
$dictionary['M06_Error']['fields']['capa_id_c']['dependency']='equal($capa_initiation_c,"Yes")';

 ?>