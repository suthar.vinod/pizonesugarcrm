<?php
 // created: 2021-01-26 19:12:17
$dictionary['M06_Error']['fields']['error_classification_c']['labelValue']='Classification';
$dictionary['M06_Error']['fields']['error_classification_c']['dependency']='';
$dictionary['M06_Error']['fields']['error_classification_c']['required_formula']='and(not(equal($resolution_c,"")),not(isInList($error_category_c,createList("IACUC Deficiency","Training","Work Product Schedule Outcome","Equipment Maintenance Request","External Audit","Feedback","Maintenance Request","Internal Feedback","Process Audit","Weekly Sweep"))))';
$dictionary['M06_Error']['fields']['error_classification_c']['visibility_grid']=array (
  'trigger' => 'error_category_c',
  'values' => 
  array (
    'Sttudy' => 
    array (
      0 => 'Choose One',
      1 => 'Error',
      2 => 'Impactful Deviation',
      3 => 'Observation',
      4 => 'Pending',
    ),
    'Facility' => 
    array (
      0 => 'Choose One',
      1 => 'Error',
      2 => 'Impactful Deviation',
      3 => 'Observation',
      4 => 'Pending',
    ),
    'Choose One' => 
    array (
      0 => '',
      1 => 'Adverse Event',
      2 => 'Error',
      3 => 'Duplicate',
      4 => 'Notification',
      5 => 'Opportunity for Improvement',
      6 => 'Pending',
      7 => 'Impactful Deviation',
      8 => 'Observation',
      9 => 'Non Impactful Deviation',
      10 => 'Unforeseen Circumstance Unanticipated Response',
      11 => 'Withdrawn Error',
    ),
    'Critical Phase Inspection' => 
    array (
      0 => '',
      1 => 'Addressed CPI COM',
      2 => 'Adverse Event Study Article Related',
      3 => 'Adverse Event Procedure Related',
      4 => 'Adverse Event Test System Model Related',
      5 => 'Adverse Event Etiology Unknown',
      6 => 'Error',
      7 => 'Duplicate',
      8 => 'Notification',
      9 => 'Opportunity for Improvement',
      10 => 'Pending',
      11 => 'Impactful Deviation',
      12 => 'Observation',
      13 => 'Non Impactful Deviation',
      14 => 'Unforeseen Circumstance Unanticipated Response',
      15 => 'Withdrawn Error',
      16 => 'Adverse Event',
    ),
    'Deceased Animal' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Duplicate',
      6 => 'Adverse Event',
    ),
    'Maintenance Request' => 
    array (
    ),
    'Observation' => 
    array (
      0 => 'Choose One',
      1 => 'Error',
      2 => 'Impactful Deviation',
      3 => 'Observation',
      4 => 'Pending',
    ),
    'Rejected Animal' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Impactful Deviation',
      11 => 'Observation',
      12 => 'Non Impactful Deviation',
      13 => 'Unforeseen Circumstance Unanticipated Response',
      14 => 'Withdrawn Error',
      15 => 'Adverse Event',
    ),
    'Vet Check' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Impactful Deviation',
      11 => 'Observation',
      12 => 'Non Impactful Deviation',
      13 => 'Unforeseen Circumstance Unanticipated Response',
      14 => 'Withdrawn Error',
      15 => 'Adverse Event',
    ),
    '' => 
    array (
    ),
    'Daily QC' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Impactful Deviation',
      11 => 'Observation',
      12 => 'Non Impactful Deviation',
      13 => 'Unforeseen Circumstance Unanticipated Response',
      14 => 'Withdrawn Error',
      15 => 'Adverse Event',
    ),
    'Data Book QC' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Impactful Deviation',
      11 => 'Observation',
      12 => 'Non Impactful Deviation',
      13 => 'Unforeseen Circumstance Unanticipated Response',
      14 => 'Withdrawn Error',
      15 => 'Adverse Event',
    ),
    'Real time study conduct' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Impactful Deviation',
      11 => 'Observation',
      12 => 'Non Impactful Deviation',
      13 => 'Unforeseen Circumstance Unanticipated Response',
      14 => 'Withdrawn Error',
      15 => 'Adverse Event',
    ),
    'Weekly Sweep' => 
    array (
    ),
    'External Audit' => 
    array (
    ),
    'Feedback' => 
    array (
      0 => '',
      1 => 'Job well done',
      2 => 'Opportunity for Improvement',
    ),
    'Process Audit' => 
    array (
    ),
    'Project Management' => 
    array (
      0 => 'Notification',
    ),
    'Gross Pathology' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Impactful Deviation',
      11 => 'Observation',
      12 => 'Non Impactful Deviation',
      13 => 'Unforeseen Circumstance Unanticipated Response',
      14 => 'Withdrawn Error',
      15 => 'Adverse Event',
    ),
    'Internal Feedback' => 
    array (
    ),
    'IACUC Deficiency' => 
    array (
    ),
    'Equipment Maintenance Request' => 
    array (
    ),
    'Retrospective Data QC' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Adverse Event',
    ),
    'Training' => 
    array (
    ),
    'Test Failure' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Adverse Event',
    ),
    'Work Product Schedule Outcome' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
    ),
  ),
);

 ?>