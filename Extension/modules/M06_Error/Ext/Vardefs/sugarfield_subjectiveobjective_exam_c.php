<?php
 // created: 2021-05-25 07:14:02
$dictionary['M06_Error']['fields']['subjectiveobjective_exam_c']['labelValue']='Subjective/Objective Examination';
$dictionary['M06_Error']['fields']['subjectiveobjective_exam_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['subjectiveobjective_exam_c']['enforced']='';
$dictionary['M06_Error']['fields']['subjectiveobjective_exam_c']['dependency']='';
$dictionary['M06_Error']['fields']['subjectiveobjective_exam_c']['required_formula']='';
$dictionary['M06_Error']['fields']['subjectiveobjective_exam_c']['readonly_formula']='';

 ?>