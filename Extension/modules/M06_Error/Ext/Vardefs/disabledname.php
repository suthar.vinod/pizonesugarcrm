<?php

$dictionary['M06_Error']['fields']['name']['readonly']=true;
$dictionary['M06_Error']['fields']['name']['required']=false;
$dictionary['M06_Error']['fields']['name']['len']='255';
$dictionary['M06_Error']['fields']['name']['audited']=true;
$dictionary['M06_Error']['fields']['name']['massupdate']=false;
$dictionary['M06_Error']['fields']['name']['unified_search']=false;
$dictionary['M06_Error']['fields']['name']['full_text_search']=array (
    'enabled' => true,
    'boost' => '1.55',
    'searchable' => true,
);

$dictionary['M06_Error']['fields']['name']['importable']='false';
$dictionary['M06_Error']['fields']['name']['duplicate_merge']='disabled';
$dictionary['M06_Error']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['name']['merge_filter']='disabled';
$dictionary['M06_Error']['fields']['name']['enforced']=true;
