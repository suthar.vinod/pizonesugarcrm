<?php
 // created: 2020-05-19 18:33:36
$dictionary['M06_Error']['fields']['vet_assessment_notes_2_c']['labelValue']='Vet Assessment';
$dictionary['M06_Error']['fields']['vet_assessment_notes_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['vet_assessment_notes_2_c']['enforced']='';
$dictionary['M06_Error']['fields']['vet_assessment_notes_2_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal"))';

 ?>