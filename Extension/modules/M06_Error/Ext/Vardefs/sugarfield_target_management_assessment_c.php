<?php
 // created: 2021-12-21 08:45:49
$dictionary['M06_Error']['fields']['target_management_assessment_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_management_assessment_c']['labelValue']='Target Subtype Date';
$dictionary['M06_Error']['fields']['target_management_assessment_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_management_assessment_c']['formula']='addDays($date_entered,3)';
$dictionary['M06_Error']['fields']['target_management_assessment_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_management_assessment_c']['dependency']='not(equal($error_category_c,"IACUC Deficiency"))';
$dictionary['M06_Error']['fields']['target_management_assessment_c']['required_formula']='';
$dictionary['M06_Error']['fields']['target_management_assessment_c']['readonly_formula']='';

 ?>