<?php
 // created: 2021-09-09 07:59:51
$dictionary['M06_Error']['fields']['reinspection_date_c']['labelValue']='Actual SD Assessment Date';
$dictionary['M06_Error']['fields']['reinspection_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['reinspection_date_c']['dependency']='and(not(equal($actual_sd_ack_date_c,"")),not(equal($error_category_c,"Study Specific Charge")))';
$dictionary['M06_Error']['fields']['reinspection_date_c']['required_formula']='not(equal($resolution_c,""))';
$dictionary['M06_Error']['fields']['reinspection_date_c']['readonly_formula']='';

 ?>