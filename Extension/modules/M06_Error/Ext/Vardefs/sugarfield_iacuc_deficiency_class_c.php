<?php
 // created: 2020-12-15 05:10:55
$dictionary['M06_Error']['fields']['iacuc_deficiency_class_c']['labelValue']='IACUC Deficiency Classification';
$dictionary['M06_Error']['fields']['iacuc_deficiency_class_c']['dependency']='equal($error_category_c,"IACUC Deficiency")';
$dictionary['M06_Error']['fields']['iacuc_deficiency_class_c']['required_formula']='';
$dictionary['M06_Error']['fields']['iacuc_deficiency_class_c']['visibility_grid']='';

 ?>