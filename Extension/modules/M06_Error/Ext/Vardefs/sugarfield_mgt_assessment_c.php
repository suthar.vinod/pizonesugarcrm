<?php
 // created: 2021-12-21 08:34:57
$dictionary['M06_Error']['fields']['mgt_assessment_c']['labelValue']='Management Assessment';
$dictionary['M06_Error']['fields']['mgt_assessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['mgt_assessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['mgt_assessment_c']['dependency']='not(equal($error_category_c,"IACUC Deficiency"))';
$dictionary['M06_Error']['fields']['mgt_assessment_c']['readonly_formula']='';

 ?>