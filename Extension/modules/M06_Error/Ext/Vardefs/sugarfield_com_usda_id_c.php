<?php
 // created: 2021-08-12 13:19:10
$dictionary['M06_Error']['fields']['com_usda_id_c']['labelValue']='Com USDA Id';
$dictionary['M06_Error']['fields']['com_usda_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['com_usda_id_c']['enforced']='';
$dictionary['M06_Error']['fields']['com_usda_id_c']['dependency']='';
$dictionary['M06_Error']['fields']['com_usda_id_c']['required_formula']='';
$dictionary['M06_Error']['fields']['com_usda_id_c']['readonly_formula']='';

 ?>