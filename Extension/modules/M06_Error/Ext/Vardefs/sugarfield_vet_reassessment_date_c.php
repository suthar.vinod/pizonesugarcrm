<?php
 // created: 2021-06-15 11:35:43
$dictionary['M06_Error']['fields']['vet_reassessment_date_c']['labelValue']='Actual Vet Reassessment Date';
$dictionary['M06_Error']['fields']['vet_reassessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['vet_reassessment_date_c']['dependency']='and(equal($vet_reassessment_required_c,true),isBefore($date_entered,date("6/7/2021")))';
$dictionary['M06_Error']['fields']['vet_reassessment_date_c']['required_formula']='';
$dictionary['M06_Error']['fields']['vet_reassessment_date_c']['readonly_formula']='';

 ?>