<?php
 // created: 2020-01-24 13:10:20
$dictionary['M06_Error']['fields']['target_eq_reassess_date_c']['labelValue']='Target Equipment Reassessment Date';
$dictionary['M06_Error']['fields']['target_eq_reassess_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['target_eq_reassess_date_c']['dependency']='equal($equipment_reassessment_req_c,true)';

 ?>