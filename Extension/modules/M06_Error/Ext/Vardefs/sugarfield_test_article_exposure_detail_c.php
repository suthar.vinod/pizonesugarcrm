<?php
 // created: 2022-01-11 07:03:42
$dictionary['M06_Error']['fields']['test_article_exposure_detail_c']['labelValue']='Test Article Exposure Details';
$dictionary['M06_Error']['fields']['test_article_exposure_detail_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['test_article_exposure_detail_c']['enforced']='';
$dictionary['M06_Error']['fields']['test_article_exposure_detail_c']['dependency']='equal($test_article_exposure_c,"Yes")';
$dictionary['M06_Error']['fields']['test_article_exposure_detail_c']['required_formula']='equal($test_article_exposure_c,"Yes")';
$dictionary['M06_Error']['fields']['test_article_exposure_detail_c']['readonly_formula']='';

 ?>