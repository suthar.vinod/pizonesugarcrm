<?php
 // created: 2021-06-01 10:02:36
$dictionary['M06_Error']['fields']['related_to_c']['labelValue']='Related to';
$dictionary['M06_Error']['fields']['related_to_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal","Work Product Schedule Outcome"))';
$dictionary['M06_Error']['fields']['related_to_c']['required_formula']='';
$dictionary['M06_Error']['fields']['related_to_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['related_to_c']['visibility_grid']='';

 ?>