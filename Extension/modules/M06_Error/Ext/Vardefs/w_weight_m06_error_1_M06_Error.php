<?php
// created: 2020-01-07 13:27:00
$dictionary["M06_Error"]["fields"]["w_weight_m06_error_1"] = array (
  'name' => 'w_weight_m06_error_1',
  'type' => 'link',
  'relationship' => 'w_weight_m06_error_1',
  'source' => 'non-db',
  'module' => 'W_Weight',
  'bean_name' => 'W_Weight',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_1_FROM_W_WEIGHT_TITLE',
  'id_name' => 'w_weight_m06_error_1w_weight_ida',
);
$dictionary["M06_Error"]["fields"]["w_weight_m06_error_1_name"] = array (
  'name' => 'w_weight_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_1_FROM_W_WEIGHT_TITLE',
  'save' => true,
  'id_name' => 'w_weight_m06_error_1w_weight_ida',
  'link' => 'w_weight_m06_error_1',
  'table' => 'w_weight',
  'module' => 'W_Weight',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["w_weight_m06_error_1w_weight_ida"] = array (
  'name' => 'w_weight_m06_error_1w_weight_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_1_FROM_W_WEIGHT_TITLE_ID',
  'id_name' => 'w_weight_m06_error_1w_weight_ida',
  'link' => 'w_weight_m06_error_1',
  'table' => 'w_weight',
  'module' => 'W_Weight',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
