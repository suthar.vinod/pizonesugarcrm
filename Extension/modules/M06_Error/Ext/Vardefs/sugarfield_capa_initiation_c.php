<?php
 // created: 2020-12-15 08:24:02
$dictionary['M06_Error']['fields']['capa_initiation_c']['labelValue']='CAPA Initiation';
$dictionary['M06_Error']['fields']['capa_initiation_c']['dependency']='or(equal($error_type_c,"Complaint"),equal($impactful_c,true),isInList($impactful_dd_c,createList("This Deviation was impactful","This Adverse Event was impactful")))';
$dictionary['M06_Error']['fields']['capa_initiation_c']['required_formula']='';
$dictionary['M06_Error']['fields']['capa_initiation_c']['visibility_grid']='';

 ?>