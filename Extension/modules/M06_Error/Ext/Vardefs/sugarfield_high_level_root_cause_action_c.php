<?php
 // created: 2021-01-26 09:11:54
$dictionary['M06_Error']['fields']['high_level_root_cause_action_c']['labelValue']='High Level Root Cause/Action';
$dictionary['M06_Error']['fields']['high_level_root_cause_action_c']['dependency']='not(equal($error_category_c,"IACUC Deficiency"))';
$dictionary['M06_Error']['fields']['high_level_root_cause_action_c']['required_formula']='not(equal($mgt_assessment_c,""))';
$dictionary['M06_Error']['fields']['high_level_root_cause_action_c']['visibility_grid']='';

 ?>