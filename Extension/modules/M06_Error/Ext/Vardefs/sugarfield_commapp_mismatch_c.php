<?php
 // created: 2021-06-03 10:19:44
$dictionary['M06_Error']['fields']['commapp_mismatch_c']['labelValue']='Commapp Mismatch Update';
$dictionary['M06_Error']['fields']['commapp_mismatch_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['commapp_mismatch_c']['enforced']='';
$dictionary['M06_Error']['fields']['commapp_mismatch_c']['dependency']='';
$dictionary['M06_Error']['fields']['commapp_mismatch_c']['required_formula']='';
$dictionary['M06_Error']['fields']['commapp_mismatch_c']['readonly_formula']='';

 ?>