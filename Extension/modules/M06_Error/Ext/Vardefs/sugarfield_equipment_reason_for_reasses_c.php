<?php
 // created: 2020-01-24 13:16:17
$dictionary['M06_Error']['fields']['equipment_reason_for_reasses_c']['labelValue']='Equipment Reason for Reassessment';
$dictionary['M06_Error']['fields']['equipment_reason_for_reasses_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['equipment_reason_for_reasses_c']['enforced']='';
$dictionary['M06_Error']['fields']['equipment_reason_for_reasses_c']['dependency']='equal($equipment_reassessment_req_c,true)';

 ?>