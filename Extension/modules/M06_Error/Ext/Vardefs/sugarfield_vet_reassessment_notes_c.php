<?php
 // created: 2021-06-15 11:36:18
$dictionary['M06_Error']['fields']['vet_reassessment_notes_c']['labelValue']='Vet Reassessment';
$dictionary['M06_Error']['fields']['vet_reassessment_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['vet_reassessment_notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['vet_reassessment_notes_c']['dependency']='and(equal($vet_reassessment_required_c,true),isBefore($date_entered,date("6/7/2021")))';
$dictionary['M06_Error']['fields']['vet_reassessment_notes_c']['required_formula']='';
$dictionary['M06_Error']['fields']['vet_reassessment_notes_c']['readonly_formula']='';

 ?>