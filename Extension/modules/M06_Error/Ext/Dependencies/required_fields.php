<?php
$dependencies['M06_Error']['required_fields'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('error_classification_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'mgt_assessment_c',
                'value' => 'isInList($error_classification_c, createList("Major Change or New","Revision Change or Minor Change"))',
            ), 
        ),  
        
    ),
);