<?php
$dependencies['M06_Error']['readonly_fields'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('error_category_c','date_time_discovered_c'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'date_error_occurred_c',
                'value' => 'equal($error_category_c,"Deceased Animal")',
                
            ),
             
        ), 
    ),
);
$dependencies['M06_Error']['setfield_value'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'isInList($error_category_c,createList("Critical Phase Inspection", "Daily QC", "Data Book QC", "External Audit", "Internal Feedback", "Process Audit", "Real time study conduct", "Retrospective Data QC", "Test Failure", "Weekly Sweep"))',
    'triggerFields' => array('error_category_c','date_error_occurred_c','date_error_documented_c','date_time_discovered_c'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'time_to_discovery_days_c',
                'value' => 'ifElse(not(equal($date_error_documented_c,"")),abs(subtract(daysUntil($date_error_occurred_c),daysUntil($date_error_documented_c))),abs(subtract(daysUntil($date_error_occurred_c),daysUntil($date_time_discovered_c))))',
            ),
        ), 
		 
    ),
);
$dependencies['M06_Error']['setDaysfield_value'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('error_category_c'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'time_to_discovery_days_c',   
				'value' => 'isInList($error_category_c,createList("Critical Phase Inspection", "Daily QC", "Data Book QC", "External Audit", "Internal Feedback", "Process Audit", "Real time study conduct", "Retrospective Data QC", "Test Failure", "Weekly Sweep"))',
            ),         
        ),
		array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'time_to_discovery_days_c',   
				'value' => 'isInList($error_category_c,createList("Critical Phase Inspection", "Daily QC", "Data Book QC", "External Audit", "Internal Feedback", "Process Audit", "Real time study conduct", "Retrospective Data QC", "Test Failure", "Weekly Sweep"))',
            ),         
        ), 
    ),
);

$dependencies['M06_Error']['actualSDACK_value'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('error_category_c','id'),
    'onload'        => true,
    'actions'       => array(
       array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'actual_sd_ack_date_c',   
				'value' => 'and(not(equal($wp_name_c,"")),not(isInList($error_category_c,createList("Feedback","Internal Feedback"))))',
            ),         
        ),
		array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'actual_sd_ack_date_c',   
				'value' => 'not(equal($actual_sd_ack_date_c,""))',
            ),         
        ),		
    ),
);

$dependencies['M06_Error']['required_reinspection_date'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('reinspection_date_c','resolution_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'reinspection_date_c',
                'value' => 'not(equal($resolution_c,""))',
            ),
        ),
    ),
);