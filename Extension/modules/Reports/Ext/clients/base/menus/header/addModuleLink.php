<?php

$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=AllNotificationforStudy',
    'label' =>'LNK_ALLNOTIFICATIONFORSTUDY',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);
$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=AllDeviationsforStudy',
    'label' =>'LNK_ALLDEVIATIONSFORSTUDY',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);
$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=AllAdverseEventsforStudy',
    'label' =>'LNK_ALLADVERSEEVENTSFORSTUDY',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);

$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=MostRecentWPAForTestSystem',
    'label' =>'LNK_MOSTRECENTWPAFORTESTSYSTEM',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);

$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=CustomComReport',
    'label' =>'LNK_CUSTOMCOMREPORT',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);

$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=CustomTSWeight',
    'label' =>'LNK_CUSTOMTSWEIGHT',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);

$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=CustomCapacity',
    'label' =>'LNK_CUSTOMTSCAPACITY',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);
/**#1548 : Custom WPA/Allocation Report : 30 Dec 2021 */
$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=CustomWPAAllocation',
    'label' =>'LNK_CUSTOMTSWPAALLOCATION',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);
/**#1698 : Custom Deviations per Animal By Department report : 07 Jan 2022 */
$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=CustomDeviation',
    'label' =>'LNK_CUSTOMTSDEVIATIONPERANIMAL',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);
/**#1949 : Custom report for Pathology WPDs : 12 Jan 2022 */
$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=CustomPathologyWPD',
    'label' =>'LNK_CUSTOMTSPATHOLOGYWPD',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);
/**#1989 : Custom Report for WPDs Out to Sponsor for 30+ days : 17 Jan 2022 */
$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=CustomOuttoSponsorWPD',
    'label' =>'LNK_CUSTOMOUTTOSPONSORWPD',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);
/**#2107 : Consolidated Daily Scheduling Needs Report : Daily Scheduling Needs - LA */
$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=DailySchedulingNeeds',
    'label' =>'LNK_DAILYSCHEDULINGNEEDS',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);
$viewdefs['Reports']['base']['menu']['header'][] = array(
    'route'=>'#bwc/index.php?module=Reports&action=DailySchedulingNeedsSA',
    'label' =>'LNK_DAILYSCHEDULINGNEEDSSA',
    'acl_module'=>'Reports',
    'icon' => 'fa-bar-chart-o',
);