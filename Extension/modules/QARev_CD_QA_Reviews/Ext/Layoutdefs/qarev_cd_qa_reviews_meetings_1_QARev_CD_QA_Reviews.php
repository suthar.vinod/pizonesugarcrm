<?php
 // created: 2021-02-11 07:49:03
$layout_defs["QARev_CD_QA_Reviews"]["subpanel_setup"]['qarev_cd_qa_reviews_meetings_1'] = array (
  'order' => 100,
  'module' => 'Meetings',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_MEETINGS_TITLE',
  'get_subpanel_data' => 'qarev_cd_qa_reviews_meetings_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
