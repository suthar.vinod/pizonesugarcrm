<?php
// created: 2021-02-11 07:49:03
$dictionary["QARev_CD_QA_Reviews"]["fields"]["qarev_cd_qa_reviews_meetings_1"] = array (
  'name' => 'qarev_cd_qa_reviews_meetings_1',
  'type' => 'link',
  'relationship' => 'qarev_cd_qa_reviews_meetings_1',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_QAREV_CD_QA_REVIEWS_TITLE',
  'id_name' => 'qarev_cd_qa_reviews_meetings_1qarev_cd_qa_reviews_ida',
  'link-type' => 'many',
  'side' => 'left',
);
