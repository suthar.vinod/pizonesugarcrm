<?php
 // created: 2021-02-11 07:50:25
$layout_defs["QARev_CD_QA_Reviews"]["subpanel_setup"]['qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1'] = array (
  'order' => 100,
  'module' => 'QADoc_CD_QA_Rev_Docs',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_QAREV_CD_QA_REVIEWS_QADOC_CD_QA_REV_DOCS_1_FROM_QADOC_CD_QA_REV_DOCS_TITLE',
  'get_subpanel_data' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1',
);
