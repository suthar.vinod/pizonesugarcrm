<?php
 // created: 2017-06-07 19:03:44
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['a1a_critical_phase_inspectio_a1a_cpi_findings_1'] = array (
  'order' => 100,
  'module' => 'A1A_CPI_Findings',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CPI_FINDINGS_TITLE',
  'get_subpanel_data' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
