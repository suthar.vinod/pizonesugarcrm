<?php
 // created: 2017-09-13 15:16:58
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['a1a_critical_phase_inspectio_activities_1_emails'] = array (
  'order' => 100,
  'module' => 'Emails',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_EMAILS_FROM_EMAILS_TITLE',
  'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_emails',
);
