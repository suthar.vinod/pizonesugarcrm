<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_INSPECTION_DATE'] = 'Inspection Date(s)';
$mod_strings['LBL_TEST_SYSTEMS'] = 'Test Systems';
$mod_strings['LBL_ASSIGNED_TO'] = 'QA Auditor';
$mod_strings['LBL_INSPECTION_COMMENTS'] = 'Inspection Comments';
$mod_strings['LBL_INSPECTION_RESULTS'] = 'Inspection Results';
$mod_strings['LBL_INSPECTION_COMMENTS_AREA'] = 'Inspection Comments Area';
$mod_strings['LBL_INSPECTION_SCOPE'] = 'Inspection Scope';
$mod_strings['LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Communications';
$mod_strings['LBL_A1A_CRITICAL_PHASE_INSPECTIO_FOCUS_DRAWER_DASHBOARD'] = 'Critical Phase Inspections Focus Drawer';
$mod_strings['LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE'] = 'Communications';
$mod_strings['LBL_CONTRIBUTING_SCIENTIST'] = 'Contributing Scientist';
