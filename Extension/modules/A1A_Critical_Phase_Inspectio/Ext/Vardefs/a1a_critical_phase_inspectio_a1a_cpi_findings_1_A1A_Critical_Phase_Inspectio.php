<?php
// created: 2017-06-07 19:03:44
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_a1a_cpi_findings_1"] = array (
  'name' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'source' => 'non-db',
  'module' => 'A1A_CPI_Findings',
  'bean_name' => 'A1A_CPI_Findings',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'id_name' => 'a1a_criticbf09spectio_ida',
  'link-type' => 'many',
  'side' => 'left',
);
