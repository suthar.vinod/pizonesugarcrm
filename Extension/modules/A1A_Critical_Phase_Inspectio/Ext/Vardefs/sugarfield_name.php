<?php
 // created: 2019-11-25 15:04:35
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['len']='255';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['required']=false;
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['audited']=true;
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['massupdate']=false;
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['unified_search']=false;
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['calculated']=false;

 ?>