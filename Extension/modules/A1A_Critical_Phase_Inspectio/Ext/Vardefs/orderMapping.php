<?php
// created: 2022-03-01 07:41:40
$extensionOrderMap = array (
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/m03_work_product_a1a_critical_phase_inspectio_1_A1A_Critical_Phase_Inspectio.php' => 
  array (
    'md5' => '53876c9f14732720b48c4b4142b58bb3',
    'mtime' => 1496862141,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_a1a_cpi_findings_1_A1A_Critical_Phase_Inspectio.php' => 
  array (
    'md5' => '35b9037fb5243499f2cffd4800c27f04',
    'mtime' => 1496862306,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_inspection_comments_c.php' => 
  array (
    'md5' => '985791b3af3bfb2f6d4b76def35ffb9c',
    'mtime' => 1496946850,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/nameFieldCustomization.php' => 
  array (
    'md5' => 'c5271dcbf71231897808ed521d190e14',
    'mtime' => 1498722372,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_calls_A1A_Critical_Phase_Inspectio.php' => 
  array (
    'md5' => '817d7888e2eb4b65bef260e349cd5191',
    'mtime' => 1505315724,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_meetings_A1A_Critical_Phase_Inspectio.php' => 
  array (
    'md5' => '35437c6825f76df9980669da20b2e4b8',
    'mtime' => 1505315759,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_notes_A1A_Critical_Phase_Inspectio.php' => 
  array (
    'md5' => '8518293b7a017956d20030f0be6cf646',
    'mtime' => 1505315789,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_tasks_A1A_Critical_Phase_Inspectio.php' => 
  array (
    'md5' => '8d490763a7df9e47378c79fb67303cfd',
    'mtime' => 1505315815,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_emails_A1A_Critical_Phase_Inspectio.php' => 
  array (
    'md5' => 'e72ae5b89dad4b7c7be49f1a5a504a0c',
    'mtime' => 1505315845,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_m06_error_1_A1A_Critical_Phase_Inspectio.php' => 
  array (
    'md5' => 'c1d1bf6958cb239f166255d271011dca',
    'mtime' => 1558336727,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_name.php' => 
  array (
    'md5' => 'e25968c53feee87fbb51262ab02a8f17',
    'mtime' => 1574694275,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_inspection_comments_area_c.php' => 
  array (
    'md5' => '87d0acdf7ccf6390120e291b8626a495',
    'mtime' => 1574694762,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_date_of_inspection.php' => 
  array (
    'md5' => '015d94fdb4019d9909fb3f9f7d5fd8f7',
    'mtime' => 1574694809,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_inspection_date_c.php' => 
  array (
    'md5' => '0e75350bdd451a29be97e3871a9aff38',
    'mtime' => 1574694827,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_inspection_scope_c.php' => 
  array (
    'md5' => '8697e85f7c78a719ef00f34b9cc3c370',
    'mtime' => 1574694926,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_test_systems_c.php' => 
  array (
    'md5' => '7ef0b2b82beaf97c3584ce5695198d1c',
    'mtime' => 1582127020,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_phase_of_inspection.php' => 
  array (
    'md5' => '6a3844f2ff864138b85a0dd954064e86',
    'mtime' => 1591869675,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_contributing_scientist_c.php' => 
  array (
    'md5' => '00226f7d37847a497964bc3c8825bd4f',
    'mtime' => 1636616440,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/edoc_email_documents_a1a_critical_phase_inspectio_1_A1A_Critical_Phase_Inspectio.php' => 
  array (
    'md5' => '159a6fe248e5c491fb87cb17a80b319d',
    'mtime' => 1643689495,
    'is_override' => false,
  ),
  'custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_inspection_results_c.php' => 
  array (
    'md5' => 'a98e7d6b5da5ed6354a8f63cad009416',
    'mtime' => 1646120446,
    'is_override' => false,
  ),
);