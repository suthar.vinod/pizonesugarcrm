<?php
// created: 2017-09-13 15:15:31
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_activities_1_meetings"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_meetings',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_meetings',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE',
);
