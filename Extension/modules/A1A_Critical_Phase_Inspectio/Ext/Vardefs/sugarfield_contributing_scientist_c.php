<?php
 // created: 2021-11-11 07:40:40
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['contributing_scientist_c']['labelValue']='Contributing Scientist';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['contributing_scientist_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['contributing_scientist_c']['enforced']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['contributing_scientist_c']['dependency']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['contributing_scientist_c']['required_formula']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['contributing_scientist_c']['readonly_formula']='';

 ?>