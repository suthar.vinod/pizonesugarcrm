<?php
// created: 2019-02-21 13:44:24
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_m06_error_1"] = array (
  'name' => 'a1a_critical_phase_inspectio_m06_error_1',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'id_name' => 'a1a_critic9e89spectio_ida',
  'link-type' => 'many',
  'side' => 'left',
);
