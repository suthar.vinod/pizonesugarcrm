<?php
 // created: 2021-02-25 08:41:49
$layout_defs["Accounts"]["subpanel_setup"]['accounts_an_account_number_1'] = array (
  'order' => 100,
  'module' => 'AN_Account_Number',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_AN_ACCOUNT_NUMBER_TITLE',
  'get_subpanel_data' => 'accounts_an_account_number_1',
);
