<?php
 // created: 2019-07-03 12:14:01
$layout_defs["Accounts"]["subpanel_setup"]['accounts_ca_company_address_1'] = array (
  'order' => 100,
  'module' => 'CA_Company_Address',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_CA_COMPANY_ADDRESS_TITLE',
  'get_subpanel_data' => 'accounts_ca_company_address_1',
);
