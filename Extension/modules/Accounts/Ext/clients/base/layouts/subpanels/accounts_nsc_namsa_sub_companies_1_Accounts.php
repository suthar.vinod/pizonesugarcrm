<?php
// created: 2021-12-07 12:21:22
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE',
  'context' => 
  array (
    'link' => 'accounts_nsc_namsa_sub_companies_1',
  ),
);