<?php
 // created: 2020-11-03 06:37:20
$layout_defs["Accounts"]["subpanel_setup"]['accounts_equip_equipment_1'] = array (
  'order' => 100,
  'module' => 'Equip_Equipment',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'get_subpanel_data' => 'accounts_equip_equipment_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
