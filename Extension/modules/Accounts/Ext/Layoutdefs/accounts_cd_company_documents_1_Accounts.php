<?php
 // created: 2019-02-06 20:01:35
$layout_defs["Accounts"]["subpanel_setup"]['accounts_cd_company_documents_1'] = array (
  'order' => 100,
  'module' => 'CD_Company_Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_CD_COMPANY_DOCUMENTS_1_FROM_CD_COMPANY_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'accounts_cd_company_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
