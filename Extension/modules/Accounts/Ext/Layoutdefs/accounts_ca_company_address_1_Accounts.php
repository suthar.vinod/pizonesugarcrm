<?php
 // created: 2019-07-03 12:14:00
$layout_defs["Accounts"]["subpanel_setup"]['accounts_ca_company_address_1'] = array (
  'order' => 100,
  'module' => 'CA_Company_Address',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_CA_COMPANY_ADDRESS_TITLE',
  'get_subpanel_data' => 'accounts_ca_company_address_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
