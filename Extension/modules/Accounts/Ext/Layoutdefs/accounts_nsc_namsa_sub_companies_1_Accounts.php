<?php
 // created: 2021-12-07 12:21:22
$layout_defs["Accounts"]["subpanel_setup"]['accounts_nsc_namsa_sub_companies_1'] = array (
  'order' => 100,
  'module' => 'NSC_NAMSA_Sub_Companies',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE',
  'get_subpanel_data' => 'accounts_nsc_namsa_sub_companies_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
