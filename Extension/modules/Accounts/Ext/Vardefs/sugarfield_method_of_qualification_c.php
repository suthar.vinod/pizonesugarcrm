<?php
 // created: 2019-07-09 12:31:13
$dictionary['Account']['fields']['method_of_qualification_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['method_of_qualification_c']['labelValue']='Method of Qualification';
$dictionary['Account']['fields']['method_of_qualification_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['method_of_qualification_c']['calculated']='true';
$dictionary['Account']['fields']['method_of_qualification_c']['formula']='related($accounts_cd_company_documents_1,"method_of_qualification_c")';
$dictionary['Account']['fields']['method_of_qualification_c']['enforced']='true';
$dictionary['Account']['fields']['method_of_qualification_c']['dependency']='equal($vendor_list_c,true)';

 ?>