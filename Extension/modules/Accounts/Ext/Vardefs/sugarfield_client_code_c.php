<?php
 // created: 2019-05-23 15:52:19
$dictionary['Account']['fields']['client_code_c']['labelValue']='Company Code';
$dictionary['Account']['fields']['client_code_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['client_code_c']['enforced']='';
$dictionary['Account']['fields']['client_code_c']['dependency']='';

 ?>