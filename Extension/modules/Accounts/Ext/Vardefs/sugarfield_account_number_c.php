<?php
 // created: 2020-07-31 14:44:58
$dictionary['Account']['fields']['account_number_c']['labelValue']='Account #';
$dictionary['Account']['fields']['account_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['account_number_c']['enforced']='';
$dictionary['Account']['fields']['account_number_c']['dependency']='isInList($category_c,createList("Sponsor and Vendor","Vendor","Manufacturer","Sponsor and Manufacturer","Vendor and Manufacturer","SponsorVendor Manufacturer"))';

 ?>