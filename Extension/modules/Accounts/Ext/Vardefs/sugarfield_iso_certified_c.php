<?php
 // created: 2019-05-21 13:07:25
$dictionary['Account']['fields']['iso_certified_c']['labelValue']='ISO Certified';
$dictionary['Account']['fields']['iso_certified_c']['formula']='equal($vendor_list_c,true)';
$dictionary['Account']['fields']['iso_certified_c']['enforced']='false';
$dictionary['Account']['fields']['iso_certified_c']['dependency']='equal($vendor_list_c,true)';

 ?>