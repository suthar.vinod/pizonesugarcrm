<?php
// created: 2018-02-14 15:35:52
$dictionary["Account"]["fields"]["accounts_ac01_acquired_companies_1"] = array (
  'name' => 'accounts_ac01_acquired_companies_1',
  'type' => 'link',
  'relationship' => 'accounts_ac01_acquired_companies_1',
  'source' => 'non-db',
  'module' => 'AC01_Acquired_Companies',
  'bean_name' => 'AC01_Acquired_Companies',
  'vname' => 'LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_ac01_acquired_companies_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
