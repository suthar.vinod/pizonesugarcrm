<?php
 // created: 2020-07-31 14:42:41
$dictionary['Account']['fields']['vendor_list_c']['labelValue']='Approved Vendor List';
$dictionary['Account']['fields']['vendor_list_c']['enforced']='';
$dictionary['Account']['fields']['vendor_list_c']['dependency']='isInList($category_c,createList("Sponsor and Vendor","Vendor","Manufacturer","Sponsor and Manufacturer","Vendor and Manufacturer","SponsorVendor Manufacturer"))';

 ?>