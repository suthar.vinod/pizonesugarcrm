<?php
 // created: 2019-12-09 05:35:35
$dictionary['Account']['fields']['next_approval_due_date_c']['labelValue']='Next Approval Due Date';
$dictionary['Account']['fields']['next_approval_due_date_c']['formula']='addDays($approval_date_c,$approval_interval_days_c)';
$dictionary['Account']['fields']['next_approval_due_date_c']['enforced']='false';
$dictionary['Account']['fields']['next_approval_due_date_c']['dependency']='and(equal($vendor_list_c,true),greaterThan(strlen(toString($next_approval_due_date_c)),0))';

 ?>