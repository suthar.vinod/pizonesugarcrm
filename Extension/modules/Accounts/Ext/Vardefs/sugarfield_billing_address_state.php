<?php
 // created: 2020-07-31 15:08:35
$dictionary['Account']['fields']['billing_address_state']['audited']=true;
$dictionary['Account']['fields']['billing_address_state']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_state']['comments']='The state used for billing address';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_state']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_state']['calculated']=false;
$dictionary['Account']['fields']['billing_address_state']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor","Sponsor and Manufacturer","SponsorVendor Manufacturer"))';
$dictionary['Account']['fields']['billing_address_state']['hidemassupdate']=false;

 ?>