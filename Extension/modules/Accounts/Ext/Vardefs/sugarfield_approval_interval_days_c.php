<?php
 // created: 2019-05-21 13:13:41
$dictionary['Account']['fields']['approval_interval_days_c']['labelValue']='Approval Interval (days)';
$dictionary['Account']['fields']['approval_interval_days_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['approval_interval_days_c']['enforced']='';
$dictionary['Account']['fields']['approval_interval_days_c']['dependency']='equal($vendor_list_c,true)';

 ?>