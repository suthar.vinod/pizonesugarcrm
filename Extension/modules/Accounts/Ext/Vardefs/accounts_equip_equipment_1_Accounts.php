<?php
// created: 2020-11-03 06:37:20
$dictionary["Account"]["fields"]["accounts_equip_equipment_1"] = array (
  'name' => 'accounts_equip_equipment_1',
  'type' => 'link',
  'relationship' => 'accounts_equip_equipment_1',
  'source' => 'non-db',
  'module' => 'Equip_Equipment',
  'bean_name' => 'Equip_Equipment',
  'vname' => 'LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'id_name' => 'accounts_equip_equipment_1equip_equipment_idb',
);
