<?php
 // created: 2019-02-20 16:59:23
$dictionary['Account']['fields']['tradeshow_name_c']['labelValue']='Trade Show Name';
$dictionary['Account']['fields']['tradeshow_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['tradeshow_name_c']['enforced']='false';
$dictionary['Account']['fields']['tradeshow_name_c']['dependency']='equal($connection_source_c,"Trade Show")';

 ?>