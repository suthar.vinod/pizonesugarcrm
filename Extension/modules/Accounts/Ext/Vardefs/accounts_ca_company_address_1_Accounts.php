<?php
// created: 2019-07-03 12:14:01
$dictionary["Account"]["fields"]["accounts_ca_company_address_1"] = array (
  'name' => 'accounts_ca_company_address_1',
  'type' => 'link',
  'relationship' => 'accounts_ca_company_address_1',
  'source' => 'non-db',
  'module' => 'CA_Company_Address',
  'bean_name' => 'CA_Company_Address',
  'vname' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_ca_company_address_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
