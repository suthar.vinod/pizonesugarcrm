<?php
 // created: 2019-02-20 16:58:40
$dictionary['Account']['fields']['referred_by_c']['labelValue']='Referred By';
$dictionary['Account']['fields']['referred_by_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['referred_by_c']['enforced']='';
$dictionary['Account']['fields']['referred_by_c']['dependency']='equal($connection_source_c,"Referral")';

 ?>