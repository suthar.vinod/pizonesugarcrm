<?php
 // created: 2020-07-31 15:13:04
$dictionary['Account']['fields']['account_representative_c']['labelValue']='Account Representative';
$dictionary['Account']['fields']['account_representative_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['account_representative_c']['enforced']='';
$dictionary['Account']['fields']['account_representative_c']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor","Sponsor and Manufacturer","SponsorVendor Manufacturer"))';

 ?>