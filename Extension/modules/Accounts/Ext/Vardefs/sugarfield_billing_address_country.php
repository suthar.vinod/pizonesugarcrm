<?php
 // created: 2020-07-31 15:07:47
$dictionary['Account']['fields']['billing_address_country']['len']='255';
$dictionary['Account']['fields']['billing_address_country']['audited']=true;
$dictionary['Account']['fields']['billing_address_country']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_country']['comments']='The country used for the billing address';
$dictionary['Account']['fields']['billing_address_country']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_country']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_country']['calculated']=false;
$dictionary['Account']['fields']['billing_address_country']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor","Sponsor and Manufacturer","SponsorVendor Manufacturer"))';
$dictionary['Account']['fields']['billing_address_country']['hidemassupdate']=false;

 ?>