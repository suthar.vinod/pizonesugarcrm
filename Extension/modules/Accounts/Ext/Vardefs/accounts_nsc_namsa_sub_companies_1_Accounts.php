<?php
// created: 2021-12-07 12:21:22
$dictionary["Account"]["fields"]["accounts_nsc_namsa_sub_companies_1"] = array (
  'name' => 'accounts_nsc_namsa_sub_companies_1',
  'type' => 'link',
  'relationship' => 'accounts_nsc_namsa_sub_companies_1',
  'source' => 'non-db',
  'module' => 'NSC_NAMSA_Sub_Companies',
  'bean_name' => 'NSC_NAMSA_Sub_Companies',
  'vname' => 'LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
