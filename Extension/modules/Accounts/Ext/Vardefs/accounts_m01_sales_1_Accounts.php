<?php
// created: 2016-02-15 18:01:52
$dictionary["Account"]["fields"]["accounts_m01_sales_1"] = array (
  'name' => 'accounts_m01_sales_1',
  'type' => 'link',
  'relationship' => 'accounts_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_m01_sales_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
