<?php
// created: 2021-02-25 08:41:49
$dictionary["Account"]["fields"]["accounts_an_account_number_1"] = array (
  'name' => 'accounts_an_account_number_1',
  'type' => 'link',
  'relationship' => 'accounts_an_account_number_1',
  'source' => 'non-db',
  'module' => 'AN_Account_Number',
  'bean_name' => 'AN_Account_Number',
  'vname' => 'LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_an_account_number_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
