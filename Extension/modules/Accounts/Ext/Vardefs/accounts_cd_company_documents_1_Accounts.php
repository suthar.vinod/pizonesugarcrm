<?php
// created: 2019-02-06 20:01:35
$dictionary["Account"]["fields"]["accounts_cd_company_documents_1"] = array (
  'name' => 'accounts_cd_company_documents_1',
  'type' => 'link',
  'relationship' => 'accounts_cd_company_documents_1',
  'source' => 'non-db',
  'module' => 'CD_Company_Documents',
  'bean_name' => 'CD_Company_Documents',
  'vname' => 'LBL_ACCOUNTS_CD_COMPANY_DOCUMENTS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_cd_company_documents_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
