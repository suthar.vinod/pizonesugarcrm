<?php
 // created: 2019-07-09 12:29:47
$dictionary['Account']['fields']['current_vendor_status_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['current_vendor_status_c']['labelValue']='Vendor Status';
$dictionary['Account']['fields']['current_vendor_status_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['current_vendor_status_c']['calculated']='true';
$dictionary['Account']['fields']['current_vendor_status_c']['formula']='related($accounts_cd_company_documents_1,"vendor_status_c")';
$dictionary['Account']['fields']['current_vendor_status_c']['enforced']='true';
$dictionary['Account']['fields']['current_vendor_status_c']['dependency']='equal($vendor_list_c,true)';

 ?>