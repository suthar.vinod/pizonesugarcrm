<?php
 // created: 2021-08-04 20:04:20
$dictionary['RI_Received_Items']['fields']['name']['importable']='false';
$dictionary['RI_Received_Items']['fields']['name']['duplicate_merge']='disabled';
$dictionary['RI_Received_Items']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['RI_Received_Items']['fields']['name']['merge_filter']='disabled';
$dictionary['RI_Received_Items']['fields']['name']['unified_search']=false;
$dictionary['RI_Received_Items']['fields']['name']['calculated']='true';
$dictionary['RI_Received_Items']['fields']['name']['formula']='concat("RI ",related($poi_purchase_order_item_ri_received_items_1,"name"),related($ori_order_request_item_ri_received_items_1,"name")," ",toString($received_date))';
$dictionary['RI_Received_Items']['fields']['name']['enforced']=true;

 ?>