<?php
 // created: 2022-02-22 08:19:05
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['required']=true;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['audited']=true;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['massupdate']=true;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['hidemassupdate']=false;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['duplicate_merge']='enabled';
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['duplicate_merge_dom_value']='1';
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['merge_filter']='disabled';
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['reportable']=false;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['unified_search']=false;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['calculated']=false;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['dependency']='equal($type_2,"POI")';
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['required_formula']='equal($type_2,"POI")';
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['vname']='LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_NAME_FIELD_TITLE';

 ?>