<?php
 // created: 2022-02-22 08:18:31
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['required']=true;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['audited']=true;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['massupdate']=true;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['hidemassupdate']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['duplicate_merge']='enabled';
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['duplicate_merge_dom_value']='1';
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['merge_filter']='disabled';
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['reportable']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['unified_search']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['calculated']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['dependency']='equal($type_2,"ORI")';
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['required_formula']='equal($type_2,"ORI")';
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['vname']='LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_NAME_FIELD_TITLE';

 ?>