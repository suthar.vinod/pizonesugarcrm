<?php
// created: 2021-08-04 19:53:25
$dictionary["RI_Received_Items"]["fields"]["ori_order_request_item_ri_received_items_1"] = array (
  'name' => 'ori_order_request_item_ri_received_items_1',
  'type' => 'link',
  'relationship' => 'ori_order_request_item_ri_received_items_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'side' => 'right',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'id_name' => 'ori_order_6b32st_item_ida',
  'link-type' => 'one',
);
$dictionary["RI_Received_Items"]["fields"]["ori_order_request_item_ri_received_items_1_name"] = array (
  'name' => 'ori_order_request_item_ri_received_items_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'save' => true,
  'id_name' => 'ori_order_6b32st_item_ida',
  'link' => 'ori_order_request_item_ri_received_items_1',
  'table' => 'ori_order_request_item',
  'module' => 'ORI_Order_Request_Item',
  'rname' => 'name',
);
$dictionary["RI_Received_Items"]["fields"]["ori_order_6b32st_item_ida"] = array (
  'name' => 'ori_order_6b32st_item_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE_ID',
  'id_name' => 'ori_order_6b32st_item_ida',
  'link' => 'ori_order_request_item_ri_received_items_1',
  'table' => 'ori_order_request_item',
  'module' => 'ORI_Order_Request_Item',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
