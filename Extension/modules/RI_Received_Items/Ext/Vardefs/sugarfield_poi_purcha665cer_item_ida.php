<?php
 // created: 2022-02-22 08:19:05
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['name']='poi_purcha665cer_item_ida';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['type']='id';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['source']='non-db';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['vname']='LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE_ID';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['id_name']='poi_purcha665cer_item_ida';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['link']='poi_purchase_order_item_ri_received_items_1';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['table']='poi_purchase_order_item';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['module']='POI_Purchase_Order_Item';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['rname']='id';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['reportable']=false;
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['side']='right';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['massupdate']=false;
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['duplicate_merge']='disabled';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['hideacl']=true;
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['audited']=false;
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['importable']='true';

 ?>