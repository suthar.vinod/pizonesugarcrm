<?php
 // created: 2022-02-22 08:18:31
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['name']='ori_order_6b32st_item_ida';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['type']='id';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['source']='non-db';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['vname']='LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE_ID';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['id_name']='ori_order_6b32st_item_ida';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['link']='ori_order_request_item_ri_received_items_1';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['table']='ori_order_request_item';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['module']='ORI_Order_Request_Item';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['rname']='id';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['reportable']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['side']='right';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['massupdate']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['duplicate_merge']='disabled';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['hideacl']=true;
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['audited']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['importable']='true';

 ?>