<?php
 // created: 2022-02-22 08:15:05
$dictionary['RI_Received_Items']['fields']['product_name_c']['duplicate_merge_dom_value']=0;
$dictionary['RI_Received_Items']['fields']['product_name_c']['labelValue']='Product Name';
$dictionary['RI_Received_Items']['fields']['product_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RI_Received_Items']['fields']['product_name_c']['calculated']='true';
$dictionary['RI_Received_Items']['fields']['product_name_c']['formula']='concat(related($poi_purchase_order_item_ri_received_items_1,"products_name_c"),related($ori_order_request_item_ri_received_items_1,"products_name_c"))';
$dictionary['RI_Received_Items']['fields']['product_name_c']['enforced']='true';
$dictionary['RI_Received_Items']['fields']['product_name_c']['dependency']='';
$dictionary['RI_Received_Items']['fields']['product_name_c']['required_formula']='';
$dictionary['RI_Received_Items']['fields']['product_name_c']['readonly_formula']='';

 ?>