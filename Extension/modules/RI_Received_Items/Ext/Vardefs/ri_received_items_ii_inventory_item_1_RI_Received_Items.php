<?php
// created: 2021-11-09 09:52:04
$dictionary["RI_Received_Items"]["fields"]["ri_received_items_ii_inventory_item_1"] = array (
  'name' => 'ri_received_items_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'ri_received_items_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'id_name' => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
  'link-type' => 'many',
  'side' => 'left',
);
