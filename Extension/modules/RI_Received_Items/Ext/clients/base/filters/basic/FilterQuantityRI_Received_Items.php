<?php
// created: 2021-08-27 
$viewdefs['RI_Received_Items']['base']['filter']['basic']['filters'][] = array(
  'id' => 'FilterQuantityRI_Received_Items',
  'name' => 'Quantity Received Greaterthen Linked Inventory Item',
  'filter_definition' => array(
    array(
      'linked_ii_sum_flag_c'  => array(
        '$equals' => '1',
      ),
    ),
  ),
  'editable' => true,
  'is_template' => true,
);
