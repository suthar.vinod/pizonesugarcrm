<?php
 // created: 2021-04-12 09:34:31
$dictionary['IM_Inventory_Management']['fields']['location_equi_serial_no_hide_c']['labelValue']='Location Equi Sr No Hidden';
$dictionary['IM_Inventory_Management']['fields']['location_equi_serial_no_hide_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['IM_Inventory_Management']['fields']['location_equi_serial_no_hide_c']['enforced']='';
$dictionary['IM_Inventory_Management']['fields']['location_equi_serial_no_hide_c']['dependency']='';
$dictionary['IM_Inventory_Management']['fields']['location_equi_serial_no_hide_c']['required_formula']='';
$dictionary['IM_Inventory_Management']['fields']['location_equi_serial_no_hide_c']['readonly_formula']='';

 ?>