<?php
 // created: 2022-04-05 09:01:45
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['name']='account_id_c';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['vname']='LBL_SHIP_TO_COMPANY_ACCOUNT_ID';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['type']='id';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['massupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['no_default']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['comments']='';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['help']='';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['importable']='true';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['duplicate_merge']='enabled';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['duplicate_merge_dom_value']=1;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['audited']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['reportable']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['unified_search']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['merge_filter']='disabled';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['pii']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['calculated']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['len']=36;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['size']='20';

 ?>