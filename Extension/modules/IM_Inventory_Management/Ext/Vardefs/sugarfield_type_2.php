<?php
 // created: 2022-04-05 09:04:51
$dictionary['IM_Inventory_Management']['fields']['type_2']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['type_2']['dependency']=false;
$dictionary['IM_Inventory_Management']['fields']['type_2']['options']='universal_inventory_management_type_list';
$dictionary['IM_Inventory_Management']['fields']['type_2']['visibility_grid']=array (
  'trigger' => 'category',
  'values' => 
  array (
    'Product' => 
    array (
      0 => '',
      1 => 'Autoclave supplies',
      2 => 'Balloon Catheter',
      3 => 'Bandaging',
      4 => 'Blood draw supply',
      5 => 'Cannula',
      6 => 'Catheter',
      7 => 'Chemical',
      8 => 'Cleaning Agent',
      9 => 'Cleaning Supplies',
      10 => 'NA Heparin Plasma',
      11 => 'Contrast',
      12 => 'Drug',
      13 => 'Endotracheal TubeSupplies',
      14 => 'Enrichment Toy',
      15 => 'Equipment',
      16 => 'ETO supplies',
      17 => 'Feeding supplies',
      18 => 'Graft',
      19 => 'Inflation Device',
      20 => 'Office Supply',
      21 => 'Other',
      22 => 'Reagent',
      23 => 'Sheath',
      24 => 'Shipping supplies',
      25 => 'Solution',
      26 => 'Stent',
      27 => 'Surgical Instrument',
      28 => 'Surgical site prep',
      29 => 'Suture',
      30 => 'Tubing',
      31 => 'Wire',
    ),
    'Record' => 
    array (
      0 => '',
      1 => 'Data Book',
      2 => 'Data Sheet',
      3 => 'Equipment Facility Record',
      4 => 'Hard Drive',
      5 => 'Protocol Book',
    ),
    'Specimen' => 
    array (
    ),
    'Study Article' => 
    array (
      0 => '',
      1 => 'Accessory Article',
      2 => 'Control Article',
      3 => 'Test Article',
    ),
    '' => 
    array (
    ),
  ),
);
$dictionary['IM_Inventory_Management']['fields']['type_2']['required']=false;

 ?>