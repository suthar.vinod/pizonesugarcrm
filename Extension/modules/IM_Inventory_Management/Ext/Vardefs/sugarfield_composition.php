<?php
 // created: 2021-02-10 10:41:26
$dictionary['IM_Inventory_Management']['fields']['composition']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['composition']['visibility_grid']=array (
  'trigger' => 'related_to',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Internal Use' => 
    array (
      0 => 'Inventory Item',
    ),
    'Sales' => 
    array (
      0 => 'Inventory Item',
    ),
    'Work Product' => 
    array (
      0 => '',
      1 => 'Inventory Item',
      2 => 'Inventory Collection',
    ),
  ),
);

 ?>