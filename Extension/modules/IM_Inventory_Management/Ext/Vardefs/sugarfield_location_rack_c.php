<?php
 // created: 2022-04-05 09:17:08
$dictionary['IM_Inventory_Management']['fields']['location_rack_c']['labelValue']='Location (Freezer Rack #)';
$dictionary['IM_Inventory_Management']['fields']['location_rack_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['IM_Inventory_Management']['fields']['location_rack_c']['enforced']='';
$dictionary['IM_Inventory_Management']['fields']['location_rack_c']['dependency']='equal($location_equi_serial_no_hide_c,"51014222")';
$dictionary['IM_Inventory_Management']['fields']['location_rack_c']['readonly_formula']='';

 ?>