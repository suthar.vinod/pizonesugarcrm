<?php
 // created: 2022-04-05 09:18:48
$dictionary['IM_Inventory_Management']['fields']['type_specimen_c']['labelValue']='Type (Specimen)';
$dictionary['IM_Inventory_Management']['fields']['type_specimen_c']['dependency']='equal($category,"Specimen")';
$dictionary['IM_Inventory_Management']['fields']['type_specimen_c']['readonly_formula']='';
$dictionary['IM_Inventory_Management']['fields']['type_specimen_c']['visibility_grid']='';

 ?>