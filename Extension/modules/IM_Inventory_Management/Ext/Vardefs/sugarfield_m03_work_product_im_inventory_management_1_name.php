<?php
 // created: 2022-04-05 09:14:20
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['audited']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['massupdate']=true;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['duplicate_merge']='enabled';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['duplicate_merge_dom_value']='1';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['merge_filter']='disabled';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['reportable']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['unified_search']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['calculated']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['dependency']='or(equal($related_to,"Work Product"),equal($related_to,"Inventory Collection"))';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['vname']='LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_NAME_FIELD_TITLE';

 ?>