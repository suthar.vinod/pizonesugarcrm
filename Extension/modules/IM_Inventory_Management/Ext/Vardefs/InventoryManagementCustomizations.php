<?php

$dictionary['IM_Inventory_Management']['fields']['name']['readonly'] = true;
$dictionary['IM_Inventory_Management']['fields']['name']['required'] = false;

$dictionary['IM_Inventory_Management']['fields']['duration_hours']['readonly'] = true;

$dictionary['IM_Inventory_Management']['fields']['im_inactive_c'] = array(
    'labelValue' => 'LBL_IM_INACTIVE',
    'source'     => 'custom_fields',
    'name'       => 'im_inactive_c',
    'vname'      => 'LBL_IM_INACTIVE',
    'type'       => 'bool',
    'importable' => 'true',
    'audited'    => false,
    'reportable' => true,
    'default'    => false,
    'size'       => '20',
);

$dictionary['IM_Inventory_Management']['fields']['inventory_management_num_c'] = array(
    'labelValue'                => 'LBL_INVENTORY_MANAGEMENT_NUM',
    'required'                  => false,
    'source'                    => 'custom_fields',
    'name'                      => 'inventory_management_num_c',
    'vname'                     => 'LBL_INVENTORY_MANAGEMENT_NUM',
    'type'                      => 'int',
    'massupdate'                => false,
    'importable'                => 'true',
    'duplicate_merge'           => 'enabled',
    'duplicate_merge_dom_value' => 1,
    'reportable'                => true,
    'len'                       => 11,
    'size'                      => '20',
    'min'                       => false,
    'max'                       => false,
);
