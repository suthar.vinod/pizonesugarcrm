<?php
 // created: 2022-04-05 08:55:07
$dictionary['IM_Inventory_Management']['fields']['location_building']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['location_building']['visibility_grid']=array (
  'trigger' => 'action',
  'values' => 
  array (
    'Initial Storage' => 
    array (
      0 => '',
      1 => '8945',
      2 => '8960',
      3 => '9055',
    ),
    'In Use' => 
    array (
      0 => '',
      1 => '780',
      2 => '8945',
      3 => '8960',
      4 => '9055',
    ),
    'Processing' => 
    array (
    ),
    'Internal Transfer' => 
    array (
      0 => '',
      1 => '780',
      2 => '8945',
      3 => '8960',
      4 => '9055',
    ),
    'External Transfer' => 
    array (
    ),
    'Archive Offsite' => 
    array (
    ),
    'Archive Onsite' => 
    array (
      0 => '',
      1 => '780',
    ),
    'Discard' => 
    array (
    ),
    'Obsolete' => 
    array (
    ),
    '' => 
    array (
    ),
    'Separate Inventory Items' => 
    array (
      0 => '',
      1 => '780',
      2 => '8945',
      3 => '8960',
      4 => '9055',
    ),
    'Create Inventory Collection' => 
    array (
    ),
  ),
);
$dictionary['IM_Inventory_Management']['fields']['location_building']['required']=false;

 ?>