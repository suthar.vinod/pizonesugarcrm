<?php
// created: 2020-07-31 13:42:07
$dictionary["IM_Inventory_Management"]["fields"]["ic_inventory_collection_im_inventory_management_1"] = array (
  'name' => 'ic_inventory_collection_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'ic_inventory_collection_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'IC_Inventory_Collection',
  'bean_name' => 'IC_Inventory_Collection',
  'side' => 'right',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'id_name' => 'ic_inventoe24election_ida',
  'link-type' => 'one',
);
$dictionary["IM_Inventory_Management"]["fields"]["ic_inventory_collection_im_inventory_management_1_name"] = array (
  'name' => 'ic_inventory_collection_im_inventory_management_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'save' => true,
  'id_name' => 'ic_inventoe24election_ida',
  'link' => 'ic_inventory_collection_im_inventory_management_1',
  'table' => 'ic_inventory_collection',
  'module' => 'IC_Inventory_Collection',
  'rname' => 'name',
);
$dictionary["IM_Inventory_Management"]["fields"]["ic_inventoe24election_ida"] = array (
  'name' => 'ic_inventoe24election_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE_ID',
  'id_name' => 'ic_inventoe24election_ida',
  'link' => 'ic_inventory_collection_im_inventory_management_1',
  'table' => 'ic_inventory_collection',
  'module' => 'IC_Inventory_Collection',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
