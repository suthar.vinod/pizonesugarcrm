<?php
 // created: 2022-04-05 09:04:22
$dictionary['IM_Inventory_Management']['fields']['category']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['category']['visibility_grid']=array (
  'trigger' => 'related_to',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Internal Use' => 
    array (
      0 => '',
      1 => 'Product',
      2 => 'Record',
      3 => 'Specimen',
      4 => 'Study Article',
    ),
    'Sales' => 
    array (
      0 => '',
      1 => 'Product',
      2 => 'Record',
      3 => 'Specimen',
      4 => 'Study Article',
    ),
    'Work Product' => 
    array (
      0 => '',
      1 => 'Product',
      2 => 'Record',
      3 => 'Specimen',
      4 => 'Study Article',
    ),
    'Inventory Collection' => 
    array (
      0 => 'Specimen',
    ),
  ),
);
$dictionary['IM_Inventory_Management']['fields']['category']['required']=false;

 ?>