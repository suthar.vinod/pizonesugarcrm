<?php
// created: 2020-07-31 13:33:19
$dictionary["IM_Inventory_Management"]["fields"]["m01_sales_im_inventory_management_1"] = array (
  'name' => 'm01_sales_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'm01_sales_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'id_name' => 'm01_sales_im_inventory_management_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["IM_Inventory_Management"]["fields"]["m01_sales_im_inventory_management_1_name"] = array (
  'name' => 'm01_sales_im_inventory_management_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_im_inventory_management_1m01_sales_ida',
  'link' => 'm01_sales_im_inventory_management_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["IM_Inventory_Management"]["fields"]["m01_sales_im_inventory_management_1m01_sales_ida"] = array (
  'name' => 'm01_sales_im_inventory_management_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE_ID',
  'id_name' => 'm01_sales_im_inventory_management_1m01_sales_ida',
  'link' => 'm01_sales_im_inventory_management_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
