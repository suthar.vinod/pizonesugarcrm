<?php
 // created: 2022-04-05 09:14:18
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['name']='m03_work_product_im_inventory_management_1m03_work_product_ida';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['type']='id';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['source']='non-db';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['vname']='LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE_ID';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['id_name']='m03_work_product_im_inventory_management_1m03_work_product_ida';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['link']='m03_work_product_im_inventory_management_1';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['table']='m03_work_product';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['module']='M03_Work_Product';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['rname']='id';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['reportable']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['side']='right';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['massupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['duplicate_merge']='disabled';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['hideacl']=true;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['audited']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['importable']='true';

 ?>