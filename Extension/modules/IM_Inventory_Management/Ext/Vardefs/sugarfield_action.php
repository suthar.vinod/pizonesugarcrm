<?php
 // created: 2022-05-19 07:22:22
$dictionary['IM_Inventory_Management']['fields']['action']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['action']['visibility_grid']=array (
  'trigger' => 'composition',
  'values' => 
  array (
    'Inventory Collection' => 
    array (
      0 => '',
      1 => 'Archive Offsite',
      2 => 'Archive Onsite',
      3 => 'Create Inventory Collection',
      4 => 'Discard',
      5 => 'Exhausted',
      6 => 'External Transfer',
      7 => 'Internal Transfer Analysis',
      8 => 'Internal Transfer',
      9 => 'Missing',
      10 => 'Separate Inventory Items',
    ),
    'Inventory Item' => 
    array (
      0 => '',
      1 => 'Archive Offsite',
      2 => 'Archive Onsite',
      3 => 'Discard',
      4 => 'Exhausted',
      5 => 'External Transfer',
      6 => 'Internal Transfer Analysis',
      7 => 'Internal Transfer',
      8 => 'Missing',
      9 => 'Obsolete',
    ),
    '' => 
    array (
    ),
  ),
);

 ?>