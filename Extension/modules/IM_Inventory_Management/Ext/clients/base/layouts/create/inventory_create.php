<?php

$module = 'IM_Inventory_Management';
$components = $viewdefs[$module]['base']['layout']['create']['components'] ?? [];
$components = \Sugarcrm\Sugarcrm\custom\src\wsystems\InventoryManagementCustomizations\IMUtils::injectAfterSidebarMainpane(
                $components, [
            'layout' => [
                'type' => 'base',
                'name' => 'dashboard-pane',
                'css_class' => 'dashboard-pane',
                'components' => [
                    [
                        'layout' => 'ii-internal-selection',
                    ],
                    [
                        'layout' => 'ii-wp-selection',
                    ],
                    [
                        'layout' => 'ii-sales-selection',
                    ],
                    [
                        'layout' => 'ii-specimen-selection',
                    ],
                ],
            ],
                ], 'preview-pane'
);
$viewdefs[$module]['base']['layout']['create']['components'] = $components;
