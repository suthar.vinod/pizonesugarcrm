<?php

$hook_array['before_save'][] = array(
    77,
    'Inventory Management Record Customizations',
    null,
    'Sugarcrm\\Sugarcrm\\custom\\src\\wsystems\\InventoryManagementCustomizations\\LogicHooks\\InventoryManagementCustomizationsHook',
    'beforeSaveMethod',
);

$hook_array['after_relationship_add'][] = array(
    78,
    'Inventory Management Record Customizations',
    null,
    'Sugarcrm\\Sugarcrm\\custom\\src\\wsystems\\InventoryManagementCustomizations\\LogicHooks\\InventoryManagementCustomizationsHook',
    'afterRelationshipAddMethod',
);

$hook_array['before_filter'][] = array(
    78,
    'Before Filter Inventory Management Records',
    null,
    'Sugarcrm\\Sugarcrm\\custom\\src\\wsystems\\InventoryManagementCustomizations\\LogicHooks\\InventoryManagementCustomizationsHook',
    'beforeFilterMethod',
);
