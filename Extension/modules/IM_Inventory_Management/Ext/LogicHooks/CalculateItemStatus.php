<?php

$hook_array['after_save'][] = array(
    //Processing index. For sorting the array.
    1,
    //Label. A string value to identify the hook.
    'After Save Change Status',

    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateMgtStatus.php',

    //The class the method is in.
    'CalculateMgtStatus',

    //The method to call.
    'calculateAfterSave',
);