<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_II_IDS_HIDDEN_C'] = 'II Ids Hidden';
$mod_strings['LBL_LOCATION_EQUI_SERIAL_NO_HIDE'] = 'Location Equi Sr No Hidden';
$mod_strings['LBL_LOCATION_BIN'] = 'Location (Bin #)';
$mod_strings['LBL_REVIEWED_RECORD'] = 'Reviewed Record';
$mod_strings['LBL_LOCATION_CUBBY'] = 'Location (Slot #)';
$mod_strings['LBL_PROCESSED_PER_PROTOCOL'] = 'Processed Per Protocol';
$mod_strings['LBL_LOCATION_RACK'] = 'Location (Freezer Rack #)';
$mod_strings['LBL_END_CONDITION_ACCEPTABLE'] = 'Physical Condition Acceptable';
$mod_strings['LBL_LOCATION_SHELF'] = 'Location (Room Shelf)';
$mod_strings['LBL_TYPE_2'] = 'Type (non-Specimen)';
$mod_strings['LBL_TYPE_SPECIMEN'] = 'Type (Specimen)';
$mod_strings['LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_NAME_FIELD_TITLE'] = 'Work Products';
