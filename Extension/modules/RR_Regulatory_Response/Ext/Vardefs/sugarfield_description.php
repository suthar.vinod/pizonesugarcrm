<?php
 // created: 2019-07-09 11:58:19
$dictionary['RR_Regulatory_Response']['fields']['description']['required']=true;
$dictionary['RR_Regulatory_Response']['fields']['description']['audited']=true;
$dictionary['RR_Regulatory_Response']['fields']['description']['massupdate']=false;
$dictionary['RR_Regulatory_Response']['fields']['description']['comments']='Full text of the note';
$dictionary['RR_Regulatory_Response']['fields']['description']['duplicate_merge']='enabled';
$dictionary['RR_Regulatory_Response']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['RR_Regulatory_Response']['fields']['description']['merge_filter']='disabled';
$dictionary['RR_Regulatory_Response']['fields']['description']['unified_search']=false;
$dictionary['RR_Regulatory_Response']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['RR_Regulatory_Response']['fields']['description']['calculated']=false;
$dictionary['RR_Regulatory_Response']['fields']['description']['rows']='6';
$dictionary['RR_Regulatory_Response']['fields']['description']['cols']='80';

 ?>