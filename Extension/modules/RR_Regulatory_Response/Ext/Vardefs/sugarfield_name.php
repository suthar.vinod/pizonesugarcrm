<?php
 // created: 2021-06-22 09:44:58
$dictionary['RR_Regulatory_Response']['fields']['name']['len']='255';
$dictionary['RR_Regulatory_Response']['fields']['name']['required']=false;
$dictionary['RR_Regulatory_Response']['fields']['name']['audited']=true;
$dictionary['RR_Regulatory_Response']['fields']['name']['massupdate']=false;
$dictionary['RR_Regulatory_Response']['fields']['name']['unified_search']=false;
$dictionary['RR_Regulatory_Response']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['RR_Regulatory_Response']['fields']['name']['calculated']=false;
$dictionary['RR_Regulatory_Response']['fields']['name']['hidemassupdate']=false;
$dictionary['RR_Regulatory_Response']['fields']['name']['readonly']=true;

 ?>