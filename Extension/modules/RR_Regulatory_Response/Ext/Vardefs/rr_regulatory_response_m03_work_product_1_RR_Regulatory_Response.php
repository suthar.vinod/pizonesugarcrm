<?php
// created: 2019-07-09 11:53:38
$dictionary["RR_Regulatory_Response"]["fields"]["rr_regulatory_response_m03_work_product_1"] = array (
  'name' => 'rr_regulatory_response_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'rr_regulatory_response_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
);
