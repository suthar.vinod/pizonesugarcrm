<?php
 // created: 2019-07-09 11:50:55
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['audited']=true;
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['comments']='Date record created';
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['unified_search']=false;
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['calculated']=false;
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['enable_range_search']='1';

 ?>