<?php
// created: 2019-07-09 12:11:34
$dictionary["RR_Regulatory_Response"]["fields"]["rrd_regulatory_response_doc_rr_regulatory_response_1"] = array (
  'name' => 'rrd_regulatory_response_doc_rr_regulatory_response_1',
  'type' => 'link',
  'relationship' => 'rrd_regulatory_response_doc_rr_regulatory_response_1',
  'source' => 'non-db',
  'module' => 'RRD_Regulatory_Response_Doc',
  'bean_name' => 'RRD_Regulatory_Response_Doc',
  'vname' => 'LBL_RRD_REGULATORY_RESPONSE_DOC_RR_REGULATORY_RESPONSE_1_FROM_RRD_REGULATORY_RESPONSE_DOC_TITLE',
  'id_name' => 'rrd_regula67cense_doc_ida',
);
