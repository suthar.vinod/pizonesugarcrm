<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_COMPANY_ACCOUNT_ID'] = 'Company (related Company ID)';
$mod_strings['LBL_COMPANY'] = 'Company';
$mod_strings['LBL_CONTACT_CONTACT_ID'] = 'Contact (related Contact ID)';
$mod_strings['LBL_CONTACT'] = 'Contact';
$mod_strings['LBL_REGULATORY_REGION'] = 'Regulatory Region';
$mod_strings['LBL_DATE_COMPLETED'] = 'Date Completed';
$mod_strings['LBL_RESPONSE_STATUS'] = 'Response Status';
$mod_strings['LBL_RECORD_BODY'] = 'Regulatory Response';
$mod_strings['LBL_DESCRIPTION'] = 'Description of Regulatory Response';
$mod_strings['LBL_FUNCTIONAL_AREA'] = 'Functional Area';
$mod_strings['LBL_RR_REGULATORY_RESPONSE_FOCUS_DRAWER_DASHBOARD'] = 'Regulatory Responses Focus Drawer';
$mod_strings['LBL_RR_REGULATORY_RESPONSE_RECORD_DASHBOARD'] = 'Regulatory Responses Record Dashboard';
