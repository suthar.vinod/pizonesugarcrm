<?php
 // created: 2022-03-23 05:50:17
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_company_c']['labelValue']='Shipping Company';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_company_c']['dependency']='or(isInList($type_3_c,createList("Shipping","Archive")),equal($deliverable_c,"Shipping Request"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_company_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_company_c']['visibility_grid']='';

 ?>