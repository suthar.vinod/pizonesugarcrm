<?php
 // created: 2021-02-25 05:55:20
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['labelValue']='Due to Complete Timeframe (Days)';
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['calculated']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['formula']='ifElse(or(equal($final_due_date_c,""),equal($deliverable_completed_date_c,"")),"",subtract(daysUntil($final_due_date_c),daysUntil($deliverable_completed_date_c)))';
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['enforced']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['required_formula']='';

 ?>