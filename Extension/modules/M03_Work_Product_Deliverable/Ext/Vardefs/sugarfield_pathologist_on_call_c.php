<?php
 // created: 2021-08-12 09:14:13
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_on_call_c']['labelValue']='Pathologist on Call During Reporting Window';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_on_call_c']['dependency']='isInList($deliverable_c,createList("Histopathology Report2","Histopathology Report","Interim Gross Pathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_on_call_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_on_call_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_on_call_c']['visibility_grid']='';

 ?>