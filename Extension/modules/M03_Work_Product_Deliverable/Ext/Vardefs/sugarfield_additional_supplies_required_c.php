<?php
 // created: 2022-05-24 08:59:30
$dictionary['M03_Work_Product_Deliverable']['fields']['additional_supplies_required_c']['labelValue']='Additional Supplies Required for Implant';
$dictionary['M03_Work_Product_Deliverable']['fields']['additional_supplies_required_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['additional_supplies_required_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['additional_supplies_required_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['additional_supplies_required_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['additional_supplies_required_c']['readonly_formula']='';

 ?>