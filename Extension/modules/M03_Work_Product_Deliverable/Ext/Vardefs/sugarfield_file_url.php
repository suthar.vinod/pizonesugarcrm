<?php
 // created: 2020-10-16 16:09:35
$GLOBALS['dictionary']['M03_Work_Product_Deliverable']['fields']['file_url'] = array (

     'name' => 'file_url',
     'vname' => 'LBL_FILE_URL',
     'type' => 'varchar',
     'source' => 'non-db',
     'reportable' => false,
     'comment' => 'Path to file (can be URL)',
     'importable' => false,

);

 ?>