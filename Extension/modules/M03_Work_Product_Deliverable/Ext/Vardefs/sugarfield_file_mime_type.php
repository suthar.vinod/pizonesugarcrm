<?php
 // created: 2020-10-16 16:09:35
$GLOBALS['dictionary']['M03_Work_Product_Deliverable']['fields']['file_mime_type'] = array(

     'name' => 'file_mime_type',
     'vname' => 'LBL_FILE_MIME_TYPE',
     'type' => 'varchar',
     'len' => '100',
     'comment' => 'Attachment MIME type',
     'importable' => false,
);

 ?>