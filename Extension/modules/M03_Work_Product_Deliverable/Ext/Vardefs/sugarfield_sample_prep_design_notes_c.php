<?php
 // created: 2022-05-24 09:03:05
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_design_notes_c']['labelValue']='Sample Prep Design Notes';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_design_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_design_notes_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_design_notes_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_design_notes_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_design_notes_c']['readonly_formula']='';

 ?>