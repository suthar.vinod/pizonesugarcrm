<?php
 // created: 2021-04-06 09:19:53
$dictionary['M03_Work_Product_Deliverable']['fields']['slide_type_c']['labelValue']='Slide Type';
$dictionary['M03_Work_Product_Deliverable']['fields']['slide_type_c']['dependency']='equal($deliverable_c,"Slide Scanning")';
$dictionary['M03_Work_Product_Deliverable']['fields']['slide_type_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['slide_type_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['slide_type_c']['visibility_grid']='';

 ?>