<?php
 // created: 2021-10-21 07:18:20
$dictionary['M03_Work_Product_Deliverable']['fields']['faxitron_required_c']['labelValue']='Faxitron Required Prior to Shipment';
$dictionary['M03_Work_Product_Deliverable']['fields']['faxitron_required_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['faxitron_required_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['faxitron_required_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['faxitron_required_c']['visibility_grid']='';

 ?>