<?php
 // created: 2022-05-24 08:45:17
$dictionary['M03_Work_Product_Deliverable']['fields']['same_day_prep_c']['labelValue']='Same Day Prep Required?';
$dictionary['M03_Work_Product_Deliverable']['fields']['same_day_prep_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['same_day_prep_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['same_day_prep_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['same_day_prep_c']['visibility_grid']='';

 ?>