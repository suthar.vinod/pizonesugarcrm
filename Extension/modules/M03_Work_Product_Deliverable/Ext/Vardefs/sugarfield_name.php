<?php
 // created: 2020-03-16 12:37:00
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['len']='255';
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['audited']=true;
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['massupdate']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['importable']='false';
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['merge_filter']='disabled';
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['unified_search']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['required']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['duplicate_merge']='disabled';
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['formula']='concat($wp_name_c," ",$deliverable_text_c)';
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['enforced']=true;

 ?>