<?php
 // created: 2022-05-24 09:11:22
$dictionary['M03_Work_Product_Deliverable']['fields']['number_control_implants_c']['labelValue']='Number of implant articles needed per procedure date (Control)';
$dictionary['M03_Work_Product_Deliverable']['fields']['number_control_implants_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['number_control_implants_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['number_control_implants_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['number_control_implants_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['number_control_implants_c']['readonly_formula']='';

 ?>