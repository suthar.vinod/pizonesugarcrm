<?php
 // created: 2020-05-21 18:10:41
$dictionary['M03_Work_Product_Deliverable']['fields']['reason_for_amendment_c']['labelValue']='Reason for Amendment';
$dictionary['M03_Work_Product_Deliverable']['fields']['reason_for_amendment_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['reason_for_amendment_c']['visibility_grid']=array (
  'trigger' => 'deliverable_c',
  'values' => 
  array (
    'Choose One' => 
    array (
    ),
    'QA Data Review' => 
    array (
    ),
    'Critical Phase Audit' => 
    array (
    ),
    'Protocol Development' => 
    array (
    ),
    'Final Report' => 
    array (
    ),
    'Final Report Amendment' => 
    array (
      0 => '',
      1 => 'Typographical Error',
      2 => 'Data Entry Error',
      3 => 'Error in Final Report PDF',
      4 => 'Sponsor Request',
      5 => 'Sponsor Request for Additional Analysis',
    ),
    'Gross Pathology Report' => 
    array (
    ),
    'Histopathology Notes' => 
    array (
    ),
    'Histopathology Report' => 
    array (
    ),
    'Histopathology and Histomorphometry Report' => 
    array (
    ),
    'Animal Health Report' => 
    array (
    ),
    'In Life Report' => 
    array (
    ),
    'Tissue Shipping' => 
    array (
    ),
    'Tissue Trimming' => 
    array (
    ),
    'SEM Prep' => 
    array (
    ),
    'SEM' => 
    array (
    ),
    'SEM Report' => 
    array (
    ),
    'Slide Completion' => 
    array (
    ),
    'Recuts' => 
    array (
    ),
    'Slide Shipping' => 
    array (
    ),
    'Histomorphometry' => 
    array (
    ),
    'Tissue Receipt' => 
    array (
    ),
    'Faxitron' => 
    array (
    ),
    'Interim Report' => 
    array (
    ),
    'Interim Gross Pathology Report' => 
    array (
    ),
    'Interim Histopathology Report' => 
    array (
    ),
    'Interim Animal Health Report' => 
    array (
    ),
    'Interim In Life Report' => 
    array (
    ),
    'Pharmacology Data Summary' => 
    array (
    ),
    'Waiting on external test site' => 
    array (
    ),
    'Bioanalytical Method Development' => 
    array (
    ),
    'Bioanalytical Validation Report' => 
    array (
    ),
    'Bioanalytical Specimen Analysis' => 
    array (
    ),
    'Bioanalytical Sample Prep' => 
    array (
    ),
    'Bioanalytical Stability Analysis' => 
    array (
    ),
    'Bioanalytical Data Table' => 
    array (
    ),
    'Analytical Report' => 
    array (
    ),
    'Bioanalytical Protocol Dvelopment' => 
    array (
    ),
    '' => 
    array (
    ),
    'Abnormal Extract' => 
    array (
    ),
    'Analytical ExhaustiveExaggerated Extraction' => 
    array (
    ),
    'Analytical Headspace Analysis' => 
    array (
    ),
    'Analytical Interim Validation Report' => 
    array (
    ),
    'Analytical Method' => 
    array (
    ),
    'Analytical Nickel Ion Release Extraction' => 
    array (
    ),
    'Analytical Pre testing' => 
    array (
    ),
    'BC Abnormal Study Article' => 
    array (
    ),
    'Analytical Simulated Use Extraction' => 
    array (
    ),
    'Biological Evaluation Plan' => 
    array (
    ),
    'Chemical and Physical Characterization of Particulates' => 
    array (
    ),
    'Contributing Scientist Report Amendment' => 
    array (
    ),
    'Decalcification' => 
    array (
    ),
    'Frozen Slide Completion' => 
    array (
    ),
    'GLP Discontinuation Report' => 
    array (
    ),
    'Gross Morphometry' => 
    array (
    ),
    'Histology Controls' => 
    array (
    ),
    'Histopathology Methods Report' => 
    array (
    ),
    'Paraffin Slide Completion' => 
    array (
    ),
    'Pathology Unknown' => 
    array (
    ),
    'EXAKT Slide Completion' => 
    array (
    ),
    'Plastic Microtome Slide Completion' => 
    array (
    ),
    'Regulatory Response' => 
    array (
    ),
    'Risk Assessment Report' => 
    array (
    ),
    'Risk Assessment Report Review' => 
    array (
    ),
    'SEND Report' => 
    array (
    ),
    'Slide Scanning' => 
    array (
    ),
    'SpecimenSample Disposition' => 
    array (
    ),
    'Sponsor Contracted Contributing Scientist Report' => 
    array (
    ),
    'Statistical Summary' => 
    array (
    ),
    'Statistics Report' => 
    array (
    ),
    'Shipping Request' => 
    array (
    ),
  ),
);

 ?>