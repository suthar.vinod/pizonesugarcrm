<?php
 // created: 2021-08-12 09:09:03
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_bc_to_complete_draft_c']['labelValue']='Actual Business Days to Complete Draft';
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_bc_to_complete_draft_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_bc_to_complete_draft_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_bc_to_complete_draft_c']['dependency']='isInList($deliverable_c,createList("Gross Pathology Report","Histopathology Report2","Histopathology Report","Interim Gross Pathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_bc_to_complete_draft_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_bc_to_complete_draft_c']['readonly_formula']='';

 ?>