<?php
 // created: 2021-10-21 06:58:56
$dictionary['M03_Work_Product_Deliverable']['fields']['address_state_c']['labelValue']='State';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_state_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['address_state_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_state_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_state_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_state_c']['readonly_formula']='';

 ?>