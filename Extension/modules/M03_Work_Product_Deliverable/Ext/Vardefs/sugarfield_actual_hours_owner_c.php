<?php
 // created: 2019-04-22 12:25:00
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_hours_owner_c']['labelValue']='Actual Hours (Deliverable Owner)';
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_hours_owner_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_hours_owner_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_hours_owner_c']['dependency']='';

 ?>