<?php
 // created: 2022-05-24 10:13:23
$dictionary['M03_Work_Product_Deliverable']['fields']['timeline_type_c']['labelValue']='Timeline';
$dictionary['M03_Work_Product_Deliverable']['fields']['timeline_type_c']['dependency']='not(equal($deliverable_c,"Sample Prep Design"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['timeline_type_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['timeline_type_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['timeline_type_c']['visibility_grid']='';

 ?>