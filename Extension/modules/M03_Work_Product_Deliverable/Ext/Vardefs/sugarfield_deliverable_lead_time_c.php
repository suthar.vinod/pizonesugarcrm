<?php
 // created: 2020-06-12 17:16:13
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['labelValue']='Deliverable Lead Time';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['formula']='ifElse(or(equal($due_date_c,""),equal(related($m03_work_product_m03_work_product_deliverable_1,"last_procedure_c"),"")),"",subtract(daysUntil($due_date_c),daysUntil(related($m03_work_product_m03_work_product_deliverable_1,"last_procedure_c"))))';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['enforced']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['dependency']='';

 ?>