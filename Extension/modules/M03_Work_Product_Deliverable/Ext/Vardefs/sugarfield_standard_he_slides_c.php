<?php
 // created: 2021-08-12 11:46:51
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_he_slides_c']['labelValue']='# of Standard H&E Slides';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_he_slides_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_he_slides_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_he_slides_c']['dependency']='isInList($deliverable_c,createList("Paraffin Slide Completion","EXAKT Slide Completion","Plastic Microtome Slide Completion","Frozen Slide Completion","Slide Scanning","Histopathology Report2","Interim Histopathology Report2","Histopathology Report","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_he_slides_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_he_slides_c']['readonly_formula']='';

 ?>