<?php
 // created: 2022-03-23 05:44:43
$dictionary['M03_Work_Product_Deliverable']['fields']['datetime_desired_c']['labelValue']='Date/Time Desired';
$dictionary['M03_Work_Product_Deliverable']['fields']['datetime_desired_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['datetime_desired_c']['dependency']='equal($type_3_c,"Transfer")';
$dictionary['M03_Work_Product_Deliverable']['fields']['datetime_desired_c']['readonly_formula']='';

 ?>