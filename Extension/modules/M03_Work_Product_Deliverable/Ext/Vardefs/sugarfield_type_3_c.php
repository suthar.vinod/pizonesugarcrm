<?php
 // created: 2022-03-23 05:40:48
$dictionary['M03_Work_Product_Deliverable']['fields']['type_3_c']['labelValue']='Type';
$dictionary['M03_Work_Product_Deliverable']['fields']['type_3_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['type_3_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['type_3_c']['visibility_grid']=array (
  'trigger' => 'deliverable_c',
  'values' => 
  array (
    'Choose One' => 
    array (
    ),
    'Abnormal Extract' => 
    array (
    ),
    'Bioanalytical Data Table' => 
    array (
    ),
    'Analytical ExhaustiveExaggerated Extraction' => 
    array (
    ),
    'Analytical Report' => 
    array (
    ),
    'Analytical Headspace Analysis' => 
    array (
    ),
    'Analytical Interim Validation Report' => 
    array (
    ),
    'Bioanalytical Method Development' => 
    array (
    ),
    'Analytical Nickel Ion Release Extraction' => 
    array (
    ),
    'Analytical Pre testing' => 
    array (
    ),
    'BC Abnormal Study Article' => 
    array (
    ),
    'Bioanalytical Protocol Dvelopment' => 
    array (
    ),
    'Bioanalytical Specimen Analysis' => 
    array (
    ),
    'Bioanalytical Sample Prep' => 
    array (
    ),
    'Analytical Simulated Use Extraction' => 
    array (
    ),
    'Bioanalytical Stability Analysis' => 
    array (
    ),
    'Bioanalytical Validation Report' => 
    array (
    ),
    'Animal Health Report' => 
    array (
    ),
    'Biological Evaluation Plan' => 
    array (
    ),
    'Chemical and Physical Characterization of Particulates' => 
    array (
    ),
    'Critical Phase Audit' => 
    array (
    ),
    'Faxitron' => 
    array (
    ),
    'Final Report' => 
    array (
    ),
    'Final Report Amendment' => 
    array (
    ),
    'Frozen Slide Completion' => 
    array (
    ),
    'GLP Discontinuation Report' => 
    array (
    ),
    'Gross Morphometry' => 
    array (
    ),
    'Gross Pathology Report' => 
    array (
    ),
    'Histology Controls' => 
    array (
    ),
    'Histomorphometry' => 
    array (
    ),
    'Histopathology Methods Report' => 
    array (
    ),
    'In Life Report' => 
    array (
    ),
    'Interim Animal Health Report' => 
    array (
    ),
    'Interim Gross Pathology Report' => 
    array (
    ),
    'Interim Histopathology Report' => 
    array (
    ),
    'Interim In Life Report' => 
    array (
    ),
    'Interim Report' => 
    array (
    ),
    'Paraffin Slide Completion' => 
    array (
    ),
    'Pathology Unknown' => 
    array (
    ),
    'Histopathology Notes' => 
    array (
    ),
    'Histopathology Report' => 
    array (
    ),
    'Pharmacology Data Summary' => 
    array (
    ),
    'EXAKT Slide Completion' => 
    array (
    ),
    'Plastic Microtome Slide Completion' => 
    array (
    ),
    'Protocol Development' => 
    array (
    ),
    'QA Data Review' => 
    array (
    ),
    'Recuts' => 
    array (
    ),
    'Regulatory Response' => 
    array (
    ),
    'Risk Assessment Report' => 
    array (
    ),
    'Risk Assessment Report Review' => 
    array (
    ),
    'SEM' => 
    array (
    ),
    'Slide Scanning' => 
    array (
    ),
    'SpecimenSample Disposition' => 
    array (
      0 => '',
      1 => 'Shipping',
      2 => 'Transfer',
      3 => 'Discard',
      4 => 'Archive',
    ),
    'Statistical Summary' => 
    array (
    ),
    'Statistics Report' => 
    array (
    ),
    'Tissue Receipt' => 
    array (
    ),
    'Tissue Trimming' => 
    array (
    ),
    'Waiting on external test site' => 
    array (
    ),
    'Histopathology and Histomorphometry Report' => 
    array (
    ),
    'SEM Prep' => 
    array (
    ),
    'SEM Report' => 
    array (
    ),
    'Shipping Request' => 
    array (
    ),
    'Slide Completion' => 
    array (
    ),
    'Slide Shipping' => 
    array (
    ),
    'Tissue Shipping' => 
    array (
    ),
    '' => 
    array (
    ),
    'Analytical Method' => 
    array (
    ),
    'Contributing Scientist Report Amendment' => 
    array (
    ),
    'Decalcification' => 
    array (
    ),
    'Early DeathTermination Investigation Report' => 
    array (
    ),
    'IACUC Submission' => 
    array (
    ),
    'Pathology Protocol Review' => 
    array (
    ),
    'Pharmacokinetic Report' => 
    array (
    ),
    'SEND Report' => 
    array (
    ),
    'Sponsor Contracted Contributing Scientist Report' => 
    array (
    ),
    'TWT Protocol Review' => 
    array (
    ),
    'TWT Report Review' => 
    array (
    ),
  ),
);

 ?>