<?php
 // created: 2021-08-12 11:51:04
$dictionary['M03_Work_Product_Deliverable']['fields']['large_special_stain_c']['labelValue']='# of Large Special Stain Slides';
$dictionary['M03_Work_Product_Deliverable']['fields']['large_special_stain_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['large_special_stain_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['large_special_stain_c']['dependency']='isInList($deliverable_c,createList("Paraffin Slide Completion","EXAKT Slide Completion","Plastic Microtome Slide Completion","Frozen Slide Completion","Slide Scanning","Histopathology Report2","Interim Histopathology Report2","Histopathology Report","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['large_special_stain_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['large_special_stain_c']['readonly_formula']='';

 ?>