<?php
 // created: 2022-12-08 09:24:38
$dictionary['M03_Work_Product_Deliverable']['fields']['final_due_date_c']['labelValue']='External Final Due Date';
$dictionary['M03_Work_Product_Deliverable']['fields']['final_due_date_c']['formula']='addDays($due_date_c,14)';
$dictionary['M03_Work_Product_Deliverable']['fields']['final_due_date_c']['enforced']='false';
$dictionary['M03_Work_Product_Deliverable']['fields']['final_due_date_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['final_due_date_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['final_due_date_c']['readonly_formula']='';

 ?>