<?php
 // created: 2020-10-16 16:09:35
/* $dictionary['M03_Work_Product_Deliverable']['fields']['filename']['audited']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['massupdate']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['comments']='File name associated with the note (attachment)';
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['importable']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['duplicate_merge']='enabled';
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['duplicate_merge_dom_value']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['merge_filter']='disabled';
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['unified_search']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['calculated']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['dependency']='equal($deliverable_c,"SpecimenSample Disposition")'; */
$GLOBALS['dictionary']['M03_Work_Product_Deliverable']['fields']['filename'] = array (
    'name' => 'filename',
    'vname' => 'LBL_FILENAME',
    'type' => 'file',
    'dbType' => 'varchar',
    'len' => '255',
    'comments' => 'File name associated with the note (attachment)',
    'reportable' => true,
    'comment' => 'File name associated with the note (attachment)',
    'importable' => false,
    'dependency' => 'equal($deliverable_c,"SpecimenSample Disposition")',
	'audited' => false,
	'massupdate' => false,
	'unified_search' => false,
	'calculated' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '1',
	'merge_filter' => 'disabled',
);
 ?>