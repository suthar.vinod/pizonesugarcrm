<?php
 // created: 2020-01-16 12:46:48
$dictionary['M03_Work_Product_Deliverable']['fields']['charge_sheet_submitted_c']['labelValue']='Charge Sheet Submitted';
$dictionary['M03_Work_Product_Deliverable']['fields']['charge_sheet_submitted_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['charge_sheet_submitted_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['charge_sheet_submitted_c']['dependency']='';

 ?>