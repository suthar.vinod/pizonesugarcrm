<?php
 // created: 2021-10-21 07:09:35
$dictionary['M03_Work_Product_Deliverable']['fields']['address_confirmed_c']['labelValue']='Address Confirmed';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_confirmed_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_confirmed_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_confirmed_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_confirmed_c']['visibility_grid']='';

 ?>