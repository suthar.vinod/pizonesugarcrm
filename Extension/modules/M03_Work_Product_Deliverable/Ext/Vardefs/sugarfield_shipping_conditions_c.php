<?php
 // created: 2022-03-23 05:37:27
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_conditions_c']['labelValue']='Shipping Conditions';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_conditions_c']['dependency']='or(isInList($type_3_c,createList("Shipping","Archive")),equal($deliverable_c,"Shipping Request"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_conditions_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_conditions_c']['visibility_grid']='';

 ?>