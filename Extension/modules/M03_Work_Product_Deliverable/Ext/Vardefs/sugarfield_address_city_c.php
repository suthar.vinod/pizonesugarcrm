<?php
 // created: 2021-10-21 06:57:08
$dictionary['M03_Work_Product_Deliverable']['fields']['address_city_c']['labelValue']='City';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_city_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['address_city_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_city_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_city_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_city_c']['readonly_formula']='';

 ?>