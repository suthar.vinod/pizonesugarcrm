<?php
 // created: 2022-05-24 10:21:02
$dictionary['M03_Work_Product_Deliverable']['fields']['overdue_risk_level_c']['labelValue']='Overdue Risk Level';
$dictionary['M03_Work_Product_Deliverable']['fields']['overdue_risk_level_c']['dependency']='not(equal($deliverable_c,"Sample Prep Design"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['overdue_risk_level_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['overdue_risk_level_c']['visibility_grid']='';

 ?>