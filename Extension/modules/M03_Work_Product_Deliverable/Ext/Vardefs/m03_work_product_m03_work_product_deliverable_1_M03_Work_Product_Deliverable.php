<?php
// created: 2016-02-01 20:37:08
$dictionary["M03_Work_Product_Deliverable"]["fields"]["m03_work_product_m03_work_product_deliverable_1"] = array (
  'name' => 'm03_work_product_m03_work_product_deliverable_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_m03_work_product_deliverable_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'id_name' => 'm03_work_p0b66product_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product_Deliverable"]["fields"]["m03_work_product_m03_work_product_deliverable_1_name"] = array (
  'name' => 'm03_work_product_m03_work_product_deliverable_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p0b66product_ida',
  'link' => 'm03_work_product_m03_work_product_deliverable_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["M03_Work_Product_Deliverable"]["fields"]["m03_work_p0b66product_ida"] = array (
  'name' => 'm03_work_p0b66product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE_ID',
  'id_name' => 'm03_work_p0b66product_ida',
  'link' => 'm03_work_product_m03_work_product_deliverable_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
