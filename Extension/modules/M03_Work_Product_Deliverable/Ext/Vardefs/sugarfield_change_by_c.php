<?php
 // created: 2021-07-29 09:29:54
$dictionary['M03_Work_Product_Deliverable']['fields']['change_by_c']['labelValue']='Change By';
$dictionary['M03_Work_Product_Deliverable']['fields']['change_by_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['change_by_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['change_by_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['change_by_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['change_by_c']['readonly_formula']='';

 ?>