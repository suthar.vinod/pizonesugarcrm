<?php
 // created: 2022-05-24 08:56:32
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_time_estimate_c']['labelValue']='Sample Prep Time Estimate (min)';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_time_estimate_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_time_estimate_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_time_estimate_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_time_estimate_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_time_estimate_c']['readonly_formula']='';

 ?>