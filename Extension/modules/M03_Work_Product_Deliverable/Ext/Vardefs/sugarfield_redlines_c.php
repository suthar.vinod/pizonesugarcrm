<?php
 // created: 2022-05-24 10:19:03
$dictionary['M03_Work_Product_Deliverable']['fields']['redlines_c']['labelValue']='Audit After Redlines?';
$dictionary['M03_Work_Product_Deliverable']['fields']['redlines_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['redlines_c']['dependency']='not(equal($deliverable_c,"Sample Prep Design"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['redlines_c']['readonly_formula']='';

 ?>