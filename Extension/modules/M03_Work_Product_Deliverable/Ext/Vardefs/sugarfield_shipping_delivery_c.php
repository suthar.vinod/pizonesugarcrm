<?php
 // created: 2022-03-23 05:33:16
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_delivery_c']['labelValue']='Shipping Delivery';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_delivery_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_delivery_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_delivery_c']['visibility_grid']=array (
  'trigger' => 'shipping_company_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Fed Ex' => 
    array (
      0 => '',
      1 => 'International Priority',
      2 => 'First Priority Overnight',
      3 => 'Priority Overnight',
      4 => '2 Day',
      5 => 'Ground',
    ),
    'On Time' => 
    array (
      0 => '',
      1 => 'Direct',
      2 => 'Same Day',
      3 => '1 HR',
      4 => '2 HR',
      5 => '3 HR',
    ),
    'World Courier' => 
    array (
    ),
  ),
);

 ?>