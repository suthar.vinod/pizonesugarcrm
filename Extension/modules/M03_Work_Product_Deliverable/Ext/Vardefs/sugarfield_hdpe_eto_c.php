<?php
 // created: 2022-05-24 08:51:14
$dictionary['M03_Work_Product_Deliverable']['fields']['hdpe_eto_c']['labelValue']='HDPE Comparison ETO Required?';
$dictionary['M03_Work_Product_Deliverable']['fields']['hdpe_eto_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['hdpe_eto_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['hdpe_eto_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['hdpe_eto_c']['visibility_grid']='';

 ?>