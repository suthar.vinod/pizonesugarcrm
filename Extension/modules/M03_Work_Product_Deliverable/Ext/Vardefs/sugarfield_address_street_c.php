<?php
 // created: 2021-10-21 06:55:28
$dictionary['M03_Work_Product_Deliverable']['fields']['address_street_c']['labelValue']='Street';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_street_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['address_street_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_street_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_street_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_street_c']['readonly_formula']='';

 ?>