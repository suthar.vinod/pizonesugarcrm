<?php
 // created: 2022-05-24 10:15:35
$dictionary['M03_Work_Product_Deliverable']['fields']['work_type_c']['labelValue']='Work Type';
$dictionary['M03_Work_Product_Deliverable']['fields']['work_type_c']['dependency']='not(equal($deliverable_c,"Sample Prep Design"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['work_type_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['work_type_c']['visibility_grid']='';

 ?>