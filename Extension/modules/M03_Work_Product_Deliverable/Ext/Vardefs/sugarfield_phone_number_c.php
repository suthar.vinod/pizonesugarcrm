<?php
 // created: 2021-10-21 07:08:03
$dictionary['M03_Work_Product_Deliverable']['fields']['phone_number_c']['labelValue']='Phone Number';
$dictionary['M03_Work_Product_Deliverable']['fields']['phone_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['phone_number_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['phone_number_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['phone_number_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['phone_number_c']['readonly_formula']='';

 ?>