<?php
 // created: 2021-08-12 09:07:15
$dictionary['M03_Work_Product_Deliverable']['fields']['planned_bd_to_draft_c']['labelValue']='Planned Business Days to Draft Report';
$dictionary['M03_Work_Product_Deliverable']['fields']['planned_bd_to_draft_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['planned_bd_to_draft_c']['dependency']='isInList($deliverable_c,createList("Gross Pathology Report","Histopathology Report2","Histopathology Report","Interim Gross Pathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['planned_bd_to_draft_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['planned_bd_to_draft_c']['readonly_formula']='';

 ?>