<?php
 // created: 2021-08-12 09:11:25
$dictionary['M03_Work_Product_Deliverable']['fields']['report_writing_days_c']['labelValue']='Report Writing Days';
$dictionary['M03_Work_Product_Deliverable']['fields']['report_writing_days_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['report_writing_days_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['report_writing_days_c']['dependency']='isInList($deliverable_c,createList("Histopathology Report2","Histopathology Report","Interim Gross Pathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['report_writing_days_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['report_writing_days_c']['readonly_formula']='';

 ?>