<?php
 // created: 2021-09-14 08:56:02
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['labelValue']='Internal BC Final Due Date Goal';
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['formula']='addDays($final_due_date_c,-3)';
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['enforced']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['dependency']='not(isInList($deliverable_c,createList("Animal Selection","SpecimenSample Disposition")))';
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['readonly_formula']='';

 ?>