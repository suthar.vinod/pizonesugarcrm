<?php
 // created: 2022-05-24 10:19:50
$dictionary['M03_Work_Product_Deliverable']['fields']['draft_deliverable_sent_date_c']['labelValue']='Draft Deliverable Sent Date';
$dictionary['M03_Work_Product_Deliverable']['fields']['draft_deliverable_sent_date_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['draft_deliverable_sent_date_c']['dependency']='not(equal($deliverable_c,"Sample Prep Design"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['draft_deliverable_sent_date_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['draft_deliverable_sent_date_c']['readonly_formula']='';

 ?>