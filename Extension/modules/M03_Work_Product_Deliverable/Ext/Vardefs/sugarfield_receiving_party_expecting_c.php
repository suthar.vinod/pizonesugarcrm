<?php
 // created: 2021-10-21 07:13:02
$dictionary['M03_Work_Product_Deliverable']['fields']['receiving_party_expecting_c']['labelValue']='Receiving Party Expecting Shipment';
$dictionary['M03_Work_Product_Deliverable']['fields']['receiving_party_expecting_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['receiving_party_expecting_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['receiving_party_expecting_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['receiving_party_expecting_c']['visibility_grid']='';

 ?>