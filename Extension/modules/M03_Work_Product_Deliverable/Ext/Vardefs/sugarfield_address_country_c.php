<?php
 // created: 2021-10-21 07:06:54
$dictionary['M03_Work_Product_Deliverable']['fields']['address_country_c']['labelValue']='Country';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_country_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['address_country_c']['enforced']='false';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_country_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_country_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_country_c']['readonly_formula']='';

 ?>