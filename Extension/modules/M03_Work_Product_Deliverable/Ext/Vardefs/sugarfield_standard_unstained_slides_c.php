<?php
 // created: 2021-04-06 09:06:39
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_unstained_slides_c']['labelValue']='# of Standard Unstained Slides';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_unstained_slides_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_unstained_slides_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_unstained_slides_c']['dependency']='isInList($deliverable_c,createList("Paraffin Slide Completion","Plastic Microtome Slide Completion","Frozen Slide Completion"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_unstained_slides_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_unstained_slides_c']['readonly_formula']='';

 ?>