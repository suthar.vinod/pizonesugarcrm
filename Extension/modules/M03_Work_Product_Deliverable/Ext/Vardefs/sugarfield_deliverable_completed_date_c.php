<?php
 // created: 2022-05-24 12:02:55
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_completed_date_c']['labelValue']='Deliverable Completed Date';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_completed_date_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_completed_date_c']['dependency']='equal($deliverable_status_c,"Completed")';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_completed_date_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_completed_date_c']['readonly_formula']='';

 ?>