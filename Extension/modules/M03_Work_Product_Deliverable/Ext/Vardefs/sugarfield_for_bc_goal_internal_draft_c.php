<?php
 // created: 2021-02-18 08:52:44
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['labelValue']='Internal BC Draft Due Date Goal';
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['formula']='addDays($final_due_date_c,-14)';
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['enforced']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['dependency']='not(isInList($deliverable_c,createList("Animal Selection","SpecimenSample Disposition")))';
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['required_formula']='';

 ?>