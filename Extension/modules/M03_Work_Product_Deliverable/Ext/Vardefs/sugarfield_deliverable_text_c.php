<?php
 // created: 2022-03-23 05:36:09
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['labelValue']='Deliverable Text';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['formula']='getDropdownValue("deliverable_list",$deliverable_c)';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['enforced']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['readonly_formula']='';

 ?>