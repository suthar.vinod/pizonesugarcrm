<?php
 // created: 2022-03-09 09:03:53
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_com_date_test_c']['labelValue']='Deliverable Com Date Test';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_com_date_test_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_com_date_test_c']['required_formula']='equal($deliverable_status_c,"Completed")';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_com_date_test_c']['readonly_formula']='';

 ?>