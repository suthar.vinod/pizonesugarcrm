<?php
 // created: 2021-08-12 09:03:05
$dictionary['M03_Work_Product_Deliverable']['fields']['minutes_per_slide_c']['labelValue']='Minutes per Slide';
$dictionary['M03_Work_Product_Deliverable']['fields']['minutes_per_slide_c']['dependency']='isInList($deliverable_c,createList("Histopathology Report2","Histopathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['minutes_per_slide_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['minutes_per_slide_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['minutes_per_slide_c']['visibility_grid']='';

 ?>