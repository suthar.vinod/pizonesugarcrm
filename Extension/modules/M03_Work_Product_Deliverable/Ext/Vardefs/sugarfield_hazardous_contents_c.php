<?php
 // created: 2022-03-23 05:49:09
$dictionary['M03_Work_Product_Deliverable']['fields']['hazardous_contents_c']['labelValue']='Hazardous Contents';
$dictionary['M03_Work_Product_Deliverable']['fields']['hazardous_contents_c']['dependency']='or(isInList($type_3_c,createList("Shipping","Archive")),equal($deliverable_c,"Shipping Request"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['hazardous_contents_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['hazardous_contents_c']['visibility_grid']='';

 ?>