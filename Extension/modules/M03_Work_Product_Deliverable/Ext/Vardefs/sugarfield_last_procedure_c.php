<?php
 // created: 2022-03-24 11:22:59
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['labelValue']='Last Procedure';
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['calculated']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['formula']='related($m03_work_product_m03_work_product_deliverable_1,"last_procedure_c")';
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['enforced']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['readonly_formula']='';

 ?>