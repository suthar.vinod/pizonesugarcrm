<?php
 // created: 2021-09-14 08:58:15
$dictionary['M03_Work_Product_Deliverable']['fields']['within_timeframe_c']['labelValue']='Within Timeframe';
$dictionary['M03_Work_Product_Deliverable']['fields']['within_timeframe_c']['dependency']='equal(related($m03_work_product_m03_work_product_deliverable_1,"functional_area_c"),"Standard Biocompatibility")';
$dictionary['M03_Work_Product_Deliverable']['fields']['within_timeframe_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['within_timeframe_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['within_timeframe_c']['visibility_grid']='';

 ?>