<?php
 // created: 2021-10-21 05:53:52
$dictionary['M03_Work_Product_Deliverable']['fields']['attn_to_name_c']['labelValue']='Attn. To Name';
$dictionary['M03_Work_Product_Deliverable']['fields']['attn_to_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['attn_to_name_c']['enforced']='false';
$dictionary['M03_Work_Product_Deliverable']['fields']['attn_to_name_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['attn_to_name_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['attn_to_name_c']['readonly_formula']='';

 ?>