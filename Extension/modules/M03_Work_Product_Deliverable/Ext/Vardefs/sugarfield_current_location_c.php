<?php
 // created: 2022-03-23 05:43:20
$dictionary['M03_Work_Product_Deliverable']['fields']['current_location_c']['labelValue']='Current Location';
$dictionary['M03_Work_Product_Deliverable']['fields']['current_location_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['current_location_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['current_location_c']['dependency']='equal($type_3_c,"Transfer")';
$dictionary['M03_Work_Product_Deliverable']['fields']['current_location_c']['readonly_formula']='';

 ?>