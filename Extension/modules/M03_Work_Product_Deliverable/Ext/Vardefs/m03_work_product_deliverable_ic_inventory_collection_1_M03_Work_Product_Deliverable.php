<?php
// created: 2022-02-22 07:48:02
$dictionary["M03_Work_Product_Deliverable"]["fields"]["m03_work_product_deliverable_ic_inventory_collection_1"] = array (
  'name' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'source' => 'non-db',
  'module' => 'IC_Inventory_Collection',
  'bean_name' => 'IC_Inventory_Collection',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'id_name' => 'm03_work_p2592verable_ida',
  'link-type' => 'many',
  'side' => 'left',
);
