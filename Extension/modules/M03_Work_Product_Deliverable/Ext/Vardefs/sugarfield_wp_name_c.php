<?php
 // created: 2022-03-23 05:34:45
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['labelValue']='WP Name';
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['formula']='related($m03_work_product_m03_work_product_deliverable_1,"name")';
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['enforced']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['readonly_formula']='';

 ?>