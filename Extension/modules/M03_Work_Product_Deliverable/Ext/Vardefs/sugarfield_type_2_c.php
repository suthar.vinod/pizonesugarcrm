<?php
 // created: 2022-03-23 05:51:00
$dictionary['M03_Work_Product_Deliverable']['fields']['type_2_c']['labelValue']='Subtype';
$dictionary['M03_Work_Product_Deliverable']['fields']['type_2_c']['dependency']='or(isInList($type_3_c,createList("Shipping","Archive")),equal($deliverable_c,"Shipping Request"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['type_2_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['type_2_c']['visibility_grid']='';

 ?>