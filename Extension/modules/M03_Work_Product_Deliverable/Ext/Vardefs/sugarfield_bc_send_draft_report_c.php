<?php
 // created: 2022-05-24 10:18:11
$dictionary['M03_Work_Product_Deliverable']['fields']['bc_send_draft_report_c']['labelValue']='Send BC Draft Report?';
$dictionary['M03_Work_Product_Deliverable']['fields']['bc_send_draft_report_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['bc_send_draft_report_c']['dependency']='not(equal($deliverable_c,"Sample Prep Design"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['bc_send_draft_report_c']['readonly_formula']='';

 ?>