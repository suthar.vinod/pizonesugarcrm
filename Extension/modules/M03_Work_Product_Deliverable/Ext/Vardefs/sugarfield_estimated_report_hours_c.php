<?php
 // created: 2019-04-22 12:24:23
$dictionary['M03_Work_Product_Deliverable']['fields']['estimated_report_hours_c']['labelValue']='Estimated Hours (Deliverable Owner)';
$dictionary['M03_Work_Product_Deliverable']['fields']['estimated_report_hours_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['estimated_report_hours_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['estimated_report_hours_c']['dependency']='';

 ?>