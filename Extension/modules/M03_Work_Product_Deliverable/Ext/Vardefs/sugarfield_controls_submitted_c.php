<?php
 // created: 2020-01-17 15:27:58
$dictionary['M03_Work_Product_Deliverable']['fields']['controls_submitted_c']['labelValue']='Controls Submitted';
$dictionary['M03_Work_Product_Deliverable']['fields']['controls_submitted_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['controls_submitted_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['controls_submitted_c']['dependency']='';

 ?>