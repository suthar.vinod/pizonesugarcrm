<?php
 // created: 2022-05-24 08:48:33
$dictionary['M03_Work_Product_Deliverable']['fields']['neat_preparation_c']['labelValue']='Neat Preparation?';
$dictionary['M03_Work_Product_Deliverable']['fields']['neat_preparation_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['neat_preparation_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['neat_preparation_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['neat_preparation_c']['visibility_grid']='';

 ?>