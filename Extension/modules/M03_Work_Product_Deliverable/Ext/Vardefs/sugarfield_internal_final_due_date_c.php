<?php
 // created: 2022-05-24 10:17:30
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['labelValue']='Internal Final Due Date';
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['formula']='addDays($final_due_date_c,-1)';
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['enforced']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['dependency']='not(isInList($deliverable_c,createList("Animal Selection","Animal Selection Populate TSD","Animal Selection Procedure Use","Sample Prep Design")))';
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['readonly_formula']='';

 ?>