<?php
 // created: 2022-03-23 05:42:32
$dictionary['M03_Work_Product_Deliverable']['fields']['stability_considerations_c']['labelValue']='Stability Considerations';
$dictionary['M03_Work_Product_Deliverable']['fields']['stability_considerations_c']['dependency']='isInList($type_3_c,createList("Shipping","Transfer","Archive"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['stability_considerations_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['stability_considerations_c']['visibility_grid']='';

 ?>