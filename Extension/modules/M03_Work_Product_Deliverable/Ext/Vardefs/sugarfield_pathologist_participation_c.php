<?php
 // created: 2021-08-12 08:58:03
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_participation_c']['labelValue']='Pathologist Participation at Necropsy Needed';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_participation_c']['dependency']='isInList($deliverable_c,createList("Histopathology Report2","Interim Histopathology Report2","Histopathology Report","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_participation_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_participation_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_participation_c']['visibility_grid']='';

 ?>