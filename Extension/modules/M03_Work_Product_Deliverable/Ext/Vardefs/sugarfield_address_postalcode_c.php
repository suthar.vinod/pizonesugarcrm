<?php
 // created: 2021-10-21 07:00:54
$dictionary['M03_Work_Product_Deliverable']['fields']['address_postalcode_c']['labelValue']='Postal Code';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_postalcode_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['address_postalcode_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_postalcode_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_postalcode_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_postalcode_c']['readonly_formula']='';

 ?>