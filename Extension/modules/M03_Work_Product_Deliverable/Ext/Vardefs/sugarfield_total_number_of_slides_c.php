<?php
 // created: 2021-08-12 09:04:23
$dictionary['M03_Work_Product_Deliverable']['fields']['total_number_of_slides_c']['labelValue']='Total Number of Slides';
$dictionary['M03_Work_Product_Deliverable']['fields']['total_number_of_slides_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['total_number_of_slides_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['total_number_of_slides_c']['dependency']='isInList($deliverable_c,createList("Histopathology Report2","Histopathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['total_number_of_slides_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['total_number_of_slides_c']['readonly_formula']='';

 ?>