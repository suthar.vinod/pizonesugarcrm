<?php
 // created: 2022-03-23 05:51:50
$dictionary['M03_Work_Product_Deliverable']['fields']['analyze_by_date_c']['labelValue']='Analyze-by Date';
$dictionary['M03_Work_Product_Deliverable']['fields']['analyze_by_date_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['analyze_by_date_c']['dependency']='equal($stability_considerations_c,"Yes")';
$dictionary['M03_Work_Product_Deliverable']['fields']['analyze_by_date_c']['readonly_formula']='';

 ?>