<?php
 // created: 2019-04-18 12:42:08
$dictionary['M03_Work_Product_Deliverable']['fields']['amendment_responsibility_c']['labelValue']='Amendment Responsibility';
$dictionary['M03_Work_Product_Deliverable']['fields']['amendment_responsibility_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['amendment_responsibility_c']['visibility_grid']=array (
  'trigger' => 'deliverable_c',
  'values' => 
  array (
    'Choose One' => 
    array (
    ),
    'QA Data Review' => 
    array (
    ),
    'Critical Phase Audit' => 
    array (
    ),
    'Protocol Development' => 
    array (
    ),
    'Final Report' => 
    array (
    ),
    'Final Report Amendment' => 
    array (
      0 => '',
      1 => 'APS Error',
      2 => 'Sponsor Error',
    ),
    'Gross Pathology Report' => 
    array (
    ),
    'Histopathology Notes' => 
    array (
    ),
    'Histopathology Report' => 
    array (
    ),
    'Histopathology and Histomorphometry Report' => 
    array (
    ),
    'Animal Health Report' => 
    array (
    ),
    'In Life Report' => 
    array (
    ),
    'Tissue Shipping' => 
    array (
    ),
    'Tissue Trimming' => 
    array (
    ),
    'SEM Prep' => 
    array (
    ),
    'SEM' => 
    array (
    ),
    'SEM Report' => 
    array (
    ),
    'Slide Completion' => 
    array (
    ),
    'Recuts' => 
    array (
    ),
    'Slide Shipping' => 
    array (
    ),
    'Histomorphometry' => 
    array (
    ),
    'Tissue Receipt' => 
    array (
    ),
    'Faxitron' => 
    array (
    ),
    'Interim Report' => 
    array (
    ),
    'Interim Gross Pathology Report' => 
    array (
    ),
    'Interim Histopathology Report' => 
    array (
    ),
    'Interim Animal Health Report' => 
    array (
    ),
    'Interim In Life Report' => 
    array (
    ),
    'Pharmacology Data Summary' => 
    array (
    ),
    'Waiting on external test site' => 
    array (
    ),
    'Bioanalytical Method Development' => 
    array (
    ),
    'Bioanalytical Validation Report' => 
    array (
    ),
    'Bioanalytical Specimen Analysis' => 
    array (
    ),
    'Bioanalytical Sample Prep' => 
    array (
    ),
    'Bioanalytical Stability Analysis' => 
    array (
    ),
    'Bioanalytical Data Table' => 
    array (
    ),
    'Analytical Report' => 
    array (
    ),
    'Bioanalytical Protocol Dvelopment' => 
    array (
    ),
  ),
);

 ?>