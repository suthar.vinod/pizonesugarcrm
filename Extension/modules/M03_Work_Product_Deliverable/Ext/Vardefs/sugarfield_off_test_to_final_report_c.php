<?php
 // created: 2021-06-17 08:50:23
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['labelValue']='Off Test to Final Report';
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['calculated']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['formula']='ifElse(and(equal($deliverable_c,"Final Report"),not(equal($deliverable_completed_date_c,"")),not(equal(related($m03_work_product_m03_work_product_deliverable_1,"last_procedure_c"),""))),subtract(daysUntil($deliverable_completed_date_c),daysUntil(related($m03_work_product_m03_work_product_deliverable_1,"last_procedure_c"))),"")';
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['enforced']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['readonly_formula']='';

 ?>