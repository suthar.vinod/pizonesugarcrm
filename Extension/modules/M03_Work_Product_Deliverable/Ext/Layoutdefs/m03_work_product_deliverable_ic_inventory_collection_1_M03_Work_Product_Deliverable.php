<?php
 // created: 2022-02-22 07:48:02
$layout_defs["M03_Work_Product_Deliverable"]["subpanel_setup"]['m03_work_product_deliverable_ic_inventory_collection_1'] = array (
  'order' => 100,
  'module' => 'IC_Inventory_Collection',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'get_subpanel_data' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
