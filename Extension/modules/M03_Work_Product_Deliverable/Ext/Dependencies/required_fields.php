<?php
$dependencies['M03_Work_Product_Deliverable']['required_fields'] = array(
	'hooks' => array("all"),
	'trigger' => 'true',
	'triggerFields' => array('deliverable_c','date_entered','id',),
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			'params' => array(
				'target' => 'work_type_c',
				'value' => 'and(or(not(greaterThan(strlen(toString($date_entered)),0)),isAfter($date_entered,"2020-06-10")),not(equal($deliverable_c,"Sample Prep Design")))',
			),
		),		
	)
);
