<?php

$dependencies['M03_Work_Product_Deliverable']['duedate_required_123'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('deliverable_c','id'),
	'onload'        => true,
    'actions'       => array(
		
       array( 
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'due_date_c',
                 'value' => 'not(isInList($deliverable_c,createList("Animal Selection","Animal Selection Populate TSD","Animal Selection Procedure Use","Sample Prep Design")))',
            ),
        ), 
		// array( 
        //     'name' => 'SetRequired',
        //     'params' => array(
        //         'target' => 'due_date_c',
        //         //'value' => 'and(not(equal(related($m03_work_product_m03_work_product_deliverable_1,"functional_area_c"),"Standard Biocompatibility")),not(isInList($deliverable_c,createList("Animal Selection"))))',
        //         'value' => 'not(equal(related($m03_work_product_m03_work_product_deliverable_1,"functional_area_c"),"Standard Biocompatibility"))',
        //     ),
        // ),
    ),
);

$dependencies['M03_Work_Product_Deliverable']['final_due_date_required'] = array(
    'hooks' => array("edit", "view"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_m03_work_product_deliverable_1','final_due_date_c','functional_area_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'final_due_date_c',
                'value' => 'not(equal(related($m03_work_product_m03_work_product_deliverable_1,"functional_area_c"),"Standard Biocompatibility"))',
            ),
        ),    
    ),
);
  
