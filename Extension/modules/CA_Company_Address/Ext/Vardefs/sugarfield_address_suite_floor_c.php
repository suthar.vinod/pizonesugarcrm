<?php
 // created: 2019-07-22 12:07:03
$dictionary['CA_Company_Address']['fields']['address_suite_floor_c']['labelValue']='Suite/Floor';
$dictionary['CA_Company_Address']['fields']['address_suite_floor_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CA_Company_Address']['fields']['address_suite_floor_c']['enforced']='';
$dictionary['CA_Company_Address']['fields']['address_suite_floor_c']['dependency']='';

 ?>