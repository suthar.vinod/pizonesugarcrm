<?php
 // created: 2019-07-22 12:09:37
$dictionary['CA_Company_Address']['fields']['address_country_c']['labelValue']='Country';
$dictionary['CA_Company_Address']['fields']['address_country_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CA_Company_Address']['fields']['address_country_c']['group']='address_c';
$dictionary['CA_Company_Address']['fields']['address_country_c']['enforced']='';
$dictionary['CA_Company_Address']['fields']['address_country_c']['dependency']='';

 ?>