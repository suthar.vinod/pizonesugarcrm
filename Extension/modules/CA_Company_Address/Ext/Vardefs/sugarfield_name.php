<?php
 // created: 2019-07-03 12:06:49
$dictionary['CA_Company_Address']['fields']['name']['len']='255';
$dictionary['CA_Company_Address']['fields']['name']['required']=false;
$dictionary['CA_Company_Address']['fields']['name']['audited']=true;
$dictionary['CA_Company_Address']['fields']['name']['massupdate']=false;
$dictionary['CA_Company_Address']['fields']['name']['unified_search']=false;
$dictionary['CA_Company_Address']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['CA_Company_Address']['fields']['name']['calculated']=false;

 ?>