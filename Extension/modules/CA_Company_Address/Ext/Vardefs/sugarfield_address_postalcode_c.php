<?php
 // created: 2019-07-22 12:05:11
$dictionary['CA_Company_Address']['fields']['address_postalcode_c']['labelValue']='PostalCode';
$dictionary['CA_Company_Address']['fields']['address_postalcode_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CA_Company_Address']['fields']['address_postalcode_c']['group']='address_c';
$dictionary['CA_Company_Address']['fields']['address_postalcode_c']['enforced']='';
$dictionary['CA_Company_Address']['fields']['address_postalcode_c']['dependency']='';

 ?>