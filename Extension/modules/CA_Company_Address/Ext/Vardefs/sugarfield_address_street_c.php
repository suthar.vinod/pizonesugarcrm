<?php
 // created: 2019-07-22 12:08:21
$dictionary['CA_Company_Address']['fields']['address_street_c']['labelValue']='Street';
$dictionary['CA_Company_Address']['fields']['address_street_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CA_Company_Address']['fields']['address_street_c']['group']='address_c';
$dictionary['CA_Company_Address']['fields']['address_street_c']['enforced']='';
$dictionary['CA_Company_Address']['fields']['address_street_c']['dependency']='';

 ?>