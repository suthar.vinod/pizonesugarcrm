<?php
 
$viewdefs['CA_Company_Address']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterCompaniesAddress',
    'name' => 'Companies',
    'filter_definition' => array(
        array(
            'accounts_ca_company_address_1_name' => array(
                '$in' => '',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);

