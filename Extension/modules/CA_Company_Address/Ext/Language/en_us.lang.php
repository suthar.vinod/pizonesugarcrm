<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TYPE_2'] = 'Type';
$mod_strings['LBL_ADDRESS'] = 'Address';
$mod_strings['LBL_ADDRESS_STREET'] = 'Street';
$mod_strings['LBL_ADDRESS_CITY'] = 'City';
$mod_strings['LBL_ADDRESS_STATE'] = 'State/Providence/District';
$mod_strings['LBL_ADDRESS_POSTALCODE'] = 'PostalCode';
$mod_strings['LBL_ADDRESS_COUNTRY'] = 'Country';
$mod_strings['LBL_ADDRESS_SUITE_FLOOR'] = 'Suite/Floor';
$mod_strings['LBL_ADDRESS_BUILDING'] = 'Building';
$mod_strings['LBL_RECORD_BODY'] = 'Company Address';
$mod_strings['LBL_CA_COMPANY_ADDRESS_FOCUS_DRAWER_DASHBOARD'] = 'Company Addresses Focus Drawer';
$mod_strings['LBL_CA_COMPANY_ADDRESS_RECORD_DASHBOARD'] = 'Company Addresses Record Dashboard';
