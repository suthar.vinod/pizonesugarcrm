<?php
// created: 2020-01-07 13:27:00
$dictionary["W_Weight"]["fields"]["w_weight_m06_error_1"] = array (
  'name' => 'w_weight_m06_error_1',
  'type' => 'link',
  'relationship' => 'w_weight_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'w_weight_m06_error_1m06_error_idb',
);
$dictionary["W_Weight"]["fields"]["w_weight_m06_error_1_name"] = array (
  'name' => 'w_weight_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'save' => true,
  'id_name' => 'w_weight_m06_error_1m06_error_idb',
  'link' => 'w_weight_m06_error_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'name',
);
$dictionary["W_Weight"]["fields"]["w_weight_m06_error_1m06_error_idb"] = array (
  'name' => 'w_weight_m06_error_1m06_error_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_1_FROM_M06_ERROR_TITLE_ID',
  'id_name' => 'w_weight_m06_error_1m06_error_idb',
  'link' => 'w_weight_m06_error_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
