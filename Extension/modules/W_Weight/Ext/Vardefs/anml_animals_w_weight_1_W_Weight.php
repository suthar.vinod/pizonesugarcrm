<?php
// created: 2020-01-07 13:24:35
$dictionary["W_Weight"]["fields"]["anml_animals_w_weight_1"] = array (
  'name' => 'anml_animals_w_weight_1',
  'type' => 'link',
  'relationship' => 'anml_animals_w_weight_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_W_WEIGHT_TITLE',
  'id_name' => 'anml_animals_w_weight_1anml_animals_ida',
  'link-type' => 'one',
  'required' => true,
);
$dictionary["W_Weight"]["fields"]["anml_animals_w_weight_1_name"] = array (
  'name' => 'anml_animals_w_weight_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_w_weight_1anml_animals_ida',
  'link' => 'anml_animals_w_weight_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
  'required' => true,
);
$dictionary["W_Weight"]["fields"]["anml_animals_w_weight_1anml_animals_ida"] = array (
  'name' => 'anml_animals_w_weight_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_W_WEIGHT_TITLE_ID',
  'id_name' => 'anml_animals_w_weight_1anml_animals_ida',
  'link' => 'anml_animals_w_weight_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
  'required' => true,
);
$dictionary["W_Weight"]["fields"]["anml_animals_w_weight_1_name"] ["required"]= true;
$dictionary["W_Weight"]["fields"]["anml_animals_w_weight_1anml_animals_ida"] ["required"]= true;
$dictionary["W_Weight"]["fields"]["anml_animals_w_weight_1"] ["required"]= true;