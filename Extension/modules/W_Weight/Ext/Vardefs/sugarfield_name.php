<?php
 // created: 2021-08-12 12:58:32
$dictionary['W_Weight']['fields']['name']['importable']='false';
$dictionary['W_Weight']['fields']['name']['duplicate_merge']='disabled';
$dictionary['W_Weight']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['W_Weight']['fields']['name']['merge_filter']='disabled';
$dictionary['W_Weight']['fields']['name']['unified_search']=false;
$dictionary['W_Weight']['fields']['name']['calculated']='1';
$dictionary['W_Weight']['fields']['name']['formula']='concat($test_system_name_c," WT ",toString($date_time),toString($date_2_c))';
$dictionary['W_Weight']['fields']['name']['enforced']=true;
$dictionary['W_Weight']['fields']['name']['hidemassupdate']=false;

 ?>