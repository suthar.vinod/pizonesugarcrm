<?php
// created: 2021-09-14 09:20:50
$dictionary["W_Weight"]["fields"]["w_weight_m06_error_2"] = array (
  'name' => 'w_weight_m06_error_2',
  'type' => 'link',
  'relationship' => 'w_weight_m06_error_2',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_2_FROM_W_WEIGHT_TITLE',
  'id_name' => 'w_weight_m06_error_2w_weight_ida',
  'link-type' => 'many',
  'side' => 'left',
);
