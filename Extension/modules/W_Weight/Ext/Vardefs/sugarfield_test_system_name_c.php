<?php
 // created: 2020-01-07 13:30:06
$dictionary['W_Weight']['fields']['test_system_name_c']['duplicate_merge_dom_value']=0;
$dictionary['W_Weight']['fields']['test_system_name_c']['labelValue']='Test System Name';
$dictionary['W_Weight']['fields']['test_system_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['W_Weight']['fields']['test_system_name_c']['calculated']='true';
$dictionary['W_Weight']['fields']['test_system_name_c']['formula']='related($anml_animals_w_weight_1,"name")';
$dictionary['W_Weight']['fields']['test_system_name_c']['enforced']='true';
$dictionary['W_Weight']['fields']['test_system_name_c']['dependency']='';

 ?>