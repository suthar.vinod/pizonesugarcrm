<?php
 // created: 2020-01-07 13:31:08
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['duplicate_merge_dom_value']=0;
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['labelValue']='Current Work Product Assignment';
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['calculated']='true';
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['formula']='related($anml_animals_w_weight_1,"assigned_to_wp_c")';
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['enforced']='true';
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['dependency']='';

 ?>