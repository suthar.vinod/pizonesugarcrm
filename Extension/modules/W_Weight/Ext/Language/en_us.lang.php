<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_ANML_ANIMALS_TITLE'] = 'Test System';
$mod_strings['LBL_W_WEIGHT_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Communication';
$mod_strings['LBL_TEST_SYSTEM_NAME'] = 'Test System Name';
$mod_strings['LBL_CURRENT_WP_ASSIGNMENT'] = 'Current Work Product Assignment';
$mod_strings['LBL_PERC_WT_CHNG'] = '% Weight Change';
$mod_strings['LBL_W_WEIGHT_FOCUS_DRAWER_DASHBOARD'] = 'Weights Focus Drawer';
$mod_strings['LBL_W_WEIGHT_RECORD_DASHBOARD'] = 'Weights Record Dashboard';
$mod_strings['LBL_DATE_2'] = 'Date';
