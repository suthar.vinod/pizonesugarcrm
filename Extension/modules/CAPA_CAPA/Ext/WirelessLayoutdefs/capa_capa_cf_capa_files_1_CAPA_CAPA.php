<?php
 // created: 2022-02-03 07:26:23
$layout_defs["CAPA_CAPA"]["subpanel_setup"]['capa_capa_cf_capa_files_1'] = array (
  'order' => 100,
  'module' => 'CF_CAPA_Files',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CF_CAPA_FILES_TITLE',
  'get_subpanel_data' => 'capa_capa_cf_capa_files_1',
);
