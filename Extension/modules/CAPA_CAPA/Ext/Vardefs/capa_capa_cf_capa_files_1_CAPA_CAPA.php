<?php
// created: 2022-02-03 07:26:23
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_cf_capa_files_1"] = array (
  'name' => 'capa_capa_cf_capa_files_1',
  'type' => 'link',
  'relationship' => 'capa_capa_cf_capa_files_1',
  'source' => 'non-db',
  'module' => 'CF_CAPA_Files',
  'bean_name' => 'CF_CAPA_Files',
  'vname' => 'LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CAPA_CAPA_TITLE',
  'id_name' => 'capa_capa_cf_capa_files_1capa_capa_ida',
  'link-type' => 'many',
  'side' => 'left',
);
