<?php
 // created: 2022-02-03 08:45:34
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['required']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['readonly']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['name']='contact_id_c';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['vname']='LBL_CAPA_OWNER_CONTACT_ID';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['type']='id';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['massupdate']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['hidemassupdate']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['no_default']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['comments']='';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['help']='';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['importable']='true';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['duplicate_merge']='enabled';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['duplicate_merge_dom_value']=1;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['audited']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['reportable']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['unified_search']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['merge_filter']='disabled';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['pii']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['calculated']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['len']=36;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['size']='20';

 ?>