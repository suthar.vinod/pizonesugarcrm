<?php
 // created: 2022-02-03 08:11:14
$dictionary['CAPA_CAPA']['fields']['ca_mgmt_ack_na_c']['labelValue']='CA Affected Management Acknowledgement Not Applicable';
$dictionary['CAPA_CAPA']['fields']['ca_mgmt_ack_na_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['ca_mgmt_ack_na_c']['dependency']='and(equal($ca_andor_pa_dd_c,"CA"),not(equal($risk_assessment,"Minor")))';
$dictionary['CAPA_CAPA']['fields']['ca_mgmt_ack_na_c']['readonly_formula']='';

 ?>