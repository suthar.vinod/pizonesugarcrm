<?php
 // created: 2022-02-03 08:53:34
$dictionary['CAPA_CAPA']['fields']['risk_assessment_score_text_c']['labelValue']='Risk Assessment Score';
$dictionary['CAPA_CAPA']['fields']['risk_assessment_score_text_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CAPA_CAPA']['fields']['risk_assessment_score_text_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['risk_assessment_score_text_c']['dependency']='';
$dictionary['CAPA_CAPA']['fields']['risk_assessment_score_text_c']['required_formula']='';
$dictionary['CAPA_CAPA']['fields']['risk_assessment_score_text_c']['readonly_formula']='';

 ?>