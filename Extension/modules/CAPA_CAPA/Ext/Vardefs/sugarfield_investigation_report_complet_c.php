<?php
 // created: 2022-02-03 08:37:10
$dictionary['CAPA_CAPA']['fields']['investigation_report_complet_c']['labelValue']='Investigation Report Completion Date';
$dictionary['CAPA_CAPA']['fields']['investigation_report_complet_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['investigation_report_complet_c']['dependency']='equal($investigation_report_na_c,false)';
$dictionary['CAPA_CAPA']['fields']['investigation_report_complet_c']['required_formula']='';
$dictionary['CAPA_CAPA']['fields']['investigation_report_complet_c']['readonly_formula']='';

 ?>