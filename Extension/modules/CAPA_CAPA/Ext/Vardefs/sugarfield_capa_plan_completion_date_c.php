<?php
 // created: 2022-02-03 08:40:32
$dictionary['CAPA_CAPA']['fields']['capa_plan_completion_date_c']['labelValue']='CA/PA Plan Completion Date';
$dictionary['CAPA_CAPA']['fields']['capa_plan_completion_date_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['capa_plan_completion_date_c']['dependency']='equal($capa_plan_na_c,false)';
$dictionary['CAPA_CAPA']['fields']['capa_plan_completion_date_c']['required_formula']='';
$dictionary['CAPA_CAPA']['fields']['capa_plan_completion_date_c']['readonly_formula']='';

 ?>