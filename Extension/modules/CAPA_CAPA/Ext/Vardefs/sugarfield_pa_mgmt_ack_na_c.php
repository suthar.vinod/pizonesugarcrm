<?php
 // created: 2022-02-03 08:16:51
$dictionary['CAPA_CAPA']['fields']['pa_mgmt_ack_na_c']['labelValue']='PA Affected Management Acknowledgement Not Applicable';
$dictionary['CAPA_CAPA']['fields']['pa_mgmt_ack_na_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['pa_mgmt_ack_na_c']['dependency']='isInList($ca_andor_pa_dd_c,createList("PA","CA and PA"))';
$dictionary['CAPA_CAPA']['fields']['pa_mgmt_ack_na_c']['readonly_formula']='';

 ?>