<?php
 // created: 2022-02-03 08:46:02
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['required']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['readonly']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['name']='contact_id1_c';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['vname']='LBL_STUDY_DIRECTOR_CONTACT_ID';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['type']='id';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['massupdate']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['hidemassupdate']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['no_default']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['comments']='';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['help']='';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['importable']='true';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['duplicate_merge']='enabled';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['duplicate_merge_dom_value']=1;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['audited']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['reportable']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['unified_search']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['merge_filter']='disabled';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['pii']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['calculated']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['len']=36;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['size']='20';

 ?>