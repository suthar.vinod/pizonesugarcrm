<?php
// created: 2022-02-03 07:32:44
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_capa_capa_1"] = array (
  'name' => 'capa_capa_capa_capa_1',
  'type' => 'link',
  'relationship' => 'capa_capa_capa_capa_1',
  'source' => 'non-db',
  'module' => 'CAPA_CAPA',
  'bean_name' => 'CAPA_CAPA',
  'vname' => 'LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_L_TITLE',
  'id_name' => 'capa_capa_capa_capa_1capa_capa_idb',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_capa_capa_1_right"] = array (
  'name' => 'capa_capa_capa_capa_1_right',
  'type' => 'link',
  'relationship' => 'capa_capa_capa_capa_1',
  'source' => 'non-db',
  'module' => 'CAPA_CAPA',
  'bean_name' => 'CAPA_CAPA',
  'side' => 'right',
  'vname' => 'LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_R_TITLE',
  'id_name' => 'capa_capa_capa_capa_1capa_capa_ida',
  'link-type' => 'one',
);
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_capa_capa_1_name"] = array (
  'name' => 'capa_capa_capa_capa_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_L_TITLE',
  'save' => true,
  'id_name' => 'capa_capa_capa_capa_1capa_capa_ida',
  'link' => 'capa_capa_capa_capa_1_right',
  'table' => 'capa_capa',
  'module' => 'CAPA_CAPA',
  'rname' => 'name',
);
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_capa_capa_1capa_capa_ida"] = array (
  'name' => 'capa_capa_capa_capa_1capa_capa_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_R_TITLE_ID',
  'id_name' => 'capa_capa_capa_capa_1capa_capa_ida',
  'link' => 'capa_capa_capa_capa_1_right',
  'table' => 'capa_capa',
  'module' => 'CAPA_CAPA',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
