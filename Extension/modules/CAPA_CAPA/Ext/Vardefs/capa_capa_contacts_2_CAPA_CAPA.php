<?php
// created: 2022-02-03 07:36:41
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_contacts_2"] = array (
  'name' => 'capa_capa_contacts_2',
  'type' => 'link',
  'relationship' => 'capa_capa_contacts_2',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CAPA_CAPA_CONTACTS_2_FROM_CONTACTS_TITLE',
  'id_name' => 'capa_capa_contacts_2contacts_idb',
);
