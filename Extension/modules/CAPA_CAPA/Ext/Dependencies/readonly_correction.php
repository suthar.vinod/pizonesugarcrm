<?php
 $dependencies['CAPA_CAPA']['readonly_correction'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('correction','ca_andor_pa_dd_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'correction',
                'value' => 'ifElse(equal($ca_andor_pa_dd_c, "PA"),true,false)',
            ),
        ),
    ),
);