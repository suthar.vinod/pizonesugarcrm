<?php
 // created: 2022-02-03 07:30:32
$layout_defs["CAPA_CAPA"]["subpanel_setup"]['capa_capa_m06_error_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAPA_CAPA_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'capa_capa_m06_error_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
