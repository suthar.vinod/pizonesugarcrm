<?php
 // created: 2018-09-13 19:36:12
$dictionary['Product']['fields']['turnaround_time_c']['duplicate_merge_dom_value']=0;
$dictionary['Product']['fields']['turnaround_time_c']['labelValue']='Standard Turnaround Time';
$dictionary['Product']['fields']['turnaround_time_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Product']['fields']['turnaround_time_c']['calculated']='1';
$dictionary['Product']['fields']['turnaround_time_c']['formula']='related($product_templates_link,"turnaround_time_c")';
$dictionary['Product']['fields']['turnaround_time_c']['enforced']='1';
$dictionary['Product']['fields']['turnaround_time_c']['dependency']='';

 ?>