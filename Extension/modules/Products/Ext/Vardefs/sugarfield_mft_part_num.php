<?php
 // created: 2018-09-13 20:01:54
$dictionary['Product']['fields']['mft_part_num']['audited']=false;
$dictionary['Product']['fields']['mft_part_num']['massupdate']=false;
$dictionary['Product']['fields']['mft_part_num']['comments']='Manufacturer part number';
$dictionary['Product']['fields']['mft_part_num']['duplicate_merge']='enabled';
$dictionary['Product']['fields']['mft_part_num']['duplicate_merge_dom_value']='1';
$dictionary['Product']['fields']['mft_part_num']['merge_filter']='disabled';
$dictionary['Product']['fields']['mft_part_num']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.15',
  'searchable' => true,
);
$dictionary['Product']['fields']['mft_part_num']['calculated']=false;

 ?>