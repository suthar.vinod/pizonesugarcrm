<?php

$dependencies['SC_Species_Census']['read_only_field'] = array(
    'hooks' => array("all"),   
    'trigger' => 'true',
    //Optional, the trigger for the dependency. Defaults to 'true'.
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'allocated',
                'value'=>'greaterThan(strlen($id),0)'
            ),
        ),
		array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'on_study',
                'value'=>'greaterThan(strlen($id),0)'
            ),
        ),
		array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'stock',
                'value'=>'greaterThan(strlen($id),0)'
            ),
        ),
		array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'total',
                'value'=>'greaterThan(strlen($id),0)'
            ),
        ),
		array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'date_2',
                'value'=>'greaterThan(strlen($id),0)'
            ),
        ),
		array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 's_species_sc_species_census_1_name',
                'value'=>'greaterThan(strlen($id),0)'
            ),
        ),
		array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'quantity_ytd_c',
                'value'=>'greaterThan(strlen($id),0)'
            ),
        ),
		array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'quantity_received_per_week_c',
                'value'=>'greaterThan(strlen($id),0)'
            ),
        ),
		array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'total_quantity_c',
                'value'=>'greaterThan(strlen($id),0)'
            ),
        ),
		
		array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'la_percent_assigned_2_c',
                'value'=>'or(equal($s_species_sc_species_census_1_name,"Caprine"),equal($s_species_sc_species_census_1_name,"Canine"),equal($s_species_sc_species_census_1_name,"Bovine"),equal($s_species_sc_species_census_1_name,"Ovine"),equal($s_species_sc_species_census_1_name,"Porcine"))'
            ), 
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'sa_percent_assigned_2_c',
                'value'=>'or(equal($s_species_sc_species_census_1_name,"Lagomorph"),equal($s_species_sc_species_census_1_name,"Rat"),equal($s_species_sc_species_census_1_name,"Mouse"),equal($s_species_sc_species_census_1_name,"Hamster"),equal($s_species_sc_species_census_1_name,"Guinea Pig"),equal($s_species_sc_species_census_1_name,"GuineaPg"))'
            ), 
        ),
		array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'la_percent_assigned_2_c',
                'value'=>'greaterThan(strlen($id),0)'
            ),
        ),
		array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'sa_percent_assigned_2_c',
                'value'=>'greaterThan(strlen($id),0)'
            ),
        ),
    ),
); 