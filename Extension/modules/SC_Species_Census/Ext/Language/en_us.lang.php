<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_QUANTITY_YTD'] = 'Quantity YTD';
$mod_strings['LBL_QUANTITY_RECEIVED_PER_WEEK'] = 'Quantity Received Per Week';
$mod_strings['LBL_TOTAL_QUANTITY'] = 'Total Quantity';
$mod_strings['LBL_LA_PERCENT_ASSIGNED_2'] = 'LA % Assigned';
$mod_strings['LBL_SA_PERCENT_ASSIGNED_2'] = 'SA % Assigned';
$mod_strings['LBL_SC_SPECIES_CENSUS_RECORD_DASHBOARD'] = 'Species Census Record Dashboard';
