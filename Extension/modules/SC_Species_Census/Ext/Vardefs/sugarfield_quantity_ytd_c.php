<?php
 // created: 2021-05-13 08:04:28
$dictionary['SC_Species_Census']['fields']['quantity_ytd_c']['labelValue']='Quantity YTD';
$dictionary['SC_Species_Census']['fields']['quantity_ytd_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['SC_Species_Census']['fields']['quantity_ytd_c']['enforced']='';
$dictionary['SC_Species_Census']['fields']['quantity_ytd_c']['dependency']='';
$dictionary['SC_Species_Census']['fields']['quantity_ytd_c']['required_formula']='';
$dictionary['SC_Species_Census']['fields']['quantity_ytd_c']['readonly_formula']='';

 ?>