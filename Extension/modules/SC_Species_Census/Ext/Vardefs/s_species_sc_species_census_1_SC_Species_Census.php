<?php
// created: 2021-04-29 06:58:32
$dictionary["SC_Species_Census"]["fields"]["s_species_sc_species_census_1"] = array (
  'name' => 's_species_sc_species_census_1',
  'type' => 'link',
  'relationship' => 's_species_sc_species_census_1',
  'source' => 'non-db',
  'module' => 'S_Species',
  'bean_name' => 'S_Species',
  'side' => 'right',
  'vname' => 'LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_SC_SPECIES_CENSUS_TITLE',
  'id_name' => 's_species_sc_species_census_1s_species_ida',
  'link-type' => 'one',
);
$dictionary["SC_Species_Census"]["fields"]["s_species_sc_species_census_1_name"] = array (
  'name' => 's_species_sc_species_census_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_S_SPECIES_TITLE',
  'save' => true,
  'id_name' => 's_species_sc_species_census_1s_species_ida',
  'link' => 's_species_sc_species_census_1',
  'table' => 's_species',
  'module' => 'S_Species',
  'rname' => 'name',
);
$dictionary["SC_Species_Census"]["fields"]["s_species_sc_species_census_1s_species_ida"] = array (
  'name' => 's_species_sc_species_census_1s_species_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_SC_SPECIES_CENSUS_TITLE_ID',
  'id_name' => 's_species_sc_species_census_1s_species_ida',
  'link' => 's_species_sc_species_census_1',
  'table' => 's_species',
  'module' => 'S_Species',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
