<?php
 // created: 2021-05-13 08:05:58
$dictionary['SC_Species_Census']['fields']['quantity_received_per_week_c']['labelValue']='Quantity Received Per Week';
$dictionary['SC_Species_Census']['fields']['quantity_received_per_week_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['SC_Species_Census']['fields']['quantity_received_per_week_c']['enforced']='';
$dictionary['SC_Species_Census']['fields']['quantity_received_per_week_c']['dependency']='';
$dictionary['SC_Species_Census']['fields']['quantity_received_per_week_c']['required_formula']='';
$dictionary['SC_Species_Census']['fields']['quantity_received_per_week_c']['readonly_formula']='';

 ?>