<?php
 // created: 2021-05-13 08:07:37
$dictionary['SC_Species_Census']['fields']['total_quantity_c']['labelValue']='Total Quantity';
$dictionary['SC_Species_Census']['fields']['total_quantity_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['SC_Species_Census']['fields']['total_quantity_c']['enforced']='';
$dictionary['SC_Species_Census']['fields']['total_quantity_c']['dependency']='';
$dictionary['SC_Species_Census']['fields']['total_quantity_c']['required_formula']='';
$dictionary['SC_Species_Census']['fields']['total_quantity_c']['readonly_formula']='';

 ?>