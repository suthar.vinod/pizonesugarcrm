<?php
 // created: 2021-04-29 07:12:22
$dictionary['SC_Species_Census']['fields']['name']['importable']='false';
$dictionary['SC_Species_Census']['fields']['name']['duplicate_merge']='disabled';
$dictionary['SC_Species_Census']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['SC_Species_Census']['fields']['name']['merge_filter']='disabled';
$dictionary['SC_Species_Census']['fields']['name']['unified_search']=false;
$dictionary['SC_Species_Census']['fields']['name']['calculated']='true';
$dictionary['SC_Species_Census']['fields']['name']['formula']='concat(related($s_species_sc_species_census_1,"name")," Census ",toString($date_2))';
$dictionary['SC_Species_Census']['fields']['name']['enforced']=true;

 ?>