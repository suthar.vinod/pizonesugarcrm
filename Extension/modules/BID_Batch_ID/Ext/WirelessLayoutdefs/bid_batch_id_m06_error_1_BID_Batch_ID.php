<?php
 // created: 2022-04-26 07:03:59
$layout_defs["BID_Batch_ID"]["subpanel_setup"]['bid_batch_id_m06_error_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_BID_BATCH_ID_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'bid_batch_id_m06_error_1',
);
