<?php
$dependencies['BID_Batch_ID']['setReadOnly_field43'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('name', 'id', 'status_readonly_c'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'name',
                'value'  => 'true',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'status_c',
                'value'  => 'ifElse(and(not(equal($id,"")),not(equal($status_readonly_c,"0"))),true,false)',
            ),
        ),
    ),
);
