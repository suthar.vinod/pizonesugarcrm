<?php
// created: 2022-09-22 05:31:46
$viewdefs['BID_Batch_ID']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_BID_BATCH_ID_TASKS_1_FROM_TASKS_TITLE',
  'context' => 
  array (
    'link' => 'bid_batch_id_tasks_1',
  ),
);