<?php
// created: 2022-04-26 06:50:11
$viewdefs['BID_Batch_ID']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'context' => 
  array (
    'link' => 'bid_batch_id_wpe_work_product_enrollment_1',
  ),
);