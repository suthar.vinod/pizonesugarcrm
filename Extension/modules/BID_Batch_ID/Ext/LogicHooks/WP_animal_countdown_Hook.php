<?php
	$hook_array['after_relationship_delete'][] = Array(
      1012, 
      'Update Animal Countdown in Work Product After Relationship Delete', 
      'custom/modules/BID_Batch_ID/BID_WP_animal_countdown_afterRelationDeleteHook.php', 
      'BID_animalCountAfterRelationDeleteHook', 
      'UpdateAnimalCountdownValues'
   );
   
   $hook_array['after_relationship_add'][] = Array(
      1011, 
      'Update Animal Countdown in Work Product After relation add', 
      'custom/modules/BID_Batch_ID/BID_WP_animal_countdown_Hook.php', 
      'BID_animalCountAfterRelationAddHook', 
      'UpdateAnimalCountdownValues'
   );   
?>