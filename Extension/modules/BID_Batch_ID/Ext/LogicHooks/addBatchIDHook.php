<?php
$hook_version = 1;
$hook_array['before_save'][] = array(
     1,
    'Auto Generate Name',
    'custom/modules/BID_Batch_ID/nameGenerationHookBID.php',
    'HookBeforeSaveForNameGenerationBID',
    'generateNameBID'
);
$hook_array['after_save'][] = array(
    6,
   'Set the Schedule date to the Linked work Product',
   'custom/modules/BID_Batch_ID/setScheduleDateToFirstProcedureWPHook.php',
   'setScheduleDateToFirstProcedureWPHook',
   'setScheduleDate'
);