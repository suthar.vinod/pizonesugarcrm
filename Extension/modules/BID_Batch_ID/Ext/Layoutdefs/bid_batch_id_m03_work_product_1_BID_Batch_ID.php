<?php
 // created: 2022-04-26 06:44:22
$layout_defs["BID_Batch_ID"]["subpanel_setup"]['bid_batch_id_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'bid_batch_id_m03_work_product_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
