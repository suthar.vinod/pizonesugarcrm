<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SCHEDULED_DATE'] = 'Scheduled Date';
$mod_strings['LBL_STATUS'] = 'Status';
$mod_strings['LBL_EXTRACT_DURATION_HOURS'] = 'Extract Condition (Hours)';
$mod_strings['LBL_CONTROL_QUANTITY'] = 'Control Quantity';
$mod_strings['LBL_IN_LIFE_DURATION'] = 'In-life Duration (days)';
$mod_strings['LBL_DOSE_ROUTE'] = 'Dose Route';
$mod_strings['LBL_OTHER_DOSE_ROUTE'] = 'Other Dose Route';
$mod_strings['LBL_STATUS_READONLY'] = 'Status Readonly';
$mod_strings['LBL_DESCRIPTION'] = 'Notes';
$mod_strings['LBL_EXTRACT_CONDITION_TEMP'] = 'Extract Condition (Temperature)';
$mod_strings['LBL_GD_COMPLETE'] = 'GD Complete';
