<?php
// created: 2022-04-26 06:50:11
$dictionary["BID_Batch_ID"]["fields"]["bid_batch_id_wpe_work_product_enrollment_1"] = array (
  'name' => 'bid_batch_id_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_BID_BATCH_ID_TITLE',
  'id_name' => 'bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida',
  'link-type' => 'many',
  'side' => 'left',
);
