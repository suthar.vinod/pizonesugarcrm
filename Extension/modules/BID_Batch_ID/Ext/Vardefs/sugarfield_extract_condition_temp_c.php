<?php
 // created: 2022-05-05 09:11:14
$dictionary['BID_Batch_ID']['fields']['extract_condition_temp_c']['labelValue']='Extract Condition (Temperature)';
$dictionary['BID_Batch_ID']['fields']['extract_condition_temp_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['extract_condition_temp_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['extract_condition_temp_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['extract_condition_temp_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['extract_condition_temp_c']['readonly_formula']='';

 ?>