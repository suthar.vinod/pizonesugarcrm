<?php
 // created: 2022-09-22 05:39:56
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['duplicate_merge_dom_value']=0;
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['labelValue']='GD Complete';
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['calculated']='true';
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['formula']='rollupSum($bid_batch_id_gd_group_design_1,"complete_yes_no_c")';
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['enforced']='true';
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['readonly_formula']='';

 ?>