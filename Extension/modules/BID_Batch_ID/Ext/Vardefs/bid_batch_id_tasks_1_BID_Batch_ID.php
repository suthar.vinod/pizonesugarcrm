<?php
// created: 2022-09-22 05:31:46
$dictionary["BID_Batch_ID"]["fields"]["bid_batch_id_tasks_1"] = array (
  'name' => 'bid_batch_id_tasks_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_BID_BATCH_ID_TASKS_1_FROM_BID_BATCH_ID_TITLE',
  'id_name' => 'bid_batch_id_tasks_1bid_batch_id_ida',
  'link-type' => 'many',
  'side' => 'left',
);
