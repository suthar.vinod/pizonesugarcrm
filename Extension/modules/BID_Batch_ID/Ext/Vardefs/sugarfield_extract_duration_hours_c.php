<?php
 // created: 2022-05-05 09:08:56
$dictionary['BID_Batch_ID']['fields']['extract_duration_hours_c']['labelValue']='Extract Condition (Hours)';
$dictionary['BID_Batch_ID']['fields']['extract_duration_hours_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['extract_duration_hours_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['extract_duration_hours_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['extract_duration_hours_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['extract_duration_hours_c']['readonly_formula']='';

 ?>