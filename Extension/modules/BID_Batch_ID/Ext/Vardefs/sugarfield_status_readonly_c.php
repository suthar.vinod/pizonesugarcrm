<?php
 // created: 2022-04-26 07:20:58
$dictionary['BID_Batch_ID']['fields']['status_readonly_c']['labelValue']='Status Readonly';
$dictionary['BID_Batch_ID']['fields']['status_readonly_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['status_readonly_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['status_readonly_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['status_readonly_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['status_readonly_c']['readonly_formula']='';

 ?>