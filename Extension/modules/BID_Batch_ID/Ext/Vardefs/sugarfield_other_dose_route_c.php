<?php
 // created: 2022-04-26 07:19:39
$dictionary['BID_Batch_ID']['fields']['other_dose_route_c']['labelValue']='Other Dose Route';
$dictionary['BID_Batch_ID']['fields']['other_dose_route_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['other_dose_route_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['other_dose_route_c']['dependency']='equal($dose_route_c,"Other")';
$dictionary['BID_Batch_ID']['fields']['other_dose_route_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['other_dose_route_c']['readonly_formula']='';

 ?>