<?php
 // created: 2022-04-26 11:39:24
$dictionary['BID_Batch_ID']['fields']['description']['audited']=true;
$dictionary['BID_Batch_ID']['fields']['description']['massupdate']=false;
$dictionary['BID_Batch_ID']['fields']['description']['hidemassupdate']=false;
$dictionary['BID_Batch_ID']['fields']['description']['comments']='Full text of the note';
$dictionary['BID_Batch_ID']['fields']['description']['duplicate_merge']='enabled';
$dictionary['BID_Batch_ID']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['BID_Batch_ID']['fields']['description']['merge_filter']='disabled';
$dictionary['BID_Batch_ID']['fields']['description']['unified_search']=false;
$dictionary['BID_Batch_ID']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['BID_Batch_ID']['fields']['description']['calculated']=false;
$dictionary['BID_Batch_ID']['fields']['description']['rows']='6';
$dictionary['BID_Batch_ID']['fields']['description']['cols']='80';

 ?>