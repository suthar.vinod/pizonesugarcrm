<?php
 // created: 2022-04-26 07:13:57
$dictionary['BID_Batch_ID']['fields']['control_quantity_c']['labelValue']='Control Quantity';
$dictionary['BID_Batch_ID']['fields']['control_quantity_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['control_quantity_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['control_quantity_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['control_quantity_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['control_quantity_c']['readonly_formula']='';

 ?>