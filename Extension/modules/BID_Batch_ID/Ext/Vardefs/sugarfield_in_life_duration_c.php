<?php
 // created: 2022-04-26 07:15:18
$dictionary['BID_Batch_ID']['fields']['in_life_duration_c']['labelValue']='In-life Duration (days)';
$dictionary['BID_Batch_ID']['fields']['in_life_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['in_life_duration_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['in_life_duration_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['in_life_duration_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['in_life_duration_c']['readonly_formula']='';

 ?>