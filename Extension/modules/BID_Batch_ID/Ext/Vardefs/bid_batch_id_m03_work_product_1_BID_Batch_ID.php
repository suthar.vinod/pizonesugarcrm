<?php
// created: 2022-04-26 06:44:22
$dictionary["BID_Batch_ID"]["fields"]["bid_batch_id_m03_work_product_1"] = array (
  'name' => 'bid_batch_id_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_BID_BATCH_ID_TITLE',
  'id_name' => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
  'link-type' => 'many',
  'side' => 'left',
);
