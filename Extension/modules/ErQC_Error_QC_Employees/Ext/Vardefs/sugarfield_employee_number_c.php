<?php
 // created: 2018-06-01 21:13:36
$dictionary['ErQC_Error_QC_Employees']['fields']['employee_number_c']['labelValue']='Employee Number';
$dictionary['ErQC_Error_QC_Employees']['fields']['employee_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ErQC_Error_QC_Employees']['fields']['employee_number_c']['enforced']='';
$dictionary['ErQC_Error_QC_Employees']['fields']['employee_number_c']['dependency']='';

 ?>