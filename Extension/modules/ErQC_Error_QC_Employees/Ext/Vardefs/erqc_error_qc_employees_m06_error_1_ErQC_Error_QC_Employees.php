<?php
// created: 2018-06-01 21:24:07
$dictionary["ErQC_Error_QC_Employees"]["fields"]["erqc_error_qc_employees_m06_error_1"] = array (
  'name' => 'erqc_error_qc_employees_m06_error_1',
  'type' => 'link',
  'relationship' => 'erqc_error_qc_employees_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'erqc_error_qc_employees_m06_error_1m06_error_idb',
);
