<?php

class AutGenerateIdLogicHook {

    function autoGenearteId($bean, $event, $arguments) {
        $tableName = 'abc12_work_product_activities';
        $idFieldName = 'time_keeping_id';
        global $db;
        $year = date("y"); ///get the year in 2 digit
        //  Read Lock
        $sql_lock = "Lock TABLES {$tableName} READ";
        $bean->db->query($sql_lock);
        //  Get Latest Id
        $query = "SELECT substring({$idFieldName},-5) AS {$idFieldName} FROM {$tableName} "
                . "WHERE {$idFieldName} LIKE '%TK" . $year . "%-%' "
                . "ORDER BY {$idFieldName} DESC LIMIT 0,1";
        $result = $db->query($query);
        //  Unlock Table
        $sql_unlock = "UNLOCK TABLES";
        $bean->db->query($sql_unlock);

        if ($result->num_rows > 0) {
            $row = $db->fetchByAssoc($result);
            $system_id = $row[$idFieldName];
            if (!empty($system_id)) {
                #$number = (int) substr($system_id, -4);    
                $number = (int) $system_id;
                $number = $number + 1;
                $length = strlen($number);
                $allowed_length = 5;
                $left_length = $allowed_length - $length;
                $code = "TK";

                $code .= $year;
                $code .= '-';

                for ($i = 0; $i < $left_length; $i++) {
                    $code .= "0";
                }
                $code .= $number;
            } else {
                $code = "TK" . $year . "-00001";
            }
        } else {
            $code = "TK" . $year . "-00001";
        }

        if ($bean->$idFieldName == "") {
            $bean->$idFieldName = $code;
        }
    }

}
