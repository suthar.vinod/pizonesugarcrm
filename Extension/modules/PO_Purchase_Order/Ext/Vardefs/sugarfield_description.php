<?php
 // created: 2022-01-11 06:18:58
$dictionary['PO_Purchase_Order']['fields']['description']['audited']=true;
$dictionary['PO_Purchase_Order']['fields']['description']['massupdate']=false;
$dictionary['PO_Purchase_Order']['fields']['description']['hidemassupdate']=false;
$dictionary['PO_Purchase_Order']['fields']['description']['comments']='Full text of the note';
$dictionary['PO_Purchase_Order']['fields']['description']['duplicate_merge']='enabled';
$dictionary['PO_Purchase_Order']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['PO_Purchase_Order']['fields']['description']['merge_filter']='disabled';
$dictionary['PO_Purchase_Order']['fields']['description']['unified_search']=false;
$dictionary['PO_Purchase_Order']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['PO_Purchase_Order']['fields']['description']['calculated']=false;
$dictionary['PO_Purchase_Order']['fields']['description']['rows']='6';
$dictionary['PO_Purchase_Order']['fields']['description']['cols']='80';

 ?>