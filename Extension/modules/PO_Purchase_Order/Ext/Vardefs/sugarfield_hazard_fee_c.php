<?php
 // created: 2021-04-22 13:51:28
$dictionary['PO_Purchase_Order']['fields']['hazard_fee_c']['labelValue']='Hazard Fee';
$dictionary['PO_Purchase_Order']['fields']['hazard_fee_c']['enforced']='';
$dictionary['PO_Purchase_Order']['fields']['hazard_fee_c']['dependency']='';
$dictionary['PO_Purchase_Order']['fields']['hazard_fee_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['PO_Purchase_Order']['fields']['hazard_fee_c']['required_formula']='';
$dictionary['PO_Purchase_Order']['fields']['hazard_fee_c']['readonly_formula']='';

 ?>