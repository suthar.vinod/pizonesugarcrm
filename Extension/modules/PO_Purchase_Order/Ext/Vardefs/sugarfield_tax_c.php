<?php
 // created: 2021-04-22 13:50:39
$dictionary['PO_Purchase_Order']['fields']['tax_c']['labelValue']='Tax';
$dictionary['PO_Purchase_Order']['fields']['tax_c']['enforced']='';
$dictionary['PO_Purchase_Order']['fields']['tax_c']['dependency']='';
$dictionary['PO_Purchase_Order']['fields']['tax_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['PO_Purchase_Order']['fields']['tax_c']['required_formula']='';
$dictionary['PO_Purchase_Order']['fields']['tax_c']['readonly_formula']='';

 ?>