<?php
 // created: 2021-07-30 04:58:50
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['required']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['name']='shipping_cost';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['vname']='LBL_SHIPPING_COST';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['type']='currency';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['massupdate']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['no_default']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['comments']='';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['help']='';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['importable']='true';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['duplicate_merge']='enabled';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['duplicate_merge_dom_value']='1';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['audited']=true;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['reportable']=true;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['unified_search']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['merge_filter']='disabled';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['pii']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['default']='';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['calculated']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['len']=26;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['size']='20';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['enable_range_search']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['precision']=6;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['hidemassupdate']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['convertToBase']=true;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['showTransactionalAmount']=true;

 ?>