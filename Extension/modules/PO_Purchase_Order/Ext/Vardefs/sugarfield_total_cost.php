<?php
 // created: 2021-08-11 11:13:23
$dictionary['PO_Purchase_Order']['fields']['total_cost']['required']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['name']='total_cost';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['vname']='LBL_TOTAL_COST';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['type']='currency';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['massupdate']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['no_default']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['comments']='';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['help']='';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['importable']='false';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['duplicate_merge']='disabled';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['duplicate_merge_dom_value']=0;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['audited']=true;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['reportable']=true;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['unified_search']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['merge_filter']='disabled';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['pii']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['calculated']='1';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['len']=26;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['size']='20';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['enable_range_search']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['precision']=6;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['PO_Purchase_Order']['fields']['total_cost']['hidemassupdate']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['convertToBase']=true;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['showTransactionalAmount']=true;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['formula']='add($hazard_fee_c,$tax_c,$shipping_cost,rollupSum($po_purchase_order_poi_purchase_order_item_1,"purchaserequesttotal_c"),rollupSum($po_purchase_order_ori_order_request_item_1,"orderrequesttotal_c"))';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['enforced']=true;

 ?>