<?php
 // created: 2022-04-14 06:54:19
$dictionary['PO_Purchase_Order']['fields']['po_comments_c']['labelValue']='PO Comments';
$dictionary['PO_Purchase_Order']['fields']['po_comments_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['PO_Purchase_Order']['fields']['po_comments_c']['enforced']='';
$dictionary['PO_Purchase_Order']['fields']['po_comments_c']['dependency']='';
$dictionary['PO_Purchase_Order']['fields']['po_comments_c']['required_formula']='';
$dictionary['PO_Purchase_Order']['fields']['po_comments_c']['readonly_formula']='';

 ?>