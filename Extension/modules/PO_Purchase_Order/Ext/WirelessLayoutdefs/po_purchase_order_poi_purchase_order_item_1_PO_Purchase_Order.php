<?php
 // created: 2021-10-19 09:29:00
$layout_defs["PO_Purchase_Order"]["subpanel_setup"]['po_purchase_order_poi_purchase_order_item_1'] = array (
  'order' => 100,
  'module' => 'POI_Purchase_Order_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'get_subpanel_data' => 'po_purchase_order_poi_purchase_order_item_1',
);
