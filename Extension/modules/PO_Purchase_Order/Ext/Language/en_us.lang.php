<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NUMBER_2_C_AN_ACCOUNT_NUMBER_ID'] = 'Account Number (related Account Number ID)';
$mod_strings['LBL_ACCOUNT_NUMBER_2'] = 'Account Number';
$mod_strings['LBL_STATUS'] = 'Status';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_TAX'] = 'Tax';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_HAZARD_FEE'] = 'Hazard Fee';
$mod_strings['LBL_ACCOUNT_NUMBER'] = 'Account # (Obsolete)';
$mod_strings['LBL_DESCRIPTION'] = 'Notes';
$mod_strings['LBL_PO_COMMENTS'] = 'PO Comments';
