<?php 
/*#1778 :  read only Complete if all of the ORIs and POIs linked to the PO have Status = Fully Received.  */
$dependencies['PO_Purchase_Order']['read_only_status'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('status_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status_c',
                'value' => 'equal($status_c,"Complete")',
            ),
        ),
    ),
);
?>