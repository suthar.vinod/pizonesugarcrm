<?php
$dependencies['PO_Purchase_Order']['account_number_empty01'] = array(
    'hooks' => array("all"),
    'triggerFields' => array('vendor'),
    'onload' => false,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => "an_account_number_id_c",
                'value' => ''
            )
        ),
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => "account_number_2_c",
                'value' => ''
            )
        ),
        
    ),
);


