<?php
 // created: 2021-06-29 08:53:22
$dictionary['Erd_Error_Documents']['fields']['average_cd_pages_per_basis_c']['labelValue']='Average CD Pages Per Basis';
$dictionary['Erd_Error_Documents']['fields']['average_cd_pages_per_basis_c']['enforced']='';
$dictionary['Erd_Error_Documents']['fields']['average_cd_pages_per_basis_c']['dependency']='isInList($deviation_rate_basis_c,createList("Animal Days","Procedures","Studies"))';
$dictionary['Erd_Error_Documents']['fields']['average_cd_pages_per_basis_c']['required_formula']='';
$dictionary['Erd_Error_Documents']['fields']['average_cd_pages_per_basis_c']['readonly_formula']='';

 ?>