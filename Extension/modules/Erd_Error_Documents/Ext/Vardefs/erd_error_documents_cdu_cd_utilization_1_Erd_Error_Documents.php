<?php
// created: 2021-06-29 08:02:02
$dictionary["Erd_Error_Documents"]["fields"]["erd_error_documents_cdu_cd_utilization_1"] = array (
  'name' => 'erd_error_documents_cdu_cd_utilization_1',
  'type' => 'link',
  'relationship' => 'erd_error_documents_cdu_cd_utilization_1',
  'source' => 'non-db',
  'module' => 'CDU_CD_Utilization',
  'bean_name' => 'CDU_CD_Utilization',
  'vname' => 'LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'id_name' => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
  'link-type' => 'many',
  'side' => 'left',
);
