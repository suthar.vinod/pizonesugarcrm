<?php
 // created: 2019-05-23 17:03:13
$dictionary['Erd_Error_Documents']['fields']['name']['len']='255';
$dictionary['Erd_Error_Documents']['fields']['name']['audited']=true;
$dictionary['Erd_Error_Documents']['fields']['name']['massupdate']=false;
$dictionary['Erd_Error_Documents']['fields']['name']['unified_search']=false;
$dictionary['Erd_Error_Documents']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Erd_Error_Documents']['fields']['name']['calculated']=false;

 ?>