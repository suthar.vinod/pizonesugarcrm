<?php
 // created: 2021-06-29 08:50:28
$dictionary['Erd_Error_Documents']['fields']['data_cells_per_cd_c']['labelValue']='Data Cells Per CD';
$dictionary['Erd_Error_Documents']['fields']['data_cells_per_cd_c']['enforced']='false';
$dictionary['Erd_Error_Documents']['fields']['data_cells_per_cd_c']['dependency']='isInList($deviation_rate_basis_c,createList("Animal Days","Procedures","Studies"))';
$dictionary['Erd_Error_Documents']['fields']['data_cells_per_cd_c']['required_formula']='';
$dictionary['Erd_Error_Documents']['fields']['data_cells_per_cd_c']['readonly_formula']='';

 ?>