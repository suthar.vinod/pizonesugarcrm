<?php
// created: 2018-01-31 21:10:46
$dictionary["Erd_Error_Documents"]["fields"]["m06_error_erd_error_documents_1"] = array (
  'name' => 'm06_error_erd_error_documents_1',
  'type' => 'link',
  'relationship' => 'm06_error_erd_error_documents_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_erd_error_documents_1m06_error_ida',
);
