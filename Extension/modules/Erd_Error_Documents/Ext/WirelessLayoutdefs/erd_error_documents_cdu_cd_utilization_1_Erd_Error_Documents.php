<?php
 // created: 2021-06-29 08:02:02
$layout_defs["Erd_Error_Documents"]["subpanel_setup"]['erd_error_documents_cdu_cd_utilization_1'] = array (
  'order' => 100,
  'module' => 'CDU_CD_Utilization',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_CDU_CD_UTILIZATION_TITLE',
  'get_subpanel_data' => 'erd_error_documents_cdu_cd_utilization_1',
);
