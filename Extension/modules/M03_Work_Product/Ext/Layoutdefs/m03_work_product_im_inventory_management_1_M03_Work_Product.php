<?php
 // created: 2021-11-09 10:14:52
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_im_inventory_management_1'] = array (
  'order' => 100,
  'module' => 'IM_Inventory_Management',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'get_subpanel_data' => 'm03_work_product_im_inventory_management_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
