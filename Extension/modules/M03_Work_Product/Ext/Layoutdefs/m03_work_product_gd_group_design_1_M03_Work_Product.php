<?php
 // created: 2021-08-14 05:59:50
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_gd_group_design_1'] = array (
  'order' => 100,
  'module' => 'GD_Group_Design',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'get_subpanel_data' => 'm03_work_product_gd_group_design_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
