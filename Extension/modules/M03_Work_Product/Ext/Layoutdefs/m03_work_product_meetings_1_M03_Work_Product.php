<?php
 // created: 2017-05-02 19:55:40
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_meetings_1'] = array (
  'order' => 100,
  'module' => 'Meetings',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE',
  'get_subpanel_data' => 'm03_work_product_meetings_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
