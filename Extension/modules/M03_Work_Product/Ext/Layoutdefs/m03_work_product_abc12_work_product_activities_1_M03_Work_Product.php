<?php
 // created: 2017-02-17 17:50:46
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_abc12_work_product_activities_1'] = array (
  'order' => 100,
  'module' => 'ABC12_Work_Product_Activities',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_ABC12_WORK_PRODUCT_ACTIVITIES_TITLE',
  'get_subpanel_data' => 'm03_work_product_abc12_work_product_activities_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
