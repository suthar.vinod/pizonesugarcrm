<?php
 // created: 2016-02-25 20:01:03
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'm03_work_product_tasks_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
  ),
);
