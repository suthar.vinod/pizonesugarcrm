<?php
 // created: 2021-02-11 08:58:28
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_tsd1_test_system_design_1_1'] = array (
  'order' => 100,
  'module' => 'TSD1_Test_System_Design_1',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_TSD1_TEST_SYSTEM_DESIGN_1_TITLE',
  'get_subpanel_data' => 'm03_work_product_tsd1_test_system_design_1_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
