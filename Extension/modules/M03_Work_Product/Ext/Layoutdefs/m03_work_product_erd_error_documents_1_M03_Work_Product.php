<?php
 // created: 2019-11-05 12:53:29
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_erd_error_documents_1'] = array (
  'order' => 100,
  'module' => 'Erd_Error_Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'm03_work_product_erd_error_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
