<?php
 // created: 2018-10-09 16:44:23
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_sw_study_workflow_1'] = array (
  'order' => 100,
  'module' => 'SW_Study_Workflow',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_SW_STUDY_WORKFLOW_TITLE',
  'get_subpanel_data' => 'm03_work_product_sw_study_workflow_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
