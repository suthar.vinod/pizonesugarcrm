<?php
 // created: 2018-01-31 15:57:03
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m06_error_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'm06_error_m03_work_product_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
