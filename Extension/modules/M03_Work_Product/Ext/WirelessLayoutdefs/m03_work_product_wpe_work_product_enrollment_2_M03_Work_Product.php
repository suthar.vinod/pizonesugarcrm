<?php
 // created: 2020-12-15 06:35:00
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_wpe_work_product_enrollment_2'] = array (
  'order' => 100,
  'module' => 'WPE_Work_Product_Enrollment',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'get_subpanel_data' => 'm03_work_product_wpe_work_product_enrollment_2',
);
