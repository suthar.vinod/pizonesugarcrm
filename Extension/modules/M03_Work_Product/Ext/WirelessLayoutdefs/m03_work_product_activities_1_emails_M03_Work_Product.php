<?php
 // created: 2019-08-05 12:04:52
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_activities_1_emails'] = array (
  'order' => 100,
  'module' => 'Emails',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_EMAILS_FROM_EMAILS_TITLE',
  'get_subpanel_data' => 'm03_work_product_activities_1_emails',
);
