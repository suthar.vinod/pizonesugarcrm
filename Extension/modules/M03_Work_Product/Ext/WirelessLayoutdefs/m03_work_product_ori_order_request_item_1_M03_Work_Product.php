<?php
 // created: 2021-10-19 10:30:05
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_ori_order_request_item_1'] = array (
  'order' => 100,
  'module' => 'ORI_Order_Request_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'get_subpanel_data' => 'm03_work_product_ori_order_request_item_1',
);
