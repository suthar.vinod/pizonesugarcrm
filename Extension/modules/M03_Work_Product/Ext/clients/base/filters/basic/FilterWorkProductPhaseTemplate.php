<?php
$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplate',
    'name' => 'Filter Work Product Phase',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'name' => array(
                '$not_equals' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);
