<?php
$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateNotAH01',
    'name' => 'Filter Work Product Phase 2',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);
