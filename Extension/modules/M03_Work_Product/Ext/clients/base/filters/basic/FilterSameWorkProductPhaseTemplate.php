<?php
$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterSameWorkProductPhaseTemplate',
    'name' => 'Filter Same Work Product',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'name' => array(
                '$equals' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);