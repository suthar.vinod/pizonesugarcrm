<?php
$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateTestSystemAPS',
    'name' => 'Filter Work Product Phase Test System APS',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
            'accounts_m03_work_product_1_name' => array(
                '$in' => '',
            ),
        ),
       
    ),
    'editable' => true,
    'is_template' => true,
);