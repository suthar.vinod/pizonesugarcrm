<?php
$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateLastNotAH01_OnStudy_In_AH15',
    'name' => 'Filter Work Product Phase NotAH01_OnStudy_In_AH15',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'name' => array(
                '$equals' => 'APS177-AH15',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);
