<?php
$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateTestSystem',
    'name' => 'Filter Work Product Phase Test System',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
            'accounts_m03_work_product_1_name' => array(
                '$in' => '',
            ),
            'name' => array(
                '$not_equals' => 'APS177-AH15',
            ),
            
        ),
       
    ),
    'editable' => true,
    'is_template' => true,
);