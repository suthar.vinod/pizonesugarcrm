<?php
$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductCodeTemplate',
    'name' => 'Filter Work Product Code',
    'filter_definition' => array(
        array(
            'm03_work_product_code_id1_c' => array(
                '$in' => array('0527341e-d0d4-11e9-8661-06eddc549468','9634c914-fe07-11e6-a663-02feb3423f0d','5e3a105c-1fa3-11eb-9b32-02fb813964b8'),
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);

