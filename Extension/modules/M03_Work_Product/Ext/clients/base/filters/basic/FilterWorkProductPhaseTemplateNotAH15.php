<?php
$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateNotAH15',
    'name' => 'Filter Work Product Phase NotAH15',
    'filter_definition' => array(
        array(
            'work_product_code_2_c' => array(
              '$in' => array('426d29a9-0810-54b0-1834-5788f6505070','c81c2577-bd7c-16e3-1248-5785ac111a73','f6b39288-96f1-11e7-b45e-02feb3423f0d','00535bde-96f2-11e7-a240-02feb3423f0d'),
            ),
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'name' => array(
                '$not_equals' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);
