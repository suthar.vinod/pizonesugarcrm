<?php
 // created: 2020-04-27 18:21:42
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_nong_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_nong_c']['labelValue']='Data Retention End Date (nonGLP)';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_nong_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_nong_c']['formula']='addDays($data_retention_start_date_c,182)';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_nong_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_nong_c']['dependency']='isInList($work_product_compliance_c,createList("nonGLP","Not Applicable","GLP Not Performed","GLP Discontinued","Withdrawn","GLP_Amended_NonGLP","NonGLP Structured","NonGLP Discovery"))';

 ?>