<?php
// created: 2017-06-07 19:01:24
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_a1a_critical_phase_inspectio_1"] = array (
  'name' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'vname' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_p33edproduct_ida',
  'link-type' => 'many',
  'side' => 'left',
);
