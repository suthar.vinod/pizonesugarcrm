<?php
 // created: 2022-07-07 12:01:05
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['name']='contacts_m03_work_product_2contacts_ida';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['type']='id';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['source']='non-db';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['vname']='LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE_ID';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['id_name']='contacts_m03_work_product_2contacts_ida';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['link']='contacts_m03_work_product_2';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['table']='contacts';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['module']='Contacts';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['rname']='id';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['reportable']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['side']='right';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['massupdate']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['duplicate_merge']='disabled';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['hideacl']=true;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['audited']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['importable']='true';

 ?>