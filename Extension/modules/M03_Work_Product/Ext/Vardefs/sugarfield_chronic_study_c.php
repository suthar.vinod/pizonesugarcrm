<?php
 // created: 2021-12-07 12:50:01
$dictionary['M03_Work_Product']['fields']['chronic_study_c']['labelValue']='Chronic Study Longer than 28 Days?';
$dictionary['M03_Work_Product']['fields']['chronic_study_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['chronic_study_c']['required_formula']='not(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No"))';
$dictionary['M03_Work_Product']['fields']['chronic_study_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['chronic_study_c']['visibility_grid']='';

 ?>