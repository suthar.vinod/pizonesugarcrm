<?php
 // created: 2021-11-18 10:20:13
$dictionary['M03_Work_Product']['fields']['aaalac_and_usda_exemptions_c']['labelValue']='AAALAC and USDA Exemptions';
$dictionary['M03_Work_Product']['fields']['aaalac_and_usda_exemptions_c']['dependency']='equal($no_exemptions_c,false)';
$dictionary['M03_Work_Product']['fields']['aaalac_and_usda_exemptions_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['aaalac_and_usda_exemptions_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['aaalac_and_usda_exemptions_c']['visibility_grid']='';

 ?>