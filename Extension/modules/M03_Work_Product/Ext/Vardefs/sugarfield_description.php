<?php
 // created: 2019-02-13 13:20:18
$dictionary['M03_Work_Product']['fields']['description']['audited']=true;
$dictionary['M03_Work_Product']['fields']['description']['massupdate']=false;
$dictionary['M03_Work_Product']['fields']['description']['comments']='Full text of the note';
$dictionary['M03_Work_Product']['fields']['description']['duplicate_merge']='enabled';
$dictionary['M03_Work_Product']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['M03_Work_Product']['fields']['description']['merge_filter']='disabled';
$dictionary['M03_Work_Product']['fields']['description']['unified_search']=false;
$dictionary['M03_Work_Product']['fields']['description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '0.5',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['description']['calculated']=false;
$dictionary['M03_Work_Product']['fields']['description']['rows']='6';
$dictionary['M03_Work_Product']['fields']['description']['cols']='80';

 ?>