<?php
 // created: 2019-04-17 11:37:13
$dictionary['M03_Work_Product']['fields']['name']['len']='255';
$dictionary['M03_Work_Product']['fields']['name']['audited']=true;
$dictionary['M03_Work_Product']['fields']['name']['massupdate']=false;
$dictionary['M03_Work_Product']['fields']['name']['unified_search']=false;
$dictionary['M03_Work_Product']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['M03_Work_Product']['fields']['name']['calculated']=false;
$dictionary['M03_Work_Product']['fields']['name']['required']=false;

 ?>