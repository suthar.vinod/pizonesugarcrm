<?php
// created: 2017-02-17 17:50:46
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_abc12_work_product_activities_1"] = array (
  'name' => 'm03_work_product_abc12_work_product_activities_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_abc12_work_product_activities_1',
  'source' => 'non-db',
  'module' => 'ABC12_Work_Product_Activities',
  'bean_name' => 'ABC12_Work_Product_Activities',
  'vname' => 'LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_p4898product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
