<?php
 // created: 2019-07-03 11:47:08
$dictionary['M03_Work_Product']['fields']['iacuc_approval_date_c']['labelValue']='IACUC Approval Date';
$dictionary['M03_Work_Product']['fields']['iacuc_approval_date_c']['formula']='$study_director_initiation_c';
$dictionary['M03_Work_Product']['fields']['iacuc_approval_date_c']['enforced']='false';
$dictionary['M03_Work_Product']['fields']['iacuc_approval_date_c']['dependency']='';

 ?>