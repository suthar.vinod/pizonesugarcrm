<?php
// created: 2017-09-12 15:10:30
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_wpe_work_product_enrollment_1"] = array (
  'name' => 'm03_work_product_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_p7d13product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
