<?php
 // created: 2021-02-16 07:09:54
$dictionary['M03_Work_Product']['fields']['quoted_protocol_hours_c']['labelValue']='Quoted Protocol Hours';
$dictionary['M03_Work_Product']['fields']['quoted_protocol_hours_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['quoted_protocol_hours_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['quoted_protocol_hours_c']['dependency']='equal($functional_area_c,"ISR")';
$dictionary['M03_Work_Product']['fields']['quoted_protocol_hours_c']['required_formula']='';

 ?>