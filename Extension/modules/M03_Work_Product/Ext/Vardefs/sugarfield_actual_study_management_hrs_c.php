<?php
 // created: 2021-04-01 09:18:25
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['labelValue']='Actual Study Management Hours';
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['formula']='add(
rollupConditionalSum(
	$m03_work_product_meetings_1,
	"actual_hours_worked_c",
	"scientific_activity_c",
	"Operations Oversight"
),
rollupConditionalSum(
	$meetings_m03_work_product_1,
	"actual_hours_worked_c",
	"scientific_activity_c",
	"Operations Oversight"
))';
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['dependency']='equal($functional_area_c,"ISR")';
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['readonly_formula']='';

 ?>