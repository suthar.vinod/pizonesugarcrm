<?php
 // created: 2021-12-07 12:46:04
$dictionary['M03_Work_Product']['fields']['work_product_compliance_c']['labelValue']='Work Product Compliance';
$dictionary['M03_Work_Product']['fields']['work_product_compliance_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['work_product_compliance_c']['required_formula']='not(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No"))';
$dictionary['M03_Work_Product']['fields']['work_product_compliance_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['work_product_compliance_c']['visibility_grid']='';

 ?>