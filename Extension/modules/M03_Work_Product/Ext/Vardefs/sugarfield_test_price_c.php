<?php
 // created: 2022-05-24 07:10:03
$dictionary['M03_Work_Product']['fields']['test_price_c']['labelValue']='Test Price';
$dictionary['M03_Work_Product']['fields']['test_price_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['test_price_c']['dependency']='isInList($functional_area_c,createList("Standard Biocompatibility","Biocompatibility"))';
$dictionary['M03_Work_Product']['fields']['test_price_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['M03_Work_Product']['fields']['test_price_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['test_price_c']['readonly_formula']='';

 ?>