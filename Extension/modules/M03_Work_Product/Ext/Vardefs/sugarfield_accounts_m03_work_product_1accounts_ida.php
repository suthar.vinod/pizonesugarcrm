<?php
 // created: 2021-12-07 12:49:12
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['name']='accounts_m03_work_product_1accounts_ida';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['type']='id';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['source']='non-db';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['vname']='LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['id_name']='accounts_m03_work_product_1accounts_ida';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['link']='accounts_m03_work_product_1';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['table']='accounts';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['module']='Accounts';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['rname']='id';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['reportable']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['side']='right';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['massupdate']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['duplicate_merge']='disabled';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['hideacl']=true;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['audited']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['importable']='true';

 ?>