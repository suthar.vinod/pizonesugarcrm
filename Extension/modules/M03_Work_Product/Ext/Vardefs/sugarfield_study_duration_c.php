<?php
 // created: 2020-06-12 17:15:10
$dictionary['M03_Work_Product']['fields']['study_duration_c']['labelValue']='Study Duration';
$dictionary['M03_Work_Product']['fields']['study_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['study_duration_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['study_duration_c']['formula']='ifElse(or(equal($last_procedure_c,""),equal($first_procedure_c,"")),"",subtract(daysUntil($last_procedure_c),daysUntil($first_procedure_c)))';
$dictionary['M03_Work_Product']['fields']['study_duration_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['study_duration_c']['dependency']='';

 ?>