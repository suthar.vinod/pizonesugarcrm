<?php
// created: 2019-08-05 12:04:19
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_activities_1_tasks"] = array (
  'name' => 'm03_work_product_activities_1_tasks',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_tasks',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_TASKS_FROM_TASKS_TITLE',
);
