<?php
 // created: 2022-08-09 10:01:16
$dictionary['M03_Work_Product']['fields']['create_tds_c']['labelValue']='create tds';
$dictionary['M03_Work_Product']['fields']['create_tds_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['create_tds_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['create_tds_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['create_tds_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['create_tds_c']['readonly_formula']='';

 ?>