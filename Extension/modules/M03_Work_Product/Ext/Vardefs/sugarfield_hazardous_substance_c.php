<?php
 // created: 2019-11-14 11:59:25
$dictionary['M03_Work_Product']['fields']['hazardous_substance_c']['labelValue']='Hazardous Agents';
$dictionary['M03_Work_Product']['fields']['hazardous_substance_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['M03_Work_Product']['fields']['hazardous_substance_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['hazardous_substance_c']['dependency']='equal($hazardous_agents_c,true)';

 ?>