<?php
 // created: 2022-07-12 09:35:16
$dictionary['M03_Work_Product']['fields']['other_extraction_conditions_c']['labelValue']='Other Extraction Conditions';
$dictionary['M03_Work_Product']['fields']['other_extraction_conditions_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['other_extraction_conditions_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['other_extraction_conditions_c']['dependency']='equal($extraction_conditions_c,"Other")';
$dictionary['M03_Work_Product']['fields']['other_extraction_conditions_c']['required_formula']='equal($extraction_conditions_c,"Other")';
$dictionary['M03_Work_Product']['fields']['other_extraction_conditions_c']['readonly_formula']='';

 ?>