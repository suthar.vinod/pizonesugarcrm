<?php
 // created: 2021-11-16 09:42:54
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['labelValue']='Specimen Retention End Date (GLP)';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['formula']='addDays($specimen_retention_start_c,2555)';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['dependency']='equal($work_product_compliance_c,"GLP")';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['readonly_formula']='';

 ?>