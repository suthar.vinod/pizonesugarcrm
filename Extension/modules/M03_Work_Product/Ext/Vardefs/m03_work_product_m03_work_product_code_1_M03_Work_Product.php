<?php
// created: 2021-01-21 14:15:48
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_m03_work_product_code_1"] = array (
  'name' => 'm03_work_product_m03_work_product_code_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_m03_work_product_code_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Code',
  'bean_name' => 'M03_Work_Product_Code',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'id_name' => 'm03_work_p5357ct_code_idb',
);
