<?php
 // created: 2020-08-11 08:04:45
$dictionary['M03_Work_Product']['fields']['control_articles_location_c']['labelValue']='Control Article(s) Location';
$dictionary['M03_Work_Product']['fields']['control_articles_location_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['control_articles_location_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['control_articles_location_c']['dependency']='equal($control_article_c,true)';

 ?>