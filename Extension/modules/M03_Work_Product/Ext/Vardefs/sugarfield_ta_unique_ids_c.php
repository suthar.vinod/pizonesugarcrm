<?php
 // created: 2020-08-11 08:08:06
$dictionary['M03_Work_Product']['fields']['ta_unique_ids_c']['labelValue']='Test Article Unique ID(s)';
$dictionary['M03_Work_Product']['fields']['ta_unique_ids_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['ta_unique_ids_c']['enforced']='false';
$dictionary['M03_Work_Product']['fields']['ta_unique_ids_c']['dependency']='equal($study_article_c,true)';

 ?>