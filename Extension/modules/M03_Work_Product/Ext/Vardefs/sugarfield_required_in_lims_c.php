<?php
 // created: 2021-12-07 12:37:33
$dictionary['M03_Work_Product']['fields']['required_in_lims_c']['labelValue']='Required In LIMS?';
$dictionary['M03_Work_Product']['fields']['required_in_lims_c']['dependency']='equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No")';
$dictionary['M03_Work_Product']['fields']['required_in_lims_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['required_in_lims_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['required_in_lims_c']['visibility_grid']='';

 ?>