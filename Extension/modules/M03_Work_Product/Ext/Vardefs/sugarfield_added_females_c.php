<?php
 // created: 2019-11-11 14:08:05
$dictionary['M03_Work_Product']['fields']['added_females_c']['labelValue']='Added Females';
$dictionary['M03_Work_Product']['fields']['added_females_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['added_females_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['added_females_c']['dependency']='equal($unspecified_sex_c,false)';

 ?>