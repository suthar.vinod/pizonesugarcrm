<?php
 // created: 2020-12-15 12:31:04
$dictionary['M03_Work_Product']['fields']['primary_animals_countdown_c']['labelValue']='Animals Remaining';
$dictionary['M03_Work_Product']['fields']['primary_animals_countdown_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['primary_animals_countdown_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['primary_animals_countdown_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['primary_animals_countdown_c']['required_formula']='';

 ?>