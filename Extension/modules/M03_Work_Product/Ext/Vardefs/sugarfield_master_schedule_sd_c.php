<?php
 // created: 2020-05-21 11:05:07
$dictionary['M03_Work_Product']['fields']['master_schedule_sd_c']['labelValue']='Master Schedule Study Director';
$dictionary['M03_Work_Product']['fields']['master_schedule_sd_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['master_schedule_sd_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['master_schedule_sd_c']['dependency']='isInList($work_product_compliance_c,createList("GLP","Choose Compliance","GLP Not Performed","GLP Discontinued","Withdrawn","GLP_Amended_NonGLP"))';

 ?>