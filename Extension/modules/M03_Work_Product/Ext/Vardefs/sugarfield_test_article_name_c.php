<?php
 // created: 2021-11-09 06:44:10
$dictionary['M03_Work_Product']['fields']['test_article_name_c']['labelValue']='Test Article Name ';
$dictionary['M03_Work_Product']['fields']['test_article_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['test_article_name_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['test_article_name_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['test_article_name_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['test_article_name_c']['readonly_formula']='';

 ?>