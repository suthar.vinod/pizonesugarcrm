<?php
 // created: 2021-04-29 08:48:48
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['labelValue']='WPC SC01';
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['calculated']='true';
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['formula']='contains($name,"SC01")';
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['enforced']='true';
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['readonly_formula']='';

 ?>