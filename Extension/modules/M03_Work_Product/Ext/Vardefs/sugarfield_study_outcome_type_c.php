<?php
 // created: 2021-10-26 09:18:48
$dictionary['M03_Work_Product']['fields']['study_outcome_type_c']['labelValue']='Study Outcome Type';
$dictionary['M03_Work_Product']['fields']['study_outcome_type_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['study_outcome_type_c']['required_formula']='and(equal($functional_area_c,"Standard Biocompatibility"),equal($work_product_status_c,"Archived"))';
$dictionary['M03_Work_Product']['fields']['study_outcome_type_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['study_outcome_type_c']['visibility_grid']='';

 ?>