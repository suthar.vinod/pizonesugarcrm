<?php
 // created: 2021-04-20 08:28:05
$dictionary['M03_Work_Product']['fields']['number_of_peaks_gcms_c']['labelValue']='Number of Peaks Identified for GCMS Analysis';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_gcms_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['number_of_peaks_gcms_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_gcms_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_gcms_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_gcms_c']['readonly_formula']='';

 ?>