<?php
 // created: 2019-11-11 14:08:31
$dictionary['M03_Work_Product']['fields']['total_males_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['total_males_c']['labelValue']='Total Males';
$dictionary['M03_Work_Product']['fields']['total_males_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['total_males_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['total_males_c']['formula']='add($initial_males_c,$added_males_c)';
$dictionary['M03_Work_Product']['fields']['total_males_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['total_males_c']['dependency']='equal($unspecified_sex_c,false)';

 ?>