<?php
// created: 2018-02-05 14:21:02
$dictionary["M03_Work_Product"]["fields"]["m99_protocol_amendments_m03_work_product"] = array (
  'name' => 'm99_protocol_amendments_m03_work_product',
  'type' => 'link',
  'relationship' => 'm99_protocol_amendments_m03_work_product',
  'source' => 'non-db',
  'module' => 'M99_Protocol_Amendments',
  'bean_name' => false,
  'vname' => 'LBL_M99_PROTOCOL_AMENDMENTS_M03_WORK_PRODUCT_FROM_M99_PROTOCOL_AMENDMENTS_TITLE',
  'id_name' => 'm99_protoc2862ndments_ida',
);
