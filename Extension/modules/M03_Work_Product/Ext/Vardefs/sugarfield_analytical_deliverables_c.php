<?php
 // created: 2020-10-26 11:16:39
$dictionary['M03_Work_Product']['fields']['analytical_deliverables_c']['labelValue']='Are there Analytical Deliverables?';
$dictionary['M03_Work_Product']['fields']['analytical_deliverables_c']['dependency']='not(equal($functional_area_c,"Analytical Services"))';
$dictionary['M03_Work_Product']['fields']['analytical_deliverables_c']['visibility_grid']='';

 ?>