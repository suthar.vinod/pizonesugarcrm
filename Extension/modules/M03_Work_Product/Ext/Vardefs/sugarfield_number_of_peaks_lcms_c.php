<?php
 // created: 2021-04-20 08:29:27
$dictionary['M03_Work_Product']['fields']['number_of_peaks_lcms_c']['labelValue']='Number of Peaks Identified for LCMS Analysis';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_lcms_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['number_of_peaks_lcms_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_lcms_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_lcms_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_lcms_c']['readonly_formula']='';

 ?>