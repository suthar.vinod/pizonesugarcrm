<?php
 // created: 2022-07-07 12:01:05
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['audited']=true;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['massupdate']=true;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['hidemassupdate']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['duplicate_merge']='enabled';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['duplicate_merge_dom_value']='1';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['merge_filter']='disabled';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['reportable']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['unified_search']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['calculated']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['vname']='LBL_CONTACTS_M03_WORK_PRODUCT_2_NAME_FIELD_TITLE';

 ?>