<?php
 // created: 2021-12-07 12:49:16
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['required']=true;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['audited']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['massupdate']=true;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['hidemassupdate']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['duplicate_merge']='enabled';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['duplicate_merge_dom_value']='1';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['merge_filter']='disabled';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['reportable']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['unified_search']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['calculated']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['required_formula']='not(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No"))';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['vname']='LBL_ACCOUNTS_M03_WORK_PRODUCT_1_NAME_FIELD_TITLE';

 ?>