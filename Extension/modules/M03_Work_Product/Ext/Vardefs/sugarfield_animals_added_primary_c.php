<?php
 // created: 2020-12-15 12:29:37
$dictionary['M03_Work_Product']['fields']['animals_added_primary_c']['labelValue']='Additional Animals Approved';
$dictionary['M03_Work_Product']['fields']['animals_added_primary_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['animals_added_primary_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['animals_added_primary_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['animals_added_primary_c']['required_formula']='';

 ?>