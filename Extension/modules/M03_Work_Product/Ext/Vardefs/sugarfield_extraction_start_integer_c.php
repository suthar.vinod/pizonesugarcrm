<?php
 // created: 2021-12-02 06:09:13
$dictionary['M03_Work_Product']['fields']['extraction_start_integer_c']['labelValue']='Extraction Start Integer';
$dictionary['M03_Work_Product']['fields']['extraction_start_integer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['extraction_start_integer_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['extraction_start_integer_c']['dependency']='equal($functional_area_c,"Standard Biocompatibility")';
$dictionary['M03_Work_Product']['fields']['extraction_start_integer_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['extraction_start_integer_c']['readonly_formula']='';

 ?>