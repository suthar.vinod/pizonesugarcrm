<?php
// created: 2022-04-26 06:44:22
$dictionary["M03_Work_Product"]["fields"]["bid_batch_id_m03_work_product_1"] = array (
  'name' => 'bid_batch_id_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'BID_Batch_ID',
  'bean_name' => 'BID_Batch_ID',
  'side' => 'right',
  'vname' => 'LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product"]["fields"]["bid_batch_id_m03_work_product_1_name"] = array (
  'name' => 'bid_batch_id_m03_work_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_BID_BATCH_ID_TITLE',
  'save' => true,
  'id_name' => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
  'link' => 'bid_batch_id_m03_work_product_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'name',
);
$dictionary["M03_Work_Product"]["fields"]["bid_batch_id_m03_work_product_1bid_batch_id_ida"] = array (
  'name' => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID',
  'id_name' => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
  'link' => 'bid_batch_id_m03_work_product_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
