<?php
 // created: 2019-02-13 13:22:58
$dictionary['M03_Work_Product']['fields']['date_entered']['audited']=true;
$dictionary['M03_Work_Product']['fields']['date_entered']['comments']='Date record created';
$dictionary['M03_Work_Product']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['M03_Work_Product']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['M03_Work_Product']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['M03_Work_Product']['fields']['date_entered']['unified_search']=false;
$dictionary['M03_Work_Product']['fields']['date_entered']['calculated']=false;
$dictionary['M03_Work_Product']['fields']['date_entered']['enable_range_search']='1';

 ?>