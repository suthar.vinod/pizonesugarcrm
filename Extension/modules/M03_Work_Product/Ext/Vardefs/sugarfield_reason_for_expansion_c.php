<?php
 // created: 2021-10-26 09:07:44
$dictionary['M03_Work_Product']['fields']['reason_for_expansion_c']['labelValue']='Reason for Expansion';
$dictionary['M03_Work_Product']['fields']['reason_for_expansion_c']['dependency']='equal($study_outcome_type_c,"Expanded Study")';
$dictionary['M03_Work_Product']['fields']['reason_for_expansion_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['reason_for_expansion_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['reason_for_expansion_c']['visibility_grid']='';

 ?>