<?php
 // created: 2021-07-22 06:50:02
$dictionary['M03_Work_Product']['fields']['report_timeline_type_c']['labelValue']='Report Timeline Type';
$dictionary['M03_Work_Product']['fields']['report_timeline_type_c']['dependency']='equal($functional_area_c,"Standard Biocompatibility")';
$dictionary['M03_Work_Product']['fields']['report_timeline_type_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['report_timeline_type_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['report_timeline_type_c']['visibility_grid']='';

 ?>