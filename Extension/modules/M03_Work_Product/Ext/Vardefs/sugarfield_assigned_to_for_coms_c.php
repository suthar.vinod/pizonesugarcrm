<?php
 // created: 2020-02-05 13:05:35
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['labelValue']='Assigned to for Coms';
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['formula']='concat(related($assigned_user_link,"first_name")," ",related($assigned_user_link,"last_name"))';
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['dependency']='';

 ?>