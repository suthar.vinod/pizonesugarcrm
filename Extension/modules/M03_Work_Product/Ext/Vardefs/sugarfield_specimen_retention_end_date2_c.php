<?php
 // created: 2020-04-27 18:23:26
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_date2_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_date2_c']['labelValue']='Specimen Retention End Date (nonGLP)';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_date2_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_date2_c']['formula']='addDays($specimen_retention_start_c,182)';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_date2_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_date2_c']['dependency']='isInList($work_product_compliance_c,createList("nonGLP","NonGLP Discovery","NonGLP Structured"))';

 ?>