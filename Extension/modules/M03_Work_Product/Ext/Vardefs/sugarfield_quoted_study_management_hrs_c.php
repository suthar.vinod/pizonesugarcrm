<?php
 // created: 2021-02-16 07:12:44
$dictionary['M03_Work_Product']['fields']['quoted_study_management_hrs_c']['labelValue']='Quoted Study Management Hours';
$dictionary['M03_Work_Product']['fields']['quoted_study_management_hrs_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['quoted_study_management_hrs_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['quoted_study_management_hrs_c']['dependency']='equal($functional_area_c,"ISR")';
$dictionary['M03_Work_Product']['fields']['quoted_study_management_hrs_c']['required_formula']='';

 ?>