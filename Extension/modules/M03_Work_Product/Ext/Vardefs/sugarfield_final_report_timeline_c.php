<?php
 // created: 2021-07-22 06:37:21
$dictionary['M03_Work_Product']['fields']['final_report_timeline_c']['labelValue']='Final Report Timeline (Weeks)';
$dictionary['M03_Work_Product']['fields']['final_report_timeline_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['final_report_timeline_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['final_report_timeline_c']['dependency']='equal($functional_area_c,"Standard Biocompatibility")';
$dictionary['M03_Work_Product']['fields']['final_report_timeline_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['final_report_timeline_c']['readonly_formula']='';

 ?>