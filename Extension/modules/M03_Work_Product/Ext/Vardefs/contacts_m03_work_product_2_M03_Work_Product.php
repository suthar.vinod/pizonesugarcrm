<?php
// created: 2022-07-07 07:02:30
$dictionary["M03_Work_Product"]["fields"]["contacts_m03_work_product_2"] = array (
  'name' => 'contacts_m03_work_product_2',
  'type' => 'link',
  'relationship' => 'contacts_m03_work_product_2',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'contacts_m03_work_product_2contacts_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product"]["fields"]["contacts_m03_work_product_2_name"] = array (
  'name' => 'contacts_m03_work_product_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_m03_work_product_2contacts_ida',
  'link' => 'contacts_m03_work_product_2',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["M03_Work_Product"]["fields"]["contacts_m03_work_product_2contacts_ida"] = array (
  'name' => 'contacts_m03_work_product_2contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE_ID',
  'id_name' => 'contacts_m03_work_product_2contacts_ida',
  'link' => 'contacts_m03_work_product_2',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
