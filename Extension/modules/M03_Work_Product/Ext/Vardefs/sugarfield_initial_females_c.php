<?php
 // created: 2019-11-11 14:07:14
$dictionary['M03_Work_Product']['fields']['initial_females_c']['labelValue']='Initial Females';
$dictionary['M03_Work_Product']['fields']['initial_females_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['initial_females_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['initial_females_c']['dependency']='equal($unspecified_sex_c,false)';

 ?>