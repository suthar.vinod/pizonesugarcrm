<?php
// created: 2021-11-09 10:14:52
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_im_inventory_management_1"] = array (
  'name' => 'm03_work_product_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'IM_Inventory_Management',
  'bean_name' => 'IM_Inventory_Management',
  'vname' => 'LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_im_inventory_management_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
