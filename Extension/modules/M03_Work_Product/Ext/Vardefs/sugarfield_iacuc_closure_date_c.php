<?php
 // created: 2019-07-25 11:48:58
$dictionary['M03_Work_Product']['fields']['iacuc_closure_date_c']['labelValue']='IACUC Closure Date';
$dictionary['M03_Work_Product']['fields']['iacuc_closure_date_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['iacuc_closure_date_c']['dependency']='equal($iacuc_protocol_status_c,"Inactive")';

 ?>