<?php
 // created: 2021-03-04 09:28:47
$dictionary['M03_Work_Product']['fields']['study_workload_c']['labelValue']='Study Workload';
$dictionary['M03_Work_Product']['fields']['study_workload_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['study_workload_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['study_workload_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['study_workload_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['study_workload_c']['readonly_formula']='';

 ?>