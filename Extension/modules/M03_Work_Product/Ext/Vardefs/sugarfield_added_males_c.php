<?php
 // created: 2019-11-11 14:07:42
$dictionary['M03_Work_Product']['fields']['added_males_c']['labelValue']='Added Males';
$dictionary['M03_Work_Product']['fields']['added_males_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['added_males_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['added_males_c']['dependency']='equal($unspecified_sex_c,false)';

 ?>