<?php
 // created: 2021-12-02 06:12:33
$dictionary['M03_Work_Product']['fields']['extraction_end_integer_c']['labelValue']='Extraction End Integer';
$dictionary['M03_Work_Product']['fields']['extraction_end_integer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['extraction_end_integer_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['extraction_end_integer_c']['dependency']='equal($functional_area_c,"Standard Biocompatibility")';
$dictionary['M03_Work_Product']['fields']['extraction_end_integer_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['extraction_end_integer_c']['readonly_formula']='';

 ?>