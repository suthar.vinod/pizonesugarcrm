<?php
 // created: 2022-02-08 08:34:42
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['labelValue']='GDs Complete';
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['calculated']='true';
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['formula']='rollupSum($m03_work_product_gd_group_design_1,"complete_yes_no_c")';
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['enforced']='true';
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['readonly_formula']='';

 ?>