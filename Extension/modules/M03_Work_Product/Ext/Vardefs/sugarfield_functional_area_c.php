<?php
 // created: 2021-12-07 12:44:56
$dictionary['M03_Work_Product']['fields']['functional_area_c']['labelValue']='Functional Area';
$dictionary['M03_Work_Product']['fields']['functional_area_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['functional_area_c']['required_formula']='not(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No"))';
$dictionary['M03_Work_Product']['fields']['functional_area_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['functional_area_c']['visibility_grid']='';

 ?>