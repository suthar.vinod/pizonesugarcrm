<?php
 // created: 2021-12-07 12:47:21
$dictionary['M03_Work_Product']['fields']['work_product_code_2_c']['labelValue']='Work Product Code';
$dictionary['M03_Work_Product']['fields']['work_product_code_2_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['work_product_code_2_c']['required_formula']='not(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No"))';
$dictionary['M03_Work_Product']['fields']['work_product_code_2_c']['readonly_formula']='';

 ?>