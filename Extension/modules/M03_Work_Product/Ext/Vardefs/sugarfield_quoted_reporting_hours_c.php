<?php
 // created: 2021-02-16 07:14:29
$dictionary['M03_Work_Product']['fields']['quoted_reporting_hours_c']['labelValue']='Quoted Reporting Hours';
$dictionary['M03_Work_Product']['fields']['quoted_reporting_hours_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['quoted_reporting_hours_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['quoted_reporting_hours_c']['dependency']='equal($functional_area_c,"ISR")';
$dictionary['M03_Work_Product']['fields']['quoted_reporting_hours_c']['required_formula']='';

 ?>