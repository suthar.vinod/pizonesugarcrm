<?php
 // created: 2021-03-12 05:36:45
$dictionary['M03_Work_Product']['fields']['timeline_type_c']['labelValue']='Timeline Type';
$dictionary['M03_Work_Product']['fields']['timeline_type_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['timeline_type_c']['enforced']='false';
$dictionary['M03_Work_Product']['fields']['timeline_type_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['timeline_type_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['timeline_type_c']['readonly_formula']='';

 ?>