<?php
 // created: 2019-10-03 12:21:30
$dictionary['M03_Work_Product']['fields']['master_schedule_company_name_c']['labelValue']='Master Schedule Sponsor Name';
$dictionary['M03_Work_Product']['fields']['master_schedule_company_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['master_schedule_company_name_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['master_schedule_company_name_c']['dependency']='isInList($work_product_compliance_c,createList("GLP","Choose Compliance","GLP Not Performed","GLP Discontinued","Withdrawn","GLP_Amended_NonGLP"))';

 ?>