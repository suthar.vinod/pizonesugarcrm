<?php
 // created: 2019-04-11 11:54:22
$dictionary['M03_Work_Product']['fields']['title_description_c']['labelValue']='Work Product Title';
$dictionary['M03_Work_Product']['fields']['title_description_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['title_description_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['title_description_c']['dependency']='';

 ?>