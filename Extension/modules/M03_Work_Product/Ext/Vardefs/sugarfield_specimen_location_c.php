<?php
 // created: 2019-04-15 13:03:08
$dictionary['M03_Work_Product']['fields']['specimen_location_c']['labelValue']='Specimen Location';
$dictionary['M03_Work_Product']['fields']['specimen_location_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['specimen_location_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['specimen_location_c']['dependency']='';

 ?>