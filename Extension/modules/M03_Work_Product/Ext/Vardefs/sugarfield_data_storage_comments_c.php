<?php
 // created: 2019-04-15 13:10:44
$dictionary['M03_Work_Product']['fields']['data_storage_comments_c']['labelValue']='Archive Comments';
$dictionary['M03_Work_Product']['fields']['data_storage_comments_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['data_storage_comments_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['data_storage_comments_c']['dependency']='';

 ?>