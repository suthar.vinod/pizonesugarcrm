<?php
 // created: 2019-05-30 15:29:39
$dictionary['M03_Work_Product']['fields']['company_status_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['company_status_c']['labelValue']='Company Status';
$dictionary['M03_Work_Product']['fields']['company_status_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['company_status_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['company_status_c']['formula']='related($m01_sales_m03_work_product_1,"account_status_2_c")';
$dictionary['M03_Work_Product']['fields']['company_status_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['company_status_c']['dependency']='';

 ?>