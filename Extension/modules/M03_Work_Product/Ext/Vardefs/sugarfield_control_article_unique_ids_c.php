<?php
 // created: 2020-08-11 08:06:10
$dictionary['M03_Work_Product']['fields']['control_article_unique_ids_c']['labelValue']='Control Article Unique ID(s)';
$dictionary['M03_Work_Product']['fields']['control_article_unique_ids_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['control_article_unique_ids_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['control_article_unique_ids_c']['dependency']='equal($control_article_c,true)';

 ?>