<?php
 // created: 2021-12-07 12:39:05
$dictionary['M03_Work_Product']['fields']['namsa_submission_id_c']['labelValue']='NAMSA Submission ID';
$dictionary['M03_Work_Product']['fields']['namsa_submission_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['namsa_submission_id_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['namsa_submission_id_c']['dependency']='equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No")';
$dictionary['M03_Work_Product']['fields']['namsa_submission_id_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['namsa_submission_id_c']['readonly_formula']='';

 ?>