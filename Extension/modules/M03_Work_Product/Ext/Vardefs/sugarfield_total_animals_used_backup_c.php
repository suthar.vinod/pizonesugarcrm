<?php
 // created: 2019-11-06 13:34:47
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['labelValue']='Total Back-Up Animals Approved';
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['formula']='add($animals_added_backup_c,$initial_animals_approved_bac_c)';
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['dependency']='';

 ?>