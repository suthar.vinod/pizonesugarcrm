<?php
 // created: 2019-11-11 14:06:49
$dictionary['M03_Work_Product']['fields']['initial_males_c']['labelValue']='Initial Males';
$dictionary['M03_Work_Product']['fields']['initial_males_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['initial_males_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['initial_males_c']['dependency']='equal($unspecified_sex_c,false)';

 ?>