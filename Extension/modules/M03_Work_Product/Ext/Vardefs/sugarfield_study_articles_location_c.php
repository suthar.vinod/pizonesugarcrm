<?php
 // created: 2020-08-11 08:11:24
$dictionary['M03_Work_Product']['fields']['study_articles_location_c']['labelValue']='Test Article(s) Location';
$dictionary['M03_Work_Product']['fields']['study_articles_location_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['study_articles_location_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['study_articles_location_c']['dependency']='equal($study_article_c,true)';

 ?>