<?php
 // created: 2021-10-26 09:11:51
$dictionary['M03_Work_Product']['fields']['reason_for_discontinuation_c']['labelValue']='Reason for Discontinuation';
$dictionary['M03_Work_Product']['fields']['reason_for_discontinuation_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['reason_for_discontinuation_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['reason_for_discontinuation_c']['dependency']='equal($study_outcome_type_c,"Discontinued Study")';
$dictionary['M03_Work_Product']['fields']['reason_for_discontinuation_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['reason_for_discontinuation_c']['readonly_formula']='';

 ?>