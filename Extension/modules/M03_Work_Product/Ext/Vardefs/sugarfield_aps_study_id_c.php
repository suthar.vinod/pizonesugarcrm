<?php
 // created: 2019-02-13 13:53:04
$dictionary['M03_Work_Product']['fields']['aps_study_id_c']['labelValue']='Method Validation Work Product ID (if applicable)';
$dictionary['M03_Work_Product']['fields']['aps_study_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['aps_study_id_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['aps_study_id_c']['dependency']='';

 ?>