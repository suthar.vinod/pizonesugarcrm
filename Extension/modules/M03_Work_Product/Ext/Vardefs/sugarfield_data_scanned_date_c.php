<?php
 // created: 2019-04-15 13:08:00
$dictionary['M03_Work_Product']['fields']['data_scanned_date_c']['labelValue']='Study Scanned Date';
$dictionary['M03_Work_Product']['fields']['data_scanned_date_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['data_scanned_date_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['data_scanned_date_c']['dependency']='';

 ?>