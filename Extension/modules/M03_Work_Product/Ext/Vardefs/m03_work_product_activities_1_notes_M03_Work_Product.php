<?php
// created: 2019-08-05 12:03:53
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_activities_1_notes"] = array (
  'name' => 'm03_work_product_activities_1_notes',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_notes',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_NOTES_FROM_NOTES_TITLE',
);
