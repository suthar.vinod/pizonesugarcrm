<?php
 // created: 2016-10-25 03:27:08
$dictionary['M03_Work_Product']['fields']['work_product_id_sponsor_c']['labelValue'] = 'Work Product ID (Sponsor)';
$dictionary['M03_Work_Product']['fields']['work_product_id_sponsor_c']['full_text_search']['enabled'] = true;
$dictionary['M03_Work_Product']['fields']['work_product_id_sponsor_c']['full_text_search']['searchable'] = false;
$dictionary['M03_Work_Product']['fields']['work_product_id_sponsor_c']['full_text_search']['boost'] = 1;
$dictionary['M03_Work_Product']['fields']['work_product_id_sponsor_c']['enforced'] = '';
$dictionary['M03_Work_Product']['fields']['work_product_id_sponsor_c']['dependency'] = '';

