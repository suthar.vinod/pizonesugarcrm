<?php
 // created: 2021-04-01 09:17:21
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['labelValue']='Actual Protocol Hours';
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['formula']='add(
rollupConditionalSum(
	$m03_work_product_meetings_1,
	"actual_hours_worked_c",
	"scientific_activity_c",
	"Protocol Development"
),
rollupConditionalSum(
	$meetings_m03_work_product_1,
	"actual_hours_worked_c",
	"scientific_activity_c",
	"Protocol Development"
))';
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['dependency']='equal($functional_area_c,"ISR")';
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['readonly_formula']='';

 ?>