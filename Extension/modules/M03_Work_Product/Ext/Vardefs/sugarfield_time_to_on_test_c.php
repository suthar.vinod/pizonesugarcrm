<?php
 // created: 2021-02-18 05:26:01
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['labelValue']='Time to On Test';
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['calculated']='true';
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['formula']='ifElse(or(equal($bc_spa_reconciled_date_c,""),equal($first_procedure_c,"")),"",subtract(daysUntil($first_procedure_c),daysUntil($bc_spa_reconciled_date_c)))';
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['enforced']='true';
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['required_formula']='';

 ?>