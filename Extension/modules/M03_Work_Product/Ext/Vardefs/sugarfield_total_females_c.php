<?php
 // created: 2019-11-11 14:08:54
$dictionary['M03_Work_Product']['fields']['total_females_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['total_females_c']['labelValue']='Total Females';
$dictionary['M03_Work_Product']['fields']['total_females_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['total_females_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['total_females_c']['formula']='add($initial_females_c,$added_females_c)';
$dictionary['M03_Work_Product']['fields']['total_females_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['total_females_c']['dependency']='equal($unspecified_sex_c,false)';

 ?>