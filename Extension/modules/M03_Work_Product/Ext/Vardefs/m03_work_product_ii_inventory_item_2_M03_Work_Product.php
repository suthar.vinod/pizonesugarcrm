<?php
// created: 2021-11-09 10:07:50
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_ii_inventory_item_2"] = array (
  'name' => 'm03_work_product_ii_inventory_item_2',
  'type' => 'link',
  'relationship' => 'm03_work_product_ii_inventory_item_2',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'm03_work_product_ii_inventory_item_2ii_inventory_item_idb',
);
