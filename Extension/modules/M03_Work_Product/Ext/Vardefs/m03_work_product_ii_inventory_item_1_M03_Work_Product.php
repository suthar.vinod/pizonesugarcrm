<?php
// created: 2021-11-09 10:04:52
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_ii_inventory_item_1"] = array (
  'name' => 'm03_work_product_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_ii_inventory_item_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
