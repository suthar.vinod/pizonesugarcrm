<?php
 // created: 2019-11-06 13:33:30
$dictionary['M03_Work_Product']['fields']['animals_added_backup_c']['labelValue']='Additional Back-Up Animals Approved';
$dictionary['M03_Work_Product']['fields']['animals_added_backup_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['animals_added_backup_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['animals_added_backup_c']['dependency']='';

 ?>