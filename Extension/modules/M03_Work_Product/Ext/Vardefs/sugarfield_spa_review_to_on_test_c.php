<?php
 // created: 2021-06-17 08:34:47
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['labelValue']='SPA Review to On Test';
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['calculated']='true';
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['formula']='ifElse(or(equal(related($m01_sales_m03_work_product_1,"bc_study_article_received_c"),""),equal($first_procedure_c,"")),"",subtract(daysUntil($first_procedure_c),daysUntil(related($m01_sales_m03_work_product_1,"bc_study_article_received_c"))))';
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['enforced']='true';
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['readonly_formula']='';

 ?>