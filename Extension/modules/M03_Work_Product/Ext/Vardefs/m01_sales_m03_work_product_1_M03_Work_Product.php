<?php
// created: 2016-01-29 21:10:09
$dictionary["M03_Work_Product"]["fields"]["m01_sales_m03_work_product_1"] = array (
  'name' => 'm01_sales_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'm01_sales_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm01_sales_m03_work_product_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product"]["fields"]["m01_sales_m03_work_product_1_name"] = array (
  'name' => 'm01_sales_m03_work_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_m03_work_product_1m01_sales_ida',
  'link' => 'm01_sales_m03_work_product_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["M03_Work_Product"]["fields"]["m01_sales_m03_work_product_1m01_sales_ida"] = array (
  'name' => 'm01_sales_m03_work_product_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID',
  'id_name' => 'm01_sales_m03_work_product_1m01_sales_ida',
  'link' => 'm01_sales_m03_work_product_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
