<?php
 // created: 2020-12-15 12:30:29
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['labelValue']='Total Animals Approved';
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['formula']='add($initial_animals_approved_pri_c,$animals_added_primary_c)';
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['required_formula']='';

 ?>