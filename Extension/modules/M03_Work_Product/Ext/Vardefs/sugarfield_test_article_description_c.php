<?php
 // created: 2021-11-09 06:42:34
$dictionary['M03_Work_Product']['fields']['test_article_description_c']['labelValue']='Test Article Name (Obsolete)';
$dictionary['M03_Work_Product']['fields']['test_article_description_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['test_article_description_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['test_article_description_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['test_article_description_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['test_article_description_c']['readonly_formula']='';

 ?>