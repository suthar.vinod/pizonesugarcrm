<?php
 // created: 2019-04-16 11:43:45
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_glp_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_glp_c']['labelValue']='Data Retention End Date (GLP)';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_glp_c']['calculated']='true';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_glp_c']['formula']='addDays($data_retention_start_date_c,3650)';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_glp_c']['enforced']='true';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_glp_c']['dependency']='equal($work_product_compliance_c,"GLP")';

 ?>