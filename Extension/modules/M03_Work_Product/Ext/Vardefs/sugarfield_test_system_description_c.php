<?php
 // created: 2019-02-12 15:41:29
$dictionary['M03_Work_Product']['fields']['test_system_description_c']['labelValue']='Test System Description';
$dictionary['M03_Work_Product']['fields']['test_system_description_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['test_system_description_c']['enforced']='false';
$dictionary['M03_Work_Product']['fields']['test_system_description_c']['dependency']='equal($test_system_c,"Bacterial Strain")';

 ?>