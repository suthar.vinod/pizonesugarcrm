<?php
 
$dependencies['M03_Work_Product']['required_fields323'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('work_product_code_2_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'extraction_conditions_c',
                'value' => 'isInList($work_product_code_2_c, createList("SE01","SE01a","SE03","SE03a","SE09","SE12","SE12a","IR01","IR01a","IR12","IR12a","IR19","IR21","IR21a","IR29","IR31","IR31a","IR39","ST01","ST01a","ST09","ST10","ST10a","ST19","ST22","ST22a","ST23","ST23a","ST24","ST24a","ST25","ST25a","ST29","GE01","GE01a","GE09","GE10","GE10a","GE11","GE11a","GE12","GE12a","GE16","GE16a","GE19","GE30","GE30a","GE39","HE01","HE01a","HE03","HE03a","HE04","HE04a","HE09"))',
            ),
        ),  
    ),
);
$dependencies['M03_Work_Product']['required_fields32354'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('work_product_code_2_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'extraction_conditions_c',
                'value' => 'isInList($work_product_code_2_c, createList("SE01","SE01a","SE03","SE03a","SE09","SE12","SE12a","IR01","IR01a","IR12","IR12a","IR19","IR21","IR21a","IR29","IR31","IR31a","IR39","ST01","ST01a","ST09","ST10","ST10a","ST19","ST22","ST22a","ST23","ST23a","ST24","ST24a","ST25","ST25a","ST29","GE01","GE01a","GE09","GE10","GE10a","GE11","GE11a","GE12","GE12a","GE16","GE16a","GE19","GE30","GE30a","GE39","HE01","HE01a","HE03","HE03a","HE04","HE04a","HE09"))',
            ),
        ),  
    ),
);


