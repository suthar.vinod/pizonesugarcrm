<?php
$dependencies['M03_Work_Product']['final_report_timeline_required123'] = array(
    'hooks'         => array("view", "edit"),
    'trigger'       => 'true',
    'triggerFields' => array('date_entered','id','functional_area_c','m01_sales_m03_work_product_1','final_report_timeline_c','report_timeline_type_c','extraction_start_integer_c','extraction_end_integer_c','integer_units_c'),
	'onload'        => true,
    'actions'       => array(
        array( 
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'final_report_timeline_c',
                 'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),and(equal($functional_area_c,"Standard Biocompatibility"),or(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"Yes"),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),""))))',
            ),
        ), 
        array( 
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'report_timeline_type_c',
                 'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),and(equal($functional_area_c,"Standard Biocompatibility"),or(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"Yes"),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),""))))',
            ),
        ),
        array( 
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'extraction_start_integer_c',
                 'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),and(equal($functional_area_c,"Standard Biocompatibility"),or(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"Yes"),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),""))))',
            ),
        ),
        array( 
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'extraction_end_integer_c',
                 'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),and(equal($functional_area_c,"Standard Biocompatibility"),or(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"Yes"),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),""))))',
            ),
        ),
        array( 
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'integer_units_c',
                 'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),and(equal($functional_area_c,"Standard Biocompatibility"),or(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"Yes"),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),""))))',
            ),
        ),
    ),
);