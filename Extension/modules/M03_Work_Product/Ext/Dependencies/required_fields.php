<?php
$dependencies['M03_Work_Product']['setfirst_readonly2'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('first_procedure_c','bid_batch_id_m03_work_product_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'first_procedure_c',
                'value'  => 'ifElse(not(equal($bid_batch_id_m03_work_product_1_name,"")),true,false)',
            ),
        ),
    ),
); 
$dependencies['M03_Work_Product']['required_fields'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('work_product_status_c', 'work_product_compliance_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'name',
                'value' => 'isInList($work_product_status_c, createList("Testing","Archived"))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'master_schedule_sd_c',
                'value' => 'and(isInList($work_product_compliance_c,createList("GLP","Choose Compliance","GLP Not Performed","GLP Discontinued","Withdrawn","GLP_Amended_NonGLP")),isInList($work_product_status_c,createList("Testing","Archived")))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'master_schedule_company_name_c',
                'value' => 'and(isInList($work_product_compliance_c,createList("GLP","Choose Compliance","GLP Not Performed","GLP Discontinued","Withdrawn","GLP_Amended_NonGLP")),isInList($work_product_status_c,createList("Testing","Archived")))',
            ),
        ),
    ),
);

/*22 oct :584: analytical_deliverables field conditional required Bug #1868*/
$dependencies['M03_Work_Product']['analytical_deliverables_required'] = array(
    'hooks'         => array("view", "edit","save","all"),
    'trigger'       => 'true',
    'triggerFields' => array('date_entered','id','functional_area_c','m01_sales_m03_work_product_1','analytical_deliverables_c'),
	'onload'        => true,
    'actions'       => array(
        array( 
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'analytical_deliverables_c',
                'value' => 'ifElse(and(not(equal($functional_area_c,"Analytical Services")),isAfter($date_entered,date("2020-10-27")),or(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"Yes"),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),""))),true,false)',
            ),
        ), 
    ),
);


$dependencies['M03_Work_Product']['set_blanket_readonly'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('work_product_code_2_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'blanket_animals_remaining_c',
                'value' => 'or(equal($work_product_code_2_c,"BCBLK"),equal($work_product_code_2_c,"PHBLK"))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'blanket_animals_remaining_c',
                'value' => 'or(equal($work_product_code_2_c,"BCBLK"),equal($work_product_code_2_c,"PHBLK"))',
            ),
        ),
    ),
); 
 

/* Change By Laxmichand 03 December 2020 */

global $current_user,$db;
$sql = "select * from acl_roles_users where role_id='fc0f19ea-41c8-11e9-bc3b-067d538c0c10' AND user_id='$current_user->id' AND deleted=0";
$result = $db->query($sql);
if($result->num_rows == 0){

    $dependencies['M03_Work_Product']['wp_fields_work_product_status_c_dp'] = array(
        'hooks' => array("edit"),
        'trigger' => 'true',
        'triggerFields' => array('id'),
        'onload' => true,
        'actions' => array(
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'work_product_status_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
        ),
    );

    $dependencies['M03_Work_Product']['wp_fields_dp'] = array(
        'hooks' => array("edit"),
        'trigger' => 'true',
        'triggerFields' => array('work_product_status_c','name','work_product_compliance_c','master_schedule_company_name_c','study_director_initiation_c','master_schedule_sd_c','test_article_description_c','test_article_name_c','test_system_c','title_description_c'),
        'onload' => true,
        //Actions is a list of actions to fire when the trigger is true
        'actions' => array(
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'name',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'work_product_compliance_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'master_schedule_company_name_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'study_director_initiation_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'master_schedule_sd_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'test_article_description_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'test_article_name_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'test_system_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'title_description_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
        ),
    );
}

/*11 Feb 2021 : #855 */
$dependencies['M03_Work_Product']['required_quoted_protocol_hours_c'] = array(
    'hooks' => array("all") ,
    'trigger' => 'true',
    'triggerFields' => array('functional_area_c','date_entered','id','quoted_protocol_hours_c','quoted_study_management_hrs_c','quoted_reporting_hours_c') ,
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'quoted_protocol_hours_c',
                //'value' => 'and(equal($functional_area_c,"ISR"),isAfter($date_entered,date("2021-02-16")))', 
                'value' => 'or(equal($id,""),and(isAfter($date_entered,date("2021-02-16")),equal($functional_area_c,"ISR")))',              
            ) ,
        ) ,
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'quoted_study_management_hrs_c',
                //'value' => 'and(equal($functional_area_c,"ISR"),isAfter($date_entered,date("2021-02-16")))', 
                'value' => 'or(equal($id,""),and(isAfter($date_entered,date("2021-02-16")),equal($functional_area_c,"ISR")))',
            ) ,
        ) ,
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'quoted_reporting_hours_c',
                //'value' => 'and(equal($functional_area_c,"ISR"),isAfter($date_entered,date("2021-02-16")))', 
                'value' => 'or(equal($id,""),and(isAfter($date_entered,date("2021-02-16")),equal($functional_area_c,"ISR")))',
            ) ,
        ) ,
    ) ,
);


$dependencies['M03_Work_Product']['wpc_required_dep_2'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('work_product_status_c','work_product_code_2_c','number_of_peaks_gcms_c','number_of_peaks_lcms_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'number_of_peaks_gcms_c',
                'value' => 'ifElse(and(
                    equal($work_product_status_c, "Archived"),
                    or(
                        equal($work_product_code_2_c, "AS71"),equal($work_product_code_2_c, "AS72"),equal($work_product_code_2_c, "AS79")
                      )        
                        ),true,false)',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'number_of_peaks_lcms_c',
                'value' => 'ifElse(and(
                    equal($work_product_status_c, "Archived"),
                    or(
                        equal($work_product_code_2_c, "AS71"),equal($work_product_code_2_c, "AS72"),equal($work_product_code_2_c, "AS79")
                      )        
                        ),true,false)',
            ),
        ),
    ),
);

