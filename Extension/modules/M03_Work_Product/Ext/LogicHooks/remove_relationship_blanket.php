<?php

    $hook_array['before_save'][] = Array(
        //Processing index. For sorting the array.
        5,

        //Label. A string value to identify the hook.
        'Remove Blanket Relationship',

        //The PHP file where your class is located.
        'custom/modules/M03_Work_Product/RemoveBlanketRelationship.php',

        //The class the method is in.
        'RemoveBlanketRelationship',

        //The method to call.
        'removeRelationship'
    );



?>
