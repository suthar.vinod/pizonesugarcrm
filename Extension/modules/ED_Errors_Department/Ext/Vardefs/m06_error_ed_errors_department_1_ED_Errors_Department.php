<?php
// created: 2018-02-22 13:58:39
$dictionary["ED_Errors_Department"]["fields"]["m06_error_ed_errors_department_1"] = array (
  'name' => 'm06_error_ed_errors_department_1',
  'type' => 'link',
  'relationship' => 'm06_error_ed_errors_department_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_ed_errors_department_1m06_error_ida',
);
