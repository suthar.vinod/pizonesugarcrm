<?php
 // created: 2019-08-26 17:25:08
$dictionary['Quote']['fields']['name']['audited']=true;
$dictionary['Quote']['fields']['name']['massupdate']=false;
$dictionary['Quote']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Quote']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Quote']['fields']['name']['merge_filter']='disabled';
$dictionary['Quote']['fields']['name']['unified_search']=false;
$dictionary['Quote']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.61',
  'searchable' => true,
);
$dictionary['Quote']['fields']['name']['calculated']=false;

 ?>