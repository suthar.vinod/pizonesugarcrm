<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_PROJECTS_SUBPANEL_TITLE'] = 'Projects';
$mod_strings['LBL_SHIPPING_ACCOUNT_NAME'] = 'Shipping Company Name:';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Company Name:';
$mod_strings['LBL_BILLING_ACCOUNT_NAME'] = 'Company Name';
$mod_strings['LBL_OPPORTUNITY_NAME'] = 'Opportunity Name:';
$mod_strings['LBL_ACCOUNT_ID'] = 'Company Id';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Company Name';
$mod_strings['EXCEPTION_QUOTE_ALREADY_CONVERTED'] = 'Quote Already Converted To Opportunity';
$mod_strings['LBL_STUDY_COMPLIANCE'] = 'Study Compliance';
$mod_strings['LBL_PRODUCT_NAME'] = 'Product Name';
$mod_strings['LBL_QUOTE_DATE'] = 'Quote Date';
$mod_strings['LBL_AMOUNT'] = 'Quote Amount';
$mod_strings['LBL_BILLING_CONTACT_NAME'] = 'Contact Name';
