<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_OW_ACCOUNTNAME'] = 'Company';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Create Opportunity';
$mod_strings['LBL_FORECAST_OPP_COUNT'] = 'Total Opportunity Count';
$mod_strings['LBL_FORECAST_PIPELINE_OPP_COUNT'] = 'Pipeline Opportunity Count';
$mod_strings['LBL_QC_OPPORTUNITY_COUNT'] = 'Opportunity Count:';
$mod_strings['LBL_OW_OPPORTUNITIES'] = 'Opportunity';
$mod_strings['LBL_OW_MODULE_TITLE'] = 'Opportunity Worksheet';
$mod_strings['LBL_FDR_OPPORTUNITIES'] = 'Opportunities in Forecast:';
$mod_strings['LBL_FDR_WEIGH'] = 'Weighted Amount of Opportunities:';
$mod_strings['LBL_DV_FORECAST_OPPORTUNITY'] = 'Forecast Opportunities';
$mod_strings['LBL_LV_OPPORTUNITIES'] = 'Opportunities';
