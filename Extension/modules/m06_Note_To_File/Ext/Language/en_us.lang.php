<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_RECORD'] = 'Create Note_To_File';
$mod_strings['LNK_LIST'] = 'View Notes_To_File';
$mod_strings['LBL_MODULE_NAME'] = 'Notes_To_File';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = '(Inactive) Note To File';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New (Inactive) Note To File';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import (Inactive) Note To File vCard';
$mod_strings['LNK_IMPORT_M06_NOTE_TO_FILE'] = 'Import Note_To_File';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Notes_To_File List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search (Inactive) Note To File';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Notes_To_File';
$mod_strings['LBL_M06_NOTE_TO_FILE_FOCUS_DRAWER_DASHBOARD'] = 'Notes_To_File Focus Drawer';
$mod_strings['LBL_M06_NOTE_TO_FILE_RECORD_DASHBOARD'] = 'Notes_To_File Record Dashboard';
