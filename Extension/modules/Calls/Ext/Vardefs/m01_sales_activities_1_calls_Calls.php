<?php
// created: 2018-12-10 23:01:47
$dictionary["Call"]["fields"]["m01_sales_activities_1_calls"] = array (
  'name' => 'm01_sales_activities_1_calls',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_calls',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_CALLS_FROM_M01_SALES_TITLE',
);
