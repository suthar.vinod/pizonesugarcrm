<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['ERR_DELETE_RECORD'] = 'A record number must be specified to delete the Company.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Company';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Select Company';
$mod_strings['LNK_NEW_ACCOUNT'] = 'New Company';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'New Opportunity';
$mod_strings['LNK_NEW_MEETING'] = 'Schedule APS Activity';
