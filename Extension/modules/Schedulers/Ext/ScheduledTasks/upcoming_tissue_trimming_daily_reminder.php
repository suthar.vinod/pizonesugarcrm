<?php
//Add job in job string
$job_strings['upcoming_tissue_trimming_daily_reminder'] = 'upcoming_tissue_trimming_daily_reminder';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function upcoming_tissue_trimming_daily_reminder()
{
	global $db;
	$current_date = date("Y-m-d"); //current date	
	global $app_list_strings;
	$compliance_dom         	= $app_list_strings['work_product_compliance_list'];	
  
	/**/
	$reportSQL = "SELECT IFNULL(m03_work_product_deliverable.id,'') primaryid,IFNULL(m03_work_product_deliverable.name,'') Deliverable_Name,IFNULL(m03_work_product_deliverable_cstm.timeline_type_c,'') Timeline,IFNULL(l1.id,'') sd_id
	,IFNULL(l1.name,'') sd_name
	,l1_cstm.contact_id_c contact_id,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contact_name,IFNULL(l1_cstm.work_product_compliance_c,'') Compliance,IFNULL(l2.id,'') lead_audi_id
	,IFNULL(l2.first_name,'') l2_first_name
	,IFNULL(l2.last_name,'') l2_last_name
	,IFNULL(l2.salutation,'') l2_salutation,IFNULL(l2.title,'') title,m03_work_product_deliverable_cstm.due_date_c due_date,m03_work_product_deliverable_cstm.work_product_deliverable_not_c notes
	FROM m03_work_product_deliverable
	LEFT JOIN  m03_work_product_m03_work_product_deliverable_1_c l1_1 ON m03_work_product_deliverable.id=l1_1.m03_work_pe584verable_idb AND l1_1.deleted=0	
	LEFT JOIN  m03_work_product l1 ON l1.id=l1_1.m03_work_p0b66product_ida AND l1.deleted=0
	LEFT JOIN  contacts_m03_work_product_2_c l2_1 ON l1.id=l2_1.contacts_m03_work_product_2m03_work_product_idb AND l2_1.deleted=0
	LEFT JOIN  contacts l2 ON l2.id=l2_1.contacts_m03_work_product_2contacts_ida AND l2.deleted=0
	LEFT JOIN m03_work_product_deliverable_cstm m03_work_product_deliverable_cstm ON m03_work_product_deliverable.id = m03_work_product_deliverable_cstm.id_c
	LEFT JOIN m03_work_product_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN contacts contacts1 ON contacts1.id = l1_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
	LEFT JOIN contacts contacts2 ON contacts2.id = l1_cstm.contact_id4_c AND IFNULL(contacts2.deleted,0)=0 
	
	WHERE (((m03_work_product_deliverable_cstm.deliverable_status_c NOT IN ('Completed','Sponsor Retracted','Not Performed') OR  m03_work_product_deliverable_cstm.deliverable_status_c IS NULL 
	) AND ((coalesce(LENGTH(m03_work_product_deliverable_cstm.due_date_c), 0) <> 0)) AND (m03_work_product_deliverable_cstm.deliverable_c = 'Tissue Trimming'
	))) 
	AND  m03_work_product_deliverable.deleted=0 
	 AND IFNULL(contacts1.deleted,0)=0 
	 AND IFNULL(contacts2.deleted,0)=0 
	 ORDER BY CASE WHEN (IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='' OR IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'') IS NULL) THEN 0
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Abnormal Extract' THEN 1
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Data Table' THEN 2
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical ExhaustiveExaggerated Extraction' THEN 3
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Report' THEN 4
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Headspace Analysis' THEN 5
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Interim Validation Report' THEN 6
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Method' THEN 7
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Method Development' THEN 8
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Nickel Ion Release Extraction' THEN 9
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Pre testing' THEN 10
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Protocol Dvelopment' THEN 11
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Specimen Analysis' THEN 12
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Sample Prep' THEN 13
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Simulated Use Extraction' THEN 14
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Stability Analysis' THEN 15
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Validation Report' THEN 16
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Animal Health Report' THEN 17
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Animal Selection Populate TSD' THEN 18
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Animal Selection Procedure Use' THEN 19
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='BC Abnormal Study Article' THEN 20
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Biological Evaluation Plan' THEN 21
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Chemical and Physical Characterization of Particulates' THEN 22
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Contributing Scientist Report Amendment' THEN 23
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Critical Phase Audit' THEN 24
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Decalcification' THEN 25
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Early DeathTermination Investigation Report' THEN 26
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Faxitron' THEN 27
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Final Report' THEN 28
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Final Report Amendment' THEN 29
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Frozen Slide Completion' THEN 30
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='GLP Discontinuation Report' THEN 31
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Gross Morphometry' THEN 32
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Gross Pathology Report' THEN 33
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histology Controls' THEN 34
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histomorphometry' THEN 35
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histopathology Methods Report' THEN 36
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histopathology Report2' THEN 37
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='IACUC Submission' THEN 38
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='In Life Report' THEN 39
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Interim Animal Health Report' THEN 40
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Interim Gross Pathology Report' THEN 41
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Interim Histopathology Report2' THEN 42
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Interim In Life Report' THEN 43
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Interim Histopathology Report' THEN 44
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Interim Report' THEN 45
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Paraffin Slide Completion' THEN 46
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Pathology Unknown' THEN 47
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Pathology Misc' THEN 48
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histopathology Notes' THEN 49
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Pathology Protocol Review' THEN 50
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histopathology Report' THEN 51
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Pharmacokinetic Report' THEN 52
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Pharmacology Data Summary' THEN 53
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='EXAKT Slide Completion' THEN 54
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Plastic Microtome Slide Completion' THEN 55
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Protocol Development' THEN 56
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='QA Data Review' THEN 57
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='QC Data Review' THEN 58
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Recuts' THEN 59
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Regulatory Response' THEN 60
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Risk Assessment Report' THEN 61
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Risk Assessment Report Review' THEN 62
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='SEM' THEN 63
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='SEND Report' THEN 64
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Slide Scanning' THEN 65
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='SpecimenSample Disposition' THEN 66
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Sponsor Contracted Contributing Scientist Report' THEN 67
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Statistical Review' THEN 68
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Statistical Summary' THEN 69
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Statistics Report' THEN 70
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Summary Certificate of ISO 10993' THEN 71
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Summary Certificate of USP Class VI Testing' THEN 72
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Tissue Receipt' THEN 73
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Tissue Trimming' THEN 74
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='TWT Protocol Review' THEN 75
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='TWT Report Review' THEN 76
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Waiting on external test site' THEN 77
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Animal Selection' THEN 78
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histopathology and Histomorphometry Report' THEN 79
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Tissue Shipping' THEN 80
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='SEM Prep' THEN 81
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='SEM Report' THEN 82
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Shipping Request' THEN 83
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Slide Completion' THEN 84
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Slide Shipping' THEN 85 ELSE 86 END ASC
	,due_date ASC";

	$reportResult	= $db->query($reportSQL);
	
	if($reportResult->num_rows>0){
		$site_url		= $GLOBALS['sugar_config']['site_url'];
		//Prepare the Email Template to send to appropriate members
		$template		= new EmailTemplate();
		$emailObj		= new Email();		
		$template->retrieve_by_string_fields(array('name' => 'Upcoming Tissue Trimming WPDs', 'type' => 'email'));
		
		//Email Body Header
		$emailBody = "<table width='100%' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 13px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>Deliverable Name</th><th bgcolor='#b3d1ff' align='left'>Timeline</th><th bgcolor='#b3d1ff' align='left'>Study ID </th><th bgcolor='#b3d1ff' align='left'>Study Director</th><th bgcolor='#b3d1ff' align='left'>Compliance</th><th bgcolor='#b3d1ff' align='left'>Lead Auditor (GLP Only)</th><th bgcolor='#b3d1ff' align='left'>Draft Due Date</th><th bgcolor='#b3d1ff' align='left'>Notes</th></tr>";
					
		while($fetchReportResult = $db->fetchByAssoc($reportResult)) {
			$wpdID					= $fetchReportResult['primaryid'];
			$Deliverable_Name		= $fetchReportResult['Deliverable_Name'];
			$Timeline				= $fetchReportResult['Timeline'];
			$sd_id					= $fetchReportResult['sd_id'];
			$sd_name				= $fetchReportResult['sd_name'];
			$contact_id				= $fetchReportResult['contact_id'];
			$contact_name			= $fetchReportResult['contact_name'];
			$Compliance  			= $compliance_dom[$fetchReportResult['Compliance']]; 
			$lead_audi_id  			= $fetchReportResult['lead_audi_id'];
			$lead_audi_f_name		= $fetchReportResult['l2_first_name'];
			$lead_audi_l_name		= $fetchReportResult['l2_last_name'];
			$due_date  				= date("m/d/Y", strtotime($fetchReportResult['due_date']));
			$notes					= $fetchReportResult['notes'];

				
			$Deliverable_Name		= '<a target="_blank" href="'.$site_url.'/#M03_Work_Product_Deliverable/'.$wpdID.'" >'.$Deliverable_Name . '</a>';			
			$wpLink		= '<a target="_blank" href="'.$site_url.'/#M03_Work_Product/'.$sd_id.'" >'.$sd_name . '</a>';
			$sdLink		= '<a target="_blank" href="'.$site_url.'/#Contacts/'.$contact_id.'" >'.$contact_name . '</a>';
			$lead_audiLink		= '<a target="_blank" href="'.$site_url.'/#Contacts/'.$lead_audi_id.'" >'.$lead_audi_f_name . ' '.$lead_audi_l_name . '</a>';
						
			$emailBody .= "<tr><td>".$Deliverable_Name."</td><td>".$Timeline."</td><td>".$wpLink."</td><td>".$sdLink."</td><td>".$Compliance."</td><td>".$lead_audiLink."</td><td>".$due_date."</td><td>".$notes."</td></tr>";
		}
	
		$emailBody .="</table>";
		$template->body_html = str_replace('[Tissue_Trimming_Report]', $emailBody, $template->body_html);
		//$GLOBALS['log']->fatal("valid email addresses are ". print_r($template->body_html, 1));			
		$defaults		= $emailObj->getSystemDefaultEmail();
		$mail			= new SugarPHPMailer();
		$mail->setMailerForSystem();
		$mail->IsHTML(true);
		$mail->From		= $defaults['email'];
		$mail->FromName = $defaults['name'];
		$mail->Subject	= $template->subject;
		$mail->Body		= $template->body_html;
				
		$mail->AddAddress('anelson@apsemail.com');
		$mail->AddAddress('QA.Auditors@apsemail.com');
		$mail->AddAddress('necropsy@apsemail.com');  
		$mail->AddAddress('jblanch@apsemail.com'); 
		$mail->AddAddress('rhanson@apsemail.com');
		$mail->AddAddress('malhunaidi@apsemail.com');
		//$mail->AddAddress('fxs_mjohnson@apsemail.com'); 
		//If their exist some valid email addresses then send email
					
		if (!$mail->Send()) {
			$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
		} else {
			$GLOBALS['log']->debug('email sent' );
		}
		unset($mail);	
	}
			 
	return true;
}