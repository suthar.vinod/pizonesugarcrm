<?php

//Add job in job string
$job_strings[] = 'communication_requiring_assessment';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function communication_requiring_assessment() {
    global $db;
    $current_date = date("Y-m-d"); //current date
    $wp_emailAdd = array(); // contains the email addresses of assigned to, Report to and Study Director
    //Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];

    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();

    $queryCustom = "SELECT 
                    commu.id AS communicationID, 
                    commu.name AS CommunicationName,  
                    commu_cstm.target_reinspection_date_c, 
					commu_cstm.error_category_c,  					
                    commu_cstm.reinspection_date_c, 
                    commu_wp.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
                    wp.name  AS workProductName 
            FROM `m06_error` AS commu
                    INNER JOIN m06_error_cstm AS commu_cstm
                    ON  commu.id = commu_cstm.id_c

                    RIGHT JOIN m06_error_m03_work_product_1_c AS commu_wp
                    ON  commu.id = commu_wp.m06_error_m03_work_product_1m06_error_ida

                    INNER JOIN m03_work_product AS wp
                    ON  commu_wp.m06_error_m03_work_product_1m03_work_product_idb=wp.id 
		WHERE 
			 commu_cstm.error_category_c != 'Internal Feedback' AND commu_cstm.error_category_c != 'External Feedback' 
			AND
            commu_cstm.target_reinspection_date_c >= '2020-01-30'
            AND
            commu_cstm.target_reinspection_date_c <'" . $current_date . "' 
            AND 
            commu_cstm.reinspection_date_c IS NULL 
            AND commu_wp.m06_error_m03_work_product_1m03_work_product_idb IS NOT NULL

            ORDER BY commu_cstm.target_reinspection_date_c DESC";

    $queryCommResult = $db->query($queryCustom);
    //
     while ($fetchCommResult = $db->fetchByAssoc($queryCommResult)) {

        $workProductID = $fetchCommResult['workProductID'];
        $communicationID = $fetchCommResult['communicationID'];
        $communicationName = $fetchCommResult['CommunicationName'];
        $workProductName = $fetchCommResult['workProductName'];

        if ($workProductID != "" && $communicationID != "") {
            //delete the previous email addresses from the array
            unset($wp_emailAdd);
            unset($wp_emailAddTest); //to Be deleted
            //$wp_emailAdd	= getEmailAddOfAssignedToAndReportTo($fetchResult['workProductID']);

            /* Get Email id of Assigned User and report to User */
            $wp_emailAdd = array();
            $wp_emailAddTest = array(); //to Be deleted
             

            /* Get Study Director */
            
            $query = new SugarQuery();
            $query->from(BeanFactory::newBean('M03_Work_Product'), array('alias' => 'wp', 'team_security' => false));

            $query->joinTable('m03_work_product_cstm', array(
                'joinType' => 'INNER',
                'alias' => 'wp_cstm',
                'linkingTable' => true,
            ))->on()->equalsField('wp.id', 'wp_cstm.id_c');
            $query->select('wp.id', 'wp_cstm.contact_id_c');
            $query->where()
                    ->equals('wp.deleted', 0)
                    ->equals('wp.id', $workProductID);
            $query->limit(1);

            $queryResult = $query->execute();
            $wp_id = $queryResult[0]['id'];
            $study_dir_id = $queryResult[0]['contact_id_c'];

            if ($study_dir_id != "" && $wp_id != "") {

                //Get the primary email address of the selected contact id
                $stdy_bean = BeanFactory::getBean('Contacts', $study_dir_id);

                $primaryEmailAddress = $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
				//$primaryEmailAddress = $stdy_bean->email1;
                if ($primaryEmailAddress != false) {
                    $wp_emailAdd[] = $primaryEmailAddress;
                }
				
				//get Study Director's Manager
				if($stdy_bean->contact_id_c){ 
					$director_id =$stdy_bean->contact_id_c;
					$director_bean = BeanFactory::getBean('Contacts', $director_id);
					if($director_bean->email1){
						$director_email = $director_bean->email1;
						 $wp_emailAdd[] = $director_email;
					}
				}
            }
			
			$wp_emailAdd = array_values($wp_emailAdd); //re-index the array elements
            //remove the repeated email addresses
            $wp_emailAdd = array_unique($wp_emailAdd);

            $template->retrieve_by_string_fields(array('name' => 'Communication Requiring Assessment', 'type' => 'email'));

            $communication_name = '<a target="_blank"href="' . $site_url . '/#M06_Error/' . $communicationID . '">' . $communicationName . '</a>';
            $workProduct_name = '<a target="_blank"href="' . $site_url . '/#M03_Work_Product/' . $workProductID . '">' . $workProductName . '</a>';

            $template->body_html = str_replace('[Communication ID]', $communication_name, $template->body_html);
            $template->body_html = str_replace('[Work Product]', $workProduct_name, $template->body_html);
            //$template->body_html .= implode(",", $wp_emailAdd); // for testing to be deleted
            //$template		= getTemplate($fetchResult, $template, $site_url);
             
			
			$wp_emailAdd[] = 'mconforti@apsemail.com';
            //$wp_emailAdd[] = 'mjohnson@apsemail.com';

            $defaults = $emailObj->getSystemDefaultEmail();
			$mail = new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From = $defaults['email'];
            $mail->FromName = $defaults['name'];
            $mail->Subject = "Test Run - ".$template->subject;
            $mail->Body = $template->body_html;
			
			// for testing to be deleted
			//$wp_emailAddTest[] = 'cjagadeeswaraiah@apsemail.com'; ////to Be deleted
			//$wp_emailAddTest[] = 'vsuthar@apsemail.com'; ////to Be deleted
			// for testing to be deleted
			//$wp_emailAddTest[] = 'vsuthar@apsemail.com'; ////to Be deleted
			
            foreach ($wp_emailAdd as $wmail) { // wp_emailAddTest to be replace with  wp_emailAdd 
                $mail->AddAddress($wmail);
            }
            
			// for testing to be deleted
            //$mail->AddBCC('lsaini@apsemail.com');
			$mail->AddBCC('cjagadeeswaraiah@apsemail.com');
			$mail->AddBCC('vsuthar@apsemail.com');
            //If their exist some valid email addresses then send email
            if (!empty($wp_emailAdd)) {
                //$GLOBALS['log']->fatal("valid email addresses are ". print_r($wpd_emailAdd, 1));
                if (!$mail->Send()) {
                    $GLOBALS['log']->debug("Fail to Send Email, Please check settings");
                } else {
                    $GLOBALS['log']->debug('email sent for ' . $communicationID);
                }
				unset($mail);
            }
        }
    }

    /// For Reassement Communication Workflow
    $queryCustomReassement = "SELECT 
					commu.id AS communicationID, 
					commu.name AS CommunicationName,  
					 commu_cstm.error_category_c,  
					commu_cstm.target_sd_reassessment_date_c,  
					commu_cstm.actual_sd_reassessment_date_c, 
					commu_wp.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
					wp.name  AS workProductName 
				FROM `m06_error` AS commu
					INNER JOIN m06_error_cstm AS commu_cstm
					ON  commu.id = commu_cstm.id_c
					
					RIGHT JOIN m06_error_m03_work_product_1_c AS commu_wp
					ON  commu.id = commu_wp.m06_error_m03_work_product_1m06_error_ida
					
					INNER JOIN m03_work_product AS wp
					ON  commu_wp.m06_error_m03_work_product_1m03_work_product_idb=wp.id 
				WHERE 
				commu_cstm.error_category_c != 'Internal Feedback' AND commu_cstm.error_category_c != 'External Feedback'
				AND
				commu_cstm.reassessment_required_c=1 AND  
				commu_cstm.target_sd_reassessment_date_c<'" . $current_date . "' 
				AND 
				commu_cstm.actual_sd_reassessment_date_c IS NULL 
				AND commu_wp.m06_error_m03_work_product_1m03_work_product_idb IS NOT NULL
				AND YEAR(commu_cstm.target_sd_reassessment_date_c) = YEAR(CURDATE()) 
				ORDER BY commu_cstm.target_sd_reassessment_date_c DESC";
 
    $queryResultReassement = $db->query($queryCustomReassement);

    while ($fetchResultReassement = $db->fetchByAssoc($queryResultReassement)) {
        $re_workProductID = $fetchResultReassement['workProductID'];
        $re_communicationID = $fetchResultReassement['communicationID'];
        $re_communicationName = $fetchResultReassement['CommunicationName'];
        $re_workProductName = $fetchResultReassement['workProductName'];

        if ($re_workProductID != "" && $re_communicationID != "") {
            //delete the previous email addresses from the array
            unset($wp_emailAdd);
            unset($wp_emailAddTest);
            //$wp_emailAdd	= getEmailAddOfAssignedToAndReportTo($fetchResultReassement['workProductID']);
            //$wp_emailAdd[]	= getTheStudyDirectorOfRelatedWP($fetchResultReassement['workProductID']);
            //===============================
            //$wp_emailAdd	= getEmailAddOfAssignedToAndReportTo($fetchResult['workProductID']);

            /* Get Email id of Assigned User and report to User */
            $ids = array(); //Array to maintain ids of Assigned user and Report to user
            $email_adds = array(); //Array to maintain the Email Addresses of Assigned user and Report to user
            $wp_emailAdd = array();
            $wp_emailAddTest = array(); //to Be deleted
             

            /* Get Study Director */
            //$wp_emailAdd[]	= getTheStudyDirectorOfRelatedWP($fetchResult['workProductID']);

            $query = new SugarQuery();
            $query->from(BeanFactory::newBean('M03_Work_Product'), array('alias' => 'wp', 'team_security' => false));

            $query->joinTable('m03_work_product_cstm', array(
                'joinType' => 'INNER',
                'alias' => 'wp_cstm',
                'linkingTable' => true,
            ))->on()->equalsField('wp.id', 'wp_cstm.id_c');
            $query->select('wp.id', 'wp_cstm.contact_id_c');
            $query->where()
                    ->equals('wp.deleted', 0)
                    ->equals('wp.id', $re_workProductID);
            $query->limit(1);

            $queryResult = $query->execute();
            $wp_id = $queryResult[0]['id'];
            $study_dir_id = $queryResult[0]['contact_id_c'];

            if ($study_dir_id != "" && $wp_id != "") {

                //Get the primary email address of the selected contact id
                $stdy_bean = BeanFactory::getBean('Contacts', $study_dir_id);

                $primaryEmailAddress = $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
                if ($primaryEmailAddress != false) {
                     $wp_emailAdd[] = $primaryEmailAddress;
                }
				
				//get Study Director's Manager
				if($stdy_bean->contact_id_c){ 
					$director_id =$stdy_bean->contact_id_c;
					$director_bean = BeanFactory::getBean('Contacts', $director_id);
					if($director_bean->email1){
						$director_email = $director_bean->email1;
						 $wp_emailAdd[] = $director_email;
					}
				}
            }
			
			 //$template		= getTemplate($fetchResultReassement, $template, $site_url);
            $wp_emailAdd = array_values($wp_emailAdd); //re-index the array elements
            //remove the repeated email addresses
            $wp_emailAdd = array_unique($wp_emailAdd);


            $template->retrieve_by_string_fields(array('name' => 'Communication Requiring Reassessment', 'type' => 'email'));

            $communication_name = '<a target="_blank"href="' . $site_url . '/#M06_Error/' . $re_communicationID . '">' . $re_communicationName . '</a>';
            $workProduct_name = '<a target="_blank"href="' . $site_url . '/#M03_Work_Product/' . $re_workProductID . '">' . $re_workProductName . '</a>';

            $template->body_html = str_replace('[Communication ID]', $communication_name, $template->body_html);
            $template->body_html = str_replace('[Work Product]', $workProduct_name, $template->body_html);
            //$template->body_html .= implode(",", $wp_emailAdd); // For testing purpose only , to be deleted
             
			$wp_emailAdd[] = 'mconforti@apsemail.com';
			
            $defaults = $emailObj->getSystemDefaultEmail();
			$mail = new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From = $defaults['email'];
            $mail->FromName = $defaults['name'];
			$mail->Subject = "Test Run - ".$template->subject;
            $mail->Body = $template->body_html;
			
			//$wp_emailAddTest[] = 'cjagadeeswaraiah@apsemail.com'; ////to Be deleted
			//$wp_emailAddTest[] = 'vsuthar@apsemail.com';
			
            foreach ($wp_emailAdd as $wmail) {
                $mail->AddAddress($wmail);
            }
             
            $mail->AddBCC('cjagadeeswaraiah@apsemail.com');
			$mail->AddBCC('vsuthar@apsemail.com');
			//$mail->AddBCC('lsaini@apsemail.com');
            //If their exist some valid email addresses then send email
            if (!empty($wp_emailAdd)) {
                //$GLOBALS['log']->fatal("valid email addresses are ". print_r($wpd_emailAdd, 1));
                if (!$mail->Send()) {
                    $GLOBALS['log']->debug("Fail to Send Email, Please check settings");
                } else {
                    $GLOBALS['log']->debug('email sent for ' . $re_communicationID);
                }
				unset($mail);
            }
        }
    }
   return true;
}
?>
