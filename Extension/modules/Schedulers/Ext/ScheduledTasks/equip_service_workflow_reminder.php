<?php
//Add job in job string

use function PHPSTORM_META\type;

$job_strings[] = 'equip_service_workflow_reminder';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function equip_service_workflow_reminder()
{
  global $db;
  $current_date = date("Y-m-d"); //current date

  $site_url = $GLOBALS['sugar_config']['site_url'];
  $domainToSearch = 'apsemail.com';

  $efIdArray = array();
  $eArr = array();
  $queryCustom = "SELECT id FROM efr_equipment_facility_recor where DATE_ADD(date(date_entered), INTERVAL 30 DAY) = '$current_date' and deleted=0";


  $efrQuery = $db->query($queryCustom);

  while ($fetchResult = $db->fetchByAssoc($efrQuery)) {
    $efrId = $fetchResult['id'];

    $sql = "SELECT equip_equi01e2uipment_ida FROM equip_equipment_efr_equipment_facility_recor_2_c where equip_equiffd2y_recor_idb='$efrId' and deleted=0";

    $sqlResult = $db->query($sql);
    $fetchResult1 = $db->fetchByAssoc($sqlResult);
    $efId = $fetchResult1['equip_equi01e2uipment_ida'];
    if ($efId != "") {
      $efIdArray[] = $efId;
    }
  }
  $efIdArray = array_unique($efIdArray);

  $p = 0;
  foreach ($efIdArray as $eId) {
    $p = 0;
    $efBean = BeanFactory::retrieveBean('Equip_Equipment', $eId, array('disable_row_level_security' => true));
    $efBean->load_relationship('equip_equipment_efr_equipment_facility_recor_2');
    $relateEfrIds = $efBean->equip_equipment_efr_equipment_facility_recor_2->get();
    if ($efBean->status_c != 'Retired') {
      foreach ($relateEfrIds as $efrrId) {
        $efBean = BeanFactory::retrieveBean('EFR_Equipment_Facility_Recor', $efrrId, array('disable_row_level_security' => true));
        if ($p == 1)
          continue;
        if (strpos($efBean->type_2_c, 'Initial In Service') && (!strpos($efBean->type_2_c, 'Initial In Service 2'))) {
          $p = 1;
        } else {
          if (strpos($efBean->type_2_c, 'Initial In Service 2'))
            $eArr[$efrrId] = $eId;
        }
      }
    }
    if ($p > 0) {
      foreach ($eArr as $value) {
        $key = array_search($eId, $eArr);
        unset($eArr[$key]);
      }
    }
  }

  foreach ($eArr as $erfval => $efVal) {
    $efrBean = BeanFactory::retrieveBean('EFR_Equipment_Facility_Recor', $erfval, array('disable_row_level_security' => true));
    $efrName = $efrBean->name;
    $efBean = BeanFactory::retrieveBean('Equip_Equipment', $efVal, array('disable_row_level_security' => true));
    $department_name = $efBean->department_c;
    $efName = $efBean->name;

    //delete the previous email addresses from the array
    unset($ef_emailAdd);
    $ef_emailAdd = array();

    $userEmailsSql = 'SELECT id_c FROM contacts_cstm where equipment_owner_for_dept_c LIKE "%^' . $department_name . '^%"';
    $userEmailsSqlResult = $db->query($userEmailsSql);

    while ($fetchResultEmail = $db->fetchByAssoc($userEmailsSqlResult)) {
      $contactID = $fetchResultEmail['id_c'];
      $Contact_bean = BeanFactory::getBean('Contacts', $contactID);

      $primaryEmailAddress = $Contact_bean->emailAddress->getPrimaryAddress($Contact_bean);
      if ($primaryEmailAddress != false) {
        $ef_emailAdd[] = $primaryEmailAddress;
      }

      //get Manager
      if ($Contact_bean->contact_id_c) {
        $director_id = $Contact_bean->contact_id_c;
        $director_bean = BeanFactory::getBean('Contacts', $director_id);
        if ($director_bean->email1) {
          $director_email = $director_bean->email1;
          $ef_emailAdd[] = $director_email;
        }
      }
    }

    $ef_emailAdd = array_values($ef_emailAdd); //re-index the array elements    
    $ef_emailAdd = array_unique($ef_emailAdd); //remove the repeated email addresses

    //Send Email/
    ///Upload Document Reminder Template 
    $template = new EmailTemplate();
    $emailObj = new Email();

    $template->retrieve_by_string_fields(array('name' => 'Initial In Service - Completed In Service workflow reminder for EF', 'type' => 'email'));

    $efrLink = '<a target="_blank"href="' . $site_url . '/#EFR_Equipment_Facility_Recor/' . $erfval . '">' . $efrName . '</a>';
    $efLink = '<a target="_blank"href="' . $site_url . '/#Equip_Equipment/' . $efVal . '">' . $efName . '</a>';

    $template->body_html = str_replace('[EFR_LINK]', $efrLink, $template->body_html);
    $template->body_html = str_replace('[EF_LINK]', $efLink, $template->body_html);

    $defaults  = $emailObj->getSystemDefaultEmail();
    $mail    = new SugarPHPMailer();
    $mail->setMailerForSystem();
    $mail->IsHTML(true);
    $mail->From    = $defaults['email'];
    $mail->FromName = $defaults['name'];
    $mail->Subject  = "Equipment & Facility In Service Reminder";
    $mail->Body    = $template->body_html;

    $mail->AddAddress('emarkuson@apsemail.com');

    foreach ($ef_emailAdd as $wmail) {
      $mail->AddAddress($wmail);
    }

    //If their exist some valid email addresses then send email
    if (!$mail->Send()) {
      $GLOBALS['log']->fatal("Fail to Send Email, Please check settings 175");
    } else {
      $GLOBALS['log']->fatal('email sent 175 ');
    }
    unset($mail);
  }
  return true;
}
