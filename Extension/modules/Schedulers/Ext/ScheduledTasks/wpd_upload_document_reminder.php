<?php
//Add job in job string
$job_strings[] = 'wpd_upload_document_reminder';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function wpd_upload_document_reminder() {
    global $db;
    $current_date = date("Y-m-d"); //current date
	 
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];
	$domainToSearch = 'apsemail.com';
 

    $queryCustom = "SELECT  WPD.id AS wpd_id,
							WPD.name AS wpd_name,
							WPD.date_entered,
							WPDCSTM.deliverable_c, 
							WPDCSTM.due_date_c, 
							WP_WPD.m03_work_p0b66product_ida AS workProductID,
							WP.name AS workProductName,
							WP.assigned_user_id AS Assigneduser,
							WPCSTM.contact_id_c AS StudyDirector,
							WPCSTM.first_procedure_c  
					FROM `m03_work_product_deliverable` AS WPD
						LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
							ON WPD.id= WPDCSTM.id_c
						RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
							ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
						LEFT JOIN m03_work_product AS WP
							ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
						LEFT JOIN m03_work_product_cstm AS WPCSTM
							ON WP.id=WPCSTM.id_c
							WHERE (((((WPDCSTM.deliverable_c = 'SpecimenSample Disposition'
									))) AND (((WPDCSTM.type_2_c IN ('Paraffin Blocks','Slide')
									) OR WPD.filename IS NULL OR WPDCSTM.attn_to_name_c IS NULL OR WPDCSTM.address_street_c IS NULL OR WPDCSTM.address_city_c IS NULL OR WPDCSTM.address_state_c IS NULL OR WPDCSTM.address_postalcode_c IS NULL OR WPDCSTM.address_country_c IS NULL OR WPDCSTM.phone_number_c IS NULL OR WPDCSTM.address_confirmed_c IS NULL OR WPDCSTM.receiving_party_expecting_c IS NULL OR WPDCSTM.faxitron_required_c IS NULL
									)) AND (((WPDCSTM.type_2_c IN ('Paraffin Blocks','Slide')
									) OR WPDCSTM.attn_to_name_c IS NULL OR WPDCSTM.address_street_c IS NULL OR WPDCSTM.address_city_c IS NULL OR WPDCSTM.address_state_c IS NULL OR WPDCSTM.address_postalcode_c IS NULL OR WPDCSTM.address_country_c IS NULL OR WPDCSTM.phone_number_c IS NULL OR WPDCSTM.address_confirmed_c IS NULL OR WPDCSTM.receiving_party_expecting_c IS NULL OR WPDCSTM.faxitron_required_c IS NULL
									))))
						AND WPDCSTM.due_date_c>'" . $current_date . "'
						AND WPD.date_entered>'2021-10-21'
						AND WPD.deleted = 0 
					ORDER BY WPD.date_entered ASC";

    $queryWPDResult = $db->query($queryCustom);
	
	
	 while($fetchResult = $db->fetchByAssoc($queryWPDResult)) {

        $wpd_id			= $fetchResult['wpd_id'];
		$wpdName		= $fetchResult['wpd_name'];
		$workProductID	= $fetchResult['workProductID'];
		$workProductName	= $fetchResult['workProductName'];
		$assignedUser_id	= $fetchResult['Assigneduser']; 
        $SD_ID				= $fetchResult['StudyDirector'];
		$dueDate		= $fetchResult['due_date_c']; 
		
		
        if (($SD_ID != "" || $assignedUser_id!="" ) && $workProductID != "" && $wpd_id != "" && $dueDate!="" ) {
			
			$UserName 			= "";
			$UserAddress	 	= "";
			$studyDirectorName	= "";
			$SDEmailAddress 	= "";
			
			/*Get Assigned User*/
			if($assignedUser_id!=""){
				$user_bean		= BeanFactory::getBean('Employees', $assignedUser_id);
				$UserName		= $user_bean->name;
				$UserAddress	= $user_bean->emailAddress->getPrimaryAddress($user_bean);
			}	
			
			/*Get Study Director*/
			if($SD_ID!=""){
				$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
			}	
			
			
			$date1= strtotime($dueDate);
			$date2= strtotime($current_date);

			$diff = abs($date1-$date2);
			$days = floor(($diff)/ (60*60*24)); 
			$wp_emailAdd_dev = array();
				
			if(($date1>$date2 && $days==14)){
					//Send Email/
					///Upload Document Reminder Template 
					$template = new EmailTemplate();
					$emailObj = new Email();
					
					$template->retrieve_by_string_fields(array('name' => 'Upload Document Reminder Template', 'type' => 'email'));
					
					$wpdLink = '<a target="_blank"href="'.$site_url.'/#M03_Work_Product_Deliverable/'.$wpd_id.'">'.$wpdName.'</a>';
					 
					$WP_MGT = ($UserName!="")?$UserName:$studyDirectorName;
					$template->body_html = str_replace('[WP_MGT]', $WP_MGT, $template->body_html);
					$template->body_html = str_replace('[WPD_ID]', $wpdLink, $template->body_html);
					
					$defaults	= $emailObj->getSystemDefaultEmail();
					$mail		= new SugarPHPMailer();
					$mail->setMailerForSystem();
					$mail->IsHTML(true);
					$mail->From		= $defaults['email'];
					$mail->FromName = $defaults['name'];
					$mail->Subject	= "Reminder - Complete fields on ".$wpdName;
					$mail->Body		= $template->body_html;
							 
					 
					$wp_emailAdd[] = 'operationssupport@apsemail.com';
					
					if(!empty($UserAddress))
						$mail->AddAddress($UserAddress);
					if(!empty($SDEmailAddress))
						$mail->AddAddress($SDEmailAddress);
					$mail->AddAddress('operationssupport@apsemail.com');
					//$mail->AddAddress('vsuthar@apsemail.com');
					
					 
					//If their exist some valid email addresses then send email
					if (!empty($wp_emailAdd)) {
						if (!$mail->Send()) {
							$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
						} else {
							$GLOBALS['log']->debug('email sent');
						}
						unset($mail);
					}
				} 	
		}
	}
	
	
	 return true;
}
