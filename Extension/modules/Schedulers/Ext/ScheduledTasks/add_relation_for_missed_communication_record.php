<?php
//Add job in job string
$job_strings[] = 'add_relation_for_missed_communication_record';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function add_relation_for_missed_communication_record()
{
	global $db;
	$emailObj = new Email();
	$related_dataArr		= array();
	$related_ErrorDataArr	= array();
	$mailContent 			= "";
	$current_date	= date("Y-m-d"); //current date
	$current_time	= date("Y-m-d H:i:s");
	$startTime 		= strtotime("-10 minutes", strtotime($current_time));
	$start_time		= date("Y-m-d H:i:s", $startTime);

	$sql = "SELECT id FROM `m06_error` AS COMM LEFT JOIN m06_error_cstm AS COMM_CSTM
	ON COMM.id = COMM_CSTM.id_c WHERE deleted=0 AND `date_entered`>='" . $start_time . "' AND `date_entered`<'" . $current_time . "' AND COMM_CSTM.related_text_c IS NOT NULL AND commapp_mismatch_c IS NULL";
	$resultComm = $db->query($sql);
	if ($resultComm->num_rows > 0) {
		while ($rowAllRecord = $db->fetchByAssoc($resultComm)) {
			$commID = $rowAllRecord['id'];

			$commBean   	= BeanFactory::getBean('M06_Error', $commID);
			$commName 		= $commBean->name;
			$commrelated 	= $commBean->related_text_c;
			$workProduct	= $commBean->work_products;

			$related_dataArr		= array();

			$res = str_ireplace(array('\"', '"', '}', '{', '[', ']', '\\', '""'), '', $commrelated);

			$dataArr = explode(",", $res);
			foreach ($dataArr as $resid) {
				$resid = preg_replace('/[^a-zA-Z0-9_ -:]/s', '', $resid);
				$residd = explode(":", $resid);
				$related_dataArr[str_replace(array('quot', '&'), '', $residd[0])] =  str_replace(array('quot', '&'), '', $residd[1]);
			}

			if ($workProduct != "")
			{
				$wp = str_ireplace(array('\"', '[', ']', '\\', 'quot', '&;','"'), '', $workProduct);
				$related_dataArr['M03_Work_Product'] = str_replace(array('quot','&','"'),'',$wp);
			}
			foreach ($related_dataArr as $key => $valueID) {
				if ($valueID != "") {
					if ($key == 'ANML_Animals') {
						$TSBean  = BeanFactory::getBean('ANML_Animals', $valueID);

						if ($TSBean->name != "") {
							$checkQuery = "SELECT * FROM m06_error_anml_animals_1_c where deleted=0 AND m06_error_anml_animals_1m06_error_ida='" . $commID . "' and  m06_error_anml_animals_1anml_animals_idb='" . $valueID . "'";
							$resultTestSystem = $db->query($checkQuery);
							if ($resultTestSystem->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_anml_animals_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
							//echo "<br>==>Animal Not available";
						}
					}

					if ($key == 'M03_Work_Product') {

						$WPBean = BeanFactory::getBean('M03_Work_Product', $valueID);
						if ($WPBean->name != "") {
							$checkQuery = "SELECT * FROM `m06_error_m03_work_product_1_c` where deleted=0 AND m06_error_m03_work_product_1m06_error_ida='" . $commID . "' and  m06_error_m03_work_product_1m03_work_product_idb='" . $valueID . "'";
							$resultTestSystem = $db->query($checkQuery);
							if ($resultTestSystem->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_m03_work_product_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
						}
					}


					if ($key == 'Erd_Error_Documents') {
						$ERDBean = BeanFactory::getBean('Erd_Error_Documents', $valueID);
						if ($ERDBean->name != "") {
							$checkQuery = "SELECT * FROM `m06_error_erd_error_documents_1_c` where deleted=0 AND  m06_error_erd_error_documents_1m06_error_ida='" . $commID . "' and  m06_error_erd_error_documents_1erd_error_documents_idb='" . $valueID . "'";
							$resultEDoc = $db->query($checkQuery);
							if ($resultEDoc->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_erd_error_documents_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
						}
					}

					if ($key == 'Equip_Equipment') {
						$EFBean = BeanFactory::getBean('Equip_Equipment', $valueID);
						if ($EFBean->name != "") {
							$checkQuery = "SELECT * FROM `m06_error_equip_equipment_1_c` where deleted=0 AND  m06_error_equip_equipment_1m06_error_ida='" . $commID . "' and  m06_error_equip_equipment_1equip_equipment_idb='" . $valueID . "'";
							$resultEquip = $db->query($checkQuery);
							if ($resultEquip->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_equip_equipment_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
						}
					}

					if ($key == 'RMS_Room') {
						$RMSBean = BeanFactory::getBean('RMS_Room', $valueID);
						if ($RMSBean->name != "") {
							$checkQuery = "SELECT * FROM `m06_error_rms_room_1_c` where deleted=0 AND  m06_error_rms_room_1m06_error_ida='" . $commID . "' and  m06_error_rms_room_1rms_room_idb='" . $valueID . "'";
							$resultRoom = $db->query($checkQuery);
							if ($resultRoom->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_rms_room_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
						}
					}
				}
			}

			$recordContent = "";

		if (count($addRelationship) > 0) {
			$mailContent .= "<br><br>For Communication ".$commName.", Missing records for relationships<br>";
			$recordContent .= "For Communication ".$commName.", Missing records for relationships \n";
			foreach ($addRelationship as $moduleRel => $recordID) {
				$mailContent .= $moduleRel . "==" . $recordID . "</br>";
				$recordContent .= $moduleRel . "==" . $recordID." \n";
				//$commBean->load_relationship($moduleRel);
				//$commBean->$moduleRel->add($recordID);
				//$commBean->save();
			}
		}

		if (count($related_ErrorDataArr) > 0) {
			$mailContent .= "<br><br>For Communication ".$commName.",Following records didnot find in System :<br>";
			$recordContent .= "For Communication ".$commName.",Following records didnot find in System :  \n";
			foreach ($related_ErrorDataArr as $moduleRel => $recordID) {
				$mailContent .= $moduleRel . "==" . $recordID . "</br>";
				$recordContent .= $moduleRel . "==" . $recordID." \n";
			}
		}

		if($recordContent!=""){
			$updateSql = "UPDATE `m06_error_cstm` SET `commapp_mismatch_c` = 'Cron Status : ".$recordContent."' WHERE `id_c` = '" . $commID . "' ";
			$db->query($updateSql);
		}else{
			$updateSql = "UPDATE `m06_error_cstm` SET `commapp_mismatch_c` = 'Relationship not required' WHERE `id_c` = '" . $commID . "' ";
			$db->query($updateSql);
		}
		}
	}

	if ($mailContent != "") {		
		$defaults = $emailObj->getSystemDefaultEmail();
		$mail = new SugarPHPMailer();
		$mail->setMailerForSystem();
		$mail->IsHTML(true);
		$mail->From = $defaults['email'];
		$mail->FromName = $defaults['name'];
		$mail->Subject = "Missed Relationship from Comm App";
		$mail->Body = $mailContent;

		$mail->AddCC('vsuthar@apsemail.com');
		$emailAdd[] = 'vsuthar@apsemail.com';
		//If their exist some valid email addresses then send email
		if (!empty($emailAdd)) {
			if (!$mail->Send()) {
				$GLOBALS['log']->fatal("Fail to sent mail for Missed Relationship from Comm App");
			} else {
				$GLOBALS['log']->fatal('Successfully mail sent for Missed Relationship from Comm App');
			}
			unset($mail);
		}
	}

	return true;
}