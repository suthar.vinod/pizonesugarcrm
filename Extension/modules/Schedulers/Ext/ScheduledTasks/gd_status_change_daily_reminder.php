<?php
//Add job in job string
$job_strings['gd_status_change_daily_reminder'] = 'gd_status_change_daily_reminder';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function gd_status_change_daily_reminder()
{
	global $db;
	$current_date = date("Y-m-d"); //current date	
	$yesterdaydate	= date("Y-m-d", strtotime("-1 days", strtotime($current_date)));
	$current_date	= $current_date . ' 05:59:59';
	$yesterdaydate	= $yesterdaydate . ' 06:00:00';
	$GLOBALS['log']->fatal("yesterdaydate ". $yesterdaydate);	
	$GLOBALS['log']->fatal("current_date ". $current_date);	

	/**/
	$reportSQL = "SELECT IFNULL(gd_group_design.id,'') primaryid
	,IFNULL(gd_group_design.name,'') gd_name
	,IFNULL(gd_group_design_cstm.status_c,'') GD_GROUP_DESIGN_CSTM_SA89250,IFNULL(gd_group_design.id,'') gd_group_design_id
	,l1_cstm.contact_id_c contact_id,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contacts_name,
	gd_audit.after_value_string AS after_value,
	gd_audit.date_created AS Date_c
	FROM gd_group_design
	 INNER JOIN  m03_work_product_gd_group_design_1_c l1_1 ON gd_group_design.id=l1_1.m03_work_product_gd_group_design_1gd_group_design_idb AND l1_1.deleted=0
	
	 INNER JOIN  m03_work_product l1 ON l1.id=l1_1.m03_work_product_gd_group_design_1m03_work_product_ida AND l1.deleted=0
	LEFT JOIN gd_group_design_cstm gd_group_design_cstm ON gd_group_design.id = gd_group_design_cstm.id_c
	LEFT JOIN m03_work_product_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN contacts contacts1 ON contacts1.id = l1_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
	LEFT JOIN gd_group_design_audit gd_audit ON gd_group_design.id = gd_audit.parent_id
	 WHERE (((l1_cstm.test_system_c IN ('Bovine','Canine','Caprine','Micro Yucatans','Ossabaw','Ovine','PorcineGeneral','Porcine Got','Porcine','Porcine Yuc','Porcine and Bovine','Porcine_Canine_Ovine','Yorkshire Yucatan')
	))) 
	AND gd_audit.after_value_string ='complete'
	AND (gd_audit.date_created - INTERVAL 360 MINUTE >= '" . $yesterdaydate . "' AND gd_audit.date_created - INTERVAL 360 MINUTE <= '" . $current_date . "')
	AND  gd_group_design.deleted=0 
	 AND IFNULL(contacts1.deleted,0)=0 group by gd_audit.parent_id ORDER BY gd_name ASC";
	 $GLOBALS['log']->fatal("reportSQL ". $reportSQL);	
	$reportResult	= $db->query($reportSQL);

	if ($reportResult->num_rows > 0) {
		$site_url		= $GLOBALS['sugar_config']['site_url'];
		//Prepare the Email Template to send to appropriate members
		$template		= new EmailTemplate();
		$emailObj		= new Email();
		$template->retrieve_by_string_fields(array('name' => 'Completed Group Design for Review', 'type' => 'email'));
		$wp_emailAdd = array();
		//Email Body Header
		$emailBody = "<table width='50%' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 13px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>Group Designs</th></tr>";
		while ($fetchReportResult = $db->fetchByAssoc($reportResult)) {
			$GDID					= $fetchReportResult['primaryid'];
			$GD_Name				= $fetchReportResult['gd_name'];
			$SD_ID				 	= $fetchReportResult['contact_id'];

			/*Get Study Director*/
			if ($SD_ID != "") {
				$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
				$wp_emailAdd[] 		= $SDEmailAddress;
			}
			$wp_emailAdd = array_values($wp_emailAdd); //re-index the array elements
			$wp_emailAdd = array_unique($wp_emailAdd);
			$Group_Design_Name		= '<a target="_blank" href="' . $site_url . '/#GD_Group_Design/' . $GDID . '" >' . $GD_Name . '</a>';

			$emailBody .= "<tr><td>" . $Group_Design_Name . "</td></tr>";
		}

		$emailBody .= "</table>";
		$template->body_html = str_replace('[Completed_GD_Report_First]', $emailBody, $template->body_html);
		//$GLOBALS['log']->fatal("valid email addresses are ". print_r($template->body_html, 1));			
		$defaults		= $emailObj->getSystemDefaultEmail();
		$mail			= new SugarPHPMailer();
		$mail->setMailerForSystem();
		$mail->IsHTML(true);
		$mail->From		= $defaults['email'];
		$mail->FromName = $defaults['name'];
		$mail->Subject	= $template->subject;
		$mail->Body		= $template->body_html;		

		$mail->AddAddress('adargis@apsemail.com');
		$mail->AddAddress('nuitermarkt@apsemail.com');
		//$mail->AddAddress('arice@apsemail.com');
		//$mail->AddAddress('fxs_mjohnson@apsemail.com');

		foreach ($wp_emailAdd as $wmail) { 
			$mail->AddAddress($wmail); 
		}

		//If their exist some valid email addresses then send email
		if (!$mail->Send()) {
			$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
		} else {
			$GLOBALS['log']->debug('email sent');
		}
		unset($mail);
	}

	return true;
}
