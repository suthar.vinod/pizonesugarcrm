<?php

//Add job in job string
array_push($job_strings, 'overdue_deliverables');

/* * This function will found out all the Work Product Deliverables whose
 * Internal Final Due Date has passed and will send the emails to "Assigned To", "Report to"
 * and Study Directors or related Work Products.
 */

function overdue_deliverables() {

    $deliverable_status = array('Completed', 'Sponsor Retracted', 'None', 'Not Performed'); //Array of those statuses at which email will not trigger
    $current_date = date("Y-m-d"); //current date
    $wpd_emailAdd = array(); // contains the email addresses of assigned to, Report to and Study Director
    //Query to get the "Internal Final Due Date" and "Deliverable Status" of Work Product Deliverables
    $query = new SugarQuery();
    $query->from(BeanFactory::newBean('M03_Work_Product_Deliverable'), array('alias' => 'wpd', 'team_security' => false));
    $query->joinTable('m03_work_product_deliverable_cstm', array(
        'joinType' => 'INNER',
        'alias' => 'wpd_cstm',
        'linkingTable' => true,
    ))->on()->equalsField('wpd.id', 'wpd_cstm.id_c');
    $query->select('wpd.id', 'wpd.name', 'wpd_cstm.internal_final_due_date_c', 'wpd_cstm.deliverable_status_c');
    $query->where()
            ->notIn('wpd_cstm.deliverable_status_c', $deliverable_status)
            ->lt('wpd_cstm.internal_final_due_date_c', $current_date)
            ->notNull('wpd_cstm.internal_final_due_date_c')
            ->isNotEmpty('wpd_cstm.internal_final_due_date_c');

    $queryCompile = $query->compile();
    $queryResult = $query->execute();

    //Get site url so that appropriate Work Product Delieverable can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];

    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();

    //We will send individual Emails for individual Work Product Deliverables
    foreach ($queryResult as $key => $values) {
        //delete the previous email addresses from the array
        unset($wpd_emailAdd);
        $template = getTemplate($values, $template, $site_url);
        $wpd_emailAdd = getEmailAddOfAssignedToAndReportTo($values['id']);
        $wpd_emailAdd[] = getTheStudyDirectorOfRelatedWP($values['id']);

        //verify all the email addresses should belongs to valid domain
        foreach ($wpd_emailAdd as $key => $emails) {
            $status = verifyContactForNotification($emails);

            //email address belongs to invalid domain
            if ($status == false) {
                unset($wpd_emailAdd[$key]);
            }
        }
        $wpd_emailAdd = array_values($wpd_emailAdd); //re-index the array elements
        //remove the repeated email addresses
        $wpd_emailAdd = array_unique($wpd_emailAdd);
       //   if ($values['id'] == 'eb144ebe-7c95-11e9-b5b7-06e41dba421a') {
        sendMail($values['id'], $template, $wpd_emailAdd);
       //   }
    }


    return true;
}

/**
 * Get the overdue email template and set the appropriate values
 * @param type $values
 * @param type $template
 * @param type $site_url
 * @return type
 */
function getTemplate($values, $template, $site_url) {
    $template->retrieve_by_string_fields(array('name' => 'Overdue Deliverable', 'type' => 'email'));


    $wpd_name = '<a target="_blank"href="' . $site_url . '/#M03_Work_Product_Deliverable/' . $values['id'] . '">' . $values['name'] . '</a>';

    $template->body_html = str_replace('[insert Work Product Deliverable name]', $wpd_name, $template->body_html);
    return $template;
}

/**
 * This function will send the emails to the "Assigned To", "Report to" and the study director
 * of Work Product
 * @param type $template
 * @param type $wpd_emailAdd
 */
function sendMail($id, $template, $wpd_emailAdd) {

    $emailObj = new Email();
    $defaults = $emailObj->getSystemDefaultEmail();
    $mail = new SugarPHPMailer();
    $mail->setMailerForSystem();
    $mail->IsHTML(true);
    $mail->From = $defaults['email'];
    $mail->FromName = $defaults['name'];

    $mail->Subject = $template->subject;


    $mail->Body = $template->body_html;


    foreach ($wpd_emailAdd as $wmail) {
        $mail->AddAddress($wmail);
    }

    //If their exist some valid email addresses then send email
    if (!empty($wpd_emailAdd)) {
	//$GLOBALS['log']->fatal("valid email addresses are ". print_r($wpd_emailAdd, 1));
        if (!$mail->Send()) {
            $GLOBALS['log']->debug("Fail to Send Email, Please check settings");
        } else {
            $GLOBALS['log']->debug('email sent for '. $id);
        }
    }
}

/**
 * Get The "Assigned to" and "Report To" users id's
 * @global type $db
 * @param type $id
 * @return type
 */
function getEmailAddOfAssignedToAndReportTo($id) {
    global $db;

    $ids = array(); //Array to maintain ids of Assigned user and Report to user
    $email_adds = array(); //Array to maintain the Email Addresses of Assigned user and Report to user
    //Get the user id
    $queryToGetUserId = "SELECT users.id, users.reports_to_id FROM `users` INNER JOIN
                  `m03_work_product_deliverable` ON m03_work_product_deliverable.assigned_user_id = users.id
                   WHERE users.deleted = 0 AND m03_work_product_deliverable.id = '" . $id . "' LIMIT 0,1";

    $result = $db->query($queryToGetUserId);
    if ($user = $db->fetchByAssoc($result)) {
        $ids[] = $user['id'];
        $ids[] = $user['reports_to_id'];
        $email_adds = getRecordEmail($ids);
    }
    //  $GLOBALS['log']->fatal("email adds are " . print_r($email_adds, 1));
    return $email_adds;
}

/**
 * Retrieves email addresses of "Assigned to" and "Report to" users based on user ids
 * @param string $record
 * @return string email_address|null
 */
function getRecordEmail($ids) {
    //  $GLOBALS['log']->fatal("ids are " . print_r($ids, 1));
    $user_ids = implode("', '", $ids);
    //   $GLOBALS['log']->fatal("user ids are " . print_r($user_ids, 1));
    $email_addresses = array();

    //Get only those email addresses which are not empty
    $query = "
        SELECT
            email_addresses.email_address
        FROM
            `email_addresses`
        INNER JOIN
            `email_addr_bean_rel` ON email_addr_bean_rel.email_address_id = email_addresses.id
        WHERE email_addr_bean_rel.deleted = 0 AND email_addresses.deleted='0' AND email_addresses.invalid_email='0' AND email_addresses.opt_out='0'
        AND email_addr_bean_rel.bean_id IN ('{$user_ids}') AND email_addresses.email_address IS NOT NULL AND email_addresses.email_address != '' ";

    $res = $GLOBALS['db']->query($query);
    if ($res->num_rows >= 1) { //If any email address is associated with user
        while ($user = $GLOBALS['db']->fetchByAssoc($res)) {
            $email_addresses[] = $user['email_address'];
        }
        return $email_addresses;
    }

    return '';
}

/**
 * This function will return the Study Director Email Address of the related Work Product
 * @param type $wpd_id
 */
function getTheStudyDirectorOfRelatedWP($wpd_id) {
    global $db;

    //Query to Get the Work Product and Study Director ids related to the Work Product Delieverables

    $query = new SugarQuery();
    $query->from(BeanFactory::newBean('M03_Work_Product_Deliverable'), array('alias' => 'wpd', 'team_security' => false));
    $query->joinTable('m03_work_product_m03_work_product_deliverable_1_c', array(
        'joinType' => 'INNER',
        'alias' => 'wp_wpd',
        'linkingTable' => true,
    ))->on()->equalsField('wpd.id', 'wp_wpd.m03_work_pe584verable_idb');
    $query->joinTable('m03_work_product', array(
        'joinType' => 'INNER',
        'alias' => 'wp',
        'linkingTable' => true,
    ))->on()->equalsField('wp_wpd.m03_work_p0b66product_ida', 'wp.id');
    $query->joinTable('m03_work_product_cstm', array(
        'joinType' => 'INNER',
        'alias' => 'wp_cstm',
        'linkingTable' => true,
    ))->on()->equalsField('wp.id', 'wp_cstm.id_c');
    $query->select('wp.id', 'wp_cstm.contact_id_c');
    $query->where()
            ->equals('wpd.deleted', 0)
            ->equals('wp_wpd.deleted', 0)
            ->equals('wp.deleted', 0)
            ->equals('wpd.id', $wpd_id);
    $query->limit(1);

    $queryResult = $query->execute();
    $wp_id = $queryResult[0]['id'];
    $study_dir_id = $queryResult[0]['contact_id_c'];

    //Get the primary email address of the selected contact id
    $stdy_bean = BeanFactory::getBean('Contacts', $study_dir_id);
    
    $primaryEmailAddress = $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
    if ($primaryEmailAddress != false) {
        return $primaryEmailAddress;
    }
}

/**
 * Verify user email address domain is 'apsemail.com' otherwise return false
 * @param string $account_name
 * @param string $email
 * @return bool true|false
 */
function verifyContactForNotification($email) {
    $domainToSearch = 'apsemail.com';
    $domain_name = substr(strrchr($email, "@"), 1);
    if (trim(strtolower($domain_name) == $domainToSearch)) {
        return true;
    }
    return false;
}
