<?php
//Add job in job string
$job_strings[] = 'create_sc_species_census_record';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function create_sc_species_census_record()
{
	global $db;
	$current_date	= date("Y-m-d"); //current date
	$current_date2	= date("m/d/Y");
	$current_date3	= date("m-d-Y");
	$current_date4	= date("Y-m-d");
	$current_year	= date("Y");
	
	$previous_week = strtotime("-1 week");
	$lastWeek_date	= date("Y-m-d",$previous_week); //lastWeek_date
	//Get site url so that appropriate Work Product can be shown in the Email Template
	$site_url = $GLOBALS['sugar_config']['site_url'];
	 

	$queryCustom = "SELECT spec.id AS speciesID, spec.name AS speciesName FROM `s_species` AS spec WHERE deleted=0";

	$querySpecResult = $db->query($queryCustom);

	while ($fetchSpecResult = $db->fetchByAssoc($querySpecResult)) {

		$speciesName		= $fetchSpecResult['speciesName'];
		$speciesIDa		    = $fetchSpecResult['speciesID'];
		 
		
		$module					= 'SC_Species_Census';
		$SC_bean				= BeanFactory::newBean($module);
		$SC_bean->date_2		= $current_date;
		$SC_bean->name			= $speciesName . ' Census ' . $current_date2;
		
		$SC_bean->s_species_sc_species_census_1_name    	 = $speciesName;
        $SC_bean->s_species_sc_species_census_1s_species_ida = $speciesIDa;
		$SC_bean->created_by    	  = '943a2d38-7bd3-11e9-99fb-06e41dba421a';
		$SC_bean->assigned_user_id    = '943a2d38-7bd3-11e9-99fb-06e41dba421a';
		$SC_bean->modified_user_id    = '943a2d38-7bd3-11e9-99fb-06e41dba421a';
		$SC_bean->save();
		


		
		$SC_bean->load_relationship("s_species_sc_species_census_1");
		$SC_bean->s_species_sc_species_census_1->add($speciesIDa);
	}
	
	$laAssignName[0] =  'Bovine Census '.$current_date2;
	$laAssignName[1] =  'Canine Census '.$current_date2;
	$laAssignName[2] =  'Caprine Census '.$current_date2;
	$laAssignName[3] =  'Ovine Census '.$current_date2;
	$laAssignName[4] =  'Porcine Census '.$current_date2;
	$laAssignName[5] =  'Bovine Census '.$current_date3;
	$laAssignName[6] =  'Canine Census '.$current_date3;
	$laAssignName[7] =  'Caprine Census '.$current_date3;
	$laAssignName[8] =  'Ovine Census '.$current_date3;
	$laAssignName[9] =  'Porcine Census '.$current_date3;
	$laAssignName[10] = 'Bovine Census '.$current_date4;
	$laAssignName[11] = 'Canine Census '.$current_date4;
	$laAssignName[12] = 'Caprine Census '.$current_date4;
	$laAssignName[13] = 'Ovine Census '.$current_date4;
	$laAssignName[14] = 'Porcine Census '.$current_date4;
	
	$laAssigned 		= implode("','",$laAssignName);	
	$laCalculation 		= 0;
	$queryCustomLaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species laCalculation Query == '.$queryCustomLaAssigned);
	$queryLAResult = $db->query($queryCustomLaAssigned);
	if ($queryLAResult->num_rows > 0) 
    {
		 $resultLA 			= $db->fetchByAssoc($queryLAResult);
		 $totalAllocated	= $resultLA['totalAllocated'];
		 $totalOnStudy		= $resultLA['totalOnStudy'];
		 $totalValue		= $resultLA['totalValue'];
		 
		 $laCalculation = (($totalAllocated+$totalOnStudy)/$totalValue)*100;
		 //$GLOBALS['log']->fatal('species laCalculation  == '.$laCalculation.'==='.$totalAllocated.'===='.$totalOnStudy.'===='.$totalStock);
	}
	
	$queryCustomLaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$queryLAIDResult = $db->query($queryCustomLaAssignedID);
	if ($queryLAIDResult->num_rows > 0) 
    {
		 while($resultLAID 			= $db->fetchByAssoc($queryLAIDResult)){
			$GLOBALS['log']->fatal('i am in the loop  == '.$resultLAID['id']); 
			$beanID		= $resultLAID['id'];
			$SC_Bean	= BeanFactory::getBean('SC_Species_Census', $beanID);
			$SC_Bean->la_percent_assigned_2_c	= $laCalculation;
			//$GLOBALS['log']->fatal('updatespecies '.$SC_Bean->name.' laCalculation  ==> '.$laCalculation);	
			$SC_Bean->save();
		}		  
	}
	
	/**/
	$saAssignName[0] =  'Lagomorph Census '.$current_date2;
	$saAssignName[1] =  'Rat Census '.$current_date2;
	$saAssignName[2] =  'Murine Census '.$current_date2;
	$saAssignName[3] =  'Hamster Census '.$current_date2;
	$saAssignName[4] =  'Guinea Pig Census '.$current_date2;
	$saAssignName[5] =  'GuineaPg Census '.$current_date2;
	$saAssignName[6] =  'Lagomorph Census '.$current_date3;
	$saAssignName[7] =  'Rat Census '.$current_date3;
	$saAssignName[8] =  'Murine Census '.$current_date3;
	$saAssignName[9] =  'Hamster Census '.$current_date3;
	$saAssignName[10] =  'Guinea Pig Census '.$current_date3;
	$saAssignName[11] =  'GuineaPg Census '.$current_date3;
	$saAssignName[12] =  'Lagomorph Census '.$current_date4;
	$saAssignName[13] =  'Rat Census '.$current_date4;
	$saAssignName[14] =  'Murine Census '.$current_date4;
	$saAssignName[15] =  'Hamster Census '.$current_date4;
	$saAssignName[16] =  'Guinea Pig Census '.$current_date4;
	$saAssignName[17] =  'GuineaPg Census '.$current_date4;
	
	$saCalculation = 0;
	$saAssigned = implode("','",$saAssignName);
	$queryCustomSaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species saCalculation Query == '.$queryCustomSaAssigned);
	$querySAResult = $db->query($queryCustomSaAssigned);
	if ($querySAResult->num_rows > 0) 
    {
		 $resultSA 			= $db->fetchByAssoc($querySAResult);
		 $totalSA_Allocated	= $resultSA['totalAllocated'];
		 $totalSA_OnStudy	= $resultSA['totalOnStudy'];
		 $totalSA_Value		= $resultSA['totalValue'];
		 $saCalculation = (($totalSA_Allocated+$totalSA_OnStudy)/$totalSA_Value)*100;
		 //$GLOBALS['log']->fatal('species saCalculation == '.$saCalculation.'==='.$totalSA_Allocated.'===='.$totalSA_OnStudy.'===='.$totalSA_Stock);	
	}
	
	$querySaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$querySAIDResult = $db->query($querySaAssignedID);
	if ($querySAIDResult->num_rows > 0) 
    {
		 while($resultSAID 			= $db->fetchByAssoc($querySAIDResult)){
			 
			$beanSAID		= $resultSAID['id'];
			$SC_SABean	= BeanFactory::getBean('SC_Species_Census', $beanSAID);
			$SC_SABean->sa_percent_assigned_2_c	= $saCalculation;
			//$GLOBALS['log']->fatal('===Newspecies '.$SC_SABean->name.' saCalculation  ==> '.$saCalculation);	
			$SC_SABean->save();
		}		  
	}
	
	return true;
}