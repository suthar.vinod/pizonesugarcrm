<?php
//Add job in job string
$job_strings[] = 'communication_requiring_assessment_consolidate';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function communication_requiring_assessment_consolidate() {
    global $db;
    $current_date = date("Y-m-d"); //current date
	
    $wp_emailAdd = array(); // contains the email addresses of Study Director and its Manager
	$sd_array = array(); //Contains id For Study Directors
    
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];
	$domainToSearch = 'apsemail.com';
    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();

	$queryCustom = "SELECT 
					commu.id AS communicationID, 
					commu.name AS CommunicationName,  
					commu_cstm.target_reinspection_date_c, 
					commu_cstm.error_category_c,  					
					commu_cstm.reinspection_date_c, 
					commu_wp.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
					wp.name  AS workProductName,
					wp_cstm.contact_id_c,
					wp.assigned_user_id,
					wpCode.name AS workProductCode,
					wp_cstm.work_product_compliance_c
				FROM `m06_error` AS commu
					INNER JOIN m06_error_cstm AS commu_cstm
					ON  commu.id = commu_cstm.id_c

					RIGHT JOIN m06_error_m03_work_product_1_c AS commu_wp
					ON  commu.id = commu_wp.m06_error_m03_work_product_1m06_error_ida
					
					INNER JOIN m03_work_product AS wp
					ON  commu_wp.m06_error_m03_work_product_1m03_work_product_idb=wp.id 
					
					INNER JOIN m03_work_product_cstm AS wp_cstm
					ON  wp.id=wp_cstm.id_c 
					
					LEFT JOIN m03_work_product_code wpCode ON wpCode.id = wp_cstm.m03_work_product_code_id1_c
				WHERE  
					commu_cstm.error_category_c != 'Internal Feedback' 
				AND commu_cstm.error_category_c != 'Feedback'
				AND commu_cstm.error_category_c != 'Equipment Maintenance Request'
				AND commu_cstm.error_category_c != 'External Audit'
				AND commu_cstm.error_category_c != 'Maintenance Request'
				AND commu_cstm.error_category_c != 'Process Audit'
				AND commu_cstm.error_category_c != 'Weekly Sweep'
				AND commu_cstm.error_category_c != 'Study Specific Charge' 
				AND NOT((commu_cstm.error_category_c = 'Work Product Schedule Outcome' AND  ( commu_cstm.error_type_c = 'Standard Biocomp TS Selection' OR commu_cstm.error_type_c = 'Passed Pyrogen' OR commu_cstm.error_type_c = 'Not used shared BU' OR commu_cstm.error_type_c = 'Performed Per Protocol' OR commu_cstm.error_type_c = 'Not Used' OR commu_cstm.error_type_c = 'Sedation for Animal Health')))
				AND YEAR(commu_cstm.target_reinspection_date_c) >= 2020 
				AND commu_cstm.target_reinspection_date_c <'" . $current_date . "'
				AND commu_wp.m06_error_m03_work_product_1m03_work_product_idb IS NOT NULL
				AND commu_wp.deleted = 0
				AND commu.deleted = 0			
				AND ((wpCode.name NOT LIKE 'AH%' AND  wpCode.name NOT LIKE 'TR%') OR wpCode.name IS NULL)
				AND(			
				(  (wp_cstm.work_product_compliance_c != 'NonGLP Discovery')
				AND (commu_cstm.resolution_c IS NULL 
						OR commu_cstm.reinspection_date_c IS NULL 
						OR commu_cstm.error_classification_c IS NULL  
						OR commu_cstm.error_classification_c ='Choose One'
					)
				)
				OR
				(
				(wp_cstm.work_product_compliance_c = 'NonGLP Discovery')
				AND  commu_cstm.reinspection_date_c IS NULL 
				)  
				)			
				ORDER BY commu_cstm.target_reinspection_date_c ASC";

			$queryCommResult = $db->query($queryCustom);


			while ($fetchCommResult = $db->fetchByAssoc($queryCommResult)) {

				$communicationID		= $fetchCommResult['communicationID'];
				$workProductID			= $fetchCommResult['workProductID'];
				$workProductName		= $fetchCommResult['workProductName'];
				$communicationName		= $fetchCommResult['CommunicationName'];
				$targetAssessmentDate	= $fetchCommResult['target_reinspection_date_c'];
				$workProductCompliance	= $fetchCommResult['work_product_compliance_c'];		
				//$SD_ID				= $fetchCommResult['contact_id_c'];
				$SD_ID					= $fetchCommResult['assigned_user_id'];

				if ($SD_ID != "" && $workProductID != "" && $communicationID != "" && $communicationName!=""  && $targetAssessmentDate!="" ) {

					$SD_UserAddress		= ""; /**Work Product's Assigned to User's Email Address */
					$SD_UserName		= "";
					$SD_manager_name	= "";
					$SD_manager_email	= ""; 


					/*Get Assigned to user detail*/
					$user_bean1	 		= BeanFactory::getBean('Employees', $SD_ID);
					$SD_Status			= $user_bean1->employee_status;
					if($SD_Status!="Terminated"){
						$SD_UserName		= $user_bean1->name;				
						$SD_UserAddress		= $user_bean1->emailAddress->getPrimaryAddress($user_bean1);
						$domain_name = substr(strrchr($SD_UserAddress, "@"), 1);
						if (trim(strtolower($domain_name) != $domainToSearch)) {
							$SD_UserAddress = "";
							$SD_UserName	= "";
						}
					}

					
					//get Assigned to User's Manager Detail
					if($user_bean1->reports_to_id){ 
						$reportsTo_id		= $user_bean1->reports_to_id;
						$director_bean 		= BeanFactory::getBean('Employees', $reportsTo_id);
						$SD_manager_name	= $director_bean->name;
						$SD_manager_email	= $director_bean->emailAddress->getPrimaryAddress($director_bean);

						$domain_name = substr(strrchr($SD_manager_email, "@"), 1);
						if (trim(strtolower($domain_name) != $domainToSearch)) {
							$SD_manager_email = "";
							$SD_manager_name  = "";
						} 
					}

					$sd_array[$SD_ID][]	 = array( 
							"communicationID" 	=> $communicationID,  
							"communicationName" => $communicationName,
							"workProductID" 	=> $workProductID, 
							"workProductName" 	=> $workProductName,
							"workProductCompliance" => $workProductCompliance,	
							"assessmentDate" 	=> date("m-d-Y", strtotime($targetAssessmentDate)),
							"mgtName" 			=> $SD_UserName,
							"mgtMail" 			=> $SD_UserAddress,
							"sd_mgtMail" 		=> $SD_manager_email,
							"sd_mgtName" 		=> $SD_manager_name,
					); 					
				}
			}


		if(count($sd_array)>0){ 
			$cntr = 1;
			foreach ($sd_array as $key => $value) { 
				$communication_name	 =""; 	

				unset($wp_emailAdd); 
				/* Get Email id of Assigned User and report to User */
				$wp_emailAdd = array();

				//Email Body Header
				$emailBody = "<table width='500px' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
				<tr><th bgcolor='#b3d1ff' align='left'>Communication ID </th><th bgcolor='#b3d1ff' align='left'>Work Product ID</th><th bgcolor='#b3d1ff' align='left'>Target Assessment Date</th></tr>";	

				for($cnt=0;$cnt<count($sd_array[$key]);$cnt++){
					$communication_name = '<a target="_blank"href="'.$site_url.'/#M06_Error/'.$sd_array[$key][$cnt]['communicationID'].'">'.$sd_array[$key][$cnt]['communicationName'] . '</a>';
					$workProduct_name   = "";
					if($sd_array[$key][$cnt]['workProductID']!="" && $sd_array[$key][$cnt]['workProductName']!=""){
						$workProduct_name   = '<a target="_blank" href="'.$site_url.'/#M03_Work_Product/'.$sd_array[$key][$cnt]['workProductID'].'" >'.$sd_array[$key][$cnt]['workProductName'] . '</a>';
					}

					$sd_mgtName  	  = $sd_array[$key][$cnt]['sd_mgtName'];
					$sd_mgtMail  	  = $sd_array[$key][$cnt]['sd_mgtMail'];
					$mgtName  		  = $sd_array[$key][$cnt]['mgtName'];
					$mgtMail  		  = $sd_array[$key][$cnt]['mgtMail'];
					$assessment_date  = $sd_array[$key][$cnt]['assessmentDate'];

					$emailBody .= "<tr><td>".$communication_name."</td><td>".$workProduct_name."</td><td>".$assessment_date."</td></tr>";
				}	
				$emailBody .="</table>";

				//$wp_emailAdd[] = 'mconforti@apsemail.com';
				$wp_emailAdd[] = $mgtMail;	
				$wp_emailAdd[] = $sd_mgtMail;
				$wp_emailAdd   = array_values($wp_emailAdd); //re-index the array elements
				//remove the repeated email addresses
				$wp_emailAdd = array_unique($wp_emailAdd);

				$emailString = implode(",",$wp_emailAdd);

				if($mgtName=="")
					$mgtName = $sd_mgtName;

				if($mgtName=="")
					$mgtName = "Michael Confirti";

				$template->retrieve_by_string_fields(array('name' => 'Communication Requiring Assessment Consolidate', 'type' => 'email'));

				$template->body_html = str_replace('[SD Name]', $mgtName, $template->body_html);
				$template->body_html = str_replace('[Consolidate_Report]', $emailBody, $template->body_html);
				//$template->body_html .= $emailString; 

				$defaults	= $emailObj->getSystemDefaultEmail();
				$mail		= new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->IsHTML(true);
				$mail->From		= $defaults['email'];
				$mail->FromName = $defaults['name'];
				$mail->Subject	= $template->subject;
				$mail->Body		= $template->body_html;
					
				foreach ($wp_emailAdd as $wmail) { 
					$domain_name = substr(strrchr($wmail, "@"), 1);
					if (trim(strtolower($domain_name) == $domainToSearch)) {
						$mail->AddAddress($wmail); 
					}
				} 

				$mail->AddBCC('vsuthar@apsemail.com');
				$mail->AddBCC('cjagadeeswaraiah@apsemail.com');
				//If their exist some valid email addresses then send email
				if (!empty($wp_emailAdd)) { 
					if (!$mail->Send()) {
						$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
					} else {
						$GLOBALS['log']->debug('email sent for ' . $study_dir_id);
					}
					unset($mail);
				}
			}
		}
    
     	 
  
	unset($sd_array);
	$sd_array = array();
	//*Functionality to send Emails for Communication Reassessments*//
	$queryCustomReassement = "SELECT 
					commu.id AS communicationID, 
					commu.name AS CommunicationName,  
					 commu_cstm.error_category_c,  
					commu_cstm.target_sd_reassessment_date_c,  
					commu_cstm.actual_sd_reassessment_date_c, 
					commu_wp.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
					wp.name  AS workProductName,
					wp_cstm.contact_id_c,
					wp.assigned_user_id,
					wp_cstm.work_product_compliance_c		
				FROM `m06_error` AS commu
					INNER JOIN m06_error_cstm AS commu_cstm
					ON  commu.id = commu_cstm.id_c
					
					RIGHT JOIN m06_error_m03_work_product_1_c AS commu_wp
					ON  commu.id = commu_wp.m06_error_m03_work_product_1m06_error_ida
					
					INNER JOIN m03_work_product AS wp
					ON  commu_wp.m06_error_m03_work_product_1m03_work_product_idb=wp.id 
					
					INNER JOIN m03_work_product_cstm AS wp_cstm
                    ON  wp.id=wp_cstm.id_c 
				WHERE 
				commu_cstm.sd_reassessment_required_c='Yes' 
				AND commu_cstm.target_sd_reassessment_date_c<'" . $current_date . "'  
				AND commu_wp.m06_error_m03_work_product_1m03_work_product_idb IS NOT NULL
				AND YEAR(commu_cstm.target_sd_reassessment_date_c) >= 2020 
				AND commu_wp.deleted = 0
				AND commu.deleted = 0
				AND commu_cstm.actual_sd_reassessment_date_c IS NULL 
			
				ORDER BY commu_cstm.target_sd_reassessment_date_c ASC";
				
				
	 

    $queryCommReassessResult = $db->query($queryCustomReassement);
    
    while ($fetchCommReassessResult = $db->fetchByAssoc($queryCommReassessResult)) {

        $workProductID			= $fetchCommReassessResult['workProductID'];
        $communicationID		= $fetchCommReassessResult['communicationID'];
        $communicationName		= $fetchCommReassessResult['CommunicationName'];
        $workProductName		= $fetchCommReassessResult['workProductName'];
		$workProductCompliance	= $fetchCommReassessResult['work_product_compliance_c'];
		$assessmentDate			= $fetchCommReassessResult['target_sd_reassessment_date_c'];
		$SD_ID					= $fetchCommResult['assigned_user_id'];
		//$SD_ID = $fetchCommReassessResult['contact_id_c'];
		
        if ($workProductID != "" && $communicationID != "" && $SD_ID != "" ) {
			$sd_array[$SD_ID][]	 = array( 
						"workProductID" => $workProductID, 
						"workProductName" => $workProductName,	
						"communicationID" => $communicationID, 
						"workProductCompliance" => $workProductCompliance,							
						"communicationName" => $communicationName,
						"assessmentDate" => date("m-d-Y", strtotime($assessmentDate)),		
					); 
					
		}
	}

	if(count($sd_array)>0){ 
		foreach ($sd_array as $key => $value) { 
			$study_dir_id		= $key;
			$communication_name	= "";
			$workProduct_name	= "";	
			
			unset($wp_emailAdd);
			unset($wp_emailAddTest); //to Be deleted
			/* Get Email id of Assigned User and report to User */
			$wp_emailAdd = array();
			$wp_emailAddTest = array(); //to Be deleted
				
				
			//Email Body Header
			$emailBody = "<table width='500px' cellspacing='0' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>Communication ID </th><th bgcolor='#b3d1ff' align='left'>Work Product ID</th><th bgcolor='#b3d1ff' align='left'>Target Reassessment Date</th></tr>";	
			for($cnt=0;$cnt<count($sd_array[$key]);$cnt++){
				$communication_name = '<a target="_blank"href="' . $site_url . '/#M06_Error/' . $sd_array[$key][$cnt]['communicationID'] . '">' . $sd_array[$key][$cnt]['communicationName'] . '</a>'; 
				
				$redStyle = "";
				if($sd_array[$key][$cnt]['workProductCompliance']=="GLP")
					$redStyle = "style='color:red'";
				$workProduct_name = '<a target="_blank" href="' . $site_url . '/#M03_Work_Product/' . $sd_array[$key][$cnt]['workProductID'] . '" '.$redStyle.'>' . $sd_array[$key][$cnt]['workProductName'] . '</a>';			
				
				$assessment_date  = $sd_array[$key][$cnt]['assessmentDate'];
				$emailBody .= "<tr><td>".$communication_name."</td><td>".$workProduct_name."</td><td>".$assessment_date."</td></tr>";
			}
			$emailBody .="</table>";
			if ($study_dir_id != "") {

				/*Get Assigned to user detail*/
				$SD_UserName		= "";	
				$user_bean1	 		= BeanFactory::getBean('Employees', $study_dir_id);
				$SD_Status			= $user_bean1->employee_status;
				if($SD_Status!="Terminated"){
					$SD_UserName		= $user_bean1->name;				
					$SD_UserAddress		= $user_bean1->emailAddress->getPrimaryAddress($user_bean1);

					$domain_name = substr(strrchr($SD_UserAddress, "@"), 1);
					if (trim(strtolower($domain_name) == $domainToSearch)) {
						$wp_emailAdd[] = $SD_UserAddress;
					}
				}

				//get Assigned to User's Manager
				if($user_bean1->reports_to_id){ 
					$reportsTo_id		= $user_bean1->reports_to_id;
					$director_bean 		= BeanFactory::getBean('Employees', $reportsTo_id);
					$SD_manager_name	= $director_bean->name;
					$SD_manager_email	= $director_bean->emailAddress->getPrimaryAddress($director_bean);

					$domain_name = substr(strrchr($SD_manager_email, "@"), 1);
					if (trim(strtolower($domain_name) == $domainToSearch)) {
						$wp_emailAdd[] = $SD_manager_email;
					} 
				}

				if($SD_UserName=="")
					$sdName = $SD_manager_name;
			
				if($sdName=="")
					$sdName = "Michael Confirti";
					
				
			}
			//$wp_emailAdd[] = 'mconforti@apsemail.com';	
			$wp_emailAdd = array_values($wp_emailAdd); //re-index the array elements
			//remove the repeated email addresses
			$wp_emailAdd = array_unique($wp_emailAdd);
				
			$template->retrieve_by_string_fields(array('name' => 'Communication Requiring Reassessment Consolidate', 'type' => 'email'));
			
			$template->body_html = str_replace('[SD Name]', $sdName, $template->body_html);
			$template->body_html = str_replace('[Consolidate_Report]', $emailBody, $template->body_html);
			//$template->body_html .= implode(",",$wp_emailAdd);    
			
			$defaults = $emailObj->getSystemDefaultEmail();
				$mail = new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->IsHTML(true);
				$mail->From = $defaults['email'];
				$mail->FromName = $defaults['name'];
				$mail->Subject = $template->subject;
				$mail->Body = $template->body_html;
				

				//$wp_emailAddTest[] = 'vsuthar@apsemail.com'; ////to Be deleted
				foreach ($wp_emailAdd as $wmail) { 
				$domain_name = substr(strrchr($wmail, "@"), 1);
					if (trim(strtolower($domain_name) == $domainToSearch)) {
						$mail->AddAddress($wmail); 
					}
				}
				
				$mail->AddBCC('vsuthar@apsemail.com');
				$mail->AddBCC('cjagadeeswaraiah@apsemail.com');
				//If their exist some valid email addresses then send email
				if (!empty($wp_emailAdd)) {
					//$GLOBALS['log']->fatal("valid email addresses are ". print_r($wpd_emailAdd, 1));
					if (!$mail->Send()) {
						$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
					} else {
						$GLOBALS['log']->debug('email sent for ' . $study_dir_id);
					}
					unset($mail);
				}
		
				
			}
		}	
	return true;
   } 
?>