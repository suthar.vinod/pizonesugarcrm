<?php
//Add job in job string
$job_strings[] = 'protocol_due_for_annual_review_email_workflow';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function protocol_due_for_annual_review_email_workflow()
{
	global $db;
	$current_date = date("Y-m-d"); //current date
	//$current_date = date("2021-10-27"); //current date

	$next_2_year_date 	=  date("Y-m-d", strtotime("+2 years", strtotime($current_date)));
	$next_1_year_date 	=  date("Y-m-d", strtotime("+1 years", strtotime($current_date)));
	$next_90_date  	  	=  date("Y-m-d", strtotime("+90 days", strtotime($current_date)));
	$next_60_date  	  	=  date("Y-m-d", strtotime("+60 days", strtotime($current_date)));
	$next_30_date 		=  date("Y-m-d", strtotime("+30 days", strtotime($current_date)));
	$next_15_date 		=  date("Y-m-d", strtotime("+15 days", strtotime($current_date)));
	$next_7_date  		=  date("Y-m-d", strtotime("+7 days",  strtotime($current_date)));

	//Get site url so that appropriate Work Product can be shown in the Email Template
	$site_url		= $GLOBALS['sugar_config']['site_url'];
	$domainToSearch = 'apsemail.com';
	$emailBody 		= "";

	$queryCustom = "SELECT  WP.id AS workProductID,
						WP.name AS workProductName, 
						WP.assigned_user_id AS Assigneduser,
						WPCSTM.contact_id_c AS StudyDirector,
						WPCSTM.iacuc_first_annual_review_c,
						WPCSTM.iacuc_second_annual_review_c,
						WPCSTM.iacuc_triennial_review_c,
						WPCSTM.first_annual_review_c,
						WPCSTM.second_annual_review_c,
						WPCSTM.triennial_review_c,
					CONCAT(SDContact.first_name,' ', SDContact.last_name) AS StudyDirectorName							
					FROM `m03_work_product` AS WP
					LEFT JOIN m03_work_product_cstm AS WPCSTM
						ON WP.id = WPCSTM.id_c
					LEFT JOIN contacts AS SDContact
					ON SDContact.id=WPCSTM.contact_id_c
					WHERE  WPCSTM.iacuc_protocol_status_c =  'Active' 
					AND WPCSTM.triennial_review_c = 0
					AND (   WPCSTM.iacuc_triennial_review_c = '" . $next_2_year_date . "'
					OR WPCSTM.iacuc_triennial_review_c = '" . $next_1_year_date . "'
					OR WPCSTM.iacuc_triennial_review_c = '" . $next_90_date . "'
					OR WPCSTM.iacuc_triennial_review_c = '" . $next_60_date . "'						 	
					OR WPCSTM.iacuc_triennial_review_c = '" . $next_30_date . "'
					OR WPCSTM.iacuc_triennial_review_c = '" . $next_15_date . "'						 
					OR (WPCSTM.iacuc_triennial_review_c <= '" . $next_7_date . "' AND  WPCSTM.iacuc_triennial_review_c >= '" . $current_date . "')
					)
					AND WP.deleted = 0 
					ORDER BY StudyDirectorName ASC";
	//$GLOBALS['log']->fatal("Review Protocol Query ==".$queryCustom);
	$queryWPDResult = $db->query($queryCustom);


	while ($fetchResult = $db->fetchByAssoc($queryWPDResult)) {

		$workProductID		= $fetchResult['workProductID'];
		$workProductName	= $fetchResult['workProductName'];
		$assignedUser_id	= $fetchResult['Assigneduser'];
		$SD_ID				= $fetchResult['StudyDirector'];
		$SD_Name			= $fetchResult['StudyDirectorName'];
		$firstReviewDate			= $fetchResult['iacuc_first_annual_review_c'];
		$secondReviewDate			= $fetchResult['iacuc_second_annual_review_c'];
		$thirdReviewDate			= $fetchResult['iacuc_triennial_review_c'];
		$firstReviewDone			= $fetchResult['first_annual_review_c'];
		$secondReviewDone			= $fetchResult['second_annual_review_c'];
		$thirdReviewDone			= $fetchResult['triennial_review_c'];
		$dueDays					= "";

		if ($workProductID != "") {

			$wpName  = '<a target="_blank" href="' . $site_url . '/#M03_Work_Product/' . $workProductID . '" >' . $workProductName . '</a>';
			$thirdReviewDateformat = date("m-d-Y", strtotime($thirdReviewDate));
			if ($thirdReviewDate == $next_2_year_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC 1 Year Reminder";
				$dueDays = "This protocol has been active with IACUC for 1 year, if work is no longer ongoing please close out with IACUC.";
			}

			if ($thirdReviewDate == $next_1_year_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC 2 Year Reminder";
				$dueDays = "This protocol has been active with IACUC for 2 years, if work is no longer ongoing please close out with IACUC.";
			}

			if ($thirdReviewDate == $next_90_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC Review Reminder";
				$dueDays = "This protocol is due for Triennial Review, please complete the IACUC Protocol Renewal/Closure Form, and protocol re-write if work is ongoing, prior to " . $thirdReviewDateformat . ".";
			}

			if ($thirdReviewDate == $next_60_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC Review Reminder";
				$dueDays = "This protocol is due for Triennial Review, please complete the IACUC Protocol Renewal/Closure Form, and protocol re-write if work is ongoing, prior to " . $thirdReviewDateformat . ".";
			}

			if ($thirdReviewDate == $next_30_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC Review Reminder";
				$dueDays = "This protocol is due for Triennial Review, please complete the IACUC Protocol Renewal/Closure Form, and protocol re-write if work is ongoing, prior to " . $thirdReviewDateformat . ".";
			}

			if ($thirdReviewDate == $next_15_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC Review Reminder";
				$dueDays = "This protocol is due for Triennial Review, please complete the IACUC Protocol Renewal/Closure Form, and protocol re-write if work is ongoing, prior to " . $thirdReviewDateformat . ".";
			}

			if ($thirdReviewDate >= $current_date && $thirdReviewDate <= $next_7_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC Review Reminder";
				$dueDays = "This protocol is due for Triennial Review, please complete the IACUC Protocol Renewal/Closure Form, and protocol re-write if work is ongoing, prior to " . $thirdReviewDateformat . ".";
			}



			/*Get Study Director*/
			if ($SD_ID != "") {
				$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);

				$domain_name = substr(strrchr($SDEmailAddress, "@"), 1);
				if (trim(strtolower($domain_name) != $domainToSearch)) {
					$SDEmailAddress 	= "";
					$studyDirectorName	= "";
				}
			}

			if ($dueDays != "") {
				///Upload Document Reminder Template 
				$template = new EmailTemplate();
				$emailObj = new Email();

				$template->retrieve_by_string_fields(array('name' => 'Notification of Protocol Due for Annual Review', 'type' => 'email'));

				$template->body_html = str_replace('[DUE_WP]', $wpName, $template->body_html);
				$template->body_html = str_replace('[DUE_DAYS]', $dueDays, $template->body_html);
				//$template->body_html .= $emailString;

				$defaults	= $emailObj->getSystemDefaultEmail();
				$mail		= new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->IsHTML(true);
				$mail->From		= $defaults['email'];
				$mail->FromName = $defaults['name'];
				$mail->Subject	= $email_subject;
				$mail->Body		= $template->body_html;

				//$mail->AddAddress('ehollen@apsemail.com');
				$mail->AddAddress('cleet@apsemail.com');
				/* #1939 add mail to flow and Study Director of the Work Product :  5 Jan 2022  */
				$mail->AddAddress('calcox@apsemail.com');
				$mail->AddAddress('lzugschwert@apsemail.com');
				$mail->AddAddress('lwalz@apsemail.com');
				$mail->AddAddress('IACUCSubmissions@apsemail.com');
				$mail->AddAddress($SDEmailAddress);
				$wp_emailAdd[] = 'ehollen@apsemail.com';


				foreach ($wp_emailAdd as $wmail) {
					$domain_name = substr(strrchr($wmail, "@"), 1);
					if (trim(strtolower($domain_name) == $domainToSearch)) {
						$mail->AddAddress($wmail);
					}
				}
				$mail->AddBCC('vsuthar@apsemail.com'); //to be deleted 

				//If their exist some valid email addresses then send email
				if (!empty($wp_emailAdd)) {
					if (!$mail->Send()) {
						$GLOBALS['log']->fatal("Fail to Send Email, Please check settings");
					} else {
						$GLOBALS['log']->fatal('email sent for ');
					}
					unset($mail);
				}
			}
		}
	}
	return true;
}
