<?php
//Add job in job string
$job_strings['daily_scheduling_needs_sa'] = 'daily_scheduling_needs_sa';
//  This function will found out all the pending workproduct Assessment in Communication. */

function daily_scheduling_needs_sa()
{
	global $db;
	$emailObj = new Email();
	//Prepare the Email Template to send to Appropriate Members
	$template		= new EmailTemplate();
	$site_url		= $GLOBALS['sugar_config']['site_url'];
	
	$hyperlink = '<a target="_blank" href="'.$site_url.'#bwc/index.php?module=Reports&action=DailySchedulingNeedsSA&bwcRedirect=1">Daily Scheduling Needs - SA</a>';

	$template->retrieve_by_string_fields(array('name' => 'Daily Scheduling Needs - SA', 'type' => 'email'));
	// $template->body_html = str_replace('[REPORT_Name]', 'Daily Scheduling Needs - LA', $template->body_html);
	$template->body_html = str_replace('[Daily_Scheduling_Needs_Report_Data_SA]', $hyperlink, $template->body_html);
	//$template->body_html .= $emailString; 

	$defaults	= $emailObj->getSystemDefaultEmail();
	$mail		= new SugarPHPMailer();
	$mail->setMailerForSystem();
	$mail->IsHTML(true);
	$mail->From		= $defaults['email'];
	$mail->FromName = $defaults['name'];
	$mail->Subject	= $template->subject;
	$mail->Body		= $template->body_html;
		
	$mail->AddAddress('jgeist@apsemail.com'); 	//Jennifer Geist
	$mail->AddAddress('treiten@apsemail.com'); //Teri Reiten
	$mail->addbcc('vsuthar@apsemail.com'); //vinod suthar
	// $mail->addbcc('spa_testing@apsemail.com'); 
	//If their exist some valid email addresses then send email
	if (!$mail->Send()) {
		$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
	} else {
		$GLOBALS['log']->debug('email sent successfully');
	}
	unset($mail);
	return true;
}