<?php
//Add job in job string
$job_strings[] = 'overdue_to_qa_email_workflow';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function overdue_to_qa_email_workflow() {
	global $db;
    $current_date = date("Y-m-d"); //current date
	 
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url		= $GLOBALS['sugar_config']['site_url'];
	$domainToSearch = 'apsemail.com';
	$emailBody 		= "";

    $queryCustom = "SELECT  WPD.id AS wpd_id,
							WPD.name AS wpd_name,
							WPD.date_entered,
							WPDCSTM.deliverable_c, 
							WPDCSTM.due_date_c, 
							WPDCSTM.final_due_date_c, 
							WP_WPD.m03_work_p0b66product_ida AS workProductID,
							WP.name AS workProductName,
							WP.assigned_user_id AS Assigneduser,
							WPCSTM.contact_id_c AS StudyDirector,
							WPCSTM.first_procedure_c,
							WPCSTM.last_procedure_c,
						CONCAT(SDContact.first_name,' ', SDContact.last_name) AS StudyDirectorName							
					FROM `m03_work_product_deliverable` AS WPD
						LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
							ON WPD.id= WPDCSTM.id_c
						RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
							ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
						LEFT JOIN m03_work_product AS WP
							ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
						LEFT JOIN m03_work_product_cstm AS WPCSTM
							ON WP.id=WPCSTM.id_c
						LEFT JOIN contacts AS SDContact
						ON SDContact.id=WPCSTM.contact_id_c
					WHERE  WPCSTM.functional_area_c =  'Standard Biocompatibility' 
					AND WPCSTM.work_product_compliance_c   =  'GLP'
						AND WPDCSTM.deliverable_c   =  'Final Report'
						AND WPDCSTM.due_date_c		<= '".$current_date."'
						AND WPDCSTM.draft_deliverable_sent_date_c IS NULL
						AND WPDCSTM.deliverable_status_c !='Completed'
						AND WPDCSTM.deliverable_status_c !='Not Performed'
						AND WPDCSTM.deliverable_status_c !='None'
						AND WPDCSTM.deliverable_status_c !='Completed Account on Hold'
						AND WPDCSTM.deliverable_status_c !='Sponsor Retracted' 
						AND WPD.date_entered>'2021-09-19'
						AND WPD.deleted = 0 
					ORDER BY StudyDirectorName ASC";

    $queryWPDResult = $db->query($queryCustom);
	
	
	 while($fetchResult = $db->fetchByAssoc($queryWPDResult)) {

        $wpd_id				= $fetchResult['wpd_id'];
		$wpdName			= $fetchResult['wpd_name'];
		$workProductID		= $fetchResult['workProductID'];
		$workProductName	= $fetchResult['workProductName'];
		$assignedUser_id	= $fetchResult['Assigneduser']; 
        $SD_ID				= $fetchResult['StudyDirector'];
        $SD_Name				= $fetchResult['StudyDirectorName'];
		
		$dueDate			= "";
		if($fetchResult['due_date_c']!="")
			$dueDate			= date("m-d-Y", strtotime($fetchResult['due_date_c']));
		
		$lastProcedureDate			= "";
		if($fetchResult['last_procedure_c']!="")
			$lastProcedureDate			= date("m-d-Y", strtotime($fetchResult['last_procedure_c']));
		
		$finalDueDate			= "";
		if($fetchResult['final_due_date_c']!="")
			$finalDueDate			= date("m-d-Y", strtotime($fetchResult['final_due_date_c']));
		
		
		
        if ($workProductID != "" && $wpd_id != "" && $dueDate!="" ) {
			
			$UserName 			= "";
			$UserAddress	 	= "";
			$studyDirectorName	= "";
			$SDEmailAddress 	= "";
			
			/*Get Study Director*/
			if($SD_ID!=""){
				$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
				
				$domain_name = substr(strrchr($SDEmailAddress, "@"), 1);
				if (trim(strtolower($domain_name) != $domainToSearch)) {
					$SDEmailAddress 	= "";
					//$studyDirectorName	= "";
				}
			}
			
			$workProduct_name   = '<a target="_blank" href="'.$site_url.'/#M03_Work_Product_Deliverable/'.$wpd_id.'" >'.$wpdName. '</a>';
			$emailBody .= "<tr><td>".$studyDirectorName."</td><td>".$workProduct_name."</td><td>".$lastProcedureDate."</td><td>".$dueDate."</td><td>".$finalDueDate."</td></tr>";
				
			$wp_emailAdd[] = $SDEmailAddress;	
		}	
	 }

	if($emailBody!="" ){ //&& count($wp_emailAdd)>0
		$cntr = 1;
		
		$emailBody = "<table width='800px' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'><tr>
			<th bgcolor='#b3d1ff' align='left'>Study Director</th>
			<th bgcolor='#b3d1ff' align='left'>Work Product Deliverable</th>
			<th bgcolor='#b3d1ff' align='left'>Last Procedure Date</th>
			<th bgcolor='#b3d1ff' align='left'>Draft Due Date</th>
			<th bgcolor='#b3d1ff' align='left'>External Final Due Date</th>
			</tr>".$emailBody."</table>";	
			
			
			//$wp_emailAdd[] = 'tfossum@apsemail.com';
			$wp_emailAdd[] = 'hackerson@apsemail.com';
			$wp_emailAdd[] = 'cleet@apsemail.com';	 
			$wp_emailAdd[] = 'emarkuson@apsemail.com';	 
			$wp_emailAdd[] = 'anelson@apsemail.com';	 
			$wp_emailAdd[] = 'cvaleri@apsemail.com';	 
			$wp_emailAdd[] = 'ablakstvedt@apsemail.com';	 
			 
			//remove the repeated email addresses
			$wp_emailAdd 	= array_unique($wp_emailAdd);
			$emailString 	= implode(",",$wp_emailAdd);	

			//Send Email/
			///Upload Document Reminder Template 
			$template = new EmailTemplate();
			$emailObj = new Email();			

			$template->retrieve_by_string_fields(array('name' => 'Notification of Final Report Overdue for QA Review', 'type' => 'email'));

			$template->body_html = str_replace('[Consolidate_Report]', $emailBody, $template->body_html);
			//$template->body_html .= $emailString;
			
			$defaults	= $emailObj->getSystemDefaultEmail();
			$mail		= new SugarPHPMailer();
			$mail->setMailerForSystem();
			$mail->IsHTML(true);
			$mail->From		= $defaults['email'];
			$mail->FromName = $defaults['name'];
			$mail->Subject	= $template->subject;
			$mail->Body		= $template->body_html;
			
			foreach ($wp_emailAdd as $wmail){ 
				$domain_name = substr(strrchr($wmail, "@"), 1);
				if (trim(strtolower($domain_name) == $domainToSearch)){
					$mail->AddAddress($wmail); 
				}
			}	
			 
			$mail->AddBCC('vsuthar@apsemail.com');		

			//If their exist some valid email addresses then send email
			if (!empty($wp_emailAdd)) {
				if (!$mail->Send()) {
					$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
				} else {
					$GLOBALS['log']->debug('email sent');
				}
				unset($mail);
			}		 
	}	
	return true;
}