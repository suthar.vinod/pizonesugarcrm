<?php
//Add job in job string
$job_strings[] = 'send_email_for_cpi_report';

function send_email_for_cpi_report()
{
	global $db;
	$current_date	= date("Y-m-d"); //current date 
	$fileDate		= date("jMY", strtotime("-1 day", strtotime($current_date)));
	$yesterdaydate	= date("Y-m-d", strtotime("-1 days", strtotime($current_date)));
	$current_date  	= $current_date .' 05:59:59';
	$yesterdaydate 	= $yesterdaydate .' 06:00:00';
	 
	/**/
	$reportSQL = "SELECT IFNULL(a1a_critical_phase_inspectio.id,'') primaryid
	,IFNULL(a1a_critical_phase_inspectio.name,'') A1A_CRITICAL_PHASE_INSF6098C
	,IFNULL(a1a_critical_phase_inspectio_cstm.inspection_results_c,'') A1A_CRITICAL_PHASE_INSBF63F7,IFNULL(l1.id,'') l1_id
	,IFNULL(l1.name,'') l1_name
	,l1_cstm.contact_id_c l1_cstm_contact_id_c,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contacts1_name
	FROM a1a_critical_phase_inspectio
	 INNER JOIN  m03_work_product_a1a_critical_phase_inspectio_1_c l1_1 ON a1a_critical_phase_inspectio.id=l1_1.m03_work_p934fspectio_idb AND l1_1.deleted=0
	 INNER JOIN  m03_work_product l1 ON l1.id=l1_1.m03_work_p33edproduct_ida AND l1.deleted=0
	LEFT JOIN m03_work_product_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN contacts contacts1 ON contacts1.id = l1_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
	LEFT JOIN a1a_critical_phase_inspectio_cstm a1a_critical_phase_inspectio_cstm ON a1a_critical_phase_inspectio.id = a1a_critical_phase_inspectio_cstm.id_c
	WHERE (((a1a_critical_phase_inspectio.date_entered >= '" . $yesterdaydate . "' AND a1a_critical_phase_inspectio.date_entered <= '" . $current_date . "'
	)))  	AND  a1a_critical_phase_inspectio.deleted=0 
	 AND IFNULL(contacts1.deleted,0)=0 
	 ORDER BY l1_cstm_contact_id_c ASC";

	$reportResult	= $db->query($reportSQL);

	if ($reportResult->num_rows > 0) {
		$template		= new EmailTemplate();
		$emailObj		= new Email();
		$wpSD_docEmailArr 	= array();
		$wpSD_Email 	= array();
		$cpiArray	 	= array();
		$$docArr	 	= array();
		$site_url		= $GLOBALS['sugar_config']['site_url'];
		$template->retrieve_by_string_fields(array('name' => 'Critical Phase Inspections for Review', 'type' => 'email'));

		//Email Body Header
		$emailBody = "<table width='100%' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 13px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>Name</th><th bgcolor='#b3d1ff' align='left'>Inspection Results</th><th bgcolor='#b3d1ff' align='left'>APS Work Product ID</th><th bgcolor='#b3d1ff' align='left'>Study Director</th></tr>";
		 
		
		 
		$cntr =0;
		while ($fetchReportResult = $db->fetchByAssoc($reportResult)) {
			$CPIId					= $fetchReportResult['primaryid'];
			$cpiArray[$cntr]		= $CPIId;
			$CPIname				= $fetchReportResult['A1A_CRITICAL_PHASE_INSF6098C'];
			$inspection_results		= $fetchReportResult['A1A_CRITICAL_PHASE_INSBF63F7'];
			$wpID					= $fetchReportResult['l1_id'];
			$wpName					= $fetchReportResult['l1_name'];
			$SDname					= $fetchReportResult['contacts1_name'];
			$SDID					= $fetchReportResult['l1_cstm_contact_id_c'];
			
			$docArr[$cntr]['cpiName']		=  $CPIname;
			$docArr[$cntr]['cpiID']			=  $CPIId;
			$docArr[$cntr]['cpiResult']  	=  $inspection_results;
			$docArr[$cntr]['wpID']  		=  $wpID;
			$docArr[$cntr]['wpName']  		=  $wpName;
			$docArr[$cntr]['sdName']  		=  $SDname;
			$docArr[$cntr]['sdID']  		=  $SDID;

			$sdBean			= BeanFactory::getBean('Contacts', $SDID);
			$account_name	= $sdBean->account_name;
			
			$primaryEmailAddress = $sdBean->emailAddress->getPrimaryAddress($sdBean);
			if ($primaryEmailAddress != false) {
				if (checkEmailForNotification($primaryEmailAddress)) {
					$wpSD_Email[] = $primaryEmailAddress;
					$wpSD_docEmailArr[] = $sdBean->first_name." ".$sdBean->last_name . "(" . $primaryEmailAddress . ")";
				}
			}

			$CPILink	= '<a target="_blank" href="'. $site_url . '/#A1A_Critical_Phase_Inspectio/' . $CPIId . '" >' . $CPIname . '</a>';
			$wpLink		= '<a target="_blank" href="' . $site_url . '/#M03_Work_Product/' . $wpID . '" >' . $wpName . '</a>';
			$sdLink		= '<a target="_blank" href="' . $site_url . '/#Contacts/' . $SDID . '" >' . $SDname . '</a>';

			$emailBody .= "<tr><td>" . $CPILink . "</td><td>" . $inspection_results . "</td><td>" . $wpLink . "</td><td>" . $sdLink . "</td></tr>";
			$cntr++;
		}

		$wpSD_Email			= array_values($wpSD_Email); //re-index the array elements
		$wpSD_Email			= array_unique($wpSD_Email);
		
		$emailBody		.= "</table>";
		 

		$template->body_html = str_replace('[Report_Table]', $emailBody, $template->body_html);

		$defaults		= $emailObj->getSystemDefaultEmail();
		$mail			= new SugarPHPMailer();
		$mail->setMailerForSystem();
		$mail->IsHTML(true);
		$mail->From		= $defaults['email'];
		$mail->FromName = $defaults['name'];
		$mail->Subject	= $template->subject;
		$mail->Body		= $template->body_html;

		foreach ($wpSD_Email as $wmail) {
			$mail->AddAddress($wmail); 
		}
		
		

		$mail->AddAddress('esteinmetz@apsemail.com');
		$mail->AddAddress('edrake@apsemail.com');
		$mail->AddAddress('jpomonis@apsemail.com');
		$mail->AddAddress('emarkuson@apsemail.com');
		//$mail->AddAddress('fxs_mjohnson@apsemail.com');
		$mail->AddBCC('vsuthar@apsemail.com');
		//If their exist some valid email addresses then send email

		if (!$mail->Send()) {
			$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
		} else {
			$GLOBALS['log']->debug('email sent');
		}
		unset($mail);


		$eDocNewName = "Critical Phase Inspections for Review " . $fileDate . ".pdf";
		/*Save Document after sending email if Work product compliance is GLP*/ {
			$emailDocId  = "";
			$edoc_sql = "SELECT `id`,`type_2`,`document_name` FROM `edoc_email_documents` AS EDOC 
									 WHERE EDOC.`document_name` LIKE '" . $eDocNewName . "' AND deleted=0 ORDER BY `EDOC`.`date_entered` ASC";
			$edoc_exec = $db->query($edoc_sql);
			while ($resultEdoc = $db->fetchByAssoc($edoc_exec)) {
				$type_2		= $resultEdoc['type_2'];
				$emailDocId  = $resultEdoc['id'];
			}

			if ($emailDocId == "") {
				//Create bean
				$emailDocBean = BeanFactory::newBean('EDoc_Email_Documents');
				$emailDocBean->document_name	= $eDocNewName;
				$emailDocBean->department		= 'Quality Assurance Unit';
				$emailDocBean->type_2			= "Other";
				$emailDocBean->assigned_user_id	= $current_user->id;

				$emailDocBean->filename	= $eDocNewName;
				$emailDocBean->file_ext	= 'pdf';
				$emailDocBean->file_mime_type	= 'application/pdf';

				$emailDocBean->save();
				$emailDocId = $emailDocBean->id;

				if (count($cpiArray) > 0) {
					foreach ($cpiArray as $cpi) {
						$gid = create_guid(); //strtolower($GUID);
						$sql_msb = "INSERT INTO `edoc_email_documents_a1a_critical_phase_inspectio_1_c` (id,edoc_emailbf38cuments_ida, edoc_emailc542spectio_idb) VALUES ('" . $gid . "','" . $emailDocId . "', '" . $cpi . "')";
						$db->query($sql_msb);
					}
				}
			}

			$ss				= new Sugar_Smarty();
			$ss->security	= true;
			$ss->security_settings['PHP_TAGS'] = false;
			if (defined('SUGAR_SHADOW_PATH')) {
				$ss->secure_dir[] = SUGAR_SHADOW_PATH;
			}
			$ss->assign('MOD', $GLOBALS['mod_strings']);
			$ss->assign('APP', $GLOBALS['app_strings']);
			$ss->assign('docArr', $docArr);
			$html2 = $ss->fetch('custom/modules/A1A_Critical_Phase_Inspectio/tpls/cpi_review_email.tpl');
			
			//top part
			
			$top_html = "";  
			
			$offset		=  -18000;
			//$offset	=  -21600; //daylight Saving
			$offset_time	= time() + $offset +1;
			$formattedDate	= date("D m/d/Y g:i a",$offset_time);

			
			
			
			$wpSD_docEmailArr[] =  "Erik Steinmetz(esteinmetz@apsemail.com)";
			$wpSD_docEmailArr[] =  "Emily Drake(edrake@apsemail.com)";
			$wpSD_docEmailArr[] =  "Jim Pomonis(jpomonis@apsemail.com)";
			$wpSD_docEmailArr[] =  "Emily Markuson(emarkuson@apsemail.com)";
			//$wpSD_docEmailArr[] =  "Madeline Johnson(fxs_mjohnson@apsemail.com)";
			
			$wpSD_docEmailArr	= array_values($wpSD_docEmailArr); //re-index the array elements
			$wpSD_docEmailArr	= array_unique($wpSD_docEmailArr);
			//$wpSD_Email[] =  "Vinod Suthar(vsuthar@apsemail.com)";
			
			$top_html = '<span style="font-size:8pt;">'.$formattedDate.'</span>';
			$top_html .= '<br/><span style="font-size:8pt;">From: PMT (do_not_reply@apsemail.com)</span>';
			$top_html .= '<br/><span style="font-size:8pt;">Subject: '.$template->subject.'</span>';
			$top_html .= '<br/><span style="font-size:8pt;">To: '.implode(",", $wpSD_docEmailArr).'</span>';
							
			$borderHtml = '<p><span style="font-size:10pt;">----------------------------------------------------------------------------------------------------------------------------------------------------------------</span></p><p><span style="font-size:12pt;">Hello</span></p><p><span style="font-size:12pt;">Please review yesterday\'s Critical Phase Inspection records below.</span></p>';
			$body_html_new = $top_html.$borderHtml.$html2;
			////////////////////


			$fileName		= $eDocNewName;
			$fileDocID		= $emailDocId;
			$fileInfo		= create_PDF($body_html_new, $fileName);
			$objFile		= new UploadFile('uploadfile');
			// Set the filename
			$objFile->file_ext	= 'pdf';
			$objFile->set_for_soap($fileName, (sugar_file_get_contents($fileInfo['file_path']))); 
			$objFile->create_stored_filename();
			$objFile->final_move($fileDocID);
		}
		/*ENd Email document creation*/
	}

	return true;
}

function create_PDF($html, $name)
{
	require_once('vendor/tcpdf/tcpdf.php');
	$tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$tcpdf->SetCreator(PDF_CREATOR);
	$name = str_replace(' ', '_', $name);
	$date = date('Y-m-d');
	$file_info['file_path'] = "upload/" . $name;
	$file_info['file_name'] = $name;

	$tcpdf->AddPage();
	$tcpdf->writeHTML($html, true, false, true, false, '');
	$tcpdf->Output($file_info['file_path'], "F");
	return $file_info;
}

function checkEmailForNotification($email) {
    $domainToSearch = 'apsemail.com';
    $domain_name = substr(strrchr($email, "@"), 1);
    if (trim(strtolower($domain_name) == $domainToSearch)) {
        return true;
    }
    return false;
}


