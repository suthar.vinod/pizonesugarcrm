<?php
//Add job in job string
$job_strings[] = 'get_days_to_expiration_date';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function get_days_to_expiration_date()
{
	global $db;
	$current_date	= date("Y-m-d"); //current date
	
	$ii_sql = "SELECT id,expiration_date,expiration_date_time,days_to_expiration 
			FROM `ii_inventory_item` 
			WHERE deleted=0 AND expiration_date >='".$current_date."' OR expiration_date_time >='".$current_date."'";
	$resultII = $db->query($ii_sql);
	if ($resultII->num_rows > 0) {
		while ($rowII = $db->fetchByAssoc($resultII)) {
			$id 				= $rowII['id'];
			$expirationDate		= $rowII['expiration_date'];
			$expirationDateTime = $rowII['expiration_date_time'];

			$checkDate = ($expirationDate!="")?$expirationDate:$expirationDateTime;

			if($checkDate!=""){				
				$expDatetime	= date('Y-m-d 0:0:0',strtotime($checkDate)); 
				$currentTime	= date('Y-m-d 0:0:0', time()); 
				$date1			= strtotime($expDatetime);  
				$date2			= strtotime($currentTime); 
				// Formulate the Difference between two dates 
				$diff = abs($date1 - $date2); /*Get Time Difference*/
				$days = floor(($diff)/ (60*60*24));	
				
				if($date1 > $date2){
					$no_of_days = $days;
				}else{
					$no_of_days = '0';
				}
				$iiBean   	= BeanFactory::getBean('II_Inventory_Item', $id);
				$iiBean->days_to_expiration    	 = $no_of_days;
				$iiBean->save();
				$iiUpdate = "UPDATE `ii_inventory_item_audit` SET created_by='943a2d38-7bd3-11e9-99fb-06e41dba421a' WHERE `field_name`='days_to_expiration' AND `parent_id`='".$id."' AND `date_created` >='".$current_date."'";
				$db->query($iiUpdate); //943a2d38-7bd3-11e9-99fb-06e41dba421a
			}
		}
	}
	return true;
}