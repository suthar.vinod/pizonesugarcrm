<?php
//Add job in job string
$job_strings[] = 'primary_animal_calculation_for_aps001';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function primary_animal_calculation_for_aps001()
{
	global $db;
	$current_date = date("Y-m-d"); //current date
	$entered_date1 = '2020-11-15';


	$WPID[0] = '4a529aae-5165-3149-6db7-5702c7b98a5e';
	$WPID[1] = '811ae58e-9281-2755-25d5-5702c7252696';


	for ($cnt = 0; $cnt < count($WPID); $cnt++) {

		$workProductId 			= $WPID[$cnt];

		$WP_Bean = BeanFactory::retrieveBean('M03_Work_Product', $workProductId);

		$onStudyWithoutTSCount		= 0;
		$onStudyWithTSCount			= 0;
		$totalOnStudyCount			= 0;

		$WPTS 						= array();
		// Query to get On_Study/On_Study Transferred Count


		/*Query to get total number of onStudy/onStudyTransferred Enrollment status Without Test System*/
		$studyWithoutTS_sql = "SELECT  WP_WPE.`m03_work_p9bf5ollment_idb` 
					FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE 
					LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p9bf5ollment_idb`=WPE_CUST.id_c 
					LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p9bf5ollment_idb`=TS.anml_anima9941ollment_idb 
					WHERE WP_WPE.`m03_work_p7d13product_ida`='" . $workProductID . "' AND 
					(WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') AND WP_WPE.deleted=0 AND (TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NULL )";
		//TS.deleted=1 OR TS.deleted IS NULL OR

		$studyWithoutTS_exec = $db->query($studyWithoutTS_sql);

		if ($studyWithoutTS_exec->num_rows > 0) {
			$onStudyWithoutTSCount = $studyWithoutTS_exec->num_rows; //total rows without TS

			while ($fetchWOTS = $db->fetchByAssoc($studyWithoutTS_exec)) {
				$WPTS[]	= $fetchWOTS['m03_work_p9bf5ollment_idb'];
			}
		}

		/*Query to get total number of onStudy/onStudyTransferred Enrollment status With Test System*/
		$studyWithTS_sql = "SELECT  WP_WPE.`m03_work_p9bf5ollment_idb` 
					FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE 
					LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p9bf5ollment_idb`=WPE_CUST.id_c 
					LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p9bf5ollment_idb`=TS.anml_anima9941ollment_idb 
					WHERE WP_WPE.`m03_work_p7d13product_ida`='" . $workProductId . "' 
					AND (WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') AND TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NOT NULL  AND WP_WPE.deleted=0 AND TS.deleted=0 GROUP BY TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida";

		$studyWithTS_exec = $db->query($studyWithTS_sql);

		if ($studyWithTS_exec->num_rows > 0) {
			$onStudyWithTSCount = $studyWithTS_exec->num_rows; //total rows with TS
			while ($fetchWTS = $db->fetchByAssoc($studyWithTS_exec)) {
				$WPTS[]	= $fetchWTS['m03_work_p9bf5ollment_idb'];
			}
		}

		/*=========================================================*/
		/*=========================================================*/

		/*Query to get total number of onStudy/onStudyTransferred Enrollment status Without Test System*/
		$blanket_sql = "SELECT  WP_WPE.`m03_work_p90c4ollment_idb` 
								FROM `m03_work_product_wpe_work_product_enrollment_2_c` AS WP_WPE 
								LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p90c4ollment_idb`=WPE_CUST.id_c 
								LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p90c4ollment_idb`=TS.anml_anima9941ollment_idb 
							WHERE WP_WPE.`m03_work_p9f23product_ida`='" . $workProductId . "' AND 
								(WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') 
								AND WP_WPE.deleted=0 AND (TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NULL )";
		//TS.deleted=1 OR TS.deleted IS NULL OR

		$blanket_exec = $db->query($blanket_sql);

		if ($blanket_exec->num_rows > 0) {
			while ($fetchB1 = $db->fetchByAssoc($blanket_exec)) {
				$WPTS[]	= $fetchB1['m03_work_p90c4ollment_idb'];
			}
		}

		/*Query to get total number of onStudy/onStudyTransferred Enrollment status With Test System*/
		$blanket2_sql = "SELECT  WP_WPE.`m03_work_p90c4ollment_idb` 
									FROM `m03_work_product_wpe_work_product_enrollment_2_c` AS WP_WPE 
										LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p90c4ollment_idb`=WPE_CUST.id_c 
										LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p90c4ollment_idb`=TS.anml_anima9941ollment_idb 
									WHERE WP_WPE.`m03_work_p9f23product_ida`='" . $workProductId . "' 
										AND (WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred')
										AND TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NOT NULL  AND WP_WPE.deleted=0 AND TS.deleted=0 
									GROUP BY TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida";

		$blanket2_exec = $db->query($blanket2_sql);

		if ($blanket2_exec->num_rows > 0) {
			while ($fetchB2 = $db->fetchByAssoc($blanket2_exec)) {
				$WPTS[]	= $fetchB2['m03_work_p90c4ollment_idb'];
			}
		}

		/*=========================================================*/
		/*=========================================================*/
		$totalOnStudyCount  = count(array_unique($WPTS));

		// Get Total value from Bean
		$totalPrimary	= $WP_Bean->total_animals_used_primary_c;

		$totalPrimaryBefore	= $WP_Bean->primary_animals_countdown_c;

		//Calculate countdown Value by subtracting onStudy/Backup value from Total value 
		$Primary_Animals_Countdown	= $totalPrimary - $totalOnStudyCount;


		if ($totalPrimaryBefore != $Primary_Animals_Countdown) {
			$updateQuery = "UPDATE `m03_work_product_cstm` SET `primary_animals_countdown_c`='" . $Primary_Animals_Countdown . "' WHERE `id_c`='" . $workProductId . "' ";
			$db->query($updateQuery);

			$event_id = create_guid();
			$auditsql = 'INSERT INTO m03_work_product_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $workProductId . '","' . $event_id . '",now(),"943a2d38-7bd3-11e9-99fb-06e41dba421a",now(),"total_animals_used_primary_c","int","' . $totalPrimary . '","' . $Primary_Animals_Countdown . '")';
			$auditsqlResult = $db->query($auditsql);

			if ($auditsqlResult) {
				//Inserting audit data in audit_events table
				$source = '{"subject":{"_type":"user","id":"943a2d38-7bd3-11e9-99fb-06e41dba421a","_module":"Users","client":{"_type":"rest-api"}},"attributes":{"platform":"base"}}';
				$sql = "INSERT INTO audit_events(id,type,parent_id,module_name,source,data,date_created) values('" . $event_id . "','update','" . $workProductId . "','M03_Work_Product','" . $source . "',NULL,now())";
				$db->query($sql);
			}
		}
	}


	return true;
}
