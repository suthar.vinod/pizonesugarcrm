<?php
//Add job in job string
$job_strings[] = 'send_analyse_by_date_notification_mail';
/* * This function will send_analyse_by_date_notification_mail from Inventory Item Module. */

function send_analyse_by_date_notification_mail()
{
	global $db;
	
	//Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();
	
	
	$current_date = date("Y-m-d"); //current date
	$current_date2 = date("m/d/Y");
	//Get site url so that appropriate Work Product can be shown in the Email Template
	$site_url = $GLOBALS['sugar_config']['site_url'];
	 

	$queryCustom = "SELECT II.*,IIC.id_c,IIC.analyze_by_date_c,
	IIWP.m03_work_product_ii_inventory_item_1m03_work_product_ida as WP,
	WPC.contact_id_c AS ContactEmailID,emailID.email_address_id,emailAddr.email_address AS emailaddress
	FROM `ii_inventory_item` AS II	
		LEFT JOIN `ii_inventory_item_cstm` AS IIC  ON II.id=IIC.id_c
		LEFT JOIN `m03_work_product_ii_inventory_item_1_c` AS IIWP ON IIWP.m03_work_product_ii_inventory_item_1ii_inventory_item_idb = II.id and IIWP.deleted=0
		LEFT JOIN m03_work_product_cstm AS WPC ON IIWP.m03_work_product_ii_inventory_item_1m03_work_product_ida=WPC.id_c    
		LEFT JOIN `email_addr_bean_rel` AS emailID  ON emailID.bean_id=WPC.contact_id_c
		LEFT JOIN `email_addresses` AS emailAddr  ON emailID.email_address_id=emailAddr.id 
	WHERE (IIC.analyze_by_date_c IS NOT NULL OR IIC.analyze_by_date_c!='') 
						AND IIC.analyze_by_date_c >'now()' AND II.deleted=0 
						AND (IIC.reminder_mail_sent_c IS NULL OR IIC.reminder_mail_sent_c='0')";

	$queryIIResult = $db->query($queryCustom);
	
	 
	while ($fetchIIResult = $db->fetchByAssoc($queryIIResult)) {

		$II_id				= $fetchIIResult['id'];
		$ii_name			= $fetchIIResult['name'];
		$analyzeByDate 		= $fetchIIResult['analyze_by_date_c'];
		$emailAddr 			= $fetchIIResult['emailaddress'];

		// $GLOBALS['log']->fatal('iiName ===>:'.$ii_name."====".$II_id);
		// $GLOBALS['log']->fatal('emailAddr ===>:'.$emailAddr."====".$II_id);
		 
		$date1 = strtotime($analyzeByDate);
		$date2 = strtotime($current_date);
		$diff = abs($date1-$date2);
		$days = floor(($diff)/ (60*60*24)); 

		if((($date1>$date2 && $days<14) || $date1<$date2 || $days==0)){
			//Send Email/
			
			$template->retrieve_by_string_fields(array('name' => 'Reminder Inventory Item Pending Analyze by Date', 'type' => 'email'));
			
			$IILink = '<a target="_blank"href="'.$site_url.'/#II_Inventory_Item/'.$II_id.'">'.$ii_name.'</a>';
					 
			
			$template->body_html = str_replace('[insert_inventory_item_name]', $IILink, $template->body_html);
					
				$defaults	= $emailObj->getSystemDefaultEmail();
				$mail		= new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->IsHTML(true);
				$mail->From		= $defaults['email'];
				$mail->FromName = $defaults['name'];
				$mail->Subject	= "Reminder - Pending Analyze by Date for ".$ii_name;
				$mail->Body		= $template->body_html;
				$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';
					
				$mail->AddAddress('fxs_mjohnson@apsemail.com');
				$mail->AddAddress('sschaefers@apsemail.com');
				// $mail->AddAddress('vsuthar@apsemail.com');
				$mail->AddAddress($emailAddr);
					
				//If their exist some valid email addresses then send email
				if (!empty($wp_emailAdd_dev)) {
					if (!$mail->Send()) {
						$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
					} else {
						$GLOBALS['log']->debug('email sent');
						
						$updateQuery = "UPDATE `ii_inventory_item_cstm` SET `reminder_mail_sent_c`='1' WHERE id_c='".$II_id."'";
						$db->query($updateQuery);
					}
					unset($mail);
				}
		} 
		
	}
	return true;
}
