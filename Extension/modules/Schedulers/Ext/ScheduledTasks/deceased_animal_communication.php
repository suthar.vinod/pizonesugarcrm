<?php
//Add job in job string
$job_strings[] = 'deceased_animal_communication';
/* * This function will send email to pathologist to inform about the Deceased Animals. */

function deceased_animal_communication() {
    global $db;
    
	$offset_time = time(); 
	$currentDate = date("Y-m-d 00:00:00",$offset_time);
	$lastmonthDate = date("Y-m-d 00:00:00", strtotime('-30 days')); 
     
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];
	 
    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();

    $emailAdd = array();  //user to whom email is being sent
	$queryCustom = "SELECT 
						l1_cstm.s_species_id_c l1_cstm_s_species_id_c,
						s_species1.name s_species1_name,
						COUNT(COMM.id) m06_error__allcount, 
						COUNT(DISTINCT  COMM.id) m06_error__count
					FROM m06_error AS COMM
					LEFT JOIN  m06_error_anml_animals_1_c AS l1_1  
						ON COMM.id=l1_1.m06_error_anml_animals_1m06_error_ida AND l1_1.deleted=0

					LEFT JOIN  anml_animals l1 
						ON l1.id=l1_1.m06_error_anml_animals_1anml_animals_idb AND l1.deleted=0
					LEFT JOIN m06_error_cstm m06_error_cstm 
						ON COMM.id = m06_error_cstm.id_c
					LEFT JOIN anml_animals_cstm l1_cstm
						ON l1.id = l1_cstm.id_c
					LEFT JOIN s_species s_species1 
						ON s_species1.id = l1_cstm.s_species_id_c AND IFNULL(s_species1.deleted,0)=0 

					 WHERE (((
					 
					 (m06_error_cstm.error_category_c = 'Deceased Animal'  
						AND m06_error_cstm.related_to_c = 'non Study'
						AND (m06_error_cstm.date_time_discovered_c >= '".$lastmonthDate."' AND m06_error_cstm.date_time_discovered_c <= '".$currentDate."')
						)) 
						AND (((( l1_cstm.s_species_id_c='5de72184-1131-11ea-abb2-02fb813964b8')
					) OR (( l1_cstm.s_species_id_c='5dbadd5e-1131-11ea-b633-02fb813964b8')
					) OR (( l1_cstm.s_species_id_c='5dad1606-1131-11ea-80f4-02fb813964b8')
					) OR (( l1_cstm.s_species_id_c='5d9c2666-1131-11ea-9421-02fb813964b8')
					) OR (( l1_cstm.s_species_id_c='5d89b5d0-1131-11ea-afac-02fb813964b8')
					))))) 
					AND  COMM.deleted=0 
					 AND IFNULL(s_species1.deleted,0)=0 
					 GROUP BY l1_cstm.s_species_id_c 
					,s_species1.name  ORDER BY l1_cstm_s_species_id_c ASC";

    $queryCommResult = $db->query($queryCustom);
    $speciesArr = array();
    while ($fetchCommResult = $db->fetchByAssoc($queryCommResult)) {

        $speciesCount = $fetchCommResult['m06_error__allcount'];
        $speciesName = $fetchCommResult['s_species1_name'];
        $l1_cstm_s_species_id_c = $fetchCommResult['l1_cstm_s_species_id_c'];
		
		if($speciesCount>4){
			$speciesArr[] = $speciesName."s - ".$speciesCount;
			
		}
         
	}

	if(count($speciesArr)>0){ 
		$emailBody = "<table width='600px' cellspacing='0' cellpadding='4' border='0' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'><tr><td>>4 Test Systems of the following species have been reported as deceased in the last 30 days: </td></tr>";
		foreach ($speciesArr as $key => $value) { 
			$emailBody .= "<tr><td>".$value."</td></tr>";		
		}		
		 
		$emailBody .= "</table><br></br>";
		
		$emailBody .= '<address style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-style: normal; font-weight: 400; letter-spacing: normal; text-indent: 0px; text-transform: none; word-spacing: 0px;"><span>CONFIDENTIALITY NOTICE: This e-mail transmission may contain confidential or legally privileged information that is intended only for the individual or entity named in the e-mail address. Any unauthorized distribution or copying of this transmittal or its attachments, if any, is prohibited. If you have received this e-mail transmission in error, please reply to&nbsp;</span><a class="email" href="mailto:mjohnson@apsemail.com">mjohnson@apsemail.com</a><span>, so that American Preclinical Services can arrange for proper delivery, then please delete the message from your inbox. Thank you.</span></address>';
	 
		//$emailAdd[] = 'mjohnson@apsemail.com';
		$emailAdd[] = 'clinicalVets@apsemail.com';
		$emailAdd[] = 'pathologists@apsemail.com';
		
		
		 
		$defaults = $emailObj->getSystemDefaultEmail();
			$mail = new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From = $defaults['email'];
            $mail->FromName = $defaults['name'];
            $mail->Subject = "Deceased Small Animal Trend";
            $mail->Body = $emailBody;
					 
			foreach ($emailAdd as $wmail) {
				$mail->AddAddress($wmail);
			}
			 
			 
			//$mail->AddCC('vsuthar@apsemail.com');
			$mail->AddBCC('cjagadeeswaraiah@apsemail.com');
			//If their exist some valid email addresses then send email
            if (!empty($emailAdd)) {
                if (!$mail->Send()) {
                    $GLOBALS['log']->fatal("Deceased Small Animal Trend email Failed");
                } else {
                    $GLOBALS['log']->fatal('Deceased Small Animal Trend email sent');
                }
				unset($mail);
            }
    }
	
	
	return true;
	}		 
  
	 
?>