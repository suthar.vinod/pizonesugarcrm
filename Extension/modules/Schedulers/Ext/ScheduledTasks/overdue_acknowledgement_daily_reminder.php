<?php
//Add job in job string
$job_strings[] = 'overdue_acknowledgement_daily_reminder';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function overdue_acknowledgement_daily_reminder() {
    global $db;
    $current_date = date("Y-m-d"); //current date
	
	$entered_date1 = '2020-12-15 00:00:00';
	
    $wp_emailAdd = array(); // contains the email addresses of Study Director and its Manager
	$sd_array = array(); //Contains id For Study Directors
    
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];
	$domainToSearch = 'apsemail.com';
    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();

    $queryCustom = "SELECT 
                    commu.id AS communicationID, 
                    commu.name AS CommunicationName,  
                    commu_cstm.target_sd_ack_date_c, 
					commu_cstm.error_category_c,  					
                    commu_cstm.reinspection_date_c, 
                    commu_wp.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
                    wp.name  AS workProductName,
					wp_cstm.contact_id_c,
					wpCode.name AS workProductCode,
					wp_cstm.work_product_compliance_c
            FROM `m06_error` AS commu
                    INNER JOIN m06_error_cstm AS commu_cstm
                    ON  commu.id = commu_cstm.id_c

                    RIGHT JOIN m06_error_m03_work_product_1_c AS commu_wp
                    ON  commu.id = commu_wp.m06_error_m03_work_product_1m06_error_ida
					
					INNER JOIN m03_work_product AS wp
                    ON  commu_wp.m06_error_m03_work_product_1m03_work_product_idb=wp.id 
					
					 INNER JOIN m03_work_product_cstm AS wp_cstm
                    ON  wp.id=wp_cstm.id_c 
					
					LEFT JOIN m03_work_product_code wpCode ON wpCode.id = wp_cstm.m03_work_product_code_id1_c
		WHERE  
			commu_cstm.error_category_c != 'Internal Feedback' 
			AND commu_cstm.error_category_c != 'Feedback'
			AND commu_cstm.error_category_c != 'Equipment Maintenance Request'
			AND commu_cstm.error_category_c != 'External Audit'
			AND commu_cstm.error_category_c != 'Maintenance Request'
			AND commu_cstm.error_category_c != 'Process Audit'
			AND commu_cstm.error_category_c != 'Weekly Sweep'  
			AND commu_cstm.error_category_c != 'Study Specific Charge'
			AND NOT((commu_cstm.error_category_c = 'Work Product Schedule Outcome' AND  ( commu_cstm.error_type_c = 'Passed Pyrogen' OR commu_cstm.error_type_c = 'Not used shared BU' OR commu_cstm.error_type_c = 'Standard Biocomp TS Selection' OR commu_cstm.error_type_c = 'Performed Per Protocol' OR commu_cstm.error_type_c = 'Not Used' OR commu_cstm.error_type_c = 'Sedation for Animal Health')))
			AND YEAR(commu_cstm.target_sd_ack_date_c) >= 2020  
			AND commu_cstm.target_sd_ack_date_c >'" . $entered_date1 . "'
			AND commu_cstm.target_sd_ack_date_c <'" . $current_date . "'
			AND commu_wp.m06_error_m03_work_product_1m03_work_product_idb IS NOT NULL
			AND commu_wp.deleted = 0
			AND commu.deleted = 0
			AND  ((wpCode.name NOT LIKE 'AH%' AND  wpCode.name NOT LIKE 'TR%') OR wpCode.name IS NULL)
			AND commu_cstm.actual_sd_ack_date_c IS NULL 
			
		ORDER BY commu_cstm.target_sd_ack_date_c ASC";

    $queryCommResult = $db->query($queryCustom);

	 while($fetchCommResult = $db->fetchByAssoc($queryCommResult)) {

        $communicationID	= $fetchCommResult['communicationID'];
		$workProductID		= $fetchCommResult['workProductID'];
		$workProductName	= $fetchCommResult['workProductName'];
        $communicationName	= $fetchCommResult['CommunicationName'];
        $targetAckDate		= $fetchCommResult['target_sd_ack_date_c']; 
		$SD_ID				= $fetchCommResult['contact_id_c'];		
		
        if ($SD_ID != "" && $communicationID != "" && $communicationName!=""  && $targetAckDate!="" ) {
			
			$mgt_bean		= BeanFactory::getBean('Contacts', $SD_ID);
			$mgtName		= $mgt_bean->first_name." ".$mgt_bean->last_name;
			$mgtEmailAddress = $mgt_bean->emailAddress->getPrimaryAddress($mgt_bean);
			
			//get Study Director's Manager
			if($mgt_bean->contact_id_c){ 
				$director_id =$mgt_bean->contact_id_c;
				$director_bean = BeanFactory::getBean('Contacts', $director_id);
				$SD_manager_name		= $director_bean->first_name." ".$director_bean->last_name;
				if($director_bean->email1){
					$director_email = $director_bean->email1;
					$SD_manager_email = $director_email;
                }
			}
			
			$sd_array[$SD_ID][]	 = array( 
						"communicationID" => $communicationID,  
						"communicationName" => $communicationName,
						"workProductID" => $workProductID, 
						"workProductName" => $workProductName,
						"ackDate" => date("m-d-Y", strtotime($targetAckDate)),
						"SDName" => $mgtName,
						"SDMail" => $mgtEmailAddress,
						"sd_mgtMail" => $SD_manager_email,
						"sd_mgtName" => $SD_manager_name,
						
					); 							
		}
	}
	
	
	if(count($sd_array)>0){ 
		$cntr = 1;
		foreach ($sd_array as $key => $value) { 
			$communication_name	 =""; 	
		
			unset($wp_emailAdd); 
            /* Get Email id of Assigned User and report to User */
            $wp_emailAdd = array();
			
			//Email Body Header
			$emailBody = "<table width='500px' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Communication ID </th><th bgcolor='#b3d1ff' align='left'>Work Product ID</th><th bgcolor='#b3d1ff' align='left'>Target Aknowledgement Date</th></tr>";	
			
			for($cnt=0;$cnt<count($sd_array[$key]);$cnt++){
				$communication_name = '<a target="_blank"href="'.$site_url.'/#M06_Error/'.$sd_array[$key][$cnt]['communicationID'].'">'.$sd_array[$key][$cnt]['communicationName'] . '</a>';
				$workProduct_name   = "";
				if($sd_array[$key][$cnt]['workProductID']!="" && $sd_array[$key][$cnt]['workProductName']!=""){
					$workProduct_name   = '<a target="_blank" href="'.$site_url.'/#M03_Work_Product/'.$sd_array[$key][$cnt]['workProductID'].'" >'.$sd_array[$key][$cnt]['workProductName'] . '</a>';
				}
			
				$mgtName  = $sd_array[$key][$cnt]['SDName'];
				$mgtMail  = $sd_array[$key][$cnt]['SDMail'];
				$sdMgtMail  = $sd_array[$key][$cnt]['sd_mgtMail'];
				$aknowledgement_date  = $sd_array[$key][$cnt]['ackDate'];
				
				$emailBody .= "<tr><td>".$communication_name."</td><td>".$workProduct_name."</td><td>".$aknowledgement_date."</td></tr>";
			}	
			$emailBody .="</table>";
			
			//$wp_emailAdd[] = 'mconforti@apsemail.com';
			$wp_emailAdd[] = $mgtMail;
			$wp_emailAdd[] = $sdMgtMail;			
			$wp_emailAdd = array_values($wp_emailAdd); //re-index the array elements
			//remove the repeated email addresses
			$wp_emailAdd = array_unique($wp_emailAdd);
			
			//$emailString = implode(",",$wp_emailAdd);
				
			$template->retrieve_by_string_fields(array('name' => 'Overdue Acknowledgement Daily Reminder Consolidate', 'type' => 'email'));
			 
			$template->body_html = str_replace('[MGT Name]', $mgtName, $template->body_html);
			$template->body_html = str_replace('[Consolidate_Report]', $emailBody, $template->body_html);
			  
			$defaults	= $emailObj->getSystemDefaultEmail();
			$mail		= new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From		= $defaults['email'];
            $mail->FromName = $defaults['name'];
            $mail->Subject	= $template->subject;
            $mail->Body		= $template->body_html;
					 
			foreach ($wp_emailAdd as $wmail) { 
				$domain_name = substr(strrchr($wmail, "@"), 1);
				if (trim(strtolower($domain_name) == $domainToSearch)) {
					$mail->AddAddress($wmail); 
				}
			} 			
			$mail->AddBCC('vsuthar@apsemail.com');
			//$mail->AddBCC('cjagadeeswaraiah@apsemail.com');
			//If their exist some valid email addresses then send email
            if (!empty($wp_emailAdd)) {
                //$GLOBALS['log']->fatal("valid email addresses are ". print_r($wpd_emailAdd, 1));
                if (!$mail->Send()) {
                    $GLOBALS['log']->debug("Fail to Send Email, Please check settings");
                } else {
                    $GLOBALS['log']->debug('email sent for ' . $wp_emailAdd);
                }
				unset($mail);
            }
		}
	}	
	return true;
} 
?>