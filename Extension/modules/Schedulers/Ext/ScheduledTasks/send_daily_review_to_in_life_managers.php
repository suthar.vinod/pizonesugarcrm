<?php
//Add job in job string
$job_strings['send_daily_review_to_in_life_managers'] = 'send_daily_review_to_in_life_managers';
//  This function will found out all the pending workproduct Assessment in Communication. */

function send_daily_review_to_in_life_managers()
{
	global $db;
	$emailObj = new Email();
	//Prepare the Email Template to send to Appropriate Members
	$template		= new EmailTemplate();
	$site_url		= $GLOBALS['sugar_config']['site_url'];
	
	$hyperlink = '<a target="_blank" href="'.$site_url.'/#bwc/index.php?module=Reports&action=DailySchedulingNeeds&bwcRedirect=1">Daily Scheduling Needs – LA</a>';

	$template->retrieve_by_string_fields(array('name' => 'Notification of daily scheduling needs - LA', 'type' => 'email'));
	// $template->body_html = str_replace('[REPORT_Name]', 'Daily Scheduling Needs - LA', $template->body_html);
	$template->body_html = str_replace('[Daily_Scheduling_Needs_Report_Data_LA]', $hyperlink, $template->body_html);
	//$template->body_html .= $emailString; 

	$defaults	= $emailObj->getSystemDefaultEmail();
	$mail		= new SugarPHPMailer();
	$mail->setMailerForSystem();
	$mail->IsHTML(true);
	$mail->From		= $defaults['email'];
	$mail->FromName = $defaults['name'];
	$mail->Subject	= $template->subject;
	$mail->Body		= $template->body_html;
		
	$mail->AddAddress('adargis@apsemail.com');  //Amber Dargis
	$mail->AddAddress('nuitermarkt@apsemail.com'); //Natalie Uitermarkt
	$mail->addbcc('vsuthar@apsemail.com'); //vinod suthar
	// $mail->addbcc('spa_testing@apsemail.com'); 
	//If their exist some valid email addresses then send email
	if (!$mail->Send()) {
		$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
	} else {
		$GLOBALS['log']->debug('email sent successfully');
	}
	unset($mail);
	return true;
}