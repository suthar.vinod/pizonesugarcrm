<?php
// created: 2022-12-11 04:10:07
$extensionOrderMap = array (
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.overdue_deliverables.php' => 
  array (
    'md5' => '0d6fa887cd9d3229aa9b6af62b91a39c',
    'mtime' => 1575870196,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.communication_requiring_assessment.php' => 
  array (
    'md5' => 'c981032e72095805c175ff38dd308773',
    'mtime' => 1580582817,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.deceased_animal_communication.php' => 
  array (
    'md5' => '6748b1ed8ed00aed7e46a07d6dd7281d',
    'mtime' => 1598513803,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.communication_requiring_assessment_consolidate.php' => 
  array (
    'md5' => 'b9fb5728eb0e96f919ba662e5eb758f0',
    'mtime' => 1600761777,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.wpd_upload_document_reminder.php' => 
  array (
    'md5' => 'ca96054e063af67fe534bffc45728071',
    'mtime' => 1605160938,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.management_comm_review_daily_reminder.php' => 
  array (
    'md5' => 'e006e1fe2e746aabf36762fd8c1e7a5e',
    'mtime' => 1608019855,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.overdue_acknowledgement_daily_reminder.php' => 
  array (
    'md5' => 'fde49d719cc692f183a465960e97c966',
    'mtime' => 1608019855,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.primary_animal_calculation_for_aps001.php' => 
  array (
    'md5' => '24f70540558f956ad049240ce2fe1314',
    'mtime' => 1616148951,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.create_sc_species_census_record.php' => 
  array (
    'md5' => '6fa352bd56e2a4fb6d6bcd723e31251b',
    'mtime' => 1619682528,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.add_relation_for_missed_communication_record.php' => 
  array (
    'md5' => '824e5b013905da12baff437d97f393ab',
    'mtime' => 1622719206,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.equip_service_workflow_reminder.php' => 
  array (
    'md5' => '0b928e92120607ca4510dd3d37c33eae',
    'mtime' => 1624963371,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.overdue_to_qa_email_workflow.php' => 
  array (
    'md5' => '0a8eecbccfbc057bad92b9a164177f2b',
    'mtime' => 1632976489,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.protocol_due_for_annual_review_email_workflow.php' => 
  array (
    'md5' => 'e85f7316040e68de824c4cea691c5459',
    'mtime' => 1633584986,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.send_analyse_by_date_notification_mail.php' => 
  array (
    'md5' => '683574965dc8f9bf3a6aeb279c380eb3',
    'mtime' => 1636501361,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.get_days_to_expiration_date.php' => 
  array (
    'md5' => '998e6f5927b82d82d3a88f26480c8a54',
    'mtime' => 1637053824,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.send_email_for_biocomp_report.php' => 
  array (
    'md5' => 'd92e2c731d963b974998cfc432f7fe84',
    'mtime' => 1640688105,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.upcoming_tissue_trimming_daily_reminder.php' => 
  array (
    'md5' => '6718d0c6b3cf56fad1241bf49731d1c4',
    'mtime' => 1643115252,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.send_email_for_cpi_report.php' => 
  array (
    'md5' => '55b4744723900659e558db85f3c49240',
    'mtime' => 1643690362,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.gd_status_change_daily_reminder.php' => 
  array (
    'md5' => '90be806f520ece2ed555ec6d3ac75065',
    'mtime' => 1644305238,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.gd_status_change_daily_reminder_second.php' => 
  array (
    'md5' => '3f8ed2646af823b7d1f5fed55424e81a',
    'mtime' => 1644305238,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.td_single_date_recalculate.php' => 
  array (
    'md5' => 'e8ed51ae1b263f117823df2fd622dcc9',
    'mtime' => 1651835002,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.gd_status_change_daily_reminder_batch_id.php' => 
  array (
    'md5' => '4ff10201677f74e7de6b27e50c574e7f',
    'mtime' => 1663837781,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.gd_status_change_daily_reminder_batch_id_second.php' => 
  array (
    'md5' => '342c22cdc14855f5ee01308e75eebd88',
    'mtime' => 1663837781,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.daily_scheduling_needs_sa.php' => 
  array (
    'md5' => '2c62784f521f8ac7c9c5991c707bd3ee',
    'mtime' => 1666261222,
    'is_override' => false,
  ),
  'custom/Extension/modules/Schedulers/Ext/Language/en_us.send_daily_review_to_in_life_managers.php' => 
  array (
    'md5' => 'f3a256ced9b9d777ef88676e648492f0',
    'mtime' => 1666261222,
    'is_override' => false,
  ),
);