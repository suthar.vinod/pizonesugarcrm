<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TYPE_2'] = 'Type';
$mod_strings['LNK_NEW_RECORD'] = 'Create Equipment &amp; Facility Document';
$mod_strings['LNK_LIST'] = 'View Equipment &amp; Facility Documents';
$mod_strings['LBL_MODULE_NAME'] = 'Equipment &amp; Facility Documents';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Equipment &amp; Facility Document';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Equipment &amp; Facility Document';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Equipment &amp; Facility Document vCard';
$mod_strings['LNK_IMPORT_EFD_EQUIPMENT_FACILITY_DOC'] = 'Import Equipment &amp; Facility Documents';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Equipment &amp; Facility Documents List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Equipment &amp; Facility Document';
$mod_strings['LBL_EFD_EQUIPMENT_FACILITY_DOC_SUBPANEL_TITLE'] = 'Equipment &amp; Facility Documents';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Equipment &amp; Facility Documents';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EFD_EQUIPMENT_FACILITY_DOC_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LBL_RECORD_BODY'] = 'Equipment &amp; Facility Documents';
$mod_strings['LBL_EFD_EQUIPMENT_FACILITY_DOC_FOCUS_DRAWER_DASHBOARD'] = 'Equipment &amp; Facility Documents Focus Drawer';
$mod_strings['LBL_EFD_EQUIPMENT_FACILITY_DOC_RECORD_DASHBOARD'] = 'Equipment &amp; Facility Documents Record Dashboard';
