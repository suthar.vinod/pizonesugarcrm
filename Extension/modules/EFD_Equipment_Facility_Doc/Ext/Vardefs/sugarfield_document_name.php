<?php
 // created: 2019-02-19 17:40:35
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['audited']=true;
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['massupdate']=false;
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['merge_filter']='disabled';
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['unified_search']=false;
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.0',
  'searchable' => true,
);
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['calculated']=false;
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['readonly'] = true;
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['required'] = false;
 ?>