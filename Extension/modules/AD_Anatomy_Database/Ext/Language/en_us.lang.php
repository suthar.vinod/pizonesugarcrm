<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SPECIES'] = 'Species (out of use)';
$mod_strings['LBL_BREED'] = 'Breed';
$mod_strings['LBL_ANIMAL_NUMBER'] = 'Animal Number';
$mod_strings['LBL_WORK_PRODUCT_ID'] = 'Work Product ID';
$mod_strings['LBL_WEIGHT'] = 'Weight (kg)';
$mod_strings['LBL_IMAGING_MODALITY'] = 'Imaging Modality';
$mod_strings['LBL_ANATOMICAL_LOCATION'] = 'Anatomical Location';
$mod_strings['LBL_MEASUREMENT'] = 'Measurement (mm)';
$mod_strings['LBL_WEIGHT_RANGE'] = 'Weight Range (kg)';
$mod_strings['LBL_SPECIES_2_S_SPECIES_ID'] = 'Species 2 (related Species ID)';
$mod_strings['LBL_SPECIES_2'] = 'Species';
$mod_strings['LBL_TEST_SYSTEM'] = 'Test System';
$mod_strings['LBL_WORK_PRODUCT'] = 'Work Product';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'For MAJ use';
$mod_strings['LBL_AD_ANATOMY_DATABASE_FOCUS_DRAWER_DASHBOARD'] = 'Anatomy Database Focus Drawer';
$mod_strings['LBL_AD_ANATOMY_DATABASE_RECORD_DASHBOARD'] = 'Anatomy Database Record Dashboard';
