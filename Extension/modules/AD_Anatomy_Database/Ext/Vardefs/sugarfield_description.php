<?php
 // created: 2020-09-03 07:39:06
$dictionary['AD_Anatomy_Database']['fields']['description']['audited']=true;
$dictionary['AD_Anatomy_Database']['fields']['description']['massupdate']=false;
$dictionary['AD_Anatomy_Database']['fields']['description']['hidemassupdate']=false;
$dictionary['AD_Anatomy_Database']['fields']['description']['comments']='Full text of the note';
$dictionary['AD_Anatomy_Database']['fields']['description']['duplicate_merge']='enabled';
$dictionary['AD_Anatomy_Database']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['AD_Anatomy_Database']['fields']['description']['merge_filter']='disabled';
$dictionary['AD_Anatomy_Database']['fields']['description']['unified_search']=false;
$dictionary['AD_Anatomy_Database']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['AD_Anatomy_Database']['fields']['description']['calculated']=false;
$dictionary['AD_Anatomy_Database']['fields']['description']['rows']='6';
$dictionary['AD_Anatomy_Database']['fields']['description']['cols']='80';

 ?>