<?php
 // created: 2019-06-25 14:14:21
$dictionary['AD_Anatomy_Database']['fields']['name']['len']='255';
$dictionary['AD_Anatomy_Database']['fields']['name']['audited']=true;
$dictionary['AD_Anatomy_Database']['fields']['name']['massupdate']=false;
$dictionary['AD_Anatomy_Database']['fields']['name']['unified_search']=false;
$dictionary['AD_Anatomy_Database']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['AD_Anatomy_Database']['fields']['name']['calculated']=false;

 ?>