<?php
 // created: 2021-01-21 14:15:48
$layout_defs["M03_Work_Product_Code"]["subpanel_setup"]['m03_work_product_m03_work_product_code_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'm03_work_product_m03_work_product_code_1',
);
