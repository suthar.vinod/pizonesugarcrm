<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Work Product Code ID';
$mod_strings['LBL_WP_CODE_DESCRIPTION'] = 'WP Code Description';
$mod_strings['LBL_STANDARD_PROTOCOL_TITLE'] = 'Standard Protocol Title';
$mod_strings['LBL_FUNCTIONAL_AREA'] = 'Functional Area';
$mod_strings['LBL_RECORD_BODY'] = 'WPC';
$mod_strings['LBL_DESCRIPTION'] = 'Notes';
$mod_strings['LBL_APPLICABLE_STANDARD_PROTOCOL_ERD_ERROR_DOCUMENTS_ID'] = 'Applicable Standard Protocol (related Error Documents ID)';
$mod_strings['LBL_APPLICABLE_STANDARD_PROTOCOL'] = 'Applicable Standard Protocol';
$mod_strings['LBL_TOTAL_ANIMAL_NUMBER'] = 'Total Animal Number';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Blanket Work Product(s)';
$mod_strings['LBL_M03_WORK_PRODUCT_CODE_FOCUS_DRAWER_DASHBOARD'] = 'Work Product Codes Focus Drawer';
$mod_strings['LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_WPG_WORK_PRODUCT_GROUP_TITLE'] = 'Work Product Group';
$mod_strings['LBL_SAMPLE_PREPARATION_CPI_AVG'] = 'Sample Preparation CPI Avg.';
$mod_strings['LBL_APPLICATION_CPI_AVG'] = 'Application CPI Avg.';
$mod_strings['LBL_EVALUATION_CPI_AVG'] = 'Evaluation CPI Avg.';
$mod_strings['LBL_FINAL_REPORT_CPI_AVG'] = 'Final Report CPI Avg.';
$mod_strings['LBL_SAMPLE_PREPARATION_CPI_AVG_H'] = 'Sample Preparation CPI Avg. (hours)';
$mod_strings['LBL_APPLICATION_CPI_AVG_HOURS'] = 'Application CPI Avg. (hours)';
$mod_strings['LBL_EVALUATION_CPI_AVG_HOURS'] = 'Evaluation CPI Avg. (hours)';
$mod_strings['LBL_FINAL_REPORT_CPI_AVG_HOURS'] = 'Final Report CPI Avg. (hours)';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_TEST_PRICE'] = 'Test Price';
$mod_strings['LBL_WPC_TYPE'] = 'WPC Type';
