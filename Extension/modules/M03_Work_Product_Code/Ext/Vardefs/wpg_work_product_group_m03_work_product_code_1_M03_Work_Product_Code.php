<?php
// created: 2021-04-20 09:04:33
$dictionary["M03_Work_Product_Code"]["fields"]["wpg_work_product_group_m03_work_product_code_1"] = array (
  'name' => 'wpg_work_product_group_m03_work_product_code_1',
  'type' => 'link',
  'relationship' => 'wpg_work_product_group_m03_work_product_code_1',
  'source' => 'non-db',
  'module' => 'WPG_Work_Product_Group',
  'bean_name' => 'WPG_Work_Product_Group',
  'side' => 'right',
  'vname' => 'LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'id_name' => 'wpg_work_p164at_group_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product_Code"]["fields"]["wpg_work_product_group_m03_work_product_code_1_name"] = array (
  'name' => 'wpg_work_product_group_m03_work_product_code_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_WPG_WORK_PRODUCT_GROUP_TITLE',
  'save' => true,
  'id_name' => 'wpg_work_p164at_group_ida',
  'link' => 'wpg_work_product_group_m03_work_product_code_1',
  'table' => 'wpg_work_product_group',
  'module' => 'WPG_Work_Product_Group',
  'rname' => 'name',
);
$dictionary["M03_Work_Product_Code"]["fields"]["wpg_work_p164at_group_ida"] = array (
  'name' => 'wpg_work_p164at_group_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE_ID',
  'id_name' => 'wpg_work_p164at_group_ida',
  'link' => 'wpg_work_product_group_m03_work_product_code_1',
  'table' => 'wpg_work_product_group',
  'module' => 'WPG_Work_Product_Group',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
