<?php
 // created: 2021-09-21 11:00:05
$dictionary['M03_Work_Product_Code']['fields']['final_report_cpi_avg_hours_c']['labelValue']='Final Report CPI Avg. (hours)';
$dictionary['M03_Work_Product_Code']['fields']['final_report_cpi_avg_hours_c']['enforced']='';
$dictionary['M03_Work_Product_Code']['fields']['final_report_cpi_avg_hours_c']['dependency']='';
$dictionary['M03_Work_Product_Code']['fields']['final_report_cpi_avg_hours_c']['required_formula']='';
$dictionary['M03_Work_Product_Code']['fields']['final_report_cpi_avg_hours_c']['readonly_formula']='';

 ?>