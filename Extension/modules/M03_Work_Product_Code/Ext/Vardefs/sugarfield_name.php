<?php
 // created: 2016-10-25 03:27:08
$dictionary['M03_Work_Product_Code']['fields']['name']['len'] = '255';
$dictionary['M03_Work_Product_Code']['fields']['name']['audited'] = true;
$dictionary['M03_Work_Product_Code']['fields']['name']['massupdate'] = false;
$dictionary['M03_Work_Product_Code']['fields']['name']['unified_search'] = false;
$dictionary['M03_Work_Product_Code']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['M03_Work_Product_Code']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['M03_Work_Product_Code']['fields']['name']['full_text_search']['boost'] = 1.5500000000000000444089209850062616169452667236328125;
$dictionary['M03_Work_Product_Code']['fields']['name']['calculated'] = false;

