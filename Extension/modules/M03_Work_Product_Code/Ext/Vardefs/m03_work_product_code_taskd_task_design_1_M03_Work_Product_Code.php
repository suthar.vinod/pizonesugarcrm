<?php
// created: 2021-12-02 09:36:03
$dictionary["M03_Work_Product_Code"]["fields"]["m03_work_product_code_taskd_task_design_1"] = array (
  'name' => 'm03_work_product_code_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_code_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'vname' => 'LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'id_name' => 'm03_work_p7fc0ct_code_ida',
  'link-type' => 'many',
  'side' => 'left',
);
