<?php
 // created: 2019-08-13 12:11:32
$dictionary['M03_Work_Product_Code']['fields']['description']['audited']=true;
$dictionary['M03_Work_Product_Code']['fields']['description']['massupdate']=false;
$dictionary['M03_Work_Product_Code']['fields']['description']['comments']='Full text of the note';
$dictionary['M03_Work_Product_Code']['fields']['description']['duplicate_merge']='enabled';
$dictionary['M03_Work_Product_Code']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['M03_Work_Product_Code']['fields']['description']['merge_filter']='disabled';
$dictionary['M03_Work_Product_Code']['fields']['description']['unified_search']=false;
$dictionary['M03_Work_Product_Code']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['M03_Work_Product_Code']['fields']['description']['calculated']=false;
$dictionary['M03_Work_Product_Code']['fields']['description']['rows']='6';
$dictionary['M03_Work_Product_Code']['fields']['description']['cols']='80';

 ?>