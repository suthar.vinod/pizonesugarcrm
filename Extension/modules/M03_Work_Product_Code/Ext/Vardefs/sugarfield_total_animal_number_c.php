<?php
 // created: 2021-01-12 10:30:38
$dictionary['M03_Work_Product_Code']['fields']['total_animal_number_c']['labelValue']='Total Animal Number';
$dictionary['M03_Work_Product_Code']['fields']['total_animal_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Code']['fields']['total_animal_number_c']['enforced']='';
$dictionary['M03_Work_Product_Code']['fields']['total_animal_number_c']['dependency']='';
$dictionary['M03_Work_Product_Code']['fields']['total_animal_number_c']['required_formula']='';

 ?>