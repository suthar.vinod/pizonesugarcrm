<?php
 // created: 2022-04-05 07:02:17
$dictionary['M03_Work_Product_Code']['fields']['test_price_c']['labelValue']='Test Price';
$dictionary['M03_Work_Product_Code']['fields']['test_price_c']['enforced']='';
$dictionary['M03_Work_Product_Code']['fields']['test_price_c']['dependency']='';
$dictionary['M03_Work_Product_Code']['fields']['test_price_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['M03_Work_Product_Code']['fields']['test_price_c']['required_formula']='';
$dictionary['M03_Work_Product_Code']['fields']['test_price_c']['readonly_formula']='';

 ?>