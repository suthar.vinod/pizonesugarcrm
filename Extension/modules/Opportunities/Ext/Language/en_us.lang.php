<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Create Opportunity';
$mod_strings['LNK_CREATE'] = 'Create Deal';
$mod_strings['LBL_MODULE_NAME'] = 'Opportunities';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Opportunity';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Create Opportunity';
$mod_strings['LNK_OPPORTUNITY_LIST'] = 'View Opportunities';
$mod_strings['LNK_OPPORTUNITY_REPORTS'] = 'View Opportunity Reports';
$mod_strings['LNK_IMPORT_OPPORTUNITIES'] = 'Import Opportunities';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Opportunity List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Opportunity Search';
$mod_strings['LBL_TOP_OPPORTUNITIES'] = 'My Top Open Opportunities';
$mod_strings['LBL_MY_CLOSED_OPPORTUNITIES'] = 'My Closed Opportunities';
$mod_strings['LBL_PROJECTS_SUBPANEL_TITLE'] = 'Projects';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Company Name';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
