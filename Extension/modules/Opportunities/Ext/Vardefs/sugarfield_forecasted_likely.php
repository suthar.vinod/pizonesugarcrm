<?php
 // created: 2022-09-05 03:29:37
$dictionary['Opportunity']['fields']['forecasted_likely']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['forecasted_likely']['comments']='Rollup of included RLIs on the Opportunity';
$dictionary['Opportunity']['fields']['forecasted_likely']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['forecasted_likely']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['forecasted_likely']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['forecasted_likely']['formula']='ifElse(equal(indexOf($commit_stage, forecastIncludedCommitStages()), -1), 0, $amount)';
$dictionary['Opportunity']['fields']['forecasted_likely']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>