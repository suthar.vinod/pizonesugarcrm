<?php
// created: 2019-02-13 23:29:07
$dictionary["TM_Tradeshow_Management"]["fields"]["tm_tradeshow_management_td_tradeshow_documents_1"] = array (
  'name' => 'tm_tradeshow_management_td_tradeshow_documents_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_td_tradeshow_documents_1',
  'source' => 'non-db',
  'module' => 'TD_Tradeshow_Documents',
  'bean_name' => 'TD_Tradeshow_Documents',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TD_TRADESHOW_DOCUMENTS_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'id_name' => 'tm_tradeshb931agement_ida',
  'link-type' => 'many',
  'side' => 'left',
);
