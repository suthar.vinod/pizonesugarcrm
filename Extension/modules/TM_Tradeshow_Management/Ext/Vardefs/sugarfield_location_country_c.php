<?php
 // created: 2019-04-23 12:23:14
$dictionary['TM_Tradeshow_Management']['fields']['location_country_c']['labelValue']='Location Country';
$dictionary['TM_Tradeshow_Management']['fields']['location_country_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['location_country_c']['group']='location_c';
$dictionary['TM_Tradeshow_Management']['fields']['location_country_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['location_country_c']['dependency']='';

 ?>