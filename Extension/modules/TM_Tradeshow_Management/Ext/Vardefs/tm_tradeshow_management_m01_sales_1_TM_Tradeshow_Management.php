<?php
// created: 2018-04-23 18:02:37
$dictionary["TM_Tradeshow_Management"]["fields"]["tm_tradeshow_management_m01_sales_1"] = array (
  'name' => 'tm_tradeshow_management_m01_sales_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'id_name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
  'link-type' => 'many',
  'side' => 'left',
);
