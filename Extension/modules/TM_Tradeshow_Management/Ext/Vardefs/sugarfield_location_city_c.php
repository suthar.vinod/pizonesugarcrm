<?php
 // created: 2019-04-23 12:22:13
$dictionary['TM_Tradeshow_Management']['fields']['location_city_c']['labelValue']='Location City';
$dictionary['TM_Tradeshow_Management']['fields']['location_city_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['location_city_c']['group']='location_c';
$dictionary['TM_Tradeshow_Management']['fields']['location_city_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['location_city_c']['dependency']='';

 ?>