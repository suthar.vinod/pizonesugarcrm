<?php
 // created: 2019-04-23 12:21:22
$dictionary['TM_Tradeshow_Management']['fields']['name']['len']='255';
$dictionary['TM_Tradeshow_Management']['fields']['name']['audited']=true;
$dictionary['TM_Tradeshow_Management']['fields']['name']['massupdate']=false;
$dictionary['TM_Tradeshow_Management']['fields']['name']['unified_search']=false;
$dictionary['TM_Tradeshow_Management']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['TM_Tradeshow_Management']['fields']['name']['calculated']=false;

 ?>