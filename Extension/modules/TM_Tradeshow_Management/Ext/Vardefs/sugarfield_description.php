<?php
 // created: 2021-01-21 06:56:36
$dictionary['TM_Tradeshow_Management']['fields']['description']['audited']=true;
$dictionary['TM_Tradeshow_Management']['fields']['description']['massupdate']=false;
$dictionary['TM_Tradeshow_Management']['fields']['description']['hidemassupdate']=false;
$dictionary['TM_Tradeshow_Management']['fields']['description']['comments']='Full text of the note';
$dictionary['TM_Tradeshow_Management']['fields']['description']['duplicate_merge']='enabled';
$dictionary['TM_Tradeshow_Management']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['TM_Tradeshow_Management']['fields']['description']['merge_filter']='disabled';
$dictionary['TM_Tradeshow_Management']['fields']['description']['unified_search']=false;
$dictionary['TM_Tradeshow_Management']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['TM_Tradeshow_Management']['fields']['description']['calculated']=false;
$dictionary['TM_Tradeshow_Management']['fields']['description']['rows']='6';
$dictionary['TM_Tradeshow_Management']['fields']['description']['cols']='80';

 ?>