<?php
 // created: 2019-04-23 12:22:45
$dictionary['TM_Tradeshow_Management']['fields']['location_state_c']['labelValue']='Location State';
$dictionary['TM_Tradeshow_Management']['fields']['location_state_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['location_state_c']['group']='location_c';
$dictionary['TM_Tradeshow_Management']['fields']['location_state_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['location_state_c']['dependency']='';

 ?>