<?php
 // created: 2019-02-13 23:29:07
$layout_defs["TM_Tradeshow_Management"]["subpanel_setup"]['tm_tradeshow_management_td_tradeshow_documents_1'] = array (
  'order' => 100,
  'module' => 'TD_Tradeshow_Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TM_TRADESHOW_MANAGEMENT_TD_TRADESHOW_DOCUMENTS_1_FROM_TD_TRADESHOW_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'tm_tradeshow_management_td_tradeshow_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
