<?php
 // created: 2022-03-31 11:41:45
$dictionary['OR_Order_Request']['fields']['notes_c']['labelValue']='Notes';
$dictionary['OR_Order_Request']['fields']['notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['OR_Order_Request']['fields']['notes_c']['enforced']='';
$dictionary['OR_Order_Request']['fields']['notes_c']['dependency']='';
$dictionary['OR_Order_Request']['fields']['notes_c']['required_formula']='';
$dictionary['OR_Order_Request']['fields']['notes_c']['readonly']='1';
$dictionary['OR_Order_Request']['fields']['notes_c']['readonly_formula']='not(equal($status_c,"Open"))';

 ?>