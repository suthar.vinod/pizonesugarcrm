<?php
// created: 2021-03-03 11:30:08
$dictionary["OR_Order_Request"]["fields"]["or_order_request_ori_order_request_item_1"] = array (
  'name' => 'or_order_request_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'or_order_request_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'vname' => 'LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_OR_ORDER_REQUEST_TITLE',
  'id_name' => 'or_order_request_ori_order_request_item_1or_order_request_ida',
  'link-type' => 'many',
  'side' => 'left',
);
