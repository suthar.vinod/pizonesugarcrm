<?php
$dependencies['OR_Order_Request']['status_field_set_options_dep'] = array(
	'hooks' => array("all"),
	'trigger' => 'true',
	'triggerFields' => array('or_order_request_ori_order_request_item_1', 'or_order_request_ori_order_request_item_1or_order_request_ida','po_purchase_order_ori_order_request_item_1','status_c'),
	'onload' => true,
	'actions' => array(
		array(
			'name' => 'SetOptions',
			'params' => array(
				'target' => 'status_c',
				'keys' => 'ifElse(or(equal($status_c,"Partially Submitted"),equal($status_c,"Fully Submitted")),getDropdownKeySet("or_status_list"),getDropdownKeySet("orstatuslistnew"))',
				'labels' => 'ifElse(or(equal($status_c,"Partially Submitted"),equal($status_c,"Fully Submitted")),getDropdownValueSet("or_status_list"),getDropdownValueSet("orstatuslistnew"))',
			),
		),
	),
);

$dependencies['OR_Order_Request']['status_field_set_options_readonly_dep'] = array(
	'hooks' => array("all"),
	'trigger' => 'true',
	'triggerFields' => array('or_order_request_ori_order_request_item_1', 'or_order_request_ori_order_request_item_1or_order_request_ida','po_purchase_order_ori_order_request_item_1','status_c'),
	'onload' => true,
	'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status_c',
                'value' => 'ifElse(or(equal($status_c,"Partially Submitted"),equal($status_c,"Fully Submitted")),true,false)',
            ),
        ),
    ),
);
?>