<?php
 // created: 2021-10-19 09:18:33
$layout_defs["OR_Order_Request"]["subpanel_setup"]['or_order_request_ori_order_request_item_1'] = array (
  'order' => 100,
  'module' => 'ORI_Order_Request_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'get_subpanel_data' => 'or_order_request_ori_order_request_item_1',
);
