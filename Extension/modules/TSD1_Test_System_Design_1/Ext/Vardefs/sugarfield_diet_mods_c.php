<?php
 // created: 2021-12-28 09:24:08
$dictionary['TSD1_Test_System_Design_1']['fields']['diet_mods_c']['labelValue']='Diet Mods or Unapproved Medications';
$dictionary['TSD1_Test_System_Design_1']['fields']['diet_mods_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['diet_mods_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['diet_mods_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['diet_mods_c']['readonly_formula']='';

 ?>