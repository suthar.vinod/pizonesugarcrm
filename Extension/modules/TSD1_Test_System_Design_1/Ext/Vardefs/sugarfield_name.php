<?php
 // created: 2021-02-11 09:15:26
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['importable']='false';
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['duplicate_merge']='disabled';
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['merge_filter']='disabled';
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['unified_search']=false;
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['calculated']='true';
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['formula']='concat(related($m03_work_product_tsd1_test_system_design_1_1,"name")," TS",getDropdownValue("number_list",$number_1))';
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['enforced']=true;

 ?>