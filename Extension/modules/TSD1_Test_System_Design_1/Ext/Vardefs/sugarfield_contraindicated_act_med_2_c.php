<?php
 // created: 2021-12-28 09:23:20
$dictionary['TSD1_Test_System_Design_1']['fields']['contraindicated_act_med_2_c']['labelValue']='Ordering Contraindicated Activities or Meds';
$dictionary['TSD1_Test_System_Design_1']['fields']['contraindicated_act_med_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['contraindicated_act_med_2_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['contraindicated_act_med_2_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['contraindicated_act_med_2_c']['readonly_formula']='';

 ?>