<?php
 // created: 2021-02-18 08:56:07
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['audited']=true;
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['massupdate']=false;
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['hidemassupdate']=false;
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['comments']='Full text of the note';
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['duplicate_merge']='enabled';
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['merge_filter']='disabled';
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['unified_search']=false;
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['calculated']=false;
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['rows']='6';
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['cols']='80';

 ?>