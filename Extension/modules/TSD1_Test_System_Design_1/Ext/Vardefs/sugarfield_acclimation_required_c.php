<?php
 // created: 2021-12-28 09:20:45
$dictionary['TSD1_Test_System_Design_1']['fields']['acclimation_required_c']['labelValue']='Prolonged Restraint Acclimation Required (Obsolete)';
$dictionary['TSD1_Test_System_Design_1']['fields']['acclimation_required_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['acclimation_required_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['acclimation_required_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['acclimation_required_c']['readonly_formula']='';

 ?>