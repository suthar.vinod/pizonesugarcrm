<?php
 // created: 2021-12-07 09:45:12
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_integer_c']['labelValue']='Max. Duration Integer';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_integer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_integer_c']['enforced']='false';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_integer_c']['dependency']='or(isInList($type_of_acclimation_c,createList("Manual restraint","Leash","Sling","Imaging Cart")),equal($purpose_c,"Restrict animals movement only while working with the animal"))';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_integer_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_integer_c']['readonly_formula']='';

 ?>