<?php
 // created: 2021-12-07 09:49:02
$dictionary['TSD1_Test_System_Design_1']['fields']['details_of_specific_position_c']['labelValue']='Details of Specific Positioning';
$dictionary['TSD1_Test_System_Design_1']['fields']['details_of_specific_position_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['details_of_specific_position_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['details_of_specific_position_c']['dependency']='equal($purpose_c,"Complete awake imaging where animal is required to be in a specific position for imaging")';
$dictionary['TSD1_Test_System_Design_1']['fields']['details_of_specific_position_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['details_of_specific_position_c']['readonly_formula']='';

 ?>