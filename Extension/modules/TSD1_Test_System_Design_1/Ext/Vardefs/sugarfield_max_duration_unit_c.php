<?php
 // created: 2021-12-07 09:46:30
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_unit_c']['labelValue']='Max. Duration Unit';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_unit_c']['dependency']='or(isInList($type_of_acclimation_c,createList("Manual restraint","Leash","Sling","Imaging Cart")),equal($purpose_c,"Restrict animals movement only while working with the animal"))';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_unit_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_unit_c']['readonly_formula']='';

 ?>