<?php
 // created: 2021-12-14 09:10:50
$dictionary['TSD1_Test_System_Design_1']['fields']['type_of_acclimation_c']['labelValue']='Type of Acclimation';
$dictionary['TSD1_Test_System_Design_1']['fields']['type_of_acclimation_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['type_of_acclimation_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['type_of_acclimation_c']['readonly_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['type_of_acclimation_c']['visibility_grid']=array (
  'trigger' => 'degree_of_immobility_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Minimal' => 
    array (
      0 => '',
      1 => 'JacketHarness',
      2 => 'E collar',
      3 => 'C collar',
      4 => 'Manual restraint',
      5 => 'Leash',
      6 => 'Other',
    ),
    'Partial' => 
    array (
      0 => '',
      1 => 'Crossties',
      2 => 'Sling',
      3 => 'Imaging Cart',
      4 => 'Other',
    ),
    'Complete' => 
    array (
      0 => '',
      1 => 'Pyrogen testingRabbit restrainer',
      2 => 'Other',
    ),
    'Minimal Partial' => 
    array (
      0 => '',
      1 => 'Crossties Jacket',
    ),
  ),
);

 ?>