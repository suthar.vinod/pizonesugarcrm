<?php
 // created: 2021-12-30 09:21:08
$dictionary['TSD1_Test_System_Design_1']['fields']['other_breedstrain_c']['labelValue']='Other Breed/Strain';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_breedstrain_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['other_breedstrain_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_breedstrain_c']['dependency']='equal($breedstrain,"Other")';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_breedstrain_c']['required_formula']='equal($breedstrain,"Other")';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_breedstrain_c']['readonly_formula']='';

 ?>