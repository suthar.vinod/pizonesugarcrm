<?php
 // created: 2021-12-07 10:00:43
$dictionary['TSD1_Test_System_Design_1']['fields']['confirm_acclimation_restrain_c']['labelValue']='Confirm with vivarium management that the type of acclimation or restraint items are in house and available to be used';
$dictionary['TSD1_Test_System_Design_1']['fields']['confirm_acclimation_restrain_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['confirm_acclimation_restrain_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['confirm_acclimation_restrain_c']['readonly_formula']='';

 ?>