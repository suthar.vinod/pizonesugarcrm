<?php
 // created: 2021-12-07 09:59:36
$dictionary['TSD1_Test_System_Design_1']['fields']['other_housing_requirements_c']['labelValue']='Other Housing Requirements';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_housing_requirements_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['other_housing_requirements_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_housing_requirements_c']['dependency']='equal($requirements_c,"Other")';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_housing_requirements_c']['required_formula']='';

 ?>