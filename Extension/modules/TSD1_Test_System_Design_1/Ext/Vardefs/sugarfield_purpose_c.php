<?php
 // created: 2021-12-14 09:20:27
$dictionary['TSD1_Test_System_Design_1']['fields']['purpose_c']['labelValue']='Purpose';
$dictionary['TSD1_Test_System_Design_1']['fields']['purpose_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['purpose_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['purpose_c']['readonly_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['purpose_c']['visibility_grid']=array (
  'trigger' => 'type_of_acclimation_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'JacketHarness' => 
    array (
      0 => '',
      1 => 'Protect item or device from animal no additional weight added',
      2 => 'Hold device or item additional weight added',
    ),
    'E collar' => 
    array (
    ),
    'C collar' => 
    array (
    ),
    'Manual restraint' => 
    array (
    ),
    'Leash' => 
    array (
    ),
    'Other' => 
    array (
    ),
    'Crossties' => 
    array (
      0 => '',
      1 => 'Connected to external items',
      2 => 'Restrict animals movement only while working with the animal',
    ),
    'Sling' => 
    array (
      0 => '',
      1 => 'Perform procedure on animal while in sling',
      2 => 'Gain access to limbs or specific area',
    ),
    'Imaging Cart' => 
    array (
      0 => '',
      1 => 'Complete awake imaging where the animal is required to remain calm and still',
      2 => 'Complete awake imaging where animal is required to be in a specific position for imaging',
    ),
    'Pyrogen testingRabbit restrainer' => 
    array (
    ),
    'Crossties Jacket' => 
    array (
      0 => '',
      1 => 'Protect item or device from animal no additional weight added',
      2 => 'Hold device or item additional weight added',
      3 => 'Connected to external items',
      4 => 'Restrict animals movement only while working with the animal',
    ),
  ),
);

 ?>