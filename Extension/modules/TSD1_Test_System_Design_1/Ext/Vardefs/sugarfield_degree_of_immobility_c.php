<?php
 // created: 2021-12-07 09:23:36
$dictionary['TSD1_Test_System_Design_1']['fields']['degree_of_immobility_c']['labelValue']='Degree of Immobility';
$dictionary['TSD1_Test_System_Design_1']['fields']['degree_of_immobility_c']['dependency']='equal($prolonged_restraint_acc_c,"Yes")';
$dictionary['TSD1_Test_System_Design_1']['fields']['degree_of_immobility_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['degree_of_immobility_c']['readonly_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['degree_of_immobility_c']['visibility_grid']='';

 ?>