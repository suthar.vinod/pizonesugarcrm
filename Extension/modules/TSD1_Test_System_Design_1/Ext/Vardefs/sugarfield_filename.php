<?php
 // created: 2020-10-16 16:09:35
$GLOBALS['dictionary']['TSD1_Test_System_Design_1']['fields']['filename'] = array ( 
	'name' => 'filename',
    'vname' => 'LBL_FILENAME',
    'type' => 'file',
    'dbType' => 'varchar',
    'len' => '255',
    'comments' => 'File name associated with the note (attachment)',
    'reportable' => true,
    'comment' => 'File name associated with the note (attachment)',
    'importable' => false,
    'dependency' => 'equal($species,"Human Cadaver")',
	'audited' => false,
	'studio' => true,
	'massupdate' => false,
	'unified_search' => false,
	'calculated' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '1',
	'merge_filter' => 'disabled',
);
 ?>