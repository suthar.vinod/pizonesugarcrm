<?php
 // created: 2020-10-16 16:09:35
$GLOBALS['dictionary']['TSD1_Test_System_Design_1']['fields']['file_url'] = array (
     'name' => 'file_url',
     'vname' => 'LBL_FILE_URL',
     'type' => 'varchar',
     'source' => 'non-db',
     'reportable' => false,
     'comment' => 'Path to file (can be URL)',
     'importable' => false,

);

 ?>