<?php
 // created: 2020-10-16 16:09:35
$GLOBALS['dictionary']['TSD1_Test_System_Design_1']['fields']['file_mime_type'] = array(

     'name' => 'file_mime_type',
     'vname' => 'LBL_FILE_MIME_TYPE',
     'type' => 'varchar',
     'len' => '100',
     'comment' => 'Attachment MIME type',
     'importable' => false,
);

 ?>