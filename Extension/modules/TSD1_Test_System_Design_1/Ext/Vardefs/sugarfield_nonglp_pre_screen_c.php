<?php
 // created: 2021-12-28 09:25:11
$dictionary['TSD1_Test_System_Design_1']['fields']['nonglp_pre_screen_c']['labelValue']='nonGLP Pre-Screening Required';
$dictionary['TSD1_Test_System_Design_1']['fields']['nonglp_pre_screen_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['nonglp_pre_screen_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['nonglp_pre_screen_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['nonglp_pre_screen_c']['readonly_formula']='';

 ?>