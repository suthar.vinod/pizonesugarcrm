<?php
 // created: 2021-12-07 09:47:46
$dictionary['TSD1_Test_System_Design_1']['fields']['other_type_of_acclimation_c']['labelValue']='Other Type of Acclimation';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_type_of_acclimation_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['other_type_of_acclimation_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_type_of_acclimation_c']['dependency']='equal($type_of_acclimation_c,"Other")';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_type_of_acclimation_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_type_of_acclimation_c']['readonly_formula']='';

 ?>