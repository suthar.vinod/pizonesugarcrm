<?php
 // created: 2022-01-06 09:44:27
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agent_details_c']['labelValue']='Hazardous Agent Details';
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agent_details_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agent_details_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agent_details_c']['dependency']='equal($hazardous_agents_c,"Yes")';
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agent_details_c']['required_formula']='equal($hazardous_agents_c,"Yes")';
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agent_details_c']['readonly_formula']='';

 ?>