<?php
$dependencies['TSD1_Test_System_Design_1']['set_options_vis11111'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('species'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'breedstrain',
                'keys' => 'ifElse(
                    equal($species,"Ovine"),
                    createList("","Cross Breed","Dorset","Dorset X","Hampshire","Hampshire X","Polypay","Polypay X","Suffolk","Suffolk X","Other"),
                    ifElse(
                    equal($species,"Canine"),
                    createList("","Beagle","Mongrel","Other"),
                    ifElse(
                    equal($species,"Porcine"),
                    createList("","Gottingen", "Hanford","Landrace Cross","LDLR","Micro Yucatan","Ossabaw","Sinclair","Yucatan","Yorkshire X","Other"),
                    ifElse(
                    equal($species,"Bovine"),
                    createList("","Holstein","Other"),
                    ifElse(
                    equal($species,"Caprine"),
                    createList("","Cross Breed","Other"),
                    ifElse(
                    equal($species,"Mouse"),
                    createList("","BALBc","C57","CD1","ND4","Other"),
                    ifElse(
                    equal($species,"Lagomorph"),
                    createList("","New Zealand White","Other"),
                    ifElse(
                    equal($species,"Rat"),
                    createList("","Sprague Dawley","Wistar","Lewis","Other"),
                    ifElse(
                    equal($species,"Hamster"),
                    createList("","Golden Syrian","Other"),
                    ifElse(
                    equal($species,"Guinea Pig"),
                    createList("","Hartley","Other"),
                    createList("")))))))))))',
            'labels' => 'ifElse(
                equal($species,"Ovine"),
                createList("","Cross Breed - NonSpecific","Dorset","Dorset X","Hampshire","Hampshire X","Polypay","Polypay X","Suffolk","Suffolk X","Other"),
                ifElse(
                equal($species,"Canine"),
                createList("","Beagle","Mongrel","Other"),
                ifElse(
                equal($species,"Porcine"),
                createList("","Gottingen","Hanford","Landrace Cross","LDLR Yucatan","Micro-Yucatan","Ossabaw","Sinclair","Yucatan","Yorkshire X","Other"),
                ifElse(
                equal($species,"Bovine"),
                createList("","Holstein","Other"),
                ifElse(
                equal($species,"Caprine"),
                createList("","Cross Breed - NonSpecific","Other"),
                ifElse(
                equal($species,"Mouse"),
                createList("","BALB/c","C57","CD1","ND4","Other"),
                ifElse(
                equal($species,"Lagomorph"),
                createList("","New Zealand White","Other"),
                ifElse(
                equal($species,"Rat"),
                createList("","Sprague Dawley","Wistar","Lewis","Other"),
                ifElse(
                equal($species,"Hamster"),
                createList("","Golden Syrian","Other"),
                ifElse(
                equal($species,"Guinea Pig"),
                createList("","Hartley","Other"),
                createList("")))))))))))'
            ),
        ),
    ),
);