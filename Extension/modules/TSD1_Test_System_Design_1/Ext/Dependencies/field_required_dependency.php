<?php

$dependencies['TSD1_Test_System_Design_1']['test_system_require_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('date_entered','id'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'tasks_start_prior_to_day_7_c',
                'value' => 'or(not(greaterThan(strlen(toString($date_entered)),0)),isAfter($date_entered,"2021-08-19"))',
            ),
        ),
    ),
);
