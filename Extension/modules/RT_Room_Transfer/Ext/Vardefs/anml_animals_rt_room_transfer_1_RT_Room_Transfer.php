<?php
// created: 2019-02-20 13:19:54
$dictionary["RT_Room_Transfer"]["fields"]["anml_animals_rt_room_transfer_1"] = array (
  'name' => 'anml_animals_rt_room_transfer_1',
  'type' => 'link',
  'relationship' => 'anml_animals_rt_room_transfer_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_RT_ROOM_TRANSFER_TITLE',
  'id_name' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["RT_Room_Transfer"]["fields"]["anml_animals_rt_room_transfer_1_name"] = array (
  'name' => 'anml_animals_rt_room_transfer_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
  'link' => 'anml_animals_rt_room_transfer_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
  'required' => 'true',  
);
$dictionary["RT_Room_Transfer"]["fields"]["anml_animals_rt_room_transfer_1anml_animals_ida"] = array (
  'name' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_RT_ROOM_TRANSFER_TITLE_ID',
  'id_name' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
  'link' => 'anml_animals_rt_room_transfer_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
