<?php
 // created: 2019-06-19 15:12:58
$dictionary['RT_Room_Transfer']['fields']['name']['len']='255';
$dictionary['RT_Room_Transfer']['fields']['name']['audited']=true;
$dictionary['RT_Room_Transfer']['fields']['name']['massupdate']=false;
$dictionary['RT_Room_Transfer']['fields']['name']['unified_search']=false;
$dictionary['RT_Room_Transfer']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['RT_Room_Transfer']['fields']['name']['calculated']=false;
$dictionary['RT_Room_Transfer']['fields']['name']['required']=false;
$dictionary['RT_Room_Transfer']['fields']['name']['readonly']=true;

 ?>