<?php
$hook_version = 1;

if (!isset($hook_array)) {
	$hook_array = array();
}
if (!isset($hook_array['after_save'])) {
	$hook_array['after_save'] = array();
}
$hook_array['after_save'][] = array(
   count($hook_array['after_save']),
   'Link Room to parent Animal from Related Room Transfer record with latest Transfer Date',
   'custom/modules/RT_Room_Transfer/LogicHooksImplementations/animalRoomUpdate.php',
   'animalRoomUpdateClass',
   'updateRoom'
);
?>