<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TRANSFER_DATE'] = 'Transfer Date';
$mod_strings['LBL_REASON_FOR_TRANSFER'] = 'Reason for Transfer';
$mod_strings['LBL_ROOM'] = 'Room';
$mod_strings['LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_ANML_ANIMALS_TITLE'] = 'Test Systems';
$mod_strings['LBL_RECORD_BODY'] = 'Room Transfer';
$mod_strings['LBL_RT_ROOM_TRANSFER_FOCUS_DRAWER_DASHBOARD'] = 'Room Transfers Focus Drawer';
$mod_strings['LBL_RT_ROOM_TRANSFER_RECORD_DASHBOARD'] = 'Room Transfers Record Dashboard';
