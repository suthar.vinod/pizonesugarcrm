<?php
 // created: 2022-02-08 07:44:27
$dictionary['OT_Operator_Tracking']['fields']['type_2']['visibility_grid']=array (
  'trigger' => 'category',
  'values' => 
  array (
    1 => 
    array (
      0 => '',
      1 => '1',
    ),
    '' => 
    array (
    ),
    'CatheterWire' => 
    array (
      0 => '',
      1 => 'Cardiac Evaluation',
      2 => 'Other',
      3 => 'Peripheral Evaluation',
    ),
    'Circulatory Assist' => 
    array (
      0 => '',
      1 => 'Aortic',
      2 => 'ECMO',
      3 => 'Other',
      4 => 'Surgical LVAD',
      5 => 'Transcatheter LVAD',
    ),
    'Electrophysiology' => 
    array (
      0 => '',
      1 => 'Ablation',
      2 => 'AblationMapping',
      3 => 'Mapping',
      4 => 'Other',
    ),
    'Embolization' => 
    array (
      0 => '',
      1 => 'Cardiac',
      2 => 'Cebrebral',
      3 => 'Other',
      4 => 'Peripheral',
      5 => 'Protection',
    ),
    'Endoscopy' => 
    array (
      0 => '',
      1 => 'Bronchoscopy',
      2 => 'Gastroscopy',
      3 => 'Laprascopy',
      4 => 'Other',
      5 => 'Thorascopy',
    ),
    'General Cardiac' => 
    array (
      0 => '',
      1 => 'Left Atrial Closure',
      2 => 'Other',
      3 => 'Septal Treatment',
      4 => 'Thrombectomy',
    ),
    'Hemostasis' => 
    array (
      0 => '',
      1 => 'Hemostatic Agents',
      2 => 'Other',
      3 => 'Vascular Closure',
      4 => 'Vessel Sealing Device',
    ),
    'Integumentary' => 
    array (
      0 => '',
      1 => 'Bandaging',
      2 => 'Other',
      3 => 'Surface modulation',
      4 => 'Wound Creation',
    ),
    'Leads' => 
    array (
      0 => '',
      1 => 'Epicardial',
      2 => 'Epidural',
      3 => 'Intercardiac',
      4 => 'Other',
      5 => 'Peripheral',
    ),
    'Muscoskeletal' => 
    array (
      0 => '',
      1 => 'Cartilage Defect',
      2 => 'Joint injection',
      3 => 'Orthopedics',
      4 => 'Other',
      5 => 'TendonLigament Repair',
    ),
    'Non Cardiac Ablation' => 
    array (
      0 => '',
      1 => 'Bladder',
      2 => 'Gastrointestinal',
      3 => 'Other',
      4 => 'Peripheral',
      5 => 'Prostate',
      6 => 'Pulmonary',
      7 => 'Renal',
      8 => 'Thigh',
    ),
    'Other' => 
    array (
      0 => '',
      1 => 'Other',
    ),
    'StentsBalloons' => 
    array (
      0 => '',
      1 => 'Coronary',
      2 => 'Gastrointestinal',
      3 => 'Other',
      4 => 'Peripheral',
      5 => 'Pulmonary',
      6 => 'Stent Graft',
      7 => 'Urinary',
    ),
    'Transplant' => 
    array (
      0 => '',
      1 => 'Kidney',
      2 => 'Liver',
      3 => 'Other',
    ),
    'Valve' => 
    array (
      0 => '',
      1 => 'Other',
      2 => 'Surgical Aortic',
      3 => 'Surgical Mitral',
      4 => 'Surgical Pulmonary',
      5 => 'Surgical Tricuspid',
      6 => 'Transcatheter Aortic',
      7 => 'Transcatheter Mitral',
      8 => 'Transcatheter Pulmonary',
      9 => 'Transcatheter Tricuspid',
    ),
    'Vascular Anastomosis' => 
    array (
      0 => '',
      1 => 'Aneurysm Elastase',
      2 => 'Aneurysm Surgical',
      3 => 'AVF',
      4 => 'AVG',
      5 => 'CABG',
      6 => 'Interpositional',
      7 => 'Other',
    ),
    'Cardiopulmonary Bypass' => 
    array (
      0 => '',
      1 => 'Other',
      2 => 'Standard',
    ),
  ),
);

 ?>