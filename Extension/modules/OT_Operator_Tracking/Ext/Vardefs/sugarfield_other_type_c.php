<?php
 // created: 2022-02-01 07:53:07
$dictionary['OT_Operator_Tracking']['fields']['other_type_c']['labelValue']='Other Type';
$dictionary['OT_Operator_Tracking']['fields']['other_type_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['OT_Operator_Tracking']['fields']['other_type_c']['enforced']='';
$dictionary['OT_Operator_Tracking']['fields']['other_type_c']['dependency']='equal($type_2,"Other")';
$dictionary['OT_Operator_Tracking']['fields']['other_type_c']['required_formula']='equal($type_2,"Other")';
$dictionary['OT_Operator_Tracking']['fields']['other_type_c']['readonly_formula']='';

 ?>