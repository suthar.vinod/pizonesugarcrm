<?php
 // created: 2020-02-21 13:13:48
$dictionary['ET_Email_Template']['fields']['document_name']['audited']=true;
$dictionary['ET_Email_Template']['fields']['document_name']['massupdate']=false;
$dictionary['ET_Email_Template']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['ET_Email_Template']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['ET_Email_Template']['fields']['document_name']['merge_filter']='disabled';
$dictionary['ET_Email_Template']['fields']['document_name']['unified_search']=false;
$dictionary['ET_Email_Template']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['ET_Email_Template']['fields']['document_name']['calculated']=false;

 ?>