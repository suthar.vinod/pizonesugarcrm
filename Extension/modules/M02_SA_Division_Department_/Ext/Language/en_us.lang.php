<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DIVISION'] = 'Division';
$mod_strings['LBL_DEPARTMENT'] = 'Department';
$mod_strings['LBL_NAME'] = 'Name';
$mod_strings['LNK_NEW_RECORD'] = 'Create SA Division Department';
$mod_strings['LNK_LIST'] = 'View SA Divisions Departments';
$mod_strings['LBL_MODULE_NAME'] = 'SA Divisions Departments';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'SA Division Department';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New SA Division Department';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import SA Division DepartmentvCard';
$mod_strings['LNK_IMPORT_M02_SA_DIVISION_DEPARTMENT_'] = 'Import SA Division Department ';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'SA Divisions Departments  List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search SA Division Department';
$mod_strings['LBL_M01_SALES_M02_SA_DIVISION_DEPARTMENT__1_FROM_M01_SALES_TITLE'] = 'Sales Activities';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My SA Divisions Departments';
$mod_strings['LBL_M02_SA_DIVISION_DEPARTMENT__SUBPANEL_TITLE'] = 'SA Divisions Departments';
$mod_strings['LBL_M02_SA_DIVISION_DEPARTMENT__FOCUS_DRAWER_DASHBOARD'] = 'SA Divisions Departments Focus Drawer';
