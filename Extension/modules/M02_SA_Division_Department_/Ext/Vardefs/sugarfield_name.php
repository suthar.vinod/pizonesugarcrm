<?php
 // created: 2016-10-25 03:27:05
$dictionary['M02_SA_Division_Department_']['fields']['name']['len'] = '255';
$dictionary['M02_SA_Division_Department_']['fields']['name']['audited'] = true;
$dictionary['M02_SA_Division_Department_']['fields']['name']['massupdate'] = false;
$dictionary['M02_SA_Division_Department_']['fields']['name']['importable'] = 'false';
$dictionary['M02_SA_Division_Department_']['fields']['name']['duplicate_merge'] = 'disabled';
$dictionary['M02_SA_Division_Department_']['fields']['name']['duplicate_merge_dom_value'] = 0;
$dictionary['M02_SA_Division_Department_']['fields']['name']['merge_filter'] = 'disabled';
$dictionary['M02_SA_Division_Department_']['fields']['name']['unified_search'] = false;
$dictionary['M02_SA_Division_Department_']['fields']['name']['calculated'] = '1';
$dictionary['M02_SA_Division_Department_']['fields']['name']['formula'] = '$division_c';
$dictionary['M02_SA_Division_Department_']['fields']['name']['enforced'] = true;
$dictionary['M02_SA_Division_Department_']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['M02_SA_Division_Department_']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['M02_SA_Division_Department_']['fields']['name']['full_text_search']['boost'] = 1.5500000000000000444089209850062616169452667236328125;

