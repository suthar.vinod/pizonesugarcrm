<?php
 // created: 2016-10-25 03:27:05
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['labelValue'] = 'Department';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['trigger'] = 'division_c';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['InLife_Services'][0] = 'Biocompatibility';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['InLife_Services'][1] = 'Interventional Surgical';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['InLife_Services'][2] = 'Pharmacology';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['InLife_Services'][3] = 'Toxicology';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['InVitro_Services'][0] = 'Biocompatibility';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['InVitro_Services'][1] = 'Microbiology';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['Pathology_Services'][0] = 'Gross Pathology';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['Pathology_Services'][1] = 'Histology';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['Analytical_Services'][0] = 'Analytical';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['full_text_search']['boost'] = 1;

