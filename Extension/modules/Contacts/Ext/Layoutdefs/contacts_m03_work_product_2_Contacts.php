<?php
 // created: 2022-07-07 07:02:30
$layout_defs["Contacts"]["subpanel_setup"]['contacts_m03_work_product_2'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'contacts_m03_work_product_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
