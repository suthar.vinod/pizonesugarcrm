<?php

$hook_version = 1;
if (!isset($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['before_save'])) {
    $hook_array['before_save'] = array();
}

$hook_array['before_save'][] = array(
    count($hook_array['before_save']),
    'When creating a new contact, send email.',
    'custom/modules/Contacts/ContactHooksImp.php',
    'ContactHooksImp',
    'beforeSave',
);
