<?php
 // created: 2019-02-20 19:46:48
$layout_defs["Contacts"]["subpanel_setup"]['contacts_de_deviation_employees_1'] = array (
  'order' => 100,
  'module' => 'DE_Deviation_Employees',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'get_subpanel_data' => 'contacts_de_deviation_employees_1',
);
