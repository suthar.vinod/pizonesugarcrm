<?php
// created: 2019-08-15 11:34:01
$dictionary["Contact"]["fields"]["contacts_maj_contact_documents_1"] = array (
  'name' => 'contacts_maj_contact_documents_1',
  'type' => 'link',
  'relationship' => 'contacts_maj_contact_documents_1',
  'source' => 'non-db',
  'module' => 'MAJ_Contact_Documents',
  'bean_name' => 'MAJ_Contact_Documents',
  'vname' => 'LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_maj_contact_documents_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
