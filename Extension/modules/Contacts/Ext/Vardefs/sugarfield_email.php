<?php
 // created: 2019-02-20 19:26:28
$dictionary['Contact']['fields']['email']['len']='100';
$dictionary['Contact']['fields']['email']['massupdate']=true;
$dictionary['Contact']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['email']['duplicate_merge_dom_value']='2';
$dictionary['Contact']['fields']['email']['merge_filter']='enabled';
$dictionary['Contact']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.0',
  'searchable' => true,
);
$dictionary['Contact']['fields']['email']['calculated']=false;

 ?>