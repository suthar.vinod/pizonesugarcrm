<?php
// created: 2018-12-21 23:14:19
$dictionary["Contact"]["fields"]["contacts_m03_work_product_1"] = array (
  'name' => 'contacts_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'contacts_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_m03_work_product_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
