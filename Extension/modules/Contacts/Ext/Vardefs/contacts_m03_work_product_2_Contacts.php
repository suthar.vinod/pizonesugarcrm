<?php
// created: 2022-07-07 07:02:30
$dictionary["Contact"]["fields"]["contacts_m03_work_product_2"] = array (
  'name' => 'contacts_m03_work_product_2',
  'type' => 'link',
  'relationship' => 'contacts_m03_work_product_2',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_m03_work_product_2contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
