<?php

$dictionary['Contact']['fields']['is_send_email'] = array(
    'required' => false,
    'studio' => false,
    'name' => 'is_send_email',
    'vname' => 'Send Email',
    'type' => 'bool',
    'massupdate' => '0',
    'default' => '0',
);
