<?php
// created: 2019-02-20 19:46:48
$dictionary["Contact"]["fields"]["contacts_de_deviation_employees_1"] = array (
  'name' => 'contacts_de_deviation_employees_1',
  'type' => 'link',
  'relationship' => 'contacts_de_deviation_employees_1',
  'source' => 'non-db',
  'module' => 'DE_Deviation_Employees',
  'bean_name' => 'DE_Deviation_Employees',
  'vname' => 'LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_de_deviation_employees_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
