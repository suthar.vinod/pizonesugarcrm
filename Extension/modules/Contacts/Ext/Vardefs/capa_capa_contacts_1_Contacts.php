<?php
// created: 2022-02-03 07:34:36
$dictionary["Contact"]["fields"]["capa_capa_contacts_1"] = array (
  'name' => 'capa_capa_contacts_1',
  'type' => 'link',
  'relationship' => 'capa_capa_contacts_1',
  'source' => 'non-db',
  'module' => 'CAPA_CAPA',
  'bean_name' => 'CAPA_CAPA',
  'vname' => 'LBL_CAPA_CAPA_CONTACTS_1_FROM_CAPA_CAPA_TITLE',
  'id_name' => 'capa_capa_contacts_1capa_capa_ida',
);
