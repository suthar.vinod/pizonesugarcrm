<?php
 
$viewdefs['Contacts']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterPurchaseOrder',
    'name' => 'Teams',
    'filter_definition' => array(
        array(
            'team_id' => array(
            '$in' => array('f540fc6a-ca9c-11ea-a8bb-02fb813964b8'),
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);

