<?php
// created: 2018-10-09 16:44:23
$dictionary["SW_Study_Workflow"]["fields"]["m03_work_product_sw_study_workflow_1"] = array (
  'name' => 'm03_work_product_sw_study_workflow_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_sw_study_workflow_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_SW_STUDY_WORKFLOW_TITLE',
  'id_name' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["SW_Study_Workflow"]["fields"]["m03_work_product_sw_study_workflow_1_name"] = array (
  'name' => 'm03_work_product_sw_study_workflow_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
  'link' => 'm03_work_product_sw_study_workflow_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["SW_Study_Workflow"]["fields"]["m03_work_product_sw_study_workflow_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_SW_STUDY_WORKFLOW_TITLE_ID',
  'id_name' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
  'link' => 'm03_work_product_sw_study_workflow_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
