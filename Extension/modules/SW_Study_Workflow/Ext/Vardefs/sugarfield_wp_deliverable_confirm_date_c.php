<?php
 // created: 2018-10-09 17:37:26
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirm_date_c']['duplicate_merge_dom_value']=0;
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirm_date_c']['labelValue']='WP Deliverable Confirmation Due Date';
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirm_date_c']['calculated']='1';
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirm_date_c']['formula']='addDays(related($m03_work_product_sw_study_workflow_1,"first_procedure_c"),-3)';
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirm_date_c']['enforced']='1';
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirm_date_c']['dependency']='';

 ?>