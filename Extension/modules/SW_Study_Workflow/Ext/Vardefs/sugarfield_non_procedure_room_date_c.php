<?php
 // created: 2018-10-09 17:13:00
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_date_c']['duplicate_merge_dom_value']=0;
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_date_c']['labelValue']='Non-Procedure Room Task Confirmation Due Date';
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_date_c']['calculated']='true';
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_date_c']['formula']='related($m03_work_product_sw_study_workflow_1,"first_procedure_c")';
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_date_c']['enforced']='true';
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_date_c']['dependency']='';

 ?>