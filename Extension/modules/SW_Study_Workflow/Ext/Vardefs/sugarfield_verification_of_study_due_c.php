<?php
 // created: 2018-10-09 16:58:26
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_due_c']['duplicate_merge_dom_value']=0;
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_due_c']['labelValue']='Verification of Study Forms Due Date';
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_due_c']['calculated']='true';
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_due_c']['formula']='addDays(related($m03_work_product_sw_study_workflow_1,"first_procedure_c"),-2)';
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_due_c']['enforced']='true';
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_due_c']['dependency']='';

 ?>