<?php
 // created: 2018-10-09 17:04:34
$dictionary['SW_Study_Workflow']['fields']['test_article_checkin_c']['duplicate_merge_dom_value']=0;
$dictionary['SW_Study_Workflow']['fields']['test_article_checkin_c']['labelValue']='Test & Control Article Check-In Due Date';
$dictionary['SW_Study_Workflow']['fields']['test_article_checkin_c']['calculated']='1';
$dictionary['SW_Study_Workflow']['fields']['test_article_checkin_c']['formula']='addDays(related($m03_work_product_sw_study_workflow_1,"first_procedure_c"),-2)';
$dictionary['SW_Study_Workflow']['fields']['test_article_checkin_c']['enforced']='1';
$dictionary['SW_Study_Workflow']['fields']['test_article_checkin_c']['dependency']='';

 ?>