<?php
 // created: 2018-10-09 17:25:22
$dictionary['SW_Study_Workflow']['fields']['archiving_scanning_prot_data_c']['labelValue']='Archiving or Scanning of Protocol/Study Data Binders';
$dictionary['SW_Study_Workflow']['fields']['archiving_scanning_prot_data_c']['enforced']='';
$dictionary['SW_Study_Workflow']['fields']['archiving_scanning_prot_data_c']['dependency']='equal(related($m03_work_product_sw_study_workflow_1,"work_product_status_c"),"Archived")';

 ?>