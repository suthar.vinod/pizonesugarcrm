<?php
 // created: 2019-04-23 12:12:52
$dictionary['SW_Study_Workflow']['fields']['study_coordinator_notes_c']['labelValue']='Study Coordinator Notes';
$dictionary['SW_Study_Workflow']['fields']['study_coordinator_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['SW_Study_Workflow']['fields']['study_coordinator_notes_c']['enforced']='';
$dictionary['SW_Study_Workflow']['fields']['study_coordinator_notes_c']['dependency']='';

 ?>