<?php
 // created: 2019-04-23 12:09:37
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_create_du_c']['duplicate_merge_dom_value']=0;
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_create_du_c']['labelValue']='Protocol & Data Book Creation Due Date';
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_create_du_c']['calculated']='1';
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_create_du_c']['formula']='addDays(related($m03_work_product_sw_study_workflow_1,"first_procedure_c"),-2)';
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_create_du_c']['enforced']='1';
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_create_du_c']['dependency']='';

 ?>