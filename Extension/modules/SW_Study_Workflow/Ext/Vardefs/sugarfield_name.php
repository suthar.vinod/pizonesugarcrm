<?php
 // created: 2019-04-23 12:08:12
$dictionary['SW_Study_Workflow']['fields']['name']['len']='255';
$dictionary['SW_Study_Workflow']['fields']['name']['audited']=true;
$dictionary['SW_Study_Workflow']['fields']['name']['massupdate']=false;
$dictionary['SW_Study_Workflow']['fields']['name']['unified_search']=false;
$dictionary['SW_Study_Workflow']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['SW_Study_Workflow']['fields']['name']['calculated']=false;

 ?>