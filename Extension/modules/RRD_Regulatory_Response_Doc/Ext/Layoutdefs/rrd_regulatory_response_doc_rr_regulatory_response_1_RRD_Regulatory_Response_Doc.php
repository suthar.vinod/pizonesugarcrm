<?php
 // created: 2019-07-09 12:11:34
$layout_defs["RRD_Regulatory_Response_Doc"]["subpanel_setup"]['rrd_regulatory_response_doc_rr_regulatory_response_1'] = array (
  'order' => 100,
  'module' => 'RR_Regulatory_Response',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_RRD_REGULATORY_RESPONSE_DOC_RR_REGULATORY_RESPONSE_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'get_subpanel_data' => 'rrd_regulatory_response_doc_rr_regulatory_response_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
