<?php
 // created: 2019-07-09 12:09:13
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['audited']=true;
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['massupdate']=false;
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['merge_filter']='disabled';
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['unified_search']=false;
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['calculated']=false;

 ?>