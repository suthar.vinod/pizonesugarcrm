<?php
 $dictionary['Ere_Error_Employees']['fields']['department'] = array(
    'name' => 'department',
    'label' => 'LBL_DEPARTMENT',
    'type' => 'enum',
    'help' => '',
    'comment' => '',
    'options' => 'departments_list', //maps to options - specify list name
    'default_value' => '', //key of entry in specified list
    'mass_update' => false, // true or false
    'required' => false, // true or false
    'reportable' => true, // true or false
    'audited' => false, // true or false
    'importable' => 'true', // 'true', 'false' or 'required'
    'duplicate_merge' => false, // true or false
);
 ?>
