<?php

$dictionary['Ere_Error_Employees']['fields']['manager'] = array(
    'name'=>'manager',
    'vname'=>'LBL_MANAGER',
    'type' => 'enum',
    'massupdate' => false,
    'importable' => 'true',
    'duplicate_merge' => 'false',
    'required' => false,
    'reportable' => true,
    'no_default' => false,
    'options' => 'is_manager_list',
    'default' => 'Choose One',
);
