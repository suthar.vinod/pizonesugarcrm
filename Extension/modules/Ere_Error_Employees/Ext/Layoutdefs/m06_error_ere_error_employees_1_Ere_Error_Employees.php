<?php
 // created: 2018-01-31 21:12:49
$layout_defs["Ere_Error_Employees"]["subpanel_setup"]['m06_error_ere_error_employees_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'm06_error_ere_error_employees_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
