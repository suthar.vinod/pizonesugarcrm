<?php
 // created: 2021-07-08 09:25:26
$dictionary['CDU_CD_Utilization']['fields']['number_of_tests_2']['required']=true;
$dictionary['CDU_CD_Utilization']['fields']['number_of_tests_2']['importable']='true';
$dictionary['CDU_CD_Utilization']['fields']['number_of_tests_2']['duplicate_merge']='enabled';
$dictionary['CDU_CD_Utilization']['fields']['number_of_tests_2']['duplicate_merge_dom_value']='1';
$dictionary['CDU_CD_Utilization']['fields']['number_of_tests_2']['precision']=1;
$dictionary['CDU_CD_Utilization']['fields']['number_of_tests_2']['required_formula']='equal(related($erd_error_documents_cdu_cd_utilization_1,"deviation_rate_basis_c"),"Studies")';

 ?>