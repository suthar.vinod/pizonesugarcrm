<?php
 // created: 2021-07-08 09:24:50
$dictionary['CDU_CD_Utilization']['fields']['number_of_procedures_2']['required']=true;
$dictionary['CDU_CD_Utilization']['fields']['number_of_procedures_2']['precision']=1;
$dictionary['CDU_CD_Utilization']['fields']['number_of_procedures_2']['required_formula']='equal(related($erd_error_documents_cdu_cd_utilization_1,"deviation_rate_basis_c"),"Procedures")';

 ?>