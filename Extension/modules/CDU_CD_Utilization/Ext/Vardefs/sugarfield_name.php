<?php
 // created: 2021-04-13 12:29:09
$dictionary['CDU_CD_Utilization']['fields']['name']['importable']='false';
$dictionary['CDU_CD_Utilization']['fields']['name']['duplicate_merge']='disabled';
$dictionary['CDU_CD_Utilization']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['CDU_CD_Utilization']['fields']['name']['merge_filter']='disabled';
$dictionary['CDU_CD_Utilization']['fields']['name']['unified_search']=false;
$dictionary['CDU_CD_Utilization']['fields']['name']['calculated']='true';
$dictionary['CDU_CD_Utilization']['fields']['name']['formula']='concat(related($erd_error_documents_cdu_cd_utilization_1,"name")," CUD ",toString($month_end))';
$dictionary['CDU_CD_Utilization']['fields']['name']['enforced']=true;

 ?>