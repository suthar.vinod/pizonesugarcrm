<?php
// created: 2016-01-25 22:19:12
$dictionary["M01_Quote_Document"]["fields"]["m01_sales_m01_quote_document_1"] = array (
  'name' => 'm01_sales_m01_quote_document_1',
  'type' => 'link',
  'relationship' => 'm01_sales_m01_quote_document_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE',
  'id_name' => 'm01_sales_m01_quote_document_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["M01_Quote_Document"]["fields"]["m01_sales_m01_quote_document_1_name"] = array (
  'name' => 'm01_sales_m01_quote_document_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_m01_quote_document_1m01_sales_ida',
  'link' => 'm01_sales_m01_quote_document_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["M01_Quote_Document"]["fields"]["m01_sales_m01_quote_document_1m01_sales_ida"] = array (
  'name' => 'm01_sales_m01_quote_document_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE_ID',
  'id_name' => 'm01_sales_m01_quote_document_1m01_sales_ida',
  'link' => 'm01_sales_m01_quote_document_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
