<?php
 // created: 2021-05-17 04:30:01
$dictionary['M01_Quote_Document']['fields']['quoteco_date_c']['labelValue']='Quote/CO Date';
$dictionary['M01_Quote_Document']['fields']['quoteco_date_c']['enforced']='';
$dictionary['M01_Quote_Document']['fields']['quoteco_date_c']['dependency']='isInList($category_id,createList("Study Quote","Quote Revision","Change Order","Pricelist Submission"))';
$dictionary['M01_Quote_Document']['fields']['quoteco_date_c']['required_formula']='isInList($category_id,createList("Study Quote","Quote Revision","Change Order","Pricelist Submission"))';
$dictionary['M01_Quote_Document']['fields']['quoteco_date_c']['readonly_formula']='';

 ?>