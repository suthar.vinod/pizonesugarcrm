<?php
 // created: 2022-02-22 12:42:21
$dictionary['M01_Quote_Document']['fields']['histopathology_quoted_amount_c']['labelValue']='Histopathology Quoted Amount';
$dictionary['M01_Quote_Document']['fields']['histopathology_quoted_amount_c']['enforced']='';
$dictionary['M01_Quote_Document']['fields']['histopathology_quoted_amount_c']['dependency']='and(
equal(related($m01_sales_m01_quote_document_1,"aps_histopathology_deliv_c"),"Yes")
,isInList($category_id,createList("Change Order","Study Quote")))';
$dictionary['M01_Quote_Document']['fields']['histopathology_quoted_amount_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['M01_Quote_Document']['fields']['histopathology_quoted_amount_c']['required_formula']='';
$dictionary['M01_Quote_Document']['fields']['histopathology_quoted_amount_c']['readonly_formula']='';

 ?>