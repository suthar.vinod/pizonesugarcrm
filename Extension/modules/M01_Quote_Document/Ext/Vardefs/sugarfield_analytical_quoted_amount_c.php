<?php
 // created: 2022-03-03 14:15:26
$dictionary['M01_Quote_Document']['fields']['analytical_quoted_amount_c']['labelValue']='Analytical Quoted Amount';
$dictionary['M01_Quote_Document']['fields']['analytical_quoted_amount_c']['enforced']='';
$dictionary['M01_Quote_Document']['fields']['analytical_quoted_amount_c']['dependency']='and(
equal(related($m01_sales_m01_quote_document_1,"aps_analytical_deliverables_c"),"Yes")
,isInList($category_id,createList("Change Order","Study Quote")))';
$dictionary['M01_Quote_Document']['fields']['analytical_quoted_amount_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['M01_Quote_Document']['fields']['analytical_quoted_amount_c']['required_formula']='';
$dictionary['M01_Quote_Document']['fields']['analytical_quoted_amount_c']['readonly_formula']='';

 ?>