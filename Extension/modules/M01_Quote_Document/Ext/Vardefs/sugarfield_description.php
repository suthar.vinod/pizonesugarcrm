<?php
 // created: 2019-04-05 11:27:44
$dictionary['M01_Quote_Document']['fields']['description']['audited']=true;
$dictionary['M01_Quote_Document']['fields']['description']['massupdate']=false;
$dictionary['M01_Quote_Document']['fields']['description']['comments']='Full text of the note';
$dictionary['M01_Quote_Document']['fields']['description']['duplicate_merge']='enabled';
$dictionary['M01_Quote_Document']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['M01_Quote_Document']['fields']['description']['merge_filter']='disabled';
$dictionary['M01_Quote_Document']['fields']['description']['unified_search']=false;
$dictionary['M01_Quote_Document']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['M01_Quote_Document']['fields']['description']['calculated']=false;
$dictionary['M01_Quote_Document']['fields']['description']['rows']='6';
$dictionary['M01_Quote_Document']['fields']['description']['cols']='80';

 ?>