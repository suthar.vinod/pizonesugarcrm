<?php
 // created: 2021-01-04 09:43:55
$dictionary['M01_Quote_Document']['fields']['quoteco_status_c']['labelValue']='Quote/CO Status';
$dictionary['M01_Quote_Document']['fields']['quoteco_status_c']['dependency']='isInList($category_id,createList("Study Quote","Quote Revision","Change Order","Pricelist Submission"))';
$dictionary['M01_Quote_Document']['fields']['quoteco_status_c']['required_formula']='';
$dictionary['M01_Quote_Document']['fields']['quoteco_status_c']['visibility_grid']='';

 ?>