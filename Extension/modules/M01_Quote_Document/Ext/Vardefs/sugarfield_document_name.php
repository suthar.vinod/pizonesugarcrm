<?php
 // created: 2019-04-17 11:59:13
$dictionary['M01_Quote_Document']['fields']['document_name']['required']=true;
$dictionary['M01_Quote_Document']['fields']['document_name']['audited']=true;
$dictionary['M01_Quote_Document']['fields']['document_name']['massupdate']=false;
$dictionary['M01_Quote_Document']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['M01_Quote_Document']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['M01_Quote_Document']['fields']['document_name']['merge_filter']='disabled';
$dictionary['M01_Quote_Document']['fields']['document_name']['unified_search']=false;
$dictionary['M01_Quote_Document']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['M01_Quote_Document']['fields']['document_name']['calculated']=false;

 ?>