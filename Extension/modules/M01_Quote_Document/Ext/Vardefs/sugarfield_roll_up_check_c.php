<?php
 // created: 2021-01-04 09:48:15
$dictionary['M01_Quote_Document']['fields']['roll_up_check_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Quote_Document']['fields']['roll_up_check_c']['labelValue']='Roll up check';
$dictionary['M01_Quote_Document']['fields']['roll_up_check_c']['calculated']='true';
$dictionary['M01_Quote_Document']['fields']['roll_up_check_c']['formula']='and(equal($status_id,"Active"),isInList($category_id,createList("Pricelist Submission","Study Quote","Change Order")))';
$dictionary['M01_Quote_Document']['fields']['roll_up_check_c']['enforced']='true';
$dictionary['M01_Quote_Document']['fields']['roll_up_check_c']['dependency']='';

 ?>