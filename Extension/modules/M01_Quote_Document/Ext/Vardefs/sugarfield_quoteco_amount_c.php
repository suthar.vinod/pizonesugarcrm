<?php
 // created: 2021-01-04 09:42:41
$dictionary['M01_Quote_Document']['fields']['quoteco_amount_c']['labelValue']='Quote/CO Amount';
$dictionary['M01_Quote_Document']['fields']['quoteco_amount_c']['enforced']='';
$dictionary['M01_Quote_Document']['fields']['quoteco_amount_c']['dependency']='isInList($category_id,createList("Study Quote","Quote Revision","Change Order","Pricelist Submission"))';
$dictionary['M01_Quote_Document']['fields']['quoteco_amount_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['M01_Quote_Document']['fields']['quoteco_amount_c']['required_formula']='';

 ?>