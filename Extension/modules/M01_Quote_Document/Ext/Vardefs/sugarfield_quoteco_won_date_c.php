<?php
 // created: 2021-05-19 05:24:04
$dictionary['M01_Quote_Document']['fields']['quoteco_won_date_c']['labelValue']='Quote/CO Won Date';
$dictionary['M01_Quote_Document']['fields']['quoteco_won_date_c']['enforced']='';
$dictionary['M01_Quote_Document']['fields']['quoteco_won_date_c']['dependency']='isInList($category_id,createList("Study Quote","Quote Revision","Change Order","Pricelist Submission"))';
$dictionary['M01_Quote_Document']['fields']['quoteco_won_date_c']['required_formula']='and(equal($quoteco_status_c,"Sponsor Approved"),isInList($category_id,createList("Study Quote","Quote Revision","Change Order","Pricelist Submission")))';
$dictionary['M01_Quote_Document']['fields']['quoteco_won_date_c']['readonly_formula']='';

 ?>