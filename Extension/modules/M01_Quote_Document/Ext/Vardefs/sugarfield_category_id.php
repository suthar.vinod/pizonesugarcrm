<?php
 // created: 2019-06-25 13:00:04
$dictionary['M01_Quote_Document']['fields']['category_id']['audited']=true;
$dictionary['M01_Quote_Document']['fields']['category_id']['massupdate']=true;
$dictionary['M01_Quote_Document']['fields']['category_id']['options']='sale_document_category_list';
$dictionary['M01_Quote_Document']['fields']['category_id']['duplicate_merge']='enabled';
$dictionary['M01_Quote_Document']['fields']['category_id']['duplicate_merge_dom_value']='1';
$dictionary['M01_Quote_Document']['fields']['category_id']['merge_filter']='disabled';
$dictionary['M01_Quote_Document']['fields']['category_id']['unified_search']=false;
$dictionary['M01_Quote_Document']['fields']['category_id']['calculated']=false;
$dictionary['M01_Quote_Document']['fields']['category_id']['dependency']=false;
$dictionary['M01_Quote_Document']['fields']['category_id']['reportable']=true;

 ?>