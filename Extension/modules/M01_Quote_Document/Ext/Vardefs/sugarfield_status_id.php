<?php
 // created: 2019-06-25 13:01:21
$dictionary['M01_Quote_Document']['fields']['status_id']['default']='Active';
$dictionary['M01_Quote_Document']['fields']['status_id']['audited']=true;
$dictionary['M01_Quote_Document']['fields']['status_id']['massupdate']=true;
$dictionary['M01_Quote_Document']['fields']['status_id']['duplicate_merge']='enabled';
$dictionary['M01_Quote_Document']['fields']['status_id']['duplicate_merge_dom_value']='1';
$dictionary['M01_Quote_Document']['fields']['status_id']['merge_filter']='disabled';
$dictionary['M01_Quote_Document']['fields']['status_id']['unified_search']=false;
$dictionary['M01_Quote_Document']['fields']['status_id']['calculated']=false;
$dictionary['M01_Quote_Document']['fields']['status_id']['dependency']=false;
$dictionary['M01_Quote_Document']['fields']['status_id']['options']='document_status_list';
$dictionary['M01_Quote_Document']['fields']['status_id']['required']=true;
$dictionary['M01_Quote_Document']['fields']['status_id']['reportable']=true;

 ?>