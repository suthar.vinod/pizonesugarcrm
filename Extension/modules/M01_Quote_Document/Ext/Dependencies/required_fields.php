<?php
$dependencies['M01_Quote_Document']['required_fields'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('quoteco_status_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'quoteco_won_date_c',
                'value' => 'isInList($quoteco_status_c, createList("Sponsor Approved"))',
            ), 
        ),  
        
    ),
);