<?php

$dependencies['M01_Quote_Document']['set_name_based_category_sales'] = array(
    'hooks' => array("all"),
    'trigger' => 'ifElse(
        or(equal($category_id,"Pricelist Submission"),
        and(equal($category_id,"Pricelist Submission"),not(equal(related($m01_sales_m01_quote_document_1,"name"),"")))
        )
        ,true,false)',
    // 'triggerFields' => array('category_id','m01_sales_m01_quote_document_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'document_name',
                'value' => 'concat($category_id," ",related($m01_sales_m01_quote_document_1,"name"))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'document_name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'disabled',
            'params' => array(
                'target' => 'document_name',
                'value' => 'true',
            ),
        ),
    ),
);

$dependencies['M01_Quote_Document']['set_name_based_category_sales_1'] = array(
    'hooks' => array("all"),
    'trigger' => 'ifElse(not(equal($category_id,"Pricelist Submission")),true,false)',
    // 'triggerFields' => array('category_id','m01_sales_m01_quote_document_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'document_name',
                'value' => 'ifElse(contains($document_name,"Pricelist Submission"),"",$document_name)',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'document_name',
                'value' => 'false',
            ),
        ),
        array(
            'name' => 'disabled',
            'params' => array(
                'target' => 'document_name',
                'value' => 'false',
            ),
        ),
    ),
);

$dependencies['M01_Quote_Document']['set_name_based_category_sales_2'] = array(
    'hooks' => array("view"),
    'trigger' => 'ifElse(not(equal($category_id,"Pricelist Submission")),true,false)',
    'triggerFields' => array('category_id'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'document_name',
                'value' => 'ifElse(contains($document_name,"Pricelist Submission"),"",$document_name)',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'document_name',
                'value' => 'false',
            ),
        ),
        array(
            'name' => 'disabled',
            'params' => array(
                'target' => 'document_name',
                'value' => 'false',
            ),
        ),
    ),
);
