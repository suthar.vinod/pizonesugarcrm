<?php
$dictionary['M99_Protocol_Amendments']['fields']['name']['required']=false;
$dictionary['M99_Protocol_Amendments']['fields']['name']['len']='255';
$dictionary['M99_Protocol_Amendments']['fields']['name']['audited']=false;
$dictionary['M99_Protocol_Amendments']['fields']['name']['massupdate']=false;
$dictionary['M99_Protocol_Amendments']['fields']['name']['unified_search']=false;
$dictionary['M99_Protocol_Amendments']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['M99_Protocol_Amendments']['fields']['name']['calculated']='true';
$dictionary['M99_Protocol_Amendments']['fields']['name']['importable']='false';
$dictionary['M99_Protocol_Amendments']['fields']['name']['duplicate_merge']='disabled';
$dictionary['M99_Protocol_Amendments']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['M99_Protocol_Amendments']['fields']['name']['merge_filter']='disabled';
$dictionary['M99_Protocol_Amendments']['fields']['name']['formula']='$animal_id_c';
$dictionary['M99_Protocol_Amendments']['fields']['name']['enforced']=true;
