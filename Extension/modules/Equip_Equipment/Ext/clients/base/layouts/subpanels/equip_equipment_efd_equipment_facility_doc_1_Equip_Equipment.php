<?php
// created: 2019-02-19 17:46:13
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EFD_EQUIPMENT_FACILITY_DOC_TITLE',
    'context' =>
        array(
            'link' => 'equip_equipment_efd_equipment_facility_doc_1',
        ),
    'override_subpanel_list_view' => 'subpanel-for-equipment-facility-doc',
    'override_paneltop_view' => 'panel-top-for-equipment-facility-doc'
);