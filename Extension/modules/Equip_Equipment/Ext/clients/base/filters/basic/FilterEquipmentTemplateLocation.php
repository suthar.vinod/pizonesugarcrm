<?php
$viewdefs['Equip_Equipment']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterEquipmentTemplateLocation',
    'name' => 'Filter Location',
    'filter_definition' => array(
        array(
            'building_c' => array(
                '$in' => '',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);

