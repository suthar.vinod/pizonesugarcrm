<?php

$viewdefs['Equip_Equipment']['base']['filter']['basic']['filters'][] = array(
    'id' => 'filterEquipmentTemplate',
    'name' => 'LBL_FILTER_EQUIPMENT_TEMPLATE',
    'filter_definition' => array(
        array(
            'name' => array(
                '$contains' => 'Scale',
            ),
        )
    ),
    'editable' => true,
    'is_template' => true,
);