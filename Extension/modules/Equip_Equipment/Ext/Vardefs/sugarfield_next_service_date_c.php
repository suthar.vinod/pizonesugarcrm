<?php
 // created: 2021-04-06 05:59:19
$dictionary['Equip_Equipment']['fields']['next_service_date_c']['labelValue']='Next Service Date';
$dictionary['Equip_Equipment']['fields']['next_service_date_c']['formula']='addDays($last_service_date_c,$service_interval_days_c)';
$dictionary['Equip_Equipment']['fields']['next_service_date_c']['enforced']='false';
$dictionary['Equip_Equipment']['fields']['next_service_date_c']['dependency']='greaterThan(strlen(toString($next_service_date_c)),0)';
$dictionary['Equip_Equipment']['fields']['next_service_date_c']['required_formula']='';
$dictionary['Equip_Equipment']['fields']['next_service_date_c']['readonly_formula']='';

 ?>