<?php
// created: 2019-02-25 15:17:34
$dictionary["Equip_Equipment"]["fields"]["equip_equipment_efs_equipment_facility_servi_1"] = array (
  'name' => 'equip_equipment_efs_equipment_facility_servi_1',
  'type' => 'link',
  'relationship' => 'equip_equipment_efs_equipment_facility_servi_1',
  'source' => 'non-db',
  'module' => 'EFS_Equipment_Facility_Servi',
  'bean_name' => 'EFS_Equipment_Facility_Servi',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'id_name' => 'equip_equia9d9uipment_ida',
  'link-type' => 'many',
  'side' => 'left',
);
