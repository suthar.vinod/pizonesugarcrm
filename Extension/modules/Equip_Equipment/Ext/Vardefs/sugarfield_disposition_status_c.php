<?php
 // created: 2020-12-08 09:32:50
$dictionary['Equip_Equipment']['fields']['disposition_status_c']['labelValue']='Disposition Status';
$dictionary['Equip_Equipment']['fields']['disposition_status_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['disposition_status_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['disposition_status_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['disposition_status_c']['required_formula']='';

 ?>