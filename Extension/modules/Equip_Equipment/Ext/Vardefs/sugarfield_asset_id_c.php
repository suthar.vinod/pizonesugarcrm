<?php
 // created: 2020-12-08 09:31:29
$dictionary['Equip_Equipment']['fields']['asset_id_c']['labelValue']='Asset ID';
$dictionary['Equip_Equipment']['fields']['asset_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['asset_id_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['asset_id_c']['dependency']='equal($expense_or_capitalize_c,"Capitalize")';
$dictionary['Equip_Equipment']['fields']['asset_id_c']['required_formula']='';

 ?>