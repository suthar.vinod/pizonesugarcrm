<?php
 // created: 2019-02-20 14:23:10
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['labelValue']='Current Software/Firmware';
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['type']= 'varchar';
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['required']=false;
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['readonly']=true;

 ?>