<?php
 // created: 2020-10-06 06:40:54
$dictionary['Equip_Equipment']['fields']['out_of_service_date_c']['labelValue']='Out of Service Date';
$dictionary['Equip_Equipment']['fields']['out_of_service_date_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['out_of_service_date_c']['dependency']='isInList($status_c,createList("Inactive","Out for Service","Retired"))';

 ?>