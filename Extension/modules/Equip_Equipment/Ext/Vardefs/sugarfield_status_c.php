<?php
 // created: 2020-12-02 14:50:37
$dictionary['Equip_Equipment']['fields']['status_c']['labelValue']='Status';
$dictionary['Equip_Equipment']['fields']['status_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['status_c']['required_formula']='';
$dictionary['Equip_Equipment']['fields']['status_c']['visibility_grid']=array (
  'trigger' => 'classification_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Analytical Services' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Business Development' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Facilities' => 
    array (
      0 => '',
      1 => 'Active',
      2 => 'Inactive',
    ),
    'Finance' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Histology Services' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Human_Resources' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'In life Large Animal Care' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'In life Large Animal Research' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'In life Small Animal Care' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'In life Small Animal Research' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Information_Technology' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Interventional Surgical Research' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Lab Services' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Operations Support' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Pathology Services' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Pharmacology' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Process Improvement' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Quality Assurance Unit' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Regulatory Services' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Scientific' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Software Development' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Toxicology' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Veterinary Services' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Inlife Large Animal prepRecovery' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Data Generator With Electronic Records' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Out for Service',
      4 => 'Quarantined',
      5 => 'Retired',
      6 => 'Inactive',
    ),
    'Data Generator Without Electronic Records' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Out for Service',
      4 => 'Quarantined',
      5 => 'Retired',
      6 => 'Inactive',
    ),
    'Facility Task' => 
    array (
      0 => '',
      1 => 'Active',
      2 => 'Retired',
      3 => 'Inactive',
    ),
    'Non Data Generator' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Out for Service',
      4 => 'Quarantined',
      5 => 'Retired',
      6 => 'Inactive',
    ),
    'Non GLP Equipment' => 
    array (
      0 => '',
      1 => 'Active GLP non compliant',
      2 => 'Out for Service',
      3 => 'Quarantined',
      4 => 'Retired',
      5 => 'Inactive',
    ),
  ),
);
 ?>