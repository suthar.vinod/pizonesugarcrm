<?php
 // created: 2019-02-20 14:13:23
$dictionary['Equip_Equipment']['fields']['name']['len']='255';
$dictionary['Equip_Equipment']['fields']['name']['audited']=true;
$dictionary['Equip_Equipment']['fields']['name']['massupdate']=false;
$dictionary['Equip_Equipment']['fields']['name']['unified_search']=false;
$dictionary['Equip_Equipment']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.0',
  'searchable' => true,
);
$dictionary['Equip_Equipment']['fields']['name']['calculated']=false;

 ?>