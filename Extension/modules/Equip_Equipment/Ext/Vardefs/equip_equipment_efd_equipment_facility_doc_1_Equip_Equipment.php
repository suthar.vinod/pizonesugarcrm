<?php
// created: 2019-02-19 17:46:13
$dictionary["Equip_Equipment"]["fields"]["equip_equipment_efd_equipment_facility_doc_1"] = array (
  'name' => 'equip_equipment_efd_equipment_facility_doc_1',
  'type' => 'link',
  'relationship' => 'equip_equipment_efd_equipment_facility_doc_1',
  'source' => 'non-db',
  'module' => 'EFD_Equipment_Facility_Doc',
  'bean_name' => 'EFD_Equipment_Facility_Doc',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'id_name' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
  'link-type' => 'many',
  'side' => 'left',
);
