<?php
 // created: 2020-01-15 13:01:13
$layout_defs["Equip_Equipment"]["subpanel_setup"]['equip_equipment_sv_service_vendor_1'] = array (
  'order' => 100,
  'module' => 'SV_Service_Vendor',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_EQUIP_EQUIPMENT_SV_SERVICE_VENDOR_1_FROM_SV_SERVICE_VENDOR_TITLE',
  'get_subpanel_data' => 'equip_equipment_sv_service_vendor_1',
);
