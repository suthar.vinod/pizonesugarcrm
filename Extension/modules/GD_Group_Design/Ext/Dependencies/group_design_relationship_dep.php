<?php
/*
    * Below code is Commented for the new Requirement in ticket #279 By: Harshit Shreshthi
    
$dependencies['GD_Group_Design']['work_product_require_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_gd_group_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_gd_group_design_1_name',
                'value' => 'true',
            ),
        ),
    ),
);
*/
