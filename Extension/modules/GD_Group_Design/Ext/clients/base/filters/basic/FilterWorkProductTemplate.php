<?php
// created: 2020-08-10 
$viewdefs['GD_Group_Design']['base']['filter']['basic']['filters'][] = array (
  'id' => 'FilterWorkProductTemplate',
  'name' => 'Work Product',
  'filter_definition' => array(
    array(
      'm03_work_product_gd_group_design_1_name'  => array(
      '$in' => '',     
    ),
  ),
), 
  'editable' => true,
  'is_template' => true,
);