<?php
 // created: 2022-02-08 08:32:38
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['duplicate_merge_dom_value']=0;
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['labelValue']='Complete Yes/No';
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['calculated']='true';
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['formula']='ifElse(equal($status_c,"Complete"),0,1)';
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['enforced']='true';
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['dependency']='';
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['required_formula']='';
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['readonly_formula']='';

 ?>