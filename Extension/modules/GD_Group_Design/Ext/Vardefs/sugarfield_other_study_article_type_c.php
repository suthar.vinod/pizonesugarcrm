<?php
 // created: 2021-10-28 08:56:28
$dictionary['GD_Group_Design']['fields']['other_study_article_type_c']['labelValue']='Other Study Article Type';
$dictionary['GD_Group_Design']['fields']['other_study_article_type_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['GD_Group_Design']['fields']['other_study_article_type_c']['enforced']='';
$dictionary['GD_Group_Design']['fields']['other_study_article_type_c']['dependency']='equal($study_article_type,"Other")';
$dictionary['GD_Group_Design']['fields']['other_study_article_type_c']['required_formula']='equal($study_article_type,"Other")';
$dictionary['GD_Group_Design']['fields']['other_study_article_type_c']['readonly_formula']='';

 ?>