<?php
// created: 2021-01-19 13:33:18
$dictionary["GD_Group_Design"]["fields"]["gd_group_design_wpe_work_product_enrollment_1"] = array (
  'name' => 'gd_group_design_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'gd_group_design_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_GD_GROUP_DESIGN_TITLE',
  'id_name' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
  'link-type' => 'many',
  'side' => 'left',
);
