<?php
 // created: 2021-03-31 05:20:02
$dictionary['GD_Group_Design']['fields']['number_3_c']['labelValue']='Number';
$dictionary['GD_Group_Design']['fields']['number_3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['GD_Group_Design']['fields']['number_3_c']['enforced']='false';
$dictionary['GD_Group_Design']['fields']['number_3_c']['dependency']='';
$dictionary['GD_Group_Design']['fields']['number_3_c']['required_formula']='';
$dictionary['GD_Group_Design']['fields']['number_3_c']['readonly']='1';
$dictionary['GD_Group_Design']['fields']['number_3_c']['readonly_formula']='';

 ?>