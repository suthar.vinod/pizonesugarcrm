<?php
 // created: 2022-09-22 05:42:29
$dictionary['GD_Group_Design']['fields']['name']['hidemassupdate']=false;
$dictionary['GD_Group_Design']['fields']['name']['importable']='false';
$dictionary['GD_Group_Design']['fields']['name']['duplicate_merge']='disabled';
$dictionary['GD_Group_Design']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['GD_Group_Design']['fields']['name']['merge_filter']='disabled';
$dictionary['GD_Group_Design']['fields']['name']['unified_search']=false;
$dictionary['GD_Group_Design']['fields']['name']['calculated']='1';
$dictionary['GD_Group_Design']['fields']['name']['required']=true;
$dictionary['GD_Group_Design']['fields']['name']['readonly']=true;
$dictionary['GD_Group_Design']['fields']['name']['formula']='concat(related($bid_batch_id_gd_group_design_1,"name"),related($m03_work_product_gd_group_design_1,"name")," GRP",getDropdownValue("number_list",$number_2))';
$dictionary['GD_Group_Design']['fields']['name']['enforced']=true;

 ?>