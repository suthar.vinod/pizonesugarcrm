<?php
 // created: 2019-08-26 17:07:15
$dictionary['A1A_CPI_Findings']['fields']['name']['len']='255';
$dictionary['A1A_CPI_Findings']['fields']['name']['required']=false;
$dictionary['A1A_CPI_Findings']['fields']['name']['audited']=true;
$dictionary['A1A_CPI_Findings']['fields']['name']['massupdate']=false;
$dictionary['A1A_CPI_Findings']['fields']['name']['unified_search']=false;
$dictionary['A1A_CPI_Findings']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['A1A_CPI_Findings']['fields']['name']['calculated']=false;

 ?>