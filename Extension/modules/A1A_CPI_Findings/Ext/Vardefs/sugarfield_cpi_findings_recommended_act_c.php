<?php
 // created: 2019-08-26 17:16:20
$dictionary['A1A_CPI_Findings']['fields']['cpi_findings_recommended_act_c']['labelValue']='CPI Findings Recommended Action';
$dictionary['A1A_CPI_Findings']['fields']['cpi_findings_recommended_act_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['A1A_CPI_Findings']['fields']['cpi_findings_recommended_act_c']['enforced']='';
$dictionary['A1A_CPI_Findings']['fields']['cpi_findings_recommended_act_c']['dependency']='';

 ?>