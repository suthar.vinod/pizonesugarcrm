<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CPI_FINDINGS_DESCRIPTION_TA'] = 'CPI Findings Description';
$mod_strings['LBL_CPI_FINDINGS_RECOMMENDED_ACT'] = 'CPI Findings Recommended Action';
$mod_strings['LBL_DEPARTMENT'] = 'Department';
$mod_strings['LBL_RESOLUTION'] = 'Resolution';
$mod_strings['LBL_A1A_CPI_FINDINGS_FOCUS_DRAWER_DASHBOARD'] = 'CPI Findings Focus Drawer';
