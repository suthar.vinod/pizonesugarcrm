<?php
 // created: 2018-09-25 12:50:30
$dictionary['Employee']['fields']['employee_status']['default']='Active';
$dictionary['Employee']['fields']['employee_status']['audited']=false;
$dictionary['Employee']['fields']['employee_status']['massupdate']=true;
$dictionary['Employee']['fields']['employee_status']['duplicate_merge']='enabled';
$dictionary['Employee']['fields']['employee_status']['duplicate_merge_dom_value']='1';
$dictionary['Employee']['fields']['employee_status']['merge_filter']='disabled';
$dictionary['Employee']['fields']['employee_status']['unified_search']=false;
$dictionary['Employee']['fields']['employee_status']['calculated']=false;
$dictionary['Employee']['fields']['employee_status']['dependency']=false;

 ?>