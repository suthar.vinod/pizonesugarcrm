<?php
 // created: 2018-09-24 15:24:08
$dictionary['Employee']['fields']['email']['len']='255';
$dictionary['Employee']['fields']['email']['required']=false;
$dictionary['Employee']['fields']['email']['audited']=false;
$dictionary['Employee']['fields']['email']['massupdate']=true;
$dictionary['Employee']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Employee']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['Employee']['fields']['email']['merge_filter']='disabled';
$dictionary['Employee']['fields']['email']['unified_search']=false;
$dictionary['Employee']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.67',
  'searchable' => true,
);
$dictionary['Employee']['fields']['email']['calculated']=false;

 ?>