<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_COMPANY_INFORMATION_ACCOUNT_ID'] = 'Company Information (related Company ID)';
$mod_strings['LBL_COMPANY_INFORMATION'] = 'Company Information';
$mod_strings['LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_ACCOUNTS_TITLE'] = 'Acquired by Company';
$mod_strings['LBL_AC01_ACQUIRED_COMPANIES_FOCUS_DRAWER_DASHBOARD'] = 'Acquired Companies Focus Drawer';
$mod_strings['LBL_AC01_ACQUIRED_COMPANIES_RECORD_DASHBOARD'] = 'Acquired Companies Record Dashboard';
