<?php
// created: 2018-02-14 15:35:52
$dictionary["AC01_Acquired_Companies"]["fields"]["accounts_ac01_acquired_companies_1"] = array (
  'name' => 'accounts_ac01_acquired_companies_1',
  'type' => 'link',
  'relationship' => 'accounts_ac01_acquired_companies_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_AC01_ACQUIRED_COMPANIES_TITLE',
  'id_name' => 'accounts_ac01_acquired_companies_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["AC01_Acquired_Companies"]["fields"]["accounts_ac01_acquired_companies_1_name"] = array (
  'name' => 'accounts_ac01_acquired_companies_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_ac01_acquired_companies_1accounts_ida',
  'link' => 'accounts_ac01_acquired_companies_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["AC01_Acquired_Companies"]["fields"]["accounts_ac01_acquired_companies_1accounts_ida"] = array (
  'name' => 'accounts_ac01_acquired_companies_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_AC01_ACQUIRED_COMPANIES_TITLE_ID',
  'id_name' => 'accounts_ac01_acquired_companies_1accounts_ida',
  'link' => 'accounts_ac01_acquired_companies_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
