<?php
 // created: 2019-06-25 14:31:30
$dictionary['AC01_Acquired_Companies']['fields']['name']['len']='255';
$dictionary['AC01_Acquired_Companies']['fields']['name']['audited']=true;
$dictionary['AC01_Acquired_Companies']['fields']['name']['massupdate']=false;
$dictionary['AC01_Acquired_Companies']['fields']['name']['unified_search']=false;
$dictionary['AC01_Acquired_Companies']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['AC01_Acquired_Companies']['fields']['name']['calculated']=false;

 ?>