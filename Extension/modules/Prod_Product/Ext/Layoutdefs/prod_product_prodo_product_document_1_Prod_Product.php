<?php
 // created: 2020-09-10 08:30:08
$layout_defs["Prod_Product"]["subpanel_setup"]['prod_product_prodo_product_document_1'] = array (
  'order' => 100,
  'module' => 'ProDo_Product_Document',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PROD_PRODUCT_PRODO_PRODUCT_DOCUMENT_1_FROM_PRODO_PRODUCT_DOCUMENT_TITLE',
  'get_subpanel_data' => 'prod_product_prodo_product_document_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
