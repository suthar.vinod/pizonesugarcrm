<?php
// created: 2021-10-19 11:02:37
$dictionary["Prod_Product"]["fields"]["prod_product_poi_purchase_order_item_1"] = array (
  'name' => 'prod_product_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'prod_product_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'POI_Purchase_Order_Item',
  'bean_name' => 'POI_Purchase_Order_Item',
  'vname' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_PROD_PRODUCT_TITLE',
  'id_name' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
