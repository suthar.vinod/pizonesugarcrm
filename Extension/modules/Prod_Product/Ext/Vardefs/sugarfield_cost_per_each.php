<?php
 // created: 2021-09-04 04:30:27
$dictionary['Prod_Product']['fields']['cost_per_each']['required']=true;
$dictionary['Prod_Product']['fields']['cost_per_each']['name']='cost_per_each';
$dictionary['Prod_Product']['fields']['cost_per_each']['vname']='LBL_COST_PER_EACH';
$dictionary['Prod_Product']['fields']['cost_per_each']['type']='currency';
$dictionary['Prod_Product']['fields']['cost_per_each']['massupdate']=false;
$dictionary['Prod_Product']['fields']['cost_per_each']['no_default']=false;
$dictionary['Prod_Product']['fields']['cost_per_each']['comments']='';
$dictionary['Prod_Product']['fields']['cost_per_each']['help']='';
$dictionary['Prod_Product']['fields']['cost_per_each']['importable']='false';
$dictionary['Prod_Product']['fields']['cost_per_each']['duplicate_merge']='disabled';
$dictionary['Prod_Product']['fields']['cost_per_each']['duplicate_merge_dom_value']=0;
$dictionary['Prod_Product']['fields']['cost_per_each']['audited']=true;
$dictionary['Prod_Product']['fields']['cost_per_each']['reportable']=true;
$dictionary['Prod_Product']['fields']['cost_per_each']['unified_search']=false;
$dictionary['Prod_Product']['fields']['cost_per_each']['merge_filter']='disabled';
$dictionary['Prod_Product']['fields']['cost_per_each']['pii']=false;
$dictionary['Prod_Product']['fields']['cost_per_each']['calculated']='1';
$dictionary['Prod_Product']['fields']['cost_per_each']['formula']='divide($cost_per_unit_2,$unit_quantity)';
$dictionary['Prod_Product']['fields']['cost_per_each']['enforced']=true;
$dictionary['Prod_Product']['fields']['cost_per_each']['len']=26;
$dictionary['Prod_Product']['fields']['cost_per_each']['size']='20';
$dictionary['Prod_Product']['fields']['cost_per_each']['enable_range_search']=false;
$dictionary['Prod_Product']['fields']['cost_per_each']['precision']=6;
$dictionary['Prod_Product']['fields']['cost_per_each']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Prod_Product']['fields']['cost_per_each']['dependency']='isInList($purchase_unit,createList("Box","Case","Sleeve","Pallet","Roll","Bottle"))';
$dictionary['Prod_Product']['fields']['cost_per_each']['hidemassupdate']=false;
$dictionary['Prod_Product']['fields']['cost_per_each']['convertToBase']=true;
$dictionary['Prod_Product']['fields']['cost_per_each']['showTransactionalAmount']=true;

 ?>