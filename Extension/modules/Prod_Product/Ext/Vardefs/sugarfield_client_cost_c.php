<?php
 // created: 2020-09-03 19:16:01
$dictionary['Prod_Product']['fields']['client_cost_c']['duplicate_merge_dom_value']=0;
$dictionary['Prod_Product']['fields']['client_cost_c']['labelValue']='Client Cost';
$dictionary['Prod_Product']['fields']['client_cost_c']['calculated']='1';
$dictionary['Prod_Product']['fields']['client_cost_c']['formula']='ifElse(not(equal($cost_per_each_2,"")),add($cost_per_each_2,multiply($cost_per_each_2,divide($mark_up_c,100))),add($cost_per_unit_2,multiply($cost_per_unit_2,divide($mark_up_c,100))))';
$dictionary['Prod_Product']['fields']['client_cost_c']['enforced']='1';
$dictionary['Prod_Product']['fields']['client_cost_c']['dependency']='';
$dictionary['Prod_Product']['fields']['client_cost_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>