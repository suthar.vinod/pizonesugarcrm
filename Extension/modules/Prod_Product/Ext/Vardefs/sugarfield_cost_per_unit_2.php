<?php
 // created: 2021-09-04 04:30:27
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['required']=true;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['name']='cost_per_unit_2';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['vname']='LBL_COST_PER_UNIT_2';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['type']='currency';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['massupdate']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['no_default']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['comments']='';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['help']='';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['importable']='true';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['duplicate_merge']='enabled';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['duplicate_merge_dom_value']='1';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['audited']=true;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['reportable']=true;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['unified_search']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['merge_filter']='disabled';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['pii']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['default']='';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['calculated']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['len']=26;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['size']='20';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['enable_range_search']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['precision']=6;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['dependency']='isInList($purchase_unit,createList("Box","Case","Sleeve","Pallet","Roll","Bottle"))';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['hidemassupdate']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['convertToBase']=true;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['showTransactionalAmount']=true;

 ?>