<?php
// created: 2020-09-10 08:30:09
$dictionary["Prod_Product"]["fields"]["prod_product_prodo_product_document_1"] = array (
  'name' => 'prod_product_prodo_product_document_1',
  'type' => 'link',
  'relationship' => 'prod_product_prodo_product_document_1',
  'source' => 'non-db',
  'module' => 'ProDo_Product_Document',
  'bean_name' => 'ProDo_Product_Document',
  'vname' => 'LBL_PROD_PRODUCT_PRODO_PRODUCT_DOCUMENT_1_FROM_PROD_PRODUCT_TITLE',
  'id_name' => 'prod_product_prodo_product_document_1prod_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
