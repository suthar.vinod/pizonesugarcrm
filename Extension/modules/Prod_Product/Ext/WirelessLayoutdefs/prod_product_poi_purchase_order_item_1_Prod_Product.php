<?php
 // created: 2021-10-19 11:02:37
$layout_defs["Prod_Product"]["subpanel_setup"]['prod_product_poi_purchase_order_item_1'] = array (
  'order' => 100,
  'module' => 'POI_Purchase_Order_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'get_subpanel_data' => 'prod_product_poi_purchase_order_item_1',
);
