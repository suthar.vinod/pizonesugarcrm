<?php
 // created: 2021-10-19 09:41:58
$layout_defs["Prod_Product"]["subpanel_setup"]['prod_product_ori_order_request_item_1'] = array (
  'order' => 100,
  'module' => 'ORI_Order_Request_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'get_subpanel_data' => 'prod_product_ori_order_request_item_1',
);
