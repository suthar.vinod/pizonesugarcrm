<?php
$viewdefs['Prod_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterProd_ProductNSBTemplate',
    'name' => 'Filter Product',
    'filter_definition' => array(
        array(
            'department' => array(
                '$contains' => '',
            ),
        ),
		array(
            'type_2' => array(
                '$in' => '',
            ),
        ),
		array(
            'status_c' => array(
                '$not_in' => '',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);

