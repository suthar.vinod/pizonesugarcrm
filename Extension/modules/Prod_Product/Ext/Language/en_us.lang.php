<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_LAST_PRICE_UPDATE'] = 'Last Price Update';
$mod_strings['LBL_STATUS'] = 'Status';
$mod_strings['LBL_NOTES_MULTISELECT'] = 'Notes Multiselect';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_CLIENT_COST'] = 'Client Cost';
$mod_strings['LBL_MARK_UP'] = 'Mark-Up (%)';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_PROD_PRODUCT_FOCUS_DRAWER_DASHBOARD'] = 'Products Focus Drawer';
$mod_strings['LBL_PROD_PRODUCT_RECORD_DASHBOARD'] = 'Products Record Dashboard';
