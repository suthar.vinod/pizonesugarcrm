<?php

$hook_array['after_save'][] = Array(1,'Set qualification method in Companies','custom/modules/CD_Company_Documents/customLogicHook.php','customLogicHook','setQualificationMethod',);
$hook_array['before_save'][] = Array('1','workflow','include/workflow/WorkFlowHandler.php','WorkFlowHandler','WorkFlowHandler',);
$hook_array['after_relationship_add'][] = Array(1,'Set qualification method in Companies','custom/modules/CD_Company_Documents/customLogicHook.php','customLogicHook','setQualificationMethod',);
$hook_array['after_relationship_delete'][] = Array(1,'Update qualification method in Companies','custom/modules/CD_Company_Documents/customLogicHook.php','customLogicHook','updateQualificationMethod',);

?>