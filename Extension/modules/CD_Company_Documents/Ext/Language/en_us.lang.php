<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DOC_ACTIVE_DATE'] = 'Active Date';
$mod_strings['LBL_VENDOR_STATUS'] = 'Vendor Status';
$mod_strings['LBL_METHOD_OF_QUALIFICATION'] = 'Method of Qualification';
$mod_strings['LBL_DATE_OF_QUALIFICATION'] = 'Qualified Date';
$mod_strings['LBL_CD_COMPANY_DOCUMENTS_FOCUS_DRAWER_DASHBOARD'] = 'Company Documents Focus Drawer';
$mod_strings['LBL_CD_COMPANY_DOCUMENTS_RECORD_DASHBOARD'] = 'Company Documents Record Dashboard';
