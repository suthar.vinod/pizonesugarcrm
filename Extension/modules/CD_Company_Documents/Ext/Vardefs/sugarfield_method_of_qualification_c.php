<?php
 // created: 2019-02-20 13:50:01
$dictionary['CD_Company_Documents']['fields']['method_of_qualification_c']['labelValue']='Method of Qualification';
$dictionary['CD_Company_Documents']['fields']['method_of_qualification_c']['visibility_grid']=array (
  'trigger' => 'category_id',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Material Transfer Agreement' => 
    array (
    ),
    'MSA' => 
    array (
    ),
    'MSA Amendment' => 
    array (
    ),
    'NDA_CDA' => 
    array (
    ),
    'Qualification' => 
    array (
      0 => '',
      1 => 'APS Management Approval',
      2 => 'CV',
      3 => 'Questionnaire',
      4 => 'Site Visit',
    ),
    'Quality Agreement' => 
    array (
    ),
    'Supplemental' => 
    array (
    ),
  ),
);

 ?>