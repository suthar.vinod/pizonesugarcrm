<?php
 // created: 2019-02-20 13:43:28
$dictionary['CD_Company_Documents']['fields']['vendor_status_c']['labelValue']='Vendor Status';
$dictionary['CD_Company_Documents']['fields']['vendor_status_c']['visibility_grid']=array (
  'trigger' => 'category_id',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Material Transfer Agreement' => 
    array (
    ),
    'MSA' => 
    array (
    ),
    'MSA Amendment' => 
    array (
    ),
    'NDA_CDA' => 
    array (
    ),
    'Qualification' => 
    array (
      0 => '',
      1 => 'Approved',
      2 => 'Disapproved',
      3 => 'Pending Approval',
      4 => 'Retired',
    ),
    'Quality Agreement' => 
    array (
    ),
    'Supplemental' => 
    array (
    ),
  ),
);

 ?>