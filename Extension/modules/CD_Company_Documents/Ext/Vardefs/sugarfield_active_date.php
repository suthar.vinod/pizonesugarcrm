<?php
 // created: 2019-06-27 13:22:40
$dictionary['CD_Company_Documents']['fields']['active_date']['audited']=true;
$dictionary['CD_Company_Documents']['fields']['active_date']['massupdate']=true;
$dictionary['CD_Company_Documents']['fields']['active_date']['duplicate_merge']='enabled';
$dictionary['CD_Company_Documents']['fields']['active_date']['duplicate_merge_dom_value']='1';
$dictionary['CD_Company_Documents']['fields']['active_date']['merge_filter']='disabled';
$dictionary['CD_Company_Documents']['fields']['active_date']['unified_search']=false;
$dictionary['CD_Company_Documents']['fields']['active_date']['calculated']=false;
$dictionary['CD_Company_Documents']['fields']['active_date']['enable_range_search']=false;
$dictionary['CD_Company_Documents']['fields']['active_date']['dependency']='isInList($category_id,createList("MSA","MSA Amendment","NDA_CDA","Quality Agreement","Material Transfer Agreement","Supplemental"))';

 ?>