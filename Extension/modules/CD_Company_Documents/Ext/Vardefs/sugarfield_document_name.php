<?php
 // created: 2019-06-19 16:05:29
$dictionary['CD_Company_Documents']['fields']['document_name']['audited']=true;
$dictionary['CD_Company_Documents']['fields']['document_name']['massupdate']=false;
$dictionary['CD_Company_Documents']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['CD_Company_Documents']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['CD_Company_Documents']['fields']['document_name']['merge_filter']='disabled';
$dictionary['CD_Company_Documents']['fields']['document_name']['unified_search']=false;
$dictionary['CD_Company_Documents']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['CD_Company_Documents']['fields']['document_name']['calculated']=false;

 ?>