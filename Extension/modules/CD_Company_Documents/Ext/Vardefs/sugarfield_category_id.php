<?php
 // created: 2019-05-23 16:46:01
$dictionary['CD_Company_Documents']['fields']['category_id']['audited']=true;
$dictionary['CD_Company_Documents']['fields']['category_id']['massupdate']=true;
$dictionary['CD_Company_Documents']['fields']['category_id']['options']='category_id_list';
$dictionary['CD_Company_Documents']['fields']['category_id']['duplicate_merge']='enabled';
$dictionary['CD_Company_Documents']['fields']['category_id']['duplicate_merge_dom_value']='1';
$dictionary['CD_Company_Documents']['fields']['category_id']['merge_filter']='disabled';
$dictionary['CD_Company_Documents']['fields']['category_id']['unified_search']=false;
$dictionary['CD_Company_Documents']['fields']['category_id']['calculated']=false;
$dictionary['CD_Company_Documents']['fields']['category_id']['dependency']=false;
$dictionary['CD_Company_Documents']['fields']['category_id']['reportable']=true;

 ?>