<?php
 // created: 2019-06-27 13:23:18
$dictionary['CD_Company_Documents']['fields']['exp_date']['audited']=true;
$dictionary['CD_Company_Documents']['fields']['exp_date']['massupdate']=true;
$dictionary['CD_Company_Documents']['fields']['exp_date']['duplicate_merge']='enabled';
$dictionary['CD_Company_Documents']['fields']['exp_date']['duplicate_merge_dom_value']='1';
$dictionary['CD_Company_Documents']['fields']['exp_date']['merge_filter']='disabled';
$dictionary['CD_Company_Documents']['fields']['exp_date']['unified_search']=false;
$dictionary['CD_Company_Documents']['fields']['exp_date']['calculated']=false;
$dictionary['CD_Company_Documents']['fields']['exp_date']['enable_range_search']=false;
$dictionary['CD_Company_Documents']['fields']['exp_date']['dependency']='isInList($category_id,createList("MSA","MSA Amendment","NDA_CDA","Quality Agreement","Material Transfer Agreement","Supplemental"))';

 ?>