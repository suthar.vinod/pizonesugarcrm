<?php
// created: 2016-02-25 20:23:10
$dictionary["Task"]["fields"]["tasks_m03_work_product_1"] = array (
  'name' => 'tasks_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'tasks_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_1_FROM_TASKS_TITLE',
  'id_name' => 'tasks_m03_work_product_1tasks_ida',
  'link-type' => 'many',
  'side' => 'left',
);
