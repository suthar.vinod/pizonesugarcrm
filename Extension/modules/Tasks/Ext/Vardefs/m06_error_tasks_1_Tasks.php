<?php
// created: 2019-05-19 21:27:08
$dictionary["Task"]["fields"]["m06_error_tasks_1"] = array (
  'name' => 'm06_error_tasks_1',
  'type' => 'link',
  'relationship' => 'm06_error_tasks_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'side' => 'right',
  'vname' => 'LBL_M06_ERROR_TASKS_1_FROM_TASKS_TITLE',
  'id_name' => 'm06_error_tasks_1m06_error_ida',
  'link-type' => 'one',
);
$dictionary["Task"]["fields"]["m06_error_tasks_1_name"] = array (
  'name' => 'm06_error_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M06_ERROR_TASKS_1_FROM_M06_ERROR_TITLE',
  'save' => true,
  'id_name' => 'm06_error_tasks_1m06_error_ida',
  'link' => 'm06_error_tasks_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["m06_error_tasks_1m06_error_ida"] = array (
  'name' => 'm06_error_tasks_1m06_error_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M06_ERROR_TASKS_1_FROM_TASKS_TITLE_ID',
  'id_name' => 'm06_error_tasks_1m06_error_ida',
  'link' => 'm06_error_tasks_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
