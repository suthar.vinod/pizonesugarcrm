<?php
 // created: 2022-02-03 10:05:11
$dictionary['Task']['fields']['contact_name']['len']=255;
$dictionary['Task']['fields']['contact_name']['audited']=false;
$dictionary['Task']['fields']['contact_name']['massupdate']=true;
$dictionary['Task']['fields']['contact_name']['hidemassupdate']=false;
$dictionary['Task']['fields']['contact_name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['contact_name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['contact_name']['merge_filter']='disabled';
$dictionary['Task']['fields']['contact_name']['unified_search']=false;
$dictionary['Task']['fields']['contact_name']['calculated']=false;
$dictionary['Task']['fields']['contact_name']['dependency']='not(equal($task_type_c,"Schedule Matrix Review"))';
$dictionary['Task']['fields']['contact_name']['related_fields']=array (
  0 => 'contact_id',
);

 ?>