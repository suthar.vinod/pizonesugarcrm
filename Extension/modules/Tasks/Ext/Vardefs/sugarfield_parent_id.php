<?php
 // created: 2022-02-03 10:00:02
$dictionary['Task']['fields']['parent_id']['audited']=false;
$dictionary['Task']['fields']['parent_id']['massupdate']=false;
$dictionary['Task']['fields']['parent_id']['hidemassupdate']=false;
$dictionary['Task']['fields']['parent_id']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['parent_id']['duplicate_merge_dom_value']=1;
$dictionary['Task']['fields']['parent_id']['merge_filter']='disabled';
$dictionary['Task']['fields']['parent_id']['unified_search']=false;
$dictionary['Task']['fields']['parent_id']['calculated']=false;
$dictionary['Task']['fields']['parent_id']['len']=255;
$dictionary['Task']['fields']['parent_id']['vname']='LBL_PARENT_TYPE';
$dictionary['Task']['fields']['parent_id']['reportable']=true;
$dictionary['Task']['fields']['parent_id']['group']='';

 ?>