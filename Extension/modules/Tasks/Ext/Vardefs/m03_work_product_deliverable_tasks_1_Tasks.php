<?php
// created: 2016-02-29 19:53:08
$dictionary["Task"]["fields"]["m03_work_product_deliverable_tasks_1"] = array (
  'name' => 'm03_work_product_deliverable_tasks_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_deliverable_tasks_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Deliverable',
  'bean_name' => 'M03_Work_Product_Deliverable',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE',
  'id_name' => 'm03_work_pbb72verable_ida',
  'link-type' => 'one',
);
$dictionary["Task"]["fields"]["m03_work_product_deliverable_tasks_1_name"] = array (
  'name' => 'm03_work_product_deliverable_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'save' => true,
  'id_name' => 'm03_work_pbb72verable_ida',
  'link' => 'm03_work_product_deliverable_tasks_1',
  'table' => 'm03_work_product_deliverable',
  'module' => 'M03_Work_Product_Deliverable',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["m03_work_pbb72verable_ida"] = array (
  'name' => 'm03_work_pbb72verable_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE_ID',
  'id_name' => 'm03_work_pbb72verable_ida',
  'link' => 'm03_work_product_deliverable_tasks_1',
  'table' => 'm03_work_product_deliverable',
  'module' => 'M03_Work_Product_Deliverable',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
