<?php
 // created: 2022-02-03 10:05:11
$dictionary['Task']['fields']['contact_id']['name']='contact_id';
$dictionary['Task']['fields']['contact_id']['type']='id';
$dictionary['Task']['fields']['contact_id']['group']='contact_name';
$dictionary['Task']['fields']['contact_id']['reportable']=false;
$dictionary['Task']['fields']['contact_id']['vname']='LBL_CONTACT_ID';
$dictionary['Task']['fields']['contact_id']['audited']=false;
$dictionary['Task']['fields']['contact_id']['importable']='true';

 ?>