<?php
 // created: 2022-02-03 10:00:02
$dictionary['Task']['fields']['parent_type']['audited']=false;
$dictionary['Task']['fields']['parent_type']['massupdate']=true;
$dictionary['Task']['fields']['parent_type']['hidemassupdate']=false;
$dictionary['Task']['fields']['parent_type']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['parent_type']['duplicate_merge_dom_value']=1;
$dictionary['Task']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['Task']['fields']['parent_type']['unified_search']=false;
$dictionary['Task']['fields']['parent_type']['calculated']=false;
$dictionary['Task']['fields']['parent_type']['len']=255;
$dictionary['Task']['fields']['parent_type']['options']='';
$dictionary['Task']['fields']['parent_type']['vname']='LBL_PARENT_TYPE';

 ?>