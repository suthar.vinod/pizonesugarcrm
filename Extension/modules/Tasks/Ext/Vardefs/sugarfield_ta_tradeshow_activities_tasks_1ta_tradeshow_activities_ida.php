<?php
 // created: 2022-02-03 10:03:21
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['name']='ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['type']='id';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['source']='non-db';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['vname']='LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TASKS_TITLE_ID';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['id_name']='ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['link']='ta_tradeshow_activities_tasks_1';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['table']='ta_tradeshow_activities';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['module']='TA_Tradeshow_Activities';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['rname']='id';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['reportable']=false;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['side']='right';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['massupdate']=false;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['duplicate_merge']='disabled';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['hideacl']=true;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['audited']=false;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['importable']='true';

 ?>