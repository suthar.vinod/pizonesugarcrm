<?php
 // created: 2022-02-03 10:02:00
$dictionary['Task']['fields']['sa_wp_id_c']['duplicate_merge_dom_value']=0;
$dictionary['Task']['fields']['sa_wp_id_c']['labelValue']='SA Quote ID';
$dictionary['Task']['fields']['sa_wp_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Task']['fields']['sa_wp_id_c']['calculated']='1';
$dictionary['Task']['fields']['sa_wp_id_c']['formula']='related($m01_sales_tasks_1,"work_product_id_c")';
$dictionary['Task']['fields']['sa_wp_id_c']['enforced']='1';
$dictionary['Task']['fields']['sa_wp_id_c']['dependency']='not(equal($task_type_c,"Schedule Matrix Review"))';
$dictionary['Task']['fields']['sa_wp_id_c']['required_formula']='';
$dictionary['Task']['fields']['sa_wp_id_c']['readonly_formula']='';

 ?>