<?php
 // created: 2022-02-03 10:01:25
$dictionary['Task']['fields']['m06_error_tasks_1_name']['audited']=false;
$dictionary['Task']['fields']['m06_error_tasks_1_name']['massupdate']=true;
$dictionary['Task']['fields']['m06_error_tasks_1_name']['hidemassupdate']=false;
$dictionary['Task']['fields']['m06_error_tasks_1_name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['m06_error_tasks_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['m06_error_tasks_1_name']['merge_filter']='disabled';
$dictionary['Task']['fields']['m06_error_tasks_1_name']['reportable']=false;
$dictionary['Task']['fields']['m06_error_tasks_1_name']['unified_search']=false;
$dictionary['Task']['fields']['m06_error_tasks_1_name']['calculated']=false;
$dictionary['Task']['fields']['m06_error_tasks_1_name']['dependency']='not(equal($task_type_c,"Schedule Matrix Review"))';
$dictionary['Task']['fields']['m06_error_tasks_1_name']['vname']='LBL_M06_ERROR_TASKS_1_NAME_FIELD_TITLE';

 ?>