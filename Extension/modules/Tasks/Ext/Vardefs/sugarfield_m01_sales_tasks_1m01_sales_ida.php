<?php
 // created: 2022-02-03 10:05:55
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['name']='m01_sales_tasks_1m01_sales_ida';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['type']='id';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['source']='non-db';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['vname']='LBL_M01_SALES_TASKS_1_FROM_TASKS_TITLE_ID';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['id_name']='m01_sales_tasks_1m01_sales_ida';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['link']='m01_sales_tasks_1';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['table']='m01_sales';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['module']='M01_Sales';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['rname']='id';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['reportable']=false;
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['side']='right';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['massupdate']=false;
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['duplicate_merge']='disabled';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['hideacl']=true;
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['audited']=false;
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['importable']='true';

 ?>