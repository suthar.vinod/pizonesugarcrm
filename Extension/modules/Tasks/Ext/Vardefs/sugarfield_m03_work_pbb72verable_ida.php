<?php
 // created: 2022-02-03 10:04:20
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['name']='m03_work_pbb72verable_ida';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['type']='id';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['source']='non-db';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['vname']='LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE_ID';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['id_name']='m03_work_pbb72verable_ida';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['link']='m03_work_product_deliverable_tasks_1';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['table']='m03_work_product_deliverable';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['module']='M03_Work_Product_Deliverable';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['rname']='id';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['reportable']=false;
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['side']='right';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['massupdate']=false;
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['duplicate_merge']='disabled';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['hideacl']=true;
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['audited']=false;
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['importable']='true';

 ?>