<?php
 // created: 2020-02-27 13:45:17
$dictionary['Task']['fields']['priority']['default']='High';
$dictionary['Task']['fields']['priority']['audited']=true;
$dictionary['Task']['fields']['priority']['massupdate']=true;
$dictionary['Task']['fields']['priority']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['priority']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['priority']['merge_filter']='disabled';
$dictionary['Task']['fields']['priority']['unified_search']=false;
$dictionary['Task']['fields']['priority']['calculated']=false;
$dictionary['Task']['fields']['priority']['dependency']=false;

 ?>