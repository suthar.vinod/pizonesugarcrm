<?php
 // created: 2022-02-03 10:01:24
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['name']='m06_error_tasks_1m06_error_ida';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['type']='id';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['source']='non-db';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['vname']='LBL_M06_ERROR_TASKS_1_FROM_TASKS_TITLE_ID';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['id_name']='m06_error_tasks_1m06_error_ida';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['link']='m06_error_tasks_1';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['table']='m06_error';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['module']='M06_Error';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['rname']='id';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['reportable']=false;
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['side']='right';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['massupdate']=false;
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['duplicate_merge']='disabled';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['hideacl']=true;
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['audited']=false;
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['importable']='true';

 ?>