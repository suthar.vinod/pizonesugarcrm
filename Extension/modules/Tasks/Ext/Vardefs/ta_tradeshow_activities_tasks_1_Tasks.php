<?php
// created: 2019-10-31 11:25:05
$dictionary["Task"]["fields"]["ta_tradeshow_activities_tasks_1"] = array (
  'name' => 'ta_tradeshow_activities_tasks_1',
  'type' => 'link',
  'relationship' => 'ta_tradeshow_activities_tasks_1',
  'source' => 'non-db',
  'module' => 'TA_Tradeshow_Activities',
  'bean_name' => 'TA_Tradeshow_Activities',
  'side' => 'right',
  'vname' => 'LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TASKS_TITLE',
  'id_name' => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
  'link-type' => 'one',
);
$dictionary["Task"]["fields"]["ta_tradeshow_activities_tasks_1_name"] = array (
  'name' => 'ta_tradeshow_activities_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'save' => true,
  'id_name' => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
  'link' => 'ta_tradeshow_activities_tasks_1',
  'table' => 'ta_tradeshow_activities',
  'module' => 'TA_Tradeshow_Activities',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida"] = array (
  'name' => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TASKS_TITLE_ID',
  'id_name' => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
  'link' => 'ta_tradeshow_activities_tasks_1',
  'table' => 'ta_tradeshow_activities',
  'module' => 'TA_Tradeshow_Activities',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
