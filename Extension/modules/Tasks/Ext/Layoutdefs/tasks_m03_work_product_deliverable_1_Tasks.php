<?php
 // created: 2016-02-29 15:01:15
$layout_defs["Tasks"]["subpanel_setup"]['tasks_m03_work_product_deliverable_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product_Deliverable',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'get_subpanel_data' => 'tasks_m03_work_product_deliverable_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
  ),
);
