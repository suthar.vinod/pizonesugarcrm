<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_TASKS_1_FROM_M01_SALES_TITLE'] = 'Sales Activity';
$mod_strings['LBL_EMAIL_REMINDER_TIME'] = 'Email Reminder Time';
$mod_strings['LBL_DUE_DATE'] = 'Send/Due Date';
$mod_strings['LBL_LIST_DUE_DATE'] = 'Send/Due Date';
$mod_strings['LBL_TASK_TYPE'] = 'Task Type';
$mod_strings['LBL_REVIEW_DATE'] = 'Review Date';
$mod_strings['LBL_STATUS'] = 'Status:';
$mod_strings['LBL_M01_SALES_TASKS_1_FROM_TASKS_TITLE_ID'] = 'Sales ID';
$mod_strings['LBL_M01_SALES_TASKS_1_FROM_TASKS_TITLE'] = 'Sales';
$mod_strings['LBL_TASKS_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA SA Documents';
$mod_strings['LBL_CONTACT_NAME'] = 'Sponsor Contact';
$mod_strings['LBL_TASKS_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';
$mod_strings['LBL_LIST_CONTACT'] = 'Sponsor Contact';
$mod_strings['LBL_LIST_RELATED_TO'] = 'Parent Record';
$mod_strings['LBL_SA_WP_ID'] = 'SA Quote ID';
$mod_strings['LBL_M06_ERROR_TASKS_1_FROM_M06_ERROR_TITLE'] = 'Communications';
$mod_strings['LBL_NO_NECROPSY_NEEDED'] = 'No Necropsy Needed';
$mod_strings['LBL_M06_ERROR_TASKS_1_NAME_FIELD_TITLE'] = 'Communications';
$mod_strings['LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_NAME_FIELD_TITLE'] = 'Tradeshow Activities';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_NAME_FIELD_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M01_SALES_TASKS_1_NAME_FIELD_TITLE'] = 'Sales Activity';
