<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_COMMUNICATION_DATE'] = 'Communication Date';
$mod_strings['LBL_NAME'] = 'System ID (Communication)';
$mod_strings['LBL_ASSIGNED_TO'] = 'Communication Owner';
$mod_strings['LNK_NEW_RECORD'] = 'Create APS Communication';
$mod_strings['LNK_LIST'] = 'View APS Communications';
$mod_strings['LBL_MODULE_NAME'] = 'APS Communications';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = '(Inactive) APS Communication';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New (Inactive) APS Communication';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import (Inactive) APS Communication vCard';
$mod_strings['LNK_IMPORT_M06_APS_COMMUNICATION'] = 'Import APS Communication';
$mod_strings['LBL_LIST_FORM_TITLE'] = '(Inactive) APS Communications List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search (Inactive) APS Communication';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My APS Communications';
$mod_strings['LBL_M06_APS_COMMUNICATION_FOCUS_DRAWER_DASHBOARD'] = 'APS Communications Focus Drawer';
$mod_strings['LBL_M06_APS_COMMUNICATION_RECORD_DASHBOARD'] = 'APS Communications Record Dashboard';
