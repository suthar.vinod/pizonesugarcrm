<?php
 // created: 2016-10-25 03:27:11
$dictionary['m06_APS_Communication']['fields']['name']['len'] = '255';
$dictionary['m06_APS_Communication']['fields']['name']['audited'] = false;
$dictionary['m06_APS_Communication']['fields']['name']['massupdate'] = false;
$dictionary['m06_APS_Communication']['fields']['name']['unified_search'] = false;
$dictionary['m06_APS_Communication']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['m06_APS_Communication']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['m06_APS_Communication']['fields']['name']['full_text_search']['boost'] = 1.5500000000000000444089209850062616169452667236328125;
$dictionary['m06_APS_Communication']['fields']['name']['calculated'] = false;

