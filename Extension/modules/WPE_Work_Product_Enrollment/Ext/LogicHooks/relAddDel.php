<?php

$hook_version = 1;
$hook_array['after_relationship_add'][] = array(
    1001,
    'Manage Category and status of Hook',
    'custom/modules/WPE_Work_Product_Enrollment/LogicHooksImplementations/updateFieldsOfAN.php',
    'HookWPERelManagement',
    'manageFieldsAdd'
);
$hook_array['after_relationship_delete'][] = array(
    1001,
    'Manage Category and status of Hook',
    'custom/modules/WPE_Work_Product_Enrollment/LogicHooksImplementations/updateFieldsOfAN.php',
    'HookWPERelManagement',
    'manageFieldsDel'
);