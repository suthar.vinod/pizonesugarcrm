<?php

class HookBackupAnimalRelManagement {

    /**
     * 
      Animal Module
      -  We would like the USDA Category pulled from the WPE module; however, it needs to pull in the highest category (E>D>C>B) associated to that animal
      o    e.g. WPE on 4/10 has USDA category D, but WPE on 5/10 has USDA category C; the animal module would then show the USDA category as a D
      -  We would like the enrollment status in the animal module to mirror of the most recent, non-deleted (active) Enrollment Status in the Work Product Enrollment record associated to that animal
      o    e.g. WPE on 4/10 states Back-up Used, and WPE on 5/10 states �Non-na�ve Stock�; the enrollment status within the animal module for that animal would then state �non-na�ve stock�
     *
     */
    public function backupCategoryAndStatus($bean, $event, $arguments) {
        $bean->fetched_row_enrollment_status_c = $bean->fetched_row['enrollment_status_c'];
        $bean->fetched_row_usda_category_c = $bean->fetched_row['usda_category_c'];
    }

}
