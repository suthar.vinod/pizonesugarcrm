<?php
// created: 2017-09-12 15:10:30
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["m03_work_product_wpe_work_product_enrollment_1"] = array (
  'name' => 'm03_work_product_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'id_name' => 'm03_work_p7d13product_ida',
  'link-type' => 'one',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["m03_work_product_wpe_work_product_enrollment_1_name"] = array (
  'name' => 'm03_work_product_wpe_work_product_enrollment_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p7d13product_ida',
  'link' => 'm03_work_product_wpe_work_product_enrollment_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["m03_work_p7d13product_ida"] = array (
  'name' => 'm03_work_p7d13product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID',
  'id_name' => 'm03_work_p7d13product_ida',
  'link' => 'm03_work_product_wpe_work_product_enrollment_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
