<?php
 // created: 2021-04-07 14:01:02
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['labelValue']='USDA ID';
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['formula']='related($anml_animals_wpe_work_product_enrollment_1,"usda_id_c")';
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['enforced']='false';
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['dependency']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['required_formula']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['readonly_formula']='';

 ?>