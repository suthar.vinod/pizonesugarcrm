<?php
 // created: 2021-11-30 09:24:54
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['audited']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['hidemassupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['duplicate_merge']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['duplicate_merge_dom_value']='0';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['merge_filter']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['calculated']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['vname']='LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_NAME_FIELD_TITLE';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['massupdate']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['reportable']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['unified_search']=false;

 ?>