<?php
 // created: 2022-04-26 11:48:58
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['len']='255';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['required']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['audited']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['massupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['unified_search']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['calculated']='1';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['hidemassupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['importable']='false';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['duplicate_merge']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['merge_filter']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['formula']='concat(related($bid_batch_id_wpe_work_product_enrollment_1,"name"),related($m03_work_product_wpe_work_product_enrollment_1,"name")," ",related($anml_animals_wpe_work_product_enrollment_1,"animal_id_c"),"-",related($anml_animals_wpe_work_product_enrollment_1,"usda_id_c"))';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['enforced']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['readonly']=true;

 ?>