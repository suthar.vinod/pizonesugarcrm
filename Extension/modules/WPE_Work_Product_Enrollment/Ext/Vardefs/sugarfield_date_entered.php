<?php
 // created: 2021-01-05 07:45:53
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['audited']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['comments']='Date record created';
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['unified_search']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['calculated']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['enable_range_search']='1';
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['hidemassupdate']=false;

 ?>