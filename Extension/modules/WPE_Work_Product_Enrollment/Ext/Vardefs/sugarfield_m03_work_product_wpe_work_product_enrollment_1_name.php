<?php
 // created: 2022-04-26 09:17:00
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['required']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['audited']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['massupdate']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['hidemassupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['duplicate_merge']='enabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['duplicate_merge_dom_value']='1';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['merge_filter']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['reportable']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['unified_search']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['calculated']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['vname']='LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_NAME_FIELD_TITLE';

 ?>