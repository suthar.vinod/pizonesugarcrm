<?php
// created: 2017-09-12 15:03:53
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["anml_animals_wpe_work_product_enrollment_1"] = array (
  'name' => 'anml_animals_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'anml_animals_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'id_name' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["anml_animals_wpe_work_product_enrollment_1_name"] = array (
  'name' => 'anml_animals_wpe_work_product_enrollment_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
  'link' => 'anml_animals_wpe_work_product_enrollment_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["anml_animals_wpe_work_product_enrollment_1anml_animals_ida"] = array (
  'name' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID',
  'id_name' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
  'link' => 'anml_animals_wpe_work_product_enrollment_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
