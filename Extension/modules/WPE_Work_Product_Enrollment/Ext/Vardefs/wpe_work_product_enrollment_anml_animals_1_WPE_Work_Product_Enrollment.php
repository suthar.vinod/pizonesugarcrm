<?php
// created: 2017-09-12 15:08:34
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["wpe_work_product_enrollment_anml_animals_1"] = array (
  'name' => 'wpe_work_product_enrollment_anml_animals_1',
  'type' => 'link',
  'relationship' => 'wpe_work_product_enrollment_anml_animals_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["wpe_work_product_enrollment_anml_animals_1_name"] = array (
  'name' => 'wpe_work_product_enrollment_anml_animals_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
  'link' => 'wpe_work_product_enrollment_anml_animals_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["wpe_work_product_enrollment_anml_animals_1anml_animals_idb"] = array (
  'name' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE_ID',
  'id_name' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
  'link' => 'wpe_work_product_enrollment_anml_animals_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
