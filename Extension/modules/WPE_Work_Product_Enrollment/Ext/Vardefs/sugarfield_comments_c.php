<?php
 // created: 2019-04-17 12:43:55
$dictionary['WPE_Work_Product_Enrollment']['fields']['comments_c']['labelValue']='Comments';
$dictionary['WPE_Work_Product_Enrollment']['fields']['comments_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['WPE_Work_Product_Enrollment']['fields']['comments_c']['enforced']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['comments_c']['dependency']='';

 ?>