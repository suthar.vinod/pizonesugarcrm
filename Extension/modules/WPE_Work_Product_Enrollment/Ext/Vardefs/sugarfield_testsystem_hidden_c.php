<?php
 // created: 2020-12-15 08:58:03
$dictionary['WPE_Work_Product_Enrollment']['fields']['testsystem_hidden_c']['labelValue']='WPA Hidden Field';
$dictionary['WPE_Work_Product_Enrollment']['fields']['testsystem_hidden_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['WPE_Work_Product_Enrollment']['fields']['testsystem_hidden_c']['enforced']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['testsystem_hidden_c']['dependency']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['testsystem_hidden_c']['required_formula']='';

 ?>