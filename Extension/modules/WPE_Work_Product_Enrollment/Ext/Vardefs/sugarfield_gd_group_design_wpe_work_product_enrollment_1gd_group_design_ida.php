<?php
 // created: 2022-02-01 14:19:08
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['name']='gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['type']='id';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['source']='non-db';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['vname']='LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['id_name']='gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['link']='gd_group_design_wpe_work_product_enrollment_1';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['table']='gd_group_design';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['module']='GD_Group_Design';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['rname']='id';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['reportable']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['side']='right';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['massupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['duplicate_merge']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['hideacl']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['audited']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['importable']='true';

 ?>