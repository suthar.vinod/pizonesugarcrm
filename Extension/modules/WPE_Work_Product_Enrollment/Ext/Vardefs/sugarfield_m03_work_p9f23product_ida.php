<?php
 // created: 2021-11-30 09:24:54
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['name']='m03_work_p9f23product_ida';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['type']='id';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['source']='non-db';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['vname']='LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['id_name']='m03_work_p9f23product_ida';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['link']='m03_work_product_wpe_work_product_enrollment_2';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['table']='m03_work_product';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['module']='M03_Work_Product';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['rname']='id';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['reportable']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['side']='right';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['massupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['duplicate_merge']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['hideacl']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['audited']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['importable']='true';

 ?>