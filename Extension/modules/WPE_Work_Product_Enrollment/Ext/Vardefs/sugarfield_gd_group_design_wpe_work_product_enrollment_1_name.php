<?php
 // created: 2022-02-01 14:19:08
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['audited']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['massupdate']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['hidemassupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['duplicate_merge']='enabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['duplicate_merge_dom_value']='1';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['merge_filter']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['reportable']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['unified_search']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['calculated']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['vname']='LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_NAME_FIELD_TITLE';

 ?>