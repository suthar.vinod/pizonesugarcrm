<?php
 // created: 2022-04-26 09:16:58
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['name']='m03_work_p7d13product_ida';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['type']='id';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['source']='non-db';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['vname']='LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['id_name']='m03_work_p7d13product_ida';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['link']='m03_work_product_wpe_work_product_enrollment_1';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['table']='m03_work_product';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['module']='M03_Work_Product';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['rname']='id';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['reportable']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['side']='right';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['massupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['duplicate_merge']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['hideacl']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['audited']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['importable']='true';

 ?>