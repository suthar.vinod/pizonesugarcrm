<?php

$dependencies['WPE_Work_Product_Enrollment']['make_read_only_enroll'] = array(
    'hooks' => array("all"),   
    'trigger' => 'true',
    //Optional, the trigger for the dependency. Defaults to 'true'.
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'm03_work_product_wpe_work_product_enrollment_2_name',
                'value'=>'true'
            ),
        ),
    ),
);

