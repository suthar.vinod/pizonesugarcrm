<?php
 // created: 2021-04-01 09:05:41
$layout_defs["Meetings"]["subpanel_setup"]['meetings_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'meetings_m03_work_product_1',
);
