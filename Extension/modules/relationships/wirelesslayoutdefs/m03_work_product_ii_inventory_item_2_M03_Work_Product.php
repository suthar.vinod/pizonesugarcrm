<?php
 // created: 2021-11-09 10:07:50
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_ii_inventory_item_2'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'm03_work_product_ii_inventory_item_2',
);
