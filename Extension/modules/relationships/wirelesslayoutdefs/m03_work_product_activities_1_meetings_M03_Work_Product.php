<?php
 // created: 2019-08-05 12:03:27
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_activities_1_meetings'] = array (
  'order' => 100,
  'module' => 'Meetings',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE',
  'get_subpanel_data' => 'm03_work_product_activities_1_meetings',
);
