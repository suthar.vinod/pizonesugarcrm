<?php
 // created: 2022-09-22 05:31:46
$layout_defs["BID_Batch_ID"]["subpanel_setup"]['bid_batch_id_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_BID_BATCH_ID_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'bid_batch_id_tasks_1',
);
