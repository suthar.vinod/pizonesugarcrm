<?php
 // created: 2022-01-03 10:55:30
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_de_deviation_employees_2'] = array (
  'order' => 100,
  'module' => 'DE_Deviation_Employees',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_2_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'get_subpanel_data' => 'm06_error_de_deviation_employees_2',
);
