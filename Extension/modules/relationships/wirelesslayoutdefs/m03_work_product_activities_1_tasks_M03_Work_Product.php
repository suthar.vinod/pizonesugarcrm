<?php
 // created: 2019-08-05 12:04:19
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_activities_1_tasks'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_TASKS_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'm03_work_product_activities_1_tasks',
);
