<?php
 // created: 2020-01-07 13:50:42
$layout_defs["CIE_Clinical_Issue_Exam"]["subpanel_setup"]['cie_clinical_issue_exam_co_clinical_observation_2'] = array (
  'order' => 100,
  'module' => 'CO_Clinical_Observation',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_2_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'get_subpanel_data' => 'cie_clinical_issue_exam_co_clinical_observation_2',
);
