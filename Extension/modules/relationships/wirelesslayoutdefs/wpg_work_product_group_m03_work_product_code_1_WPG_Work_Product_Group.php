<?php
 // created: 2021-04-20 09:04:33
$layout_defs["WPG_Work_Product_Group"]["subpanel_setup"]['wpg_work_product_group_m03_work_product_code_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product_Code',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'get_subpanel_data' => 'wpg_work_product_group_m03_work_product_code_1',
);
