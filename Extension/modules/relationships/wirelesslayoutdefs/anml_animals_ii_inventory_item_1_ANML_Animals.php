<?php
 // created: 2021-11-09 10:02:11
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'anml_animals_ii_inventory_item_1',
);
