<?php
 // created: 2021-11-09 10:19:23
$layout_defs["IC_Inventory_Collection"]["subpanel_setup"]['ic_inventory_collection_im_inventory_management_1'] = array (
  'order' => 100,
  'module' => 'IM_Inventory_Management',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'get_subpanel_data' => 'ic_inventory_collection_im_inventory_management_1',
);
