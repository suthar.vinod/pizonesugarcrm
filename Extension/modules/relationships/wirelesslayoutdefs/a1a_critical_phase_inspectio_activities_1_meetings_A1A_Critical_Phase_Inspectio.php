<?php
 // created: 2017-09-13 15:15:31
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['a1a_critical_phase_inspectio_activities_1_meetings'] = array (
  'order' => 100,
  'module' => 'Meetings',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE',
  'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_meetings',
);
