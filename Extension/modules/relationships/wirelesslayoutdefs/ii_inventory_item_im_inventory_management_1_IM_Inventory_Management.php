<?php
 // created: 2021-11-09 09:39:34
$layout_defs["IM_Inventory_Management"]["subpanel_setup"]['ii_inventory_item_im_inventory_management_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'ii_inventory_item_im_inventory_management_1',
);
