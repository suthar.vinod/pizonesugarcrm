<?php
 // created: 2021-12-07 12:00:27
$layout_defs["TC_NAMSA_Test_Codes"]["subpanel_setup"]['tc_namsa_test_codes_m03_work_product_code_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product_Code',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'get_subpanel_data' => 'tc_namsa_test_codes_m03_work_product_code_1',
);
