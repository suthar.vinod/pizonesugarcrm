<?php
 // created: 2016-02-29 19:53:08
$layout_defs["M03_Work_Product_Deliverable"]["subpanel_setup"]['m03_work_product_deliverable_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'm03_work_product_deliverable_tasks_1',
);
