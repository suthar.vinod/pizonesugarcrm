<?php
 // created: 2020-01-07 14:17:22
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_co_clinical_observation_1'] = array (
  'order' => 100,
  'module' => 'CO_Clinical_Observation',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'get_subpanel_data' => 'anml_animals_co_clinical_observation_1',
);
