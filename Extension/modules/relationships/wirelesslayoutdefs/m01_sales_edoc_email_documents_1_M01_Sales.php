<?php
 // created: 2019-08-27 11:35:41
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_edoc_email_documents_1'] = array (
  'order' => 100,
  'module' => 'EDoc_Email_Documents',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'm01_sales_edoc_email_documents_1',
);
