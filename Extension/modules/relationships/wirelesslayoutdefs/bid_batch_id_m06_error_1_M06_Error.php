<?php
 // created: 2022-04-26 07:03:59
$layout_defs["M06_Error"]["subpanel_setup"]['bid_batch_id_m06_error_1'] = array (
  'order' => 100,
  'module' => 'BID_Batch_ID',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_BID_BATCH_ID_M06_ERROR_1_FROM_BID_BATCH_ID_TITLE',
  'get_subpanel_data' => 'bid_batch_id_m06_error_1',
);
