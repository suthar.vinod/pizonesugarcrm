<?php
 // created: 2021-02-23 10:42:04
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_usda_historical_usda_id_1'] = array (
  'order' => 100,
  'module' => 'USDA_Historical_USDA_ID',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_USDA_HISTORICAL_USDA_ID_TITLE',
  'get_subpanel_data' => 'anml_animals_usda_historical_usda_id_1',
);
