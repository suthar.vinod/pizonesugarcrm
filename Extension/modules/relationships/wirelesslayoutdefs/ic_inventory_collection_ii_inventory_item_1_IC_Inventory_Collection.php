<?php
 // created: 2021-11-09 09:43:46
$layout_defs["IC_Inventory_Collection"]["subpanel_setup"]['ic_inventory_collection_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'ic_inventory_collection_ii_inventory_item_1',
);
