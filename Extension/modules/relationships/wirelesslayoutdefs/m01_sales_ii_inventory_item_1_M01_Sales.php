<?php
 // created: 2021-11-09 09:56:04
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'm01_sales_ii_inventory_item_1',
);
