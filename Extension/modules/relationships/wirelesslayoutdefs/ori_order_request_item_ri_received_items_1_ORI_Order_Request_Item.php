<?php
 // created: 2021-10-19 09:15:00
$layout_defs["ORI_Order_Request_Item"]["subpanel_setup"]['ori_order_request_item_ri_received_items_1'] = array (
  'order' => 100,
  'module' => 'RI_Received_Items',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'get_subpanel_data' => 'ori_order_request_item_ri_received_items_1',
);
