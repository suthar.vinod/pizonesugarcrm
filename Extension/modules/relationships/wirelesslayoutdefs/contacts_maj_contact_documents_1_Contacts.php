<?php
 // created: 2019-08-15 11:34:01
$layout_defs["Contacts"]["subpanel_setup"]['contacts_maj_contact_documents_1'] = array (
  'order' => 100,
  'module' => 'MAJ_Contact_Documents',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_MAJ_CONTACT_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'contacts_maj_contact_documents_1',
);
