<?php
 // created: 2021-11-09 09:48:27
$layout_defs["POI_Purchase_Order_Item"]["subpanel_setup"]['poi_purchase_order_item_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'poi_purchase_order_item_ii_inventory_item_1',
);
