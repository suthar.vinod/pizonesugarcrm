<?php
 // created: 2022-04-26 06:50:11
$layout_defs["BID_Batch_ID"]["subpanel_setup"]['bid_batch_id_wpe_work_product_enrollment_1'] = array (
  'order' => 100,
  'module' => 'WPE_Work_Product_Enrollment',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'get_subpanel_data' => 'bid_batch_id_wpe_work_product_enrollment_1',
);
