<?php
 // created: 2021-10-19 11:00:50
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_poi_purchase_order_item_1'] = array (
  'order' => 100,
  'module' => 'POI_Purchase_Order_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'get_subpanel_data' => 'm03_work_product_poi_purchase_order_item_1',
);
