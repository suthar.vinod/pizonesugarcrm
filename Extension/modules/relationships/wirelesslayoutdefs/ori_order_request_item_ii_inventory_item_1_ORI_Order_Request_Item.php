<?php
 // created: 2021-11-09 09:46:03
$layout_defs["ORI_Order_Request_Item"]["subpanel_setup"]['ori_order_request_item_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'ori_order_request_item_ii_inventory_item_1',
);
