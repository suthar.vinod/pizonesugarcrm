<?php
 // created: 2021-11-09 09:52:04
$layout_defs["RI_Received_Items"]["subpanel_setup"]['ri_received_items_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'ri_received_items_ii_inventory_item_1',
);
