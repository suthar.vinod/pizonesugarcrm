<?php
 // created: 2019-07-09 11:54:56
$layout_defs["M01_Sales"]["subpanel_setup"]['rr_regulatory_response_m01_sales_1'] = array (
  'order' => 100,
  'module' => 'RR_Regulatory_Response',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'get_subpanel_data' => 'rr_regulatory_response_m01_sales_1',
);
