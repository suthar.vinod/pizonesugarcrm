<?php
// created: 2020-01-07 13:46:57
$dictionary["cie_clinical_issue_exam_cie_clinical_issue_exam_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'cie_clinical_issue_exam_cie_clinical_issue_exam_1' => 
    array (
      'lhs_module' => 'CIE_Clinical_Issue_Exam',
      'lhs_table' => 'cie_clinical_issue_exam',
      'lhs_key' => 'id',
      'rhs_module' => 'CIE_Clinical_Issue_Exam',
      'rhs_table' => 'cie_clinical_issue_exam',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_c',
      'join_key_lhs' => 'cie_clinic2bf6ue_exam_ida',
      'join_key_rhs' => 'cie_clinic7f62ue_exam_idb',
    ),
  ),
  'table' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'cie_clinic2bf6ue_exam_ida' => 
    array (
      'name' => 'cie_clinic2bf6ue_exam_ida',
      'type' => 'id',
    ),
    'cie_clinic7f62ue_exam_idb' => 
    array (
      'name' => 'cie_clinic7f62ue_exam_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_cie_clinical_issue_exam_cie_clinical_issue_exam_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_cie_clinical_issue_exam_cie_clinical_issue_exam_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cie_clinic2bf6ue_exam_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_cie_clinical_issue_exam_cie_clinical_issue_exam_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cie_clinic7f62ue_exam_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'cie_clinic7f62ue_exam_idb',
      ),
    ),
  ),
);