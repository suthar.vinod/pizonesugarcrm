<?php
// created: 2021-08-14 05:50:46
$dictionary["gd_group_design_taskd_task_design_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'gd_group_design_taskd_task_design_1' => 
    array (
      'lhs_module' => 'GD_Group_Design',
      'lhs_table' => 'gd_group_design',
      'lhs_key' => 'id',
      'rhs_module' => 'TaskD_Task_Design',
      'rhs_table' => 'taskd_task_design',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'gd_group_design_taskd_task_design_1_c',
      'join_key_lhs' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
      'join_key_rhs' => 'gd_group_design_taskd_task_design_1taskd_task_design_idb',
    ),
  ),
  'table' => 'gd_group_design_taskd_task_design_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'gd_group_design_taskd_task_design_1gd_group_design_ida' => 
    array (
      'name' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
      'type' => 'id',
    ),
    'gd_group_design_taskd_task_design_1taskd_task_design_idb' => 
    array (
      'name' => 'gd_group_design_taskd_task_design_1taskd_task_design_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_gd_group_design_taskd_task_design_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_gd_group_design_taskd_task_design_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_gd_group_design_taskd_task_design_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'gd_group_design_taskd_task_design_1taskd_task_design_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'gd_group_design_taskd_task_design_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'gd_group_design_taskd_task_design_1taskd_task_design_idb',
      ),
    ),
  ),
);