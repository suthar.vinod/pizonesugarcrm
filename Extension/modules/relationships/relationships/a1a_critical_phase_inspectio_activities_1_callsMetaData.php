<?php
// created: 2017-09-13 15:14:51
$dictionary["a1a_critical_phase_inspectio_activities_1_calls"] = array (
  'relationships' => 
  array (
    'a1a_critical_phase_inspectio_activities_1_calls' => 
    array (
      'lhs_module' => 'A1A_Critical_Phase_Inspectio',
      'lhs_table' => 'a1a_critical_phase_inspectio',
      'lhs_key' => 'id',
      'rhs_module' => 'Calls',
      'rhs_table' => 'calls',
      'relationship_role_column_value' => 'A1A_Critical_Phase_Inspectio',
      'rhs_key' => 'parent_id',
      'relationship_type' => 'one-to-many',
      'relationship_role_column' => 'parent_type',
    ),
  ),
  'fields' => '',
  'indices' => '',
  'table' => '',
);