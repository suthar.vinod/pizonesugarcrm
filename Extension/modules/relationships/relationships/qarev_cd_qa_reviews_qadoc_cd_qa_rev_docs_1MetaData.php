<?php
// created: 2021-02-11 07:50:25
$dictionary["qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1' => 
    array (
      'lhs_module' => 'QARev_CD_QA_Reviews',
      'lhs_table' => 'qarev_cd_qa_reviews',
      'lhs_key' => 'id',
      'rhs_module' => 'QADoc_CD_QA_Rev_Docs',
      'rhs_table' => 'qadoc_cd_qa_rev_docs',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1_c',
      'join_key_lhs' => 'qarev_cd_q5440reviews_ida',
      'join_key_rhs' => 'qarev_cd_qeeebev_docs_idb',
    ),
  ),
  'table' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'qarev_cd_q5440reviews_ida' => 
    array (
      'name' => 'qarev_cd_q5440reviews_ida',
      'type' => 'id',
    ),
    'qarev_cd_qeeebev_docs_idb' => 
    array (
      'name' => 'qarev_cd_qeeebev_docs_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'qarev_cd_q5440reviews_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'qarev_cd_qeeebev_docs_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'qarev_cd_qeeebev_docs_idb',
      ),
    ),
  ),
);