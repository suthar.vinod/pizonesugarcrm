<?php
// created: 2020-01-07 13:43:54
$dictionary["cie_clinical_issue_exam_co_clinical_observation_1"] = array (
  'true_relationship_type' => 'one-to-one',
  'from_studio' => true,
  'relationships' => 
  array (
    'cie_clinical_issue_exam_co_clinical_observation_1' => 
    array (
      'lhs_module' => 'CIE_Clinical_Issue_Exam',
      'lhs_table' => 'cie_clinical_issue_exam',
      'lhs_key' => 'id',
      'rhs_module' => 'CO_Clinical_Observation',
      'rhs_table' => 'co_clinical_observation',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cie_clinical_issue_exam_co_clinical_observation_1_c',
      'join_key_lhs' => 'cie_clinicb9a6ue_exam_ida',
      'join_key_rhs' => 'cie_clinicecbbrvation_idb',
    ),
  ),
  'table' => 'cie_clinical_issue_exam_co_clinical_observation_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'cie_clinicb9a6ue_exam_ida' => 
    array (
      'name' => 'cie_clinicb9a6ue_exam_ida',
      'type' => 'id',
    ),
    'cie_clinicecbbrvation_idb' => 
    array (
      'name' => 'cie_clinicecbbrvation_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_cie_clinical_issue_exam_co_clinical_observation_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_cie_clinical_issue_exam_co_clinical_observation_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cie_clinicb9a6ue_exam_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_cie_clinical_issue_exam_co_clinical_observation_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cie_clinicecbbrvation_idb',
        1 => 'deleted',
      ),
    ),
  ),
);