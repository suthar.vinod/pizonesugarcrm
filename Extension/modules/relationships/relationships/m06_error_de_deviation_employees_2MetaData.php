<?php
// created: 2022-01-03 10:55:30
$dictionary["m06_error_de_deviation_employees_2"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'm06_error_de_deviation_employees_2' => 
    array (
      'lhs_module' => 'M06_Error',
      'lhs_table' => 'm06_error',
      'lhs_key' => 'id',
      'rhs_module' => 'DE_Deviation_Employees',
      'rhs_table' => 'de_deviation_employees',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'm06_error_de_deviation_employees_2_c',
      'join_key_lhs' => 'm06_error_de_deviation_employees_2m06_error_ida',
      'join_key_rhs' => 'm06_error_de_deviation_employees_2de_deviation_employees_idb',
    ),
  ),
  'table' => 'm06_error_de_deviation_employees_2_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'm06_error_de_deviation_employees_2m06_error_ida' => 
    array (
      'name' => 'm06_error_de_deviation_employees_2m06_error_ida',
      'type' => 'id',
    ),
    'm06_error_de_deviation_employees_2de_deviation_employees_idb' => 
    array (
      'name' => 'm06_error_de_deviation_employees_2de_deviation_employees_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_m06_error_de_deviation_employees_2_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_m06_error_de_deviation_employees_2_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm06_error_de_deviation_employees_2m06_error_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_m06_error_de_deviation_employees_2_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm06_error_de_deviation_employees_2de_deviation_employees_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'm06_error_de_deviation_employees_2_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'm06_error_de_deviation_employees_2m06_error_ida',
        1 => 'm06_error_de_deviation_employees_2de_deviation_employees_idb',
      ),
    ),
  ),
);