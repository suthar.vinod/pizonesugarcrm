<?php
// created: 2021-01-12 11:07:14
$dictionary["sp_service_pricing_sp_service_pricing_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sp_service_pricing_sp_service_pricing_1' => 
    array (
      'lhs_module' => 'SP_Service_Pricing',
      'lhs_table' => 'sp_service_pricing',
      'lhs_key' => 'id',
      'rhs_module' => 'SP_Service_Pricing',
      'rhs_table' => 'sp_service_pricing',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sp_service_pricing_sp_service_pricing_1_c',
      'join_key_lhs' => 'sp_service_pricing_sp_service_pricing_1sp_service_pricing_ida',
      'join_key_rhs' => 'sp_service_pricing_sp_service_pricing_1sp_service_pricing_idb',
    ),
  ),
  'table' => 'sp_service_pricing_sp_service_pricing_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sp_service_pricing_sp_service_pricing_1sp_service_pricing_ida' => 
    array (
      'name' => 'sp_service_pricing_sp_service_pricing_1sp_service_pricing_ida',
      'type' => 'id',
    ),
    'sp_service_pricing_sp_service_pricing_1sp_service_pricing_idb' => 
    array (
      'name' => 'sp_service_pricing_sp_service_pricing_1sp_service_pricing_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sp_service_pricing_sp_service_pricing_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sp_service_pricing_sp_service_pricing_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sp_service_pricing_sp_service_pricing_1sp_service_pricing_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sp_service_pricing_sp_service_pricing_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sp_service_pricing_sp_service_pricing_1sp_service_pricing_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sp_service_pricing_sp_service_pricing_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sp_service_pricing_sp_service_pricing_1sp_service_pricing_ida',
        1 => 'sp_service_pricing_sp_service_pricing_1sp_service_pricing_idb',
      ),
    ),
  ),
);