<?php
// created: 2017-09-12 15:08:34
$dictionary["wpe_work_product_enrollment_anml_animals_1"] = array (
  'true_relationship_type' => 'one-to-one',
  'from_studio' => true,
  'relationships' => 
  array (
    'wpe_work_product_enrollment_anml_animals_1' => 
    array (
      'lhs_module' => 'WPE_Work_Product_Enrollment',
      'lhs_table' => 'wpe_work_product_enrollment',
      'lhs_key' => 'id',
      'rhs_module' => 'ANML_Animals',
      'rhs_table' => 'anml_animals',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'wpe_work_product_enrollment_anml_animals_1_c',
      'join_key_lhs' => 'wpe_work_p83ebollment_ida',
      'join_key_rhs' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
    ),
  ),
  'table' => 'wpe_work_product_enrollment_anml_animals_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'wpe_work_p83ebollment_ida' => 
    array (
      'name' => 'wpe_work_p83ebollment_ida',
      'type' => 'id',
    ),
    'wpe_work_product_enrollment_anml_animals_1anml_animals_idb' => 
    array (
      'name' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_wpe_work_product_enrollment_anml_animals_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_wpe_work_product_enrollment_anml_animals_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'wpe_work_p83ebollment_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_wpe_work_product_enrollment_anml_animals_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
        1 => 'deleted',
      ),
    ),
  ),
);