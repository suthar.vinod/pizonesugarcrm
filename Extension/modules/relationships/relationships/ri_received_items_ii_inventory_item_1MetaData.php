<?php
// created: 2021-11-09 09:52:04
$dictionary["ri_received_items_ii_inventory_item_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'ri_received_items_ii_inventory_item_1' => 
    array (
      'lhs_module' => 'RI_Received_Items',
      'lhs_table' => 'ri_received_items',
      'lhs_key' => 'id',
      'rhs_module' => 'II_Inventory_Item',
      'rhs_table' => 'ii_inventory_item',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'ri_received_items_ii_inventory_item_1_c',
      'join_key_lhs' => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
      'join_key_rhs' => 'ri_received_items_ii_inventory_item_1ii_inventory_item_idb',
    ),
  ),
  'table' => 'ri_received_items_ii_inventory_item_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'ri_received_items_ii_inventory_item_1ri_received_items_ida' => 
    array (
      'name' => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
      'type' => 'id',
    ),
    'ri_received_items_ii_inventory_item_1ii_inventory_item_idb' => 
    array (
      'name' => 'ri_received_items_ii_inventory_item_1ii_inventory_item_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_ri_received_items_ii_inventory_item_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_ri_received_items_ii_inventory_item_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_ri_received_items_ii_inventory_item_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ri_received_items_ii_inventory_item_1ii_inventory_item_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'ri_received_items_ii_inventory_item_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'ri_received_items_ii_inventory_item_1ii_inventory_item_idb',
      ),
    ),
  ),
);