<?php
// created: 2021-04-20 09:04:33
$dictionary["wpg_work_product_group_m03_work_product_code_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'wpg_work_product_group_m03_work_product_code_1' => 
    array (
      'lhs_module' => 'WPG_Work_Product_Group',
      'lhs_table' => 'wpg_work_product_group',
      'lhs_key' => 'id',
      'rhs_module' => 'M03_Work_Product_Code',
      'rhs_table' => 'm03_work_product_code',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'wpg_work_product_group_m03_work_product_code_1_c',
      'join_key_lhs' => 'wpg_work_p164at_group_ida',
      'join_key_rhs' => 'wpg_work_pa92cct_code_idb',
    ),
  ),
  'table' => 'wpg_work_product_group_m03_work_product_code_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'wpg_work_p164at_group_ida' => 
    array (
      'name' => 'wpg_work_p164at_group_ida',
      'type' => 'id',
    ),
    'wpg_work_pa92cct_code_idb' => 
    array (
      'name' => 'wpg_work_pa92cct_code_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_wpg_work_product_group_m03_work_product_code_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_wpg_work_product_group_m03_work_product_code_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'wpg_work_p164at_group_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_wpg_work_product_group_m03_work_product_code_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'wpg_work_pa92cct_code_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'wpg_work_product_group_m03_work_product_code_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'wpg_work_pa92cct_code_idb',
      ),
    ),
  ),
);