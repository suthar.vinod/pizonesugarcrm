<?php
// created: 2019-02-25 15:18:39
$dictionary["efs_equipment_facility_servi_efr_equipment_facility_recor_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'efs_equipment_facility_servi_efr_equipment_facility_recor_1' => 
    array (
      'lhs_module' => 'EFS_Equipment_Facility_Servi',
      'lhs_table' => 'efs_equipment_facility_servi',
      'lhs_key' => 'id',
      'rhs_module' => 'EFR_Equipment_Facility_Recor',
      'rhs_table' => 'efr_equipment_facility_recor',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1_c',
      'join_key_lhs' => 'efs_equipmd3d1y_servi_ida',
      'join_key_rhs' => 'efs_equipm4bb2y_recor_idb',
    ),
  ),
  'table' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'efs_equipmd3d1y_servi_ida' => 
    array (
      'name' => 'efs_equipmd3d1y_servi_ida',
      'type' => 'id',
    ),
    'efs_equipm4bb2y_recor_idb' => 
    array (
      'name' => 'efs_equipm4bb2y_recor_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_efs_equipment_facility_servi_efr_equipment_facility_recor_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_efs_equipment_facility_servi_efr_equipment_facility_recor_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'efs_equipmd3d1y_servi_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_efs_equipment_facility_servi_efr_equipment_facility_recor_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'efs_equipm4bb2y_recor_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'efs_equipm4bb2y_recor_idb',
      ),
    ),
  ),
);