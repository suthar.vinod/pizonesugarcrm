<?php
// created: 2021-04-29 06:58:32
$dictionary["s_species_sc_species_census_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    's_species_sc_species_census_1' => 
    array (
      'lhs_module' => 'S_Species',
      'lhs_table' => 's_species',
      'lhs_key' => 'id',
      'rhs_module' => 'SC_Species_Census',
      'rhs_table' => 'sc_species_census',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 's_species_sc_species_census_1_c',
      'join_key_lhs' => 's_species_sc_species_census_1s_species_ida',
      'join_key_rhs' => 's_species_sc_species_census_1sc_species_census_idb',
    ),
  ),
  'table' => 's_species_sc_species_census_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    's_species_sc_species_census_1s_species_ida' => 
    array (
      'name' => 's_species_sc_species_census_1s_species_ida',
      'type' => 'id',
    ),
    's_species_sc_species_census_1sc_species_census_idb' => 
    array (
      'name' => 's_species_sc_species_census_1sc_species_census_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_s_species_sc_species_census_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_s_species_sc_species_census_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 's_species_sc_species_census_1s_species_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_s_species_sc_species_census_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 's_species_sc_species_census_1sc_species_census_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 's_species_sc_species_census_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 's_species_sc_species_census_1sc_species_census_idb',
      ),
    ),
  ),
);