<?php
// created: 2018-04-23 17:12:12
$dictionary["accounts_ta_tradeshow_activities_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'accounts_ta_tradeshow_activities_1' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'TA_Tradeshow_Activities',
      'rhs_table' => 'ta_tradeshow_activities',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_ta_tradeshow_activities_1_c',
      'join_key_lhs' => 'accounts_ta_tradeshow_activities_1accounts_ida',
      'join_key_rhs' => 'accounts_ta_tradeshow_activities_1ta_tradeshow_activities_idb',
    ),
  ),
  'table' => 'accounts_ta_tradeshow_activities_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'accounts_ta_tradeshow_activities_1accounts_ida' => 
    array (
      'name' => 'accounts_ta_tradeshow_activities_1accounts_ida',
      'type' => 'id',
    ),
    'accounts_ta_tradeshow_activities_1ta_tradeshow_activities_idb' => 
    array (
      'name' => 'accounts_ta_tradeshow_activities_1ta_tradeshow_activities_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_accounts_ta_tradeshow_activities_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_accounts_ta_tradeshow_activities_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_ta_tradeshow_activities_1accounts_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_accounts_ta_tradeshow_activities_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_ta_tradeshow_activities_1ta_tradeshow_activities_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'accounts_ta_tradeshow_activities_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'accounts_ta_tradeshow_activities_1ta_tradeshow_activities_idb',
      ),
    ),
  ),
);