<?php
// created: 2019-07-09 12:11:34
$dictionary["rrd_regulatory_response_doc_rr_regulatory_response_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'rrd_regulatory_response_doc_rr_regulatory_response_1' => 
    array (
      'lhs_module' => 'RRD_Regulatory_Response_Doc',
      'lhs_table' => 'rrd_regulatory_response_doc',
      'lhs_key' => 'id',
      'rhs_module' => 'RR_Regulatory_Response',
      'rhs_table' => 'rr_regulatory_response',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'rrd_regulatory_response_doc_rr_regulatory_response_1_c',
      'join_key_lhs' => 'rrd_regula67cense_doc_ida',
      'join_key_rhs' => 'rrd_regulac86besponse_idb',
    ),
  ),
  'table' => 'rrd_regulatory_response_doc_rr_regulatory_response_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'rrd_regula67cense_doc_ida' => 
    array (
      'name' => 'rrd_regula67cense_doc_ida',
      'type' => 'id',
    ),
    'rrd_regulac86besponse_idb' => 
    array (
      'name' => 'rrd_regulac86besponse_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_rrd_regulatory_response_doc_rr_regulatory_response_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_rrd_regulatory_response_doc_rr_regulatory_response_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'rrd_regula67cense_doc_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_rrd_regulatory_response_doc_rr_regulatory_response_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'rrd_regulac86besponse_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'rrd_regulatory_response_doc_rr_regulatory_response_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'rrd_regula67cense_doc_ida',
        1 => 'rrd_regulac86besponse_idb',
      ),
    ),
  ),
);