<?php
// created: 2020-01-07 13:27:00
$dictionary["w_weight_m06_error_1"] = array (
  'true_relationship_type' => 'one-to-one',
  'from_studio' => true,
  'relationships' => 
  array (
    'w_weight_m06_error_1' => 
    array (
      'lhs_module' => 'W_Weight',
      'lhs_table' => 'w_weight',
      'lhs_key' => 'id',
      'rhs_module' => 'M06_Error',
      'rhs_table' => 'm06_error',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'w_weight_m06_error_1_c',
      'join_key_lhs' => 'w_weight_m06_error_1w_weight_ida',
      'join_key_rhs' => 'w_weight_m06_error_1m06_error_idb',
    ),
  ),
  'table' => 'w_weight_m06_error_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'w_weight_m06_error_1w_weight_ida' => 
    array (
      'name' => 'w_weight_m06_error_1w_weight_ida',
      'type' => 'id',
    ),
    'w_weight_m06_error_1m06_error_idb' => 
    array (
      'name' => 'w_weight_m06_error_1m06_error_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_w_weight_m06_error_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_w_weight_m06_error_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'w_weight_m06_error_1w_weight_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_w_weight_m06_error_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'w_weight_m06_error_1m06_error_idb',
        1 => 'deleted',
      ),
    ),
  ),
);