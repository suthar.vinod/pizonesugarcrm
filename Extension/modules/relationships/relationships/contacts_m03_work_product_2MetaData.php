<?php
// created: 2022-07-07 07:02:30
$dictionary["contacts_m03_work_product_2"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'contacts_m03_work_product_2' => 
    array (
      'lhs_module' => 'Contacts',
      'lhs_table' => 'contacts',
      'lhs_key' => 'id',
      'rhs_module' => 'M03_Work_Product',
      'rhs_table' => 'm03_work_product',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'contacts_m03_work_product_2_c',
      'join_key_lhs' => 'contacts_m03_work_product_2contacts_ida',
      'join_key_rhs' => 'contacts_m03_work_product_2m03_work_product_idb',
    ),
  ),
  'table' => 'contacts_m03_work_product_2_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'contacts_m03_work_product_2contacts_ida' => 
    array (
      'name' => 'contacts_m03_work_product_2contacts_ida',
      'type' => 'id',
    ),
    'contacts_m03_work_product_2m03_work_product_idb' => 
    array (
      'name' => 'contacts_m03_work_product_2m03_work_product_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_contacts_m03_work_product_2_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_contacts_m03_work_product_2_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'contacts_m03_work_product_2contacts_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_contacts_m03_work_product_2_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'contacts_m03_work_product_2m03_work_product_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'contacts_m03_work_product_2_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'contacts_m03_work_product_2m03_work_product_idb',
      ),
    ),
  ),
);