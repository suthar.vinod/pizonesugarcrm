<?php
// created: 2022-04-26 06:44:22
$dictionary["bid_batch_id_m03_work_product_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'bid_batch_id_m03_work_product_1' => 
    array (
      'lhs_module' => 'BID_Batch_ID',
      'lhs_table' => 'bid_batch_id',
      'lhs_key' => 'id',
      'rhs_module' => 'M03_Work_Product',
      'rhs_table' => 'm03_work_product',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'bid_batch_id_m03_work_product_1_c',
      'join_key_lhs' => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
      'join_key_rhs' => 'bid_batch_id_m03_work_product_1m03_work_product_idb',
    ),
  ),
  'table' => 'bid_batch_id_m03_work_product_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'bid_batch_id_m03_work_product_1bid_batch_id_ida' => 
    array (
      'name' => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
      'type' => 'id',
    ),
    'bid_batch_id_m03_work_product_1m03_work_product_idb' => 
    array (
      'name' => 'bid_batch_id_m03_work_product_1m03_work_product_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_bid_batch_id_m03_work_product_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_bid_batch_id_m03_work_product_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_bid_batch_id_m03_work_product_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'bid_batch_id_m03_work_product_1m03_work_product_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'bid_batch_id_m03_work_product_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'bid_batch_id_m03_work_product_1m03_work_product_idb',
      ),
    ),
  ),
);