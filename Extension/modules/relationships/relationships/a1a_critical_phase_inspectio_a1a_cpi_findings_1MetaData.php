<?php
// created: 2017-06-07 19:03:44
$dictionary["a1a_critical_phase_inspectio_a1a_cpi_findings_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'a1a_critical_phase_inspectio_a1a_cpi_findings_1' => 
    array (
      'lhs_module' => 'A1A_Critical_Phase_Inspectio',
      'lhs_table' => 'a1a_critical_phase_inspectio',
      'lhs_key' => 'id',
      'rhs_module' => 'A1A_CPI_Findings',
      'rhs_table' => 'a1a_cpi_findings',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1_c',
      'join_key_lhs' => 'a1a_criticbf09spectio_ida',
      'join_key_rhs' => 'a1a_critic8af6indings_idb',
    ),
  ),
  'table' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'a1a_criticbf09spectio_ida' => 
    array (
      'name' => 'a1a_criticbf09spectio_ida',
      'type' => 'id',
    ),
    'a1a_critic8af6indings_idb' => 
    array (
      'name' => 'a1a_critic8af6indings_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_a1a_critical_phase_inspectio_a1a_cpi_findings_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_a1a_critical_phase_inspectio_a1a_cpi_findings_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'a1a_criticbf09spectio_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_a1a_critical_phase_inspectio_a1a_cpi_findings_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'a1a_critic8af6indings_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'a1a_critic8af6indings_idb',
      ),
    ),
  ),
);