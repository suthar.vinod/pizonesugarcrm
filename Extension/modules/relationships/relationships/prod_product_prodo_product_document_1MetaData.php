<?php
// created: 2020-09-10 08:30:08
$dictionary["prod_product_prodo_product_document_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'prod_product_prodo_product_document_1' => 
    array (
      'lhs_module' => 'Prod_Product',
      'lhs_table' => 'prod_product',
      'lhs_key' => 'id',
      'rhs_module' => 'ProDo_Product_Document',
      'rhs_table' => 'prodo_product_document',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'prod_product_prodo_product_document_1_c',
      'join_key_lhs' => 'prod_product_prodo_product_document_1prod_product_ida',
      'join_key_rhs' => 'prod_product_prodo_product_document_1prodo_product_document_idb',
    ),
  ),
  'table' => 'prod_product_prodo_product_document_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'prod_product_prodo_product_document_1prod_product_ida' => 
    array (
      'name' => 'prod_product_prodo_product_document_1prod_product_ida',
      'type' => 'id',
    ),
    'prod_product_prodo_product_document_1prodo_product_document_idb' => 
    array (
      'name' => 'prod_product_prodo_product_document_1prodo_product_document_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_prod_product_prodo_product_document_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_prod_product_prodo_product_document_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'prod_product_prodo_product_document_1prod_product_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_prod_product_prodo_product_document_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'prod_product_prodo_product_document_1prodo_product_document_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'prod_product_prodo_product_document_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'prod_product_prodo_product_document_1prodo_product_document_idb',
      ),
    ),
  ),
);