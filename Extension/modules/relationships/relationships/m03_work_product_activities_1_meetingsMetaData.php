<?php
// created: 2019-08-05 12:03:27
$dictionary["m03_work_product_activities_1_meetings"] = array (
  'relationships' => 
  array (
    'm03_work_product_activities_1_meetings' => 
    array (
      'lhs_module' => 'M03_Work_Product',
      'lhs_table' => 'm03_work_product',
      'lhs_key' => 'id',
      'rhs_module' => 'Meetings',
      'rhs_table' => 'meetings',
      'relationship_role_column_value' => 'M03_Work_Product',
      'rhs_key' => 'parent_id',
      'relationship_type' => 'one-to-many',
      'relationship_role_column' => 'parent_type',
    ),
  ),
  'fields' => '',
  'indices' => '',
  'table' => '',
);