<?php
// created: 2019-09-30 12:27:41
$dictionary["anml_animals_tsdoc_test_system_documents_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'anml_animals_tsdoc_test_system_documents_1' => 
    array (
      'lhs_module' => 'ANML_Animals',
      'lhs_table' => 'anml_animals',
      'lhs_key' => 'id',
      'rhs_module' => 'TSdoc_Test_System_Documents',
      'rhs_table' => 'tsdoc_test_system_documents',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'anml_animals_tsdoc_test_system_documents_1_c',
      'join_key_lhs' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
      'join_key_rhs' => 'anml_animad2fbcuments_idb',
    ),
  ),
  'table' => 'anml_animals_tsdoc_test_system_documents_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'anml_animals_tsdoc_test_system_documents_1anml_animals_ida' => 
    array (
      'name' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
      'type' => 'id',
    ),
    'anml_animad2fbcuments_idb' => 
    array (
      'name' => 'anml_animad2fbcuments_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_anml_animals_tsdoc_test_system_documents_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_anml_animals_tsdoc_test_system_documents_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_anml_animals_tsdoc_test_system_documents_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'anml_animad2fbcuments_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'anml_animals_tsdoc_test_system_documents_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'anml_animad2fbcuments_idb',
      ),
    ),
  ),
);