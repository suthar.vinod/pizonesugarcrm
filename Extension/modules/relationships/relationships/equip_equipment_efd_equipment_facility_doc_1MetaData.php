<?php
// created: 2019-02-19 17:46:13
$dictionary["equip_equipment_efd_equipment_facility_doc_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'equip_equipment_efd_equipment_facility_doc_1' => 
    array (
      'lhs_module' => 'Equip_Equipment',
      'lhs_table' => 'equip_equipment',
      'lhs_key' => 'id',
      'rhs_module' => 'EFD_Equipment_Facility_Doc',
      'rhs_table' => 'efd_equipment_facility_doc',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'equip_equipment_efd_equipment_facility_doc_1_c',
      'join_key_lhs' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
      'join_key_rhs' => 'equip_equiffa6ity_doc_idb',
    ),
  ),
  'table' => 'equip_equipment_efd_equipment_facility_doc_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida' => 
    array (
      'name' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
      'type' => 'id',
    ),
    'equip_equiffa6ity_doc_idb' => 
    array (
      'name' => 'equip_equiffa6ity_doc_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_equip_equipment_efd_equipment_facility_doc_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_equip_equipment_efd_equipment_facility_doc_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_equip_equipment_efd_equipment_facility_doc_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'equip_equiffa6ity_doc_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'equip_equipment_efd_equipment_facility_doc_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'equip_equiffa6ity_doc_idb',
      ),
    ),
  ),
);