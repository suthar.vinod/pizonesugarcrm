<?php
// created: 2019-02-21 20:21:31
$dictionary["m06_error_rms_room_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'm06_error_rms_room_1' => 
    array (
      'lhs_module' => 'M06_Error',
      'lhs_table' => 'm06_error',
      'lhs_key' => 'id',
      'rhs_module' => 'RMS_Room',
      'rhs_table' => 'rms_room',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'm06_error_rms_room_1_c',
      'join_key_lhs' => 'm06_error_rms_room_1m06_error_ida',
      'join_key_rhs' => 'm06_error_rms_room_1rms_room_idb',
    ),
  ),
  'table' => 'm06_error_rms_room_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'm06_error_rms_room_1m06_error_ida' => 
    array (
      'name' => 'm06_error_rms_room_1m06_error_ida',
      'type' => 'id',
    ),
    'm06_error_rms_room_1rms_room_idb' => 
    array (
      'name' => 'm06_error_rms_room_1rms_room_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_m06_error_rms_room_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_m06_error_rms_room_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm06_error_rms_room_1m06_error_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_m06_error_rms_room_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm06_error_rms_room_1rms_room_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'm06_error_rms_room_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'm06_error_rms_room_1m06_error_ida',
        1 => 'm06_error_rms_room_1rms_room_idb',
      ),
    ),
  ),
);