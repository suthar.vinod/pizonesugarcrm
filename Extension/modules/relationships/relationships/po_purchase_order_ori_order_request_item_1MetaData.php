<?php
// created: 2021-10-19 09:25:42
$dictionary["po_purchase_order_ori_order_request_item_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'po_purchase_order_ori_order_request_item_1' => 
    array (
      'lhs_module' => 'PO_Purchase_Order',
      'lhs_table' => 'po_purchase_order',
      'lhs_key' => 'id',
      'rhs_module' => 'ORI_Order_Request_Item',
      'rhs_table' => 'ori_order_request_item',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'po_purchase_order_ori_order_request_item_1_c',
      'join_key_lhs' => 'po_purchase_order_ori_order_request_item_1po_purchase_order_ida',
      'join_key_rhs' => 'po_purchasdbbcst_item_idb',
    ),
  ),
  'table' => 'po_purchase_order_ori_order_request_item_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'po_purchase_order_ori_order_request_item_1po_purchase_order_ida' => 
    array (
      'name' => 'po_purchase_order_ori_order_request_item_1po_purchase_order_ida',
      'type' => 'id',
    ),
    'po_purchasdbbcst_item_idb' => 
    array (
      'name' => 'po_purchasdbbcst_item_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_po_purchase_order_ori_order_request_item_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_po_purchase_order_ori_order_request_item_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'po_purchase_order_ori_order_request_item_1po_purchase_order_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_po_purchase_order_ori_order_request_item_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'po_purchasdbbcst_item_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'po_purchase_order_ori_order_request_item_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'po_purchasdbbcst_item_idb',
      ),
    ),
  ),
);