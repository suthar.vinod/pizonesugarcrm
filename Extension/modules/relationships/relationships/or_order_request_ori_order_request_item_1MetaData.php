<?php
// created: 2021-10-19 09:18:33
$dictionary["or_order_request_ori_order_request_item_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'or_order_request_ori_order_request_item_1' => 
    array (
      'lhs_module' => 'OR_Order_Request',
      'lhs_table' => 'or_order_request',
      'lhs_key' => 'id',
      'rhs_module' => 'ORI_Order_Request_Item',
      'rhs_table' => 'ori_order_request_item',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'or_order_request_ori_order_request_item_1_c',
      'join_key_lhs' => 'or_order_request_ori_order_request_item_1or_order_request_ida',
      'join_key_rhs' => 'or_order_r2609st_item_idb',
    ),
  ),
  'table' => 'or_order_request_ori_order_request_item_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'or_order_request_ori_order_request_item_1or_order_request_ida' => 
    array (
      'name' => 'or_order_request_ori_order_request_item_1or_order_request_ida',
      'type' => 'id',
    ),
    'or_order_r2609st_item_idb' => 
    array (
      'name' => 'or_order_r2609st_item_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_or_order_request_ori_order_request_item_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_or_order_request_ori_order_request_item_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'or_order_request_ori_order_request_item_1or_order_request_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_or_order_request_ori_order_request_item_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'or_order_r2609st_item_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'or_order_request_ori_order_request_item_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'or_order_r2609st_item_idb',
      ),
    ),
  ),
);