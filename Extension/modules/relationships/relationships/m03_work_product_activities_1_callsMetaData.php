<?php
// created: 2019-08-05 12:02:55
$dictionary["m03_work_product_activities_1_calls"] = array (
  'relationships' => 
  array (
    'm03_work_product_activities_1_calls' => 
    array (
      'lhs_module' => 'M03_Work_Product',
      'lhs_table' => 'm03_work_product',
      'lhs_key' => 'id',
      'rhs_module' => 'Calls',
      'rhs_table' => 'calls',
      'relationship_role_column_value' => 'M03_Work_Product',
      'rhs_key' => 'parent_id',
      'relationship_type' => 'one-to-many',
      'relationship_role_column' => 'parent_type',
    ),
  ),
  'fields' => '',
  'indices' => '',
  'table' => '',
);