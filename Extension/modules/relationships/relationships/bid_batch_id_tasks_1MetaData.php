<?php
// created: 2022-09-22 05:31:46
$dictionary["bid_batch_id_tasks_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'bid_batch_id_tasks_1' => 
    array (
      'lhs_module' => 'BID_Batch_ID',
      'lhs_table' => 'bid_batch_id',
      'lhs_key' => 'id',
      'rhs_module' => 'Tasks',
      'rhs_table' => 'tasks',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'bid_batch_id_tasks_1_c',
      'join_key_lhs' => 'bid_batch_id_tasks_1bid_batch_id_ida',
      'join_key_rhs' => 'bid_batch_id_tasks_1tasks_idb',
    ),
  ),
  'table' => 'bid_batch_id_tasks_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'bid_batch_id_tasks_1bid_batch_id_ida' => 
    array (
      'name' => 'bid_batch_id_tasks_1bid_batch_id_ida',
      'type' => 'id',
    ),
    'bid_batch_id_tasks_1tasks_idb' => 
    array (
      'name' => 'bid_batch_id_tasks_1tasks_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_bid_batch_id_tasks_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_bid_batch_id_tasks_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'bid_batch_id_tasks_1bid_batch_id_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_bid_batch_id_tasks_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'bid_batch_id_tasks_1tasks_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'bid_batch_id_tasks_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'bid_batch_id_tasks_1tasks_idb',
      ),
    ),
  ),
);