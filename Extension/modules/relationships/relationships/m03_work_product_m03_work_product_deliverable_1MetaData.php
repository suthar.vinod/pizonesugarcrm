<?php
// created: 2017-08-29 00:56:43
$dictionary['m03_work_product_m03_work_product_deliverable_1'] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'm03_work_product_m03_work_product_deliverable_1' => 
    array (
      'lhs_module' => 'M03_Work_Product',
      'lhs_table' => 'm03_work_product',
      'lhs_key' => 'id',
      'rhs_module' => 'M03_Work_Product_Deliverable',
      'rhs_table' => 'm03_work_product_deliverable',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'm03_work_product_m03_work_product_deliverable_1_c',
      'join_key_lhs' => 'm03_work_p0b66product_ida',
      'join_key_rhs' => 'm03_work_pe584verable_idb',
    ),
  ),
  'table' => 'm03_work_product_m03_work_product_deliverable_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    'm03_work_p0b66product_ida' => 
    array (
      'name' => 'm03_work_p0b66product_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    'm03_work_pe584verable_idb' => 
    array (
      'name' => 'm03_work_pe584verable_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'm03_work_product_m03_work_product_deliverable_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'm03_work_product_m03_work_product_deliverable_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm03_work_p0b66product_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'm03_work_product_m03_work_product_deliverable_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'm03_work_pe584verable_idb',
      ),
    ),
  ),
);