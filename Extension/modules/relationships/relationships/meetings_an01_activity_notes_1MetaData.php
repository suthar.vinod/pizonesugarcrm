<?php
// created: 2017-05-11 22:18:50
$dictionary["meetings_an01_activity_notes_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'meetings_an01_activity_notes_1' => 
    array (
      'lhs_module' => 'Meetings',
      'lhs_table' => 'meetings',
      'lhs_key' => 'id',
      'rhs_module' => 'AN01_Activity_Notes',
      'rhs_table' => 'an01_activity_notes',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'meetings_an01_activity_notes_1_c',
      'join_key_lhs' => 'meetings_an01_activity_notes_1meetings_ida',
      'join_key_rhs' => 'meetings_an01_activity_notes_1an01_activity_notes_idb',
    ),
  ),
  'table' => 'meetings_an01_activity_notes_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'meetings_an01_activity_notes_1meetings_ida' => 
    array (
      'name' => 'meetings_an01_activity_notes_1meetings_ida',
      'type' => 'id',
    ),
    'meetings_an01_activity_notes_1an01_activity_notes_idb' => 
    array (
      'name' => 'meetings_an01_activity_notes_1an01_activity_notes_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_meetings_an01_activity_notes_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_meetings_an01_activity_notes_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'meetings_an01_activity_notes_1meetings_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_meetings_an01_activity_notes_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'meetings_an01_activity_notes_1an01_activity_notes_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'meetings_an01_activity_notes_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'meetings_an01_activity_notes_1an01_activity_notes_idb',
      ),
    ),
  ),
);