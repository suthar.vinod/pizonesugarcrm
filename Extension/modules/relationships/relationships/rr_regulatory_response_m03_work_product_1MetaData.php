<?php
// created: 2019-07-09 11:53:38
$dictionary["rr_regulatory_response_m03_work_product_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'rr_regulatory_response_m03_work_product_1' => 
    array (
      'lhs_module' => 'RR_Regulatory_Response',
      'lhs_table' => 'rr_regulatory_response',
      'lhs_key' => 'id',
      'rhs_module' => 'M03_Work_Product',
      'rhs_table' => 'm03_work_product',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'rr_regulatory_response_m03_work_product_1_c',
      'join_key_lhs' => 'rr_regulat4120esponse_ida',
      'join_key_rhs' => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
    ),
  ),
  'table' => 'rr_regulatory_response_m03_work_product_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'rr_regulat4120esponse_ida' => 
    array (
      'name' => 'rr_regulat4120esponse_ida',
      'type' => 'id',
    ),
    'rr_regulatory_response_m03_work_product_1m03_work_product_idb' => 
    array (
      'name' => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_rr_regulatory_response_m03_work_product_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_rr_regulatory_response_m03_work_product_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'rr_regulat4120esponse_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_rr_regulatory_response_m03_work_product_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'rr_regulatory_response_m03_work_product_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'rr_regulat4120esponse_ida',
        1 => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
      ),
    ),
  ),
);