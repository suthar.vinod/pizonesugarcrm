<?php
// created: 2021-11-09 09:43:46
$dictionary["ic_inventory_collection_ii_inventory_item_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'ic_inventory_collection_ii_inventory_item_1' => 
    array (
      'lhs_module' => 'IC_Inventory_Collection',
      'lhs_table' => 'ic_inventory_collection',
      'lhs_key' => 'id',
      'rhs_module' => 'II_Inventory_Item',
      'rhs_table' => 'ii_inventory_item',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'ic_inventory_collection_ii_inventory_item_1_c',
      'join_key_lhs' => 'ic_invento128flection_ida',
      'join_key_rhs' => 'ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb',
    ),
  ),
  'table' => 'ic_inventory_collection_ii_inventory_item_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'ic_invento128flection_ida' => 
    array (
      'name' => 'ic_invento128flection_ida',
      'type' => 'id',
    ),
    'ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb' => 
    array (
      'name' => 'ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_ic_inventory_collection_ii_inventory_item_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_ic_inventory_collection_ii_inventory_item_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ic_invento128flection_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_ic_inventory_collection_ii_inventory_item_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'ic_inventory_collection_ii_inventory_item_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb',
      ),
    ),
  ),
);