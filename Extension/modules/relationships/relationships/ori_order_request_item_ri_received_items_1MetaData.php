<?php
// created: 2021-10-19 09:15:00
$dictionary["ori_order_request_item_ri_received_items_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'ori_order_request_item_ri_received_items_1' => 
    array (
      'lhs_module' => 'ORI_Order_Request_Item',
      'lhs_table' => 'ori_order_request_item',
      'lhs_key' => 'id',
      'rhs_module' => 'RI_Received_Items',
      'rhs_table' => 'ri_received_items',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'ori_order_request_item_ri_received_items_1_c',
      'join_key_lhs' => 'ori_order_6b32st_item_ida',
      'join_key_rhs' => 'ori_order_request_item_ri_received_items_1ri_received_items_idb',
    ),
  ),
  'table' => 'ori_order_request_item_ri_received_items_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'ori_order_6b32st_item_ida' => 
    array (
      'name' => 'ori_order_6b32st_item_ida',
      'type' => 'id',
    ),
    'ori_order_request_item_ri_received_items_1ri_received_items_idb' => 
    array (
      'name' => 'ori_order_request_item_ri_received_items_1ri_received_items_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_ori_order_request_item_ri_received_items_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_ori_order_request_item_ri_received_items_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ori_order_6b32st_item_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_ori_order_request_item_ri_received_items_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ori_order_request_item_ri_received_items_1ri_received_items_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'ori_order_request_item_ri_received_items_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'ori_order_request_item_ri_received_items_1ri_received_items_idb',
      ),
    ),
  ),
);