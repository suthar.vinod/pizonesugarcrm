<?php
// created: 2017-09-12 15:06:13
$dictionary["wpe_work_product_enrollment_wpe_work_product_enrollment_1"] = array (
  'true_relationship_type' => 'one-to-one',
  'from_studio' => true,
  'relationships' => 
  array (
    'wpe_work_product_enrollment_wpe_work_product_enrollment_1' => 
    array (
      'lhs_module' => 'WPE_Work_Product_Enrollment',
      'lhs_table' => 'wpe_work_product_enrollment',
      'lhs_key' => 'id',
      'rhs_module' => 'WPE_Work_Product_Enrollment',
      'rhs_table' => 'wpe_work_product_enrollment',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1_c',
      'join_key_lhs' => 'wpe_work_pd83eollment_ida',
      'join_key_rhs' => 'wpe_work_pab8dollment_idb',
    ),
  ),
  'table' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'wpe_work_pd83eollment_ida' => 
    array (
      'name' => 'wpe_work_pd83eollment_ida',
      'type' => 'id',
    ),
    'wpe_work_pab8dollment_idb' => 
    array (
      'name' => 'wpe_work_pab8dollment_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_wpe_work_product_enrollment_wpe_work_product_enrollment_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_wpe_work_product_enrollment_wpe_work_product_enrollment_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'wpe_work_pd83eollment_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_wpe_work_product_enrollment_wpe_work_product_enrollment_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'wpe_work_pab8dollment_idb',
        1 => 'deleted',
      ),
    ),
  ),
);