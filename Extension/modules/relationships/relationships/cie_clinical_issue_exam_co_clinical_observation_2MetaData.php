<?php
// created: 2020-01-07 13:50:42
$dictionary["cie_clinical_issue_exam_co_clinical_observation_2"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'cie_clinical_issue_exam_co_clinical_observation_2' => 
    array (
      'lhs_module' => 'CIE_Clinical_Issue_Exam',
      'lhs_table' => 'cie_clinical_issue_exam',
      'lhs_key' => 'id',
      'rhs_module' => 'CO_Clinical_Observation',
      'rhs_table' => 'co_clinical_observation',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cie_clinical_issue_exam_co_clinical_observation_2_c',
      'join_key_lhs' => 'cie_clinicddcbue_exam_ida',
      'join_key_rhs' => 'cie_clinic87bcrvation_idb',
    ),
  ),
  'table' => 'cie_clinical_issue_exam_co_clinical_observation_2_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'cie_clinicddcbue_exam_ida' => 
    array (
      'name' => 'cie_clinicddcbue_exam_ida',
      'type' => 'id',
    ),
    'cie_clinic87bcrvation_idb' => 
    array (
      'name' => 'cie_clinic87bcrvation_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_cie_clinical_issue_exam_co_clinical_observation_2_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_cie_clinical_issue_exam_co_clinical_observation_2_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cie_clinicddcbue_exam_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_cie_clinical_issue_exam_co_clinical_observation_2_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cie_clinic87bcrvation_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'cie_clinical_issue_exam_co_clinical_observation_2_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'cie_clinic87bcrvation_idb',
      ),
    ),
  ),
);