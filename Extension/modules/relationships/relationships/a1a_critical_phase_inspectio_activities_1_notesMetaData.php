<?php
// created: 2017-09-13 15:16:04
$dictionary["a1a_critical_phase_inspectio_activities_1_notes"] = array (
  'relationships' => 
  array (
    'a1a_critical_phase_inspectio_activities_1_notes' => 
    array (
      'lhs_module' => 'A1A_Critical_Phase_Inspectio',
      'lhs_table' => 'a1a_critical_phase_inspectio',
      'lhs_key' => 'id',
      'rhs_module' => 'Notes',
      'rhs_table' => 'notes',
      'relationship_role_column_value' => 'A1A_Critical_Phase_Inspectio',
      'rhs_key' => 'parent_id',
      'relationship_type' => 'one-to-many',
      'relationship_role_column' => 'parent_type',
    ),
  ),
  'fields' => '',
  'indices' => '',
  'table' => '',
);