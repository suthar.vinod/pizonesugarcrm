<?php
// created: 2021-08-14 11:12:04
$dictionary["gd_group_design_wpe_work_product_enrollment_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'gd_group_design_wpe_work_product_enrollment_1' => 
    array (
      'lhs_module' => 'GD_Group_Design',
      'lhs_table' => 'gd_group_design',
      'lhs_key' => 'id',
      'rhs_module' => 'WPE_Work_Product_Enrollment',
      'rhs_table' => 'wpe_work_product_enrollment',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'gd_group_design_wpe_work_product_enrollment_1_c',
      'join_key_lhs' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
      'join_key_rhs' => 'gd_group_daf65ollment_idb',
    ),
  ),
  'table' => 'gd_group_design_wpe_work_product_enrollment_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida' => 
    array (
      'name' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
      'type' => 'id',
    ),
    'gd_group_daf65ollment_idb' => 
    array (
      'name' => 'gd_group_daf65ollment_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_gd_group_design_wpe_work_product_enrollment_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_gd_group_design_wpe_work_product_enrollment_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_gd_group_design_wpe_work_product_enrollment_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'gd_group_daf65ollment_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'gd_group_design_wpe_work_product_enrollment_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'gd_group_daf65ollment_idb',
      ),
    ),
  ),
);