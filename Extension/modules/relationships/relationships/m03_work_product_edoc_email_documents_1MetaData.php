<?php
// created: 2019-08-27 11:37:03
$dictionary["m03_work_product_edoc_email_documents_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'm03_work_product_edoc_email_documents_1' => 
    array (
      'lhs_module' => 'M03_Work_Product',
      'lhs_table' => 'm03_work_product',
      'lhs_key' => 'id',
      'rhs_module' => 'EDoc_Email_Documents',
      'rhs_table' => 'edoc_email_documents',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'm03_work_product_edoc_email_documents_1_c',
      'join_key_lhs' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
      'join_key_rhs' => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
    ),
  ),
  'table' => 'm03_work_product_edoc_email_documents_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'm03_work_product_edoc_email_documents_1m03_work_product_ida' => 
    array (
      'name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
      'type' => 'id',
    ),
    'm03_work_product_edoc_email_documents_1edoc_email_documents_idb' => 
    array (
      'name' => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_m03_work_product_edoc_email_documents_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_m03_work_product_edoc_email_documents_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_m03_work_product_edoc_email_documents_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'm03_work_product_edoc_email_documents_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
      ),
    ),
  ),
);