<?php
 // created: 2021-02-25 08:41:49
$layout_defs["Accounts"]["subpanel_setup"]['accounts_an_account_number_1'] = array (
  'order' => 100,
  'module' => 'AN_Account_Number',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_AN_ACCOUNT_NUMBER_TITLE',
  'get_subpanel_data' => 'accounts_an_account_number_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
