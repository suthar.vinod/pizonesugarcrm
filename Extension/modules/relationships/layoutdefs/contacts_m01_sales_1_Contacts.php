<?php
 // created: 2018-12-13 16:29:00
$layout_defs["Contacts"]["subpanel_setup"]['contacts_m01_sales_1'] = array (
  'order' => 100,
  'module' => 'M01_Sales',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_M01_SALES_1_FROM_M01_SALES_TITLE',
  'get_subpanel_data' => 'contacts_m01_sales_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
