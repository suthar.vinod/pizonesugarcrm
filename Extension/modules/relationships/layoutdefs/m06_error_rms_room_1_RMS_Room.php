<?php
 // created: 2019-02-21 20:21:31
$layout_defs["RMS_Room"]["subpanel_setup"]['m06_error_rms_room_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M06_ERROR_RMS_ROOM_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'm06_error_rms_room_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
