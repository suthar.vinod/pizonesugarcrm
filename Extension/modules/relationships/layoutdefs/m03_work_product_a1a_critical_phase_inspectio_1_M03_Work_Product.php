<?php
 // created: 2017-06-07 19:01:24
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_a1a_critical_phase_inspectio_1'] = array (
  'order' => 100,
  'module' => 'A1A_Critical_Phase_Inspectio',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'get_subpanel_data' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
