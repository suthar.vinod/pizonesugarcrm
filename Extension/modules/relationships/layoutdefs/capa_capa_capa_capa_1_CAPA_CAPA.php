<?php
 // created: 2022-02-03 07:32:44
$layout_defs["CAPA_CAPA"]["subpanel_setup"]['capa_capa_capa_capa_1'] = array (
  'order' => 100,
  'module' => 'CAPA_CAPA',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_R_TITLE',
  'get_subpanel_data' => 'capa_capa_capa_capa_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
