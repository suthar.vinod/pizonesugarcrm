<?php
 // created: 2019-02-20 13:19:54
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_rt_room_transfer_1'] = array (
  'order' => 100,
  'module' => 'RT_Room_Transfer',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_RT_ROOM_TRANSFER_TITLE',
  'get_subpanel_data' => 'anml_animals_rt_room_transfer_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
