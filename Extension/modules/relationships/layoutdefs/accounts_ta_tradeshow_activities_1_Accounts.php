<?php
 // created: 2018-04-23 17:12:12
$layout_defs["Accounts"]["subpanel_setup"]['accounts_ta_tradeshow_activities_1'] = array (
  'order' => 100,
  'module' => 'TA_Tradeshow_Activities',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'get_subpanel_data' => 'accounts_ta_tradeshow_activities_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
