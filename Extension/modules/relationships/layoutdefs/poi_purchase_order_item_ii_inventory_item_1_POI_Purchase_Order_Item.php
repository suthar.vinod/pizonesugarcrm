<?php
 // created: 2021-11-09 09:48:27
$layout_defs["POI_Purchase_Order_Item"]["subpanel_setup"]['poi_purchase_order_item_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'poi_purchase_order_item_ii_inventory_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
