<?php
 // created: 2019-02-19 17:46:13
$layout_defs["Equip_Equipment"]["subpanel_setup"]['equip_equipment_efd_equipment_facility_doc_1'] = array (
  'order' => 100,
  'module' => 'EFD_Equipment_Facility_Doc',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EFD_EQUIPMENT_FACILITY_DOC_TITLE',
  'get_subpanel_data' => 'equip_equipment_efd_equipment_facility_doc_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
