<?php
 // created: 2018-02-14 15:35:52
$layout_defs["Accounts"]["subpanel_setup"]['accounts_ac01_acquired_companies_1'] = array (
  'order' => 100,
  'module' => 'AC01_Acquired_Companies',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_AC01_ACQUIRED_COMPANIES_TITLE',
  'get_subpanel_data' => 'accounts_ac01_acquired_companies_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
