<?php
 // created: 2022-09-22 05:28:30
$layout_defs["BID_Batch_ID"]["subpanel_setup"]['bid_batch_id_gd_group_design_1'] = array (
  'order' => 100,
  'module' => 'GD_Group_Design',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'get_subpanel_data' => 'bid_batch_id_gd_group_design_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
