<?php
 // created: 2021-11-09 10:07:50
$layout_defs["II_Inventory_Item"]["subpanel_setup"]['m03_work_product_ii_inventory_item_2'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'm03_work_product_ii_inventory_item_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
