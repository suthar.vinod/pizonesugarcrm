<?php
 // created: 2019-09-26 14:13:30
$layout_defs["Accounts"]["subpanel_setup"]['accounts_cn_company_name_1'] = array (
  'order' => 100,
  'module' => 'CN_Company_Name',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_CN_COMPANY_NAME_1_FROM_CN_COMPANY_NAME_TITLE',
  'get_subpanel_data' => 'accounts_cn_company_name_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
