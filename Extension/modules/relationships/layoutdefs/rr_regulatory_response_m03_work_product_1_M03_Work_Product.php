<?php
 // created: 2019-07-09 11:53:38
$layout_defs["M03_Work_Product"]["subpanel_setup"]['rr_regulatory_response_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'RR_Regulatory_Response',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'get_subpanel_data' => 'rr_regulatory_response_m03_work_product_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
