<?php
 // created: 2018-04-23 17:10:34
$layout_defs["TM_Tradeshow_Management"]["subpanel_setup"]['tm_tradeshow_management_ta_tradeshow_activities_1'] = array (
  'order' => 100,
  'module' => 'TA_Tradeshow_Activities',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'get_subpanel_data' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
