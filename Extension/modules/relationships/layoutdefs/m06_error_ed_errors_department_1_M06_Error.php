<?php
 // created: 2018-02-22 13:58:39
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_ed_errors_department_1'] = array (
  'order' => 100,
  'module' => 'ED_Errors_Department',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_ED_ERRORS_DEPARTMENT_TITLE',
  'get_subpanel_data' => 'm06_error_ed_errors_department_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
