<?php
 // created: 2016-02-01 20:37:08
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_m03_work_product_deliverable_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product_Deliverable',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'get_subpanel_data' => 'm03_work_product_m03_work_product_deliverable_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
  ),
);
