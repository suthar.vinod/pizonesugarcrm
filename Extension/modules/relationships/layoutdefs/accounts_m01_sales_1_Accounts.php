<?php
 // created: 2016-02-15 18:01:52
$layout_defs["Accounts"]["subpanel_setup"]['accounts_m01_sales_1'] = array (
  'order' => 100,
  'module' => 'M01_Sales',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE',
  'get_subpanel_data' => 'accounts_m01_sales_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
  ),
);
