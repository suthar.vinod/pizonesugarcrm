<?php
 // created: 2021-08-14 06:08:31
$layout_defs["TaskD_Task_Design"]["subpanel_setup"]['taskd_task_design_taskd_task_design_1'] = array (
  'order' => 100,
  'module' => 'TaskD_Task_Design',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_R_TITLE',
  'get_subpanel_data' => 'taskd_task_design_taskd_task_design_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
