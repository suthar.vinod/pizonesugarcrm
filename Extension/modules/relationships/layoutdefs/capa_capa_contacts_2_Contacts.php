<?php
 // created: 2022-02-03 07:36:41
$layout_defs["Contacts"]["subpanel_setup"]['capa_capa_contacts_2'] = array (
  'order' => 100,
  'module' => 'CAPA_CAPA',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAPA_CAPA_CONTACTS_2_FROM_CAPA_CAPA_TITLE',
  'get_subpanel_data' => 'capa_capa_contacts_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
