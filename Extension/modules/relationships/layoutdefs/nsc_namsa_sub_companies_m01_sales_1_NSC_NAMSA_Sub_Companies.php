<?php
 // created: 2021-12-07 12:33:38
$layout_defs["NSC_NAMSA_Sub_Companies"]["subpanel_setup"]['nsc_namsa_sub_companies_m01_sales_1'] = array (
  'order' => 100,
  'module' => 'M01_Sales',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_M01_SALES_TITLE',
  'get_subpanel_data' => 'nsc_namsa_sub_companies_m01_sales_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
