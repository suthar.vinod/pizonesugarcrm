<?php
 // created: 2019-02-25 15:17:34
$layout_defs["Equip_Equipment"]["subpanel_setup"]['equip_equipment_efs_equipment_facility_servi_1'] = array (
  'order' => 100,
  'module' => 'EFS_Equipment_Facility_Servi',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE',
  'get_subpanel_data' => 'equip_equipment_efs_equipment_facility_servi_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
