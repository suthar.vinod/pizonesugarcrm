<?php
 // created: 2018-06-01 21:24:07
$layout_defs["M06_Error"]["subpanel_setup"]['erqc_error_qc_employees_m06_error_1'] = array (
  'order' => 100,
  'module' => 'ErQC_Error_QC_Employees',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_ERQC_ERROR_QC_EMPLOYEES_TITLE',
  'get_subpanel_data' => 'erqc_error_qc_employees_m06_error_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
