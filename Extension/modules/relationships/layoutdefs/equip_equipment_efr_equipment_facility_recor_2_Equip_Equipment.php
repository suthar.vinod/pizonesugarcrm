<?php
 // created: 2019-02-20 15:09:02
$layout_defs["Equip_Equipment"]["subpanel_setup"]['equip_equipment_efr_equipment_facility_recor_2'] = array (
  'order' => 100,
  'module' => 'EFR_Equipment_Facility_Recor',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE',
  'get_subpanel_data' => 'equip_equipment_efr_equipment_facility_recor_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
