<?php
 // created: 2019-02-20 19:46:48
$layout_defs["Contacts"]["subpanel_setup"]['contacts_de_deviation_employees_1'] = array (
  'order' => 100,
  'module' => 'DE_Deviation_Employees',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'get_subpanel_data' => 'contacts_de_deviation_employees_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
