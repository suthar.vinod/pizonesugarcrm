<?php
 // created: 2021-08-14 11:12:04
$layout_defs["GD_Group_Design"]["subpanel_setup"]['gd_group_design_wpe_work_product_enrollment_1'] = array (
  'order' => 100,
  'module' => 'WPE_Work_Product_Enrollment',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'get_subpanel_data' => 'gd_group_design_wpe_work_product_enrollment_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
