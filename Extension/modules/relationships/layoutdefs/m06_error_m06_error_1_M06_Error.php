<?php
 // created: 2020-07-07 08:16:55
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_m06_error_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_R_TITLE',
  'get_subpanel_data' => 'm06_error_m06_error_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
