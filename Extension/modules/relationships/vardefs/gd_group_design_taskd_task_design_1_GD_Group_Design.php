<?php
// created: 2021-08-14 05:50:46
$dictionary["GD_Group_Design"]["fields"]["gd_group_design_taskd_task_design_1"] = array (
  'name' => 'gd_group_design_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'gd_group_design_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'vname' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'id_name' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
  'link-type' => 'many',
  'side' => 'left',
);
