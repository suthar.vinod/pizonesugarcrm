<?php
// created: 2022-02-03 07:36:41
$dictionary["Contact"]["fields"]["capa_capa_contacts_2"] = array (
  'name' => 'capa_capa_contacts_2',
  'type' => 'link',
  'relationship' => 'capa_capa_contacts_2',
  'source' => 'non-db',
  'module' => 'CAPA_CAPA',
  'bean_name' => 'CAPA_CAPA',
  'vname' => 'LBL_CAPA_CAPA_CONTACTS_2_FROM_CAPA_CAPA_TITLE',
  'id_name' => 'capa_capa_contacts_2capa_capa_ida',
);
