<?php
// created: 2021-06-29 08:02:02
$dictionary["CDU_CD_Utilization"]["fields"]["erd_error_documents_cdu_cd_utilization_1"] = array (
  'name' => 'erd_error_documents_cdu_cd_utilization_1',
  'type' => 'link',
  'relationship' => 'erd_error_documents_cdu_cd_utilization_1',
  'source' => 'non-db',
  'module' => 'Erd_Error_Documents',
  'bean_name' => 'Erd_Error_Documents',
  'side' => 'right',
  'vname' => 'LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_CDU_CD_UTILIZATION_TITLE',
  'id_name' => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
  'link-type' => 'one',
);
$dictionary["CDU_CD_Utilization"]["fields"]["erd_error_documents_cdu_cd_utilization_1_name"] = array (
  'name' => 'erd_error_documents_cdu_cd_utilization_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'save' => true,
  'id_name' => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
  'link' => 'erd_error_documents_cdu_cd_utilization_1',
  'table' => 'erd_error_documents',
  'module' => 'Erd_Error_Documents',
  'rname' => 'name',
);
$dictionary["CDU_CD_Utilization"]["fields"]["erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida"] = array (
  'name' => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_CDU_CD_UTILIZATION_TITLE_ID',
  'id_name' => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
  'link' => 'erd_error_documents_cdu_cd_utilization_1',
  'table' => 'erd_error_documents',
  'module' => 'Erd_Error_Documents',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
