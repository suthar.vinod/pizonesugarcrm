<?php
// created: 2021-02-25 08:41:49
$dictionary["AN_Account_Number"]["fields"]["accounts_an_account_number_1"] = array (
  'name' => 'accounts_an_account_number_1',
  'type' => 'link',
  'relationship' => 'accounts_an_account_number_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_AN_ACCOUNT_NUMBER_TITLE',
  'id_name' => 'accounts_an_account_number_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["AN_Account_Number"]["fields"]["accounts_an_account_number_1_name"] = array (
  'name' => 'accounts_an_account_number_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_an_account_number_1accounts_ida',
  'link' => 'accounts_an_account_number_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["AN_Account_Number"]["fields"]["accounts_an_account_number_1accounts_ida"] = array (
  'name' => 'accounts_an_account_number_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_AN_ACCOUNT_NUMBER_TITLE_ID',
  'id_name' => 'accounts_an_account_number_1accounts_ida',
  'link' => 'accounts_an_account_number_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
