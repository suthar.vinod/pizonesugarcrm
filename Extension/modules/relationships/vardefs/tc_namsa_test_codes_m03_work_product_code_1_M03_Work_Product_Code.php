<?php
// created: 2021-12-07 12:00:27
$dictionary["M03_Work_Product_Code"]["fields"]["tc_namsa_test_codes_m03_work_product_code_1"] = array (
  'name' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'type' => 'link',
  'relationship' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'source' => 'non-db',
  'module' => 'TC_NAMSA_Test_Codes',
  'bean_name' => 'TC_NAMSA_Test_Codes',
  'side' => 'right',
  'vname' => 'LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'id_name' => 'tc_namsa_t49d3t_codes_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product_Code"]["fields"]["tc_namsa_test_codes_m03_work_product_code_1_name"] = array (
  'name' => 'tc_namsa_test_codes_m03_work_product_code_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_TC_NAMSA_TEST_CODES_TITLE',
  'save' => true,
  'id_name' => 'tc_namsa_t49d3t_codes_ida',
  'link' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'table' => 'tc_namsa_test_codes',
  'module' => 'TC_NAMSA_Test_Codes',
  'rname' => 'name',
);
$dictionary["M03_Work_Product_Code"]["fields"]["tc_namsa_t49d3t_codes_ida"] = array (
  'name' => 'tc_namsa_t49d3t_codes_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE_ID',
  'id_name' => 'tc_namsa_t49d3t_codes_ida',
  'link' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'table' => 'tc_namsa_test_codes',
  'module' => 'TC_NAMSA_Test_Codes',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
