<?php
// created: 2021-11-09 10:07:50
$dictionary["II_Inventory_Item"]["fields"]["m03_work_product_ii_inventory_item_2"] = array (
  'name' => 'm03_work_product_ii_inventory_item_2',
  'type' => 'link',
  'relationship' => 'm03_work_product_ii_inventory_item_2',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_ii_inventory_item_2m03_work_product_ida',
);
