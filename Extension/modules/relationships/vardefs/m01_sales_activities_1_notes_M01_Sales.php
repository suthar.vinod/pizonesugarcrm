<?php
// created: 2018-12-10 23:02:35
$dictionary["M01_Sales"]["fields"]["m01_sales_activities_1_notes"] = array (
  'name' => 'm01_sales_activities_1_notes',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_notes',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_NOTES_FROM_NOTES_TITLE',
);
