<?php
// created: 2019-02-25 15:17:34
$dictionary["EFS_Equipment_Facility_Servi"]["fields"]["equip_equipment_efs_equipment_facility_servi_1"] = array (
  'name' => 'equip_equipment_efs_equipment_facility_servi_1',
  'type' => 'link',
  'relationship' => 'equip_equipment_efs_equipment_facility_servi_1',
  'source' => 'non-db',
  'module' => 'Equip_Equipment',
  'bean_name' => 'Equip_Equipment',
  'side' => 'right',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE',
  'id_name' => 'equip_equia9d9uipment_ida',
  'link-type' => 'one',
);
$dictionary["EFS_Equipment_Facility_Servi"]["fields"]["equip_equipment_efs_equipment_facility_servi_1_name"] = array (
  'name' => 'equip_equipment_efs_equipment_facility_servi_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'save' => true,
  'id_name' => 'equip_equia9d9uipment_ida',
  'link' => 'equip_equipment_efs_equipment_facility_servi_1',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'name',
);
$dictionary["EFS_Equipment_Facility_Servi"]["fields"]["equip_equia9d9uipment_ida"] = array (
  'name' => 'equip_equia9d9uipment_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE_ID',
  'id_name' => 'equip_equia9d9uipment_ida',
  'link' => 'equip_equipment_efs_equipment_facility_servi_1',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
