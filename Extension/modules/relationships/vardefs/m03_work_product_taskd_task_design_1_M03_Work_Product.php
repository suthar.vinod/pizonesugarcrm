<?php
// created: 2021-08-14 06:01:53
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_taskd_task_design_1"] = array (
  'name' => 'm03_work_product_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'vname' => 'LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
