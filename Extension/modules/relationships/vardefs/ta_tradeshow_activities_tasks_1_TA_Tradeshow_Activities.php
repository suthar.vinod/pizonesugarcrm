<?php
// created: 2019-10-31 11:25:05
$dictionary["TA_Tradeshow_Activities"]["fields"]["ta_tradeshow_activities_tasks_1"] = array (
  'name' => 'ta_tradeshow_activities_tasks_1',
  'type' => 'link',
  'relationship' => 'ta_tradeshow_activities_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'id_name' => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
  'link-type' => 'many',
  'side' => 'left',
);
