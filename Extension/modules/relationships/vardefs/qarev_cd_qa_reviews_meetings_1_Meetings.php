<?php
// created: 2021-02-11 07:49:03
$dictionary["Meeting"]["fields"]["qarev_cd_qa_reviews_meetings_1"] = array (
  'name' => 'qarev_cd_qa_reviews_meetings_1',
  'type' => 'link',
  'relationship' => 'qarev_cd_qa_reviews_meetings_1',
  'source' => 'non-db',
  'module' => 'QARev_CD_QA_Reviews',
  'bean_name' => 'QARev_CD_QA_Reviews',
  'side' => 'right',
  'vname' => 'LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_MEETINGS_TITLE',
  'id_name' => 'qarev_cd_qa_reviews_meetings_1qarev_cd_qa_reviews_ida',
  'link-type' => 'one',
);
$dictionary["Meeting"]["fields"]["qarev_cd_qa_reviews_meetings_1_name"] = array (
  'name' => 'qarev_cd_qa_reviews_meetings_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_QAREV_CD_QA_REVIEWS_TITLE',
  'save' => true,
  'id_name' => 'qarev_cd_qa_reviews_meetings_1qarev_cd_qa_reviews_ida',
  'link' => 'qarev_cd_qa_reviews_meetings_1',
  'table' => 'qarev_cd_qa_reviews',
  'module' => 'QARev_CD_QA_Reviews',
  'rname' => 'name',
);
$dictionary["Meeting"]["fields"]["qarev_cd_qa_reviews_meetings_1qarev_cd_qa_reviews_ida"] = array (
  'name' => 'qarev_cd_qa_reviews_meetings_1qarev_cd_qa_reviews_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_MEETINGS_TITLE_ID',
  'id_name' => 'qarev_cd_qa_reviews_meetings_1qarev_cd_qa_reviews_ida',
  'link' => 'qarev_cd_qa_reviews_meetings_1',
  'table' => 'qarev_cd_qa_reviews',
  'module' => 'QARev_CD_QA_Reviews',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
