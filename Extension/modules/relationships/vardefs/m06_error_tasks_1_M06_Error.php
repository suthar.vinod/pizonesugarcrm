<?php
// created: 2019-05-19 21:27:08
$dictionary["M06_Error"]["fields"]["m06_error_tasks_1"] = array (
  'name' => 'm06_error_tasks_1',
  'type' => 'link',
  'relationship' => 'm06_error_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_M06_ERROR_TASKS_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_tasks_1m06_error_ida',
  'link-type' => 'many',
  'side' => 'left',
);
