<?php
// created: 2016-02-01 20:37:08
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_m03_work_product_deliverable_1"] = array (
  'name' => 'm03_work_product_m03_work_product_deliverable_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_m03_work_product_deliverable_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Deliverable',
  'bean_name' => 'M03_Work_Product_Deliverable',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_p0b66product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
