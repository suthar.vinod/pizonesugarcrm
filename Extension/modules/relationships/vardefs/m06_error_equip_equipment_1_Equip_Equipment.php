<?php
// created: 2018-01-31 16:07:58
$dictionary["Equip_Equipment"]["fields"]["m06_error_equip_equipment_1"] = array (
  'name' => 'm06_error_equip_equipment_1',
  'type' => 'link',
  'relationship' => 'm06_error_equip_equipment_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_equip_equipment_1m06_error_ida',
);
