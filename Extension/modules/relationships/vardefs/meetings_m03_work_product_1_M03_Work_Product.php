<?php
// created: 2021-04-01 09:05:41
$dictionary["M03_Work_Product"]["fields"]["meetings_m03_work_product_1"] = array (
  'name' => 'meetings_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'meetings_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_MEETINGS_TITLE',
  'id_name' => 'meetings_m03_work_product_1meetings_ida',
);
