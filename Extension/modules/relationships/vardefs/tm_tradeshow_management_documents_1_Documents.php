<?php
// created: 2018-01-18 22:47:15
$dictionary["Document"]["fields"]["tm_tradeshow_management_documents_1"] = array (
  'name' => 'tm_tradeshow_management_documents_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_documents_1',
  'source' => 'non-db',
  'module' => 'TM_Tradeshow_Management',
  'bean_name' => 'TM_Tradeshow_Management',
  'side' => 'right',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
  'link-type' => 'one',
);
$dictionary["Document"]["fields"]["tm_tradeshow_management_documents_1_name"] = array (
  'name' => 'tm_tradeshow_management_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'save' => true,
  'id_name' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
  'link' => 'tm_tradeshow_management_documents_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["tm_tradeshow_management_documents_1tm_tradeshow_management_ida"] = array (
  'name' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE_ID',
  'id_name' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
  'link' => 'tm_tradeshow_management_documents_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
