<?php
// created: 2019-02-20 15:09:02
$dictionary["Equip_Equipment"]["fields"]["equip_equipment_efr_equipment_facility_recor_2"] = array (
  'name' => 'equip_equipment_efr_equipment_facility_recor_2',
  'type' => 'link',
  'relationship' => 'equip_equipment_efr_equipment_facility_recor_2',
  'source' => 'non-db',
  'module' => 'EFR_Equipment_Facility_Recor',
  'bean_name' => 'EFR_Equipment_Facility_Recor',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EQUIP_EQUIPMENT_TITLE',
  'id_name' => 'equip_equi01e2uipment_ida',
  'link-type' => 'many',
  'side' => 'left',
);
