<?php
// created: 2021-10-19 11:00:50
$dictionary["POI_Purchase_Order_Item"]["fields"]["m03_work_product_poi_purchase_order_item_1"] = array (
  'name' => 'm03_work_product_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'id_name' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["m03_work_product_poi_purchase_order_item_1_name"] = array (
  'name' => 'm03_work_product_poi_purchase_order_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
  'link' => 'm03_work_product_poi_purchase_order_item_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["m03_work_product_poi_purchase_order_item_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID',
  'id_name' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
  'link' => 'm03_work_product_poi_purchase_order_item_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
