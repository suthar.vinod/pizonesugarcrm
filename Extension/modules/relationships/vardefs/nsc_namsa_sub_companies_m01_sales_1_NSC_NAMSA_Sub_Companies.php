<?php
// created: 2021-12-07 12:33:38
$dictionary["NSC_NAMSA_Sub_Companies"]["fields"]["nsc_namsa_sub_companies_m01_sales_1"] = array (
  'name' => 'nsc_namsa_sub_companies_m01_sales_1',
  'type' => 'link',
  'relationship' => 'nsc_namsa_sub_companies_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE',
  'id_name' => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
  'link-type' => 'many',
  'side' => 'left',
);
