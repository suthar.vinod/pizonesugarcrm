<?php
// created: 2018-12-10 23:03:18
$dictionary["Email"]["fields"]["m01_sales_activities_1_emails"] = array (
  'name' => 'm01_sales_activities_1_emails',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_emails',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_EMAILS_FROM_M01_SALES_TITLE',
);
