<?php
// created: 2021-12-02 09:36:03
$dictionary["TaskD_Task_Design"]["fields"]["m03_work_product_code_taskd_task_design_1"] = array (
  'name' => 'm03_work_product_code_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_code_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Code',
  'bean_name' => 'M03_Work_Product_Code',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'id_name' => 'm03_work_p7fc0ct_code_ida',
  'link-type' => 'one',
);
$dictionary["TaskD_Task_Design"]["fields"]["m03_work_product_code_taskd_task_design_1_name"] = array (
  'name' => 'm03_work_product_code_taskd_task_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p7fc0ct_code_ida',
  'link' => 'm03_work_product_code_taskd_task_design_1',
  'table' => 'm03_work_product_code',
  'module' => 'M03_Work_Product_Code',
  'rname' => 'name',
);
$dictionary["TaskD_Task_Design"]["fields"]["m03_work_p7fc0ct_code_ida"] = array (
  'name' => 'm03_work_p7fc0ct_code_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID',
  'id_name' => 'm03_work_p7fc0ct_code_ida',
  'link' => 'm03_work_product_code_taskd_task_design_1',
  'table' => 'm03_work_product_code',
  'module' => 'M03_Work_Product_Code',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
