<?php
// created: 2017-09-13 15:16:33
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_activities_1_tasks"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_tasks',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_tasks',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_TASKS_FROM_TASKS_TITLE',
);
