<?php
// created: 2017-09-12 15:08:34
$dictionary["ANML_Animals"]["fields"]["wpe_work_product_enrollment_anml_animals_1"] = array (
  'name' => 'wpe_work_product_enrollment_anml_animals_1',
  'type' => 'link',
  'relationship' => 'wpe_work_product_enrollment_anml_animals_1',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'id_name' => 'wpe_work_p83ebollment_ida',
);
$dictionary["ANML_Animals"]["fields"]["wpe_work_product_enrollment_anml_animals_1_name"] = array (
  'name' => 'wpe_work_product_enrollment_anml_animals_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'save' => true,
  'id_name' => 'wpe_work_p83ebollment_ida',
  'link' => 'wpe_work_product_enrollment_anml_animals_1',
  'table' => 'wpe_work_product_enrollment',
  'module' => 'WPE_Work_Product_Enrollment',
  'rname' => 'name',
);
$dictionary["ANML_Animals"]["fields"]["wpe_work_p83ebollment_ida"] = array (
  'name' => 'wpe_work_p83ebollment_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID',
  'id_name' => 'wpe_work_p83ebollment_ida',
  'link' => 'wpe_work_product_enrollment_anml_animals_1',
  'table' => 'wpe_work_product_enrollment',
  'module' => 'WPE_Work_Product_Enrollment',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
