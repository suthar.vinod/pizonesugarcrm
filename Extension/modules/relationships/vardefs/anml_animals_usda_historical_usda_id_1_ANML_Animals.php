<?php
// created: 2021-02-23 10:42:04
$dictionary["ANML_Animals"]["fields"]["anml_animals_usda_historical_usda_id_1"] = array (
  'name' => 'anml_animals_usda_historical_usda_id_1',
  'type' => 'link',
  'relationship' => 'anml_animals_usda_historical_usda_id_1',
  'source' => 'non-db',
  'module' => 'USDA_Historical_USDA_ID',
  'bean_name' => 'USDA_Historical_USDA_ID',
  'vname' => 'LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_usda_historical_usda_id_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);
