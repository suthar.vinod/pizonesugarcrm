<?php
// created: 2020-11-03 06:37:20
$dictionary["Equip_Equipment"]["fields"]["accounts_equip_equipment_1"] = array (
  'name' => 'accounts_equip_equipment_1',
  'type' => 'link',
  'relationship' => 'accounts_equip_equipment_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_equip_equipment_1accounts_ida',
);
