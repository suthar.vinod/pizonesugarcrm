<?php
// created: 2018-02-22 13:58:39
$dictionary["M06_Error"]["fields"]["m06_error_ed_errors_department_1"] = array (
  'name' => 'm06_error_ed_errors_department_1',
  'type' => 'link',
  'relationship' => 'm06_error_ed_errors_department_1',
  'source' => 'non-db',
  'module' => 'ED_Errors_Department',
  'bean_name' => 'ED_Errors_Department',
  'vname' => 'LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_ED_ERRORS_DEPARTMENT_TITLE',
  'id_name' => 'm06_error_ed_errors_department_1ed_errors_department_idb',
);
