<?php
// created: 2017-09-13 15:16:58
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_activities_1_emails"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_emails',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_emails',
  'source' => 'non-db',
  'module' => 'Emails',
  'bean_name' => 'Email',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_EMAILS_FROM_EMAILS_TITLE',
);
