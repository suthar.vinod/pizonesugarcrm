<?php
// created: 2016-02-29 15:01:15
$dictionary["Task"]["fields"]["tasks_m03_work_product_deliverable_1"] = array (
  'name' => 'tasks_m03_work_product_deliverable_1',
  'type' => 'link',
  'relationship' => 'tasks_m03_work_product_deliverable_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Deliverable',
  'bean_name' => 'M03_Work_Product_Deliverable',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_TASKS_TITLE',
  'id_name' => 'tasks_m03_work_product_deliverable_1tasks_ida',
  'link-type' => 'many',
  'side' => 'left',
);
