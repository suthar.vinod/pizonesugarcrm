<?php
// created: 2018-04-23 17:07:52
$dictionary["Contact"]["fields"]["contacts_ta_tradeshow_activities_1"] = array (
  'name' => 'contacts_ta_tradeshow_activities_1',
  'type' => 'link',
  'relationship' => 'contacts_ta_tradeshow_activities_1',
  'source' => 'non-db',
  'module' => 'TA_Tradeshow_Activities',
  'bean_name' => 'TA_Tradeshow_Activities',
  'vname' => 'LBL_CONTACTS_TA_TRADESHOW_ACTIVITIES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_ta_tradeshow_activities_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
