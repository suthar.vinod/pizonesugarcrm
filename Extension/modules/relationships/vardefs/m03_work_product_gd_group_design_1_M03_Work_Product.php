<?php
// created: 2021-08-14 05:59:50
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_gd_group_design_1"] = array (
  'name' => 'm03_work_product_gd_group_design_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_gd_group_design_1',
  'source' => 'non-db',
  'module' => 'GD_Group_Design',
  'bean_name' => 'GD_Group_Design',
  'vname' => 'LBL_M03_WORK_PRODUCT_GD_GROUP_DESIGN_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_gd_group_design_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
