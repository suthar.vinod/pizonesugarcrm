<?php
// created: 2020-01-07 13:43:54
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_1"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'source' => 'non-db',
  'module' => 'CO_Clinical_Observation',
  'bean_name' => 'CO_Clinical_Observation',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'id_name' => 'cie_clinicecbbrvation_idb',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_1_name"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'save' => true,
  'id_name' => 'cie_clinicecbbrvation_idb',
  'link' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'table' => 'co_clinical_observation',
  'module' => 'CO_Clinical_Observation',
  'rname' => 'name',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinicecbbrvation_idb"] = array (
  'name' => 'cie_clinicecbbrvation_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE_ID',
  'id_name' => 'cie_clinicecbbrvation_idb',
  'link' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'table' => 'co_clinical_observation',
  'module' => 'CO_Clinical_Observation',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
