<?php
// created: 2019-07-09 11:53:38
$dictionary["M03_Work_Product"]["fields"]["rr_regulatory_response_m03_work_product_1"] = array (
  'name' => 'rr_regulatory_response_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'rr_regulatory_response_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'RR_Regulatory_Response',
  'bean_name' => 'RR_Regulatory_Response',
  'vname' => 'LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'id_name' => 'rr_regulat4120esponse_ida',
);
