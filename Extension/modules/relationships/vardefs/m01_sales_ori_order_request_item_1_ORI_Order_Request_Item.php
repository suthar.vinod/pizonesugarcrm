<?php
// created: 2021-10-19 10:26:08
$dictionary["ORI_Order_Request_Item"]["fields"]["m01_sales_ori_order_request_item_1"] = array (
  'name' => 'm01_sales_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'm01_sales_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["m01_sales_ori_order_request_item_1_name"] = array (
  'name' => 'm01_sales_ori_order_request_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
  'link' => 'm01_sales_ori_order_request_item_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["m01_sales_ori_order_request_item_1m01_sales_ida"] = array (
  'name' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID',
  'id_name' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
  'link' => 'm01_sales_ori_order_request_item_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
