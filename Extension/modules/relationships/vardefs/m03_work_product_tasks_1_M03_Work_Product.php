<?php
// created: 2016-02-25 20:01:03
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_tasks_1"] = array (
  'name' => 'm03_work_product_tasks_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_M03_WORK_PRODUCT_TASKS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_tasks_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
