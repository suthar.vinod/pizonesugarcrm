<?php
// created: 2016-01-25 02:57:18
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_m03_work_product_facility_1"] = array (
  'name' => 'm03_work_product_m03_work_product_facility_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_m03_work_product_facility_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Facility',
  'bean_name' => 'M03_Work_Product_Facility',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_FACILITY_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_m03_work_product_facility_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
