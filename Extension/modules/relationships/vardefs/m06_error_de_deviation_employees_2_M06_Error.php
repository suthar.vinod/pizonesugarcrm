<?php
// created: 2022-01-03 10:55:30
$dictionary["M06_Error"]["fields"]["m06_error_de_deviation_employees_2"] = array (
  'name' => 'm06_error_de_deviation_employees_2',
  'type' => 'link',
  'relationship' => 'm06_error_de_deviation_employees_2',
  'source' => 'non-db',
  'module' => 'DE_Deviation_Employees',
  'bean_name' => 'DE_Deviation_Employees',
  'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_2_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'id_name' => 'm06_error_de_deviation_employees_2de_deviation_employees_idb',
);
