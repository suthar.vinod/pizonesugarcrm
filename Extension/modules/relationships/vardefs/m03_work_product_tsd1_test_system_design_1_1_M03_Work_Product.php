<?php
// created: 2021-02-11 08:58:28
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_tsd1_test_system_design_1_1"] = array (
  'name' => 'm03_work_product_tsd1_test_system_design_1_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_tsd1_test_system_design_1_1',
  'source' => 'non-db',
  'module' => 'TSD1_Test_System_Design_1',
  'bean_name' => 'TSD1_Test_System_Design_1',
  'vname' => 'LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
