<?php
// created: 2019-09-26 14:13:30
$dictionary["Account"]["fields"]["accounts_cn_company_name_1"] = array (
  'name' => 'accounts_cn_company_name_1',
  'type' => 'link',
  'relationship' => 'accounts_cn_company_name_1',
  'source' => 'non-db',
  'module' => 'CN_Company_Name',
  'bean_name' => 'CN_Company_Name',
  'vname' => 'LBL_ACCOUNTS_CN_COMPANY_NAME_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_cn_company_name_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
