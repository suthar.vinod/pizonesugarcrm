<?php
// created: 2021-10-19 09:22:27
$dictionary["RI_Received_Items"]["fields"]["poi_purchase_order_item_ri_received_items_1"] = array (
  'name' => 'poi_purchase_order_item_ri_received_items_1',
  'type' => 'link',
  'relationship' => 'poi_purchase_order_item_ri_received_items_1',
  'source' => 'non-db',
  'module' => 'POI_Purchase_Order_Item',
  'bean_name' => 'POI_Purchase_Order_Item',
  'side' => 'right',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'id_name' => 'poi_purcha665cer_item_ida',
  'link-type' => 'one',
);
$dictionary["RI_Received_Items"]["fields"]["poi_purchase_order_item_ri_received_items_1_name"] = array (
  'name' => 'poi_purchase_order_item_ri_received_items_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'save' => true,
  'id_name' => 'poi_purcha665cer_item_ida',
  'link' => 'poi_purchase_order_item_ri_received_items_1',
  'table' => 'poi_purchase_order_item',
  'module' => 'POI_Purchase_Order_Item',
  'rname' => 'name',
);
$dictionary["RI_Received_Items"]["fields"]["poi_purcha665cer_item_ida"] = array (
  'name' => 'poi_purcha665cer_item_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE_ID',
  'id_name' => 'poi_purcha665cer_item_ida',
  'link' => 'poi_purchase_order_item_ri_received_items_1',
  'table' => 'poi_purchase_order_item',
  'module' => 'POI_Purchase_Order_Item',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
