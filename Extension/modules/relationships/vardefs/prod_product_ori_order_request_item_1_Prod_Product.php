<?php
// created: 2021-10-19 09:41:58
$dictionary["Prod_Product"]["fields"]["prod_product_ori_order_request_item_1"] = array (
  'name' => 'prod_product_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'prod_product_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'vname' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_PROD_PRODUCT_TITLE',
  'id_name' => 'prod_product_ori_order_request_item_1prod_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
