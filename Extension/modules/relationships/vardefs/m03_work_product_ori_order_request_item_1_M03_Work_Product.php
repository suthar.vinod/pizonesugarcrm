<?php
// created: 2021-10-19 10:30:05
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_ori_order_request_item_1"] = array (
  'name' => 'm03_work_product_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'vname' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
