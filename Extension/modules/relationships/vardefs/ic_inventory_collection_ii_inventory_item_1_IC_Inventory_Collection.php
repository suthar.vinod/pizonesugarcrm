<?php
// created: 2021-11-09 09:43:46
$dictionary["IC_Inventory_Collection"]["fields"]["ic_inventory_collection_ii_inventory_item_1"] = array (
  'name' => 'ic_inventory_collection_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'ic_inventory_collection_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'id_name' => 'ic_invento128flection_ida',
  'link-type' => 'many',
  'side' => 'left',
);
