<?php
// created: 2021-10-19 11:00:50
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_poi_purchase_order_item_1"] = array (
  'name' => 'm03_work_product_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'POI_Purchase_Order_Item',
  'bean_name' => 'POI_Purchase_Order_Item',
  'vname' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
