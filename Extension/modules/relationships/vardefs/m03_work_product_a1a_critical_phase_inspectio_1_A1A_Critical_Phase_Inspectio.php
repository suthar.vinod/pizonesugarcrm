<?php
// created: 2017-06-07 19:01:24
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["m03_work_product_a1a_critical_phase_inspectio_1"] = array (
  'name' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'id_name' => 'm03_work_p33edproduct_ida',
  'link-type' => 'one',
);
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["m03_work_product_a1a_critical_phase_inspectio_1_name"] = array (
  'name' => 'm03_work_product_a1a_critical_phase_inspectio_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p33edproduct_ida',
  'link' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["m03_work_p33edproduct_ida"] = array (
  'name' => 'm03_work_p33edproduct_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE_ID',
  'id_name' => 'm03_work_p33edproduct_ida',
  'link' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
