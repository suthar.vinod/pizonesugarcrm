<?php
// created: 2020-01-07 14:17:22
$dictionary["ANML_Animals"]["fields"]["anml_animals_co_clinical_observation_1"] = array (
  'name' => 'anml_animals_co_clinical_observation_1',
  'type' => 'link',
  'relationship' => 'anml_animals_co_clinical_observation_1',
  'source' => 'non-db',
  'module' => 'CO_Clinical_Observation',
  'bean_name' => 'CO_Clinical_Observation',
  'vname' => 'LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_co_clinical_observation_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);
