<?php
// created: 2019-08-27 11:37:04
$dictionary["EDoc_Email_Documents"]["fields"]["m03_work_product_edoc_email_documents_1"] = array (
  'name' => 'm03_work_product_edoc_email_documents_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_edoc_email_documents_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'id_name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["EDoc_Email_Documents"]["fields"]["m03_work_product_edoc_email_documents_1_name"] = array (
  'name' => 'm03_work_product_edoc_email_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
  'link' => 'm03_work_product_edoc_email_documents_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["EDoc_Email_Documents"]["fields"]["m03_work_product_edoc_email_documents_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE_ID',
  'id_name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
  'link' => 'm03_work_product_edoc_email_documents_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
