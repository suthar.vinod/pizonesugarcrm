<?php
// created: 2019-08-05 12:04:52
$dictionary["Email"]["fields"]["m03_work_product_activities_1_emails"] = array (
  'name' => 'm03_work_product_activities_1_emails',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_emails',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_EMAILS_FROM_M03_WORK_PRODUCT_TITLE',
);
