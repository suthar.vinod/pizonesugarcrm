<?php
// created: 2018-12-26 15:29:08
$dictionary["Account"]["fields"]["accounts_m03_work_product_1"] = array (
  'name' => 'accounts_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'accounts_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_m03_work_product_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
