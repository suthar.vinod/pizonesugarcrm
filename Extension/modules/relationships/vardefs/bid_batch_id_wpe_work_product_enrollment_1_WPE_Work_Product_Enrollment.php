<?php
// created: 2022-04-26 06:50:11
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["bid_batch_id_wpe_work_product_enrollment_1"] = array (
  'name' => 'bid_batch_id_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'BID_Batch_ID',
  'bean_name' => 'BID_Batch_ID',
  'side' => 'right',
  'vname' => 'LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'id_name' => 'bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida',
  'link-type' => 'one',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["bid_batch_id_wpe_work_product_enrollment_1_name"] = array (
  'name' => 'bid_batch_id_wpe_work_product_enrollment_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_BID_BATCH_ID_TITLE',
  'save' => true,
  'id_name' => 'bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida',
  'link' => 'bid_batch_id_wpe_work_product_enrollment_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida"] = array (
  'name' => 'bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID',
  'id_name' => 'bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida',
  'link' => 'bid_batch_id_wpe_work_product_enrollment_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
