<?php
// created: 2018-04-23 17:12:12
$dictionary["TA_Tradeshow_Activities"]["fields"]["accounts_ta_tradeshow_activities_1"] = array (
  'name' => 'accounts_ta_tradeshow_activities_1',
  'type' => 'link',
  'relationship' => 'accounts_ta_tradeshow_activities_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'id_name' => 'accounts_ta_tradeshow_activities_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["TA_Tradeshow_Activities"]["fields"]["accounts_ta_tradeshow_activities_1_name"] = array (
  'name' => 'accounts_ta_tradeshow_activities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_ta_tradeshow_activities_1accounts_ida',
  'link' => 'accounts_ta_tradeshow_activities_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["TA_Tradeshow_Activities"]["fields"]["accounts_ta_tradeshow_activities_1accounts_ida"] = array (
  'name' => 'accounts_ta_tradeshow_activities_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE_ID',
  'id_name' => 'accounts_ta_tradeshow_activities_1accounts_ida',
  'link' => 'accounts_ta_tradeshow_activities_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
