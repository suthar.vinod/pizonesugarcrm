<?php
// created: 2021-11-09 09:52:04
$dictionary["II_Inventory_Item"]["fields"]["ri_received_items_ii_inventory_item_1"] = array (
  'name' => 'ri_received_items_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'ri_received_items_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'RI_Received_Items',
  'bean_name' => 'RI_Received_Items',
  'side' => 'right',
  'vname' => 'LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["ri_received_items_ii_inventory_item_1_name"] = array (
  'name' => 'ri_received_items_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'save' => true,
  'id_name' => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
  'link' => 'ri_received_items_ii_inventory_item_1',
  'table' => 'ri_received_items',
  'module' => 'RI_Received_Items',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["ri_received_items_ii_inventory_item_1ri_received_items_ida"] = array (
  'name' => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
  'link' => 'ri_received_items_ii_inventory_item_1',
  'table' => 'ri_received_items',
  'module' => 'RI_Received_Items',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
