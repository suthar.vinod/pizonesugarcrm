<?php
// created: 2022-02-03 07:34:36
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_contacts_1"] = array (
  'name' => 'capa_capa_contacts_1',
  'type' => 'link',
  'relationship' => 'capa_capa_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CAPA_CAPA_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'capa_capa_contacts_1contacts_idb',
);
