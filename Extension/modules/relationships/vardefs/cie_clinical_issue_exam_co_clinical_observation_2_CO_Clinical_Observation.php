<?php
// created: 2020-01-07 13:50:42
$dictionary["CO_Clinical_Observation"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_2"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  'source' => 'non-db',
  'module' => 'CIE_Clinical_Issue_Exam',
  'bean_name' => 'CIE_Clinical_Issue_Exam',
  'side' => 'right',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_2_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'id_name' => 'cie_clinicddcbue_exam_ida',
  'link-type' => 'one',
);
$dictionary["CO_Clinical_Observation"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_2_name"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_2_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'save' => true,
  'id_name' => 'cie_clinicddcbue_exam_ida',
  'link' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'name',
);
$dictionary["CO_Clinical_Observation"]["fields"]["cie_clinicddcbue_exam_ida"] = array (
  'name' => 'cie_clinicddcbue_exam_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_2_FROM_CO_CLINICAL_OBSERVATION_TITLE_ID',
  'id_name' => 'cie_clinicddcbue_exam_ida',
  'link' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
