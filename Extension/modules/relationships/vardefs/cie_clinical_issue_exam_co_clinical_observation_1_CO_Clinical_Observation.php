<?php
// created: 2020-01-07 13:43:54
$dictionary["CO_Clinical_Observation"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_1"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'source' => 'non-db',
  'module' => 'CIE_Clinical_Issue_Exam',
  'bean_name' => 'CIE_Clinical_Issue_Exam',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'id_name' => 'cie_clinicb9a6ue_exam_ida',
);
$dictionary["CO_Clinical_Observation"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_1_name"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'save' => true,
  'id_name' => 'cie_clinicb9a6ue_exam_ida',
  'link' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'name',
);
$dictionary["CO_Clinical_Observation"]["fields"]["cie_clinicb9a6ue_exam_ida"] = array (
  'name' => 'cie_clinicb9a6ue_exam_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE_ID',
  'id_name' => 'cie_clinicb9a6ue_exam_ida',
  'link' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
