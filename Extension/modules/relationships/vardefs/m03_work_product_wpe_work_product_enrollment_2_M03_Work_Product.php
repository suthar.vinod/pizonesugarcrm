<?php
// created: 2020-12-15 06:35:00
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_wpe_work_product_enrollment_2"] = array (
  'name' => 'm03_work_product_wpe_work_product_enrollment_2',
  'type' => 'link',
  'relationship' => 'm03_work_product_wpe_work_product_enrollment_2',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_p9f23product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
