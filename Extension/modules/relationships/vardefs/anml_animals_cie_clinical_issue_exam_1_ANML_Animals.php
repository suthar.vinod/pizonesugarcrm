<?php
// created: 2020-01-07 13:40:49
$dictionary["ANML_Animals"]["fields"]["anml_animals_cie_clinical_issue_exam_1"] = array (
  'name' => 'anml_animals_cie_clinical_issue_exam_1',
  'type' => 'link',
  'relationship' => 'anml_animals_cie_clinical_issue_exam_1',
  'source' => 'non-db',
  'module' => 'CIE_Clinical_Issue_Exam',
  'bean_name' => 'CIE_Clinical_Issue_Exam',
  'vname' => 'LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);
