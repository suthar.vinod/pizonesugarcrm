<?php
// created: 2017-09-13 15:14:51
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_activities_1_calls"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_calls',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_calls',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_CALLS_FROM_CALLS_TITLE',
);
