<?php
// created: 2018-12-13 16:29:00
$dictionary["Contact"]["fields"]["contacts_m01_sales_1"] = array (
  'name' => 'contacts_m01_sales_1',
  'type' => 'link',
  'relationship' => 'contacts_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_CONTACTS_M01_SALES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_m01_sales_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
