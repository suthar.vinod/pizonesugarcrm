<?php
// created: 2021-11-09 10:26:31
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_ic_inventory_collection_1"] = array (
  'name' => 'm03_work_product_ic_inventory_collection_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_ic_inventory_collection_1',
  'source' => 'non-db',
  'module' => 'IC_Inventory_Collection',
  'bean_name' => 'IC_Inventory_Collection',
  'vname' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_ic_inventory_collection_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
