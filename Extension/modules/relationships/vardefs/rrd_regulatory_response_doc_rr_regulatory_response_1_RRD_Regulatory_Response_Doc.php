<?php
// created: 2019-07-09 12:11:34
$dictionary["RRD_Regulatory_Response_Doc"]["fields"]["rrd_regulatory_response_doc_rr_regulatory_response_1"] = array (
  'name' => 'rrd_regulatory_response_doc_rr_regulatory_response_1',
  'type' => 'link',
  'relationship' => 'rrd_regulatory_response_doc_rr_regulatory_response_1',
  'source' => 'non-db',
  'module' => 'RR_Regulatory_Response',
  'bean_name' => 'RR_Regulatory_Response',
  'vname' => 'LBL_RRD_REGULATORY_RESPONSE_DOC_RR_REGULATORY_RESPONSE_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'id_name' => 'rrd_regulac86besponse_idb',
);
