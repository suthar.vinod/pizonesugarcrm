<?php
// created: 2016-01-24 19:47:53
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_m03_work_product_ids_1"] = array (
  'name' => 'm03_work_product_m03_work_product_ids_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_m03_work_product_ids_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_IDs',
  'bean_name' => 'M03_Work_Product_IDs',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_IDS_1_FROM_M03_WORK_PRODUCT_IDS_TITLE',
  'id_name' => 'm03_work_product_m03_work_product_ids_1m03_work_product_ids_idb',
);
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_m03_work_product_ids_1_name"] = array (
  'name' => 'm03_work_product_m03_work_product_ids_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_IDS_1_FROM_M03_WORK_PRODUCT_IDS_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_m03_work_product_ids_1m03_work_product_ids_idb',
  'link' => 'm03_work_product_m03_work_product_ids_1',
  'table' => 'm03_work_product_ids',
  'module' => 'M03_Work_Product_IDs',
  'rname' => 'name',
);
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_m03_work_product_ids_1m03_work_product_ids_idb"] = array (
  'name' => 'm03_work_product_m03_work_product_ids_1m03_work_product_ids_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_IDS_1_FROM_M03_WORK_PRODUCT_IDS_TITLE_ID',
  'id_name' => 'm03_work_product_m03_work_product_ids_1m03_work_product_ids_idb',
  'link' => 'm03_work_product_m03_work_product_ids_1',
  'table' => 'm03_work_product_ids',
  'module' => 'M03_Work_Product_IDs',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
