<?php
// created: 2017-06-07 19:03:44
$dictionary["A1A_CPI_Findings"]["fields"]["a1a_critical_phase_inspectio_a1a_cpi_findings_1"] = array (
  'name' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'side' => 'right',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CPI_FINDINGS_TITLE',
  'id_name' => 'a1a_criticbf09spectio_ida',
  'link-type' => 'one',
);
$dictionary["A1A_CPI_Findings"]["fields"]["a1a_critical_phase_inspectio_a1a_cpi_findings_1_name"] = array (
  'name' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'save' => true,
  'id_name' => 'a1a_criticbf09spectio_ida',
  'link' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'table' => 'a1a_critical_phase_inspectio',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'rname' => 'name',
);
$dictionary["A1A_CPI_Findings"]["fields"]["a1a_criticbf09spectio_ida"] = array (
  'name' => 'a1a_criticbf09spectio_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CPI_FINDINGS_TITLE_ID',
  'id_name' => 'a1a_criticbf09spectio_ida',
  'link' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'table' => 'a1a_critical_phase_inspectio',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
