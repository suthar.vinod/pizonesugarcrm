<?php
// created: 2018-04-23 17:10:34
$dictionary["TM_Tradeshow_Management"]["fields"]["tm_tradeshow_management_ta_tradeshow_activities_1"] = array (
  'name' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'source' => 'non-db',
  'module' => 'TA_Tradeshow_Activities',
  'bean_name' => 'TA_Tradeshow_Activities',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'id_name' => 'tm_tradesh0f8eagement_ida',
  'link-type' => 'many',
  'side' => 'left',
);
