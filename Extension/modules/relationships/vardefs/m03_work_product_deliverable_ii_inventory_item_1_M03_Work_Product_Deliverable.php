<?php
// created: 2022-02-22 07:44:29
$dictionary["M03_Work_Product_Deliverable"]["fields"]["m03_work_product_deliverable_ii_inventory_item_1"] = array (
  'name' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'id_name' => 'm03_work_p7e14verable_ida',
  'link-type' => 'many',
  'side' => 'left',
);
