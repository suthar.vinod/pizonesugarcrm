<?php
// created: 2021-04-01 09:05:41
$dictionary["Meeting"]["fields"]["meetings_m03_work_product_1"] = array (
  'name' => 'meetings_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'meetings_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'meetings_m03_work_product_1m03_work_product_idb',
);
