<?php
// created: 2022-02-22 07:48:02
$dictionary["IC_Inventory_Collection"]["fields"]["m03_work_product_deliverable_ic_inventory_collection_1"] = array (
  'name' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Deliverable',
  'bean_name' => 'M03_Work_Product_Deliverable',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'id_name' => 'm03_work_p2592verable_ida',
  'link-type' => 'one',
);
$dictionary["IC_Inventory_Collection"]["fields"]["m03_work_product_deliverable_ic_inventory_collection_1_name"] = array (
  'name' => 'm03_work_product_deliverable_ic_inventory_collection_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p2592verable_ida',
  'link' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'table' => 'm03_work_product_deliverable',
  'module' => 'M03_Work_Product_Deliverable',
  'rname' => 'name',
);
$dictionary["IC_Inventory_Collection"]["fields"]["m03_work_p2592verable_ida"] = array (
  'name' => 'm03_work_p2592verable_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE_ID',
  'id_name' => 'm03_work_p2592verable_ida',
  'link' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'table' => 'm03_work_product_deliverable',
  'module' => 'M03_Work_Product_Deliverable',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
