<?php
// created: 2021-10-19 10:58:37
$dictionary["M01_Sales"]["fields"]["m01_sales_poi_purchase_order_item_1"] = array (
  'name' => 'm01_sales_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'm01_sales_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'POI_Purchase_Order_Item',
  'bean_name' => 'POI_Purchase_Order_Item',
  'vname' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);
