<?php
// created: 2016-02-29 19:53:08
$dictionary["M03_Work_Product_Deliverable"]["fields"]["m03_work_product_deliverable_tasks_1"] = array (
  'name' => 'm03_work_product_deliverable_tasks_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_deliverable_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'id_name' => 'm03_work_pbb72verable_ida',
  'link-type' => 'many',
  'side' => 'left',
);
