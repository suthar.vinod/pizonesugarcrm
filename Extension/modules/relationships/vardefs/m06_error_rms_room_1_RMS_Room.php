<?php
// created: 2019-02-21 20:21:31
$dictionary["RMS_Room"]["fields"]["m06_error_rms_room_1"] = array (
  'name' => 'm06_error_rms_room_1',
  'type' => 'link',
  'relationship' => 'm06_error_rms_room_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_RMS_ROOM_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_rms_room_1m06_error_ida',
);
