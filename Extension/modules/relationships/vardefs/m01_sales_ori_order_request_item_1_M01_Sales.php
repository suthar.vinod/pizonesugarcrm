<?php
// created: 2021-10-19 10:26:08
$dictionary["M01_Sales"]["fields"]["m01_sales_ori_order_request_item_1"] = array (
  'name' => 'm01_sales_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'm01_sales_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'vname' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);
