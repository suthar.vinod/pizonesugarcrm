<?php
// created: 2021-10-19 10:58:37
$dictionary["POI_Purchase_Order_Item"]["fields"]["m01_sales_poi_purchase_order_item_1"] = array (
  'name' => 'm01_sales_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'm01_sales_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'id_name' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["m01_sales_poi_purchase_order_item_1_name"] = array (
  'name' => 'm01_sales_poi_purchase_order_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
  'link' => 'm01_sales_poi_purchase_order_item_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["m01_sales_poi_purchase_order_item_1m01_sales_ida"] = array (
  'name' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID',
  'id_name' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
  'link' => 'm01_sales_poi_purchase_order_item_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
