<?php
// created: 2021-08-14 11:12:04
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["gd_group_design_wpe_work_product_enrollment_1"] = array (
  'name' => 'gd_group_design_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'gd_group_design_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'GD_Group_Design',
  'bean_name' => 'GD_Group_Design',
  'side' => 'right',
  'vname' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'id_name' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
  'link-type' => 'one',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["gd_group_design_wpe_work_product_enrollment_1_name"] = array (
  'name' => 'gd_group_design_wpe_work_product_enrollment_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_GD_GROUP_DESIGN_TITLE',
  'save' => true,
  'id_name' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
  'link' => 'gd_group_design_wpe_work_product_enrollment_1',
  'table' => 'gd_group_design',
  'module' => 'GD_Group_Design',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida"] = array (
  'name' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID',
  'id_name' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
  'link' => 'gd_group_design_wpe_work_product_enrollment_1',
  'table' => 'gd_group_design',
  'module' => 'GD_Group_Design',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
