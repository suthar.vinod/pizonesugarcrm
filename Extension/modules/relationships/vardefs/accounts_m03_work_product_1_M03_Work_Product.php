<?php
// created: 2018-12-26 15:29:08
$dictionary["M03_Work_Product"]["fields"]["accounts_m03_work_product_1"] = array (
  'name' => 'accounts_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'accounts_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'accounts_m03_work_product_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product"]["fields"]["accounts_m03_work_product_1_name"] = array (
  'name' => 'accounts_m03_work_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_m03_work_product_1accounts_ida',
  'link' => 'accounts_m03_work_product_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["M03_Work_Product"]["fields"]["accounts_m03_work_product_1accounts_ida"] = array (
  'name' => 'accounts_m03_work_product_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID',
  'id_name' => 'accounts_m03_work_product_1accounts_ida',
  'link' => 'accounts_m03_work_product_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
