<?php
// created: 2020-01-07 13:48:18
$dictionary["M06_Error"]["fields"]["cie_clinical_issue_exam_m06_error_1"] = array (
  'name' => 'cie_clinical_issue_exam_m06_error_1',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_m06_error_1',
  'source' => 'non-db',
  'module' => 'CIE_Clinical_Issue_Exam',
  'bean_name' => 'CIE_Clinical_Issue_Exam',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'id_name' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
);
$dictionary["M06_Error"]["fields"]["cie_clinical_issue_exam_m06_error_1_name"] = array (
  'name' => 'cie_clinical_issue_exam_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'save' => true,
  'id_name' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
  'link' => 'cie_clinical_issue_exam_m06_error_1',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida"] = array (
  'name' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE_ID',
  'id_name' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
  'link' => 'cie_clinical_issue_exam_m06_error_1',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
