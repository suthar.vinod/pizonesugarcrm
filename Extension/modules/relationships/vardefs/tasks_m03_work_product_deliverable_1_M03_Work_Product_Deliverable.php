<?php
// created: 2016-02-29 15:01:15
$dictionary["M03_Work_Product_Deliverable"]["fields"]["tasks_m03_work_product_deliverable_1"] = array (
  'name' => 'tasks_m03_work_product_deliverable_1',
  'type' => 'link',
  'relationship' => 'tasks_m03_work_product_deliverable_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'id_name' => 'tasks_m03_work_product_deliverable_1tasks_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product_Deliverable"]["fields"]["tasks_m03_work_product_deliverable_1_name"] = array (
  'name' => 'tasks_m03_work_product_deliverable_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_TASKS_TITLE',
  'save' => true,
  'id_name' => 'tasks_m03_work_product_deliverable_1tasks_ida',
  'link' => 'tasks_m03_work_product_deliverable_1',
  'table' => 'tasks',
  'module' => 'Tasks',
  'rname' => 'name',
);
$dictionary["M03_Work_Product_Deliverable"]["fields"]["tasks_m03_work_product_deliverable_1tasks_ida"] = array (
  'name' => 'tasks_m03_work_product_deliverable_1tasks_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE_ID',
  'id_name' => 'tasks_m03_work_product_deliverable_1tasks_ida',
  'link' => 'tasks_m03_work_product_deliverable_1',
  'table' => 'tasks',
  'module' => 'Tasks',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
