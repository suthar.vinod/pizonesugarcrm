<?php
// created: 2019-08-05 12:03:27
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_activities_1_meetings"] = array (
  'name' => 'm03_work_product_activities_1_meetings',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_meetings',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE',
);
