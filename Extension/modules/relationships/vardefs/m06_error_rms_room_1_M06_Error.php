<?php
// created: 2019-02-21 20:21:31
$dictionary["M06_Error"]["fields"]["m06_error_rms_room_1"] = array (
  'name' => 'm06_error_rms_room_1',
  'type' => 'link',
  'relationship' => 'm06_error_rms_room_1',
  'source' => 'non-db',
  'module' => 'RMS_Room',
  'bean_name' => 'RMS_Room',
  'vname' => 'LBL_M06_ERROR_RMS_ROOM_1_FROM_RMS_ROOM_TITLE',
  'id_name' => 'm06_error_rms_room_1rms_room_idb',
);
