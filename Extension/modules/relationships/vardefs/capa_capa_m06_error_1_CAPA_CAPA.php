<?php
// created: 2022-02-03 07:30:32
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_m06_error_1"] = array (
  'name' => 'capa_capa_m06_error_1',
  'type' => 'link',
  'relationship' => 'capa_capa_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_CAPA_CAPA_M06_ERROR_1_FROM_CAPA_CAPA_TITLE',
  'id_name' => 'capa_capa_m06_error_1capa_capa_ida',
  'link-type' => 'many',
  'side' => 'left',
);
