<?php
// created: 2019-02-19 17:46:13
$dictionary["EFD_Equipment_Facility_Doc"]["fields"]["equip_equipment_efd_equipment_facility_doc_1"] = array (
  'name' => 'equip_equipment_efd_equipment_facility_doc_1',
  'type' => 'link',
  'relationship' => 'equip_equipment_efd_equipment_facility_doc_1',
  'source' => 'non-db',
  'module' => 'Equip_Equipment',
  'bean_name' => 'Equip_Equipment',
  'side' => 'right',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EFD_EQUIPMENT_FACILITY_DOC_TITLE',
  'id_name' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
  'link-type' => 'one',
);
$dictionary["EFD_Equipment_Facility_Doc"]["fields"]["equip_equipment_efd_equipment_facility_doc_1_name"] = array (
  'name' => 'equip_equipment_efd_equipment_facility_doc_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'save' => true,
  'id_name' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
  'link' => 'equip_equipment_efd_equipment_facility_doc_1',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'name',
);
$dictionary["EFD_Equipment_Facility_Doc"]["fields"]["equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida"] = array (
  'name' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EFD_EQUIPMENT_FACILITY_DOC_TITLE_ID',
  'id_name' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
  'link' => 'equip_equipment_efd_equipment_facility_doc_1',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
