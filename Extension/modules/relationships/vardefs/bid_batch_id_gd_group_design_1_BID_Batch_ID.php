<?php
// created: 2022-09-22 05:28:30
$dictionary["BID_Batch_ID"]["fields"]["bid_batch_id_gd_group_design_1"] = array (
  'name' => 'bid_batch_id_gd_group_design_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_gd_group_design_1',
  'source' => 'non-db',
  'module' => 'GD_Group_Design',
  'bean_name' => 'GD_Group_Design',
  'vname' => 'LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_BID_BATCH_ID_TITLE',
  'id_name' => 'bid_batch_id_gd_group_design_1bid_batch_id_ida',
  'link-type' => 'many',
  'side' => 'left',
);
