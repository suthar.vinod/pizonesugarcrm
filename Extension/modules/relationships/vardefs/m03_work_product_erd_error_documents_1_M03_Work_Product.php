<?php
// created: 2019-11-05 12:53:29
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_erd_error_documents_1"] = array (
  'name' => 'm03_work_product_erd_error_documents_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_erd_error_documents_1',
  'source' => 'non-db',
  'module' => 'Erd_Error_Documents',
  'bean_name' => 'Erd_Error_Documents',
  'vname' => 'LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'id_name' => 'm03_work_product_erd_error_documents_1erd_error_documents_idb',
);
