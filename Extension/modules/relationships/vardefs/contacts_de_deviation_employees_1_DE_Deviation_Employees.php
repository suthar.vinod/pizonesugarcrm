<?php
// created: 2019-02-20 19:46:48
$dictionary["DE_Deviation_Employees"]["fields"]["contacts_de_deviation_employees_1"] = array (
  'name' => 'contacts_de_deviation_employees_1',
  'type' => 'link',
  'relationship' => 'contacts_de_deviation_employees_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'id_name' => 'contacts_de_deviation_employees_1contacts_ida',
  'link-type' => 'one',
);
$dictionary["DE_Deviation_Employees"]["fields"]["contacts_de_deviation_employees_1_name"] = array (
  'name' => 'contacts_de_deviation_employees_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_de_deviation_employees_1contacts_ida',
  'link' => 'contacts_de_deviation_employees_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["DE_Deviation_Employees"]["fields"]["contacts_de_deviation_employees_1contacts_ida"] = array (
  'name' => 'contacts_de_deviation_employees_1contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE_ID',
  'id_name' => 'contacts_de_deviation_employees_1contacts_ida',
  'link' => 'contacts_de_deviation_employees_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
