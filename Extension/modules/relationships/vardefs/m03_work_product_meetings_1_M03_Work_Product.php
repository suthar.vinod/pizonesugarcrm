<?php
// created: 2017-05-02 19:55:40
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_meetings_1"] = array (
  'name' => 'm03_work_product_meetings_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_meetings_1',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_meetings_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
