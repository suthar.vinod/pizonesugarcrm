<?php
// created: 2021-12-07 12:00:27
$dictionary["TC_NAMSA_Test_Codes"]["fields"]["tc_namsa_test_codes_m03_work_product_code_1"] = array (
  'name' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'type' => 'link',
  'relationship' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Code',
  'bean_name' => 'M03_Work_Product_Code',
  'vname' => 'LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_TC_NAMSA_TEST_CODES_TITLE',
  'id_name' => 'tc_namsa_t49d3t_codes_ida',
  'link-type' => 'many',
  'side' => 'left',
);
