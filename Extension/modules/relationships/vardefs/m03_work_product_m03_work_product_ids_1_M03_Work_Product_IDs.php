<?php
// created: 2016-01-24 19:47:53
$dictionary["M03_Work_Product_IDs"]["fields"]["m03_work_product_m03_work_product_ids_1"] = array (
  'name' => 'm03_work_product_m03_work_product_ids_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_m03_work_product_ids_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_IDS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_m03_work_product_ids_1m03_work_product_ida',
);
$dictionary["M03_Work_Product_IDs"]["fields"]["m03_work_product_m03_work_product_ids_1_name"] = array (
  'name' => 'm03_work_product_m03_work_product_ids_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_IDS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_m03_work_product_ids_1m03_work_product_ida',
  'link' => 'm03_work_product_m03_work_product_ids_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["M03_Work_Product_IDs"]["fields"]["m03_work_product_m03_work_product_ids_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_m03_work_product_ids_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_IDS_1_FROM_M03_WORK_PRODUCT_TITLE_ID',
  'id_name' => 'm03_work_product_m03_work_product_ids_1m03_work_product_ida',
  'link' => 'm03_work_product_m03_work_product_ids_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
