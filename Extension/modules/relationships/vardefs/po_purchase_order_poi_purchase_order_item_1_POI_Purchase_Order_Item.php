<?php
// created: 2021-10-19 09:29:00
$dictionary["POI_Purchase_Order_Item"]["fields"]["po_purchase_order_poi_purchase_order_item_1"] = array (
  'name' => 'po_purchase_order_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'po_purchase_order_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'PO_Purchase_Order',
  'bean_name' => 'PO_Purchase_Order',
  'side' => 'right',
  'vname' => 'LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'id_name' => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
  'link-type' => 'one',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["po_purchase_order_poi_purchase_order_item_1_name"] = array (
  'name' => 'po_purchase_order_poi_purchase_order_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE',
  'save' => true,
  'id_name' => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
  'link' => 'po_purchase_order_poi_purchase_order_item_1',
  'table' => 'po_purchase_order',
  'module' => 'PO_Purchase_Order',
  'rname' => 'name',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida"] = array (
  'name' => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID',
  'id_name' => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
  'link' => 'po_purchase_order_poi_purchase_order_item_1',
  'table' => 'po_purchase_order',
  'module' => 'PO_Purchase_Order',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
