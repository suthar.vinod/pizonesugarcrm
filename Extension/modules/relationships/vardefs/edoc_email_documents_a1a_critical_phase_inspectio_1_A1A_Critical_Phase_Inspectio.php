<?php
// created: 2022-02-01 04:24:04
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["edoc_email_documents_a1a_critical_phase_inspectio_1"] = array (
  'name' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'type' => 'link',
  'relationship' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'source' => 'non-db',
  'module' => 'EDoc_Email_Documents',
  'bean_name' => 'EDoc_Email_Documents',
  'side' => 'right',
  'vname' => 'LBL_EDOC_EMAIL_DOCUMENTS_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'id_name' => 'edoc_emailbf38cuments_ida',
  'link-type' => 'one',
);
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["edoc_email_documents_a1a_critical_phase_inspectio_1_name"] = array (
  'name' => 'edoc_email_documents_a1a_critical_phase_inspectio_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EDOC_EMAIL_DOCUMENTS_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'save' => true,
  'id_name' => 'edoc_emailbf38cuments_ida',
  'link' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'table' => 'edoc_email_documents',
  'module' => 'EDoc_Email_Documents',
  'rname' => 'document_name',
);
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["edoc_emailbf38cuments_ida"] = array (
  'name' => 'edoc_emailbf38cuments_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_EDOC_EMAIL_DOCUMENTS_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE_ID',
  'id_name' => 'edoc_emailbf38cuments_ida',
  'link' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'table' => 'edoc_email_documents',
  'module' => 'EDoc_Email_Documents',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
