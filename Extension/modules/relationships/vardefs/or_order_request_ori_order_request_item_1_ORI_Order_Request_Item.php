<?php
// created: 2021-10-19 09:18:33
$dictionary["ORI_Order_Request_Item"]["fields"]["or_order_request_ori_order_request_item_1"] = array (
  'name' => 'or_order_request_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'or_order_request_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'OR_Order_Request',
  'bean_name' => 'OR_Order_Request',
  'side' => 'right',
  'vname' => 'LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'or_order_request_ori_order_request_item_1or_order_request_ida',
  'link-type' => 'one',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["or_order_request_ori_order_request_item_1_name"] = array (
  'name' => 'or_order_request_ori_order_request_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_OR_ORDER_REQUEST_TITLE',
  'save' => true,
  'id_name' => 'or_order_request_ori_order_request_item_1or_order_request_ida',
  'link' => 'or_order_request_ori_order_request_item_1',
  'table' => 'or_order_request',
  'module' => 'OR_Order_Request',
  'rname' => 'name',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["or_order_request_ori_order_request_item_1or_order_request_ida"] = array (
  'name' => 'or_order_request_ori_order_request_item_1or_order_request_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID',
  'id_name' => 'or_order_request_ori_order_request_item_1or_order_request_ida',
  'link' => 'or_order_request_ori_order_request_item_1',
  'table' => 'or_order_request',
  'module' => 'OR_Order_Request',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
