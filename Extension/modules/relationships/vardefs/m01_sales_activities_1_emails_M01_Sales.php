<?php
// created: 2018-12-10 23:03:18
$dictionary["M01_Sales"]["fields"]["m01_sales_activities_1_emails"] = array (
  'name' => 'm01_sales_activities_1_emails',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_emails',
  'source' => 'non-db',
  'module' => 'Emails',
  'bean_name' => 'Email',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_EMAILS_FROM_EMAILS_TITLE',
);
