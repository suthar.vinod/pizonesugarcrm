<?php
// created: 2022-04-26 06:47:59
$dictionary["BID_Batch_ID"]["fields"]["bid_batch_id_taskd_task_design_1"] = array (
  'name' => 'bid_batch_id_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'vname' => 'LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_BID_BATCH_ID_TITLE',
  'id_name' => 'bid_batch_id_taskd_task_design_1bid_batch_id_ida',
  'link-type' => 'many',
  'side' => 'left',
);
