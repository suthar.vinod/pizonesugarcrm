<?php
// created: 2018-10-09 16:44:23
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_sw_study_workflow_1"] = array (
  'name' => 'm03_work_product_sw_study_workflow_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_sw_study_workflow_1',
  'source' => 'non-db',
  'module' => 'SW_Study_Workflow',
  'bean_name' => 'SW_Study_Workflow',
  'vname' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
