<?php
// created: 2021-10-19 09:25:42
$dictionary["PO_Purchase_Order"]["fields"]["po_purchase_order_ori_order_request_item_1"] = array (
  'name' => 'po_purchase_order_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'po_purchase_order_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'vname' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE',
  'id_name' => 'po_purchase_order_ori_order_request_item_1po_purchase_order_ida',
  'link-type' => 'many',
  'side' => 'left',
);
