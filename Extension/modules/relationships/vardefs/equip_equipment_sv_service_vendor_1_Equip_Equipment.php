<?php
// created: 2020-01-15 13:01:13
$dictionary["Equip_Equipment"]["fields"]["equip_equipment_sv_service_vendor_1"] = array (
  'name' => 'equip_equipment_sv_service_vendor_1',
  'type' => 'link',
  'relationship' => 'equip_equipment_sv_service_vendor_1',
  'source' => 'non-db',
  'module' => 'SV_Service_Vendor',
  'bean_name' => 'SV_Service_Vendor',
  'vname' => 'LBL_EQUIP_EQUIPMENT_SV_SERVICE_VENDOR_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'id_name' => 'equip_equipment_sv_service_vendor_1equip_equipment_ida',
  'link-type' => 'many',
  'side' => 'left',
);
