<?php
// created: 2021-11-09 10:22:13
$dictionary["M01_Sales"]["fields"]["m01_sales_im_inventory_management_1"] = array (
  'name' => 'm01_sales_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'm01_sales_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'IM_Inventory_Management',
  'bean_name' => 'IM_Inventory_Management',
  'vname' => 'LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_im_inventory_management_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);
