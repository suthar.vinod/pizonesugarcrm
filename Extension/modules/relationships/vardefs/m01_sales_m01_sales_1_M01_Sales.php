<?php
// created: 2021-09-23 08:38:16
$dictionary["M01_Sales"]["fields"]["m01_sales_m01_sales_1"] = array (
  'name' => 'm01_sales_m01_sales_1',
  'type' => 'link',
  'relationship' => 'm01_sales_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_M01_SALES_M01_SALES_1_FROM_M01_SALES_L_TITLE',
  'id_name' => 'm01_sales_m01_sales_1m01_sales_ida',
);
$dictionary["M01_Sales"]["fields"]["m01_sales_m01_sales_1"] = array (
  'name' => 'm01_sales_m01_sales_1',
  'type' => 'link',
  'relationship' => 'm01_sales_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_M01_SALES_M01_SALES_1_FROM_M01_SALES_R_TITLE',
  'id_name' => 'm01_sales_m01_sales_1m01_sales_ida',
);
