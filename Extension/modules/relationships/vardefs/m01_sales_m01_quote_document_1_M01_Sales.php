<?php
// created: 2016-01-25 22:19:12
$dictionary["M01_Sales"]["fields"]["m01_sales_m01_quote_document_1"] = array (
  'name' => 'm01_sales_m01_quote_document_1',
  'type' => 'link',
  'relationship' => 'm01_sales_m01_quote_document_1',
  'source' => 'non-db',
  'module' => 'M01_Quote_Document',
  'bean_name' => 'M01_Quote_Document',
  'vname' => 'LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_m01_quote_document_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);
