<?php
// created: 2020-07-07 08:16:55
$dictionary["M06_Error"]["fields"]["m06_error_m06_error_1"] = array (
  'name' => 'm06_error_m06_error_1',
  'type' => 'link',
  'relationship' => 'm06_error_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_L_TITLE',
  'id_name' => 'm06_error_m06_error_1m06_error_idb',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["M06_Error"]["fields"]["m06_error_m06_error_1_right"] = array (
  'name' => 'm06_error_m06_error_1_right',
  'type' => 'link',
  'relationship' => 'm06_error_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'side' => 'right',
  'vname' => 'LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_R_TITLE',
  'id_name' => 'm06_error_m06_error_1m06_error_ida',
  'link-type' => 'one',
);
$dictionary["M06_Error"]["fields"]["m06_error_m06_error_1_name"] = array (
  'name' => 'm06_error_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_L_TITLE',
  'save' => true,
  'id_name' => 'm06_error_m06_error_1m06_error_ida',
  'link' => 'm06_error_m06_error_1_right',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["m06_error_m06_error_1m06_error_ida"] = array (
  'name' => 'm06_error_m06_error_1m06_error_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_R_TITLE_ID',
  'id_name' => 'm06_error_m06_error_1m06_error_ida',
  'link' => 'm06_error_m06_error_1_right',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
