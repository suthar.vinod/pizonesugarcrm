<?php
// created: 2020-01-15 13:01:13
$dictionary["SV_Service_Vendor"]["fields"]["equip_equipment_sv_service_vendor_1"] = array (
  'name' => 'equip_equipment_sv_service_vendor_1',
  'type' => 'link',
  'relationship' => 'equip_equipment_sv_service_vendor_1',
  'source' => 'non-db',
  'module' => 'Equip_Equipment',
  'bean_name' => 'Equip_Equipment',
  'side' => 'right',
  'vname' => 'LBL_EQUIP_EQUIPMENT_SV_SERVICE_VENDOR_1_FROM_SV_SERVICE_VENDOR_TITLE',
  'id_name' => 'equip_equipment_sv_service_vendor_1equip_equipment_ida',
  'link-type' => 'one',
);
$dictionary["SV_Service_Vendor"]["fields"]["equip_equipment_sv_service_vendor_1_name"] = array (
  'name' => 'equip_equipment_sv_service_vendor_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_SV_SERVICE_VENDOR_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'save' => true,
  'id_name' => 'equip_equipment_sv_service_vendor_1equip_equipment_ida',
  'link' => 'equip_equipment_sv_service_vendor_1',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'name',
);
$dictionary["SV_Service_Vendor"]["fields"]["equip_equipment_sv_service_vendor_1equip_equipment_ida"] = array (
  'name' => 'equip_equipment_sv_service_vendor_1equip_equipment_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_SV_SERVICE_VENDOR_1_FROM_SV_SERVICE_VENDOR_TITLE_ID',
  'id_name' => 'equip_equipment_sv_service_vendor_1equip_equipment_ida',
  'link' => 'equip_equipment_sv_service_vendor_1',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
