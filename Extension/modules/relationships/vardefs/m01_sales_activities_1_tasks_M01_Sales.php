<?php
// created: 2018-12-10 23:02:55
$dictionary["M01_Sales"]["fields"]["m01_sales_activities_1_tasks"] = array (
  'name' => 'm01_sales_activities_1_tasks',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_tasks',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_TASKS_FROM_TASKS_TITLE',
);
