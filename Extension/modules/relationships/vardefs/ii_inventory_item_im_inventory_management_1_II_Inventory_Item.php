<?php
// created: 2021-11-09 09:39:34
$dictionary["II_Inventory_Item"]["fields"]["ii_inventory_item_im_inventory_management_1"] = array (
  'name' => 'ii_inventory_item_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'ii_inventory_item_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'IM_Inventory_Management',
  'bean_name' => 'IM_Inventory_Management',
  'vname' => 'LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'id_name' => 'ii_invento5fdeagement_idb',
);
