<?php
// created: 2018-04-23 18:02:37
$dictionary["M01_Sales"]["fields"]["tm_tradeshow_management_m01_sales_1"] = array (
  'name' => 'tm_tradeshow_management_m01_sales_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_m01_sales_1',
  'source' => 'non-db',
  'module' => 'TM_Tradeshow_Management',
  'bean_name' => 'TM_Tradeshow_Management',
  'side' => 'right',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_M01_SALES_TITLE',
  'id_name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
  'link-type' => 'one',
);
$dictionary["M01_Sales"]["fields"]["tm_tradeshow_management_m01_sales_1_name"] = array (
  'name' => 'tm_tradeshow_management_m01_sales_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'save' => true,
  'id_name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
  'link' => 'tm_tradeshow_management_m01_sales_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'name',
);
$dictionary["M01_Sales"]["fields"]["tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida"] = array (
  'name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_M01_SALES_TITLE_ID',
  'id_name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
  'link' => 'tm_tradeshow_management_m01_sales_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
