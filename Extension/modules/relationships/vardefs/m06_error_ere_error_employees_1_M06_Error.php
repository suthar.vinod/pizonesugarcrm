<?php
// created: 2018-01-31 21:12:49
$dictionary["M06_Error"]["fields"]["m06_error_ere_error_employees_1"] = array (
  'name' => 'm06_error_ere_error_employees_1',
  'type' => 'link',
  'relationship' => 'm06_error_ere_error_employees_1',
  'source' => 'non-db',
  'module' => 'Ere_Error_Employees',
  'bean_name' => 'Ere_Error_Employees',
  'vname' => 'LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE',
  'id_name' => 'm06_error_ere_error_employees_1ere_error_employees_idb',
);
