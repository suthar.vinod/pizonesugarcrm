<?php
// created: 2021-02-11 08:58:28
$dictionary["TSD1_Test_System_Design_1"]["fields"]["m03_work_product_tsd1_test_system_design_1_1"] = array (
  'name' => 'm03_work_product_tsd1_test_system_design_1_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_tsd1_test_system_design_1_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_TSD1_TEST_SYSTEM_DESIGN_1_TITLE',
  'id_name' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["TSD1_Test_System_Design_1"]["fields"]["m03_work_product_tsd1_test_system_design_1_1_name"] = array (
  'name' => 'm03_work_product_tsd1_test_system_design_1_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
  'link' => 'm03_work_product_tsd1_test_system_design_1_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["TSD1_Test_System_Design_1"]["fields"]["m03_work_product_tsd1_test_system_design_1_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_TSD1_TEST_SYSTEM_DESIGN_1_TITLE_ID',
  'id_name' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
  'link' => 'm03_work_product_tsd1_test_system_design_1_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
