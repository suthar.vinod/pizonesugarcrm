<?php
// created: 2022-09-22 05:28:30
$dictionary["GD_Group_Design"]["fields"]["bid_batch_id_gd_group_design_1"] = array (
  'name' => 'bid_batch_id_gd_group_design_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_gd_group_design_1',
  'source' => 'non-db',
  'module' => 'BID_Batch_ID',
  'bean_name' => 'BID_Batch_ID',
  'side' => 'right',
  'vname' => 'LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'id_name' => 'bid_batch_id_gd_group_design_1bid_batch_id_ida',
  'link-type' => 'one',
);
$dictionary["GD_Group_Design"]["fields"]["bid_batch_id_gd_group_design_1_name"] = array (
  'name' => 'bid_batch_id_gd_group_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_BID_BATCH_ID_TITLE',
  'save' => true,
  'id_name' => 'bid_batch_id_gd_group_design_1bid_batch_id_ida',
  'link' => 'bid_batch_id_gd_group_design_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'name',
);
$dictionary["GD_Group_Design"]["fields"]["bid_batch_id_gd_group_design_1bid_batch_id_ida"] = array (
  'name' => 'bid_batch_id_gd_group_design_1bid_batch_id_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE_ID',
  'id_name' => 'bid_batch_id_gd_group_design_1bid_batch_id_ida',
  'link' => 'bid_batch_id_gd_group_design_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
