<?php
// created: 2019-07-09 11:54:56
$dictionary["RR_Regulatory_Response"]["fields"]["rr_regulatory_response_m01_sales_1"] = array (
  'name' => 'rr_regulatory_response_m01_sales_1',
  'type' => 'link',
  'relationship' => 'rr_regulatory_response_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_M01_SALES_TITLE',
  'id_name' => 'rr_regulatory_response_m01_sales_1m01_sales_idb',
);
