<?php
// created: 2021-11-09 09:39:34
$dictionary["IM_Inventory_Management"]["fields"]["ii_inventory_item_im_inventory_management_1"] = array (
  'name' => 'ii_inventory_item_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'ii_inventory_item_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'ii_inventory_item_im_inventory_management_1ii_inventory_item_ida',
);
