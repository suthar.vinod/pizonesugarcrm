<?php
// created: 2021-11-09 10:02:11
$dictionary["II_Inventory_Item"]["fields"]["anml_animals_ii_inventory_item_1"] = array (
  'name' => 'anml_animals_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'anml_animals_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'anml_animals_ii_inventory_item_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["anml_animals_ii_inventory_item_1_name"] = array (
  'name' => 'anml_animals_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_ii_inventory_item_1anml_animals_ida',
  'link' => 'anml_animals_ii_inventory_item_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["anml_animals_ii_inventory_item_1anml_animals_ida"] = array (
  'name' => 'anml_animals_ii_inventory_item_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'anml_animals_ii_inventory_item_1anml_animals_ida',
  'link' => 'anml_animals_ii_inventory_item_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
