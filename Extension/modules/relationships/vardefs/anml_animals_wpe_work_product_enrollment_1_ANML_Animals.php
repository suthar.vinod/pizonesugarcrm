<?php
// created: 2017-09-12 15:03:53
$dictionary["ANML_Animals"]["fields"]["anml_animals_wpe_work_product_enrollment_1"] = array (
  'name' => 'anml_animals_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'anml_animals_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);
