<?php
// created: 2021-09-14 09:20:50
$dictionary["M06_Error"]["fields"]["w_weight_m06_error_2"] = array (
  'name' => 'w_weight_m06_error_2',
  'type' => 'link',
  'relationship' => 'w_weight_m06_error_2',
  'source' => 'non-db',
  'module' => 'W_Weight',
  'bean_name' => 'W_Weight',
  'side' => 'right',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_2_FROM_M06_ERROR_TITLE',
  'id_name' => 'w_weight_m06_error_2w_weight_ida',
  'link-type' => 'one',
);
$dictionary["M06_Error"]["fields"]["w_weight_m06_error_2_name"] = array (
  'name' => 'w_weight_m06_error_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_2_FROM_W_WEIGHT_TITLE',
  'save' => true,
  'id_name' => 'w_weight_m06_error_2w_weight_ida',
  'link' => 'w_weight_m06_error_2',
  'table' => 'w_weight',
  'module' => 'W_Weight',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["w_weight_m06_error_2w_weight_ida"] = array (
  'name' => 'w_weight_m06_error_2w_weight_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_2_FROM_M06_ERROR_TITLE_ID',
  'id_name' => 'w_weight_m06_error_2w_weight_ida',
  'link' => 'w_weight_m06_error_2',
  'table' => 'w_weight',
  'module' => 'W_Weight',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
