<?php
// created: 2021-11-09 10:02:11
$dictionary["ANML_Animals"]["fields"]["anml_animals_ii_inventory_item_1"] = array (
  'name' => 'anml_animals_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'anml_animals_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_ii_inventory_item_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);
