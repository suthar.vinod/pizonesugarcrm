<?php
// created: 2018-01-31 15:57:04
$dictionary["M06_Error"]["fields"]["m06_error_m03_work_product_1"] = array (
  'name' => 'm06_error_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'm06_error_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm06_error_m03_work_product_1m03_work_product_idb',
);
