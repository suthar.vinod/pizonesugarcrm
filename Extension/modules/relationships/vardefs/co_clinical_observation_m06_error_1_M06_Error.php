<?php
// created: 2020-01-07 20:01:08
$dictionary["M06_Error"]["fields"]["co_clinical_observation_m06_error_1"] = array (
  'name' => 'co_clinical_observation_m06_error_1',
  'type' => 'link',
  'relationship' => 'co_clinical_observation_m06_error_1',
  'source' => 'non-db',
  'module' => 'CO_Clinical_Observation',
  'bean_name' => 'CO_Clinical_Observation',
  'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'id_name' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
);
$dictionary["M06_Error"]["fields"]["co_clinical_observation_m06_error_1_name"] = array (
  'name' => 'co_clinical_observation_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'save' => true,
  'id_name' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
  'link' => 'co_clinical_observation_m06_error_1',
  'table' => 'co_clinical_observation',
  'module' => 'CO_Clinical_Observation',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["co_clinical_observation_m06_error_1co_clinical_observation_ida"] = array (
  'name' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_CO_CLINICAL_OBSERVATION_TITLE_ID',
  'id_name' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
  'link' => 'co_clinical_observation_m06_error_1',
  'table' => 'co_clinical_observation',
  'module' => 'CO_Clinical_Observation',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
