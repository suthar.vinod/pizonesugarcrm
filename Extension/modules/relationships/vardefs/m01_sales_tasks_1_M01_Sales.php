<?php
// created: 2016-01-25 23:00:46
$dictionary["M01_Sales"]["fields"]["m01_sales_tasks_1"] = array (
  'name' => 'm01_sales_tasks_1',
  'type' => 'link',
  'relationship' => 'm01_sales_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_M01_SALES_TASKS_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_tasks_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);
