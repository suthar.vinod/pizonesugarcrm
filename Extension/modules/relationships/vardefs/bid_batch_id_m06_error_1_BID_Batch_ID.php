<?php
// created: 2022-04-26 07:03:59
$dictionary["BID_Batch_ID"]["fields"]["bid_batch_id_m06_error_1"] = array (
  'name' => 'bid_batch_id_m06_error_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_BID_BATCH_ID_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'bid_batch_id_m06_error_1m06_error_idb',
);
