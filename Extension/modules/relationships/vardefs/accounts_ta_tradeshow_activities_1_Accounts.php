<?php
// created: 2018-04-23 17:12:12
$dictionary["Account"]["fields"]["accounts_ta_tradeshow_activities_1"] = array (
  'name' => 'accounts_ta_tradeshow_activities_1',
  'type' => 'link',
  'relationship' => 'accounts_ta_tradeshow_activities_1',
  'source' => 'non-db',
  'module' => 'TA_Tradeshow_Activities',
  'bean_name' => 'TA_Tradeshow_Activities',
  'vname' => 'LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_ta_tradeshow_activities_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
