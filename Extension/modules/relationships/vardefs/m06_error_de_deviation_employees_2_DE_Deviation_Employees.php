<?php
// created: 2022-01-03 10:55:30
$dictionary["DE_Deviation_Employees"]["fields"]["m06_error_de_deviation_employees_2"] = array (
  'name' => 'm06_error_de_deviation_employees_2',
  'type' => 'link',
  'relationship' => 'm06_error_de_deviation_employees_2',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_2_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_de_deviation_employees_2m06_error_ida',
);
