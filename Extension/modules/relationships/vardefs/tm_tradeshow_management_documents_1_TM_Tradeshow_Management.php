<?php
// created: 2018-01-18 22:47:15
$dictionary["TM_Tradeshow_Management"]["fields"]["tm_tradeshow_management_documents_1"] = array (
  'name' => 'tm_tradeshow_management_documents_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'id_name' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
  'link-type' => 'many',
  'side' => 'left',
);
