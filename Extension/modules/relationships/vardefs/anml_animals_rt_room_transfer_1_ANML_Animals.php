<?php
// created: 2019-02-20 13:19:54
$dictionary["ANML_Animals"]["fields"]["anml_animals_rt_room_transfer_1"] = array (
  'name' => 'anml_animals_rt_room_transfer_1',
  'type' => 'link',
  'relationship' => 'anml_animals_rt_room_transfer_1',
  'source' => 'non-db',
  'module' => 'RT_Room_Transfer',
  'bean_name' => 'RT_Room_Transfer',
  'vname' => 'LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);
