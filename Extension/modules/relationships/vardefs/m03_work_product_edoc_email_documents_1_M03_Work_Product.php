<?php
// created: 2019-08-27 11:37:03
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_edoc_email_documents_1"] = array (
  'name' => 'm03_work_product_edoc_email_documents_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_edoc_email_documents_1',
  'source' => 'non-db',
  'module' => 'EDoc_Email_Documents',
  'bean_name' => 'EDoc_Email_Documents',
  'vname' => 'LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);
