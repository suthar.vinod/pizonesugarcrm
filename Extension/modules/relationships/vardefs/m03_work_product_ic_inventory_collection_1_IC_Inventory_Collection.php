<?php
// created: 2021-11-09 10:26:31
$dictionary["IC_Inventory_Collection"]["fields"]["m03_work_product_ic_inventory_collection_1"] = array (
  'name' => 'm03_work_product_ic_inventory_collection_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_ic_inventory_collection_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'id_name' => 'm03_work_product_ic_inventory_collection_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["IC_Inventory_Collection"]["fields"]["m03_work_product_ic_inventory_collection_1_name"] = array (
  'name' => 'm03_work_product_ic_inventory_collection_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_ic_inventory_collection_1m03_work_product_ida',
  'link' => 'm03_work_product_ic_inventory_collection_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["IC_Inventory_Collection"]["fields"]["m03_work_product_ic_inventory_collection_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_ic_inventory_collection_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE_ID',
  'id_name' => 'm03_work_product_ic_inventory_collection_1m03_work_product_ida',
  'link' => 'm03_work_product_ic_inventory_collection_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
