<?php
// created: 2019-08-05 12:02:55
$dictionary["Call"]["fields"]["m03_work_product_activities_1_calls"] = array (
  'name' => 'm03_work_product_activities_1_calls',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_calls',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_CALLS_FROM_M03_WORK_PRODUCT_TITLE',
);
