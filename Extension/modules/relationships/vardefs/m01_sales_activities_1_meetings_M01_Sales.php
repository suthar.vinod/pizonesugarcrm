<?php
// created: 2018-12-10 23:02:13
$dictionary["M01_Sales"]["fields"]["m01_sales_activities_1_meetings"] = array (
  'name' => 'm01_sales_activities_1_meetings',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_meetings',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE',
);
