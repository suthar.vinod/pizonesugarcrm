<?php
// created: 2017-09-13 15:16:58
$dictionary["Email"]["fields"]["a1a_critical_phase_inspectio_activities_1_emails"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_emails',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_emails',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_EMAILS_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
);
