<?php
// created: 2018-06-01 21:24:07
$dictionary["M06_Error"]["fields"]["erqc_error_qc_employees_m06_error_1"] = array (
  'name' => 'erqc_error_qc_employees_m06_error_1',
  'type' => 'link',
  'relationship' => 'erqc_error_qc_employees_m06_error_1',
  'source' => 'non-db',
  'module' => 'ErQC_Error_QC_Employees',
  'bean_name' => 'ErQC_Error_QC_Employees',
  'vname' => 'LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_ERQC_ERROR_QC_EMPLOYEES_TITLE',
  'id_name' => 'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida',
);
