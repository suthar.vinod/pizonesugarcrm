<?php
// created: 2017-09-13 15:16:04
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_activities_1_notes"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_notes',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_notes',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_NOTES_FROM_NOTES_TITLE',
);
