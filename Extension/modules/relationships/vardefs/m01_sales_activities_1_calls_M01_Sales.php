<?php
// created: 2018-12-10 23:01:47
$dictionary["M01_Sales"]["fields"]["m01_sales_activities_1_calls"] = array (
  'name' => 'm01_sales_activities_1_calls',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_calls',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_CALLS_FROM_CALLS_TITLE',
);
