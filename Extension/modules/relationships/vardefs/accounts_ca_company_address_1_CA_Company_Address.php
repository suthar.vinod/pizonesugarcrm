<?php
// created: 2019-07-03 12:14:01
$dictionary["CA_Company_Address"]["fields"]["accounts_ca_company_address_1"] = array (
  'name' => 'accounts_ca_company_address_1',
  'type' => 'link',
  'relationship' => 'accounts_ca_company_address_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_CA_COMPANY_ADDRESS_TITLE',
  'id_name' => 'accounts_ca_company_address_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["CA_Company_Address"]["fields"]["accounts_ca_company_address_1_name"] = array (
  'name' => 'accounts_ca_company_address_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_ca_company_address_1accounts_ida',
  'link' => 'accounts_ca_company_address_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["CA_Company_Address"]["fields"]["accounts_ca_company_address_1accounts_ida"] = array (
  'name' => 'accounts_ca_company_address_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_CA_COMPANY_ADDRESS_TITLE_ID',
  'id_name' => 'accounts_ca_company_address_1accounts_ida',
  'link' => 'accounts_ca_company_address_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
