<?php
 // created: 2021-11-10 02:26:10
$dictionary['IC_Inventory_Collection']['fields']['test_system_c']['labelValue']='Test System(s)';
$dictionary['IC_Inventory_Collection']['fields']['test_system_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['IC_Inventory_Collection']['fields']['test_system_c']['enforced']='';
$dictionary['IC_Inventory_Collection']['fields']['test_system_c']['dependency']='';
$dictionary['IC_Inventory_Collection']['fields']['test_system_c']['required_formula']='';
$dictionary['IC_Inventory_Collection']['fields']['test_system_c']['readonly_formula']='';

 ?>