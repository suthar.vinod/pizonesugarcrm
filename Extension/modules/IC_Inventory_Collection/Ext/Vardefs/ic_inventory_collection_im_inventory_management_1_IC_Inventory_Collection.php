<?php
// created: 2020-07-31 13:42:07
$dictionary["IC_Inventory_Collection"]["fields"]["ic_inventory_collection_im_inventory_management_1"] = array (
  'name' => 'ic_inventory_collection_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'ic_inventory_collection_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'IM_Inventory_Management',
  'bean_name' => 'IM_Inventory_Management',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'id_name' => 'ic_inventoe24election_ida',
  'link-type' => 'many',
  'side' => 'left',
);
