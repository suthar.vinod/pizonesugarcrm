<?php

$dependencies['IC_Inventory_Collection']['wp_required_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('m03_work_product_ic_inventory_collection_1_name'),
    'onload'        => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions'       => array(
        array(
            'name'   => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'm03_work_product_ic_inventory_collection_1_name',
                'label'  => 'work_product_required_label',
                'value'  => 'true'
            ),
        ),
    ),
);