<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_JOB_TITLE'] = 'Job Title';
$mod_strings['LBL_RESUME_REVIEWED'] = 'Resume Reviewed';
$mod_strings['LBL_INTERVIEW_SETUP_REQUESTED'] = 'Interview Setup Requested';
$mod_strings['LBL_CANDIDATE_NAME'] = 'Candidate Name';
$mod_strings['LBL_NAME'] = 'Name';
$mod_strings['LBL_JOB_CATEGORY'] = 'Job Category';
$mod_strings['LBL_CURRENT_EMPLOYEE'] = 'Current Employee';
$mod_strings['LBL_HR01_RESUMES_FOCUS_DRAWER_DASHBOARD'] = 'Resumes Focus Drawer';
