<?php


$dependencies['ORI_Order_Request_Item']['status_reada11123'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_ori_order_request_item_1_name',
        'order_date',
        'status'
    ),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    // You could list multiple fields here each in their own array under 'actions'
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'status',
                'keys' => 'ifElse(and(
                    not(equal($request_date,"")),
                    not(equal(related($po_purchase_order_ori_order_request_item_1,"name"),"")),
                    equal(related($po_purchase_order_ori_order_request_item_1,"status_c"),"Submitted"),
                    equal(related($ori_order_request_item_ri_received_items_1,"name"),"")),getDropdownKeySet("ORI_or_status_list"),getDropdownKeySet("ORI_status_list"))',
                'labels' => 'ifElse(and(
                    not(equal($request_date,"")),
                    not(equal(related($po_purchase_order_ori_order_request_item_1,"name"),"")),
                    equal(related($po_purchase_order_ori_order_request_item_1,"status_c"),"Submitted"),
                    equal(related($ori_order_request_item_ri_received_items_1,"name"),"")),getDropdownValueSet("ORI_or_status_list"),getDropdownValueSet("ORI_status_list"))'
            ),
        ),
        
    ),
);

$dependencies['ORI_Order_Request_Item']['status3_reada11jkk1245a1hh'] = array(
    'hooks' => array("all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'ifElse(
        and(
        not(equal($request_date,"")),
        equal(related($po_purchase_order_ori_order_request_item_1,"name"),""),
        equal(related($ori_order_request_item_ri_received_items_1,"name"),"")),
        "true","false")', //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'received_date',
        'status'
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true'
            ),
        ),
    )
);

$dependencies['ORI_Order_Request_Item']['status_r2eada11jkk1245a1hh'] = array(
    'hooks' => array("all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'ifElse(
        and(
            equal($status,"Requested"),
            not(equal($request_date,"")),
            not(equal(related($po_purchase_order_ori_order_request_item_1,"name"),"")),
            equal(related($po_purchase_order_ori_order_request_item_1,"status_c"),"Pending"),            
            equal(related($ori_order_request_item_ri_received_items_1,"name"),"")),
        "true","false")', //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'received_date',
        'status'
    ),
    'onload' => true,
    'actions' => array(

        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true'
            ),
        ),
    )
);

$dependencies['ORI_Order_Request_Item']['statusg1_r2eada11jkk1245a1hh'] = array(
    'hooks' => array("all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'ifElse(and(
            not(equal($request_date,"")),
            not(equal(related($po_purchase_order_ori_order_request_item_1,"name"),"")),
            equal(related($po_purchase_order_ori_order_request_item_1,"status_c"),"Submitted"),          
            equal(related($ori_order_request_item_ri_received_items_1,"name"),"")),"false","true")', //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'received_date',
        'status'
    ),
    'onload' => true,
    'actions' => array(

        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'false'
            ),
        ),
    )
);

$dependencies['ORI_Order_Request_Item']['stgatusg1_r2eadafsd11jkk1245a1hh'] = array(
    'hooks' => array("all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'ifElse(and(
        not(equal($request_date,"")),
        not(equal(related($po_purchase_order_ori_order_request_item_1,"name"),"")),
        equal(related($po_purchase_order_ori_order_request_item_1,"status_c"),"Submitted"),
        not(equal(related($ori_order_request_item_ri_received_items_1,"name"),""))),"true","false")', //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'received_date',
        'status'
    ),
    'onload' => true,
    'actions' => array(

        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true'
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'ifElse(or(equal($status,"Ordered"),equal($status,"Backordered")),false,true)'
            ),
        ),
    )
);


/* 06-April */

$dependencies['ORI_Order_Request_Item']['status_readLux'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'received_date',
        'status',
    ),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    // You could list multiple fields here each in their own array under 'actions'
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'status',
                'keys' => 'ifElse(or(equal($status,"Ordered"),equal($status,"Backordered")),getDropdownKeySet("ORI_or_status_list"),getDropdownKeySet("ORI_status_list"))',
                'labels' => 'ifElse(or(equal($status,"Ordered"),equal($status,"Backordered")),getDropdownValueSet("ORI_or_status_list"),getDropdownValueSet("ORI_status_list"))'
            ),
        ),
    ),
);

$dependencies['ORI_Order_Request_Item']['fr33status_reada11a1245'] = array(
    'hooks' => array("edit","all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'and(equal($status,"Ordered"),equal($status,"Backordered"))',
    'triggerFields' => array(
        'status',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'false'
            ),
        ),
    )
);


$dependencies['ORI_Order_Request_Item']['fr33status_reada11a1245'] = array(
    'hooks' => array("edit","all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'and(equal($status,"Ordered"),equal($status,"Backordered"))',
    'triggerFields' => array(
        'status',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'false'
            ),
        ),
    )
);



$dependencies['ORI_Order_Request_Item']['fr33status_reada111d245sd'] = array(
    'hooks' => array("edit","all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'equal($status,"Inventory")',
    'triggerFields' => array(
        'status',
        'id'
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'setValue',
            'params' => array(
                'target' => 'status',
                'value' => 'Inventory'
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true'
            ),
        ),
        
    )
);

$dependencies['ORI_Order_Request_Item']['fr33status_reada11v1245sdaaaaa'] = array(
    'hooks' => array("edit","all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'equal($status,"Partially Received")',
    'triggerFields' => array(
        'status',
        'id'
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'setValue',
            'params' => array(
                'target' => 'status',
                'value' => 'Partially Received'
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true'
            ),
        ),
    )
);

