<?php

$dependencies['ORI_Order_Request_Item']['set_or_required12'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('or_order_request_ori_order_request_item_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'or_order_request_ori_order_request_item_1_name',
                'value' => 'true',
            ),
        ),
    ),
);
