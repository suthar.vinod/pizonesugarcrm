<?php

$dependencies['ORI_Order_Request_Item']['cost_per_unit_readonly'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array(
        'cost_per_unit',
       // 'id',
        //'po_purchase_order_ori_order_request_item_1_name',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'cost_per_unit',
                'value' => 'equal($po_purchase_order_ori_order_request_item_1_name,"")',
            ),
        ),
    ),
);

$dependencies['ORI_Order_Request_Item']['subtype_field_readonly01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('product_selection','type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'subtype',
                'value' => 'ifElse(or(equal($product_selection,"In System Vendor Non Specific"),equal($product_selection,"In System Vendor Specific")),true,false)',
            ),
        ),
    ),
);