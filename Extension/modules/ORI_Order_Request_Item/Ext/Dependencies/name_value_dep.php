<?php

$dependencies['ORI_Order_Request_Item']['set_visibility'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('related_to'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm01_sales_ori_order_request_item_1_name',
                'value' => 'equal($related_to,"Sales")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_ori_order_request_item_1_name',
                'value' => 'equal($related_to,"Work Product")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm01_sales_ori_order_request_item_1_name',
                'value' => 'equal($related_to, "Sales")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_ori_order_request_item_1_name',
                'value' => 'equal($related_to, "Work Product")',
            ),
        ),
    ),
);

$dependencies['ORI_Order_Request_Item']['set_visibility_re'] = array(
    'hooks' => array("edit", "view"),
    'trigger' => 'true',
    'triggerFields' => array('product_selection', 'department_new_c', 'type_2'),
    'onload' => false,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'prod_product_ori_order_request_item_1_name',
                'value' => 'and(isInList($product_selection, createList("In System Vendor Non Specific", "In System Vendor Specific")), greaterThan(strlen($department_new_c),0), greaterThan(strlen($type_2),0))',
            ),
        ),
    ),
);


$dependencies['ORI_Order_Request_Item']['setStatus'] = array(
    'hooks' => array("edit", "view"),
    'trigger' => 'true',
    'triggerFields' => array(
        'status',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'status',
                'keys' => 'ifElse(isInList($status, createList("Ordered", "Backordered")), getDropdownKeySet("ORI_or_status_list"), getDropdownKeySet("ORI_status_list"))',
                'labels' => 'ifElse(isInList($status, createList("Ordered", "Backordered")), getDropdownKeySet("ORI_or_status_list"), getDropdownKeySet("ORI_status_list"))',
            ),
        ),
    ),
);

$dependencies['ORI_Order_Request_Item']['status_read'] = array(
    'hooks' => array("edit", "view"),
    'trigger' => 'true',
    'triggerFields' => array(
        'status',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'ifElse(isInList($status, createList("Requested", "Inventory")),true,false)',
            ),
        ),
    ),
);
$dependencies['ORI_Order_Request_Item']['statusasasasa_read'] = array(
    'hooks' => array("edit", "view"),
    'trigger' => 'true',
    'triggerFields' => array(
        'status',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'ifElse(isInList($status, createList("Ordered", "Backordered")),false,true)',
            ),
        ),
    ),
);


/* 
$dependencies['ORI_Order_Request_Item']['read_only_type_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'cost_per_unit',
                'value' => 'ifElse(equal($po_purchase_order_ori_order_request_item_1_name,""),true,false)',
            ),
        ),
    ),
); */