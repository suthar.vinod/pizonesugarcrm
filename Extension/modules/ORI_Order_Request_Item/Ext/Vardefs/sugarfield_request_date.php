<?php
 // created: 2020-07-31 13:04:48
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['massupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['importable']='false';
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['duplicate_merge']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['calculated']='true';
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['formula']='related($or_order_request_ori_order_request_item_1,"request_date")';
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['enforced']=true;

 ?>