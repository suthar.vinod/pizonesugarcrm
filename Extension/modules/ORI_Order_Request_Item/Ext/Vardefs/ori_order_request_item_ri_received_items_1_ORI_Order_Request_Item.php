<?php
// created: 2021-08-04 19:53:25
$dictionary["ORI_Order_Request_Item"]["fields"]["ori_order_request_item_ri_received_items_1"] = array (
  'name' => 'ori_order_request_item_ri_received_items_1',
  'type' => 'link',
  'relationship' => 'ori_order_request_item_ri_received_items_1',
  'source' => 'non-db',
  'module' => 'RI_Received_Items',
  'bean_name' => 'RI_Received_Items',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'ori_order_6b32st_item_ida',
  'link-type' => 'many',
  'side' => 'left',
);
