<?php
 // created: 2021-10-19 11:34:56
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['name']='m01_sales_ori_order_request_item_1m01_sales_ida';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['type']='id';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['source']='non-db';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['vname']='LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['id_name']='m01_sales_ori_order_request_item_1m01_sales_ida';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['link']='m01_sales_ori_order_request_item_1';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['table']='m01_sales';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['module']='M01_Sales';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['rname']='id';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['side']='right';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['massupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['duplicate_merge']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['hideacl']=true;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['importable']='true';

 ?>