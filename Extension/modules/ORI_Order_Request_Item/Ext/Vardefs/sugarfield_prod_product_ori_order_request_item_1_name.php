<?php
 // created: 2021-10-19 11:37:06
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['massupdate']=true;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['duplicate_merge']='enabled';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['duplicate_merge_dom_value']='1';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['merge_filter']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['unified_search']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['calculated']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['vname']='LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE';

 ?>