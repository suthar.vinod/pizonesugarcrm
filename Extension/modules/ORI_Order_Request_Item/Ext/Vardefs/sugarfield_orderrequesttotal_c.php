<?php
 // created: 2021-08-10 21:20:15
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['labelValue']='orderrequesttotal';
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['calculated']='true';
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['formula']='multiply($cost_per_unit,$unit_quantity_requested)';
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['enforced']='true';
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['dependency']='';
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['required_formula']='';
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['readonly_formula']='';

 ?>