<?php
// created: 2020-07-31 12:47:27
$dictionary["ORI_Order_Request_Item"]["fields"]["prod_product_ori_order_request_item_1"] = array (
  'name' => 'prod_product_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'prod_product_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'Prod_Product',
  'bean_name' => 'Prod_Product',
  'side' => 'right',
  'vname' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'prod_product_ori_order_request_item_1prod_product_ida',
  'link-type' => 'one',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["prod_product_ori_order_request_item_1_name"] = array (
  'name' => 'prod_product_ori_order_request_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_PROD_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'prod_product_ori_order_request_item_1prod_product_ida',
  'link' => 'prod_product_ori_order_request_item_1',
  'table' => 'prod_product',
  'module' => 'Prod_Product',
  'rname' => 'name',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["prod_product_ori_order_request_item_1prod_product_ida"] = array (
  'name' => 'prod_product_ori_order_request_item_1prod_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID',
  'id_name' => 'prod_product_ori_order_request_item_1prod_product_ida',
  'link' => 'prod_product_ori_order_request_item_1',
  'table' => 'prod_product',
  'module' => 'Prod_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
