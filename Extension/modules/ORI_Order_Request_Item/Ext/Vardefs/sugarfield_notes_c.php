<?php
 // created: 2022-03-31 11:40:06
$dictionary['ORI_Order_Request_Item']['fields']['notes_c']['labelValue']='Notes';
$dictionary['ORI_Order_Request_Item']['fields']['notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ORI_Order_Request_Item']['fields']['notes_c']['enforced']='';
$dictionary['ORI_Order_Request_Item']['fields']['notes_c']['dependency']='';
$dictionary['ORI_Order_Request_Item']['fields']['notes_c']['required_formula']='';
$dictionary['ORI_Order_Request_Item']['fields']['notes_c']['readonly_formula']='';

 ?>