<?php
 // created: 2021-10-19 11:37:05
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['name']='prod_product_ori_order_request_item_1prod_product_ida';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['type']='id';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['source']='non-db';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['vname']='LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['id_name']='prod_product_ori_order_request_item_1prod_product_ida';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['link']='prod_product_ori_order_request_item_1';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['table']='prod_product';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['module']='Prod_Product';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['rname']='id';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['side']='right';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['massupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['duplicate_merge']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['hideacl']=true;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['importable']='true';

 ?>