<?php
 // created: 2021-12-02 06:29:40
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['labelValue']='In System Product ID';
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['calculated']='true';
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['formula']='related($prod_product_ori_order_request_item_1,"product_id_2")';
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['enforced']='true';
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['dependency']='';
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['required_formula']='';
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['readonly_formula']='';

 ?>