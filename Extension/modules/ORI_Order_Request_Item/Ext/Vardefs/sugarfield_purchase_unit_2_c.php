<?php
 // created: 2021-02-26 12:07:46
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['labelValue']='Purchase Unit';
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['calculated']='1';
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['formula']='getDropdownValue("purchase_unit_list",related($prod_product_ori_order_request_item_1,"purchase_unit"))';
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['enforced']='1';
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['dependency']='not(isInList($product_selection,createList("Not In System Vendor Non Specific","Not In System Vendor Specific")))';
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['required_formula']='';

 ?>