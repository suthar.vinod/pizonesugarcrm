<?php
// created: 2021-11-09 09:46:03
$dictionary["ORI_Order_Request_Item"]["fields"]["ori_order_request_item_ii_inventory_item_1"] = array (
  'name' => 'ori_order_request_item_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'ori_order_request_item_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'ori_order_2123st_item_ida',
  'link-type' => 'many',
  'side' => 'left',
);
