<?php
 // created: 2021-10-19 11:35:30
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['name']='m03_work_product_ori_order_request_item_1m03_work_product_ida';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['type']='id';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['source']='non-db';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['vname']='LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['id_name']='m03_work_product_ori_order_request_item_1m03_work_product_ida';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['link']='m03_work_product_ori_order_request_item_1';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['table']='m03_work_product';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['module']='M03_Work_Product';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['rname']='id';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['side']='right';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['massupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['duplicate_merge']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['hideacl']=true;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['importable']='true';

 ?>