<?php
 // created: 2022-03-23 12:35:46
$dictionary['ORI_Order_Request_Item']['fields']['subtype']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['subtype']['visibility_grid']=array (
  'trigger' => 'type_2',
  'values' => 
  array (
    'Balloon Catheter' => 
    array (
      0 => '',
      1 => 'Angioplasty Balloon',
      2 => 'Cutting Balloon',
      3 => 'Drug Coated Balloon',
      4 => 'Other',
      5 => 'OTW over the wire',
      6 => 'POBA Balloon',
      7 => 'PTA Balloon',
    ),
    'Cannula' => 
    array (
      0 => '',
      1 => 'Aortic Root Cannula',
      2 => 'Arterial Cannula',
      3 => 'Other',
      4 => 'Pediatric Cannula',
      5 => 'Venous Cannula',
      6 => 'Vessel Cannula',
    ),
    'Catheter' => 
    array (
      0 => '',
      1 => 'Access Catheter',
      2 => 'Butterfly Catheter',
      3 => 'Central Venous Catheter',
      4 => 'Diagnostic Catheter',
      5 => 'EP Catheter',
      6 => 'Extension Catheter',
      7 => 'Foley Catheter',
      8 => 'Guide Catheter',
      9 => 'Imaging Catheter',
      10 => 'IV Catheter',
      11 => 'Mapping Catheter',
      12 => 'Micro Catheter',
      13 => 'Mila Catheter',
      14 => 'Other',
      15 => 'Pressure Catheter',
      16 => 'Support Catheter',
      17 => 'Transseptal Catheter',
    ),
    'Chemical' => 
    array (
      0 => '',
      1 => 'Analytes',
      2 => 'Control Matrix',
      3 => 'Corrosive Solution',
      4 => 'Flammable',
      5 => 'Flammable Solution',
      6 => 'Non Flammable',
      7 => 'Solvents Aqueous',
      8 => 'Solvents Organic',
      9 => 'Toxic Solution',
    ),
    'Cleaning Agent' => 
    array (
    ),
    'Contrast' => 
    array (
      0 => '',
      1 => 'Barium Contrast',
      2 => 'Iodinated Contrast',
      3 => 'Other',
    ),
    'Drug' => 
    array (
      0 => '',
      1 => 'Inhalant',
      2 => 'Injectable',
      3 => 'Intranasal',
      4 => 'IV Solution',
      5 => 'Oral',
      6 => 'Other',
      7 => 'Topical',
    ),
    'Graft' => 
    array (
      0 => '',
      1 => 'Coated',
      2 => 'ePTFE',
      3 => 'Knitted',
      4 => 'Other',
      5 => 'Patches',
      6 => 'Woven',
    ),
    'Inflation Device' => 
    array (
      0 => '',
      1 => 'High Pressure',
      2 => 'Other',
    ),
    'Reagent' => 
    array (
      0 => '',
      1 => 'Flammable',
      2 => 'Non Flammable',
    ),
    'Sheath' => 
    array (
      0 => '',
      1 => 'Introducer Sheath',
      2 => 'Other',
      3 => 'Peel Away',
      4 => 'Steerable Sheath',
    ),
    'Stent' => 
    array (
      0 => '',
      1 => 'Bare Metal Stent',
      2 => 'Drug Coated Stent',
      3 => 'Other',
    ),
    'Suture' => 
    array (
      0 => '',
      1 => 'Ethibond',
      2 => 'Ethilon',
      3 => 'MonoAdjustable Loop',
      4 => 'Monocryl',
      5 => 'Monoderm',
      6 => 'Other',
      7 => 'PDO',
      8 => 'PDS',
      9 => 'Pledget',
      10 => 'Prolene',
      11 => 'Silk',
      12 => 'Ti Cron',
      13 => 'Vicryl',
    ),
    'Wire' => 
    array (
      0 => '',
      1 => 'Exchange Wire',
      2 => 'Guide Wire',
      3 => 'Marking Wire',
      4 => 'Other',
      5 => 'Support Wire',
    ),
    '' => 
    array (
    ),
    'Autoclave supplies' => 
    array (
    ),
    'Bandaging' => 
    array (
      0 => '',
      1 => 'Dressing',
      2 => 'Sub layer wrap',
      3 => 'Tape',
      4 => 'Top layer wrap',
      5 => 'Other',
    ),
    'Blood draw supply' => 
    array (
      0 => '',
      1 => 'Cryovial',
      2 => 'Extension line',
      3 => 'Injection port',
      4 => 'Needle',
      5 => 'Needless port',
      6 => 'Other',
      7 => 'Syringe',
      8 => 'Vacutainer',
    ),
    'Cleaning Supplies' => 
    array (
      0 => '',
      1 => 'Bottle',
      2 => 'Brush',
      3 => 'Floor scrubber supplies',
      4 => 'Other',
      5 => 'SpongePad',
      6 => 'Trash bag',
    ),
    'Endotracheal TubeSupplies' => 
    array (
      0 => '',
      1 => 'Adaptor',
      2 => 'Breathing circuit',
      3 => 'Brush',
      4 => 'ET Tube',
      5 => 'Other',
      6 => 'Stylet',
    ),
    'Enrichment Toy' => 
    array (
    ),
    'Equipment' => 
    array (
      0 => '',
      1 => 'GCMS Columns',
      2 => 'GCMS Parts',
      3 => 'ICPMS Parts',
      4 => 'LCMS Columns',
      5 => 'LCMS Parts',
      6 => 'Machine',
      7 => 'Other',
      8 => 'Replacement Part',
      9 => 'Software',
    ),
    'ETO supplies' => 
    array (
    ),
    'Feeding supplies' => 
    array (
      0 => '',
      1 => 'Enrichment treats',
      2 => 'Feeding Tube',
      3 => 'Other',
      4 => 'Primary diet',
      5 => 'Supplement diet',
    ),
    'Office Supply' => 
    array (
      0 => '',
      1 => 'Binders and supplies',
      2 => 'Label',
      3 => 'Laminating Sheets',
      4 => 'Other',
      5 => 'Paper',
    ),
    'Other' => 
    array (
      0 => '',
      1 => 'Contrast Spike',
      2 => 'Dressing',
      3 => 'Essentials Kit',
      4 => 'Invasive Blood Pressure Supplies',
      5 => 'Other',
      6 => 'Perc Needle',
      7 => 'Probe Cover',
      8 => 'Stopcock',
      9 => 'Torque Device',
      10 => 'Tuohy',
    ),
    'Shipping supplies' => 
    array (
      0 => '',
      1 => 'CD case',
      2 => 'Container',
      3 => 'Cold Pack',
      4 => 'Corrugated box',
      5 => 'Dry Ice',
      6 => 'Indicator label',
      7 => 'Insulated shipper',
      8 => 'Other',
      9 => 'Padding',
      10 => 'Pallet',
      11 => 'Tape',
      12 => 'Wrap',
    ),
    'Solution' => 
    array (
    ),
    'Surgical Instrument' => 
    array (
    ),
    'Surgical site prep' => 
    array (
      0 => '',
      1 => 'Incision site cover',
      2 => 'Other',
      3 => 'Surgical scrub',
    ),
    'Tubing' => 
    array (
      0 => '',
      1 => 'Adaptor',
      2 => 'Connector',
      3 => 'Extension Tubing',
      4 => 'High Pressure Tubing',
      5 => 'Other',
      6 => 'Reducer',
      7 => 'WYE',
    ),
    'Bypass supply' => 
    array (
      0 => '',
      1 => 'Hemoconcentrator',
      2 => 'Other',
      3 => 'Oxygenator',
      4 => 'Pump pack',
      5 => 'Reservoir',
      6 => 'Tubing',
    ),
    'PPE supply' => 
    array (
      0 => '',
      1 => 'Boots',
      2 => 'Bouffanthair net',
      3 => 'Coveralloverall',
      4 => 'Ear protection',
      5 => 'Face shield',
      6 => 'Gloves non sterile',
      7 => 'Gloves sterile',
      8 => 'Mask',
      9 => 'Other',
      10 => 'Safety glasses',
      11 => 'Shoe covers',
      12 => 'Surgical gown non sterile',
      13 => 'Surgical gown sterile',
    ),
    'Control Matrix' => 
    array (
      0 => '',
      1 => 'Fluid',
      2 => 'Other',
      3 => 'Tissue',
    ),
    'Interventional Supplies' => 
    array (
      0 => '',
      1 => 'Other',
    ),
    'Surgical Supplies' => 
    array (
      0 => '',
      1 => 'Aortic Punch',
      2 => 'Biopsy Punch',
      3 => 'Blade',
      4 => 'Cautery Supply',
      5 => 'CT supply',
      6 => 'Drainage supply',
      7 => 'Drape',
      8 => 'ECG supply',
      9 => 'Gauze',
      10 => 'Hand Scrub',
      11 => 'Other',
      12 => 'Patient Drape',
      13 => 'SharpsBiohazard',
      14 => 'Sponge',
      15 => 'Table drape',
    ),
    'Glassware' => 
    array (
      0 => '',
      1 => 'Beaker',
      2 => 'Beakers',
      3 => 'Bottle',
      4 => 'Consumable',
      5 => 'Flask',
      6 => 'Grade A Glassware',
      7 => 'Other',
    ),
    'Plastic Ware' => 
    array (
      0 => '',
      1 => 'Beaker',
      2 => 'Bottle',
      3 => 'Consumable',
      4 => 'Flask',
      5 => 'Other',
      6 => 'Pipette Tips',
      7 => 'Tubes',
      8 => 'Vial Closures',
    ),
    'Lab Supplies' => 
    array (
      0 => '',
      1 => 'Misc',
      2 => 'Storage container',
      3 => 'Storage Racks',
    ),
  ),
);
$dictionary['ORI_Order_Request_Item']['fields']['subtype']['required']=false;

 ?>