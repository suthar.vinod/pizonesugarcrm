<?php
 // created: 2021-02-16 18:39:35
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['labelValue']='Unit Quantity';
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['calculated']='true';
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['formula']='related($prod_product_ori_order_request_item_1,"unit_quantity")';
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['enforced']='true';
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['dependency']='not(isInList($product_selection,createList("Not In System Vendor Non Specific","Not In System Vendor Specific")))';

 ?>