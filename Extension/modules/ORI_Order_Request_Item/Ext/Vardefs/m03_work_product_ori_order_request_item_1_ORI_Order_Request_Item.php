<?php
// created: 2020-07-31 12:46:07
$dictionary["ORI_Order_Request_Item"]["fields"]["m03_work_product_ori_order_request_item_1"] = array (
  'name' => 'm03_work_product_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["m03_work_product_ori_order_request_item_1_name"] = array (
  'name' => 'm03_work_product_ori_order_request_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
  'link' => 'm03_work_product_ori_order_request_item_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["m03_work_product_ori_order_request_item_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID',
  'id_name' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
  'link' => 'm03_work_product_ori_order_request_item_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
