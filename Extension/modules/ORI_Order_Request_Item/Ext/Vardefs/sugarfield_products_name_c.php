<?php
 // created: 2022-02-22 08:11:36
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['labelValue']='Product\'s Name';
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['calculated']='true';
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['formula']='concat($product_name,related($prod_product_ori_order_request_item_1,"name"))';
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['enforced']='true';
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['dependency']='';
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['required_formula']='';
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['readonly_formula']='';

 ?>