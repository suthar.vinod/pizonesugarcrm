<?php
 // created: 2021-10-19 11:36:20
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['name']='or_order_request_ori_order_request_item_1or_order_request_ida';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['type']='id';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['source']='non-db';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['vname']='LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['id_name']='or_order_request_ori_order_request_item_1or_order_request_ida';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['link']='or_order_request_ori_order_request_item_1';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['table']='or_order_request';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['module']='OR_Order_Request';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['rname']='id';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['side']='right';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['massupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['duplicate_merge']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['hideacl']=true;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['importable']='true';

 ?>