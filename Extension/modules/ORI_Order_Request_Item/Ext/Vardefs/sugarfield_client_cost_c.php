<?php
 // created: 2021-02-16 19:18:10
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['labelValue']='Client Cost';
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['calculated']='1';
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['formula']='related($prod_product_ori_order_request_item_1,"client_cost_c")';
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['enforced']='1';
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['dependency']='and(equal($owner,"APS Supplied Client Owned"),not(isInList($product_selection,createList("Not In System Vendor Non Specific","Not In System Vendor Specific"))))';
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['required_formula']='';

 ?>