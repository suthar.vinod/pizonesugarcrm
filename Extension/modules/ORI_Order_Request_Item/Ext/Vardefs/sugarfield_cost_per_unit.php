<?php
 // created: 2021-07-30 04:58:51
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['required']=true;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['name']='cost_per_unit';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['vname']='LBL_COST_PER_UNIT';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['type']='currency';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['massupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['no_default']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['comments']='';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['help']='';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['importable']='true';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['duplicate_merge']='enabled';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['duplicate_merge_dom_value']='1';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['audited']=true;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['reportable']=true;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['unified_search']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['merge_filter']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['pii']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['default']='';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['calculated']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['dependency']='not(isInList($product_selection,createList("Not In System Vendor Non Specific","Not In System Vendor Specific")))';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['len']=26;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['size']='20';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['enable_range_search']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['precision']=6;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['convertToBase']=true;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['showTransactionalAmount']=true;

 ?>