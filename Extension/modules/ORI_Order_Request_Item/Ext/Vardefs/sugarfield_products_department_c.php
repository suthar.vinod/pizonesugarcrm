<?php
 // created: 2021-12-14 06:04:40
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['labelValue']='Product\'s Department(s)';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['calculated']='true';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['formula']='related($prod_product_ori_order_request_item_1,"department")';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['enforced']='true';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['dependency']='';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['required_formula']='';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['readonly']='1';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['readonly_formula']='';

 ?>