<?php

$hook_array['before_save'][] = Array(
    1,
    'append a sequence of numbers at the name of the Item.',
    'custom/src/wsystems/ORI/LogicHooks/SetORINameSequence.php',
    'SetNameSequenceForORI',
    'setName'
);

/* $hook_array['before_save'][] = Array(

  5,

  'append a sequence of numbers at the name of the Item.',

  'custom/src/wsystems/ORI/LogicHooks/SetORINameSequence.php',

  'SetNameSequenceForORI',

  'setStatusBefore'
  );

  $hook_array['after_save'][] = Array(

  10,

  'append a sequence of numbers at the name of the Item.',

  'custom/src/wsystems/ORI/LogicHooks/SetORINameSequence.php',

  'SetNameSequenceForORI',

  'setStatusMeth'
  ); */

$hook_array['after_relationship_delete'][] = Array(
    10,
    'Update the ORI Status after II delete.',
    'custom/src/wsystems/ORI/LogicHooks/SetORIStatus.php',
    'SetStatusForORI',
    'setStatus',
);

$hook_array['after_relationship_add'][] = Array(
    10,
    'aUpdate the ORI Status after II add.',
    'custom/src/wsystems/ORI/LogicHooks/SetORIStatus.php',
    'SetStatusForORI',
    'setStatus',
);



