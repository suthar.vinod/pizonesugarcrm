<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_CLIENT_COST'] = 'Client Cost';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_DEPARTMENT_NEW'] = 'Department';
$mod_strings['LBL_PURCHASE_UNIT_2'] = 'Purchase Unit';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_ORDERREQUESTTOTAL'] = 'orderrequesttotal';
$mod_strings['LBL_PURCHASE_UNIT'] = 'Purchase Unit (Obsolete)';
$mod_strings['LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE'] = 'Sales Activity';
$mod_strings['LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_M01_SALES_TITLE'] = 'Sales Activity';
$mod_strings['LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE'] = 'Work Product';
$mod_strings['LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Product';
$mod_strings['LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE'] = 'Order Request';
$mod_strings['LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_OR_ORDER_REQUEST_TITLE'] = 'Order Request';
$mod_strings['LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE'] = 'Product';
$mod_strings['LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_PROD_PRODUCT_TITLE'] = 'Product';
$mod_strings['LBL_IN_SYSTEM_PRODUCT_ID_C'] = 'In System Product ID';
$mod_strings['LBL_PRODUCTS_DEPARTMENT'] = 'Product&#039;s Department(s)';
$mod_strings['LBL_PRODUCTS_NAME'] = 'Product&#039;s Name';
$mod_strings['LBL_OR_REQUEST_BY_C_USER_ID'] = 'Request By (related User ID)';
$mod_strings['LBL_OR_REQUEST_BY_C'] = 'Request By';
$mod_strings['LBL_NOTES_C'] = 'Notes';
