<?php
 // created: 2020-03-02 13:01:40
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['audited']=true;
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['massupdate']=false;
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['merge_filter']='disabled';
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['unified_search']=false;
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['calculated']=false;

 ?>