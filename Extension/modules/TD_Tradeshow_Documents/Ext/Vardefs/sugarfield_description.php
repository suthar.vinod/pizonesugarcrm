<?php
 // created: 2021-01-14 09:52:20
$dictionary['TD_Tradeshow_Documents']['fields']['description']['audited']=true;
$dictionary['TD_Tradeshow_Documents']['fields']['description']['massupdate']=false;
$dictionary['TD_Tradeshow_Documents']['fields']['description']['hidemassupdate']=false;
$dictionary['TD_Tradeshow_Documents']['fields']['description']['comments']='Full text of the note';
$dictionary['TD_Tradeshow_Documents']['fields']['description']['duplicate_merge']='enabled';
$dictionary['TD_Tradeshow_Documents']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['TD_Tradeshow_Documents']['fields']['description']['merge_filter']='disabled';
$dictionary['TD_Tradeshow_Documents']['fields']['description']['unified_search']=false;
$dictionary['TD_Tradeshow_Documents']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['TD_Tradeshow_Documents']['fields']['description']['calculated']=false;
$dictionary['TD_Tradeshow_Documents']['fields']['description']['rows']='6';
$dictionary['TD_Tradeshow_Documents']['fields']['description']['cols']='80';

 ?>