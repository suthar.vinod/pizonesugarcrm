<?php
// created: 2020-01-07 13:48:18
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_m06_error_1"] = array (
  'name' => 'cie_clinical_issue_exam_m06_error_1',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_m06_error_1_name"] = array (
  'name' => 'cie_clinical_issue_exam_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'save' => true,
  'id_name' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
  'link' => 'cie_clinical_issue_exam_m06_error_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'name',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_m06_error_1m06_error_idb"] = array (
  'name' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_M06_ERROR_TITLE_ID',
  'id_name' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
  'link' => 'cie_clinical_issue_exam_m06_error_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
