<?php
// created: 2019-12-20 17:02:38
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["anml_animals_cie_clinical_issue_exam_1"] = array (
  'name' => 'anml_animals_cie_clinical_issue_exam_1',
  'type' => 'link',
  'relationship' => 'anml_animals_cie_clinical_issue_exam_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'id_name' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["anml_animals_cie_clinical_issue_exam_1_name"] = array (
  'name' => 'anml_animals_cie_clinical_issue_exam_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'required' => true,
  'id_name' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
  'link' => 'anml_animals_cie_clinical_issue_exam_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["anml_animals_cie_clinical_issue_exam_1anml_animals_ida"] = array (
  'name' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE_ID',
  'id_name' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
  'link' => 'anml_animals_cie_clinical_issue_exam_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);