<?php
 // created: 2020-03-17 11:27:51
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['required']=false;
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['unified_search']=false;
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['importable']='false';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['duplicate_merge']='disabled';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['merge_filter']='disabled';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['calculated']='true';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['formula']='concat($test_system_name_c," ",toString($exam_datetime)," ",$cie_type_text_c)';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['enforced']=true;

 ?>