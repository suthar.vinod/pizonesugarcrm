<?php
// created: 2020-01-07 13:50:42
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_2"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  'source' => 'non-db',
  'module' => 'CO_Clinical_Observation',
  'bean_name' => 'CO_Clinical_Observation',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_2_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'id_name' => 'cie_clinicddcbue_exam_ida',
  'link-type' => 'many',
  'side' => 'left',
);
