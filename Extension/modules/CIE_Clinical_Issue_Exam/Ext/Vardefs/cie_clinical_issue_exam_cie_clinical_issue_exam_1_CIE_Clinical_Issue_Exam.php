<?php
// created: 2020-01-07 13:46:57
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_cie_clinical_issue_exam_1"] = array (
  'name' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1',
  'source' => 'non-db',
  'module' => 'CIE_Clinical_Issue_Exam',
  'bean_name' => 'CIE_Clinical_Issue_Exam',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_L_TITLE',
  'id_name' => 'cie_clinic7f62ue_exam_idb',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_cie_clinical_issue_exam_1_right"] = array (
  'name' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_right',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1',
  'source' => 'non-db',
  'module' => 'CIE_Clinical_Issue_Exam',
  'bean_name' => 'CIE_Clinical_Issue_Exam',
  'side' => 'right',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_R_TITLE',
  'id_name' => 'cie_clinic2bf6ue_exam_ida',
  'link-type' => 'one',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_cie_clinical_issue_exam_1_name"] = array (
  'name' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_L_TITLE',
  'save' => true,
  'id_name' => 'cie_clinic2bf6ue_exam_ida',
  'link' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_right',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'name',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinic2bf6ue_exam_ida"] = array (
  'name' => 'cie_clinic2bf6ue_exam_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_R_TITLE_ID',
  'id_name' => 'cie_clinic2bf6ue_exam_ida',
  'link' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_right',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
