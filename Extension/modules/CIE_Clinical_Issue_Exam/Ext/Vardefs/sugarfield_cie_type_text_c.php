<?php
 // created: 2020-01-07 13:54:42
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['duplicate_merge_dom_value']=0;
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['labelValue']='CIE Type Text';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['calculated']='true';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['formula']='getDropdownValue("cie_type_list",$type_2)';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['enforced']='true';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['dependency']='';

 ?>