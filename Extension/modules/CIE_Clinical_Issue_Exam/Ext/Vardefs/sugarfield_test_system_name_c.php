<?php
 // created: 2020-01-07 13:53:17
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['duplicate_merge_dom_value']=0;
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['labelValue']='Test System Name';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['calculated']='true';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['formula']='related($anml_animals_cie_clinical_issue_exam_1,"name")';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['enforced']='true';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['dependency']='';

 ?>