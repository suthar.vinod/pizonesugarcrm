<?php

$hook_array['after_save'][] = Array(
        //Processing index. For sorting the array.
        1,

        //Label. A string value to identify the hook.
        'after_save related_clinical_issues',

        //The PHP file where your class is located.
        'custom/modules/CIE_Clinical_Issue_Exam/related_clinical_issues.php',

        //The class the method is in.
        'RelatedClinicalIssues',

        //The method to call.
        'relatedClinicalIssuesMethod'
    );

