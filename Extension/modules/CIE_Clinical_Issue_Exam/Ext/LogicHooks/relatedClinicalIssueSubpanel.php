<?php

$hook_array['after_relationship_add'][] = Array(
    //Processing index. For sorting the array.
    99,

    //Label. A string value to identify the hook.
    'after_relationship_add RelatedClinicalIssueSubpanel',

    //The PHP file where your class is located.
    'custom/modules/CIE_Clinical_Issue_Exam/relatedClinicalIssueSubpanel.php',

    //The class the method is in.
    'RelatedClinicalIssueSubpanel',

    //The method to call.
    'relatedClinicalIssueSubpanelMethod'
);



