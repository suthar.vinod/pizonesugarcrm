<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymph_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(or(equal($section_notes_data,"All None"),or(or(equal($submandibular_ln,"See Lymphatic Notes"),equal($prescapular_ln,"See Lymphatic Notes")),equal($popliteal_ln_caninelagamorph,"See Lymphatic Notes"))),not(equal($lymphatic_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'submandibular_ln', 'prescapular_ln','popliteal_ln_caninelagamorph','lymphatic_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymph_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"All None"),and(and(equal($submandibular_ln,"Normal"),equal($prescapular_ln,"Normal")),equal($popliteal_ln_caninelagamorph,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'submandibular_ln', 'prescapular_ln','popliteal_ln_caninelagamorph','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymph_notes_species_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"All None"),and(and(equal($submandibular_ln,"Normal"),equal($prescapular_ln,"Normal")),not(equal($popliteal_ln_caninelagamorph,"See Lymphatic Notes")))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'submandibular_ln', 'prescapular_ln','popliteal_ln_caninelagamorph','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymph_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"Complete Individual Sections"),and(and(equal($submandibular_ln,"Normal"),equal($prescapular_ln,"Normal")),equal($popliteal_ln_caninelagamorph,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'submandibular_ln', 'prescapular_ln','popliteal_ln_caninelagamorph','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymphs_species_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"Complete Individual Sections"),and(and(equal($submandibular_ln,"Normal"),equal($prescapular_ln,"Normal")),not(equal($popliteal_ln_caninelagamorph,"See Lymphatic Notes")))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'submandibular_ln', 'prescapular_ln','popliteal_ln_caninelagamorph','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);
