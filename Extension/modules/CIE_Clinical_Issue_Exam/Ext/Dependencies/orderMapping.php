<?php
// created: 2021-09-02 17:10:20
$extensionOrderMap = array (
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/obs_rel_field.php' => 
  array (
    'md5' => '53aed0446241622469f991bb953c3140',
    'mtime' => 1578467326,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/cli_obs_rel_field.php' => 
  array (
    'md5' => '5a18b1393d6c78e403747b30bf1ac1e5',
    'mtime' => 1578554939,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_examiner_type_dep.php' => 
  array (
    'md5' => 'f70a783fe9df9b446987c576a6ea137b',
    'mtime' => 1578901598,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_follow_up_type.php' => 
  array (
    'md5' => '0ef383e05f5351eabd4a3fc7df21f49f',
    'mtime' => 1578901598,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_related_cli_issue_examiner_type.php' => 
  array (
    'md5' => 'cda6c22abd82f4fe62317857ed21b6f6',
    'mtime' => 1578901598,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/cie_rel_field.php' => 
  array (
    'md5' => 'a8cbc395eb04f279e5809f93319afbad',
    'mtime' => 1579068655,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/set_options_for_oral_cavity_field.php' => 
  array (
    'md5' => '96884c83c9d2e43eb6066c4a8130ae84',
    'mtime' => 1579255894,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/set_options_for_abdominal_auscultation.php' => 
  array (
    'md5' => '6cd0494c17a286448e564854456ff30a',
    'mtime' => 1579255894,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/set_options_for_popliteal_LN.php' => 
  array (
    'md5' => 'c583dfb6a2baa2678803f61a5b4c313d',
    'mtime' => 1579255894,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/set_required_for_textfields.php' => 
  array (
    'md5' => 'b3fe638be1f022d18105535263e87fd1',
    'mtime' => 1580193196,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_ophthal_dep.php' => 
  array (
    'md5' => 'b5991d68eb25686abafe365c38b9d50f',
    'mtime' => 1580361521,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_gastro_dep.php' => 
  array (
    'md5' => '8694f978d5c07f5c1544939655876e1e',
    'mtime' => 1580361521,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_integument_dep.php' => 
  array (
    'md5' => 'a98cbe9d1e0c7582e8c2deeae524a199',
    'mtime' => 1580361521,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_lymph_dep.php' => 
  array (
    'md5' => '215edacfdac47c73e0391c49d6b6c8a3',
    'mtime' => 1580361521,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_cardiovascular_dep.php' => 
  array (
    'md5' => 'f5de7a20d100b3dea245e57ec8558d36',
    'mtime' => 1580361521,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_nervous_dep.php' => 
  array (
    'md5' => 'b58733fbf238e1feeb7e8ce2a2143198',
    'mtime' => 1580361521,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_muscu_dep.php' => 
  array (
    'md5' => 'ea7598b1feed2a7687f072d9ed32d1f7',
    'mtime' => 1580361521,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_resp_dep.php' => 
  array (
    'md5' => '5802c3714c588b051769bf111f331437',
    'mtime' => 1580361521,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_urogen_dep.php' => 
  array (
    'md5' => '3e38986c4193a491734f986bbf91e4e4',
    'mtime' => 1580361521,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_data_field_dep.php' => 
  array (
    'md5' => '494e0ae3ab1b2763ec2118c847c784fd',
    'mtime' => 1580361521,
    'is_override' => false,
  ),
  'custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_format_dep.php' => 
  array (
    'md5' => '6627c68a93f5d7f76c47cf8a41dd9359',
    'mtime' => 1580361521,
    'is_override' => false,
  ),
);