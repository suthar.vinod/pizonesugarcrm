<?php

/**
 * If Lymphatic Data Data = All Normal AND Test System is related to Canine OR Lagamorph,
 * then Popliteal LN (Canine/Lagamorph) = Normal
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_popliteal_ln_for_normal'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($lymphatic_data,createList("All Normal")),or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
    'triggerFields' => array('lymphatic_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'popliteal_ln_caninelagamorph',
                'keys' => 'getDropdownKeySet("oral_cavity_list_normal")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_normal")'
            ),
        ),
    ),

);

/**
 * If Lymphatic Data Data = All Not Examined, then Popliteal LN (Canine/Lagamorph) = Not Examined
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_popliteal_ln_for_not_exam'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($lymphatic_data,createList("All Not Examined"))',
    'triggerFields' => array('lymphatic_data'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'popliteal_ln_caninelagamorph',
                'keys' => 'getDropdownKeySet("oral_cavity_list_not_exam")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_not_exam")'
            ),
        ),
    ),

);

/**
 * If Lymphatic Data = Complete Section AND Test System is related to Canine OR Lagamorph,
 * then Popliteal  LN (Canine/Lagamorph) = Normal, See Lymphatic Notes
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_popliteal_ln_for_complete_section'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($lymphatic_data,createList("Complete Section")),or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
    'triggerFields' => array('lymphatic_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'popliteal_ln_caninelagamorph',
                'keys' => 'getDropdownKeySet("normal_sln_list")',
                'labels' => 'getDropdownValueSet("normal_sln_list")'
            ),
        ),
    ),
);

/**
 * If all 3 above mentioned dependencies criteria didn't meet then default dropdown list should be display
 */
//$dependencies['CIE_Clinical_Issue_Exam']['unset_options_for_all_popliteal_dependencies'] = array(
//   'hooks' => array("edit", "save"),
//   // 'trigger' => 'not(greaterThan(strlen($lymphatic_data),0))',
//     'trigger' => 'or(greaterThan(strlen($lymphatic_data),0),and(not(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine")),not(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph"))))',
//    'triggerFields' => array('lymphatic_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
//    'onload' => 'true',
//    //Actions is a list of actions to fire when the trigger is true
//    'actions' => array(
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'popliteal_ln_caninelagamorph',
//                'keys' => 'getDropdownKeySet("normal_ne_sln_list")',
//                'labels' => 'getDropdownValueSet("normal_ne_sln_list")'
//            ),
//        ),
//    ),
//);

/**
 * Set visibility for "Popliteal LN" field
 * If lymphatic_data is not empty and Species is either Canine or Lagamorph
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_visibity_for_pop_ln'] = array(
    'hooks' => array("edit", "save"),
    // 'trigger' => 'not(equal($gastrointestinal_data,""))',
    'triggerFields' => array('lymphatic_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetVisibility', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'popliteal_ln_caninelagamorph',
                'value' => 'and(not(equal($lymphatic_data,"")), or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
            ),
        ),
    ),
);



