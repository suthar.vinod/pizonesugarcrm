<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_format_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($format,"Open Notes"),equal($section_notes_data,"All None"))',
    'triggerFields' => array('section_notes_data', 'format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'urogenital_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_format_section_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($format,"Open Notes"),equal($section_notes_data,"Complete Individual Sections"))',
    'triggerFields' => array('section_notes_data', 'format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'urogenital_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_format_organ_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'equal($format,"Organ Systems")',
    'triggerFields' => array('format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'urogenital_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);