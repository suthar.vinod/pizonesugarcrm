<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_nervous_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(or(equal($section_notes_data, "All None"),isInList("See Nervous System Notes",createList($circlingpacing,$head_tilt,$head_pressing,$ataxia,$tremors,$cn2_optic_vision_plr_menace,$cn3_oculomotor_eyeeyelid_pos,$cn4_trochlear_eye_position,$cn5_trigeminal_masseter_symmet,$cn6_abducent_eye_position,$cn7_facial_facial_symmetry,$cn8_vestibulochoclear_head_til,$cn9_glosspharyngeal_swallow,$cn10_vagus_gi_auscultation_swa,$cn11_accessory_thoracic_muscle,$cn12_hypoglossal_tongue_moveme))),not(equal($nervous_system_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'circlingpacing', 'head_tilt','head_pressing','ataxia','tremors','cn2_optic_vision_plr_menace','cn3_oculomotor_eyeeyelid_pos','cn4_trochlear_eye_position','cn5_trigeminal_masseter_symmet','cn6_abducent_eye_position','cn7_facial_facial_symmetry','cn8_vestibulochoclear_head_til','cn9_glosspharyngeal_swallow','cn10_vagus_gi_auscultation_swa','cn11_accessory_thoracic_muscle','cn12_hypoglossal_tongue_moveme','nervous_system_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_nervous_notes_none'] = array(
  'trigger' => 'and(and(equal($section_notes_data, "All None"),
  and(and(and(and(and(and(equal($circlingpacing,"No"),equal($head_tilt,"No")),equal($head_pressing,"No")),equal($head_pressing,"No")),equal($ataxia,"No")),equal($tremors,"No")),
  and(and(and(and(and(and(equal($cn2_optic_vision_plr_menace,"Normal"),equal($cn3_oculomotor_eyeeyelid_pos,"Normal")),and(equal($cn4_trochlear_eye_position,"Normal"),equal($cn5_trigeminal_masseter_symmet,"Normal"))),and(equal($cn6_abducent_eye_position,"Normal"),equal($cn7_facial_facial_symmetry,"Normal"))),and(equal($cn8_vestibulochoclear_head_til,"Normal")
  ,equal($cn9_glosspharyngeal_swallow,"Normal"))),and(equal($cn10_vagus_gi_auscultation_swa,"Normal"),equal($cn11_accessory_thoracic_muscle,"Normal"))),equal($cn12_hypoglossal_tongue_moveme,"Normal")))),equal($format,"Organ Systems"))',
  'triggerFields' => array('section_notes_data', 'circlingpacing', 'head_tilt','head_pressing','ataxia','tremors','cn2_optic_vision_plr_menace','cn3_oculomotor_eyeeyelid_pos','cn4_trochlear_eye_position','cn5_trigeminal_masseter_symmet','cn6_abducent_eye_position','cn7_facial_facial_symmetry','cn8_vestibulochoclear_head_til','cn9_glosspharyngeal_swallow','cn10_vagus_gi_auscultation_swa','cn11_accessory_thoracic_muscle','cn12_hypoglossal_tongue_moveme','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_nervous_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "Complete Individual Sections"),
    and(and(and(and(and(and(equal($circlingpacing,"No"),equal($head_tilt,"No")),equal($head_pressing,"No")),equal($head_pressing,"No")),equal($ataxia,"No")),equal($tremors,"No")),
    and(and(and(and(and(and(equal($cn2_optic_vision_plr_menace,"Normal"),equal($cn3_oculomotor_eyeeyelid_pos,"Normal")),and(equal($cn4_trochlear_eye_position,"Normal"),equal($cn5_trigeminal_masseter_symmet,"Normal"))),and(equal($cn6_abducent_eye_position,"Normal"),equal($cn7_facial_facial_symmetry,"Normal"))),and(equal($cn8_vestibulochoclear_head_til,"Normal")
    ,equal($cn9_glosspharyngeal_swallow,"Normal"))),and(equal($cn10_vagus_gi_auscultation_swa,"Normal"),equal($cn11_accessory_thoracic_muscle,"Normal"))),equal($cn12_hypoglossal_tongue_moveme,"Normal")))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'circlingpacing', 'head_tilt','head_pressing','ataxia','tremors','cn2_optic_vision_plr_menace','cn3_oculomotor_eyeeyelid_pos','cn4_trochlear_eye_position','cn5_trigeminal_masseter_symmet','cn6_abducent_eye_position','cn7_facial_facial_symmetry','cn8_vestibulochoclear_head_til','cn9_glosspharyngeal_swallow','cn10_vagus_gi_auscultation_swa','cn11_accessory_thoracic_muscle','cn12_hypoglossal_tongue_moveme','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);
