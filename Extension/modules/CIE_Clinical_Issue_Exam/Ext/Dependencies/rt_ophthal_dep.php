<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_ophthal_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(or(equal($section_notes_data, "All None"),or(or(or(or(equal($eye_positionsymmetry,"See Ophthalmic Notes"),equal($conjunctiva,"See Ophthalmic Notes")),equal($cornea,"See Ophthalmic Notes")),equal($pupil,"See Ophthalmic Notes")),equal($ocular_discharge,"See Ophthalmic Notes"))),not(equal($ophthalmic_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'eye_positionsymmetry', 'conjunctiva','cornea','pupil','ocular_discharge','ophthalmic_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_ophthal_notes_none'] = array(
  'trigger' => 'and(and(equal($section_notes_data, "All None"),and(and(and(and(equal($eye_positionsymmetry,"Normal"),equal($conjunctiva,"Normal")),equal($cornea,"Normal")),equal($pupil,"Normal")),equal($ocular_discharge,"No"))),equal($format,"Organ Systems"))',
  'triggerFields' => array('section_notes_data', 'eye_positionsymmetry', 'conjunctiva','cornea','pupil','ocular_discharge','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_ophthal_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "Complete Individual Sections"),and(and(and(and(equal($eye_positionsymmetry,"Normal"),equal($conjunctiva,"Normal")),equal($cornea,"Normal")),equal($pupil,"Normal")),equal($ocular_discharge,"No"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'eye_positionsymmetry', 'conjunctiva','cornea','pupil','ocular_discharge','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);
