<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_urogen_notes_yes'] = array(
  'hooks' => array("edit", "save"),
  'trigger' => 'and(and(or(equal($section_notes_data,"All None"),or(equal($external_genitalia,"See Urogenital Notes"),equal($urine,"See Urogenital Notes"))),not(equal($urogenital_data,"All Not Examined"))),equal($format,"Organ Systems"))',
  'triggerFields' => array('section_notes_data', 'external_genitalia', 'urine','urogenital_data','format'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetOptions',
      'params' => array(
        'target' => 'urogenital_notes_data',
        'keys' => 'getDropdownKeySet("options_yes")',
        'labels' => 'getDropdownValueSet("options_yes")'
      ),
    ),
  ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_urogen_notes_none'] = array(
  'hooks' => array("edit", "save"),
  'trigger' => 'and(and(equal($section_notes_data,"All None"),and(equal($external_genitalia,"Normal"),or(equal($urine,"Normal"),equal($urine,"Not Examined")))),equal($format,"Organ Systems"))',
  'triggerFields' => array('section_notes_data', 'external_genitalia', 'urine','format'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetOptions',
      'params' => array(
        'target' => 'urogenital_notes_data',
        'keys' => 'getDropdownKeySet("options_none")',
        'labels' => 'getDropdownValueSet("options_none")'
      ),
    ),
  ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_urogen_complete_sec'] = array(
  'hooks' => array("edit", "save"),
  'trigger' => 'and(and(equal($section_notes_data,"Complete Individual Sections"),and(equal($external_genitalia,"Normal"),or(equal($urine,"Normal"),equal($urine,"Not Examined")))),equal($format,"Organ Systems"))',
  'triggerFields' => array('section_notes_data', 'external_genitalia', 'urine','format'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetOptions',
      'params' => array(
        'target' => 'urogenital_notes_data',
        'keys' => 'getDropdownKeySet("options_yes_none")',
        'labels' => 'getDropdownValueSet("options_yes_none")'
      ),
    ),
  ),
);
