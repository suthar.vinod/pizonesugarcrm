<?php


$dependencies['CIE_Clinical_Issue_Exam']['set_required_for_textfields'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('musculoskeletal_data', 'musculoskeletal_palpation'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'swelling_location',
                'value' => 'or(not(equal($musculoskeletal_data, "")), not(equal($musculoskeletal_palpation, "")))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'swelling_size',
                'value' => 'or(not(equal($musculoskeletal_data, "")), not(equal($musculoskeletal_palpation, "")))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'heat_location',
                'value' => 'or(not(equal($musculoskeletal_data, "")), not(equal($musculoskeletal_palpation, "")))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'pain_location',
                'value' => 'or(not(equal($musculoskeletal_data, "")), not(equal($musculoskeletal_palpation, "")))',
            ),
        ),
    ),
);

