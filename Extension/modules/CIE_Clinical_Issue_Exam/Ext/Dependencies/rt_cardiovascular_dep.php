<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_cardio_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(and(isInList($section_notes_data,createList("All None", "Complete Individual Sections")),or(equal($cardiac_auscultation,"See Cardiovascular Notes"),equal($cardiac_rhythm,"See Cardiovascular Notes"))),not(equal($cardiovascular_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'cardiac_auscultation', 'cardiac_rhythm','cardiovascular_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_cardio_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"All None"),and(equal($cardiac_auscultation,"Normal"),equal($cardiac_rhythm,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'cardiac_auscultation', 'cardiac_rhythm','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_cardio_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"Complete Individual Sections"),and(equal($cardiac_auscultation,"Normal"),equal($cardiac_rhythm,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'cardiac_auscultation', 'cardiac_rhythm','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);
