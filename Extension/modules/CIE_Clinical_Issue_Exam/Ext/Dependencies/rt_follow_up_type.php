<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_follow_up_type'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(or(equal($category,"Vet Check Clin Ob Related"),equal($category,"Vet Check Not Clin Ob Related")),equal($type_2,"Initial"))',
   'triggerFields' => array('type_2','category'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("vet_list")',
        'labels' => 'getDropdownValueSet("vet_list")'
       ),
     ),
   ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_follow_up_type_not'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(isInList($category,createList("Vet Check Clin Ob Related", "Vet Check Not Clin Ob Related")),not(equal($type_2,"Initial")))',
   'triggerFields' => array('category','type_2'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("examiner_type_list")',
        'labels' => 'getDropdownValueSet("examiner_type_list")'
       ),
     ),
   ),
 );


