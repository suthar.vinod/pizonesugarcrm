<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_resp_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(or(equal($section_notes_data, "All None"),or(or(or(equal($upper_airway_auscultation,"See Respiratory Notes"),equal($lung_auscultation,"See Respiratory Notes")),equal($respiratory_effort,"See Respiratory Notes")),equal($nasal_discharge,"See Respiratory Notes"))),not(equal($respiratory_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'upper_airway_auscultation', 'lung_auscultation','respiratory_effort','nasal_discharge','respiratory_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_resp_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "All None"),and(and(and(equal($upper_airway_auscultation,"Normal"),equal($lung_auscultation,"Normal")),equal($respiratory_effort,"Normal")),equal($nasal_discharge,"No"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'upper_airway_auscultation', 'lung_auscultation','respiratory_effort','nasal_discharge','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_resp_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "Complete Individual Sections"),and(and(and(equal($upper_airway_auscultation,"Normal"),equal($lung_auscultation,"Normal")),equal($respiratory_effort,"Normal")),equal($nasal_discharge,"No"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'upper_airway_auscultation', 'lung_auscultation','respiratory_effort','nasal_discharge','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

