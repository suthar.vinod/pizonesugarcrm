<?php
$dependencies['CIE_Clinical_Issue_Exam']['com_rel_dep'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('category','type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'cie_clinical_issue_exam_m06_error_1_name',
                'value' => 'and(equal($category, "Vet Check Clin Ob Related"),equal($type_2,"Initial"))', //Formula
            ),
        ),
    ),
);
