<?php

/**
 * When Gastrointestinal Data = All Normal AND Test System species = Canine|Lagamorph
 * then set Normal option in both Oral Cavity and Abdominal Palpation fields
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_oral_cavity_field'] = array(
    'hooks' => array("all"),
    'trigger' => 'and(isInList($gastrointestinal_data,createList("All Normal")),or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
    'triggerFields' => array('gastrointestinal_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
    //'trigger' => 'equal($test_c, true)',
    //'triggerFields' => array('test_c'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'oral_cavity',
                'keys' => 'getDropdownKeySet("oral_cavity_list_normal")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_normal")'
               //'keys' => "createList('Normal')",
              //'labels' => "createList('Normal')"
            ),
        ),
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_palpation',
                'keys' => 'getDropdownKeySet("oral_cavity_list_normal")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_normal")'
                 //'keys' => "createList('Normal')",
               //'labels' => "createList('Normal')"
            ),
        ),
    ),
//    'notActions' => array(
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'oral_cavity',
//                'keys' => 'getDropdownKeySet("oral_cavity_list_ass")',
//                'labels' => 'getDropdownValueSet("oral_cavity_list_ass")'
//            ),
//        ),
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'abdominal_palpation',
//                'keys' => 'getDropdownKeySet("oral_cavity_list")',
//                'labels' => 'getDropdownValueSet("oral_cavity_list")'
//            ),
//        ),
//    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_Examiner'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($gastrointestinal_data,createList("All Not Examined"))',
    'triggerFields' => array('gastrointestinal_data'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'oral_cavity',
                'keys' => 'getDropdownKeySet("oral_cavity_list_not_exam")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_not_exam")'
                // 'keys' => "createList('Not Examined')",
               //'labels' => "createList('Not Examined')"
            ),
        ),
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_palpation',
                'keys' => 'getDropdownKeySet("oral_cavity_list_not_exam")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_not_exam")'
                // 'keys' => "createList('Not Examined')",
                //'labels' => "createList('Not Examined')"
            ),
        ),
    ),
//    'notActions' => array(
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'oral_cavity',
//                'keys' => 'getDropdownKeySet("oral_cavity_list_ass")',
//                'labels' => 'getDropdownValueSet("oral_cavity_list_ass")'
//            ),
//        ),
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'abdominal_palpation',
//                'keys' => 'getDropdownKeySet("oral_cavity_list")',
//                'labels' => 'getDropdownValueSet("oral_cavity_list")'
//            ),
//        ),
//    ),
);

/**
 * If Gastrointestinal Data = Complete Section AND Test System has species Canine OR Lagamorph,
 * then Oral Cavity = Normal, See Additional Subjective Observations
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_complete_section'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($gastrointestinal_data,createList("Complete Section")),or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
    'triggerFields' => array('gastrointestinal_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
    //'trigger' => 'equal($gt_complete_section_c, true)',
    //'triggerFields' => array('gt_complete_section_c'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'oral_cavity',
                'keys' => 'getDropdownKeySet("normal_see_gas_list")',
                'labels' => 'getDropdownValueSet("normal_see_gas_list")'
                // 'keys' => "createList('Normal', 'See Additional Subjective Observations')",
                //'labels' =>"createList('Normal', 'See Additional Subjective Observations')",
            ),
        ),
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_palpation',
                'keys' => 'getDropdownKeySet("normal_see_gas_list")',
                'labels' => 'getDropdownValueSet("normal_see_gas_list")'
              //'keys' => "createList('Normal', 'See Additional Subjective Observations')",
              //'labels' =>"createList('Normal', 'See Additional Subjective Observations')",
            ),
        ),
    ),
//    'notActions' => array(
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'oral_cavity',
//                'keys' => 'getDropdownKeySet("oral_cavity_list_ass")',
//                'labels' => 'getDropdownValueSet("oral_cavity_list_ass")'
//            ),
//        ),
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'abdominal_palpation',
//                'keys' => 'getDropdownKeySet("oral_cavity_list")',
//                'labels' => 'getDropdownValueSet("oral_cavity_list")'
//            ),
//        ),
//    ),
);

/**
 * If all 3 above mentioned dependencies criteria didn't meat then default dropdown list should be display
 */
//$dependencies['CIE_Clinical_Issue_Exam']['unset_options_for_all_dependencies'] = array(
//   'hooks' => array("edit", "save"),
//   // 'trigger' => 'and(not(and(isInList($gastrointestinal_data,createList("All Normal")),or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_c"),"Lagamorph")))),'
//   // . ' not(isInList($gastrointestinal_data,createList("All Not Examined")),'
//   // . 'not(and(isInList($gastrointestinal_data,createList("Complete Section")),or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_c"),"Lagamorph")))) ))',
//    'trigger' => 'and(greaterThan(strlen($gastrointestinal_data),0),and(not(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine")),not(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph"))))',
//   // 'trigger' => 'and(not(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine")),not(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
//    'triggerFields' => array('gastrointestinal_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
//    //'trigger' => 'equal($test_c, true)',
//    //'triggerFields' => array('test_c'),
//    'onload' => 'true',
//    //Actions is a list of actions to fire when the trigger is true
//    'actions' => array(
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'oral_cavity',
//                'keys' => 'getDropdownKeySet("normal_ne_ginotes_subnotes_list")',
//                'labels' => 'getDropdownValueSet("normal_ne_ginotes_subnotes_list")'
//               //'keys' => "createList('Normal')",
//              //'labels' => "createList('Normal')"
//            ),
//        ),
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'abdominal_palpation',
//                'keys' => 'getDropdownKeySet("normal_ne_ginotes_subnotes_list")',
//                'labels' => 'getDropdownValueSet("normal_ne_ginotes_subnotes_list")'
//                 //'keys' => "createList('Normal')",
//               //'labels' => "createList('Normal')"
//            ),
//        ),
//    ),
//);


/**
 * Set visibility for "Oral Cavity" and "Abdominal Palpation"
 * If $gastrointestinal_data is not empty and Species is either Canine or Lagamorph
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_visibity_for_oc_and_ap'] = array(
    'hooks' => array("edit", "save"),
    // 'trigger' => 'not(equal($gastrointestinal_data,""))',
    'triggerFields' => array('gastrointestinal_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetVisibility', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'oral_cavity',
                'value' => 'and(not(equal($gastrointestinal_data,"")), or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
            ),
        ),
        array(
            'name' => 'SetVisibility', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_palpation',
                'value' => 'and(not(equal($gastrointestinal_data,"")), or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
            ),
        ),
    ),
);


