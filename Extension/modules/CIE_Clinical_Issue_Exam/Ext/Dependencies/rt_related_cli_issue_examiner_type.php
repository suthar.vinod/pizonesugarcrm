<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_related_examiner_type_tech'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(equal(related($cie_clinical_issue_exam_cie_clinical_issue_exam_1_right,"examiner_type"),"Technician"),and(or(equal($category,"Physical Exam"),equal($category,"Vet Check Not Clin Ob Related")),not(equal($type_2,"Initial"))))',
   'triggerFields' => array('cie_clinical_issue_exam_cie_clinical_issue_exam_1_right','examiner_type','category'.'type_2'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("tech_list")',
        'labels' => 'getDropdownValueSet("tech_list")'
       ),
     ),
   ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_related_examiner_type_vet'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(equal(related($cie_clinical_issue_exam_cie_clinical_issue_exam_1_right,"examiner_type"),"Veterinarian"),and(or(equal($category,"Physical Exam"),equal($category,"Vet Check Not Clin Ob Related")),not(equal($type_2,"Initial"))))',
   'triggerFields' => array('cie_clinical_issue_exam_cie_clinical_issue_exam_1_right','examiner_type','category','type_2'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("vet_list")',
        'labels' => 'getDropdownValueSet("vet_list")'
       ),
     ),
   ),
);

