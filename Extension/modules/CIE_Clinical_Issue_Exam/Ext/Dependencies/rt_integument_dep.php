<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_integ_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(and(isInList($section_notes_data,createList("All None", "Complete Individual Sections")),or(or(or(equal($xerosis,"See Integument Notes"),equal($pruritus,"See Integument Notes")),equal($alopecia,"See Integument Notes")),contains($lesionswound,"See"))),not(equal($integument_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'xerosis', 'pruritus','alopecia','lesionswound','integument_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_integ_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "All None"),and(and(and(equal($xerosis,"No"),equal($pruritus,"No")),equal($alopecia,"No")),equal($lesionswound,"No"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'xerosis', 'pruritus','alopecia','lesionswound','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_integ_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "Complete Individual Sections"),and(and(and(equal($xerosis,"No"),equal($pruritus,"No")),equal($alopecia,"No")),equal($lesionswound,"No"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'xerosis', 'pruritus','alopecia','lesionswound','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);
