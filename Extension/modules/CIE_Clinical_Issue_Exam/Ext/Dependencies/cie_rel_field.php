<?php
$dependencies['CIE_Clinical_Issue_Exam']['cie_rel_dep'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_name',
                'value' => 'or(equal($type_2,"Follow Up"),equal($type_2,"Resolution"))', //Formula
            ),
        ),
    ),
);
