<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_muscu_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(or(equal($section_notes_data, "All None"),or(or(or(equal($musculoskeletal_symmetry,"See Musculoskeletal Notes"),equal($muscle_atrophy,"See Musculoskeletal Notes")),equal($posture,"See Musculoskeletal Notes")),equal($stanceweight_bearing,"See Musculoskeletal Notes"))),not(equal($musculoskeletal_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'musculoskeletal_symmetry', 'muscle_atrophy','posture','stanceweight_bearing','musculoskeletal_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_muscu_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "All None"),and(and(and(equal($musculoskeletal_symmetry,"Normal"),equal($muscle_atrophy,"No")),equal($posture,"Normal")),equal($stanceweight_bearing,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'musculoskeletal_symmetry', 'muscle_atrophy','posture','stanceweight_bearing','lymphatic_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_muscu_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "Complete Individual Sections"),and(and(and(equal($musculoskeletal_symmetry,"Normal"),equal($muscle_atrophy,"No")),equal($posture,"Normal")),equal($stanceweight_bearing,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'musculoskeletal_symmetry', 'muscle_atrophy','posture','stanceweight_bearing','lymphatic_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);
