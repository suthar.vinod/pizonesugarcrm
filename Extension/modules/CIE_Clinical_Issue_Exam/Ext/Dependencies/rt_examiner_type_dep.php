<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_examiner_type_tech'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(and(equal(related($cie_clinical_issue_exam_cie_clinical_issue_exam_1_right,"follow_upresolution_allowed_by"),"Technician"),equal($category,"Vet Check Clin Ob Related")),or(equal($type_2,"Follow Up"),equal($type_2,"Resolution")))',
   'triggerFields' => array('follow_upresolution_allowed_by','category','cie_clinical_issue_exam_cie_clinical_issue_exam_1_right','type_2'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("tech_list")',
         'labels' => 'getDropdownValueSet("tech_list")'
       ),
     ),
   ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_examiner_type_vet'] = array(
   'hooks' => array("edit","save"),
    'trigger' => 'and(and(equal(related($cie_clinical_issue_exam_cie_clinical_issue_exam_1_right,"follow_upresolution_allowed_by"),"Veterinarian"),equal($category,"Vet Check Clin Ob Related")),or(equal($type_2,"Follow Up"),equal($type_2,"Resolution")))',
   'triggerFields' => array('follow_upresolution_allowed_by','category','cie_clinical_issue_exam_cie_clinical_issue_exam_1_right','type_2'),
    'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("vet_list")',
        'labels' => 'getDropdownValueSet("vet_list")'
       ),
     ),
   ),

);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_examiner_type_both'] = array(
   'hooks' => array("edit","save"),
    'trigger' => 'and(and(equal(related($cie_clinical_issue_exam_cie_clinical_issue_exam_1_right,"follow_upresolution_allowed_by"),"Technician or Veterinarian"),equal($category,"Vet Check Clin Ob Related")),or(equal($type_2,"Follow Up"),equal($type_2,"Resolution")))',
   'triggerFields' => array('follow_upresolution_allowed_by','category','cie_clinical_issue_exam_cie_clinical_issue_exam_1_right','type_2'),
 'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("examiner_type_list")',
         'labels' => 'getDropdownValueSet("examiner_type_list")'
       ),
     ),
   ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_not_clin'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(equal($category,"Vet Check Not Clin Ob Related"),or(equal($type_2,"Follow Up"),equal($type_2,"Resolution")))',
   'triggerFields' => array('category','type_2'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("examiner_type_list")',
         'labels' => 'getDropdownValueSet("examiner_type_list")'
       ),
     ),
   ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_type_physical_exam'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'equal($category,"Physical Exam")',
   'triggerFields' => array('category'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("examiner_type_list")',
         'labels' => 'getDropdownValueSet("examiner_type_list")'
       ),
     ),
   ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_related_clin_issue'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'equal($cie_clinical_issue_exam_cie_clinical_issue_exam_1_right,"")',
   'triggerFields' => array('cie_clinical_issue_exam_cie_clinical_issue_exam_1_right'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("examiner_type_list")',
         'labels' => 'getDropdownValueSet("examiner_type_list")'
       ),
     ),
   ),
);


