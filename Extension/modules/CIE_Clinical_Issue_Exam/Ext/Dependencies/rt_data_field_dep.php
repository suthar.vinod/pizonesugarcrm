<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_cardio_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($cardiovascular_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'cardiovascular_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($gastrointestinal_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'gastrointestinal_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_integ_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($integument_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'integument_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymph_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($lymphatic_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'lymphatic_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_muscu_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($musculoskeletal_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'musculoskeletal_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_nervous_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($nervous_system_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'nervous_system_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_ophth_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($ophthalmic_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'ophthalmic_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_resp_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($respiratory_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'respiratory_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_urogen_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($urogenital_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'urogenital_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'urogenital_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_cardio_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($cardiovascular_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'cardiovascular_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($gastrointestinal_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'gastrointestinal_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_integ_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($integument_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'integument_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymph_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($lymphatic_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'lymphatic_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_muscu_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($musculoskeletal_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'musculoskeletal_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_nervous_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($nervous_system_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'nervous_system_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_ophth_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($ophthalmic_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'ophthalmic_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_resp_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($respiratory_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'respiratory_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_urogen_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($urogenital_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'urogenital_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'urogenital_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);