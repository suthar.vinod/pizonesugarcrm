<?php

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(isInList($section_notes_data,createList("All None", "Complete Individual Sections")),or(or(equal($oral_cavity,"See Gastrointestinal Notes"),equal($abdominal_auscultation,"See Gastrointestinal Notes")),equal($abdominal_palpation,"See Gastrointestinal Notes"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'oral_cavity', 'abdominal_palpation', 'abdominal_auscultation','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"All None"),and(and(equal($oral_cavity,"Normal"),equal($abdominal_auscultation,"Normal")),equal($abdominal_palpation,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'oral_cavity', 'abdominal_palpation', 'abdominal_auscultation','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_notes_species_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(and(equal($section_notes_data,"All None"),equal($abdominal_auscultation,"Normal")),and(not(equal($abdominal_palpation,"See Gastrointestinal Notes")),not(equal($oral_cavity,"See Gastrointestinal Notes")))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'oral_cavity', 'abdominal_palpation', 'abdominal_auscultation','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"Complete Individual Sections"),and(and(equal($oral_cavity,"Normal"),equal($abdominal_auscultation,"Normal")),equal($abdominal_palpation,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'oral_cavity', 'abdominal_palpation', 'abdominal_auscultation','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_species_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(and(equal($section_notes_data,"Complete Individual Sections"),equal($abdominal_auscultation,"Normal")),and(not(equal($abdominal_palpation,"See Gastrointestinal Notes")),not(equal($oral_cavity,"See Gastrointestinal Notes")))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'oral_cavity', 'abdominal_palpation', 'abdominal_auscultation','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);
