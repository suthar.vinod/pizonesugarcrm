<?php

$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_abdo_aus_for_ne'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($gastrointestinal_data,createList("All Not Examined"))',
    'triggerFields' => array('gastrointestinal_data'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_auscultation',
                'keys' => 'getDropdownKeySet("oral_cavity_list_not_exam")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_not_exam")'
            ),
        ),
    ),

);

$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_abdo_aus_for_normal'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($gastrointestinal_data,createList("All Normal"))',
    'triggerFields' => array('gastrointestinal_data'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_auscultation',
               'keys' => 'getDropdownKeySet("oral_cavity_list_normal")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_normal")'
            ),
        ),
    ),

);

$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_abdo_aus_for_com_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'or(and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($appetite,createList("Mild Inappetent", "Moderate Inappetent", "Anorexic"))), '
    . 'and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($feces,createList("Absent", "Diarrhea", "Scant", "Soft"))),'
    . ' and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($oral_cavity,createList("See Gastrointestinal Notes"))),'
    . 'and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($abdominal_palpation,createList("See Gastrointestinal Notes"))))',
    'triggerFields' => array('gastrointestinal_data', 'appetite', 'feces', 'oral_cavity', 'abdominal_palpation'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_auscultation',
                'keys' => 'getDropdownKeySet("normal_see_gas_list")',
                'labels' => 'getDropdownValueSet("normal_see_gas_list")'
            ),
        ),
    ),

);

$dependencies['CIE_Clinical_Issue_Exam']['unset_options_for_abdo_aus'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(not(isInList($gastrointestinal_data,createList("All Not Examined"))),not(isInList($gastrointestinal_data,createList("All Normal"))),not(or(and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($appetite,createList("Mild Inappetent", "Moderate Inappetent", "Anorexic"))), '
    . 'and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($feces,createList("Absent", "Diarrhea", "Scant", "Soft"))),'
    . ' and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($oral_cavity,createList("See Gastrointestinal Notes"))),'
    . 'and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($abdominal_palpation,createList("See Gastrointestinal Notes"))))))',
    'triggerFields' => array('gastrointestinal_data', 'appetite', 'feces', 'oral_cavity', 'abdominal_palpation'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_auscultation',
                'keys' => 'getDropdownKeySet("normal_ne_ginotes_subnotes_list")',
                'labels' => 'getDropdownValueSet("normal_ne_ginotes_subnotes_list")'
            ),
        ),
    ),

);

