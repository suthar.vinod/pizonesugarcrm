<?php
 // created: 2020-01-07 13:46:57
$layout_defs["CIE_Clinical_Issue_Exam"]["subpanel_setup"]['cie_clinical_issue_exam_cie_clinical_issue_exam_1'] = array (
  'order' => 100,
  'module' => 'CIE_Clinical_Issue_Exam',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_R_TITLE',
  'get_subpanel_data' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1',
);
