<?php
	$viewdefs['CIE_Clinical_Issue_Exam']['base']['filter']['basic']['filters'][] = array(
		'id' => 'filterByTestSystem',
		'name' => 'LBL_TEST_FILTER_TEMPLATE',
		'filter_definition' => array(
					array(
						'anml_animals_cie_clinical_issue_exam_1anml_animals_ida'  => array(
						    '$in' => array(),
					    ),
				    ),

					array(
						'resolution_date'  => array(
						    '$empty' => ' ',
					    ),
				    ),
			),
		'editable' => true,
		'is_template' => true,
	);
