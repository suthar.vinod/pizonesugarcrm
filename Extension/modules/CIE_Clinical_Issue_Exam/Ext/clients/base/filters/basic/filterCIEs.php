<?php
$viewdefs['CIE_Clinical_Issue_Exam']['base']['filter']['basic']['filters'][] = array(
    'id' => 'filterCIEs',
    'name' => 'LBL_FILTER_CIE_TEMPLATE',
    'filter_definition' => array(
        array(
            'anml_animals_cie_clinical_issue_exam_1anml_animals_ida'=> array(
              '$in'=>array(),
            ),
            'type_2'=> array(
              '$in'=>array(),
            ),
            'resolution_date'=> array(
              '$empty'=>true,
            ),

        )
    ),
    'editable' => true,
    'is_template' => true,
);

