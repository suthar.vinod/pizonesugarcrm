<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_ANML_ANIMALS_TITLE'] = 'Test System';
$mod_strings['LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE'] = 'Initial Clinical Observation';
$mod_strings['LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_L_TITLE'] = 'Initial Clinical Issue';
$mod_strings['LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Communication';
$mod_strings['LBL_TEST_SYSTEM_NAME'] = 'Test System Name';
$mod_strings['LBL_CIE_TYPE_TEXT'] = 'CIE Type Text';
$mod_strings['LBL_POPLITEAL_LN_CANINELAGAMORPH'] = 'Popliteal LN  (Canine/Lagomorph)';
$mod_strings['LNK_NEW_RECORD'] = 'Create Clinical Issue &amp; Exam';
$mod_strings['LNK_LIST'] = 'View Clinical Issues &amp; Exams';
$mod_strings['LBL_MODULE_NAME'] = 'Clinical Issues &amp; Exams';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Clinical Issue &amp; Exam';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Clinical Issue &amp; Exam';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Clinical Issue &amp; Exam vCard';
$mod_strings['LNK_IMPORT_CIE_CLINICAL_ISSUE_EXAM'] = 'Import Clinical Issues &amp; Exams';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Clinical Issue &amp; Exam';
$mod_strings['LBL_CIE_CLINICAL_ISSUE_EXAM_SUBPANEL_TITLE'] = 'Clinical Issues &amp; Exams';
$mod_strings['LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_R_TITLE'] = 'Clinical Issues &amp; Exams';
$mod_strings['LBL_CIE_CLINICAL_ISSUE_EXAM_FOCUS_DRAWER_DASHBOARD'] = 'Clinical Issues &amp; Exams Focus Drawer';
$mod_strings['LBL_CIE_CLINICAL_ISSUE_EXAM_RECORD_DASHBOARD'] = 'Clinical Issues &amp; Exams Record Dashboard';
