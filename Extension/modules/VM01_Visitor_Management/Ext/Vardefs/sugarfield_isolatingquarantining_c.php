<?php
 // created: 2021-12-14 12:27:57
$dictionary['VM01_Visitor_Management']['fields']['isolatingquarantining_c']['labelValue']='Is visitor isolating or quarantining because they tested positive for COVID-19 or are worried that they may be sick with COVID-19?';
$dictionary['VM01_Visitor_Management']['fields']['isolatingquarantining_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['isolatingquarantining_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['isolatingquarantining_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['isolatingquarantining_c']['visibility_grid']='';

 ?>