<?php
 // created: 2022-01-07 06:37:26
$dictionary['VM01_Visitor_Management']['fields']['mandatory_presence_c']['labelValue']='Is visitor\'s presence at APS mandatory for the success of the study?';
$dictionary['VM01_Visitor_Management']['fields']['mandatory_presence_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['mandatory_presence_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['mandatory_presence_c']['visibility_grid']='';

 ?>