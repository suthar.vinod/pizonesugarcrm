<?php
 // created: 2022-01-04 12:44:17
$dictionary['VM01_Visitor_Management']['fields']['explain_symptoms_c']['labelValue']='Explain Symptoms';
$dictionary['VM01_Visitor_Management']['fields']['explain_symptoms_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explain_symptoms_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explain_symptoms_c']['dependency']='equal($symptoms_past_48_hrs_c,"Yes")';
$dictionary['VM01_Visitor_Management']['fields']['explain_symptoms_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['explain_symptoms_c']['readonly_formula']='';

 ?>