<?php
 // created: 2020-11-24 08:43:47
$dictionary['VM01_Visitor_Management']['fields']['explanation_testquarantine_c']['labelValue']='Explanation for Test and/or Quarantine';
$dictionary['VM01_Visitor_Management']['fields']['explanation_testquarantine_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explanation_testquarantine_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explanation_testquarantine_c']['dependency']='equal($test_result_quarantine_c,"Yes")';
$dictionary['VM01_Visitor_Management']['fields']['explanation_testquarantine_c']['required_formula']='';

 ?>