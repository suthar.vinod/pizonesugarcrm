<?php
 // created: 2019-08-27 12:01:22
$dictionary['VM01_Visitor_Management']['fields']['experience_rating_c']['labelValue']='Experience Rating';
$dictionary['VM01_Visitor_Management']['fields']['experience_rating_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['experience_rating_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['experience_rating_c']['dependency']='';

 ?>