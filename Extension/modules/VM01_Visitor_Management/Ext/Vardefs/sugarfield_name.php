<?php
 // created: 2019-08-26 18:17:56
$dictionary['VM01_Visitor_Management']['fields']['name']['required']=false;
$dictionary['VM01_Visitor_Management']['fields']['name']['readonly']=true;
$dictionary['VM01_Visitor_Management']['fields']['name']['len']='255';
$dictionary['VM01_Visitor_Management']['fields']['name']['audited']=true;
$dictionary['VM01_Visitor_Management']['fields']['name']['massupdate']=false;
$dictionary['VM01_Visitor_Management']['fields']['name']['unified_search']=false;
$dictionary['VM01_Visitor_Management']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['VM01_Visitor_Management']['fields']['name']['calculated']=false;

 ?>