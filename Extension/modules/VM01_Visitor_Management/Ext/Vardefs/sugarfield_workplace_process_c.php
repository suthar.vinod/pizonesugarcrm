<?php
 // created: 2022-01-07 12:38:38
$dictionary['VM01_Visitor_Management']['fields']['workplace_process_c']['labelValue']='Is there a procedure in place at visitor\'s workplace to notify test sites (APS) that they have visited if a case of COVID-19 has been confirmed at their place of business?';
$dictionary['VM01_Visitor_Management']['fields']['workplace_process_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['workplace_process_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['workplace_process_c']['visibility_grid']='';

 ?>