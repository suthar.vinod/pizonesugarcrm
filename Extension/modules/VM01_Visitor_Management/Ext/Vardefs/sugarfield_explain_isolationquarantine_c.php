<?php
 // created: 2021-12-14 12:32:31
$dictionary['VM01_Visitor_Management']['fields']['explain_isolationquarantine_c']['labelValue']='Explain Isolation/Quarantine';
$dictionary['VM01_Visitor_Management']['fields']['explain_isolationquarantine_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explain_isolationquarantine_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explain_isolationquarantine_c']['dependency']='equal($isolatingquarantining_c,"Yes")';
$dictionary['VM01_Visitor_Management']['fields']['explain_isolationquarantine_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['explain_isolationquarantine_c']['readonly_formula']='';

 ?>