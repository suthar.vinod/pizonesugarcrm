<?php

$dictionary['VM01_Visitor_Management']['fields']['office_address_c'] = array(
    'name' => 'office_address_c',
    'vname' => 'LBL_OFFICE_ADDRESS',
    'type' => 'text',
    'dbType' => 'varchar',
    'len' => 150,
);
