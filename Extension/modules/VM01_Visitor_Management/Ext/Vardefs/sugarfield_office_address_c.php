<?php
 // created: 2019-08-27 12:02:44
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['audited']=true;
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['massupdate']=false;
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['duplicate_merge']='enabled';
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['duplicate_merge_dom_value']='1';
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['merge_filter']='disabled';
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['unified_search']=false;
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['calculated']=false;
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['rows']='4';
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['cols']='20';

 ?>