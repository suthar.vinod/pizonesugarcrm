<?php
 // created: 2021-12-14 12:23:18
$dictionary['VM01_Visitor_Management']['fields']['symptoms_past_48_hrs_c']['labelValue']='Regardless of vaccination status, has visitor experienced any of the symptoms in the list below in the past 48 hours?';
$dictionary['VM01_Visitor_Management']['fields']['symptoms_past_48_hrs_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['symptoms_past_48_hrs_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['symptoms_past_48_hrs_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['symptoms_past_48_hrs_c']['visibility_grid']='';

 ?>