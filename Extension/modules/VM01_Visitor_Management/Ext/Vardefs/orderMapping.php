<?php
// created: 2022-01-07 12:38:52
$extensionOrderMap = array (
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/office_address_c.php' => 
  array (
    'md5' => '3d3e1d353b70f7dd860191ab6cc2effb',
    'mtime' => 1516708442,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/experience_rating.php' => 
  array (
    'md5' => '4413143455bda471cf0af055cb323a91',
    'mtime' => 1516708442,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/office_phone_c.php' => 
  array (
    'md5' => 'f9717ab1baace8b25ca842a2ac263b90',
    'mtime' => 1516708442,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_name.php' => 
  array (
    'md5' => '6c18a4fa0f60d4c026b3e7cf92fa4ea6',
    'mtime' => 1566843476,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_aps_host_procedure_room_c.php' => 
  array (
    'md5' => '0567450a70d51f6adf06cb8662558f75',
    'mtime' => 1566843529,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_company_c.php' => 
  array (
    'md5' => 'ed895a8b8654b50f7a523d8234b6212b',
    'mtime' => 1566843580,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_email_address_c.php' => 
  array (
    'md5' => '163b09ba886ce9cf2e91789912c31148',
    'mtime' => 1566907048,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_first_name_c.php' => 
  array (
    'md5' => '10c11fe69eec217ff1b4dfa07bf80537',
    'mtime' => 1566907101,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_last_name_c.php' => 
  array (
    'md5' => '8c136eba6cc19975a5cbdd28894f77ea',
    'mtime' => 1566907131,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_visit_status_c.php' => 
  array (
    'md5' => '9ed85e7bd61f26e420433f49afbaa21d',
    'mtime' => 1566907207,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_experience_rating_c.php' => 
  array (
    'md5' => 'ec0f1c4b736e1b8ad358e421796dcb67',
    'mtime' => 1566907282,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_office_address_c.php' => 
  array (
    'md5' => '0edda6657f88f8b6318d53f14a6e2d82',
    'mtime' => 1566907364,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_office_phone_c.php' => 
  array (
    'md5' => 'f22400f9aab4098cb79f6c02207539cd',
    'mtime' => 1566907416,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_description.php' => 
  array (
    'md5' => 'b55bfb58dfdd526550288e5bf52b735b',
    'mtime' => 1566907498,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_approved_visit_c.php' => 
  array (
    'md5' => 'da18868e6dad119474917f74733750b2',
    'mtime' => 1602587515,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explanation_contact_c.php' => 
  array (
    'md5' => '065b6e71dbf373b5af8dcf89b3263412',
    'mtime' => 1602587772,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explanation_for_confirmed_c.php' => 
  array (
    'md5' => 'c3f8fa629fcd3caf48ff955faa56326f',
    'mtime' => 1602587815,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explanation_outside_us_c.php' => 
  array (
    'md5' => '2252f864af8fd12a1695ef5acae3be30',
    'mtime' => 1602587860,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explanation_presence_c.php' => 
  array (
    'md5' => '302d6bf1fd44d168911c45a820326162',
    'mtime' => 1602587899,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_visitor_approval_notes_c.php' => 
  array (
    'md5' => '0295eb9a70d5e461b2c8dd8cfef12ac5',
    'mtime' => 1602590573,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_manager_reviewed_c.php' => 
  array (
    'md5' => '35c6ad6e03dc9e33c01063ce2a644b3b',
    'mtime' => 1604398618,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_test_result_quarantine_c.php' => 
  array (
    'md5' => '35f08f35c670a49665289fa2fc29e50c',
    'mtime' => 1606207306,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explanation_testquarantine_c.php' => 
  array (
    'md5' => 'a395b2bb18c30122e513906107a51238',
    'mtime' => 1606207427,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_experience_rating.php' => 
  array (
    'md5' => '0497cfaeeee704f09ec2ec1fcb45548d',
    'mtime' => 1611652058,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_symptoms_past_48_hrs_c.php' => 
  array (
    'md5' => '10b6c47eff313043f0097e563017ad5a',
    'mtime' => 1639484598,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_isolatingquarantining_c.php' => 
  array (
    'md5' => 'b1cd6867e03c2cba829b1570853d59ec',
    'mtime' => 1639484877,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explain_isolationquarantine_c.php' => 
  array (
    'md5' => '3a2510338177af54335a33e5c37cfee8',
    'mtime' => 1639485151,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_physical_contact_c.php' => 
  array (
    'md5' => '3c724c9c8291995bd496e2a86142efdf',
    'mtime' => 1639485263,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explain_contact_c.php' => 
  array (
    'md5' => '11e514c01daeabc2ef3870dadcc17d5b',
    'mtime' => 1639485339,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explain_symptoms_c.php' => 
  array (
    'md5' => 'bc01d726378dbbe65d3aebb047ccbb0b',
    'mtime' => 1641300257,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_mandatory_presence_c.php' => 
  array (
    'md5' => '083feb2366557887315a4b1efe798292',
    'mtime' => 1641537446,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_outside_us_c.php' => 
  array (
    'md5' => 'e91e562dd412b359ee4708fd19ee35dd',
    'mtime' => 1641537484,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_contact_covid_19_c.php' => 
  array (
    'md5' => '8d71afc78208406d76617bc90ec1b98c',
    'mtime' => 1641537538,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_visitor_confirmed_case_c.php' => 
  array (
    'md5' => 'b24caa958168fe62a27e90a9ca85e0e0',
    'mtime' => 1641537573,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_acknowledgement_no_symptoms_c.php' => 
  array (
    'md5' => 'f3f807dec25c909bda1719554f4621b1',
    'mtime' => 1641559053,
    'is_override' => false,
  ),
  'custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_workplace_process_c.php' => 
  array (
    'md5' => 'ac412a8d8474982ca248b3e1d5c3e217',
    'mtime' => 1641559118,
    'is_override' => false,
  ),
);