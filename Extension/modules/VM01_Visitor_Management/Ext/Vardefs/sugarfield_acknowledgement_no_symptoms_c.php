<?php
 // created: 2022-01-07 12:37:33
$dictionary['VM01_Visitor_Management']['fields']['acknowledgement_no_symptoms_c']['labelValue']='Visitor read and understood that they should enter the facility only if they have had none of the COVID-19 symptoms for a period of at least 7 days';
$dictionary['VM01_Visitor_Management']['fields']['acknowledgement_no_symptoms_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['acknowledgement_no_symptoms_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['acknowledgement_no_symptoms_c']['visibility_grid']='';

 ?>