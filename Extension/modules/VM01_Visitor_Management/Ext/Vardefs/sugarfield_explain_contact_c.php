<?php
 // created: 2021-12-14 12:35:39
$dictionary['VM01_Visitor_Management']['fields']['explain_contact_c']['labelValue']='Explain Contact';
$dictionary['VM01_Visitor_Management']['fields']['explain_contact_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explain_contact_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explain_contact_c']['dependency']='equal($physical_contact_c,"Yes")';
$dictionary['VM01_Visitor_Management']['fields']['explain_contact_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['explain_contact_c']['readonly_formula']='';

 ?>