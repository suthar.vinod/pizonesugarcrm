<?php
 // created: 2022-01-07 06:38:58
$dictionary['VM01_Visitor_Management']['fields']['contact_covid_19_c']['labelValue']='Has visitor come in contact (or if visitor is a physician that treated a patient) with a confirmed case of COVID-19 in the last 14 days?';
$dictionary['VM01_Visitor_Management']['fields']['contact_covid_19_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['contact_covid_19_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['contact_covid_19_c']['visibility_grid']='';

 ?>