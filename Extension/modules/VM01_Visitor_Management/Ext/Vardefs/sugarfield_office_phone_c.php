<?php
 // created: 2019-08-27 12:03:36
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['len']='20';
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['audited']=true;
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['massupdate']=false;
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['duplicate_merge']='enabled';
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['duplicate_merge_dom_value']='1';
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['merge_filter']='disabled';
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['unified_search']=false;
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['calculated']=false;

 ?>