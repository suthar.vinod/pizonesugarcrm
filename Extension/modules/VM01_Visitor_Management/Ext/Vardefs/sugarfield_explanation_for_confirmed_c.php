<?php
 // created: 2020-10-13 11:16:55
$dictionary['VM01_Visitor_Management']['fields']['explanation_for_confirmed_c']['labelValue']='Explanation for Confirmed COVID-19 Case';
$dictionary['VM01_Visitor_Management']['fields']['explanation_for_confirmed_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explanation_for_confirmed_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explanation_for_confirmed_c']['dependency']='equal($visitor_confirmed_case_c,"Yes")';

 ?>