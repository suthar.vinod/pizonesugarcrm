<?php
 // created: 2020-10-13 11:16:12
$dictionary['VM01_Visitor_Management']['fields']['explanation_contact_c']['labelValue']='Explanation of COVID-19 Contact';
$dictionary['VM01_Visitor_Management']['fields']['explanation_contact_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explanation_contact_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explanation_contact_c']['dependency']='equal($contact_covid_19_c,"Yes")';

 ?>