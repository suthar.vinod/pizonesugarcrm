<?php
 // created: 2022-01-07 06:38:04
$dictionary['VM01_Visitor_Management']['fields']['outside_us_c']['labelValue']='Has visitor been outside the US anytime in the last 14 days?';
$dictionary['VM01_Visitor_Management']['fields']['outside_us_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['outside_us_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['outside_us_c']['visibility_grid']='';

 ?>