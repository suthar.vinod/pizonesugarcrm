<?php
 // created: 2020-10-13 11:17:40
$dictionary['VM01_Visitor_Management']['fields']['explanation_outside_us_c']['labelValue']='Explanation for Outside of US';
$dictionary['VM01_Visitor_Management']['fields']['explanation_outside_us_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explanation_outside_us_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explanation_outside_us_c']['dependency']='equal($outside_us_c,"Yes")';

 ?>