<?php
 // created: 2021-01-26 09:07:38
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['len']='11';
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['audited']=true;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['massupdate']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['hidemassupdate']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['duplicate_merge']='enabled';
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['duplicate_merge_dom_value']='1';
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['merge_filter']='disabled';
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['unified_search']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['calculated']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['enable_range_search']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['min']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['max']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['disable_num_format']='';

 ?>