<?php
 // created: 2020-11-24 08:41:46
$dictionary['VM01_Visitor_Management']['fields']['test_result_quarantine_c']['labelValue']='Are you or anyone in your household awaiting a COVID-19 test result, or have you or anyone in your household been asked to quarantine?';
$dictionary['VM01_Visitor_Management']['fields']['test_result_quarantine_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['test_result_quarantine_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['test_result_quarantine_c']['visibility_grid']='';

 ?>