<?php
 // created: 2019-08-27 12:04:58
$dictionary['VM01_Visitor_Management']['fields']['description']['audited']=true;
$dictionary['VM01_Visitor_Management']['fields']['description']['massupdate']=false;
$dictionary['VM01_Visitor_Management']['fields']['description']['comments']='Full text of the note';
$dictionary['VM01_Visitor_Management']['fields']['description']['duplicate_merge']='enabled';
$dictionary['VM01_Visitor_Management']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['VM01_Visitor_Management']['fields']['description']['merge_filter']='disabled';
$dictionary['VM01_Visitor_Management']['fields']['description']['unified_search']=false;
$dictionary['VM01_Visitor_Management']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['VM01_Visitor_Management']['fields']['description']['calculated']=false;
$dictionary['VM01_Visitor_Management']['fields']['description']['rows']='6';
$dictionary['VM01_Visitor_Management']['fields']['description']['cols']='80';

 ?>