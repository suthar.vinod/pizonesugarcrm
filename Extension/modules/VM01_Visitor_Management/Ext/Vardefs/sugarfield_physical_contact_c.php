<?php
 // created: 2021-12-14 12:34:23
$dictionary['VM01_Visitor_Management']['fields']['physical_contact_c']['labelValue']='Has visitor been in close physical contact in the last 14 days with: anyone who is known to have laboratory-confirmed COVID-19? OR Anyone who has any symptoms consistent with COVID-19?';
$dictionary['VM01_Visitor_Management']['fields']['physical_contact_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['physical_contact_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['physical_contact_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['physical_contact_c']['visibility_grid']='';

 ?>