<?php
 // created: 2019-08-27 11:57:28
$dictionary['VM01_Visitor_Management']['fields']['email_address_c']['labelValue']='Email Address';
$dictionary['VM01_Visitor_Management']['fields']['email_address_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['email_address_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['email_address_c']['dependency']='';

 ?>