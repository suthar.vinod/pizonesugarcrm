<?php
 // created: 2022-01-07 06:39:33
$dictionary['VM01_Visitor_Management']['fields']['visitor_confirmed_case_c']['labelValue']='Has there been a confirmed COVID-19 case at visitor\'s place of business within the past 14 days?';
$dictionary['VM01_Visitor_Management']['fields']['visitor_confirmed_case_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['visitor_confirmed_case_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['visitor_confirmed_case_c']['visibility_grid']='';

 ?>