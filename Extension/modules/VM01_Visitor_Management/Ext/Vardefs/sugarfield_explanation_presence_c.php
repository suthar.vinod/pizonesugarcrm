<?php
 // created: 2020-10-13 11:18:19
$dictionary['VM01_Visitor_Management']['fields']['explanation_presence_c']['labelValue']='Explanation for Mandatory Presence';
$dictionary['VM01_Visitor_Management']['fields']['explanation_presence_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explanation_presence_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explanation_presence_c']['dependency']='equal($mandatory_presence_c,"Yes")';

 ?>