<?php
 // created: 2021-02-18 16:11:44
$dictionary['II_Inventory_Item']['fields']['sterilization_exp_date_c']['labelValue']='Sterilization Expiration Date';
$dictionary['II_Inventory_Item']['fields']['sterilization_exp_date_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['sterilization_exp_date_c']['dependency']='equal($type_2,"Control Article")';
$dictionary['II_Inventory_Item']['fields']['sterilization_exp_date_c']['required_formula']='';

 ?>