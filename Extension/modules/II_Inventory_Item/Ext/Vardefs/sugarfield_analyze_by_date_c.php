<?php
 // created: 2021-04-15 10:30:47
$dictionary['II_Inventory_Item']['fields']['analyze_by_date_c']['labelValue']='Analyze by Date';
$dictionary['II_Inventory_Item']['fields']['analyze_by_date_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['analyze_by_date_c']['dependency']='equal($stability_considerations_c,"Yes")';
$dictionary['II_Inventory_Item']['fields']['analyze_by_date_c']['required_formula']='equal($stability_considerations_c,"Yes")';
$dictionary['II_Inventory_Item']['fields']['analyze_by_date_c']['readonly_formula']='';

 ?>