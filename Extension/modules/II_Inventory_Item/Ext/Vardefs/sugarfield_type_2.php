<?php
 // created: 2022-01-18 07:05:59
$dictionary['II_Inventory_Item']['fields']['type_2']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['type_2']['visibility_grid']=array (
  'trigger' => 'category',
  'values' => 
  array (
    'Product' => 
    array (
      0 => '',
      1 => 'Chemical',
      2 => 'Cleaning Agent',
      3 => 'Drug',
      4 => 'Reagent',
    ),
    'Record' => 
    array (
      0 => '',
      1 => 'Data Book',
      2 => 'Data Sheet',
      3 => 'Equipment Facility Record',
      4 => 'Hard Drive',
      5 => 'Protocol Book',
    ),
    'Solution' => 
    array (
    ),
    'Specimen' => 
    array (
      0 => '',
      1 => 'Balloons',
      2 => 'Block',
      3 => 'Culture',
      4 => 'EDTA Plasma',
      5 => 'Extract',
      6 => 'Fecal',
      7 => 'Na Heparin Plasma',
      8 => 'NaCit Plasma',
      9 => 'Other',
      10 => 'Serum',
      11 => 'Slide',
      12 => 'Tissue',
      13 => 'Urine',
      14 => 'Whole Blood',
    ),
    'Study Article' => 
    array (
      0 => '',
      1 => 'Accessory Article',
      2 => 'Control Article',
      3 => 'Test Article',
    ),
    '' => 
    array (
    ),
  ),
);
$dictionary['II_Inventory_Item']['fields']['type_2']['default']='';
$dictionary['II_Inventory_Item']['fields']['type_2']['required']=false;

 ?>