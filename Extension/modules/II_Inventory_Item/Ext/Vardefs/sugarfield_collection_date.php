<?php
 // created: 2022-03-31 11:45:53
$dictionary['II_Inventory_Item']['fields']['collection_date']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['collection_date']['required_formula']='equal($collection_date_time_type,"Date at Record Creation")';
$dictionary['II_Inventory_Item']['fields']['collection_date']['massupdate']=true;

 ?>