<?php
 // created: 2021-04-28 05:55:05
$dictionary['II_Inventory_Item']['fields']['planned_discard_date']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['planned_discard_date']['dependency']='isInList($status,createList("Onsite Archived","Onsite Archived Expired","Offsite Archived","Offsite Archived Expired"))';
$dictionary['II_Inventory_Item']['fields']['planned_discard_date']['required_formula']='isInList($status,createList("Onsite Archived","Onsite Archived Expired","Offsite Archived","Offsite Archived Expired"))';

 ?>