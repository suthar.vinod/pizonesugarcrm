<?php
 // created: 2021-02-05 09:44:22
$dictionary['II_Inventory_Item']['fields']['product_id_2_c']['labelValue']='Product ID';
$dictionary['II_Inventory_Item']['fields']['product_id_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['product_id_2_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['product_id_2_c']['dependency']='isInList($category,createList("Product","Solution"))';

 ?>