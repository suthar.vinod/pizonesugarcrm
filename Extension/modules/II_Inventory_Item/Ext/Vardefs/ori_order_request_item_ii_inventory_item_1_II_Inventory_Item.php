<?php
// created: 2021-02-25 08:49:48
$dictionary["II_Inventory_Item"]["fields"]["ori_order_request_item_ii_inventory_item_1"] = array (
  'name' => 'ori_order_request_item_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'ori_order_request_item_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'side' => 'right',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'ori_order_2123st_item_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["ori_order_request_item_ii_inventory_item_1_name"] = array (
  'name' => 'ori_order_request_item_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'save' => true,
  'id_name' => 'ori_order_2123st_item_ida',
  'link' => 'ori_order_request_item_ii_inventory_item_1',
  'table' => 'ori_order_request_item',
  'module' => 'ORI_Order_Request_Item',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["ori_order_2123st_item_ida"] = array (
  'name' => 'ori_order_2123st_item_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'ori_order_2123st_item_ida',
  'link' => 'ori_order_request_item_ii_inventory_item_1',
  'table' => 'ori_order_request_item',
  'module' => 'ORI_Order_Request_Item',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
