<?php
 // created: 2021-08-04 20:33:04
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['required']=true;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['audited']=true;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['massupdate']=true;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['duplicate_merge']='enabled';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['duplicate_merge_dom_value']='1';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['merge_filter']='disabled';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['reportable']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['unified_search']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['calculated']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['dependency']='isInList($related_to_c,createList("Purchase Order Item","Order Request Item"))';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['required_formula']='isInList($related_to_c,createList("Purchase Order Item","Order Request Item"))';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['vname']='LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_NAME_FIELD_TITLE';

 ?>