<?php
 // created: 2021-11-10 02:10:09
$dictionary['II_Inventory_Item']['fields']['quantity_2_c']['labelValue']='Quantity';
$dictionary['II_Inventory_Item']['fields']['quantity_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['quantity_2_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['quantity_2_c']['dependency']='isInList($related_to_c,createList("Purchase Order Item","Order Request Item"))';
$dictionary['II_Inventory_Item']['fields']['quantity_2_c']['required_formula']='isInList($related_to_c,createList("Purchase Order Item","Order Request Item"))';
$dictionary['II_Inventory_Item']['fields']['quantity_2_c']['readonly_formula']='';

 ?>