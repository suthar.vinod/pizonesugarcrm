<?php
 // created: 2021-11-10 00:27:06
$dictionary['II_Inventory_Item']['fields']['category']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['category']['visibility_grid']=array (
  'trigger' => 'related_to_c',
  'values' => 
  array (
    'Internal Use' => 
    array (
      0 => 'Record',
    ),
    'Multiple Test Systems' => 
    array (
      0 => 'Specimen',
    ),
    'Sales' => 
    array (
      0 => '',
      1 => 'Record',
      2 => 'Specimen',
      3 => 'Study Article',
    ),
    'Single Test System' => 
    array (
      0 => '',
      1 => 'Record',
      2 => 'Specimen',
    ),
    'Work Product' => 
    array (
      0 => '',
      1 => 'Record',
      2 => 'Specimen',
      3 => 'Study Article',
    ),
    '' => 
    array (
    ),
    'Order Request Item' => 
    array (
      0 => 'Product',
    ),
    'Purchase Order Item' => 
    array (
      0 => 'Product',
    ),
  ),
);

 ?>