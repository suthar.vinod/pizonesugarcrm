<?php
 // created: 2021-04-08 12:25:44
$dictionary['II_Inventory_Item']['fields']['timepoint_name_c']['labelValue']='Timepoint Name';
$dictionary['II_Inventory_Item']['fields']['timepoint_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['timepoint_name_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['timepoint_name_c']['dependency']='isInList($timepoint_type,createList("Defined","Ad Hoc"))';
$dictionary['II_Inventory_Item']['fields']['timepoint_name_c']['readonly_formula']='';

 ?>