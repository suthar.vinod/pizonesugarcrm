<?php
 // created: 2021-11-16 09:46:44
$dictionary['II_Inventory_Item']['fields']['stability_considerations_c']['labelValue']='PK Stability Considerations';
$dictionary['II_Inventory_Item']['fields']['stability_considerations_c']['dependency']='or(
isInList($type_2,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_1_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_2_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_3_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_4_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_5_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")))';
$dictionary['II_Inventory_Item']['fields']['stability_considerations_c']['required_formula']='or(
isInList($type_2,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_1_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_2_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_3_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_4_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_5_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")))';
$dictionary['II_Inventory_Item']['fields']['stability_considerations_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['stability_considerations_c']['visibility_grid']='';

 ?>