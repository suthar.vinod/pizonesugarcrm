<?php
 // created: 2021-07-09 12:43:51
$dictionary['II_Inventory_Item']['fields']['location_rack_c']['labelValue']='Location (Freezer Rack #)';
$dictionary['II_Inventory_Item']['fields']['location_rack_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['location_rack_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['location_rack_c']['dependency']='and(
and(
not(equal($location_type,"")),
not(equal($location_type,"Unknown"))),
equal($location_equi_serial_no_hide_c,"51014222")
)';
$dictionary['II_Inventory_Item']['fields']['location_rack_c']['required_formula']='and(
and(
not(equal($location_type,"")),
not(equal($location_type,"Unknown"))),
equal($location_equi_serial_no_hide_c,"51014222")
)';
$dictionary['II_Inventory_Item']['fields']['location_rack_c']['readonly_formula']='';

 ?>