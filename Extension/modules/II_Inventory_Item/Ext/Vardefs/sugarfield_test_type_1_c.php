<?php
 // created: 2022-04-12 09:06:19
$dictionary['II_Inventory_Item']['fields']['test_type_1_c']['labelValue']='Test Type 1';
$dictionary['II_Inventory_Item']['fields']['test_type_1_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['test_type_1_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['test_type_1_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['test_type_1_c']['visibility_grid']=array (
  'trigger' => 'specimen_type_1_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Culture' => 
    array (
      0 => '',
      1 => 'Aerobic and Anaerobic Culture ID Bacteria and Fungi',
      2 => 'Aerobic Culture and ID Bacteria only',
      3 => 'Aerobic Culture and ID Fungi only',
      4 => 'Blood Culture',
    ),
    'Fecal' => 
    array (
      0 => '',
      1 => 'Cryptosporidium ELISA',
      2 => 'Fecal Ova and Parasite',
      3 => 'Giardia ELISA',
    ),
    'NaCit Plasma' => 
    array (
      0 => '',
      1 => 'Anti Factor Xa',
      2 => 'Coagulation',
      3 => 'D Dimer',
      4 => 'Fibrinogen',
      5 => 'PT aPTT',
      6 => 'PT aPTT Fibrinogen',
    ),
    'Na Heparin Plasma' => 
    array (
      0 => '',
      1 => 'Free Hemoglobin',
    ),
    'Other' => 
    array (
      0 => '',
      1 => 'Other',
    ),
    'Serum' => 
    array (
      0 => '',
      1 => 'Amylase andor Lipase',
      2 => 'Haptoglobin',
      3 => 'Ionized Calcium',
      4 => 'PK in Serum',
      5 => 'Troponin 1',
    ),
    'Urine' => 
    array (
      0 => '',
      1 => 'Urinalysis',
      2 => 'Urine ProteinCreatinine Ratio',
    ),
    'Whole Blood' => 
    array (
      0 => '',
      1 => 'CBC',
      2 => 'CBC plus Heartworm',
      3 => 'CBC with Reticulocytes',
      4 => 'Chemistry',
      5 => 'Custom Chemistry',
      6 => 'Other',
      7 => 'PK in Plasma',
      8 => 'PK in Serum',
      9 => 'PK in Whole Blood',
    ),
    'EDTA Plasma' => 
    array (
      0 => '',
      1 => 'PK in Plasma',
    ),
    'Balloons' => 
    array (
      0 => '',
      1 => 'Other',
      2 => 'PK in Balloons',
    ),
  ),
);

 ?>