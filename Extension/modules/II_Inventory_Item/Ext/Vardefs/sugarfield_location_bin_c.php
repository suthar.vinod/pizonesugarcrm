<?php
 // created: 2021-05-31 11:55:50
$dictionary['II_Inventory_Item']['fields']['location_bin_c']['labelValue']='Location (Bin #)';
$dictionary['II_Inventory_Item']['fields']['location_bin_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['location_bin_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['location_bin_c']['dependency']='and(
and(
not(equal($location_type,"")),
not(equal($location_type,"Unknown"))),
equal($location_equi_serial_no_hide_c,"51014222")
)';
$dictionary['II_Inventory_Item']['fields']['location_bin_c']['required_formula']='and(
and(
not(equal($location_type,"")),
not(equal($location_type,"Unknown"))),
equal($location_equi_serial_no_hide_c,"51014222")
)';
$dictionary['II_Inventory_Item']['fields']['location_bin_c']['readonly_formula']='';

 ?>