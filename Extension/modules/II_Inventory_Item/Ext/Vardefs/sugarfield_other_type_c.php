<?php
 // created: 2022-02-01 06:54:34
$dictionary['II_Inventory_Item']['fields']['other_type_c']['labelValue']='Other Specimen Type';
$dictionary['II_Inventory_Item']['fields']['other_type_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['other_type_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['other_type_c']['dependency']='or(and(equal($type_2,"Other"),equal($category,"Specimen")),equal($specimen_type_1_c,"Other"))';
$dictionary['II_Inventory_Item']['fields']['other_type_c']['required_formula']='or(and(equal($type_2,"Other"),equal($category,"Specimen")),equal($specimen_type_1_c,"Other"))';
$dictionary['II_Inventory_Item']['fields']['other_type_c']['readonly_formula']='';

 ?>