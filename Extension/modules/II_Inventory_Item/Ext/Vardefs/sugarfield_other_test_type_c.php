<?php
 // created: 2022-02-01 06:56:22
$dictionary['II_Inventory_Item']['fields']['other_test_type_c']['labelValue']='Other Test Type';
$dictionary['II_Inventory_Item']['fields']['other_test_type_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['other_test_type_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['other_test_type_c']['dependency']='or(equal($test_type_c,"Other"),equal($test_type_1_c,"Other"))';
$dictionary['II_Inventory_Item']['fields']['other_test_type_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['other_test_type_c']['readonly_formula']='';

 ?>