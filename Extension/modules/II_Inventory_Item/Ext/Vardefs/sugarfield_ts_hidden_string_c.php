<?php
 // created: 2021-06-03 12:06:06
$dictionary['II_Inventory_Item']['fields']['ts_hidden_string_c']['labelValue']='TS Hidden String';
$dictionary['II_Inventory_Item']['fields']['ts_hidden_string_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['ts_hidden_string_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['ts_hidden_string_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['ts_hidden_string_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['ts_hidden_string_c']['readonly_formula']='';

 ?>