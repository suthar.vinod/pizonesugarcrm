<?php
 // created: 2021-04-12 09:34:57
$dictionary['II_Inventory_Item']['fields']['location_equi_serial_no_hide_c']['labelValue']='Location Equi Sr No Hidden';
$dictionary['II_Inventory_Item']['fields']['location_equi_serial_no_hide_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['location_equi_serial_no_hide_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['location_equi_serial_no_hide_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['location_equi_serial_no_hide_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['location_equi_serial_no_hide_c']['readonly_formula']='';

 ?>