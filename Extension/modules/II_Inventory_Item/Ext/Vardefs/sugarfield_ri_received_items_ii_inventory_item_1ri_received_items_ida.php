<?php
 // created: 2021-08-04 20:33:03
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['name']='ri_received_items_ii_inventory_item_1ri_received_items_ida';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['type']='id';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['source']='non-db';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['vname']='LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['id_name']='ri_received_items_ii_inventory_item_1ri_received_items_ida';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['link']='ri_received_items_ii_inventory_item_1';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['table']='ri_received_items';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['module']='RI_Received_Items';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['rname']='id';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['reportable']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['side']='right';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['massupdate']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['duplicate_merge']='disabled';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['hideacl']=true;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['audited']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['importable']='true';

 ?>