<?php
 // created: 2021-04-28 05:48:57
$dictionary['II_Inventory_Item']['fields']['expiration_date']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['expiration_date']['required_formula']='or(equal($category,"Product"),isInList($type_2,createList("Accessory Article","Control Article")),and(equal($type_2,"Test Article"),equal($expiration_test_article,"Known")))';

 ?>