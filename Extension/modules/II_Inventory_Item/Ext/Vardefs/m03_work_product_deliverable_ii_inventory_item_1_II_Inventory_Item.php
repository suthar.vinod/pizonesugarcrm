<?php
// created: 2022-02-22 07:44:29
$dictionary["II_Inventory_Item"]["fields"]["m03_work_product_deliverable_ii_inventory_item_1"] = array (
  'name' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Deliverable',
  'bean_name' => 'M03_Work_Product_Deliverable',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'm03_work_p7e14verable_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["m03_work_product_deliverable_ii_inventory_item_1_name"] = array (
  'name' => 'm03_work_product_deliverable_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p7e14verable_ida',
  'link' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'table' => 'm03_work_product_deliverable',
  'module' => 'M03_Work_Product_Deliverable',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["m03_work_p7e14verable_ida"] = array (
  'name' => 'm03_work_p7e14verable_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'm03_work_p7e14verable_ida',
  'link' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'table' => 'm03_work_product_deliverable',
  'module' => 'M03_Work_Product_Deliverable',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
