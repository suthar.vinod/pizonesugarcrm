<?php
 // created: 2021-08-13 12:16:19
$dictionary['II_Inventory_Item']['fields']['quantity_c']['default']='1';
$dictionary['II_Inventory_Item']['fields']['quantity_c']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['quantity_c']['enable_range_search']=false;
$dictionary['II_Inventory_Item']['fields']['quantity_c']['min']=1;
$dictionary['II_Inventory_Item']['fields']['quantity_c']['max']=false;
$dictionary['II_Inventory_Item']['fields']['quantity_c']['disable_num_format']='';
$dictionary['II_Inventory_Item']['fields']['quantity_c']['required_formula']='isInList($category,createList("Study Article"))';
$dictionary['II_Inventory_Item']['fields']['quantity_c']['dependency']='isInList($category,createList("Study Article"))';

 ?>