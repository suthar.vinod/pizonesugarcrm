<?php
 // created: 2021-07-08 19:23:33
$dictionary['II_Inventory_Item']['fields']['location_cubby_c']['labelValue']='Location (Slot #)';
$dictionary['II_Inventory_Item']['fields']['location_cubby_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['location_cubby_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['location_cubby_c']['dependency']='and(
and(
not(equal($location_type,"")),
not(equal($location_type,"Unknown"))),
equal($location_equi_serial_no_hide_c,"51014222")
)';
$dictionary['II_Inventory_Item']['fields']['location_cubby_c']['required_formula']='and(
and(
not(equal($location_type,"")),
not(equal($location_type,"Unknown"))),
equal($location_equi_serial_no_hide_c,"51014222")
)';
$dictionary['II_Inventory_Item']['fields']['location_cubby_c']['readonly_formula']='';

 ?>