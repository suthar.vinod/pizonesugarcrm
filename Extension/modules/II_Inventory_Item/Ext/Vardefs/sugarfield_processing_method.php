<?php
 // created: 2021-02-16 16:39:57
$dictionary['II_Inventory_Item']['fields']['processing_method']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['processing_method']['visibility_grid']=array (
  'trigger' => 'type_2',
  'values' => 
  array (
    'Accessory Article' => 
    array (
    ),
    'Block' => 
    array (
    ),
    'Control Article' => 
    array (
    ),
    'Culture' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Data Book' => 
    array (
    ),
    'Data Sheet' => 
    array (
    ),
    'Equipment Facility Record' => 
    array (
    ),
    'Extract' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Fecal' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Hard Drive' => 
    array (
    ),
    'Plasma' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Protocol Book' => 
    array (
    ),
    'Serum' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Slide' => 
    array (
    ),
    'Test Article' => 
    array (
    ),
    'Tissue' => 
    array (
    ),
    'Urine' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Whole Blood' => 
    array (
      0 => '',
      1 => '1',
    ),
    '' => 
    array (
    ),
    'Accessory Product' => 
    array (
    ),
    'Chemical' => 
    array (
    ),
    'Cleaning Agent' => 
    array (
    ),
    'Drug' => 
    array (
    ),
    'EDTA Plasma' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Hard drive' => 
    array (
    ),
    'Na Heparin Plasma' => 
    array (
      0 => '',
      1 => '1',
    ),
    'NaCit Plasma' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Reagent' => 
    array (
    ),
  ),
);

 ?>