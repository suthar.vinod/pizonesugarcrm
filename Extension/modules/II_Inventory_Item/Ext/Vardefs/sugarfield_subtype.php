<?php
 // created: 2020-08-29 07:47:49
$dictionary['II_Inventory_Item']['fields']['subtype']['required']=false;
$dictionary['II_Inventory_Item']['fields']['subtype']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['subtype']['massupdate']=false;
$dictionary['II_Inventory_Item']['fields']['subtype']['options']='pro_subtype_list';
$dictionary['II_Inventory_Item']['fields']['subtype']['dependency']='or(equal($category,"Product"),equal($type_2,"Block"),equal($type_2,"Culture"),equal($type_2,"Slide"),equal($type_2,"Tissue"),equal($type_2,"Equipment Facility Record"))';
$dictionary['II_Inventory_Item']['fields']['subtype']['default']='';

 ?>