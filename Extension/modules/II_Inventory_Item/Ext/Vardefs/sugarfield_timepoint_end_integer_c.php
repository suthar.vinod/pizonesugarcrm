<?php
 // created: 2021-04-28 12:34:02
$dictionary['II_Inventory_Item']['fields']['timepoint_end_integer_c']['labelValue']='Timepoint End Integer';
$dictionary['II_Inventory_Item']['fields']['timepoint_end_integer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['timepoint_end_integer_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['timepoint_end_integer_c']['dependency']='isInList($timepoint_type,createList("Defined","Ad Hoc"))';
$dictionary['II_Inventory_Item']['fields']['timepoint_end_integer_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['timepoint_end_integer_c']['readonly_formula']='';

 ?>