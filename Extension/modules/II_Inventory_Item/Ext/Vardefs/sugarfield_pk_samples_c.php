<?php
 // created: 2022-02-01 07:08:34
$dictionary['II_Inventory_Item']['fields']['pk_samples_c']['labelValue']='Multiple Aliquots Needed?';
$dictionary['II_Inventory_Item']['fields']['pk_samples_c']['dependency']='equal($category,"Specimen")';
$dictionary['II_Inventory_Item']['fields']['pk_samples_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['pk_samples_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['pk_samples_c']['visibility_grid']='';

 ?>