<?php
// created: 2020-07-31 13:15:46
$dictionary["II_Inventory_Item"]["fields"]["poi_purchase_order_item_ii_inventory_item_1"] = array (
  'name' => 'poi_purchase_order_item_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'poi_purchase_order_item_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'POI_Purchase_Order_Item',
  'bean_name' => 'POI_Purchase_Order_Item',
  'side' => 'right',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'poi_purcha8945er_item_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["poi_purchase_order_item_ii_inventory_item_1_name"] = array (
  'name' => 'poi_purchase_order_item_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'save' => true,
  'id_name' => 'poi_purcha8945er_item_ida',
  'link' => 'poi_purchase_order_item_ii_inventory_item_1',
  'table' => 'poi_purchase_order_item',
  'module' => 'POI_Purchase_Order_Item',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["poi_purcha8945er_item_ida"] = array (
  'name' => 'poi_purcha8945er_item_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'poi_purcha8945er_item_ida',
  'link' => 'poi_purchase_order_item_ii_inventory_item_1',
  'table' => 'poi_purchase_order_item',
  'module' => 'POI_Purchase_Order_Item',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
