<?php
$dictionary['II_Inventory_Item']['fields']['inactive_c'] = array(
    'name'            => 'inactive_c',
    'label'           => 'Inactive',
    'type'            => 'bool',
    'module'          => 'II_Inventory_Item',
    'default_value'   => true,
    'help'            => '',
    'comment'         => '',
    'audited'         => false,
    'mass_update'     => false,
    'duplicate_merge' => false,
    'reportable'      => true,
    'importable'      => 'true',
);

//$dictionary['II_Inventory_Item']['fields']['name']['fields'] = array(0 => 'name', 1 => 'inactive_c');
