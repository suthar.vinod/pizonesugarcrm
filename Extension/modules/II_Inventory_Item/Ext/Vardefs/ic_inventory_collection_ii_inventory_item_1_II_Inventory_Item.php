<?php
// created: 2020-07-31 13:29:35
$dictionary["II_Inventory_Item"]["fields"]["ic_inventory_collection_ii_inventory_item_1"] = array (
  'name' => 'ic_inventory_collection_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'ic_inventory_collection_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'IC_Inventory_Collection',
  'bean_name' => 'IC_Inventory_Collection',
  'side' => 'right',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'ic_invento128flection_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["ic_inventory_collection_ii_inventory_item_1_name"] = array (
  'name' => 'ic_inventory_collection_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'save' => true,
  'id_name' => 'ic_invento128flection_ida',
  'link' => 'ic_inventory_collection_ii_inventory_item_1',
  'table' => 'ic_inventory_collection',
  'module' => 'IC_Inventory_Collection',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["ic_invento128flection_ida"] = array (
  'name' => 'ic_invento128flection_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'ic_invento128flection_ida',
  'link' => 'ic_inventory_collection_ii_inventory_item_1',
  'table' => 'ic_inventory_collection',
  'module' => 'IC_Inventory_Collection',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
