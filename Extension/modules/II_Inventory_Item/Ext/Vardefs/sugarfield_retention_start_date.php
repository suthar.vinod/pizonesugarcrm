<?php
 // created: 2021-04-28 05:49:43
$dictionary['II_Inventory_Item']['fields']['retention_start_date']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['retention_start_date']['dependency']='isInList($status,createList("Onsite Archived","Onsite Archived Expired","Offsite Archived","Offsite Archived Expired"))';
$dictionary['II_Inventory_Item']['fields']['retention_start_date']['required_formula']='isInList($status,createList("Onsite Archived","Onsite Archived Expired","Offsite Archived","Offsite Archived Expired"))';

 ?>