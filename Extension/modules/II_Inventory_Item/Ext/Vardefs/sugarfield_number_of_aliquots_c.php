<?php
 // created: 2022-02-24 08:30:16
$dictionary['II_Inventory_Item']['fields']['number_of_aliquots_c']['labelValue']='Number of Aliquots';
$dictionary['II_Inventory_Item']['fields']['number_of_aliquots_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['number_of_aliquots_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['number_of_aliquots_c']['dependency']='equal($pk_samples_c,"Yes")';
$dictionary['II_Inventory_Item']['fields']['number_of_aliquots_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['number_of_aliquots_c']['readonly_formula']='';

 ?>