<?php
$dependencies['II_Inventory_Item']['set_multi_vis_drop'] = array(
    'hooks' => array("all"),
    'trigger' => 'ifElse(equal($category,"Specimen"),true,false)',
    'triggerFields' => array('type_2'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'test_type_c',
                'keys' => 'ifElse(
                    and(equal($type_2,"Whole Blood"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","CBC","CBC with Reticulocytes","Chemistry","Custom Chemistry","PK in Whole Blood","PK in Serum","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Whole Blood"),equal($pk_samples_c,"NA")),
                    createList("","PK in Whole Blood","Other"),
                    ifElse(
                    and(equal($type_2,"Balloons"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Other","PK in Balloons"),
                    ifElse(
                    and(equal($type_2,"Balloons"),equal($pk_samples_c,"NA")),
                    createList("","Other","PK in Balloons"),
                    ifElse(
                    and(equal($type_2,"Serum"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Amylase andor Lipase","Haptoglobin","Troponin1","Ionized Calcium","PK in Serum"),
                    ifElse(
                    and(equal($type_2,"Serum"),equal($pk_samples_c,"NA")),
                    createList("","PK in Serum","Other"),
                    ifElse(
                    and(equal($type_2,"EDTA Plasma"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"EDTA Plasma"),equal($pk_samples_c,"NA")),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Na Heparin Plasma"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Free Hemoglobin"),
                    ifElse(
                    and(equal($type_2,"Na Heparin Plasma"),equal($pk_samples_c,"NA")),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"NaCit Plasma"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Coagulation","Anti Factor Xa","D Dimer","Fibrinogen","PT aPTT","PT aPTT Fibrinogen"),
                    ifElse(
                    and(equal($type_2,"NaCit Plasma"),equal($pk_samples_c,"NA")),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Urine"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Urinalysis","Urine ProteinCreatinine Ratio"),
                    ifElse(
                    and(equal($type_2,"Urine"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Fecal"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Fecal Ova and Parasite","Cryptosporidium ELISA","Giardia ELISA"),
                    ifElse(
                    and(equal($type_2,"Fecal"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Other"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Other"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Culture"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Aerobic and Anaerobic Culture ID Bacteria and Fungi","Aerobic Culture and ID Bacteria only","Aerobic Culture and ID Fungi only","Blood Culture"),
                    ifElse(
                    and(equal($type_2,"Culture"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Whole Blood"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Whole Blood","Other"),
                    ifElse(
                    and(equal($type_2,"Balloons"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Balloons","Other"),
                    ifElse(
                    and(equal($type_2,"Serum"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Serum","Other"),
                    ifElse(
                    and(equal($type_2,"EDTA Plasma"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Na Heparin Plasma"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"NaCit Plasma"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Urine"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Fecal"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Other"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Culture"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Block"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Extract"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Slide"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Tissue"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Tissue","Other"),
                    ifElse(
                    and(equal($type_2,"Extract"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Slide"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Tissue"),equal($pk_samples_c,"NA")),
                    createList("","PK in Tissue","Other"),
                    ifElse(
                    and(equal($type_2,"Block"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    createList("")))))))))))))))))))))))))))))))))))))))',
                'labels' => 'ifElse(
                    and(equal($type_2,"Whole Blood"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","CBC","CBC with Reticulocytes","Chemistry","Custom Chemistry","Pharmacokinetic (PK) in Whole Blood","Pharmacokinetic (PK) in Serum","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Whole Blood"),equal($pk_samples_c,"NA")),
                    createList("","Pharmacokinetic (PK) in Whole Blood","Other"),
                    ifElse(
                    and(equal($type_2,"Balloons"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Other","Pharmacokinetic (PK) in Balloons"),
                    ifElse(
                    and(equal($type_2,"Balloons"),equal($pk_samples_c,"NA")),
                    createList("","Other","Pharmacokinetic (PK) in Balloons"),
                    ifElse(
                    and(equal($type_2,"Serum"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Amylase and/or Lipase","Haptoglobin","Troponin1","Ionized Calcium","Pharmacokinetic (PK) in Serum"),
                    ifElse(
                    and(equal($type_2,"Serum"),equal($pk_samples_c,"NA")),
                    createList("","Pharmacokinetic (PK) in Serum","Other"),
                    ifElse(
                    and(equal($type_2,"EDTA Plasma"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"EDTA Plasma"),equal($pk_samples_c,"NA")),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Na Heparin Plasma"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Free Hemoglobin"),
                    ifElse(
                    and(equal($type_2,"Na Heparin Plasma"),equal($pk_samples_c,"NA")),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"NaCit Plasma"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Coagulation","Anti-Factor Xa","D-Dimer","Fibrinogen","PT, aPTT","PT, aPTT, Fibrinogen"),
                    ifElse(
                    and(equal($type_2,"NaCit Plasma"),equal($pk_samples_c,"NA")),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Urine"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Urinalysis","Urine Protein/Creatinine Ratio"),
                    ifElse(
                    and(equal($type_2,"Urine"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Fecal"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Fecal Ova and Parasite","Cryptosporidium ELISA","Giardia ELISA"),
                    ifElse(
                    and(equal($type_2,"Fecal"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Other"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Other"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Culture"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Aerobic and Anaerobic Culture, ID-Bacteria and Fungi","Aerobic Culture and ID-Bacteria only","Aerobic Culture and ID-Fungi only","Blood Culture"),
                    ifElse(
                    and(equal($type_2,"Culture"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Whole Blood"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Whole Blood","Other"),
                    ifElse(
                    and(equal($type_2,"Balloons"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Balloons","Other"),
                    ifElse(
                    and(equal($type_2,"Serum"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Serum","Other"),
                    ifElse(
                    and(equal($type_2,"EDTA Plasma"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Na Heparin Plasma"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"NaCit Plasma"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Urine"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Fecal"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Other"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Culture"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Block"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Extract"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Slide"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Tissue"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Tissue","Other"),
                    ifElse(
                    and(equal($type_2,"Extract"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Slide"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Tissue"),equal($pk_samples_c,"NA")),
                    createList("","Pharmacokinetic (PK) in Tissue","Other"),
                    ifElse(
                    and(equal($type_2,"Block"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    createList("")))))))))))))))))))))))))))))))))))))))'
            ),
        ),
    ),
);

$dependencies['II_Inventory_Item']['set_vis_drop01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'test_type_c',
                'value'  => 'and(equal($category,"Specimen"),isInList($type_2,createList("Balloons","Whole Blood","Serum","EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Urine","Fecal","Culture","Other","Tissue","Slide","Block","Extract")))',
            ),
        ),
    ),
);
$dependencies['II_Inventory_Item']['set_vis_type2_123'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('pk_samples_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'type_2',
                'value'  => 'and(equal($category,"Specimen"),or(equal($pk_samples_c,"Yes"),equal($pk_samples_c,"NA")))',
            ),
        ),
    ),
);
