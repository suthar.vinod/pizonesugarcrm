<?php
$dependencies['II_Inventory_Item']['ii_inventory_collection_visibility_dep'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('ic_inventory_collection_ii_inventory_item_1_name'),
    'onload'        => true,
    'actions'       => array(     
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ic_inventory_collection_ii_inventory_item_1_name',
                'value'  => 'not(equal($ic_inventory_collection_ii_inventory_item_1_name, ""))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ic_inventory_collection_ii_inventory_item_1_name',
                'value'  => 'not(equal($ic_inventory_collection_ii_inventory_item_1_name, ""))',
            ),
        ),
     ),
);