<?php

$dependencies['II_Inventory_Item']['poi_ori_visibility_dep'] = array(
    'hooks'         => array("all"),
    'triggerFields' => array('related_to_c'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'poi_purchase_order_item_ii_inventory_item_1_name',
                'value'  => 'equal($related_to_c, "Purchase Order Item")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'poi_purchase_order_item_ii_inventory_item_1_name',
                'value'  => 'equal($related_to_c, "Purchase Order Item")',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ori_order_request_item_ii_inventory_item_1_name',
                'value'  => 'equal($related_to_c, "Order Request Item")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ori_order_request_item_ii_inventory_item_1_name',
                'value'  => 'equal($related_to_c, "Order Request Item")',
            ),
        ),
    ),
);
/*To set type and subtype list from ori/poi into II type and subtype */
$dependencies['II_Inventory_Item']['setoptions_type'] = array(
    'hooks' => array("all"),
    'trigger' => 'or(equal($related_to_c, "Purchase Order Item"),equal($related_to_c, "Order Request Item"))',
    'triggerFields' => array('related_to_c','type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'type_2',
                'keys' => 'getDropdownKeySet("product_category_list")',
                'labels' => 'getDropdownValueSet("product_category_list")'
            ),
        ),
    ),
);

