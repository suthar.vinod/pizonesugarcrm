<?php

$dependencies['II_Inventory_Item']['shipping_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('ship_to_contact', 'ship_to_address', 'ship_to_company'),
    'onload'        => true,
    'actions'       => array(

        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'ship_to_contact',
                'label'  => 'ship_to_contact_label',
                'value'  => 'greaterThan(strlen($id), 0)',
            ),
        ),
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'ship_to_address',
                'label'  => 'ship_to_address_label',
                'value'  => 'greaterThan(strlen($id), 0)',
            ),
        ),
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'ship_to_company',
                'label'  => 'ship_to_company_label',
                'value'  => 'greaterThan(strlen($id), 0)',
            ),
        ),
		
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ship_to_contact',
                'value'  => 'not(equal(strlen($ship_to_contact), 0))',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ship_to_address',
                'value'  => 'not(equal(strlen($ship_to_address), 0))',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ship_to_company',
                'value'  => 'not(equal(strlen($ship_to_company), 0))',
            ),
        ),

        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ship_to_contact',
                'value'  => 'true',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ship_to_address',
                'value'  => 'true',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ship_to_company',
                'value'  => 'true',
            ),
        ),
    ),
);
