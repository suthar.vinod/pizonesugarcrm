<?php

$dependencies['II_Inventory_Item']['setoptions_location_type'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('test_ii_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'location_type',
                'keys' => 'ifElse(greaterThan(strlen($test_ii_c),0), getDropdownKeySet("im_location_type_list"), getDropdownKeySet("known_unknown_list"))',
                'labels' => 'ifElse(greaterThan(strlen($test_ii_c),0), getDropdownValueSet("im_location_type_list"), getDropdownValueSet("known_unknown_list"))'
            ),
        ),
    ),
);

$dependencies['II_Inventory_Item']['location_dep'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('location_type', 'location_building', 'location_room', 'location_shelf', 'location_cabinet', 'location_equipment', 'storage_medium', 'storage_condition', 'test_ii_c','status'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'location_room',
                'value' => 'not(greaterThan(strlen($test_ii_c), 0))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'location_type',
                'value' => 'not(greaterThan(strlen($ship_to_contact), 0))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'location_building',
                'value' => 'not(greaterThan(strlen($ship_to_contact), 0))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'location_room',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),equal($location_type,"Known"))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_type',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),not(greaterThan(strlen($ship_to_address), 0)),not(greaterThan(strlen($ship_to_company), 0)), not(equal($status, "Discarded")),not(equal($status, "Obsolete")),not(equal($status, "Exhausted")))',                
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_building',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),isInList($location_type, createList("Known", "Cabinet", "Equipment", "Room", "RoomCabinet", "RoomShelf", "Shelf")))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_room',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),isInList($location_type, createList("Known", "Cabinet", "Equipment", "Room", "RoomCabinet", "RoomShelf", "Shelf")))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_equipment',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),isInList($location_type, createList("Known", "Cabinet", "Equipment", "Room", "RoomCabinet", "RoomShelf", "Shelf")))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_shelf',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),isInList($location_type, createList("Known", "Cabinet", "Equipment", "Room", "RoomCabinet", "RoomShelf", "Shelf")))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_cabinet',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),isInList($location_type, createList("Known", "Cabinet", "Equipment", "Room", "RoomCabinet", "RoomShelf", "Shelf")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_type',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        //, not(equal(strlen($location_type), "Known"),equal(strlen($location_type), "Unknown"))  
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_building',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_room',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_shelf',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_equipment',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_cabinet',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        /*array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'storage_condition',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),greaterThan(strlen(related($ii_inventory_item_im_inventory_management_1,"end_condition")),0),greaterThan(strlen($storage_condition),0))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'storage_medium',
                'value' => 'or(greaterThan(strlen($test_ii_c),0),greaterThan(strlen(related($ii_inventory_item_im_inventory_management_1,"ending_storage_media")),0))',
            ),
        ),*/

        /* Prakash 1117 */
       /* array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_bin_c',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_cubby_c',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_rack_c',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),*/
    ),
);
/*#391 : 03 March 2021 */
$dependencies['II_Inventory_Item']['Shipping_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Cabinet", "Equipment", "Room", "RoomCabinet", "RoomShelf", "Shelf"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'contact_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'ca_company_address_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'account_id_c',
                'value'  => '',
            ),
        ),    
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'ship_to_contact',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'ship_to_company',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'ship_to_address',
                'value'  => '',
            ),
        ),
       
     ),
);
/*#391 : 08 March 2021 */
$dependencies['II_Inventory_Item']['cabinet_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Cabinet"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'rms_room_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_room',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['II_Inventory_Item']['equipement_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Equipment"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'rms_room_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_room',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['II_Inventory_Item']['shelf_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Shelf"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'rms_room_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_room',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['II_Inventory_Item']['room_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Room"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['II_Inventory_Item']['roomshelf_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("RoomShelf"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['II_Inventory_Item']['roomscabinet_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("RoomCabinet"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
       
     ),
);
/**12 nov 2021 : prod bug fix 391 */
/*#391 : 03 March 2021 */
$dependencies['II_Inventory_Item']['storage_dep'] = array(
    'hooks'         => array("all"),
    'trigger' => 'and(greaterThan(strlen($test_ii_c),0),greaterThan(strlen(related($ii_inventory_item_im_inventory_management_1,"end_condition")),0),greaterThan(strlen($storage_condition),0))',
    'triggerFields' => array('storage_condition','test_ii_c','ii_inventory_item_im_inventory_management_1','end_condition'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'storage_condition',
                'value' => 'true',
            ),
        ),           
       
     ),
);

$dependencies['II_Inventory_Item']['storage_medium_dep'] = array(
    'hooks'         => array("all"),
    'trigger' => 'or(greaterThan(strlen($test_ii_c),0),greaterThan(strlen(related($ii_inventory_item_im_inventory_management_1,"ending_storage_media")),0))',
    'triggerFields' => array('storage_medium','test_ii_c','ii_inventory_item_im_inventory_management_1','ending_storage_media'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'storage_medium',
                'value' => 'true',
            ),
        ),           
       
     ),
);

/**////////// */
/*12 nov 2021 : 1117 bug fix prod */
$dependencies['II_Inventory_Item']['location_bin_cubb_rack_dep'] = array(
    'hooks'         => array("all"),
    'trigger' => 'and(greaterThan(strlen($test_ii_c),0),not(isInList($location_type,createList("Known","Unknown"))))',
    'triggerFields' => array('test_ii_c','location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_bin_c',
                'value' => 'true',
            ),
        ), 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_cubby_c',
                'value' => 'true',
            ),
        ), 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_rack_c',
                'value' => 'true',
            ),
        ),           
       
     ),
);
/**/////// */
