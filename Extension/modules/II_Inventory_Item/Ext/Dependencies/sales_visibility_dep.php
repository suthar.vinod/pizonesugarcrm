<?php

$dependencies['II_Inventory_Item']['sales_visibility_dep'] = array(
    'hooks'         => array("edit"),
    'triggerFields' => array('related_to_c'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'm01_sales_ii_inventory_item_1_name',
                'value'  => 'equal($related_to_c, "Sales")',
            ),
        ),
    ),
);
