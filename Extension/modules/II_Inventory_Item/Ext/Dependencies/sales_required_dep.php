<?php

$dependencies['II_Inventory_Item']['sales_required_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('m01_sales_ii_inventory_item_1_name', 'related_to_c'),
    'onload'        => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions'       => array(
        array(
            'name'   => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'm01_sales_ii_inventory_item_1_name',
                'label'  => 'sales_required_label',
                'value'  => 'equal($related_to_c, "Sales")',
            ),
        ),
    ),
);
