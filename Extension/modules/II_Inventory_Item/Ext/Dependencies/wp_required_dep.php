<?php

$dependencies['II_Inventory_Item']['wp_required_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('m03_work_product_ii_inventory_item_1_name', 'related_to_c'),
    'onload'        => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions'       => array(
        array(
            'name'   => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'm03_work_product_ii_inventory_item_1_name',
                'label'  => 'sales_required_label',
                'value'  => 'or(equal($related_to_c, "Work Product"),equal($related_to_c, "Single Test System"),equal($related_to_c, "Multiple Test Systems"))',
            ),
        ),
    ),
);

$dependencies['II_Inventory_Item']['ii_required_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array(
        'id',
        'm01_sales_ii_inventory_item_1_name',
        'm03_work_product_ii_inventory_item_1_name',
        'anml_animals_ii_inventory_item_1_name',
        'category',
        'type_2',
        'unique_id_type',
        'unique_id_c',
        'subtype',
    ),
    'onload'        => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions'       => array(
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'quantity_c',
                'label'  => 'quantity_c_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'type_2',
                'label'  => 'type_2_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'm03_work_product_ii_inventory_item_1_name',
                'label'  => 'm03_work_product_ii_inventory_item_1_name_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'm01_sales_ii_inventory_item_1_name',
                'label'  => 'm01_sales_ii_inventory_item_1_name_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'anml_animals_ii_inventory_item_1_name',
                'label'  => 'anml_animals_ii_inventory_item_1_name_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'category',
                'label'  => 'category_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),

        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'unique_id_type',
                'label'  => 'unique_id_type_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'unique_id_c',
                'label'  => 'unique_id_c_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'subtype',
                'label'  => 'subtype_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
    ),
);
