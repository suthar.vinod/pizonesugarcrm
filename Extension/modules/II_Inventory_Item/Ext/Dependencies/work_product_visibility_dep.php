<?php

$dependencies['II_Inventory_Item']['work_product_visibility_dep'] = array(
    'hooks'         => array("edit"),
    'triggerFields' => array('related_to_c'),
    'onload'        => true,

    'actions' => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_ii_inventory_item_1_name',
                'value'  => 'or(equal($related_to_c, "Work Product"),equal($related_to_c, "Single Test System"),equal($related_to_c, "Multiple Test Systems"))',
            ),
        ),
    ),
);
$dependencies['II_Inventory_Item']['test_system_visibility_dep'] = array(
    'hooks'         => array("edit"),
    'triggerFields' => array('related_to_c'),
    'onload'        => true,

    'actions' => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'anml_animals_ii_inventory_item_1_name',                
                'value'  => 'or(equal($related_to_c, "Single Test System"),equal($related_to_c, "Multiple Test Systems"))',
            ),
        ),
    ),
);