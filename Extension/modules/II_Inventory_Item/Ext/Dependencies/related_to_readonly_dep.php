<?php

$dependencies['II_Inventory_Item']['related_to_readonly_dep'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('related_to_c'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'related_to_c',
                'value'  => 'greaterThan(strlen($id), 0)',
            ),
        ),
    ),
);
