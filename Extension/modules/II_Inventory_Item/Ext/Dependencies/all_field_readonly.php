<?php 
$dependencies['II_Inventory_Item']['ii_inventory_item_allFieldReadonly_depsss'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('status'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'actual_discard_date',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'allowed_storage_conditions',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'allowed_storage_medium',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ), 
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'analyze_by_date_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'category',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'collection_date',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'collection_date_time_type',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'collection_date_time',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'concentration_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'concentration_unit_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'storage_condition',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ), 
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'storage_medium',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ), 
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'days_to_expiration',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ), 
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'expiration_test_article',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'expiration_date',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'expiration_date_time',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'external_barcode',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'inactive_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),    
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ic_inventory_collection_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        /*array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'invi_invoice_item_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),*/
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_bin_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_building',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_rack_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_room',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_cubby_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_equi_serial_no_hide_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_type',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_values_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'manufacturer_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'description',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'number_of_aliquots_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ori_order_request_item_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'other_test_type_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'other_type_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'owner',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'pk_samples_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'planned_discard_date',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'processed_per_protocol_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'processing_method',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'product_id_2_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'product_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'poi_purchase_order_item_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'quantity_2_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'quantity_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'received_date',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ri_received_items_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'related_to_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'reminder_mail_sent_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'retention_interval_days',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'retention_start_date',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'm01_sales_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ship_to_address',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ship_to_company',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ship_to_contact',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'specimen_type_1_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'specimen_type_2_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'specimen_type_3_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'specimen_type_4_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'specimen_type_5_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'stability_considerations_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'sterilization',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'sterilization_exp_date_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'subtype',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'anml_animals_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'test_type_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'test_type_1_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'test_type_2_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'test_type_3_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'test_type_4_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'test_type_5_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'timepoint_end_integer_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'timepoint_name_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'timepoint_integer',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'timepoint_type',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'timepoint_unit',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ts_hidden_string_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'type_2',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'lot_number_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'm03_work_product_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        
     ),
);