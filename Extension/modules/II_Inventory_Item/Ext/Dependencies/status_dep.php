<?php

$dependencies['II_Inventory_Item']['status_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('status'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value'  => 'true',
            ),
        ),
    ),
);
