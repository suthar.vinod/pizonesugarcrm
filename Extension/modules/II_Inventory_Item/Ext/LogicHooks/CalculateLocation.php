<?php

$hook_array['after_relationship_add'][] = array(
    //Processing index. For sorting the array.
    201,

    //Label. A string value to identify the hook.
    'After relating a Inventory Management record resave the bean so the status field is calculated',

    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateLocation.php',

    //The class the method is in.
    'CalculateLocation',

    //The method to call.
    'calculate',
);

$hook_array['after_relationship_delete'][] = array(
    //Processing index. For sorting the array.
    202,

    //Label. A string value to identify the hook.
    'After relating a Inventory Management record resave the bean so the status field is calculated',

    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateLocation.php',

    //The class the method is in.
    'CalculateLocation',

    //The method to call.
    'calculate',
);
