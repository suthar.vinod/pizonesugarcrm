<?php

$hook_array['before_save'][] = Array(
    //Processing index. For sorting the array.
    2,
    //Label. A string value to identify the hook.
    'append a sequence of numbers at the name of the Item.',
    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/SetNameSequence.php',
    //The class the method is in.
    'SetNameSequence',
    //The method to call.
    'setName'
);

$hook_array['before_save'][] = array(
    //Processing index. For sorting the array.
    10,
    //Label. A string value to identify the hook.
    'Before Save Change Status',
    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateStatus.php',
    //The class the method is in.
    'CalculateStatus',
    //The method to call.
    'calculateBeforeSave',
);

$hook_array['before_save'][] = array(
    107,
    'Internal Barcode Creation',
    'custom/modules/II_Inventory_Item/customLogicInternalBarcodeHook.php',
    'customLogicInternalBarcodeHook',
    'generateInternalBarcodeId',
);

$hook_array['before_save'][] = array(
    //Processing index. For sorting the array.
    20,
    //Label. A string value to identify the hook.
    'Before Save Change Status',
    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateLocation.php',
    //The class the method is in.
    'CalculateLocation',
    //The method to call.
    'calculateBeforeSave',
);

$hook_array['before_save'][] = array(
    //Processing index. For sorting the array.
    21,
    //Label. A string value to identify the hook.
    'Before Save Change Status',
    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateShipping.php',
    //The class the method is in.
    'CalculateShipping',
    //The method to call.
    'calculateBeforeSave',
);

$hook_array['after_save'][] = array(
    //Processing index. For sorting the array.
    22,
    //Label. A string value to identify the hook.
    'After Save Change Location',
    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateLocation.php',
    //The class the method is in.
    'CalculateLocation',
    //The method to call.
    'calculateAfterSave',
);

 $hook_array['after_save'][] = Array(
    51,
    'After Save Change RI Filter',
    'custom/modules/RI_Received_Items/ReceivedItemFilterInII.php',
    'ReceivedItemFilterInIIHook',
    'ReceivedItemFilterInIIMethodIIAfterSave',
 );

?>