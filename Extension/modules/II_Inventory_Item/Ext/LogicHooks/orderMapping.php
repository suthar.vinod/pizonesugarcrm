<?php
// created: 2021-11-11 00:27:22
$extensionOrderMap = array (
  'custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/SetORIStatus.php' => 
  array (
    'md5' => '80f0d81a0f582a379d66aee0736e6d74',
    'mtime' => 1636498140,
    'is_override' => false,
  ),
  'custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/CalculateReceivedDate.php' => 
  array (
    'md5' => 'd3f51c4a74deed146394a5e3868b299c',
    'mtime' => 1636498140,
    'is_override' => false,
  ),
  'custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/CalculateShipping.php' => 
  array (
    'md5' => '1e15e3d7e1f274ddc6d1316a579ddcf2',
    'mtime' => 1636498140,
    'is_override' => false,
  ),
  'custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/PopulateInventorySubpanelsFromWorkProduct.php' => 
  array (
    'md5' => 'dfe71df1f62cf862551f7958d0f758d2',
    'mtime' => 1636498140,
    'is_override' => false,
  ),
  'custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/CalculateLocation.php' => 
  array (
    'md5' => '5011bd7675d26b0d9ab26f7187afee49',
    'mtime' => 1636551722,
    'is_override' => false,
  ),
  'custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/CalculateStatus.php' => 
  array (
    'md5' => '29ff6573abf8a0b7c3cd5b8bc29e798b',
    'mtime' => 1636551722,
    'is_override' => false,
  ),
  'custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/II_emailReminder_Hook.php' => 
  array (
    'md5' => '825f9210622891065fcc91fc653aa2d5',
    'mtime' => 1636551722,
    'is_override' => false,
  ),
  'custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/SetName.php' => 
  array (
    'md5' => 'fac75c372c36150e03c7d8f399ed8219',
    'mtime' => 1636551722,
    'is_override' => false,
  ),
  'custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/IIFilterInactiveItemsHook.php' => 
  array (
    'md5' => '41d4aca8bdee46a9cf972c7133aad77d',
    'mtime' => 1636590440,
    'is_override' => false,
  ),
);