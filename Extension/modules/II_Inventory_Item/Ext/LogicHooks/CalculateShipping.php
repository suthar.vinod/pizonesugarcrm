<?php

$hook_array['after_relationship_add'][] = array(
    //Processing index. For sorting the array.
    25,

    //Label. A string value to identify the hook.
    'After relating a Inventory Management record calculate shipping data',

    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateShipping.php',

    //The class the method is in.
    'CalculateShipping',

    //The method to call.
    'calculate',
);

$hook_array['after_relationship_delete'][] = array(
    //Processing index. For sorting the array.
    26,

    //Label. A string value to identify the hook.
    'After relating a Inventory Management record calculate shipping data',

    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateShipping.php',

    //The class the method is in.
    'CalculateShipping',

    //The method to call.
    'calculate',
);
