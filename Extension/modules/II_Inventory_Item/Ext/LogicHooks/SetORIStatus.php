<?php

$hook_array['after_relationship_delete'][] = Array(
    
    5,
    
    'Update the ORI Status after II delete.',
    
    'custom/src/wsystems/ORI/LogicHooks/SetORIStatus.php',
    
    'SetStatusForORI',
    
    'setStatusMethod'
);

$hook_array['after_relationship_add'][] = Array(
    
    5,
    
    'aUpdate the ORI Status after II add.',
    
    'custom/src/wsystems/ORI/LogicHooks/SetORIStatus.php',
    
    'SetStatusForORI',
    
    'setStatusMethod'
);