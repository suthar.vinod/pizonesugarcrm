<?php

$hook_array['after_relationship_delete'][] = array(
    1,
    'Delete record on unlink action from m03_work_product_ii_inventory_item_2 relationship',
    null,
    'Sugarcrm\\Sugarcrm\\custom\\wsystems\\PopulateInventorySubpanelsFromWorkProduct\\LogicHooks\\IIDeleteRelationshipHook',
    'deleteIISubpanelRelationship',
);
