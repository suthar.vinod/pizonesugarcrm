<?php
 // created: 2016-01-25 23:00:46
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M01_SALES_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'm01_sales_tasks_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
  ),
);
