<?php
 // created: 2021-09-23 08:38:16
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_m01_sales_1'] = array (
  'order' => 100,
  'module' => 'M01_Sales',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M01_SALES_M01_SALES_1_FROM_M01_SALES_R_TITLE',
  'get_subpanel_data' => 'm01_sales_m01_sales_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
