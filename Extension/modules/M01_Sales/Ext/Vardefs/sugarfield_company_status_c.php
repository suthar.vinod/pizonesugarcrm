<?php
 // created: 2019-04-02 11:57:02
$dictionary['M01_Sales']['fields']['company_status_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Sales']['fields']['company_status_c']['labelValue']='Company Status';
$dictionary['M01_Sales']['fields']['company_status_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['company_status_c']['calculated']='1';
$dictionary['M01_Sales']['fields']['company_status_c']['formula']='related($accounts_m01_sales_1,"company_status_c")';
$dictionary['M01_Sales']['fields']['company_status_c']['enforced']='1';
$dictionary['M01_Sales']['fields']['company_status_c']['dependency']='';

 ?>