<?php
 // created: 2021-06-17 08:44:04
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['labelValue']='SPA Review to SPA Finalization';
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['calculated']='true';
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['formula']='ifElse(or(equal($spa_date_c,""),equal($bc_study_article_received_c,"")),"",subtract(daysUntil($spa_date_c),daysUntil($bc_study_article_received_c)))';
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['enforced']='true';
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['dependency']='';
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['readonly_formula']='';

 ?>