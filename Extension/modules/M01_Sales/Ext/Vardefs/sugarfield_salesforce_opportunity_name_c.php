<?php
 // created: 2021-06-03 10:51:32
$dictionary['M01_Sales']['fields']['salesforce_opportunity_name_c']['labelValue']='Salesforce Opportunity Name';
$dictionary['M01_Sales']['fields']['salesforce_opportunity_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['salesforce_opportunity_name_c']['dependency']='';
$dictionary['M01_Sales']['fields']['salesforce_opportunity_name_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['salesforce_opportunity_name_c']['readonly_formula']='';

 ?>