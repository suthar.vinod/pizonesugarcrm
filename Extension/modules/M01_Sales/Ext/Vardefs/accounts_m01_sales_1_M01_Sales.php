<?php
// created: 2016-02-15 18:01:52
$dictionary["M01_Sales"]["fields"]["accounts_m01_sales_1"] = array (
  'name' => 'accounts_m01_sales_1',
  'type' => 'link',
  'relationship' => 'accounts_m01_sales_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE',
  'id_name' => 'accounts_m01_sales_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["M01_Sales"]["fields"]["accounts_m01_sales_1_name"] = array (
  'name' => 'accounts_m01_sales_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_m01_sales_1accounts_ida',
  'link' => 'accounts_m01_sales_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["M01_Sales"]["fields"]["accounts_m01_sales_1accounts_ida"] = array (
  'name' => 'accounts_m01_sales_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE_ID',
  'id_name' => 'accounts_m01_sales_1accounts_ida',
  'link' => 'accounts_m01_sales_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
