<?php
 // created: 2021-01-04 09:39:11
$dictionary['M01_Sales']['fields']['thirty_day_followup_notes_c']['labelValue']='Second Follow-Up Notes';
$dictionary['M01_Sales']['fields']['thirty_day_followup_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['thirty_day_followup_notes_c']['enforced']='';
$dictionary['M01_Sales']['fields']['thirty_day_followup_notes_c']['dependency']='';
$dictionary['M01_Sales']['fields']['thirty_day_followup_notes_c']['required_formula']='';

 ?>