<?php
 // created: 2021-04-29 08:46:32
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['labelValue']='WPC SC01 check';
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['calculated']='true';
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['formula']='countConditional($m01_sales_m03_work_product_1,"wpc_sc01_c",true)';
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['enforced']='true';
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['dependency']='';
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['readonly_formula']='';

 ?>