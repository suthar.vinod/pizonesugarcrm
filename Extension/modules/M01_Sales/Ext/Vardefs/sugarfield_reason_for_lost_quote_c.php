<?php
 // created: 2020-05-21 17:55:43
$dictionary['M01_Sales']['fields']['reason_for_lost_quote_c']['labelValue']='Reason for Lost Quote';
$dictionary['M01_Sales']['fields']['reason_for_lost_quote_c']['dependency']='';
$dictionary['M01_Sales']['fields']['reason_for_lost_quote_c']['visibility_grid']=array (
  'trigger' => 'sales_activity_quote_req_c',
  'values' => 
  array (
    'Lost' => 
    array (
      0 => '',
      1 => 'no response from client',
      2 => 'Chose another lab',
      3 => 'lab proximity to company',
      4 => 'lower quote from competitive lab',
      5 => 'scheduling issue',
      6 => 'experience',
      7 => 'lack of required equipment',
      8 => 'lack of timely response',
      9 => 'report timelines',
      10 => 'capacity',
      11 => 'WL',
    ),
    'Choose SA Status' => 
    array (
    ),
    'Lead' => 
    array (
    ),
    'Lead_Long Term Follow Up' => 
    array (
    ),
    'Study Plan Development' => 
    array (
    ),
    'Required' => 
    array (
    ),
    'Quote_Required_Scheduled' => 
    array (
    ),
    'draft for review' => 
    array (
    ),
    'Send to Sponsor' => 
    array (
    ),
    'Out for Sponsor Review' => 
    array (
    ),
    'Revision Required' => 
    array (
    ),
    'Won_waiting' => 
    array (
    ),
    'Won' => 
    array (
    ),
    'Won Quote Revision Required' => 
    array (
    ),
    'Won Quote Revision for Review' => 
    array (
    ),
    'Won Quote Revision Out for Review' => 
    array (
    ),
    'Invoicing_Initiated' => 
    array (
    ),
    'Invoicing_Complete' => 
    array (
    ),
    'Invoice_Additional_Testing_Performed' => 
    array (
    ),
    'Quote Expired Long Term FollowUp' => 
    array (
    ),
    'Not Performed' => 
    array (
    ),
    'Unknown' => 
    array (
    ),
    'Duplicate' => 
    array (
    ),
    '' => 
    array (
    ),
    'Internal APS Project' => 
    array (
    ),
    'MSA for Review' => 
    array (
    ),
    'NDA for Review' => 
    array (
    ),
    'Pricelist Submitted' => 
    array (
    ),
    'Send MSA to Sponsor' => 
    array (
    ),
    'Send NDA to Sponsor' => 
    array (
    ),
    'Send SOW to Sponsor' => 
    array (
    ),
    'SOW for Review' => 
    array (
    ),
  ),
);

 ?>