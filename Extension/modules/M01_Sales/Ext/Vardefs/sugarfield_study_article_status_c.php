<?php
 // created: 2021-12-07 12:28:20
$dictionary['M01_Sales']['fields']['study_article_status_c']['labelValue']='Study Article Status';
$dictionary['M01_Sales']['fields']['study_article_status_c']['dependency']='equal($namsa_bde_referred_c,"No")';
$dictionary['M01_Sales']['fields']['study_article_status_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['study_article_status_c']['readonly_formula']='';
$dictionary['M01_Sales']['fields']['study_article_status_c']['visibility_grid']='';

 ?>