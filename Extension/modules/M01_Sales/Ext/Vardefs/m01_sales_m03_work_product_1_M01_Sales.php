<?php
// created: 2016-01-29 21:10:09
$dictionary["M01_Sales"]["fields"]["m01_sales_m03_work_product_1"] = array (
  'name' => 'm01_sales_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'm01_sales_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_m03_work_product_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);
