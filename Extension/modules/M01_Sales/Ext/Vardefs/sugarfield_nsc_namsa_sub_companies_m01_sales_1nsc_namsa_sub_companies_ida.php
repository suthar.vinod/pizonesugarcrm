<?php
 // created: 2021-12-07 14:43:28
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['name']='nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['type']='id';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['source']='non-db';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['vname']='LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_M01_SALES_TITLE_ID';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['id_name']='nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['link']='nsc_namsa_sub_companies_m01_sales_1';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['table']='nsc_namsa_sub_companies';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['module']='NSC_NAMSA_Sub_Companies';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['rname']='id';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['reportable']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['side']='right';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['massupdate']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['duplicate_merge']='disabled';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['hideacl']=true;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['audited']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['importable']='true';

 ?>