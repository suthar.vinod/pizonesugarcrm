<?php
 // created: 2022-02-22 07:19:46
$dictionary['M01_Sales']['fields']['aps_analytical_deliverables_c']['labelValue']='Analytical Deliverables';
$dictionary['M01_Sales']['fields']['aps_analytical_deliverables_c']['dependency']='isInList($functional_area_c,createList("ISR","Pharmacology","Toxicology"))';
$dictionary['M01_Sales']['fields']['aps_analytical_deliverables_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['aps_analytical_deliverables_c']['readonly_formula']='';
$dictionary['M01_Sales']['fields']['aps_analytical_deliverables_c']['visibility_grid']='';

 ?>