<?php
 // created: 2021-02-25 05:59:28
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['labelValue']='SPA Reconciliation Duration';
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['calculated']='true';
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['formula']='ifElse(or(equal($spa_date_c,""),equal($bc_study_article_received_c,"")),"",subtract(daysUntil($spa_date_c),daysUntil($bc_study_article_received_c)))';
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['enforced']='true';
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['dependency']='';
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['required_formula']='';

 ?>