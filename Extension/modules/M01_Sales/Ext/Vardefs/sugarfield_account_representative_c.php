<?php
 // created: 2019-04-02 11:55:35
$dictionary['M01_Sales']['fields']['account_representative_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Sales']['fields']['account_representative_c']['labelValue']='Account Representative';
$dictionary['M01_Sales']['fields']['account_representative_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['account_representative_c']['calculated']='1';
$dictionary['M01_Sales']['fields']['account_representative_c']['formula']='related($accounts_m01_sales_1,"account_representative_c")';
$dictionary['M01_Sales']['fields']['account_representative_c']['enforced']='1';
$dictionary['M01_Sales']['fields']['account_representative_c']['dependency']='';

 ?>