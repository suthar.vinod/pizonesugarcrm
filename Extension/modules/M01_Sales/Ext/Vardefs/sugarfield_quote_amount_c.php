<?php
 // created: 2021-01-26 06:41:27
$dictionary['M01_Sales']['fields']['quote_amount_c']['labelValue']='Quote Amount';
$dictionary['M01_Sales']['fields']['quote_amount_c']['enforced']='false';
$dictionary['M01_Sales']['fields']['quote_amount_c']['dependency']='isBefore($date_entered,date("1/1/2021"))';
$dictionary['M01_Sales']['fields']['quote_amount_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['M01_Sales']['fields']['quote_amount_c']['required_formula']='';

 ?>