<?php
 // created: 2021-12-07 14:43:28
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['required']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['audited']=true;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['massupdate']=true;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['hidemassupdate']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['duplicate_merge']='enabled';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['duplicate_merge_dom_value']='1';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['merge_filter']='disabled';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['reportable']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['unified_search']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['calculated']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['dependency']='equal($namsa_bde_referred_c,"No")';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['vname']='LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_NAME_FIELD_TITLE';

 ?>