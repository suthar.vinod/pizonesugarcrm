<?php
 // created: 2016-10-25 03:27:01
$dictionary['M01_Sales']['fields']['description']['required'] = true;
$dictionary['M01_Sales']['fields']['description']['audited'] = true;
$dictionary['M01_Sales']['fields']['description']['massupdate'] = false;
$dictionary['M01_Sales']['fields']['description']['comments'] = 'Full text of the note';
$dictionary['M01_Sales']['fields']['description']['duplicate_merge'] = 'enabled';
$dictionary['M01_Sales']['fields']['description']['duplicate_merge_dom_value'] = '1';
$dictionary['M01_Sales']['fields']['description']['merge_filter'] = 'disabled';
$dictionary['M01_Sales']['fields']['description']['unified_search'] = false;
$dictionary['M01_Sales']['fields']['description']['full_text_search']['enabled'] = true;
$dictionary['M01_Sales']['fields']['description']['full_text_search']['searchable'] = false;
$dictionary['M01_Sales']['fields']['description']['full_text_search']['boost'] = 0.5;
$dictionary['M01_Sales']['fields']['description']['calculated'] = false;
$dictionary['M01_Sales']['fields']['description']['rows'] = '6';
$dictionary['M01_Sales']['fields']['description']['cols'] = '80';

