<?php
// created: 2018-12-13 16:29:00
$dictionary["M01_Sales"]["fields"]["contacts_m01_sales_1"] = array (
  'name' => 'contacts_m01_sales_1',
  'type' => 'link',
  'relationship' => 'contacts_m01_sales_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_M01_SALES_1_FROM_M01_SALES_TITLE',
  'id_name' => 'contacts_m01_sales_1contacts_ida',
  'link-type' => 'one',
);
$dictionary["M01_Sales"]["fields"]["contacts_m01_sales_1_name"] = array (
  'name' => 'contacts_m01_sales_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_M01_SALES_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_m01_sales_1contacts_ida',
  'link' => 'contacts_m01_sales_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["M01_Sales"]["fields"]["contacts_m01_sales_1contacts_ida"] = array (
  'name' => 'contacts_m01_sales_1contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_M01_SALES_1_FROM_M01_SALES_TITLE_ID',
  'id_name' => 'contacts_m01_sales_1contacts_ida',
  'link' => 'contacts_m01_sales_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
