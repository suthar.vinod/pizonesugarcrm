<?php
// created: 2021-12-07 12:33:38
$dictionary["M01_Sales"]["fields"]["nsc_namsa_sub_companies_m01_sales_1"] = array (
  'name' => 'nsc_namsa_sub_companies_m01_sales_1',
  'type' => 'link',
  'relationship' => 'nsc_namsa_sub_companies_m01_sales_1',
  'source' => 'non-db',
  'module' => 'NSC_NAMSA_Sub_Companies',
  'bean_name' => 'NSC_NAMSA_Sub_Companies',
  'side' => 'right',
  'vname' => 'LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_M01_SALES_TITLE',
  'id_name' => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
  'link-type' => 'one',
);
$dictionary["M01_Sales"]["fields"]["nsc_namsa_sub_companies_m01_sales_1_name"] = array (
  'name' => 'nsc_namsa_sub_companies_m01_sales_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE',
  'save' => true,
  'id_name' => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
  'link' => 'nsc_namsa_sub_companies_m01_sales_1',
  'table' => 'nsc_namsa_sub_companies',
  'module' => 'NSC_NAMSA_Sub_Companies',
  'rname' => 'name',
);
$dictionary["M01_Sales"]["fields"]["nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida"] = array (
  'name' => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_M01_SALES_TITLE_ID',
  'id_name' => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
  'link' => 'nsc_namsa_sub_companies_m01_sales_1',
  'table' => 'nsc_namsa_sub_companies',
  'module' => 'NSC_NAMSA_Sub_Companies',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
