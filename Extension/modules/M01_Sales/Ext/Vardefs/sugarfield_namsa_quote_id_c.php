<?php
 // created: 2021-12-07 10:54:35
$dictionary['M01_Sales']['fields']['namsa_quote_id_c']['labelValue']='NAMSA Quote ID (and SSF)';
$dictionary['M01_Sales']['fields']['namsa_quote_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['namsa_quote_id_c']['enforced']='';
$dictionary['M01_Sales']['fields']['namsa_quote_id_c']['dependency']='equal($namsa_bde_referred_c,"No")';
$dictionary['M01_Sales']['fields']['namsa_quote_id_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['namsa_quote_id_c']['readonly_formula']='';

 ?>