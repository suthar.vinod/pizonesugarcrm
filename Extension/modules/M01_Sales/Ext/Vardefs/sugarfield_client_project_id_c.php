<?php
 // created: 2019-04-02 12:03:43
$dictionary['M01_Sales']['fields']['client_project_id_c']['labelValue']='Sponsor Study ID';
$dictionary['M01_Sales']['fields']['client_project_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['client_project_id_c']['enforced']='';
$dictionary['M01_Sales']['fields']['client_project_id_c']['dependency']='';

 ?>