<?php
 // created: 2022-02-22 07:20:49
$dictionary['M01_Sales']['fields']['name']['readonly']=false;
$dictionary['M01_Sales']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['M01_Sales']['fields']['name']['len']='255';
$dictionary['M01_Sales']['fields']['name']['audited']=true;
$dictionary['M01_Sales']['fields']['name']['massupdate']=false;
$dictionary['M01_Sales']['fields']['name']['unified_search']=false;
$dictionary['M01_Sales']['fields']['name']['calculated']=false;
$dictionary['M01_Sales']['fields']['name']['hidemassupdate']=false;

 ?>