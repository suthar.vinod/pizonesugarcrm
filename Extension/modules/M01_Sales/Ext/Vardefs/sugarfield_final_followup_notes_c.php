<?php
 // created: 2016-10-25 03:27:01
$dictionary['M01_Sales']['fields']['final_followup_notes_c']['labelValue'] = 'Final Follow-Up Notes';
$dictionary['M01_Sales']['fields']['final_followup_notes_c']['full_text_search']['enabled'] = true;
$dictionary['M01_Sales']['fields']['final_followup_notes_c']['full_text_search']['searchable'] = false;
$dictionary['M01_Sales']['fields']['final_followup_notes_c']['full_text_search']['boost'] = 1;
$dictionary['M01_Sales']['fields']['final_followup_notes_c']['enforced'] = '';
$dictionary['M01_Sales']['fields']['final_followup_notes_c']['dependency'] = '';

