<?php
 // created: 2019-08-28 11:51:23
$dictionary['M01_Sales']['fields']['anticipated_study_start_time_c']['labelValue']='Anticipated Study Start Timeline';
$dictionary['M01_Sales']['fields']['anticipated_study_start_time_c']['dependency']='isInList($sales_activity_quote_req_c,createList("draft for review","Out for Sponsor Review"))';
$dictionary['M01_Sales']['fields']['anticipated_study_start_time_c']['visibility_grid']='';

 ?>