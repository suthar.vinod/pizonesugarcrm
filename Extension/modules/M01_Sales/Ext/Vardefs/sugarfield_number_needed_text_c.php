<?php
 // created: 2021-12-07 12:31:02
$dictionary['M01_Sales']['fields']['number_needed_text_c']['labelValue']='Number of Study Article Needed';
$dictionary['M01_Sales']['fields']['number_needed_text_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['number_needed_text_c']['enforced']='';
$dictionary['M01_Sales']['fields']['number_needed_text_c']['dependency']='equal($namsa_bde_referred_c,"No")';
$dictionary['M01_Sales']['fields']['number_needed_text_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['number_needed_text_c']['readonly_formula']='';

 ?>