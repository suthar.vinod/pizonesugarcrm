<?php
 // created: 2022-03-01 07:45:39
$dictionary['M01_Sales']['fields']['aps_histopathology_deliv_c']['labelValue']='Histopathology Deliverables';
$dictionary['M01_Sales']['fields']['aps_histopathology_deliv_c']['dependency']='isInList($functional_area_c,createList("ISR","Bioskills","Biocompatibility","Pharmacology","Toxicology"))';
$dictionary['M01_Sales']['fields']['aps_histopathology_deliv_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['aps_histopathology_deliv_c']['readonly_formula']='';
$dictionary['M01_Sales']['fields']['aps_histopathology_deliv_c']['visibility_grid']='';

 ?>