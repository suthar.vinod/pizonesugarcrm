<?php
 // created: 2022-01-20 08:06:11
$dictionary['M01_Sales']['fields']['study_article_storage_locati_c']['labelValue']='Study Article Storage Location';
$dictionary['M01_Sales']['fields']['study_article_storage_locati_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['study_article_storage_locati_c']['enforced']='';
$dictionary['M01_Sales']['fields']['study_article_storage_locati_c']['dependency']='';
$dictionary['M01_Sales']['fields']['study_article_storage_locati_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['study_article_storage_locati_c']['readonly_formula']='';

 ?>