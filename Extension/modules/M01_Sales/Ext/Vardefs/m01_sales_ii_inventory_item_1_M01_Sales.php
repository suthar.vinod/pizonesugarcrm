<?php
// created: 2021-11-09 09:56:04
$dictionary["M01_Sales"]["fields"]["m01_sales_ii_inventory_item_1"] = array (
  'name' => 'm01_sales_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'm01_sales_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_ii_inventory_item_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);
