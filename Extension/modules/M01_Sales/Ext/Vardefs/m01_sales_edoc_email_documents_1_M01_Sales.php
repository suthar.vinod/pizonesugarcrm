<?php
// created: 2019-08-27 11:35:41
$dictionary["M01_Sales"]["fields"]["m01_sales_edoc_email_documents_1"] = array (
  'name' => 'm01_sales_edoc_email_documents_1',
  'type' => 'link',
  'relationship' => 'm01_sales_edoc_email_documents_1',
  'source' => 'non-db',
  'module' => 'EDoc_Email_Documents',
  'bean_name' => 'EDoc_Email_Documents',
  'vname' => 'LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);
