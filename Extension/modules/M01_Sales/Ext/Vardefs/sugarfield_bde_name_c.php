<?php
 // created: 2022-01-18 06:48:07
$dictionary['M01_Sales']['fields']['bde_name_c']['labelValue']='BDE Name';
$dictionary['M01_Sales']['fields']['bde_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['bde_name_c']['enforced']='';
$dictionary['M01_Sales']['fields']['bde_name_c']['dependency']='equal($namsa_bde_referred_c,"Yes")';
$dictionary['M01_Sales']['fields']['bde_name_c']['required_formula']='equal($namsa_bde_referred_c,"Yes")';
$dictionary['M01_Sales']['fields']['bde_name_c']['readonly_formula']='';

 ?>