<?php
 // created: 2020-01-13 15:22:42
$dictionary['M01_Sales']['fields']['payment_terms_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Sales']['fields']['payment_terms_c']['labelValue']='Payment Terms';
$dictionary['M01_Sales']['fields']['payment_terms_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['payment_terms_c']['calculated']='1';
$dictionary['M01_Sales']['fields']['payment_terms_c']['formula']='related($accounts_m01_sales_1,"payment_terms_2_c")';
$dictionary['M01_Sales']['fields']['payment_terms_c']['enforced']='1';
$dictionary['M01_Sales']['fields']['payment_terms_c']['dependency']='';

 ?>