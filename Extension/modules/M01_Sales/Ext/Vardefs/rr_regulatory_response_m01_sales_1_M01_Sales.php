<?php
// created: 2019-07-09 11:54:56
$dictionary["M01_Sales"]["fields"]["rr_regulatory_response_m01_sales_1"] = array (
  'name' => 'rr_regulatory_response_m01_sales_1',
  'type' => 'link',
  'relationship' => 'rr_regulatory_response_m01_sales_1',
  'source' => 'non-db',
  'module' => 'RR_Regulatory_Response',
  'bean_name' => 'RR_Regulatory_Response',
  'vname' => 'LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'id_name' => 'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida',
);
