<?php
 // created: 2021-10-19 10:58:38
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_poi_purchase_order_item_1'] = array (
  'order' => 100,
  'module' => 'POI_Purchase_Order_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'get_subpanel_data' => 'm01_sales_poi_purchase_order_item_1',
);
