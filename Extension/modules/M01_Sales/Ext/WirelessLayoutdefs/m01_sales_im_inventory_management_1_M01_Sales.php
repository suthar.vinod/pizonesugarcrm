<?php
 // created: 2021-11-09 10:22:13
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_im_inventory_management_1'] = array (
  'order' => 100,
  'module' => 'IM_Inventory_Management',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'get_subpanel_data' => 'm01_sales_im_inventory_management_1',
);
