<?php
 // created: 2021-10-19 10:26:08
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_ori_order_request_item_1'] = array (
  'order' => 100,
  'module' => 'ORI_Order_Request_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'get_subpanel_data' => 'm01_sales_ori_order_request_item_1',
);
