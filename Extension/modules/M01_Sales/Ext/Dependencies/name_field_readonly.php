<?php
    $dependencies['M01_Sales']['readonlynamefield'] = array(
        'hooks' => array("all") ,
        'trigger' => 'true',
        'triggerFields' => array('name') ,
        'onload' => true,
        'actions' => array(
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'name',
                    'value' => 'true',
                ) ,
            ) ,
        ) ,
    );

