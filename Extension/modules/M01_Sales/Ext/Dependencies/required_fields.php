<?php
/*12 Feb 2021 : #892 */
$dependencies['M01_Sales']['required_aps_analytical_deliverables_c'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('functional_area_c', 'date_entered', 'id', 'aps_analytical_deliverables_c', 'aps_histopathology_deliv_c', 'initial_followup_notes_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'aps_analytical_deliverables_c',
                //'value' => 'or(equal($id,""),isAfter($date_entered,date("2021-02-16")))',
                'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-02-16"))),equal($functional_area_c,"ISR"))'
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'aps_histopathology_deliv_c',
                'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-02-16"))),equal($functional_area_c,"ISR"))',
            ),
        ),      
             
    ),
);

$dependencies['M01_Sales']['required_1'] = array(
    'hooks' => array("view", "edit"),
    'trigger' => 'true',
    'triggerFields' => array('namsa_bde_referred_c', 'namsa_quote2_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'namsa_bde_referred_c',
                'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),equal($namsa_quote2_c,"Yes"))',
            ),
        ),
    ),
);

/* $dependencies['M01_Sales']['required_2'] = array(
    'hooks' => array("view", "edit"),
    'trigger' => 'true',
    'triggerFields' => array('action_needed_c','nsc_namsa_sub_companies_m01_sales_1_name','id'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'action_needed_c',
                //'value' => 'true',
                'value' => 'or(not(greaterThan(strlen(toString($date_entered)),0)),isAfter($date_entered,"2021-12-07"))',
            ),
        ),        
    ),
); */
$dependencies['M01_Sales']['required_3'] = array(
    'hooks' => array("view", "edit"),
    'trigger' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),equal($namsa_bde_referred_c,"No"))',
    'triggerFields' => array('namsa_bde_referred_c','namsa_quote_id_c','spa_needed_c','study_article_sent_to_aps_c','number_needed_c','number_needed_text_c','nsc_namsa_sub_companies_m01_sales_1_name','action_needed_c'),
    'onload' => true,
    'actions' => array(
        /* array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'namsa_quote_id_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'spa_needed_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'number_needed_text_c',
                'value' => 'true',
            ),
        ), */
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'study_article_sent_to_aps_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'number_needed_c',
                'value' => 'true',
            ),
        ),
        /* array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'nsc_namsa_sub_companies_m01_sales_1_name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'action_needed_c',
                'value' => 'true',
            ),
        ), */
    ),
);
/* $dependencies['M01_Sales']['required_4'] = array(
    'hooks' => array("view", "edit"),
    'trigger' => 'true',
    'triggerFields' => array('date_entered','study_article_status_c','study_article_to_mpls_date_c'),
    'onload' => true,
    'actions' => array(       
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'study_article_to_mpls_date_c',
                'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-11-14"))),equal($study_article_status_c,"Shipped to MPLS"))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'study_article_to_mpls_date_c',
                'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-11-14"))),equal($study_article_status_c,"Shipped to MPLS"))',
            ),
        ),
    ),
);  
*/
$dependencies['M01_Sales']['required_4'] = array(
    'hooks' => array("view", "edit"),
    'trigger' => 'true',
    'triggerFields' => array('date_entered','study_article_status_c','study_article_to_mpls_date_c'),
    'onload' => true,
    'actions' => array(       
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'study_article_to_mpls_date_c',
                //'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-11-14"))),isInList($study_article_status_c, createList("Shipped to MPLS")))',
                'value' => 'ifElse(
                    equal($study_article_status_c,"Shipped to MPLS"),
                    true,
                    ifElse(
                        and(equal($study_article_status_c,"To Ship From NW,Shipped to MPLS")),
                        true,
                        ifElse(
                            and(equal($study_article_status_c,"Shipped to MPLS,Sponsor to Ship Directly to MPLS")),
                            true,
                            ifElse(
                                and(equal($study_article_status_c,"To Ship From NW,Shipped to MPLS,Sponsor to Ship Directly to MPLS")),
                                true,
                            )
                        )
                    )
                )',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'study_article_to_mpls_date_c',
                // 'value' => 'isInList($study_article_status_c,createList("Shipped to MPLS"))',
                'value' => 'ifElse(
                    and(equal($study_article_status_c,"Shipped to MPLS"),or(not(greaterThan(strlen(toString($date_entered)),0)),isAfter($date_entered,"2021-12-07"))),
                    true,
                    ifElse(
                        and(equal($study_article_status_c,"To Ship From NW,Shipped to MPLS"),or(not(greaterThan(strlen(toString($date_entered)),0)),isAfter($date_entered,"2021-11-24"))),
                        true,
                        ifElse(
                            and(equal($study_article_status_c,"Shipped to MPLS,Sponsor to Ship Directly to MPLS"),or(not(greaterThan(strlen(toString($date_entered)),0)),isAfter($date_entered,"2021-11-24"))),
                            true,
                            ifElse(
                                and(equal($study_article_status_c,"To Ship From NW,Shipped to MPLS,Sponsor to Ship Directly to MPLS"),or(not(greaterThan(strlen(toString($date_entered)),0)),isAfter($date_entered,"2021-11-24"))),
                                true,
                            )
                        )
                    )
                )',
            ),
        ),
    ),
);   

$dependencies['M01_Sales']['required_5'] = array(
    'hooks' => array("view", "edit"),
    'trigger' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),not(equal($anticipated_receipt_date_c,"")))',
    'triggerFields' => array('mpls_approved_transfer_date_c','anticipated_receipt_date_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'mpls_approved_transfer_date_c',
                'value' => 'true',
            ),
        ),
    ),
);
