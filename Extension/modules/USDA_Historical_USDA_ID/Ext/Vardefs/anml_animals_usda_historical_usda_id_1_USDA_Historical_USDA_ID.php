<?php
// created: 2021-02-23 10:42:04
$dictionary["USDA_Historical_USDA_ID"]["fields"]["anml_animals_usda_historical_usda_id_1"] = array (
  'name' => 'anml_animals_usda_historical_usda_id_1',
  'type' => 'link',
  'relationship' => 'anml_animals_usda_historical_usda_id_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_USDA_HISTORICAL_USDA_ID_TITLE',
  'id_name' => 'anml_animals_usda_historical_usda_id_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["USDA_Historical_USDA_ID"]["fields"]["anml_animals_usda_historical_usda_id_1_name"] = array (
  'name' => 'anml_animals_usda_historical_usda_id_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_usda_historical_usda_id_1anml_animals_ida',
  'link' => 'anml_animals_usda_historical_usda_id_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["USDA_Historical_USDA_ID"]["fields"]["anml_animals_usda_historical_usda_id_1anml_animals_ida"] = array (
  'name' => 'anml_animals_usda_historical_usda_id_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_USDA_HISTORICAL_USDA_ID_TITLE_ID',
  'id_name' => 'anml_animals_usda_historical_usda_id_1anml_animals_ida',
  'link' => 'anml_animals_usda_historical_usda_id_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
