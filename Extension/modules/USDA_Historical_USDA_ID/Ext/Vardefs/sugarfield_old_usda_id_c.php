<?php
 // created: 2021-03-04 09:12:19
$dictionary['USDA_Historical_USDA_ID']['fields']['old_usda_id_c']['labelValue']='Old USDA ID';
$dictionary['USDA_Historical_USDA_ID']['fields']['old_usda_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['USDA_Historical_USDA_ID']['fields']['old_usda_id_c']['enforced']='';
$dictionary['USDA_Historical_USDA_ID']['fields']['old_usda_id_c']['dependency']='';
$dictionary['USDA_Historical_USDA_ID']['fields']['old_usda_id_c']['required_formula']='';
$dictionary['USDA_Historical_USDA_ID']['fields']['old_usda_id_c']['readonly_formula']='';

 ?>