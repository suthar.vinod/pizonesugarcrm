<?php
 // created: 2020-09-03 07:37:55
$dictionary['GE_GDP_Examples']['fields']['description']['audited']=true;
$dictionary['GE_GDP_Examples']['fields']['description']['massupdate']=false;
$dictionary['GE_GDP_Examples']['fields']['description']['hidemassupdate']=false;
$dictionary['GE_GDP_Examples']['fields']['description']['comments']='Full text of the note';
$dictionary['GE_GDP_Examples']['fields']['description']['duplicate_merge']='enabled';
$dictionary['GE_GDP_Examples']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['GE_GDP_Examples']['fields']['description']['merge_filter']='disabled';
$dictionary['GE_GDP_Examples']['fields']['description']['unified_search']=false;
$dictionary['GE_GDP_Examples']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['GE_GDP_Examples']['fields']['description']['calculated']=false;
$dictionary['GE_GDP_Examples']['fields']['description']['rows']='6';
$dictionary['GE_GDP_Examples']['fields']['description']['cols']='80';

 ?>