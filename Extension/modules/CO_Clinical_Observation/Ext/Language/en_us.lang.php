<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE'] = 'Initial Clinical Issue';
$mod_strings['LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_2_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE'] = 'Active Clinical Issue';
$mod_strings['LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_ANML_ANIMALS_TITLE'] = 'Test System';
$mod_strings['LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Communication';
$mod_strings['LBL_TEST_SYSTEM_NAME'] = 'Test System Name';
$mod_strings['LBL_CO_CLINICAL_OBSERVATION_FOCUS_DRAWER_DASHBOARD'] = 'Clinical Observations Focus Drawer';
$mod_strings['LBL_CO_CLINICAL_OBSERVATION_RECORD_DASHBOARD'] = 'Clinical Observations Record Dashboard';
