<?php

$dependencies['CO_Clinical_Observation']['appetite_dep'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2,createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc","Morbidity and Mortality")),equal($observation,"See Observation Text"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'appetite',
                'keys' => 'getDropdownKeySet("appetite_dd_list")',
                'labels' => 'getDropdownValueSet("appetite_dd_list")'
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['appetite_dep4'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($type_2,createList("Post Operative Day 1 AM"))',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'appetite',
                'keys' => 'getDropdownKeySet("appetite_dd_list")',
                'labels' => 'getDropdownValueSet("appetite_dd_list")'
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['appetite_dep3'] = array(
    'hooks' => array("edit", "save"),
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'appetite',
                'value' => 'not(equal($type_2,"Pain Assessment"))',
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['appetite_dep2'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2,createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc","Morbidity and Mortality")),equal($observation,"Normal"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'appetite',
                'keys' => 'getDropdownKeySet("app_feces_normal_list")',
                'labels' => 'getDropdownValueSet("app_feces_normal_list")'
            ),
        ),
    ),
);

