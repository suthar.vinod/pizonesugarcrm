<?php
$dependencies['CO_Clinical_Observation']['CO_observation_readonly_field'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'true',
    //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array('active_clinical_issue','observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                
                'target' => 'cie_clinical_issue_exam_co_clinical_observation_2_name',
                'value' => 'equal($active_clinical_issue,"Yes")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                
                'target' => 'cie_clinical_issue_exam_co_clinical_observation_2_name',
                'value' => 'equal($active_clinical_issue,"Yes")',
            ),
        ),
    ),
);
