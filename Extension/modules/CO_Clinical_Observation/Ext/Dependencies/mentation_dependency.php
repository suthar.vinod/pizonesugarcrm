<?php

$dependencies['CO_Clinical_Observation']['mentation_dep'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2,createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc","Morbidity and Mortality")),equal($observation,"See Observation Text"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'mentation',
                'keys' => 'getDropdownKeySet("mentation_dd_list")',
                'labels' => 'getDropdownValueSet("mentation_dd_list")'
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['mentation_dep4'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($type_2,createList("Post Operative Day 1 AM"))',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'mentation',
                'keys' => 'getDropdownKeySet("mentation_dd_list")',
                'labels' => 'getDropdownValueSet("mentation_dd_list")'
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['mentation_dep3'] = array(
    'hooks' => array("edit", "save"),
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'mentation',
                'value' => 'not(equal($type_2,"Pain Assessment"))',
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['mentation_dep2'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2,createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc","Morbidity and Mortality")),equal($observation,"Normal"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'mentation',
                'keys' => 'getDropdownKeySet("mentation_bar_list")',
                'labels' => 'getDropdownValueSet("mentation_bar_list")'
            ),
        ),
    ),
);

