<?php

$dependencies['CO_Clinical_Observation']['setoptions_type_observation1'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2, createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc")),equal($observation,"See Observation Text"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => 'true',
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_taken_list")',
                'labels' => 'getDropdownValueSet("rt_taken_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
    ),
);

$dependencies['CO_Clinical_Observation']['setoptions_morbidity1'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'equal($type_2,"Morbidity and Mortality")',
    'triggerFields' => array('type_2','observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
    ),


);

$dependencies['CO_Clinical_Observation']['setoptions_studio_type1'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'and(isInList($type_2,createList("Ad Hoc","Post Operative AM","Post Operative PM","Standard")),equal($observation,""))',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_taken_list")',
                'labels' => 'getDropdownValueSet("rt_taken_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
    ),
);

$dependencies['CO_Clinical_Observation']['setoptions_pain_assessment1'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'equal($type_2,"Pain Assessment")',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_not_taken_list")',
                'labels' => 'getDropdownValueSet("rt_not_taken_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_not_taken_list")',
                'labels' => 'getDropdownValueSet("rt_not_taken_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_not_taken_list")',
                'labels' => 'getDropdownValueSet("rt_not_taken_list")'
            ),
        ),
    ),
);

$dependencies['CO_Clinical_Observation']['setoptions_post_day_11'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'equal($type_2,"Post Operative Day 1 AM")',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_taken_list")',
                'labels' => 'getDropdownValueSet("rt_taken_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_taken_list")',
                'labels' => 'getDropdownValueSet("rt_taken_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_taken_list")',
                'labels' => 'getDropdownValueSet("rt_taken_list")'
            ),
        ),
    ),
);


