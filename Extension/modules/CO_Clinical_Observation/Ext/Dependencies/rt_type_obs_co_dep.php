<?php

$dependencies['CO_Clinical_Observation']['setoptions_type_obs_update1'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2, createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc")),equal($observation,"Normal"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => 'true',
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
    ),
);

$dependencies['CO_Clinical_Observation']['setoptions_type_obs_update_null1'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'equal($type_2,"")',
    'triggerFields' => array('type_2'),
    'onload' => 'true',
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
    ),
);

