<?php
// created: 2020-01-07 20:01:08
$dictionary["CO_Clinical_Observation"]["fields"]["co_clinical_observation_m06_error_1"] = array (
  'name' => 'co_clinical_observation_m06_error_1',
  'type' => 'link',
  'relationship' => 'co_clinical_observation_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'co_clinical_observation_m06_error_1m06_error_idb',
);
$dictionary["CO_Clinical_Observation"]["fields"]["co_clinical_observation_m06_error_1_name"] = array (
  'name' => 'co_clinical_observation_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'save' => true,
  'id_name' => 'co_clinical_observation_m06_error_1m06_error_idb',
  'link' => 'co_clinical_observation_m06_error_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'name',
);
$dictionary["CO_Clinical_Observation"]["fields"]["co_clinical_observation_m06_error_1m06_error_idb"] = array (
  'name' => 'co_clinical_observation_m06_error_1m06_error_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_M06_ERROR_TITLE_ID',
  'id_name' => 'co_clinical_observation_m06_error_1m06_error_idb',
  'link' => 'co_clinical_observation_m06_error_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
