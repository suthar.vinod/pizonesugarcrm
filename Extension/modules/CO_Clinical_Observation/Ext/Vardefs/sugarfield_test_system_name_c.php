<?php
 // created: 2020-01-07 20:06:12
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['duplicate_merge_dom_value']=0;
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['labelValue']='Test System Name';
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['calculated']='true';
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['formula']='related($anml_animals_co_clinical_observation_1,"name")';
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['enforced']='true';
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['dependency']='';

 ?>