<?php
 // created: 2020-03-17 11:33:49
$dictionary['CO_Clinical_Observation']['fields']['name']['required']=false;
$dictionary['CO_Clinical_Observation']['fields']['name']['unified_search']=false;
$dictionary['CO_Clinical_Observation']['fields']['name']['importable']='false';
$dictionary['CO_Clinical_Observation']['fields']['name']['duplicate_merge']='disabled';
$dictionary['CO_Clinical_Observation']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['CO_Clinical_Observation']['fields']['name']['merge_filter']='disabled';
$dictionary['CO_Clinical_Observation']['fields']['name']['calculated']='true';
$dictionary['CO_Clinical_Observation']['fields']['name']['formula']='concat($test_system_name_c," ",toString($datetime)," Clinical Observation")';
$dictionary['CO_Clinical_Observation']['fields']['name']['enforced']=true;

 ?>