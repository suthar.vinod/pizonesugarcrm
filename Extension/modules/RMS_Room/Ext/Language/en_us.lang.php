<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_BUILDING'] = 'Building';
$mod_strings['LBL_CAPACITY'] = 'Capacity';
$mod_strings['LBL_ROOM_NUMBER'] = 'Room #';
$mod_strings['LBL_ROOM_TYPE'] = 'Type';
$mod_strings['LBL_RECORD_BODY'] = 'Rooms';
$mod_strings['LBL_M06_ERROR_RMS_ROOM_1_FROM_M06_ERROR_TITLE'] = 'Communications';
$mod_strings['LBL_RMS_ROOM_FOCUS_DRAWER_DASHBOARD'] = 'Rooms Focus Drawer';
$mod_strings['LBL_RMS_ROOM_RECORD_DASHBOARD'] = 'Rooms Record Dashboard';
