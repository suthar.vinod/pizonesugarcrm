<?php
 // created: 2019-08-09 11:42:47
$dictionary['RMS_Room']['fields']['room_number_c']['labelValue']='Room #';
$dictionary['RMS_Room']['fields']['room_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RMS_Room']['fields']['room_number_c']['enforced']='';
$dictionary['RMS_Room']['fields']['room_number_c']['dependency']='';

 ?>