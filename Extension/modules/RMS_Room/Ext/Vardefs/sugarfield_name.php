<?php
 // created: 2019-06-19 15:18:22
$dictionary['RMS_Room']['fields']['name']['len']='255';
$dictionary['RMS_Room']['fields']['name']['audited']=true;
$dictionary['RMS_Room']['fields']['name']['massupdate']=false;
$dictionary['RMS_Room']['fields']['name']['unified_search']=false;
$dictionary['RMS_Room']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['RMS_Room']['fields']['name']['calculated']=false;

 ?>