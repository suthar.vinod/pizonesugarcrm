<?php
 // created: 2016-10-25 03:26:58
$dictionary['Document']['fields']['description']['audited'] = false;
$dictionary['Document']['fields']['description']['massupdate'] = false;
$dictionary['Document']['fields']['description']['comments'] = 'Full text of the note';
$dictionary['Document']['fields']['description']['duplicate_merge'] = 'enabled';
$dictionary['Document']['fields']['description']['duplicate_merge_dom_value'] = '1';
$dictionary['Document']['fields']['description']['merge_filter'] = 'disabled';
$dictionary['Document']['fields']['description']['full_text_search']['enabled'] = true;
$dictionary['Document']['fields']['description']['full_text_search']['searchable'] = false;
$dictionary['Document']['fields']['description']['full_text_search']['boost'] = 0.60999999999999998667732370449812151491641998291015625;
$dictionary['Document']['fields']['description']['calculated'] = false;
$dictionary['Document']['fields']['description']['rows'] = '6';
$dictionary['Document']['fields']['description']['cols'] = '120';

