<?php
 // created: 2018-01-31 16:48:51
$dictionary['Document']['fields']['controlled_document_number_c']['labelValue']='Controlled Document Number';
$dictionary['Document']['fields']['controlled_document_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Document']['fields']['controlled_document_number_c']['enforced']='';
$dictionary['Document']['fields']['controlled_document_number_c']['dependency']='';

 ?>