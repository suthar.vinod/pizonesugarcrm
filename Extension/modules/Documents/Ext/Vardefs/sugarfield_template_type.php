<?php
 // created: 2016-10-25 03:26:58
$dictionary['Document']['fields']['template_type']['audited'] = false;
$dictionary['Document']['fields']['template_type']['massupdate'] = true;
$dictionary['Document']['fields']['template_type']['duplicate_merge'] = 'enabled';
$dictionary['Document']['fields']['template_type']['duplicate_merge_dom_value'] = '1';
$dictionary['Document']['fields']['template_type']['merge_filter'] = 'disabled';
$dictionary['Document']['fields']['template_type']['calculated'] = false;
$dictionary['Document']['fields']['template_type']['dependency'] = false;
$dictionary['Document']['fields']['template_type']['full_text_search']['boost'] = 1;

