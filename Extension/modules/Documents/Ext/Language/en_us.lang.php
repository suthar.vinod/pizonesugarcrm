<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Companies';
$mod_strings['LBL_OPPORTUNITIES_SUBPANEL_TITLE'] = 'Opportunities';
$mod_strings['LBL_M01_SALES_DOCUMENTS_1_FROM_M01_SALES_TITLE'] = 'Sales Activity';
$mod_strings['LBL_M01_SALES_DOCUMENTS_1_FROM_DOCUMENTS_TITLE_ID'] = 'Sales ID';
$mod_strings['LBL_M01_SALES_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Sales';
$mod_strings['LBL_TEMPLATE_TYPE'] = 'Document Type';
$mod_strings['LBL_DESCRIPTION'] = 'Notes';
$mod_strings['LBL_TEST_ACCOUNT_ID'] = 'test (related Company ID)';
$mod_strings['LBL_TEST'] = 'test';
$mod_strings['LBL_CONTROLLED_DOCUMENT_NUMBER'] = 'Controlled Document Number';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE'] = 'Tradeshow_Management';
