<?php
// created: 2018-04-23 17:10:34
$dictionary["TA_Tradeshow_Activities"]["fields"]["tm_tradeshow_management_ta_tradeshow_activities_1"] = array (
  'name' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'source' => 'non-db',
  'module' => 'TM_Tradeshow_Management',
  'bean_name' => 'TM_Tradeshow_Management',
  'side' => 'right',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'id_name' => 'tm_tradesh0f8eagement_ida',
  'link-type' => 'one',
);
$dictionary["TA_Tradeshow_Activities"]["fields"]["tm_tradeshow_management_ta_tradeshow_activities_1_name"] = array (
  'name' => 'tm_tradeshow_management_ta_tradeshow_activities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'save' => true,
  'id_name' => 'tm_tradesh0f8eagement_ida',
  'link' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'name',
);
$dictionary["TA_Tradeshow_Activities"]["fields"]["tm_tradesh0f8eagement_ida"] = array (
  'name' => 'tm_tradesh0f8eagement_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE_ID',
  'id_name' => 'tm_tradesh0f8eagement_ida',
  'link' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
