<?php
// created: 2018-04-23 17:07:52
$dictionary["TA_Tradeshow_Activities"]["fields"]["contacts_ta_tradeshow_activities_1"] = array (
  'name' => 'contacts_ta_tradeshow_activities_1',
  'type' => 'link',
  'relationship' => 'contacts_ta_tradeshow_activities_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'id_name' => 'contacts_ta_tradeshow_activities_1contacts_ida',
  'link-type' => 'one',
);
$dictionary["TA_Tradeshow_Activities"]["fields"]["contacts_ta_tradeshow_activities_1_name"] = array (
  'name' => 'contacts_ta_tradeshow_activities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_TA_TRADESHOW_ACTIVITIES_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_ta_tradeshow_activities_1contacts_ida',
  'link' => 'contacts_ta_tradeshow_activities_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["TA_Tradeshow_Activities"]["fields"]["contacts_ta_tradeshow_activities_1contacts_ida"] = array (
  'name' => 'contacts_ta_tradeshow_activities_1contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE_ID',
  'id_name' => 'contacts_ta_tradeshow_activities_1contacts_ida',
  'link' => 'contacts_ta_tradeshow_activities_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
