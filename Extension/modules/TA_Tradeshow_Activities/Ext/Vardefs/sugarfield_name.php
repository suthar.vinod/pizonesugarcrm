<?php
 // created: 2019-04-23 12:14:38
$dictionary['TA_Tradeshow_Activities']['fields']['name']['len']='255';
$dictionary['TA_Tradeshow_Activities']['fields']['name']['audited']=true;
$dictionary['TA_Tradeshow_Activities']['fields']['name']['massupdate']=false;
$dictionary['TA_Tradeshow_Activities']['fields']['name']['unified_search']=false;
$dictionary['TA_Tradeshow_Activities']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['TA_Tradeshow_Activities']['fields']['name']['calculated']=false;

 ?>