<?php
 // created: 2021-01-14 09:51:40
$dictionary['TA_Tradeshow_Activities']['fields']['description']['audited']=true;
$dictionary['TA_Tradeshow_Activities']['fields']['description']['massupdate']=false;
$dictionary['TA_Tradeshow_Activities']['fields']['description']['hidemassupdate']=false;
$dictionary['TA_Tradeshow_Activities']['fields']['description']['comments']='Full text of the note';
$dictionary['TA_Tradeshow_Activities']['fields']['description']['duplicate_merge']='enabled';
$dictionary['TA_Tradeshow_Activities']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['TA_Tradeshow_Activities']['fields']['description']['merge_filter']='disabled';
$dictionary['TA_Tradeshow_Activities']['fields']['description']['unified_search']=false;
$dictionary['TA_Tradeshow_Activities']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['TA_Tradeshow_Activities']['fields']['description']['calculated']=false;
$dictionary['TA_Tradeshow_Activities']['fields']['description']['rows']='6';
$dictionary['TA_Tradeshow_Activities']['fields']['description']['cols']='80';

 ?>