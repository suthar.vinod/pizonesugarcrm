<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_LEAD_QUALITY'] = 'Lead Quality';
$mod_strings['LBL_CONTACT_NAME'] = 'Contact Name';
$mod_strings['LBL_COMPANY_NAME'] = 'Company Name';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'New Panel 1';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'New Panel 2';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE'] = 'Tradeshow Name';
$mod_strings['LBL_FOLLOW_UP_COMPLETED'] = 'Follow-Up Completed';
$mod_strings['LBL_TA_TRADESHOW_ACTIVITIES_FOCUS_DRAWER_DASHBOARD'] = 'Tradeshow Activities Focus Drawer';
$mod_strings['LBL_TA_TRADESHOW_ACTIVITIES_RECORD_DASHBOARD'] = 'Tradeshow Activities Record Dashboard';
