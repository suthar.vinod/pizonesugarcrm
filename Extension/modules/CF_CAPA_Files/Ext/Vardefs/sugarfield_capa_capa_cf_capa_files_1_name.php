<?php
 // created: 2022-02-03 07:41:25
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['required']=true;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['audited']=true;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['massupdate']=true;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['hidemassupdate']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['duplicate_merge']='enabled';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['duplicate_merge_dom_value']='1';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['merge_filter']='disabled';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['reportable']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['unified_search']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['calculated']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['vname']='LBL_CAPA_CAPA_CF_CAPA_FILES_1_NAME_FIELD_TITLE';

 ?>