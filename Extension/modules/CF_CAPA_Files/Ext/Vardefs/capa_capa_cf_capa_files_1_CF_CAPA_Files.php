<?php
// created: 2022-02-03 07:26:23
$dictionary["CF_CAPA_Files"]["fields"]["capa_capa_cf_capa_files_1"] = array (
  'name' => 'capa_capa_cf_capa_files_1',
  'type' => 'link',
  'relationship' => 'capa_capa_cf_capa_files_1',
  'source' => 'non-db',
  'module' => 'CAPA_CAPA',
  'bean_name' => 'CAPA_CAPA',
  'side' => 'right',
  'vname' => 'LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CF_CAPA_FILES_TITLE',
  'id_name' => 'capa_capa_cf_capa_files_1capa_capa_ida',
  'link-type' => 'one',
);
$dictionary["CF_CAPA_Files"]["fields"]["capa_capa_cf_capa_files_1_name"] = array (
  'name' => 'capa_capa_cf_capa_files_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CAPA_CAPA_TITLE',
  'save' => true,
  'id_name' => 'capa_capa_cf_capa_files_1capa_capa_ida',
  'link' => 'capa_capa_cf_capa_files_1',
  'table' => 'capa_capa',
  'module' => 'CAPA_CAPA',
  'rname' => 'name',
);
$dictionary["CF_CAPA_Files"]["fields"]["capa_capa_cf_capa_files_1capa_capa_ida"] = array (
  'name' => 'capa_capa_cf_capa_files_1capa_capa_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CF_CAPA_FILES_TITLE_ID',
  'id_name' => 'capa_capa_cf_capa_files_1capa_capa_ida',
  'link' => 'capa_capa_cf_capa_files_1',
  'table' => 'capa_capa',
  'module' => 'CAPA_CAPA',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
