<?php
 // created: 2022-02-03 07:41:23
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['name']='capa_capa_cf_capa_files_1capa_capa_ida';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['type']='id';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['source']='non-db';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['vname']='LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CF_CAPA_FILES_TITLE_ID';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['id_name']='capa_capa_cf_capa_files_1capa_capa_ida';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['link']='capa_capa_cf_capa_files_1';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['table']='capa_capa';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['module']='CAPA_CAPA';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['rname']='id';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['reportable']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['side']='right';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['massupdate']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['duplicate_merge']='disabled';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['hideacl']=true;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['audited']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['importable']='true';

 ?>