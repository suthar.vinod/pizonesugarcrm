<?php
// created: 2022-02-01 04:24:04
$dictionary["EDoc_Email_Documents"]["fields"]["edoc_email_documents_a1a_critical_phase_inspectio_1"] = array (
  'name' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'type' => 'link',
  'relationship' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'vname' => 'LBL_EDOC_EMAIL_DOCUMENTS_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'id_name' => 'edoc_emailbf38cuments_ida',
  'link-type' => 'many',
  'side' => 'left',
);
