<?php
// created: 2019-08-27 11:35:41
$dictionary["EDoc_Email_Documents"]["fields"]["m01_sales_edoc_email_documents_1"] = array (
  'name' => 'm01_sales_edoc_email_documents_1',
  'type' => 'link',
  'relationship' => 'm01_sales_edoc_email_documents_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'id_name' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["EDoc_Email_Documents"]["fields"]["m01_sales_edoc_email_documents_1_name"] = array (
  'name' => 'm01_sales_edoc_email_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
  'link' => 'm01_sales_edoc_email_documents_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["EDoc_Email_Documents"]["fields"]["m01_sales_edoc_email_documents_1m01_sales_ida"] = array (
  'name' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE_ID',
  'id_name' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
  'link' => 'm01_sales_edoc_email_documents_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
