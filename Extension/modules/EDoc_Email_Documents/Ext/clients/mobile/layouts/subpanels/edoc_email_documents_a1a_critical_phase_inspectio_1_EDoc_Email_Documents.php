<?php
// created: 2022-02-01 04:24:04
$viewdefs['EDoc_Email_Documents']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_EDOC_EMAIL_DOCUMENTS_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'context' => 
  array (
    'link' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  ),
);