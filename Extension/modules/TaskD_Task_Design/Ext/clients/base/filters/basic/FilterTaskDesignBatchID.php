<?php
// created: 2020-08-10 
$viewdefs['TaskD_Task_Design']['base']['filter']['basic']['filters'][] = array(
  'id' => 'FilterTaskDesignBatchID',
  'name' => 'Batch ID',
  'filter_definition' => array(
    array(
      'bid_batch_id_taskd_task_design_1_name'  => array(
        '$in' => '',
      ),
      'relative'  => array(
        '$in' => '',
      ),
    ),
  ),
  'editable' => true,
  'is_template' => true,
);
