<?php
 // created: 2022-03-22 07:16:46
$dictionary['TaskD_Task_Design']['fields']['initial_upon_completion_c']['labelValue']='Initial Upon Completion';
$dictionary['TaskD_Task_Design']['fields']['initial_upon_completion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['initial_upon_completion_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['initial_upon_completion_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['initial_upon_completion_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['initial_upon_completion_c']['readonly_formula']='';

 ?>