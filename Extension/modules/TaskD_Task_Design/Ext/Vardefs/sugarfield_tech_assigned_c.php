<?php
 // created: 2022-03-22 07:15:15
$dictionary['TaskD_Task_Design']['fields']['tech_assigned_c']['labelValue']='Tech Assigned';
$dictionary['TaskD_Task_Design']['fields']['tech_assigned_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['tech_assigned_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['tech_assigned_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['tech_assigned_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['tech_assigned_c']['readonly_formula']='';

 ?>