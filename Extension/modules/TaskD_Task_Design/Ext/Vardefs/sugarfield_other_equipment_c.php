<?php
 // created: 2022-01-25 08:26:06
$dictionary['TaskD_Task_Design']['fields']['other_equipment_c']['labelValue']='Other Equipment';
$dictionary['TaskD_Task_Design']['fields']['other_equipment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['other_equipment_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['other_equipment_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['other_equipment_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['other_equipment_c']['readonly_formula']='';

 ?>