<?php
 // created: 2021-12-03 13:11:36
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['audited']=true;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['massupdate']=true;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['hidemassupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['duplicate_merge']='enabled';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['duplicate_merge_dom_value']='1';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['vname']='LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE';

 ?>