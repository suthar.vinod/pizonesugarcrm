<?php
 // created: 2021-12-02 09:45:03
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['importable']='false';
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['duplicate_merge_dom_value']='0';
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['readonly']=true;
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['dependency']='and(isInList($type_2,createList("Actual","Actual SP")),equal($relative,"1st Tier"))';

 ?>