<?php
 // created: 2021-12-03 13:12:23
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['name']='anml_animals_taskd_task_design_1anml_animals_ida';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['type']='id';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['source']='non-db';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['vname']='LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['id_name']='anml_animals_taskd_task_design_1anml_animals_ida';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['link']='anml_animals_taskd_task_design_1';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['table']='anml_animals';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['module']='ANML_Animals';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['rname']='id';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['side']='right';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['hideacl']=true;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['audited']=false;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['importable']='true';

 ?>