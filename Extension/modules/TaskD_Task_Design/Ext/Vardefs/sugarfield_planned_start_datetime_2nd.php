<?php
 // created: 2021-12-02 09:45:31
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['importable']='false';
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['duplicate_merge_dom_value']='0';
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['readonly']=true;
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['dependency']='and(isInList($type_2,createList("Actual","Actual SP")),equal($relative,"2nd Tier"))';

 ?>