<?php
 // created: 2022-06-14 06:15:48
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_days_c']['labelValue']='Recurrence Pattern - Recur Every X Days';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_days_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_days_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_days_c']['dependency']='equal($recurring_td_c,"Yes")';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_days_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_days_c']['readonly_formula']='';

 ?>