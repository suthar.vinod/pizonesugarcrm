<?php
// created: 2021-01-19 13:36:34
$dictionary["TaskD_Task_Design"]["fields"]["gd_group_design_taskd_task_design_1"] = array (
  'name' => 'gd_group_design_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'gd_group_design_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'GD_Group_Design',
  'bean_name' => 'GD_Group_Design',
  'side' => 'right',
  'vname' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'id_name' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
  'link-type' => 'one',
);
$dictionary["TaskD_Task_Design"]["fields"]["gd_group_design_taskd_task_design_1_name"] = array (
  'name' => 'gd_group_design_taskd_task_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'save' => true,
  'id_name' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
  'link' => 'gd_group_design_taskd_task_design_1',
  'table' => 'gd_group_design',
  'module' => 'GD_Group_Design',
  'rname' => 'name',
);
$dictionary["TaskD_Task_Design"]["fields"]["gd_group_design_taskd_task_design_1gd_group_design_ida"] = array (
  'name' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID',
  'id_name' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
  'link' => 'gd_group_design_taskd_task_design_1',
  'table' => 'gd_group_design',
  'module' => 'GD_Group_Design',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
