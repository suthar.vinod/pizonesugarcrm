<?php
// created: 2021-01-19 13:39:59
$dictionary["TaskD_Task_Design"]["fields"]["anml_animals_taskd_task_design_1"] = array (
  'name' => 'anml_animals_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'anml_animals_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'id_name' => 'anml_animals_taskd_task_design_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["TaskD_Task_Design"]["fields"]["anml_animals_taskd_task_design_1_name"] = array (
  'name' => 'anml_animals_taskd_task_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_taskd_task_design_1anml_animals_ida',
  'link' => 'anml_animals_taskd_task_design_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["TaskD_Task_Design"]["fields"]["anml_animals_taskd_task_design_1anml_animals_ida"] = array (
  'name' => 'anml_animals_taskd_task_design_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID',
  'id_name' => 'anml_animals_taskd_task_design_1anml_animals_ida',
  'link' => 'anml_animals_taskd_task_design_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
