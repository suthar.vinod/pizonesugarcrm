<?php
 // created: 2022-11-08 05:47:09
$dictionary['TaskD_Task_Design']['fields']['completion_status_c']['labelValue']='Completion Status';
$dictionary['TaskD_Task_Design']['fields']['completion_status_c']['dependency']='equal($standard_task,"Sample Preparation")';
$dictionary['TaskD_Task_Design']['fields']['completion_status_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['completion_status_c']['readonly_formula']='';
$dictionary['TaskD_Task_Design']['fields']['completion_status_c']['visibility_grid']='';

 ?>