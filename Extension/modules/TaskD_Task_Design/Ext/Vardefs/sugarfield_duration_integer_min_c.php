<?php
 // created: 2022-03-15 07:29:47
$dictionary['TaskD_Task_Design']['fields']['duration_integer_min_c']['labelValue']='Duration Integer (Min)';
$dictionary['TaskD_Task_Design']['fields']['duration_integer_min_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['duration_integer_min_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['duration_integer_min_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['duration_integer_min_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['duration_integer_min_c']['readonly_formula']='';

 ?>