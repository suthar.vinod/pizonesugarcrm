<?php
 // created: 2022-05-05 20:19:12
$dictionary['TaskD_Task_Design']['fields']['single_date_2_c']['labelValue']='Single Date';
$dictionary['TaskD_Task_Design']['fields']['single_date_2_c']['formula']='ifElse(not(equal($actual_datetime,"")),$actual_datetime,
ifElse(not(equal($planned_start_datetime_1st,"")),$planned_start_datetime_1st,
ifElse(not(equal($planned_start_datetime_2nd,"")),$planned_start_datetime_2nd,$scheduled_start_datetime)))';
$dictionary['TaskD_Task_Design']['fields']['single_date_2_c']['enforced']='false';
$dictionary['TaskD_Task_Design']['fields']['single_date_2_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['single_date_2_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['single_date_2_c']['readonly_formula']='';

 ?>