<?php
 // created: 2022-05-06 06:35:33
$dictionary['TaskD_Task_Design']['fields']['cron_updated_c']['labelValue']='cron updated';
$dictionary['TaskD_Task_Design']['fields']['cron_updated_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['cron_updated_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['cron_updated_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['cron_updated_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['cron_updated_c']['readonly_formula']='';

 ?>