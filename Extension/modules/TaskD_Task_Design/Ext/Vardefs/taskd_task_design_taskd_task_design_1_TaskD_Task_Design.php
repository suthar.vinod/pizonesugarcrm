<?php
// created: 2021-01-19 13:43:24
$dictionary["TaskD_Task_Design"]["fields"]["taskd_task_design_taskd_task_design_1"] = array (
  'name' => 'taskd_task_design_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'taskd_task_design_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'vname' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_L_TITLE',
  'id_name' => 'taskd_task_design_taskd_task_design_1taskd_task_design_idb',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["TaskD_Task_Design"]["fields"]["taskd_task_design_taskd_task_design_1_right"] = array (
  'name' => 'taskd_task_design_taskd_task_design_1_right',
  'type' => 'link',
  'relationship' => 'taskd_task_design_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'side' => 'right',
  'vname' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_R_TITLE',
  'id_name' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
  'link-type' => 'one',
);
$dictionary["TaskD_Task_Design"]["fields"]["taskd_task_design_taskd_task_design_1_name"] = array (
  'name' => 'taskd_task_design_taskd_task_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_L_TITLE',
  'save' => true,
  'id_name' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
  'link' => 'taskd_task_design_taskd_task_design_1_right',
  'table' => 'taskd_task_design',
  'module' => 'TaskD_Task_Design',
  'rname' => 'name',
);
$dictionary["TaskD_Task_Design"]["fields"]["taskd_task_design_taskd_task_design_1taskd_task_design_ida"] = array (
  'name' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_R_TITLE_ID',
  'id_name' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
  'link' => 'taskd_task_design_taskd_task_design_1_right',
  'table' => 'taskd_task_design',
  'module' => 'TaskD_Task_Design',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
