<?php
 // created: 2022-12-20 09:01:20
$dictionary['TaskD_Task_Design']['fields']['integer_units']['importable']='true';
$dictionary['TaskD_Task_Design']['fields']['integer_units']['readonly']=false;
$dictionary['TaskD_Task_Design']['fields']['integer_units']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['integer_units']['dependency']='or(and(isInList($recurring_td_c,createList("No","")),equal(related($m03_work_product_code_taskd_task_design_1,"name"),""),isInList($relative,createList("1st Tier","2nd Tier"))),and(isInList($recurring_td_c,createList("No","")),not(equal(related($m03_work_product_code_taskd_task_design_1,"name"),"")),isInList($relative,createList("1st Tier","2nd Tier")),or(isInList($standard_task,createList("Extract Out","Provide Material","Sample Preparation")),isInList($task_type,createList("Custom")))))';
$dictionary['TaskD_Task_Design']['fields']['integer_units']['required_formula']='or(and(isInList($recurring_td_c,createList("No","")),equal(related($m03_work_product_code_taskd_task_design_1,"name"),""),isInList($relative,createList("1st Tier","2nd Tier"))),and(isInList($recurring_td_c,createList("No","")),not(equal(related($m03_work_product_code_taskd_task_design_1,"name"),"")),isInList($relative,createList("1st Tier","2nd Tier")),or(isInList($standard_task,createList("Extract Out","Provide Material","Sample Preparation")),isInList($task_type,createList("Custom")))))';

 ?>