<?php
 // created: 2021-12-03 13:10:25
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['name']='gd_group_design_taskd_task_design_1gd_group_design_ida';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['type']='id';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['source']='non-db';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['vname']='LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['id_name']='gd_group_design_taskd_task_design_1gd_group_design_ida';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['link']='gd_group_design_taskd_task_design_1';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['table']='gd_group_design';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['module']='GD_Group_Design';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['rname']='id';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['side']='right';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['hideacl']=true;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['audited']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['importable']='true';

 ?>