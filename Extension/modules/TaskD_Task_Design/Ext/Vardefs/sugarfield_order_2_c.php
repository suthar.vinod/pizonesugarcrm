<?php
 // created: 2022-06-14 06:45:00
$dictionary['TaskD_Task_Design']['fields']['order_2_c']['labelValue']='Order';
$dictionary['TaskD_Task_Design']['fields']['order_2_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['order_2_c']['dependency']='not(equal($recurring_td_c,"Yes"))';
$dictionary['TaskD_Task_Design']['fields']['order_2_c']['required_formula']='not(equal($recurring_td_c,"Yes"))';
$dictionary['TaskD_Task_Design']['fields']['order_2_c']['readonly_formula']='';

 ?>