<?php
 // created: 2022-05-27 05:31:37
$dictionary['TaskD_Task_Design']['fields']['task_design_linked_id_c']['labelValue']='task design linked id c';
$dictionary['TaskD_Task_Design']['fields']['task_design_linked_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['task_design_linked_id_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['task_design_linked_id_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['task_design_linked_id_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['task_design_linked_id_c']['readonly_formula']='';

 ?>