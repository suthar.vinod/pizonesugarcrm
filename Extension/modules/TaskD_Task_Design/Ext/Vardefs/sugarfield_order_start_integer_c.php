<?php
 // created: 2022-06-14 06:37:09
$dictionary['TaskD_Task_Design']['fields']['order_start_integer_c']['labelValue']='Order Start Integer';
$dictionary['TaskD_Task_Design']['fields']['order_start_integer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['order_start_integer_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['order_start_integer_c']['dependency']='equal($recurring_td_c,"Yes")';
$dictionary['TaskD_Task_Design']['fields']['order_start_integer_c']['required_formula']='equal($recurring_td_c,"Yes")';
$dictionary['TaskD_Task_Design']['fields']['order_start_integer_c']['readonly_formula']='';

 ?>