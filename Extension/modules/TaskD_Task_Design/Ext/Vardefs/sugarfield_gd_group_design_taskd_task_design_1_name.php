<?php
 // created: 2021-12-03 13:10:27
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['audited']=true;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['massupdate']=true;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['hidemassupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['duplicate_merge']='enabled';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['duplicate_merge_dom_value']='1';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['vname']='LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE';

 ?>