<?php
 // created: 2022-06-14 06:18:11
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_duration_c']['labelValue']='Recurrence Pattern - Duration of Recurrence in Days';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_duration_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_duration_c']['dependency']='equal($recurring_td_c,"Yes")';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_duration_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_duration_c']['readonly_formula']='';

 ?>