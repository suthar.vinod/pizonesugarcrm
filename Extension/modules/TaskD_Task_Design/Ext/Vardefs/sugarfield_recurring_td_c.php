<?php
 // created: 2022-06-14 06:13:23
$dictionary['TaskD_Task_Design']['fields']['recurring_td_c']['labelValue']='Recurring TD?';
$dictionary['TaskD_Task_Design']['fields']['recurring_td_c']['dependency']='and(isInList($type_2,createList("Plan","Actual SP")),isInList($relative,createList("1st Tier","2nd Tier")))';
$dictionary['TaskD_Task_Design']['fields']['recurring_td_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['recurring_td_c']['readonly_formula']='';
$dictionary['TaskD_Task_Design']['fields']['recurring_td_c']['visibility_grid']='';

 ?>