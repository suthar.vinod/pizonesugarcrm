<?php
 // created: 2021-01-19 13:57:07
$dictionary['TaskD_Task_Design']['fields']['days_from_initial_task']['importable']='false';
$dictionary['TaskD_Task_Design']['fields']['days_from_initial_task']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['days_from_initial_task']['duplicate_merge_dom_value']=0;
$dictionary['TaskD_Task_Design']['fields']['days_from_initial_task']['calculated']='true';
$dictionary['TaskD_Task_Design']['fields']['days_from_initial_task']['formula']='ifElse(equal($actual_datetime,""),"",abs(subtract(daysUntil(related($taskd_task_design_taskd_task_design_1_right,"actual_datetime")),daysUntil($actual_datetime))))';
$dictionary['TaskD_Task_Design']['fields']['days_from_initial_task']['enforced']=true;

 ?>