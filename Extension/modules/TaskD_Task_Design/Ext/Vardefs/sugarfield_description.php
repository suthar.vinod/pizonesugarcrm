<?php
 // created: 2022-11-22 06:05:36
$dictionary['TaskD_Task_Design']['fields']['description']['audited']=true;
$dictionary['TaskD_Task_Design']['fields']['description']['massupdate']=true;
$dictionary['TaskD_Task_Design']['fields']['description']['hidemassupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['description']['comments']='Full text of the note';
$dictionary['TaskD_Task_Design']['fields']['description']['duplicate_merge']='enabled';
$dictionary['TaskD_Task_Design']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['TaskD_Task_Design']['fields']['description']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['description']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['TaskD_Task_Design']['fields']['description']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['description']['rows']='6';
$dictionary['TaskD_Task_Design']['fields']['description']['cols']='80';

 ?>