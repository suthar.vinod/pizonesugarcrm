<?php
 // created: 2022-09-22 10:42:25
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['audited']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['massupdate']=true;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['hidemassupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['duplicate_merge']='enabled';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['duplicate_merge_dom_value']='1';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['vname']='LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE';

 ?>