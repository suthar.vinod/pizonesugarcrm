<?php
 // created: 2022-09-22 10:42:25
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['name']='bid_batch_id_taskd_task_design_1bid_batch_id_ida';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['type']='id';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['source']='non-db';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['vname']='LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['id_name']='bid_batch_id_taskd_task_design_1bid_batch_id_ida';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['link']='bid_batch_id_taskd_task_design_1';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['table']='bid_batch_id';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['module']='BID_Batch_ID';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['rname']='id';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['side']='right';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['hideacl']=true;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['audited']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['importable']='true';

 ?>