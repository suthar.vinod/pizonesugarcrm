<?php
 // created: 2022-03-22 07:17:59
$dictionary['TaskD_Task_Design']['fields']['data_reviewed_by_c']['labelValue']='Data Reviewed By';
$dictionary['TaskD_Task_Design']['fields']['data_reviewed_by_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['data_reviewed_by_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['data_reviewed_by_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['data_reviewed_by_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['data_reviewed_by_c']['readonly_formula']='';

 ?>