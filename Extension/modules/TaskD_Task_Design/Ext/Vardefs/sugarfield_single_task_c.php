<?php
 // created: 2022-03-24 09:11:06
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['duplicate_merge_dom_value']=0;
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['labelValue']='Single Task';
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['calculated']='true';
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['formula']='ifElse(equal($task_type,"Custom"),$custom_task,getDropdownValue("standard_task_list",$standard_task))';
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['enforced']='true';
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['readonly_formula']='';

 ?>