<?php
 // created: 2021-06-10 07:19:07
$dictionary['TaskD_Task_Design']['fields']['name']['importable']='false';
$dictionary['TaskD_Task_Design']['fields']['name']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['TaskD_Task_Design']['fields']['name']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['name']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['name']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['name']['readonly']=true;
$dictionary['TaskD_Task_Design']['fields']['name']['required']=false;

 ?>