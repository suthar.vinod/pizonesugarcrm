<?php
// created: 2022-04-26 06:47:59
$dictionary["TaskD_Task_Design"]["fields"]["bid_batch_id_taskd_task_design_1"] = array (
  'name' => 'bid_batch_id_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'BID_Batch_ID',
  'bean_name' => 'BID_Batch_ID',
  'side' => 'right',
  'vname' => 'LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'id_name' => 'bid_batch_id_taskd_task_design_1bid_batch_id_ida',
  'link-type' => 'one',
);
$dictionary["TaskD_Task_Design"]["fields"]["bid_batch_id_taskd_task_design_1_name"] = array (
  'name' => 'bid_batch_id_taskd_task_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_BID_BATCH_ID_TITLE',
  'save' => true,
  'id_name' => 'bid_batch_id_taskd_task_design_1bid_batch_id_ida',
  'link' => 'bid_batch_id_taskd_task_design_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'name',
);
$dictionary["TaskD_Task_Design"]["fields"]["bid_batch_id_taskd_task_design_1bid_batch_id_ida"] = array (
  'name' => 'bid_batch_id_taskd_task_design_1bid_batch_id_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID',
  'id_name' => 'bid_batch_id_taskd_task_design_1bid_batch_id_ida',
  'link' => 'bid_batch_id_taskd_task_design_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
