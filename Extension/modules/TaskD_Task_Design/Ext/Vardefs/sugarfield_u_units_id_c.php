<?php
 // created: 2022-12-20 09:01:20
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['required']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['name']='u_units_id_c';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['vname']='LBL_INTEGER_UNITS_U_UNITS_ID';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['type']='id';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['hidemassupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['no_default']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['comments']='';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['help']='';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['importable']='true';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['duplicate_merge']='enabled';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['duplicate_merge_dom_value']=1;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['audited']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['pii']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['len']=36;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['size']='20';

 ?>