<?php
 // created: 2022-05-05 09:22:47
$dictionary['TaskD_Task_Design']['fields']['single_date_c']['labelValue']='(Obsolete) Single Date';
$dictionary['TaskD_Task_Design']['fields']['single_date_c']['formula']='ifElse(equal($relative,"NA"),$actual_datetime,ifElse(equal($relative,"1st Tier"),$planned_start_datetime_1st,$planned_start_datetime_2nd))';
$dictionary['TaskD_Task_Design']['fields']['single_date_c']['enforced']='false';
$dictionary['TaskD_Task_Design']['fields']['single_date_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['single_date_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['single_date_c']['readonly_formula']='';

 ?>