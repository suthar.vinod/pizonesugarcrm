<?php
 // created: 2021-12-03 13:11:34
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['name']='m03_work_product_taskd_task_design_1m03_work_product_ida';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['type']='id';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['source']='non-db';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['vname']='LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['id_name']='m03_work_product_taskd_task_design_1m03_work_product_ida';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['link']='m03_work_product_taskd_task_design_1';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['table']='m03_work_product';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['module']='M03_Work_Product';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['rname']='id';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['side']='right';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['hideacl']=true;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['audited']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['importable']='true';

 ?>