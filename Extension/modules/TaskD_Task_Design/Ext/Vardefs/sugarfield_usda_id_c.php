<?php
 // created: 2021-12-07 10:47:31
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['duplicate_merge_dom_value']=0;
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['labelValue']='USDA ID';
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['calculated']='true';
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['formula']='related($anml_animals_taskd_task_design_1,"usda_id_c")';
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['enforced']='true';
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['dependency']='equal($type_2,"Actual")';
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['readonly_formula']='';

 ?>