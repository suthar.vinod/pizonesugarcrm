<?php
 // created: 2022-06-14 06:35:07
$dictionary['TaskD_Task_Design']['fields']['start_day_of_recurrence_c']['labelValue']='Start Day of Recurrence';
$dictionary['TaskD_Task_Design']['fields']['start_day_of_recurrence_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['start_day_of_recurrence_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['start_day_of_recurrence_c']['dependency']='equal($recurring_td_c,"Yes")';
$dictionary['TaskD_Task_Design']['fields']['start_day_of_recurrence_c']['required_formula']='equal($recurring_td_c,"Yes")';
$dictionary['TaskD_Task_Design']['fields']['start_day_of_recurrence_c']['readonly_formula']='';

 ?>