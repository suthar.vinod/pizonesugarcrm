<?php

$dependencies['TaskD_Task_Design']['test_system_require_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'anml_animals_taskd_task_design_1_name',
                'value' => 'equal($type_2,"Actual")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'gd_group_design_taskd_task_design_1_name',
                'value' => 'equal($type_2,"Plan")',
            ),
        ),
    ),
);


$dependencies['TaskD_Task_Design']['test_system_visibility_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'anml_animals_taskd_task_design_1_name',
                'value' => 'equal($type_2,"Actual")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'gd_group_design_taskd_task_design_1_name',
                'value' => 'equal($type_2,"Plan")',
            ),
        ),
    ),
);


/* $dependencies['TaskD_Task_Design']['task_design_require_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('relative'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1_name',
                'value' => 'or(equal($relative,"1st Tier"),equal($relative,"2nd Tier"))',
            ),
        ),
    ),
); */


$dependencies['TaskD_Task_Design']['task_design_visibility_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('relative'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1_name',
                'value' => 'or(equal($relative,"1st Tier"),equal($relative,"2nd Tier"))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1_name',
                'value' => 'or(equal($relative,"1st Tier"),equal($relative,"2nd Tier"))',
            ),
        ),
    ),
);


$dependencies['TaskD_Task_Design']['task_design_set_val01'] = array(
    'hooks' => array("all"),
    'trigger' => 'not(or(equal($relative,"1st Tier"),equal($relative,"2nd Tier")))',
    'triggerFields' => array('relative'),
    'onload' => true,
    'actions' => array( 
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
                'value' => '',
            ),
        ),
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1_name',
                'value' => '',
            ),
        ),
    ),
);

$dependencies['TaskD_Task_Design']['work_product_change02'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_taskd_task_design_1_name'),
    'onload' => false,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
                'value' => '',
            ),
        ),
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1_name',
                'value' => '',
            ),
        ),
        
    ),
);


/*$dependencies['TaskD_Task_Design']['work_product_relationship_reuired01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_taskd_task_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => 'true',
            ),
        ),   
    ),
);*/
/*1490 : 19 Aug 2021 */

/* $dependencies['TaskD_Task_Design']['work_product_relationship_reuired01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => 'or(equal($type_2,"Plan"),equal($type_2,"Actual"),equal($type_2,"Actual SP"))',
            ),
        ),
    ),
);

$dependencies['TaskD_Task_Design']['work_product_relationship_visibility'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => 'or(equal($type_2,"Plan"),equal($type_2,"Actual"),equal($type_2,"Actual SP"))',
            ),
        ),
    ),
); */
/***/// 1490 end */

/*1491 : 19 Aug 2021 */

$dependencies['TaskD_Task_Design']['m03_work_product_code_taskd_task_design_1_name_reuired01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_code_taskd_task_design_1_name',
                'value' => 'or(equal($type_2,"Plan SP"))',
            ),
        ),
    ),
);

$dependencies['TaskD_Task_Design']['m03_work_product_code_taskd_task_design_1_name_visibility'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_code_taskd_task_design_1_name',
                'value' => 'or(equal($type_2,"Plan SP"))',
            ),
        ),
    ),
);

$dependencies['TaskD_Task_Design']['m03_work_product_code_taskd_task_design_1_name_setnull'] = array(
    'hooks' => array("all"),
    'trigger' => 'or(equal($type_2,"Plan"),equal($type_2,"Actual"),equal($type_2,"Actual SP"))',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'm03_work_product_code_taskd_task_design_1_name',
                'value' => '',
            ),
        ),
    ),
);
/**/// 1491 end */

$dependencies['TaskD_Task_Design']['work_product_relationship_setnull'] = array(
    'hooks' => array("all"),
    'trigger' => 'or(equal($type_2,"Plan SP"))',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => '',
            ),
        ),
    ),
);




/* 792: Task Design - Type field */
$dependencies['TaskD_Task_Design']['read_only_type_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('gd_group_design_taskd_task_design_1_name','anml_animals_taskd_task_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'type_2',
                'value' => 'or(greaterThan(strlen(related($anml_animals_taskd_task_design_1,"name")),0),greaterThan(strlen(related($gd_group_design_taskd_task_design_1,"name")),0))',
            ),
        ),
    ),
);

/* 1489: Task Design - Type field */
$dependencies['TaskD_Task_Design']['read_only_plan_type_dep01'] = array(
    'hooks' => array("save"),
    'trigger' => 'ifElse(not(equal(related($gd_group_design_taskd_task_design_1,"name"),"")),"true","false")',
    'triggerFields' => array('gd_group_design_taskd_task_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'type_2',
                'value' => 'true',
            ),
        ),
    ),
); 
$dependencies['TaskD_Task_Design']['read_only_plan_type_dep02'] = array(
    'hooks' => array("save"),
    'trigger' => 'ifElse(not(equal(related($anml_animals_taskd_task_design_1,"name"),"")),"true","false")',
    'triggerFields' => array('anml_animals_taskd_task_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'type_2',
                'value' => 'true',
            ),
        ),
    ),
); 
$dependencies['TaskD_Task_Design']['read_only_plan_type_dep02'] = array(
    'hooks' => array("save"),
    'trigger' => 'ifElse(not(equal(related($m03_work_product_code_taskd_task_design_1,"name"),"")),"true","false")',
    'triggerFields' => array('m03_work_product_code_taskd_task_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'type_2',
                'value' => 'true',
            ),
        ),
    ),
); 
$dependencies['TaskD_Task_Design']['gd_set_null_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => false,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => "gd_group_design_taskd_task_design_1gd_group_design_ida",
                'value' => '',
            ),
        ),
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'gd_group_design_taskd_task_design_1_name',
                'value' => '',
            ),
        ),
        
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => "anml_animals_taskd_task_design_1anml_animals_ida",
                'value' => '',
            ),
        ),
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'anml_animals_taskd_task_design_1_name',
                'value' => '',
            ),
        ),
    ),
);

$dependencies['TaskD_Task_Design']['actual_date_readonly'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2','relative'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'actual_datetime',
                'value' => 'and(equal($type_2,"Actual SP"),equal($relative,"NA"))',
            ),
        ),
    ),
);
/*2313 : 04 April 2022 */

// $dependencies['TaskD_Task_Design']['work_product_relationship_reuired01'] = array(
//     'hooks' => array("all"),
//     'trigger' => 'true',
//     'triggerFields' => array('type_2','m03_work_product_taskd_task_design_1_name','bid_batch_id_taskd_task_design_1_name'),
//     'onload' => true,
//     'actions' => array(
//         array(
//             'name' => 'SetRequired',
//             'params' => array(
//                 'target' => 'm03_work_product_taskd_task_design_1_name',
//                 'value' => 'or(or(equal($type_2,"Plan"),equal($type_2,"Actual")),and(equal($type_2,"Actual SP"),equal($bid_batch_id_taskd_task_design_1_name,"")))',
//             ),
//         ),
//     ),
// );
/*
$dependencies['TaskD_Task_Design']['m03_work_product_Visibility'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2','m03_work_product_taskd_task_design_1_name','bid_batch_id_taskd_task_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => 'or(or(equal($type_2,"Plan"),equal($type_2,"Actual")),and(equal($type_2,"Actual SP"),equal($bid_batch_id_taskd_task_design_1_name,"")))',
            ),
        ),
        
    ),
);
*/
$dependencies['TaskD_Task_Design']['WP_Visibility_dep43324'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_taskd_task_design_1_name','bid_batch_id_taskd_task_design_1_name'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(        
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => 'or(and(equal($bid_batch_id_taskd_task_design_1_name,""),not(equal($m03_work_product_taskd_task_design_1_name,""))),and(equal($bid_batch_id_taskd_task_design_1_name,""),equal($m03_work_product_taskd_task_design_1_name,"")))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'bid_batch_id_taskd_task_design_1_name',
                'value' => 'or(and(equal($m03_work_product_taskd_task_design_1_name,""),not(equal($bid_batch_id_taskd_task_design_1_name,""))),and(equal($m03_work_product_taskd_task_design_1_name,""),equal($bid_batch_id_taskd_task_design_1_name,"")))',
            ),
        ),
    ),
);
$dependencies['TaskD_Task_Design']['task_design_required_field'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_taskd_task_design_1_name','bid_batch_id_taskd_task_design_1_name','type_2'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(        
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => 'or(and(equal($bid_batch_id_taskd_task_design_1_name,""),not(equal($m03_work_product_taskd_task_design_1_name,""))),and(equal($bid_batch_id_taskd_task_design_1_name,""),equal($m03_work_product_taskd_task_design_1_name,""),not(equal($type_2,"Plan SP"))))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'bid_batch_id_taskd_task_design_1_name',
                'value' => 'or(and(equal($m03_work_product_taskd_task_design_1_name,""),not(equal($bid_batch_id_taskd_task_design_1_name,""))),and(equal($m03_work_product_taskd_task_design_1_name,""),equal($bid_batch_id_taskd_task_design_1_name,""),not(equal($type_2,"Plan SP"))))',
            ),
        ),
    ),
);
/***/// 2313 end */