<?php
 // created: 2019-09-30 12:27:41
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_tsdoc_test_system_documents_1'] = array (
  'order' => 100,
  'module' => 'TSdoc_Test_System_Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_TSDOC_TEST_SYSTEM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'anml_animals_tsdoc_test_system_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
