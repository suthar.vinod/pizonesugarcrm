<?php
 // created: 2021-02-23 10:42:04
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_usda_historical_usda_id_1'] = array (
  'order' => 100,
  'module' => 'USDA_Historical_USDA_ID',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_USDA_HISTORICAL_USDA_ID_TITLE',
  'get_subpanel_data' => 'anml_animals_usda_historical_usda_id_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
