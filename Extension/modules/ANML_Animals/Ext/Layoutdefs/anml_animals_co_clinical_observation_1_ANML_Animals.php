<?php
 // created: 2020-01-07 14:17:22
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_co_clinical_observation_1'] = array (
  'order' => 100,
  'module' => 'CO_Clinical_Observation',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'get_subpanel_data' => 'anml_animals_co_clinical_observation_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
