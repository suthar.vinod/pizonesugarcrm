<?php
 // created: 2020-01-07 13:24:35
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_w_weight_1'] = array (
  'order' => 100,
  'module' => 'W_Weight',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_W_WEIGHT_TITLE',
  'get_subpanel_data' => 'anml_animals_w_weight_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
