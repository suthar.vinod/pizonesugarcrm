<?php
$hook_version = 1;
if (!isset($hook_array)) {
	$hook_array = array();
}
if (!isset($hook_array['after_relationship_add'])) {
	$hook_array['after_relationship_add'] = array();
}
$hook_array['after_relationship_add'][] = array(
   count($hook_array['after_relationship_add']),
   'Link Room from Related Room Transfer record with latest Transfer Date',
   'custom/modules/ANML_Animals/LogicHooksImplementations/updateRoom.php',
   'updateRoomClass',
   'updateRoom'
);

$hook_version = 1;
if (!isset($hook_array)) {
	$hook_array = array();
}
if (!isset($hook_array['after_relationship_delete'])) {
	$hook_array['after_relationship_delete'] = array();
}
$hook_array['after_relationship_delete'][] = array(
   count($hook_array['after_relationship_delete']),
   'Link Room from Related Room Transfer record with latest Transfer Date',
   'custom/modules/ANML_Animals/LogicHooksImplementations/updateRoom.php',
   'updateRoomClass',
   'updateRoom'
);
?>