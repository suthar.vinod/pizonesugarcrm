<?php
$dependencies['ANML_Animals']['readonly_usda_id'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('usda_id_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'usda_id_c',
                'value' => 'ifElse(greaterThan(strlen($usda_id_hidden_c2_c),0),true,false)',
            ),
        ),
    ),
);

// $dependencies['ANML_Animals']['readonly_bodyweight'] = array(
//     'hooks'         => array("all"),
//     'triggerFields' => array('id'),
//     'onload'        => true,
//     //Actions is a list of actions to fire when the trigger is true
//     'actions' => array(
//         array(
//             'name' => 'ReadOnly',
//             'params' => array(
//                 'target' => 'bodyweight_c',
//                 'value' => 'greaterThan(strlen(related($anml_animals_w_weight_1,"name")),0)',
//                 //'value' => 'ifElse(greaterThan(strlen(related($anml_animals_w_weight_1,"name")),0),true,false)',
//             ),
//         ),
//     ),
// );
