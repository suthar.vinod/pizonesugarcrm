<?php
 // created: 2020-01-07 13:40:49
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_cie_clinical_issue_exam_1'] = array (
  'order' => 100,
  'module' => 'CIE_Clinical_Issue_Exam',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'get_subpanel_data' => 'anml_animals_cie_clinical_issue_exam_1',
);
