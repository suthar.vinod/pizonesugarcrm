<?php

$dictionary['ANML_Animals']['fields']['enrollment_status'] = array(
    'name' => "enrollment_status",
    'default' => "Choose One",
    'importable' => "true",
    'len' => 100,
    'no_default' => false,
    'options' => "enrollment_status_list",
    'reportable' => true,
    'required' => false,
    'size' => 20,
    'type' => "enum",
    'unified_search' => false,
    'vname' => "LBL_ENROLLMENT_STATUS",
    'label' => "LBL_ENROLLMENT_STATUS",
    'readonly' => true,
);

