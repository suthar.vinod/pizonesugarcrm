<?php
// created: 2021-08-14 06:05:14
$dictionary["ANML_Animals"]["fields"]["anml_animals_taskd_task_design_1"] = array (
  'name' => 'anml_animals_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'anml_animals_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'vname' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_taskd_task_design_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);
