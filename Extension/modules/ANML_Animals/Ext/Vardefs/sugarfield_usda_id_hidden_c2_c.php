<?php
 // created: 2021-05-24 09:09:22
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['duplicate_merge_dom_value']=0;
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['labelValue']='Hidden USDA ID 2';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['calculated']='true';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['formula']='related($anml_animals_usda_historical_usda_id_1,"name")';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['enforced']='true';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['readonly_formula']='';

 ?>