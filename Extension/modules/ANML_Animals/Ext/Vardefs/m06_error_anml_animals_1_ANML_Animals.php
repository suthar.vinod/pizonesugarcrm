<?php
// created: 2018-01-31 16:00:43
$dictionary["ANML_Animals"]["fields"]["m06_error_anml_animals_1"] = array (
  'name' => 'm06_error_anml_animals_1',
  'type' => 'link',
  'relationship' => 'm06_error_anml_animals_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_ANML_ANIMALS_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_anml_animals_1m06_error_ida',
);
