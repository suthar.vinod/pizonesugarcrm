<?php
 // created: 2019-04-11 11:08:52
$dictionary['ANML_Animals']['fields']['assigned_to_wp_c']['labelValue']='Assigned to WP';
$dictionary['ANML_Animals']['fields']['assigned_to_wp_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['assigned_to_wp_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['assigned_to_wp_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['assigned_to_wp_c']['readonly']=true;

 ?>