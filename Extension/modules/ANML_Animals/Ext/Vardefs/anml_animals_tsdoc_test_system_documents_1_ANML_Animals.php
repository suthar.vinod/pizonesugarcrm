<?php
// created: 2019-09-30 12:27:41
$dictionary["ANML_Animals"]["fields"]["anml_animals_tsdoc_test_system_documents_1"] = array (
  'name' => 'anml_animals_tsdoc_test_system_documents_1',
  'type' => 'link',
  'relationship' => 'anml_animals_tsdoc_test_system_documents_1',
  'source' => 'non-db',
  'module' => 'TSdoc_Test_System_Documents',
  'bean_name' => 'TSdoc_Test_System_Documents',
  'vname' => 'LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);
