<?php
 // created: 2019-09-11 13:19:29
$dictionary['ANML_Animals']['fields']['name_concat_c']['duplicate_merge_dom_value']=0;
$dictionary['ANML_Animals']['fields']['name_concat_c']['labelValue']='Name Concat';
$dictionary['ANML_Animals']['fields']['name_concat_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['name_concat_c']['calculated']='true';
$dictionary['ANML_Animals']['fields']['name_concat_c']['formula']='concat($animal_id_c," ","(",$usda_id_c,")")';
$dictionary['ANML_Animals']['fields']['name_concat_c']['enforced']='true';
$dictionary['ANML_Animals']['fields']['name_concat_c']['dependency']='';

 ?>