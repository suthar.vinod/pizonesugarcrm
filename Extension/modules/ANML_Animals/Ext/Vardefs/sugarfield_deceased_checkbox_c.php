<?php
 // created: 2021-03-09 05:02:15
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['duplicate_merge_dom_value']=0;
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['labelValue']='Deceased';
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['calculated']='true';
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['formula']='greaterThan(strlen(toString($termination_date_c)),0)';
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['enforced']='true';
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['readonly_formula']='';

 ?>