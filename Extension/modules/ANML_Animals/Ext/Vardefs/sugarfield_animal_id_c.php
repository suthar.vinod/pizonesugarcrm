<?php
 // created: 2022-08-03 09:55:47
$dictionary['ANML_Animals']['fields']['animal_id_c']['labelValue']='Test System ID';
$dictionary['ANML_Animals']['fields']['animal_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['animal_id_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['animal_id_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['animal_id_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['animal_id_c']['readonly_formula']='';

 ?>