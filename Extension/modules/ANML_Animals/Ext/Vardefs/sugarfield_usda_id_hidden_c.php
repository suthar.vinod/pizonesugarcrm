<?php
 // created: 2021-02-23 10:47:52
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c']['labelValue']='USDA ID Hidden';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c']['required_formula']='';

 ?>