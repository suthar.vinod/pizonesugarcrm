<?php
// created: 2020-01-07 13:24:35
$dictionary["ANML_Animals"]["fields"]["anml_animals_w_weight_1"] = array (
  'name' => 'anml_animals_w_weight_1',
  'type' => 'link',
  'relationship' => 'anml_animals_w_weight_1',
  'source' => 'non-db',
  'module' => 'W_Weight',
  'bean_name' => 'W_Weight',
  'vname' => 'LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_w_weight_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);
