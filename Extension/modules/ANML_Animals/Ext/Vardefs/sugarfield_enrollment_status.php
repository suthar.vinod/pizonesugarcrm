<?php
 // created: 2020-12-15 12:39:26
$dictionary['ANML_Animals']['fields']['enrollment_status']['audited']=true;
$dictionary['ANML_Animals']['fields']['enrollment_status']['massupdate']=false;
$dictionary['ANML_Animals']['fields']['enrollment_status']['duplicate_merge']='enabled';
$dictionary['ANML_Animals']['fields']['enrollment_status']['duplicate_merge_dom_value']='1';
$dictionary['ANML_Animals']['fields']['enrollment_status']['merge_filter']='disabled';
$dictionary['ANML_Animals']['fields']['enrollment_status']['calculated']=false;
$dictionary['ANML_Animals']['fields']['enrollment_status']['dependency']=false;
$dictionary['ANML_Animals']['fields']['enrollment_status']['default']='';
$dictionary['ANML_Animals']['fields']['enrollment_status']['hidemassupdate']=false;

 ?>