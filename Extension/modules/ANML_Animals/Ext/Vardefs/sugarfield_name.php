<?php
 // created: 2019-07-09 12:08:31
$dictionary['ANML_Animals']['fields']['name']['len']='255';
$dictionary['ANML_Animals']['fields']['name']['audited']=true;
$dictionary['ANML_Animals']['fields']['name']['massupdate']=false;
$dictionary['ANML_Animals']['fields']['name']['unified_search']=false;
$dictionary['ANML_Animals']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['ANML_Animals']['fields']['name']['calculated']='true';
$dictionary['ANML_Animals']['fields']['name']['importable']='false';
$dictionary['ANML_Animals']['fields']['name']['duplicate_merge']='disabled';
$dictionary['ANML_Animals']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['ANML_Animals']['fields']['name']['merge_filter']='disabled';
$dictionary['ANML_Animals']['fields']['name']['required']=true;
$dictionary['ANML_Animals']['fields']['name']['formula']='$animal_id_c';
$dictionary['ANML_Animals']['fields']['name']['enforced']=true;

 ?>