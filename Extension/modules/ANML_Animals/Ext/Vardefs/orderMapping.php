<?php
// created: 2022-08-30 09:55:42
$extensionOrderMap = array (
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/nameFieldCustomization.php' => 
  array (
    'md5' => '3b174b2d9ba35a226b3958666ec453d1',
    'mtime' => 1505227912,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_wpe_work_product_enrollment_1_ANML_Animals.php' => 
  array (
    'md5' => '79f4ab5a9185df1b864c986d9b91e99a',
    'mtime' => 1505228665,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/wpe_work_product_enrollment_anml_animals_1_ANML_Animals.php' => 
  array (
    'md5' => '6876704c64c21c95a1892969918915c2',
    'mtime' => 1505228931,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_comments_c.php' => 
  array (
    'md5' => '62757e3a9cbeace60dbae3ba669e195c',
    'mtime' => 1507297007,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/m06_error_anml_animals_1_ANML_Animals.php' => 
  array (
    'md5' => 'e411c3f6f9228bd738a93b3be62f5d21',
    'mtime' => 1517414482,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_usda_id_c.php' => 
  array (
    'md5' => '2c644ea6d3b64c1a3c5013ccd68ddc94',
    'mtime' => 1554980094,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_usda_category_c.php' => 
  array (
    'md5' => '64a3f1718a60de9ef0a1d0684899265e',
    'mtime' => 1554980278,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_first_procedure_date_c.php' => 
  array (
    'md5' => '5d6d86c99bb9e0b34aaece7446eea27f',
    'mtime' => 1554980329,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_termination_date_c.php' => 
  array (
    'md5' => '54877118c5dd948bd8a59beb3e48ad43',
    'mtime' => 1554980367,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_date_of_birth_c.php' => 
  array (
    'md5' => '0b1a236080384ae2dcd24e2cdffab457',
    'mtime' => 1554980500,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_date_received_c.php' => 
  array (
    'md5' => 'a1cbd0d25581f56a8e3b8bafee00e4ac',
    'mtime' => 1554980568,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_date_last_weighed_c.php' => 
  array (
    'md5' => '944ed64b829fa56177e89046ef1055d4',
    'mtime' => 1554980660,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_initial_a_c.php' => 
  array (
    'md5' => '32cfce215cdec7e37a9a246c27763ce3',
    'mtime' => 1554981129,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_a1_c.php' => 
  array (
    'md5' => '4d4ea72bcdc3027cd97235fe1b820172',
    'mtime' => 1554981169,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_a2_c.php' => 
  array (
    'md5' => 'e35ab13332b372e3457a53aba47736d1',
    'mtime' => 1554981206,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_a3_c.php' => 
  array (
    'md5' => '8668126bbcdf814f52a269d9fc317537',
    'mtime' => 1554981266,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_a4_c.php' => 
  array (
    'md5' => '5265da6146b175b90ba3b3f4684dfc18',
    'mtime' => 1554981302,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_initial_b_c.php' => 
  array (
    'md5' => 'f66eb7aa260f9f53b1a9133314b2f415',
    'mtime' => 1554981386,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_b1_c.php' => 
  array (
    'md5' => '5eb0c76aff42c1e0adb16a01c687ebf9',
    'mtime' => 1554981431,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_b2_c.php' => 
  array (
    'md5' => 'ec833efa8aa86f323de72de35ebecff4',
    'mtime' => 1554981478,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_b3_c.php' => 
  array (
    'md5' => 'e1c0c42380b6199b5d070b1f16488370',
    'mtime' => 1554981518,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_b4_c.php' => 
  array (
    'md5' => '2939fc221442d2d1f665624fc44c051f',
    'mtime' => 1554981549,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_initial_c.php' => 
  array (
    'md5' => '597acbec889680ca82eaebcebbe9e178',
    'mtime' => 1554982196,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_c1_c.php' => 
  array (
    'md5' => '302eb56bbc3754705bc72d71e655d5b6',
    'mtime' => 1554982241,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_c2_c.php' => 
  array (
    'md5' => 'e49cefb7f804960b46115a256dc4ce29',
    'mtime' => 1554982334,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_c3_c.php' => 
  array (
    'md5' => '3916678b385e297d9e7122d845b592cb',
    'mtime' => 1554982396,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_c4_c.php' => 
  array (
    'md5' => '1f742e85dfeaf7762db73dea1584db21',
    'mtime' => 1554982458,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_rms_room_id_c.php' => 
  array (
    'md5' => '3d5685bb9ec8fe0f549072023bbab40a',
    'mtime' => 1558163815,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_rt_room_transfer_1_ANML_Animals.php' => 
  array (
    'md5' => 'e0e99285a9566f2687d61a7380f13e9f',
    'mtime' => 1558336727,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_account_id_c.php' => 
  array (
    'md5' => '65cb817ac5002eea53e05ad0925a3c68',
    'mtime' => 1562045094,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_name.php' => 
  array (
    'md5' => 'ae2069bb5e7dd9e818e0aafdd6c41e46',
    'mtime' => 1562674111,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_name_concat_c.php' => 
  array (
    'md5' => 'dfc6fa4e48b0d330c60a045d7386826b',
    'mtime' => 1568207969,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_tsdoc_test_system_documents_1_ANML_Animals.php' => 
  array (
    'md5' => '8ce9739cdad1d127c5f6666a69439fe5',
    'mtime' => 1569846493,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_animal_room_c.php' => 
  array (
    'md5' => 'f1f035f1e5cb5552a24cf1f0bcec5ef2',
    'mtime' => 1574054318,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_bodyweight_c.php' => 
  array (
    'md5' => '0e3631adfb5650e3e0e9113fa8e8746e',
    'mtime' => 1574857627,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_s_species_id_c.php' => 
  array (
    'md5' => 'c46328bdf5b52449b21c0066795d9a37',
    'mtime' => 1575294625,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/enrollment_status.php' => 
  array (
    'md5' => '821fc072d1be1d93c8168aebd26cbdfd',
    'mtime' => 1576042870,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_assigned_to_wp_c.php' => 
  array (
    'md5' => '7a6f4c623eeecce0485cbdfa3c45d723',
    'mtime' => 1576042870,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/custom_import_index.php' => 
  array (
    'md5' => 'b6aef4865c9131f337ad08fdfd041ed3',
    'mtime' => 1576473620,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_w_weight_1_ANML_Animals.php' => 
  array (
    'md5' => 'afab618131d54be2285a57b303c8a9f8',
    'mtime' => 1578403499,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_cie_clinical_issue_exam_1_ANML_Animals.php' => 
  array (
    'md5' => 'ed2f725f9dbefd847565bc88efca29ec',
    'mtime' => 1578404479,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_co_clinical_observation_1_ANML_Animals.php' => 
  array (
    'md5' => '2a0cdc10af9389d4f371a405d20e35f3',
    'mtime' => 1578406674,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_vaccine_c.php' => 
  array (
    'md5' => 'edb79e8d570a2ad8a47117f36ac91a96',
    'mtime' => 1591869676,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_vaccine_a_c.php' => 
  array (
    'md5' => '085604d6df99c8c4c1a5b1f9ae15df4c',
    'mtime' => 1591869676,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_vaccine_b_c.php' => 
  array (
    'md5' => '2d41a879e24a4f582103f0a7dacdd1a7',
    'mtime' => 1591869676,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_sex_c.php' => 
  array (
    'md5' => '58b6429a7af54a1d25e8d82227bf64b5',
    'mtime' => 1591869676,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_breed_c.php' => 
  array (
    'md5' => 'ecbf9a61fb869eb93f32f0fee6e2136f',
    'mtime' => 1591879018,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_m03_work_product_id_c.php' => 
  array (
    'md5' => '1a868c40f825c783ed109da8ed4e29f8',
    'mtime' => 1608013878,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_enrollment_status.php' => 
  array (
    'md5' => '928a41afbe7c375eb52b73ebf6a4114f',
    'mtime' => 1608035966,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_usda_historical_usda_id_1_ANML_Animals.php' => 
  array (
    'md5' => '2376d3b25d7dc87982231afcbbfb994f',
    'mtime' => 1614076950,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_usda_id_hidden_c.php' => 
  array (
    'md5' => 'b27f6fa2c5833def3ccd1992139b1e2c',
    'mtime' => 1614077272,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_confirmed_on_study_c.php' => 
  array (
    'md5' => '71f0e9fea93cd35f45a4d0838350a71a',
    'mtime' => 1614241254,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_abnormality_c.php' => 
  array (
    'md5' => '33a9a9d6270ad325b3eeacda48914ea3',
    'mtime' => 1614851857,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_deceased_checkbox_c.php' => 
  array (
    'md5' => '45f9a078bcf23650f4cf1403f9a3c18d',
    'mtime' => 1615266135,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_a5_c.php' => 
  array (
    'md5' => '3c63dea18d029fb54f1a0b7d76d3afff',
    'mtime' => 1621321723,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_a6_c.php' => 
  array (
    'md5' => '947aa8facb5c6dca30616a7a3404c941',
    'mtime' => 1621321832,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_b5_c.php' => 
  array (
    'md5' => '297fa21cdc1b14db5c2b15448e7edce2',
    'mtime' => 1621321966,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_b6_c.php' => 
  array (
    'md5' => 'e3c836971f5d49ed7d6a1290751b7c88',
    'mtime' => 1621322073,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_c5_c.php' => 
  array (
    'md5' => 'b37e5a85cf682cef1470aa3717ac202c',
    'mtime' => 1621322200,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_c6_c.php' => 
  array (
    'md5' => '1bfed8208edd68ea9881960fb90ed2d2',
    'mtime' => 1621322314,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_usda_id_hidden_c2_c.php' => 
  array (
    'md5' => '885c7f826e768e22de7ae94db302a69c',
    'mtime' => 1621847362,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_allocated_work_product_c.php' => 
  array (
    'md5' => '00c5b473fe10274d005ebbe2f5fe3577',
    'mtime' => 1622719005,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_taskd_task_design_1_ANML_Animals.php' => 
  array (
    'md5' => '710529af109a818804bc0523c55356ec',
    'mtime' => 1628921162,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_vendor_relate_c.php' => 
  array (
    'md5' => 'f7214d021aea329a33367a7961d4920b',
    'mtime' => 1629358184,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_ii_inventory_item_1_ANML_Animals.php' => 
  array (
    'md5' => '9cf714a3120900900741f22a761c7e2d',
    'mtime' => 1636452180,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_animal_id_c.php' => 
  array (
    'md5' => '4c3d599fe3699bfc5dc71910f8de2136',
    'mtime' => 1659520547,
    'is_override' => false,
  ),
  'custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_species_2_c.php' => 
  array (
    'md5' => 'b591fa533cc6292678ce06158dbf14dc',
    'mtime' => 1661853286,
    'is_override' => false,
  ),
);