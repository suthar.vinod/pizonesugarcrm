<?php

$viewdefs['ANML_Animals']['base']['view']['recordlist']['selection']['actions'][] = array(
    'name' => 'wpe_mass_create_button',
    'label' => 'LBL_WPE_MASS_CREATE_BUTTON',
    'type' => 'button',
    'primary' => true,
    'events' => array(
        'click' => 'list:wpemasscreaterecords:fire',
    ),
    'acl_action' => 'massupdate',
);
