<?php

$viewdefs['ANML_Animals']['base']['view']['recordlist']['selection']['actions'][] = array(
    'name' => 'rt_mass_create_button',
    'label' => 'LBL_RT_MASS_CREATE_BUTTON',
    'type' => 'button',
    'primary' => true,
    'events' => array(
        'click' => 'list:rtmasscreaterecords:fire',
    ),
    'acl_action' => 'massupdate',
);
