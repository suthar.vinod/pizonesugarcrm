<?php

$hook_array['after_save'][] = array(
    1,
    'Auto Generate Name For Equipment & Facility Services',
    'custom/modules/EFS_Equipment_Facility_Servi/EquipmentFacilityServicesLogicHook.php',
    'EquipmentFacilityServicesLogicHook',
    'generateName',
);

$hook_array['before_save'][] = array(
    11,
    'Make Next Service Date Null',
    'custom/modules/EFS_Equipment_Facility_Servi/EquipmentFacilityServicesLogicHook.php',
    'EquipmentFacilityServicesLogicHook',
    'checkBeforeSaveNextServiceDate',
);

$hook_array['after_relationship_add'][] = Array(12,
     'Make Next Service Date Null',
    'custom/modules/EFS_Equipment_Facility_Servi/EquipmentFacilityServicesLogicHook.php',
    'EquipmentFacilityServicesLogicHook',
    'checkNextServiceDate',);

$hook_array['before_relationship_delete'][] = Array(13,
     'Make Next Service Date Null',
    'custom/modules/EFS_Equipment_Facility_Servi/EquipmentFacilityServicesLogicHook.php',
    'EquipmentFacilityServicesLogicHook',
    'checkNextServiceDate',);
 

?>