<?php
 // created: 2019-02-25 15:12:14
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['audited']=true;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['massupdate']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['comments']='Full text of the note';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['duplicate_merge']='enabled';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['merge_filter']='disabled';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['unified_search']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['calculated']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['rows']='6';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['cols']='80';

 ?>