<?php
 // created: 2019-07-11 05:45:02
$dictionary['EFS_Equipment_Facility_Servi']['fields']['next_service_date_c']['labelValue']='Next Service Date';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['next_service_date_c']['enforced']='';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['next_service_date_c']['dependency']='and(not(equal($is_status_retired_c,"1")),equal($inactive_service_c,false))';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['next_service_date_c']['readonly']= true;

 ?>
