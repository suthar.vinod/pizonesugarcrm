<?php
 // created: 2019-02-25 15:06:50
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['len']='255';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['audited']=true;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['massupdate']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['unified_search']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.0',
  'searchable' => true,
);
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['calculated']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['required']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['readonly']=true;

 ?>