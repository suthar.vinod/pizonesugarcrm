<?php
// created: 2019-02-25 15:18:39
$dictionary["EFS_Equipment_Facility_Servi"]["fields"]["efs_equipment_facility_servi_efr_equipment_facility_recor_1"] = array (
  'name' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'type' => 'link',
  'relationship' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'source' => 'non-db',
  'module' => 'EFR_Equipment_Facility_Recor',
  'bean_name' => 'EFR_Equipment_Facility_Recor',
  'vname' => 'LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE',
  'id_name' => 'efs_equipmd3d1y_servi_ida',
  'link-type' => 'many',
  'side' => 'left',
);
