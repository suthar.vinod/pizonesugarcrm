<?php
 // created: 2019-02-25 15:13:17
$dictionary['EFS_Equipment_Facility_Servi']['fields']['service_interval_days_c']['labelValue']='Service Interval (Days)';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['service_interval_days_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['EFS_Equipment_Facility_Servi']['fields']['service_interval_days_c']['enforced']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['service_interval_days_c']['dependency']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['service_interval_days_c']['required'] = true;

 ?>