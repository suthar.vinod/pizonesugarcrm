<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EFFECTIVE_DATE'] = 'Effective Date';
$mod_strings['LBL_RECORD_BODY'] = 'Contact Document';
$mod_strings['LBL_APPROVED_PRD_LEVEL'] = 'Approved PRD Level';
$mod_strings['LBL_MAJ_CONTACT_DOCUMENTS_FOCUS_DRAWER_DASHBOARD'] = 'Contact Documents Focus Drawer';
$mod_strings['LBL_MAJ_CONTACT_DOCUMENTS_RECORD_DASHBOARD'] = 'Contact Documents Record Dashboard';
