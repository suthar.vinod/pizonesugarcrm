<?php
 // created: 2019-08-15 11:31:35
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['required']=true;
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['audited']=true;
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['massupdate']=true;
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['options']='contact_document_list';
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['duplicate_merge']='enabled';
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['duplicate_merge_dom_value']='1';
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['merge_filter']='disabled';
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['reportable']=true;
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['unified_search']=false;
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['calculated']=false;
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['dependency']=false;

 ?>