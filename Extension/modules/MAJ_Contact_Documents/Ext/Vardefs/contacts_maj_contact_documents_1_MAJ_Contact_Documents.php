<?php
// created: 2019-08-15 11:34:01
$dictionary["MAJ_Contact_Documents"]["fields"]["contacts_maj_contact_documents_1"] = array (
  'name' => 'contacts_maj_contact_documents_1',
  'type' => 'link',
  'relationship' => 'contacts_maj_contact_documents_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_MAJ_CONTACT_DOCUMENTS_TITLE',
  'id_name' => 'contacts_maj_contact_documents_1contacts_ida',
  'link-type' => 'one',
);
$dictionary["MAJ_Contact_Documents"]["fields"]["contacts_maj_contact_documents_1_name"] = array (
  'name' => 'contacts_maj_contact_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_maj_contact_documents_1contacts_ida',
  'link' => 'contacts_maj_contact_documents_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["MAJ_Contact_Documents"]["fields"]["contacts_maj_contact_documents_1contacts_ida"] = array (
  'name' => 'contacts_maj_contact_documents_1contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_MAJ_CONTACT_DOCUMENTS_TITLE_ID',
  'id_name' => 'contacts_maj_contact_documents_1contacts_ida',
  'link' => 'contacts_maj_contact_documents_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
