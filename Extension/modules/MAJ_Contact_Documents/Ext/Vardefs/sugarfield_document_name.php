<?php
 // created: 2019-08-15 11:30:28
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['audited']=true;
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['massupdate']=false;
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['merge_filter']='disabled';
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['unified_search']=false;
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['calculated']=false;

 ?>