<?php
 // created: 2020-09-08 11:29:39
$dictionary['MAJ_Contact_Documents']['fields']['approved_prd_level_c']['labelValue']='Approved PRD Level';
$dictionary['MAJ_Contact_Documents']['fields']['approved_prd_level_c']['visibility_grid']=array (
  'trigger' => 'category_id',
  'values' => 
  array (
    '' => 
    array (
    ),
    'CV' => 
    array (
    ),
    'CV and PRD' => 
    array (
      0 => '',
      1 => 'Assistant',
      2 => 'Primary',
    ),
    'PRD' => 
    array (
      0 => '',
      1 => 'Assistant',
      2 => 'Primary',
    ),
  ),
);

 ?>