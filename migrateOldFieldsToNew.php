<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');


$updateQuery1 = "UPDATE m06_error_cstm SET occured_on_weekend_c = 0 WHERE (error_occured_on_weekend_c = 'No' OR error_occured_on_weekend_c = 'Choose One' OR error_occured_on_weekend_c IS NULL)";
$updateQuery2 = "UPDATE m06_error_cstm SET occured_on_weekend_c = 1 WHERE error_occured_on_weekend_c = 'Yes'";
$error = "";
$msg = "";

if (!$GLOBALS['db']->query($updateQuery1)) {
    $error .= "Conversion of 'No /Choose One / Null' values failed for field: Error Occurred on Weekend.\n";
} else {
    $msg .= "All the previous 'No /Choose One / Null' values of Error Occurred on Weekend field get converted to 0 (Un-checked)</br>";
}

if (!$GLOBALS['db']->query($updateQuery2)) {
    $error .= "Conversion of 'Yes' value failed for field: Error Occurred on Weekend.";
} else {
    $msg .= "All the previous 'Yes' value of Error Occured on Weekend get converted to 1 (Checked).";
}

if(!empty($error)) {
    echo "Error: " . $error . "</br>";
}
echo "Success: ". $msg;
