<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$result = $db->query("SELECT m03_work_product_cstm.id_c, m03_work_product_cstm.aps_principal_investigator_c FROM m03_work_product_cstm INNER JOIN m03_work_product ON m03_work_product.id = m03_work_product_cstm.id_c WHERE m03_work_product.deleted = 0");
if ($result) {
    while ($row = $db->fetchByAssoc($result)) {
        $id = $row['id_c'];
        $aps_principal_investigator_key = $row['aps_principal_investigator_c'];
        $aps_principal_investigator_value = $GLOBALS['app_list_strings']['study_director_list'][$aps_principal_investigator_key];

        if ($aps_principal_investigator_key) {
            if ($aps_principal_investigator_value == "Liz ROby") {
                $query = "select id from contacts where concat(first_name,' ',last_name) = 'Liz Roby' and deleted = 0";
            } else if ($aps_principal_investigator_value == "Melissa Carlson") {
                $query = "select id from contacts where concat(first_name,' ',last_name) = 'Melissa Bruns' and deleted = 0";
            } else if ($aps_principal_investigator_value == "Kristin Booth") {
                $query = "select id from contacts where id = '42a2cbd0-19cf-11e9-b47c-06036fae356c' and deleted = 0";
            } else if ($aps_principal_investigator_value == "Thomas van Valkenburg") {
                $query = "select id from contacts where concat(first_name,' ',last_name) = 'Thomas Van Valkenburg' and deleted = 0";
            } else if ($aps_principal_investigator_value == "Muhammad Ashan") {
                $query = "select id from contacts where concat(first_name,' ',last_name) = 'Muhammad Ahsan' and deleted = 0";
            } else if ($aps_principal_investigator_value == "Maria Emery") {
                $query = "select id from contacts where concat(first_name,' ',last_name) = 'Marla Emery' and deleted = 0";
            } else {
                $query = "select id from contacts where concat(first_name,' ',last_name) = '" . $aps_principal_investigator_value . "' and deleted = 0";
            }
            $result2 = $db->query($query);
            if ($row2 = $db->fetchByAssoc($result2)) {
                $query2 = "update m03_work_product_cstm set contact_id1_c = '" . $row2['id'] . "' where id_c = '" . $id . "'";
                if (!$db->query($query2)) {
                    echo "Updation Failed";
                }
            } else {
                $msg[] = "Fail to populate the contact '" . $aps_principal_investigator_value . "' for Work Product Id : '" . $id . "' <br>";
            }
        }
    }
    print_r($msg);
    echo "Script executed successfully";
} else {
    echo "Table not found";
}
