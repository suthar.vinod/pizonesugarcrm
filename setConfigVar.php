<?php

require_once 'modules/Configurator/Configurator.php';
$configuratorObj = new Configurator();
// //Load config
$configuratorObj->loadConfig();
$url = "<a href='index.php?module=Configurator&action=EditView'>System Settings</a>";
if(isset($configuratorObj->config['observation_notification_email'])) {
  if(empty($configuratorObj->config['observation_notification_email'])) {
    echo "Observation notification email key already set with empty value. Please update email in Administration > ".$url;
  } else {
    echo "Observation notification email key already set with value: ".$configuratorObj->config['observation_notification_email'];
  }
} else {
  //Update a specific setting
  $configuratorObj->config['observation_notification_email'] = "";
  //Save the new setting
  $configuratorObj->saveConfig();
  echo "Observation notification email key is added with empty value. Please update email in Administration > ".$url;
}
