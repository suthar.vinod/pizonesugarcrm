<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$result = $db->query("SELECT ere_error_employees.name, ere_error_employees.id, ere_error_employees_de_deviation_employees_1_c.ere_error_7902ployees_idb FROM ere_error_employees INNER JOIN ere_error_employees_de_deviation_employees_1_c ON ere_error_employees.id = ere_error_employees_de_deviation_employees_1_c.ere_error_ee6cployees_ida WHERE ere_error_employees.deleted = 0 AND ere_error_employees_de_deviation_employees_1_c.deleted = 0");
if ($result) {
    while ($row = $db->fetchByAssoc($result)) {
        $emp_id = $row['id'];
        $name = $row['name'];
        $msgTemp = "Name is ". $name;
        $ObserEmployeeIDs = $row['ere_error_7902ployees_idb'];

        if ($ObserEmployeeIDs) {

            if ($name == 'Abigail Beltrame')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Abby Beltrame' and deleted = 0";
            else if ($name == 'Alex Johnson')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Alexander Johnson' and deleted = 0";
            else if ($name == 'Alex Peters')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Alexander Peters' and deleted = 0";
            else if ($name == 'Ashley Lee')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Pa (Ashley) Lee' and deleted = 0";
            else if ($name == 'Benjamin Vos')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Ben Vos' and deleted = 0";
            else if ($name == 'Carlus Dingfelder Jr')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Carlus Dingfelder' and deleted = 0";
            else if ($name == 'Cassie Ditter')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Cassandra Ditter' and deleted = 0";
            else if ($name == 'Cecilia Strother')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Cecelia Strother' and deleted = 0";

            else if ($name == 'Christopher Lafean')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Chris Lafean' and deleted = 0";
            else if ($name == 'Elizabeth Carter')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Liisa Carter' and deleted = 0";
            else if ($name == 'Elizabeth Clark')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Liz Clark' and deleted = 0";
            else if ($name == 'Erica Gearen')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Erika Gearen' and deleted = 0";
            else if ($name == 'Erica Vanreeth')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Erica Van Reeth' and deleted = 0";
            else if ($name == 'Jenny Preuss')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Jennifer Preuss' and deleted = 0";
            else if ($name == 'Jim Pomonis')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'James Pomonis' and deleted = 0";
            else if ($name == 'Josh Albrecht')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Joshua Albrecht' and deleted = 0";
            else if ($name == 'Kate Kleinert')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Katherine Kleinert' and deleted = 0";
            else if ($name == 'Kim Jessen')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Kimberly Jessen' and deleted = 0";

            else if ($name == 'Kimberly Castle')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Kim Castle' and deleted = 0";
            else if ($name == 'Matthew Cunningham')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Matt Cunningham' and deleted = 0";
            else if ($name == "Megan O&#039;Brien"){
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) like '%Megan%Brien' and deleted = 0";
            }else if ($name == 'Pam Genereux')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Pamela Genereux' and deleted = 0";

            else if ($name == 'Tim Green')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Timothy Green' and deleted = 0";
            else if ($name == 'Ashley Sanders')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Sarah (Ashley) Sanders' and deleted = 0";
            else if ($name == 'Derek Meacham')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Derrek Meacham' and deleted = 0";
            else if ($name == 'JLF, KAV')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) IN ('Jordan Fick','Katherine Vail') and deleted = 0";
            else if ($name == 'Sponsor')
                $query = "SELECT * FROM contacts where (first_name is null or first_name = '') and last_name = 'Sponsor' and deleted = 0";
            else if ($name == 'Unknown (IVT)')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Unknown (Lab Services)' and deleted = 0";
            else if ($name == 'Unknown (SP)')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Unknown (Lab Services)' and deleted = 0";
            else if ($name == 'Ashley Sanders')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Sarah (Ashley) Sanders' and deleted = 0";
            else if ($name == 'Andre Nelson')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'André Nelson' and deleted = 0";
            else if ($name == 'Daniel Kroeninger')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Dan Kroeninger' and deleted = 0";
            else if ($name == 'Danielle Zehowski')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Dani Zehowski' and deleted = 0";
            else if ($name == 'Katherine Dunford')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Katie Dunford' and deleted = 0";
            else if ($name == 'Katherine Jenkins')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Katie Jenkins' and deleted = 0";
            else if ($name == 'Nicholas Mccune')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Nick McCune' and deleted = 0";
            else if ($name == 'Tamara Fossum')
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = 'Tammy Fossum' and deleted = 0";
            else
                $query = "SELECT id FROM contacts where concat(first_name,' ',last_name) = '" . $name . "' and deleted = 0";

            $result2 = $db->query($query);

            if ($row = $db->fetchByAssoc($result2)) {
                $contact_id = $row['id'];
                $id = create_guid();
                $date_modified = date("Y-m-d H:i:s");
                $deleted = 0;
                $queryForContact = "insert into contacts_de_deviation_employees_1_c (id , date_modified , deleted , contacts_de_deviation_employees_1contacts_ida, contacts_de_deviation_employees_1de_deviation_employees_idb) "
                        . "values ('" . $id . "', '" . $date_modified . "', '" . $deleted . "', '" . $contact_id . "',  '" . $ObserEmployeeIDs . "')";
                if (!$db->query($queryForContact)) {
                    echo "Migration failed for '" . $name . "'";
                    exit(1);
                } else {
                    $msg1[] = " '" . $emp_id . "'. '  ' . " . $name . "' <br>";
                    if ($rowNested = $db->fetchByAssoc($result2)) {
                        $contact_id = $rowNested['id'];
                        $id = create_guid();
                        $date_modified = date("Y-m-d H:i:s");
                        $deleted = 0;
                        $queryForContact = "insert into contacts_de_deviation_employees_1_c (id , date_modified , deleted , contacts_de_deviation_employees_1contacts_ida, contacts_de_deviation_employees_1de_deviation_employees_idb) "
                                . "values ('" . $id . "', '" . $date_modified . "', '" . $deleted . "', '" . $contact_id . "',  '" . $ObserEmployeeIDs . "')";
                        if (!$db->query($queryForContact)) {
                            echo "Migration failed for '" . $name . "'";
                            exit(1);
                        } else {
                            $msg1[] = " '" . $emp_id . "'. '  ' . " . $name . "' <br>";
                        }
                    }
                }
            } else {
                $msg[] = " '" . $emp_id . "'. '  ' . " . $name . "' <br>";
            }
        }
    }

    echo "Script executed successfully";
    echo "Following are the APS Employees which don't match with any Contact";
    echo "***************************************************";
    echo "<br>";
    print_r($msg);

    echo "<br>";
    echo "Following APS Employees Data copy successfully!";
    echo "***************************************************";
    echo "<br>";
    print_r($msg1);
} else {
    echo "Table not found";
}

