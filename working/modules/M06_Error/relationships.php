<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'm06_error_m03_work_product_1' => 
  array (
    'name' => 'm06_error_m03_work_product_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_m03_work_product_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product',
        'rhs_table' => 'm03_work_product',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_m03_work_product_1_c',
        'join_key_lhs' => 'm06_error_m03_work_product_1m06_error_ida',
        'join_key_rhs' => 'm06_error_m03_work_product_1m03_work_product_idb',
      ),
    ),
    'table' => 'm06_error_m03_work_product_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_m03_work_product_1m06_error_ida' => 
      array (
        'name' => 'm06_error_m03_work_product_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_m03_work_product_1m03_work_product_idb' => 
      array (
        'name' => 'm06_error_m03_work_product_1m03_work_product_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_m03_work_product_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_m03_work_product_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_m03_work_product_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_m03_work_product_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_m03_work_product_1m03_work_product_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_m03_work_product_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_m03_work_product_1m06_error_ida',
          1 => 'm06_error_m03_work_product_1m03_work_product_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm06_error_m03_work_product_1_c',
    'join_key_lhs' => 'm06_error_m03_work_product_1m06_error_ida',
    'join_key_rhs' => 'm06_error_m03_work_product_1m03_work_product_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_m03_work_product_1',
    'rhs_subpanel' => 'ForM06_errorM06_error_m03_work_product_1',
    'lhs_subpanel' => 'ForM03_work_productM06_error_m03_work_product_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm06_error_anml_animals_1' => 
  array (
    'name' => 'm06_error_anml_animals_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_anml_animals_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'ANML_Animals',
        'rhs_table' => 'anml_animals',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_anml_animals_1_c',
        'join_key_lhs' => 'm06_error_anml_animals_1m06_error_ida',
        'join_key_rhs' => 'm06_error_anml_animals_1anml_animals_idb',
      ),
    ),
    'table' => 'm06_error_anml_animals_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_anml_animals_1m06_error_ida' => 
      array (
        'name' => 'm06_error_anml_animals_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_anml_animals_1anml_animals_idb' => 
      array (
        'name' => 'm06_error_anml_animals_1anml_animals_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_anml_animals_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_anml_animals_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_anml_animals_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_anml_animals_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_anml_animals_1anml_animals_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_anml_animals_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_anml_animals_1m06_error_ida',
          1 => 'm06_error_anml_animals_1anml_animals_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'ANML_Animals',
    'rhs_table' => 'anml_animals',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm06_error_anml_animals_1_c',
    'join_key_lhs' => 'm06_error_anml_animals_1m06_error_ida',
    'join_key_rhs' => 'm06_error_anml_animals_1anml_animals_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_anml_animals_1',
    'rhs_subpanel' => 'ForM06_errorM06_error_anml_animals_1',
    'lhs_subpanel' => 'ForAnml_animalsM06_error_anml_animals_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm06_error_equip_equipment_1' => 
  array (
    'name' => 'm06_error_equip_equipment_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_equip_equipment_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'Equip_Equipment',
        'rhs_table' => 'equip_equipment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_equip_equipment_1_c',
        'join_key_lhs' => 'm06_error_equip_equipment_1m06_error_ida',
        'join_key_rhs' => 'm06_error_equip_equipment_1equip_equipment_idb',
      ),
    ),
    'table' => 'm06_error_equip_equipment_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_equip_equipment_1m06_error_ida' => 
      array (
        'name' => 'm06_error_equip_equipment_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_equip_equipment_1equip_equipment_idb' => 
      array (
        'name' => 'm06_error_equip_equipment_1equip_equipment_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_equip_equipment_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_equip_equipment_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_equip_equipment_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_equip_equipment_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_equip_equipment_1equip_equipment_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_equip_equipment_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_equip_equipment_1m06_error_ida',
          1 => 'm06_error_equip_equipment_1equip_equipment_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'Equip_Equipment',
    'rhs_table' => 'equip_equipment',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm06_error_equip_equipment_1_c',
    'join_key_lhs' => 'm06_error_equip_equipment_1m06_error_ida',
    'join_key_rhs' => 'm06_error_equip_equipment_1equip_equipment_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_equip_equipment_1',
    'rhs_subpanel' => 'ForM06_errorM06_error_equip_equipment_1',
    'lhs_subpanel' => 'ForEquip_equipmentM06_error_equip_equipment_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm06_error_erd_error_documents_1' => 
  array (
    'name' => 'm06_error_erd_error_documents_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_erd_error_documents_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'Erd_Error_Documents',
        'rhs_table' => 'erd_error_documents',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_erd_error_documents_1_c',
        'join_key_lhs' => 'm06_error_erd_error_documents_1m06_error_ida',
        'join_key_rhs' => 'm06_error_erd_error_documents_1erd_error_documents_idb',
      ),
    ),
    'table' => 'm06_error_erd_error_documents_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_erd_error_documents_1m06_error_ida' => 
      array (
        'name' => 'm06_error_erd_error_documents_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_erd_error_documents_1erd_error_documents_idb' => 
      array (
        'name' => 'm06_error_erd_error_documents_1erd_error_documents_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_erd_error_documents_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_erd_error_documents_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_erd_error_documents_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_erd_error_documents_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_erd_error_documents_1erd_error_documents_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_erd_error_documents_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_erd_error_documents_1m06_error_ida',
          1 => 'm06_error_erd_error_documents_1erd_error_documents_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'Erd_Error_Documents',
    'rhs_table' => 'erd_error_documents',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm06_error_erd_error_documents_1_c',
    'join_key_lhs' => 'm06_error_erd_error_documents_1m06_error_ida',
    'join_key_rhs' => 'm06_error_erd_error_documents_1erd_error_documents_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_erd_error_documents_1',
    'rhs_subpanel' => 'ForM06_errorM06_error_erd_error_documents_1',
    'lhs_subpanel' => 'ForErd_error_documentsM06_error_erd_error_documents_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm06_error_ere_error_employees_1' => 
  array (
    'name' => 'm06_error_ere_error_employees_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_ere_error_employees_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'Ere_Error_Employees',
        'rhs_table' => 'ere_error_employees',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_ere_error_employees_1_c',
        'join_key_lhs' => 'm06_error_ere_error_employees_1m06_error_ida',
        'join_key_rhs' => 'm06_error_ere_error_employees_1ere_error_employees_idb',
      ),
    ),
    'table' => 'm06_error_ere_error_employees_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_ere_error_employees_1m06_error_ida' => 
      array (
        'name' => 'm06_error_ere_error_employees_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_ere_error_employees_1ere_error_employees_idb' => 
      array (
        'name' => 'm06_error_ere_error_employees_1ere_error_employees_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_ere_error_employees_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_ere_error_employees_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_ere_error_employees_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_ere_error_employees_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_ere_error_employees_1ere_error_employees_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_ere_error_employees_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_ere_error_employees_1m06_error_ida',
          1 => 'm06_error_ere_error_employees_1ere_error_employees_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'Ere_Error_Employees',
    'rhs_table' => 'ere_error_employees',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm06_error_ere_error_employees_1_c',
    'join_key_lhs' => 'm06_error_ere_error_employees_1m06_error_ida',
    'join_key_rhs' => 'm06_error_ere_error_employees_1ere_error_employees_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_ere_error_employees_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm06_error_ed_errors_department_1' => 
  array (
    'name' => 'm06_error_ed_errors_department_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_ed_errors_department_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'ED_Errors_Department',
        'rhs_table' => 'ed_errors_department',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_ed_errors_department_1_c',
        'join_key_lhs' => 'm06_error_ed_errors_department_1m06_error_ida',
        'join_key_rhs' => 'm06_error_ed_errors_department_1ed_errors_department_idb',
      ),
    ),
    'table' => 'm06_error_ed_errors_department_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_ed_errors_department_1m06_error_ida' => 
      array (
        'name' => 'm06_error_ed_errors_department_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_ed_errors_department_1ed_errors_department_idb' => 
      array (
        'name' => 'm06_error_ed_errors_department_1ed_errors_department_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_ed_errors_department_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_ed_errors_department_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_ed_errors_department_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_ed_errors_department_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_ed_errors_department_1ed_errors_department_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_ed_errors_department_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_ed_errors_department_1m06_error_ida',
          1 => 'm06_error_ed_errors_department_1ed_errors_department_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'ED_Errors_Department',
    'rhs_table' => 'ed_errors_department',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm06_error_ed_errors_department_1_c',
    'join_key_lhs' => 'm06_error_ed_errors_department_1m06_error_ida',
    'join_key_rhs' => 'm06_error_ed_errors_department_1ed_errors_department_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_ed_errors_department_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'erqc_error_qc_employees_m06_error_1' => 
  array (
    'name' => 'erqc_error_qc_employees_m06_error_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'erqc_error_qc_employees_m06_error_1' => 
      array (
        'lhs_module' => 'ErQC_Error_QC_Employees',
        'lhs_table' => 'erqc_error_qc_employees',
        'lhs_key' => 'id',
        'rhs_module' => 'M06_Error',
        'rhs_table' => 'm06_error',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'erqc_error_qc_employees_m06_error_1_c',
        'join_key_lhs' => 'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida',
        'join_key_rhs' => 'erqc_error_qc_employees_m06_error_1m06_error_idb',
      ),
    ),
    'table' => 'erqc_error_qc_employees_m06_error_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida' => 
      array (
        'name' => 'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida',
        'type' => 'id',
      ),
      'erqc_error_qc_employees_m06_error_1m06_error_idb' => 
      array (
        'name' => 'erqc_error_qc_employees_m06_error_1m06_error_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_erqc_error_qc_employees_m06_error_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_erqc_error_qc_employees_m06_error_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_erqc_error_qc_employees_m06_error_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'erqc_error_qc_employees_m06_error_1m06_error_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'erqc_error_qc_employees_m06_error_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida',
          1 => 'erqc_error_qc_employees_m06_error_1m06_error_idb',
        ),
      ),
    ),
    'lhs_module' => 'ErQC_Error_QC_Employees',
    'lhs_table' => 'erqc_error_qc_employees',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'erqc_error_qc_employees_m06_error_1_c',
    'join_key_lhs' => 'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida',
    'join_key_rhs' => 'erqc_error_qc_employees_m06_error_1m06_error_idb',
    'readonly' => true,
    'relationship_name' => 'erqc_error_qc_employees_m06_error_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm06_error_de_deviation_employees_1' => 
  array (
    'name' => 'm06_error_de_deviation_employees_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_de_deviation_employees_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'DE_Deviation_Employees',
        'rhs_table' => 'de_deviation_employees',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_de_deviation_employees_1_c',
        'join_key_lhs' => 'm06_error_de_deviation_employees_1m06_error_ida',
        'join_key_rhs' => 'm06_error_de_deviation_employees_1de_deviation_employees_idb',
      ),
    ),
    'table' => 'm06_error_de_deviation_employees_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_de_deviation_employees_1m06_error_ida' => 
      array (
        'name' => 'm06_error_de_deviation_employees_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_de_deviation_employees_1de_deviation_employees_idb' => 
      array (
        'name' => 'm06_error_de_deviation_employees_1de_deviation_employees_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_de_deviation_employees_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_de_deviation_employees_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_de_deviation_employees_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_de_deviation_employees_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_de_deviation_employees_1de_deviation_employees_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_de_deviation_employees_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_de_deviation_employees_1de_deviation_employees_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'DE_Deviation_Employees',
    'rhs_table' => 'de_deviation_employees',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm06_error_de_deviation_employees_1_c',
    'join_key_lhs' => 'm06_error_de_deviation_employees_1m06_error_ida',
    'join_key_rhs' => 'm06_error_de_deviation_employees_1de_deviation_employees_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_de_deviation_employees_1',
    'rhs_subpanel' => 'ForM06_errorM06_error_de_deviation_employees_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm06_error_tasks_1' => 
  array (
    'name' => 'm06_error_tasks_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_tasks_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'Tasks',
        'rhs_table' => 'tasks',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_tasks_1_c',
        'join_key_lhs' => 'm06_error_tasks_1m06_error_ida',
        'join_key_rhs' => 'm06_error_tasks_1tasks_idb',
      ),
    ),
    'table' => 'm06_error_tasks_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_tasks_1m06_error_ida' => 
      array (
        'name' => 'm06_error_tasks_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_tasks_1tasks_idb' => 
      array (
        'name' => 'm06_error_tasks_1tasks_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_tasks_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_tasks_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_tasks_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_tasks_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_tasks_1tasks_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_tasks_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_tasks_1tasks_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'Tasks',
    'rhs_table' => 'tasks',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm06_error_tasks_1_c',
    'join_key_lhs' => 'm06_error_tasks_1m06_error_ida',
    'join_key_rhs' => 'm06_error_tasks_1tasks_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_tasks_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'a1a_critical_phase_inspectio_m06_error_1' => 
  array (
    'name' => 'a1a_critical_phase_inspectio_m06_error_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'a1a_critical_phase_inspectio_m06_error_1' => 
      array (
        'lhs_module' => 'A1A_Critical_Phase_Inspectio',
        'lhs_table' => 'a1a_critical_phase_inspectio',
        'lhs_key' => 'id',
        'rhs_module' => 'M06_Error',
        'rhs_table' => 'm06_error',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'a1a_critical_phase_inspectio_m06_error_1_c',
        'join_key_lhs' => 'a1a_critic9e89spectio_ida',
        'join_key_rhs' => 'a1a_critical_phase_inspectio_m06_error_1m06_error_idb',
      ),
    ),
    'table' => 'a1a_critical_phase_inspectio_m06_error_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'a1a_critic9e89spectio_ida' => 
      array (
        'name' => 'a1a_critic9e89spectio_ida',
        'type' => 'id',
      ),
      'a1a_critical_phase_inspectio_m06_error_1m06_error_idb' => 
      array (
        'name' => 'a1a_critical_phase_inspectio_m06_error_1m06_error_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_a1a_critical_phase_inspectio_m06_error_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_a1a_critical_phase_inspectio_m06_error_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'a1a_critic9e89spectio_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_a1a_critical_phase_inspectio_m06_error_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'a1a_critical_phase_inspectio_m06_error_1m06_error_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'a1a_critical_phase_inspectio_m06_error_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'a1a_critical_phase_inspectio_m06_error_1m06_error_idb',
        ),
      ),
    ),
    'lhs_module' => 'A1A_Critical_Phase_Inspectio',
    'lhs_table' => 'a1a_critical_phase_inspectio',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'a1a_critical_phase_inspectio_m06_error_1_c',
    'join_key_lhs' => 'a1a_critic9e89spectio_ida',
    'join_key_rhs' => 'a1a_critical_phase_inspectio_m06_error_1m06_error_idb',
    'readonly' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_m06_error_1',
    'rhs_subpanel' => 'ForA1a_critical_phase_inspectioA1a_critical_phase_inspectio_m06_error_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm06_error_rms_room_1' => 
  array (
    'name' => 'm06_error_rms_room_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_rms_room_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'RMS_Room',
        'rhs_table' => 'rms_room',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_rms_room_1_c',
        'join_key_lhs' => 'm06_error_rms_room_1m06_error_ida',
        'join_key_rhs' => 'm06_error_rms_room_1rms_room_idb',
      ),
    ),
    'table' => 'm06_error_rms_room_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_rms_room_1m06_error_ida' => 
      array (
        'name' => 'm06_error_rms_room_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_rms_room_1rms_room_idb' => 
      array (
        'name' => 'm06_error_rms_room_1rms_room_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_rms_room_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_rms_room_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_rms_room_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_rms_room_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_rms_room_1rms_room_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_rms_room_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_rms_room_1m06_error_ida',
          1 => 'm06_error_rms_room_1rms_room_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'RMS_Room',
    'rhs_table' => 'rms_room',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm06_error_rms_room_1_c',
    'join_key_lhs' => 'm06_error_rms_room_1m06_error_ida',
    'join_key_rhs' => 'm06_error_rms_room_1rms_room_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_rms_room_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'w_weight_m06_error_1' => 
  array (
    'name' => 'w_weight_m06_error_1',
    'true_relationship_type' => 'one-to-one',
    'from_studio' => true,
    'relationships' => 
    array (
      'w_weight_m06_error_1' => 
      array (
        'lhs_module' => 'W_Weight',
        'lhs_table' => 'w_weight',
        'lhs_key' => 'id',
        'rhs_module' => 'M06_Error',
        'rhs_table' => 'm06_error',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'w_weight_m06_error_1_c',
        'join_key_lhs' => 'w_weight_m06_error_1w_weight_ida',
        'join_key_rhs' => 'w_weight_m06_error_1m06_error_idb',
      ),
    ),
    'table' => 'w_weight_m06_error_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'w_weight_m06_error_1w_weight_ida' => 
      array (
        'name' => 'w_weight_m06_error_1w_weight_ida',
        'type' => 'id',
      ),
      'w_weight_m06_error_1m06_error_idb' => 
      array (
        'name' => 'w_weight_m06_error_1m06_error_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_w_weight_m06_error_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_w_weight_m06_error_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'w_weight_m06_error_1w_weight_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_w_weight_m06_error_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'w_weight_m06_error_1m06_error_idb',
          1 => 'deleted',
        ),
      ),
    ),
    'lhs_module' => 'W_Weight',
    'lhs_table' => 'w_weight',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-one',
    'join_table' => 'w_weight_m06_error_1_c',
    'join_key_lhs' => 'w_weight_m06_error_1w_weight_ida',
    'join_key_rhs' => 'w_weight_m06_error_1m06_error_idb',
    'readonly' => true,
    'relationship_name' => 'w_weight_m06_error_1',
    'rhs_subpanel' => 'ForW_weightW_weight_m06_error_2',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'cie_clinical_issue_exam_m06_error_1' => 
  array (
    'name' => 'cie_clinical_issue_exam_m06_error_1',
    'true_relationship_type' => 'one-to-one',
    'from_studio' => true,
    'relationships' => 
    array (
      'cie_clinical_issue_exam_m06_error_1' => 
      array (
        'lhs_module' => 'CIE_Clinical_Issue_Exam',
        'lhs_table' => 'cie_clinical_issue_exam',
        'lhs_key' => 'id',
        'rhs_module' => 'M06_Error',
        'rhs_table' => 'm06_error',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'cie_clinical_issue_exam_m06_error_1_c',
        'join_key_lhs' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
        'join_key_rhs' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
      ),
    ),
    'table' => 'cie_clinical_issue_exam_m06_error_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida' => 
      array (
        'name' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
        'type' => 'id',
      ),
      'cie_clinical_issue_exam_m06_error_1m06_error_idb' => 
      array (
        'name' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_cie_clinical_issue_exam_m06_error_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_cie_clinical_issue_exam_m06_error_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_cie_clinical_issue_exam_m06_error_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
          1 => 'deleted',
        ),
      ),
    ),
    'lhs_module' => 'CIE_Clinical_Issue_Exam',
    'lhs_table' => 'cie_clinical_issue_exam',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-one',
    'join_table' => 'cie_clinical_issue_exam_m06_error_1_c',
    'join_key_lhs' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
    'join_key_rhs' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
    'readonly' => true,
    'relationship_name' => 'cie_clinical_issue_exam_m06_error_1',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'co_clinical_observation_m06_error_1' => 
  array (
    'name' => 'co_clinical_observation_m06_error_1',
    'true_relationship_type' => 'one-to-one',
    'from_studio' => true,
    'relationships' => 
    array (
      'co_clinical_observation_m06_error_1' => 
      array (
        'lhs_module' => 'CO_Clinical_Observation',
        'lhs_table' => 'co_clinical_observation',
        'lhs_key' => 'id',
        'rhs_module' => 'M06_Error',
        'rhs_table' => 'm06_error',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'co_clinical_observation_m06_error_1_c',
        'join_key_lhs' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
        'join_key_rhs' => 'co_clinical_observation_m06_error_1m06_error_idb',
      ),
    ),
    'table' => 'co_clinical_observation_m06_error_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'co_clinical_observation_m06_error_1co_clinical_observation_ida' => 
      array (
        'name' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
        'type' => 'id',
      ),
      'co_clinical_observation_m06_error_1m06_error_idb' => 
      array (
        'name' => 'co_clinical_observation_m06_error_1m06_error_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_co_clinical_observation_m06_error_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_co_clinical_observation_m06_error_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_co_clinical_observation_m06_error_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'co_clinical_observation_m06_error_1m06_error_idb',
          1 => 'deleted',
        ),
      ),
    ),
    'lhs_module' => 'CO_Clinical_Observation',
    'lhs_table' => 'co_clinical_observation',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-one',
    'join_table' => 'co_clinical_observation_m06_error_1_c',
    'join_key_lhs' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
    'join_key_rhs' => 'co_clinical_observation_m06_error_1m06_error_idb',
    'readonly' => true,
    'relationship_name' => 'co_clinical_observation_m06_error_1',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm06_error_m06_error_1' => 
  array (
    'name' => 'm06_error_m06_error_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_m06_error_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'M06_Error',
        'rhs_table' => 'm06_error',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_m06_error_1_c',
        'join_key_lhs' => 'm06_error_m06_error_1m06_error_ida',
        'join_key_rhs' => 'm06_error_m06_error_1m06_error_idb',
      ),
    ),
    'table' => 'm06_error_m06_error_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_m06_error_1m06_error_ida' => 
      array (
        'name' => 'm06_error_m06_error_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_m06_error_1m06_error_idb' => 
      array (
        'name' => 'm06_error_m06_error_1m06_error_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_m06_error_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_m06_error_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_m06_error_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_m06_error_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_m06_error_1m06_error_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_m06_error_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_m06_error_1m06_error_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm06_error_m06_error_1_c',
    'join_key_lhs' => 'm06_error_m06_error_1m06_error_ida',
    'join_key_rhs' => 'm06_error_m06_error_1m06_error_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_m06_error_1',
    'rhs_subpanel' => 'ForM06_errorM06_error_m06_error_1',
    'lhs_subpanel' => 'ForM06_errorM06_error_m06_error_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'w_weight_m06_error_2' => 
  array (
    'name' => 'w_weight_m06_error_2',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'w_weight_m06_error_2' => 
      array (
        'lhs_module' => 'W_Weight',
        'lhs_table' => 'w_weight',
        'lhs_key' => 'id',
        'rhs_module' => 'M06_Error',
        'rhs_table' => 'm06_error',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'w_weight_m06_error_2_c',
        'join_key_lhs' => 'w_weight_m06_error_2w_weight_ida',
        'join_key_rhs' => 'w_weight_m06_error_2m06_error_idb',
      ),
    ),
    'table' => 'w_weight_m06_error_2_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'w_weight_m06_error_2w_weight_ida' => 
      array (
        'name' => 'w_weight_m06_error_2w_weight_ida',
        'type' => 'id',
      ),
      'w_weight_m06_error_2m06_error_idb' => 
      array (
        'name' => 'w_weight_m06_error_2m06_error_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_w_weight_m06_error_2_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_w_weight_m06_error_2_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'w_weight_m06_error_2w_weight_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_w_weight_m06_error_2_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'w_weight_m06_error_2m06_error_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'w_weight_m06_error_2_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'w_weight_m06_error_2m06_error_idb',
        ),
      ),
    ),
    'lhs_module' => 'W_Weight',
    'lhs_table' => 'w_weight',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'w_weight_m06_error_2_c',
    'join_key_lhs' => 'w_weight_m06_error_2w_weight_ida',
    'join_key_rhs' => 'w_weight_m06_error_2m06_error_idb',
    'readonly' => true,
    'relationship_name' => 'w_weight_m06_error_2',
    'rhs_subpanel' => 'ForW_weightW_weight_m06_error_2',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm06_error_modified_user' => 
  array (
    'name' => 'm06_error_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm06_error_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm06_error_created_by' => 
  array (
    'name' => 'm06_error_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm06_error_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm06_error_activities' => 
  array (
    'name' => 'm06_error_activities',
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'M06_Error',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'm06_error_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm06_error_following' => 
  array (
    'name' => 'm06_error_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'M06_Error',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'm06_error_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm06_error_favorite' => 
  array (
    'name' => 'm06_error_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'M06_Error',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'm06_error_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm06_error_assigned_user' => 
  array (
    'name' => 'm06_error_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm06_error_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm06_error_de_deviation_employees_2' => 
  array (
    'rhs_label' => 'Responsible Personnel M2M',
    'lhs_label' => 'Communications',
    'lhs_subpanel' => 'default',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'M06_Error',
    'rhs_module' => 'DE_Deviation_Employees',
    'relationship_type' => 'many-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'm06_error_de_deviation_employees_2',
  ),
);