<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'm03_work_product_m03_work_product_deliverable_1' => 
  array (
    'name' => 'm03_work_product_m03_work_product_deliverable_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_m03_work_product_deliverable_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product_Deliverable',
        'rhs_table' => 'm03_work_product_deliverable',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_m03_work_product_deliverable_1_c',
        'join_key_lhs' => 'm03_work_p0b66product_ida',
        'join_key_rhs' => 'm03_work_pe584verable_idb',
      ),
    ),
    'table' => 'm03_work_product_m03_work_product_deliverable_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'm03_work_p0b66product_ida' => 
      array (
        'name' => 'm03_work_p0b66product_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'm03_work_pe584verable_idb' => 
      array (
        'name' => 'm03_work_pe584verable_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'm03_work_product_m03_work_product_deliverable_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'm03_work_product_m03_work_product_deliverable_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p0b66product_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'm03_work_product_m03_work_product_deliverable_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_pe584verable_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Deliverable',
    'rhs_table' => 'm03_work_product_deliverable',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_m03_work_product_deliverable_1_c',
    'join_key_lhs' => 'm03_work_p0b66product_ida',
    'join_key_rhs' => 'm03_work_pe584verable_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_m03_work_product_deliverable_1',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_m03_work_product_deliverable_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'tasks_m03_work_product_deliverable_1' => 
  array (
    'name' => 'tasks_m03_work_product_deliverable_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'tasks_m03_work_product_deliverable_1' => 
      array (
        'lhs_module' => 'Tasks',
        'lhs_table' => 'tasks',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product_Deliverable',
        'rhs_table' => 'm03_work_product_deliverable',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'tasks_m03_work_product_deliverable_1_c',
        'join_key_lhs' => 'tasks_m03_work_product_deliverable_1tasks_ida',
        'join_key_rhs' => 'tasks_m03_3e21verable_idb',
      ),
    ),
    'table' => 'tasks_m03_work_product_deliverable_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'tasks_m03_work_product_deliverable_1tasks_ida' => 
      array (
        'name' => 'tasks_m03_work_product_deliverable_1tasks_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'tasks_m03_3e21verable_idb' => 
      array (
        'name' => 'tasks_m03_3e21verable_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'tasks_m03_work_product_deliverable_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'tasks_m03_work_product_deliverable_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'tasks_m03_work_product_deliverable_1tasks_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'tasks_m03_work_product_deliverable_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'tasks_m03_3e21verable_idb',
        ),
      ),
    ),
    'lhs_module' => 'Tasks',
    'lhs_table' => 'tasks',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Deliverable',
    'rhs_table' => 'm03_work_product_deliverable',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'tasks_m03_work_product_deliverable_1_c',
    'join_key_lhs' => 'tasks_m03_work_product_deliverable_1tasks_ida',
    'join_key_rhs' => 'tasks_m03_3e21verable_idb',
    'readonly' => true,
    'relationship_name' => 'tasks_m03_work_product_deliverable_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_deliverable_tasks_1' => 
  array (
    'name' => 'm03_work_product_deliverable_tasks_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_deliverable_tasks_1' => 
      array (
        'lhs_module' => 'M03_Work_Product_Deliverable',
        'lhs_table' => 'm03_work_product_deliverable',
        'lhs_key' => 'id',
        'rhs_module' => 'Tasks',
        'rhs_table' => 'tasks',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_deliverable_tasks_1_c',
        'join_key_lhs' => 'm03_work_pbb72verable_ida',
        'join_key_rhs' => 'm03_work_product_deliverable_tasks_1tasks_idb',
      ),
    ),
    'table' => 'm03_work_product_deliverable_tasks_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'm03_work_pbb72verable_ida' => 
      array (
        'name' => 'm03_work_pbb72verable_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'm03_work_product_deliverable_tasks_1tasks_idb' => 
      array (
        'name' => 'm03_work_product_deliverable_tasks_1tasks_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'm03_work_product_deliverable_tasks_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'm03_work_product_deliverable_tasks_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_pbb72verable_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'm03_work_product_deliverable_tasks_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_deliverable_tasks_1tasks_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product_Deliverable',
    'lhs_table' => 'm03_work_product_deliverable',
    'lhs_key' => 'id',
    'rhs_module' => 'Tasks',
    'rhs_table' => 'tasks',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_deliverable_tasks_1_c',
    'join_key_lhs' => 'm03_work_pbb72verable_ida',
    'join_key_rhs' => 'm03_work_product_deliverable_tasks_1tasks_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_deliverable_tasks_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_deliverable_modified_user' => 
  array (
    'name' => 'm03_work_product_deliverable_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Deliverable',
    'rhs_table' => 'm03_work_product_deliverable',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_deliverable_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_deliverable_created_by' => 
  array (
    'name' => 'm03_work_product_deliverable_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Deliverable',
    'rhs_table' => 'm03_work_product_deliverable',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_deliverable_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_deliverable_activities' => 
  array (
    'name' => 'm03_work_product_deliverable_activities',
    'lhs_module' => 'M03_Work_Product_Deliverable',
    'lhs_table' => 'm03_work_product_deliverable',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'M03_Work_Product_Deliverable',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'm03_work_product_deliverable_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_deliverable_following' => 
  array (
    'name' => 'm03_work_product_deliverable_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Deliverable',
    'rhs_table' => 'm03_work_product_deliverable',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'M03_Work_Product_Deliverable',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_deliverable_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_deliverable_favorite' => 
  array (
    'name' => 'm03_work_product_deliverable_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Deliverable',
    'rhs_table' => 'm03_work_product_deliverable',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'M03_Work_Product_Deliverable',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_deliverable_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_deliverable_assigned_user' => 
  array (
    'name' => 'm03_work_product_deliverable_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Deliverable',
    'rhs_table' => 'm03_work_product_deliverable',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_deliverable_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_deliverable_ii_inventory_item_1' => 
  array (
    'name' => 'm03_work_product_deliverable_ii_inventory_item_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_deliverable_ii_inventory_item_1' => 
      array (
        'lhs_module' => 'M03_Work_Product_Deliverable',
        'lhs_table' => 'm03_work_product_deliverable',
        'lhs_key' => 'id',
        'rhs_module' => 'II_Inventory_Item',
        'rhs_table' => 'ii_inventory_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_deliverable_ii_inventory_item_1_c',
        'join_key_lhs' => 'm03_work_p7e14verable_ida',
        'join_key_rhs' => 'm03_work_p3c4fry_item_idb',
      ),
    ),
    'table' => 'm03_work_product_deliverable_ii_inventory_item_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_p7e14verable_ida' => 
      array (
        'name' => 'm03_work_p7e14verable_ida',
        'type' => 'id',
      ),
      'm03_work_p3c4fry_item_idb' => 
      array (
        'name' => 'm03_work_p3c4fry_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_deliverable_ii_inventory_item_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_deliverable_ii_inventory_item_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p7e14verable_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_deliverable_ii_inventory_item_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p3c4fry_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_deliverable_ii_inventory_item_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_p3c4fry_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product_Deliverable',
    'lhs_table' => 'm03_work_product_deliverable',
    'lhs_key' => 'id',
    'rhs_module' => 'II_Inventory_Item',
    'rhs_table' => 'ii_inventory_item',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_deliverable_ii_inventory_item_1_c',
    'join_key_lhs' => 'm03_work_p7e14verable_ida',
    'join_key_rhs' => 'm03_work_p3c4fry_item_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_deliverable_ii_inventory_item_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_deliverable_ic_inventory_collection_1' => 
  array (
    'rhs_label' => 'Inventory Collections',
    'lhs_label' => 'Work Product Deliverables',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'M03_Work_Product_Deliverable',
    'rhs_module' => 'IC_Inventory_Collection',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  ),
);