<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'm06_error_de_deviation_employees_1' => 
  array (
    'name' => 'm06_error_de_deviation_employees_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_de_deviation_employees_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'DE_Deviation_Employees',
        'rhs_table' => 'de_deviation_employees',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_de_deviation_employees_1_c',
        'join_key_lhs' => 'm06_error_de_deviation_employees_1m06_error_ida',
        'join_key_rhs' => 'm06_error_de_deviation_employees_1de_deviation_employees_idb',
      ),
    ),
    'table' => 'm06_error_de_deviation_employees_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_de_deviation_employees_1m06_error_ida' => 
      array (
        'name' => 'm06_error_de_deviation_employees_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_de_deviation_employees_1de_deviation_employees_idb' => 
      array (
        'name' => 'm06_error_de_deviation_employees_1de_deviation_employees_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_de_deviation_employees_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_de_deviation_employees_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_de_deviation_employees_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_de_deviation_employees_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_de_deviation_employees_1de_deviation_employees_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_de_deviation_employees_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_de_deviation_employees_1de_deviation_employees_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'DE_Deviation_Employees',
    'rhs_table' => 'de_deviation_employees',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm06_error_de_deviation_employees_1_c',
    'join_key_lhs' => 'm06_error_de_deviation_employees_1m06_error_ida',
    'join_key_rhs' => 'm06_error_de_deviation_employees_1de_deviation_employees_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_de_deviation_employees_1',
    'rhs_subpanel' => 'ForM06_errorM06_error_de_deviation_employees_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'contacts_de_deviation_employees_1' => 
  array (
    'name' => 'contacts_de_deviation_employees_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'contacts_de_deviation_employees_1' => 
      array (
        'lhs_module' => 'Contacts',
        'lhs_table' => 'contacts',
        'lhs_key' => 'id',
        'rhs_module' => 'DE_Deviation_Employees',
        'rhs_table' => 'de_deviation_employees',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'contacts_de_deviation_employees_1_c',
        'join_key_lhs' => 'contacts_de_deviation_employees_1contacts_ida',
        'join_key_rhs' => 'contacts_de_deviation_employees_1de_deviation_employees_idb',
      ),
    ),
    'table' => 'contacts_de_deviation_employees_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'contacts_de_deviation_employees_1contacts_ida' => 
      array (
        'name' => 'contacts_de_deviation_employees_1contacts_ida',
        'type' => 'id',
      ),
      'contacts_de_deviation_employees_1de_deviation_employees_idb' => 
      array (
        'name' => 'contacts_de_deviation_employees_1de_deviation_employees_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_contacts_de_deviation_employees_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_contacts_de_deviation_employees_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'contacts_de_deviation_employees_1contacts_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_contacts_de_deviation_employees_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'contacts_de_deviation_employees_1de_deviation_employees_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'contacts_de_deviation_employees_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'contacts_de_deviation_employees_1de_deviation_employees_idb',
        ),
      ),
    ),
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'DE_Deviation_Employees',
    'rhs_table' => 'de_deviation_employees',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'contacts_de_deviation_employees_1_c',
    'join_key_lhs' => 'contacts_de_deviation_employees_1contacts_ida',
    'join_key_rhs' => 'contacts_de_deviation_employees_1de_deviation_employees_idb',
    'readonly' => true,
    'relationship_name' => 'contacts_de_deviation_employees_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'de_deviation_employees_modified_user' => 
  array (
    'name' => 'de_deviation_employees_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'DE_Deviation_Employees',
    'rhs_table' => 'de_deviation_employees',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'de_deviation_employees_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'de_deviation_employees_created_by' => 
  array (
    'name' => 'de_deviation_employees_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'DE_Deviation_Employees',
    'rhs_table' => 'de_deviation_employees',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'de_deviation_employees_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'de_deviation_employees_activities' => 
  array (
    'name' => 'de_deviation_employees_activities',
    'lhs_module' => 'DE_Deviation_Employees',
    'lhs_table' => 'de_deviation_employees',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'DE_Deviation_Employees',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'de_deviation_employees_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'de_deviation_employees_following' => 
  array (
    'name' => 'de_deviation_employees_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'DE_Deviation_Employees',
    'rhs_table' => 'de_deviation_employees',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'DE_Deviation_Employees',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'de_deviation_employees_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'de_deviation_employees_favorite' => 
  array (
    'name' => 'de_deviation_employees_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'DE_Deviation_Employees',
    'rhs_table' => 'de_deviation_employees',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'DE_Deviation_Employees',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'de_deviation_employees_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'de_deviation_employees_assigned_user' => 
  array (
    'name' => 'de_deviation_employees_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'DE_Deviation_Employees',
    'rhs_table' => 'de_deviation_employees',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'de_deviation_employees_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
);