<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'm01_sales_m01_quote_document_1' => 
  array (
    'name' => 'm01_sales_m01_quote_document_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_sales_m01_quote_document_1' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'M01_Quote_Document',
        'rhs_table' => 'm01_quote_document',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_sales_m01_quote_document_1_c',
        'join_key_lhs' => 'm01_sales_m01_quote_document_1m01_sales_ida',
        'join_key_rhs' => 'm01_sales_m01_quote_document_1m01_quote_document_idb',
      ),
    ),
    'table' => 'm01_sales_m01_quote_document_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'm01_sales_m01_quote_document_1m01_sales_ida' => 
      array (
        'name' => 'm01_sales_m01_quote_document_1m01_sales_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'm01_sales_m01_quote_document_1m01_quote_document_idb' => 
      array (
        'name' => 'm01_sales_m01_quote_document_1m01_quote_document_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'm01_sales_m01_quote_document_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'm01_sales_m01_quote_document_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_m01_quote_document_1m01_sales_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'm01_sales_m01_quote_document_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_sales_m01_quote_document_1m01_quote_document_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Quote_Document',
    'rhs_table' => 'm01_quote_document',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm01_sales_m01_quote_document_1_c',
    'join_key_lhs' => 'm01_sales_m01_quote_document_1m01_sales_ida',
    'join_key_rhs' => 'm01_sales_m01_quote_document_1m01_quote_document_idb',
    'readonly' => true,
    'relationship_name' => 'm01_sales_m01_quote_document_1',
    'rhs_subpanel' => 'ForM01_salesM01_sales_m01_quote_document_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm01_sales_tasks_1' => 
  array (
    'name' => 'm01_sales_tasks_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_sales_tasks_1' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'Tasks',
        'rhs_table' => 'tasks',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_sales_tasks_1_c',
        'join_key_lhs' => 'm01_sales_tasks_1m01_sales_ida',
        'join_key_rhs' => 'm01_sales_tasks_1tasks_idb',
      ),
    ),
    'table' => 'm01_sales_tasks_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'm01_sales_tasks_1m01_sales_ida' => 
      array (
        'name' => 'm01_sales_tasks_1m01_sales_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'm01_sales_tasks_1tasks_idb' => 
      array (
        'name' => 'm01_sales_tasks_1tasks_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'm01_sales_tasks_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'm01_sales_tasks_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_tasks_1m01_sales_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'm01_sales_tasks_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_sales_tasks_1tasks_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'Tasks',
    'rhs_table' => 'tasks',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm01_sales_tasks_1_c',
    'join_key_lhs' => 'm01_sales_tasks_1m01_sales_ida',
    'join_key_rhs' => 'm01_sales_tasks_1tasks_idb',
    'readonly' => true,
    'relationship_name' => 'm01_sales_tasks_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm01_sales_m03_work_product_1' => 
  array (
    'name' => 'm01_sales_m03_work_product_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_sales_m03_work_product_1' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product',
        'rhs_table' => 'm03_work_product',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_sales_m03_work_product_1_c',
        'join_key_lhs' => 'm01_sales_m03_work_product_1m01_sales_ida',
        'join_key_rhs' => 'm01_sales_m03_work_product_1m03_work_product_idb',
      ),
    ),
    'table' => 'm01_sales_m03_work_product_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'm01_sales_m03_work_product_1m01_sales_ida' => 
      array (
        'name' => 'm01_sales_m03_work_product_1m01_sales_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'm01_sales_m03_work_product_1m03_work_product_idb' => 
      array (
        'name' => 'm01_sales_m03_work_product_1m03_work_product_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'm01_sales_m03_work_product_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'm01_sales_m03_work_product_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_m03_work_product_1m01_sales_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'm01_sales_m03_work_product_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_sales_m03_work_product_1m03_work_product_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm01_sales_m03_work_product_1_c',
    'join_key_lhs' => 'm01_sales_m03_work_product_1m01_sales_ida',
    'join_key_rhs' => 'm01_sales_m03_work_product_1m03_work_product_idb',
    'readonly' => true,
    'relationship_name' => 'm01_sales_m03_work_product_1',
    'rhs_subpanel' => 'ForM01_salesM01_sales_m03_work_product_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'accounts_m01_sales_1' => 
  array (
    'name' => 'accounts_m01_sales_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'accounts_m01_sales_1' => 
      array (
        'lhs_module' => 'Accounts',
        'lhs_table' => 'accounts',
        'lhs_key' => 'id',
        'rhs_module' => 'M01_Sales',
        'rhs_table' => 'm01_sales',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'accounts_m01_sales_1_c',
        'join_key_lhs' => 'accounts_m01_sales_1accounts_ida',
        'join_key_rhs' => 'accounts_m01_sales_1m01_sales_idb',
      ),
    ),
    'table' => 'accounts_m01_sales_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'accounts_m01_sales_1accounts_ida' => 
      array (
        'name' => 'accounts_m01_sales_1accounts_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'accounts_m01_sales_1m01_sales_idb' => 
      array (
        'name' => 'accounts_m01_sales_1m01_sales_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'accounts_m01_sales_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'accounts_m01_sales_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'accounts_m01_sales_1accounts_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'accounts_m01_sales_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'accounts_m01_sales_1m01_sales_idb',
        ),
      ),
    ),
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Sales',
    'rhs_table' => 'm01_sales',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'accounts_m01_sales_1_c',
    'join_key_lhs' => 'accounts_m01_sales_1accounts_ida',
    'join_key_rhs' => 'accounts_m01_sales_1m01_sales_idb',
    'readonly' => true,
    'relationship_name' => 'accounts_m01_sales_1',
    'rhs_subpanel' => 'ForAccountsAccounts_m01_sales_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'tm_tradeshow_management_m01_sales_1' => 
  array (
    'name' => 'tm_tradeshow_management_m01_sales_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'tm_tradeshow_management_m01_sales_1' => 
      array (
        'lhs_module' => 'TM_Tradeshow_Management',
        'lhs_table' => 'tm_tradeshow_management',
        'lhs_key' => 'id',
        'rhs_module' => 'M01_Sales',
        'rhs_table' => 'm01_sales',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'tm_tradeshow_management_m01_sales_1_c',
        'join_key_lhs' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
        'join_key_rhs' => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
      ),
    ),
    'table' => 'tm_tradeshow_management_m01_sales_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida' => 
      array (
        'name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
        'type' => 'id',
      ),
      'tm_tradeshow_management_m01_sales_1m01_sales_idb' => 
      array (
        'name' => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_tm_tradeshow_management_m01_sales_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_tm_tradeshow_management_m01_sales_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_tm_tradeshow_management_m01_sales_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'tm_tradeshow_management_m01_sales_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
        ),
      ),
    ),
    'lhs_module' => 'TM_Tradeshow_Management',
    'lhs_table' => 'tm_tradeshow_management',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Sales',
    'rhs_table' => 'm01_sales',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'tm_tradeshow_management_m01_sales_1_c',
    'join_key_lhs' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
    'join_key_rhs' => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
    'readonly' => true,
    'relationship_name' => 'tm_tradeshow_management_m01_sales_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm01_sales_activities_1_calls' => 
  array (
    'name' => 'm01_sales_activities_1_calls',
    'relationships' => 
    array (
      'm01_sales_activities_1_calls' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'Calls',
        'rhs_table' => 'calls',
        'relationship_role_column_value' => 'M01_Sales',
        'rhs_key' => 'parent_id',
        'relationship_type' => 'one-to-many',
        'relationship_role_column' => 'parent_type',
      ),
    ),
    'fields' => '',
    'indices' => '',
    'table' => '',
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'Calls',
    'rhs_table' => 'calls',
    'relationship_role_column_value' => 'M01_Sales',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'readonly' => true,
    'relationship_name' => 'm01_sales_activities_1_calls',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'm01_sales_activities_1_meetings' => 
  array (
    'name' => 'm01_sales_activities_1_meetings',
    'relationships' => 
    array (
      'm01_sales_activities_1_meetings' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'Meetings',
        'rhs_table' => 'meetings',
        'relationship_role_column_value' => 'M01_Sales',
        'rhs_key' => 'parent_id',
        'relationship_type' => 'one-to-many',
        'relationship_role_column' => 'parent_type',
      ),
    ),
    'fields' => '',
    'indices' => '',
    'table' => '',
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'relationship_role_column_value' => 'M01_Sales',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'readonly' => true,
    'relationship_name' => 'm01_sales_activities_1_meetings',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'm01_sales_activities_1_notes' => 
  array (
    'name' => 'm01_sales_activities_1_notes',
    'relationships' => 
    array (
      'm01_sales_activities_1_notes' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'Notes',
        'rhs_table' => 'notes',
        'relationship_role_column_value' => 'M01_Sales',
        'rhs_key' => 'parent_id',
        'relationship_type' => 'one-to-many',
        'relationship_role_column' => 'parent_type',
      ),
    ),
    'fields' => '',
    'indices' => '',
    'table' => '',
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'Notes',
    'rhs_table' => 'notes',
    'relationship_role_column_value' => 'M01_Sales',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'readonly' => true,
    'relationship_name' => 'm01_sales_activities_1_notes',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'm01_sales_activities_1_tasks' => 
  array (
    'name' => 'm01_sales_activities_1_tasks',
    'relationships' => 
    array (
      'm01_sales_activities_1_tasks' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'Tasks',
        'rhs_table' => 'tasks',
        'relationship_role_column_value' => 'M01_Sales',
        'rhs_key' => 'parent_id',
        'relationship_type' => 'one-to-many',
        'relationship_role_column' => 'parent_type',
      ),
    ),
    'fields' => '',
    'indices' => '',
    'table' => '',
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'Tasks',
    'rhs_table' => 'tasks',
    'relationship_role_column_value' => 'M01_Sales',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'readonly' => true,
    'relationship_name' => 'm01_sales_activities_1_tasks',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'contacts_m01_sales_1' => 
  array (
    'name' => 'contacts_m01_sales_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'contacts_m01_sales_1' => 
      array (
        'lhs_module' => 'Contacts',
        'lhs_table' => 'contacts',
        'lhs_key' => 'id',
        'rhs_module' => 'M01_Sales',
        'rhs_table' => 'm01_sales',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'contacts_m01_sales_1_c',
        'join_key_lhs' => 'contacts_m01_sales_1contacts_ida',
        'join_key_rhs' => 'contacts_m01_sales_1m01_sales_idb',
      ),
    ),
    'table' => 'contacts_m01_sales_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'contacts_m01_sales_1contacts_ida' => 
      array (
        'name' => 'contacts_m01_sales_1contacts_ida',
        'type' => 'id',
      ),
      'contacts_m01_sales_1m01_sales_idb' => 
      array (
        'name' => 'contacts_m01_sales_1m01_sales_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_contacts_m01_sales_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_contacts_m01_sales_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'contacts_m01_sales_1contacts_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_contacts_m01_sales_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'contacts_m01_sales_1m01_sales_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'contacts_m01_sales_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'contacts_m01_sales_1m01_sales_idb',
        ),
      ),
    ),
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Sales',
    'rhs_table' => 'm01_sales',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'contacts_m01_sales_1_c',
    'join_key_lhs' => 'contacts_m01_sales_1contacts_ida',
    'join_key_rhs' => 'contacts_m01_sales_1m01_sales_idb',
    'readonly' => true,
    'relationship_name' => 'contacts_m01_sales_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'rr_regulatory_response_m01_sales_1' => 
  array (
    'name' => 'rr_regulatory_response_m01_sales_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'rr_regulatory_response_m01_sales_1' => 
      array (
        'lhs_module' => 'RR_Regulatory_Response',
        'lhs_table' => 'rr_regulatory_response',
        'lhs_key' => 'id',
        'rhs_module' => 'M01_Sales',
        'rhs_table' => 'm01_sales',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'rr_regulatory_response_m01_sales_1_c',
        'join_key_lhs' => 'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida',
        'join_key_rhs' => 'rr_regulatory_response_m01_sales_1m01_sales_idb',
      ),
    ),
    'table' => 'rr_regulatory_response_m01_sales_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida' => 
      array (
        'name' => 'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida',
        'type' => 'id',
      ),
      'rr_regulatory_response_m01_sales_1m01_sales_idb' => 
      array (
        'name' => 'rr_regulatory_response_m01_sales_1m01_sales_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_rr_regulatory_response_m01_sales_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_rr_regulatory_response_m01_sales_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_rr_regulatory_response_m01_sales_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'rr_regulatory_response_m01_sales_1m01_sales_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'rr_regulatory_response_m01_sales_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida',
          1 => 'rr_regulatory_response_m01_sales_1m01_sales_idb',
        ),
      ),
    ),
    'lhs_module' => 'RR_Regulatory_Response',
    'lhs_table' => 'rr_regulatory_response',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Sales',
    'rhs_table' => 'm01_sales',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'rr_regulatory_response_m01_sales_1_c',
    'join_key_lhs' => 'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida',
    'join_key_rhs' => 'rr_regulatory_response_m01_sales_1m01_sales_idb',
    'readonly' => true,
    'relationship_name' => 'rr_regulatory_response_m01_sales_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm01_sales_edoc_email_documents_1' => 
  array (
    'name' => 'm01_sales_edoc_email_documents_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_sales_edoc_email_documents_1' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'EDoc_Email_Documents',
        'rhs_table' => 'edoc_email_documents',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_sales_edoc_email_documents_1_c',
        'join_key_lhs' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
        'join_key_rhs' => 'm01_sales_edoc_email_documents_1edoc_email_documents_idb',
      ),
    ),
    'table' => 'm01_sales_edoc_email_documents_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm01_sales_edoc_email_documents_1m01_sales_ida' => 
      array (
        'name' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
        'type' => 'id',
      ),
      'm01_sales_edoc_email_documents_1edoc_email_documents_idb' => 
      array (
        'name' => 'm01_sales_edoc_email_documents_1edoc_email_documents_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m01_sales_edoc_email_documents_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m01_sales_edoc_email_documents_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_edoc_email_documents_1m01_sales_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m01_sales_edoc_email_documents_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_edoc_email_documents_1edoc_email_documents_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm01_sales_edoc_email_documents_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_sales_edoc_email_documents_1edoc_email_documents_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'EDoc_Email_Documents',
    'rhs_table' => 'edoc_email_documents',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm01_sales_edoc_email_documents_1_c',
    'join_key_lhs' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
    'join_key_rhs' => 'm01_sales_edoc_email_documents_1edoc_email_documents_idb',
    'readonly' => true,
    'relationship_name' => 'm01_sales_edoc_email_documents_1',
    'rhs_subpanel' => 'ForM01_salesM01_sales_edoc_email_documents_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm01_sales_m01_sales_1' => 
  array (
    'name' => 'm01_sales_m01_sales_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_sales_m01_sales_1' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'M01_Sales',
        'rhs_table' => 'm01_sales',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_sales_m01_sales_1_c',
        'join_key_lhs' => 'm01_sales_m01_sales_1m01_sales_ida',
        'join_key_rhs' => 'm01_sales_m01_sales_1m01_sales_idb',
      ),
    ),
    'table' => 'm01_sales_m01_sales_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm01_sales_m01_sales_1m01_sales_ida' => 
      array (
        'name' => 'm01_sales_m01_sales_1m01_sales_ida',
        'type' => 'id',
      ),
      'm01_sales_m01_sales_1m01_sales_idb' => 
      array (
        'name' => 'm01_sales_m01_sales_1m01_sales_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m01_sales_m01_sales_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m01_sales_m01_sales_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_m01_sales_1m01_sales_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m01_sales_m01_sales_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_m01_sales_1m01_sales_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm01_sales_m01_sales_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_sales_m01_sales_1m01_sales_ida',
          1 => 'm01_sales_m01_sales_1m01_sales_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Sales',
    'rhs_table' => 'm01_sales',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm01_sales_m01_sales_1_c',
    'join_key_lhs' => 'm01_sales_m01_sales_1m01_sales_ida',
    'join_key_rhs' => 'm01_sales_m01_sales_1m01_sales_idb',
    'readonly' => true,
    'relationship_name' => 'm01_sales_m01_sales_1',
    'rhs_subpanel' => 'ForM01_salesM01_sales_m01_sales_1',
    'lhs_subpanel' => 'ForM01_salesM01_sales_m01_sales_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm01_sales_ori_order_request_item_1' => 
  array (
    'name' => 'm01_sales_ori_order_request_item_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_sales_ori_order_request_item_1' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'ORI_Order_Request_Item',
        'rhs_table' => 'ori_order_request_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_sales_ori_order_request_item_1_c',
        'join_key_lhs' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
        'join_key_rhs' => 'm01_sales_ori_order_request_item_1ori_order_request_item_idb',
      ),
    ),
    'table' => 'm01_sales_ori_order_request_item_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm01_sales_ori_order_request_item_1m01_sales_ida' => 
      array (
        'name' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
        'type' => 'id',
      ),
      'm01_sales_ori_order_request_item_1ori_order_request_item_idb' => 
      array (
        'name' => 'm01_sales_ori_order_request_item_1ori_order_request_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m01_sales_ori_order_request_item_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m01_sales_ori_order_request_item_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_ori_order_request_item_1m01_sales_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m01_sales_ori_order_request_item_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_ori_order_request_item_1ori_order_request_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm01_sales_ori_order_request_item_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_sales_ori_order_request_item_1ori_order_request_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'ORI_Order_Request_Item',
    'rhs_table' => 'ori_order_request_item',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm01_sales_ori_order_request_item_1_c',
    'join_key_lhs' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
    'join_key_rhs' => 'm01_sales_ori_order_request_item_1ori_order_request_item_idb',
    'readonly' => true,
    'relationship_name' => 'm01_sales_ori_order_request_item_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm01_sales_poi_purchase_order_item_1' => 
  array (
    'name' => 'm01_sales_poi_purchase_order_item_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_sales_poi_purchase_order_item_1' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'POI_Purchase_Order_Item',
        'rhs_table' => 'poi_purchase_order_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_sales_poi_purchase_order_item_1_c',
        'join_key_lhs' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
        'join_key_rhs' => 'm01_sales_poi_purchase_order_item_1poi_purchase_order_item_idb',
      ),
    ),
    'table' => 'm01_sales_poi_purchase_order_item_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm01_sales_poi_purchase_order_item_1m01_sales_ida' => 
      array (
        'name' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
        'type' => 'id',
      ),
      'm01_sales_poi_purchase_order_item_1poi_purchase_order_item_idb' => 
      array (
        'name' => 'm01_sales_poi_purchase_order_item_1poi_purchase_order_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m01_sales_poi_purchase_order_item_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m01_sales_poi_purchase_order_item_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m01_sales_poi_purchase_order_item_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_poi_purchase_order_item_1poi_purchase_order_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm01_sales_poi_purchase_order_item_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_sales_poi_purchase_order_item_1poi_purchase_order_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'POI_Purchase_Order_Item',
    'rhs_table' => 'poi_purchase_order_item',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm01_sales_poi_purchase_order_item_1_c',
    'join_key_lhs' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
    'join_key_rhs' => 'm01_sales_poi_purchase_order_item_1poi_purchase_order_item_idb',
    'readonly' => true,
    'relationship_name' => 'm01_sales_poi_purchase_order_item_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm01_sales_ii_inventory_item_1' => 
  array (
    'name' => 'm01_sales_ii_inventory_item_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_sales_ii_inventory_item_1' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'II_Inventory_Item',
        'rhs_table' => 'ii_inventory_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_sales_ii_inventory_item_1_c',
        'join_key_lhs' => 'm01_sales_ii_inventory_item_1m01_sales_ida',
        'join_key_rhs' => 'm01_sales_ii_inventory_item_1ii_inventory_item_idb',
      ),
    ),
    'table' => 'm01_sales_ii_inventory_item_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm01_sales_ii_inventory_item_1m01_sales_ida' => 
      array (
        'name' => 'm01_sales_ii_inventory_item_1m01_sales_ida',
        'type' => 'id',
      ),
      'm01_sales_ii_inventory_item_1ii_inventory_item_idb' => 
      array (
        'name' => 'm01_sales_ii_inventory_item_1ii_inventory_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m01_sales_ii_inventory_item_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m01_sales_ii_inventory_item_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_ii_inventory_item_1m01_sales_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m01_sales_ii_inventory_item_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_ii_inventory_item_1ii_inventory_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm01_sales_ii_inventory_item_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_sales_ii_inventory_item_1ii_inventory_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'II_Inventory_Item',
    'rhs_table' => 'ii_inventory_item',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm01_sales_ii_inventory_item_1_c',
    'join_key_lhs' => 'm01_sales_ii_inventory_item_1m01_sales_ida',
    'join_key_rhs' => 'm01_sales_ii_inventory_item_1ii_inventory_item_idb',
    'readonly' => true,
    'relationship_name' => 'm01_sales_ii_inventory_item_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm01_sales_modified_user' => 
  array (
    'name' => 'm01_sales_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Sales',
    'rhs_table' => 'm01_sales',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm01_sales_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm01_sales_created_by' => 
  array (
    'name' => 'm01_sales_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Sales',
    'rhs_table' => 'm01_sales',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm01_sales_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm01_sales_activities' => 
  array (
    'name' => 'm01_sales_activities',
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'M01_Sales',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'm01_sales_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm01_sales_following' => 
  array (
    'name' => 'm01_sales_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Sales',
    'rhs_table' => 'm01_sales',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'M01_Sales',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'm01_sales_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm01_sales_favorite' => 
  array (
    'name' => 'm01_sales_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Sales',
    'rhs_table' => 'm01_sales',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'M01_Sales',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'm01_sales_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm01_sales_assigned_user' => 
  array (
    'name' => 'm01_sales_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Sales',
    'rhs_table' => 'm01_sales',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm01_sales_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm01_sales_im_inventory_management_1' => 
  array (
    'rhs_label' => 'Inventory Management',
    'lhs_label' => 'Sales',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'M01_Sales',
    'rhs_module' => 'IM_Inventory_Management',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'm01_sales_im_inventory_management_1',
  ),
);