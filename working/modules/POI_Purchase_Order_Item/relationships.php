<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'poi_purchase_order_item_ri_received_items_1' => 
  array (
    'name' => 'poi_purchase_order_item_ri_received_items_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'poi_purchase_order_item_ri_received_items_1' => 
      array (
        'lhs_module' => 'POI_Purchase_Order_Item',
        'lhs_table' => 'poi_purchase_order_item',
        'lhs_key' => 'id',
        'rhs_module' => 'RI_Received_Items',
        'rhs_table' => 'ri_received_items',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'poi_purchase_order_item_ri_received_items_1_c',
        'join_key_lhs' => 'poi_purcha665cer_item_ida',
        'join_key_rhs' => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
      ),
    ),
    'table' => 'poi_purchase_order_item_ri_received_items_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'poi_purcha665cer_item_ida' => 
      array (
        'name' => 'poi_purcha665cer_item_ida',
        'type' => 'id',
      ),
      'poi_purchase_order_item_ri_received_items_1ri_received_items_idb' => 
      array (
        'name' => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_poi_purchase_order_item_ri_received_items_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_poi_purchase_order_item_ri_received_items_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'poi_purcha665cer_item_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_poi_purchase_order_item_ri_received_items_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'poi_purchase_order_item_ri_received_items_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
        ),
      ),
    ),
    'lhs_module' => 'POI_Purchase_Order_Item',
    'lhs_table' => 'poi_purchase_order_item',
    'lhs_key' => 'id',
    'rhs_module' => 'RI_Received_Items',
    'rhs_table' => 'ri_received_items',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'poi_purchase_order_item_ri_received_items_1_c',
    'join_key_lhs' => 'poi_purcha665cer_item_ida',
    'join_key_rhs' => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
    'readonly' => true,
    'relationship_name' => 'poi_purchase_order_item_ri_received_items_1',
    'rhs_subpanel' => 'ForPoi_purchase_order_itemPoi_purchase_order_item_ri_received_items_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'po_purchase_order_poi_purchase_order_item_1' => 
  array (
    'name' => 'po_purchase_order_poi_purchase_order_item_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'po_purchase_order_poi_purchase_order_item_1' => 
      array (
        'lhs_module' => 'PO_Purchase_Order',
        'lhs_table' => 'po_purchase_order',
        'lhs_key' => 'id',
        'rhs_module' => 'POI_Purchase_Order_Item',
        'rhs_table' => 'poi_purchase_order_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'po_purchase_order_poi_purchase_order_item_1_c',
        'join_key_lhs' => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
        'join_key_rhs' => 'po_purchas4ee6er_item_idb',
      ),
    ),
    'table' => 'po_purchase_order_poi_purchase_order_item_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida' => 
      array (
        'name' => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
        'type' => 'id',
      ),
      'po_purchas4ee6er_item_idb' => 
      array (
        'name' => 'po_purchas4ee6er_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_po_purchase_order_poi_purchase_order_item_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_po_purchase_order_poi_purchase_order_item_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_po_purchase_order_poi_purchase_order_item_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'po_purchas4ee6er_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'po_purchase_order_poi_purchase_order_item_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'po_purchas4ee6er_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'PO_Purchase_Order',
    'lhs_table' => 'po_purchase_order',
    'lhs_key' => 'id',
    'rhs_module' => 'POI_Purchase_Order_Item',
    'rhs_table' => 'poi_purchase_order_item',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'po_purchase_order_poi_purchase_order_item_1_c',
    'join_key_lhs' => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
    'join_key_rhs' => 'po_purchas4ee6er_item_idb',
    'readonly' => true,
    'relationship_name' => 'po_purchase_order_poi_purchase_order_item_1',
    'rhs_subpanel' => 'ForPo_purchase_orderPo_purchase_order_poi_purchase_order_item_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm01_sales_poi_purchase_order_item_1' => 
  array (
    'name' => 'm01_sales_poi_purchase_order_item_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_sales_poi_purchase_order_item_1' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'POI_Purchase_Order_Item',
        'rhs_table' => 'poi_purchase_order_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_sales_poi_purchase_order_item_1_c',
        'join_key_lhs' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
        'join_key_rhs' => 'm01_sales_poi_purchase_order_item_1poi_purchase_order_item_idb',
      ),
    ),
    'table' => 'm01_sales_poi_purchase_order_item_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm01_sales_poi_purchase_order_item_1m01_sales_ida' => 
      array (
        'name' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
        'type' => 'id',
      ),
      'm01_sales_poi_purchase_order_item_1poi_purchase_order_item_idb' => 
      array (
        'name' => 'm01_sales_poi_purchase_order_item_1poi_purchase_order_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m01_sales_poi_purchase_order_item_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m01_sales_poi_purchase_order_item_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m01_sales_poi_purchase_order_item_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_poi_purchase_order_item_1poi_purchase_order_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm01_sales_poi_purchase_order_item_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_sales_poi_purchase_order_item_1poi_purchase_order_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'POI_Purchase_Order_Item',
    'rhs_table' => 'poi_purchase_order_item',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm01_sales_poi_purchase_order_item_1_c',
    'join_key_lhs' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
    'join_key_rhs' => 'm01_sales_poi_purchase_order_item_1poi_purchase_order_item_idb',
    'readonly' => true,
    'relationship_name' => 'm01_sales_poi_purchase_order_item_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_poi_purchase_order_item_1' => 
  array (
    'name' => 'm03_work_product_poi_purchase_order_item_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_poi_purchase_order_item_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'POI_Purchase_Order_Item',
        'rhs_table' => 'poi_purchase_order_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_poi_purchase_order_item_1_c',
        'join_key_lhs' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_p0605er_item_idb',
      ),
    ),
    'table' => 'm03_work_product_poi_purchase_order_item_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_poi_purchase_order_item_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_p0605er_item_idb' => 
      array (
        'name' => 'm03_work_p0605er_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_poi_purchase_order_item_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_poi_purchase_order_item_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_poi_purchase_order_item_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p0605er_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_poi_purchase_order_item_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_p0605er_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'POI_Purchase_Order_Item',
    'rhs_table' => 'poi_purchase_order_item',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_poi_purchase_order_item_1_c',
    'join_key_lhs' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_p0605er_item_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_poi_purchase_order_item_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'prod_product_poi_purchase_order_item_1' => 
  array (
    'name' => 'prod_product_poi_purchase_order_item_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'prod_product_poi_purchase_order_item_1' => 
      array (
        'lhs_module' => 'Prod_Product',
        'lhs_table' => 'prod_product',
        'lhs_key' => 'id',
        'rhs_module' => 'POI_Purchase_Order_Item',
        'rhs_table' => 'poi_purchase_order_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'prod_product_poi_purchase_order_item_1_c',
        'join_key_lhs' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
        'join_key_rhs' => 'prod_produf05ber_item_idb',
      ),
    ),
    'table' => 'prod_product_poi_purchase_order_item_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'prod_product_poi_purchase_order_item_1prod_product_ida' => 
      array (
        'name' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
        'type' => 'id',
      ),
      'prod_produf05ber_item_idb' => 
      array (
        'name' => 'prod_produf05ber_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_prod_product_poi_purchase_order_item_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_prod_product_poi_purchase_order_item_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'prod_product_poi_purchase_order_item_1prod_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_prod_product_poi_purchase_order_item_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'prod_produf05ber_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'prod_product_poi_purchase_order_item_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'prod_produf05ber_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'Prod_Product',
    'lhs_table' => 'prod_product',
    'lhs_key' => 'id',
    'rhs_module' => 'POI_Purchase_Order_Item',
    'rhs_table' => 'poi_purchase_order_item',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'prod_product_poi_purchase_order_item_1_c',
    'join_key_lhs' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
    'join_key_rhs' => 'prod_produf05ber_item_idb',
    'readonly' => true,
    'relationship_name' => 'prod_product_poi_purchase_order_item_1',
    'rhs_subpanel' => 'ForProd_productProd_product_poi_purchase_order_item_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'poi_purchase_order_item_modified_user' => 
  array (
    'name' => 'poi_purchase_order_item_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'POI_Purchase_Order_Item',
    'rhs_table' => 'poi_purchase_order_item',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'poi_purchase_order_item_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'poi_purchase_order_item_created_by' => 
  array (
    'name' => 'poi_purchase_order_item_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'POI_Purchase_Order_Item',
    'rhs_table' => 'poi_purchase_order_item',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'poi_purchase_order_item_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'poi_purchase_order_item_activities' => 
  array (
    'name' => 'poi_purchase_order_item_activities',
    'lhs_module' => 'POI_Purchase_Order_Item',
    'lhs_table' => 'poi_purchase_order_item',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'POI_Purchase_Order_Item',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'poi_purchase_order_item_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'poi_purchase_order_item_following' => 
  array (
    'name' => 'poi_purchase_order_item_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'POI_Purchase_Order_Item',
    'rhs_table' => 'poi_purchase_order_item',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'POI_Purchase_Order_Item',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'poi_purchase_order_item_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'poi_purchase_order_item_favorite' => 
  array (
    'name' => 'poi_purchase_order_item_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'POI_Purchase_Order_Item',
    'rhs_table' => 'poi_purchase_order_item',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'POI_Purchase_Order_Item',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'poi_purchase_order_item_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'poi_purchase_order_item_assigned_user' => 
  array (
    'name' => 'poi_purchase_order_item_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'POI_Purchase_Order_Item',
    'rhs_table' => 'poi_purchase_order_item',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'poi_purchase_order_item_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'poi_purchase_order_item_ii_inventory_item_1' => 
  array (
    'rhs_label' => 'Inventory Items',
    'lhs_label' => 'Purchase Order Items',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'POI_Purchase_Order_Item',
    'rhs_module' => 'II_Inventory_Item',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'poi_purchase_order_item_ii_inventory_item_1',
  ),
);