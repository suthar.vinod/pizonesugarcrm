<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'taskd_task_design_modified_user' => 
  array (
    'name' => 'taskd_task_design_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'TaskD_Task_Design',
    'rhs_table' => 'taskd_task_design',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'taskd_task_design_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'taskd_task_design_created_by' => 
  array (
    'name' => 'taskd_task_design_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'TaskD_Task_Design',
    'rhs_table' => 'taskd_task_design',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'taskd_task_design_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'taskd_task_design_activities' => 
  array (
    'name' => 'taskd_task_design_activities',
    'lhs_module' => 'TaskD_Task_Design',
    'lhs_table' => 'taskd_task_design',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'TaskD_Task_Design',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'taskd_task_design_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'taskd_task_design_following' => 
  array (
    'name' => 'taskd_task_design_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'TaskD_Task_Design',
    'rhs_table' => 'taskd_task_design',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'TaskD_Task_Design',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'taskd_task_design_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'taskd_task_design_favorite' => 
  array (
    'name' => 'taskd_task_design_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'TaskD_Task_Design',
    'rhs_table' => 'taskd_task_design',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'TaskD_Task_Design',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'taskd_task_design_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'taskd_task_design_assigned_user' => 
  array (
    'name' => 'taskd_task_design_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'TaskD_Task_Design',
    'rhs_table' => 'taskd_task_design',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'taskd_task_design_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'gd_group_design_taskd_task_design_1' => 
  array (
    'name' => 'gd_group_design_taskd_task_design_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'gd_group_design_taskd_task_design_1' => 
      array (
        'lhs_module' => 'GD_Group_Design',
        'lhs_table' => 'gd_group_design',
        'lhs_key' => 'id',
        'rhs_module' => 'TaskD_Task_Design',
        'rhs_table' => 'taskd_task_design',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'gd_group_design_taskd_task_design_1_c',
        'join_key_lhs' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
        'join_key_rhs' => 'gd_group_design_taskd_task_design_1taskd_task_design_idb',
      ),
    ),
    'table' => 'gd_group_design_taskd_task_design_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'gd_group_design_taskd_task_design_1gd_group_design_ida' => 
      array (
        'name' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
        'type' => 'id',
      ),
      'gd_group_design_taskd_task_design_1taskd_task_design_idb' => 
      array (
        'name' => 'gd_group_design_taskd_task_design_1taskd_task_design_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_gd_group_design_taskd_task_design_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_gd_group_design_taskd_task_design_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_gd_group_design_taskd_task_design_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'gd_group_design_taskd_task_design_1taskd_task_design_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'gd_group_design_taskd_task_design_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'gd_group_design_taskd_task_design_1taskd_task_design_idb',
        ),
      ),
    ),
    'lhs_module' => 'GD_Group_Design',
    'lhs_table' => 'gd_group_design',
    'lhs_key' => 'id',
    'rhs_module' => 'TaskD_Task_Design',
    'rhs_table' => 'taskd_task_design',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'gd_group_design_taskd_task_design_1_c',
    'join_key_lhs' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
    'join_key_rhs' => 'gd_group_design_taskd_task_design_1taskd_task_design_idb',
    'readonly' => true,
    'relationship_name' => 'gd_group_design_taskd_task_design_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_taskd_task_design_1' => 
  array (
    'name' => 'm03_work_product_taskd_task_design_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_taskd_task_design_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'TaskD_Task_Design',
        'rhs_table' => 'taskd_task_design',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_taskd_task_design_1_c',
        'join_key_lhs' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_product_taskd_task_design_1taskd_task_design_idb',
      ),
    ),
    'table' => 'm03_work_product_taskd_task_design_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_taskd_task_design_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_product_taskd_task_design_1taskd_task_design_idb' => 
      array (
        'name' => 'm03_work_product_taskd_task_design_1taskd_task_design_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_taskd_task_design_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_taskd_task_design_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_taskd_task_design_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_taskd_task_design_1taskd_task_design_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_taskd_task_design_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_taskd_task_design_1taskd_task_design_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'TaskD_Task_Design',
    'rhs_table' => 'taskd_task_design',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_taskd_task_design_1_c',
    'join_key_lhs' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_product_taskd_task_design_1taskd_task_design_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_taskd_task_design_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'anml_animals_taskd_task_design_1' => 
  array (
    'name' => 'anml_animals_taskd_task_design_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'anml_animals_taskd_task_design_1' => 
      array (
        'lhs_module' => 'ANML_Animals',
        'lhs_table' => 'anml_animals',
        'lhs_key' => 'id',
        'rhs_module' => 'TaskD_Task_Design',
        'rhs_table' => 'taskd_task_design',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'anml_animals_taskd_task_design_1_c',
        'join_key_lhs' => 'anml_animals_taskd_task_design_1anml_animals_ida',
        'join_key_rhs' => 'anml_animals_taskd_task_design_1taskd_task_design_idb',
      ),
    ),
    'table' => 'anml_animals_taskd_task_design_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'anml_animals_taskd_task_design_1anml_animals_ida' => 
      array (
        'name' => 'anml_animals_taskd_task_design_1anml_animals_ida',
        'type' => 'id',
      ),
      'anml_animals_taskd_task_design_1taskd_task_design_idb' => 
      array (
        'name' => 'anml_animals_taskd_task_design_1taskd_task_design_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_anml_animals_taskd_task_design_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_anml_animals_taskd_task_design_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_taskd_task_design_1anml_animals_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_anml_animals_taskd_task_design_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_taskd_task_design_1taskd_task_design_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'anml_animals_taskd_task_design_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'anml_animals_taskd_task_design_1taskd_task_design_idb',
        ),
      ),
    ),
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'TaskD_Task_Design',
    'rhs_table' => 'taskd_task_design',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'anml_animals_taskd_task_design_1_c',
    'join_key_lhs' => 'anml_animals_taskd_task_design_1anml_animals_ida',
    'join_key_rhs' => 'anml_animals_taskd_task_design_1taskd_task_design_idb',
    'readonly' => true,
    'relationship_name' => 'anml_animals_taskd_task_design_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'taskd_task_design_taskd_task_design_1' => 
  array (
    'rhs_label' => 'Task Designs',
    'lhs_label' => 'Task Designs',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'TaskD_Task_Design',
    'rhs_module' => 'TaskD_Task_Design',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'taskd_task_design_taskd_task_design_1',
  ),
);