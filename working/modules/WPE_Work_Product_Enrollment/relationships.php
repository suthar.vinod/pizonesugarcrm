<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'wpe_work_product_enrollment_modified_user' => 
  array (
    'name' => 'wpe_work_product_enrollment_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'WPE_Work_Product_Enrollment',
    'rhs_table' => 'wpe_work_product_enrollment',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'wpe_work_product_enrollment_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'wpe_work_product_enrollment_created_by' => 
  array (
    'name' => 'wpe_work_product_enrollment_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'WPE_Work_Product_Enrollment',
    'rhs_table' => 'wpe_work_product_enrollment',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'wpe_work_product_enrollment_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'wpe_work_product_enrollment_activities' => 
  array (
    'name' => 'wpe_work_product_enrollment_activities',
    'lhs_module' => 'WPE_Work_Product_Enrollment',
    'lhs_table' => 'wpe_work_product_enrollment',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'WPE_Work_Product_Enrollment',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'wpe_work_product_enrollment_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'wpe_work_product_enrollment_following' => 
  array (
    'name' => 'wpe_work_product_enrollment_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'WPE_Work_Product_Enrollment',
    'rhs_table' => 'wpe_work_product_enrollment',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'WPE_Work_Product_Enrollment',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'wpe_work_product_enrollment_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'wpe_work_product_enrollment_favorite' => 
  array (
    'name' => 'wpe_work_product_enrollment_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'WPE_Work_Product_Enrollment',
    'rhs_table' => 'wpe_work_product_enrollment',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'WPE_Work_Product_Enrollment',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'wpe_work_product_enrollment_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'wpe_work_product_enrollment_assigned_user' => 
  array (
    'name' => 'wpe_work_product_enrollment_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'WPE_Work_Product_Enrollment',
    'rhs_table' => 'wpe_work_product_enrollment',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'wpe_work_product_enrollment_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'anml_animals_wpe_work_product_enrollment_1' => 
  array (
    'name' => 'anml_animals_wpe_work_product_enrollment_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'anml_animals_wpe_work_product_enrollment_1' => 
      array (
        'lhs_module' => 'ANML_Animals',
        'lhs_table' => 'anml_animals',
        'lhs_key' => 'id',
        'rhs_module' => 'WPE_Work_Product_Enrollment',
        'rhs_table' => 'wpe_work_product_enrollment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'anml_animals_wpe_work_product_enrollment_1_c',
        'join_key_lhs' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
        'join_key_rhs' => 'anml_anima9941ollment_idb',
      ),
    ),
    'table' => 'anml_animals_wpe_work_product_enrollment_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'anml_animals_wpe_work_product_enrollment_1anml_animals_ida' => 
      array (
        'name' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
        'type' => 'id',
      ),
      'anml_anima9941ollment_idb' => 
      array (
        'name' => 'anml_anima9941ollment_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_anml_animals_wpe_work_product_enrollment_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_anml_animals_wpe_work_product_enrollment_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_anml_animals_wpe_work_product_enrollment_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_anima9941ollment_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'anml_animals_wpe_work_product_enrollment_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'anml_anima9941ollment_idb',
        ),
      ),
    ),
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'WPE_Work_Product_Enrollment',
    'rhs_table' => 'wpe_work_product_enrollment',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'anml_animals_wpe_work_product_enrollment_1_c',
    'join_key_lhs' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
    'join_key_rhs' => 'anml_anima9941ollment_idb',
    'readonly' => true,
    'relationship_name' => 'anml_animals_wpe_work_product_enrollment_1',
    'rhs_subpanel' => 'ForAnml_animalsanml_animals_wpe_work_product_enrollment_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'wpe_work_product_enrollment_wpe_work_product_enrollment_1' => 
  array (
    'name' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1',
    'true_relationship_type' => 'one-to-one',
    'from_studio' => true,
    'relationships' => 
    array (
      'wpe_work_product_enrollment_wpe_work_product_enrollment_1' => 
      array (
        'lhs_module' => 'WPE_Work_Product_Enrollment',
        'lhs_table' => 'wpe_work_product_enrollment',
        'lhs_key' => 'id',
        'rhs_module' => 'WPE_Work_Product_Enrollment',
        'rhs_table' => 'wpe_work_product_enrollment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1_c',
        'join_key_lhs' => 'wpe_work_pd83eollment_ida',
        'join_key_rhs' => 'wpe_work_pab8dollment_idb',
      ),
    ),
    'table' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'wpe_work_pd83eollment_ida' => 
      array (
        'name' => 'wpe_work_pd83eollment_ida',
        'type' => 'id',
      ),
      'wpe_work_pab8dollment_idb' => 
      array (
        'name' => 'wpe_work_pab8dollment_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_wpe_work_product_enrollment_wpe_work_product_enrollment_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_wpe_work_product_enrollment_wpe_work_product_enrollment_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'wpe_work_pd83eollment_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_wpe_work_product_enrollment_wpe_work_product_enrollment_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'wpe_work_pab8dollment_idb',
          1 => 'deleted',
        ),
      ),
    ),
    'lhs_module' => 'WPE_Work_Product_Enrollment',
    'lhs_table' => 'wpe_work_product_enrollment',
    'lhs_key' => 'id',
    'rhs_module' => 'WPE_Work_Product_Enrollment',
    'rhs_table' => 'wpe_work_product_enrollment',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-one',
    'join_table' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1_c',
    'join_key_lhs' => 'wpe_work_pd83eollment_ida',
    'join_key_rhs' => 'wpe_work_pab8dollment_idb',
    'readonly' => true,
    'relationship_name' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'wpe_work_product_enrollment_anml_animals_1' => 
  array (
    'lhs_module' => 'WPE_Work_Product_Enrollment',
    'rhs_module' => 'ANML_Animals',
    'relationship_type' => 'one-to-one',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'wpe_work_product_enrollment_anml_animals_1',
  ),
);