<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'm03_work_product_a1a_critical_phase_inspectio_1' => 
  array (
    'name' => 'm03_work_product_a1a_critical_phase_inspectio_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_a1a_critical_phase_inspectio_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'A1A_Critical_Phase_Inspectio',
        'rhs_table' => 'a1a_critical_phase_inspectio',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_a1a_critical_phase_inspectio_1_c',
        'join_key_lhs' => 'm03_work_p33edproduct_ida',
        'join_key_rhs' => 'm03_work_p934fspectio_idb',
      ),
    ),
    'table' => 'm03_work_product_a1a_critical_phase_inspectio_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_p33edproduct_ida' => 
      array (
        'name' => 'm03_work_p33edproduct_ida',
        'type' => 'id',
      ),
      'm03_work_p934fspectio_idb' => 
      array (
        'name' => 'm03_work_p934fspectio_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_a1a_critical_phase_inspectio_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_a1a_critical_phase_inspectio_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p33edproduct_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_a1a_critical_phase_inspectio_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p934fspectio_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_a1a_critical_phase_inspectio_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_p934fspectio_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'A1A_Critical_Phase_Inspectio',
    'rhs_table' => 'a1a_critical_phase_inspectio',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_a1a_critical_phase_inspectio_1_c',
    'join_key_lhs' => 'm03_work_p33edproduct_ida',
    'join_key_rhs' => 'm03_work_p934fspectio_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_a1a_critical_phase_inspectio_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'a1a_critical_phase_inspectio_a1a_cpi_findings_1' => 
  array (
    'name' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'a1a_critical_phase_inspectio_a1a_cpi_findings_1' => 
      array (
        'lhs_module' => 'A1A_Critical_Phase_Inspectio',
        'lhs_table' => 'a1a_critical_phase_inspectio',
        'lhs_key' => 'id',
        'rhs_module' => 'A1A_CPI_Findings',
        'rhs_table' => 'a1a_cpi_findings',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1_c',
        'join_key_lhs' => 'a1a_criticbf09spectio_ida',
        'join_key_rhs' => 'a1a_critic8af6indings_idb',
      ),
    ),
    'table' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'a1a_criticbf09spectio_ida' => 
      array (
        'name' => 'a1a_criticbf09spectio_ida',
        'type' => 'id',
      ),
      'a1a_critic8af6indings_idb' => 
      array (
        'name' => 'a1a_critic8af6indings_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_a1a_critical_phase_inspectio_a1a_cpi_findings_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_a1a_critical_phase_inspectio_a1a_cpi_findings_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'a1a_criticbf09spectio_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_a1a_critical_phase_inspectio_a1a_cpi_findings_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'a1a_critic8af6indings_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'a1a_critic8af6indings_idb',
        ),
      ),
    ),
    'lhs_module' => 'A1A_Critical_Phase_Inspectio',
    'lhs_table' => 'a1a_critical_phase_inspectio',
    'lhs_key' => 'id',
    'rhs_module' => 'A1A_CPI_Findings',
    'rhs_table' => 'a1a_cpi_findings',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1_c',
    'join_key_lhs' => 'a1a_criticbf09spectio_ida',
    'join_key_rhs' => 'a1a_critic8af6indings_idb',
    'readonly' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'a1a_critical_phase_inspectio_modified_user' => 
  array (
    'name' => 'a1a_critical_phase_inspectio_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'A1A_Critical_Phase_Inspectio',
    'rhs_table' => 'a1a_critical_phase_inspectio',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'a1a_critical_phase_inspectio_created_by' => 
  array (
    'name' => 'a1a_critical_phase_inspectio_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'A1A_Critical_Phase_Inspectio',
    'rhs_table' => 'a1a_critical_phase_inspectio',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'a1a_critical_phase_inspectio_activities' => 
  array (
    'name' => 'a1a_critical_phase_inspectio_activities',
    'lhs_module' => 'A1A_Critical_Phase_Inspectio',
    'lhs_table' => 'a1a_critical_phase_inspectio',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'A1A_Critical_Phase_Inspectio',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'a1a_critical_phase_inspectio_following' => 
  array (
    'name' => 'a1a_critical_phase_inspectio_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'A1A_Critical_Phase_Inspectio',
    'rhs_table' => 'a1a_critical_phase_inspectio',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'A1A_Critical_Phase_Inspectio',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'a1a_critical_phase_inspectio_favorite' => 
  array (
    'name' => 'a1a_critical_phase_inspectio_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'A1A_Critical_Phase_Inspectio',
    'rhs_table' => 'a1a_critical_phase_inspectio',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'A1A_Critical_Phase_Inspectio',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'a1a_critical_phase_inspectio_assigned_user' => 
  array (
    'name' => 'a1a_critical_phase_inspectio_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'A1A_Critical_Phase_Inspectio',
    'rhs_table' => 'a1a_critical_phase_inspectio',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'a1a_critical_phase_inspectio_activities_1_calls' => 
  array (
    'rhs_label' => 'Activities',
    'lhs_label' => 'Critical Phase Inspections',
    'rhs_subpanel' => 'Default',
    'lhs_module' => 'A1A_Critical_Phase_Inspectio',
    'rhs_module' => 'Calls',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => true,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_activities_1_calls',
  ),
  'a1a_critical_phase_inspectio_activities_1_meetings' => 
  array (
    'rhs_label' => 'Activities',
    'lhs_label' => 'Critical Phase Inspections',
    'rhs_subpanel' => 'Default',
    'lhs_module' => 'A1A_Critical_Phase_Inspectio',
    'rhs_module' => 'Meetings',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => true,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_activities_1_meetings',
  ),
  'a1a_critical_phase_inspectio_activities_1_notes' => 
  array (
    'rhs_label' => 'Activities',
    'lhs_label' => 'Critical Phase Inspections',
    'rhs_subpanel' => 'Default',
    'lhs_module' => 'A1A_Critical_Phase_Inspectio',
    'rhs_module' => 'Notes',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => true,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_activities_1_notes',
  ),
  'a1a_critical_phase_inspectio_activities_1_tasks' => 
  array (
    'rhs_label' => 'Activities',
    'lhs_label' => 'Critical Phase Inspections',
    'rhs_subpanel' => 'Default',
    'lhs_module' => 'A1A_Critical_Phase_Inspectio',
    'rhs_module' => 'Tasks',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => true,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_activities_1_tasks',
  ),
  'a1a_critical_phase_inspectio_activities_1_emails' => 
  array (
    'rhs_label' => 'Activities',
    'lhs_label' => 'Critical Phase Inspections',
    'rhs_subpanel' => 'Default',
    'lhs_module' => 'A1A_Critical_Phase_Inspectio',
    'rhs_module' => 'Emails',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => true,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'a1a_critical_phase_inspectio_activities_1_emails',
  ),
);