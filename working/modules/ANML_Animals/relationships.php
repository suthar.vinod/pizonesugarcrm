<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'anml_animals_wpe_work_product_enrollment_1' => 
  array (
    'name' => 'anml_animals_wpe_work_product_enrollment_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'anml_animals_wpe_work_product_enrollment_1' => 
      array (
        'lhs_module' => 'ANML_Animals',
        'lhs_table' => 'anml_animals',
        'lhs_key' => 'id',
        'rhs_module' => 'WPE_Work_Product_Enrollment',
        'rhs_table' => 'wpe_work_product_enrollment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'anml_animals_wpe_work_product_enrollment_1_c',
        'join_key_lhs' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
        'join_key_rhs' => 'anml_anima9941ollment_idb',
      ),
    ),
    'table' => 'anml_animals_wpe_work_product_enrollment_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'anml_animals_wpe_work_product_enrollment_1anml_animals_ida' => 
      array (
        'name' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
        'type' => 'id',
      ),
      'anml_anima9941ollment_idb' => 
      array (
        'name' => 'anml_anima9941ollment_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_anml_animals_wpe_work_product_enrollment_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_anml_animals_wpe_work_product_enrollment_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_anml_animals_wpe_work_product_enrollment_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_anima9941ollment_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'anml_animals_wpe_work_product_enrollment_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'anml_anima9941ollment_idb',
        ),
      ),
    ),
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'WPE_Work_Product_Enrollment',
    'rhs_table' => 'wpe_work_product_enrollment',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'anml_animals_wpe_work_product_enrollment_1_c',
    'join_key_lhs' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
    'join_key_rhs' => 'anml_anima9941ollment_idb',
    'readonly' => true,
    'relationship_name' => 'anml_animals_wpe_work_product_enrollment_1',
    'rhs_subpanel' => 'ForAnml_animalsAnml_animals_wpe_work_product_enrollment_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'wpe_work_product_enrollment_anml_animals_1' => 
  array (
    'name' => 'wpe_work_product_enrollment_anml_animals_1',
    'true_relationship_type' => 'one-to-one',
    'from_studio' => true,
    'relationships' => 
    array (
      'wpe_work_product_enrollment_anml_animals_1' => 
      array (
        'lhs_module' => 'WPE_Work_Product_Enrollment',
        'lhs_table' => 'wpe_work_product_enrollment',
        'lhs_key' => 'id',
        'rhs_module' => 'ANML_Animals',
        'rhs_table' => 'anml_animals',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'wpe_work_product_enrollment_anml_animals_1_c',
        'join_key_lhs' => 'wpe_work_p83ebollment_ida',
        'join_key_rhs' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
      ),
    ),
    'table' => 'wpe_work_product_enrollment_anml_animals_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'wpe_work_p83ebollment_ida' => 
      array (
        'name' => 'wpe_work_p83ebollment_ida',
        'type' => 'id',
      ),
      'wpe_work_product_enrollment_anml_animals_1anml_animals_idb' => 
      array (
        'name' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_wpe_work_product_enrollment_anml_animals_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_wpe_work_product_enrollment_anml_animals_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'wpe_work_p83ebollment_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_wpe_work_product_enrollment_anml_animals_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
          1 => 'deleted',
        ),
      ),
    ),
    'lhs_module' => 'WPE_Work_Product_Enrollment',
    'lhs_table' => 'wpe_work_product_enrollment',
    'lhs_key' => 'id',
    'rhs_module' => 'ANML_Animals',
    'rhs_table' => 'anml_animals',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-one',
    'join_table' => 'wpe_work_product_enrollment_anml_animals_1_c',
    'join_key_lhs' => 'wpe_work_p83ebollment_ida',
    'join_key_rhs' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
    'readonly' => true,
    'relationship_name' => 'wpe_work_product_enrollment_anml_animals_1',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => 'ForAnml_animalsAnml_animals_wpe_work_product_enrollment_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm06_error_anml_animals_1' => 
  array (
    'name' => 'm06_error_anml_animals_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_anml_animals_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'ANML_Animals',
        'rhs_table' => 'anml_animals',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_anml_animals_1_c',
        'join_key_lhs' => 'm06_error_anml_animals_1m06_error_ida',
        'join_key_rhs' => 'm06_error_anml_animals_1anml_animals_idb',
      ),
    ),
    'table' => 'm06_error_anml_animals_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_anml_animals_1m06_error_ida' => 
      array (
        'name' => 'm06_error_anml_animals_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_anml_animals_1anml_animals_idb' => 
      array (
        'name' => 'm06_error_anml_animals_1anml_animals_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_anml_animals_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_anml_animals_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_anml_animals_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_anml_animals_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_anml_animals_1anml_animals_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_anml_animals_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_anml_animals_1m06_error_ida',
          1 => 'm06_error_anml_animals_1anml_animals_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'ANML_Animals',
    'rhs_table' => 'anml_animals',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm06_error_anml_animals_1_c',
    'join_key_lhs' => 'm06_error_anml_animals_1m06_error_ida',
    'join_key_rhs' => 'm06_error_anml_animals_1anml_animals_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_anml_animals_1',
    'rhs_subpanel' => 'ForM06_errorM06_error_anml_animals_1',
    'lhs_subpanel' => 'ForAnml_animalsM06_error_anml_animals_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'anml_animals_rt_room_transfer_1' => 
  array (
    'name' => 'anml_animals_rt_room_transfer_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'anml_animals_rt_room_transfer_1' => 
      array (
        'lhs_module' => 'ANML_Animals',
        'lhs_table' => 'anml_animals',
        'lhs_key' => 'id',
        'rhs_module' => 'RT_Room_Transfer',
        'rhs_table' => 'rt_room_transfer',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'anml_animals_rt_room_transfer_1_c',
        'join_key_lhs' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
        'join_key_rhs' => 'anml_animals_rt_room_transfer_1rt_room_transfer_idb',
      ),
    ),
    'table' => 'anml_animals_rt_room_transfer_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'anml_animals_rt_room_transfer_1anml_animals_ida' => 
      array (
        'name' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
        'type' => 'id',
      ),
      'anml_animals_rt_room_transfer_1rt_room_transfer_idb' => 
      array (
        'name' => 'anml_animals_rt_room_transfer_1rt_room_transfer_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_anml_animals_rt_room_transfer_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_anml_animals_rt_room_transfer_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_rt_room_transfer_1anml_animals_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_anml_animals_rt_room_transfer_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_rt_room_transfer_1rt_room_transfer_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'anml_animals_rt_room_transfer_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'anml_animals_rt_room_transfer_1rt_room_transfer_idb',
        ),
      ),
    ),
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'RT_Room_Transfer',
    'rhs_table' => 'rt_room_transfer',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'anml_animals_rt_room_transfer_1_c',
    'join_key_lhs' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
    'join_key_rhs' => 'anml_animals_rt_room_transfer_1rt_room_transfer_idb',
    'readonly' => true,
    'relationship_name' => 'anml_animals_rt_room_transfer_1',
    'rhs_subpanel' => 'ForAnml_animalsAnml_animals_rt_room_transfer_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'anml_animals_tsdoc_test_system_documents_1' => 
  array (
    'name' => 'anml_animals_tsdoc_test_system_documents_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'anml_animals_tsdoc_test_system_documents_1' => 
      array (
        'lhs_module' => 'ANML_Animals',
        'lhs_table' => 'anml_animals',
        'lhs_key' => 'id',
        'rhs_module' => 'TSdoc_Test_System_Documents',
        'rhs_table' => 'tsdoc_test_system_documents',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'anml_animals_tsdoc_test_system_documents_1_c',
        'join_key_lhs' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
        'join_key_rhs' => 'anml_animad2fbcuments_idb',
      ),
    ),
    'table' => 'anml_animals_tsdoc_test_system_documents_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'anml_animals_tsdoc_test_system_documents_1anml_animals_ida' => 
      array (
        'name' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
        'type' => 'id',
      ),
      'anml_animad2fbcuments_idb' => 
      array (
        'name' => 'anml_animad2fbcuments_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_anml_animals_tsdoc_test_system_documents_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_anml_animals_tsdoc_test_system_documents_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_anml_animals_tsdoc_test_system_documents_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animad2fbcuments_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'anml_animals_tsdoc_test_system_documents_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'anml_animad2fbcuments_idb',
        ),
      ),
    ),
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'TSdoc_Test_System_Documents',
    'rhs_table' => 'tsdoc_test_system_documents',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'anml_animals_tsdoc_test_system_documents_1_c',
    'join_key_lhs' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
    'join_key_rhs' => 'anml_animad2fbcuments_idb',
    'readonly' => true,
    'relationship_name' => 'anml_animals_tsdoc_test_system_documents_1',
    'rhs_subpanel' => 'ForAnml_animalsAnml_animals_tsdoc_test_system_documents_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'anml_animals_w_weight_1' => 
  array (
    'name' => 'anml_animals_w_weight_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'anml_animals_w_weight_1' => 
      array (
        'lhs_module' => 'ANML_Animals',
        'lhs_table' => 'anml_animals',
        'lhs_key' => 'id',
        'rhs_module' => 'W_Weight',
        'rhs_table' => 'w_weight',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'anml_animals_w_weight_1_c',
        'join_key_lhs' => 'anml_animals_w_weight_1anml_animals_ida',
        'join_key_rhs' => 'anml_animals_w_weight_1w_weight_idb',
      ),
    ),
    'table' => 'anml_animals_w_weight_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'anml_animals_w_weight_1anml_animals_ida' => 
      array (
        'name' => 'anml_animals_w_weight_1anml_animals_ida',
        'type' => 'id',
      ),
      'anml_animals_w_weight_1w_weight_idb' => 
      array (
        'name' => 'anml_animals_w_weight_1w_weight_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_anml_animals_w_weight_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_anml_animals_w_weight_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_w_weight_1anml_animals_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_anml_animals_w_weight_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_w_weight_1w_weight_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'anml_animals_w_weight_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'anml_animals_w_weight_1w_weight_idb',
        ),
      ),
    ),
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'W_Weight',
    'rhs_table' => 'w_weight',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'anml_animals_w_weight_1_c',
    'join_key_lhs' => 'anml_animals_w_weight_1anml_animals_ida',
    'join_key_rhs' => 'anml_animals_w_weight_1w_weight_idb',
    'readonly' => true,
    'relationship_name' => 'anml_animals_w_weight_1',
    'rhs_subpanel' => 'ForAnml_animalsAnml_animals_w_weight_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'anml_animals_cie_clinical_issue_exam_1' => 
  array (
    'name' => 'anml_animals_cie_clinical_issue_exam_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'anml_animals_cie_clinical_issue_exam_1' => 
      array (
        'lhs_module' => 'ANML_Animals',
        'lhs_table' => 'anml_animals',
        'lhs_key' => 'id',
        'rhs_module' => 'CIE_Clinical_Issue_Exam',
        'rhs_table' => 'cie_clinical_issue_exam',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'anml_animals_cie_clinical_issue_exam_1_c',
        'join_key_lhs' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
        'join_key_rhs' => 'anml_anima4710ue_exam_idb',
      ),
    ),
    'table' => 'anml_animals_cie_clinical_issue_exam_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'anml_animals_cie_clinical_issue_exam_1anml_animals_ida' => 
      array (
        'name' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
        'type' => 'id',
      ),
      'anml_anima4710ue_exam_idb' => 
      array (
        'name' => 'anml_anima4710ue_exam_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_anml_animals_cie_clinical_issue_exam_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_anml_animals_cie_clinical_issue_exam_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_anml_animals_cie_clinical_issue_exam_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_anima4710ue_exam_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'anml_animals_cie_clinical_issue_exam_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'anml_anima4710ue_exam_idb',
        ),
      ),
    ),
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'CIE_Clinical_Issue_Exam',
    'rhs_table' => 'cie_clinical_issue_exam',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'anml_animals_cie_clinical_issue_exam_1_c',
    'join_key_lhs' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
    'join_key_rhs' => 'anml_anima4710ue_exam_idb',
    'readonly' => true,
    'relationship_name' => 'anml_animals_cie_clinical_issue_exam_1',
    'rhs_subpanel' => 'ForAnml_animalsAnml_animals_cie_clinical_issue_exam_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'anml_animals_co_clinical_observation_1' => 
  array (
    'name' => 'anml_animals_co_clinical_observation_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'anml_animals_co_clinical_observation_1' => 
      array (
        'lhs_module' => 'ANML_Animals',
        'lhs_table' => 'anml_animals',
        'lhs_key' => 'id',
        'rhs_module' => 'CO_Clinical_Observation',
        'rhs_table' => 'co_clinical_observation',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'anml_animals_co_clinical_observation_1_c',
        'join_key_lhs' => 'anml_animals_co_clinical_observation_1anml_animals_ida',
        'join_key_rhs' => 'anml_anima15d0rvation_idb',
      ),
    ),
    'table' => 'anml_animals_co_clinical_observation_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'anml_animals_co_clinical_observation_1anml_animals_ida' => 
      array (
        'name' => 'anml_animals_co_clinical_observation_1anml_animals_ida',
        'type' => 'id',
      ),
      'anml_anima15d0rvation_idb' => 
      array (
        'name' => 'anml_anima15d0rvation_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_anml_animals_co_clinical_observation_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_anml_animals_co_clinical_observation_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_co_clinical_observation_1anml_animals_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_anml_animals_co_clinical_observation_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_anima15d0rvation_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'anml_animals_co_clinical_observation_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'anml_anima15d0rvation_idb',
        ),
      ),
    ),
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'CO_Clinical_Observation',
    'rhs_table' => 'co_clinical_observation',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'anml_animals_co_clinical_observation_1_c',
    'join_key_lhs' => 'anml_animals_co_clinical_observation_1anml_animals_ida',
    'join_key_rhs' => 'anml_anima15d0rvation_idb',
    'readonly' => true,
    'relationship_name' => 'anml_animals_co_clinical_observation_1',
    'rhs_subpanel' => 'ForAnml_animalsAnml_animals_co_clinical_observation_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'anml_animals_usda_historical_usda_id_1' => 
  array (
    'name' => 'anml_animals_usda_historical_usda_id_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'anml_animals_usda_historical_usda_id_1' => 
      array (
        'lhs_module' => 'ANML_Animals',
        'lhs_table' => 'anml_animals',
        'lhs_key' => 'id',
        'rhs_module' => 'USDA_Historical_USDA_ID',
        'rhs_table' => 'usda_historical_usda_id',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'anml_animals_usda_historical_usda_id_1_c',
        'join_key_lhs' => 'anml_animals_usda_historical_usda_id_1anml_animals_ida',
        'join_key_rhs' => 'anml_anima2e7fusda_id_idb',
      ),
    ),
    'table' => 'anml_animals_usda_historical_usda_id_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'anml_animals_usda_historical_usda_id_1anml_animals_ida' => 
      array (
        'name' => 'anml_animals_usda_historical_usda_id_1anml_animals_ida',
        'type' => 'id',
      ),
      'anml_anima2e7fusda_id_idb' => 
      array (
        'name' => 'anml_anima2e7fusda_id_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_anml_animals_usda_historical_usda_id_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_anml_animals_usda_historical_usda_id_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_usda_historical_usda_id_1anml_animals_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_anml_animals_usda_historical_usda_id_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_anima2e7fusda_id_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'anml_animals_usda_historical_usda_id_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'anml_anima2e7fusda_id_idb',
        ),
      ),
    ),
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'USDA_Historical_USDA_ID',
    'rhs_table' => 'usda_historical_usda_id',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'anml_animals_usda_historical_usda_id_1_c',
    'join_key_lhs' => 'anml_animals_usda_historical_usda_id_1anml_animals_ida',
    'join_key_rhs' => 'anml_anima2e7fusda_id_idb',
    'readonly' => true,
    'relationship_name' => 'anml_animals_usda_historical_usda_id_1',
    'rhs_subpanel' => 'ForAnml_animalsAnml_animals_usda_historical_usda_id_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'anml_animals_taskd_task_design_1' => 
  array (
    'name' => 'anml_animals_taskd_task_design_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'anml_animals_taskd_task_design_1' => 
      array (
        'lhs_module' => 'ANML_Animals',
        'lhs_table' => 'anml_animals',
        'lhs_key' => 'id',
        'rhs_module' => 'TaskD_Task_Design',
        'rhs_table' => 'taskd_task_design',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'anml_animals_taskd_task_design_1_c',
        'join_key_lhs' => 'anml_animals_taskd_task_design_1anml_animals_ida',
        'join_key_rhs' => 'anml_animals_taskd_task_design_1taskd_task_design_idb',
      ),
    ),
    'table' => 'anml_animals_taskd_task_design_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'anml_animals_taskd_task_design_1anml_animals_ida' => 
      array (
        'name' => 'anml_animals_taskd_task_design_1anml_animals_ida',
        'type' => 'id',
      ),
      'anml_animals_taskd_task_design_1taskd_task_design_idb' => 
      array (
        'name' => 'anml_animals_taskd_task_design_1taskd_task_design_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_anml_animals_taskd_task_design_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_anml_animals_taskd_task_design_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_taskd_task_design_1anml_animals_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_anml_animals_taskd_task_design_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_taskd_task_design_1taskd_task_design_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'anml_animals_taskd_task_design_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'anml_animals_taskd_task_design_1taskd_task_design_idb',
        ),
      ),
    ),
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'TaskD_Task_Design',
    'rhs_table' => 'taskd_task_design',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'anml_animals_taskd_task_design_1_c',
    'join_key_lhs' => 'anml_animals_taskd_task_design_1anml_animals_ida',
    'join_key_rhs' => 'anml_animals_taskd_task_design_1taskd_task_design_idb',
    'readonly' => true,
    'relationship_name' => 'anml_animals_taskd_task_design_1',
    'rhs_subpanel' => 'ForAnml_animalsAnml_animals_taskd_task_design_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'anml_animals_modified_user' => 
  array (
    'name' => 'anml_animals_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'ANML_Animals',
    'rhs_table' => 'anml_animals',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'anml_animals_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'anml_animals_created_by' => 
  array (
    'name' => 'anml_animals_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'ANML_Animals',
    'rhs_table' => 'anml_animals',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'anml_animals_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'anml_animals_activities' => 
  array (
    'name' => 'anml_animals_activities',
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'ANML_Animals',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'anml_animals_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'anml_animals_following' => 
  array (
    'name' => 'anml_animals_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'ANML_Animals',
    'rhs_table' => 'anml_animals',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'ANML_Animals',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'anml_animals_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'anml_animals_favorite' => 
  array (
    'name' => 'anml_animals_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'ANML_Animals',
    'rhs_table' => 'anml_animals',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'ANML_Animals',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'anml_animals_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'anml_animals_assigned_user' => 
  array (
    'name' => 'anml_animals_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'ANML_Animals',
    'rhs_table' => 'anml_animals',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'anml_animals_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'anml_animals_ii_inventory_item_1' => 
  array (
    'rhs_label' => 'Inventory Items',
    'lhs_label' => 'Test Systems',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'ANML_Animals',
    'rhs_module' => 'II_Inventory_Item',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'anml_animals_ii_inventory_item_1',
  ),
);