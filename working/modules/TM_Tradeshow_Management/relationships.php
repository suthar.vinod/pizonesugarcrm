<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'tm_tradeshow_management_documents_1' => 
  array (
    'name' => 'tm_tradeshow_management_documents_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'tm_tradeshow_management_documents_1' => 
      array (
        'lhs_module' => 'TM_Tradeshow_Management',
        'lhs_table' => 'tm_tradeshow_management',
        'lhs_key' => 'id',
        'rhs_module' => 'Documents',
        'rhs_table' => 'documents',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'tm_tradeshow_management_documents_1_c',
        'join_key_lhs' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
        'join_key_rhs' => 'tm_tradeshow_management_documents_1documents_idb',
      ),
    ),
    'table' => 'tm_tradeshow_management_documents_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'tm_tradeshow_management_documents_1tm_tradeshow_management_ida' => 
      array (
        'name' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
        'type' => 'id',
      ),
      'tm_tradeshow_management_documents_1documents_idb' => 
      array (
        'name' => 'tm_tradeshow_management_documents_1documents_idb',
        'type' => 'id',
      ),
      'document_revision_id' => 
      array (
        'name' => 'document_revision_id',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_tm_tradeshow_management_documents_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_tm_tradeshow_management_documents_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_tm_tradeshow_management_documents_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'tm_tradeshow_management_documents_1documents_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'tm_tradeshow_management_documents_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'tm_tradeshow_management_documents_1documents_idb',
        ),
      ),
    ),
    'lhs_module' => 'TM_Tradeshow_Management',
    'lhs_table' => 'tm_tradeshow_management',
    'lhs_key' => 'id',
    'rhs_module' => 'Documents',
    'rhs_table' => 'documents',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'tm_tradeshow_management_documents_1_c',
    'join_key_lhs' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
    'join_key_rhs' => 'tm_tradeshow_management_documents_1documents_idb',
    'readonly' => true,
    'relationship_name' => 'tm_tradeshow_management_documents_1',
    'rhs_subpanel' => 'ForTm_tradeshow_managementTm_tradeshow_management_documents_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'tm_tradeshow_management_ta_tradeshow_activities_1' => 
  array (
    'name' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'tm_tradeshow_management_ta_tradeshow_activities_1' => 
      array (
        'lhs_module' => 'TM_Tradeshow_Management',
        'lhs_table' => 'tm_tradeshow_management',
        'lhs_key' => 'id',
        'rhs_module' => 'TA_Tradeshow_Activities',
        'rhs_table' => 'ta_tradeshow_activities',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'tm_tradeshow_management_ta_tradeshow_activities_1_c',
        'join_key_lhs' => 'tm_tradesh0f8eagement_ida',
        'join_key_rhs' => 'tm_tradeshdb10ivities_idb',
      ),
    ),
    'table' => 'tm_tradeshow_management_ta_tradeshow_activities_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'tm_tradesh0f8eagement_ida' => 
      array (
        'name' => 'tm_tradesh0f8eagement_ida',
        'type' => 'id',
      ),
      'tm_tradeshdb10ivities_idb' => 
      array (
        'name' => 'tm_tradeshdb10ivities_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_tm_tradeshow_management_ta_tradeshow_activities_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_tm_tradeshow_management_ta_tradeshow_activities_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'tm_tradesh0f8eagement_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_tm_tradeshow_management_ta_tradeshow_activities_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'tm_tradeshdb10ivities_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'tm_tradeshow_management_ta_tradeshow_activities_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'tm_tradeshdb10ivities_idb',
        ),
      ),
    ),
    'lhs_module' => 'TM_Tradeshow_Management',
    'lhs_table' => 'tm_tradeshow_management',
    'lhs_key' => 'id',
    'rhs_module' => 'TA_Tradeshow_Activities',
    'rhs_table' => 'ta_tradeshow_activities',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'tm_tradeshow_management_ta_tradeshow_activities_1_c',
    'join_key_lhs' => 'tm_tradesh0f8eagement_ida',
    'join_key_rhs' => 'tm_tradeshdb10ivities_idb',
    'readonly' => true,
    'relationship_name' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
    'rhs_subpanel' => 'ForTm_tradeshow_managementTm_tradeshow_management_ta_tradeshow_activities_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'tm_tradeshow_management_m01_sales_1' => 
  array (
    'name' => 'tm_tradeshow_management_m01_sales_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'tm_tradeshow_management_m01_sales_1' => 
      array (
        'lhs_module' => 'TM_Tradeshow_Management',
        'lhs_table' => 'tm_tradeshow_management',
        'lhs_key' => 'id',
        'rhs_module' => 'M01_Sales',
        'rhs_table' => 'm01_sales',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'tm_tradeshow_management_m01_sales_1_c',
        'join_key_lhs' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
        'join_key_rhs' => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
      ),
    ),
    'table' => 'tm_tradeshow_management_m01_sales_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida' => 
      array (
        'name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
        'type' => 'id',
      ),
      'tm_tradeshow_management_m01_sales_1m01_sales_idb' => 
      array (
        'name' => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_tm_tradeshow_management_m01_sales_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_tm_tradeshow_management_m01_sales_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_tm_tradeshow_management_m01_sales_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'tm_tradeshow_management_m01_sales_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
        ),
      ),
    ),
    'lhs_module' => 'TM_Tradeshow_Management',
    'lhs_table' => 'tm_tradeshow_management',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Sales',
    'rhs_table' => 'm01_sales',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'tm_tradeshow_management_m01_sales_1_c',
    'join_key_lhs' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
    'join_key_rhs' => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
    'readonly' => true,
    'relationship_name' => 'tm_tradeshow_management_m01_sales_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'tm_tradeshow_management_modified_user' => 
  array (
    'name' => 'tm_tradeshow_management_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'TM_Tradeshow_Management',
    'rhs_table' => 'tm_tradeshow_management',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'tm_tradeshow_management_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'tm_tradeshow_management_created_by' => 
  array (
    'name' => 'tm_tradeshow_management_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'TM_Tradeshow_Management',
    'rhs_table' => 'tm_tradeshow_management',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'tm_tradeshow_management_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'tm_tradeshow_management_activities' => 
  array (
    'name' => 'tm_tradeshow_management_activities',
    'lhs_module' => 'TM_Tradeshow_Management',
    'lhs_table' => 'tm_tradeshow_management',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'TM_Tradeshow_Management',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'tm_tradeshow_management_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'tm_tradeshow_management_following' => 
  array (
    'name' => 'tm_tradeshow_management_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'TM_Tradeshow_Management',
    'rhs_table' => 'tm_tradeshow_management',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'TM_Tradeshow_Management',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'tm_tradeshow_management_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'tm_tradeshow_management_favorite' => 
  array (
    'name' => 'tm_tradeshow_management_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'TM_Tradeshow_Management',
    'rhs_table' => 'tm_tradeshow_management',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'TM_Tradeshow_Management',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'tm_tradeshow_management_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'tm_tradeshow_management_assigned_user' => 
  array (
    'name' => 'tm_tradeshow_management_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'TM_Tradeshow_Management',
    'rhs_table' => 'tm_tradeshow_management',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'tm_tradeshow_management_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'tm_tradeshow_management_td_tradeshow_documents_1' => 
  array (
    'rhs_label' => 'Tradeshow Documents',
    'lhs_label' => 'Tradeshow_Management',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'TM_Tradeshow_Management',
    'rhs_module' => 'TD_Tradeshow_Documents',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'tm_tradeshow_management_td_tradeshow_documents_1',
  ),
);