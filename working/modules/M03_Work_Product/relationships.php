<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'm03_work_product_m03_work_product_facility_1' => 
  array (
    'name' => 'm03_work_product_m03_work_product_facility_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_m03_work_product_facility_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product_Facility',
        'rhs_table' => 'm03_work_product_facility',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_m03_work_product_facility_1_c',
        'join_key_lhs' => 'm03_work_product_m03_work_product_facility_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_p86d3acility_idb',
      ),
    ),
    'table' => 'm03_work_product_m03_work_product_facility_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'm03_work_product_m03_work_product_facility_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_m03_work_product_facility_1m03_work_product_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'm03_work_p86d3acility_idb' => 
      array (
        'name' => 'm03_work_p86d3acility_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'm03_work_product_m03_work_product_facility_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'm03_work_product_m03_work_product_facility_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_m03_work_product_facility_1m03_work_product_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'm03_work_product_m03_work_product_facility_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_p86d3acility_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Facility',
    'rhs_table' => 'm03_work_product_facility',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_m03_work_product_facility_1_c',
    'join_key_lhs' => 'm03_work_product_m03_work_product_facility_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_p86d3acility_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_m03_work_product_facility_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm01_sales_m03_work_product_1' => 
  array (
    'name' => 'm01_sales_m03_work_product_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_sales_m03_work_product_1' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product',
        'rhs_table' => 'm03_work_product',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_sales_m03_work_product_1_c',
        'join_key_lhs' => 'm01_sales_m03_work_product_1m01_sales_ida',
        'join_key_rhs' => 'm01_sales_m03_work_product_1m03_work_product_idb',
      ),
    ),
    'table' => 'm01_sales_m03_work_product_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'm01_sales_m03_work_product_1m01_sales_ida' => 
      array (
        'name' => 'm01_sales_m03_work_product_1m01_sales_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'm01_sales_m03_work_product_1m03_work_product_idb' => 
      array (
        'name' => 'm01_sales_m03_work_product_1m03_work_product_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'm01_sales_m03_work_product_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'm01_sales_m03_work_product_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_m03_work_product_1m01_sales_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'm01_sales_m03_work_product_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_sales_m03_work_product_1m03_work_product_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm01_sales_m03_work_product_1_c',
    'join_key_lhs' => 'm01_sales_m03_work_product_1m01_sales_ida',
    'join_key_rhs' => 'm01_sales_m03_work_product_1m03_work_product_idb',
    'readonly' => true,
    'relationship_name' => 'm01_sales_m03_work_product_1',
    'rhs_subpanel' => 'ForM01_salesM01_sales_m03_work_product_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_m03_work_product_deliverable_1' => 
  array (
    'name' => 'm03_work_product_m03_work_product_deliverable_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_m03_work_product_deliverable_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product_Deliverable',
        'rhs_table' => 'm03_work_product_deliverable',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_m03_work_product_deliverable_1_c',
        'join_key_lhs' => 'm03_work_p0b66product_ida',
        'join_key_rhs' => 'm03_work_pe584verable_idb',
      ),
    ),
    'table' => 'm03_work_product_m03_work_product_deliverable_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'm03_work_p0b66product_ida' => 
      array (
        'name' => 'm03_work_p0b66product_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'm03_work_pe584verable_idb' => 
      array (
        'name' => 'm03_work_pe584verable_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'm03_work_product_m03_work_product_deliverable_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'm03_work_product_m03_work_product_deliverable_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p0b66product_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'm03_work_product_m03_work_product_deliverable_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_pe584verable_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Deliverable',
    'rhs_table' => 'm03_work_product_deliverable',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_m03_work_product_deliverable_1_c',
    'join_key_lhs' => 'm03_work_p0b66product_ida',
    'join_key_rhs' => 'm03_work_pe584verable_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_m03_work_product_deliverable_1',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_m03_work_product_deliverable_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_tasks_1' => 
  array (
    'name' => 'm03_work_product_tasks_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_tasks_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'Tasks',
        'rhs_table' => 'tasks',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_tasks_1_c',
        'join_key_lhs' => 'm03_work_product_tasks_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_product_tasks_1tasks_idb',
      ),
    ),
    'table' => 'm03_work_product_tasks_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'm03_work_product_tasks_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_tasks_1m03_work_product_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'm03_work_product_tasks_1tasks_idb' => 
      array (
        'name' => 'm03_work_product_tasks_1tasks_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'm03_work_product_tasks_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'm03_work_product_tasks_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_tasks_1m03_work_product_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'm03_work_product_tasks_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_tasks_1tasks_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'Tasks',
    'rhs_table' => 'tasks',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_tasks_1_c',
    'join_key_lhs' => 'm03_work_product_tasks_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_product_tasks_1tasks_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_tasks_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'ForTasksTasks_m03_work_product_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'tasks_m03_work_product_1' => 
  array (
    'name' => 'tasks_m03_work_product_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'tasks_m03_work_product_1' => 
      array (
        'lhs_module' => 'Tasks',
        'lhs_table' => 'tasks',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product',
        'rhs_table' => 'm03_work_product',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'tasks_m03_work_product_1_c',
        'join_key_lhs' => 'tasks_m03_work_product_1tasks_ida',
        'join_key_rhs' => 'tasks_m03_work_product_1m03_work_product_idb',
      ),
    ),
    'table' => 'tasks_m03_work_product_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'tasks_m03_work_product_1tasks_ida' => 
      array (
        'name' => 'tasks_m03_work_product_1tasks_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'tasks_m03_work_product_1m03_work_product_idb' => 
      array (
        'name' => 'tasks_m03_work_product_1m03_work_product_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'tasks_m03_work_product_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'tasks_m03_work_product_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'tasks_m03_work_product_1tasks_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'tasks_m03_work_product_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'tasks_m03_work_product_1m03_work_product_idb',
        ),
      ),
    ),
    'lhs_module' => 'Tasks',
    'lhs_table' => 'tasks',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'tasks_m03_work_product_1_c',
    'join_key_lhs' => 'tasks_m03_work_product_1tasks_ida',
    'join_key_rhs' => 'tasks_m03_work_product_1m03_work_product_idb',
    'readonly' => true,
    'relationship_name' => 'tasks_m03_work_product_1',
    'rhs_subpanel' => 'ForTasksTasks_m03_work_product_1',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_abc12_work_product_activities_1' => 
  array (
    'name' => 'm03_work_product_abc12_work_product_activities_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_abc12_work_product_activities_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'ABC12_Work_Product_Activities',
        'rhs_table' => 'abc12_work_product_activities',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_abc12_work_product_activities_1_c',
        'join_key_lhs' => 'm03_work_p4898product_ida',
        'join_key_rhs' => 'm03_work_p9069ivities_idb',
      ),
    ),
    'table' => 'm03_work_product_abc12_work_product_activities_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_p4898product_ida' => 
      array (
        'name' => 'm03_work_p4898product_ida',
        'type' => 'id',
      ),
      'm03_work_p9069ivities_idb' => 
      array (
        'name' => 'm03_work_p9069ivities_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'm03_work_product_abc12_work_product_activities_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'm03_work_product_abc12_work_product_activities_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p4898product_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'm03_work_product_abc12_work_product_activities_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_p9069ivities_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'ABC12_Work_Product_Activities',
    'rhs_table' => 'abc12_work_product_activities',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_abc12_work_product_activities_1_c',
    'join_key_lhs' => 'm03_work_p4898product_ida',
    'join_key_rhs' => 'm03_work_p9069ivities_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_abc12_work_product_activities_1',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_abc12_work_product_activities_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_meetings_1' => 
  array (
    'name' => 'm03_work_product_meetings_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_meetings_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'Meetings',
        'rhs_table' => 'meetings',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_meetings_1_c',
        'join_key_lhs' => 'm03_work_product_meetings_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_product_meetings_1meetings_idb',
      ),
    ),
    'table' => 'm03_work_product_meetings_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_meetings_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_meetings_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_product_meetings_1meetings_idb' => 
      array (
        'name' => 'm03_work_product_meetings_1meetings_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_meetings_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_meetings_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_meetings_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_meetings_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_meetings_1meetings_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_meetings_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_meetings_1meetings_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_meetings_1_c',
    'join_key_lhs' => 'm03_work_product_meetings_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_product_meetings_1meetings_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_meetings_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'ForMeetingsMeetings_m03_work_product_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_a1a_critical_phase_inspectio_1' => 
  array (
    'name' => 'm03_work_product_a1a_critical_phase_inspectio_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_a1a_critical_phase_inspectio_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'A1A_Critical_Phase_Inspectio',
        'rhs_table' => 'a1a_critical_phase_inspectio',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_a1a_critical_phase_inspectio_1_c',
        'join_key_lhs' => 'm03_work_p33edproduct_ida',
        'join_key_rhs' => 'm03_work_p934fspectio_idb',
      ),
    ),
    'table' => 'm03_work_product_a1a_critical_phase_inspectio_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_p33edproduct_ida' => 
      array (
        'name' => 'm03_work_p33edproduct_ida',
        'type' => 'id',
      ),
      'm03_work_p934fspectio_idb' => 
      array (
        'name' => 'm03_work_p934fspectio_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_a1a_critical_phase_inspectio_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_a1a_critical_phase_inspectio_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p33edproduct_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_a1a_critical_phase_inspectio_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p934fspectio_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_a1a_critical_phase_inspectio_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_p934fspectio_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'A1A_Critical_Phase_Inspectio',
    'rhs_table' => 'a1a_critical_phase_inspectio',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_a1a_critical_phase_inspectio_1_c',
    'join_key_lhs' => 'm03_work_p33edproduct_ida',
    'join_key_rhs' => 'm03_work_p934fspectio_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_a1a_critical_phase_inspectio_1',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_a1a_critical_phase_inspectio_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_wpe_work_product_enrollment_1' => 
  array (
    'name' => 'm03_work_product_wpe_work_product_enrollment_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_wpe_work_product_enrollment_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'WPE_Work_Product_Enrollment',
        'rhs_table' => 'wpe_work_product_enrollment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_wpe_work_product_enrollment_1_c',
        'join_key_lhs' => 'm03_work_p7d13product_ida',
        'join_key_rhs' => 'm03_work_p9bf5ollment_idb',
      ),
    ),
    'table' => 'm03_work_product_wpe_work_product_enrollment_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_p7d13product_ida' => 
      array (
        'name' => 'm03_work_p7d13product_ida',
        'type' => 'id',
      ),
      'm03_work_p9bf5ollment_idb' => 
      array (
        'name' => 'm03_work_p9bf5ollment_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_wpe_work_product_enrollment_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_wpe_work_product_enrollment_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p7d13product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_wpe_work_product_enrollment_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p9bf5ollment_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_wpe_work_product_enrollment_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_p9bf5ollment_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'WPE_Work_Product_Enrollment',
    'rhs_table' => 'wpe_work_product_enrollment',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_wpe_work_product_enrollment_1_c',
    'join_key_lhs' => 'm03_work_p7d13product_ida',
    'join_key_rhs' => 'm03_work_p9bf5ollment_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_wpe_work_product_enrollment_1',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_wpe_work_product_enrollment_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm06_error_m03_work_product_1' => 
  array (
    'name' => 'm06_error_m03_work_product_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_m03_work_product_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product',
        'rhs_table' => 'm03_work_product',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_m03_work_product_1_c',
        'join_key_lhs' => 'm06_error_m03_work_product_1m06_error_ida',
        'join_key_rhs' => 'm06_error_m03_work_product_1m03_work_product_idb',
      ),
    ),
    'table' => 'm06_error_m03_work_product_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_m03_work_product_1m06_error_ida' => 
      array (
        'name' => 'm06_error_m03_work_product_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_m03_work_product_1m03_work_product_idb' => 
      array (
        'name' => 'm06_error_m03_work_product_1m03_work_product_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_m03_work_product_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_m03_work_product_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_m03_work_product_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_m03_work_product_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_m03_work_product_1m03_work_product_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_m03_work_product_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_m03_work_product_1m06_error_ida',
          1 => 'm06_error_m03_work_product_1m03_work_product_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm06_error_m03_work_product_1_c',
    'join_key_lhs' => 'm06_error_m03_work_product_1m06_error_ida',
    'join_key_rhs' => 'm06_error_m03_work_product_1m03_work_product_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_m03_work_product_1',
    'rhs_subpanel' => 'ForM06_errorM06_error_m03_work_product_1',
    'lhs_subpanel' => 'ForM03_work_productM06_error_m03_work_product_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm99_protocol_amendments_m03_work_product' => 
  array (
    'name' => 'm99_protocol_amendments_m03_work_product',
    'true_relationship_type' => 'many-to-many',
    'relationships' => 
    array (
      'm99_protocol_amendments_m03_work_product' => 
      array (
        'lhs_module' => 'M99_Protocol_Amendments',
        'lhs_table' => 'm99_protocol_amendments',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product',
        'rhs_table' => 'm03_work_product',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm99_protocol_amendments_m03_work_product_c',
        'join_key_lhs' => 'm99_protoc2862ndments_ida',
        'join_key_rhs' => 'm99_protocol_amendments_m03_work_productm03_work_product_idb',
      ),
    ),
    'table' => 'm99_protocol_amendments_m03_work_product_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm99_protoc2862ndments_ida' => 
      array (
        'name' => 'm99_protoc2862ndments_ida',
        'type' => 'id',
      ),
      'm99_protocol_amendments_m03_work_productm03_work_product_idb' => 
      array (
        'name' => 'm99_protocol_amendments_m03_work_productm03_work_product_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m99_protocol_amendments_m03_work_product_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m99_protocol_amendments_m03_work_product_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm99_protoc2862ndments_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m99_protocol_amendments_m03_work_product_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm99_protocol_amendments_m03_work_productm03_work_product_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm99_protocol_amendments_m03_work_product_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm99_protoc2862ndments_ida',
          1 => 'm99_protocol_amendments_m03_work_productm03_work_product_idb',
        ),
      ),
    ),
    'lhs_module' => 'M99_Protocol_Amendments',
    'lhs_table' => 'm99_protocol_amendments',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm99_protocol_amendments_m03_work_product_c',
    'join_key_lhs' => 'm99_protoc2862ndments_ida',
    'join_key_rhs' => 'm99_protocol_amendments_m03_work_productm03_work_product_idb',
    'readonly' => true,
    'relationship_name' => 'm99_protocol_amendments_m03_work_product',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'm03_work_product_sw_study_workflow_1' => 
  array (
    'name' => 'm03_work_product_sw_study_workflow_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_sw_study_workflow_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'SW_Study_Workflow',
        'rhs_table' => 'sw_study_workflow',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_sw_study_workflow_1_c',
        'join_key_lhs' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_product_sw_study_workflow_1sw_study_workflow_idb',
      ),
    ),
    'table' => 'm03_work_product_sw_study_workflow_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_sw_study_workflow_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_product_sw_study_workflow_1sw_study_workflow_idb' => 
      array (
        'name' => 'm03_work_product_sw_study_workflow_1sw_study_workflow_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_sw_study_workflow_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_sw_study_workflow_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_sw_study_workflow_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_sw_study_workflow_1sw_study_workflow_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_sw_study_workflow_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_sw_study_workflow_1sw_study_workflow_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'SW_Study_Workflow',
    'rhs_table' => 'sw_study_workflow',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_sw_study_workflow_1_c',
    'join_key_lhs' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_product_sw_study_workflow_1sw_study_workflow_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_sw_study_workflow_1',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_sw_study_workflow_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'contacts_m03_work_product_1' => 
  array (
    'name' => 'contacts_m03_work_product_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'contacts_m03_work_product_1' => 
      array (
        'lhs_module' => 'Contacts',
        'lhs_table' => 'contacts',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product',
        'rhs_table' => 'm03_work_product',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'contacts_m03_work_product_1_c',
        'join_key_lhs' => 'contacts_m03_work_product_1contacts_ida',
        'join_key_rhs' => 'contacts_m03_work_product_1m03_work_product_idb',
      ),
    ),
    'table' => 'contacts_m03_work_product_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'contacts_m03_work_product_1contacts_ida' => 
      array (
        'name' => 'contacts_m03_work_product_1contacts_ida',
        'type' => 'id',
      ),
      'contacts_m03_work_product_1m03_work_product_idb' => 
      array (
        'name' => 'contacts_m03_work_product_1m03_work_product_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_contacts_m03_work_product_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_contacts_m03_work_product_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'contacts_m03_work_product_1contacts_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_contacts_m03_work_product_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'contacts_m03_work_product_1m03_work_product_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'contacts_m03_work_product_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'contacts_m03_work_product_1m03_work_product_idb',
        ),
      ),
    ),
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'contacts_m03_work_product_1_c',
    'join_key_lhs' => 'contacts_m03_work_product_1contacts_ida',
    'join_key_rhs' => 'contacts_m03_work_product_1m03_work_product_idb',
    'readonly' => true,
    'relationship_name' => 'contacts_m03_work_product_1',
    'rhs_subpanel' => 'ForContactsContacts_m03_work_product_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'accounts_m03_work_product_1' => 
  array (
    'name' => 'accounts_m03_work_product_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'accounts_m03_work_product_1' => 
      array (
        'lhs_module' => 'Accounts',
        'lhs_table' => 'accounts',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product',
        'rhs_table' => 'm03_work_product',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'accounts_m03_work_product_1_c',
        'join_key_lhs' => 'accounts_m03_work_product_1accounts_ida',
        'join_key_rhs' => 'accounts_m03_work_product_1m03_work_product_idb',
      ),
    ),
    'table' => 'accounts_m03_work_product_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'accounts_m03_work_product_1accounts_ida' => 
      array (
        'name' => 'accounts_m03_work_product_1accounts_ida',
        'type' => 'id',
      ),
      'accounts_m03_work_product_1m03_work_product_idb' => 
      array (
        'name' => 'accounts_m03_work_product_1m03_work_product_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_accounts_m03_work_product_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_accounts_m03_work_product_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'accounts_m03_work_product_1accounts_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_accounts_m03_work_product_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'accounts_m03_work_product_1m03_work_product_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'accounts_m03_work_product_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'accounts_m03_work_product_1m03_work_product_idb',
        ),
      ),
    ),
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'accounts_m03_work_product_1_c',
    'join_key_lhs' => 'accounts_m03_work_product_1accounts_ida',
    'join_key_rhs' => 'accounts_m03_work_product_1m03_work_product_idb',
    'readonly' => true,
    'relationship_name' => 'accounts_m03_work_product_1',
    'rhs_subpanel' => 'ForAccountsAccounts_m03_work_product_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'rr_regulatory_response_m03_work_product_1' => 
  array (
    'name' => 'rr_regulatory_response_m03_work_product_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'rr_regulatory_response_m03_work_product_1' => 
      array (
        'lhs_module' => 'RR_Regulatory_Response',
        'lhs_table' => 'rr_regulatory_response',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product',
        'rhs_table' => 'm03_work_product',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'rr_regulatory_response_m03_work_product_1_c',
        'join_key_lhs' => 'rr_regulat4120esponse_ida',
        'join_key_rhs' => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
      ),
    ),
    'table' => 'rr_regulatory_response_m03_work_product_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'rr_regulat4120esponse_ida' => 
      array (
        'name' => 'rr_regulat4120esponse_ida',
        'type' => 'id',
      ),
      'rr_regulatory_response_m03_work_product_1m03_work_product_idb' => 
      array (
        'name' => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_rr_regulatory_response_m03_work_product_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_rr_regulatory_response_m03_work_product_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'rr_regulat4120esponse_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_rr_regulatory_response_m03_work_product_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'rr_regulatory_response_m03_work_product_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'rr_regulat4120esponse_ida',
          1 => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
        ),
      ),
    ),
    'lhs_module' => 'RR_Regulatory_Response',
    'lhs_table' => 'rr_regulatory_response',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'rr_regulatory_response_m03_work_product_1_c',
    'join_key_lhs' => 'rr_regulat4120esponse_ida',
    'join_key_rhs' => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
    'readonly' => true,
    'relationship_name' => 'rr_regulatory_response_m03_work_product_1',
    'rhs_subpanel' => 'ForRr_regulatory_responseRr_regulatory_response_m03_work_product_1',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_activities_1_calls' => 
  array (
    'name' => 'm03_work_product_activities_1_calls',
    'relationships' => 
    array (
      'm03_work_product_activities_1_calls' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'Calls',
        'rhs_table' => 'calls',
        'relationship_role_column_value' => 'M03_Work_Product',
        'rhs_key' => 'parent_id',
        'relationship_type' => 'one-to-many',
        'relationship_role_column' => 'parent_type',
      ),
    ),
    'fields' => '',
    'indices' => '',
    'table' => '',
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'Calls',
    'rhs_table' => 'calls',
    'relationship_role_column_value' => 'M03_Work_Product',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_activities_1_calls',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'm03_work_product_activities_1_meetings' => 
  array (
    'name' => 'm03_work_product_activities_1_meetings',
    'relationships' => 
    array (
      'm03_work_product_activities_1_meetings' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'Meetings',
        'rhs_table' => 'meetings',
        'relationship_role_column_value' => 'M03_Work_Product',
        'rhs_key' => 'parent_id',
        'relationship_type' => 'one-to-many',
        'relationship_role_column' => 'parent_type',
      ),
    ),
    'fields' => '',
    'indices' => '',
    'table' => '',
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'relationship_role_column_value' => 'M03_Work_Product',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_activities_1_meetings',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'ForMeetingsMeetings_m03_work_product_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'm03_work_product_activities_1_notes' => 
  array (
    'name' => 'm03_work_product_activities_1_notes',
    'relationships' => 
    array (
      'm03_work_product_activities_1_notes' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'Notes',
        'rhs_table' => 'notes',
        'relationship_role_column_value' => 'M03_Work_Product',
        'rhs_key' => 'parent_id',
        'relationship_type' => 'one-to-many',
        'relationship_role_column' => 'parent_type',
      ),
    ),
    'fields' => '',
    'indices' => '',
    'table' => '',
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'Notes',
    'rhs_table' => 'notes',
    'relationship_role_column_value' => 'M03_Work_Product',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_activities_1_notes',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'm03_work_product_activities_1_tasks' => 
  array (
    'name' => 'm03_work_product_activities_1_tasks',
    'relationships' => 
    array (
      'm03_work_product_activities_1_tasks' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'Tasks',
        'rhs_table' => 'tasks',
        'relationship_role_column_value' => 'M03_Work_Product',
        'rhs_key' => 'parent_id',
        'relationship_type' => 'one-to-many',
        'relationship_role_column' => 'parent_type',
      ),
    ),
    'fields' => '',
    'indices' => '',
    'table' => '',
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'Tasks',
    'rhs_table' => 'tasks',
    'relationship_role_column_value' => 'M03_Work_Product',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_activities_1_tasks',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'ForTasksTasks_m03_work_product_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'm03_work_product_edoc_email_documents_1' => 
  array (
    'name' => 'm03_work_product_edoc_email_documents_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_edoc_email_documents_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'EDoc_Email_Documents',
        'rhs_table' => 'edoc_email_documents',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_edoc_email_documents_1_c',
        'join_key_lhs' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
      ),
    ),
    'table' => 'm03_work_product_edoc_email_documents_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_edoc_email_documents_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_product_edoc_email_documents_1edoc_email_documents_idb' => 
      array (
        'name' => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_edoc_email_documents_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_edoc_email_documents_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_edoc_email_documents_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_edoc_email_documents_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'EDoc_Email_Documents',
    'rhs_table' => 'edoc_email_documents',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_edoc_email_documents_1_c',
    'join_key_lhs' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_edoc_email_documents_1',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_edoc_email_documents_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_erd_error_documents_1' => 
  array (
    'name' => 'm03_work_product_erd_error_documents_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_erd_error_documents_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'Erd_Error_Documents',
        'rhs_table' => 'erd_error_documents',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_erd_error_documents_1_c',
        'join_key_lhs' => 'm03_work_product_erd_error_documents_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_product_erd_error_documents_1erd_error_documents_idb',
      ),
    ),
    'table' => 'm03_work_product_erd_error_documents_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_erd_error_documents_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_erd_error_documents_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_product_erd_error_documents_1erd_error_documents_idb' => 
      array (
        'name' => 'm03_work_product_erd_error_documents_1erd_error_documents_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_erd_error_documents_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_erd_error_documents_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_erd_error_documents_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_erd_error_documents_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_erd_error_documents_1erd_error_documents_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_erd_error_documents_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_erd_error_documents_1m03_work_product_ida',
          1 => 'm03_work_product_erd_error_documents_1erd_error_documents_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'Erd_Error_Documents',
    'rhs_table' => 'erd_error_documents',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm03_work_product_erd_error_documents_1_c',
    'join_key_lhs' => 'm03_work_product_erd_error_documents_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_product_erd_error_documents_1erd_error_documents_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_erd_error_documents_1',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_erd_error_documents_1',
    'lhs_subpanel' => 'ForErd_error_documentsM03_work_product_erd_error_documents_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_wpe_work_product_enrollment_2' => 
  array (
    'name' => 'm03_work_product_wpe_work_product_enrollment_2',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_wpe_work_product_enrollment_2' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'WPE_Work_Product_Enrollment',
        'rhs_table' => 'wpe_work_product_enrollment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_wpe_work_product_enrollment_2_c',
        'join_key_lhs' => 'm03_work_p9f23product_ida',
        'join_key_rhs' => 'm03_work_p90c4ollment_idb',
      ),
    ),
    'table' => 'm03_work_product_wpe_work_product_enrollment_2_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_p9f23product_ida' => 
      array (
        'name' => 'm03_work_p9f23product_ida',
        'type' => 'id',
      ),
      'm03_work_p90c4ollment_idb' => 
      array (
        'name' => 'm03_work_p90c4ollment_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_wpe_work_product_enrollment_2_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_wpe_work_product_enrollment_2_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p9f23product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_wpe_work_product_enrollment_2_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p90c4ollment_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_wpe_work_product_enrollment_2_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_p90c4ollment_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'WPE_Work_Product_Enrollment',
    'rhs_table' => 'wpe_work_product_enrollment',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_wpe_work_product_enrollment_2_c',
    'join_key_lhs' => 'm03_work_p9f23product_ida',
    'join_key_rhs' => 'm03_work_p90c4ollment_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_wpe_work_product_enrollment_2',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_wpe_work_product_enrollment_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_m03_work_product_code_1' => 
  array (
    'name' => 'm03_work_product_m03_work_product_code_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_m03_work_product_code_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product_Code',
        'rhs_table' => 'm03_work_product_code',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_m03_work_product_code_1_c',
        'join_key_lhs' => 'm03_work_product_m03_work_product_code_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_p5357ct_code_idb',
      ),
    ),
    'table' => 'm03_work_product_m03_work_product_code_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_m03_work_product_code_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_m03_work_product_code_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_p5357ct_code_idb' => 
      array (
        'name' => 'm03_work_p5357ct_code_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_m03_work_product_code_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_m03_work_product_code_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_m03_work_product_code_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_m03_work_product_code_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p5357ct_code_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_m03_work_product_code_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_m03_work_product_code_1m03_work_product_ida',
          1 => 'm03_work_p5357ct_code_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Code',
    'rhs_table' => 'm03_work_product_code',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm03_work_product_m03_work_product_code_1_c',
    'join_key_lhs' => 'm03_work_product_m03_work_product_code_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_p5357ct_code_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_m03_work_product_code_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'ForM03_work_product_codeM03_work_product_m03_work_product_code_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_tsd1_test_system_design_1_1' => 
  array (
    'name' => 'm03_work_product_tsd1_test_system_design_1_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_tsd1_test_system_design_1_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'TSD1_Test_System_Design_1',
        'rhs_table' => 'tsd1_test_system_design_1',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_tsd1_test_system_design_1_1_c',
        'join_key_lhs' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_p4745esign_1_idb',
      ),
    ),
    'table' => 'm03_work_product_tsd1_test_system_design_1_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_p4745esign_1_idb' => 
      array (
        'name' => 'm03_work_p4745esign_1_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_tsd1_test_system_design_1_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_tsd1_test_system_design_1_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_tsd1_test_system_design_1_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p4745esign_1_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_tsd1_test_system_design_1_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_p4745esign_1_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'TSD1_Test_System_Design_1',
    'rhs_table' => 'tsd1_test_system_design_1',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_tsd1_test_system_design_1_1_c',
    'join_key_lhs' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_p4745esign_1_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_tsd1_test_system_design_1_1',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_tsd1_test_system_design_1_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'meetings_m03_work_product_1' => 
  array (
    'name' => 'meetings_m03_work_product_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'meetings_m03_work_product_1' => 
      array (
        'lhs_module' => 'Meetings',
        'lhs_table' => 'meetings',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product',
        'rhs_table' => 'm03_work_product',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'meetings_m03_work_product_1_c',
        'join_key_lhs' => 'meetings_m03_work_product_1meetings_ida',
        'join_key_rhs' => 'meetings_m03_work_product_1m03_work_product_idb',
      ),
    ),
    'table' => 'meetings_m03_work_product_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'meetings_m03_work_product_1meetings_ida' => 
      array (
        'name' => 'meetings_m03_work_product_1meetings_ida',
        'type' => 'id',
      ),
      'meetings_m03_work_product_1m03_work_product_idb' => 
      array (
        'name' => 'meetings_m03_work_product_1m03_work_product_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_meetings_m03_work_product_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_meetings_m03_work_product_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'meetings_m03_work_product_1meetings_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_meetings_m03_work_product_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'meetings_m03_work_product_1m03_work_product_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'meetings_m03_work_product_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'meetings_m03_work_product_1meetings_ida',
          1 => 'meetings_m03_work_product_1m03_work_product_idb',
        ),
      ),
    ),
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'meetings_m03_work_product_1_c',
    'join_key_lhs' => 'meetings_m03_work_product_1meetings_ida',
    'join_key_rhs' => 'meetings_m03_work_product_1m03_work_product_idb',
    'readonly' => true,
    'relationship_name' => 'meetings_m03_work_product_1',
    'rhs_subpanel' => 'ForMeetingsMeetings_m03_work_product_1',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_gd_group_design_1' => 
  array (
    'name' => 'm03_work_product_gd_group_design_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_gd_group_design_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'GD_Group_Design',
        'rhs_table' => 'gd_group_design',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_gd_group_design_1_c',
        'join_key_lhs' => 'm03_work_product_gd_group_design_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_product_gd_group_design_1gd_group_design_idb',
      ),
    ),
    'table' => 'm03_work_product_gd_group_design_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_gd_group_design_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_gd_group_design_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_product_gd_group_design_1gd_group_design_idb' => 
      array (
        'name' => 'm03_work_product_gd_group_design_1gd_group_design_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_gd_group_design_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_gd_group_design_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_gd_group_design_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_gd_group_design_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_gd_group_design_1gd_group_design_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_gd_group_design_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_gd_group_design_1gd_group_design_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'GD_Group_Design',
    'rhs_table' => 'gd_group_design',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_gd_group_design_1_c',
    'join_key_lhs' => 'm03_work_product_gd_group_design_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_product_gd_group_design_1gd_group_design_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_gd_group_design_1',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_gd_group_design_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_taskd_task_design_1' => 
  array (
    'name' => 'm03_work_product_taskd_task_design_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_taskd_task_design_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'TaskD_Task_Design',
        'rhs_table' => 'taskd_task_design',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_taskd_task_design_1_c',
        'join_key_lhs' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_product_taskd_task_design_1taskd_task_design_idb',
      ),
    ),
    'table' => 'm03_work_product_taskd_task_design_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_taskd_task_design_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_product_taskd_task_design_1taskd_task_design_idb' => 
      array (
        'name' => 'm03_work_product_taskd_task_design_1taskd_task_design_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_taskd_task_design_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_taskd_task_design_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_taskd_task_design_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_taskd_task_design_1taskd_task_design_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_taskd_task_design_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_taskd_task_design_1taskd_task_design_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'TaskD_Task_Design',
    'rhs_table' => 'taskd_task_design',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_taskd_task_design_1_c',
    'join_key_lhs' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_product_taskd_task_design_1taskd_task_design_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_taskd_task_design_1',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_taskd_task_design_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_ori_order_request_item_1' => 
  array (
    'name' => 'm03_work_product_ori_order_request_item_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_ori_order_request_item_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'ORI_Order_Request_Item',
        'rhs_table' => 'ori_order_request_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_ori_order_request_item_1_c',
        'join_key_lhs' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_p8743st_item_idb',
      ),
    ),
    'table' => 'm03_work_product_ori_order_request_item_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_ori_order_request_item_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_p8743st_item_idb' => 
      array (
        'name' => 'm03_work_p8743st_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_ori_order_request_item_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_ori_order_request_item_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_ori_order_request_item_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p8743st_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_ori_order_request_item_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_p8743st_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'ORI_Order_Request_Item',
    'rhs_table' => 'ori_order_request_item',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_ori_order_request_item_1_c',
    'join_key_lhs' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_p8743st_item_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_ori_order_request_item_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_poi_purchase_order_item_1' => 
  array (
    'name' => 'm03_work_product_poi_purchase_order_item_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_poi_purchase_order_item_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'POI_Purchase_Order_Item',
        'rhs_table' => 'poi_purchase_order_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_poi_purchase_order_item_1_c',
        'join_key_lhs' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_p0605er_item_idb',
      ),
    ),
    'table' => 'm03_work_product_poi_purchase_order_item_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_poi_purchase_order_item_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_p0605er_item_idb' => 
      array (
        'name' => 'm03_work_p0605er_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_poi_purchase_order_item_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_poi_purchase_order_item_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_poi_purchase_order_item_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p0605er_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_poi_purchase_order_item_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_p0605er_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'POI_Purchase_Order_Item',
    'rhs_table' => 'poi_purchase_order_item',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_poi_purchase_order_item_1_c',
    'join_key_lhs' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_p0605er_item_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_poi_purchase_order_item_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_modified_user' => 
  array (
    'name' => 'm03_work_product_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_created_by' => 
  array (
    'name' => 'm03_work_product_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_activities' => 
  array (
    'name' => 'm03_work_product_activities',
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'M03_Work_Product',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'm03_work_product_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_following' => 
  array (
    'name' => 'm03_work_product_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'M03_Work_Product',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_favorite' => 
  array (
    'name' => 'm03_work_product_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'M03_Work_Product',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_assigned_user' => 
  array (
    'name' => 'm03_work_product_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_ii_inventory_item_1' => 
  array (
    'name' => 'm03_work_product_ii_inventory_item_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_ii_inventory_item_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'II_Inventory_Item',
        'rhs_table' => 'ii_inventory_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_ii_inventory_item_1_c',
        'join_key_lhs' => 'm03_work_product_ii_inventory_item_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_product_ii_inventory_item_1ii_inventory_item_idb',
      ),
    ),
    'table' => 'm03_work_product_ii_inventory_item_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_ii_inventory_item_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_ii_inventory_item_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_product_ii_inventory_item_1ii_inventory_item_idb' => 
      array (
        'name' => 'm03_work_product_ii_inventory_item_1ii_inventory_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_ii_inventory_item_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_ii_inventory_item_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_ii_inventory_item_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_ii_inventory_item_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_ii_inventory_item_1ii_inventory_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_ii_inventory_item_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_ii_inventory_item_1ii_inventory_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'II_Inventory_Item',
    'rhs_table' => 'ii_inventory_item',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_ii_inventory_item_1_c',
    'join_key_lhs' => 'm03_work_product_ii_inventory_item_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_product_ii_inventory_item_1ii_inventory_item_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_ii_inventory_item_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_ii_inventory_item_2' => 
  array (
    'name' => 'm03_work_product_ii_inventory_item_2',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_ii_inventory_item_2' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'II_Inventory_Item',
        'rhs_table' => 'ii_inventory_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_ii_inventory_item_2_c',
        'join_key_lhs' => 'm03_work_product_ii_inventory_item_2m03_work_product_ida',
        'join_key_rhs' => 'm03_work_product_ii_inventory_item_2ii_inventory_item_idb',
      ),
    ),
    'table' => 'm03_work_product_ii_inventory_item_2_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_ii_inventory_item_2m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_ii_inventory_item_2m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_product_ii_inventory_item_2ii_inventory_item_idb' => 
      array (
        'name' => 'm03_work_product_ii_inventory_item_2ii_inventory_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_ii_inventory_item_2_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_ii_inventory_item_2_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_ii_inventory_item_2m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_ii_inventory_item_2_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_ii_inventory_item_2ii_inventory_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_ii_inventory_item_2_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_ii_inventory_item_2m03_work_product_ida',
          1 => 'm03_work_product_ii_inventory_item_2ii_inventory_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'II_Inventory_Item',
    'rhs_table' => 'ii_inventory_item',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm03_work_product_ii_inventory_item_2_c',
    'join_key_lhs' => 'm03_work_product_ii_inventory_item_2m03_work_product_ida',
    'join_key_rhs' => 'm03_work_product_ii_inventory_item_2ii_inventory_item_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_ii_inventory_item_2',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_im_inventory_management_1' => 
  array (
    'name' => 'm03_work_product_im_inventory_management_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_im_inventory_management_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'IM_Inventory_Management',
        'rhs_table' => 'im_inventory_management',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_im_inventory_management_1_c',
        'join_key_lhs' => 'm03_work_product_im_inventory_management_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_p071bagement_idb',
      ),
    ),
    'table' => 'm03_work_product_im_inventory_management_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_im_inventory_management_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_im_inventory_management_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_p071bagement_idb' => 
      array (
        'name' => 'm03_work_p071bagement_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_im_inventory_management_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_im_inventory_management_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_im_inventory_management_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_im_inventory_management_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p071bagement_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_im_inventory_management_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_p071bagement_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'IM_Inventory_Management',
    'rhs_table' => 'im_inventory_management',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_im_inventory_management_1_c',
    'join_key_lhs' => 'm03_work_product_im_inventory_management_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_p071bagement_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_im_inventory_management_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_ic_inventory_collection_1' => 
  array (
    'rhs_label' => 'Inventory Collections',
    'lhs_label' => 'Work Products',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'M03_Work_Product',
    'rhs_module' => 'IC_Inventory_Collection',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'm03_work_product_ic_inventory_collection_1',
  ),
);