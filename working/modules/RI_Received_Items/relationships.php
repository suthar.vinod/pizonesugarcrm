<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'ori_order_request_item_ri_received_items_1' => 
  array (
    'name' => 'ori_order_request_item_ri_received_items_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'ori_order_request_item_ri_received_items_1' => 
      array (
        'lhs_module' => 'ORI_Order_Request_Item',
        'lhs_table' => 'ori_order_request_item',
        'lhs_key' => 'id',
        'rhs_module' => 'RI_Received_Items',
        'rhs_table' => 'ri_received_items',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'ori_order_request_item_ri_received_items_1_c',
        'join_key_lhs' => 'ori_order_6b32st_item_ida',
        'join_key_rhs' => 'ori_order_request_item_ri_received_items_1ri_received_items_idb',
      ),
    ),
    'table' => 'ori_order_request_item_ri_received_items_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'ori_order_6b32st_item_ida' => 
      array (
        'name' => 'ori_order_6b32st_item_ida',
        'type' => 'id',
      ),
      'ori_order_request_item_ri_received_items_1ri_received_items_idb' => 
      array (
        'name' => 'ori_order_request_item_ri_received_items_1ri_received_items_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_ori_order_request_item_ri_received_items_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_ori_order_request_item_ri_received_items_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'ori_order_6b32st_item_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_ori_order_request_item_ri_received_items_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'ori_order_request_item_ri_received_items_1ri_received_items_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'ori_order_request_item_ri_received_items_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'ori_order_request_item_ri_received_items_1ri_received_items_idb',
        ),
      ),
    ),
    'lhs_module' => 'ORI_Order_Request_Item',
    'lhs_table' => 'ori_order_request_item',
    'lhs_key' => 'id',
    'rhs_module' => 'RI_Received_Items',
    'rhs_table' => 'ri_received_items',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'ori_order_request_item_ri_received_items_1_c',
    'join_key_lhs' => 'ori_order_6b32st_item_ida',
    'join_key_rhs' => 'ori_order_request_item_ri_received_items_1ri_received_items_idb',
    'readonly' => true,
    'relationship_name' => 'ori_order_request_item_ri_received_items_1',
    'rhs_subpanel' => 'ForOri_order_request_itemOri_order_request_item_ri_received_items_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'poi_purchase_order_item_ri_received_items_1' => 
  array (
    'name' => 'poi_purchase_order_item_ri_received_items_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'poi_purchase_order_item_ri_received_items_1' => 
      array (
        'lhs_module' => 'POI_Purchase_Order_Item',
        'lhs_table' => 'poi_purchase_order_item',
        'lhs_key' => 'id',
        'rhs_module' => 'RI_Received_Items',
        'rhs_table' => 'ri_received_items',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'poi_purchase_order_item_ri_received_items_1_c',
        'join_key_lhs' => 'poi_purcha665cer_item_ida',
        'join_key_rhs' => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
      ),
    ),
    'table' => 'poi_purchase_order_item_ri_received_items_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'poi_purcha665cer_item_ida' => 
      array (
        'name' => 'poi_purcha665cer_item_ida',
        'type' => 'id',
      ),
      'poi_purchase_order_item_ri_received_items_1ri_received_items_idb' => 
      array (
        'name' => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_poi_purchase_order_item_ri_received_items_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_poi_purchase_order_item_ri_received_items_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'poi_purcha665cer_item_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_poi_purchase_order_item_ri_received_items_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'poi_purchase_order_item_ri_received_items_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
        ),
      ),
    ),
    'lhs_module' => 'POI_Purchase_Order_Item',
    'lhs_table' => 'poi_purchase_order_item',
    'lhs_key' => 'id',
    'rhs_module' => 'RI_Received_Items',
    'rhs_table' => 'ri_received_items',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'poi_purchase_order_item_ri_received_items_1_c',
    'join_key_lhs' => 'poi_purcha665cer_item_ida',
    'join_key_rhs' => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
    'readonly' => true,
    'relationship_name' => 'poi_purchase_order_item_ri_received_items_1',
    'rhs_subpanel' => 'ForPoi_purchase_order_itemPoi_purchase_order_item_ri_received_items_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'ri_received_items_modified_user' => 
  array (
    'name' => 'ri_received_items_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RI_Received_Items',
    'rhs_table' => 'ri_received_items',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'ri_received_items_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ri_received_items_created_by' => 
  array (
    'name' => 'ri_received_items_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RI_Received_Items',
    'rhs_table' => 'ri_received_items',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'ri_received_items_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ri_received_items_activities' => 
  array (
    'name' => 'ri_received_items_activities',
    'lhs_module' => 'RI_Received_Items',
    'lhs_table' => 'ri_received_items',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'RI_Received_Items',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'ri_received_items_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ri_received_items_following' => 
  array (
    'name' => 'ri_received_items_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RI_Received_Items',
    'rhs_table' => 'ri_received_items',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'RI_Received_Items',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'ri_received_items_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ri_received_items_favorite' => 
  array (
    'name' => 'ri_received_items_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RI_Received_Items',
    'rhs_table' => 'ri_received_items',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'RI_Received_Items',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'ri_received_items_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ri_received_items_assigned_user' => 
  array (
    'name' => 'ri_received_items_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RI_Received_Items',
    'rhs_table' => 'ri_received_items',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'ri_received_items_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ri_received_items_ii_inventory_item_1' => 
  array (
    'rhs_label' => 'Inventory Items',
    'lhs_label' => 'Received Items',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'RI_Received_Items',
    'rhs_module' => 'II_Inventory_Item',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'ri_received_items_ii_inventory_item_1',
  ),
);