<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'anml_animals_w_weight_1' => 
  array (
    'name' => 'anml_animals_w_weight_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'anml_animals_w_weight_1' => 
      array (
        'lhs_module' => 'ANML_Animals',
        'lhs_table' => 'anml_animals',
        'lhs_key' => 'id',
        'rhs_module' => 'W_Weight',
        'rhs_table' => 'w_weight',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'anml_animals_w_weight_1_c',
        'join_key_lhs' => 'anml_animals_w_weight_1anml_animals_ida',
        'join_key_rhs' => 'anml_animals_w_weight_1w_weight_idb',
      ),
    ),
    'table' => 'anml_animals_w_weight_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'anml_animals_w_weight_1anml_animals_ida' => 
      array (
        'name' => 'anml_animals_w_weight_1anml_animals_ida',
        'type' => 'id',
      ),
      'anml_animals_w_weight_1w_weight_idb' => 
      array (
        'name' => 'anml_animals_w_weight_1w_weight_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_anml_animals_w_weight_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_anml_animals_w_weight_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_w_weight_1anml_animals_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_anml_animals_w_weight_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_w_weight_1w_weight_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'anml_animals_w_weight_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'anml_animals_w_weight_1w_weight_idb',
        ),
      ),
    ),
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'W_Weight',
    'rhs_table' => 'w_weight',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'anml_animals_w_weight_1_c',
    'join_key_lhs' => 'anml_animals_w_weight_1anml_animals_ida',
    'join_key_rhs' => 'anml_animals_w_weight_1w_weight_idb',
    'readonly' => true,
    'relationship_name' => 'anml_animals_w_weight_1',
    'rhs_subpanel' => 'ForAnml_animalsAnml_animals_w_weight_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'w_weight_m06_error_1' => 
  array (
    'name' => 'w_weight_m06_error_1',
    'true_relationship_type' => 'one-to-one',
    'from_studio' => true,
    'relationships' => 
    array (
      'w_weight_m06_error_1' => 
      array (
        'lhs_module' => 'W_Weight',
        'lhs_table' => 'w_weight',
        'lhs_key' => 'id',
        'rhs_module' => 'M06_Error',
        'rhs_table' => 'm06_error',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'w_weight_m06_error_1_c',
        'join_key_lhs' => 'w_weight_m06_error_1w_weight_ida',
        'join_key_rhs' => 'w_weight_m06_error_1m06_error_idb',
      ),
    ),
    'table' => 'w_weight_m06_error_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'w_weight_m06_error_1w_weight_ida' => 
      array (
        'name' => 'w_weight_m06_error_1w_weight_ida',
        'type' => 'id',
      ),
      'w_weight_m06_error_1m06_error_idb' => 
      array (
        'name' => 'w_weight_m06_error_1m06_error_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_w_weight_m06_error_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_w_weight_m06_error_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'w_weight_m06_error_1w_weight_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_w_weight_m06_error_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'w_weight_m06_error_1m06_error_idb',
          1 => 'deleted',
        ),
      ),
    ),
    'lhs_module' => 'W_Weight',
    'lhs_table' => 'w_weight',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-one',
    'join_table' => 'w_weight_m06_error_1_c',
    'join_key_lhs' => 'w_weight_m06_error_1w_weight_ida',
    'join_key_rhs' => 'w_weight_m06_error_1m06_error_idb',
    'readonly' => true,
    'relationship_name' => 'w_weight_m06_error_1',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'w_weight_modified_user' => 
  array (
    'name' => 'w_weight_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'W_Weight',
    'rhs_table' => 'w_weight',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'w_weight_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'w_weight_created_by' => 
  array (
    'name' => 'w_weight_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'W_Weight',
    'rhs_table' => 'w_weight',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'w_weight_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'w_weight_activities' => 
  array (
    'name' => 'w_weight_activities',
    'lhs_module' => 'W_Weight',
    'lhs_table' => 'w_weight',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'W_Weight',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'w_weight_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'w_weight_following' => 
  array (
    'name' => 'w_weight_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'W_Weight',
    'rhs_table' => 'w_weight',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'W_Weight',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'w_weight_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'w_weight_favorite' => 
  array (
    'name' => 'w_weight_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'W_Weight',
    'rhs_table' => 'w_weight',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'W_Weight',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'w_weight_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'w_weight_assigned_user' => 
  array (
    'name' => 'w_weight_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'W_Weight',
    'rhs_table' => 'w_weight',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'w_weight_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'w_weight_m06_error_2' => 
  array (
    'rhs_label' => 'Communications',
    'lhs_label' => 'Weights',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'W_Weight',
    'rhs_module' => 'M06_Error',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'w_weight_m06_error_2',
  ),
);