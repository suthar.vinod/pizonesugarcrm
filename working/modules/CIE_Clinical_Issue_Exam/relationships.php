<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'cie_clinical_issue_exam_modified_user' => 
  array (
    'name' => 'cie_clinical_issue_exam_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'CIE_Clinical_Issue_Exam',
    'rhs_table' => 'cie_clinical_issue_exam',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'cie_clinical_issue_exam_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'cie_clinical_issue_exam_created_by' => 
  array (
    'name' => 'cie_clinical_issue_exam_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'CIE_Clinical_Issue_Exam',
    'rhs_table' => 'cie_clinical_issue_exam',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'cie_clinical_issue_exam_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'cie_clinical_issue_exam_activities' => 
  array (
    'name' => 'cie_clinical_issue_exam_activities',
    'lhs_module' => 'CIE_Clinical_Issue_Exam',
    'lhs_table' => 'cie_clinical_issue_exam',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'CIE_Clinical_Issue_Exam',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'cie_clinical_issue_exam_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'cie_clinical_issue_exam_following' => 
  array (
    'name' => 'cie_clinical_issue_exam_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'CIE_Clinical_Issue_Exam',
    'rhs_table' => 'cie_clinical_issue_exam',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'CIE_Clinical_Issue_Exam',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'cie_clinical_issue_exam_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'cie_clinical_issue_exam_favorite' => 
  array (
    'name' => 'cie_clinical_issue_exam_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'CIE_Clinical_Issue_Exam',
    'rhs_table' => 'cie_clinical_issue_exam',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'CIE_Clinical_Issue_Exam',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'cie_clinical_issue_exam_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'cie_clinical_issue_exam_assigned_user' => 
  array (
    'name' => 'cie_clinical_issue_exam_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'CIE_Clinical_Issue_Exam',
    'rhs_table' => 'cie_clinical_issue_exam',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'cie_clinical_issue_exam_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'anml_animals_cie_clinical_issue_exam_1' => 
  array (
    'name' => 'anml_animals_cie_clinical_issue_exam_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'anml_animals_cie_clinical_issue_exam_1' => 
      array (
        'lhs_module' => 'ANML_Animals',
        'lhs_table' => 'anml_animals',
        'lhs_key' => 'id',
        'rhs_module' => 'CIE_Clinical_Issue_Exam',
        'rhs_table' => 'cie_clinical_issue_exam',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'anml_animals_cie_clinical_issue_exam_1_c',
        'join_key_lhs' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
        'join_key_rhs' => 'anml_anima4710ue_exam_idb',
      ),
    ),
    'table' => 'anml_animals_cie_clinical_issue_exam_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'anml_animals_cie_clinical_issue_exam_1anml_animals_ida' => 
      array (
        'name' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
        'type' => 'id',
      ),
      'anml_anima4710ue_exam_idb' => 
      array (
        'name' => 'anml_anima4710ue_exam_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_anml_animals_cie_clinical_issue_exam_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_anml_animals_cie_clinical_issue_exam_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_anml_animals_cie_clinical_issue_exam_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'anml_anima4710ue_exam_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'anml_animals_cie_clinical_issue_exam_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'anml_anima4710ue_exam_idb',
        ),
      ),
    ),
    'lhs_module' => 'ANML_Animals',
    'lhs_table' => 'anml_animals',
    'lhs_key' => 'id',
    'rhs_module' => 'CIE_Clinical_Issue_Exam',
    'rhs_table' => 'cie_clinical_issue_exam',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'anml_animals_cie_clinical_issue_exam_1_c',
    'join_key_lhs' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
    'join_key_rhs' => 'anml_anima4710ue_exam_idb',
    'readonly' => true,
    'relationship_name' => 'anml_animals_cie_clinical_issue_exam_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'cie_clinical_issue_exam_co_clinical_observation_1' => 
  array (
    'name' => 'cie_clinical_issue_exam_co_clinical_observation_1',
    'true_relationship_type' => 'one-to-one',
    'from_studio' => true,
    'relationships' => 
    array (
      'cie_clinical_issue_exam_co_clinical_observation_1' => 
      array (
        'lhs_module' => 'CIE_Clinical_Issue_Exam',
        'lhs_table' => 'cie_clinical_issue_exam',
        'lhs_key' => 'id',
        'rhs_module' => 'CO_Clinical_Observation',
        'rhs_table' => 'co_clinical_observation',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'cie_clinical_issue_exam_co_clinical_observation_1_c',
        'join_key_lhs' => 'cie_clinicb9a6ue_exam_ida',
        'join_key_rhs' => 'cie_clinicecbbrvation_idb',
      ),
    ),
    'table' => 'cie_clinical_issue_exam_co_clinical_observation_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'cie_clinicb9a6ue_exam_ida' => 
      array (
        'name' => 'cie_clinicb9a6ue_exam_ida',
        'type' => 'id',
      ),
      'cie_clinicecbbrvation_idb' => 
      array (
        'name' => 'cie_clinicecbbrvation_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_cie_clinical_issue_exam_co_clinical_observation_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_cie_clinical_issue_exam_co_clinical_observation_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'cie_clinicb9a6ue_exam_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_cie_clinical_issue_exam_co_clinical_observation_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'cie_clinicecbbrvation_idb',
          1 => 'deleted',
        ),
      ),
    ),
    'lhs_module' => 'CIE_Clinical_Issue_Exam',
    'lhs_table' => 'cie_clinical_issue_exam',
    'lhs_key' => 'id',
    'rhs_module' => 'CO_Clinical_Observation',
    'rhs_table' => 'co_clinical_observation',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-one',
    'join_table' => 'cie_clinical_issue_exam_co_clinical_observation_1_c',
    'join_key_lhs' => 'cie_clinicb9a6ue_exam_ida',
    'join_key_rhs' => 'cie_clinicecbbrvation_idb',
    'readonly' => true,
    'relationship_name' => 'cie_clinical_issue_exam_co_clinical_observation_1',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'cie_clinical_issue_exam_cie_clinical_issue_exam_1' => 
  array (
    'name' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'cie_clinical_issue_exam_cie_clinical_issue_exam_1' => 
      array (
        'lhs_module' => 'CIE_Clinical_Issue_Exam',
        'lhs_table' => 'cie_clinical_issue_exam',
        'lhs_key' => 'id',
        'rhs_module' => 'CIE_Clinical_Issue_Exam',
        'rhs_table' => 'cie_clinical_issue_exam',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_c',
        'join_key_lhs' => 'cie_clinic2bf6ue_exam_ida',
        'join_key_rhs' => 'cie_clinic7f62ue_exam_idb',
      ),
    ),
    'table' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'cie_clinic2bf6ue_exam_ida' => 
      array (
        'name' => 'cie_clinic2bf6ue_exam_ida',
        'type' => 'id',
      ),
      'cie_clinic7f62ue_exam_idb' => 
      array (
        'name' => 'cie_clinic7f62ue_exam_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_cie_clinical_issue_exam_cie_clinical_issue_exam_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_cie_clinical_issue_exam_cie_clinical_issue_exam_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'cie_clinic2bf6ue_exam_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_cie_clinical_issue_exam_cie_clinical_issue_exam_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'cie_clinic7f62ue_exam_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'cie_clinic7f62ue_exam_idb',
        ),
      ),
    ),
    'lhs_module' => 'CIE_Clinical_Issue_Exam',
    'lhs_table' => 'cie_clinical_issue_exam',
    'lhs_key' => 'id',
    'rhs_module' => 'CIE_Clinical_Issue_Exam',
    'rhs_table' => 'cie_clinical_issue_exam',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_c',
    'join_key_lhs' => 'cie_clinic2bf6ue_exam_ida',
    'join_key_rhs' => 'cie_clinic7f62ue_exam_idb',
    'readonly' => true,
    'relationship_name' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'cie_clinical_issue_exam_m06_error_1' => 
  array (
    'name' => 'cie_clinical_issue_exam_m06_error_1',
    'true_relationship_type' => 'one-to-one',
    'from_studio' => true,
    'relationships' => 
    array (
      'cie_clinical_issue_exam_m06_error_1' => 
      array (
        'lhs_module' => 'CIE_Clinical_Issue_Exam',
        'lhs_table' => 'cie_clinical_issue_exam',
        'lhs_key' => 'id',
        'rhs_module' => 'M06_Error',
        'rhs_table' => 'm06_error',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'cie_clinical_issue_exam_m06_error_1_c',
        'join_key_lhs' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
        'join_key_rhs' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
      ),
    ),
    'table' => 'cie_clinical_issue_exam_m06_error_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida' => 
      array (
        'name' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
        'type' => 'id',
      ),
      'cie_clinical_issue_exam_m06_error_1m06_error_idb' => 
      array (
        'name' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_cie_clinical_issue_exam_m06_error_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_cie_clinical_issue_exam_m06_error_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_cie_clinical_issue_exam_m06_error_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
          1 => 'deleted',
        ),
      ),
    ),
    'lhs_module' => 'CIE_Clinical_Issue_Exam',
    'lhs_table' => 'cie_clinical_issue_exam',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-one',
    'join_table' => 'cie_clinical_issue_exam_m06_error_1_c',
    'join_key_lhs' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
    'join_key_rhs' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
    'readonly' => true,
    'relationship_name' => 'cie_clinical_issue_exam_m06_error_1',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'cie_clinical_issue_exam_co_clinical_observation_2' => 
  array (
    'rhs_label' => 'Clinical Observations',
    'lhs_label' => 'Clinical Issues ',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'CIE_Clinical_Issue_Exam',
    'rhs_module' => 'CO_Clinical_Observation',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  ),
);