<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'nsc_namsa_sub_companies_modified_user' => 
  array (
    'name' => 'nsc_namsa_sub_companies_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'NSC_NAMSA_Sub_Companies',
    'rhs_table' => 'nsc_namsa_sub_companies',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'nsc_namsa_sub_companies_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'nsc_namsa_sub_companies_created_by' => 
  array (
    'name' => 'nsc_namsa_sub_companies_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'NSC_NAMSA_Sub_Companies',
    'rhs_table' => 'nsc_namsa_sub_companies',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'nsc_namsa_sub_companies_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'nsc_namsa_sub_companies_activities' => 
  array (
    'name' => 'nsc_namsa_sub_companies_activities',
    'lhs_module' => 'NSC_NAMSA_Sub_Companies',
    'lhs_table' => 'nsc_namsa_sub_companies',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'NSC_NAMSA_Sub_Companies',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'nsc_namsa_sub_companies_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'nsc_namsa_sub_companies_following' => 
  array (
    'name' => 'nsc_namsa_sub_companies_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'NSC_NAMSA_Sub_Companies',
    'rhs_table' => 'nsc_namsa_sub_companies',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'NSC_NAMSA_Sub_Companies',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'nsc_namsa_sub_companies_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'nsc_namsa_sub_companies_favorite' => 
  array (
    'name' => 'nsc_namsa_sub_companies_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'NSC_NAMSA_Sub_Companies',
    'rhs_table' => 'nsc_namsa_sub_companies',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'NSC_NAMSA_Sub_Companies',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'nsc_namsa_sub_companies_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'nsc_namsa_sub_companies_assigned_user' => 
  array (
    'name' => 'nsc_namsa_sub_companies_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'NSC_NAMSA_Sub_Companies',
    'rhs_table' => 'nsc_namsa_sub_companies',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'nsc_namsa_sub_companies_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'accounts_nsc_namsa_sub_companies_1' => 
  array (
    'name' => 'accounts_nsc_namsa_sub_companies_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'accounts_nsc_namsa_sub_companies_1' => 
      array (
        'lhs_module' => 'Accounts',
        'lhs_table' => 'accounts',
        'lhs_key' => 'id',
        'rhs_module' => 'NSC_NAMSA_Sub_Companies',
        'rhs_table' => 'nsc_namsa_sub_companies',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'accounts_nsc_namsa_sub_companies_1_c',
        'join_key_lhs' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
        'join_key_rhs' => 'accounts_nsc_namsa_sub_companies_1nsc_namsa_sub_companies_idb',
      ),
    ),
    'table' => 'accounts_nsc_namsa_sub_companies_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'accounts_nsc_namsa_sub_companies_1accounts_ida' => 
      array (
        'name' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
        'type' => 'id',
      ),
      'accounts_nsc_namsa_sub_companies_1nsc_namsa_sub_companies_idb' => 
      array (
        'name' => 'accounts_nsc_namsa_sub_companies_1nsc_namsa_sub_companies_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_accounts_nsc_namsa_sub_companies_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_accounts_nsc_namsa_sub_companies_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_accounts_nsc_namsa_sub_companies_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'accounts_nsc_namsa_sub_companies_1nsc_namsa_sub_companies_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'accounts_nsc_namsa_sub_companies_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'accounts_nsc_namsa_sub_companies_1nsc_namsa_sub_companies_idb',
        ),
      ),
    ),
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'NSC_NAMSA_Sub_Companies',
    'rhs_table' => 'nsc_namsa_sub_companies',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'accounts_nsc_namsa_sub_companies_1_c',
    'join_key_lhs' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
    'join_key_rhs' => 'accounts_nsc_namsa_sub_companies_1nsc_namsa_sub_companies_idb',
    'readonly' => true,
    'relationship_name' => 'accounts_nsc_namsa_sub_companies_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'nsc_namsa_sub_companies_m01_sales_1' => 
  array (
    'rhs_label' => 'Sales',
    'lhs_label' => 'NAMSA Subcontracting Companies',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'NSC_NAMSA_Sub_Companies',
    'rhs_module' => 'M01_Sales',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'nsc_namsa_sub_companies_m01_sales_1',
  ),
);