<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'ic_inventory_collection_ii_inventory_item_1' => 
  array (
    'name' => 'ic_inventory_collection_ii_inventory_item_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'ic_inventory_collection_ii_inventory_item_1' => 
      array (
        'lhs_module' => 'IC_Inventory_Collection',
        'lhs_table' => 'ic_inventory_collection',
        'lhs_key' => 'id',
        'rhs_module' => 'II_Inventory_Item',
        'rhs_table' => 'ii_inventory_item',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'ic_inventory_collection_ii_inventory_item_1_c',
        'join_key_lhs' => 'ic_invento128flection_ida',
        'join_key_rhs' => 'ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb',
      ),
    ),
    'table' => 'ic_inventory_collection_ii_inventory_item_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'ic_invento128flection_ida' => 
      array (
        'name' => 'ic_invento128flection_ida',
        'type' => 'id',
      ),
      'ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb' => 
      array (
        'name' => 'ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_ic_inventory_collection_ii_inventory_item_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_ic_inventory_collection_ii_inventory_item_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'ic_invento128flection_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_ic_inventory_collection_ii_inventory_item_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'ic_inventory_collection_ii_inventory_item_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb',
        ),
      ),
    ),
    'lhs_module' => 'IC_Inventory_Collection',
    'lhs_table' => 'ic_inventory_collection',
    'lhs_key' => 'id',
    'rhs_module' => 'II_Inventory_Item',
    'rhs_table' => 'ii_inventory_item',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'ic_inventory_collection_ii_inventory_item_1_c',
    'join_key_lhs' => 'ic_invento128flection_ida',
    'join_key_rhs' => 'ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb',
    'readonly' => true,
    'relationship_name' => 'ic_inventory_collection_ii_inventory_item_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'ic_inventory_collection_modified_user' => 
  array (
    'name' => 'ic_inventory_collection_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'IC_Inventory_Collection',
    'rhs_table' => 'ic_inventory_collection',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'ic_inventory_collection_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ic_inventory_collection_created_by' => 
  array (
    'name' => 'ic_inventory_collection_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'IC_Inventory_Collection',
    'rhs_table' => 'ic_inventory_collection',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'ic_inventory_collection_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ic_inventory_collection_activities' => 
  array (
    'name' => 'ic_inventory_collection_activities',
    'lhs_module' => 'IC_Inventory_Collection',
    'lhs_table' => 'ic_inventory_collection',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'IC_Inventory_Collection',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'ic_inventory_collection_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ic_inventory_collection_following' => 
  array (
    'name' => 'ic_inventory_collection_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'IC_Inventory_Collection',
    'rhs_table' => 'ic_inventory_collection',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'IC_Inventory_Collection',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'ic_inventory_collection_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ic_inventory_collection_favorite' => 
  array (
    'name' => 'ic_inventory_collection_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'IC_Inventory_Collection',
    'rhs_table' => 'ic_inventory_collection',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'IC_Inventory_Collection',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'ic_inventory_collection_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ic_inventory_collection_assigned_user' => 
  array (
    'name' => 'ic_inventory_collection_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'IC_Inventory_Collection',
    'rhs_table' => 'ic_inventory_collection',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'ic_inventory_collection_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ic_inventory_collection_im_inventory_management_1' => 
  array (
    'rhs_label' => 'Inventory Management',
    'lhs_label' => 'Inventory Collections',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'IC_Inventory_Collection',
    'rhs_module' => 'IM_Inventory_Management',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'ic_inventory_collection_im_inventory_management_1',
  ),
);