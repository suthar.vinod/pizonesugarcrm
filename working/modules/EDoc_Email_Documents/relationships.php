<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'm01_sales_edoc_email_documents_1' => 
  array (
    'name' => 'm01_sales_edoc_email_documents_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_sales_edoc_email_documents_1' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'EDoc_Email_Documents',
        'rhs_table' => 'edoc_email_documents',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_sales_edoc_email_documents_1_c',
        'join_key_lhs' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
        'join_key_rhs' => 'm01_sales_edoc_email_documents_1edoc_email_documents_idb',
      ),
    ),
    'table' => 'm01_sales_edoc_email_documents_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm01_sales_edoc_email_documents_1m01_sales_ida' => 
      array (
        'name' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
        'type' => 'id',
      ),
      'm01_sales_edoc_email_documents_1edoc_email_documents_idb' => 
      array (
        'name' => 'm01_sales_edoc_email_documents_1edoc_email_documents_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m01_sales_edoc_email_documents_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m01_sales_edoc_email_documents_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_edoc_email_documents_1m01_sales_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m01_sales_edoc_email_documents_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_edoc_email_documents_1edoc_email_documents_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm01_sales_edoc_email_documents_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_sales_edoc_email_documents_1edoc_email_documents_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'EDoc_Email_Documents',
    'rhs_table' => 'edoc_email_documents',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm01_sales_edoc_email_documents_1_c',
    'join_key_lhs' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
    'join_key_rhs' => 'm01_sales_edoc_email_documents_1edoc_email_documents_idb',
    'readonly' => true,
    'relationship_name' => 'm01_sales_edoc_email_documents_1',
    'rhs_subpanel' => 'ForM01_salesM01_sales_edoc_email_documents_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_edoc_email_documents_1' => 
  array (
    'name' => 'm03_work_product_edoc_email_documents_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_edoc_email_documents_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'EDoc_Email_Documents',
        'rhs_table' => 'edoc_email_documents',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_edoc_email_documents_1_c',
        'join_key_lhs' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
      ),
    ),
    'table' => 'm03_work_product_edoc_email_documents_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_edoc_email_documents_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_product_edoc_email_documents_1edoc_email_documents_idb' => 
      array (
        'name' => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_edoc_email_documents_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_edoc_email_documents_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_edoc_email_documents_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_edoc_email_documents_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'EDoc_Email_Documents',
    'rhs_table' => 'edoc_email_documents',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm03_work_product_edoc_email_documents_1_c',
    'join_key_lhs' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_product_edoc_email_documents_1edoc_email_documents_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_edoc_email_documents_1',
    'rhs_subpanel' => 'ForM03_work_productM03_work_product_edoc_email_documents_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'edoc_email_documents_modified_user' => 
  array (
    'name' => 'edoc_email_documents_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'EDoc_Email_Documents',
    'rhs_table' => 'edoc_email_documents',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'edoc_email_documents_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'edoc_email_documents_created_by' => 
  array (
    'name' => 'edoc_email_documents_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'EDoc_Email_Documents',
    'rhs_table' => 'edoc_email_documents',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'edoc_email_documents_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'edoc_email_documents_activities' => 
  array (
    'name' => 'edoc_email_documents_activities',
    'lhs_module' => 'EDoc_Email_Documents',
    'lhs_table' => 'edoc_email_documents',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'EDoc_Email_Documents',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'edoc_email_documents_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'edoc_email_documents_following' => 
  array (
    'name' => 'edoc_email_documents_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'EDoc_Email_Documents',
    'rhs_table' => 'edoc_email_documents',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'EDoc_Email_Documents',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'edoc_email_documents_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'edoc_email_documents_favorite' => 
  array (
    'name' => 'edoc_email_documents_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'EDoc_Email_Documents',
    'rhs_table' => 'edoc_email_documents',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'EDoc_Email_Documents',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'edoc_email_documents_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'edoc_email_documents_assigned_user' => 
  array (
    'name' => 'edoc_email_documents_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'EDoc_Email_Documents',
    'rhs_table' => 'edoc_email_documents',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'edoc_email_documents_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'edoc_email_documents_a1a_critical_phase_inspectio_1' => 
  array (
    'rhs_label' => 'Critical Phase Inspections',
    'lhs_label' => 'Email Documents',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'EDoc_Email_Documents',
    'rhs_module' => 'A1A_Critical_Phase_Inspectio',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  ),
);