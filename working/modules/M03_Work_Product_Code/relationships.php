<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'm03_work_product_m03_work_product_code_1' => 
  array (
    'name' => 'm03_work_product_m03_work_product_code_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm03_work_product_m03_work_product_code_1' => 
      array (
        'lhs_module' => 'M03_Work_Product',
        'lhs_table' => 'm03_work_product',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product_Code',
        'rhs_table' => 'm03_work_product_code',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm03_work_product_m03_work_product_code_1_c',
        'join_key_lhs' => 'm03_work_product_m03_work_product_code_1m03_work_product_ida',
        'join_key_rhs' => 'm03_work_p5357ct_code_idb',
      ),
    ),
    'table' => 'm03_work_product_m03_work_product_code_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm03_work_product_m03_work_product_code_1m03_work_product_ida' => 
      array (
        'name' => 'm03_work_product_m03_work_product_code_1m03_work_product_ida',
        'type' => 'id',
      ),
      'm03_work_p5357ct_code_idb' => 
      array (
        'name' => 'm03_work_p5357ct_code_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m03_work_product_m03_work_product_code_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m03_work_product_m03_work_product_code_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_product_m03_work_product_code_1m03_work_product_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m03_work_product_m03_work_product_code_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm03_work_p5357ct_code_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm03_work_product_m03_work_product_code_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm03_work_product_m03_work_product_code_1m03_work_product_ida',
          1 => 'm03_work_p5357ct_code_idb',
        ),
      ),
    ),
    'lhs_module' => 'M03_Work_Product',
    'lhs_table' => 'm03_work_product',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Code',
    'rhs_table' => 'm03_work_product_code',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm03_work_product_m03_work_product_code_1_c',
    'join_key_lhs' => 'm03_work_product_m03_work_product_code_1m03_work_product_ida',
    'join_key_rhs' => 'm03_work_p5357ct_code_idb',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_m03_work_product_code_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'ForM03_work_product_codeM03_work_product_m03_work_product_code_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'wpg_work_product_group_m03_work_product_code_1' => 
  array (
    'name' => 'wpg_work_product_group_m03_work_product_code_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'wpg_work_product_group_m03_work_product_code_1' => 
      array (
        'lhs_module' => 'WPG_Work_Product_Group',
        'lhs_table' => 'wpg_work_product_group',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product_Code',
        'rhs_table' => 'm03_work_product_code',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'wpg_work_product_group_m03_work_product_code_1_c',
        'join_key_lhs' => 'wpg_work_p164at_group_ida',
        'join_key_rhs' => 'wpg_work_pa92cct_code_idb',
      ),
    ),
    'table' => 'wpg_work_product_group_m03_work_product_code_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'wpg_work_p164at_group_ida' => 
      array (
        'name' => 'wpg_work_p164at_group_ida',
        'type' => 'id',
      ),
      'wpg_work_pa92cct_code_idb' => 
      array (
        'name' => 'wpg_work_pa92cct_code_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_wpg_work_product_group_m03_work_product_code_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_wpg_work_product_group_m03_work_product_code_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'wpg_work_p164at_group_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_wpg_work_product_group_m03_work_product_code_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'wpg_work_pa92cct_code_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'wpg_work_product_group_m03_work_product_code_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'wpg_work_pa92cct_code_idb',
        ),
      ),
    ),
    'lhs_module' => 'WPG_Work_Product_Group',
    'lhs_table' => 'wpg_work_product_group',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Code',
    'rhs_table' => 'm03_work_product_code',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'wpg_work_product_group_m03_work_product_code_1_c',
    'join_key_lhs' => 'wpg_work_p164at_group_ida',
    'join_key_rhs' => 'wpg_work_pa92cct_code_idb',
    'readonly' => true,
    'relationship_name' => 'wpg_work_product_group_m03_work_product_code_1',
    'rhs_subpanel' => 'ForWpg_work_product_groupWpg_work_product_group_m03_work_product_code_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm03_work_product_code_modified_user' => 
  array (
    'name' => 'm03_work_product_code_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Code',
    'rhs_table' => 'm03_work_product_code',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_code_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_code_created_by' => 
  array (
    'name' => 'm03_work_product_code_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Code',
    'rhs_table' => 'm03_work_product_code',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_code_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_code_activities' => 
  array (
    'name' => 'm03_work_product_code_activities',
    'lhs_module' => 'M03_Work_Product_Code',
    'lhs_table' => 'm03_work_product_code',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'M03_Work_Product_Code',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'm03_work_product_code_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_code_following' => 
  array (
    'name' => 'm03_work_product_code_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Code',
    'rhs_table' => 'm03_work_product_code',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'M03_Work_Product_Code',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_code_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_code_favorite' => 
  array (
    'name' => 'm03_work_product_code_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Code',
    'rhs_table' => 'm03_work_product_code',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'M03_Work_Product_Code',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_code_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_code_assigned_user' => 
  array (
    'name' => 'm03_work_product_code_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product_Code',
    'rhs_table' => 'm03_work_product_code',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm03_work_product_code_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm03_work_product_code_taskd_task_design_1' => 
  array (
    'rhs_label' => 'Task Designs',
    'lhs_label' => 'Work Product Codes',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'M03_Work_Product_Code',
    'rhs_module' => 'TaskD_Task_Design',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'm03_work_product_code_taskd_task_design_1',
  ),
);