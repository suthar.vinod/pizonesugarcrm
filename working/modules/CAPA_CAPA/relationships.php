<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'capa_capa_modified_user' => 
  array (
    'name' => 'capa_capa_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'CAPA_CAPA',
    'rhs_table' => 'capa_capa',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'capa_capa_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'capa_capa_created_by' => 
  array (
    'name' => 'capa_capa_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'CAPA_CAPA',
    'rhs_table' => 'capa_capa',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'capa_capa_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'capa_capa_activities' => 
  array (
    'name' => 'capa_capa_activities',
    'lhs_module' => 'CAPA_CAPA',
    'lhs_table' => 'capa_capa',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'CAPA_CAPA',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'capa_capa_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'capa_capa_following' => 
  array (
    'name' => 'capa_capa_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'CAPA_CAPA',
    'rhs_table' => 'capa_capa',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'CAPA_CAPA',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'capa_capa_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'capa_capa_favorite' => 
  array (
    'name' => 'capa_capa_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'CAPA_CAPA',
    'rhs_table' => 'capa_capa',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'CAPA_CAPA',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'capa_capa_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'capa_capa_assigned_user' => 
  array (
    'name' => 'capa_capa_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'CAPA_CAPA',
    'rhs_table' => 'capa_capa',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'capa_capa_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'capa_capa_cf_capa_files_1' => 
  array (
    'name' => 'capa_capa_cf_capa_files_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'capa_capa_cf_capa_files_1' => 
      array (
        'lhs_module' => 'CAPA_CAPA',
        'lhs_table' => 'capa_capa',
        'lhs_key' => 'id',
        'rhs_module' => 'CF_CAPA_Files',
        'rhs_table' => 'cf_capa_files',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'capa_capa_cf_capa_files_1_c',
        'join_key_lhs' => 'capa_capa_cf_capa_files_1capa_capa_ida',
        'join_key_rhs' => 'capa_capa_cf_capa_files_1cf_capa_files_idb',
      ),
    ),
    'table' => 'capa_capa_cf_capa_files_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'capa_capa_cf_capa_files_1capa_capa_ida' => 
      array (
        'name' => 'capa_capa_cf_capa_files_1capa_capa_ida',
        'type' => 'id',
      ),
      'capa_capa_cf_capa_files_1cf_capa_files_idb' => 
      array (
        'name' => 'capa_capa_cf_capa_files_1cf_capa_files_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_capa_capa_cf_capa_files_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_capa_capa_cf_capa_files_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'capa_capa_cf_capa_files_1capa_capa_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_capa_capa_cf_capa_files_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'capa_capa_cf_capa_files_1cf_capa_files_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'capa_capa_cf_capa_files_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'capa_capa_cf_capa_files_1cf_capa_files_idb',
        ),
      ),
    ),
    'lhs_module' => 'CAPA_CAPA',
    'lhs_table' => 'capa_capa',
    'lhs_key' => 'id',
    'rhs_module' => 'CF_CAPA_Files',
    'rhs_table' => 'cf_capa_files',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'capa_capa_cf_capa_files_1_c',
    'join_key_lhs' => 'capa_capa_cf_capa_files_1capa_capa_ida',
    'join_key_rhs' => 'capa_capa_cf_capa_files_1cf_capa_files_idb',
    'readonly' => true,
    'relationship_name' => 'capa_capa_cf_capa_files_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'capa_capa_m06_error_1' => 
  array (
    'name' => 'capa_capa_m06_error_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'capa_capa_m06_error_1' => 
      array (
        'lhs_module' => 'CAPA_CAPA',
        'lhs_table' => 'capa_capa',
        'lhs_key' => 'id',
        'rhs_module' => 'M06_Error',
        'rhs_table' => 'm06_error',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'capa_capa_m06_error_1_c',
        'join_key_lhs' => 'capa_capa_m06_error_1capa_capa_ida',
        'join_key_rhs' => 'capa_capa_m06_error_1m06_error_idb',
      ),
    ),
    'table' => 'capa_capa_m06_error_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'capa_capa_m06_error_1capa_capa_ida' => 
      array (
        'name' => 'capa_capa_m06_error_1capa_capa_ida',
        'type' => 'id',
      ),
      'capa_capa_m06_error_1m06_error_idb' => 
      array (
        'name' => 'capa_capa_m06_error_1m06_error_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_capa_capa_m06_error_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_capa_capa_m06_error_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'capa_capa_m06_error_1capa_capa_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_capa_capa_m06_error_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'capa_capa_m06_error_1m06_error_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'capa_capa_m06_error_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'capa_capa_m06_error_1m06_error_idb',
        ),
      ),
    ),
    'lhs_module' => 'CAPA_CAPA',
    'lhs_table' => 'capa_capa',
    'lhs_key' => 'id',
    'rhs_module' => 'M06_Error',
    'rhs_table' => 'm06_error',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'capa_capa_m06_error_1_c',
    'join_key_lhs' => 'capa_capa_m06_error_1capa_capa_ida',
    'join_key_rhs' => 'capa_capa_m06_error_1m06_error_idb',
    'readonly' => true,
    'relationship_name' => 'capa_capa_m06_error_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'capa_capa_capa_capa_1' => 
  array (
    'name' => 'capa_capa_capa_capa_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'capa_capa_capa_capa_1' => 
      array (
        'lhs_module' => 'CAPA_CAPA',
        'lhs_table' => 'capa_capa',
        'lhs_key' => 'id',
        'rhs_module' => 'CAPA_CAPA',
        'rhs_table' => 'capa_capa',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'capa_capa_capa_capa_1_c',
        'join_key_lhs' => 'capa_capa_capa_capa_1capa_capa_ida',
        'join_key_rhs' => 'capa_capa_capa_capa_1capa_capa_idb',
      ),
    ),
    'table' => 'capa_capa_capa_capa_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'capa_capa_capa_capa_1capa_capa_ida' => 
      array (
        'name' => 'capa_capa_capa_capa_1capa_capa_ida',
        'type' => 'id',
      ),
      'capa_capa_capa_capa_1capa_capa_idb' => 
      array (
        'name' => 'capa_capa_capa_capa_1capa_capa_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_capa_capa_capa_capa_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_capa_capa_capa_capa_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'capa_capa_capa_capa_1capa_capa_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_capa_capa_capa_capa_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'capa_capa_capa_capa_1capa_capa_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'capa_capa_capa_capa_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'capa_capa_capa_capa_1capa_capa_idb',
        ),
      ),
    ),
    'lhs_module' => 'CAPA_CAPA',
    'lhs_table' => 'capa_capa',
    'lhs_key' => 'id',
    'rhs_module' => 'CAPA_CAPA',
    'rhs_table' => 'capa_capa',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'capa_capa_capa_capa_1_c',
    'join_key_lhs' => 'capa_capa_capa_capa_1capa_capa_ida',
    'join_key_rhs' => 'capa_capa_capa_capa_1capa_capa_idb',
    'readonly' => true,
    'relationship_name' => 'capa_capa_capa_capa_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'capa_capa_contacts_1' => 
  array (
    'name' => 'capa_capa_contacts_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'capa_capa_contacts_1' => 
      array (
        'lhs_module' => 'CAPA_CAPA',
        'lhs_table' => 'capa_capa',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'capa_capa_contacts_1_c',
        'join_key_lhs' => 'capa_capa_contacts_1capa_capa_ida',
        'join_key_rhs' => 'capa_capa_contacts_1contacts_idb',
      ),
    ),
    'table' => 'capa_capa_contacts_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'capa_capa_contacts_1capa_capa_ida' => 
      array (
        'name' => 'capa_capa_contacts_1capa_capa_ida',
        'type' => 'id',
      ),
      'capa_capa_contacts_1contacts_idb' => 
      array (
        'name' => 'capa_capa_contacts_1contacts_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_capa_capa_contacts_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_capa_capa_contacts_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'capa_capa_contacts_1capa_capa_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_capa_capa_contacts_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'capa_capa_contacts_1contacts_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'capa_capa_contacts_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'capa_capa_contacts_1capa_capa_ida',
          1 => 'capa_capa_contacts_1contacts_idb',
        ),
      ),
    ),
    'lhs_module' => 'CAPA_CAPA',
    'lhs_table' => 'capa_capa',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'capa_capa_contacts_1_c',
    'join_key_lhs' => 'capa_capa_contacts_1capa_capa_ida',
    'join_key_rhs' => 'capa_capa_contacts_1contacts_idb',
    'readonly' => true,
    'relationship_name' => 'capa_capa_contacts_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'capa_capa_contacts_2' => 
  array (
    'rhs_label' => 'Contacts',
    'lhs_label' => 'CAPAs',
    'lhs_subpanel' => 'default',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'CAPA_CAPA',
    'rhs_module' => 'Contacts',
    'relationship_type' => 'many-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'capa_capa_contacts_2',
  ),
);