<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'rr_regulatory_response_modified_user' => 
  array (
    'name' => 'rr_regulatory_response_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RR_Regulatory_Response',
    'rhs_table' => 'rr_regulatory_response',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'rr_regulatory_response_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'rr_regulatory_response_created_by' => 
  array (
    'name' => 'rr_regulatory_response_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RR_Regulatory_Response',
    'rhs_table' => 'rr_regulatory_response',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'rr_regulatory_response_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'rr_regulatory_response_activities' => 
  array (
    'name' => 'rr_regulatory_response_activities',
    'lhs_module' => 'RR_Regulatory_Response',
    'lhs_table' => 'rr_regulatory_response',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'RR_Regulatory_Response',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'rr_regulatory_response_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'rr_regulatory_response_following' => 
  array (
    'name' => 'rr_regulatory_response_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RR_Regulatory_Response',
    'rhs_table' => 'rr_regulatory_response',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'RR_Regulatory_Response',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'rr_regulatory_response_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'rr_regulatory_response_favorite' => 
  array (
    'name' => 'rr_regulatory_response_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RR_Regulatory_Response',
    'rhs_table' => 'rr_regulatory_response',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'RR_Regulatory_Response',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'rr_regulatory_response_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'rr_regulatory_response_assigned_user' => 
  array (
    'name' => 'rr_regulatory_response_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RR_Regulatory_Response',
    'rhs_table' => 'rr_regulatory_response',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'rr_regulatory_response_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'rr_regulatory_response_m03_work_product_1' => 
  array (
    'name' => 'rr_regulatory_response_m03_work_product_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'rr_regulatory_response_m03_work_product_1' => 
      array (
        'lhs_module' => 'RR_Regulatory_Response',
        'lhs_table' => 'rr_regulatory_response',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product',
        'rhs_table' => 'm03_work_product',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'rr_regulatory_response_m03_work_product_1_c',
        'join_key_lhs' => 'rr_regulat4120esponse_ida',
        'join_key_rhs' => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
      ),
    ),
    'table' => 'rr_regulatory_response_m03_work_product_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'rr_regulat4120esponse_ida' => 
      array (
        'name' => 'rr_regulat4120esponse_ida',
        'type' => 'id',
      ),
      'rr_regulatory_response_m03_work_product_1m03_work_product_idb' => 
      array (
        'name' => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_rr_regulatory_response_m03_work_product_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_rr_regulatory_response_m03_work_product_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'rr_regulat4120esponse_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_rr_regulatory_response_m03_work_product_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'rr_regulatory_response_m03_work_product_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'rr_regulat4120esponse_ida',
          1 => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
        ),
      ),
    ),
    'lhs_module' => 'RR_Regulatory_Response',
    'lhs_table' => 'rr_regulatory_response',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'rr_regulatory_response_m03_work_product_1_c',
    'join_key_lhs' => 'rr_regulat4120esponse_ida',
    'join_key_rhs' => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
    'readonly' => true,
    'relationship_name' => 'rr_regulatory_response_m03_work_product_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'rr_regulatory_response_m01_sales_1' => 
  array (
    'rhs_label' => 'Sales',
    'lhs_label' => 'Regulatory Responses',
    'lhs_subpanel' => 'default',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'RR_Regulatory_Response',
    'rhs_module' => 'M01_Sales',
    'relationship_type' => 'many-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'rr_regulatory_response_m01_sales_1',
  ),
);