<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'm01_sales_m01_quote_document_1' => 
  array (
    'name' => 'm01_sales_m01_quote_document_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_sales_m01_quote_document_1' => 
      array (
        'lhs_module' => 'M01_Sales',
        'lhs_table' => 'm01_sales',
        'lhs_key' => 'id',
        'rhs_module' => 'M01_Quote_Document',
        'rhs_table' => 'm01_quote_document',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_sales_m01_quote_document_1_c',
        'join_key_lhs' => 'm01_sales_m01_quote_document_1m01_sales_ida',
        'join_key_rhs' => 'm01_sales_m01_quote_document_1m01_quote_document_idb',
      ),
    ),
    'table' => 'm01_sales_m01_quote_document_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'm01_sales_m01_quote_document_1m01_sales_ida' => 
      array (
        'name' => 'm01_sales_m01_quote_document_1m01_sales_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'm01_sales_m01_quote_document_1m01_quote_document_idb' => 
      array (
        'name' => 'm01_sales_m01_quote_document_1m01_quote_document_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'm01_sales_m01_quote_document_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'm01_sales_m01_quote_document_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_sales_m01_quote_document_1m01_sales_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'm01_sales_m01_quote_document_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_sales_m01_quote_document_1m01_quote_document_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Sales',
    'lhs_table' => 'm01_sales',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Quote_Document',
    'rhs_table' => 'm01_quote_document',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'm01_sales_m01_quote_document_1_c',
    'join_key_lhs' => 'm01_sales_m01_quote_document_1m01_sales_ida',
    'join_key_rhs' => 'm01_sales_m01_quote_document_1m01_quote_document_idb',
    'readonly' => true,
    'relationship_name' => 'm01_sales_m01_quote_document_1',
    'rhs_subpanel' => 'ForM01_salesM01_sales_m01_quote_document_1',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'm01_quote_document_m03_work_product_2' => 
  array (
    'name' => 'm01_quote_document_m03_work_product_2',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm01_quote_document_m03_work_product_2' => 
      array (
        'lhs_module' => 'M01_Quote_Document',
        'lhs_table' => 'm01_quote_document',
        'lhs_key' => 'id',
        'rhs_module' => 'M03_Work_Product',
        'rhs_table' => 'm03_work_product',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm01_quote_document_m03_work_product_2_c',
        'join_key_lhs' => 'm01_quote_document_m03_work_product_2m01_quote_document_ida',
        'join_key_rhs' => 'm01_quote_document_m03_work_product_2m03_work_product_idb',
      ),
    ),
    'table' => 'm01_quote_document_m03_work_product_2_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'varchar',
        'len' => 36,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'm01_quote_document_m03_work_product_2m01_quote_document_ida' => 
      array (
        'name' => 'm01_quote_document_m03_work_product_2m01_quote_document_ida',
        'type' => 'varchar',
        'len' => 36,
      ),
      'm01_quote_document_m03_work_product_2m03_work_product_idb' => 
      array (
        'name' => 'm01_quote_document_m03_work_product_2m03_work_product_idb',
        'type' => 'varchar',
        'len' => 36,
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'm01_quote_document_m03_work_product_2spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'm01_quote_document_m03_work_product_2_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm01_quote_document_m03_work_product_2m01_quote_document_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'm01_quote_document_m03_work_product_2_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm01_quote_document_m03_work_product_2m03_work_product_idb',
        ),
      ),
    ),
    'lhs_module' => 'M01_Quote_Document',
    'lhs_table' => 'm01_quote_document',
    'lhs_key' => 'id',
    'rhs_module' => 'M03_Work_Product',
    'rhs_table' => 'm03_work_product',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm01_quote_document_m03_work_product_2_c',
    'join_key_lhs' => 'm01_quote_document_m03_work_product_2m01_quote_document_ida',
    'join_key_rhs' => 'm01_quote_document_m03_work_product_2m03_work_product_idb',
    'readonly' => true,
    'relationship_name' => 'm01_quote_document_m03_work_product_2',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
  ),
  'm01_quote_document_modified_user' => 
  array (
    'name' => 'm01_quote_document_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Quote_Document',
    'rhs_table' => 'm01_quote_document',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm01_quote_document_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm01_quote_document_created_by' => 
  array (
    'name' => 'm01_quote_document_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Quote_Document',
    'rhs_table' => 'm01_quote_document',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm01_quote_document_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm01_quote_document_activities' => 
  array (
    'name' => 'm01_quote_document_activities',
    'lhs_module' => 'M01_Quote_Document',
    'lhs_table' => 'm01_quote_document',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'M01_Quote_Document',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'm01_quote_document_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm01_quote_document_following' => 
  array (
    'name' => 'm01_quote_document_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Quote_Document',
    'rhs_table' => 'm01_quote_document',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'M01_Quote_Document',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'm01_quote_document_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm01_quote_document_favorite' => 
  array (
    'name' => 'm01_quote_document_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Quote_Document',
    'rhs_table' => 'm01_quote_document',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'M01_Quote_Document',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'm01_quote_document_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'm01_quote_document_assigned_user' => 
  array (
    'name' => 'm01_quote_document_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'M01_Quote_Document',
    'rhs_table' => 'm01_quote_document',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'm01_quote_document_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
);