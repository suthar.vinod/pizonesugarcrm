<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'm06_error_equip_equipment_1' => 
  array (
    'name' => 'm06_error_equip_equipment_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'm06_error_equip_equipment_1' => 
      array (
        'lhs_module' => 'M06_Error',
        'lhs_table' => 'm06_error',
        'lhs_key' => 'id',
        'rhs_module' => 'Equip_Equipment',
        'rhs_table' => 'equip_equipment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'm06_error_equip_equipment_1_c',
        'join_key_lhs' => 'm06_error_equip_equipment_1m06_error_ida',
        'join_key_rhs' => 'm06_error_equip_equipment_1equip_equipment_idb',
      ),
    ),
    'table' => 'm06_error_equip_equipment_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'm06_error_equip_equipment_1m06_error_ida' => 
      array (
        'name' => 'm06_error_equip_equipment_1m06_error_ida',
        'type' => 'id',
      ),
      'm06_error_equip_equipment_1equip_equipment_idb' => 
      array (
        'name' => 'm06_error_equip_equipment_1equip_equipment_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_m06_error_equip_equipment_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_m06_error_equip_equipment_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_equip_equipment_1m06_error_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_m06_error_equip_equipment_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'm06_error_equip_equipment_1equip_equipment_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'm06_error_equip_equipment_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'm06_error_equip_equipment_1m06_error_ida',
          1 => 'm06_error_equip_equipment_1equip_equipment_idb',
        ),
      ),
    ),
    'lhs_module' => 'M06_Error',
    'lhs_table' => 'm06_error',
    'lhs_key' => 'id',
    'rhs_module' => 'Equip_Equipment',
    'rhs_table' => 'equip_equipment',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'm06_error_equip_equipment_1_c',
    'join_key_lhs' => 'm06_error_equip_equipment_1m06_error_ida',
    'join_key_rhs' => 'm06_error_equip_equipment_1equip_equipment_idb',
    'readonly' => true,
    'relationship_name' => 'm06_error_equip_equipment_1',
    'rhs_subpanel' => 'ForM06_errorM06_error_equip_equipment_1',
    'lhs_subpanel' => 'ForEquip_equipmentM06_error_equip_equipment_1',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'equip_equipment_efd_equipment_facility_doc_1' => 
  array (
    'name' => 'equip_equipment_efd_equipment_facility_doc_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'equip_equipment_efd_equipment_facility_doc_1' => 
      array (
        'lhs_module' => 'Equip_Equipment',
        'lhs_table' => 'equip_equipment',
        'lhs_key' => 'id',
        'rhs_module' => 'EFD_Equipment_Facility_Doc',
        'rhs_table' => 'efd_equipment_facility_doc',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'equip_equipment_efd_equipment_facility_doc_1_c',
        'join_key_lhs' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
        'join_key_rhs' => 'equip_equiffa6ity_doc_idb',
      ),
    ),
    'table' => 'equip_equipment_efd_equipment_facility_doc_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida' => 
      array (
        'name' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
        'type' => 'id',
      ),
      'equip_equiffa6ity_doc_idb' => 
      array (
        'name' => 'equip_equiffa6ity_doc_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_equip_equipment_efd_equipment_facility_doc_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_equip_equipment_efd_equipment_facility_doc_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_equip_equipment_efd_equipment_facility_doc_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'equip_equiffa6ity_doc_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'equip_equipment_efd_equipment_facility_doc_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'equip_equiffa6ity_doc_idb',
        ),
      ),
    ),
    'lhs_module' => 'Equip_Equipment',
    'lhs_table' => 'equip_equipment',
    'lhs_key' => 'id',
    'rhs_module' => 'EFD_Equipment_Facility_Doc',
    'rhs_table' => 'efd_equipment_facility_doc',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'equip_equipment_efd_equipment_facility_doc_1_c',
    'join_key_lhs' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
    'join_key_rhs' => 'equip_equiffa6ity_doc_idb',
    'readonly' => true,
    'relationship_name' => 'equip_equipment_efd_equipment_facility_doc_1',
    'rhs_subpanel' => 'ForEquip_equipmentEquip_equipment_efd_equipment_facility_doc_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'equip_equipment_efr_equipment_facility_recor_2' => 
  array (
    'name' => 'equip_equipment_efr_equipment_facility_recor_2',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'equip_equipment_efr_equipment_facility_recor_2' => 
      array (
        'lhs_module' => 'Equip_Equipment',
        'lhs_table' => 'equip_equipment',
        'lhs_key' => 'id',
        'rhs_module' => 'EFR_Equipment_Facility_Recor',
        'rhs_table' => 'efr_equipment_facility_recor',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'equip_equipment_efr_equipment_facility_recor_2_c',
        'join_key_lhs' => 'equip_equi01e2uipment_ida',
        'join_key_rhs' => 'equip_equiffd2y_recor_idb',
      ),
    ),
    'table' => 'equip_equipment_efr_equipment_facility_recor_2_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'equip_equi01e2uipment_ida' => 
      array (
        'name' => 'equip_equi01e2uipment_ida',
        'type' => 'id',
      ),
      'equip_equiffd2y_recor_idb' => 
      array (
        'name' => 'equip_equiffd2y_recor_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_equip_equipment_efr_equipment_facility_recor_2_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_equip_equipment_efr_equipment_facility_recor_2_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'equip_equi01e2uipment_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_equip_equipment_efr_equipment_facility_recor_2_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'equip_equiffd2y_recor_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'equip_equipment_efr_equipment_facility_recor_2_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'equip_equiffd2y_recor_idb',
        ),
      ),
    ),
    'lhs_module' => 'Equip_Equipment',
    'lhs_table' => 'equip_equipment',
    'lhs_key' => 'id',
    'rhs_module' => 'EFR_Equipment_Facility_Recor',
    'rhs_table' => 'efr_equipment_facility_recor',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'equip_equipment_efr_equipment_facility_recor_2_c',
    'join_key_lhs' => 'equip_equi01e2uipment_ida',
    'join_key_rhs' => 'equip_equiffd2y_recor_idb',
    'readonly' => true,
    'relationship_name' => 'equip_equipment_efr_equipment_facility_recor_2',
    'rhs_subpanel' => 'ForEquip_equipmentEquip_equipment_efr_equipment_facility_recor_2',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'equip_equipment_efs_equipment_facility_servi_1' => 
  array (
    'name' => 'equip_equipment_efs_equipment_facility_servi_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'equip_equipment_efs_equipment_facility_servi_1' => 
      array (
        'lhs_module' => 'Equip_Equipment',
        'lhs_table' => 'equip_equipment',
        'lhs_key' => 'id',
        'rhs_module' => 'EFS_Equipment_Facility_Servi',
        'rhs_table' => 'efs_equipment_facility_servi',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'equip_equipment_efs_equipment_facility_servi_1_c',
        'join_key_lhs' => 'equip_equia9d9uipment_ida',
        'join_key_rhs' => 'equip_equi3f6dy_servi_idb',
      ),
    ),
    'table' => 'equip_equipment_efs_equipment_facility_servi_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'equip_equia9d9uipment_ida' => 
      array (
        'name' => 'equip_equia9d9uipment_ida',
        'type' => 'id',
      ),
      'equip_equi3f6dy_servi_idb' => 
      array (
        'name' => 'equip_equi3f6dy_servi_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_equip_equipment_efs_equipment_facility_servi_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_equip_equipment_efs_equipment_facility_servi_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'equip_equia9d9uipment_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_equip_equipment_efs_equipment_facility_servi_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'equip_equi3f6dy_servi_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'equip_equipment_efs_equipment_facility_servi_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'equip_equi3f6dy_servi_idb',
        ),
      ),
    ),
    'lhs_module' => 'Equip_Equipment',
    'lhs_table' => 'equip_equipment',
    'lhs_key' => 'id',
    'rhs_module' => 'EFS_Equipment_Facility_Servi',
    'rhs_table' => 'efs_equipment_facility_servi',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'equip_equipment_efs_equipment_facility_servi_1_c',
    'join_key_lhs' => 'equip_equia9d9uipment_ida',
    'join_key_rhs' => 'equip_equi3f6dy_servi_idb',
    'readonly' => true,
    'relationship_name' => 'equip_equipment_efs_equipment_facility_servi_1',
    'rhs_subpanel' => 'ForEquip_equipmentEquip_equipment_efs_equipment_facility_servi_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'equip_equipment_modified_user' => 
  array (
    'name' => 'equip_equipment_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Equip_Equipment',
    'rhs_table' => 'equip_equipment',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'equip_equipment_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'equip_equipment_created_by' => 
  array (
    'name' => 'equip_equipment_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Equip_Equipment',
    'rhs_table' => 'equip_equipment',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'equip_equipment_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'equip_equipment_activities' => 
  array (
    'name' => 'equip_equipment_activities',
    'lhs_module' => 'Equip_Equipment',
    'lhs_table' => 'equip_equipment',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Equip_Equipment',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'equip_equipment_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'equip_equipment_following' => 
  array (
    'name' => 'equip_equipment_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Equip_Equipment',
    'rhs_table' => 'equip_equipment',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Equip_Equipment',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'equip_equipment_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'equip_equipment_favorite' => 
  array (
    'name' => 'equip_equipment_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Equip_Equipment',
    'rhs_table' => 'equip_equipment',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'Equip_Equipment',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'equip_equipment_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'equip_equipment_assigned_user' => 
  array (
    'name' => 'equip_equipment_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Equip_Equipment',
    'rhs_table' => 'equip_equipment',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'equip_equipment_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'equip_equipment_sv_service_vendor_1' => 
  array (
    'rhs_label' => 'Service Vendors',
    'lhs_label' => 'Equipment ',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'Equip_Equipment',
    'rhs_module' => 'SV_Service_Vendor',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'equip_equipment_sv_service_vendor_1',
  ),
);