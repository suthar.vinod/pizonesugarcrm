<?php
// created: 2020-01-15 13:01:13
$dictionary["equip_equipment_sv_service_vendor_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'equip_equipment_sv_service_vendor_1' => 
    array (
      'lhs_module' => 'Equip_Equipment',
      'lhs_table' => 'equip_equipment',
      'lhs_key' => 'id',
      'rhs_module' => 'SV_Service_Vendor',
      'rhs_table' => 'sv_service_vendor',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'equip_equipment_sv_service_vendor_1_c',
      'join_key_lhs' => 'equip_equipment_sv_service_vendor_1equip_equipment_ida',
      'join_key_rhs' => 'equip_equipment_sv_service_vendor_1sv_service_vendor_idb',
    ),
  ),
  'table' => 'equip_equipment_sv_service_vendor_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'equip_equipment_sv_service_vendor_1equip_equipment_ida' => 
    array (
      'name' => 'equip_equipment_sv_service_vendor_1equip_equipment_ida',
      'type' => 'id',
    ),
    'equip_equipment_sv_service_vendor_1sv_service_vendor_idb' => 
    array (
      'name' => 'equip_equipment_sv_service_vendor_1sv_service_vendor_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_equip_equipment_sv_service_vendor_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_equip_equipment_sv_service_vendor_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'equip_equipment_sv_service_vendor_1equip_equipment_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_equip_equipment_sv_service_vendor_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'equip_equipment_sv_service_vendor_1sv_service_vendor_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'equip_equipment_sv_service_vendor_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'equip_equipment_sv_service_vendor_1sv_service_vendor_idb',
      ),
    ),
  ),
);