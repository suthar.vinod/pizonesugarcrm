<?php
// created: 2018-04-23 18:02:37
$dictionary["tm_tradeshow_management_m01_sales_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'tm_tradeshow_management_m01_sales_1' => 
    array (
      'lhs_module' => 'TM_Tradeshow_Management',
      'lhs_table' => 'tm_tradeshow_management',
      'lhs_key' => 'id',
      'rhs_module' => 'M01_Sales',
      'rhs_table' => 'm01_sales',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'tm_tradeshow_management_m01_sales_1_c',
      'join_key_lhs' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
      'join_key_rhs' => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
    ),
  ),
  'table' => 'tm_tradeshow_management_m01_sales_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida' => 
    array (
      'name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
      'type' => 'id',
    ),
    'tm_tradeshow_management_m01_sales_1m01_sales_idb' => 
    array (
      'name' => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_tm_tradeshow_management_m01_sales_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_tm_tradeshow_management_m01_sales_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_tm_tradeshow_management_m01_sales_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'tm_tradeshow_management_m01_sales_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'tm_tradeshow_management_m01_sales_1m01_sales_idb',
      ),
    ),
  ),
);