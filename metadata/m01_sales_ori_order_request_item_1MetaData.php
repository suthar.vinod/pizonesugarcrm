<?php
// created: 2021-10-19 10:26:07
$dictionary["m01_sales_ori_order_request_item_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'm01_sales_ori_order_request_item_1' => 
    array (
      'lhs_module' => 'M01_Sales',
      'lhs_table' => 'm01_sales',
      'lhs_key' => 'id',
      'rhs_module' => 'ORI_Order_Request_Item',
      'rhs_table' => 'ori_order_request_item',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'm01_sales_ori_order_request_item_1_c',
      'join_key_lhs' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
      'join_key_rhs' => 'm01_sales_ori_order_request_item_1ori_order_request_item_idb',
    ),
  ),
  'table' => 'm01_sales_ori_order_request_item_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'm01_sales_ori_order_request_item_1m01_sales_ida' => 
    array (
      'name' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
      'type' => 'id',
    ),
    'm01_sales_ori_order_request_item_1ori_order_request_item_idb' => 
    array (
      'name' => 'm01_sales_ori_order_request_item_1ori_order_request_item_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_m01_sales_ori_order_request_item_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_m01_sales_ori_order_request_item_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm01_sales_ori_order_request_item_1m01_sales_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_m01_sales_ori_order_request_item_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm01_sales_ori_order_request_item_1ori_order_request_item_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'm01_sales_ori_order_request_item_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'm01_sales_ori_order_request_item_1ori_order_request_item_idb',
      ),
    ),
  ),
);