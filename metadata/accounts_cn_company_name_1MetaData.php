<?php
// created: 2019-09-26 14:13:30
$dictionary["accounts_cn_company_name_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'accounts_cn_company_name_1' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'CN_Company_Name',
      'rhs_table' => 'cn_company_name',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_cn_company_name_1_c',
      'join_key_lhs' => 'accounts_cn_company_name_1accounts_ida',
      'join_key_rhs' => 'accounts_cn_company_name_1cn_company_name_idb',
    ),
  ),
  'table' => 'accounts_cn_company_name_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'accounts_cn_company_name_1accounts_ida' => 
    array (
      'name' => 'accounts_cn_company_name_1accounts_ida',
      'type' => 'id',
    ),
    'accounts_cn_company_name_1cn_company_name_idb' => 
    array (
      'name' => 'accounts_cn_company_name_1cn_company_name_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_accounts_cn_company_name_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_accounts_cn_company_name_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_cn_company_name_1accounts_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_accounts_cn_company_name_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_cn_company_name_1cn_company_name_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'accounts_cn_company_name_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'accounts_cn_company_name_1cn_company_name_idb',
      ),
    ),
  ),
);