<?php
// created: 2017-02-17 17:50:46
$dictionary["m03_work_product_abc12_work_product_activities_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'm03_work_product_abc12_work_product_activities_1' => 
    array (
      'lhs_module' => 'M03_Work_Product',
      'lhs_table' => 'm03_work_product',
      'lhs_key' => 'id',
      'rhs_module' => 'ABC12_Work_Product_Activities',
      'rhs_table' => 'abc12_work_product_activities',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'm03_work_product_abc12_work_product_activities_1_c',
      'join_key_lhs' => 'm03_work_p4898product_ida',
      'join_key_rhs' => 'm03_work_p9069ivities_idb',
    ),
  ),
  'table' => 'm03_work_product_abc12_work_product_activities_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'm03_work_p4898product_ida' => 
    array (
      'name' => 'm03_work_p4898product_ida',
      'type' => 'id',
    ),
    'm03_work_p9069ivities_idb' => 
    array (
      'name' => 'm03_work_p9069ivities_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'm03_work_product_abc12_work_product_activities_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'm03_work_product_abc12_work_product_activities_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm03_work_p4898product_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'm03_work_product_abc12_work_product_activities_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'm03_work_p9069ivities_idb',
      ),
    ),
  ),
);