<?php
// created: 2022-02-01 04:24:04
$dictionary["edoc_email_documents_a1a_critical_phase_inspectio_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'edoc_email_documents_a1a_critical_phase_inspectio_1' => 
    array (
      'lhs_module' => 'EDoc_Email_Documents',
      'lhs_table' => 'edoc_email_documents',
      'lhs_key' => 'id',
      'rhs_module' => 'A1A_Critical_Phase_Inspectio',
      'rhs_table' => 'a1a_critical_phase_inspectio',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'edoc_email_documents_a1a_critical_phase_inspectio_1_c',
      'join_key_lhs' => 'edoc_emailbf38cuments_ida',
      'join_key_rhs' => 'edoc_emailc542spectio_idb',
    ),
  ),
  'table' => 'edoc_email_documents_a1a_critical_phase_inspectio_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'edoc_emailbf38cuments_ida' => 
    array (
      'name' => 'edoc_emailbf38cuments_ida',
      'type' => 'id',
    ),
    'edoc_emailc542spectio_idb' => 
    array (
      'name' => 'edoc_emailc542spectio_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_edoc_email_documents_a1a_critical_phase_inspectio_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_edoc_email_documents_a1a_critical_phase_inspectio_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'edoc_emailbf38cuments_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_edoc_email_documents_a1a_critical_phase_inspectio_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'edoc_emailc542spectio_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'edoc_email_documents_a1a_critical_phase_inspectio_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'edoc_emailc542spectio_idb',
      ),
    ),
  ),
);