<?php
// created: 2021-12-07 12:33:38
$dictionary["nsc_namsa_sub_companies_m01_sales_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'nsc_namsa_sub_companies_m01_sales_1' => 
    array (
      'lhs_module' => 'NSC_NAMSA_Sub_Companies',
      'lhs_table' => 'nsc_namsa_sub_companies',
      'lhs_key' => 'id',
      'rhs_module' => 'M01_Sales',
      'rhs_table' => 'm01_sales',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'nsc_namsa_sub_companies_m01_sales_1_c',
      'join_key_lhs' => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
      'join_key_rhs' => 'nsc_namsa_sub_companies_m01_sales_1m01_sales_idb',
    ),
  ),
  'table' => 'nsc_namsa_sub_companies_m01_sales_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida' => 
    array (
      'name' => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
      'type' => 'id',
    ),
    'nsc_namsa_sub_companies_m01_sales_1m01_sales_idb' => 
    array (
      'name' => 'nsc_namsa_sub_companies_m01_sales_1m01_sales_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_nsc_namsa_sub_companies_m01_sales_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_nsc_namsa_sub_companies_m01_sales_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_nsc_namsa_sub_companies_m01_sales_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'nsc_namsa_sub_companies_m01_sales_1m01_sales_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'nsc_namsa_sub_companies_m01_sales_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'nsc_namsa_sub_companies_m01_sales_1m01_sales_idb',
      ),
    ),
  ),
);