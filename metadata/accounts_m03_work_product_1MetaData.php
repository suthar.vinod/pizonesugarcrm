<?php
// created: 2018-12-26 15:29:08
$dictionary["accounts_m03_work_product_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'accounts_m03_work_product_1' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'M03_Work_Product',
      'rhs_table' => 'm03_work_product',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_m03_work_product_1_c',
      'join_key_lhs' => 'accounts_m03_work_product_1accounts_ida',
      'join_key_rhs' => 'accounts_m03_work_product_1m03_work_product_idb',
    ),
  ),
  'table' => 'accounts_m03_work_product_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'accounts_m03_work_product_1accounts_ida' => 
    array (
      'name' => 'accounts_m03_work_product_1accounts_ida',
      'type' => 'id',
    ),
    'accounts_m03_work_product_1m03_work_product_idb' => 
    array (
      'name' => 'accounts_m03_work_product_1m03_work_product_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_accounts_m03_work_product_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_accounts_m03_work_product_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_m03_work_product_1accounts_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_accounts_m03_work_product_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_m03_work_product_1m03_work_product_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'accounts_m03_work_product_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'accounts_m03_work_product_1m03_work_product_idb',
      ),
    ),
  ),
);