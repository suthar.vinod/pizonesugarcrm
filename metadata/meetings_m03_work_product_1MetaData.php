<?php
// created: 2021-04-01 09:05:41
$dictionary["meetings_m03_work_product_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'meetings_m03_work_product_1' => 
    array (
      'lhs_module' => 'Meetings',
      'lhs_table' => 'meetings',
      'lhs_key' => 'id',
      'rhs_module' => 'M03_Work_Product',
      'rhs_table' => 'm03_work_product',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'meetings_m03_work_product_1_c',
      'join_key_lhs' => 'meetings_m03_work_product_1meetings_ida',
      'join_key_rhs' => 'meetings_m03_work_product_1m03_work_product_idb',
    ),
  ),
  'table' => 'meetings_m03_work_product_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'meetings_m03_work_product_1meetings_ida' => 
    array (
      'name' => 'meetings_m03_work_product_1meetings_ida',
      'type' => 'id',
    ),
    'meetings_m03_work_product_1m03_work_product_idb' => 
    array (
      'name' => 'meetings_m03_work_product_1m03_work_product_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_meetings_m03_work_product_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_meetings_m03_work_product_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'meetings_m03_work_product_1meetings_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_meetings_m03_work_product_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'meetings_m03_work_product_1m03_work_product_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'meetings_m03_work_product_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'meetings_m03_work_product_1meetings_ida',
        1 => 'meetings_m03_work_product_1m03_work_product_idb',
      ),
    ),
  ),
);