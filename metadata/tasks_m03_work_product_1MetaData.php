<?php
// created: 2017-08-29 00:56:43
$dictionary['tasks_m03_work_product_1'] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'tasks_m03_work_product_1' => 
    array (
      'lhs_module' => 'Tasks',
      'lhs_table' => 'tasks',
      'lhs_key' => 'id',
      'rhs_module' => 'M03_Work_Product',
      'rhs_table' => 'm03_work_product',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'tasks_m03_work_product_1_c',
      'join_key_lhs' => 'tasks_m03_work_product_1tasks_ida',
      'join_key_rhs' => 'tasks_m03_work_product_1m03_work_product_idb',
    ),
  ),
  'table' => 'tasks_m03_work_product_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    'tasks_m03_work_product_1tasks_ida' => 
    array (
      'name' => 'tasks_m03_work_product_1tasks_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    'tasks_m03_work_product_1m03_work_product_idb' => 
    array (
      'name' => 'tasks_m03_work_product_1m03_work_product_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'tasks_m03_work_product_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'tasks_m03_work_product_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'tasks_m03_work_product_1tasks_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'tasks_m03_work_product_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'tasks_m03_work_product_1m03_work_product_idb',
      ),
    ),
  ),
);