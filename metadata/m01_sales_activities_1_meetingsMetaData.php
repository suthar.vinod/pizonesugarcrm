<?php
// created: 2018-12-10 23:02:13
$dictionary["m01_sales_activities_1_meetings"] = array (
  'relationships' => 
  array (
    'm01_sales_activities_1_meetings' => 
    array (
      'lhs_module' => 'M01_Sales',
      'lhs_table' => 'm01_sales',
      'lhs_key' => 'id',
      'rhs_module' => 'Meetings',
      'rhs_table' => 'meetings',
      'relationship_role_column_value' => 'M01_Sales',
      'rhs_key' => 'parent_id',
      'relationship_type' => 'one-to-many',
      'relationship_role_column' => 'parent_type',
    ),
  ),
  'fields' => '',
  'indices' => '',
  'table' => '',
);