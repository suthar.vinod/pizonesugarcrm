<?php
// created: 2021-12-07 12:00:27
$dictionary["tc_namsa_test_codes_m03_work_product_code_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'tc_namsa_test_codes_m03_work_product_code_1' => 
    array (
      'lhs_module' => 'TC_NAMSA_Test_Codes',
      'lhs_table' => 'tc_namsa_test_codes',
      'lhs_key' => 'id',
      'rhs_module' => 'M03_Work_Product_Code',
      'rhs_table' => 'm03_work_product_code',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'tc_namsa_test_codes_m03_work_product_code_1_c',
      'join_key_lhs' => 'tc_namsa_t49d3t_codes_ida',
      'join_key_rhs' => 'tc_namsa_tc500ct_code_idb',
    ),
  ),
  'table' => 'tc_namsa_test_codes_m03_work_product_code_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'tc_namsa_t49d3t_codes_ida' => 
    array (
      'name' => 'tc_namsa_t49d3t_codes_ida',
      'type' => 'id',
    ),
    'tc_namsa_tc500ct_code_idb' => 
    array (
      'name' => 'tc_namsa_tc500ct_code_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_tc_namsa_test_codes_m03_work_product_code_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_tc_namsa_test_codes_m03_work_product_code_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'tc_namsa_t49d3t_codes_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_tc_namsa_test_codes_m03_work_product_code_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'tc_namsa_tc500ct_code_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'tc_namsa_test_codes_m03_work_product_code_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'tc_namsa_tc500ct_code_idb',
      ),
    ),
  ),
);