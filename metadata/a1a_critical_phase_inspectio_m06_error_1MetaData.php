<?php
// created: 2019-02-21 13:44:24
$dictionary["a1a_critical_phase_inspectio_m06_error_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'a1a_critical_phase_inspectio_m06_error_1' => 
    array (
      'lhs_module' => 'A1A_Critical_Phase_Inspectio',
      'lhs_table' => 'a1a_critical_phase_inspectio',
      'lhs_key' => 'id',
      'rhs_module' => 'M06_Error',
      'rhs_table' => 'm06_error',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'a1a_critical_phase_inspectio_m06_error_1_c',
      'join_key_lhs' => 'a1a_critic9e89spectio_ida',
      'join_key_rhs' => 'a1a_critical_phase_inspectio_m06_error_1m06_error_idb',
    ),
  ),
  'table' => 'a1a_critical_phase_inspectio_m06_error_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'a1a_critic9e89spectio_ida' => 
    array (
      'name' => 'a1a_critic9e89spectio_ida',
      'type' => 'id',
    ),
    'a1a_critical_phase_inspectio_m06_error_1m06_error_idb' => 
    array (
      'name' => 'a1a_critical_phase_inspectio_m06_error_1m06_error_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_a1a_critical_phase_inspectio_m06_error_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_a1a_critical_phase_inspectio_m06_error_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'a1a_critic9e89spectio_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_a1a_critical_phase_inspectio_m06_error_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'a1a_critical_phase_inspectio_m06_error_1m06_error_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'a1a_critical_phase_inspectio_m06_error_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'a1a_critical_phase_inspectio_m06_error_1m06_error_idb',
      ),
    ),
  ),
);