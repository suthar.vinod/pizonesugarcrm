<?php
// created: 2021-11-09 10:19:23
$dictionary["ic_inventory_collection_im_inventory_management_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'ic_inventory_collection_im_inventory_management_1' => 
    array (
      'lhs_module' => 'IC_Inventory_Collection',
      'lhs_table' => 'ic_inventory_collection',
      'lhs_key' => 'id',
      'rhs_module' => 'IM_Inventory_Management',
      'rhs_table' => 'im_inventory_management',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'ic_inventory_collection_im_inventory_management_1_c',
      'join_key_lhs' => 'ic_inventoe24election_ida',
      'join_key_rhs' => 'ic_inventod6aaagement_idb',
    ),
  ),
  'table' => 'ic_inventory_collection_im_inventory_management_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'ic_inventoe24election_ida' => 
    array (
      'name' => 'ic_inventoe24election_ida',
      'type' => 'id',
    ),
    'ic_inventod6aaagement_idb' => 
    array (
      'name' => 'ic_inventod6aaagement_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_ic_inventory_collection_im_inventory_management_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_ic_inventory_collection_im_inventory_management_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ic_inventoe24election_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_ic_inventory_collection_im_inventory_management_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ic_inventod6aaagement_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'ic_inventory_collection_im_inventory_management_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'ic_inventod6aaagement_idb',
      ),
    ),
  ),
);