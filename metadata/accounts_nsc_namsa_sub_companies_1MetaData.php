<?php
// created: 2021-12-07 12:21:22
$dictionary["accounts_nsc_namsa_sub_companies_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'accounts_nsc_namsa_sub_companies_1' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'NSC_NAMSA_Sub_Companies',
      'rhs_table' => 'nsc_namsa_sub_companies',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_nsc_namsa_sub_companies_1_c',
      'join_key_lhs' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
      'join_key_rhs' => 'accounts_nsc_namsa_sub_companies_1nsc_namsa_sub_companies_idb',
    ),
  ),
  'table' => 'accounts_nsc_namsa_sub_companies_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'accounts_nsc_namsa_sub_companies_1accounts_ida' => 
    array (
      'name' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
      'type' => 'id',
    ),
    'accounts_nsc_namsa_sub_companies_1nsc_namsa_sub_companies_idb' => 
    array (
      'name' => 'accounts_nsc_namsa_sub_companies_1nsc_namsa_sub_companies_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_accounts_nsc_namsa_sub_companies_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_accounts_nsc_namsa_sub_companies_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_accounts_nsc_namsa_sub_companies_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_nsc_namsa_sub_companies_1nsc_namsa_sub_companies_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'accounts_nsc_namsa_sub_companies_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'accounts_nsc_namsa_sub_companies_1nsc_namsa_sub_companies_idb',
      ),
    ),
  ),
);