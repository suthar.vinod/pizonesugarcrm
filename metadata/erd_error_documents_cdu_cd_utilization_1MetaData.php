<?php
// created: 2021-06-29 08:02:02
$dictionary["erd_error_documents_cdu_cd_utilization_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'erd_error_documents_cdu_cd_utilization_1' => 
    array (
      'lhs_module' => 'Erd_Error_Documents',
      'lhs_table' => 'erd_error_documents',
      'lhs_key' => 'id',
      'rhs_module' => 'CDU_CD_Utilization',
      'rhs_table' => 'cdu_cd_utilization',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'erd_error_documents_cdu_cd_utilization_1_c',
      'join_key_lhs' => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
      'join_key_rhs' => 'erd_error_documents_cdu_cd_utilization_1cdu_cd_utilization_idb',
    ),
  ),
  'table' => 'erd_error_documents_cdu_cd_utilization_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida' => 
    array (
      'name' => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
      'type' => 'id',
    ),
    'erd_error_documents_cdu_cd_utilization_1cdu_cd_utilization_idb' => 
    array (
      'name' => 'erd_error_documents_cdu_cd_utilization_1cdu_cd_utilization_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_erd_error_documents_cdu_cd_utilization_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_erd_error_documents_cdu_cd_utilization_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_erd_error_documents_cdu_cd_utilization_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'erd_error_documents_cdu_cd_utilization_1cdu_cd_utilization_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'erd_error_documents_cdu_cd_utilization_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'erd_error_documents_cdu_cd_utilization_1cdu_cd_utilization_idb',
      ),
    ),
  ),
);