<?php
// created: 2017-08-29 00:56:43
$dictionary['m01_sales_m03_work_product_1'] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'm01_sales_m03_work_product_1' => 
    array (
      'lhs_module' => 'M01_Sales',
      'lhs_table' => 'm01_sales',
      'lhs_key' => 'id',
      'rhs_module' => 'M03_Work_Product',
      'rhs_table' => 'm03_work_product',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'm01_sales_m03_work_product_1_c',
      'join_key_lhs' => 'm01_sales_m03_work_product_1m01_sales_ida',
      'join_key_rhs' => 'm01_sales_m03_work_product_1m03_work_product_idb',
    ),
  ),
  'table' => 'm01_sales_m03_work_product_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    'm01_sales_m03_work_product_1m01_sales_ida' => 
    array (
      'name' => 'm01_sales_m03_work_product_1m01_sales_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    'm01_sales_m03_work_product_1m03_work_product_idb' => 
    array (
      'name' => 'm01_sales_m03_work_product_1m03_work_product_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'm01_sales_m03_work_product_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'm01_sales_m03_work_product_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm01_sales_m03_work_product_1m01_sales_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'm01_sales_m03_work_product_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'm01_sales_m03_work_product_1m03_work_product_idb',
      ),
    ),
  ),
);