<?php
// created: 2019-02-13 23:29:07
$dictionary["tm_tradeshow_management_td_tradeshow_documents_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'tm_tradeshow_management_td_tradeshow_documents_1' => 
    array (
      'lhs_module' => 'TM_Tradeshow_Management',
      'lhs_table' => 'tm_tradeshow_management',
      'lhs_key' => 'id',
      'rhs_module' => 'TD_Tradeshow_Documents',
      'rhs_table' => 'td_tradeshow_documents',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'tm_tradeshow_management_td_tradeshow_documents_1_c',
      'join_key_lhs' => 'tm_tradeshb931agement_ida',
      'join_key_rhs' => 'tm_tradeshfddfcuments_idb',
    ),
  ),
  'table' => 'tm_tradeshow_management_td_tradeshow_documents_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'tm_tradeshb931agement_ida' => 
    array (
      'name' => 'tm_tradeshb931agement_ida',
      'type' => 'id',
    ),
    'tm_tradeshfddfcuments_idb' => 
    array (
      'name' => 'tm_tradeshfddfcuments_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_tm_tradeshow_management_td_tradeshow_documents_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_tm_tradeshow_management_td_tradeshow_documents_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'tm_tradeshb931agement_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_tm_tradeshow_management_td_tradeshow_documents_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'tm_tradeshfddfcuments_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'tm_tradeshow_management_td_tradeshow_documents_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'tm_tradeshfddfcuments_idb',
      ),
    ),
  ),
);