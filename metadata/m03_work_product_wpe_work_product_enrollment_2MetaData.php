<?php
// created: 2020-12-15 06:35:00
$dictionary["m03_work_product_wpe_work_product_enrollment_2"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'm03_work_product_wpe_work_product_enrollment_2' => 
    array (
      'lhs_module' => 'M03_Work_Product',
      'lhs_table' => 'm03_work_product',
      'lhs_key' => 'id',
      'rhs_module' => 'WPE_Work_Product_Enrollment',
      'rhs_table' => 'wpe_work_product_enrollment',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'm03_work_product_wpe_work_product_enrollment_2_c',
      'join_key_lhs' => 'm03_work_p9f23product_ida',
      'join_key_rhs' => 'm03_work_p90c4ollment_idb',
    ),
  ),
  'table' => 'm03_work_product_wpe_work_product_enrollment_2_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'm03_work_p9f23product_ida' => 
    array (
      'name' => 'm03_work_p9f23product_ida',
      'type' => 'id',
    ),
    'm03_work_p90c4ollment_idb' => 
    array (
      'name' => 'm03_work_p90c4ollment_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_m03_work_product_wpe_work_product_enrollment_2_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_m03_work_product_wpe_work_product_enrollment_2_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm03_work_p9f23product_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_m03_work_product_wpe_work_product_enrollment_2_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm03_work_p90c4ollment_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'm03_work_product_wpe_work_product_enrollment_2_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'm03_work_p90c4ollment_idb',
      ),
    ),
  ),
);