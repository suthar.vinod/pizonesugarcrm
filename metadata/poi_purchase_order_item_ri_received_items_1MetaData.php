<?php
// created: 2021-10-19 09:22:27
$dictionary["poi_purchase_order_item_ri_received_items_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'poi_purchase_order_item_ri_received_items_1' => 
    array (
      'lhs_module' => 'POI_Purchase_Order_Item',
      'lhs_table' => 'poi_purchase_order_item',
      'lhs_key' => 'id',
      'rhs_module' => 'RI_Received_Items',
      'rhs_table' => 'ri_received_items',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'poi_purchase_order_item_ri_received_items_1_c',
      'join_key_lhs' => 'poi_purcha665cer_item_ida',
      'join_key_rhs' => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
    ),
  ),
  'table' => 'poi_purchase_order_item_ri_received_items_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'poi_purcha665cer_item_ida' => 
    array (
      'name' => 'poi_purcha665cer_item_ida',
      'type' => 'id',
    ),
    'poi_purchase_order_item_ri_received_items_1ri_received_items_idb' => 
    array (
      'name' => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_poi_purchase_order_item_ri_received_items_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_poi_purchase_order_item_ri_received_items_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'poi_purcha665cer_item_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_poi_purchase_order_item_ri_received_items_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'poi_purchase_order_item_ri_received_items_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'poi_purchase_order_item_ri_received_items_1ri_received_items_idb',
      ),
    ),
  ),
);