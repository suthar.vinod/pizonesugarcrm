<?php
// created: 2019-10-31 11:25:05
$dictionary["ta_tradeshow_activities_tasks_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'ta_tradeshow_activities_tasks_1' => 
    array (
      'lhs_module' => 'TA_Tradeshow_Activities',
      'lhs_table' => 'ta_tradeshow_activities',
      'lhs_key' => 'id',
      'rhs_module' => 'Tasks',
      'rhs_table' => 'tasks',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'ta_tradeshow_activities_tasks_1_c',
      'join_key_lhs' => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
      'join_key_rhs' => 'ta_tradeshow_activities_tasks_1tasks_idb',
    ),
  ),
  'table' => 'ta_tradeshow_activities_tasks_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida' => 
    array (
      'name' => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
      'type' => 'id',
    ),
    'ta_tradeshow_activities_tasks_1tasks_idb' => 
    array (
      'name' => 'ta_tradeshow_activities_tasks_1tasks_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_ta_tradeshow_activities_tasks_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_ta_tradeshow_activities_tasks_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_ta_tradeshow_activities_tasks_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ta_tradeshow_activities_tasks_1tasks_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'ta_tradeshow_activities_tasks_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'ta_tradeshow_activities_tasks_1tasks_idb',
      ),
    ),
  ),
);