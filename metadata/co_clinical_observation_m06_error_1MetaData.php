<?php
// created: 2020-01-07 20:01:08
$dictionary["co_clinical_observation_m06_error_1"] = array (
  'true_relationship_type' => 'one-to-one',
  'from_studio' => true,
  'relationships' => 
  array (
    'co_clinical_observation_m06_error_1' => 
    array (
      'lhs_module' => 'CO_Clinical_Observation',
      'lhs_table' => 'co_clinical_observation',
      'lhs_key' => 'id',
      'rhs_module' => 'M06_Error',
      'rhs_table' => 'm06_error',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'co_clinical_observation_m06_error_1_c',
      'join_key_lhs' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
      'join_key_rhs' => 'co_clinical_observation_m06_error_1m06_error_idb',
    ),
  ),
  'table' => 'co_clinical_observation_m06_error_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'co_clinical_observation_m06_error_1co_clinical_observation_ida' => 
    array (
      'name' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
      'type' => 'id',
    ),
    'co_clinical_observation_m06_error_1m06_error_idb' => 
    array (
      'name' => 'co_clinical_observation_m06_error_1m06_error_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_co_clinical_observation_m06_error_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_co_clinical_observation_m06_error_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_co_clinical_observation_m06_error_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'co_clinical_observation_m06_error_1m06_error_idb',
        1 => 'deleted',
      ),
    ),
  ),
);