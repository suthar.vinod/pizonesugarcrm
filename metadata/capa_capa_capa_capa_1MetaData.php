<?php
// created: 2022-02-03 07:32:44
$dictionary["capa_capa_capa_capa_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'capa_capa_capa_capa_1' => 
    array (
      'lhs_module' => 'CAPA_CAPA',
      'lhs_table' => 'capa_capa',
      'lhs_key' => 'id',
      'rhs_module' => 'CAPA_CAPA',
      'rhs_table' => 'capa_capa',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'capa_capa_capa_capa_1_c',
      'join_key_lhs' => 'capa_capa_capa_capa_1capa_capa_ida',
      'join_key_rhs' => 'capa_capa_capa_capa_1capa_capa_idb',
    ),
  ),
  'table' => 'capa_capa_capa_capa_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'capa_capa_capa_capa_1capa_capa_ida' => 
    array (
      'name' => 'capa_capa_capa_capa_1capa_capa_ida',
      'type' => 'id',
    ),
    'capa_capa_capa_capa_1capa_capa_idb' => 
    array (
      'name' => 'capa_capa_capa_capa_1capa_capa_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_capa_capa_capa_capa_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_capa_capa_capa_capa_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'capa_capa_capa_capa_1capa_capa_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_capa_capa_capa_capa_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'capa_capa_capa_capa_1capa_capa_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'capa_capa_capa_capa_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'capa_capa_capa_capa_1capa_capa_idb',
      ),
    ),
  ),
);