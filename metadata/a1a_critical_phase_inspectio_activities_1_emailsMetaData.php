<?php
// created: 2017-09-13 15:16:58
$dictionary["a1a_critical_phase_inspectio_activities_1_emails"] = array (
  'relationships' => 
  array (
    'a1a_critical_phase_inspectio_activities_1_emails' => 
    array (
      'lhs_module' => 'A1A_Critical_Phase_Inspectio',
      'lhs_table' => 'a1a_critical_phase_inspectio',
      'lhs_key' => 'id',
      'rhs_module' => 'Emails',
      'rhs_table' => 'emails',
      'relationship_role_column_value' => 'A1A_Critical_Phase_Inspectio',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'emails_beans',
      'join_key_rhs' => 'email_id',
      'join_key_lhs' => 'bean_id',
      'relationship_role_column' => 'bean_module',
    ),
  ),
  'fields' => '',
  'indices' => '',
  'table' => '',
);