<?php
// created: 2018-02-05 14:21:02
$dictionary["m99_protocol_amendments_m03_work_product"] = array (
  'true_relationship_type' => 'many-to-many',
  'relationships' => 
  array (
    'm99_protocol_amendments_m03_work_product' => 
    array (
      'lhs_module' => 'M99_Protocol_Amendments',
      'lhs_table' => 'm99_protocol_amendments',
      'lhs_key' => 'id',
      'rhs_module' => 'M03_Work_Product',
      'rhs_table' => 'm03_work_product',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'm99_protocol_amendments_m03_work_product_c',
      'join_key_lhs' => 'm99_protoc2862ndments_ida',
      'join_key_rhs' => 'm99_protocol_amendments_m03_work_productm03_work_product_idb',
    ),
  ),
  'table' => 'm99_protocol_amendments_m03_work_product_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'm99_protoc2862ndments_ida' => 
    array (
      'name' => 'm99_protoc2862ndments_ida',
      'type' => 'id',
    ),
    'm99_protocol_amendments_m03_work_productm03_work_product_idb' => 
    array (
      'name' => 'm99_protocol_amendments_m03_work_productm03_work_product_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_m99_protocol_amendments_m03_work_product_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_m99_protocol_amendments_m03_work_product_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm99_protoc2862ndments_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_m99_protocol_amendments_m03_work_product_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm99_protocol_amendments_m03_work_productm03_work_product_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'm99_protocol_amendments_m03_work_product_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'm99_protoc2862ndments_ida',
        1 => 'm99_protocol_amendments_m03_work_productm03_work_product_idb',
      ),
    ),
  ),
);