<?php
// created: 2018-06-01 21:24:07
$dictionary["erqc_error_qc_employees_m06_error_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'erqc_error_qc_employees_m06_error_1' => 
    array (
      'lhs_module' => 'ErQC_Error_QC_Employees',
      'lhs_table' => 'erqc_error_qc_employees',
      'lhs_key' => 'id',
      'rhs_module' => 'M06_Error',
      'rhs_table' => 'm06_error',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'erqc_error_qc_employees_m06_error_1_c',
      'join_key_lhs' => 'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida',
      'join_key_rhs' => 'erqc_error_qc_employees_m06_error_1m06_error_idb',
    ),
  ),
  'table' => 'erqc_error_qc_employees_m06_error_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida' => 
    array (
      'name' => 'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida',
      'type' => 'id',
    ),
    'erqc_error_qc_employees_m06_error_1m06_error_idb' => 
    array (
      'name' => 'erqc_error_qc_employees_m06_error_1m06_error_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_erqc_error_qc_employees_m06_error_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_erqc_error_qc_employees_m06_error_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_erqc_error_qc_employees_m06_error_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'erqc_error_qc_employees_m06_error_1m06_error_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'erqc_error_qc_employees_m06_error_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida',
        1 => 'erqc_error_qc_employees_m06_error_1m06_error_idb',
      ),
    ),
  ),
);