<?php

$dictionary['wsys_observations_audit'] = array(
    'table'         => 'wsys_observations_audit',
    'relationships' => array(),
    'fields'        => array(
        'id'                      => array(
            "name"     => "id",
            "type"     => "id",
            "required" => true,
        ),
        'observation_id'          => array(
            "name"     => "observation_id",
            "type"     => "id",
            "required" => true,
        ),
        'inventory_management_id' => array(
            "name" => "inventory_management_id",
            "type" => "id",
        ),
        'inventory_item_id'       => array(
            "name" => "inventory_item_id",
            "type" => "id",
        ),
        'date_entered'            => array(
            "name"     => "date_entered",
            "type"     => "datetime",
            "required" => true,
            "readonly" => true,
        ),
        'date_modified'           => array(
            "name"     => "date_modified",
            "type"     => "datetime",
            "required" => true,
            "readonly" => true,
        ),
        'deleted'                 => array(
            'name'    => 'deleted',
            'type'    => 'bool',
            'default' => '0',
        ),
    ),
    'indices'       => array(
        array(
            'name'   => 'wsys_observations_auditpk',
            'type'   => 'primary',
            'fields' => array('id'),
        ),
        array(
            'name'   => 'wsys_observations_observation_auditpk',
            'type'   => 'index',
            'fields' => array('observation_id'),
        ),
        array(
            'name'   => 'wsys_observations_im_auditpk',
            'type'   => 'index',
            'fields' => array('inventory_management_id'),
        ),
        array(
            'name'   => 'wsys_observations_ii_auditpk',
            'type'   => 'index',
            'fields' => array('inventory_item_id'),
        ),
    ),
);
