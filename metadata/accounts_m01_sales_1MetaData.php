<?php
// created: 2017-08-29 00:56:42
$dictionary['accounts_m01_sales_1'] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'accounts_m01_sales_1' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'M01_Sales',
      'rhs_table' => 'm01_sales',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_m01_sales_1_c',
      'join_key_lhs' => 'accounts_m01_sales_1accounts_ida',
      'join_key_rhs' => 'accounts_m01_sales_1m01_sales_idb',
    ),
  ),
  'table' => 'accounts_m01_sales_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    'accounts_m01_sales_1accounts_ida' => 
    array (
      'name' => 'accounts_m01_sales_1accounts_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    'accounts_m01_sales_1m01_sales_idb' => 
    array (
      'name' => 'accounts_m01_sales_1m01_sales_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'accounts_m01_sales_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'accounts_m01_sales_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_m01_sales_1accounts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'accounts_m01_sales_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'accounts_m01_sales_1m01_sales_idb',
      ),
    ),
  ),
);