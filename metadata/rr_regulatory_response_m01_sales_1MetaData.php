<?php
// created: 2019-07-09 11:54:56
$dictionary["rr_regulatory_response_m01_sales_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'rr_regulatory_response_m01_sales_1' => 
    array (
      'lhs_module' => 'RR_Regulatory_Response',
      'lhs_table' => 'rr_regulatory_response',
      'lhs_key' => 'id',
      'rhs_module' => 'M01_Sales',
      'rhs_table' => 'm01_sales',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'rr_regulatory_response_m01_sales_1_c',
      'join_key_lhs' => 'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida',
      'join_key_rhs' => 'rr_regulatory_response_m01_sales_1m01_sales_idb',
    ),
  ),
  'table' => 'rr_regulatory_response_m01_sales_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida' => 
    array (
      'name' => 'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida',
      'type' => 'id',
    ),
    'rr_regulatory_response_m01_sales_1m01_sales_idb' => 
    array (
      'name' => 'rr_regulatory_response_m01_sales_1m01_sales_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_rr_regulatory_response_m01_sales_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_rr_regulatory_response_m01_sales_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_rr_regulatory_response_m01_sales_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'rr_regulatory_response_m01_sales_1m01_sales_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'rr_regulatory_response_m01_sales_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida',
        1 => 'rr_regulatory_response_m01_sales_1m01_sales_idb',
      ),
    ),
  ),
);