<?php
// created: 2019-02-25 15:17:34
$dictionary["equip_equipment_efs_equipment_facility_servi_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'equip_equipment_efs_equipment_facility_servi_1' => 
    array (
      'lhs_module' => 'Equip_Equipment',
      'lhs_table' => 'equip_equipment',
      'lhs_key' => 'id',
      'rhs_module' => 'EFS_Equipment_Facility_Servi',
      'rhs_table' => 'efs_equipment_facility_servi',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'equip_equipment_efs_equipment_facility_servi_1_c',
      'join_key_lhs' => 'equip_equia9d9uipment_ida',
      'join_key_rhs' => 'equip_equi3f6dy_servi_idb',
    ),
  ),
  'table' => 'equip_equipment_efs_equipment_facility_servi_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'equip_equia9d9uipment_ida' => 
    array (
      'name' => 'equip_equia9d9uipment_ida',
      'type' => 'id',
    ),
    'equip_equi3f6dy_servi_idb' => 
    array (
      'name' => 'equip_equi3f6dy_servi_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_equip_equipment_efs_equipment_facility_servi_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_equip_equipment_efs_equipment_facility_servi_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'equip_equia9d9uipment_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_equip_equipment_efs_equipment_facility_servi_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'equip_equi3f6dy_servi_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'equip_equipment_efs_equipment_facility_servi_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'equip_equi3f6dy_servi_idb',
      ),
    ),
  ),
);