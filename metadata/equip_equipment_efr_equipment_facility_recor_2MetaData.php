<?php
// created: 2019-02-20 15:09:02
$dictionary["equip_equipment_efr_equipment_facility_recor_2"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'equip_equipment_efr_equipment_facility_recor_2' => 
    array (
      'lhs_module' => 'Equip_Equipment',
      'lhs_table' => 'equip_equipment',
      'lhs_key' => 'id',
      'rhs_module' => 'EFR_Equipment_Facility_Recor',
      'rhs_table' => 'efr_equipment_facility_recor',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'equip_equipment_efr_equipment_facility_recor_2_c',
      'join_key_lhs' => 'equip_equi01e2uipment_ida',
      'join_key_rhs' => 'equip_equiffd2y_recor_idb',
    ),
  ),
  'table' => 'equip_equipment_efr_equipment_facility_recor_2_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'equip_equi01e2uipment_ida' => 
    array (
      'name' => 'equip_equi01e2uipment_ida',
      'type' => 'id',
    ),
    'equip_equiffd2y_recor_idb' => 
    array (
      'name' => 'equip_equiffd2y_recor_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_equip_equipment_efr_equipment_facility_recor_2_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_equip_equipment_efr_equipment_facility_recor_2_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'equip_equi01e2uipment_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_equip_equipment_efr_equipment_facility_recor_2_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'equip_equiffd2y_recor_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'equip_equipment_efr_equipment_facility_recor_2_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'equip_equiffd2y_recor_idb',
      ),
    ),
  ),
);