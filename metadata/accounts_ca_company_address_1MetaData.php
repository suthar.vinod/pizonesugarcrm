<?php
// created: 2019-07-03 12:14:00
$dictionary["accounts_ca_company_address_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'accounts_ca_company_address_1' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'CA_Company_Address',
      'rhs_table' => 'ca_company_address',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_ca_company_address_1_c',
      'join_key_lhs' => 'accounts_ca_company_address_1accounts_ida',
      'join_key_rhs' => 'accounts_ca_company_address_1ca_company_address_idb',
    ),
  ),
  'table' => 'accounts_ca_company_address_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'accounts_ca_company_address_1accounts_ida' => 
    array (
      'name' => 'accounts_ca_company_address_1accounts_ida',
      'type' => 'id',
    ),
    'accounts_ca_company_address_1ca_company_address_idb' => 
    array (
      'name' => 'accounts_ca_company_address_1ca_company_address_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_accounts_ca_company_address_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_accounts_ca_company_address_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_ca_company_address_1accounts_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_accounts_ca_company_address_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_ca_company_address_1ca_company_address_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'accounts_ca_company_address_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'accounts_ca_company_address_1ca_company_address_idb',
      ),
    ),
  ),
);