<?php
// created: 2019-02-20 13:19:54
$dictionary["anml_animals_rt_room_transfer_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'anml_animals_rt_room_transfer_1' => 
    array (
      'lhs_module' => 'ANML_Animals',
      'lhs_table' => 'anml_animals',
      'lhs_key' => 'id',
      'rhs_module' => 'RT_Room_Transfer',
      'rhs_table' => 'rt_room_transfer',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'anml_animals_rt_room_transfer_1_c',
      'join_key_lhs' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
      'join_key_rhs' => 'anml_animals_rt_room_transfer_1rt_room_transfer_idb',
    ),
  ),
  'table' => 'anml_animals_rt_room_transfer_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'anml_animals_rt_room_transfer_1anml_animals_ida' => 
    array (
      'name' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
      'type' => 'id',
    ),
    'anml_animals_rt_room_transfer_1rt_room_transfer_idb' => 
    array (
      'name' => 'anml_animals_rt_room_transfer_1rt_room_transfer_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_anml_animals_rt_room_transfer_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_anml_animals_rt_room_transfer_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'anml_animals_rt_room_transfer_1anml_animals_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_anml_animals_rt_room_transfer_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'anml_animals_rt_room_transfer_1rt_room_transfer_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'anml_animals_rt_room_transfer_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'anml_animals_rt_room_transfer_1rt_room_transfer_idb',
      ),
    ),
  ),
);