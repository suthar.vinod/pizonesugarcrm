<?php
// created: 2020-07-07 08:16:55
$dictionary["m06_error_m06_error_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'm06_error_m06_error_1' => 
    array (
      'lhs_module' => 'M06_Error',
      'lhs_table' => 'm06_error',
      'lhs_key' => 'id',
      'rhs_module' => 'M06_Error',
      'rhs_table' => 'm06_error',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'm06_error_m06_error_1_c',
      'join_key_lhs' => 'm06_error_m06_error_1m06_error_ida',
      'join_key_rhs' => 'm06_error_m06_error_1m06_error_idb',
    ),
  ),
  'table' => 'm06_error_m06_error_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'm06_error_m06_error_1m06_error_ida' => 
    array (
      'name' => 'm06_error_m06_error_1m06_error_ida',
      'type' => 'id',
    ),
    'm06_error_m06_error_1m06_error_idb' => 
    array (
      'name' => 'm06_error_m06_error_1m06_error_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_m06_error_m06_error_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_m06_error_m06_error_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm06_error_m06_error_1m06_error_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_m06_error_m06_error_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'm06_error_m06_error_1m06_error_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'm06_error_m06_error_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'm06_error_m06_error_1m06_error_idb',
      ),
    ),
  ),
);