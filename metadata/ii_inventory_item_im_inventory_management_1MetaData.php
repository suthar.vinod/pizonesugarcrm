<?php
// created: 2021-11-09 09:39:34
$dictionary["ii_inventory_item_im_inventory_management_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'ii_inventory_item_im_inventory_management_1' => 
    array (
      'lhs_module' => 'II_Inventory_Item',
      'lhs_table' => 'ii_inventory_item',
      'lhs_key' => 'id',
      'rhs_module' => 'IM_Inventory_Management',
      'rhs_table' => 'im_inventory_management',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'ii_inventory_item_im_inventory_management_1_c',
      'join_key_lhs' => 'ii_inventory_item_im_inventory_management_1ii_inventory_item_ida',
      'join_key_rhs' => 'ii_invento5fdeagement_idb',
    ),
  ),
  'table' => 'ii_inventory_item_im_inventory_management_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'ii_inventory_item_im_inventory_management_1ii_inventory_item_ida' => 
    array (
      'name' => 'ii_inventory_item_im_inventory_management_1ii_inventory_item_ida',
      'type' => 'id',
    ),
    'ii_invento5fdeagement_idb' => 
    array (
      'name' => 'ii_invento5fdeagement_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_ii_inventory_item_im_inventory_management_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_ii_inventory_item_im_inventory_management_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ii_inventory_item_im_inventory_management_1ii_inventory_item_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_ii_inventory_item_im_inventory_management_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ii_invento5fdeagement_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'ii_inventory_item_im_inventory_management_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'ii_inventory_item_im_inventory_management_1ii_inventory_item_ida',
        1 => 'ii_invento5fdeagement_idb',
      ),
    ),
  ),
);