<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$yes = 1;
$no = 0;
$result = $db->query("select id_c, anml_animals_cstm.deceased_c from anml_animals_cstm inner join anml_animals on anml_animals.id = anml_animals_cstm.id_c where anml_animals.deleted = 0");
if ($result) {
    while ($row = $db->fetchByAssoc($result)) {
        $id = $row['id_c'];
        $deceased = $row['deceased_c'];
        
        if ($deceased) {
            if($deceased == "Yes"){
                $query = "update anml_animals_cstm set deceased_checkbox_c = '".$yes."' where id_c = '".$id."'";
            }else if($deceased == "No"){
                $query = "update anml_animals_cstm set deceased_checkbox_c = '".$no."' where id_c = '".$id."'";
            }
            
            if(!$db->query($query)) {
                    echo "Fail to Test System " . $id . "<br>";
            }
        }
    }
    echo "Script executed successfully";
} else {
    echo "Table not found";
}

