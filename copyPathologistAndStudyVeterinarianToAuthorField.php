<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$result = $db->query("SELECT m03_work_product_deliverable_cstm.id_c, m03_work_product_deliverable_cstm.study_veterinarian_c, m03_work_product_deliverable_cstm.pathologist_c FROM m03_work_product_deliverable_cstm INNER JOIN m03_work_product_deliverable ON m03_work_product_deliverable.id = m03_work_product_deliverable_cstm.id_c WHERE m03_work_product_deliverable.deleted = 0");
if ($result) {
    while ($row = $db->fetchByAssoc($result)) {
        $id = $row['id_c'];
        $study_veterinarian_key = $row['study_veterinarian_c'];
        $pathologist_c_key = $row['pathologist_c'];

        $study_veterinarian_value = $GLOBALS['app_list_strings']['veterinarian_list'][$study_veterinarian_key];
        $pathologist_c_value = $GLOBALS['app_list_strings']['study_pathologist_list'][$pathologist_c_key];

        $temp = 1;
        if (!empty($study_veterinarian_key) || $study_veterinarian_key != NULL || !empty($pathologist_c_key) || $pathologist_c_key != NULL) {
            $query = "select id ,first_name , last_name from contacts where concat(first_name,' ',last_name) = '" . $pathologist_c_value . "' and deleted = 0";
            $result2 = $db->query($query);
            if ($result2) {
                if ($row2 = $db->fetchByAssoc($result2)) {

                    $query2 = "update m03_work_product_deliverable_cstm set contact_id_c = '" . $row2['id'] . "' where id_c = '" . $id . "'";
                    if (!$db->query($query2)) {
                         echo "Fail to populate the contact '" . $pathologist_c_value . "' for Work Product Deliverable Id : '" . $id . "' <br>";
                    }
                }
            } else {
                $temp = 2;
                $query = "select id from contacts where concat(first_name,' ',last_name) = '" . $study_veterinarian_value . "' and deleted = 0";
                $result2 = $db->query($query);
                if ($row2 = $db->fetchByAssoc($result2)) {
                    $query2 = "update m03_work_product_deliverable_cstm set contact_id_c = '" . $row2['id'] . "' where id_c = '" . $id . "'";
                    if (!$db->query($query2)) {
                         echo "Fail to populate the contact '" . $study_veterinarian_value . "' for Work Product Deliverable Id : '" . $id . "' <br>";
                    }
                }
            }
        }
    }
    echo "Script executed successfully";
} else {
    echo "Table not found";
}

