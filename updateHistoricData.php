<?php
require_once 'custom/modules/M06_Error/create_record.php';
// Migrating Data for Error QC Employees Subpanel
$sql_run = "SELECT m_error.id AS MO6_ID, 
                m_emp_error.erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida AS QC_Emp_ID, 
                error_emp.id AS Err_Emp_ID, error_emp.name AS NAME
            FROM m06_error AS m_error

            INNER JOIN erqc_error_qc_employees_m06_error_1_c AS m_emp_error
                ON m_emp_error.erqc_error_qc_employees_m06_error_1m06_error_idb = m_error.id
                    AND m_emp_error.deleted = 0

            INNER JOIN erqc_error_qc_employees AS emp_err
                    ON emp_err.id = m_emp_error.erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida
                AND emp_err.deleted = 0

            INNER JOIN ere_error_employees error_emp
                    ON lower(error_emp.name) = lower(emp_err.name) AND error_emp.deleted = 0

            WHERE m_error.deleted = 0;";

$result_error_qc = $GLOBALS['db']->query($sql_run);
createDeviationEmployesRecord($result_error_qc, 'Review');

// Migrating Data for APS Employees Subpanel
$sql_aps_emp = ' SELECT m06_error.id AS MO6_ID, ere_error.id AS Err_Emp_ID
            FROM m06_error AS m06_error
            
        INNER JOIN m06_error_ere_error_employees_1_c AS error_aps_emp
            ON error_aps_emp.m06_error_ere_error_employees_1m06_error_ida = m06_error.id
                AND error_aps_emp.deleted = 0
        
        INNER JOIN ere_error_employees AS ere_error
            ON ere_error.id = error_aps_emp.m06_error_ere_error_employees_1ere_error_employees_idb
                AND ere_error.deleted = 0
        
        WHERE m06_error.deleted = 0';

$result_aps_emp = $GLOBALS['db']->query($sql_aps_emp);
createDeviationEmployesRecord($result_aps_emp, 'Entry');

echo "Record Updated Successfully -- ";
/**
 * @description: For creating Deviation Employees record
 * using existing Logic Hook Class for following
 * naming convention for record creation
 * @param type $result
 */
function createDeviationEmployesRecord($result, $type)
{
    $helperObj = new create_record();

    while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
        $GLOBALS['log']->fatal('Data in Row :: ',$row);
        $beanDeviation = 
            BeanFactory::getBean('M06_Error', $row['MO6_ID'], array('disable_row_level_security' => true));
        if ($beanDeviation->id) {
            $beanApsEmp = 
                BeanFactory::getBean('DE_Deviation_Employees', null, array('disable_row_level_security' => true));
            $beanApsEmp->name = $helperObj->nameCreation( $beanDeviation, array('isUpdate' => false), $row['Err_Emp_ID'] );
            $beanApsEmp->ere_error_ee6cployees_ida = $row['Err_Emp_ID'];
            $beanApsEmp->deviation_type_c = $type;
            $beanApsEmp->processed = true;
            $beanApsEmp->save();
            $beanDeviation->load_relationship( 'm06_error_de_deviation_employees_1' );
            $beanDeviation->m06_error_de_deviation_employees_1->add( $beanApsEmp->id );
        }
    }
}
