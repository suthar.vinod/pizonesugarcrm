({
    className: 'blockedit tcenter',
    bLoaded: true,
    field: "",
    strRecordID: "",
    events: {
        'click .goto-record' : 'onGotoOpportunity',
        // 'click .manage-quote' : 'onManageQuote',        
        'click .save-field' : 'onSave',
        // 'click .add-line' : 'addQuoteLine',
        // 'click .remove-line' : 'removeQuoteLine',
        'focus textarea.flexi': 'onTextAreaFocus',
        'focusout textarea.flexi': 'onTextAreaFocusOut',
        'click .text-content' : 'onDivContentClicked',
        'click .image-content' : 'onDivContentClicked',
        'change .image_field' : 'onChangeImage',
        'change .flexi-image-url' : 'onChangeImageURL',
        // 'click .flexi-edit-style' : 'onChangeStyle',
        // 'click .flexi-apply-style' : 'onApplyStyle',
        // 'mouseover .text-content': 'showEditIcon',
        // 'mouseout .text-content': 'hideEditIcon',
    },
    showEditIcon: function(objEvent) {
        // stop event propagate
        objEvent.stopPropagation();
        console.log('mouse over text content');
        console.log(objEvent.target);
        // get the targe object
        var objTarget = $(objEvent.target);   
        // get the edit icon element
        var objEditIcon = objTarget.find('span.flexi-edit-style');    
        // if the target object is a span
        if ( !objTarget.hasClass('flexi-edit-style') ) {         
            if (!$(objEditIcon).is(':visible')) {
                // display the icon
                objEditIcon.show();
            }
        }
    },
    hideEditIcon: function(objEvent) {
        // stop event propagate
        objEvent.stopPropagation();
        // get the targe object
        var objTarget = $(objEvent.target); 
        console.log(objTarget.hasClass('flexi-edit-style'));
        // if the target object is a span
        if ( !objTarget.hasClass('flexi-edit-style') ) {
            // hide the icon
            objTarget.find('span.flexi-edit-style').hide();
        }
    },
    onGotoOpportunity: function() {
        // Log the event
        app.router.redirect("#Opportunities/" + this.strRecordID);
    },
    initialize: function(options) {
        // Call the parent handler
        this._super('initialize', [options]);
    },
    loadData: function() {
        // assign scope
        var self = this;
        // assign ID
        this.strRecordID = this.model.get('id');        
        // Get the field name
        var arPath = window.location.hash.split('/');
        // The last element in the path, should correspond to the field name
        self.field = arPath[arPath.length - 1];
        // get related records
        var strUrl = app.api.buildURL('blockedit/retrieve');
        // Set up the data
        var objData = {
            "OAuth-Token": app.api.getOAuthToken(),            
            "module": self.module,
            "record": self.model.get("id"),
            "field": self.field
        };
        // initialize ajax
        $.ajax({
            url: strUrl,
            type: "GET",
            dataType: "json",
            data: objData,
            headers: {"OAuth-Token": app.api.getOAuthToken()},
            success: function(objResponse) {
                // if we have a response
                if (objResponse) {

                    console.log(objResponse);

                    // flag to true
                    self.bLoaded = true;
                    // render
                    self.render();
                }
            }
        });    
    },
    _render: function() {   
        // assign scope
        var self = this;  
        // call parent handler
        this._super("_render");      
        $('.content-list').sortable({
            tolerance: 'pointer', 
            remove: function(event, ui) {
                var objItem = ui.item.clone();
                objItem.draggable();
                objItem.appendTo('.content-list');
            } 
        });
        // Set up the drag/drop list
        $('.element-list').sortable({
            connectWith: ".content-list",
            tolerance: 'pointer',
            containment: '#flexi-sortable',
            remove: function(event, ui) {
                // Get the position where this has been dropped
                var intIndex = $('.content-list').children().index($(ui.item[0]));
                // Clone the original element
                var objCloneDiv = ui.item.clone();
                // Get the field type
                var strFieldType = objCloneDiv.data("fieldtype");
                // does the class has flexi-image class
                if(strFieldType == "image"){
                    // Create an image type
                    // var strInput = "<label for='flexi-image-url'>Image URL<label><input type='text' class='span12 flexi flexi-image-url'> </input>"
                    var strInput = '<div class="flexi-image-component"><form action="" class="dropzone span12 flexi flexi-image-upload"></form></div>';
                    var strFieldClass = 'image-content';
                } else {
                    // Transform dragged element to an input
                    strInput = "<textarea class='span12 flexi' >" + strFieldType + "</textarea>";
                    var strFieldClass = 'text-content';
                }                
                // Add a class for the content div
                objCloneDiv.addClass( strFieldClass + " flexi-" + strFieldType);
                // Set the content of the target div
                objCloneDiv.html(strInput);                
                // does the class has flexi-image class
                if(strFieldType == "image"){
                    // initialise dropzone on the element
                    self._initialiseDropZone(objCloneDiv);    
                    // append to the content list
                    $('.content-list').append(objCloneDiv);
                } else {
                    // append to the content list
                    $('.content-list').append(objCloneDiv);
                    // set focus at the end of text
                    $('.content-list div:last-child').find('textarea').addClass('flexi-highlight').focus().val('').val(strFieldType);
                }
                // Cancel the move - it's a copy
                $(this).sortable('cancel');
            },            
        });
        // Make droppable
        $(".trash-location").droppable({
            accept: function(objEvent) {                
                return true;
            },
            hoverClass: "trash-hover",
            drop: function( event, ui ) {                
                // if not included on the element list
                if (!$(ui.draggable.context).parent().hasClass('element-list')) {
                    // remove dropped element
                    ui.draggable[0].remove();
                }
            }
        });
    },
    _initialiseDropZone: function(objCloneDiv) {
        // Generate the REST URL for image upload
        var strUrl = app.api.buildURL('blockedit/upload');     
        console.log(objCloneDiv);
        // initialise form with class flexi-image as dropzone
        $(objCloneDiv).find('form').dropzone({ 
            url: strUrl,
            paramName: 'file',
            maxFilesize: 2,
            maxFiles: 1,
            acceptedFiles: 'image/*',
            method: 'post',
            previewTemplate: $('#preview-template').html(),
            headers: {
                "OAuth-Token": app.api.getOAuthToken()
            },
            accept: function(file, done) {
                console.log(file);
                done();                 
            },
            success: function(objEvent, objResponse) {    

                console.log('Successfully uploaded file');
                // find all flexi-highlights and remove class
                $('.flexi-highlight').removeClass('flexi-highlight');
                // remove message
                $(objEvent.previewElement.offsetParent).find('.dz-message').remove();                
                // console.log($(objEvent.previewElement.offsetParent.find('.dz-message').remove()));
                // get the image container
                var objFlexiImageContainer = $(objEvent.previewElement.offsetParent);
                // get the image
                var objImage = $(objEvent.previewElement).find('img.resize-image');
                // is there an original size?
                if (!$(objImage).attr('original-img')) {
                    // set attribute
                    $(objImage).attr('original-img',$(objImage).attr('src') );
                } 
                // add highlight
                $(objEvent.previewElement.offsetParent).parent().addClass('flexi-highlight');
                // remove dropzone tags and added image only
                $(objEvent.previewElement.offsetParent).empty().html(objImage);                

                // add edit icon
                // $(objEvent.previewElement.offsetParent).prepend('<span class="fa fa-edit flexi-edit-style"></span>');

                // on success
                window.resizeableImage($('.resize-image'));

                $('.dz-message').remove();

            }
        });
    },
    onTextAreaFocus: function(objEvent) {          
        // Get the target object
        var objTarget = $(objEvent.target);
        // Increase the size
        objTarget.css("height", "10em");
    },
    onTextAreaFocusOut: function(objEvent) {        
        console.log('textarea focusout')
        // Get the target object
        var objTarget = $(objEvent.target);    
        // get value of text area
        var strValue = objTarget.val();            
        console.log(objTarget.parent());    
        // Get the parent dei
        objTarget.parent().removeClass('flexi-highlight').html(strValue);
        // objTarget.parent().css('background-color','');
    },  
    _removeHighlightsBlocks: function(objEvent) {
        // loop through and find highlighted blocks
        $('.content-list .flexi-highlight').each(function(idx, objElement){
            // cache the element to jquery
            var objElement = $(objElement);
            // remove the highlight
            objElement.removeClass('flexi-highlight');
            // if image content

            if (objElement.hasClass('image-content')) {
                // get image element and remove resize image class
                var objImage = objElement.find('img').removeClass('resize-image');
                // replace the html with image only
                objElement.html(objImage);
            }
        });

    },
    onDivContentClicked: function(objEvent){        
        console.log('div content clicked');
        // Get the target object
        var objTarget = $(objEvent.target);
        // remove other focused blocks
        this._removeHighlightsBlocks();
        // Is this a text content?
        if (objTarget.hasClass("text-content")) {
            // Get the field type
            var strFieldType = objTarget.data("fieldtype");            
            // if the content of div does not have textarea
            if ($(objTarget.context.firstChild).prop('nodeName') != 'TEXTAREA') {
                // get value of text area
                var strValue = objTarget.text();            
                // set element to replace textarea
                objTarget.html("<textarea class='flexi' style='height: 10em;'>" + strValue + "</textarea>").addClass('flexi-highlight');
                // set textarea focus at the end of text
                objTarget.find('textarea.flexi').focus().val('').val(strValue).addClass('flexi-highlight');
            }
        } else if (objTarget.hasClass("image-content") || objTarget.is('img')) {
            // trigger textarea focusout
            $('textarea').trigger('focusout');
            // if target is image, set target to parent
            objTarget = ( objTarget.is('img') ) ? objTarget.parent() : objTarget;
            // add highlight
            objTarget.addClass('flexi-highlight');
            // get the image
            objImage = objTarget.find('img');
            // check if we have original size
            // if ($(objImage).attr('original-img')) {
            //     // change the src to original
            //         $(objImage).attr('src',$(objImage).attr('original-img') );
            // }
            // if this an image content            
            window.resizeableImage(objImage);
        }
    },
    onChangeImage: function(objEvent){
        console.log("change image");
        console.log(objEvent);
    },
    onChangeImageURL: function(objEvent){
        console.log("change image url");
        // Get the target object
        var objTarget = $(objEvent.target);
        console.log("objEvent");
        console.log(objEvent);
        console.log("objTarget");
        console.log(objTarget);
        // get url value
        // var strValue = 
    },
    onChangeStyle: function(objEvent) {       

        console.log('CHANGE STYLE EDIT'); 
        // find all flexi-highlights
        $('.flexi-highlight').removeClass('flexi-highlight');
        // get the target object
        var objTarget = $(objEvent.target);
        // get parent element and hightlight
        var objParent = objTarget.parent();
        // highlight and add class flexi-highlight
        objParent.addClass('flexi-highlight');
    },
    onApplyStyle: function(objEvent) {        
        // get the target object
        var objTarget = $(objEvent.target);
        // get the style and value
        var strStyle = objTarget.data('style');
        var strStyleValue = objTarget.data('style-value');
        // get all highlighted element in the content list
        $('.flexi-highlight').each(function(idx, objElement){            
            // apply the style and remove class
            $(objElement).css(strStyle, strStyleValue).removeClass('flexi-highlight');
            $(objElement).css('background-color', '');
        });        
    },
    onSave: function(objEvent) {
        // Store access to self
        var self = this;
        // Set the target object
        var objTarget = $(objEvent.target);
        // Initialize the list
        var arBlocks = [];
        // Loop through the content items and populate them
        $(".content-list").find("div").each(function() {
            // Append
            arBlocks.push({
                "type": $(this).data("fieldtype"),
                "value": $(this).html()
            });
        });
        console.log("Content:");
        console.log(arBlocks);
        // Validate data
        bDataValid = true;
        // Do we have any errors
        if (!bDataValid) {
            // Indicate the error
            $(".field_error").find("td").each(function() {
                // Get current background
                var strBackground = $(this).css("background-color");
                // Set the colour
                $(this).css("background-color", "#ff0000").animate({backgroundColor: strBackground});
            });
            // Indicate the success
            app.alert.show('Saving', {
                level: 'error',
                messages: 'Please fix milestone errors before proceeding',
                autoClose: true
            });
        } else {
            // Change the icon
            var objIcon = objTarget.find("i:first");
            // Store existing classes
            var strClasses = objIcon.attr("class");
            // Update icon
            objIcon.attr("class", "fa fa-spin fa-spinner");
            // Generate the REST URL to get related records
            var strUrl = app.api.buildURL('intellidocs/saveblockdata');
            // Set up the data
            var objData = {
                "OAuth-Token": app.api.getOAuthToken(),
                "module": self.module,
                "record": self.model.get("id"),
                "blockdata": arBlocks
            };
            // Make the call
            $.ajax({
                url: strUrl,
                type: "POST",
                dataType: "json",
                data: objData,
                success: function(objResponse) {
                    // Update icon
                    objIcon.attr("class", strClasses);
                    // Indicate the success
                    app.alert.show('Saving', {
                        level: 'success',
                        messages: 'Successfully updated data',
                        autoClose: true
                    });
                    // Refresh
                    app.router.refresh();
                },
                error: function(objError) {
                    // Update icon
                    objIcon.attr("class", strClasses);
                    // Indicate the success
                    app.alert.show('Saving', {
                        level: 'error',
                        messages: 'Error saving milestone data',
                        autoClose: true
                    });
                }
            });
        }
    }
})
