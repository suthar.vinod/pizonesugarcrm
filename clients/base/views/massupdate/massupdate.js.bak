/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Views.Base.MassupdateView
 * @alias SUGAR.App.view.views.BaseMassupdateView
 * @extends View.View
 */
({
    extendsFrom: 'MassupdateView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.setRFCMetadata(options);
    },
    _render: function () {
        var result = this._super('_render');
        if (!_.isUndefined(this.options.meta.panels) && !_.isUndefined(this.options.meta.panels[0]) &&
            !_.isUndefined(this.options.meta.panels[0].fields) && this.options.meta.panels[0].fields.length > 0
            && this.options.meta.panels[0].fields[this.options.meta.panels[0].fields.length - 1].type === 'Reasonforchange'
        ) {
            if (this.$(".select2.mu_attribute")) {
                this.$(".select2.mu_attribute").each(function (index) {
                    if ($(this).is('select') && index === 1) {
                        $(this).attr('disabled', 'disabled');
                    }
                });
            }
            if (this.$(".btn.btn-invisible.btn-dark")) {
                this.$(".btn.btn-invisible.btn-dark").each(function () {
                    if ($(this).data('index') === 0 && $(this).data('action') === 'remove') {
                        $(this).attr('disabled', 'disabled');
                    }

                });
            }
        }
        return result;
    },
    setRFCMetadata: function (options) {
        var fieldList,
            massFields = [],
            metadataModule = app.metadata.getModule(options.module);
        if (!metadataModule) {
            app.logger.error('Failed to get module ' + options.module);
            return;
        }
        fieldList = metadataModule.fields;
        _.each(fieldList, function (field) {
            if (field.type === 'Reasonforchange') {
                //we clone the metadata definition
                //to make sure we don't change the original metadata
                var cloneField = app.utils.deepCopy(field);
                cloneField.label = cloneField.label || cloneField.vname;
                massFields.push(cloneField);
                if (!_.isUndefined(options.meta.panels)) {
                    options.meta.panels[0].fields.push(cloneField);
                }
            }
        });
    },
    setDefault: function () {
        if ((!_.isUndefined(this.options.meta.panels) && !_.isUndefined(this.options.meta.panels[0]) &&
                !_.isUndefined(this.options.meta.panels[0].fields) && this.options.meta.panels[0].fields.length > 0
                && this.options.meta.panels[0].fields[this.options.meta.panels[0].fields.length - 1].type === 'Reasonforchange'
            )) {
            var assignedValues = _.pluck(this.fieldValues, 'name');
            if (this.defaultOption) {
                assignedValues = assignedValues.concat(this.defaultOption.name);
            }
            //remove the attribute options that has been already assigned
            this.fieldOptions = (this.meta) ? _.reject(_.flatten(_.pluck(this.meta.panels, 'fields')), function (field) {
                return (field) ? _.contains(assignedValues, field.name) : false;
            }) : [];
            //set first item as default

            var res = this.fieldOptions.filter(function (field) {
                return field.type == "Reasonforchange"
            });
            if (Array.isArray(res) && res.length > 0) {
                this.defaultOption = this.fieldOptions.splice(this.fieldOptions.length - 1, 1)[0];
            }
            else {
                this.defaultOption = this.defaultOption || this.fieldOptions.splice(0, 1)[0];
            }

        }
        else {
            this._super('setDefault');
        }
    },
})
