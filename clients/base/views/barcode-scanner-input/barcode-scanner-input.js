({
    typedText: "",

    initialize: function(){
        this._super("initialize", arguments);
        this.collection.on("data:sync:complete", _.bind(this.collectionChanged, this));
    },
 
    render: function(){ 
        this._super("render", arguments); 
        this.registerScannerListener();
    },
    
    /**
     * Detects if the current input comes from a scanner or not
     * 
     */
    detectScanner: _.debounce(function(evt){   
        var term = this.typedText;
        if(term.length > 5){
            //scanned text was found
            this.scan(term);  
        }
        this.typedText = "";
    }, 100),
 
    /**
     * If the search term comes from a scanner, 
     * create a filter and set it on the collection. 
     * After this apply the filter
     * @param {String} term -the search input 
     */
    scan: function(term){
        var filter_def = null; 
        
        if(term){  
            this.$el.find("#barcode_text").val("Scanning for "+term);
            filter_def = [{
                unique_id_c: {$equals: term}
            }];
 
            this.collection.barcode_scanned = true; 
        } 
        
        var filterpanel = this.layout.getComponent("filterpanel");
        filterpanel.trigger('filter:apply', "", filter_def);
    },

    /**
     * When the collection is changed 
     * Add the found item to the current colection
     * Show messages relate dto the current searched item
     */
    collectionChanged: function(){
        if(this.collection.barcode_scanned === true){
            this.collection.barcode_scanned = false;
            if(this.collection.length === 1){
                this.$el.find("#barcode_text").val("");
                if(this.collection.models[0].get("inactive_c") === false){
                    this.layout.context.get("mass_collection").add(this.collection.models[0]);       
                } else {
                    app.alert.show('error_link', {
                        messages: 'This Item cannot be linked!',
                        level: "error"
                    });
                }
                this.collection.origFilterDef = null;
                this.scan(null);         
            } else if(this.collection.length > 1) {
                this.$el.find("#barcode_text").val("Multiple items found... Please Select which ones you want added!");
            } else if(this.collection.length == 0) {
                this.$el.find("#barcode_text").val("No items found");
                this.collection.origFilterDef = null;
                this.scan(null);
            }            
        } 
    },

    /**
     * register a listener for the scanner
     */
    registerScannerListener: function(){
        document.addEventListener("keyup", _.bind(function(evt){
            //it is a single letter (not Shift or other special keys)
            if(evt.key && evt.key.length === 1){
                this.typedText += evt.key;
            }

            this.detectScanner();
        }, this), false);
    }
});   