<?php

class GetObservationNameApi extends SugarApi {

    public function registerApiRest() {
        return array(
            //GET
            'MyGetObservationName' => array(
                
                //request type
                'reqType' => 'GET',
                
                //set authentication
                'noLoginRequired' => false,
                
                //endpoint path
                'path' => array('ObservationName'),
                
                //endpoint variables
                'pathVars' => array('', 'id'),
                
                //method to call
                'method' => 'GetObservationNameMethod',
                
                //short help string to be displayed in the help documentation
                'shortHelp' => 'An example of a GET endpoint',
                
                //long help to be displayed in the help documentation
                'longHelp' => '',
            ),
        );
    }

    /**
     * Method to be used for my ObservationName endpoint
     */
    public function GetObservationNameMethod($api, $args) {
        
        $strQuery = "SELECT name FROM m06_error WHERE id = '" . $args['id'] . "'";

        // Execute
        $results = $GLOBALS['db']->query($strQuery);

        // Loop through
        if ($row = $GLOBALS['db']->fetchByAssoc($results)) {
            return $row['name'];
        }
        return null;
    }

}
