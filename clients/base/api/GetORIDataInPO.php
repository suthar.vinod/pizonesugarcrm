<?php
if (!defined('sugarEntry') || !sugarEntry) {
  die('Not A Valid Entry Point');
}

class GetORIDataInPO extends SugarApi
{
  public function registerApiRest()
  {
    return array(
      "checkCompliance1" => array(
        "reqType" => "GET",
        "noLoginRequired" => true,
        'path' => array('?', '?', '?', '?', '?', '?', 'get_ori_data'),
        'pathVars' => array('module', 'vendorName', 'vendorId', 'offset', 'column', 'order'),
        'method' => 'get_ori_data',

        "shortHelp" => "Get Ori Data in PO based on Vendor",
        "longHelp" => "",
      ),
      "checkCompliance2" => array(
        "reqType" => "GET",
        "noLoginRequired" => true,
        'path' => array('?', '?', '?', '?', 'get_ori_data'),
        'pathVars' => array('module', 'vendorName', 'vendorId', 'ORIName'),
        'method' => 'get_ori_data',

        "shortHelp" => "Get Ori Data in PO based on Vendor In filter",
        "longHelp" => "",
      ),
    );
  }


  public function get_ori_data($api, $args)
  {

    $module = $args['module'];
    $vendorName = $args['vendorName'];
    $vendorId = $args['vendorId'];

    // $filterByORIName="";
    // if(isset($args['ORIName']) && $args['ORIName']!=""){
    //   $filterByORIName=" AND ori_order_request_item.name like '".$args['ORIName']."%'";
    // }

    if (strlen($args['offset']) == 0)
      $args['offset'] = "0";

    $sqlAppendFilter = " order by " . $args['column'] . " " . $args['order'] . " limit 20 offset " . $args['offset'];


    global $db;

    $sqlGetORI = "SELECT
    IFNULL(ori_order_request_item.id, '') primaryid,
    IFNULL(ori_order_request_item.name, '') name,
    IFNULL(ori_order_request_item_cstm.department_new_c, '') department_new_c,
    IFNULL(l1.id, '') l1_id,
    IFNULL(l1.name, '') prod_product_ori_order_request_item_1_name,
    IFNULL(l1.product_id_2, '') prod_product_ori_order_request_item_1_product_id_2,
    IFNULL(ori_order_request_item.product_name, '') product_name,
    IFNULL(ori_order_request_item.product_id_2, '') product_id_2,
    ori_order_request_item_cstm.user_id_c,
    concat(users.first_name,' ',users.last_name) AS requestedBy
    FROM ori_order_request_item 
    LEFT JOIN prod_product_ori_order_request_item_1_c l1_1 
    ON ori_order_request_item.id = l1_1.prod_product_ori_order_request_item_1ori_order_request_item_idb AND l1_1.deleted = 0 
    LEFT JOIN prod_product l1 
    ON l1.id = l1_1.prod_product_ori_order_request_item_1prod_product_ida AND l1.deleted = 0 

    LEFT JOIN  or_order_request_ori_order_request_item_1_c l2_1 ON ori_order_request_item.id=l2_1.or_order_r2609st_item_idb AND l2_1.deleted=0
    LEFT JOIN  or_order_request l2 ON l2.id=l2_1.or_order_request_ori_order_request_item_1or_order_request_ida AND l2.deleted=0

    LEFT JOIN ori_order_request_item_cstm ori_order_request_item_cstm 
    ON ori_order_request_item.id = ori_order_request_item_cstm.id_c 
    LEFT JOIN accounts accounts1 
    ON accounts1.id = l1.account_id1_c AND IFNULL(accounts1.deleted, 0) = 0 

    LEFT JOIN or_order_request_cstm l2_cstm ON l2.id = l2_cstm.id_c
    
    LEFT JOIN users ON users.id = ori_order_request_item_cstm.user_id_c
    WHERE (((((( l1.account_id1_c = '$vendorId') ) )) 
    AND (((ori_order_request_item.status = 'Requested' ))) AND (((l2_cstm.status_c IN ('Complete','Partially Submitted')
    )))))
    AND ori_order_request_item.deleted = 0 $sqlAppendFilter";


    $data = array();
    $results = $db->query($sqlGetORI);

    while ($row = $db->fetchByAssoc($results)) {
      if ($row['product_id_2'] != "") {
        $productId = $row['product_id_2'];
      } else {
        $productId = $row['prod_product_ori_order_request_item_1_product_id_2'];
      }
      array_push($data, array(

        "id" => $row['primaryid'],
        "name" => $row['name'],
        "department_new_c" => $row['department_new_c'],
        "prod_product_ori_order_request_item_1_name" => $row['prod_product_ori_order_request_item_1_name'],
        "product_name" => $row['product_name'],
        "product_id_2" => $productId,
        "user_id_c" => $row['user_id_c'],
        "or_request_by_c" => $row['requestedBy'],
      ));
    }
    return array("records" => $data);
  }
}
