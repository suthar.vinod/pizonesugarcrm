<?php  
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');  
require_once("clients/base/api/CurrentUserApi.php");  
class CustomCurrentUserApi extends CurrentUserApi  
{  
   public function registerApiRest()  
   {  
       return parent::registerApiRest();  
   }  
    public function retrieveCurrentUser($api, $args)  
   {  
       $result = parent::retrieveCurrentUser($api, $args);  
              $result['current_user']['acl']['M06_Error']['create'] = 'no';
              $result['current_user']['acl']['SC_Species_Census']['create'] = 'no'; 
              $result['current_user']['acl']['IC_Inventory_Collection']['create'] = 'no';
              $result['current_user']['acl']['ANML_Animals']['create'] = 'no';     
       return $result;  
   }  
}