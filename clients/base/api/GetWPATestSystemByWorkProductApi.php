<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class GetWPATestSystemByWorkProductApi extends SugarApi {

    public function registerApiRest() {
        return array(
            "checkCompliance21" => array(
                "reqType" => "GET",
                "noLoginRequired" => true,
                'path' => array('?', '?', 'getWPATestSystemByWorkProduct'),
                'pathVars' => array('module', 'wpid'),
                'method' => 'get_Ts_data',
                "shortHelp" => "Getting Test System data based on character match",
                "longHelp" => "",
            ),
        );
    }

    public function get_Ts_data($api, $args) {
        global $db;
        $data = array();
        //Current Work Product Id
        $WP_ID = $args['wpid']; 
        $bean = BeanFactory::retrieveBean('M03_Work_Product', $WP_ID, array('disable_row_level_security' => true));
        
        $bean->load_relationship('m03_work_product_wpe_work_product_enrollment_1');
        $WPAIds = $bean->m03_work_product_wpe_work_product_enrollment_1->get();
 
        $wpeIds = implode("','", $WPAIds);
        
        $sqlTestSys = "SELECT TS.name,TSCSTM.usda_id_c,TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida as testId 
                        FROM anml_animals_wpe_work_product_enrollment_1_c AS TSWPA
                        LEFT JOIN anml_animals AS TS ON TS.id=TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida
                        LEFT JOIN anml_animals_cstm AS TSCSTM ON TS.id=TSCSTM.id_c
                      WHERE anml_anima9941ollment_idb IN ('" . $wpeIds . "') AND TS.deleted=0 group by testId";

        $resultTestSystem = $db->query($sqlTestSys);
        while ($rowTS = $db->fetchByAssoc($resultTestSystem)) {
            array_push($data, array(
                "testSystemId" => $rowTS['testId'],
                "testSystemName" => $rowTS['name'],
                "usdaId" => $rowTS['usda_id_c'],
            ));
        }
        return $data;
    }

}
?>
