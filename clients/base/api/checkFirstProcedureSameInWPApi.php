<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class checkFirstProcedureSameInWPApi extends SugarApi
{

    public function registerApiRest()
    {
        return array(
            // 'checkFirstProcedureSameInWP' => array(
            //     'reqType'                 => 'POST',
            //     'noLoginRequired'         => false,
            //     'path'                    => array('checkFirstProcedureSameInWP'),
            //     'pathVars'                => array('checkFirstProcedureSameInWP'),
            //     'method'                  => 'checkFirstProcedureSameInWP',
            //     'shortHelp'               => '',
            //     'longHelp'                => '',
            // ),
            'insertSheduledDateToFirstProcedureInWPD' => array(
                'reqType'                 => 'POST',
                'noLoginRequired'         => false,
                'path'                    => array('insertSheduledDateToFirstProcedureInWPD'),
                'pathVars'                => array('insertSheduledDateToFirstProcedureInWPD'),
                'method'                  => 'insertSheduledDateToFirstProcedureInWPD',
                'shortHelp'               => '',
                'longHelp'                => '',
            ),
        );
        
    }

    /*
    // Rollback this feature from @2268 ticket
    function checkFirstProcedureSameInWP($api, $args)
    {
        global $db;
        $module = $args['module'];
        $WPIds  = $args['WPIds'];
        $data   = array();
        foreach ($WPIds as $Ids) {
            $sql = "SELECT 
                        m03_work_product_cstm.first_procedure_c,
                        m03_work_product_cstm.id_c
                    FROM m03_work_product_cstm
                    WHERE  m03_work_product_cstm.id_c = '".$Ids."'";
            $results = $db->query($sql);
            while ($row = $db->fetchByAssoc($results)) {
                array_push($data, $row['first_procedure_c']);
            }
        }
        if(count(array_unique($data)) === 1){
            return array("Data" => '1'); // 1 indicates all dates are same in array
        }else{
            return array("Data" => count(array_unique($data)));
        }
    }
    */
    //Added By: Harshit Shreshthi for ticket @2377
    function insertSheduledDateToFirstProcedureInWPD($api, $args)
    {   
        $module        = $args['module'];
        $WPIds         = $args['WPIds'];
        $scheduleDate  = $args['scheduleDate'];
        // $GLOBALS['log']->fatal("From Schedule date===>". $module);
        // $GLOBALS['log']->fatal("From Schedule date WPIds===>". print_r($WPIds,1));
        // $GLOBALS['log']->fatal("From Schedule date WPIds===>". $scheduleDate);
        foreach($WPIds as $Ids){
            $WPBean  = Beanfactory::getBean($module,$Ids);
            $batchId = $WPBean->bid_batch_id_m03_work_product_1bid_batch_id_ida;
            if(!empty($batchId)){
                $WPBean->first_procedure_c = $scheduleDate;
                $WPBean->save();
            }
        }
        return 1;
    }
}
