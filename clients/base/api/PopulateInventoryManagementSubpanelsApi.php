<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once 'clients/base/api/RelateApi.php';

class PopulateInventoryManagementSubpanelsApi extends RelateApi
{
    const INVENTORY_MANAGEMENT_RELATIONSHIP = "m03_work_product_im_inventory_management_1";

    public function registerApiRest()
    {
        return array(
            'listRelatedRecords'      => array(
                'reqType'    => 'GET',
                'path'       => array('?', '?', 'link', 'm03_work_product_im_inventory_management_1'),
                'pathVars'   => array('module', 'record', '', 'link_name'),
                'jsonParams' => array('filter'),
                'method'     => 'filterRelated',
                'shortHelp'  => 'Lists related records.',
                'longHelp'   => 'include/api/help/module_record_link_link_name_filter_get_help.html',
            ),
            'listRelatedRecordsCount' => array(
                'reqType'    => 'GET',
                'path'       => array('?', '?', 'link', 'm03_work_product_im_inventory_management_1', 'count'),
                'pathVars'   => array('module', 'record', '', 'link_name', ''),
                'jsonParams' => array('filter'),
                'method'     => 'filterRelatedCount',
                'shortHelp'  => 'Counts all filtered related records.',
                'longHelp'   => 'include/api/help/module_record_link_link_name_filter_get_help.html',
            ),
        );
    }

    public function filterRelated(ServiceBase $api, array $args)
    {
        $inventoryManagement            = parent::filterRelated($api, $args);
        $args["link_name"]              = self::INVENTORY_MANAGEMENT_RELATIONSHIP;
        $inventoryManagementOTM         = parent::filterRelated($api, $args);
        $inventoryManagement["records"] = array_merge($inventoryManagement["records"], $inventoryManagementOTM["records"]);

        return $inventoryManagement;
    }
}
