<?php

class checkMissingRelationApi extends SugarApi
{
	public function registerApiRest()
	{
		return array(
			//GET
			'checkMissingRelationship' => array(

				//request type
				'reqType' => 'GET',

				//set authentication
				'noLoginRequired' => false,

				//endpoint path
				'path' => array('checkRelationshipMethod'),

				//endpoint variables
				'pathVars' => array('checkRelationshipMethod'),

				//method to call
				'method' => 'checkRelationshipMethod',

				//short help string to be displayed in the help documentation
				'shortHelp' => 'An example of a GET endpoint',

				//long help to be displayed in the help documentation
				'longHelp' => '',
			),
		);
	}

	/**
	 * Method to be used for my ObservationName endpoint
	 */
	public function checkRelationshipMethod($api, $args)
	{
		$commID = $args['id'];		
		global $db;
		$commBean   	= BeanFactory::getBean('M06_Error', $commID);
		$commName 		= $commBean->name;
		$commrelated 	= $commBean->related_text_c;
		$workProduct	= $commBean->work_products;		
		
		if ($commName != "" && ($commrelated != "" || $workProduct!="") ) {
			$related_dataArr		= array();

			$res = str_ireplace(array('\"', '"', '}', '{', '[', ']', '\\', '""'), '', $commrelated);

			$dataArr = explode(",", $res);
			foreach ($dataArr as $resid) {
				$resid = preg_replace('/[^a-zA-Z0-9_ -:]/s', '', $resid);
				$residd = explode(":", $resid);
				$related_dataArr[str_replace(array('quot', '&'), '', $residd[0])] =  str_replace(array('quot', '&'), '', $residd[1]);
			}

			if ($workProduct != "") {
				$wp = str_ireplace(array('\"', '[', ']', '\\', 'quot', '&;', '"'), '', $workProduct);
				$related_dataArr['M03_Work_Product'] = str_replace(array('quot', '&', '"'), '', $wp);
			}
			
			foreach ($related_dataArr as $key => $valueID) {
				$key = trim($key);
				if ($valueID != "") {
					if ($key == 'ANML_Animals') {
						$TSBean  = BeanFactory::getBean('ANML_Animals', $valueID);

						if ($TSBean->name != "") {
							$checkQuery = "SELECT * FROM m06_error_anml_animals_1_c where deleted=0 AND m06_error_anml_animals_1m06_error_ida='" . $commID . "' and  m06_error_anml_animals_1anml_animals_idb='" . $valueID . "'";
							$resultTestSystem = $db->query($checkQuery);
							if ($resultTestSystem->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_anml_animals_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
							//echo "<br>==>Animal Not available";
						}
					}

					if ($key == 'M03_Work_Product') {

						$WPBean = BeanFactory::getBean('M03_Work_Product', $valueID);
						if ($WPBean->name != "") {
							$checkQuery = "SELECT * FROM `m06_error_m03_work_product_1_c` where deleted=0 AND m06_error_m03_work_product_1m06_error_ida='" . $commID . "' and  m06_error_m03_work_product_1m03_work_product_idb='" . $valueID . "'";
							$resultTestSystem = $db->query($checkQuery);
							if ($resultTestSystem->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_m03_work_product_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
						}
					}

					if ($key == 'Erd_Error_Documents') {
						$ERDBean = BeanFactory::getBean('Erd_Error_Documents', $valueID);
						if ($ERDBean->name != "") {
							$checkQuery = "SELECT * FROM `m06_error_erd_error_documents_1_c` where deleted=0 AND  m06_error_erd_error_documents_1m06_error_ida='" . $commID . "' and  m06_error_erd_error_documents_1erd_error_documents_idb='" . $valueID . "'";
							$resultEDoc = $db->query($checkQuery);
							if ($resultEDoc->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_erd_error_documents_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
						}
					}

					if ($key == 'Equip_Equipment') {
						$EFBean = BeanFactory::getBean('Equip_Equipment', $valueID);
						if ($EFBean->name != "") {
							$checkQuery = "SELECT * FROM `m06_error_equip_equipment_1_c` where deleted=0 AND  m06_error_equip_equipment_1m06_error_ida='" . $commID . "' and  m06_error_equip_equipment_1equip_equipment_idb='" . $valueID . "'";
							$resultEquip = $db->query($checkQuery);
							if ($resultEquip->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_equip_equipment_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
						}
					}

					if ($key == 'RMS_Room') {
						$RMSBean = BeanFactory::getBean('RMS_Room', $valueID);
						if ($RMSBean->name != "") {
							$checkQuery = "SELECT * FROM `m06_error_rms_room_1_c` where deleted=0 AND  m06_error_rms_room_1m06_error_ida='" . $commID . "' and  m06_error_rms_room_1rms_room_idb='" . $valueID . "'";
							$resultRoom = $db->query($checkQuery);
							if ($resultRoom->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_rms_room_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
						}
					}
				}
			}

			$recordContent = "";
			if (count($addRelationship) > 0) {
				$recordContent .= "For Communication " . $commName . ", Missing records for relationships \n";
				foreach ($addRelationship as $moduleRel => $recordID) {				
					$commBean->load_relationship($moduleRel);
					$commBean->$moduleRel->add($recordID);
					//$commBean->save();
				}
			}

			$mailContent = '';
			if (count($related_ErrorDataArr) > 0) {
				$mailContent .= "<br><br>For Communication " . $commName . ",Following records didnot find in System :<br>";
				$recordContent .= "For Communication " . $commName . ",Following records didnot find in System :  \n";
				foreach ($related_ErrorDataArr as $moduleRel => $recordID) {
					$mailContent .= $moduleRel . "==" . $recordID . "</br>";
					$recordContent .= $moduleRel . "==" . $recordID . " \n";
				}
			}

			if ($recordContent != "") {
				$updateSql = "UPDATE `m06_error_cstm` SET `commapp_mismatch_c` = 'API Status : " . $recordContent . "' WHERE `id_c` = '" . $commID . "' ";
				$db->query($updateSql);
			} else {
				$updateSql = "UPDATE `m06_error_cstm` SET `commapp_mismatch_c` = 'Relationship not required' WHERE `id_c` = '" . $commID . "' ";
				$db->query($updateSql);
			}
		} 

		return null;
	}
}