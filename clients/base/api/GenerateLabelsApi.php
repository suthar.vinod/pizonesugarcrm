<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class GenerateLabelsApi extends ModuleApi
{
    public function registerApiRest()
    {

        return array(
            'generateLabels'    => array(
                'reqType'   => 'POST',
                'path'      => array('generateLabels'),
                'pathVars'  => array(''),
                'method'    => 'getInvetoryData',
                'shortHelp' => '',
                'longHelp'  => '',
            ),
			'getValidTemplates' => array(
                'reqType'   => 'POST',
                'path'      => array('getValidTemplates'),
                'pathVars'  => array(''),
                'method'    => 'getValidTemplates',
                'shortHelp' => '',
                'longHelp'  => '',
            ),
        );
    }

    public function getInvetoryData(RestService $api, array $args): array
    {
        global $current_user;
        global $timedate;

        $data         = array();
        $records      = array();
        $templateId   = $args['templateId'];
        $inventoryIds = $args["inventoryIds"];
		
		$pdfBean          = BeanFactory::retrieveBean("PdfManager", $templateId);
        $data['bodyHtml'] = $pdfBean->body_html;

        foreach ($inventoryIds as $inventoryIdValue) {
            $inventoryId   = $inventoryIdValue['inventoryId'];
            $inventoryBean = BeanFactory::retrieveBean("II_Inventory_Item", $inventoryId);
			
			$inventoryName	= $inventoryBean->name;
			$inventoryName2 = $inventoryName;

            $inventoryBean->load_relationship("m03_work_product_ii_inventory_item_1");

            if ($inventoryBean->m03_work_product_ii_inventory_item_1) {
                $workProductBean = $inventoryBean->m03_work_product_ii_inventory_item_1->getBeans();

                $allRelatedWorkProd = array();
                foreach ($workProductBean as $workBean) {
                    $allRelatedWorkProd[] = $workBean->name;
					$functional_area_c = $workBean->functional_area_c;
                }
            }
			
			if ($inventoryBean->m01_sales_ii_inventory_item_1) {
                $salesBean = $inventoryBean->m01_sales_ii_inventory_item_1->getBeans();

                $allRelatedWorkProd = array();
                foreach ($salesBean as $saleBean) {
                    $allRelatedSales[] = $saleBean->name;
					$functional_area_c = $saleBean->functional_area_c;
                }
            }
			
			
			/*Get Inventory Collection*/
			$allRelatedIC = array();
			$allRelatedII = array();
			$iiNames = "";
			$icName = "";
			$icID = "";
			$inventoryBean->load_relationship("ic_inventory_collection_ii_inventory_item_1");

            if ($inventoryBean->ic_inventory_collection_ii_inventory_item_1) {
                $icBean = $inventoryBean->ic_inventory_collection_ii_inventory_item_1->getBeans();
				foreach ($icBean as $icBean) {
                    $allRelatedIC[] = $icBean->name;
                    $icID =  $icBean->id ;

                    $icBean->load_relationship("ic_inventory_collection_ii_inventory_item_1");
                    $inventoryItemBean = $icBean->ic_inventory_collection_ii_inventory_item_1->getBeans();
                    foreach ($inventoryItemBean as $inventoryBean) {
                        $allRelatedII[]			= $inventoryBean->name;			
                    }    

                }
				$icName  = implode(",",$allRelatedIC);
                $iiNames     = implode(",",$allRelatedII);
            } 

           
            $storageCondition = $GLOBALS['app_list_strings']['inventory_item_storage_condition_list'][$inventoryBean->storage_condition];
			
            $storageMedium = $GLOBALS['app_list_strings']['inventory_item_storage_medium_list'][$inventoryBean->storage_medium];
            $owner = $GLOBALS['app_list_strings']['inventory_item_owner_list'][$inventoryBean->owner];
            
			/*Get Unique barcode of Inventory Item*/
			$uniqueBarCode = $inventoryBean->internal_barcode;
			$uniqueBarCode1 = $inventoryBean->lot_number_c;
			
			
			
			$subtype = "";
			if($inventoryBean->subtype!=""){
				$subtype = str_replace("^","",$inventoryBean->subtype);
			}
			
			
			if($inventoryBean->subtype_specimen_c!=""){				 
				$subtype = str_replace("^","",$inventoryBean->subtype_specimen_c);
			}
			
			$precision 		= 2;
			$concentration  = "";
			if($inventoryBean->concentration_c!="")
				$concentration  = number_format((float) $inventoryBean->concentration_c, $precision, '.', '');
			
				
			$collection_date  = "";
            $collection_time  = "";
            //$GLOBALS['log']->fatal('In collection_date_time '.$inventoryBean->collection_date_time);
 

			if($inventoryBean->collection_date!=""){
				//$collectionDateObj	= new TimeDate();
				//$formattedCD		= $collectionDateObj->to_display_date_time($inventoryBean->collection_date, true, true, $current_user);
				$formattedCD 		= str_replace('-', '/', $formattedCD);
				$collection_date	= date("m/d/Y",strtotime($inventoryBean->collection_date)+18000); 
			}
			
			if($inventoryBean->collection_date_time!=""){
				$collectionDateObj		= new TimeDate();
				 
				$formattedCDT			= $collectionDateObj->to_display_date_time($inventoryBean->collection_date_time, true, true, $current_user);
				$formattedCDT 			= str_replace('-', '/', $formattedCDT);
				
				$collection_datetime	= date("m/d/Y H:i:s",strtotime($formattedCDT));
				$collection_date		= date("m/d/Y",strtotime($formattedCDT)); 
				$collectionTimeArr		= explode(" ",$collection_datetime);
                $collection_time		= $collectionTimeArr[1]; 
			}
			
			
			$expirayDate = "";
			if($inventoryBean->type_2=="Test Article" && ($inventoryBean->expiration_test_article == "Pending" || $inventoryBean->expiration_test_article =="Unknown")){
				$expirayDate = $inventoryBean->expiration_test_article;
			}else{
				if($inventoryBean->expiration_date!=""){
					$expirayDate = date("m/d/Y",strtotime($inventoryBean->expiration_date));
				}
            }
			
			$sterilization_exp_date_c = "";
			if($inventoryBean->sterilization_exp_date_c!=""){
				$sterilization_exp_date_c = date("m/d/Y",strtotime($inventoryBean->sterilization_exp_date_c));
			}
			
			
            
            $iiTimePoint = "";
            /*if($inventoryBean->ii_timepoint_c==""){*/

                if($inventoryBean->timepoint_type=="None")
                {
                    $iiTimePoint = "";//$inventoryBean->timepoint_type;
                }elseif($inventoryBean->timepoint_type=="Ad Hoc" || $inventoryBean->timepoint_type=="Defined" ){
                    if($inventoryBean->timepoint_name_c!="")
                        $iiTimePoint .= $inventoryBean->timepoint_name_c;
                    if($inventoryBean->timepoint_unit!="")
                        $iiTimePoint .= " ".$inventoryBean->timepoint_unit;
                    if($inventoryBean->timepoint_integer!="" || $inventoryBean->timepoint_integer=="0" )
                        $iiTimePoint .= " ".$inventoryBean->timepoint_integer;
                    if($inventoryBean->timepoint_end_integer_c!="" || $inventoryBean->timepoint_end_integer_c=="0")
                        $iiTimePoint .= " - ".$inventoryBean->timepoint_end_integer_c;//

                    
                }else{
                    $iiTimePoint =  $GLOBALS['app_list_strings']['timepoint_type_list'][$inventoryBean->timepoint_type];
				}   
           /* }else{
                $iiTimePoint = $inventoryBean->ii_timepoint_c;
            }*/

            $itemCategory 	= $inventoryBean->category;
            $itemType 		= $inventoryBean->type_2;
			$collDateType 	= $inventoryBean->collection_date_time_type;
			$number_of_aliquots_c 	= $inventoryBean->number_of_aliquots_c;
             

            $specimenDateTemplate				= "";
            $specimenDateDoubleTemplate		= "";
			
			$specimenStorageTemplate			= "";
			$specimenWithoutDateTemplate		= "";
			$specimenWithoutDateDoubleTemplate	= "";
			$specimenCollection 				= "";
            $studyTemplate      				= "";
            $chemicalTemplate   				= "";
            $recordTemplate     				= "";
            $solutionTemplate   				= "";
			$AccessoryTemplate   				= "";
			
			$studyCompatibleTemplate   			= "";
			$studyNotCompatibleTemplate 		= "";
			$studyClientOwnedTemplate   		= "";
			$studyClientNotOwnedTemplate  		= "";
			$studyAPSOwnedTemplate   			= "";
			$studyAPSNotOwnedTemplate   		= "";
			 
           

            if($itemCategory=="Specimen")
            {                
			  if(($itemType == "Block"  || $itemType == "Fecal" || $itemType == "Slide" || $itemType == "Culture" 
					|| $itemType == "Urine" || $itemType == "Whole Blood" || $itemType == "EDTA Plasma") 
					&& ($collDateType =="Date at Record Creation" || $collDateType =="Date Time at Record Creation"))   
                {
                    $specimenDateTemplate = "Specimen with date"; 
                }
								
				if(($itemType == "Na Heparin Plasma" || $itemType == "NaCit Plasma" || $itemType == "Serum" || $itemType == "Other" || $itemType == "Balloons"))   
                {
					if($number_of_aliquots_c =="" && $itemType != "Other" )
					{
						$specimenDateDoubleTemplate = "Specimen with date"; 
						$inventoryName2				= str_replace($itemType,"Whole Blood",$inventoryName);
							
					}	
					else if($collDateType =="Date at Record Creation" || $collDateType =="Date Time at Record Creation"){
						$specimenDateTemplate = "Specimen with date";
					}else if(($collDateType =="Date at Specimen Collection" || $collDateType =="Date Time at Specimen Collection")){
						$specimenWithoutDateTemplate = "Specimen without date"; 
					}
                }	

				if($itemType == "Tissue" && ($collDateType =="Date at Record Creation" || $collDateType =="Date Time at Record Creation"))     
                {
                    $specimenStorageTemplate = "Specimen with date Storage";
                }
				
				if($itemType == "Tissue" && ($collDateType =="Date at Specimen Collection" || $collDateType =="Date Time at Specimen Collection"))     
                {
                    $specimenCollection = "Specimen Collection";
                }
				 
				if(($itemType == "Culture" || $itemType == "Slide" || $itemType == "Urine" || $itemType == "Whole Blood" || $itemType == "EDTA Plasma" ) 
					&& ($collDateType =="Date at Specimen Collection" || $collDateType =="Date Time at Specimen Collection"))     
                {
                    $specimenWithoutDateTemplate = "Specimen without date"; 
                }
				 

            }else if($itemCategory=="Product"){
				$AccessoryTemplate   = "Accessory Product";				

            }else if($itemCategory=="Study Article"){
				
				if($itemType == "Test Article" || $itemType == "Accessory Article"){
					
					if($functional_area_c=="Standard Biocompatibility" || $functional_area_c=="Biocompatibility" ){
						$studyCompatibleTemplate = "studyCompatibleTemplate";
					}else{
						$studyNotCompatibleTemplate = "studyNotCompatibleTemplate";
					}
				}
				
				if($itemType == "Control Article"){
					
					if($inventoryBean->owner=="APS Supplied Client Owned" || $inventoryBean->owner=="Client Owned"){
						if($functional_area_c=="Standard Biocompatibility" || $functional_area_c=="Biocompatibility"){
							$studyClientOwnedTemplate = "studyClientOwnedTemplate";
						}else{
							$studyClientNotOwnedTemplate = "studyClientNotOwnedTemplate";
						}						
					} 
					
					if($inventoryBean->owner=="APS Owned"){
						if($functional_area_c=="Standard Biocompatibility" || $functional_area_c=="Biocompatibility"){
							$studyAPSOwnedTemplate = "studyAPSOwnedTemplate";
						}else{
							$studyAPSNotOwnedTemplate = "studyAPSNotOwnedTemplate";
						}						
					} 
						 
				}
				  
			}else if($itemCategory=="Record"){

                $recordTemplate = "Record";

            }
			
			if($inventoryBean->test_type_c=="Other"){
				$test_type_c = $inventoryBean->other_test_type_c;
			}else{
				$test_type_c = $inventoryBean->test_type_c;
			}
			
			
					
            
			$inventoryData = array(
                "name"                  => $inventoryName,
                "category"              => $inventoryBean->category,
                "type"                  => $itemType,
                "inventoryName"         => $inventoryName,
                "inventoryName2"        => $inventoryName2,
                "timepoint"             => $iiTimePoint,
                "collectionDate"        => $collection_date,
				"collectionTime"        => $collection_time,
                "uniqueId"              => $uniqueBarCode,
                "uniqueIdProduct"       => $uniqueBarCode1,
                "expirationDateTime"    => $expirayDate,
                "storageCondition"      => $storageCondition,
				"storageMedium"         => $storageMedium,
                "owner"                 => $owner,
                "sterilization"         => $inventoryBean->sterilization,
                "sterilizationExpDate"  => $sterilization_exp_date_c,
                "manufacturer"          => $inventoryBean->manufacturer_c,
                "expirationTestArticle" => $inventoryBean->expiration_test_article,
                "subtype"               => $subtype,
                "productName"           => $inventoryBean->product_name,
                "testSystemName"        => $inventoryBean->anml_animals_ii_inventory_item_1_name,
                "relatedTo"             => $inventoryBean->related_to_c,
                "allRelatedWorkProd"    => $allRelatedWorkProd, 
                "inactive"              => $inventoryBean->inactive_c,
                "solutionName"          => $inventoryBean->solution_name_c,
                "productNameRelate"     => $inventoryBean->product_name_c,
                "testType"     			=> $test_type_c,
				"concentration"         => $concentration,
                "concentrationUnit"     => $inventoryBean->concentration_unit_c,
				"collectionName"		=> $icName,
				"allIINames"			=> $iiNames,
				"specimenDateTemplate"     			=> $specimenDateTemplate,
				"specimenDateDoubleTemplate"     	=> $specimenDateDoubleTemplate,
				"specimenWithoutDateDoubleTemplate" => $specimenWithoutDateDoubleTemplate,
				"specimenStorageTemplate"   	=> $specimenStorageTemplate,
				"specimenWithoutDateTemplate"   => $specimenWithoutDateTemplate,
				"specimenCollection"    		=> $specimenCollection,
				"studyCompatibleTemplate"     	=> $studyCompatibleTemplate,
				"studyNotCompatibleTemplate"    => $studyNotCompatibleTemplate,
				"studyClientOwnedTemplate"      => $studyClientOwnedTemplate,
				"studyClientNotOwnedTemplate"   => $studyClientNotOwnedTemplate,
				"studyAPSOwnedTemplate"     	=> $studyAPSOwnedTemplate,
				"studyAPSNotOwnedTemplate"     	=> $studyAPSNotOwnedTemplate,
                "chemicalTemplate"     			=> $chemicalTemplate,
                "recordTemplate"     			=> $recordTemplate,
                "solutionTemplate"     			=> $solutionTemplate,
				"accessoryTemplate"     		=> $AccessoryTemplate,
				
				 

            );
			//$GLOBALS['log']->fatal('In PrintLabel API - inventoryData '.implode("==",$inventoryData));	
            $records[] = $inventoryData;
        }

        $data['records'] = array("records" => $records);

        return $data;
    }
	
	public function getValidTemplates(RestService $api, array $args): array
    {
        $templates              = array();
        $inventoryItemsCategory = $args["inventoryItemsCategory"];

       
		
		$templatesMappingTable = array(
            "Specimen" => array(
                "Block"      => array("Specimen"),
                "Culture"    => array("Specimen"),
                "Fecal"      => array("Specimen"),
                "Plasma"     => array("Specimen"),
                "Serum"      => array("Specimen"),
                "Slide"      => array("Specimen"),
                "Tissue"     => array("Specimen"),
                "Urine"      => array("Specimen"),
                "WholeBlood" => array("Specimen"),
                "Extract"    => array("Specimen"),
                "Balloons"      => array("Specimen"),
            ),

            "Product" => array(
                "AccessoryProduct" => array("Study Article"),
                "ControlArticle"   => array("Study Article"),
                "TestArticle"      => array("Study Article"),
                "Chemical"         => array("Chemical, Cleaning Agent, Drug, Reagent or Solution"),
                "Reagent"          => array("Chemical, Cleaning Agent, Drug, Reagent or Solution"),
                "Drug"             => array("Chemical, Cleaning Agent, Drug, Reagent or Solution"),
                "CleaningAgent"    => array("Chemical, Cleaning Agent, Drug, Reagent or Solution"),
            ),

            "Solution" => array(
                ""      => array("Chemical, Cleaning Agent, Drug, Reagent or Solution"),
            ),

            "Record" => array(
                "DataBook"                => array("Record"),
                "DataSheet"               => array("Record"),
                "EquipmentFacilityRecord" => array("Record"),
                "Harddrive"               => array("Record"),
                "ProtocolBook"            => array("Record"),
            ),
        );

        foreach ($inventoryItemsCategory as $key => $itemData) {
            $category = $itemData["category"];
            $type     = str_replace(' ', '', $itemData["type"]);
 
            if (isset($templatesMappingTable[$category]) === true) {
                $templates = array_merge($templates, $templatesMappingTable[$category][$type]);
                if (isset($templatesMappingTable[$category][$type]) === true) {
                    $templates = array_merge($templates, $templatesMappingTable[$category][$type]);
                }
            }
        }
		$GLOBALS['log']->fatal('In pdf templates API - tempResult '.implode("==",$templates));
        $templateName = array_unique($templates);

        return $templateName;
    }
    
}