<?php
//GetCustomReportData
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class GetCustomReportData extends SugarApi{
	public function registerApiRest()
    {
        return array(
            //GET
            'getCSTMReportData' => array(
                //request type
                'reqType' => 'POST',

                //set authentication
                'noLoginRequired' => false,

                //endpoint path
                'path' => array('getCSTMReportData'),

                //endpoint variables
                'pathVars' => array('', '', 'data'),

                //method to call
                'method' => 'ProcessCustomReportData',

                //short help string to be displayed in the help documentation
                'shortHelp' => '',

                //long help to be displayed in the help documentation
                'longHelp' => '',
            ),
        );
    }
	public function ProcessCustomReportData($api, $args)
    {
        //custom logic
		//$return_data = array($_REQUEST,$args);
        //return $args;
		
		require_once('custom/modules/Reports/CustomReportUtils.php');
		global $timedate, $current_user;
		// $report_id = $_REQUEST['report_id'];
		// $id = 'fbb122b2-8e99-11ea-a9f5-0242ac130006';
		// $id = $args['report_id'];
		$work_product_id = $args['workproduct_id'];
		$report_name = $args['report_name'];
		$user_id = $args['user_id'];
		$filter_start_date = $args['filter_start_date'];
		$filter_end_date = $args['filter_end_date'];
		$reportData = array();
		if($report_name == 'AllNotificationforStudy'){
			$reportData = AllNotificationforStudy($work_product_id);
		}else if($report_name == 'AllDeviationsforStudy'){
			$reportData = AllDeviationsforStudy($work_product_id);
		}else if($report_name == 'AllAdverseEventsforStudy'){
			$reportData = AllAdverseEventsforStudy($work_product_id);
		}
		else if($report_name == 'CustomTSWeight'){
			$reportData = CustomTSWeight($filter_start_date,$filter_end_date);
		}
		

		$return_data = array();

		// echo '<pre>';
		// print_r($return_data);
		/* get date as per required formate and time zone */
		$User = BeanFactory::getBean('Users', $user_id);
		$timedate = new TimeDate($User);
		// returns current date/time in the user's timezone
		$now_userTZ = $timedate->getNow(true);
		$report_print_time = $now_userTZ->format("D, d M Y H:i:s O");
		
		//report_name,report_type,report_module
		$return_data['status'] = 'Success';
		$return_data['report_name'] = $report_name;
		$return_data['work_product_id'] = $work_product_id;
		$return_data['report_type'] = 'Custom';
		$return_data['report_module'] = 'Reports';
		$return_data['report_print_time'] = $report_print_time;
		$return_data['work_product'] = $reportData['work_product'];
		$return_data['report_length'] = sizeof($reportData['ReportData']);
		$return_data['report_data'] = $reportData['ReportData'];
		$return_data['ReportSubtypeDataCount'] = $reportData['ReportSubtypeDataCount'];
		$return_data['ReportClassificationDataCount'] = $reportData['ReportClassificationDataCount'];
		$return_data['Related_Comm_Data'] = $reportData['Related_Comm_Data'];
		/*#633 */
		$return_data['filter_start_date'] = $filter_start_date;
		$return_data['filter_end_date'] = $filter_end_date;
		
		return $return_data;
		/* Process Report logic */
		// return $args;
    }
}

?>