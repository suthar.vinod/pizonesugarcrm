<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class GetTestSystemDataApi extends SugarApi {

    public function registerApiRest() {
        return array(
            "checkCompliance1" => array(
                "reqType" => "POST",
                "noLoginRequired" => true,
                'path' => array('get_test_system_data'),
                'pathVars' => array('get_test_system_data'),
                'method' => 'get_data',
                "shortHelp" => "Getting Test System data based on character match",
                "longHelp" => "",
            ),
        );
    }

    public function get_data($api, $args) {
        global $db;

        //Current Work Product Id
        $WP_ID = $args['wpid'];
        $batchId = $args['batchId'];
        $character = $args['character'];
        $tsChecked = $args['tsChecked'];
         
        $data = array();
        $WPID = '';
        foreach($WP_ID as $WPIDs){
            $WPID_s = "'".$WPIDs."'";
            $WPID .=  $WPID_s.","; 
         }
        $WP_IDs = rtrim($WPID, ','); 
         
        $idArr = array();
        if(empty($WP_IDs) || $WP_IDs == ""){
            $wpidCond = '= ""';
        }else{
            $wpidCond = 'IN (' . $WP_IDs . ')';
        }
         
        if($tsChecked != '1'){
            if(!empty($batchId)){
                // Getting WPE ids where WP field in WPE module is equal to current WP id or WP blanket Id
                $sqlWPEqual='SELECT  bid_batch_f386ollment_idb as WPEID
                FROM bid_batch_id_wpe_work_product_enrollment_1_c 
                where bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida="'.$batchId.'" and deleted=0';
                
            }else{
                // Getting WPE ids where WP field in WPE module is equal to current WP id or WP blanket Id
                $sqlWPEqual='SELECT  m03_work_p9bf5ollment_idb AS WPEID 
                FROM m03_work_product_wpe_work_product_enrollment_1_c 
                where m03_work_p7d13product_ida '.$wpidCond.' and deleted=0 
                UNION 
                SELECT  m03_work_p90c4ollment_idb as WPEID
                FROM m03_work_product_wpe_work_product_enrollment_2_c 
                where m03_work_p9f23product_ida '.$wpidCond.' and deleted=0';
            } 
             
            $resultWPEqual = $db->query($sqlWPEqual);
            while ($rowWPEqual = $db->fetchByAssoc($resultWPEqual)) {
                array_push($idArr, $rowWPEqual['WPEID']);
            }

            //Removing Duplicate WPE ids, if present
            $uniqWPEIds = array_unique($idArr);

            $wpestr = implode("','", $uniqWPEIds); 
            if(!empty($character)){
                $character = "AND ( TS.name like '" . $character . "%' OR  TSCSTM.usda_id_c like '" . $character . "%')";
            }else{
                $character = "";
            } 
              
            $limit = '';
            if($WP_ID=="4a529aae-5165-3149-6db7-5702c7b98a5e" || $WP_ID=="811ae58e-9281-2755-25d5-5702c7252696")
            {
                    $limit = 'limit 20';
            }   

            $sqlTestSysId = "SELECT TS.name,TSCSTM.usda_id_c,TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida as testId 
                        FROM anml_animals_wpe_work_product_enrollment_1_c AS TSWPA
                        LEFT JOIN anml_animals AS TS ON TS.id=TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida
                        LEFT JOIN anml_animals_cstm AS TSCSTM ON TS.id=TSCSTM.id_c
                        WHERE anml_anima9941ollment_idb IN ('" . $wpestr . "') $character AND TS.deleted=0 group by testId $limit";
            
            $resultTestSystem = $db->query($sqlTestSysId);
            while ($rowTS = $db->fetchByAssoc($resultTestSystem)) {
                array_push($data, array(
                    "testSystemId" => $rowTS['testId'],
                    "testSystemName" => $rowTS['name'],
                    "usdaId" => $rowTS['usda_id_c'],
                ));
            } 
            
            sleep(1);
            return $data;
        }else{ 
            if(!empty($batchId)){
                // Getting WPE ids where WP field in WPE module is equal to current WP id or WP blanket Id
                $sqlWPEqual='SELECT  bid_batch_f386ollment_idb as WPEID
                FROM bid_batch_id_wpe_work_product_enrollment_1_c 
                where bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida="'.$batchId.'" and deleted=0'; 
                 
            }else{
                // Getting WPE ids where WP field in WPE module is equal to current WP id or WP blanket Id
                $sqlWPEqual='SELECT  m03_work_p9bf5ollment_idb AS WPEID 
                FROM m03_work_product_wpe_work_product_enrollment_1_c 
                where m03_work_p7d13product_ida '.$wpidCond.' and deleted=0';
            }

            $resultWPEqual = $db->query($sqlWPEqual);
            while ($rowWPEqual = $db->fetchByAssoc($resultWPEqual)) { 
                array_push($idArr, $rowWPEqual['WPEID']);
            }

            //Removing Duplicate WPE ids, if present
            $uniqWPEIds = array_unique($idArr); 

            $wpestr = implode("','", $uniqWPEIds);

            $sqlTestSysId = "SELECT TS.name,TSCSTM.usda_id_c,TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida as testId 
                    FROM anml_animals_wpe_work_product_enrollment_1_c AS TSWPA
                    LEFT JOIN anml_animals AS TS ON TS.id=TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida
                    LEFT JOIN anml_animals_cstm AS TSCSTM ON TS.id=TSCSTM.id_c
                    WHERE anml_anima9941ollment_idb IN ('" . $wpestr . "') AND TS.deleted=0 group by testId";
             
            $resultTestSystem = $db->query($sqlTestSysId);
            while ($rowTS = $db->fetchByAssoc($resultTestSystem)) {
                array_push($data, array(
                    "testSystemId" => $rowTS['testId'],
                    "testSystemName" => $rowTS['name'],
                    "usdaId" => $rowTS['usda_id_c'],
                ));
            } 
            
            sleep(1);
            return $data;
        }
        
    }

}
