<?php

if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

class customTestSystemRelatedCommApi extends SugarApi
{

	public function registerApiRest()
	{
		return array(
			'getTsRelatedComm' => array(
				'reqType' => 'POST',
				'path' => array('getTsRelatedComm'),
				'pathVars' => array('getTsRelatedComm'),
				'method' => 'getTsRelatedComm',
				'shortHelp' => 'getRelatedComm for the selected Ts',
				'longHelp' => '',
			),
		);
	}

	public function getTsRelatedComm($api, $args)
	{
		global $db, $current_user;
		$ts_Ids = $args["ts_Id"];

		$returnArr = array();
		$tsName = array();
		$CommName = array();
		$status = '';
		foreach ($ts_Ids as $recordId) {
			if ($recordId != '') {

				$Query = "SELECT ts.id as tsid, ts.name as tsName,m06.name as commName,m06c.error_type_c,m06c.error_category_c FROM anml_animals AS ts
							LEFT JOIN m06_error_anml_animals_1_c AS EA ON EA.m06_error_anml_animals_1anml_animals_idb = ts.id
							LEFT JOIN m06_error AS m06 ON EA.m06_error_anml_animals_1m06_error_ida = m06.id
							LEFT JOIN m06_error_cstm AS m06c ON m06c.id_c = m06.id
							WHERE ts.id = '" . $recordId . "'";

				$result = $db->query($Query);

				if ($result->num_rows > 0) {

					while ($row = $db->fetchByAssoc($result)) {

						if ($row['error_type_c'] == 'Rejected' || $row['error_category_c'] == 'Rejected Animal') {
							$tsName[] = $row['tsName'];
							$CommName[] = $row['commName'];
						}
					}

					if (!empty($tsName)) {
						$tsNameStr = trim(implode(',', $tsName));
						$commNameStr = trim(implode(',', $CommName));
						$status = 'Rejected';
					} else {
						$tsNameStr = '';
						$commNameStr = '';
						$status = 'NotRejected';
					}
					$returnArr['tsName'] = $tsNameStr;
					$returnArr['commName'] = $commNameStr;
					$returnArr['status'] = $status;
				} else {
					$returnArr['tsName'] = '';
					$returnArr['commName'] = '';
					$returnArr['status'] = 'NotRejected';
				}
			}
		}
		//$GLOBALS['log']->fatal('returnArr '.print_r($returnArr,1));
		return $returnArr;
	}
}
