<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class ProcedureRoomSigninAPI extends SugarApi
{
    // default function to extends SugarAPI
    public function registerApiRest()
    {
        return array(
            'ProcedureRoomSignin' => array(
                'reqType' => 'POST',
                'path' => array('ProcedureRoomSignin'),
                'method' => 'ProcedureRoomSignin',
                'shortHelp' => 'When the person signing in enters in the Procedure Room or Employee their meeting with an email needs to be sent to that employee/room (email address is in Sugar) stating that their guest has arrived at APS',
                'longHelp' => '',
            ),
        );
    }

    /**
     * @param type $api
     * @param type $args
     * @return type
     */
    public function ProcedureRoomSignin($api, $args)
    {
        if (!isset($args['name']) || !isset($args['email'])) {
            throw new SugarApiExceptionMissingParameter();
        } else {
            $template = new EmailTemplate();
            $template->retrieve_by_string_fields(array('id' => '1684ce06-fa93-11e7-9c18-6cc217ea273c'));
            $template->body_html = str_replace('$name', $args['name'], $template->body_html);
            $template->body_html = str_replace('$visitor_name', $args['visitor_name'], $template->body_html);
            $emailObj = new Email();
            $defaults = $emailObj->getSystemDefaultEmail();
            $mail = new SugarPHPMailer();
            $mail->IsHTML(true);
            $mail->setMailerForSystem();
            $mail->From = $defaults['email'];
            $mail->FromName = $defaults['name'];
            $mail->Subject = $template->subject;
            $mail->Body = from_html($template->body_html);
            $mail->prepForOutbound();
            $mail->AddAddress($args['email']);
            $sent = $mail->Send();
            if (!$sent) {
                $GLOBALS['log']->debug('Contact creation mail not sent');
            }
        }
    }
}
