<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
class GetWPBlanketApi extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            "checkCompliance1" => array(
                "reqType"         => "GET",
                "noLoginRequired" => true,
                'path'            => array('M03_Work_Product', '?', 'get_blanket_product'),
                'pathVars'        => array('module', 'record'),
                'method'          => 'get_blanket_product',

                "shortHelp"       => "Getting WPBlanket field data based on selected WP",
                "longHelp"        => "",
            ),
            "checkCompliance2" => array(
                "reqType" => "GET",
                "noLoginRequired" => true,
                'path' => array('M03_Work_Product', '?', '?', 'get_test_system_design'),
                'pathVars' => array('module', 'record', 'TSID'),
                'method' => 'get_test_system_design',
                "shortHelp" => "Get details of Test System Design from work product record",
                "longHelp" => "",
            ),
        );
    }

    public function get_blanket_product($api, $args)
    {

        $module   = $args['module'];
        $record_id = $args['record'];

        $bean = BeanFactory::retrieveBean($module, $record_id, array('disable_row_level_security' => true));
        $work_id = $bean->m03_work_product_id_c;

        $data = array("blanket_protocol_c" => $bean->blanket_protocol_c, "workID" => $work_id);

        return $data;
    }

    public function get_test_system_design($api, $args)
    {

        $module     = $args['module'];
        $record_id  = $args['record'];
        $ts_id      = $args['TSID'];
        $data       = array();



        $tsBean = BeanFactory::retrieveBean('ANML_Animals', $ts_id, array('disable_row_level_security' => true));

        $tsSex = $tsBean->sex_c;
        if ($tsSex == "")
            return 1;
        $tsSexFirstChar = substr($tsSex, 0, 1);

        /*$checkArr['Male'] = 'M';
        $checkArr['Female'] = 'F';
        $checkArr['Castrated Male'] = 'CM';*/

        $strQuery = "SELECT m03_work_p4745esign_1_idb as tsdID,TSD.sex 
                     FROM m03_work_product_tsd1_test_system_design_1_1_c AS WPTSD
                     LEFT JOIN tsd1_test_system_design_1 AS TSD ON WPTSD.m03_work_p4745esign_1_idb = TSD.id
                     where m03_work_product_tsd1_test_system_design_1_1m03_work_product_ida = '" . $record_id . "' and WPTSD.deleted=0";

        // Execute
        $results = $GLOBALS['db']->query($strQuery);

        if ($results->num_rows > 0) {
            while ($row = $GLOBALS['db']->fetchByAssoc($results)) {
                $TSDSex =  $row['sex'];

                switch ($TSDSex) {
                    case "Equal CMF":
                        $TSDSex = str_replace("Equal CMF", "CM F", $TSDSex);
                        break;
                    case "Equal CMM":
                        $TSDSex = str_replace("Equal CMM", "CM M", $TSDSex);
                        break;
                    case "Equal MF":
                        $TSDSex = str_replace("Equal MF", "M F", $TSDSex);
                        break;
                    case "Equal MFCM":
                        $TSDSex = str_replace("Equal MFCM", "M F CM", $TSDSex);
                        break;
                    case "or":
                        $TSDSex = str_replace("or", "", $TSDSex);
                        break;
                    case "Quantity Specific":
                        $TSDSex = str_replace("Quantity Specific", "Q", $TSDSex);
                        break;
                }

                $TSDSexArr = explode(" ", $TSDSex);

                foreach ($TSDSexArr as $data => $value) {
                    $tsdFirstChar = substr($value, 0, 1);
                    if ($tsdFirstChar=="Q" || $tsdFirstChar == $tsSexFirstChar)
                        return 1;
                }
            }
            return 0;
        } else {
            return 1;
        }
    }
}
