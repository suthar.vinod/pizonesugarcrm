<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class CheckTestSystemRemainingApi extends SugarApi {

    public function registerApiRest()
	{
		return array(
			'CheckTestSystemRemainingInWP' => array(
				'reqType' => 'POST',
				'path' => array('CheckTestSystemRemainingInWP'),
				'pathVars' => array('CheckTestSystemRemainingInWP'),
				'method' => 'CheckTestSystemRemainingInWP',
				'shortHelp' => 'Check Test System Remaining In WP',
				'longHelp' => '',
			),
		);
	}

	public function CheckTestSystemRemainingInWP($api, $args)
	{
		global $db;

		//Current Work Product Id
		$workProductIDs = $args['workProductID'];
		$WPACount = $args['WPACount'];
		$batchId = $args['batchId'];
		//$GLOBALS['log']->fatal('CheckTestSystemRemainingInWP WPACount' . $WPACount);
		//$GLOBALS['log']->fatal('CheckTestSystemRemainingInWP workProductIDs' . print_r($workProductIDs,1));

		if(empty($batchId)){
			$workProductIDs = json_encode($workProductIDs); 
  
			$workProductIDs = str_replace("[", '', $workProductIDs);
			$workProductIDs = str_replace("]", '', $workProductIDs);

			//Final output array
			$data = array();
			
			$sqlWP = 'SELECT sum(wpc.primary_animals_countdown_c) as animal_remaining FROM m03_work_product_cstm AS wpc WHERE id_c IN ('.$workProductIDs.')';
			//$GLOBALS['log']->fatal('CheckTestSystemRemainingInWP sqlWP ' . $sqlWP);
			
			$resultWP = $db->query($sqlWP);
			$rowWP = $db->fetchByAssoc($resultWP); 
			
			$animal_remaining = $rowWP['animal_remaining']-$WPACount;
			//$GLOBALS['log']->fatal('CheckTestSystemRemainingInWP animal_remaining ' . $rowWP['animal_remaining']."   ".$animal_remaining);
			if ($animal_remaining < 0) {
				$data = array(
					"status" => false,
					"animalRemaining" =>$animal_remaining
				);
			}else{
				$data = array(
					"status" => true,
					"animalRemaining" =>$animal_remaining
				);
			}
		}else{
			$data = array(
				"status" => true
			);
		}
		
		 
		//$GLOBALS['log']->fatal('CheckTestSystemRemainingInWP data' . print_r($data,1));
		return $data;
	}
}
?>
