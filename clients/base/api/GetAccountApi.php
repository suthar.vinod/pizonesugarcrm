<?php

class GetAccountApi extends SugarApi {
    public function registerApiRest()
    {
        return array(
            //GET
            'MyGetAccount' => array(
                //request type
                'reqType' => 'GET',

                //set authentication
                'noLoginRequired' => false,

                //endpoint path
                'path' => array('AllAccounts'),

                //endpoint variables
//                'pathVars' => array('', 'data'),

                //method to call
                'method' => 'GetAccountMethod',

                //short help string to be displayed in the help documentation
                'shortHelp' => 'An example of a GET endpoint',

                //long help to be displayed in the help documentation
                'longHelp' => '',
            ),
        );
    }

    /**
     * Method to be used for my AllAccounts endpoint
     */
    public function GetAccountMethod($api, $args)
    {

        $arReturn = array();
        $company_name = "American Preclinical Services";
        // Get a list of accounts name and email addresses associated with contacts 
        $strQuery = "SELECT accounts.name, contacts.id, email_addresses.email_address AS email "
                . "FROM accounts INNER JOIN accounts_contacts ON accounts.id = accounts_contacts.account_id "
                . "AND accounts.deleted = 0 AND accounts.name = '".$company_name."' "
                . "AND accounts_contacts.deleted = 0 INNER JOIN contacts ON accounts_contacts.contact_id = contacts.id "
                . "AND contacts.deleted = 0 LEFT JOIN email_addr_bean_rel ON contacts.id = email_addr_bean_rel.bean_id "
                . "AND email_addr_bean_rel.deleted = 0 LEFT JOIN email_addresses "
                . "ON email_addr_bean_rel.email_address_id = email_addresses.id "
                . "AND email_addresses.deleted = 0";
        
        // Execute
        $results = $GLOBALS['db']->query($strQuery);

        // Loop through
        while ($arRow = $GLOBALS['db']->fetchByAssoc($results)) {
            $arReturn[] = array(
                'id' => $arRow["id"],
                'name' => $arRow["name"],
                'email' => $arRow["email"],
            );
        }
        
        return  $arReturn ;
    }
}