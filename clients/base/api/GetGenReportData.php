<?php
//GetGenReportData
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
use Sugarcrm\Sugarcrm\modules\Reports\Exporters\ReportExporter;
class GetGenReportData extends SugarApi{
	public function registerApiRest()
    {
        return array(
            //GET
            'getReportData' => array(
                //request type
                'reqType' => 'POST',

                //set authentication
                'noLoginRequired' => false,

                //endpoint path
                'path' => array('GetReportData'),

                //endpoint variables
                'pathVars' => array('', '', 'data'),

                //method to call
                'method' => 'ProcessReportData',

                //short help string to be displayed in the help documentation
                'shortHelp' => '',

                //long help to be displayed in the help documentation
                'longHelp' => '',
            ),
        );
    }
	public function ProcessReportData($api, $args)
    {
        //custom logic
		//$return_data = array($_REQUEST,$args);
        //return $args;
		// use Sugarcrm\Sugarcrm\modules\Reports\Exporters\ReportExporter;
		/* Process Report logic */
		
		//use Sugarcrm\Sugarcrm\modules\Reports\Exporters\ReportExporter;
		require_once('include/export_utils.php');
		global $db, $timedate, $current_user;
		// $report_id = $_REQUEST['report_id'];
		// $id = 'fbb122b2-8e99-11ea-a9f5-0242ac130006';
		$id = $args['report_id'];
		$user_id = $args['user_id'];
		$User = BeanFactory::getBean('Users', $user_id);
		$saved_report_seed = BeanFactory::newBean('Reports');
		$saved_report_seed->retrieve($id, false);

		$report_name = $saved_report_seed->name;
		$report_type = $saved_report_seed->report_type;
		$report_module = $saved_report_seed->module;
		
		/*Saved Report Content**/
		$report_content = $saved_report_seed->content;
		
		/*Get Run Time Filter*/
		$queryRuntimeFilter = "SELECT * FROM report_cache WHERE id='".$id."' and assigned_user_id='$user_id'";
		$runtimeFilterResult = $db->query($queryRuntimeFilter);
    	while ($fetchResult = $db->fetchByAssoc($runtimeFilterResult)) {
			$cacheContent = $fetchResult['contents'];
		}	

		if($cacheContent!="") // if Runtime filter is available then need to update the Filter Definition.
		{
			//Decode the JSON data into a PHP array.
			$contentsDecoded = json_decode($report_content, true);
			$json2Decoded = json_decode($cacheContent, true);
			$contentsDecoded['filters_def'] = $json2Decoded['filters_def'];
			$report_content = json_encode($contentsDecoded);
		} 
		
		// $reporter = new Report($saved_report_seed->content);
		
		/* Get report data from Report Content.*/
		$reporter = new Report($report_content);
		

		$reporter->plain_text_output = true;
		//disable paging so we get all results in one pass
		$reporter->enable_paging = false;

		$exporter = new ReportExporter($reporter);
		$content = $exporter->export();
		

		global $locale, $sugar_config;

		//$transContent = $GLOBALS['locale']->translateCharset(content, 'UTF-8', $GLOBALS['locale']->getExportCharset(), false, true);

 
		$contentArr  = explode("\r\n",$content);

		$report_data = array();
		foreach($contentArr as $row){
			//$simple_str =  substr($row,1,-2);
			$first = substr($row,0,1);
			if($first=='"')
				$row  = substr($row,1);

			$last = substr($row,-1);
			if($last=='"')
				$row  = substr($row,0,-1);

			$arr = explode('","',$row);
			// $report_data[] =$arr;
			if(sizeof($arr)> 1 && $arr[0]){
				$report_data[] = $arr;
			}else if($arr[0]){
				$report_data[] = $arr;
			}
		}
		//remove extra empty row
		// array_pop($report_data);
		$return_data = array();
		if($report_type == 'detailed_summary'){
			$summition_report_data = array();
			$ind = 0;
			foreach($report_data as $indx => $datarow){
				if(sizeof($datarow) == 1 ){		
					$ind++;
				}
				$summition_report_data[$ind][] = $datarow;
			}
			$return_data['report_length'] = sizeof($summition_report_data);
			$return_data['report_data'] = $summition_report_data;
		}else{
			$return_data['report_length'] = sizeof($report_data);
			$return_data['report_data'] = $report_data;
		}
		// echo '<pre>';
		// print_r($return_data); 
		/* get date as per required formate and time zone */
		$timedate = new TimeDate($User);
		// returns current date/time in the user's timezone
		$now_userTZ = $timedate->getNow(true);
		$report_print_time = $now_userTZ->format("D, d M Y H:i:s O");
		
		//report_name,report_type,report_module
		$return_data['status'] = 'Success';
		$return_data['report_name'] = $report_name;
		$return_data['report_id'] = $report_name;
		$return_data['report_type'] = $report_type;
		$return_data['report_module'] = $report_module;
		$return_data['report_print_time'] = $report_print_time;
		// $return_data['report_length'] = sizeof($report_data);
		// $return_data['report_data'] = $report_data;
		
		return $return_data; 
		/* Process Report logic */
		// return $args;
    }
}

?>