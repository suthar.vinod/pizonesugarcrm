<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class getWPSidecarDataApi extends SugarApi {

    public function registerApiRest() {
        return array(
            'getWPSidecarData'    => array(
                'reqType'         => 'POST',
                'noLoginRequired' => false,
                'path'            => array('getWPSidecarData'),
                'pathVars'        => array('getWPSidecarData'),
                'method'          => 'getWPSidecarData',
                'shortHelp'       => '',
                'longHelp'        => '',
            ),
        );
    }

    function getWPSidecarData($api, $args) {
		global $db,$current_user;
        // $GLOBALS['log']->fatal('args of Deliverable Template' .print_r($args,1));
        $module       = $args['module'];
        $offsetBID    = $args['offsetBID'];
        $wpNameSearch = $args['wpNameSearch'];

        if($offsetBID != 0)
            $offSet = $offsetBID;
        else
            $offSet = 0;

        // $GLOBALS['log']->fatal('offsetBID====>'.$offsetBID);
        // $GLOBALS['log']->fatal('wpNameSearch====>'.$wpNameSearch);

        if(!empty($wpNameSearch)){
            $sqlWP ='SELECT 
                        wp.name,
                        wp.id,
                        bidwp.bid_batch_id_m03_work_product_1m03_work_product_idb 
                    FROM m03_work_product AS wp
                    INNER JOIN m03_work_product_cstm wpc ON wp.id = wpc.id_c AND wp.deleted =0
                    LEFT JOIN bid_batch_id_m03_work_product_1_c AS bidwp 
                    ON bidwp.bid_batch_id_m03_work_product_1m03_work_product_idb = wp.id  
                    AND (bidwp.bid_batch_id_m03_work_product_1bid_batch_id_ida = "" || bidwp.bid_batch_id_m03_work_product_1bid_batch_id_ida = NULL)
                    WHERE (wpc.work_product_status_c IN ("Pending","In Development","Testing")) 
                    AND (wpc.functional_area_c IN ("Standard Biocompatibility","Biocompatibility")) AND wp.name LIKE "'.$wpNameSearch.'%"
                    ORDER BY wp.name ASC LIMIT 20 OFFSET '.$offSet.'';
                    // $GLOBALS['log']->fatal('sqlWP====>'.$sqlWP);
        }else{
            $sqlWP = "SELECT 
                    wp.name,
                    wp.id,
                    bidwp.bid_batch_id_m03_work_product_1m03_work_product_idb 
                FROM m03_work_product AS wp
                INNER JOIN m03_work_product_cstm wpc ON wp.id = wpc.id_c AND wp.deleted = 0
                LEFT JOIN bid_batch_id_m03_work_product_1_c AS bidwp ON bidwp.bid_batch_id_m03_work_product_1m03_work_product_idb = wp.id  AND (bidwp.bid_batch_id_m03_work_product_1bid_batch_id_ida = '' || bidwp.bid_batch_id_m03_work_product_1bid_batch_id_ida = NULL)
                WHERE (wpc.work_product_status_c IN ('Pending','In Development','Testing')) AND (wpc.functional_area_c IN ('Standard Biocompatibility','Biocompatibility')) ORDER BY wp.name ASC LIMIT 20 OFFSET ".$offSet."";
        }

        $dataWP = array();
        $resultsWP = $db->query($sqlWP);

        while ($row = $db->fetchByAssoc($resultsWP)) {
            array_push($dataWP, array(
                "name" => $row['name'],
                "id" => $row['id'],
            ));
        }
        // $GLOBALS['log']->fatal('records from getSidecarDataAPI====>'.print_r($dataWP));
        return array("records" => $dataWP);
    }

}

