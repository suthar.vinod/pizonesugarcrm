<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
class InventoryItemsCompliance extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            "checkCompliance" => array(
                "reqType"         => "POST",
                "noLoginRequired" => false,
                "path"            => array('checkInventoryItemsCompliance'),
                "pathVars"        => array(),
                "method"          => "checkCompliance",
                "shortHelp"       => "Checking if the Invetory Items can be linked to certain Inventory Management record",
                "longHelp"        => "",
            ),
        );
    }
    /**
     * Function to check either the current item can be linked to a inventory management record
     */
    public function checkCompliance(ServiceBase $api, array $args)
    {
        $item_errors             = array("inactive_items" => array(), "not_matching" => array());
        $inventory_items_ids     = $args["inventory_items_ids"];
        $inventory_management_id = $args["inventory_managemet_id"];
        $inventory_items         = array();
        foreach ($inventory_items_ids as $i_id) {
            $item = BeanFactory::retrieveBean("II_Inventory_Item", $i_id);
            if ($item !== null) {
                if ($item->inactive_c) {
                    $item_errors["inactive_items"][] = $item->id;
                } else {
                    $item->load_relationship("m03_work_product_ii_inventory_item_1");
                    $inventory_items[] = $item;
                }
            }
        }
        $items_work_products = $this->getItemsWorkProducts($inventory_items);
        //$GLOBALS['log']->fatal('items_work_products:=>' . print_r($items_work_products,1));
        $inventoryManagement = BeanFactory::retrieveBean("IM_Inventory_Management", $inventory_management_id);
        //$GLOBALS['log']->fatal('inventoryManagement:=>' . print_r($inventoryManagement,1));
        if ($inventoryManagement !== null) {
            $relatedTo = $inventoryManagement->related_to;
            //$GLOBALS['log']->fatal('relatedTo:=>' . print_r($relatedTo,1));
            if ($relatedTo === "Work Product") {
                $wpId              = $inventoryManagement->m03_work_product_im_inventory_management_1m03_work_product_ida;
                $work_product_name = $inventoryManagement->m03_work_product_im_inventory_management_1_name;
                //$GLOBALS['log']->fatal('work_product_name:=>' . $work_product_name);
                //$GLOBALS['log']->fatal('wpId:=>' . $wpId);
                foreach ($items_work_products as $item_id => $wps) {
                    $item = BeanFactory::retrieveBean("II_Inventory_Item", $item_id);
                    if ($item->related_to === "Work Product"
                        && $item->m03_work_product_ii_inventory_item_1_name !== $work_product_name
                    ) {
                        $item_errors["not_matching"][] = $item_id;
                        //$GLOBALS['log']->fatal('Not matching');
                        
                    }
                    if ($item->related_to === "Sales"
                        && $item->load_relationship("m03_work_product_ii_inventory_item_1")
                    ) {
                        $found     = false;
                        $itemWPIds = $item->m03_work_product_ii_inventory_item_1->get();
                        foreach ($itemWPIds as $itemWPId) {
                            if ($wpId === $itemWPId) {
                                $found = true;
                            }
                        }
                        if ($found === false) {
                            $item_errors["not_matching"][] = $item_id;
                        }
                    }
                }
            } else if ($relatedTo === "Sales") {
                if ($inventoryManagement->load_relationship("m03_work_product_im_inventory_management_1")) {
                    $wp_beans = $inventoryManagement->m03_work_product_im_inventory_management_1->getBeans();
                    $wp_names = array();
                    foreach ($wp_beans as $wp) {
                        $wp_names[] = $wp->name;
                    }
                    foreach ($items_work_products as $item_id => $wps) {
                        $masterWps = $wp_names;
                        $slaveWps  = $wps;

                        if (count($masterWps) > count($slaveWps)) {
                            $item_errors["not_matching"][] = $item_id;
                        } else {
                            $diff = array_diff($masterWps, $slaveWps);
                            if (count($diff) > 0) {
                                $item_errors["not_matching"][] = $item_id;
                            }
                        }
                    }
                }
            } else {
                throw new Exception('Inventory Management error. No "Related To" found!');
            }
        } else {
            throw new Exception('Inventory Management error. Please try again!');
        }
        return array("items" => $item_errors);
    }
    /**
     * Get the items of work products
     */
    private function getItemsWorkProducts($inventory_items)
    {
        $item_work_products_result = array();
        foreach ($inventory_items as $item) {
            $item_work_products       = $item->m03_work_product_ii_inventory_item_1->getBeans();
            $item_work_products_names = [];
            foreach ($item_work_products as $wp) {
                $item_work_products_names[] = $wp->name;
            }
            if ($item->related_to === "Work Product") {
                $item_work_products_names[] = $item->m03_work_product_ii_inventory_item_1_name;
            }
            $item_work_products_result[$item->id] = $item_work_products_names;
        }
        return $item_work_products_result;
    }
}
