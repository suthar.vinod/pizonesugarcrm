<?php
/**
 * Created by PhpStorm.
 * User: nabeel.alvi
 * Date: 8/17/18
 * Time: 12:42 PM
 */

class GetEmailApi extends SugarApi {
    public function registerApiRest()
    {
        return array(
            //GET
            'MyGetEmail' => array(
                //request type
                'reqType' => 'GET',

                //set authentication
                'noLoginRequired' => false,

                //endpoint path
                'path' => array('AllEmails'),

                //endpoint variables
//                'pathVars' => array('', 'data'),

                //method to call
                'method' => 'GetEmailMethod',

                //short help string to be displayed in the help documentation
                'shortHelp' => 'An example of a GET endpoint',

                //long help to be displayed in the help documentation
                'longHelp' => '',
            ),
        );
    }

    /**
     * Method to be used for my AllEmails endpoint
     */
    public function GetEmailMethod($api, $args)
    {
        // require args
//        $this->requireArgs($args, array('parent_module'));
        // Initialize the data
        $arReturn = array();
        // Get a list of the reports
        $strQuery = "
        SELECT
            email_addresses.email_address, 
            email_addr_bean_rel.bean_id, 
            CONCAT_WS(' ', contacts.first_name ,contacts.last_name) as name
            FROM
            `email_addresses`
            INNER JOIN `email_addr_bean_rel` ON email_addr_bean_rel.email_address_id = email_addresses.id
            INNER JOIN `contacts` ON contacts.id = email_addr_bean_rel.bean_id
            WHERE email_addr_bean_rel.deleted = 0 AND (email_addresses.email_address like '%@apsemail.com%' OR email_addresses.email_address like '%@namsa.com%')";
        // Execute
        $results = $GLOBALS['db']->query($strQuery);

        // Loop through
        while ($arRow = $GLOBALS['db']->fetchByAssoc($results)) {
            $arReturn[] = array(
                'id'   => $arRow["bean_id"],
                'text' => $arRow["email_address"],
                'name' => $arRow["name"],
            );
        }
        return  $arReturn ;
    }
}
