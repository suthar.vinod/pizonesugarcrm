<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once 'clients/base/api/RelateApi.php';

class PopulateInventoryItemsSubpanelsApi extends RelateApi
{
    const INVENTORY_ITEMS_RELATIONSHIP = "m03_work_product_ii_inventory_item_2";

    public function registerApiRest()
    {
        return array(
            'listRelatedRecords'      => array(
                'reqType'    => 'GET',
                'path'       => array('?', '?', 'link', 'm03_work_product_ii_inventory_item_1'),
                'pathVars'   => array('module', 'record', '', 'link_name'),
                'jsonParams' => array('filter'),
                'method'     => 'filterRelated',
                'shortHelp'  => 'Lists related records.',
                'longHelp'   => 'include/api/help/module_record_link_link_name_filter_get_help.html',
            ),
            'listRelatedRecordsCount' => array(
                'reqType'    => 'GET',
                'path'       => array('?', '?', 'link', 'm03_work_product_ii_inventory_item_1', 'count'),
                'pathVars'   => array('module', 'record', '', 'link_name', ''),
                'jsonParams' => array('filter'),
                'method'     => 'filterRelatedCount',
                'shortHelp'  => 'Counts all filtered related records.',
                'longHelp'   => 'include/api/help/module_record_link_link_name_filter_get_help.html',
            ),
        );
    }

    public function filterRelated(ServiceBase $api, array $args)
    {
        $inventoryItems            = parent::filterRelated($api, $args);
        $args["link_name"]         = self::INVENTORY_ITEMS_RELATIONSHIP;
        $inventoryItemsOTM         = parent::filterRelated($api, $args);
        $inventoryItems["records"] = array_merge($inventoryItems["records"], $inventoryItemsOTM["records"]);

        return $inventoryItems;
    }
}
