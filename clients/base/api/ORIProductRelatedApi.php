<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class ORIProductRelatedApi extends FilterApi
{
    protected function runQuery(ServiceBase $api, array $args, SugarQuery $q, array $options, SugarBean $seed = null)
    {
        if ($args["_filterProd_ProductRelatedToORI"]) {
            $department = $args["_ORIRelateddepartment"];
			if(is_array($args["_ORIRelateddepartment"])) {
				$department = implode(",", $args["_ORIRelateddepartment"]);
			} 
			$type = $args["_ORIRelatedtype"];
			//$subtype = $args["_ORIRelatedsubtype"];

			$q->where()->addRaw("prod_product.department like '%^{$department}^%' AND prod_product.type_2 = '{$type}'");
            /*if($subtype != '') {
				$q->where()->addRaw("prod_product.department like '%^{$department}^%' AND prod_product.type_2 = '{$type}' AND prod_product.subtype = '{$subtype}' ");
			}*/

            if (isset($options['id_query'])) { 
				$options['id_query']->where()->addRaw("prod_product.department like '%^{$department}^%' AND prod_product.type_2 = '{$type}' ");
				/*if($subtype != '') {
					$options['id_query']->where()->addRaw("prod_product.department like '%^{$department}^%' AND prod_product.type_2 = '{$type}' AND prod_product.subtype = '{$subtype}' ");
				}*/
            }
        }
		
        return parent::runQuery($api, $args, $q, $options, $seed);
    }
}
