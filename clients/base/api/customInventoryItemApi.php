<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class customInventoryItemApi extends SugarApi {

    public function registerApiRest() {
        return array(
            'getItemValue' => array(
                'reqType' => 'POST',
                'path' => array('getItemValue'),
                'pathVars' => array('getItemValue'),
                'method' => 'getItemValue',
                'shortHelp' => 'getItemValue for the selected ITEM',
                'longHelp' => '',
            ),
        );
    }

    public function getItemValue($api, $args) {
        $ii_id = $args["id"];
        $i = 0;
        $returnArr = array();
        foreach ($args["ii_Id"] as $recordId) {
			if($recordId != '') {
				$iiBean = BeanFactory::retrieveBean('II_Inventory_Item', $recordId);
				$returnArr[$i]['id'] = $iiBean->id;
				$returnArr[$i]['name'] = $iiBean->name;
				$storageMedium = str_replace("^", "", $iiBean->allowed_storage_medium);
				$storageCondition = str_replace("^", "", $iiBean->allowed_storage_conditions);
				$returnArr[$i]['allowedStorageMedium'] = $storageMedium;
				$returnArr[$i]['allowedStorageCondition'] = $storageCondition;
				$i++;
			}
        }
        return $returnArr;
    }

}
