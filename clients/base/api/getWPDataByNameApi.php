<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class getWPDataByNameApi extends SugarApi {

    public function registerApiRest() {
        return array(
            'getWPDataByName' => array(
                'reqType' => 'POST',
                'noLoginRequired' => false,
                'path' => array('getWPDataByName'),
                'pathVars' => array('getWPDataByName'),
                'method' => 'getWPDataByName',
                'shortHelp' => '',
                'longHelp' => '',
            ),
        );
    }

    
    function getWPDataByName($api, $args) {
		global $db,$current_user;
		 
		$module		= $args['module'];
		$lastwp_name	= $args['lastwp_name'];
		//$GLOBALS['log']->fatal(" lastwp_name ".$lastwp_name);
		$queryWPC = "SELECT WPC.m03_work_product_code_id1_c AS WPCode,WPCode.name AS WPCodeName
					FROM m03_work_product as WP 
					INNER JOIN m03_work_product_cstm as WPC ON WP.id = WPC.id_c 
					LEFT JOIN m03_work_product_code AS WPCode
					on WPC.m03_work_product_code_id1_c=WPCode.id
					where WP.name = '".$lastwp_name."' AND WP.deleted=0 LIMIT 1";
		//$GLOBALS['log']->fatal(" queryWPC ".$queryWPC);
		$resultAllRecord = $db->query($queryWPC);
		$rowAllRecord = $db->fetchByAssoc($resultAllRecord);
		//$GLOBALS['log']->fatal(" queryWPC =====".$rowAllRecord['WPCodeName']);
		return $rowAllRecord['WPCodeName'];
    }
}