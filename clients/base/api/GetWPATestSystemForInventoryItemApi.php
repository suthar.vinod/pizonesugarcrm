<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class GetWPATestSystemForInventoryItemApi extends SugarApi {

    public function registerApiRest()
	{
		return array(
			'get_wpa_test_system' => array(
				'reqType' => 'POST',
				'path' => array('get_wpa_test_system'),
				'pathVars' => array('get_wpa_test_system'),
				'method' => 'get_wpa_test_system',
				'shortHelp' => 'get wpa test system for the selected WP in II',
				'longHelp' => '',
			),

			'get_wpa_test_system_search' => array(
				'reqType' => 'POST',
				'path' => array('get_wpa_test_system_search'),
				'pathVars' => array('get_wpa_test_system_search'),
				'method' => 'get_wpa_test_system_search',
				'shortHelp' => 'get wpa test system for the selected WP in II',
				'longHelp' => '',
			),
		);
	}

	public function get_wpa_test_system_search($api, $args)
	{
		global $db;

		//Current Work Product Id
		$workProductID = $args['workProduct'];
		$searchText = $args['searchText'];
		$record_count = $args['record_count'];
		$relatedFrom = $args['relatedFrom'];
		//$GLOBALS['log']->fatal('record_count '.$record_count);

		//$GLOBALS['log']->fatal('args '.print_r($args,1));

		if ($record_count == 0) {
			$LIMITOFFSET = 'LIMIT 20';
		} else {
			$LIMITOFFSET = 'LIMIT 20 OFFSET ' . $record_count;
		}

		//$GLOBALS['log']->fatal('LIMITOFFSET ' . $LIMITOFFSET);
		//Final output array
		$data = array();
		$records = array();


		$sqlWPA_TS = '(SELECT  TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida AS TSID, 
					TS.name AS TsName,TSC.usda_id_c AS usda_id_c  

                    FROM m03_work_product_wpe_work_product_enrollment_1_c WPWPA1

                    LEFT JOIN anml_animals_wpe_work_product_enrollment_1_c AS TSWPA 
                    ON WPWPA1.m03_work_p9bf5ollment_idb = TSWPA.anml_anima9941ollment_idb

                    LEFT JOIN anml_animals AS TS ON TS.id = TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida

                    LEFT JOIN anml_animals_cstm AS TSC ON TS.id = TSC.id_c

                    LEFT JOIN m03_work_product_cstm AS WPC 
                    ON WPC.id_c = WPWPA1.m03_work_p7d13product_ida
					
					LEFT JOIN s_species AS SP 
					ON SP.id = TSC.s_species_id_c

                    where m03_work_p7d13product_ida="' . $workProductID . '" and WPWPA1.deleted=0 AND (TS.name Like "%' . $searchText . '%" || TSC.usda_id_c Like "%' . $searchText . '%") GROUP BY TSID)
                            
                    UNION  
                            
                    (SELECT  TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida AS TSID, 
					TS.name AS TsName,TSC.usda_id_c AS usda_id_c
                    FROM m03_work_product_wpe_work_product_enrollment_2_c WPWPA2

                    LEFT JOIN anml_animals_wpe_work_product_enrollment_1_c AS TSWPA 
                    ON WPWPA2.m03_work_p90c4ollment_idb = TSWPA.anml_anima9941ollment_idb

                    LEFT JOIN anml_animals AS TS ON TS.id = TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida

                    LEFT JOIN anml_animals_cstm AS TSC ON TS.id = TSC.id_c

                    LEFT JOIN m03_work_product_cstm AS WPC 
                    ON WPC.id_c = WPWPA2.m03_work_p9f23product_ida
					
					LEFT JOIN s_species AS SP 
					ON SP.id = TSC.s_species_id_c

                    where m03_work_p9f23product_ida="' . $workProductID . '" and WPWPA2.deleted=0 AND (TS.name Like "%' . $searchText . '%" || TSC.usda_id_c Like "%' . $searchText . '%") GROUP BY TSID)
					
					
					UNION

					(SELECT  TS.id AS TSID, 
					TS.name AS TsName,TSC.usda_id_c AS usda_id_c FROM anml_animals TS
								 
					LEFT JOIN anml_animals_cstm AS TSC ON TS.id = TSC.id_c

					LEFT JOIN s_species AS SP 
					ON SP.id = TSC.s_species_id_c

					LEFT JOIN m03_work_product_cstm AS WPC 
					ON SP.name = WPC.test_system_c      
						  
					where WPC.id_c="' . $workProductID . '" and TS.deleted=0 AND (TS.name Like "%' . $searchText . '%" || TSC.usda_id_c Like "%' . $searchText . '%") GROUP BY TSID) ' . $LIMITOFFSET;


		//$GLOBALS['log']->fatal('rowWPA_TS sql Seacrh' . $sqlWPA_TS);
		$resultWPA_TS = $db->query($sqlWPA_TS);
		while ($rowWPA_TS = $db->fetchByAssoc($resultWPA_TS)) {
			//$GLOBALS['log']->fatal('rowWPA_TS '.print_r($rowWPA_TS,1));
			if ($rowWPA_TS['TsName'] != '') {
				$records[] = array(
					"id" => $rowWPA_TS['TSID'],
					"name" => $rowWPA_TS['TsName'],
					"usda_id_c" => $rowWPA_TS['usda_id_c'],
				);
			}
		}



		$data  = array("records" => $records);

		return $data;
	}

    public function get_wpa_test_system($api, $args) {
        global $db;

        //Current Work Product Id
        $workProductID = $args['workProduct'];
        $record_count = $args['record_count'];
        $relatedFrom = $args['relatedFrom'];
		//$GLOBALS['log']->fatal('record_count '.$record_count);
		
		//$GLOBALS['log']->fatal('args '.print_r($args,1));
		 
		if($record_count == 'all' && $relatedFrom == "single_testsystem"){
			$LIMITOFFSET = '';
		}else{
			if($record_count == 0){
				$LIMITOFFSET = 'LIMIT 20';
			}else{
				$LIMITOFFSET = 'LIMIT 20 OFFSET '.$record_count;
			}
		}
 
 		//$GLOBALS['log']->fatal('LIMITOFFSET '.$LIMITOFFSET);
        //Final output array
        $data = array();
        $records = array();
 

        $sqlWPA_TS='(SELECT  TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida AS TSID, 
					TS.name AS TsName,TSC.usda_id_c AS usda_id_c  

                    FROM m03_work_product_wpe_work_product_enrollment_1_c WPWPA1

                    LEFT JOIN anml_animals_wpe_work_product_enrollment_1_c AS TSWPA 
                    ON WPWPA1.m03_work_p9bf5ollment_idb = TSWPA.anml_anima9941ollment_idb

                    LEFT JOIN anml_animals AS TS ON TS.id = TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida

                    LEFT JOIN anml_animals_cstm AS TSC ON TS.id = TSC.id_c

                    LEFT JOIN m03_work_product_cstm AS WPC 
                    ON WPC.id_c = WPWPA1.m03_work_p7d13product_ida
					
					LEFT JOIN s_species AS SP 
					ON SP.id = TSC.s_species_id_c

                    where m03_work_p7d13product_ida="'.$workProductID.'" and WPWPA1.deleted=0 GROUP BY TSID)
                            
                    UNION  
                            
                    (SELECT  TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida AS TSID, 
					TS.name AS TsName,TSC.usda_id_c AS usda_id_c
                    FROM m03_work_product_wpe_work_product_enrollment_2_c WPWPA2

                    LEFT JOIN anml_animals_wpe_work_product_enrollment_1_c AS TSWPA 
                    ON WPWPA2.m03_work_p90c4ollment_idb = TSWPA.anml_anima9941ollment_idb

                    LEFT JOIN anml_animals AS TS ON TS.id = TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida

                    LEFT JOIN anml_animals_cstm AS TSC ON TS.id = TSC.id_c

                    LEFT JOIN m03_work_product_cstm AS WPC 
                    ON WPC.id_c = WPWPA2.m03_work_p9f23product_ida
					
					LEFT JOIN s_species AS SP 
					ON SP.id = TSC.s_species_id_c

                    where m03_work_p9f23product_ida="'.$workProductID.'" and WPWPA2.deleted=0 GROUP BY TSID)
					
					
					UNION

					(SELECT  TS.id AS TSID, 
					TS.name AS TsName,TSC.usda_id_c AS usda_id_c FROM anml_animals TS
								 
					LEFT JOIN anml_animals_cstm AS TSC ON TS.id = TSC.id_c

					LEFT JOIN s_species AS SP 
					ON SP.id = TSC.s_species_id_c

					LEFT JOIN m03_work_product_cstm AS WPC 
					ON SP.name = WPC.test_system_c      
						  
					where WPC.id_c="'.$workProductID.'" and TS.deleted=0 GROUP BY TSID) '.$LIMITOFFSET;  


        //$GLOBALS['log']->fatal('rowWPA_TS sql '.$sqlWPA_TS);
        $resultWPA_TS = $db->query($sqlWPA_TS);
        while ($rowWPA_TS = $db->fetchByAssoc($resultWPA_TS)) {
			//$GLOBALS['log']->fatal('rowWPA_TS '.print_r($rowWPA_TS,1));
			if($rowWPA_TS['TsName'] != ''){
				$records[] = array(
					"id" => $rowWPA_TS['TSID'],
					"name" => $rowWPA_TS['TsName'],
					"usda_id_c" => $rowWPA_TS['usda_id_c'],
				);
			}
        }
 
        

        $data  = array("records" => $records);

        return $data;
    }

}
?>
