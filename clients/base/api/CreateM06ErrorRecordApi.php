<?php

class CreateM06ErrorRecordApi extends SugarApi
{

	public function registerApiRest()
	{
		return array(
			//GET
			'MyCreateM06ErrorRecord' => array(
				'reqType'			=> 'POST',
				'noLoginRequired'	=> false,
				'path'				=> array('createM06ErrorRecord'),
				'pathVars'			=> array('createM06ErrorRecord'),
				'method'			=> 'createM06ErrorRecord',
				'shortHelp'			=> '',
				'longHelp'			=> '',
			),
		);
	}

	/**
	 * Method to be used for my ObservationName endpoint
	 */
	public function createM06ErrorRecord($api, $args)
	{
		//$GLOBALS['log']->fatal('220 comm create args ' . print_r($args,1));
		//$GLOBALS['log']->fatal(' comm create  ');
		global $db;
		$returnData = array();
		$dataReturn = "";
		$errorCategory			= $args['error_category_c'];
		$errorType			    = $args['error_type_c'];
		$related_data			= $args['related_data'];
		$workProductArr			= $args['work_products'];
		$testSystemArr			= $related_data['ANML_Animals'];
		$erdDocArr				= $related_data['Erd_Error_Documents'];
		$equipmentArr			= $related_data['Equip_Equipment'];
		$roomsArr				= $related_data['RMS_Room'];
		$TeamArr				= $related_data['Teams'];
		$m06ErrorArr			= $related_data['M06_Error'];
		$a1aArr					= $related_data['A1A_Critical_Phase_Inspectio'];
		$BID_Batch_ID			= $related_data['BID_Batch_ID'];
		 
		if ($errorCategory == 'Work Product Schedule Outcome') {
			$commId = array();
			if ($errorType == 'Standard Biocomp TS Selection') {
				//$GLOBALS['log']->fatal(' comm create type '.$errorType);
				foreach ($args['testSystemAndUsdaIdData'] as $testSystemAndUsdaIdData) {
					$relatedData['ANML_Animals']		= array($testSystemAndUsdaIdData['testSystemValue'][0]['id']);
					$relatedData['Work_Product']		= $workProductArr;
					$relatedData['Erd_Error_Documents']	= $erdDocArr;
					$relatedData['Equip_Equipment']		= $equipmentArr;
					$relatedData['RMS_Room']			= $roomsArr;
					$relatedData['Teams']				= $TeamArr;
					$relatedData['M06_Error']			= $m06ErrorArr;
					$relatedData['A1A_Critical']		= $a1aArr;
					$relatedData['BID_Batch_ID']		= $BID_Batch_ID;

					$testSystemId 						= $testSystemAndUsdaIdData['testSystemValue'][0]['id'];

					$Tsquery 	= "SELECT name FROM anml_animals WHERE id = '" . $testSystemId . "'";
					$Tsresult 	= $db->query($Tsquery);
					$Tsrows 	= $db->fetchByAssoc($Tsresult);

					$TS_usda_id 				= $testSystemAndUsdaIdData['usdaIdValue'];
					$args['multiple_usda_id'] 	= $TS_usda_id;
 
					//Create Comm records
					$returnData 	= $this->createComRecordMethod($args, $relatedData, $Tsrows['name']);
					//WPA autocreate records
					 
					$this->autoCreateWPARecords($testSystemId, $workProductArr,$BID_Batch_ID);
					 
					$returnData12 	= json_decode($returnData);
					array_push($commId, $returnData12->id);
					$dataReturn .= $returnData . ','; 
				}
			} else {
				for ($ts = 0; $ts < count($testSystemArr); $ts++) {

					$relatedData['ANML_Animals']		= array($testSystemArr[$ts]);
					$relatedData['Work_Product']		= $workProductArr[0];
					$relatedData['Erd_Error_Documents']	= $erdDocArr;
					$relatedData['Equip_Equipment']		= $equipmentArr;
					$relatedData['RMS_Room']			= $roomsArr;
					$relatedData['Teams']				= $TeamArr;
					$relatedData['M06_Error']			= $m06ErrorArr;
					$relatedData['A1A_Critical']		= $a1aArr;
					$relatedData['BID_Batch_ID']		= $BID_Batch_ID;

					$Tsquery = "SELECT name FROM anml_animals WHERE id = '" . $testSystemArr[$ts] . "'";
					$Tsresult = $db->query($Tsquery);
					$Tsrows = $db->fetchByAssoc($Tsresult);

					$returnData = $this->createComRecordMethod($args, $relatedData, $Tsrows['name']);

					$returnData12 = json_decode($returnData);
					array_push($commId, $returnData12->id);

					$dataReturn .= $returnData . ',';
				}
			}

			if ($args['rp_entry'] != '' || $args['rp_entry'] != null) {
				//Link Responsible Person
				$RP_args = array(
					'employee_Ids' => $args['rp_entry'],
					'commIds' => $commId,
					'rp_type' => 'Entry',
					'date_error_occurred_c' => $args['date_error_occurred_c'],
				);
				$this->createDeviationEmployee($RP_args);
			}

			if ($args['rp_review'] != '' || $args['rp_review'] != null) {
				//Link Responsible Person
				$RP_args = array(
					'employee_Ids' => $args['rp_review'],
					'commIds' => $commId,
					'rp_type' => 'Review',
					'date_error_occurred_c' => $args['date_error_occurred_c'],
				);
				$this->createDeviationEmployee($RP_args);
			}

			$dataReturn1 = rtrim($dataReturn, ',');
			$dataReturn1 = '[' . $dataReturn1 . ']';
			$TestSystem = array("TestSystem" => $dataReturn1);
			return $TestSystem;
		} elseif ($errorCategory == 'Training') {
			$commId = array();
			for ($erd = 0; $erd < count($erdDocArr); $erd++) {

				$relatedData['ANML_Animals']		= $testSystemArr;
				$relatedData['Work_Product']		= $workProductArr[0];
				$relatedData['Erd_Error_Documents']	= array($erdDocArr[$erd]);
				$relatedData['Equip_Equipment']		= $equipmentArr;
				$relatedData['RMS_Room']			= $roomsArr;
				$relatedData['Teams']				= $TeamArr;
				$relatedData['M06_Error']			= $m06ErrorArr;
				$relatedData['A1A_Critical']		= $a1aArr;
				$relatedData['BID_Batch_ID']		= $BID_Batch_ID;

				$Dcquery = "SELECT name FROM erd_error_documents WHERE id = '" . $erdDocArr[$erd] . "'";
				$Dcresult = $db->query($Dcquery);
				$Dcrows = $db->fetchByAssoc($Dcresult);

				$returnData = $this->createComRecordMethod($args, $relatedData, $Dcrows['name']);
				$returnData12 = json_decode($returnData);
				array_push($commId, $returnData12->id);

				$dataReturn .= $returnData . ',';
			}
			if ($args['rp_entry'] != '' || $args['rp_entry'] != null) {
				//Link Responsible Person
				$RP_args = array(
					'employee_Ids' => $args['rp_entry'],
					'commIds' => $commId,
					'rp_type' => 'Entry',
					'date_error_occurred_c' => $args['date_error_occurred_c'],
				);
				$this->createDeviationEmployee($RP_args);
			}

			if ($args['rp_review'] != '' || $args['rp_review'] != null) {
				//Link Responsible Person
				$RP_args = array(
					'employee_Ids' => $args['rp_review'],
					'commIds' => $commId,
					'rp_type' => 'Review',
					'date_error_occurred_c' => $args['date_error_occurred_c'],
				);
				$this->createDeviationEmployee($RP_args);
			}
			$dataReturn1 = rtrim($dataReturn, ',');
			$dataReturn1 = '[' . $dataReturn1 . ']';
			$Document = array("Document" => $dataReturn1);
			return $Document;
		} else {

			$commId = array();
			if (count($workProductArr) > 0) {
				for ($wp = 0; $wp < count($workProductArr); $wp++) {
					//$GLOBALS['log']->fatal("In condition wp greated 0 line 162");
					$relatedData['ANML_Animals']		= $testSystemArr;
					$relatedData['Work_Product']		= $workProductArr[$wp];
					$relatedData['Erd_Error_Documents']	= $erdDocArr;
					$relatedData['Equip_Equipment']		= $equipmentArr;
					$relatedData['RMS_Room']			= $roomsArr;
					$relatedData['Teams']				= $TeamArr;
					$relatedData['M06_Error']			= $m06ErrorArr;
					$relatedData['A1A_Critical']		= $a1aArr;
					$relatedData['BID_Batch_ID']		= $BID_Batch_ID;

					$Wpquery = "SELECT name FROM m03_work_product WHERE id = '" . $workProductArr[$wp] . "'";
					$Wpresult = $db->query($Wpquery);
					$Wprows = $db->fetchByAssoc($Wpresult);

					$returnData = $this->createComRecordMethod($args, $relatedData, $Wprows['name']);
					$returnData12 = json_decode($returnData);
					array_push($commId, $returnData12->id);

					$dataReturn .= $returnData . ',';
				}

				if ($args['rp_entry'] != '' || $args['rp_entry'] != null) {
					//Link Responsible Person
					$RP_args = array(
						'employee_Ids' => $args['rp_entry'],
						'commIds' => $commId,
						'rp_type' => 'Entry',
						'date_error_occurred_c' => $args['date_error_occurred_c'],
					);
					$this->createDeviationEmployee($RP_args);
				}

				if ($args['rp_review'] != '' || $args['rp_review'] != null) {
					//Link Responsible Person
					$RP_args = array(
						'employee_Ids' => $args['rp_review'],
						'commIds' => $commId,
						'rp_type' => 'Review',
						'date_error_occurred_c' => $args['date_error_occurred_c'],
					);
					$this->createDeviationEmployee($RP_args);
				}

				$dataReturn1 = rtrim($dataReturn, ',');
				$dataReturn1 = '[' . $dataReturn1 . ']';
				$WorkProduct = array("WorkProduct" => $dataReturn1);
				return $WorkProduct;
			} else {
				$relatedData['ANML_Animals']		= $testSystemArr;
				$relatedData['Work_Product']		= $workProductArr;
				$relatedData['Erd_Error_Documents']	= $erdDocArr;
				$relatedData['Equip_Equipment']		= $equipmentArr;
				$relatedData['RMS_Room']			= $roomsArr;
				$relatedData['Teams']				= $TeamArr;
				$relatedData['M06_Error']			= $m06ErrorArr;
				$relatedData['A1A_Critical']		= $a1aArr;
				$relatedData['BID_Batch_ID']		= $BID_Batch_ID;

				$LinkReocrdname = "";
				$returnData = $this->createComRecordMethod($args, $relatedData, $LinkReocrdname);
				$returnData12 = json_decode($returnData);
				array_push($commId, $returnData12->id);

				if ($args['rp_entry'] != '' || $args['rp_entry'] != null) {
					//Link Responsible Person
					$RP_args = array(
						'employee_Ids' => $args['rp_entry'],
						'commIds' => $commId,
						'rp_type' => 'Entry',
						'date_error_occurred_c' => $args['date_error_occurred_c'],
					);
					$this->createDeviationEmployee($RP_args);
				}

				if ($args['rp_review'] != '' || $args['rp_review'] != null) {
					//Link Responsible Person
					$RP_args = array(
						'employee_Ids' => $args['rp_review'],
						'commIds' => $commId,
						'rp_type' => 'Review',
						'date_error_occurred_c' => $args['date_error_occurred_c'],
					);
					$this->createDeviationEmployee($RP_args);
				}

				$dataReturn .= $returnData . ',';
				$dataReturn1 = rtrim($dataReturn, ',');
				$dataReturn1 = '[' . $dataReturn1 . ']';
				$Other = array("Other" => $dataReturn1);
				return $Other;
			}
		}
	}

	function  createComRecordMethod($args, $relatedData, $linkRecordName)
	{
		$data = array();
		global $db, $current_user;
		$commID					= $args['id'];
		$employee_id			= $args['employee_id'];
		$submitter_c			= $args['submitter_c'];
		$errorCategory			= $args['error_category_c'];
		$errorType 				= $args['error_type_c'];
		$expectedEvent			= $args['expected_event_c'];
		$actualEvent			= $args['actual_event_c'];
		$dateErrorOccurred		= $args['date_error_occurred_c'];
		$dateTimeDiscovered		= $args['date_time_discovered_c'];
		$resolutionDate			= $args['resolution_date_c'];
		$dueDate				= $args['due_date_c'];
		$dateErrorDocumented	= $args['date_error_documented_c'];
		$errorOccuredWeekend	= $args['error_occured_on_weekend_c'];
		$classification			= $args['error_classification_c'];
		$diagnosis				= $args['diagnosis_c'];
		$planCorrection			= $args['plan_for_correction_c'];
		$whyItHappened			= $args['why_it_happened_c'];
		$detailsWhyHappened		= $args['details_why_happened_c'];
		$iacucDeficiency		= $args['iacuc_deficiency_class_c'];
		$procedureType			= $args['procedure_type_c'];
		$recommendation			= $args['recommendation_c'];
		$related_data			= $args['related_data'];
		$testSystemInvolved		= $args['test_system_involved_c'];
		$rpEntryEmailDept		= $args['rp_entry_email_dept'];
		$rpReviewEmailDept		= $args['rp_review_email_dept'];
		$rpEntry				= $args['rp_entry'];
		$rpReview				= $args['rp_review'];
		$workProductArr			= $args['work_products'];
		
		$com_usda_id			= $args['com_usda_id_c'];

		$wordpress_flag 		= $args['wordpress_flag'];

		$sedated 		                = $args['sedated_c'];
		$substance_administration 		= $args['substance_administration_c'];
		$blood_collection 		        = $args['blood_collection_c'];
		$test_article_exposure 	        = $args['test_article_exposure_c'];
		$test_article_exposure_detail 	= $args['test_article_exposure_detail_c'];
		$surgical_access 		        = $args['surgical_access_c'];
		$surgical_access_details 	    = $args['surgical_access_details_c'];
		$first_procedure_date 	        = $args['first_procedure_date'];
		$multiple_usda_id 	            = $args['multiple_usda_id'];
		$twentyfour_animal_quantity 	= $args['twentyfour_animal_quantity_c'];
		$fortyeight_animal_quantity 	= $args['fortyeight_animal_quantity_c'];
		//$GLOBALS['log']->fatal('comm create multiple_usda_id ' .$multiple_usda_id);
		$activitiesArr = array();
		foreach ($args['activities_c'] as $activities) {
			$activitiesStr = "^" . $activities . "^";
			array_push($activitiesArr, $activitiesStr);
		}
		$activities = implode(',', $activitiesArr);


		$m06_error_m06_error_1_id = $args['m06_error_m06_error_1_id'];

		$arr = explode("+", $dateTimeDiscovered);
		$dateTimeDiscovered1 = $arr[0];
		$dateTimeDiscovered_c = strtotime($dateTimeDiscovered1);
		$dateTimeDiscovered = date("Y-m-d H:i:s", $dateTimeDiscovered_c);

		$testSystemLink			= $relatedData['ANML_Animals'];
		$workProductLink		= $relatedData['Work_Product'];
		$erdDocLink				= $relatedData['Erd_Error_Documents'];
		$equipmentLink			= $relatedData['Equip_Equipment'];
		$roomsLink				= $relatedData['RMS_Room'];
		$TeamLink				= $relatedData['Teams'];
		$m06ErrorLink			= $relatedData['M06_Error'];
		$a1aLink				= $relatedData['A1A_Critical'];
		$bidLink                 = $relatedData['BID_Batch_ID'];

		$commBean		= BeanFactory::newBean('M06_Error');
		$new_bean_id	= create_guid();
		$commBean->id	= $new_bean_id;
		$commBean->new_with_id = true;
		$commBean->fetched_row = null;

		$commBean->name				= $this->generateSystemId($args);
		$commBean->submitter_c		= $submitter_c;
		$commBean->error_category_c	= $errorCategory;
		$commBean->error_type_c		= $errorType;
		$commBean->actual_event_c	= $actualEvent;
		$commBean->expected_event_c	= $expectedEvent;

		$dateOccurred = '';
		if ($dateErrorOccurred != "" || $dateErrorOccurred != NULL) {
			$dateOccurred = date("Y-m-d", strtotime($dateErrorOccurred));
			//$GLOBALS['log']->fatal('220 comm create dateErrorOccurred 1==> ' . $dateOccurred);
		} else {
			if ($errorCategory == 'Work Product Schedule Outcome' && $errorType == 'Standard Biocomp TS Selection') {
				$dateOccurred = date("Y-m-d");
				//$GLOBALS['log']->fatal('220 comm create Laxsaini 4==> ' . $dateOccurred);
			} else {
				if (($dateErrorOccurred == NULL || $dateErrorOccurred == '') && ($args['date_time_discovered_c'] == NULL || $args['date_time_discovered_c'] == '')) {
					$dateOccurred = date("Y-m-d", strtotime($dateErrorDocumented));
					//$GLOBALS['log']->fatal('220 comm create dateErrorOccurred 2==> ' . $dateOccurred);
				} else if ($dateErrorOccurred == NULL || $dateErrorOccurred == '') {
					if ($args['date_time_discovered_c'] != NULL || $args['date_time_discovered_c'] != "") {
						$dateOccurred = date("Y-m-d", strtotime($dateTimeDiscovered));
						//$GLOBALS['log']->fatal('220 comm create dateErrorOccurred 3==> ' . $dateOccurred);
					} else {
						$dateOccurred = date("Y-m-d");
						//$GLOBALS['log']->fatal('220 comm create dateErrorOccurred 4==> ' . $dateOccurred);
					}
				}
			}
		}

		if ($resolutionDate != "" || $resolutionDate != NULL) {
			$dateOccurred = date("Y-m-d", strtotime($resolutionDate));
			//$GLOBALS['log']->fatal('220 comm create resolutionDate 5==> ' . $dateOccurred);
		}

		//$GLOBALS['log']->fatal('220 comm create dateErrorOccurred Final==> ' . $dateOccurred);

		if ($args['date_time_discovered_c'] != NULL || $args['date_time_discovered_c'] != '') {
			$commBean->date_time_discovered_c = $dateTimeDiscovered;
		}
		if ($dateErrorDocumented != "" || $dateErrorDocumented != NULL) {
			$commBean->date_error_documented_c = date("Y-m-d", strtotime($dateErrorDocumented));
		}
		if ($resolutionDate != "" || $resolutionDate != NULL) {
			$commBean->resolution_date_c = date("Y-m-d", strtotime($resolutionDate));
		}
		if ($dueDate != "" || $dueDate != NULL) {
			$commBean->due_date_c = date("Y-m-d", strtotime($dueDate));
		}

		$commBean->date_error_occurred_c = $dateOccurred;
		$commBean->error_occured_on_weekend_c	= $errorOccuredWeekend;
		$commBean->error_classification_c		= $classification;
		$commBean->diagnosis_c					= $diagnosis;
		$commBean->wordpress_flag				= $wordpress_flag;
		$commBean->plan_for_correction_c		= $planCorrection;
		$commBean->why_it_happened_c			= $whyItHappened;
		$commBean->details_why_happened_c		= $detailsWhyHappened;
		$commBean->iacuc_deficiency_class_c		= $iacucDeficiency;
		$commBean->procedure_type_c				= $procedureType;
		$commBean->recommendation_c				= $recommendation;
		$commBean->test_system_involved_c	    = $testSystemInvolved;
		$commBean->activities_c				    = $activities;
		$commBean->com_usda_id_c				= $com_usda_id;
		//$commBean->rp_review                  = $employee_id;
		//$commBean->rp_entry                   = $employee_id;
		$commBean->employee_id                  = $employee_id;
		$commBean->rp_entry_email_dept          = json_encode($rpEntryEmailDept);
		$commBean->rp_review_email_dept         = json_encode($rpReviewEmailDept);

		$commBean->sedated_c 		                = $sedated;
		$commBean->substance_administration_c 		= $substance_administration;
		$commBean->blood_collection_c 		        = $blood_collection;
		$commBean->test_article_exposure_c 	        = $test_article_exposure;
		$commBean->test_article_exposure_detail_c 	= $test_article_exposure_detail;
		$commBean->surgical_access_c 	            = $surgical_access;
		$commBean->surgical_access_details_c 	    = $surgical_access_details;

		$commBean->first_procedure_date             = $first_procedure_date;
		$commBean->multiple_usda_id                 = $multiple_usda_id;
		$commBean->twentyfour_animal_quantity_c     = $twentyfour_animal_quantity;
		$commBean->fortyeight_animal_quantity_c     = $fortyeight_animal_quantity;

		$commBean->related_data = array(
			"Erd_Error_Documents"	=> $erdDocLink,
			"ANML_Animals"			=> $testSystemLink,
			"Equip_Equipment"		=> $equipmentLink,
			"RMS_Room"				=> $roomsLink,
			"Teams"					=> $TeamLink,
			"M06_Error"				=> $m06ErrorLink,
			"A1A_Critical_Phase_Inspectio" => $a1aLink,
			"BID_Batch_ID" 			=> $bidLink,
		);
		//$GLOBALS['log']->fatal('comm create workProductLink ' .print_r($workProductLink,1));
		//$commBean->work_products  = '[' . $workProductLink[0] . ']';
 
		if($errorCategory == 'Work Product Schedule Outcome' && $errorType == 'Standard Biocomp TS Selection'){	
			$commBean->work_products  = '[' . implode(',',$workProductLink) . ']';
		}else{
			$commBean->work_products  = '[' . $workProductLink . ']';
		}

		
		//$GLOBALS['log']->fatal('comm create testSystemLink ' .print_r($testSystemLink,1));
		if (!empty($testSystemLink)) {
			
			foreach ($testSystemLink as $testSystemLinkId) {
				$commBean->load_relationship('m06_error_anml_animals_1');
				//$GLOBALS['log']->fatal('220 comm create testSystemLinkId ' . $testSystemLinkId);
				$commBean->m06_error_anml_animals_1->add($testSystemLinkId);
			}
		}

		$commBean->save();

		
		if (!empty($bidLink)) {
			$commBean->load_relationship('bid_batch_id_m06_error_1');
			$commBean->bid_batch_id_m06_error_1->add($bidLink);
		}
		
		if (!empty($workProductLink)) {			
			if($errorCategory == 'Work Product Schedule Outcome' && $errorType == 'Standard Biocomp TS Selection'){				
				foreach ($workProductLink as $workProductLinkId) {
					$relationid = create_guid();
					$WPsql = 'INSERT INTO m06_error_m03_work_product_1_c  (id,date_modified,deleted,m06_error_m03_work_product_1m06_error_ida,m06_error_m03_work_product_1m03_work_product_idb) 
					values("' . $relationid . '",now(),0,"' . $commBean->id . '","' . $workProductLinkId . '")';
					$db->query($WPsql);
				}
			}else{
				$commBean->load_relationship('m06_error_m03_work_product_1');
				$commBean->m06_error_m03_work_product_1->add($workProductLink);
			}
		}

		// if (!empty($workProductLink)) {
		// 	$commBean->load_relationship('m06_error_m03_work_product_1');
		// 	if($errorCategory == 'Work Product Schedule Outcome' && $errorType == 'Standard Biocomp TS Selection'){				
		// 		foreach ($workProductLink as $workProductLinkId) {
		// 			$commBean->m06_error_m03_work_product_1->add($workProductLinkId);
		// 		}
		// 	}else{
		// 		$commBean->m06_error_m03_work_product_1->add($workProductLink);
		// 	}
		// }

		if (!empty($erdDocLink)) {
			$commBean->load_relationship('m06_error_erd_error_documents_1');
			foreach ($erdDocLink as $erdDocLinkId) {
				$commBean->m06_error_erd_error_documents_1->add($erdDocLinkId);
			}
		}

		if (!empty($m06ErrorLink)) {
			$commBean->load_relationship('m06_error_m06_error_1');
			$commBean->m06_error_m06_error_1->add($m06ErrorLink);
		}

		//$GLOBALS['log']->fatal('220 comm create $equipmentLink ' . print_r($equipmentLink, 1));
		if (!empty($equipmentLink)) {
			//$commBean->load_relationship('m06_error_equip_equipment_1');
			//$commBean->m06_error_equip_equipment_1->add($equipmentLink);  
			foreach ($equipmentLink as $equipmentLinkID) {
				$relationid = create_guid();
				$eqsql = 'INSERT INTO m06_error_equip_equipment_1_c  (id,date_modified,deleted,m06_error_equip_equipment_1m06_error_ida,m06_error_equip_equipment_1equip_equipment_idb) 
				values("' . $relationid . '",now(),0,"' . $commBean->id . '","' . $equipmentLinkID . '")';
				$aa = $db->query($eqsql);
				//$GLOBALS['log']->fatal('220 comm create $equipmentLink aa ' . $eqsql . print_r($aa, 1));
			}
		}

		//$GLOBALS['log']->fatal('220 comm create $roomsLink ' . print_r($roomsLink, 1));
		if (!empty($roomsLink)) {
			//$commBean->load_relationship('m06_error_rms_room_1');
			//$commBean->m06_error_rms_room_1->add($roomsLink);
			foreach ($roomsLink as $roomsLinkID) {
				$relationid = create_guid();
				$rmsql = 'INSERT INTO m06_error_rms_room_1_c  (id,date_modified,deleted,m06_error_rms_room_1m06_error_ida,m06_error_rms_room_1rms_room_idb) 
			 	values("' . $relationid . '",now(),0,"' . $commBean->id . '","' . $roomsLinkID . '")';
				$bb = $db->query($rmsql);
				//$GLOBALS['log']->fatal('220 comm create $equipmeroomsLinkntLink bb ' . $rmsql . print_r($bb, 1));
			}
		}

		if (!empty($a1aLink)) {
			$commBean->load_relationship('a1a_critical_phase_inspectio_m06_error_1');
			$commBean->a1a_critical_phase_inspectio_m06_error_1->add($a1aLink);
		}

		$data = array("id" => $commBean->id, "name" => $commBean->name, "linkRecordName" => $linkRecordName);
		return json_encode($data);
	}

	function  autoCreateWPARecords($testSystemId, $workProductID, $bid_batch_id)
	{
		global $db;
		//Create Work Product Enrollment Record
		$WPE_Bean 				    	= BeanFactory::newBean('WPE_Work_Product_Enrollment');
		$WPE_Bean->id	        		= create_guid();
		$WPE_Bean->new_with_id 			= true;
		$WPE_Bean->fetched_row 			= null;
		$WPE_Bean->name			= "";
		$WPE_Bean->activities_c			= "^Performed Per Protocol^";
		$WPE_Bean->enrollment_status_c	= 'On Study';
		$WPE_Bean->createFromCommApp	= 1;
		$WPE_Bean->anml_animals_wpe_work_product_enrollment_1anml_animals_ida = $testSystemId;
		//$WPE_Bean->usda_id_c		 = $TS_usda_id;
	 
		if(empty($bid_batch_id)){
			$WPE_Bean->m03_work_p7d13product_ida = $workProductID[0];
			$WPE_Bean->bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida = "";

			//Get Blanket wp ID with where parent WP checked
			$wpcsql = 'SELECT wpc.m03_work_product_id_c AS blanketwp ,wpc.usda_category_multiselect_c FROM  m03_work_product_cstm AS wpc WHERE  wpc.id_c = "'.$workProductID[0].'"';
			
			$wpcresult = $db->query($wpcsql);			
			if ($wpcresult->num_rows > 0) {
				$Wpcrows = $db->fetchByAssoc($wpcresult);
				$blanketwp = $Wpcrows['blanketwp'];
			}

			$WPE_Bean->m03_work_p9f23product_ida = $blanketwp;

			//Get heighest USDA category from wp
			$usdaStr = $Wpcrows['usda_category_multiselect_c'];
			$resUsdaStr = str_replace("^","",$usdaStr);
			$resUsda_Str = explode(',',$resUsdaStr);
			
			$WPE_Bean->usda_category_c = max($resUsda_Str);
		}else{
			$WPE_Bean->m03_work_p7d13product_ida = '';
			$WPE_Bean->bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida = $bid_batch_id;
			 
			//Get Blanket ID with where parent WP checked
			$wpsql = 'SELECT bidwp.bid_batch_id_m03_work_product_1m03_work_product_idb AS wpid, wpc.m03_work_product_id_c as blanketwp, wpc.parent_work_product_c FROM bid_batch_id_m03_work_product_1_c as bidwp

			LEFT JOIN m03_work_product_cstm AS wpc ON wpc.id_c = bidwp.bid_batch_id_m03_work_product_1m03_work_product_idb
			WHERE bidwp.bid_batch_id_m03_work_product_1bid_batch_id_ida = "'.$bid_batch_id.'" AND wpc.parent_work_product_c = 1';
			$wpresult = $db->query($wpsql);
			
			if ($wpresult->num_rows > 0) {
				$Wprows = $db->fetchByAssoc($wpresult);
					$blanketwp = $Wprows['blanketwp'];
			}

			$WPE_Bean->m03_work_p9f23product_ida = $blanketwp;
			//$GLOBALS['log']->fatal('comm create blanketwp ' .$blanketwp);

			//Get heighest USDA category
			$usdasql = 'SELECT wpc.usda_category_multiselect_c FROM bid_batch_id_m03_work_product_1_c as bidwp

			LEFT JOIN m03_work_product_cstm AS wpc ON wpc.id_c = bidwp.bid_batch_id_m03_work_product_1m03_work_product_idb
			WHERE bidwp.bid_batch_id_m03_work_product_1bid_batch_id_ida = "'.$bid_batch_id.'"';
			$usdaresult = $db->query($usdasql);
			if ($usdaresult->num_rows > 0) {
				$usdaArr = array();			 
				while($usdarows = $db->fetchByAssoc($usdaresult)){
					$usda_category = $usdarows['usda_category_multiselect_c'];
					array_push($usdaArr,$usda_category);
				}
				$usdaStr = implode(',',$usdaArr);
				$resUsdaStr = str_replace("^","",$usdaStr);
				$resUsda_Str = explode(',',$resUsdaStr);
				
				$WPE_Bean->usda_category_c = max($resUsda_Str);
			}			
		}		
		$WPE_Bean->save();
		if(!empty($blanketwp)){
			$WPE_Bean->load_relationship("m03_work_product_wpe_work_product_enrollment_2");
			$WPE_Bean->m03_work_product_wpe_work_product_enrollment_2->add($blanketwp);
		}
	}

	function generateSystemId($arguments)
	{
		global $db;
		$year = null;

		$date_error_occurred_c		= strtotime($arguments['date_error_occurred_c']);
		$date_time_discovered_c		= strtotime($arguments['date_time_discovered_c']);
		$date_error_documented_c	= strtotime($arguments['date_error_documented_c']);
		//$resolution_date_c		= strtotime($bean->resolution_date_c);
		$resolution_date_c			= strtotime($arguments['resolution_date_c']);

		if ($arguments['error_category_c'] == 'Vet Check' && $arguments['error_type_c'] == 'Resolution Notification') {
			$year = date("y", $resolution_date_c);
			$capitalYear = date("Y", $resolution_date_c);
		} else if ($arguments['error_category_c'] == 'Vet Check' && $arguments['error_type_c'] == 'Repeat Issue') {
			$year = date("y", $date_error_occurred_c);
			$capitalYear = date("Y", $date_error_occurred_c);
		} else {
			//When Category = Maintenance Request 
			if (($date_error_occurred_c == NULL || $date_error_occurred_c == '') && ($date_time_discovered_c == NULL || $date_time_discovered_c == '')) {
				$year = date("y", $date_error_documented_c);
				$capitalYear = date("Y", $date_error_documented_c);
			}
			//When Category = Deceased Animal
			else if ($date_error_occurred_c == NULL || $date_error_occurred_c == '') {
				$year = date("y", $date_time_discovered_c);
				$capitalYear = date("Y", $date_time_discovered_c);
			} else {
				$year = date("y", $date_error_occurred_c); //get year of "Date Error Occured" 2 digit
				$capitalYear = date("Y", $date_error_occurred_c);
			}
		}

		if ($arguments['error_category_c'] == 'Work Product Schedule Outcome' && $arguments['error_type_c'] == 'Standard Biocomp TS Selection') {
			//$GLOBALS['log']->fatal('comm create name Laxgfgfffgfgf');
			$year = date("y"); //get year of "Date Error Occured" 2 digit
			$capitalYear = date("Y");
		}

		if ($capitalYear == 1970 || $year == 70) {
			$capitalYear = date("Y");
			$year = date("y");
		}

		//$Current_year = date("Y");
		//if ($arguments['isUpdate'] == false) {
		if ($arguments['name'] == '') {

			$query = "SELECT * FROM custom_modules_sequence WHERE module_name = 'Communications' AND sequence_year_short_name = '" . $year . "' ORDER BY id DESC LIMIT 0,1";
			$result = $db->query($query);

			if ($rows = $db->fetchByAssoc($result)) {
				$id = $rows['id'];
				$running_number = $rows['running_number'] + 1;
				$query_update = "UPDATE custom_modules_sequence SET running_number = '" . $running_number . "' WHERE id = '" . $id . "'";
				$db->query($query_update);
			} else {

				$nameCheck = "C" . $year . "%";
				$queryGetLastName = "SELECT MAX(CAST(SUBSTRING(NAME, 5, LENGTH(NAME)-4) AS UNSIGNED)) AS `name_error` FROM `m06_error` WHERE NAME LIKE '" . $nameCheck . "' and deleted = 0 ";
				$resultGetLastName = $db->query($queryGetLastName);
				if ($rowGetLastName = $db->fetchByAssoc($resultGetLastName)) {
					$lastNumber = $rowGetLastName['name_error'];
					$running_number = $lastNumber + 1;
				} else {
					$running_number = 1;
				}
				$queryInsert = "INSERT INTO `custom_modules_sequence` (`module_name`, `module_short_name`, `sequence_year`, `sequence_year_short_name`, `start_number`, `lpad_string`, `sequence_number_length`, `running_number`, `active_status`) VALUES ('Communications', 'C', '" . $capitalYear . "', '" . $year . "', '1', '0', '3', '" . $running_number . "', 1)";
				$db->query($queryInsert);
			}

			if ($year < '20') {
				$number_len = 4;
			} else {
				$number_len = 5;
			}
			$running_number = str_pad(strval($running_number), $number_len, "0", STR_PAD_LEFT);

			$code = 'C' . $year . '-' . $running_number;
			return $code;
		}
	}

	function createDeviationEmployee($args)
	{
		global $db, $current_user;
		$employee_Ids		= $args['employee_Ids']; //Comunication Employee Id
		$commIds			= $args['commIds']; //M06_Error Id
		$type				= $args['rp_type']; //Entry,Review
		$date_occurred  	= $args['date_error_occurred_c'];

		if (empty($date_occurred)) {
			$date_occurred  	= date('Y-m-d');
		}

		if (!empty($employee_Ids)) {

			foreach ($employee_Ids as $ceId) {
				$new_bean			= BeanFactory::newBean('DE_Deviation_Employees');
				$new_bean->id = create_guid();
				$new_bean->new_with_id = true;
				$new_bean->fetched_row = null;

				$new_bean->name		= $this->nameCreation($date_occurred, $ceId);
				$new_bean->ere_error_ee6cployees_ida	= $ceId;
				$new_bean->deviation_type_c 			= $type;
				$new_bean->contacts_de_deviation_employees_1contacts_ida = $ceId;
				$new_bean->save();

				$new_bean->load_relationship('m06_error_de_deviation_employees_2');
				foreach ($commIds as $commId) {
					$new_bean->m06_error_de_deviation_employees_2->add($commId);
				}
			}
		}
	}

	function nameCreation($date_occurred, $ceId)
	{

		global $db;
		$date_occurred	= strtotime($date_occurred);
		$curr_year		= date("y", $date_occurred); //get year of "Date Error Occured" 2 digit

		$query = "SELECT max(de_deviation_employees.name) AS name FROM `de_deviation_employees`
				INNER JOIN contacts_de_deviation_employees_1_c
					ON de_deviation_employees.id = contacts_de_deviation_employees_1_c.contacts_de_deviation_employees_1de_deviation_employees_idb
					AND contacts_de_deviation_employees_1_c.deleted = 0
				WHERE contacts_de_deviation_employees_1_c.contacts_de_deviation_employees_1contacts_ida = '{$ceId}'
					AND de_deviation_employees.deleted = 0 AND de_deviation_employees.name LIKE '%{$curr_year}%'
				ORDER BY contacts_de_deviation_employees_1_c.date_modified DESC;";

		$result = $db->query($query);

		if ($result->num_rows > 0) {
			$row = $db->fetchByAssoc($result);

			if (!empty($row['name'])) {
				$system_id = $row['name'];
				$system_id = explode('-', $system_id);

				$prev_year = str_split($system_id[0]);

				$system_id = str_split($system_id[1]);
				$system_id = $system_id[0] . $system_id[1] . $system_id[2] . $system_id[3];

				if ($prev_year[0] == 'D') {
					$prev_year = $prev_year[1] . $prev_year[2];
				} else {
					$prev_year = $prev_year[2] . $prev_year[3];
				}
			} else {
				$prev_year = 0;
				$system_id = 1;
			}


			if (!empty($system_id)) {
				if ($prev_year == $curr_year) {
					// we are in same year					
					$code = $this->generate_code($system_id, $curr_year);
				} else {
					$code = 'CE' . $curr_year . '-0001';
				}
			}
		} else {
			$code = 'CE' . $curr_year . '-0001';
		}

		if ($code != "") {
			return $code;
		}
	}

	function generate_code($system_id, $curr_year)
	{
		#$number = (int) substr($system_id, -4);
		$number = (int) $system_id;
		$number = $number + 1;
		$length = strlen($number);
		if ($length == 1) {
			$number = '000' . $number;
		} else if ($length == 2) {
			$number = '00' . $number;
		} else if ($length == 3) {
			$number = '0' . $number;
		}
		$code = 'CE' . $curr_year . '-' . $number;

		return $code;
	}
}
