<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class copyOrderRequest extends SugarApi
{
    public function registerApiRest()
    {

        return array(
            'generateLabels'    => array(
                'reqType'   => 'POST',
                'path' => array('copy_order_request'),
                'pathVars' => array(''),
                'method'    => 'copyOrderRequestData',
                'shortHelp' => '',
                'longHelp'  => '',
            ),
        );
    }

    public function copyOrderRequestData($api, $args)
    {
        global $current_user;
        global $timedate;

        $data         = array();
        $records      = array();
        $recordIds    = $args['recordId'];

        foreach ($recordIds as $recordId) {

            $orOldBean     = BeanFactory::retrieveBean("OR_Order_Request", $recordId);
            $orOldBean->load_relationship("or_order_request_ori_order_request_item_1");
            $relatedORI  = $orOldBean->or_order_request_ori_order_request_item_1->get();

            $orNewBean                 = BeanFactory::newBean('OR_Order_Request');
            $newORid                   = create_guid();
            $orNewBean->id             = $newORid;
            $orNewBean->new_with_id    = true;
            $orNewBean->fetched_row    = null;
            $orNewBean->request_date   = date("Y-m-d");
            $orNewBean->department     = $orOldBean->department;;
            $orNewBean->user_id_c      = $current_user->id;//$orOldBean->user_id_c;
            $orNewBean->assigned_user_id     = $current_user->id;//$orOldBean->user_id_c;
            $orNewBean->name           = $this->createORAutoName();//$orOldBean->name;
            $orNewBean->save();

            foreach ($relatedORI as $oriID) {

                $clonedBean = BeanFactory::retrieveBean('ORI_Order_Request_Item', $oriID);

                $clonedBean->load_relationship("prod_product_ori_order_request_item_1");
                $clonedBean->load_relationship("m03_work_product_ori_order_request_item_1");
                $clonedBean->load_relationship("m01_sales_ori_order_request_item_1");

                $product_id = $clonedBean->prod_product_ori_order_request_item_1->get();
                $wp_id      = $clonedBean->m03_work_product_ori_order_request_item_1->get();
                $sale_id    = $clonedBean->m01_sales_ori_order_request_item_1->get();

                $orBean = BeanFactory::newBean('ORI_Order_Request_Item');
                $bid                    = create_guid();
                $orBean->id             = $bid;
                $orBean->new_with_id    = true;
                $orBean->fetched_row    = null;
                $orBean->or_order_request_ori_order_request_item_1or_order_request_ida = $newORid;
                $orBean->request_date   = $clonedBean->request_date;;
                $orBean->status         = 'Requested';
                $orBean->department_new_c  = $clonedBean->department_new_c;;
                $orBean->date_entered   = $clonedBean->date_entered;;
                $orBean->date_modified  = $clonedBean->date_modified;;
                $orBean->description    = $clonedBean->description;;
                $orBean->owner          = $clonedBean->owner;;
                $orBean->type_2         = $clonedBean->type_2;;
                $orBean->product_name   = $clonedBean->product_name;;
                $orBean->product_id_2   = $clonedBean->product_id_2;;
                $orBean->vendor_name    = $clonedBean->vendor_name;;
                $orBean->purchase_unit  = $clonedBean->purchase_unit;;
                $orBean->cost_per_unit  = $clonedBean->cost_per_unit;;
                $orBean->required_by_date   = '';
                $orBean->related_to         = $clonedBean->related_to;;
                $orBean->product_selection  = $clonedBean->product_selection;;
                $orBean->subtype            = $clonedBean->subtype;;
                $orBean->manufacturer       = $clonedBean->manufacturer;;
                $orBean->concentration      = $clonedBean->concentration;;
                $orBean->concentration_unit = $clonedBean->concentration_unit;;
                $orBean->quality_requirement        = $clonedBean->quality_requirement;;
                $orBean->estimated_arrival_date     = '';
                $orBean->unit_quantity_requested    = $clonedBean->unit_quantity_requested;;
                $orBean->u_units_id_c    = $clonedBean->u_units_id_c;;
                $orBean->Duplicate_ORI    = 'Duplicate ORI';




                $orBean->save();
                $orBean->or_order_request_ori_order_request_item_1->add($newORid);

                $orBean->prod_product_ori_order_request_item_1->add($product_id);
                $orBean->m03_work_product_ori_order_request_item_1->add($wp_id);
                $orBean->m01_sales_ori_order_request_item_1->add($sale_id);

                $orBean->purchase_unit_2_c = $clonedBean->purchase_unit_2_c;
                $orBean->unit_quantity_c = $clonedBean->unit_quantity_c;
                $orBean->client_cost_c = $clonedBean->client_cost_c;
                $orBean->save();
            } 
            sleep(1);
        }
        return 1;
    }

    private function createORAutoName() {
		global $db;
		$current_year = date("y");
        $code = '';
        $query = "SELECT MAX(CAST(SUBSTRING(name,6,4) AS UNSIGNED)) AS or_name FROM or_order_request WHERE name LIKE 'OR{$current_year}%' and deleted = 0 ";
			$result = $db->query($query);
			$row = $db->fetchByAssoc($result);
			$system_id = $row['or_name'];
			if (!empty($system_id)) {
				$number = (int) $system_id;
				$number = $number + 1;
			}else{
				$number = 1;
			}
			
			$nextNumber  = str_pad($number,4,"0",STR_PAD_LEFT);
			$code .= 'OR'.$current_year.'-'.$nextNumber;
				
		return $code;	 

        
	}
} 