<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class Generate_IC_LabelsApi extends ModuleApi
{
    public function registerApiRest()
    {
        return array(
            'generateLabelsIC'    => array(
                'reqType'   => 'POST',
                'path'      => array('generateLabelsIC'),
                'pathVars'  => array(''),
                'method'    => 'getInvetoryICData',
                'shortHelp' => '',
                'longHelp'  => '',
            ),
	    );
    }

    public function getInvetoryICData(RestService $api, array $args): array
    {
        global $current_user;
        global $timedate;

        $data         = array();
        $records      = array();
        $templateId   = $args['templateId'];
        $inventoryCollectionIds = $args["inventorycIds"];
		
		$pdfBean          = BeanFactory::retrieveBean("PdfManager", $templateId);
        $data['bodyHtml'] = $pdfBean->body_html;
		
		
		/*Get Inventory Collection Data*/
        foreach ($inventoryCollectionIds as $ic_IdValue) {
            $inventoryCId   = $ic_IdValue['inventoryCId'];
			//$GLOBALS['log']->fatal('In PrintLabel icid '.$inventoryCId);
            $inventoryCLBean = BeanFactory::retrieveBean("IC_Inventory_Collection", $inventoryCId); //Retrieve IC Bean

            /*Get All related Inventory Items data*/
			$inventoryCLBean->load_relationship("ic_inventory_collection_ii_inventory_item_1");
			
			$allRelatedII = array();
			$iiName = "";
			
			$II_StorageCondition = array();
            $II_StorageMedium = array();
			$II_CollectionDate = array();
            $iiName = "";
            $iiSC = "";
            $iiSM = "";
            $iiCD  = "";
            $collection_time  = "";

            if ($inventoryCLBean->ic_inventory_collection_ii_inventory_item_1) {
				//$GLOBALS['log']->fatal('In ic_inventory_collection_ii_inventory_item_1_c');
                $inventoryItemBean = $inventoryCLBean->ic_inventory_collection_ii_inventory_item_1->getBeans();
                 
                foreach ($inventoryItemBean as $inventoryBean) {
                    $allRelatedII[]			= $inventoryBean->name;					
                    $collection_date 		= "";
                    $ii_SC = "";
                    $ii_SM = "";
                    if($inventoryBean->storage_condition!=""){

                        $ii_SC      = str_replace("^","",$inventoryBean->storage_condition);
                        $ii_SCArr   = explode(",",$ii_SC);
                        foreach($ii_SCArr as $sc){
                            $II_StorageCondition[]	= $GLOBALS['app_list_strings']['inventory_item_storage_condition_list'][$sc];
                        }
                    }

                    if($inventoryBean->storage_medium!=""){
                        $ii_SM      = str_replace("^","",$inventoryBean->storage_medium);
                        $ii_SMArr   = explode(",",$ii_SM);
                        foreach($ii_SMArr as $sm){
                            $II_StorageMedium[]	= $GLOBALS['app_list_strings']['inventory_item_storage_medium_list'][$sm];
                        }
                    }

                    if($inventoryBean->collection_date!=""){
						$unformattedCollectionDate	= new TimeDate();
						$formattedCollectionDate	= $unformattedCollectionDate->to_display_date_time($inventoryBean->collection_date, true, true, $current_user);
                        //$collection_date			= date("m/d/Y",strtotime($formattedCollectionDate)); 
                        if($formattedCollectionDate!="")
						{
							$collection_date			= date("m/d/Y",strtotime($formattedCollectionDate)); 
						}else{
							$collection_date			= date("m/d/Y",strtotime($inventoryBean->collection_date)); 
						}

					}else if($inventoryBean->collection_date_time!=""){
						$collectionDateObj				= new TimeDate();
						$formattedCollectionDateTime	= $collectionDateObj->to_display_date_time($inventoryBean->collection_date_time, true, true, $current_user);
						if($formattedCollectionDate!="")
						{
							$collection_datetime			= date("m/d/Y H:i:s",strtotime($formattedCollectionDateTime));
						    $collection_date				= date("m/d/Y",strtotime($formattedCollectionDateTime)); 
						    $collectionTimeArr				= explode(" ",$collection_datetime);
                            $collection_time				= $collectionTimeArr[1];// date("H:i:s",$collectionTimeArr);
						}else{
							$collection_date			= date("m/d/Y",strtotime($inventoryBean->collection_date_time)); 
						}
					} else {
                        $collection_date = '';
                    }
					$II_CollectionDate[]		= $collection_date;
			
                }
				$iiName     = implode(",",$allRelatedII);
				$iiSC       = implode(",",array_unique($II_StorageCondition));
				$iiSM       = implode(",",array_unique($II_StorageMedium));
                $iiCD_date  = implode(",",array_unique($II_CollectionDate));
                $output     = str_replace(',,', ',', $iiCD_date);
                $iiCD       = trim($output, ",");

            }
			
			$uniqueBarCode = $inventoryCLBean->internal_barcode;

            $specimenCollection	 = "specimenCollection";	
			$precision =2; 
            $inventoryCLData = array(
                "name"                  => $inventoryCLBean->name,
                "inventoryCollectionName"  => $inventoryCLBean->name,
				"uniqueId"              => $uniqueBarCode,
                "collectionDate"        => $iiCD,
                "storageCondition"      => $iiSC,
				"storageMedium"         => $iiSM,
				"iiName"				=> $iiName,
				"specimenCollection"    => $specimenCollection,
            );	
            $records[] = $inventoryCLData;
        }

        $data['records'] = array("records" => $records);

        return $data;
    }
}