<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class customORIApi extends SugarApi {

    public function registerApiRest() {
        return array(
            'getArrivalDateValue' => array(
                'reqType' => 'POST',
                'path' => array('getArrivalDateValue'),
                'pathVars' => array('getArrivalDateValue'),
                'method' => 'getArrivalDateValue',
                'shortHelp' => 'getArrivalDateValue for the selected ITEM',
                'longHelp' => '',
            ),
            'getCostPerUnit' => array(
                'reqType' => 'POST',
                'path' => array('getCostPerUnit'),
                'pathVars' => array('getCostPerUnit'),
                'method' => 'getCostPerUnit',
                'shortHelp' => 'getCostPerUnit for the selected Product',
                'longHelp' => '',
            ),
        );
    }

    public function getArrivalDateValue($api, $args) {
        $po_id = $args["po_id"];
        $poBean = BeanFactory::retrieveBean('PO_Purchase_Order', $po_id);
        return $poBean->estimated_arrival_date;
    }

    public function getCostPerUnit($api, $args) {
        $p_id = $args["p_id"];
        $pBean = BeanFactory::retrieveBean('Prod_Product', $p_id);
        if ($pBean->purchase_unit == 'Each') {
            return $pBean->cost_per_each_2;
        } else {
            return $pBean->cost_per_unit_2;
        }
    }

}
