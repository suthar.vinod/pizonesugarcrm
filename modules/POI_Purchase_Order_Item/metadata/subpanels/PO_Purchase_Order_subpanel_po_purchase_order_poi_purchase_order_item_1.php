<?php
// created: 2022-04-12 09:37:23
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STATUS',
    'width' => 10,
  ),
  'prod_product_poi_purchase_order_item_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_PROD_PRODUCT_TITLE',
    'id' => 'PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1PROD_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Prod_Product',
    'target_record_key' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
  ),
  'products_department_c' => 
  array (
    'readonly' => '1',
    'readonly_formula' => '',
    'type' => 'varchar',
    'vname' => 'LBL_PRODUCTS_DEPARTMENT',
    'width' => 10,
    'default' => true,
  ),
  'unit_quantity_ordered' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_UNIT_QUANTITY_ORDERED',
    'width' => 10,
  ),
  'cost_per_unit' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_COST_PER_UNIT',
    'currency_format' => true,
    'width' => 10,
  ),
  'order_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_ORDER_DATE',
    'width' => 10,
    'default' => true,
  ),
  'estimated_arrival_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_ESTIMATED_ARRIVAL_DATE',
    'width' => 10,
    'default' => true,
  ),
  'notes_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_NOTES',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'm01_sales_poi_purchase_order_item_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_M01_SALES_TITLE',
    'id' => 'M01_SALES_POI_PURCHASE_ORDER_ITEM_1M01_SALES_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M01_Sales',
    'target_record_key' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
  ),
  'm03_work_product_poi_purchase_order_item_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
    'id' => 'M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1M03_WORK_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M03_Work_Product',
    'target_record_key' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
  ),
);