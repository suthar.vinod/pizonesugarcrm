<?php
// created: 2021-10-21 05:06:41
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'related_to' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_RELATED_TO',
    'width' => 10,
  ),
  'status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STATUS',
    'width' => 10,
  ),
  'department' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_DEPARTMENT',
    'width' => 10,
  ),
  'owner' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_OWNER',
    'width' => 10,
  ),
  'order_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_ORDER_DATE',
    'width' => 10,
    'default' => true,
  ),
  'estimated_arrival_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_ESTIMATED_ARRIVAL_DATE',
    'width' => 10,
    'default' => true,
  ),
  'received_date' => 
  array (
    'type' => 'date',
    'readonly' => true,
    'vname' => 'LBL_RECEIVED_DATE',
    'width' => 10,
    'default' => true,
  ),
  'm01_sales_poi_purchase_order_item_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_M01_SALES_TITLE',
    'id' => 'M01_SALES_POI_PURCHASE_ORDER_ITEM_1M01_SALES_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M01_Sales',
    'target_record_key' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
  ),
  'm03_work_product_poi_purchase_order_item_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
    'id' => 'M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1M03_WORK_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M03_Work_Product',
    'target_record_key' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
  ),
);