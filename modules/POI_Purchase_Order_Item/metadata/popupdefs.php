<?php
$popupMeta = array (
    'moduleMain' => 'POI_Purchase_Order_Item',
    'varName' => 'POI_Purchase_Order_Item',
    'orderBy' => 'poi_purchase_order_item.name',
    'whereClauses' => array (
  'name' => 'poi_purchase_order_item.name',
),
    'searchInputs' => array (
  0 => 'poi_purchase_order_item_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_PROD_PRODUCT_TITLE',
    'id' => 'PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1PROD_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'name' => 'prod_product_poi_purchase_order_item_1_name',
  ),
  'UNIT_QUANTITY_ORDERED' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_UNIT_QUANTITY_ORDERED',
    'width' => 10,
    'name' => 'unit_quantity_ordered',
  ),
  'DEPARTMENT' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_DEPARTMENT',
    'width' => 10,
    'name' => 'department',
  ),
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE',
    'id' => 'PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1PO_PURCHASE_ORDER_IDA',
    'width' => 10,
    'default' => true,
    'name' => 'po_purchase_order_poi_purchase_order_item_1_name',
  ),
),
);
