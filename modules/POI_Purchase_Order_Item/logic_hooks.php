<?php

$hook_version = 1;
$hook_array = Array();
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(
    '1',
    'Before Save Change Purchase Order Item Name',
    //The PHP file where your class is located.
    'custom/modules/POI_Purchase_Order_Item/customLogicPOINameHook.php',
    //The class the method is in.
    'customLogicPOINameHook',
    //The method to call.
    'generatePOIName',
);

$hook_array['after_relationship_add'][] = array(
    1,
    'After relating a Inventory Item record resave the bean so the Received Date field is calculated',
    'custom/modules/POI_Purchase_Order_Item/CalculateRelateDate.php',
    'CalculateRelateDate',
    'calculate',
);

$hook_array['after_relationship_delete'][] = array(
    1,
    'After relating a Inventory Item record resave the bean so the Received Date field is calculated',
    'custom/modules/POI_Purchase_Order_Item/CalculateRelateDate.php',
    'CalculateRelateDate',
    'calculate',
);

$hook_array['before_save'][] = Array(
    '2',
    'Before relating a Inventory Item record resave the bean so the Received Date field is calculated',
    'custom/modules/POI_Purchase_Order_Item/CalculateRelateDate.php',
    'CalculateRelateDate',
    'calculateBeforeSave',
);
/**23 March 2022 : Bug Fixes #2292 */
$hook_array['before_save'][] = Array(
    '3',
    'Update POI Status on RI linking or poi status updation',
    'custom/modules/POI_Purchase_Order_Item/CalculatePOIStatus.php',
    'CalculatePOIStatus',
    'CalculatePOIStatusBeforeSave',
);
?>