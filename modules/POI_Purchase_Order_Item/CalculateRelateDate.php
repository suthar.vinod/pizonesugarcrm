<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class CalculateRelateDate
{

    public function calculate($bean, $event, $arguments)
    {
        $module = $arguments['module'];
        $modelId = $arguments['id'];

        $relatedModule = $arguments['related_module'];
        $relatedId = $arguments['related_id'];
        if ($module === 'POI_Purchase_Order_Item' && $relatedModule === 'RI_Received_Items') {
            $this->calcStatus($bean, $modelId, $arguments);
        } else if ($relatedModule === 'POI_Purchase_Order_Item' && $module === 'RI_Received_Items') {
            $this->calcStatus($bean, $relatedId, $arguments);
        }
    }

    public function calcStatus($bean, $id, $arguments)
    {
        global $db;
        $purchase_order_item = BeanFactory::retrieveBean('POI_Purchase_Order_Item', $id);

        $sql = "SELECT * FROM `po_purchase_order_poi_purchase_order_item_1_c` WHERE po_purchas4ee6er_item_idb='" . $id . "' AND deleted=0 order by date_modified desc limit 1";

        $result = $db->query($sql);

        $row = $db->fetchByAssoc($result);
        $PO_id = $row['po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida'];
        $purchaseOrderBean = BeanFactory::retrieveBean('PO_Purchase_Order', $PO_id);
        $POStatus = $purchaseOrderBean->status_c;
        $purchase_order_item->load_relationship('poi_purchase_order_item_ri_received_items_1');
        $relatedRI = $purchase_order_item->poi_purchase_order_item_ri_received_items_1->get();


        array_push($relatedRI, $arguments['related_id']);
        $relatedRI = array_unique($relatedRI);

        $total = 0;
        for ($a = 0; $a < count($relatedRI); $a++) {
            $RI_Received_Items = BeanFactory::retrieveBean('RI_Received_Items', $relatedRI[$a]);
            $quantity_received = $RI_Received_Items->quantity_received;
            $total = $total + $quantity_received;
        }

        $unit_quantity_ordered = $purchase_order_item->unit_quantity_ordered;
        if (($POStatus == "Pending" || $POStatus == "Cancelled") && $unit_quantity_ordered != "") {
            $purchase_order_item->status = "Pending";
            $purchase_order_item->save();
        } elseif ($POStatus == "Submitted" && $purchase_order_item->order_date != "" && count($relatedRI) <= 0) {
            $purchase_order_item->status = $purchase_order_item->status = "Ordered";
            $purchase_order_item->save();
        } elseif ($purchase_order_item->order_date != "" && $unit_quantity_ordered <= $total) {
            $purchase_order_item->status = "Inventory";
            $purchase_order_item->save();
        } elseif ($purchase_order_item->order_date != "" && count($relatedRI) > 0) {
            $purchase_order_item->status = "Partially Received";
            $purchase_order_item->save();
        }

        $this->checkPOIStatusAndSetPoStatus($purchase_order_item);
    }

    function calculateBeforeSave($bean, $event, $arguments)
    {
        global $db, $app_list_strings;
        $productsDepartmentString = $app_list_strings['department_list'];

        $products_department_c = str_replace("^", "", $bean->products_department_c);
        $products_department = explode(',', $products_department_c);
        $product_Department = '';
        for ($x = 0; $x <= count($products_department); $x++) {
            $product_Department .= $productsDepartmentString[$products_department[$x]] . ',';
        }
        $product_Department = rtrim($product_Department, ',');
        $bean->products_department_c = $product_Department;
        /**Bug fix #2324 PO Status issue : 25 April 2022 : Gsingh */
        $this->checkPOIStatusAndSetPoStatus($bean);
    }

    function checkPOIStatusAndSetPoStatus($purchase_order_item)
    {
        if ($purchase_order_item->po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida != "") {

            $poid = $purchase_order_item->po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida;

            $currentPOBean = BeanFactory::retrieveBean('PO_Purchase_Order', $poid);
            $currentPOBean->load_relationship('po_purchase_order_poi_purchase_order_item_1');
            $currentPOBean->load_relationship('po_purchase_order_ori_order_request_item_1');
            /** Load poi and ori bean to get all the linked record of poi and ori */
            $relatedPOI = $currentPOBean->po_purchase_order_poi_purchase_order_item_1->get();
            $relatedORI = $currentPOBean->po_purchase_order_ori_order_request_item_1->get();
            /**linkedrecstatus array : To store the status valye of all the linked poi and ori records  */
            $linkedrecstatus = array();

            /*find all the lined poi record and get the status value */
            foreach ($relatedPOI as $POIid) {
                $clonedBeanPOI = BeanFactory::getBean('POI_Purchase_Order_Item', $POIid);
                $poi_status = $clonedBeanPOI->status;
                if ($poi_status != 'Inventory') {
                    $linkedrecstatus[] = $poi_status;
                }
            }
            /*find all the lined ori record and get the status value */
            foreach ($relatedORI as $ORIid) {
                $clonedBeanORI = BeanFactory::getBean('ORI_Order_Request_Item', $ORIid);
                $ori_status = $clonedBeanORI->status;
                if ($ori_status != 'Inventory') {
                    $linkedrecstatus[] = $ori_status;
                }
            }
            /**25 nov 2021 : #1778 : Status field on Purchase Orders to automatically update to read only 
             * Complete if all of the ORIs and POIs linked to the PO have Status = Fully Received. */
            if ((count($linkedrecstatus) == 0) && (count($relatedPOI) != 0 || count($relatedORI) != 0)) {
                /**To store the old value of status field , we are using status_old_c field */
                $currentPOBean->status_old_c = $currentPOBean->fetched_row['status_c'];
                $currentPOBean->status_c = "Complete";
                $currentPOBean->save();
            } else {
                /**In case status valye changed from full recevied to other then it will pull the old value to status field */
                if ($currentPOBean->status_old_c != '') {
                    $currentPOBean->status_c = $currentPOBean->status_old_c;
                    $currentPOBean->save();
                }
            }
        }
    }
}
