<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_m01_sales_poi_purchase_order_item_1m01_sales_ida.php

 // created: 2021-10-19 11:28:56
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['name']='m01_sales_poi_purchase_order_item_1m01_sales_ida';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['type']='id';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['source']='non-db';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['vname']='LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['id_name']='m01_sales_poi_purchase_order_item_1m01_sales_ida';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['link']='m01_sales_poi_purchase_order_item_1';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['table']='m01_sales';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['module']='M01_Sales';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['rname']='id';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['reportable']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['side']='right';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['massupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['duplicate_merge']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['hideacl']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['audited']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1m01_sales_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_m01_sales_poi_purchase_order_item_1_name.php

 // created: 2021-10-19 11:28:58
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['audited']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['massupdate']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['hidemassupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['duplicate_merge']='enabled';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['duplicate_merge_dom_value']='1';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['merge_filter']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['reportable']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['unified_search']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['calculated']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m01_sales_poi_purchase_order_item_1_name']['vname']='LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_m03_work_product_poi_purchase_order_item_1m03_work_product_ida.php

 // created: 2021-10-19 11:29:50
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['name']='m03_work_product_poi_purchase_order_item_1m03_work_product_ida';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['type']='id';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['source']='non-db';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['vname']='LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['id_name']='m03_work_product_poi_purchase_order_item_1m03_work_product_ida';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['link']='m03_work_product_poi_purchase_order_item_1';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['table']='m03_work_product';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['module']='M03_Work_Product';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['rname']='id';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['reportable']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['side']='right';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['massupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['duplicate_merge']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['hideacl']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['audited']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1m03_work_product_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_m03_work_product_poi_purchase_order_item_1_name.php

 // created: 2021-10-19 11:29:50
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1_name']['audited']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1_name']['massupdate']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1_name']['hidemassupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1_name']['duplicate_merge']='enabled';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1_name']['duplicate_merge_dom_value']='1';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1_name']['merge_filter']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1_name']['reportable']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1_name']['unified_search']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1_name']['calculated']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['m03_work_product_poi_purchase_order_item_1_name']['vname']='LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_prod_product_poi_purchase_order_item_1prod_product_ida.php

 // created: 2021-10-19 11:30:13
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['name']='prod_product_poi_purchase_order_item_1prod_product_ida';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['type']='id';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['source']='non-db';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['vname']='LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['id_name']='prod_product_poi_purchase_order_item_1prod_product_ida';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['link']='prod_product_poi_purchase_order_item_1';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['table']='prod_product';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['module']='Prod_Product';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['rname']='id';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['reportable']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['side']='right';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['massupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['duplicate_merge']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['hideacl']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['audited']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1prod_product_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_prod_product_poi_purchase_order_item_1_name.php

 // created: 2021-10-19 11:30:14
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1_name']['audited']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1_name']['massupdate']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1_name']['hidemassupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1_name']['duplicate_merge']='enabled';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1_name']['duplicate_merge_dom_value']='1';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1_name']['merge_filter']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1_name']['reportable']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1_name']['unified_search']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1_name']['calculated']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['prod_product_poi_purchase_order_item_1_name']['vname']='LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/poi_purchase_order_item_ri_received_items_1_POI_Purchase_Order_Item.php

// created: 2021-08-04 19:45:33
$dictionary["POI_Purchase_Order_Item"]["fields"]["poi_purchase_order_item_ri_received_items_1"] = array (
  'name' => 'poi_purchase_order_item_ri_received_items_1',
  'type' => 'link',
  'relationship' => 'poi_purchase_order_item_ri_received_items_1',
  'source' => 'non-db',
  'module' => 'RI_Received_Items',
  'bean_name' => 'RI_Received_Items',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'id_name' => 'poi_purcha665cer_item_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/po_purchase_order_poi_purchase_order_item_1_POI_Purchase_Order_Item.php

// created: 2020-07-31 12:33:12
$dictionary["POI_Purchase_Order_Item"]["fields"]["po_purchase_order_poi_purchase_order_item_1"] = array (
  'name' => 'po_purchase_order_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'po_purchase_order_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'PO_Purchase_Order',
  'bean_name' => 'PO_Purchase_Order',
  'side' => 'right',
  'vname' => 'LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'id_name' => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
  'link-type' => 'one',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["po_purchase_order_poi_purchase_order_item_1_name"] = array (
  'name' => 'po_purchase_order_poi_purchase_order_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE',
  'save' => true,
  'id_name' => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
  'link' => 'po_purchase_order_poi_purchase_order_item_1',
  'table' => 'po_purchase_order',
  'module' => 'PO_Purchase_Order',
  'rname' => 'name',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida"] = array (
  'name' => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID',
  'id_name' => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
  'link' => 'po_purchase_order_poi_purchase_order_item_1',
  'table' => 'po_purchase_order',
  'module' => 'PO_Purchase_Order',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_purchaserequesttotal_c.php

 // created: 2021-08-10 21:23:15
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['duplicate_merge_dom_value']=0;
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['labelValue']='purchaserequesttotal';
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['calculated']='true';
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['formula']='multiply($cost_per_unit,$unit_quantity_ordered)';
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['enforced']='true';
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['dependency']='';
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['required_formula']='';
$dictionary['POI_Purchase_Order_Item']['fields']['purchaserequesttotal_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/m01_sales_poi_purchase_order_item_1_POI_Purchase_Order_Item.php

// created: 2020-07-31 12:35:50
$dictionary["POI_Purchase_Order_Item"]["fields"]["m01_sales_poi_purchase_order_item_1"] = array (
  'name' => 'm01_sales_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'm01_sales_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'id_name' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["m01_sales_poi_purchase_order_item_1_name"] = array (
  'name' => 'm01_sales_poi_purchase_order_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
  'link' => 'm01_sales_poi_purchase_order_item_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["m01_sales_poi_purchase_order_item_1m01_sales_ida"] = array (
  'name' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID',
  'id_name' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
  'link' => 'm01_sales_poi_purchase_order_item_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/m03_work_product_poi_purchase_order_item_1_POI_Purchase_Order_Item.php

// created: 2020-07-31 12:37:53
$dictionary["POI_Purchase_Order_Item"]["fields"]["m03_work_product_poi_purchase_order_item_1"] = array (
  'name' => 'm03_work_product_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'id_name' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["m03_work_product_poi_purchase_order_item_1_name"] = array (
  'name' => 'm03_work_product_poi_purchase_order_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
  'link' => 'm03_work_product_poi_purchase_order_item_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["m03_work_product_poi_purchase_order_item_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID',
  'id_name' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
  'link' => 'm03_work_product_poi_purchase_order_item_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/prod_product_poi_purchase_order_item_1_POI_Purchase_Order_Item.php

// created: 2020-07-31 12:39:49
$dictionary["POI_Purchase_Order_Item"]["fields"]["prod_product_poi_purchase_order_item_1"] = array (
  'name' => 'prod_product_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'prod_product_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'Prod_Product',
  'bean_name' => 'Prod_Product',
  'side' => 'right',
  'vname' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'id_name' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
  'link-type' => 'one',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["prod_product_poi_purchase_order_item_1_name"] = array (
  'name' => 'prod_product_poi_purchase_order_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_PROD_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
  'link' => 'prod_product_poi_purchase_order_item_1',
  'table' => 'prod_product',
  'module' => 'Prod_Product',
  'rname' => 'name',
);
$dictionary["POI_Purchase_Order_Item"]["fields"]["prod_product_poi_purchase_order_item_1prod_product_ida"] = array (
  'name' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE_ID',
  'id_name' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
  'link' => 'prod_product_poi_purchase_order_item_1',
  'table' => 'prod_product',
  'module' => 'Prod_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_received_date.php

 // created: 2020-11-17 07:04:28
$dictionary['POI_Purchase_Order_Item']['fields']['received_date']['required']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['received_date']['readonly']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['received_date']['massupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['received_date']['hidemassupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['received_date']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_cost_per_unit.php

 // created: 2021-07-30 04:58:50
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['required']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['name']='cost_per_unit';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['vname']='LBL_COST_PER_UNIT';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['type']='currency';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['massupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['no_default']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['comments']='';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['help']='';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['importable']='false';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['duplicate_merge']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['duplicate_merge_dom_value']='0';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['audited']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['reportable']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['unified_search']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['merge_filter']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['pii']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['default']='';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['calculated']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['len']=26;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['size']='20';
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['enable_range_search']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['precision']=6;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['hidemassupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['convertToBase']=true;
$dictionary['POI_Purchase_Order_Item']['fields']['cost_per_unit']['showTransactionalAmount']=true;

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_order_date.php

 // created: 2020-12-18 14:10:19
$dictionary['POI_Purchase_Order_Item']['fields']['order_date']['massupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['order_date']['hidemassupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['order_date']['importable']='false';
$dictionary['POI_Purchase_Order_Item']['fields']['order_date']['duplicate_merge']='disabled';
$dictionary['POI_Purchase_Order_Item']['fields']['order_date']['duplicate_merge_dom_value']='0';
$dictionary['POI_Purchase_Order_Item']['fields']['order_date']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_name.php

 // created: 2020-10-09 08:51:27
$dictionary['POI_Purchase_Order_Item']['fields']['name']['required']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['name']['unified_search']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['name']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_status.php

 // created: 2020-11-10 11:07:35
$dictionary['POI_Purchase_Order_Item']['fields']['status']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/poi_purchase_order_item_ii_inventory_item_1_POI_Purchase_Order_Item.php

// created: 2021-11-09 09:48:27
$dictionary["POI_Purchase_Order_Item"]["fields"]["poi_purchase_order_item_ii_inventory_item_1"] = array (
  'name' => 'poi_purchase_order_item_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'poi_purchase_order_item_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'id_name' => 'poi_purcha8945er_item_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_a_subtype.php

 // created: 2021-11-09 07:23:36
$dictionary['POI_Purchase_Order_Item']['fields']['subtype']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_products_department_c.php

 // created: 2021-12-14 06:07:50
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['duplicate_merge_dom_value']=0;
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['labelValue']='Product\'s Department(s)';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['calculated']='true';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['formula']='related($prod_product_poi_purchase_order_item_1,"department")';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['enforced']='true';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['dependency']='';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['required_formula']='';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['readonly']='1';
$dictionary['POI_Purchase_Order_Item']['fields']['products_department_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_products_name_c.php

 // created: 2022-02-22 08:13:28
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['duplicate_merge_dom_value']=0;
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['labelValue']='Product\'s Name';
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['calculated']='true';
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['formula']='related($prod_product_poi_purchase_order_item_1,"name")';
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['enforced']='true';
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['dependency']='';
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['required_formula']='';
$dictionary['POI_Purchase_Order_Item']['fields']['products_name_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_subtype.php

 // created: 2022-03-23 12:40:32
$dictionary['POI_Purchase_Order_Item']['fields']['subtype']['hidemassupdate']=false;
$dictionary['POI_Purchase_Order_Item']['fields']['subtype']['visibility_grid']=array (
  'trigger' => 'type_2',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Balloon Catheter' => 
    array (
      0 => '',
      1 => 'Angioplasty Balloon',
      2 => 'Cutting Balloon',
      3 => 'Drug Coated Balloon',
      4 => 'Other',
      5 => 'OTW over the wire',
      6 => 'POBA Balloon',
      7 => 'PTA Balloon',
    ),
    'Cannula' => 
    array (
      0 => '',
      1 => 'Aortic Root Cannula',
      2 => 'Arterial Cannula',
      3 => 'Other',
      4 => 'Pediatric Cannula',
      5 => 'Venous Cannula',
      6 => 'Vessel Cannula',
    ),
    'Catheter' => 
    array (
      0 => '',
      1 => 'Access Catheter',
      2 => 'Butterfly Catheter',
      3 => 'Central Venous Catheter',
      4 => 'Diagnostic Catheter',
      5 => 'EP Catheter',
      6 => 'Extension Catheter',
      7 => 'Foley Catheter',
      8 => 'Guide Catheter',
      9 => 'Imaging Catheter',
      10 => 'IV Catheter',
      11 => 'Mapping Catheter',
      12 => 'Micro Catheter',
      13 => 'Mila Catheter',
      14 => 'Other',
      15 => 'Pressure Catheter',
      16 => 'Support Catheter',
      17 => 'Transseptal Catheter',
    ),
    'Chemical' => 
    array (
      0 => '',
      1 => 'Analytes',
      2 => 'Control Matrix',
      3 => 'Corrosive Solution',
      4 => 'Flammable',
      5 => 'Flammable Solution',
      6 => 'Non Flammable',
      7 => 'Solvents Aqueous',
      8 => 'Solvents Organic',
      9 => 'Toxic Solution',
    ),
    'Cleaning Agent' => 
    array (
    ),
    'Contrast' => 
    array (
      0 => '',
      1 => 'Barium Contrast',
      2 => 'Iodinated Contrast',
      3 => 'Other',
    ),
    'Drug' => 
    array (
      0 => '',
      1 => 'Inhalant',
      2 => 'Injectable',
      3 => 'Intranasal',
      4 => 'IV Solution',
      5 => 'Oral',
      6 => 'Other',
      7 => 'Topical',
    ),
    'Equipment' => 
    array (
      0 => '',
      1 => 'GCMS Columns',
      2 => 'GCMS Parts',
      3 => 'ICPMS Parts',
      4 => 'LCMS Columns',
      5 => 'LCMS Parts',
      6 => 'Machine',
      7 => 'Other',
      8 => 'Replacement Part',
      9 => 'Software',
    ),
    'Graft' => 
    array (
      0 => '',
      1 => 'Coated',
      2 => 'ePTFE',
      3 => 'Knitted',
      4 => 'Other',
      5 => 'Patches',
      6 => 'Woven',
    ),
    'Inflation Device' => 
    array (
      0 => '',
      1 => 'High Pressure',
      2 => 'Other',
    ),
    'Reagent' => 
    array (
      0 => '',
      1 => 'Flammable',
      2 => 'Non Flammable',
    ),
    'Sheath' => 
    array (
      0 => '',
      1 => 'Introducer Sheath',
      2 => 'Other',
      3 => 'Peel Away',
      4 => 'Steerable Sheath',
    ),
    'Solution' => 
    array (
    ),
    'Stent' => 
    array (
      0 => '',
      1 => 'Bare Metal Stent',
      2 => 'Drug Coated Stent',
      3 => 'Other',
      4 => 'Peel Away',
    ),
    'Suture' => 
    array (
      0 => '',
      1 => 'Ethibond',
      2 => 'Ethilon',
      3 => 'MonoAdjustable Loop',
      4 => 'Monoderm',
      5 => 'Monocryl',
      6 => 'Other',
      7 => 'PDO',
      8 => 'PDS',
      9 => 'Pledget',
      10 => 'Prolene',
      11 => 'Silk',
      12 => 'Ti Cron',
      13 => 'Vicryl',
    ),
    'Wire' => 
    array (
      0 => '',
      1 => 'Exchange Wire',
      2 => 'Guide Wire',
      3 => 'Marking Wire',
      4 => 'Other',
      5 => 'Support Wire',
    ),
    'Autoclave supplies' => 
    array (
    ),
    'Bandaging' => 
    array (
      0 => '',
      1 => 'Dressing',
      2 => 'Sub layer wrap',
      3 => 'Tape',
      4 => 'Top layer wrap',
      5 => 'Other',
    ),
    'Blood draw supply' => 
    array (
      0 => '',
      1 => 'Cryovial',
      2 => 'Extension line',
      3 => 'Injection port',
      4 => 'Needle',
      5 => 'Needless port',
      6 => 'Other',
      7 => 'Syringe',
      8 => 'Vacutainer',
    ),
    'Cleaning Supplies' => 
    array (
      0 => '',
      1 => 'Bottle',
      2 => 'Brush',
      3 => 'Floor scrubber supplies',
      4 => 'Other',
      5 => 'Trash bag',
      6 => 'SpongePad',
    ),
    'Endotracheal TubeSupplies' => 
    array (
      0 => '',
      1 => 'Adaptor',
      2 => 'Breathing circuit',
      3 => 'Brush',
      4 => 'ET Tube',
      5 => 'Other',
      6 => 'Stylet',
    ),
    'Enrichment Toy' => 
    array (
    ),
    'ETO supplies' => 
    array (
    ),
    'Feeding supplies' => 
    array (
      0 => '',
      1 => 'Enrichment treats',
      2 => 'Feeding Tube',
      3 => 'Other',
      4 => 'Primary diet',
      5 => 'Supplement diet',
    ),
    'Office Supply' => 
    array (
      0 => '',
      1 => 'Binders and supplies',
      2 => 'Label',
      3 => 'Laminating Sheets',
      4 => 'Other',
      5 => 'Paper',
    ),
    'Other' => 
    array (
      0 => '',
      1 => 'Contrast Spike',
      2 => 'Dressing',
      3 => 'Essentials Kit',
      4 => 'Invasive Blood Pressure Supplies',
      5 => 'Other',
      6 => 'Perc Needle',
      7 => 'Probe Cover',
      8 => 'Stopcock',
      9 => 'Torque Device',
      10 => 'Tuohy',
    ),
    'Shipping supplies' => 
    array (
      0 => '',
      1 => 'CD case',
      2 => 'Container',
      3 => 'Cold Pack',
      4 => 'Corrugated box',
      5 => 'Dry Ice',
      6 => 'Indicator label',
      7 => 'Insulated shipper',
      8 => 'Other',
      9 => 'Padding',
      10 => 'Pallet',
      11 => 'Tape',
      12 => 'Wrap',
    ),
    'Surgical Instrument' => 
    array (
    ),
    'Surgical site prep' => 
    array (
      0 => '',
      1 => 'Incision site cover',
      2 => 'Other',
      3 => 'Surgical scrub',
    ),
    'Tubing' => 
    array (
      0 => '',
      1 => 'Adaptor',
      2 => 'Connector',
      3 => 'Extension Tubing',
      4 => 'High Pressure Tubing',
      5 => 'Other',
      6 => 'Reducer',
      7 => 'WYE',
    ),
    'Bypass supply' => 
    array (
      0 => '',
      1 => 'Hemoconcentrator',
      2 => 'Other',
      3 => 'Oxygenator',
      4 => 'Pump pack',
      5 => 'Reservoir',
      6 => 'Tubing',
    ),
    'PPE supply' => 
    array (
      0 => '',
      1 => 'Boots',
      2 => 'Bouffanthair net',
      3 => 'Coveralloverall',
      4 => 'Ear protection',
      5 => 'Face shield',
      6 => 'Gloves non sterile',
      7 => 'Gloves sterile',
      8 => 'Mask',
      9 => 'Other',
      10 => 'Safety glasses',
      11 => 'Shoe covers',
      12 => 'Surgical gown non sterile',
      13 => 'Surgical gown sterile',
    ),
    'Control Matrix' => 
    array (
      0 => '',
      1 => 'Fluid',
      2 => 'Other',
      3 => 'Tissue',
    ),
    'Interventional Supplies' => 
    array (
      0 => '',
      1 => 'Other',
    ),
    'Surgical Supplies' => 
    array (
      0 => '',
      1 => 'Aortic Punch',
      2 => 'Biopsy Punch',
      3 => 'Blade',
      4 => 'Cautery Supply',
      5 => 'CT supply',
      6 => 'Drainage supply',
      7 => 'Drape',
      8 => 'ECG supply',
      9 => 'Gauze',
      10 => 'Hand Scrub',
      11 => 'Other',
      12 => 'Patient Drape',
      13 => 'SharpsBiohazard',
      14 => 'Sponge',
      15 => 'Table drape',
    ),
    'Glassware' => 
    array (
      0 => '',
      1 => 'Beaker',
      2 => 'Beakers',
      3 => 'Bottle',
      4 => 'Consumable',
      5 => 'Flask',
      6 => 'Grade A Glassware',
      7 => 'Other',
    ),
    'Plastic Ware' => 
    array (
      0 => '',
      1 => 'Beaker',
      2 => 'Bottle',
      3 => 'Consumable',
      4 => 'Flask',
      5 => 'Other',
      6 => 'Pipette Tips',
      7 => 'Tubes',
      8 => 'Vial Closures',
    ),
    'Lab Supplies' => 
    array (
      0 => '',
      1 => 'Misc',
      2 => 'Storage container',
      3 => 'Storage Racks',
    ),
  ),
);
$dictionary['POI_Purchase_Order_Item']['fields']['subtype']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Vardefs/sugarfield_notes_c.php

 // created: 2022-04-12 09:28:16
$dictionary['POI_Purchase_Order_Item']['fields']['notes_c']['labelValue']='Notes';
$dictionary['POI_Purchase_Order_Item']['fields']['notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['POI_Purchase_Order_Item']['fields']['notes_c']['enforced']='';
$dictionary['POI_Purchase_Order_Item']['fields']['notes_c']['dependency']='';
$dictionary['POI_Purchase_Order_Item']['fields']['notes_c']['required_formula']='';
$dictionary['POI_Purchase_Order_Item']['fields']['notes_c']['readonly_formula']='';

 
?>
