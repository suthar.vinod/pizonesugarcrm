<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Layoutdefs/poi_purchase_order_item_ri_received_items_1_POI_Purchase_Order_Item.php

 // created: 2021-10-19 09:22:27
$layout_defs["POI_Purchase_Order_Item"]["subpanel_setup"]['poi_purchase_order_item_ri_received_items_1'] = array (
  'order' => 100,
  'module' => 'RI_Received_Items',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'get_subpanel_data' => 'poi_purchase_order_item_ri_received_items_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Layoutdefs/poi_purchase_order_item_ii_inventory_item_1_POI_Purchase_Order_Item.php

 // created: 2021-11-09 09:48:27
$layout_defs["POI_Purchase_Order_Item"]["subpanel_setup"]['poi_purchase_order_item_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'poi_purchase_order_item_ii_inventory_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
