<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Dependencies/PurchageOrederItem_dep.php


$dependencies['POI_Purchase_Order_Item']['poi_visibility_dep'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('related_to'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_poi_purchase_order_item_1_name',
                'value' => 'equal($related_to, "Work Product")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm01_sales_poi_purchase_order_item_1_name',
                'value' => 'equal($related_to, "Sales")',
            ),
        ),
    ),
);

$dependencies['POI_Purchase_Order_Item']['poi_required_dep'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_poi_purchase_order_item_1_name', 'related_to', 'm01_sales_poi_purchase_order_item_1_name', 'prod_product_poi_purchase_order_item_1_name'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_poi_purchase_order_item_1_name',
                'label' => 'wp_required_label',
                'value' => 'equal($related_to, "Work Product")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm01_sales_poi_purchase_order_item_1_name',
                'label' => 'sales_required_label',
                'value' => 'equal($related_to, "Sales")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'prod_product_poi_purchase_order_item_1_name',
                'label' => 'product_required_label',
                'value' => 'true',
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Dependencies/po_required_dep.php


$dependencies['POI_Purchase_Order_Item']['po_required_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('po_purchase_order_poi_purchase_order_item_1_name'),
    'onload'        => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions'       => array(
        array(
            'name'   => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'po_purchase_order_poi_purchase_order_item_1_name',
                'label'  => 'purchase_order_required_label',
                'value'  => 'true'
            ),
        ),
    ),
);
?>
<?php
// Merged from custom/Extension/modules/POI_Purchase_Order_Item/Ext/Dependencies/status_dynamic_dep.php

$dependencies['POI_Purchase_Order_Item']['poi_status_readonly_depnn'] = array(
    'hooks' => array("edit"),
    'trigger' => 'not(equal($order_date,""))',
    'triggerFields' => array('po_purchase_order_poi_purchase_order_item_1_name', 'order_date', 'status'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true',
            ),
        ),
    ),
);
 

$dependencies['POI_Purchase_Order_Item']['status_reada1112'] = array(
    'hooks' => array("edit","all"),
    'trigger' => 'true',
    //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'status',
    ),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    // You could list multiple fields here each in their own array under 'actions'
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'status',
                'keys' => 'ifElse(
                                greaterThan(strlen(related($poi_purchase_order_item_ri_received_items_1,"quantity_received")),0),
                                getDropdownKeySet("oi_status_list"),
                                ifElse(
                                    and(
                                    equal(related($po_purchase_order_poi_purchase_order_item_1,"status_c"),"Submitted"), 
                                    not(equal($order_date,"")),
                                    not(greaterThan(strlen(related($poi_purchase_order_item_ri_received_items_1,"quantity_received")),0))
                                    ),
                                    getDropdownKeySet("POI_or_status_list"),
                                    getDropdownKeySet("oi_status_list")
                                )
                            )',
                'labels' => 'ifElse(
                                greaterThan(strlen(related($poi_purchase_order_item_ri_received_items_1,"quantity_received")),0),
                                getDropdownValueSet("oi_status_list"),
                                ifElse(
                                    and(
                                    equal(related($po_purchase_order_poi_purchase_order_item_1,"status_c"),"Submitted"), 
                                    not(equal($order_date,"")),
                                    not(greaterThan(strlen(related($poi_purchase_order_item_ri_received_items_1,"quantity_received")),0))
                                    ),
                                    getDropdownValueSet("POI_or_status_list"),
                                    getDropdownValueSet("oi_status_list")
                                )
                            )'
            ),
        ),
    ),
);

$dependencies['POI_Purchase_Order_Item']['statusssss_reada1sdsd112'] = array(
    'hooks' => array("edit","all"),
    'trigger' => 'and(equal($status,"Ordered"),equal($status,"Backordered"))',
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'status'
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'status',
                'keys' => 'getDropdownKeySet("POI_or_status_list")',
                'labels' => 'getDropdownKeySet("POI_or_status_list")'
            ),
        ),
    ),
);

$dependencies['POI_Purchase_Order_Item']['status_reada111245'] = array(
    'hooks' => array("edit","all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'true', //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'status',
    ),
    'onload' => true, //Whether or not to trigger the dependencies when the page is loaded
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'ifElse(
                    and(equal(related($po_purchase_order_poi_purchase_order_item_1,"status_c"),"Submitted"), not(equal($order_date,"")),greaterThan(strlen(related($poi_purchase_order_item_ri_received_items_1,"quantity_received")),0)),true,false)'
            ),
        ), 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'ifElse(
                    or(equal(related($po_purchase_order_poi_purchase_order_item_1,"status_c"),"Pending"),equal(related($po_purchase_order_poi_purchase_order_item_1,"status_c"),"Cancelled")),true,false)'
            ),
        ),
    )
);


$dependencies['POI_Purchase_Order_Item']['fr33status_reada11a1245'] = array(
    'hooks' => array("edit","all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'and(equal($status,"Ordered"),equal($status,"Backordered"))',
    'triggerFields' => array(
        'status',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'false'
            ),
        ),
    )
);



$dependencies['POI_Purchase_Order_Item']['fr33status_reada111d245sd'] = array(
    'hooks' => array("edit","all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'equal($status,"Inventory")',
    'triggerFields' => array(
        'status',
        'id'
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'setValue',
            'params' => array(
                'target' => 'status',
                'value' => 'Inventory'
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true'
            ),
        ),
        
    )
);

$dependencies['POI_Purchase_Order_Item']['fr33status_reada11v1245sdaaaaa'] = array(
    'hooks' => array("edit","all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'equal($status,"Partially Received")',
    'triggerFields' => array(
        'status',
        'id'
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'setValue',
            'params' => array(
                'target' => 'status',
                'value' => 'Partially Received'
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true'
            ),
        ),
    )
);

?>
