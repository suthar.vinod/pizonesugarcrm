<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-11-09 09:48:27
$viewdefs['POI_Purchase_Order_Item']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'poi_purchase_order_item_ii_inventory_item_1',
  ),
);

// created: 2021-08-04 19:45:33
$viewdefs['POI_Purchase_Order_Item']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'context' => 
  array (
    'link' => 'poi_purchase_order_item_ri_received_items_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['POI_Purchase_Order_Item']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'poi_purchase_order_item_ri_received_items_1',
  'view' => 'subpanel-for-poi_purchase_order_item-poi_purchase_order_item_ri_received_items_1',
);
