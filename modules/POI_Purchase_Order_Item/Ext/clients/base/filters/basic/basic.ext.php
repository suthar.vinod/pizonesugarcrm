<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-08-10 
$viewdefs['POI_Purchase_Order_Item']['base']['filter']['basic']['filters'][] = array(
  'id' => 'FilterPOI_Purchase_Order_Item',
  'name' => 'POI Status',
  'filter_definition' => array(
    array(
      'status'  => array(
        '$in' => '',
      ),
    ),
  ),
  'editable' => true,
  'is_template' => true,
);
