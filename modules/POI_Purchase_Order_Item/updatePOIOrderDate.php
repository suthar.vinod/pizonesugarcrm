<?php

class updatePOIOrderDate {

    function updateOrderDate($bean, $event, $arguments) {
         global $db;
        $beanId = $bean->id;
        $orderDate = $bean->order_date;
        $POStatus = $bean->status_c;
        $estimated_arrival_date = $bean->estimated_arrival_date;
        
        if (isset($arguments['dataChanges']['status_c']) || isset($arguments['dataChanges']['order_date']) || isset($arguments['dataChanges']['estimated_arrival_date']) && $beanId != "") {
            $sqlpo = 'SELECT * FROM po_purchase_order_poi_purchase_order_item_1_c WHERE po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida = "' . $beanId . '" AND deleted = "0" ';
            $resultpo = $db->query($sqlpo);
            if ($resultpo->num_rows > 0) {
                while ($row = $db->fetchByAssoc($resultpo)) {
                    $poiId = $row['po_purchas4ee6er_item_idb'];
                    $purchase_order_item = BeanFactory::retrieveBean('POI_Purchase_Order_Item', $poiId);
                    $purchase_order_item->estimated_arrival_date = $estimated_arrival_date;
                    $purchase_order_item->order_date = $orderDate;
    
                    $purchase_order_item->load_relationship('poi_purchase_order_item_ri_received_items_1');
                    $relatedRI = $purchase_order_item->poi_purchase_order_item_ri_received_items_1->get();

                    $total = 0;
                    for ($a = 0; $a < count($relatedRI); $a++) {
                        $RI_Received_Items = BeanFactory::retrieveBean('RI_Received_Items', $relatedRI[$a]);
                        $quantity_received = $RI_Received_Items->quantity_received;             
                        $total = $total+$quantity_received;            
                    } 
                    $unit_quantity_ordered = $purchase_order_item->unit_quantity_ordered;
                    
                    if (($POStatus == "Pending" || $POStatus == "Cancelled") && $unit_quantity_ordered != "") {
                        $purchase_order_item->status = "Pending";
                        $purchase_order_item->save();                        
                    }elseif ($POStatus == "Submitted" && $purchase_order_item->order_date != "" && count($relatedRI) <= 0) {                        
                        $purchase_order_item->status = $purchase_order_item->status = "Ordered";  
                        $purchase_order_item->save();                      
                    }elseif ($purchase_order_item->order_date != "" && $unit_quantity_ordered <= $total) {
                        $purchase_order_item->status = "Inventory"; 
                        $purchase_order_item->save();                       
                    }elseif ($purchase_order_item->order_date != "" && count($relatedRI) > 0) {
                        $purchase_order_item->status = "Partially Received"; 
                        $purchase_order_item->save();                      
                    }
                }
            }
        }
    }

}
