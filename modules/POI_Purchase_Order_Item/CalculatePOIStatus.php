<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class CalculatePOIStatus {
    static $already_ran = false;  // To make sure LogicHooks will execute just onces
    function CalculatePOIStatusBeforeSave($bean, $event, $arguments) {
        if (self::$already_ran == true) return;   //So that hook will only trigger once
        self::$already_ran = true;
        global $db;
        /**23 March 2022 : Bug Fixes #2292 */
        $sql = "SELECT * FROM `po_purchase_order_poi_purchase_order_item_1_c` WHERE po_purchas4ee6er_item_idb='" . $bean->id . "' AND deleted=0 order by date_modified desc limit 1";

        $result = $db->query($sql);

        $row = $db->fetchByAssoc($result);
        $PO_id = $row['po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida'];       

        $purchaseOrderBean = BeanFactory::retrieveBean('PO_Purchase_Order', $PO_id);
        $POStatus = $purchaseOrderBean->status_c;
        
        $bean->load_relationship('poi_purchase_order_item_ri_received_items_1');
        $relatedRI = $bean->poi_purchase_order_item_ri_received_items_1->get();
        
        
        $total = 0;
        for ($a = 0; $a < count($relatedRI); $a++) {
            $RI_Received_Items = BeanFactory::retrieveBean('RI_Received_Items', $relatedRI[$a]);
            $quantity_received = $RI_Received_Items->quantity_received;             
            $total = $total+$quantity_received;            
        } 
               
        $unit_quantity_ordered = $bean->unit_quantity_ordered; 
              
        if (($POStatus == "Pending" || $POStatus == "Cancelled") && $unit_quantity_ordered != "") {
            $bean->status = "Pending";      
        }elseif ($POStatus == "Submitted" && $bean->order_date != "" && count($relatedRI) <= 0) {           
            $bean->status = "Ordered";      
        }elseif ($bean->order_date != "" && $unit_quantity_ordered <= $total) {
            $bean->status = "Inventory";          
        }elseif ($bean->order_date != "" && count($relatedRI) > 0) {
            $bean->status = "Partially Received";
        }
        /**//////////////////////////////// */
    }

   

    

}
