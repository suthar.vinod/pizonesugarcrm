<?php
/*
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class DuplicatePOIItemApi extends SugarApi {

    public function registerApiRest() {
        return array(
            'MyGetEndpoint' => array(
                'reqType' => 'POST',
                'noLoginRequired' => false,
                'path' => array('create_poi_duplicate'),
                'pathVars' => array(),
                'method' => 'create_poi_duplicate',
                'shortHelp' => '',
                'longHelp' => '',
            ),
        );
    }

    public function create_poi_duplicate($api, $args) {
        global $db, $current_user;
        $module = $args['module'];
        $recordId = $args['record'];
        
        $clonedBean = BeanFactory::retrieveBean($module, $recordId);
        $clonedBean->load_relationship("prod_product_poi_purchase_order_item_1");
        $clonedBean->load_relationship("po_purchase_order_poi_purchase_order_item_1");
        $clonedBean->load_relationship("m03_work_product_poi_purchase_order_item_1");
        $clonedBean->load_relationship("m01_sales_poi_purchase_order_item_1");

        if (is_numeric($clonedBean->unit_quantity_ordered)) {
            $quantity = intval($clonedBean->unit_quantity_ordered);

            $product_id = $clonedBean->prod_product_poi_purchase_order_item_1->get();
            $po_id = $clonedBean->po_purchase_order_poi_purchase_order_item_1->get();
            $wp_id = $clonedBean->m03_work_product_poi_purchase_order_item_1->get();
            $sale_id = $clonedBean->m01_sales_poi_purchase_order_item_1->get();

            $po_id1 = $clonedBean->po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida;

            for ($i = 0; $i < $quantity - 1; $i++) {
                $POIBean = BeanFactory::newBean('POI_Purchase_Order_Item');
                $bid = create_guid();
                $POIBean->id = $bid;
                $POIBean->new_with_id = true;
                $POIBean->fetched_row = null;                
                $POIBean->po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida = $po_id1;
                $POIBean->description = $clonedBean->description;
                $POIBean->order_date = $clonedBean->order_date;
                $POIBean->related_to = $clonedBean->related_to;
                $POIBean->status = $clonedBean->status;
                $POIBean->department = $clonedBean->department;
                $POIBean->type_2 = $clonedBean->type_2;
                $POIBean->subtype = $clonedBean->subtype;
                $POIBean->cost_per_unit = $clonedBean->cost_per_unit;
                $POIBean->unit_quantity_ordered = $clonedBean->unit_quantity_ordered;
                $POIBean->estimated_arrival_date = $clonedBean->estimated_arrival_date;
                $POIBean->received_date = $clonedBean->received_date;
                $POIBean->owner = $clonedBean->owner;                
                $POIBean->save();
                
                $POIBean->prod_product_poi_purchase_order_item_1->add($product_id);
                $POIBean->po_purchase_order_poi_purchase_order_item_1->add($po_id);
                $POIBean->m03_work_product_poi_purchase_order_item_1->add($wp_id);
                $POIBean->m01_sales_poi_purchase_order_item_1->add($sale_id);
            }
        }

        return true;
    }

}*/
