<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class UpdateProductUnitApi extends SugarApi
{

    public function registerApiRest()
    {
        return array(
            'MyGetEndpoint' => array(
                'reqType' => 'POST',
                'noLoginRequired' => false,
                'path' => array('update_product_cost_unit'),
                'pathVars' => array(),
                'method' => 'update_product_cost_unit',
                'shortHelp' => '',
                'longHelp' => '',
            ),
        );
    }

    public function update_product_cost_unit($api, $args)
    {
        global $db, $current_user;
        $module = $args['module'];
        $productId = $args['product'];
        $cost_per_unit = $args['cost_per_unit'];

        $ProductBean = BeanFactory::retrieveBean($module, $productId);
        $purchase_unit = $ProductBean->purchase_unit;

        $costPerUnitBefore  = $ProductBean->cost_per_unit_2;
        $costPerEach2Before  = $ProductBean->cost_per_each_2;
        $costPerEachBefore  = $ProductBean->cost_per_each;
        $clientCostBefore  = $ProductBean->client_cost_c;
        $lastPriceUpdateDateBefore  = $ProductBean->last_price_update_c;
        if ($ProductBean != "" && $purchase_unit == "Each") {
            //$cost_per_unit_2    = $cost_per_unit;
            $cost_per_each_2    = $cost_per_unit;
            //$ProductBean->cost_per_unit_2 = $cost_per_unit;
            //$ProductBean->cost_per_each_2 = $cost_per_unit;
            //$ProductBean->save();    

            $updateQuery = "UPDATE `prod_product` SET `cost_per_each_2` ='" . $cost_per_each_2 . "' WHERE `id`='" . $productId . "' ";
            $db->query($updateQuery);

            $event_id = create_guid();
            $auditsql = 'INSERT INTO prod_product_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $productId . '","' . $event_id . '",now(),"' . $current_user->id . '",now(),"cost_per_each_2","currency","' . $costPerEach2Before . '","' . $cost_per_each_2 . '")';
            $auditsqlResult = $db->query($auditsql);

            if ($auditsqlResult) {
                //Inserting audit data in audit_events table
                $source = '{"subject":{"_type":"user","id":"' . $current_user->id . '","_module":"Users","client":{"_type":"rest-api"}},"attributes":{"platform":"base"}}';
                $sql = "INSERT INTO audit_events(id,type,parent_id,module_name,source,data,date_created) values('" . $event_id . "','update','" . $productId . "','Prod_Product','" . $source . "',NULL,now())";
                $db->query($sql);
            }
        } else {
            $cost_per_each_2 = $cost_per_unit / $ProductBean->unit_quantity;
            $cost_per_unit_2    = $cost_per_unit;
            //$ProductBean->cost_per_each_2 = $cost_per_unit/$ProductBean->unit_quantity;
            //$ProductBean->cost_per_unit_2 = $cost_per_unit;
            //$ProductBean->save(); 

            $updateQuery = "UPDATE `prod_product` SET `cost_per_unit_2`='" . $cost_per_unit_2 . "', `cost_per_each` ='" . $cost_per_each_2 . "' WHERE `id`='" . $productId . "' ";
            $db->query($updateQuery);


            $event_id = create_guid();
            $auditsql = 'INSERT INTO prod_product_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $productId . '","' . $event_id . '",now(),"' . $current_user->id . '",now(),"cost_per_unit_2","currency","' . $costPerUnitBefore . '","' . $cost_per_unit_2 . '")';
            $auditsqlResult = $db->query($auditsql);

            if ($auditsqlResult) {
                //Inserting audit data in audit_events table
                $source = '{"subject":{"_type":"user","id":"' . $current_user->id . '","_module":"Users","client":{"_type":"rest-api"}},"attributes":{"platform":"base"}}';
                $sql = "INSERT INTO audit_events(id,type,parent_id,module_name,source,data,date_created) values('" . $event_id . "','update','" . $productId . "','Prod_Product','" . $source . "',NULL,now())";
                $db->query($sql);
            }

            $event_id = create_guid();
            $auditsql = 'INSERT INTO prod_product_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $productId . '","' . $event_id . '",now(),"' . $current_user->id . '",now(),"cost_per_each","currency","' . $costPerEachBefore . '","' . $cost_per_each_2 . '")';
            $auditsqlResult = $db->query($auditsql);

            if ($auditsqlResult) {
                //Inserting audit data in audit_events table
                $source = '{"subject":{"_type":"user","id":"' . $current_user->id . '","_module":"Users","client":{"_type":"rest-api"}},"attributes":{"platform":"base"}}';
                $sql = "INSERT INTO audit_events(id,type,parent_id,module_name,source,data,date_created) values('" . $event_id . "','update','" . $productId . "','Prod_Product','" . $source . "',NULL,now())";
                $db->query($sql);
            }
        }
        /* New Comment 460 for client_cost_c and 1045 */
        if ($cost_per_each_2 != "") {
            $newCost = $cost_per_each_2 + $cost_per_each_2 * $ProductBean->mark_up_c / 100;
        } else {
            $newCost = $cost_per_unit_2 + $cost_per_unit_2 * $ProductBean->mark_up_c / 100;
        }
        $todayDate = date("Y-m-d");
        $updateClientCostSql = "UPDATE `prod_product_cstm` 
                                SET `client_cost_c`='" . $newCost . "',
                                `last_price_update_c` ='" . $todayDate . "'
                                WHERE `id_c`='" . $productId . "' ";
        $db->query($updateClientCostSql);

        /* Audit log for client cost */
        $event_id = create_guid();
        $auditsql = 'INSERT INTO prod_product_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $productId . '","' . $event_id . '",now(),"' . $current_user->id . '",now(),"client_cost_c","currency","' . $clientCostBefore . '","' . $newCost . '")';
        $auditsqlResult = $db->query($auditsql);

        if ($auditsqlResult) {
            //Inserting audit data in audit_events table
            $source = '{"subject":{"_type":"user","id":"' . $current_user->id . '","_module":"Users","client":{"_type":"rest-api"}},"attributes":{"platform":"base"}}';
            $sql = "INSERT INTO audit_events(id,type,parent_id,module_name,source,data,date_created) values('" . $event_id . "','update','" . $productId . "','Prod_Product','" . $source . "',NULL,now())";
            $db->query($sql);
        }


        /* Audit log for last_price_update_c  */

        $checkAuditExistSql = "SELECT after_value_string from prod_product_audit where field_name='last_price_update_c' AND parent_id='$productId'  order by date_created desc limit 1";
        $checkAuditExistSqlResult = $db->query($checkAuditExistSql);
        $lastPriceUpdatedRow = $db->fetchByAssoc($checkAuditExistSqlResult);
        $lastPriceUpdateAuditDate = $lastPriceUpdatedRow['after_value_string'];

        $GLOBALS['log']->fatal('Fatal level message'.$lastPriceUpdateAuditDate." ".$todayDate);


        if (strtotime($lastPriceUpdateAuditDate) != strtotime($todayDate)) {
            $event_id = create_guid();
            $auditsql = 'INSERT INTO prod_product_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $productId . '","' . $event_id . '",now(),"' . $current_user->id . '",now(),"last_price_update_c","date","' . $lastPriceUpdateDateBefore . '","' . $todayDate . '") ON DUPLICATE KEY UPDATE parent_id="$productId", before_value_string="$lastPriceUpdateDateBefore", after_value_string="$todayDate"';
            $auditsqlResult = $db->query($auditsql);

            if ($auditsqlResult) {
                //Inserting audit data in audit_events table
                $source = '{"subject":{"_type":"user","id":"' . $current_user->id . '","_module":"Users","client":{"_type":"rest-api"}},"attributes":{"platform":"base"}}';
                $sql = "INSERT INTO audit_events(id,type,parent_id,module_name,source,data,date_created) values('" . $event_id . "','update','" . $productId . "','Prod_Product','" . $source . "',NULL,now())";
                $db->query($sql);
            }
        }



        return true;
    }
}
