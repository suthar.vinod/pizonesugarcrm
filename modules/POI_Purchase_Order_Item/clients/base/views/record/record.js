({
    extendsFrom: 'RecordView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.onload_function, this);
        //this.context.on('button:save_button:click', this.update_cost, this);
        this.context.on('button:edit_button:click', this.get_cost, this);
        this.model.once("sync",
            function () {
                this.model.on(
                    "change:department",
                    this.function_department,
                    this
                );
            },
            this
        );

        this.model.once("sync",
            function () {
                this.model.on(
                    "change:type_2",
                    this.function_type_2,
                    this
                );
            },
            this
        );
        /*this.model.once("sync",
            function () {
                this.model.on(
                    "change:subtype",
                    this.function_subtype,
                    this
                );
            },
            this
        );*/

        this.model.once("sync",
            function () {
                this.model.on(
                    "change:prod_product_poi_purchase_order_item_1_name",
                    this.function_get_costPerUnit,
                    this
                );
            },
            this
        );

        this.model.once("sync",
            function () {
                this.model.on(
                    "change:po_purchase_order_poi_purchase_order_item_1_name",
                    this.function_get_EstArrivalDate,
                    this
                );
            },
            this
        );

    },
    handleSave: function () {
        var self = this;
        var Product_ID = self.model.get('prod_product_poi_purchase_order_item_1prod_product_ida');
        var cost_per_unit = self.model.get('cost_per_unit');
        // variable is undefined
        if (parseFloat(cost_per_unit) != parseFloat(productCost) && Product_ID != "" && cost_per_unit != "") {
            app.alert.show('myconfirm-msg', {
                level: 'confirmation',
                messages: 'Cost Per Unit field was updated. Click Confirm to edit corresponding Product record.',
                autoClose: false,
                onConfirm: function () {
                    self._super('handleSave');
                    app.api.call("create", "rest/v10/update_product_cost_unit", {
                        module: 'Prod_Product',
                        product: Product_ID,
                        cost_per_unit: cost_per_unit,
                    });
                },
                onCancel: function () {
                    self._super('handleSave');
                }
            });
        } else {
            self._super('handleSave');
        }
    },
    onload_function: function () {

        var order_date = this.model.get('order_date');

        //get related Data 	
        var statusVal = this.model.get('status');
        var uniqueArr = new Object();
        uniqueArr["Ordered"] = "Ordered";
        uniqueArr["Backordered"] = "Backordered";
        console.log('statusVal', statusVal);
        var self = this;
        app.api.call("get", "rest/v10/POI_Purchase_Order_Item/" + this.model.get('id') + "/link/poi_purchase_order_item_ri_received_items_1?fields=name&max_num=1&order_by=name:desc", null, {
            success: function (RelatedData) {
                console.log('RelatedData', RelatedData);

                if (RelatedData.records.length == 0 && order_date != "") {
                    if (statusVal != "Inventory" || statusVal != "Pending" || statusVal != "Partially Received") {
                        if (statusVal == "Ordered" || statusVal == "Backordered") {
                            self.model.set('status', uniqueArr[statusVal]);
                        }

                    }
                }

            }
        });

        $('div[data-name="order_date"]').css('pointer-events', 'none');

        var Product_ID = this.model.get('prod_product_poi_purchase_order_item_1prod_product_ida');
        if (Product_ID == "" || Product_ID == null) {
            this.model.set('cost_per_unit', "");
        }

        if (Product_ID != "" && Product_ID != null) {
            var self = this;
            App.api.call("get", "rest/v10/Prod_Product/" + Product_ID + "?fields=purchase_unit,cost_per_unit_2,cost_per_each_2", null, {
                success: function (Data) {
                    //self.model.set('cost_per_unit', Data.cost_per_unit_2);
                    console.log(Data.cost_per_unit_2 + " == " + Data.cost_per_each_2 + " Data.purchase_unit");
                    if (Data.purchase_unit == "Each") {
                        productCost = Data.cost_per_each_2;
                    } else {
                        productCost = Data.cost_per_unit_2;
                    }
                }
            });
        }

    },

    function_get_EstArrivalDate: function () {
        var PO_ID = this.model.get('po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida');
        if (PO_ID != "" && PO_ID != null) {
            var self = this;
            App.api.call("get", "rest/v10/PO_Purchase_Order/" + PO_ID + "?fields=estimated_arrival_date,order_date,status_c", null, {
                success: function (Data) {
                    setTimeout(function () {
                        self.model.set('estimated_arrival_date', Data.estimated_arrival_date);
                        self.model.set('order_date', Data.order_date);
                        if (Data.status_c == "Pending" || Data.status_c == "Cancelled") {
                            self.model.set('status', "Pending");
                        } else {
                            self.model.set('status', Data.status_c);
                        }
                        $('div[data-name="order_date"]').css('pointer-events', 'none');
                        self.$el.find('.record-edit-link-wrapper[data-name=order_date]').hide();
                    }, 300);
                }
            });
        }
    },
    function_get_costPerUnit: function () {
        var Product_ID = this.model.get('prod_product_poi_purchase_order_item_1prod_product_ida');
        if (Product_ID == "" || Product_ID == null) {
            this.model.set('cost_per_unit', "");
            this.model.set('subtype', '');
        }

        if (Product_ID != "" && Product_ID != null) {
            var self = this;
            App.api.call("get", "rest/v10/Prod_Product/" + Product_ID + "?fields=purchase_unit,cost_per_unit_2,cost_per_each_2,subtype", null, {
                success: function (Data) {
                    self.model.set('subtype', Data.subtype);
                    if (Data.purchase_unit == "Each") {
                        self.model.set('cost_per_unit', Data.cost_per_each_2);
                        productCost = Data.cost_per_each_2;
                    } else {
                        self.model.set('cost_per_unit', Data.cost_per_unit_2);
                        productCost = Data.cost_per_unit_2;
                    }
                }
            });
        }
    },
    get_cost: function () {
        var Product_ID = this.model.get('prod_product_poi_purchase_order_item_1prod_product_ida');
        if (Product_ID == "" || Product_ID == null) {
            this.model.set('cost_per_unit', "");
        }

        if (Product_ID != "" && Product_ID != null) {
            var self = this;
            App.api.call("get", "rest/v10/Prod_Product/" + Product_ID + "?fields=purchase_unit,cost_per_unit_2,cost_per_each_2", null, {
                success: function (Data) {
                    //self.model.set('cost_per_unit', Data.cost_per_unit_2);
                    console.log(Data.cost_per_unit_2 + " == " + Data.cost_per_each_2 + " Data.purchase_unit");
                    if (Data.purchase_unit == "Each") {
                        productCost = Data.cost_per_each_2;
                    } else {
                        productCost = Data.cost_per_unit_2;
                    }
                }
            });
        }
    },
    function_department: function () {
        this.model.set('prod_product_poi_purchase_order_item_1prod_product_ida', null);
        this.model.set('prod_product_poi_purchase_order_item_1_name', null);
    },
    function_type_2: function () {
        this.model.set('prod_product_poi_purchase_order_item_1prod_product_ida', null);
        this.model.set('prod_product_poi_purchase_order_item_1_name', null);
    },
    /*function_subtype: function () {
        this.model.set('prod_product_poi_purchase_order_item_1prod_product_ida', null);
        this.model.set('prod_product_poi_purchase_order_item_1_name', null);
    },*/
})