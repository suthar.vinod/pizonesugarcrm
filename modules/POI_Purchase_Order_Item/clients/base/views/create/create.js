({
    extendsFrom: 'CreateView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.model.on('change:department', this.function_department, this);
        this.model.on('change:type_2', this.function_type_2, this);
        //this.model.on('change:subtype', this.function_subtype, this);
        this.model.on('change:prod_product_poi_purchase_order_item_1_name', this.function_get_costPerUnit, this);
        this.model.on('change:po_purchase_order_poi_purchase_order_item_1_name', this.function_get_EstArrivalDate, this);
        //this.on('render', _.bind(this.onload_function, this));
    },

    saveAndClose: function () {
        var self = this;
        var Product_ID = self.model.get('prod_product_poi_purchase_order_item_1prod_product_ida');
        var cost_per_unit = self.model.get('cost_per_unit');

        if (!(typeof productCost === 'undefined')) {
            if (parseFloat(cost_per_unit) != parseFloat(productCost) && Product_ID != "" && cost_per_unit != "") {

                app.alert.show('message-id', {
                    level: 'confirmation',
                    messages: 'Cost Per Unit field was updated. Click Confirm to edit corresponding Product record.',
                    autoClose: false,
                    onConfirm: function () {
                        app.api.call("create", "rest/v10/update_product_cost_unit", {
                            module: 'Prod_Product',
                            product: Product_ID,
                            cost_per_unit: cost_per_unit,
                        }, {
                            success: function (result) {
                                console.log('done', result);
                                self.initiateSave(_.bind(function () {
                                    app.navigate(self.context, self.model);
                                }, self));
                            },
                            error: function (error) {
                                app.alert.show('link_wps_error', {
                                    level: 'error',
                                    messages: 'Failed To Create'
                                });
                            }
                        });
                    },
                    onCancel: function () {
                        self.initiateSave(_.bind(function () {
                            app.navigate(self.context, self.model);
                        }, self));
                    }
                });

            } else {
                self.initiateSave(_.bind(function () {
                    app.navigate(self.context, self.model);
                }, self));
            }
            return;
        } else {
            self.initiateSave(_.bind(function () {
                app.navigate(self.context, self.model);
            }, self));
        }
    },
    function_get_EstArrivalDate: function () {
        var PO_ID = this.model.get('po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida');
        if (PO_ID != "" && PO_ID != null) {
            var self = this;
            App.api.call("get", "rest/v10/PO_Purchase_Order/" + PO_ID + "?fields=estimated_arrival_date,order_date,status_c", null, {
                success: function (Data) {
                    setTimeout(function () {
                        console.log('sss', Data.status_c);
                        self.model.set('estimated_arrival_date', Data.estimated_arrival_date);
                        self.model.set('order_date', Data.order_date);
                        if (Data.status_c == "Pending" || Data.status_c == "Cancelled"){
                            self.model.set('status', "Pending");
                        }else{
                            self.model.set('status', Data.status_c);
                        }
                        $('div[data-name="order_date"]').css('pointer-events', 'none');
                        $('div[data-name="order_date"]').css('disabled', true);
                    }, 300);
                }
            });
        } else {
            this.model.set('status', "");
        }
    },
    function_get_costPerUnit: function () {
        var Product_ID = this.model.get('prod_product_poi_purchase_order_item_1prod_product_ida');
        if (Product_ID == "" || Product_ID == null) {
            this.model.set('cost_per_unit', "");
            this.model.set('subtype', '');
        }

        if (Product_ID != "" && Product_ID != null) {
            var self = this;
            App.api.call("get", "rest/v10/Prod_Product/" + Product_ID + "?fields=purchase_unit,cost_per_unit_2,cost_per_each_2,subtype", null, {
                success: function (Data) {
                    self.model.set('subtype', Data.subtype);
                    if (Data.purchase_unit == "Each") {
                        self.model.set('cost_per_unit', Data.cost_per_each_2);
                        productCost = Data.cost_per_each_2;
                    } else {
                        self.model.set('cost_per_unit', Data.cost_per_unit_2);
                        productCost = Data.cost_per_unit_2;
                    }
                }
            });
        }
    },
    function_department: function () {
        this.model.set('prod_product_poi_purchase_order_item_1prod_product_ida', null);
        this.model.set('prod_product_poi_purchase_order_item_1_name', null);

    },
    function_type_2: function () {
        this.model.set('prod_product_poi_purchase_order_item_1prod_product_ida', null);
        this.model.set('prod_product_poi_purchase_order_item_1_name', null);
    },
    /*function_subtype: function () {
        this.model.set('prod_product_poi_purchase_order_item_1prod_product_ida', null);
        this.model.set('prod_product_poi_purchase_order_item_1_name', null);
    },*/
})