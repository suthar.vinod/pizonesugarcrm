<?php
// created: 2022-04-12 09:37:26
$viewdefs['POI_Purchase_Order_Item']['base']['view']['subpanel-for-po_purchase_order-po_purchase_order_poi_purchase_order_item_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'prod_product_poi_purchase_order_item_1_name',
          'label' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_PROD_PRODUCT_TITLE',
          'enabled' => true,
          'id' => 'PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1PROD_PRODUCT_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'products_department_c',
          'label' => 'LBL_PRODUCTS_DEPARTMENT',
          'enabled' => true,
          'readonly' => '1',
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'unit_quantity_ordered',
          'label' => 'LBL_UNIT_QUANTITY_ORDERED',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'cost_per_unit',
          'label' => 'LBL_COST_PER_UNIT',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'order_date',
          'label' => 'LBL_ORDER_DATE',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'estimated_arrival_date',
          'label' => 'LBL_ESTIMATED_ARRIVAL_DATE',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'notes_c',
          'label' => 'LBL_NOTES',
          'enabled' => true,
          'readonly' => false,
          'sortable' => false,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'm01_sales_poi_purchase_order_item_1_name',
          'label' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_M01_SALES_TITLE',
          'enabled' => true,
          'id' => 'M01_SALES_POI_PURCHASE_ORDER_ITEM_1M01_SALES_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'm03_work_product_poi_purchase_order_item_1_name',
          'label' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
          'enabled' => true,
          'id' => 'M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1M03_WORK_PRODUCT_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);