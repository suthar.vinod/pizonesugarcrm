<?php
// created: 2022-04-12 09:34:46
$viewdefs['POI_Purchase_Order_Item']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'date_entered' => 
    array (
    ),
    'cost_per_unit' => 
    array (
    ),
    'department' => 
    array (
    ),
    'estimated_arrival_date' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'name' => 
    array (
    ),
    'notes_c' => 
    array (
    ),
    'order_date' => 
    array (
    ),
    'owner' => 
    array (
    ),
    'prod_product_poi_purchase_order_item_1_name' => 
    array (
    ),
    'po_purchase_order_poi_purchase_order_item_1_name' => 
    array (
    ),
    'related_to' => 
    array (
    ),
    'm01_sales_poi_purchase_order_item_1_name' => 
    array (
    ),
    'status' => 
    array (
    ),
    'subtype' => 
    array (
    ),
    'type_2' => 
    array (
    ),
    'unit_quantity_ordered' => 
    array (
    ),
    'm03_work_product_poi_purchase_order_item_1_name' => 
    array (
    ),
  ),
);