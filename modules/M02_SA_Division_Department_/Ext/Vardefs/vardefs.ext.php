<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M02_SA_Division_Department_/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:56
$dictionary['M02_SA_Division_Department_']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/M02_SA_Division_Department_/Ext/Vardefs/sugarfield_division_c.php

 // created: 2016-10-25 03:27:05
$dictionary['M02_SA_Division_Department_']['fields']['division_c']['labelValue'] = 'Division';
$dictionary['M02_SA_Division_Department_']['fields']['division_c']['dependency'] = '';
$dictionary['M02_SA_Division_Department_']['fields']['division_c']['visibility_grid'] = '';
$dictionary['M02_SA_Division_Department_']['fields']['division_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/M02_SA_Division_Department_/Ext/Vardefs/sugarfield_department_c.php

 // created: 2016-10-25 03:27:05
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['labelValue'] = 'Department';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['trigger'] = 'division_c';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['InLife_Services'][0] = 'Biocompatibility';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['InLife_Services'][1] = 'Interventional Surgical';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['InLife_Services'][2] = 'Pharmacology';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['InLife_Services'][3] = 'Toxicology';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['InVitro_Services'][0] = 'Biocompatibility';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['InVitro_Services'][1] = 'Microbiology';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['Pathology_Services'][0] = 'Gross Pathology';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['Pathology_Services'][1] = 'Histology';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['visibility_grid']['values']['Analytical_Services'][0] = 'Analytical';
$dictionary['M02_SA_Division_Department_']['fields']['department_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/M02_SA_Division_Department_/Ext/Vardefs/sugarfield_name.php

 // created: 2016-10-25 03:27:05
$dictionary['M02_SA_Division_Department_']['fields']['name']['len'] = '255';
$dictionary['M02_SA_Division_Department_']['fields']['name']['audited'] = true;
$dictionary['M02_SA_Division_Department_']['fields']['name']['massupdate'] = false;
$dictionary['M02_SA_Division_Department_']['fields']['name']['importable'] = 'false';
$dictionary['M02_SA_Division_Department_']['fields']['name']['duplicate_merge'] = 'disabled';
$dictionary['M02_SA_Division_Department_']['fields']['name']['duplicate_merge_dom_value'] = 0;
$dictionary['M02_SA_Division_Department_']['fields']['name']['merge_filter'] = 'disabled';
$dictionary['M02_SA_Division_Department_']['fields']['name']['unified_search'] = false;
$dictionary['M02_SA_Division_Department_']['fields']['name']['calculated'] = '1';
$dictionary['M02_SA_Division_Department_']['fields']['name']['formula'] = '$division_c';
$dictionary['M02_SA_Division_Department_']['fields']['name']['enforced'] = true;
$dictionary['M02_SA_Division_Department_']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['M02_SA_Division_Department_']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['M02_SA_Division_Department_']['fields']['name']['full_text_search']['boost'] = 1.5500000000000000444089209850062616169452667236328125;


?>
