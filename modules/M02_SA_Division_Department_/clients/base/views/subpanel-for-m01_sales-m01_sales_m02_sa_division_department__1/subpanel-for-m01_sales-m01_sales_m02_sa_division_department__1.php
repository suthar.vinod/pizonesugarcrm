<?php
// created: 2016-01-26 04:01:12
$viewdefs['M02_SA_Division_Department_']['base']['view']['subpanel-for-m01_sales-m01_sales_m02_sa_division_department__1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'division_c',
          'label' => 'LBL_DIVISION',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'department_c',
          'label' => 'LBL_DEPARTMENT',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);