<?php
// created: 2016-01-26 04:00:45
$subpanel_layout['list_fields'] = array (
  'division_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_DIVISION',
    'width' => '10%',
  ),
  'department_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_DEPARTMENT',
    'width' => '10%',
  ),
);