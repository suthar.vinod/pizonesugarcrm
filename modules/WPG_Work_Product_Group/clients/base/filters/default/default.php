<?php
// created: 2021-07-06 17:08:51
$viewdefs['WPG_Work_Product_Group']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'assigned_user_name' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'name' => 
    array (
    ),
    'test_system_c' => 
    array (
    ),
    'total_capacity' => 
    array (
    ),
  ),
);