<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/WPG_Work_Product_Group/Ext/Vardefs/wpg_work_product_group_m03_work_product_code_1_WPG_Work_Product_Group.php

// created: 2021-04-20 09:04:33
$dictionary["WPG_Work_Product_Group"]["fields"]["wpg_work_product_group_m03_work_product_code_1"] = array (
  'name' => 'wpg_work_product_group_m03_work_product_code_1',
  'type' => 'link',
  'relationship' => 'wpg_work_product_group_m03_work_product_code_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Code',
  'bean_name' => 'M03_Work_Product_Code',
  'vname' => 'LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_WPG_WORK_PRODUCT_GROUP_TITLE',
  'id_name' => 'wpg_work_p164at_group_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/WPG_Work_Product_Group/Ext/Vardefs/sugarfield_test_system_c.php

 // created: 2021-07-08 09:17:26
$dictionary['WPG_Work_Product_Group']['fields']['test_system_c']['labelValue']='Test System';
$dictionary['WPG_Work_Product_Group']['fields']['test_system_c']['dependency']='';
$dictionary['WPG_Work_Product_Group']['fields']['test_system_c']['required_formula']='';
$dictionary['WPG_Work_Product_Group']['fields']['test_system_c']['readonly_formula']='';
$dictionary['WPG_Work_Product_Group']['fields']['test_system_c']['visibility_grid']='';

 
?>
