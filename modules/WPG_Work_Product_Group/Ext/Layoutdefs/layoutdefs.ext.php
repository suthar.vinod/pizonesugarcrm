<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/WPG_Work_Product_Group/Ext/Layoutdefs/wpg_work_product_group_m03_work_product_code_1_WPG_Work_Product_Group.php

 // created: 2021-04-20 09:04:33
$layout_defs["WPG_Work_Product_Group"]["subpanel_setup"]['wpg_work_product_group_m03_work_product_code_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product_Code',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'get_subpanel_data' => 'wpg_work_product_group_m03_work_product_code_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/WPG_Work_Product_Group/Ext/Layoutdefs/_overrideWPG_Work_Product_Group_subpanel_wpg_work_product_group_m03_work_product_code_1.php

//auto-generated file DO NOT EDIT
$layout_defs['WPG_Work_Product_Group']['subpanel_setup']['wpg_work_product_group_m03_work_product_code_1']['override_subpanel_name'] = 'WPG_Work_Product_Group_subpanel_wpg_work_product_group_m03_work_product_code_1';

?>
