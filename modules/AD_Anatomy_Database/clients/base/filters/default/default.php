<?php
// created: 2019-12-18 12:35:36
$viewdefs['AD_Anatomy_Database']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'animal_number_c' => 
    array (
    ),
    'work_product_id_c' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'weight_range_c' => 
    array (
    ),
    'weight_c' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'species_2_c' => 
    array (
    ),
    'breed_c' => 
    array (
    ),
    'anatomical_location_c' => 
    array (
    ),
    'imaging_modality_c' => 
    array (
    ),
    'measurement_c' => 
    array (
    ),
  ),
);