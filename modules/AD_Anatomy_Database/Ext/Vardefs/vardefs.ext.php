<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/AD_Anatomy_Database/Ext/Vardefs/sugarfield_name.php

 // created: 2019-06-25 14:14:21
$dictionary['AD_Anatomy_Database']['fields']['name']['len']='255';
$dictionary['AD_Anatomy_Database']['fields']['name']['audited']=true;
$dictionary['AD_Anatomy_Database']['fields']['name']['massupdate']=false;
$dictionary['AD_Anatomy_Database']['fields']['name']['unified_search']=false;
$dictionary['AD_Anatomy_Database']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['AD_Anatomy_Database']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/AD_Anatomy_Database/Ext/Vardefs/sugarfield_animal_number_c.php

 // created: 2019-06-25 14:18:40
$dictionary['AD_Anatomy_Database']['fields']['animal_number_c']['labelValue']='Animal Number';
$dictionary['AD_Anatomy_Database']['fields']['animal_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['AD_Anatomy_Database']['fields']['animal_number_c']['enforced']='';
$dictionary['AD_Anatomy_Database']['fields']['animal_number_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/AD_Anatomy_Database/Ext/Vardefs/sugarfield_work_product_id_c.php

 // created: 2019-06-25 14:19:35
$dictionary['AD_Anatomy_Database']['fields']['work_product_id_c']['labelValue']='Work Product ID';
$dictionary['AD_Anatomy_Database']['fields']['work_product_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['AD_Anatomy_Database']['fields']['work_product_id_c']['enforced']='';
$dictionary['AD_Anatomy_Database']['fields']['work_product_id_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/AD_Anatomy_Database/Ext/Vardefs/sugarfield_anatomical_location_c.php

 // created: 2019-06-25 14:20:24
$dictionary['AD_Anatomy_Database']['fields']['anatomical_location_c']['labelValue']='Anatomical Location';
$dictionary['AD_Anatomy_Database']['fields']['anatomical_location_c']['dependency']='';
$dictionary['AD_Anatomy_Database']['fields']['anatomical_location_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/AD_Anatomy_Database/Ext/Vardefs/sugarfield_imaging_modality_c.php

 // created: 2019-06-25 14:21:13
$dictionary['AD_Anatomy_Database']['fields']['imaging_modality_c']['labelValue']='Imaging Modality';
$dictionary['AD_Anatomy_Database']['fields']['imaging_modality_c']['dependency']='';
$dictionary['AD_Anatomy_Database']['fields']['imaging_modality_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/AD_Anatomy_Database/Ext/Vardefs/sugarfield_weight_c.php

 // created: 2019-06-25 14:22:04
$dictionary['AD_Anatomy_Database']['fields']['weight_c']['labelValue']='Weight (kg)';
$dictionary['AD_Anatomy_Database']['fields']['weight_c']['enforced']='';
$dictionary['AD_Anatomy_Database']['fields']['weight_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/AD_Anatomy_Database/Ext/Vardefs/sugarfield_weight_range_c.php

 // created: 2019-06-25 14:23:14
$dictionary['AD_Anatomy_Database']['fields']['weight_range_c']['labelValue']='Weight Range (kg)';
$dictionary['AD_Anatomy_Database']['fields']['weight_range_c']['dependency']='';
$dictionary['AD_Anatomy_Database']['fields']['weight_range_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/AD_Anatomy_Database/Ext/Vardefs/sugarfield_measurement_c.php

 // created: 2019-06-25 14:25:45
$dictionary['AD_Anatomy_Database']['fields']['measurement_c']['labelValue']='Measurement (mm)';
$dictionary['AD_Anatomy_Database']['fields']['measurement_c']['enforced']='';
$dictionary['AD_Anatomy_Database']['fields']['measurement_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/AD_Anatomy_Database/Ext/Vardefs/sugarfield_s_species_id_c.php

 // created: 2019-12-10 20:07:37

 
?>
<?php
// Merged from custom/Extension/modules/AD_Anatomy_Database/Ext/Vardefs/sugarfield_species_2_c.php

 // created: 2019-12-18 12:37:05
$dictionary['AD_Anatomy_Database']['fields']['species_2_c']['labelValue']='Species';
$dictionary['AD_Anatomy_Database']['fields']['species_2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/AD_Anatomy_Database/Ext/Vardefs/sugarfield_breed_c.php

 // created: 2020-06-11 12:36:12
$dictionary['AD_Anatomy_Database']['fields']['breed_c']['labelValue']='Breed';
$dictionary['AD_Anatomy_Database']['fields']['breed_c']['dependency']='';
$dictionary['AD_Anatomy_Database']['fields']['breed_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/AD_Anatomy_Database/Ext/Vardefs/sugarfield_description.php

 // created: 2020-09-03 07:39:06
$dictionary['AD_Anatomy_Database']['fields']['description']['audited']=true;
$dictionary['AD_Anatomy_Database']['fields']['description']['massupdate']=false;
$dictionary['AD_Anatomy_Database']['fields']['description']['hidemassupdate']=false;
$dictionary['AD_Anatomy_Database']['fields']['description']['comments']='Full text of the note';
$dictionary['AD_Anatomy_Database']['fields']['description']['duplicate_merge']='enabled';
$dictionary['AD_Anatomy_Database']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['AD_Anatomy_Database']['fields']['description']['merge_filter']='disabled';
$dictionary['AD_Anatomy_Database']['fields']['description']['unified_search']=false;
$dictionary['AD_Anatomy_Database']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['AD_Anatomy_Database']['fields']['description']['calculated']=false;
$dictionary['AD_Anatomy_Database']['fields']['description']['rows']='6';
$dictionary['AD_Anatomy_Database']['fields']['description']['cols']='80';

 
?>
