<?php

class HookBeforeSaveForNameGenerationOT
{

    public function generateNameOT($bean, $event, $arguments)
    {
        global $db,$sugar_config,$current_user; 
		global $app_list_strings;
        $category_dom	 	= $app_list_strings['ot_category_list'];
        $op_id = $bean->contact_id_c;

        $offset1 =  18000;
		//$offset1 =  21600; //daylight Saving
        $arr = explode("+", $bean->date_2);
        $OT_date = $arr[0]; //date("m/d/Y H:i:s",$bean->date_time_discovered_c); //strtotime($bean->date_time_discovered_c);
        $OT_date = strtotime($OT_date) + $offset1;
        //$GLOBALS['log']->fatal(' datefirst_procedure_c ' . $OT_date);

        $OT_date_final = date("m/d/Y",$OT_date);
        //$GLOBALS['log']->fatal("scheduled_start " . $OT_date_final);
        //$GLOBALS['log']->fatal("room_id " . $room_id);
        if (!empty($op_id)) {
            $Contact_Bean = BeanFactory::getBean("Contacts", $op_id, array('disable_row_level_security' => true));
            $Contact_Name = $Contact_Bean->full_name;;
            //$GLOBALS['log']->fatal("Contact_Name " . $Contact_Name);
        }
        $Cat_name =  $category_dom[$bean->category];
        
        
        $nameToSet = $Contact_Name . ' ' .$Cat_name. ' ' .$OT_date_final;
        //$GLOBALS['log']->fatal("nameToSet " . $nameToSet);
        $bean->name = $nameToSet;
    }
}