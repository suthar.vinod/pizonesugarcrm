<?php

if (!defined('sugarEntry') || !sugarEntry)
  die('Not A Valid Entry Point');

class UpdateRelatedRecord
{
  static $already_ran = false;  // To make sure LogicHooks will execute just onces
  function UpdateRelatedRec($bean, $event, $arguments)
  {
    if (self::$already_ran == true)
      return;   //So that hook will only trigger once
    self::$already_ran = true;
    global $db, $current_user;
    if ($arguments['isUpdate'] == 1) {
      /*Get the related record based on record being edited */
      $sql = "SELECT tbl_tsd.id,tbl_tsd.name,tbl_tsd.deleted
      FROM taskd_task_design AS tbl_tsd
      LEFT JOIN taskd_task_design_cstm AS tbl_tsd_cstm ON tbl_tsd.id = tbl_tsd_cstm.id_c
      WHERE tbl_tsd_cstm.parent_td_id_c='" . $bean->id . "' AND tbl_tsd.deleted=0";
      $result = $db->query($sql);
      $clonedBean = BeanFactory::getBean('TaskD_Task_Design', $bean->id);
      $u_units_id_c = $clonedBean->u_units_id_c;
      $start_integer = $clonedBean->start_integer;
      $end_integer = $clonedBean->end_integer;
      $task_type = $clonedBean->task_type;
      $custom_task = $clonedBean->custom_task;
      $standard_task = $clonedBean->standard_task;
      $time_window = $clonedBean->time_window;
      $actual_datetime = $clonedBean->actual_datetime;
      $scheduled_start_datetime = $clonedBean->scheduled_start_datetime;
      $scheduled_end_datetime = $clonedBean->scheduled_end_datetime;
      $category = $clonedBean->category;
      $equipment_required = $clonedBean->equipment_required;
      $description = $clonedBean->description;
      $phase_c = $clonedBean->phase_c;
      $relative = $clonedBean->relative;
      $days_from_initial_task = $clonedBean->days_from_initial_task;
      //if ($result->num_rows > 0) {
      for ($i = 0; $i < $result->num_rows; $i++) {
        //while ($row = $db->fetchByAssoc($result)) {
        $row = $db->fetchByAssoc($result);
        $relatedTSid = $row['id'];
        /** clone data from main parent record */
        /**Get the child record id and save the data from main record into related record*/
        $childBean = BeanFactory::getBean('TaskD_Task_Design', $relatedTSid);
        $childBean->load_relationship('taskd_task_design_taskd_task_design_1');
        /* $wpid = $childBean->m03_work_product_taskd_task_design_1m03_work_product_ida;*/
        $wpname = $childBean->m03_work_product_taskd_task_design_1_name;
        $tsName =  $childBean->anml_animals_taskd_task_design_1_name;
        //$tsID =  $childBean->anml_animals_taskd_task_design_1anml_animals_ida; 
        // $childBean->u_units_id_c = $u_units_id_c;
        //$childBean->start_integer = $start_integer;
        // $childBean->end_integer = $end_integer;
        if (isset($arguments['dataChanges']['task_type'])) {
          $childBean->task_type = $task_type;
        }
        if (isset($arguments['dataChanges']['custom_task'])) {
          $childBean->custom_task = $custom_task;
        }
        if (isset($arguments['dataChanges']['standard_task'])) {
          $childBean->standard_task = $standard_task;
        }
        if (isset($arguments['dataChanges']['time_window'])) {
          $childBean->time_window = $time_window;
        }
        if (isset($arguments['dataChanges']['actual_datetime'])) {
          $childBean->actual_datetime = $actual_datetime;
        }
        if (isset($arguments['dataChanges']['scheduled_start_datetime'])) {
          $childBean->scheduled_start_datetime = $scheduled_start_datetime;
        }
        if (isset($arguments['dataChanges']['scheduled_end_datetime'])) {
          $childBean->scheduled_end_datetime = $scheduled_end_datetime;
        }
        if (isset($arguments['dataChanges']['category'])) {
          $childBean->category = $category;;
        }
        if (isset($arguments['dataChanges']['equipment_required'])) {
          $childBean->equipment_required = $equipment_required;
        }
        if (isset($arguments['dataChanges']['description'])) {
          $childBean->description = $description;
        }
        if (isset($arguments['dataChanges']['phase_c'])) {
          $childBean->phase_c = $phase_c;
        }
        if (isset($arguments['dataChanges']['relative'])) {
          $childBean->relative = $relative;
        }
        if (isset($arguments['dataChanges']['days_from_initial_task'])) {
          $childBean->days_from_initial_task = $days_from_initial_task;
        }
        if (isset($arguments['dataChanges']['order_2_c'])) {
          $childBean->order_2_c = $clonedBean->order_2_c;
        }
        if (isset($arguments['dataChanges']['type_of_personnel_2_c'])) {
          $childBean->type_of_personnel_2_c = $clonedBean->type_of_personnel_2_c;
        }
        if (isset($arguments['dataChanges']['duration_integer_min_c'])) {
          $childBean->duration_integer_min_c = $clonedBean->duration_integer_min_c;
        }
        if (isset($arguments['dataChanges']['other_equipment_c'])) {
          $childBean->other_equipment_c = $clonedBean->other_equipment_c;
        }

        /** Name should be update for child records if  value changes in custom/standarad task in parent record */
        $old_custom_task = $childBean->fetched_row['custom_task'];
        $new_custom_task = $childBean->custom_task;
        $old_standard_task = $childBean->fetched_row['standard_task'];
        $new_standard_task = $childBean->standard_task;

        if ($old_custom_task != $new_custom_task) {
          $updatedatedname = $wpname . ' ' . $tsName . ' ' . $new_custom_task;
        } else if ($old_standard_task != $new_standard_task) {
          $updatedatedname = $wpname . ' ' . $tsName . ' ' . $new_standard_task;
        }
        if ($old_custom_task != $new_custom_task || $old_standard_task != $new_standard_task) {
          $childBean->name = $updatedatedname;
        }

        //$tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $childBean->taskd_task_design_taskd_task_design_1taskd_task_design_ida);
        // $tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $childBean->task_design_linked_id_c);
        /*  $old_start_integer_val = $childBean->fetched_row['start_integer'];
        $new_start_integer_val = $childBean->start_integer;
        $old_end_integer_val = $childBean->fetched_row['end_integer'];
        $new_end_integer_val = $childBean->end_integer;
        $old_integer_units_val = $childBean->fetched_row['u_units_id_c'];
        $new_integer_units_val = $childBean->u_units_id_c; */
        /**Fetch the new tsd linked from parent TSD and add the new tsd into related tsd record */
        //$main_td_tsd_link =  $clonedBean->task_design_linked_id_c;
        // $task_design_linked_id_c = $tdBean->id;
        /** #1551 : 24 Nov 2021 : Update the calculation of child record if parent tsd start/end integer or integer units changes */
        $childBean->save();
      }
      /** 08 dec 2021 : #1551 Bug :  To fix the issue of not updating the data for multiple child records , we are running the loop again basend on the parent record change
       * in this loop we are calculating the calculation for planned start/end date and linking the new ts records as well
       */
      $sql_1 = "SELECT tbl_tsd.id,tbl_tsd.name,tbl_tsd.deleted
      FROM taskd_task_design AS tbl_tsd
      LEFT JOIN taskd_task_design_cstm AS tbl_tsd_cstm ON tbl_tsd.id = tbl_tsd_cstm.id_c
      WHERE tbl_tsd_cstm.parent_td_id_c='" . $bean->id . "' AND tbl_tsd.deleted=0";
      $result_1 = $db->query($sql_1);
      $clonedBean_1 = BeanFactory::getBean('TaskD_Task_Design', $bean->id);
      $u_units_id_c = $clonedBean_1->u_units_id_c;
      $start_integer = $clonedBean_1->start_integer;
      $end_integer = $clonedBean_1->end_integer;
      $task_type = $clonedBean_1->task_type;
      $custom_task = $clonedBean_1->custom_task;
      $standard_task = $clonedBean_1->standard_task;
      $time_window = $clonedBean_1->time_window;
      $actual_datetime = $clonedBean_1->actual_datetime;
      $scheduled_start_datetime = $clonedBean_1->scheduled_start_datetime;
      $scheduled_end_datetime = $clonedBean_1->scheduled_end_datetime;
      $category = $clonedBean_1->category;
      $equipment_required = $clonedBean_1->equipment_required;
      $description = $clonedBean_1->description;
      $phase_c = $clonedBean_1->phase_c;
      $relative = $clonedBean_1->relative;
      $days_from_initial_task = $clonedBean_1->days_from_initial_task;


      /****07 dec 2021 */
      for ($i = 0; $i < $result_1->num_rows; $i++) {
        //while ($row = $db->fetchByAssoc($result_1)) {
        $row_1 = $db->fetchByAssoc($result_1);
        $relatedTSid_1 = $row_1['id'];
        /** clone data from main parent record */
        /**Get the child record id and save the data from main record into related record*/
        $childBean_1 = BeanFactory::getBean('TaskD_Task_Design', $relatedTSid_1);
        $childBean_1->load_relationship('taskd_task_design_taskd_task_design_1');

        $wpid = $childBean_1->m03_work_product_taskd_task_design_1m03_work_product_ida;
        $wpname = $childBean_1->m03_work_product_taskd_task_design_1_name;
        $tsName =  $childBean_1->anml_animals_taskd_task_design_1_name;
        $tsID =  $childBean_1->anml_animals_taskd_task_design_1anml_animals_ida;
        $childBean_1->u_units_id_c = $u_units_id_c;
        $childBean_1->start_integer = $start_integer;
        $childBean_1->end_integer = $end_integer;
        $childBean_1->task_type = $task_type;
        $childBean_1->custom_task = $custom_task;
        $childBean_1->standard_task = $standard_task;
        $childBean_1->time_window = $time_window;
        $childBean_1->actual_datetime = $actual_datetime;
        $childBean_1->scheduled_start_datetime = $scheduled_start_datetime;
        $childBean_1->scheduled_end_datetime = $scheduled_end_datetime;
        $childBean_1->category = $category;
        $childBean_1->equipment_required = $equipment_required;
        $childBean_1->description = $description;
        $childBean_1->phase_c = $phase_c;
        $childBean_1->relative = $relative;
        $childBean_1->days_from_initial_task = $days_from_initial_task;

        $childBean_1->order_2_c = $clonedBean_1->order_2_c;
        $childBean_1->type_of_personnel_2_c = $clonedBean_1->type_of_personnel_2_c;
        $childBean_1->duration_integer_min_c = $clonedBean_1->duration_integer_min_c;
        $childBean_1->other_equipment_c = $clonedBean_1->other_equipment_c;
        //$tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $childBean_1->taskd_task_design_taskd_task_design_1taskd_task_design_ida);
        $tdBean_1 = BeanFactory::retrieveBean('TaskD_Task_Design', $childBean_1->taskd_task_design_taskd_task_design_1taskd_task_design_ida);
        $old_start_integer_val = $childBean_1->fetched_row['start_integer'];
        $new_start_integer_val = $childBean_1->start_integer;
        $old_end_integer_val = $childBean_1->fetched_row['end_integer'];
        $new_end_integer_val = $childBean_1->end_integer;
        $old_integer_units_val = $childBean_1->fetched_row['u_units_id_c'];
        $new_integer_units_val = $childBean_1->u_units_id_c;
        /**Fetch the new tsd linked from parent TSD and add the new tsd into related tsd record */
        $main_td_tsd_link =  $clonedBean->task_design_linked_id_c;
        $task_design_linked_id_c = $tdBean_1->id;
        /**If  new tsd linked to parent tsd record , then it will linked into related copied tsd also */
        if ($main_td_tsd_link != $task_design_linked_id_c) {
          $uniqId = create_guid();
          /**Create a new TSD record of task design linked to  linked test system */
          $clonedBean_linked = BeanFactory::retrieveBean('TaskD_Task_Design', $main_td_tsd_link);
          $clonedBean_linked->load_relationship("anml_animals_taskd_task_design_1");
          /**Get the child record id and save the data from main record into related record*/
          $childBean_new = BeanFactory::newBean('TaskD_Task_Design');
          $tsid_new = create_guid();
          $childBean_new->id = $tsid_new;
          $childBean_new->new_with_id = true;
          $custom_task_link = $clonedBean_linked->custom_task;
          $standard_task_link = $clonedBean_linked->standard_task;
          $usda_id_name_new = $clonedBean_linked->usda_id_c;

          if ($clonedBean_linked->task_type == 'Custom') {
            $standard_task_link = '';
            $standard_task_val_linked = $custom_task_link;
          } else if ($clonedBean_linked->task_type == 'Standard') {
            $custom_task_link = '';
            $standard_task_val_linked = $standard_task_link;
          }
          if ($usda_id_name_new != '') {
            $chilbean_name_new = $wpname . ' ' . $tsName . ' ' . $usda_id_name_new . ' ' . $standard_task_val_linked;
          } else {
            $chilbean_name_new = $wpname . ' ' . $tsName . ' ' . $standard_task_val_linked;
          }
          $name_without_space = str_replace(' ', '', $chilbean_name_new);
          $chilbean_name_new_dblspace = $wpname . ' ' . $tsName . '  ' . $standard_task_val_linked;

          $childBean_new->name = $chilbean_name_new;
          /**Check the record already exist for same name  */
          /* $sqlGettsname = "SELECT id,name FROM taskd_task_design where name='$chilbean_name_new' or name='$chilbean_name_new_dblspace' and deleted=0"; */
          $sqlGettsname = "SELECT id,name FROM taskd_task_design where REPLACE(name, ' ', '')='$name_without_space' and deleted=0";
          $resultsSqlGettsid = $db->query($sqlGettsname);
          $rowSqlGetTsId = $db->fetchByAssoc($resultsSqlGettsid);
          $Ts_id = $rowSqlGetTsId['id'];
          $Ts_name = $rowSqlGetTsId['name'];
          /*If there is no such tsd record exist then below code will create new tsd record and will link it to related record */
          if ($Ts_id != '') {
            /**Check if the tsd already linked to chil record, if yes then update the existing else insert it */
            $sqlGetlinkedTS = "SELECT * 
            FROM taskd_task_design_taskd_task_design_1_c 
            where taskd_task_design_taskd_task_design_1taskd_task_design_idb='$childBean_1->id' and deleted=0";
            $resultsSqlGetlinkedTS = $db->query($sqlGetlinkedTS);
            $rowSqlGetLinkedId = $db->fetchByAssoc($resultsSqlGetlinkedTS);
            $TsRecid = $rowSqlGetLinkedId['taskd_task_design_taskd_task_design_1taskd_task_design_ida'];
            if ($rowSqlGetLinkedId != '') {
              $sqlUpdateRelationship = "Update taskd_task_design_taskd_task_design_1_c
              SET taskd_task_design_taskd_task_design_1taskd_task_design_ida='$Ts_id',taskd_task_design_taskd_task_design_1taskd_task_design_idb='$childBean_1->id'
              WHERE taskd_task_design_taskd_task_design_1taskd_task_design_idb= '$childBean_1->id'";
              $resultsSqlGetTaskDesignIds = $db->query($sqlUpdateRelationship);
            } else {
              $sqlInsertRelationship = "INSERT INTO `taskd_task_design_taskd_task_design_1_c` 
                         (`id`, `date_modified`, `deleted`, `taskd_task_design_taskd_task_design_1taskd_task_design_ida`, `taskd_task_design_taskd_task_design_1taskd_task_design_idb`) 
                         VALUES ('$uniqId', now(), '0', '$Ts_id','$childBean_1->id')";
              $resultsSqlGetTaskDesignIds = $db->query($sqlInsertRelationship);
            }
            /** */
          }
        }

        /** #1551 : 24 Nov 2021 : Update the calculation of child record if parent tsd start/end integer or integer units changes */
        if (($old_start_integer_val != $new_start_integer_val || $old_end_integer_val != $new_end_integer_val || $old_integer_units_val != $new_integer_units_val)) {
          $unit_Bean = BeanFactory::getBean('U_Units', $childBean_1->u_units_id_c);
          $unit_Bean_name = $unit_Bean->name;
          if ($unit_Bean_name == "Day") {
            $integer_u = 'days';
          } else if ($unit_Bean_name == "Hour") {
            $integer_u = 'hours';
          } else {
            $integer_u = 'minutes';
          }
          $tdBean_2 = BeanFactory::retrieveBean('TaskD_Task_Design', $childBean_1->taskd_task_design_taskd_task_design_1taskd_task_design_ida);
          if ($tdBean_2->actual_datetime == "") {
            if (($childBean_1->type_2 == "Actual SP" || $childBean_1->type_2 == "Actual") && $childBean_1->relative == "1st Tier") {
              if (($old_start_integer_val != $new_start_integer_val && $tdBean_2->scheduled_start_datetime != '') || ($old_integer_units_val != $new_integer_units_val && $tdBean_2->scheduled_start_datetime != '')) {
                $result = date($tdBean_2->scheduled_start_datetime);
                $result = strtotime($result . '+' . $childBean_1->start_integer . ' ' . $integer_u);
                $planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
                $childBean_1->planned_start_datetime_1st = $planned_start_datetime_1st;
              }
              if (($old_end_integer_val != $new_end_integer_val && $tdBean_2->scheduled_end_datetime != '') || ($old_integer_units_val != $new_integer_units_val && $tdBean_2->scheduled_end_datetime != '')) {
                $result = date($tdBean_2->scheduled_end_datetime);
                $result = strtotime($result . '+' . $childBean_1->end_integer . ' ' . $integer_u);
                $planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
                $childBean_1->planned_end_datetime_1st = $planned_end_datetime_1st;
              }
            }
            if (($childBean_1->type_2 == "Actual SP" || $childBean_1->type_2 == "Actual") && $childBean_1->relative == "2nd Tier") {
              if (($old_start_integer_val != $new_start_integer_val && $childBean_1->planned_start_datetime_2nd != "") || ($old_integer_units_val != $new_integer_units_val && $childBean_1->planned_start_datetime_2nd != "")) {
                $result = date($tdBean_2->planned_start_datetime_1st);
                $result = strtotime($result . '+' . $childBean_1->start_integer . ' ' . $integer_u);
                $planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
                $childBean_1->planned_start_datetime_2nd = $planned_start_datetime_2nd;
              }
              if (($old_end_integer_val != $new_end_integer_val && $childBean_1->planned_end_datetime_2nd != "") || ($old_integer_units_val != $new_integer_units_val && $childBean_1->planned_end_datetime_2nd != "")) {
                $result = date($tdBean_2->planned_end_datetime_1st);
                $result = strtotime($result . '+' . $childBean_1->end_integer . ' ' . $integer_u);
                $planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
                $childBean_1->planned_end_datetime_2nd = $planned_end_datetime_2nd;
              }
            }
          } else if ($tdBean_2->actual_datetime != "") {
            if (($childBean_1->type_2 == "Actual SP" || $childBean_1->type_2 == "Actual") && $childBean_1->relative == "1st Tier") {
              if (($old_start_integer_val != $new_start_integer_val) || ($old_integer_units_val != $new_integer_units_val)) {
                $result = date($tdBean_2->actual_datetime);
                $result = strtotime($result . '+' . $childBean_1->start_integer . ' ' . $integer_u);
                $planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
                $childBean_1->planned_start_datetime_1st = $planned_start_datetime_1st;
              }
              if (($old_end_integer_val != $new_end_integer_val)  || ($old_integer_units_val != $new_integer_units_val)) {
                $result = date($tdBean_2->actual_datetime);
                $result = strtotime($result . '+' . $childBean_1->end_integer . ' ' . $integer_u);
                $planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
                $childBean_1->planned_end_datetime_1st = $planned_end_datetime_1st;
              }
            }
            if (($childBean_1->type_2 == "Actual SP" || $childBean_1->type_2 == "Actual") && $childBean_1->relative == "2nd Tier") {
              if (($old_start_integer_val != $new_start_integer_val && $childBean_1->planned_start_datetime_2nd != "") || ($old_integer_units_val != $new_integer_units_val && $childBean_1->planned_start_datetime_2nd != "")) {
                $result = date($tdBean_2->planned_start_datetime_1st);
                $result = strtotime($result . '+' . $childBean_1->start_integer . ' ' . $integer_u);
                $planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
                $childBean_1->planned_start_datetime_2nd = $planned_start_datetime_2nd;
              }
              if (($old_end_integer_val != $new_end_integer_val && $childBean_1->planned_end_datetime_2nd != "")  || ($old_integer_units_val != $new_integer_units_val && $childBean_1->planned_end_datetime_2nd != "")) {
                $result = date($tdBean_2->planned_end_datetime_1st);
                $result = strtotime($result . '+' . $childBean_1->end_integer . ' ' . $integer_u);
                $planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
                $childBean_1->planned_end_datetime_2nd = $planned_end_datetime_2nd;
              }
            }
          }
          /**/ ///////////////////////// */
          /**Get the data of parent record whenever parent record is updating*/
          $childBean_1->save();
        }
      }
      /**/ /////////////// */

      //}
    }
  }
}
