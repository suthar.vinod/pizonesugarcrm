<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class TimeWindowCalculationHook
{
    function TimeWindowCalculation($bean, $event, $arguments)
    {
        $unit_id = $bean->u_units_id_c;
        $unit_Bean = BeanFactory::getBean('U_Units', $unit_id);
        $unit_Bean_name = $unit_Bean->name;
       /*  $GLOBALS['log']->fatal(' TimeWindowCalculation Line 13 ' . $unit_Bean_name);
        $GLOBALS['log']->fatal(' TimeWindowCalculation Line 14 ' . $bean->start_integer);
        $GLOBALS['log']->fatal(' TimeWindowCalculation Line 15 ' . $bean->end_integer); */
        if((!empty($unit_Bean_name)) && (!empty($bean->start_integer)) && (!empty($bean->end_integer))) {
            $bean->time_window = $unit_Bean_name . " " . $bean->start_integer . "-" . $bean->end_integer;
        }
        
    }
}