<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/gd_group_design_taskd_task_design_1_TaskD_Task_Design.php

// created: 2021-01-19 13:36:34
$dictionary["TaskD_Task_Design"]["fields"]["gd_group_design_taskd_task_design_1"] = array (
  'name' => 'gd_group_design_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'gd_group_design_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'GD_Group_Design',
  'bean_name' => 'GD_Group_Design',
  'side' => 'right',
  'vname' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'id_name' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
  'link-type' => 'one',
);
$dictionary["TaskD_Task_Design"]["fields"]["gd_group_design_taskd_task_design_1_name"] = array (
  'name' => 'gd_group_design_taskd_task_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'save' => true,
  'id_name' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
  'link' => 'gd_group_design_taskd_task_design_1',
  'table' => 'gd_group_design',
  'module' => 'GD_Group_Design',
  'rname' => 'name',
);
$dictionary["TaskD_Task_Design"]["fields"]["gd_group_design_taskd_task_design_1gd_group_design_ida"] = array (
  'name' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID',
  'id_name' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
  'link' => 'gd_group_design_taskd_task_design_1',
  'table' => 'gd_group_design',
  'module' => 'GD_Group_Design',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/m03_work_product_taskd_task_design_1_TaskD_Task_Design.php

// created: 2021-01-19 13:28:41
$dictionary["TaskD_Task_Design"]["fields"]["m03_work_product_taskd_task_design_1"] = array (
  'name' => 'm03_work_product_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'id_name' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["TaskD_Task_Design"]["fields"]["m03_work_product_taskd_task_design_1_name"] = array (
  'name' => 'm03_work_product_taskd_task_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
  'link' => 'm03_work_product_taskd_task_design_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["TaskD_Task_Design"]["fields"]["m03_work_product_taskd_task_design_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID',
  'id_name' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
  'link' => 'm03_work_product_taskd_task_design_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/anml_animals_taskd_task_design_1_TaskD_Task_Design.php

// created: 2021-01-19 13:39:59
$dictionary["TaskD_Task_Design"]["fields"]["anml_animals_taskd_task_design_1"] = array (
  'name' => 'anml_animals_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'anml_animals_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'id_name' => 'anml_animals_taskd_task_design_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["TaskD_Task_Design"]["fields"]["anml_animals_taskd_task_design_1_name"] = array (
  'name' => 'anml_animals_taskd_task_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_taskd_task_design_1anml_animals_ida',
  'link' => 'anml_animals_taskd_task_design_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["TaskD_Task_Design"]["fields"]["anml_animals_taskd_task_design_1anml_animals_ida"] = array (
  'name' => 'anml_animals_taskd_task_design_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID',
  'id_name' => 'anml_animals_taskd_task_design_1anml_animals_ida',
  'link' => 'anml_animals_taskd_task_design_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/taskd_task_design_taskd_task_design_1_TaskD_Task_Design.php

// created: 2021-01-19 13:43:24
$dictionary["TaskD_Task_Design"]["fields"]["taskd_task_design_taskd_task_design_1"] = array (
  'name' => 'taskd_task_design_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'taskd_task_design_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'vname' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_L_TITLE',
  'id_name' => 'taskd_task_design_taskd_task_design_1taskd_task_design_idb',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["TaskD_Task_Design"]["fields"]["taskd_task_design_taskd_task_design_1_right"] = array (
  'name' => 'taskd_task_design_taskd_task_design_1_right',
  'type' => 'link',
  'relationship' => 'taskd_task_design_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'side' => 'right',
  'vname' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_R_TITLE',
  'id_name' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
  'link-type' => 'one',
);
$dictionary["TaskD_Task_Design"]["fields"]["taskd_task_design_taskd_task_design_1_name"] = array (
  'name' => 'taskd_task_design_taskd_task_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_L_TITLE',
  'save' => true,
  'id_name' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
  'link' => 'taskd_task_design_taskd_task_design_1_right',
  'table' => 'taskd_task_design',
  'module' => 'TaskD_Task_Design',
  'rname' => 'name',
);
$dictionary["TaskD_Task_Design"]["fields"]["taskd_task_design_taskd_task_design_1taskd_task_design_ida"] = array (
  'name' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_R_TITLE_ID',
  'id_name' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
  'link' => 'taskd_task_design_taskd_task_design_1_right',
  'table' => 'taskd_task_design',
  'module' => 'TaskD_Task_Design',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_days_from_initial_task.php

 // created: 2021-01-19 13:57:07
$dictionary['TaskD_Task_Design']['fields']['days_from_initial_task']['importable']='false';
$dictionary['TaskD_Task_Design']['fields']['days_from_initial_task']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['days_from_initial_task']['duplicate_merge_dom_value']=0;
$dictionary['TaskD_Task_Design']['fields']['days_from_initial_task']['calculated']='true';
$dictionary['TaskD_Task_Design']['fields']['days_from_initial_task']['formula']='ifElse(equal($actual_datetime,""),"",abs(subtract(daysUntil(related($taskd_task_design_taskd_task_design_1_right,"actual_datetime")),daysUntil($actual_datetime))))';
$dictionary['TaskD_Task_Design']['fields']['days_from_initial_task']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_name.php

 // created: 2021-06-10 07:19:07
$dictionary['TaskD_Task_Design']['fields']['name']['importable']='false';
$dictionary['TaskD_Task_Design']['fields']['name']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['TaskD_Task_Design']['fields']['name']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['name']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['name']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['name']['readonly']=true;
$dictionary['TaskD_Task_Design']['fields']['name']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_type_2.php

 // created: 2021-12-02 09:20:22

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_phase_c.php

 // created: 2021-12-02 09:33:31
$dictionary['TaskD_Task_Design']['fields']['phase_c']['labelValue']='Phase';
$dictionary['TaskD_Task_Design']['fields']['phase_c']['dependency']='isInList($standard_task,createList("Extract In","Extract Out","Provide Material"))';
$dictionary['TaskD_Task_Design']['fields']['phase_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['phase_c']['readonly_formula']='';
$dictionary['TaskD_Task_Design']['fields']['phase_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/m03_work_product_code_taskd_task_design_1_TaskD_Task_Design.php

// created: 2021-12-02 09:36:03
$dictionary["TaskD_Task_Design"]["fields"]["m03_work_product_code_taskd_task_design_1"] = array (
  'name' => 'm03_work_product_code_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_code_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Code',
  'bean_name' => 'M03_Work_Product_Code',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'id_name' => 'm03_work_p7fc0ct_code_ida',
  'link-type' => 'one',
);
$dictionary["TaskD_Task_Design"]["fields"]["m03_work_product_code_taskd_task_design_1_name"] = array (
  'name' => 'm03_work_product_code_taskd_task_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p7fc0ct_code_ida',
  'link' => 'm03_work_product_code_taskd_task_design_1',
  'table' => 'm03_work_product_code',
  'module' => 'M03_Work_Product_Code',
  'rname' => 'name',
);
$dictionary["TaskD_Task_Design"]["fields"]["m03_work_p7fc0ct_code_ida"] = array (
  'name' => 'm03_work_p7fc0ct_code_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID',
  'id_name' => 'm03_work_p7fc0ct_code_ida',
  'link' => 'm03_work_product_code_taskd_task_design_1',
  'table' => 'm03_work_product_code',
  'module' => 'M03_Work_Product_Code',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_planned_end_datetime_1st.php

 // created: 2021-12-02 09:43:56
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_1st']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_1st']['importable']='false';
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_1st']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_1st']['duplicate_merge_dom_value']='0';
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_1st']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_1st']['readonly']=true;
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_1st']['dependency']='and(isInList($type_2,createList("Actual","Actual SP")),equal($relative,"1st Tier"))';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_planned_end_datetime_2nd.php

 // created: 2021-12-02 09:44:36
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_2nd']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_2nd']['importable']='false';
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_2nd']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_2nd']['duplicate_merge_dom_value']='0';
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_2nd']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_2nd']['readonly']=true;
$dictionary['TaskD_Task_Design']['fields']['planned_end_datetime_2nd']['dependency']='and(isInList($type_2,createList("Actual","Actual SP")),equal($relative,"2nd Tier"))';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_planned_start_datetime_1st.php

 // created: 2021-12-02 09:45:03
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['importable']='false';
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['duplicate_merge_dom_value']='0';
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['readonly']=true;
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_1st']['dependency']='and(isInList($type_2,createList("Actual","Actual SP")),equal($relative,"1st Tier"))';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_planned_start_datetime_2nd.php

 // created: 2021-12-02 09:45:31
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['importable']='false';
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['duplicate_merge_dom_value']='0';
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['readonly']=true;
$dictionary['TaskD_Task_Design']['fields']['planned_start_datetime_2nd']['dependency']='and(isInList($type_2,createList("Actual","Actual SP")),equal($relative,"2nd Tier"))';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_scheduled_end_datetime.php

 // created: 2021-12-02 09:46:06
$dictionary['TaskD_Task_Design']['fields']['scheduled_end_datetime']['dependency']='and(isInList($type_2,createList("Actual","Actual SP")),equal($relative,"NA"))';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_scheduled_start_datetime.php

 // created: 2021-12-02 09:56:46
$dictionary['TaskD_Task_Design']['fields']['scheduled_start_datetime']['dependency']='and(isInList($type_2,createList("Actual","Actual SP")),equal($relative,"NA"))';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_gd_group_design_taskd_task_design_1gd_group_design_ida.php

 // created: 2021-12-03 13:10:25
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['name']='gd_group_design_taskd_task_design_1gd_group_design_ida';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['type']='id';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['source']='non-db';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['vname']='LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['id_name']='gd_group_design_taskd_task_design_1gd_group_design_ida';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['link']='gd_group_design_taskd_task_design_1';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['table']='gd_group_design';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['module']='GD_Group_Design';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['rname']='id';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['side']='right';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['hideacl']=true;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['audited']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1gd_group_design_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_gd_group_design_taskd_task_design_1_name.php

 // created: 2021-12-03 13:10:27
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['audited']=true;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['massupdate']=true;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['hidemassupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['duplicate_merge']='enabled';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['duplicate_merge_dom_value']='1';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['gd_group_design_taskd_task_design_1_name']['vname']='LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_m03_work_product_taskd_task_design_1m03_work_product_ida.php

 // created: 2021-12-03 13:11:34
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['name']='m03_work_product_taskd_task_design_1m03_work_product_ida';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['type']='id';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['source']='non-db';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['vname']='LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['id_name']='m03_work_product_taskd_task_design_1m03_work_product_ida';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['link']='m03_work_product_taskd_task_design_1';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['table']='m03_work_product';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['module']='M03_Work_Product';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['rname']='id';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['side']='right';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['hideacl']=true;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['audited']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1m03_work_product_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_m03_work_product_taskd_task_design_1_name.php

 // created: 2021-12-03 13:11:36
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['audited']=true;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['massupdate']=true;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['hidemassupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['duplicate_merge']='enabled';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['duplicate_merge_dom_value']='1';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['m03_work_product_taskd_task_design_1_name']['vname']='LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_anml_animals_taskd_task_design_1anml_animals_ida.php

 // created: 2021-12-03 13:12:23
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['name']='anml_animals_taskd_task_design_1anml_animals_ida';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['type']='id';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['source']='non-db';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['vname']='LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['id_name']='anml_animals_taskd_task_design_1anml_animals_ida';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['link']='anml_animals_taskd_task_design_1';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['table']='anml_animals';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['module']='ANML_Animals';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['rname']='id';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['side']='right';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['hideacl']=true;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['audited']=false;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1anml_animals_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_anml_animals_taskd_task_design_1_name.php

 // created: 2021-12-03 13:12:26
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1_name']['audited']=true;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1_name']['massupdate']=true;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1_name']['hidemassupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1_name']['duplicate_merge']='enabled';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1_name']['duplicate_merge_dom_value']='1';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1_name']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1_name']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1_name']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1_name']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['anml_animals_taskd_task_design_1_name']['vname']='LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_usda_id_c.php

 // created: 2021-12-07 10:47:31
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['duplicate_merge_dom_value']=0;
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['labelValue']='USDA ID';
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['calculated']='true';
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['formula']='related($anml_animals_taskd_task_design_1,"usda_id_c")';
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['enforced']='true';
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['dependency']='equal($type_2,"Actual")';
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['usda_id_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_other_equipment_c.php

 // created: 2022-01-25 08:26:06
$dictionary['TaskD_Task_Design']['fields']['other_equipment_c']['labelValue']='Other Equipment';
$dictionary['TaskD_Task_Design']['fields']['other_equipment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['other_equipment_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['other_equipment_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['other_equipment_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['other_equipment_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_duration_integer_min_c.php

 // created: 2022-03-15 07:29:47
$dictionary['TaskD_Task_Design']['fields']['duration_integer_min_c']['labelValue']='Duration Integer (Min)';
$dictionary['TaskD_Task_Design']['fields']['duration_integer_min_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['duration_integer_min_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['duration_integer_min_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['duration_integer_min_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['duration_integer_min_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_type_of_personnel_c.php

 // created: 2022-03-17 07:26:10
$dictionary['TaskD_Task_Design']['fields']['type_of_personnel_c']['labelValue']='Type of Personnel (Obsolete)';
$dictionary['TaskD_Task_Design']['fields']['type_of_personnel_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['type_of_personnel_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['type_of_personnel_c']['readonly_formula']='';
$dictionary['TaskD_Task_Design']['fields']['type_of_personnel_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_type_of_personnel_2_c.php

 // created: 2022-03-17 07:33:21
$dictionary['TaskD_Task_Design']['fields']['type_of_personnel_2_c']['labelValue']='Type of Personnel';
$dictionary['TaskD_Task_Design']['fields']['type_of_personnel_2_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['type_of_personnel_2_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['type_of_personnel_2_c']['readonly_formula']='';
$dictionary['TaskD_Task_Design']['fields']['type_of_personnel_2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_tech_assigned_c.php

 // created: 2022-03-22 07:15:15
$dictionary['TaskD_Task_Design']['fields']['tech_assigned_c']['labelValue']='Tech Assigned';
$dictionary['TaskD_Task_Design']['fields']['tech_assigned_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['tech_assigned_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['tech_assigned_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['tech_assigned_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['tech_assigned_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_initial_upon_completion_c.php

 // created: 2022-03-22 07:16:46
$dictionary['TaskD_Task_Design']['fields']['initial_upon_completion_c']['labelValue']='Initial Upon Completion';
$dictionary['TaskD_Task_Design']['fields']['initial_upon_completion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['initial_upon_completion_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['initial_upon_completion_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['initial_upon_completion_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['initial_upon_completion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_data_reviewed_by_c.php

 // created: 2022-03-22 07:17:59
$dictionary['TaskD_Task_Design']['fields']['data_reviewed_by_c']['labelValue']='Data Reviewed By';
$dictionary['TaskD_Task_Design']['fields']['data_reviewed_by_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['data_reviewed_by_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['data_reviewed_by_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['data_reviewed_by_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['data_reviewed_by_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_audit_phase_c.php

 // created: 2022-03-24 08:54:25
$dictionary['TaskD_Task_Design']['fields']['audit_phase_c']['labelValue']='Audit Phase';
$dictionary['TaskD_Task_Design']['fields']['audit_phase_c']['dependency']='equal($type_2,"Plan SP")';
$dictionary['TaskD_Task_Design']['fields']['audit_phase_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['audit_phase_c']['readonly_formula']='';
$dictionary['TaskD_Task_Design']['fields']['audit_phase_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_single_task_c.php

 // created: 2022-03-24 09:11:06
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['duplicate_merge_dom_value']=0;
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['labelValue']='Single Task';
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['calculated']='true';
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['formula']='ifElse(equal($task_type,"Custom"),$custom_task,getDropdownValue("standard_task_list",$standard_task))';
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['enforced']='true';
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['single_task_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_category.php

 // created: 2022-04-21 08:33:58

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_actual_datetime.php

 // created: 2022-04-21 08:34:57
$dictionary['TaskD_Task_Design']['fields']['actual_datetime']['dependency']='and(isInList($type_2,createList("Actual","Actual SP")),not(equal($category,"Unscheduled Task")))';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_audit_2_c.php

 // created: 2022-04-21 08:36:52
$dictionary['TaskD_Task_Design']['fields']['audit_2_c']['labelValue']='Audit?';
$dictionary['TaskD_Task_Design']['fields']['audit_2_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['audit_2_c']['dependency']='equal(related($m03_work_product_taskd_task_design_1,"work_product_compliance_c"),"GLP")';
$dictionary['TaskD_Task_Design']['fields']['audit_2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/bid_batch_id_taskd_task_design_1_TaskD_Task_Design.php

// created: 2022-04-26 06:47:59
$dictionary["TaskD_Task_Design"]["fields"]["bid_batch_id_taskd_task_design_1"] = array (
  'name' => 'bid_batch_id_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'BID_Batch_ID',
  'bean_name' => 'BID_Batch_ID',
  'side' => 'right',
  'vname' => 'LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'id_name' => 'bid_batch_id_taskd_task_design_1bid_batch_id_ida',
  'link-type' => 'one',
);
$dictionary["TaskD_Task_Design"]["fields"]["bid_batch_id_taskd_task_design_1_name"] = array (
  'name' => 'bid_batch_id_taskd_task_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_BID_BATCH_ID_TITLE',
  'save' => true,
  'id_name' => 'bid_batch_id_taskd_task_design_1bid_batch_id_ida',
  'link' => 'bid_batch_id_taskd_task_design_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'name',
);
$dictionary["TaskD_Task_Design"]["fields"]["bid_batch_id_taskd_task_design_1bid_batch_id_ida"] = array (
  'name' => 'bid_batch_id_taskd_task_design_1bid_batch_id_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID',
  'id_name' => 'bid_batch_id_taskd_task_design_1bid_batch_id_ida',
  'link' => 'bid_batch_id_taskd_task_design_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_single_date_c.php

 // created: 2022-05-05 09:22:47
$dictionary['TaskD_Task_Design']['fields']['single_date_c']['labelValue']='(Obsolete) Single Date';
$dictionary['TaskD_Task_Design']['fields']['single_date_c']['formula']='ifElse(equal($relative,"NA"),$actual_datetime,ifElse(equal($relative,"1st Tier"),$planned_start_datetime_1st,$planned_start_datetime_2nd))';
$dictionary['TaskD_Task_Design']['fields']['single_date_c']['enforced']='false';
$dictionary['TaskD_Task_Design']['fields']['single_date_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['single_date_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['single_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_single_date_2_c.php

 // created: 2022-05-05 20:19:12
$dictionary['TaskD_Task_Design']['fields']['single_date_2_c']['labelValue']='Single Date';
$dictionary['TaskD_Task_Design']['fields']['single_date_2_c']['formula']='ifElse(not(equal($actual_datetime,"")),$actual_datetime,
ifElse(not(equal($planned_start_datetime_1st,"")),$planned_start_datetime_1st,
ifElse(not(equal($planned_start_datetime_2nd,"")),$planned_start_datetime_2nd,$scheduled_start_datetime)))';
$dictionary['TaskD_Task_Design']['fields']['single_date_2_c']['enforced']='false';
$dictionary['TaskD_Task_Design']['fields']['single_date_2_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['single_date_2_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['single_date_2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_cron_updated_c.php

 // created: 2022-05-06 06:35:33
$dictionary['TaskD_Task_Design']['fields']['cron_updated_c']['labelValue']='cron updated';
$dictionary['TaskD_Task_Design']['fields']['cron_updated_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['cron_updated_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['cron_updated_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['cron_updated_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['cron_updated_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_parent_td_id_c.php

 // created: 2022-05-27 05:30:09
$dictionary['TaskD_Task_Design']['fields']['parent_td_id_c']['labelValue']='parent td id';
$dictionary['TaskD_Task_Design']['fields']['parent_td_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['parent_td_id_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['parent_td_id_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['parent_td_id_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['parent_td_id_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_task_design_linked_id_c.php

 // created: 2022-05-27 05:31:37
$dictionary['TaskD_Task_Design']['fields']['task_design_linked_id_c']['labelValue']='task design linked id c';
$dictionary['TaskD_Task_Design']['fields']['task_design_linked_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['task_design_linked_id_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['task_design_linked_id_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['task_design_linked_id_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['task_design_linked_id_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_recurring_td_c.php

 // created: 2022-06-14 06:13:23
$dictionary['TaskD_Task_Design']['fields']['recurring_td_c']['labelValue']='Recurring TD?';
$dictionary['TaskD_Task_Design']['fields']['recurring_td_c']['dependency']='and(isInList($type_2,createList("Plan","Actual SP")),isInList($relative,createList("1st Tier","2nd Tier")))';
$dictionary['TaskD_Task_Design']['fields']['recurring_td_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['recurring_td_c']['readonly_formula']='';
$dictionary['TaskD_Task_Design']['fields']['recurring_td_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_recurrence_pattern_days_c.php

 // created: 2022-06-14 06:15:48
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_days_c']['labelValue']='Recurrence Pattern - Recur Every X Days';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_days_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_days_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_days_c']['dependency']='equal($recurring_td_c,"Yes")';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_days_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_days_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_recurrence_pattern_duration_c.php

 // created: 2022-06-14 06:18:11
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_duration_c']['labelValue']='Recurrence Pattern - Duration of Recurrence in Days';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_duration_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_duration_c']['dependency']='equal($recurring_td_c,"Yes")';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_duration_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['recurrence_pattern_duration_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_start_day_of_recurrence_c.php

 // created: 2022-06-14 06:35:07
$dictionary['TaskD_Task_Design']['fields']['start_day_of_recurrence_c']['labelValue']='Start Day of Recurrence';
$dictionary['TaskD_Task_Design']['fields']['start_day_of_recurrence_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['start_day_of_recurrence_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['start_day_of_recurrence_c']['dependency']='equal($recurring_td_c,"Yes")';
$dictionary['TaskD_Task_Design']['fields']['start_day_of_recurrence_c']['required_formula']='equal($recurring_td_c,"Yes")';
$dictionary['TaskD_Task_Design']['fields']['start_day_of_recurrence_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_order_start_integer_c.php

 // created: 2022-06-14 06:37:09
$dictionary['TaskD_Task_Design']['fields']['order_start_integer_c']['labelValue']='Order Start Integer';
$dictionary['TaskD_Task_Design']['fields']['order_start_integer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['order_start_integer_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['order_start_integer_c']['dependency']='equal($recurring_td_c,"Yes")';
$dictionary['TaskD_Task_Design']['fields']['order_start_integer_c']['required_formula']='equal($recurring_td_c,"Yes")';
$dictionary['TaskD_Task_Design']['fields']['order_start_integer_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_order_2_c.php

 // created: 2022-06-14 06:45:00
$dictionary['TaskD_Task_Design']['fields']['order_2_c']['labelValue']='Order';
$dictionary['TaskD_Task_Design']['fields']['order_2_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['order_2_c']['dependency']='not(equal($recurring_td_c,"Yes"))';
$dictionary['TaskD_Task_Design']['fields']['order_2_c']['required_formula']='not(equal($recurring_td_c,"Yes"))';
$dictionary['TaskD_Task_Design']['fields']['order_2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_bid_batch_id_taskd_task_design_1bid_batch_id_ida.php

 // created: 2022-09-22 10:42:25
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['name']='bid_batch_id_taskd_task_design_1bid_batch_id_ida';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['type']='id';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['source']='non-db';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['vname']='LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE_ID';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['id_name']='bid_batch_id_taskd_task_design_1bid_batch_id_ida';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['link']='bid_batch_id_taskd_task_design_1';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['table']='bid_batch_id';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['module']='BID_Batch_ID';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['rname']='id';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['side']='right';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['duplicate_merge']='disabled';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['hideacl']=true;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['audited']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1bid_batch_id_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_bid_batch_id_taskd_task_design_1_name.php

 // created: 2022-09-22 10:42:25
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['audited']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['massupdate']=true;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['hidemassupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['duplicate_merge']='enabled';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['duplicate_merge_dom_value']='1';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['bid_batch_id_taskd_task_design_1_name']['vname']='LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_creation_mode_c.php

 // created: 2022-09-26 11:57:32
$dictionary['TaskD_Task_Design']['fields']['creation_mode_c']['labelValue']='creation mode c';
$dictionary['TaskD_Task_Design']['fields']['creation_mode_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TaskD_Task_Design']['fields']['creation_mode_c']['enforced']='';
$dictionary['TaskD_Task_Design']['fields']['creation_mode_c']['dependency']='';
$dictionary['TaskD_Task_Design']['fields']['creation_mode_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['creation_mode_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_custom_task.php

 // created: 2022-10-21 12:45:47
$dictionary['TaskD_Task_Design']['fields']['custom_task']['massupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_equipment_required.php

 // created: 2022-10-27 05:53:10
$dictionary['TaskD_Task_Design']['fields']['equipment_required']['default']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_completion_status_c.php

 // created: 2022-11-08 05:47:09
$dictionary['TaskD_Task_Design']['fields']['completion_status_c']['labelValue']='Completion Status';
$dictionary['TaskD_Task_Design']['fields']['completion_status_c']['dependency']='equal($standard_task,"Sample Preparation")';
$dictionary['TaskD_Task_Design']['fields']['completion_status_c']['required_formula']='';
$dictionary['TaskD_Task_Design']['fields']['completion_status_c']['readonly_formula']='';
$dictionary['TaskD_Task_Design']['fields']['completion_status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_description.php

 // created: 2022-11-22 06:05:36
$dictionary['TaskD_Task_Design']['fields']['description']['audited']=true;
$dictionary['TaskD_Task_Design']['fields']['description']['massupdate']=true;
$dictionary['TaskD_Task_Design']['fields']['description']['hidemassupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['description']['comments']='Full text of the note';
$dictionary['TaskD_Task_Design']['fields']['description']['duplicate_merge']='enabled';
$dictionary['TaskD_Task_Design']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['TaskD_Task_Design']['fields']['description']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['description']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['TaskD_Task_Design']['fields']['description']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['description']['rows']='6';
$dictionary['TaskD_Task_Design']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_start_integer.php

 // created: 2022-12-20 08:57:10
$dictionary['TaskD_Task_Design']['fields']['start_integer']['dependency']='or(and(isInList($recurring_td_c,createList("No","")),equal(related($m03_work_product_code_taskd_task_design_1,"name"),""),isInList($relative,createList("1st Tier","2nd Tier"))),and(isInList($recurring_td_c,createList("No","")),not(equal(related($m03_work_product_code_taskd_task_design_1,"name"),"")),isInList($relative,createList("1st Tier","2nd Tier")),or(isInList($standard_task,createList("Extract Out","Provide Material","Sample Preparation")),isInList($task_type,createList("Custom")))))';
$dictionary['TaskD_Task_Design']['fields']['start_integer']['required_formula']='or(and(isInList($recurring_td_c,createList("No","")),equal(related($m03_work_product_code_taskd_task_design_1,"name"),""),isInList($relative,createList("1st Tier","2nd Tier"))),and(isInList($recurring_td_c,createList("No","")),not(equal(related($m03_work_product_code_taskd_task_design_1,"name"),"")),isInList($relative,createList("1st Tier","2nd Tier")),or(isInList($standard_task,createList("Extract Out","Provide Material","Sample Preparation")),isInList($task_type,createList("Custom")))))';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_end_integer.php

 // created: 2022-12-20 09:00:18
$dictionary['TaskD_Task_Design']['fields']['end_integer']['dependency']='or(and(isInList($recurring_td_c,createList("No","")),equal(related($m03_work_product_code_taskd_task_design_1,"name"),""),isInList($relative,createList("1st Tier","2nd Tier"))),and(isInList($recurring_td_c,createList("No","")),not(equal(related($m03_work_product_code_taskd_task_design_1,"name"),"")),isInList($relative,createList("1st Tier","2nd Tier")),or(isInList($standard_task,createList("Extract Out","Provide Material","Sample Preparation")),isInList($task_type,createList("Custom")))))';
$dictionary['TaskD_Task_Design']['fields']['end_integer']['required_formula']='or(and(isInList($recurring_td_c,createList("No","")),equal(related($m03_work_product_code_taskd_task_design_1,"name"),""),isInList($relative,createList("1st Tier","2nd Tier"))),and(isInList($recurring_td_c,createList("No","")),not(equal(related($m03_work_product_code_taskd_task_design_1,"name"),"")),isInList($relative,createList("1st Tier","2nd Tier")),or(isInList($standard_task,createList("Extract Out","Provide Material","Sample Preparation")),isInList($task_type,createList("Custom")))))';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_integer_units.php

 // created: 2022-12-20 09:01:20
$dictionary['TaskD_Task_Design']['fields']['integer_units']['importable']='true';
$dictionary['TaskD_Task_Design']['fields']['integer_units']['readonly']=false;
$dictionary['TaskD_Task_Design']['fields']['integer_units']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['integer_units']['dependency']='or(and(isInList($recurring_td_c,createList("No","")),equal(related($m03_work_product_code_taskd_task_design_1,"name"),""),isInList($relative,createList("1st Tier","2nd Tier"))),and(isInList($recurring_td_c,createList("No","")),not(equal(related($m03_work_product_code_taskd_task_design_1,"name"),"")),isInList($relative,createList("1st Tier","2nd Tier")),or(isInList($standard_task,createList("Extract Out","Provide Material","Sample Preparation")),isInList($task_type,createList("Custom")))))';
$dictionary['TaskD_Task_Design']['fields']['integer_units']['required_formula']='or(and(isInList($recurring_td_c,createList("No","")),equal(related($m03_work_product_code_taskd_task_design_1,"name"),""),isInList($relative,createList("1st Tier","2nd Tier"))),and(isInList($recurring_td_c,createList("No","")),not(equal(related($m03_work_product_code_taskd_task_design_1,"name"),"")),isInList($relative,createList("1st Tier","2nd Tier")),or(isInList($standard_task,createList("Extract Out","Provide Material","Sample Preparation")),isInList($task_type,createList("Custom")))))';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_u_units_id_c.php

 // created: 2022-12-20 09:01:20
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['required']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['name']='u_units_id_c';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['vname']='LBL_INTEGER_UNITS_U_UNITS_ID';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['type']='id';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['massupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['hidemassupdate']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['no_default']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['comments']='';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['help']='';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['importable']='true';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['duplicate_merge']='enabled';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['duplicate_merge_dom_value']=1;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['audited']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['reportable']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['unified_search']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['merge_filter']='disabled';
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['pii']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['len']=36;
$dictionary['TaskD_Task_Design']['fields']['u_units_id_c']['size']='20';

 
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Vardefs/sugarfield_time_window.php

 // created: 2022-12-20 09:02:34
$dictionary['TaskD_Task_Design']['fields']['time_window']['required']=false;
$dictionary['TaskD_Task_Design']['fields']['time_window']['pii']=true;
$dictionary['TaskD_Task_Design']['fields']['time_window']['calculated']=false;
$dictionary['TaskD_Task_Design']['fields']['time_window']['readonly']=true;
$dictionary['TaskD_Task_Design']['fields']['time_window']['dependency']='or(and(isInList($recurring_td_c,createList("No","")),equal(related($m03_work_product_code_taskd_task_design_1,"name"),""),isInList($relative,createList("1st Tier","2nd Tier"))),and(isInList($recurring_td_c,createList("No","")),not(equal(related($m03_work_product_code_taskd_task_design_1,"name"),"")),isInList($relative,createList("1st Tier","2nd Tier")),or(isInList($standard_task,createList("Extract Out","Provide Material","Sample Preparation")),isInList($task_type,createList("Custom")))))';
$dictionary['TaskD_Task_Design']['fields']['time_window']['formula']='';
$dictionary['TaskD_Task_Design']['fields']['time_window']['enforced']=false;

 
?>
