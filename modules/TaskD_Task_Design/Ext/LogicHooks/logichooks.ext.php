<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/LogicHooks/empty_fields_hook.php


// $hook_array['before_save'][] = array(
//     '15',
//     'Empty Fields',
//     'custom/modules/TaskD_Task_Design/emptyFieldsHook.php',
//     'EmptyFieldsHook',
//     'emptyFields'
//  );
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/LogicHooks/singleDateCalculationHook.php



?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/LogicHooks/logic_hooks.php

$hook_version = 1;
$hook_array = array();
$hook_array['after_save'][] = array(
    11,
    'Set Custom Calculation',
    'custom/modules/TaskD_Task_Design/setCustomCalculation.php',
    'SetCustomCalculation',
    'setCalculation',
); 
/**01 dec 2021 for ticket #1551 */
$hook_array['after_save'][] = array(
    22,
    'Update Related Record',
    'custom/modules/TaskD_Task_Design/UpdateRelatedRecord.php',
    'UpdateRelatedRecord',
    'UpdateRelatedRec',
);
$hook_array['after_relationship_add'][] = array(
    1001,
    'Set Custom Calculation',
    'custom/modules/TaskD_Task_Design/setCustomCalculationAfterRelAdd.php',
    'setCustomCalculationAfterRelAdd',
    'setCalculationAfterRelAdd'
);
$hook_array['before_save'][] = array(
    '16',
    'Name field calculation',
    'custom/modules/TaskD_Task_Design/setCustomNameCalculation.php',
    'setCustomNameCalculation',
    'customnamecalculation'
 );
//  $hook_array['process_record'][] = array(
//     '10',
//     'delete duplicate Tier Audit log Record',
//     'custom/modules/TaskD_Task_Design/deleteCalculationAuditLog.php',
//     'deleteCalculationAuditLog',
//     'deleteAudit'
//  );
 $hook_array['after_retrieve'][] = array(
    1005,
    'delete duplicate Tier Audit log Record',
    'custom/modules/TaskD_Task_Design/deleteCalculationAuditLog.php',
    'deleteCalculationAuditLog',
    'deleteAudit'
 );

 $hook_array['before_save'][] = array(
    1006,
    'Time Window display discrepancy Calculation',
    'custom/modules/TaskD_Task_Design/customTimeWindowCalculation.php',
    'TimeWindowCalculationHook',
    'TimeWindowCalculation',
);
$hook_array['before_save'][] = array(
    1007,
    'Custom Single Date Calculation',
    'custom/modules/TaskD_Task_Design/customSingleDateCalculationnew.php',
    'SingleDateCalculationupdateHook',
    'SingleDateCalculationupdate',
);
$hook_array['after_save'][] = array(
    1008,
    'Set Custom Calculation Single Date',
    'custom/modules/TaskD_Task_Design/setCustomCalculationSingleDate.php',
    'setCustomCalculationSingleDateHook',
    'setCalculationSingleDate',
);
$hook_array['before_save'][] = array(
    1009,
    'Auto-create recurring TD records',
    'custom/modules/TaskD_Task_Design/create_auto_td_records.php',
    'create_auto_td_recordsHook',
    'create_auto_td_records',
);
?>
