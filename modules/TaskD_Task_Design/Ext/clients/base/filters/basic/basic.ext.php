<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-08-10 
$viewdefs['TaskD_Task_Design']['base']['filter']['basic']['filters'][] = array(
  'id' => 'FilterTaskDesign',
  'name' => 'Work Product',
  'filter_definition' => array(
    array(
      'm03_work_product_taskd_task_design_1_name'  => array(
        '$in' => '',
      ),
      'relative'  => array(
        '$in' => '',
      ),
    ),
  ),
  'editable' => true,
  'is_template' => true,
);


// created: 2020-08-10 
$viewdefs['TaskD_Task_Design']['base']['filter']['basic']['filters'][] = array(
  'id' => 'FilterTaskDesignBatchID',
  'name' => 'Batch ID',
  'filter_definition' => array(
    array(
      'bid_batch_id_taskd_task_design_1_name'  => array(
        '$in' => '',
      ),
      'relative'  => array(
        '$in' => '',
      ),
    ),
  ),
  'editable' => true,
  'is_template' => true,
);


// created: 2020-08-10 
$viewdefs['TaskD_Task_Design']['base']['filter']['basic']['filters'][] = array(
  'id' => 'FilterTaskDesigncode',
  'name' => 'Work Product Code',
  'filter_definition' => array(
    array(
      'm03_work_product_code_taskd_task_design_1_name'  => array(
        '$in' => '',
      ),
      'relative'  => array(
        '$in' => '',
      ),
    ),
  ),
  'editable' => true,
  'is_template' => true,
);
