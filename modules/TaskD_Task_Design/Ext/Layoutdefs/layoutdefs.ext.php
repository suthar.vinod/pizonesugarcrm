<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Layoutdefs/taskd_task_design_taskd_task_design_1_TaskD_Task_Design.php

 // created: 2021-08-14 06:08:31
$layout_defs["TaskD_Task_Design"]["subpanel_setup"]['taskd_task_design_taskd_task_design_1'] = array (
  'order' => 100,
  'module' => 'TaskD_Task_Design',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_R_TITLE',
  'get_subpanel_data' => 'taskd_task_design_taskd_task_design_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Layoutdefs/_overrideTaskD_Task_Design_subpanel_taskd_task_design_taskd_task_design_1.php

//auto-generated file DO NOT EDIT
$layout_defs['TaskD_Task_Design']['subpanel_setup']['taskd_task_design_taskd_task_design_1']['override_subpanel_name'] = 'TaskD_Task_Design_subpanel_taskd_task_design_taskd_task_design_1';

?>
