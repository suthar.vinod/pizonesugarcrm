<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Language/en_us.customgd_group_design_taskd_task_design_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE'] = 'Group Designs';
$mod_strings['LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE'] = 'Group Designs';

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Language/en_us.customm03_work_product_taskd_task_design_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Language/en_us.customanml_animals_taskd_task_design_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_ANML_ANIMALS_TITLE'] = 'Test Systems';
$mod_strings['LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE'] = 'Test Systems';

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Language/en_us.customtaskd_task_design_taskd_task_design_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_R_TITLE'] = 'Task Designs';
$mod_strings['LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_L_TITLE'] = 'Task Designs';

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Language/en_us.customm03_work_product_code_taskd_task_design_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_CODE_TITLE'] = 'Work Product Codes';
$mod_strings['LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE'] = 'Work Product Codes';

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Language/en_us.custombid_batch_id_taskd_task_design_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_BID_BATCH_ID_TITLE'] = 'Batch IDs';
$mod_strings['LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE'] = 'Batch IDs';

?>
<?php
// Merged from custom/Extension/modules/TaskD_Task_Design/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ORDER_2'] = 'Order';
$mod_strings['LBL_PHASE'] = 'Phase';
$mod_strings['LBL_DESCRIPTION'] = 'Notes';
$mod_strings['LBL_SINGLE_DATE'] = '(Obsolete) Single Date';
$mod_strings['LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE'] = 'Group Designs';
$mod_strings['LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE'] = 'Work Products';
$mod_strings['LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE'] = 'Test Systems';
$mod_strings['LBL_USDA_ID'] = 'USDA ID';
$mod_strings['LBL_OTHER_EQUIPMENT'] = 'Other Equipment';
$mod_strings['LBL_TYPE_OF_PERSONNEL'] = 'Type of Personnel (Obsolete)';
$mod_strings['LBL_DURATION_INTEGER_MIN'] = 'Duration Integer (Min)';
$mod_strings['LBL_TYPE_OF_PERSONNEL_2'] = 'Type of Personnel';
$mod_strings['LBL_TECH_ASSIGNED'] = 'Tech Assigned';
$mod_strings['LBL_INITIAL_UPON_COMPLETION'] = 'Initial Upon Completion';
$mod_strings['LBL_DATA_REVIEWED_BY'] = 'Data Reviewed By';
$mod_strings['LBL_AUDIT_2'] = 'Audit?';
$mod_strings['LBL_AUDIT_PHASE'] = 'Audit Phase';
$mod_strings['LBL_SINGLE_DATE_2'] = 'Single Date';
$mod_strings['LBL_SINGLE_TASK'] = 'Single Task';
$mod_strings['LBL_AUDIT_TEST'] = 'Audit Test';
$mod_strings['LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE'] = 'Batch IDs';
$mod_strings['LBL_CRON_UPDATED'] = 'cron updated';
$mod_strings['LBL_PARENT_TD_ID'] = 'parent td id';
$mod_strings['LBL_TASK_DESIGN_LINKED_ID_C'] = 'task design linked id c';
$mod_strings['LBL_RECURRING_TD'] = 'Recurring TD?';
$mod_strings['LBL_RECURRENCE_PATTERN_DAYS'] = 'Recurrence Pattern - Recur Every X Days';
$mod_strings['LBL_RECURRENCE_PATTERN_DURATION'] = 'Recurrence Pattern - Duration of Recurrence in Days';
$mod_strings['LBL_START_DAY_OF_RECURRENCE'] = 'Start Day of Recurrence';
$mod_strings['LBL_ORDER_START_INTEGER'] = 'Order Start Integer';
$mod_strings['LBL_CREATION_MODE_C'] = 'creation mode c';
$mod_strings['LBL_COMPLETION_STATUS_C'] = 'Completion Status';

?>
