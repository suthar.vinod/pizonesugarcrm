<?php

if (!defined('sugarEntry') || !sugarEntry)
  die('Not A Valid Entry Point');

class setCustomCalculationAfterRelAdd
{
  static $already_ran = false;
  function setCalculationAfterRelAdd($bean, $event, $arguments)
  {
      if (self::$already_ran == true)
      return;   //So that hook will only trigger once
      self::$already_ran = true;
   global $db, $current_user;
    $module = $arguments['module'];
    $relatedId = $arguments['related_id'];
    $relatedModule = $arguments['related_module'];
    $bean->load_relationship("m03_work_product_taskd_task_design_1");
    $workProductId = $bean->m03_work_product_taskd_task_design_1->get();
    $wpBean = BeanFactory::getBean('M03_Work_Product', $workProductId[0]);
    $wpfirst_procedure =  $wpBean->first_procedure_c;
    $type = $bean->type_2;
    $relative = $bean->relative;
    if($wpfirst_procedure!='' && $type == 'Actual SP' && $relative == 'NA')
    { 
      $datefirst_procedure_c = strtotime($wpfirst_procedure);;
      $bean->actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);      
      //$bean->save();
    } 

    if ($module === 'TaskD_Task_Design' && $relatedModule === 'TaskD_Task_Design') {
      $tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $relatedId);
      $planned_start_datetime_1st_Old = $tdBean->planned_start_datetime_1st;
      $planned_end_datetime_1st_Old = $tdBean->planned_end_datetime_1st;
      $planned_start_datetime_2nd_Old = $tdBean->planned_start_datetime_2nd;
      $planned_end_datetime_2nd_Old = $tdBean->planned_end_datetime_2nd;

      if ($bean->actual_datetime == "") {
        if ($tdBean->integer_units == "Day") {
          if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "1st Tier") {
            if ($bean->scheduled_start_datetime != "") {
              $result = date($bean->scheduled_start_datetime);
              $result = strtotime($result . '+' . $tdBean->start_integer . ' days');
              $tdBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean->planned_start_datetime_1st = null;
            }
            if ($bean->scheduled_end_datetime != "") {
              $result = date($bean->scheduled_end_datetime);
              $result = strtotime($result . '+' . $tdBean->end_integer . ' days');
              $tdBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean->planned_end_datetime_1st = null;
            }
          }
          
          if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "2nd Tier") {
            if ($bean->planned_start_datetime_1st != "") {
              $result = date($bean->planned_start_datetime_1st);
              $result = strtotime($result . '+' . $tdBean->start_integer . ' days');
              $tdBean->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean->planned_start_datetime_2nd = null;
            }
            if ($bean->planned_end_datetime_1st != "") {
              $result = date($bean->planned_end_datetime_1st);
              $result = strtotime($result . '+' . $tdBean->end_integer . ' days');
              $tdBean->planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean->planned_end_datetime_2nd = null;
            }
          }
        } else if ($tdBean->integer_units == "Hour") {
          if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "1st Tier") {
            if ($bean->scheduled_start_datetime != "") {
              $result = date($bean->scheduled_start_datetime);
              $result = strtotime($result . '+' . $tdBean->start_integer . ' hours');
              $tdBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean->planned_start_datetime_1st = null;
            }
            if ($bean->scheduled_end_datetime != "") {
              $result = date($bean->scheduled_end_datetime);
              $result = strtotime($result . '+' . $tdBean->end_integer . ' hours');
              $tdBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean->planned_end_datetime_1st = null;
            }
            
          }
         
          if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "2nd Tier") {
            if ($bean->planned_start_datetime_1st != "") {
              $result = date($bean->planned_start_datetime_1st);
              $result = strtotime($result . '+' . $tdBean->start_integer . ' hours');
              $tdBean->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
            }  else {
              $tdBean->planned_start_datetime_2nd = null;
            }
            if ($bean->planned_end_datetime_1st != "") {
              $result = date($bean->planned_end_datetime_1st);
              $result = strtotime($result . '+' . $tdBean->end_integer . ' hours');
              $tdBean->planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean->planned_end_datetime_2nd = null;
            }
          }
        } else if ($tdBean->integer_units == "Minute") {
          if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "1st Tier") {
            if ($bean->scheduled_start_datetime != "") {
              $result = date($bean->scheduled_start_datetime);
              $result = strtotime($result . '+' . $tdBean->start_integer . ' minutes');
              $tdBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean->planned_start_datetime_1st = null;
            }
            if ($bean->scheduled_end_datetime != "") {
              $result = date($bean->scheduled_end_datetime);
              $result = strtotime($result . '+' . $tdBean->end_integer . ' minutes');
              $tdBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean->planned_end_datetime_1st = null;
            }
          }          
          if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "2nd Tier") {
            if ($bean->planned_start_datetime_1st != "") {
              $result = date($bean->planned_start_datetime_1st);
              $result = strtotime($result . '+' . $tdBean->start_integer . ' minutes');
              $tdBean->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean->planned_start_datetime_2nd = null;
            }
            if ($bean->planned_end_datetime_1st != "") {
              $result = date($bean->planned_end_datetime_1st);
              $result = strtotime($result . '+' . $tdBean->end_integer . ' minutes');
              $tdBean->planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean->planned_end_datetime_2nd = null;
            }
          }
        }
      } else {
        if ($tdBean->integer_units == "Day") {
          if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "1st Tier") {
            $result = date($bean->actual_datetime);
            $result = strtotime($result . '+' . $tdBean->start_integer . ' days');
            $tdBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);

            $result = date($bean->actual_datetime);
            $result = strtotime($result . '+' . $tdBean->end_integer . ' days');
            $tdBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
            if (!empty($bean->actual_datetime)) {
              $single_date = strtotime($bean->actual_datetime);
              $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else if (!empty($bean->planned_start_datetime_1st)) {
                $single_date = strtotime($bean->planned_start_datetime_1st);
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            }

          }
          if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "2nd Tier") {
            $result = date($bean->actual_datetime);
            $result = strtotime($result . '+' . $tdBean->start_integer . ' days');
            $tdBean->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);

            $result = date($bean->actual_datetime);
            $result = strtotime($result . '+' . $tdBean->end_integer . ' days');
            $tdBean->planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);

            if (!empty($bean->actual_datetime)) {
              $single_date = strtotime($bean->actual_datetime);
              $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else if (!empty($bean->planned_start_datetime_2nd)) {
                $single_date = strtotime($bean->planned_start_datetime_2nd);
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            }

          }        
        } else if ($tdBean->integer_units == "Hour") {
          if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "1st Tier") {
            $result = date($bean->actual_datetime);
            $result = strtotime($result . '+' . $tdBean->start_integer . ' hours');
            $tdBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);

            $result = date($bean->actual_datetime);
            $result = strtotime($result . '+' . $tdBean->end_integer . ' hours');
            $tdBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);

            if (!empty($bean->actual_datetime)) {
              $single_date = strtotime($bean->actual_datetime);
              $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else if (!empty($bean->planned_start_datetime_1st)) {
                $single_date = strtotime($bean->planned_start_datetime_1st);
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            }
          }
          if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "2nd Tier") {
            $result = date($bean->actual_datetime);
            $result = strtotime($result . '+' . $tdBean->start_integer . ' hours');
            $tdBean->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);

            $result = date($bean->actual_datetime);
            $result = strtotime($result . '+' . $tdBean->end_integer . ' hours');
            $tdBean->planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);

            if (!empty($bean->actual_datetime)) {
              $single_date = strtotime($bean->actual_datetime);
              $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else if (!empty($bean->planned_start_datetime_2nd)) {
                $single_date = strtotime($bean->planned_start_datetime_2nd);
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            }
          }        
        } else if ($tdBean->integer_units == "Minute") {
                  
          if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "1st Tier") {
            $result = date($bean->actual_datetime);
            $result = strtotime($result . '+' . $tdBean->start_integer . ' minutes');
            $tdBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);

            $result = date($bean->actual_datetime);
            $result = strtotime($result . '+' . $tdBean->end_integer . ' minutes');
            $tdBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);

            if (!empty($bean->actual_datetime)) {
              $single_date = strtotime($bean->actual_datetime);
              $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else if (!empty($bean->planned_start_datetime_1st)) {
                $single_date = strtotime($bean->planned_start_datetime_1st);
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            }
          }
          if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "2nd Tier") {
            $result = date($bean->actual_datetime);
            $result = strtotime($result . '+' . $tdBean->start_integer . ' minutes');
            $tdBean->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);

            $result = date($bean->actual_datetime);
            $result = strtotime($result . '+' . $tdBean->end_integer . ' minutes');
            $tdBean->planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);

            if (!empty($bean->actual_datetime)) {
              $single_date = strtotime($bean->actual_datetime);
              $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else if (!empty($bean->planned_start_datetime_2nd)) {
                $single_date = strtotime($bean->planned_start_datetime_2nd);
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            }
          }
        }

        if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && ($tdBean->relative == "1st Tier" || $tdBean->relative == "NA")) {
          $tdBean->planned_start_datetime_2nd = null;
          $tdBean->planned_end_datetime_2nd = null;
        }
        if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && ($tdBean->relative == "2nd Tier" || $tdBean->relative == "NA")) {
          $tdBean->planned_start_datetime_1st = null;
          $tdBean->planned_end_datetime_1st = null;
        }
      }
      
      if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "1st Tier") {
        $sql = "";


        if ($tdBean->planned_start_datetime_1st != "" && $tdBean->planned_end_datetime_1st == "") {
          $sql = "UPDATE `taskd_task_design` 
                    SET  `planned_start_datetime_1st` = '" . $tdBean->planned_start_datetime_1st . "',
                         `planned_end_datetime_1st` = null 
                    WHERE (`id` = '" . $relatedId . "')";
        } else if ($tdBean->planned_start_datetime_1st == "" && $tdBean->planned_end_datetime_1st != "") {
          $sql = "UPDATE `taskd_task_design` 
                    SET  `planned_start_datetime_1st` = null,
                         `planned_end_datetime_1st` = '" . $tdBean->planned_end_datetime_1st . "' 
                    WHERE (`id` = '" . $relatedId . "')";
        } else if ($tdBean->planned_start_datetime_1st != "" && $tdBean->planned_end_datetime_1st != "") {
          $sql = "UPDATE `taskd_task_design` 
                    SET  `planned_start_datetime_1st` = '" . $tdBean->planned_start_datetime_1st . "',
                         `planned_end_datetime_1st` = '" . $tdBean->planned_end_datetime_1st . "' 
                    WHERE (`id` = '" . $relatedId . "')";
        } else {
          $sql = "UPDATE `taskd_task_design` 
                    SET  `planned_start_datetime_1st` = null,
                         `planned_end_datetime_1st` = null 
                    WHERE (`id` = '" . $relatedId . "')";
        }

        $result = $db->query($sql);
        if ($result) {
          $source = '{"subject":{"_type":"logic-hook","class":"SetCustomCalculation","method":"setCalculation"},"attributes":[]}';

          $auditEventid = create_guid();
          $auditsql = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $relatedId . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_start_datetime_1st","datetime","' . $planned_start_datetime_1st_Old . '","' . $tdBean->planned_start_datetime_1st . '")';
          $auditsqlResult = $db->query($auditsql);

          $auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $relatedId . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus);

          $auditEventid = create_guid();
          $auditsql = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $relatedId . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_end_datetime_1st","datetime","' . $planned_end_datetime_1st_Old . '","' . $tdBean->planned_end_datetime_1st . '")';
          $auditsqlResult = $db->query($auditsql);

          $auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $relatedId . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus);
        }
      }
      if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "2nd Tier") {

        $sql = "";


        if ($tdBean->planned_start_datetime_2nd != "" && $tdBean->planned_end_datetime_2nd == "") {
          $sql = "UPDATE `taskd_task_design` 
                  SET  `planned_start_datetime_2nd` = '" . $tdBean->planned_start_datetime_2nd . "',
                       `planned_end_datetime_2nd` = null 
                  WHERE (`id` = '" . $relatedId . "')";
        } else if ($tdBean->planned_start_datetime_2nd == "" && $tdBean->planned_end_datetime_2nd != "") {
          $sql = "UPDATE `taskd_task_design` 
                  SET  `planned_start_datetime_2nd` = null,
                       `planned_end_datetime_2nd` = '" . $tdBean->planned_end_datetime_2nd . "' 
                  WHERE (`id` = '" . $relatedId . "')";
        } else if ($tdBean->planned_start_datetime_2nd != "" && $tdBean->planned_end_datetime_2nd != "") {
          $sql = "UPDATE `taskd_task_design` 
                  SET  `planned_start_datetime_2nd` = '" . $tdBean->planned_start_datetime_2nd . "',
                       `planned_end_datetime_2nd` = '" . $tdBean->planned_end_datetime_2nd . "' 
                  WHERE (`id` = '" . $relatedId . "')";
        } else {
          $sql = "UPDATE `taskd_task_design` 
                  SET  `planned_start_datetime_2nd` = null,
                       `planned_end_datetime_2nd` = null 
                  WHERE (`id` = '" . $relatedId . "')";
        }
        $result = $db->query($sql);
        if ($result) {
          $source = '{"subject":{"_type":"logic-hook","class":"SetCustomCalculation","method":"setCalculation"},"attributes":[]}';

          $auditEventid = create_guid();
          $auditsql = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $relatedId . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_start_datetime_2nd","datetime","' . $planned_start_datetime_2nd_Old . '","' . $tdBean->planned_start_datetime_2nd . '")';
          $auditsqlResult = $db->query($auditsql);

          $auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $relatedId . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus);

          $auditEventid = create_guid();
          $auditsql = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $relatedId . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_end_datetime_2nd","datetime","' . $planned_end_datetime_2nd_Old . '","' . $tdBean->planned_end_datetime_2nd . '")';
          $auditsqlResult = $db->query($auditsql);

          $auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $relatedId . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus);
        }
      }    
    }
    
  }
}
