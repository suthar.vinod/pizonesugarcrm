<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class SingleDateCalculationupdateHook
{
    function SingleDateCalculationupdate($bean, $event, $arguments)
    {
        if ($bean->relative == "NA") {
            $single_date = strtotime($bean->actual_datetime);
            if (!empty($bean->actual_datetime)) {
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else if (!empty($bean->scheduled_start_datetime)) {
                $single_date = strtotime($bean->scheduled_start_datetime);
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else {
                $bean->single_date_2_c = "";
            }
        } else if ($bean->relative == "1st Tier") { 
            if (!empty($bean->actual_datetime)) {
                $single_date = strtotime($bean->actual_datetime);
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else if (!empty($bean->planned_start_datetime_1st)) {
                $single_date = strtotime($bean->planned_start_datetime_1st);
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else if (!empty($bean->scheduled_start_datetime)) {
                $single_date = strtotime($bean->scheduled_start_datetime);
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else {
                $bean->single_date_2_c = "";
            }
        } else if ($bean->relative == "2nd Tier") {
            if (!empty($bean->actual_datetime)) {
                $single_date = strtotime($bean->actual_datetime);
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else if (!empty($bean->planned_start_datetime_2nd)) {
                $single_date = strtotime($bean->planned_start_datetime_2nd);
                $bean->single_date_2_c = date("Y-m-d", $single_date);
            } else {
                $bean->single_date_2_c = "";
            }
        } 
        
    }
}