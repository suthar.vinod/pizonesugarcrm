<?php

if (!defined('sugarEntry') || !sugarEntry)
  die('Not A Valid Entry Point');

class setCustomCalculationSingleDateHook
{
  //static $already_ran = false;  // To make sure LogicHooks will execute just onces
  function setCalculationSingleDate($bean, $event, $arguments)
  {
    // if (self::$already_ran == true)
    // return;   //So that hook will only trigger once
    // self::$already_ran = true;
    global $db, $current_user;
    $bean->load_relationship('taskd_task_design_taskd_task_design_1');
    $taskDesignId = $bean->taskd_task_design_taskd_task_design_1->get();

    $currentWPBean = BeanFactory::retrieveBean('M03_Work_Product', $bean->m03_work_product_taskd_task_design_1m03_work_product_ida);
    $currentWPBean->load_relationship('m03_work_product_taskd_task_design_1');
    $relatedTD1 = $currentWPBean->m03_work_product_taskd_task_design_1->get();

    foreach ($taskDesignId as $tdID) {
      $tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $tdID);
      if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "1st Tier") {

        if (!empty($tdBean->actual_datetime)) {
          $single_date = strtotime($tdBean->actual_datetime);
          $tdBean->single_date_2_c = date("Y-m-d", $single_date);
        } else if (!empty($tdBean->planned_start_datetime_1st)) {
          $single_date = strtotime($tdBean->planned_start_datetime_1st);
          $tdBean->single_date_2_c = date("Y-m-d", $single_date);
        } else {
          $bean->single_date_2_c = "";
        }

        $tdBean1 = BeanFactory::retrieveBean('TaskD_Task_Design', $tdBean->id);
        $tdBean1->load_relationship('taskd_task_design_taskd_task_design_1');
        $taskDesignId1 = $tdBean1->taskd_task_design_taskd_task_design_1->get();

        foreach ($taskDesignId1 as $tdID1) {
          $tdBean2tier = BeanFactory::retrieveBean('TaskD_Task_Design', $tdID1);
          if (($tdBean2tier->type_2 == "Actual" || $tdBean2tier->type_2 == "Actual SP") && $tdBean2tier->relative == "2nd Tier") {

            if (!empty($tdBean2tier->actual_datetime)) {
              $single_date = strtotime($tdBean2tier->actual_datetime);
              $tdBean2tier->single_date_2_c = date("Y-m-d", $single_date);
            } else if (!empty($tdBean2tier->planned_start_datetime_2nd)) {
              $single_date = strtotime($tdBean2tier->planned_start_datetime_2nd);
              $tdBean2tier->single_date_2_c = date("Y-m-d", $single_date);
            } else {
              $bean->single_date_2_c = "";
            }
          }
          $tdBean2tier->save();
        }
      }
      if (($tdBean->type_2 == "Actual" || $tdBean->type_2 == "Actual SP") && $tdBean->relative == "2nd Tier") {
        if (!empty($tdBean->actual_datetime)) {
          $single_date = strtotime($tdBean->actual_datetime);
          $tdBean->single_date_2_c = date("Y-m-d", $single_date);
        } else if (!empty($tdBean->planned_start_datetime_2nd)) {
          $single_date = strtotime($tdBean->planned_start_datetime_2nd);
          $tdBean->single_date_2_c = date("Y-m-d", $single_date);
        } else {
          $bean->single_date_2_c = "";
        }
      }
      $tdBean->save();
    }
  }
}
