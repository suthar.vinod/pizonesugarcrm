<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class EmptyFieldsHook
{
  function emptyFields($bean, $event, $arguments)
  {
    $type = $bean->type_2;
    if ($type == "Plan") {
      $bean->actual_datetime = null;
      $bean->scheduled_start_datetime = null;
      $bean->scheduled_end_datetime = null;
      $bean->planned_start_datetime_1st = null;
      $bean->planned_end_datetime_1st = null;
      $bean->planned_start_datetime_2nd = null;
      $bean->planned_end_datetime_2nd = null;
    }

    $relative = $bean->relative;
    if ($relative == "NA") {
      $bean->planned_end_datetime_1st = null;
      $bean->planned_start_datetime_1st = null;
      $bean->planned_start_datetime_2nd = null;
      $bean->planned_end_datetime_2nd = null;
    } else if ($relative == "1st Tier") {
      $bean->actual_datetime = null;
      $bean->scheduled_start_datetime = null;
      $bean->scheduled_end_datetime = null;
      $bean->planned_start_datetime_2nd = null;
      $bean->planned_end_datetime_2nd = null;
    } else if ($relative == "2nd Tier") {
      $bean->actual_datetime = null;
      $bean->scheduled_start_datetime = null;
      $bean->scheduled_end_datetime = null;
      $bean->planned_start_datetime_1st = null;
      $bean->planned_end_datetime_1st = null;
    }

    $bean->load_relationship('taskd_task_design_taskd_task_design_1');
    $taskDesignId = $bean->taskd_task_design_taskd_task_design_1->get();

    $bean->load_relationship('taskd_task_design_taskd_task_design_1_right');
    $taskDesignIdRight = $bean->taskd_task_design_taskd_task_design_1_right->get();

    //Day = ad61bde4-1129-11ea-8f07-06e41dba421a

    $GLOBALS['log']->fatal('>>> Rel Not Right  ' . print_r($taskDesignId, 1));
    $GLOBALS['log']->fatal('>>> Rel Right  ' . print_r($taskDesignIdRight, 1));
  }
}
