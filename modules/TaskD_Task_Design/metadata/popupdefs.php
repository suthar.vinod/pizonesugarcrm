<?php
$popupMeta = array (
    'moduleMain' => 'TaskD_Task_Design',
    'varName' => 'TaskD_Task_Design',
    'orderBy' => 'taskd_task_design.name',
    'whereClauses' => array (
  'name' => 'taskd_task_design.name',
),
    'searchInputs' => array (
  0 => 'taskd_task_design_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'TIME_WINDOW' => 
  array (
    'type' => 'varchar',
    'readonly' => true,
    'label' => 'LBL_TIME_WINDOW',
    'width' => 10,
    'default' => true,
    'name' => 'time_window',
  ),
  'TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_L_TITLE',
    'id' => 'TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1TASKD_TASK_DESIGN_IDA',
    'width' => 10,
    'default' => true,
    'name' => 'taskd_task_design_taskd_task_design_1_name',
  ),
),
);
