<?php
// created: 2022-01-18 06:45:02
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'type_2' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TYPE_2',
    'width' => 10,
  ),
  'relative' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_RELATIVE',
    'width' => 10,
  ),
  'order_2_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'decimal',
    'vname' => 'LBL_ORDER_2',
    'width' => 10,
    'default' => true,
  ),
  'taskd_task_design_taskd_task_design_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_L_TITLE',
    'id' => 'TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1TASKD_TASK_DESIGN_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'TaskD_Task_Design',
    'target_record_key' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
  ),
  'category' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_CATEGORY',
    'width' => 10,
  ),
  'time_window' => 
  array (
    'type' => 'varchar',
    'readonly' => true,
    'vname' => 'LBL_TIME_WINDOW',
    'width' => 10,
    'default' => true,
  ),
  'equipment_required' => 
  array (
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_EQUIPMENT_REQUIRED',
    'width' => 10,
  ),
  'actual_datetime' => 
  array (
    'type' => 'datetimecombo',
    'vname' => 'LBL_ACTUAL_DATETIME',
    'width' => 10,
    'default' => true,
  ),
  'planned_start_datetime_1st' => 
  array (
    'type' => 'datetimecombo',
    'readonly' => true,
    'vname' => 'LBL_PLANNED_START_DATETIME_1ST',
    'width' => 10,
    'default' => true,
  ),
  'planned_end_datetime_1st' => 
  array (
    'type' => 'datetimecombo',
    'readonly' => true,
    'vname' => 'LBL_PLANNED_END_DATETIME_1ST',
    'width' => 10,
    'default' => true,
  ),
  'planned_start_datetime_2nd' => 
  array (
    'type' => 'datetimecombo',
    'readonly' => true,
    'vname' => 'LBL_PLANNED_START_DATETIME_2ND',
    'width' => 10,
    'default' => true,
  ),
  'planned_end_datetime_2nd' => 
  array (
    'type' => 'datetimecombo',
    'readonly' => true,
    'vname' => 'LBL_PLANNED_END_DATETIME_2ND',
    'width' => 10,
    'default' => true,
  ),
  'days_from_initial_task' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_DAYS_FROM_INITIAL_TASK',
    'width' => 10,
  ),
  'm03_work_product_taskd_task_design_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE',
    'id' => 'M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1M03_WORK_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M03_Work_Product',
    'target_record_key' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
);