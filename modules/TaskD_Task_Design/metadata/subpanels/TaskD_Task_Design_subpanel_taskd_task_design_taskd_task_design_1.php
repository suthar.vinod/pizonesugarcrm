<?php
// created: 2021-12-07 10:37:40
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'type_2' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TYPE_2',
    'width' => 10,
  ),
  'relative' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_RELATIVE',
    'width' => 10,
  ),
  'order_2_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'decimal',
    'vname' => 'LBL_ORDER_2',
    'width' => 10,
    'default' => true,
  ),
  'category' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_CATEGORY',
    'width' => 10,
  ),
  'time_window' => 
  array (
    'type' => 'varchar',
    'readonly' => true,
    'vname' => 'LBL_TIME_WINDOW',
    'width' => 10,
    'default' => true,
  ),
  'equipment_required' => 
  array (
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_EQUIPMENT_REQUIRED',
    'width' => 10,
  ),
  'actual_datetime' => 
  array (
    'type' => 'datetimecombo',
    'vname' => 'LBL_ACTUAL_DATETIME',
    'width' => 10,
    'default' => true,
  ),
  'planned_start_datetime_1st' => 
  array (
    'type' => 'datetimecombo',
    'readonly' => true,
    'vname' => 'LBL_PLANNED_START_DATETIME_1ST',
    'width' => 10,
    'default' => true,
  ),
  'planned_end_datetime_1st' => 
  array (
    'type' => 'datetimecombo',
    'readonly' => true,
    'vname' => 'LBL_PLANNED_END_DATETIME_1ST',
    'width' => 10,
    'default' => true,
  ),
  'planned_start_datetime_2nd' => 
  array (
    'type' => 'datetimecombo',
    'readonly' => true,
    'vname' => 'LBL_PLANNED_START_DATETIME_2ND',
    'width' => 10,
    'default' => true,
  ),
  'planned_end_datetime_2nd' => 
  array (
    'type' => 'datetimecombo',
    'readonly' => true,
    'vname' => 'LBL_PLANNED_END_DATETIME_2ND',
    'width' => 10,
    'default' => true,
  ),
  'days_from_initial_task' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_DAYS_FROM_INITIAL_TASK',
    'width' => 10,
  ),
  'm03_work_product_taskd_task_design_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE',
    'id' => 'M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1M03_WORK_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M03_Work_Product',
    'target_record_key' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
  ),
  'anml_animals_taskd_task_design_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE',
    'id' => 'ANML_ANIMALS_TASKD_TASK_DESIGN_1ANML_ANIMALS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'ANML_Animals',
    'target_record_key' => 'anml_animals_taskd_task_design_1anml_animals_ida',
  ),
  'gd_group_design_taskd_task_design_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE',
    'id' => 'GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1GD_GROUP_DESIGN_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'GD_Group_Design',
    'target_record_key' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
);