<?php
// created: 2022-04-26 11:05:33
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'order_2_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'decimal',
    'vname' => 'LBL_ORDER_2',
    'width' => 10,
    'default' => true,
  ),
  'relative' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_RELATIVE',
    'width' => 10,
  ),
  'taskd_task_design_taskd_task_design_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_L_TITLE',
    'id' => 'TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1TASKD_TASK_DESIGN_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'TaskD_Task_Design',
    'target_record_key' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
  ),
  'category' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_CATEGORY',
    'width' => 10,
  ),
  'time_window' => 
  array (
    'type' => 'varchar',
    'readonly' => true,
    'vname' => 'LBL_TIME_WINDOW',
    'width' => 10,
    'default' => true,
  ),
  'actual_datetime' => 
  array (
    'type' => 'datetimecombo',
    'vname' => 'LBL_ACTUAL_DATETIME',
    'width' => 10,
    'default' => true,
  ),
  'scheduled_start_datetime' => 
  array (
    'type' => 'datetimecombo',
    'vname' => 'LBL_SCHEDULED_START_DATETIME',
    'width' => 10,
    'default' => true,
  ),
  'scheduled_end_datetime' => 
  array (
    'type' => 'datetimecombo',
    'vname' => 'LBL_SCHEDULED_END_DATETIME',
    'width' => 10,
    'default' => true,
  ),
  'planned_start_datetime_1st' => 
  array (
    'type' => 'datetimecombo',
    'readonly' => true,
    'vname' => 'LBL_PLANNED_START_DATETIME_1ST',
    'width' => 10,
    'default' => true,
  ),
  'planned_end_datetime_1st' => 
  array (
    'type' => 'datetimecombo',
    'readonly' => true,
    'vname' => 'LBL_PLANNED_END_DATETIME_1ST',
    'width' => 10,
    'default' => true,
  ),
  'planned_start_datetime_2nd' => 
  array (
    'type' => 'datetimecombo',
    'readonly' => true,
    'vname' => 'LBL_PLANNED_START_DATETIME_2ND',
    'width' => 10,
    'default' => true,
  ),
  'planned_end_datetime_2nd' => 
  array (
    'type' => 'datetimecombo',
    'readonly' => true,
    'vname' => 'LBL_PLANNED_END_DATETIME_2ND',
    'width' => 10,
    'default' => true,
  ),
  'type_of_personnel_2_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_TYPE_OF_PERSONNEL_2',
    'width' => 10,
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
);