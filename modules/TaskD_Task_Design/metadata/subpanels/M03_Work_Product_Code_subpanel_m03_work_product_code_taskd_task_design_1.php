<?php
// created: 2022-03-15 08:47:59
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'order_2_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'decimal',
    'vname' => 'LBL_ORDER_2',
    'width' => 10,
    'default' => true,
  ),
  'relative' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_RELATIVE',
    'width' => 10,
  ),
  'taskd_task_design_taskd_task_design_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_L_TITLE',
    'id' => 'TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1TASKD_TASK_DESIGN_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'TaskD_Task_Design',
    'target_record_key' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
  ),
  'time_window' => 
  array (
    'type' => 'varchar',
    'readonly' => true,
    'vname' => 'LBL_TIME_WINDOW',
    'width' => 10,
    'default' => true,
  ),
  'standard_task' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STANDARD_TASK',
    'width' => 10,
  ),
  'phase_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_PHASE',
    'width' => 10,
  ),
  'custom_task' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_CUSTOM_TASK',
    'width' => 10,
  ),
);