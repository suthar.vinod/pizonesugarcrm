<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class create_auto_td_recordsHook
{
    function create_auto_td_records($bean, $event, $arguments)
    {
        global $db, $current_user;
        global $app_list_strings;
        $module = "TaskD_Task_Design";
        //  first check the record is newly createing or updating old record

        if ($bean->id != $bean->fetched_row['id'] && $bean->recurring_td_c == "Yes") {

            //$GLOBALS['log']->fatal(' create_auto_td_records bean->id ' . $bean->id);
            $duration = intval($bean->recurrence_pattern_duration_c);
            $days = intval($bean->recurrence_pattern_days_c);

            $calc = $duration / $days;
            $records_to_create = ceil($calc) - 1;
            //$GLOBALS['log']->fatal(' create_auto_td_records records_to_create ' . $records_to_create);
            $start_day_of_recurrence_c = intval($bean->start_day_of_recurrence_c);

            $SInt = $start_day_of_recurrence_c;
            //$EInt = $start_day_of_recurrence_c;
            $order = floatval($bean->order_start_integer_c);

            $wpc_id = $bean->m03_work_product_code_taskd_task_design_1_name;
            $wp_id = $bean->m03_work_product_taskd_task_design_1m03_work_product_ida;
            $gd_id = $bean->gd_group_design_taskd_task_design_1gd_group_design_ida;
            $td_id = $bean->taskd_task_design_taskd_task_design_1taskd_task_design_ida;

            $bean->order_2_c		= floatval($order);
			$bean->start_integer	= $bean->start_day_of_recurrence_c; 
			$bean->end_integer		= $bean->start_day_of_recurrence_c; 
            $bean->start_integer	= $bean->start_integer; 
			$bean->end_integer		= $bean->end_integer; 
			$bean->recurring_td_c	= "No";  
            $bean->taskd_task_design_taskd_task_design_1taskd_task_design_ida = $td_id;
            $unit_Bean = BeanFactory::getBean('U_Units', 'ad61bde4-1129-11ea-8f07-06e41dba421a');
            $bean->u_units_id_c = "ad61bde4-1129-11ea-8f07-06e41dba421a";
            $unit_Bean_name = $unit_Bean->name;               

            $bean->time_window = $unit_Bean_name . " " . $bean->start_integer . "-" . $bean->end_integer;

            $TdBean = BeanFactory::getBean('TaskD_Task_Design', $td_id);

            $GLOBALS['log']->fatal(' create_auto_td_records TdBean->actual_datetime ' . $TdBean->actual_datetime);

            $result = date($TdBean->actual_datetime);

            $resultstart = strtotime($result . '+' . $bean->start_integer . ' days');
            $bean->planned_start_datetime_1st = date("Y-m-d H:i:s", $resultstart);
            
           
            $resultend = strtotime($result . '+' . $bean->end_integer . ' days');
            $bean->planned_end_datetime_1st = date("Y-m-d H:i:s", $resultend);

            for ($i = 1; $i <= $records_to_create; $i++) {
                // creating new bean
                $newBean = BeanFactory::newBean($module);
                $newBean->new_with_id = true;
                // Work Product Codes
                // Audit Phase
                $SInt = $SInt + $days;
                //$EInt = $EInt + $days;
                $order = $order + 5;

                $newBean->m03_work_product_code_taskd_task_design_1_name = $wpc_id;
                $newBean->audit_phase_c = $bean->audit_phase_c;               
                
                $newBean->m03_work_product_taskd_task_design_1m03_work_product_ida = $wp_id;
                
                // Type
                $newBean->type_2 = $bean->type_2;
                // Group Designs
                $newBean->gd_group_design_taskd_task_design_1gd_group_design_ida = $gd_id;
                // Relative
                $newBean->relative = $bean->relative;
                // Task Designs
                $newBean->taskd_task_design_taskd_task_design_1taskd_task_design_ida = $td_id;
                // Category
                $newBean->category = $bean->category;
                // Recurrence Pattern - Recur Every x Days
                $newBean->recurrence_pattern_days_c = "";
                // Recurrence Pattern - Duration of Recurrence in Days
                $newBean->recurrence_pattern_duration_c = "";
                // Start Integer
                $newBean->start_integer = $SInt;
                // End Integer
                $newBean->end_integer = $SInt;
                // Start Integer
                $newBean->start_integer = $newBean->start_integer;
                // End Integer
                $newBean->end_integer = $newBean->end_integer;
                // Integer Units
                $newBean->u_units_id_c = "ad61bde4-1129-11ea-8f07-06e41dba421a";
                // Equipment Required
                $newBean->equipment_required = $bean->equipment_required;
                // Task Type
                $newBean->task_type = $bean->task_type;
                // Custom Task
                $newBean->custom_task = $bean->custom_task;
                // Standard Task
                $newBean->standard_task = $bean->standard_task;
                // Phase
                $newBean->phase_c = $bean->phase_c;
                // Recurring TD
                $newBean->recurring_td_c = "No";
                // Order
                $newBean->order_2_c = floatval($order);
                $newBean->task_design_linked_id_c = $td_id;
                $newBean->type_of_personnel_2_c     = $bean->type_of_personnel_2_c;
                $newBean->duration_integer_min_c    = $bean->duration_integer_min_c;
                $newBean->days_from_initial_task    = $bean->days_from_initial_task;
                $newBean->time_window = $unit_Bean_name . " " . $newBean->start_integer . "-" . $newBean->end_integer;

                
                $resultstart = strtotime($result . '+' . $SInt . ' days');
                $newBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $resultstart);
    
                
                $resultend = strtotime($result . '+' . $SInt . ' days');
                $newBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $resultend);

                $newBean->save();
            }
        }
    }
}
