({
    extendsFrom: 'RecordView',
    initialize: function(options) {
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.onload_function, this);
        //this.model.on("change:integer_units change:start_integer change:end_integer", this.setTimeWindow, this);
        this.model.once("sync",
            function() {
                this.model.on("change:integer_units change:start_integer change:end_integer", this.setTimeWindow, this);
            },
            this
        );
        this.model.once("sync",
            function() {
                this.model.on("change:taskd_task_design_taskd_task_design_1_name change:start_integer change:end_integer change:integer_units change:relative", this.customCalculation, this);
                this.model.on("change:taskd_task_design_taskd_task_design_1_name", this.Taskdesignlink, this);
            },
            this
        );
        this.model.once("sync",
            function() {
                this.model.on("change:type_2", this.emptyTimeFields, this);
            },
            this
        );
        this.model.once("sync",
            function() {
                this.model.on("change:relative", this.emptyTimeFieldsRelative, this);
            },
            this
        );
        this.model.on("change:equipment_required", this.fun_equipment_required, this);
        this.model.addValidationTask('other_equipment_c', _.bind(this._doValidateOtherEquipment, this));

    },  

    _doValidateOtherEquipment: function(fields, errors, callback) {
        var equipment_required = this.model.get("equipment_required");
        var other_equipment_c = this.model.get("other_equipment_c");
        //console.log('1qqq',other_equipment_c)
        //console.log('11',equipment_required.indexOf('Other'))
        if (equipment_required.indexOf('Other') >= 0 && (other_equipment_c == "" || other_equipment_c == undefined))
        {
            //console.log('2')
            errors['other_equipment_c'] = errors['other_equipment_c'] || {};
            errors['other_equipment_c'].required = true;
            
        }
        callback(null, fields, errors); 
    },

    fun_equipment_required: function(test) {
        var equipment_required = this.model.get("equipment_required");
        if(equipment_required == ""){
            //console.log('3',equipment_required)
            this.$el.find('[data-name="other_equipment_c"]').css('visibility', 'hidden');
            this.$el.find('[data-name="other_equipment_c"] textarea[name="other_equipment_c"]').attr('placeholder', '');
        }else{
            //console.log('4',equipment_required)
            if (_.indexOf(equipment_required, "Other") === -1){
                this.$el.find('[data-name="other_equipment_c"]').css('visibility', 'hidden');
                this.$el.find('[data-name="other_equipment_c"] textarea[name="other_equipment_c"]').attr('placeholder', '');
            }else{
                this.$el.find('[data-name="other_equipment_c"]').css('visibility', 'unset');
                this.$el.find('[data-name="other_equipment_c"] textarea[name="other_equipment_c"]').attr('placeholder', 'Required');
            }
        }
    },

    setTimeWindow: function() {
        var integer_units = this.model.get("integer_units");
        var start_integer = this.model.get("start_integer");
        var end_integer = this.model.get("end_integer");

        var newValue = integer_units + " " + start_integer + "-" + end_integer;
        /* We need this twice */
        newValue = newValue.replace("null", "");
        newValue = newValue.replace("null", "");

        this.model.set("time_window", newValue);
    },
    onload_function: function() {
        var equipment_required = this.model.get("equipment_required");
        if (equipment_required == "") {
           // console.log('31', equipment_required)
            this.$el.find('[data-name="other_equipment_c"]').css('visibility', 'hidden');
        }

        var gd_group_design_taskd_task_design_1_name = this.model.get('gd_group_design_taskd_task_design_1_name');
        var anml_animals_taskd_task_design_1_name = this.model.get('anml_animals_taskd_task_design_1_name');
        var m03_work_product_code_taskd_task_design_1_name = this.model.get('m03_work_product_code_taskd_task_design_1_name');
        console.log('anml_animals_taskd_task_design_1_name', anml_animals_taskd_task_design_1_name);
        if (gd_group_design_taskd_task_design_1_name != "" || anml_animals_taskd_task_design_1_name != "" || m03_work_product_code_taskd_task_design_1_name != "") {
            $('input[name="type_2"]').prop('disabled', true);
            $('div[data-name="type_2"]').css('pointer-events', 'none');
            $('[name="type_2"]').attr('readOnly', true);
        } else if (bid_batch_id_taskd_task_design_1_name != "" && type_2 == 'Actual SP') {
            $('input[name="type_2"]').prop('disabled', true);
            $('div[data-name="type_2"]').css('pointer-events', 'none');
            $('[name="type_2"]').attr('readOnly', true);
        }
        else {
            $('input[name="type_2"]').prop('disabled', false);
            $('div[data-name="type_2"]').css('pointer-events', 'unset');
            $('[name="type_2"]').attr('readOnly', false);
        }
    },

    emptyTimeFields: function() {
        var type = this.model.get("type_2");
        if (type == "Plan") {
            this.model.set("actual_datetime", null);
            this.model.set("scheduled_start_datetime", null);
            this.model.set("scheduled_end_datetime", null);
            this.model.set("planned_start_datetime_1st", null);
            this.model.set("planned_end_datetime_1st", null);
            this.model.set("planned_start_datetime_2nd", null);
            this.model.set("planned_end_datetime_2nd", null);
        }
    },
    emptyTimeFieldsRelative: function() {
        this.model.set("taskd_task_design_taskd_task_design_1taskd_task_design_ida", null);
        this.model.set("taskd_task_design_taskd_task_design_1_name", null);
        var relative = this.model.get("relative");
        if (relative == "NA") {
            this.model.set("planned_end_datetime_1st", null);
            this.model.set("planned_start_datetime_1st", null);
            this.model.set("planned_start_datetime_2nd", null);
            this.model.set("planned_end_datetime_2nd", null);
        } else if (relative == "1st Tier") {
            this.model.set("actual_datetime", null);
            this.model.set("scheduled_start_datetime", null);
            this.model.set("scheduled_end_datetime", null);
            this.model.set("planned_start_datetime_2nd", null);
            this.model.set("planned_end_datetime_2nd", null);
        } else if (relative == "2nd Tier") {
            this.model.set("actual_datetime", null);
            this.model.set("scheduled_start_datetime", null);
            this.model.set("scheduled_end_datetime", null);
            this.model.set("planned_start_datetime_1st", null);
            this.model.set("planned_end_datetime_1st", null);
        }
    },
    customCalculation: function() {
        if (this.model.get("relative") == "1st Tier" || this.model.get("relative") == "2nd Tier") {

            var taskDesignId = this.model.get("taskd_task_design_taskd_task_design_1taskd_task_design_ida");
            var startInteger = this.model.get("start_integer") == "" ? 0 : this.model.get("start_integer");
            var endInteger = this.model.get("end_integer") == "" ? 0 : this.model.get("end_integer");
            var integerUnit = this.model.get("integer_units");

            var relative = this.model.get("relative");
            var type_2 = this.model.get("type_2");

            var self = this;

            if (taskDesignId != undefined) {
                App.api.call("get", "rest/v10/TaskD_Task_Design/" + taskDesignId + "?fields=actual_datetime,scheduled_start_datetime,scheduled_end_datetime,planned_start_datetime_1st,planned_end_datetime_1st", null, {
                    success: function(data) {
                        var actualDateTime = data.actual_datetime;
                        var scheduleStartTime = data.scheduled_start_datetime;
                        var scheduleEndTime = data.scheduled_end_datetime;
                        var plannedStartDatetime1st = data.planned_start_datetime_1st;
                        var plannedEndDatetime1st = data.planned_end_datetime_1st;

                        if (actualDateTime == "") {
                            if (integerUnit == "Day") {
                                if (scheduleStartTime != "") {
                                    if (relative == "1st Tier") {
                                        var result = new Date(scheduleStartTime);
                                        result.setDate(result.getDate() + startInteger);
                                        self.model.set("planned_start_datetime_1st", moment(result).format());
                                        self.model.save();
                                    }
                                }
                                if (scheduleEndTime != "") {
                                    if (relative == "1st Tier") {
                                        var result = new Date(scheduleEndTime);
                                        result.setDate(result.getDate() + endInteger);
                                        self.model.set("planned_end_datetime_1st", moment(result).format());
                                        self.model.save();
                                    }
                                }
                                if (plannedStartDatetime1st != "") {
                                    if (relative == "2nd Tier") {
                                        var result = new Date(plannedStartDatetime1st);
                                        result.setDate(result.getDate() + startInteger);
                                        self.model.set("planned_start_datetime_2nd", moment(result).format());
                                        self.model.save();
                                    }
                                }
                                if (plannedEndDatetime1st != "") {
                                    if (relative == "2nd Tier") {
                                        var result = new Date(plannedEndDatetime1st);
                                        result.setDate(result.getDate() + endInteger);
                                        self.model.set("planned_end_datetime_2nd", moment(result).format());
                                        self.model.save();                                       
                                    }
                                }

                            } else if (integerUnit == "Hour") {
                                if (scheduleStartTime != "") {
                                    if (relative == "1st Tier") {
                                        var result = new Date(scheduleStartTime);
                                        result.setHours(result.getHours() + startInteger);
                                        self.model.set("planned_start_datetime_1st", moment(result).format());
                                        self.model.save();
                                    }
                                }
                                if (scheduleEndTime != "") {
                                    if (relative == "1st Tier") {
                                        var result = new Date(scheduleEndTime);
                                        result.setHours(result.getHours() + endInteger);
                                        self.model.set("planned_end_datetime_1st", moment(result).format());
                                        self.model.save();
                                    }
                                }


                                if (plannedStartDatetime1st != "") {
                                    if (relative == "2nd Tier") {
                                        var result = new Date(plannedStartDatetime1st);
                                        result.setHours(result.getHours() + startInteger);
                                        self.model.set("planned_start_datetime_2nd", moment(result).format());
                                        self.model.save();
                                    }
                                }
                                if (plannedEndDatetime1st != "") {
                                    if (relative == "2nd Tier") {
                                        var result = new Date(plannedEndDatetime1st);
                                        result.setHours(result.getHours() + endInteger);
                                        self.model.set("planned_end_datetime_2nd", moment(result).format());
                                        self.model.save();
                                    }
                                }

                            } else {
                                if (scheduleStartTime != "") {
                                    if (relative == "1st Tier") {
                                        var result = new Date(scheduleStartTime);
                                        result.setMinutes(result.getMinutes() + startInteger);
                                        self.model.set("planned_start_datetime_1st", moment(result).format());
                                        self.model.save();
                                    }
                                }
                                if (scheduleEndTime != "") {
                                    if (relative == "1st Tier") {
                                        var result = new Date(scheduleEndTime);
                                        result.setMinutes(result.getMinutes() + endInteger);
                                        self.model.set("planned_end_datetime_1st", moment(result).format());
                                        self.model.save();
                                    }
                                }
                                if (plannedStartDatetime1st != "") {
                                    if (relative == "2nd Tier") {
                                        var result = new Date(plannedStartDatetime1st);
                                        result.setMinutes(result.getMinutes() + startInteger);
                                        self.model.set("planned_start_datetime_2nd", moment(result).format());
                                        self.model.save();
                                    }
                                }
                                if (plannedEndDatetime1st != "") {
                                    if (relative == "2nd Tier") {
                                        var result = new Date(plannedEndDatetime1st);
                                        result.setMinutes(result.getMinutes() + endInteger);
                                        self.model.set("planned_end_datetime_2nd", moment(result).format());
                                        self.model.save();                                       
                                    }
                                }
                            }
                        } else {
                            if (integerUnit == "Day") {
                                if (actualDateTime != "") {
                                    if (relative == "1st Tier") {
                                        var result = new Date(actualDateTime);
                                        result.setDate(result.getDate() + startInteger);
                                        self.model.set("planned_start_datetime_1st", moment(result).format());

                                        var result = new Date(actualDateTime);
                                        result.setDate(result.getDate() + endInteger);
                                        self.model.set("planned_end_datetime_1st", moment(result).format());
                                        self.model.save();
                                    }
                                }

                                if (actualDateTime != "") {
                                    if (relative == "2nd Tier") {
                                        var result = new Date(actualDateTime);
                                        result.setDate(result.getDate() + startInteger);
                                        self.model.set("planned_start_datetime_2nd", moment(result).format());

                                        var result = new Date(actualDateTime);
                                        result.setDate(result.getDate() + endInteger);
                                        self.model.set("planned_end_datetime_2nd", moment(result).format());
                                        self.model.save();
                                    }
                                }
                            } else if (integerUnit == "Hour") {
                                if (actualDateTime != "") {
                                    if (relative == "1st Tier") {
                                        var result = new Date(actualDateTime);
                                        result.setHours(result.getHours() + startInteger);
                                        self.model.set("planned_start_datetime_1st", moment(result).format());

                                        var result = new Date(actualDateTime);
                                        result.setHours(result.getHours() + endInteger);
                                        self.model.set("planned_end_datetime_1st", moment(result).format());
                                        self.model.save();
                                    }
                                }
                                if (actualDateTime != "") {
                                    if (relative == "2nd Tier") {
                                        var result = new Date(actualDateTime);
                                        result.setHours(result.getHours() + startInteger);
                                        self.model.set("planned_start_datetime_2nd", moment(result).format());

                                        var result = new Date(actualDateTime);
                                        result.setHours(result.getHours() + endInteger);
                                        self.model.set("planned_end_datetime_2nd", moment(result).format());
                                        self.model.save();
                                    }
                                }
                            } else {
                                if (actualDateTime != "") {
                                    if (relative == "1st Tier") {
                                        var result = new Date(actualDateTime);
                                        result.setMinutes(result.getMinutes() + startInteger);
                                        self.model.set("planned_start_datetime_1st", moment(result).format());

                                        var result = new Date(actualDateTime);
                                        result.setMinutes(result.getMinutes() + endInteger);
                                        self.model.set("planned_end_datetime_1st", moment(result).format());
                                        self.model.save();
                                    }
                                }
                                if (actualDateTime != "") {
                                    if (relative == "2nd Tier") {
                                        var result = new Date(actualDateTime);
                                        result.setMinutes(result.getMinutes() + startInteger);
                                        self.model.set("planned_start_datetime_2nd", moment(result).format());

                                        var result = new Date(actualDateTime);
                                        result.setMinutes(result.getMinutes() + endInteger);
                                        self.model.set("planned_end_datetime_2nd", moment(result).format());
                                        self.model.save();
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }
    },
    /**17 nov 2021 : #1551 */
    Taskdesignlink: function () {
        var taskDesignId = this.model.get("taskd_task_design_taskd_task_design_1taskd_task_design_ida");
        //alert(taskDesignId);
        this.model.set("task_design_linked_id_c", taskDesignId);
        //this.model.save();
    },
   
})