<?php
$module_name = 'TaskD_Task_Design';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'TaskD_Task_Design',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
                'size' => 'large',
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'm03_work_product_taskd_task_design_1_name',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'audit_2_c',
                'label' => 'LBL_AUDIT_2',
              ),
              2 => 
              array (
                'name' => 'm03_work_product_code_taskd_task_design_1_name',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'audit_phase_c',
                'label' => 'LBL_AUDIT_PHASE',
              ),
              4 => 
              array (
                'name' => 'bid_batch_id_taskd_task_design_1_name',
              ),
              5 => 
              array (
              ),
              6 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
              ),
              7 => 
              array (
                'readonly' => false,
                'name' => 'order_2_c',
                'label' => 'LBL_ORDER_2',
              ),
              8 => 
              array (
                'name' => 'gd_group_design_taskd_task_design_1_name',
                'label' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_NAME_FIELD_TITLE',
              ),
              9 => 
              array (
              ),
              10 => 
              array (
                'name' => 'anml_animals_taskd_task_design_1_name',
              ),
              11 => 
              array (
                'readonly' => false,
                'name' => 'usda_id_c',
                'label' => 'LBL_USDA_ID',
              ),
              12 => 
              array (
                'name' => 'relative',
                'label' => 'LBL_RELATIVE',
              ),
              13 => 
              array (
                'name' => 'taskd_task_design_taskd_task_design_1_name',
              ),
              14 => 
              array (
                'name' => 'category',
                'label' => 'LBL_CATEGORY',
              ),
              15 => 
              array (
                'readonly' => false,
                'name' => 'recurring_td_c',
                'label' => 'LBL_RECURRING_TD',
              ),
              16 => 
              array (
                'readonly' => false,
                'name' => 'recurrence_pattern_days_c',
                'label' => 'LBL_RECURRENCE_PATTERN_DAYS',
              ),
              17 => 
              array (
                'readonly' => false,
                'name' => 'recurrence_pattern_duration_c',
                'label' => 'LBL_RECURRENCE_PATTERN_DURATION',
              ),
              18 => 
              array (
                'readonly' => false,
                'name' => 'start_day_of_recurrence_c',
                'label' => 'LBL_START_DAY_OF_RECURRENCE',
              ),
              19 => 
              array (
                'readonly' => false,
                'name' => 'order_start_integer_c',
                'label' => 'LBL_ORDER_START_INTEGER',
              ),
              20 => 
              array (
                'name' => 'start_integer',
                'label' => 'LBL_START_INTEGER',
              ),
              21 => 
              array (
                'name' => 'end_integer',
                'label' => 'LBL_END_INTEGER',
              ),
              22 => 
              array (
                'name' => 'integer_units',
                'studio' => 'visible',
                'label' => 'LBL_INTEGER_UNITS',
              ),
              23 => 
              array (
                'name' => 'time_window',
                'label' => 'LBL_TIME_WINDOW',
              ),
              24 => 
              array (
                'name' => 'equipment_required',
                'label' => 'LBL_EQUIPMENT_REQUIRED',
              ),
              25 => 
              array (
              ),
              26 => 
              array (
                'readonly' => false,
                'name' => 'other_equipment_c',
                'studio' => 'visible',
                'label' => 'LBL_OTHER_EQUIPMENT',
                'span' => 12,
              ),
              27 => 
              array (
                'name' => 'task_type',
                'label' => 'LBL_TASK_TYPE',
              ),
              28 => 
              array (
                'name' => 'custom_task',
                'label' => 'LBL_CUSTOM_TASK',
              ),
              29 => 
              array (
                'name' => 'standard_task',
                'label' => 'LBL_STANDARD_TASK',
              ),
              30 => 
              array (
                'readonly' => false,
                'name' => 'phase_c',
                'label' => 'LBL_PHASE',
              ),
              31 => 
              array (
                'readonly' => false,
                'name' => 'type_of_personnel_2_c',
                'label' => 'LBL_TYPE_OF_PERSONNEL_2',
              ),
              32 => 
              array (
                'readonly' => false,
                'name' => 'duration_integer_min_c',
                'label' => 'LBL_DURATION_INTEGER_MIN',
              ),
              33 => 
              array (
                'name' => 'days_from_initial_task',
                'label' => 'LBL_DAYS_FROM_INITIAL_TASK',
                'span' => 12,
              ),
              34 => 
              array (
                'name' => 'scheduled_start_datetime',
                'label' => 'LBL_SCHEDULED_START_DATETIME',
              ),
              35 => 
              array (
                'name' => 'scheduled_end_datetime',
                'label' => 'LBL_SCHEDULED_END_DATETIME',
              ),
              36 => 
              array (
                'name' => 'planned_start_datetime_1st',
                'label' => 'LBL_PLANNED_START_DATETIME_1ST',
              ),
              37 => 
              array (
                'name' => 'planned_end_datetime_1st',
                'label' => 'LBL_PLANNED_END_DATETIME_1ST',
              ),
              38 => 
              array (
                'name' => 'planned_start_datetime_2nd',
                'label' => 'LBL_PLANNED_START_DATETIME_2ND',
              ),
              39 => 
              array (
                'name' => 'planned_end_datetime_2nd',
                'label' => 'LBL_PLANNED_END_DATETIME_2ND',
              ),
              40 => 
              array (
                'name' => 'actual_datetime',
                'label' => 'LBL_ACTUAL_DATETIME',
              ),
              41 => 
              array (
              ),
              42 => 
              array (
                'name' => 'description',
                'comment' => 'Full text of the note',
                'label' => 'LBL_DESCRIPTION',
                'span' => 12,
              ),
              43 => 
              array (
                'readonly' => false,
                'name' => 'completion_status_c',
                'label' => 'LBL_COMPLETION_STATUS_C',
                'span' => 12,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
