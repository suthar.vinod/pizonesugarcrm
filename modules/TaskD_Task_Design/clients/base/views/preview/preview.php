<?php
$module_name = 'TaskD_Task_Design';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
                'size' => 'large',
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'm03_work_product_taskd_task_design_1_name',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'audit_2_c',
                'label' => 'LBL_AUDIT_2',
              ),
              2 => 
              array (
                'name' => 'm03_work_product_code_taskd_task_design_1_name',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'audit_phase_c',
                'label' => 'LBL_AUDIT_PHASE',
              ),
              4 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'order_2_c',
                'label' => 'LBL_ORDER_2',
              ),
              6 => 
              array (
                'name' => 'anml_animals_taskd_task_design_1_name',
              ),
              7 => 
              array (
                'readonly' => false,
                'name' => 'usda_id_c',
                'label' => 'LBL_USDA_ID',
              ),
              8 => 
              array (
                'name' => 'relative',
                'label' => 'LBL_RELATIVE',
              ),
              9 => 
              array (
                'name' => 'taskd_task_design_taskd_task_design_1_name',
              ),
              10 => 
              array (
                'name' => 'category',
                'label' => 'LBL_CATEGORY',
              ),
              11 => 
              array (
              ),
              12 => 
              array (
                'name' => 'start_integer',
                'label' => 'LBL_START_INTEGER',
              ),
              13 => 
              array (
                'name' => 'end_integer',
                'label' => 'LBL_END_INTEGER',
              ),
              14 => 
              array (
                'name' => 'integer_units',
                'studio' => 'visible',
                'label' => 'LBL_INTEGER_UNITS',
              ),
              15 => 
              array (
                'name' => 'time_window',
                'label' => 'LBL_TIME_WINDOW',
              ),
              16 => 
              array (
                'name' => 'equipment_required',
                'label' => 'LBL_EQUIPMENT_REQUIRED',
              ),
              17 => 
              array (
              ),
              18 => 
              array (
                'readonly' => false,
                'name' => 'other_equipment_c',
                'studio' => 'visible',
                'label' => 'LBL_OTHER_EQUIPMENT',
                'span' => 12,
              ),
              19 => 
              array (
                'name' => 'task_type',
                'label' => 'LBL_TASK_TYPE',
              ),
              20 => 
              array (
                'name' => 'custom_task',
                'label' => 'LBL_CUSTOM_TASK',
              ),
              21 => 
              array (
                'name' => 'standard_task',
                'label' => 'LBL_STANDARD_TASK',
              ),
              22 => 
              array (
                'readonly' => false,
                'name' => 'phase_c',
                'label' => 'LBL_PHASE',
              ),
              23 => 
              array (
                'readonly' => false,
                'name' => 'type_of_personnel_2_c',
                'label' => 'LBL_TYPE_OF_PERSONNEL_2',
              ),
              24 => 
              array (
                'readonly' => false,
                'name' => 'duration_integer_min_c',
                'label' => 'LBL_DURATION_INTEGER_MIN',
              ),
              25 => 
              array (
                'name' => 'days_from_initial_task',
                'label' => 'LBL_DAYS_FROM_INITIAL_TASK',
                'span' => 12,
              ),
              26 => 
              array (
                'name' => 'scheduled_start_datetime',
                'label' => 'LBL_SCHEDULED_START_DATETIME',
              ),
              27 => 
              array (
                'name' => 'scheduled_end_datetime',
                'label' => 'LBL_SCHEDULED_END_DATETIME',
              ),
              28 => 
              array (
                'name' => 'planned_start_datetime_1st',
                'label' => 'LBL_PLANNED_START_DATETIME_1ST',
              ),
              29 => 
              array (
                'name' => 'planned_end_datetime_1st',
                'label' => 'LBL_PLANNED_END_DATETIME_1ST',
              ),
              30 => 
              array (
                'name' => 'planned_start_datetime_2nd',
                'label' => 'LBL_PLANNED_START_DATETIME_2ND',
              ),
              31 => 
              array (
                'name' => 'planned_end_datetime_2nd',
                'label' => 'LBL_PLANNED_END_DATETIME_2ND',
              ),
              32 => 
              array (
                'name' => 'actual_datetime',
                'label' => 'LBL_ACTUAL_DATETIME',
              ),
              33 => 
              array (
              ),
              34 => 
              array (
                'name' => 'description',
                'comment' => 'Full text of the note',
                'label' => 'LBL_DESCRIPTION',
                'span' => 12,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);
