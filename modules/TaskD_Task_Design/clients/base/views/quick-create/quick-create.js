/**
 * @class View.Views.Base.QuickCreateView
 * @alias SUGAR.App.view.views.BaseQuickCreateView
 * @extends View.Views.Base.BaseeditmodalView
 */
 ({
    extendsFrom: 'BaseeditmodalView',
    fallbackFieldTemplate: 'edit',
    initialize: function(options) {
        app.view.View.prototype.initialize.call(this, options);
        if (this.layout) {
            /*Get the current group design id to get the related current WP from group design's api */
            var GDesignId = this.context.get('GDesignId');           
            var GDAPIUrl = "GD_Group_Design/"+ GDesignId+"/get_wp_of_gdlinked";
  
            this.layout.on('app:view:quick-create', function() {
                let self = this;
                /*API to fetch the current WP & Current GD name */              
                        app.api.call("read", app.api.buildURL(GDAPIUrl), null, {
                            success: function(Response) {
                                self.getOAuthToken = app.api.getOAuthToken();
                                console.log('getOAuthToken of GDAPIUrl :', self.getOAuthToken);
                                console.log('Response : ', Response);
                                self.ResData = Response.data;
                                console.log('length :',self.ResData.length);
                                if(self.ResData.length == 0)
                                {
                                  app.alert.show("NoRecord", {
                                      level: "error",
                                      messages: "There is no other record of group design related to same work product.."
                                  });
                                }
                                else {
                                    self.render();
                                    var WP = $('#Wplist').val();
                                    var GDname = $('#Wplist').find('option:selected').attr('currntgdname');                                                                       
                                    var WPGDUrl = "GD_Group_Design/" + WP + "/" + GDname + "/get_gd_of_currentWP";                                    
                                    app.api.call("read", app.api.buildURL(WPGDUrl), null, {
                                        success: function (res) {
                                            console.log('Response : ', res);                                            
                                            self.resData = res.data;                                           
                                            if (res == '') {
                                                $('.subbtn').css('pointer-events', 'none');
                                                $('.subbtn').css('opacity', '0.3');
                                            }
                                            else {
                                                $('.subbtn').css('pointer-events', 'unset');
                                                $('.subbtn').css('opacity', 'unset');
                                            }
                                            $("#GDlist option").remove();
                                            $('#GDlist').append(res);
                                            console.log('getOAuthToken of WPGDUrl:', self.getOAuthToken);
                                        },
                                    })        
                                    self.$('.modal').modal({
                                        backdrop: 'static',
                                        tabindex: '-1',
                                        keyboard: false
                                    });
                                    self.$('.modal').modal('show');
                                    $('.datepicker').css('z-index', '20000');
                                    app.$contentEl.attr('aria-hidden', true);
                                    $('.modal-backdrop').insertAfter($('.modal'));
                                }
                                
  
                            },
                        })
  
  
            }, this);
        }
        this.bindDataChange();
    },
    /**Overriding the base saveButton method*/
    saveButton: function() {        
        var SelectedGD = this.context.get('GDesignId');
        var GDesignId_Curr = $("#GDlist option:selected").val();
        var CopyTDUrl = "GD_Group_Design/" + GDesignId_Curr + "/" + SelectedGD + "/copy_task_design_to_gd"; 
       // alert(SelectedGD); 
       // alert(GDesignId_Curr); 
       // alert(CopyTDUrl);       
        $(".subbtn").addClass("disabled");
        $(".cancle-btn").addClass("disabled");
        var classname = '';
        app.alert.show("LoadingRes", {
          level: "process",
          title: "In Process..."
      });
        /*Post the current group design id and selected group design id to process the copy task design process to selected gd from current gd */       
       app.api.call("read", app.api.buildURL(CopyTDUrl), null, {
            success: function (dataResponse) {
                console.log('Response of copy td api : ', dataResponse);
                //alert(dataResponse);
                //alert(res);
                //self.resultData = dataResponse.data;
                self.getOAuthToken = app.api.getOAuthToken();
                if(dataResponse=='Error! Something went wrong. Please try again.')
                {
                  classname = "error";
                }
                else
                {
                  classname = "success";
                }
                console.log('getOAuthToken of copy td api :', self.getOAuthToken);
                app.alert.show("Response", {
                    level: classname,
                    messages: dataResponse
                });                
                  setTimeout(function() {
                      $('.modal').modal('hide');
                  }, 00);
                  $(".alert-process").hide();
            },
        })
  
  
    },  
    
    cancelButton: function() {
        $('.modal').modal('hide');
        if (Modernizr.touch) {
            app.$contentEl.removeClass('content-overflow-visible');
        }
        //this.$('.modal').modal('hide').find('form').get(0).reset();
        if (this.context.has('createModel')) {
            this.context.get('createModel').clear();
        }
    },
  
    /**Custom method to dispose the view*/
     _disposeView: function() {
         /**Find the index of the view in the components list of the layout*/
        var index = _.indexOf(this.layout._components, _.findWhere(this.layout._components, {
          name: 'quick-create'
        }));
        if (index > -1) {
            /** dispose the view so that the evnets, context elements etc created by it will be released*/
            this.layout._components[index].dispose();
            /**remove the view from the components list**/
            this.layout._components.splice(index, 1);
         }
     },
         
  })