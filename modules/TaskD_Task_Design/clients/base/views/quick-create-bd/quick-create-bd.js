/**
 * @class View.Views.Base.QuickCreateView
 * @alias SUGAR.App.view.views.BaseQuickCreateView
 * @extends View.Views.Base.BaseeditmodalView
 */
({
    extendsFrom: 'BaseeditmodalView',
    fallbackFieldTemplate: 'edit',
    initialize: function (options) {
        app.view.View.prototype.initialize.call(this, options);
        if (this.layout) {
            /*Get the current group design id to get the related current WP from group design's api */
            var WPId = this.context.get('WPId');
            var BDAPIUrl = "M03_Work_Product/" + WPId;

            this.layout.on('app:view:quick-create-bd', function () {
                let self = this;
                /*API to fetch the current WP & Current GD name */
                app.api.call("read", app.api.buildURL(BDAPIUrl), null, {
                    success: function (resdata) {
                        var CurrentWP = resdata['bid_batch_id_m03_work_product_1']['id'];
                        /*API to fetch the other group design's records of same work product */
                        var APIUrl = "BID_Batch_ID?erased_fields=true&fields=id,name&order_by=date_modified:desc&filter[0][bid_batch_id_m03_work_product_1.id][$in][]=" + WPId;
                        app.api.call("read", app.api.buildURL(APIUrl), null, {
                            success: function (Response) {
                                console.log('Response : ', Response);
                                self.ResData = Response.records;
                                console.log('length :', self.ResData.length);
                                if (self.ResData.length == 0) {
                                    //alert('There is no other record of groud design related to same work product.')
                                    app.alert.show("NoRecord", {
                                        level: "error",
                                        messages: "This Work Product does not have a relationship to a Batch ID. Please link it to a Batch ID prior to copying."
                                    });
                                }
                                else {
                                    self.render();
                                    /** #2428 : 04 may 2022 : Update to Copy All to Batch Design : Need to show the dropdown
                                     * of GD based on selected work product*/
                                    var WPGDUrl = "GD_Group_Design/" + WPId + "/get_gd_of_selectedWP";
                                    app.api.call("read", app.api.buildURL(WPGDUrl), null, {
                                        success: function (res) {
                                            console.log('Response : ', res);
                                            //alert(res);
                                            self.resData = res.data;
                                            self.getOAuthToken = app.api.getOAuthToken();
                                            /**Hide the drop down box of GD if there is no task linked with Type = Plan - TS */
                                            if (res == '') {
                                                $('#GDlistGRP').css('display', 'none');
                                                $('#GDlistOPT').css('display', 'none');
                                            }
                                            else {
                                                $('#GDlistGRP').css('display', 'block');
                                                $('#GDlistOPT').css('display', 'block');
                                            }
                                            $('#GDlist').append(res);
                                            console.log('getOAuthToken :', self.getOAuthToken);
                                        },
                                    })

                                    self.$('.modal').modal({
                                        backdrop: 'static'
                                    });
                                    self.$('.modal').modal('show');
                                    $('.datepicker').css('z-index', '20000');
                                    app.$contentEl.attr('aria-hidden', true);
                                    $('.modal-backdrop').insertAfter($('.modal'));

                                }


                            },
                        })

                    },
                })

            }, this);
        }
        this.bindDataChange();
    },
    /**Overriding the base saveButton method*/
    saveButton: function () {
        /** Copy from current wp , so we will send wp id to api */
        var WPId_Curr = this.context.get('WPId');
        /** Copy to selected batch design, so we will send selected bd id to api */
        var SelectedBD = $("#BDlist").attr("value");
        var SelectedGD = $("#GDlist option:selected").val();
        if(SelectedGD=='' || SelectedGD == null)
        {
            SelectedGD = '00';
        }
        
        var CopyTDUrl = "BID_Batch_ID/" + WPId_Curr + "/" + SelectedBD + "/" + SelectedGD + "/copy_task_design_to_bd";
        $(".subbtn").addClass("disabled");
        $(".cancle-btn").addClass("disabled");
        var classname = '';
        app.alert.show("LoadingRes", {
            level: "process",
            title: "In Process..."
        });
        /*Post the current wp id and selected batch design id to process the copy task design process to selected bd from current wp */
        app.api.call("read", app.api.buildURL(CopyTDUrl), null, {
            success: function (dataResponse) {
                console.log('Response of copy td api line 81 : ', dataResponse);
                //alert(dataResponse);
                //alert(res);
                //self.resultData = dataResponse.data;
                self.getOAuthToken = app.api.getOAuthToken();
                if (dataResponse == 'Error! There is no task designs linked to selected work product. Please link it to a work product prior to copying.') {
                    classname = "error";
                }
                else {
                    classname = "success";
                }
                console.log('getOAuthToken of copy td api :', self.getOAuthToken);
                app.alert.show("Response", {
                    level: classname,
                    messages: dataResponse
                });
                setTimeout(function () {
                    $('.modal').modal('hide');
                }, 100);
                $(".alert-process").hide();
            },
        })


    },

    cancelButton: function () {
        $('.modal').modal('hide');
        if (Modernizr.touch) {
            app.$contentEl.removeClass('content-overflow-visible');
        }
        //this.$('.modal').modal('hide').find('form').get(0).reset();
        if (this.context.has('createModel')) {
            this.context.get('createModel').clear();
        }
    },

    /**Custom method to dispose the view*/
    _disposeView: function () {
        /**Find the index of the view in the components list of the layout*/
        var index = _.indexOf(this.layout._components, _.findWhere(this.layout._components, {
            name: 'quick-create-bd'
        }));
        if (index > -1) {
            /** dispose the view so that the evnets, context elements etc created by it will be released*/
            this.layout._components[index].dispose();
            /**remove the view from the components list**/
            this.layout._components.splice(index, 1);
        }
    },

})