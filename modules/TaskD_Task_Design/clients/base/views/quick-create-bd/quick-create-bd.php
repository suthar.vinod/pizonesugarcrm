<?php
/** Metdata for the add note custom popup view
 * The buttons array contains the buttons to be shown in the popu
 * The fields array can be modified accordingly to display more number of fields if required
 *
 */
$viewdefs['TaskD_Task_Design']['base']['view']['quick-create-bd'] = array(
    'buttons' => array(
        array(
            'name' => 'cancel_button',
            'type' => 'button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'value' => 'Cancel',
            'css_class' => 'btn-invisible btn-link cancle-btn',
        ) ,
        array(
            'name' => 'save_button',
            'type' => 'button',
            'label' => 'Paste',
            'value' => 'Paste',
            'css_class' => 'btn-primary subbtn',
        ) ,
    )
);

