/**
 * @class View.Views.Base.QuickCreateView
 * @alias SUGAR.App.view.views.BaseQuickCreateView
 * @extends View.Views.Base.BaseeditmodalView
 */
 ({
    extendsFrom: 'BaseeditmodalView',
    fallbackFieldTemplate: 'edit',
    initialize: function (options) {
        app.view.View.prototype.initialize.call(this, options);
        if (this.layout) {
            /*#2384 : Get all the wp records and set into drop down list */
            var WPId = this.context.get('WPId');
            var WPAPIUrl = "M03_Work_Product?erased_fields=true&fields=id,name&&order_by=name:asc&max_num=-1&filter[0][work_product_status_c][$in][]=Pending&filter[0][work_product_status_c][$in][]=In Development&filter[0][work_product_status_c][$in][]=Testing";
            this.layout.on('app:view:quick-create-wp', function () {
                let self = this;
                app.alert.show("LoadingRes", {
                    level: "process",
                    messages: "Loading Response..."
                });
                app.api.call("read", app.api.buildURL(WPAPIUrl), null, {
                    success: function (Response) {                       
                        console.log('Response : ', Response);
                        self.ResData = Response.records;
                        console.log('length :', self.ResData.length);
                        if (self.ResData.length == 0) {
                            app.alert.show("NoRecord", {
                                level: "error",
                                messages: "No work product found."
                            });
                        }
                        else {
                            $(".alert-process").hide();
                            self.render();
                            self.$('.modal').modal({
                                backdrop: 'static'
                            });
                            self.$('.modal').modal('show');
                            $('.datepicker').css('z-index', '20000');
                            app.$contentEl.attr('aria-hidden', true);
                            $('.modal-backdrop').insertAfter($('.modal'));

                        }


                    },
                })

            }, this);
        }
        this.bindDataChange();
    },
    /**Overriding the base saveButton method*/
    saveButton: function () {
        /** Copy from current wp , so we will send wp id to api */
        var WPId_Curr = this.context.get('WPId');
        /** Copy to selected wp, so we will send selected wp id to api */
        var SelectedWP = $("#WPlist option:selected").val();
        var CopyTDUrl = "M03_Work_Product/" + WPId_Curr + "/" + SelectedWP + "/copy_task_design_to_wp";
        $(".subbtn").addClass("disabled");
        $(".cancle-btn").addClass("disabled");
        var classname = '';
        app.alert.show("LoadingRes", {
            level: "process",
            title: "In Process..."
        });
        /*Post the current wp id and selected wp id to process the copy task design process to selected wp from current wp */
        app.api.call("read", app.api.buildURL(CopyTDUrl), null, {
            success: function (dataResponse) {
                console.log('Response of copy td api line 81 : ', dataResponse);
                //self.resultData = dataResponse.data;
                self.getOAuthToken = app.api.getOAuthToken();
                if (dataResponse == 'Task Designs have been copied successfully to the selected Work Product.') {
                    classname = "success";
                }
                else {
                    classname = "error";
                }
                console.log('getOAuthToken of copy td api :', self.getOAuthToken);
                app.alert.show("Response", {
                    level: classname,
                    messages: dataResponse
                });
                setTimeout(function () {
                    $('.modal').modal('hide');
                }, 100);
                $(".alert-process").hide();
            },
        })


    },

    cancelButton: function () {
        $('.modal').modal('hide');
        if (Modernizr.touch) {
            app.$contentEl.removeClass('content-overflow-visible');
        }
        if (this.context.has('createModel')) {
            this.context.get('createModel').clear();
        }
    },
    /**Custom method to dispose the view*/
    _disposeView: function () {
        /**Find the index of the view in the components list of the layout*/
        var index = _.indexOf(this.layout._components, _.findWhere(this.layout._components, {
            name: 'quick-create-wp'
        }));
        if (index > -1) {
            /** dispose the view so that the evnets, context elements etc created by it will be released*/
            this.layout._components[index].dispose();
            /**remove the view from the components list**/
            this.layout._components.splice(index, 1);
        }
    },

})