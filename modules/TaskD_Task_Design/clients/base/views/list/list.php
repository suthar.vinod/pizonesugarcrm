<?php
$module_name = 'TaskD_Task_Design';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'order_2_c',
                'label' => 'LBL_ORDER_2',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'time_window',
                'label' => 'LBL_TIME_WINDOW',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'taskd_task_design_taskd_task_design_1_name',
                'label' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_L_TITLE',
                'enabled' => true,
                'id' => 'TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1TASKD_TASK_DESIGN_IDA',
                'link' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'type_of_personnel_2_c',
                'label' => 'LBL_TYPE_OF_PERSONNEL_2',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'custom_task',
                'label' => 'LBL_CUSTOM_TASK',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'completion_status_c',
                'label' => 'LBL_COMPLETION_STATUS_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'duration_integer_min_c',
                'label' => 'LBL_DURATION_INTEGER_MIN',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'anml_animals_taskd_task_design_1_name',
                'label' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_ANML_ANIMALS_TITLE',
                'enabled' => true,
                'id' => 'ANML_ANIMALS_TASKD_TASK_DESIGN_1ANML_ANIMALS_IDA',
                'link' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'usda_id_c',
                'label' => 'LBL_USDA_ID',
                'enabled' => true,
                'readonly' => false,
                'sortable' => false,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'single_date_2_c',
                'label' => 'LBL_SINGLE_DATE_2',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'actual_datetime',
                'label' => 'LBL_ACTUAL_DATETIME',
                'enabled' => true,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'gd_group_design_taskd_task_design_1_name',
                'label' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
                'enabled' => true,
                'id' => 'GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1GD_GROUP_DESIGN_IDA',
                'link' => true,
                'default' => true,
              ),
              13 => 
              array (
                'name' => 'scheduled_start_datetime',
                'label' => 'LBL_SCHEDULED_START_DATETIME',
                'enabled' => true,
                'default' => true,
              ),
              14 => 
              array (
                'name' => 'scheduled_end_datetime',
                'label' => 'LBL_SCHEDULED_END_DATETIME',
                'enabled' => true,
                'default' => true,
              ),
              15 => 
              array (
                'name' => 'planned_start_datetime_1st',
                'label' => 'LBL_PLANNED_START_DATETIME_1ST',
                'enabled' => true,
                'default' => true,
              ),
              16 => 
              array (
                'name' => 'planned_end_datetime_1st',
                'label' => 'LBL_PLANNED_END_DATETIME_1ST',
                'enabled' => true,
                'default' => true,
              ),
              17 => 
              array (
                'name' => 'planned_start_datetime_2nd',
                'label' => 'LBL_PLANNED_START_DATETIME_2ND',
                'enabled' => true,
                'default' => true,
              ),
              18 => 
              array (
                'name' => 'planned_end_datetime_2nd',
                'label' => 'LBL_PLANNED_END_DATETIME_2ND',
                'enabled' => true,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
