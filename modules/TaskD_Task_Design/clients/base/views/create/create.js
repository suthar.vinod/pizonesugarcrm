({
    extendsFrom: 'CreateView',
    initialize: function(options) {
        this._super('initialize', [options]);
        //this.model.on("change:relative", this.setIntegerUnitDays, this);
        this.model.on("change:type_test_c", this.checkForMultiselect, this);

        this.model.on("change:integer_units change:start_integer change:end_integer", this.setTimeWindow, this);
        this.model.on("change:type_2", this.emptyTimeFields, this);
        this.model.on("change:relative", this.emptyTimeFieldsRelative, this);

        this.model.on("change:taskd_task_design_taskd_task_design_1_name change:start_integer change:end_integer change:integer_units change:relative", this.customCalculation, this);
        this.model.on("change:taskd_task_design_taskd_task_design_1_name", this.Taskdesignlink, this);

        this.model.on("change:equipment_required", this.fun_equipment_required, this);
        this.model.addValidationTask('other_equipment_c', _.bind(this._doValidateOtherEquipment, this));

        this.on('render', _.bind(this.onload_function, this));
    },

    _doValidateOtherEquipment: function(fields, errors, callback) {
        var equipment_required = this.model.get("equipment_required");
        var other_equipment_c = this.model.get("other_equipment_c");
        //console.log('1qqq',other_equipment_c)
        //console.log('11',equipment_required.indexOf('Other'))
        if (equipment_required.indexOf('Other') >= 0 && (other_equipment_c == "" || other_equipment_c == undefined))
        {
            //console.log('2')
            errors['other_equipment_c'] = errors['other_equipment_c'] || {};
            errors['other_equipment_c'].required = true;
            
        }
        callback(null, fields, errors);        
    },

    fun_equipment_required: function(test) {
        var equipment_required = this.model.get("equipment_required");
        if(equipment_required == ""){
            //console.log('3',equipment_required)
            this.$el.find('[data-name="other_equipment_c"]').css('visibility', 'hidden');
            this.$el.find('[data-name="other_equipment_c"] textarea[name="other_equipment_c"]').attr('placeholder', '');
        }else{
            //console.log('4',equipment_required)
            if (_.indexOf(equipment_required, "Other") === -1){
                this.$el.find('[data-name="other_equipment_c"]').css('visibility', 'hidden');
                this.$el.find('[data-name="other_equipment_c"] textarea[name="other_equipment_c"]').attr('placeholder', '');
            }else{
                this.$el.find('[data-name="other_equipment_c"]').css('visibility', 'unset');
                this.$el.find('[data-name="other_equipment_c"] textarea[name="other_equipment_c"]').attr('placeholder', 'Required');
            }
        }
    },
    /* #791 : Task Design - Work Product Relationship field */
    onload_function: function() {
        var equipment_required = this.model.get("equipment_required");
        if(equipment_required == ""){
            this.$el.find('[data-name="other_equipment_c"]').css('visibility', 'hidden'); 
        }
        var self = this;
        // Check if there's related parent record
        if (!_.isEmpty(this.model.link)) {
            var parent_module = this.model.link.bean.attributes._module;
            // Check if  related parent record is GD_Group_Design
            if (parent_module == "GD_Group_Design") {
                // Set the work product value into wp of task design from wp of GD_Group_Design 
                this.model.attributes.m03_work_product_taskd_task_design_1m03_work_product_ida = this.model.link.bean.attributes.m03_work_product_gd_group_design_1m03_work_product_ida;
                this.model.attributes.m03_work_product_taskd_task_design_1_name = this.model.link.bean.attributes.m03_work_product_gd_group_design_1_name;

                let batchID = this.model.link.bean.attributes.bid_batch_id_gd_group_design_1bid_batch_id_ida;
                let workProductId = this.model.link.bean.attributes.m03_work_product_gd_group_design_1m03_work_product_ida;

                /*#792 */
                this.model.set('type_2', "Plan");
                // $('div[data-name="type_2"]').css('pointer-events', 'none');
                var self = this;
                setTimeout(function() {

                    self.model.set("gd_group_design_taskd_task_design_1gd_group_design_ida", self.model.link.bean.attributes.id);
                    self.model.set("gd_group_design_taskd_task_design_1_name", self.model.link.bean.attributes.name);

                    self.$el.find('input[name="gd_group_design_taskd_task_design_1_name"]').prop('disabled', true);
                    self.$el.find('input[name="m03_work_product_taskd_task_design_1_name"]').prop('disabled', true);
                    self.$el.find('input[name="type_2"]').prop('disabled', true);
                }, 500);
                if(batchID != "") {
                    // console.log("Inside batchid");
                    $('[data-name="m03_work_product_taskd_task_design_1_name"]').css("visibility", "hidden");
                    this.model.attributes.bid_batch_id_taskd_task_design_1bid_batch_id_ida = this.model.link.bean.attributes.bid_batch_id_gd_group_design_1bid_batch_id_ida;
                    this.model.attributes.bid_batch_id_taskd_task_design_1_name = this.model.link.bean.attributes.bid_batch_id_gd_group_design_1_name;
                    var self = this;
                    setTimeout(function () {
                        self.$el.find('input[name="bid_batch_id_taskd_task_design_1_name"]').prop('disabled', true);
                    }, 500);
                }
                else if(workProductId != "") {
                    // console.log("Inside workproduct");
                    $('[data-name="bid_batch_id_taskd_task_design_1_name"]').css("visibility", "hidden");
                    this.model.attributes.m03_work_product_taskd_task_design_1m03_work_product_ida = this.model.link.bean.attributes.m03_work_product_gd_group_design_1m03_work_product_ida;
                    this.model.attributes.m03_work_product_taskd_task_design_1_name = this.model.link.bean.attributes.m03_work_product_gd_group_design_1_name;
                    // $('div[data-name="type_2"]').css('pointer-events', 'none');
                    var self = this;
                    setTimeout(function () {
                        self.$el.find('input[name="m03_work_product_taskd_task_design_1_name"]').prop('disabled', true);
                    }, 500);
                }
            } else {
                self.$el.find('input[name="gd_group_design_taskd_task_design_1_name"]').prop('disabled', false);
                self.$el.find('input[name="m03_work_product_taskd_task_design_1_name"]').prop('disabled', false);
                self.$el.find('input[name="type_2"]').prop('disabled', false);
                self.$el.find('input[name="bid_batch_id_taskd_task_design_1_name"]').prop('disabled', false);
            }

            if (parent_module == "M03_Work_Product_Code") {
                /*#1489 */
                this.model.set('type_2', "Plan SP");
                // $('div[data-name="type_2"]').css('pointer-events', 'none');
                var self = this;
                setTimeout(function() {
                    self.$el.find('input[name="m03_work_product_code_taskd_task_design_1_name"]').prop('disabled', true);
                    self.$el.find('input[name="type_2"]').prop('disabled', true);
                }, 500);
            } else {
                self.$el.find('input[name="m03_work_product_code_taskd_task_design_1_name"]').prop('disabled', false);
                self.$el.find('input[name="type_2"]').prop('disabled', false);
            }


        }
        this._super("render");
    },
    checkForMultiselect: function(test) {
        var type_test = this.model.get("type_test_c");
        var type_test_p = this.model.previous("type_test_c");

        if (typeof type_test == 'object' && type_test.length > 1) {
            console.log(type_test[0]);
            console.log(type_test_p[0]);
            console.log(type_test[0] == type_test_p[0]);

            // // console.log(type_test[0]);
            // // console.log(type_test_p[0]);
            // // // console.log(typeof type_test);


            prev = typeof type_test_p == 'object' ? type_test_p[0] : type_test_p;
            curr = type_test[0];          
            value = JSON.stringify(curr === prev ? type_test[0] : type_test[1]);
            
            // type_test = type_test[type_test.length - 1];
            this.model.set("type_test_c", value);
        }

    },
    emptyTimeFields: function() {
        var type = this.model.get("type_2");
        if (type == "Plan") {
            this.model.set("actual_datetime", null);
            this.model.set("scheduled_start_datetime", null);
            this.model.set("scheduled_end_datetime", null);
            this.model.set("planned_start_datetime_1st", null);
            this.model.set("planned_end_datetime_1st", null);
            this.model.set("planned_start_datetime_2nd", null);
            this.model.set("planned_end_datetime_2nd", null);
        }
    },
    emptyTimeFieldsRelative: function() {
        this.model.set("taskd_task_design_taskd_task_design_1taskd_task_design_ida", null);
        this.model.set("taskd_task_design_taskd_task_design_1_name", null);
        var relative = this.model.get("relative");
        if (relative == "NA") {
            this.model.set("planned_end_datetime_1st", null);
            this.model.set("planned_start_datetime_1st", null);
            this.model.set("planned_start_datetime_2nd", null);
            this.model.set("planned_end_datetime_2nd", null);
        } else if (relative == "1st Tier") {
            this.model.set("actual_datetime", null);
            this.model.set("scheduled_start_datetime", null);
            this.model.set("scheduled_end_datetime", null);
            this.model.set("planned_start_datetime_2nd", null);
            this.model.set("planned_end_datetime_2nd", null);
        } else if (relative == "2nd Tier") {
            this.model.set("actual_datetime", null);
            this.model.set("scheduled_start_datetime", null);
            this.model.set("scheduled_end_datetime", null);
            this.model.set("planned_start_datetime_1st", null);
            this.model.set("planned_end_datetime_1st", null);
        }
    },
    setTimeWindow: function() {
        var integer_units = typeof this.model.get("integer_units") !== 'undefined' ? this.model.get("integer_units") : "";
        var start_integer = typeof this.model.get("start_integer") !== 'undefined' ? this.model.get("start_integer") : "";
        var end_integer = typeof this.model.get("end_integer") !== 'undefined' ? this.model.get("end_integer") : "";
        var newValue = integer_units + " " + start_integer + "-" + end_integer;
        this.model.set("time_window", newValue);

    },
    Taskdesignlink: function() {
        var taskDesignId = this.model.get("taskd_task_design_taskd_task_design_1taskd_task_design_ida");
        //alert(taskDesignId);
        this.model.set("task_design_linked_id_c", taskDesignId);  
        //this.model.save();
    },
    customCalculation: function() {
        if (this.model.get("relative") == "1st Tier" || this.model.get("relative") == "2nd Tier") {
            var taskDesignId = this.model.get("taskd_task_design_taskd_task_design_1taskd_task_design_ida");
            var startInteger = this.model.get("start_integer") == "" ? 0 : this.model.get("start_integer");
            var endInteger = this.model.get("end_integer") == "" ? 0 : this.model.get("end_integer");
            var integerUnit = this.model.get("integer_units");

            var relative = this.model.get("relative");
            var type_2 = this.model.get("type_2");

            var self = this;

            if (taskDesignId != undefined) {
                App.api.call("get", "rest/v10/TaskD_Task_Design/" + taskDesignId + "?fields=actual_datetime,scheduled_start_datetime,scheduled_end_datetime,planned_start_datetime_1st,planned_end_datetime_1st", null, {
                    success: function(data) {
                        var actualDateTime = data.actual_datetime;
                        var scheduleStartTime = data.scheduled_start_datetime;
                        var scheduleEndTime = data.scheduled_end_datetime;
                        var plannedStartDatetime1st = data.planned_start_datetime_1st;
                        var plannedEndDatetime1st = data.planned_end_datetime_1st;

                        if (actualDateTime == "") {
                            if (integerUnit == "Day") {
                                if ((type_2 == "Actual" || type_2 == "Actual SP") && relative == "1st Tier") {
                                    if (scheduleStartTime != "") {
                                        var result = new Date(scheduleStartTime);
                                        result.setDate(result.getDate() + startInteger);
                                        self.model.set("planned_start_datetime_1st", moment(result).format());
                                    }
                                    if (scheduleEndTime != "") {
                                        var result = new Date(scheduleEndTime);
                                        result.setDate(result.getDate() + endInteger);
                                        self.model.set("planned_end_datetime_1st", moment(result).format());
                                    }
                                }

                                if ((type_2 == "Actual" || type_2 == "Actual SP") && relative == "2nd Tier") {
                                    if (plannedStartDatetime1st != "") {
                                        var result = new Date(plannedStartDatetime1st);
                                        result.setDate(result.getDate() + startInteger);
                                        self.model.set("planned_start_datetime_2nd", moment(result).format());
                                    }
                                    if (plannedEndDatetime1st != "") {
                                        var result = new Date(plannedEndDatetime1st);
                                        result.setDate(result.getDate() + endInteger);
                                        self.model.set("planned_end_datetime_2nd", moment(result).format());
                                    }
                                }

                            } else if (integerUnit == "Hour") {

                                if ((type_2 == "Actual" || type_2 == "Actual SP") && relative == "1st Tier") {
                                    if (scheduleStartTime != "") {
                                        var result = new Date(scheduleStartTime);
                                        result.setHours(result.getHours() + startInteger);
                                        self.model.set("planned_start_datetime_1st", moment(result).format());
                                    }
                                    if (scheduleEndTime != "") {
                                        var result = new Date(scheduleEndTime);
                                        result.setHours(result.getHours() + endInteger);
                                        self.model.set("planned_end_datetime_1st", moment(result).format());
                                    }
                                }

                                if ((type_2 == "Actual" || type_2 == "Actual SP") && relative == "2nd Tier") {
                                    if (plannedStartDatetime1st != "") {
                                        var result = new Date(plannedStartDatetime1st);
                                        result.setHours(result.getHours() + startInteger);
                                        self.model.set("planned_start_datetime_2nd", moment(result).format());
                                    }
                                    if (plannedEndDatetime1st != "") {
                                        var result = new Date(plannedEndDatetime1st);
                                        result.setHours(result.getHours() + endInteger);
                                        self.model.set("planned_end_datetime_2nd", moment(result).format());
                                    }
                                }

                            } else {
                                if ((type_2 == "Actual" || type_2 == "Actual SP") && relative == "1st Tier") {
                                    if (scheduleStartTime != "") {
                                        var result = new Date(scheduleStartTime);
                                        result.setMinutes(result.getMinutes() + startInteger);
                                        self.model.set("planned_start_datetime_1st", moment(result).format());
                                    }
                                    if (scheduleEndTime != "") {
                                        var result = new Date(scheduleEndTime);
                                        result.setMinutes(result.getMinutes() + endInteger);
                                        self.model.set("planned_end_datetime_1st", moment(result).format());
                                    }
                                }

                                if ((type_2 == "Actual" || type_2 == "Actual SP") && relative == "2nd Tier") {
                                    if (plannedStartDatetime1st != "") {
                                        var result = new Date(plannedStartDatetime1st);
                                        result.setMinutes(result.getMinutes() + startInteger);
                                        self.model.set("planned_start_datetime_2nd", moment(result).format());
                                    }
                                    if (plannedEndDatetime1st != "") {
                                        var result = new Date(plannedEndDatetime1st);
                                        result.setMinutes(result.getMinutes() + endInteger);
                                        self.model.set("planned_end_datetime_2nd", moment(result).format());
                                    }
                                }
                            }
                        } else {
                            if (integerUnit == "Day") {

                                if ((type_2 == "Actual" || type_2 == "Actual SP") && relative == "1st Tier") {
                                    if (actualDateTime != "") {
                                        var result = new Date(actualDateTime);
                                        result.setDate(result.getDate() + startInteger);
                                        self.model.set("planned_start_datetime_1st", moment(result).format());

                                        var result = new Date(actualDateTime);
                                        result.setDate(result.getDate() + endInteger);
                                        self.model.set("planned_end_datetime_1st", moment(result).format());
                                    }
                                }

                                if ((type_2 == "Actual" || type_2 == "Actual SP") && relative == "2nd Tier") {
                                    if (actualDateTime != "") {
                                        var result = new Date(actualDateTime);
                                        result.setDate(result.getDate() + startInteger);
                                        self.model.set("planned_start_datetime_2nd", moment(result).format());

                                        var result = new Date(actualDateTime);
                                        result.setDate(result.getDate() + endInteger);
                                        self.model.set("planned_end_datetime_2nd", moment(result).format());
                                    }
                                }
                            } else if (integerUnit == "Hour") {

                                if ((type_2 == "Actual" || type_2 == "Actual SP") && relative == "1st Tier") {
                                    if (actualDateTime != "") {
                                        var result = new Date(actualDateTime);
                                        result.setHours(result.getHours() + startInteger);
                                        self.model.set("planned_start_datetime_1st", moment(result).format());

                                        var result = new Date(actualDateTime);
                                        result.setHours(result.getHours() + endInteger);
                                        self.model.set("planned_end_datetime_1st", moment(result).format());
                                    }
                                }
                                if ((type_2 == "Actual" || type_2 == "Actual SP") && relative == "2nd Tier") {
                                    if (actualDateTime != "") {
                                        var result = new Date(actualDateTime);
                                        result.setHours(result.getHours() + startInteger);
                                        self.model.set("planned_start_datetime_2nd", moment(result).format());

                                        var result = new Date(actualDateTime);
                                        result.setHours(result.getHours() + endInteger);
                                        self.model.set("planned_end_datetime_2nd", moment(result).format());
                                    }
                                }
                            } else {
                                if ((type_2 == "Actual" || type_2 == "Actual SP") && relative == "1st Tier") {
                                    if (actualDateTime != "") {
                                        var result = new Date(actualDateTime);
                                        result.setMinutes(result.getMinutes() + startInteger);
                                        self.model.set("planned_start_datetime_1st", moment(result).format());

                                        var result = new Date(actualDateTime);
                                        result.setMinutes(result.getMinutes() + endInteger);
                                        self.model.set("planned_end_datetime_1st", moment(result).format());
                                    }
                                }
                                if ((type_2 == "Actual" || type_2 == "Actual SP") && relative == "2nd Tier") {
                                    if (actualDateTime != "") {
                                        var result = new Date(actualDateTime);
                                        result.setMinutes(result.getMinutes() + startInteger);
                                        self.model.set("planned_start_datetime_2nd", moment(result).format());

                                        var result = new Date(actualDateTime);
                                        result.setMinutes(result.getMinutes() + endInteger);
                                        self.model.set("planned_end_datetime_2nd", moment(result).format());
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }
    },
    
})