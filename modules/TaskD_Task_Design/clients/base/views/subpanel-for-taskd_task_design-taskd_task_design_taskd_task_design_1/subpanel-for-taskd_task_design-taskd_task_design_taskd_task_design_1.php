<?php
// created: 2021-12-07 10:37:42
$viewdefs['TaskD_Task_Design']['base']['view']['subpanel-for-taskd_task_design-taskd_task_design_taskd_task_design_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'type_2',
          'label' => 'LBL_TYPE_2',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'relative',
          'label' => 'LBL_RELATIVE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'order_2_c',
          'label' => 'LBL_ORDER_2',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'category',
          'label' => 'LBL_CATEGORY',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'time_window',
          'label' => 'LBL_TIME_WINDOW',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'equipment_required',
          'label' => 'LBL_EQUIPMENT_REQUIRED',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'actual_datetime',
          'label' => 'LBL_ACTUAL_DATETIME',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'planned_start_datetime_1st',
          'label' => 'LBL_PLANNED_START_DATETIME_1ST',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'planned_end_datetime_1st',
          'label' => 'LBL_PLANNED_END_DATETIME_1ST',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'planned_start_datetime_2nd',
          'label' => 'LBL_PLANNED_START_DATETIME_2ND',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'planned_end_datetime_2nd',
          'label' => 'LBL_PLANNED_END_DATETIME_2ND',
          'enabled' => true,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'days_from_initial_task',
          'label' => 'LBL_DAYS_FROM_INITIAL_TASK',
          'enabled' => true,
          'default' => true,
        ),
        13 => 
        array (
          'name' => 'm03_work_product_taskd_task_design_1_name',
          'label' => 'LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_TITLE',
          'enabled' => true,
          'id' => 'M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1M03_WORK_PRODUCT_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        14 => 
        array (
          'name' => 'anml_animals_taskd_task_design_1_name',
          'label' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_ANML_ANIMALS_TITLE',
          'enabled' => true,
          'id' => 'ANML_ANIMALS_TASKD_TASK_DESIGN_1ANML_ANIMALS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        15 => 
        array (
          'name' => 'gd_group_design_taskd_task_design_1_name',
          'label' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
          'enabled' => true,
          'id' => 'GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1GD_GROUP_DESIGN_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        16 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);