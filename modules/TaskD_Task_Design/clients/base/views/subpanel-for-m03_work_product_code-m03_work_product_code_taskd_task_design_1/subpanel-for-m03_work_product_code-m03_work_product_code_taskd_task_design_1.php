<?php
// created: 2022-03-15 08:48:01
$viewdefs['TaskD_Task_Design']['base']['view']['subpanel-for-m03_work_product_code-m03_work_product_code_taskd_task_design_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'order_2_c',
          'label' => 'LBL_ORDER_2',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'relative',
          'label' => 'LBL_RELATIVE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'taskd_task_design_taskd_task_design_1_name',
          'label' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_L_TITLE',
          'enabled' => true,
          'id' => 'TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1TASKD_TASK_DESIGN_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'time_window',
          'label' => 'LBL_TIME_WINDOW',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'standard_task',
          'label' => 'LBL_STANDARD_TASK',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'phase_c',
          'label' => 'LBL_PHASE',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'custom_task',
          'label' => 'LBL_CUSTOM_TASK',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);