<?php
// created: 2022-04-26 11:05:34
$viewdefs['TaskD_Task_Design']['base']['view']['subpanel-for-bid_batch_id-bid_batch_id_taskd_task_design_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'order_2_c',
          'label' => 'LBL_ORDER_2',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'relative',
          'label' => 'LBL_RELATIVE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'taskd_task_design_taskd_task_design_1_name',
          'label' => 'LBL_TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_L_TITLE',
          'enabled' => true,
          'id' => 'TASKD_TASK_DESIGN_TASKD_TASK_DESIGN_1TASKD_TASK_DESIGN_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'category',
          'label' => 'LBL_CATEGORY',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'time_window',
          'label' => 'LBL_TIME_WINDOW',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'actual_datetime',
          'label' => 'LBL_ACTUAL_DATETIME',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'scheduled_start_datetime',
          'label' => 'LBL_SCHEDULED_START_DATETIME',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'scheduled_end_datetime',
          'label' => 'LBL_SCHEDULED_END_DATETIME',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'planned_start_datetime_1st',
          'label' => 'LBL_PLANNED_START_DATETIME_1ST',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'planned_end_datetime_1st',
          'label' => 'LBL_PLANNED_END_DATETIME_1ST',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'planned_start_datetime_2nd',
          'label' => 'LBL_PLANNED_START_DATETIME_2ND',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'planned_end_datetime_2nd',
          'label' => 'LBL_PLANNED_END_DATETIME_2ND',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        13 => 
        array (
          'name' => 'type_of_personnel_2_c',
          'label' => 'LBL_TYPE_OF_PERSONNEL_2',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        14 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);