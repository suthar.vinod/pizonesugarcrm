({
    extendsFrom:'EditablelistbuttonField',
    initialize: function(options) {
     this._super("initialize", [options]);
    // this.model.on('change: error_category_c', this.function_error_category_c,this);
     
    },
    _loadTemplate: function() {
		this._super('_loadTemplate'); 
		var type_2 = this.model.get('type_2');
		var gd_group_design_taskd_task_design_1_name = this.model.get('gd_group_design_taskd_task_design_1_name');
		var anml_animals_taskd_task_design_1_name = this.model.get('anml_animals_taskd_task_design_1_name');

        if (gd_group_design_taskd_task_design_1_name!="" || anml_animals_taskd_task_design_1_name!="") {            
            $('input[name="type_2"]').prop('disabled', true);
            $('div[data-name="type_2"]').css('pointer-events', 'none');
            $('[name="type_2"]').attr('readOnly',true);
        }else{
            $('input[name="type_2"]').prop('disabled', false);
            $('div[data-name="type_2"]').css('pointer-events', 'unset');
            $('[name="type_2"]').attr('readOnly',false);
        }
    }, 
})
