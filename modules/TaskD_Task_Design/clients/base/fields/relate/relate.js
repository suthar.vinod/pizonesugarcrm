({
    extendsFrom: 'RelateField',
    initialize: function(options) {
        this._super('initialize', [options]);

    },
    openSelectDrawer: function() {

        if (this.getSearchModule() == "TaskD_Task_Design" && this.model.get("m03_work_product_taskd_task_design_1_name") != "" && this.model.get("m03_work_product_taskd_task_design_1_name") != undefined) {
            this.selectTask_Design();

            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else if (this.getSearchModule() == "TaskD_Task_Design" && this.model.get("bid_batch_id_taskd_task_design_1_name") != "" && this.model.get("bid_batch_id_taskd_task_design_1_name") != undefined) {
            this.selectTask_DesignBatchID();

            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else if(this.getSearchModule() == "TaskD_Task_Design" && this.model.get("m03_work_product_code_taskd_task_design_1_name") != "" && this.model.get("m03_work_product_code_taskd_task_design_1_name") != undefined) {
            this.selectTask_Designcode();

            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else if (this.getSearchModule() == "U_Units") {
            this.selectIntegerUnit();

            $("div.main-pane.span8 div.search-filter").hide();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else {
            this._super('openSelectDrawer');
            $("button.btn.btn-invisible.btn-dark").css("display", "block");
            $(".filter-definition-container").css('pointer-events', 'unset');
            $(".choice-filter-clickable").css('pointer-events', 'unset');

        }

    },

    selectTask_Design: function() {
        var m03_work_product_name = this.model.get("m03_work_product_taskd_task_design_1m03_work_product_ida");
        var relativeVal = this.model.get("relative");
        if (relativeVal == '1st Tier') {
            relative = 'NA';
        } else if (relativeVal == '2nd Tier') {
            relative = '1st Tier';
        }
        console.log('m03_work_product_name', m03_work_product_name);
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterTaskDesign',
                'initial_filter_label': 'Work Product',
                'filter_populate': {
                    'm03_work_product_taskd_task_design_1_name': [m03_work_product_name],
                    'relative': [relative],
                },
                'filter_relate': {
                    'm03_work_product_taskd_task_design_1_name': [m03_work_product_name],
                    'relative': [relative],
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Test system relate field.
        filterOptions = (this.getSearchModule() == "TaskD_Task_Design") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },

    selectTask_DesignBatchID: function() {
        var BID_Batch_ID_name = this.model.get("bid_batch_id_taskd_task_design_1bid_batch_id_ida");
        var relativeVal = this.model.get("relative");
        if (relativeVal == '1st Tier') {
            relative = 'NA';
        } else if (relativeVal == '2nd Tier') {
            relative = '1st Tier';
        }
        console.log('bid_batch_id_Name', BID_Batch_ID_name);
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterTaskDesignBatchID',
                'initial_filter_label': 'Batch ID',
                'filter_populate': {
                    'bid_batch_id_taskd_task_design_1_name': [BID_Batch_ID_name],
                    'relative': [relative],
                },
                'filter_relate': {
                    'bid_batch_id_taskd_task_design_1_name': [BID_Batch_ID_name],
                    'relative': [relative],
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Test system relate field.
        filterOptions = (this.getSearchModule() == "TaskD_Task_Design") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },

    selectTask_Designcode: function() {
        var m03_work_product_code_name = this.model.get("m03_work_p7fc0ct_code_ida");
        var relativeVal = this.model.get("relative");
        if (relativeVal == '1st Tier') {
            relative = 'NA';
        } else if (relativeVal == '2nd Tier') {
            relative = '1st Tier';
        }
        console.log('m03_work_product_code_name', m03_work_product_code_name);
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterTaskDesigncode',
                'initial_filter_label': 'Work Product Code',
                'filter_populate': {
                    'm03_work_product_code_taskd_task_design_1_name': [m03_work_product_code_name],
                    'relative': [relative],
                },
                'filter_relate': {
                    'm03_work_product_code_taskd_task_design_1_name': [m03_work_product_code_name],
                    'relative': [relative],
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Test system relate field.
        filterOptions = (this.getSearchModule() == "TaskD_Task_Design") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },

    selectIntegerUnit: function() {

        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterUnitTemplate',
                'initial_filter_label': 'Units',
                'filter_populate': {
                    'name': ["Day", "Hour", "Minute"],
                },
                'filter_relate': {
                    'name': ["Day", "Hour", "Minute"],
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Test system relate field.
        filterOptions = (this.getSearchModule() == "U_Units") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    /*To disable search text box filter of Test System */
    search: _.debounce(function(query) {
        var term = query.term || '',
            self = this,
            searchModule = this.getSearchModule(),
            params = {},
            limit = self.def.limit || 5,
            relatedModuleField = this.getRelatedModuleField();

        if (query.context) {
            params.offset = this.searchCollection.next_offset;
        }

        params.filter = this.buildFilterDefinition(term);

        this.searchCollection.fetch({
            //Don't show alerts for this request
            showAlerts: false,
            update: true,
            remove: _.isUndefined(params.offset),
            fields: this.getSearchFields(),
            context: self,
            params: params,
            //limit: limit,
            success: function(data) {
                var fetch = { results: [], more: data.next_offset > 0, context: data };
                if (fetch.more) {
                    var fieldEl = self.$(self.fieldTag),
                        //For teamset widget, we should specify which index element to be filled in
                        plugin = (fieldEl.length > 1) ? $(fieldEl.get(self.currentIndex)).data("select2") : fieldEl.data("select2"),
                        height = plugin.searchmore.children("li:first").children(":first").outerHeight(),
                        //0.2 makes scroll not to touch the bottom line which avoid fetching next record set
                        maxHeight = height * (limit - .2);
                    plugin.results.css("max-height", maxHeight);
                }
                _.each(data.models, function(model, index) {
                    if (params.offset && index < params.offset) {
                        return;
                    }
                    fetch.results.push({
                        id: model.id,
                        text: model.get(relatedModuleField) + ''
                    });
                });
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback(fetch);
                }
            },
            error: function() {
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback({ results: [] });
                }
                app.logger.error("Unable to fetch the bean collection.");
            }
        });

    }, app.config.requiredElapsed || 500),
    _getCustomFilter1: function() {
        var m03_work_product_name = this.model.get('m03_work_product_taskd_task_design_1_name');
        var relativeVal = this.model.get("relative");
        if (relativeVal == '1st Tier') {
            relative = 'NA';
        } else if (relativeVal == '2nd Tier') {
            relative = '1st Tier';
        }
        return [{
            'm03_work_product_taskd_task_design_1_name': {
                '$in': [m03_work_product_name],
            },
            'relative': {
                '$in': [relative],
            }
        }];
    },
    _getCustomFilterbatchID: function() {
        var BID_Batch_ID_name = this.model.get('bid_batch_id_taskd_task_design_1_name');
        var relativeVal = this.model.get("relative");
        if (relativeVal == '1st Tier') {
            relative = 'NA';
        } else if (relativeVal == '2nd Tier') {
            relative = '1st Tier';
        }
        return [{
            'bid_batch_id_taskd_task_design_1_name': {
                '$in': [BID_Batch_ID_name],
            },
            'relative': {
                '$in': [relative],
            }
        }];
    },
    _getCustomFilter3: function() {
        var m03_work_product_code_name = this.model.get('m03_work_product_code_taskd_task_design_1_name');
        var relativeVal = this.model.get("relative");
        if (relativeVal == '1st Tier') {
            relative = 'NA';
        } else if (relativeVal == '2nd Tier') {
            relative = '1st Tier';
        }
        return [{
            'm03_work_product_code_taskd_task_design_1_name': {
                '$in': [m03_work_product_code_name],
            },
            'relative': {
                '$in': [relative],
            }
        }];
    },
    _getCustomFilter2: function() {
        return [{
            'name': {
                '$in': ["Day", "Hour", "Minute"],
            }
        }];
    },
    /**
     * @override
     */
    buildFilterDefinition: function(searchTerm) {
        var parentFilter = this._super('buildFilterDefinition', [searchTerm]);
        if (this.getSearchModule() == 'TaskD_Task_Design' && this.model.get("m03_work_product_taskd_task_design_1_name") != "" && this.model.get("m03_work_product_taskd_task_design_1_name") != undefined) {
            return parentFilter.concat(this._getCustomFilter1());
        } else if (this.getSearchModule() == 'TaskD_Task_Design' && this.model.get("bid_batch_id_taskd_task_design_1_name") != "" && this.model.get("bid_batch_id_taskd_task_design_1_name") != undefined) {
            return parentFilter.concat(this._getCustomFilterbatchID());
        } else if(this.getSearchModule() == 'TaskD_Task_Design' && this.model.get("m03_work_product_code_taskd_task_design_1_name") != "" && this.model.get("m03_work_product_code_taskd_task_design_1_name") != undefined) {
            return parentFilter.concat(this._getCustomFilter3());
        } else if (this.getSearchModule() == 'U_Units') {
            return parentFilter.concat(this._getCustomFilter2());
        } else {
            return parentFilter;
        }
    },
    /**EOC Disable search filter code */
})