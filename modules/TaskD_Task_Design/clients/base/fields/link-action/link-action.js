({

    extendsFrom: 'LinkActionField',
    initialize: function (options) {
        this._super('initialize', [options]);
    },
    openSelectDrawer: function () {
        if (this.isDisabled()) {
            return;
        }
        if (_.isEqual(this.context.get('parentModel').get('_module'), "PS_Procedure_Schedule")) {
            setTimeout(function () {
                $("button.btn.btn-invisible.btn-dark").css("display", "none");
                $(".filter-definition-container").css('pointer-events', 'none');
                $(".choice-filter-clickable").css('pointer-events', 'none');
                $(".filter-definition-container [name='filter_row_name']").prop('disabled', true);
                $(".filter-definition-container [name='filter_row_operator']").prop('disabled', true);
                $(".filter-definition-container [name='m03_work_product_taskd_task_design_1_name']").prop('disabled', true);
                $(".filter-definition-container [name='category']").prop('disabled', true);
            }, 500);
        }
        console.log(this.context.get('parentModel').get("id"));
        var parentModel = this.context.get('parentModel');
        linkModule = this.context.get('module');
        link = this.context.get('link');
      //  var m03_work_product_name = this.context.get('parentModel').get("m03_work_product_ps_procedure_schedule_1").id;
        var m03_work_product_name = this.context.get('parentModel').get("m03_work_product_ps_procedure_schedule_1m03_work_product_ida");
        var Procedure = 'Procedure';
        var type_2 = 'Actual';
        self = this;
        console.log('m03_work_product_name ', m03_work_product_name);
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterTaskDesignSameWPOnLink',
                'initial_filter_label': 'Filter By Work Product',
                'filter_populate': {
                    'm03_work_product_taskd_task_design_1_name': [m03_work_product_name],
                    'category': [Procedure],
                    'type_2': [type_2],
                },
                'filter_relate': {
                    'm03_work_product_taskd_task_design_1_name': [m03_work_product_name],
                    'category': [Procedure],
                    'type_2': [type_2],
                }
            })
            .format();
        var context = {
            module: linkModule,
            recParentModel: parentModel,
            recLink: link,
            recContext: this.context,
            recView: this.view
        }
        //this code will execute for all contact subpanel,to apply the filter only for opportunities we will give a condition   
        if (_.isEqual(this.context.get('parentModel').get('_module'), "PS_Procedure_Schedule")) {
            context = _.extend(context, {
                filterOptions: filterOptions
            });
        }
        app.drawer.open({
            layout: 'selection-list',
            context: context,
        }, function (model) {
            if (!model) {
                return;
            }
            var relatedModel = app.data.createRelatedBean(parentModel, model.id, link),
                options = {
                    showAlerts: true,
                    relate: true,
                    success: function (model) {
                        self.context.get('collection').resetPagination();
                        self.context.resetLoadFlag();
                        self.context.set('skipFetch', false);
                        var collectionOptions = self.context.get('collectionOptions') || {};
                        if (collectionOptions.limit) self.context.set('limit', collectionOptions.limit);
                        self.context.loadData({
                            success: function () {
                                self.view.layout.trigger('filter:record:linked');
                            },
                            error: function (error) {
                                app.alert.show('server-error', {
                                    level: 'error',
                                    messages: 'ERR_GENERIC_SERVER_ERROR'
                                });
                            }
                        });
                    },
                    error: function (error) {
                        app.alert.show('server-error', {
                            level: 'error',
                            messages: 'ERR_GENERIC_SERVER_ERROR'
                        });
                    }
                };
            relatedModel.save(null, options);
        });
    },

})