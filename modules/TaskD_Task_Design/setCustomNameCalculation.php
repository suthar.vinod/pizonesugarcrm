<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class setCustomNameCalculation
{
  function customnamecalculation($bean, $event, $arguments)
  {
    //$GLOBALS['log']->fatal(' In file setcustomname cal task design ');
    global $app_list_strings;

    $type = $bean->type_2;
    $parent_tsd_id = $bean->parent_td_id_c;
    //$Bean = BeanFactory::getBean('TaskD_Task_Design', $bean->id);
    $bean->load_relationship("m03_work_product_taskd_task_design_1");
    $bean->load_relationship("gd_group_design_taskd_task_design_1");
    $bean->load_relationship("taskd_task_design_taskd_task_design_1");
    $bean->load_relationship("anml_animals_taskd_task_design_1");
    $bean->load_relationship("m03_work_product_code_taskd_task_design_1");
    $bean->load_relationship("bid_batch_id_taskd_task_design_1");

    $workProductId  = $bean->m03_work_product_taskd_task_design_1->get();
    $groupDesignIds = $bean->gd_group_design_taskd_task_design_1->get();
    $TestSystemIds  = $bean->anml_animals_taskd_task_design_1->get();
    $WPCID          = $bean->m03_work_product_code_taskd_task_design_1->get();
    $Batch_id          = $bean->bid_batch_id_taskd_task_design_1->get();

    $wpBean       =   BeanFactory::getBean('M03_Work_Product', $workProductId[0]);
    $wpName       =   $wpBean->name;
    $gdBean       =   BeanFactory::getBean('GD_Group_Design', $groupDesignIds[0]);
    $gdName       =   $gdBean->name;
    $TSBean       =   BeanFactory::getBean('ANML_Animals', $TestSystemIds[0]);
    $tsName       =   $TSBean->name;
    $WPCBean      =   BeanFactory::getBean('M03_Work_Product_Code', $WPCID[0]);
    $WPCName      =   $WPCBean->name;
    $BatchBean    =   BeanFactory::getBean('BID_Batch_ID', $Batch_id[0]);
    $BatchName    =   $BatchBean->name;
    //$GLOBALS['log']->fatal('BatchName TD'.$BatchName);
    $task_type    =   $bean->task_type;
    $gdNameNumber = "";

    if ($gdName != '') {
      $gdNameNumber = substr($gdName, strrpos($gdName, ' ') + 1);
    }

    $custom_task = $bean->custom_task;
    $standard_task = $bean->standard_task;
    $phase = $bean->phase_c;
    $usda_id_name = $bean->usda_id_c;
    if ($task_type == 'Custom') {
      $bean->standard_task = '';
      $standard_task_val = $custom_task;
    } else if ($task_type == 'Standard') {
      $standard_dom = $app_list_strings['standard_task_list'];
      $standard_task = $standard_dom[$bean->standard_task];
      $bean->custom_task = '';
      $standard_task_val = $standard_task;
    }
   if(!empty($BatchName))
    {      
      $WPBID = $BatchName;
      //$GLOBALS['log']->fatal('in line 70 '.$BatchName);
      //$GLOBALS['log']->fatal('in line 71 '.$wpName);
    } else {
      $WPBID = $wpName;
      //$GLOBALS['log']->fatal('in line 74 '.$wpName);
      //$GLOBALS['log']->fatal('in line 75 '.$BatchName);     
    }
    if($parent_tsd_id == '' && $phase == '')
    {
      if ($type == 'Plan') {
        $customname = $WPBID . ' ' . $gdNameNumber . ' ' . $standard_task_val;
        // $customname = $wpName . ' ' . $gdNameNumber . ' ' . $standard_task_val;
        //$GLOBALS['log']->fatal('in type plan line 61');
      } else if ($type == 'Actual') {
        $customname = $WPBID . ' ' . $tsName . ' ' . $usda_id_name . ' ' . $standard_task_val;
        //$GLOBALS['log']->fatal('in type actual line 66');
      } else if ($type == 'Plan SP') {
        $customname = $WPCName . ' ' . $standard_task_val;
        //$GLOBALS['log']->fatal('in type plan sp line 71');
      } else if ($type == 'Actual SP') {
        $customname = $WPBID . ' ' . $standard_task_val;
        //$GLOBALS['log']->fatal('in type null else  line 76');
      }    
    } else if ($parent_tsd_id == '') {
      if ($type == 'Plan') {
        $customname = $WPBID . ' ' . $phase . ' ' . $gdNameNumber . ' ' . $standard_task_val;
        // $customname = $wpName . ' ' . $phase . ' ' . $gdNameNumber . ' ' . $standard_task_val;
        //$GLOBALS['log']->fatal('in type plan line 61');
      } else if ($type == 'Actual') {
        $customname = $WPBID . ' ' . $phase . ' ' . $tsName . ' ' . $usda_id_name . ' ' . $standard_task_val;
        //$GLOBALS['log']->fatal('in type actual line 66');
      } else if ($type == 'Plan SP') {
        $customname = $WPCName . ' ' . $phase . ' ' . $standard_task_val;
        //$GLOBALS['log']->fatal('in type plan sp line 71');
      } else if ($type == 'Actual SP') {
        $customname = $WPBID . ' ' . $phase . ' ' . $standard_task_val;
        //$GLOBALS['log']->fatal('in type null else  line 76');
      }
    } else {
      if ($type == 'Actual' && $usda_id_name != '' && $phase != '') {
        $customname = $WPBID . ' ' . $phase . ' ' . $tsName . ' ' . $usda_id_name . ' ' . $standard_task_val;
        //$GLOBALS['log']->fatal('in type actual line 66');
      } else if ($type == 'Actual' && $usda_id_name != '' ){
        $customname = $WPBID . ' ' . $tsName . ' ' . $usda_id_name . ' ' . $standard_task_val;
      } else if ($type == 'Actual SP' && $phase == '') {
        $customname = $WPBID . ' ' . $standard_task_val;
      } else if ($type == 'Actual SP') {
        $customname = $WPBID . ' ' . $phase . ' ' . $standard_task_val;
      } else {
        $customname = $bean->name;
      }
      //$GLOBALS['log']->fatal(' in else con '.$customname);
    }

    //$bean->custom_task = $custom_task;
    //$bean->standard_task = $standard_task;
    //$GLOBALS['log']->fatal(' customname '.$customname);
    $bean->name = $customname;
  }
}
