<?php 
global $db;

 
$site_url = $GLOBALS['sugar_config']['site_url'];
 
    $query = "SELECT TD.id AS TDID,TD.name AS tdName, WP.id AS WPID,WP.name AS WPName,TD_TD.taskd_task_design_taskd_task_design_1taskd_task_design_ida AS TDTDID 
	FROM taskd_task_design AS TD 
	LEFT JOIN m03_work_product_taskd_task_design_1_c AS WP_TD ON WP_TD.m03_work_product_taskd_task_design_1taskd_task_design_idb=TD.id 
	LEFT JOIN  m03_work_product AS WP ON WP_TD.m03_work_product_taskd_task_design_1m03_work_product_ida = WP.id
	LEFT JOIN taskd_task_design_taskd_task_design_1_c AS TD_TD ON TD_TD.taskd_task_design_taskd_task_design_1taskd_task_design_idb=TD.id
	WHERE TD.deleted=0 AND TD_TD.taskd_task_design_taskd_task_design_1taskd_task_design_ida IS NOT NULL AND TD_TD.deleted=0 AND WP_TD.deleted=0";
	$queryResult = $db->query($query);
		 
	$srno = 1;
	
	$emailBody = "<table width='1550px' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th bgcolor='#b3d1ff' align='left'> TD ID </th>
			<th bgcolor='#b3d1ff' align='left'>TD Name</th>
			<th bgcolor='#b3d1ff' align='left'>WP ID</th>
			<th bgcolor='#b3d1ff' align='left'>WP Name</th>
			<th bgcolor='#b3d1ff' align='left'>Child TD</th>
			<th bgcolor='#b3d1ff' align='left'>Child TD Name</th>
			<th bgcolor='#b3d1ff' align='left'>WP for Child TD</th></tr>";
			
	$wpArray = array();		
    while ($fetchResult = $db->fetchByAssoc($queryResult)) {
       $tdID		= $fetchResult['TDID'];
	   $tdName	 	= $fetchResult['tdName']; 
	   $WPID	 	= $fetchResult['WPID'];
	   $WPName	 	= $fetchResult['WPName']; 
	   $TDTDID	 	= $fetchResult['TDTDID'];


	   if($TDTDID!=""){
		   $query2 = "SELECT TD.name AS tdName, WP.id AS WPID,WP.name AS WPName
			   FROM taskd_task_design AS TD 
	   			LEFT JOIN m03_work_product_taskd_task_design_1_c AS WP_TD ON WP_TD.m03_work_product_taskd_task_design_1taskd_task_design_idb=TD.id 
	   			LEFT JOIN  m03_work_product AS WP ON WP_TD.m03_work_product_taskd_task_design_1m03_work_product_ida = WP.id 
	   			WHERE TD.deleted=0 AND  TD.id='".$TDTDID."' LIMIT 1";
	 	  	$queryResult2 = $db->query($query2);

			if ($queryResult2->num_rows > 0){
				$fetchResult2 = $db->fetchByAssoc($queryResult2);
				$childTD = $fetchResult2['tdName'];
				$childTDWPID = $fetchResult2['WPID'];
				$childTD_WP = $fetchResult2['WPName'];
			}
	   }

	   if($childTDWPID!=$WPID && !in_array($WPID, $wpArray) ){

		$wpArray[$srno] = $WPID; 

		if($srno%2==0)
	 	  $stylr = "style='background-color: #e6e6e6;'";
	    else
			$stylr = "style='background-color: #f3f3f3;'";
				
			 


		$emailBody .= "<tr><td ".$stylr.">".$srno++."</td>
						<td ".$stylr.">".$tdID."</td> 
						<td ".$stylr."><a href='".$site_url."/#TaskD_Task_Design/".$tdID."' target='_blank'>".$tdName."</a></td>
						<td ".$stylr.">".$WPID."</td>
						<td ".$stylr."><a href='".$site_url."/#M03_Work_Product/".$WPID."' target='_blank'>".$WPName."</a></td>
						<td ".$stylr.">".$TDTDID."</td>
						<td ".$stylr."><a href='".$site_url."/#TaskD_Task_Design/".$TDTDID."' target='_blank'>".$childTD."</a></td>				
						<td ".$stylr."><a href='".$site_url."/#M03_Work_Product/".$childTDWPID."' target='_blank'>".$childTD_WP."</a></td>
						</tr>";

	   }
	 
	 
	   
			
	 }
	 
	 echo "<br><br>".$emailBody .="</table>";
?>