<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class deleteCalculationAuditLog
{
  function deleteAudit($bean, $event, $arguments)
  {
    global $db;
    $TsdID = $bean->id;

    $queryAuditLog = "SELECT id,count(*) as NUM FROM `taskd_task_design_audit` WHERE `field_name`='planned_start_datetime_1st' AND `parent_id`='" . $TsdID . "' group by date(before_value_string),date(after_value_string),date(date_created)";
    $auditLogResult = $db->query($queryAuditLog);
    if ($auditLogResult->num_rows > 0) {
      while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
        if ($fetchAuditLog['NUM'] > 1) {
          $recordID = $fetchAuditLog['id'];
          $sql_DeleteAudit = "DELETE from `taskd_task_design_audit` WHERE `field_name`='planned_start_datetime_1st' AND `parent_id`='" . $TsdID . "' AND id = '" . $recordID . "'";
          $db->query($sql_DeleteAudit);
        }
      }
    }
    $queryAuditLog2 = "SELECT id,count(*) as NUM FROM `taskd_task_design_audit` WHERE `field_name`='planned_end_datetime_1st' AND `parent_id`='" . $TsdID . "' group by date(before_value_string),date(after_value_string),date(date_created)";
    $auditLogResult2 = $db->query($queryAuditLog2);
    if ($auditLogResult2->num_rows > 0) {
      while ($fetchAuditLog2 = $db->fetchByAssoc($auditLogResult2)) {
        if ($fetchAuditLog2['NUM'] > 1) {
          $recordID2 = $fetchAuditLog2['id'];
          $sql_DeleteAudit2 = "DELETE from `taskd_task_design_audit` WHERE `field_name`='planned_end_datetime_1st' AND `parent_id`='" . $TsdID . "' AND id = '" . $recordID2 . "'";
          $db->query($sql_DeleteAudit2);
        }
      }
    }

    $sql_DeleteAudit5 = "DELETE from `taskd_task_design_audit` WHERE `field_name`='planned_start_datetime_1st' AND `before_value_string` = `after_value_string` AND `parent_id`='" . $TsdID . "'";
    $db->query($sql_DeleteAudit5);

    $sql_DeleteAudit6 = "DELETE from `taskd_task_design_audit` WHERE `field_name`='planned_end_datetime_1st' AND `before_value_string` = `after_value_string` AND `parent_id`='" . $TsdID . "'";
    $db->query($sql_DeleteAudit6);

    $sql_del_null1 = "DELETE FROM `taskd_task_design_audit` WHERE `field_name`='planned_end_datetime_1st' AND `parent_id`='" . $TsdID . "' AND  (`before_value_string` is NULL OR `before_value_string`='') AND  (`after_value_string` is NULL OR `after_value_string`='')";
    $db->query($sql_del_null1);


    // 2nd Tier
    $queryAuditLog2nd = "SELECT id,count(*) as NUM FROM `taskd_task_design_audit` WHERE `field_name`='planned_start_datetime_2nd' AND `parent_id`='" . $TsdID . "' group by date(before_value_string),date(after_value_string),date(date_created)";
    $auditLogResult2nd = $db->query($queryAuditLog2nd);
    if ($auditLogResult2nd->num_rows > 0) {
      while ($fetchAuditLog2nd = $db->fetchByAssoc($auditLogResult2nd)) {
        if ($fetchAuditLog2nd['NUM'] > 1) {
          $recordID2nd = $fetchAuditLog2nd['id'];
          $sql_DeleteAudit2nd = "DELETE from `taskd_task_design_audit` WHERE `field_name`='planned_start_datetime_2nd' AND `parent_id`='" . $TsdID . "' AND id = '" . $recordID2nd . "'";
          $db->query($sql_DeleteAudit2nd);
        }
      }
    }

    $queryAuditLog2nde = "SELECT id,count(*) as NUM FROM `taskd_task_design_audit` WHERE `field_name`='planned_end_datetime_2nd' AND `parent_id`='" . $TsdID . "' group by date(before_value_string),date(after_value_string),date(date_created)";
    $auditLogResult2nde = $db->query($queryAuditLog2nde);
    if ($auditLogResult2nde->num_rows > 0) {
      while ($fetchAuditLog2nde = $db->fetchByAssoc($auditLogResult2nde)) {
        if ($fetchAuditLog2nde['NUM'] > 1) {
          $recordID2nde = $fetchAuditLog2nde['id'];
          $sql_DeleteAudit2nde = "DELETE from `taskd_task_design_audit` WHERE `field_name`='planned_end_datetime_2nd' AND `parent_id`='" . $TsdID . "' AND id = '" . $recordID2nde . "'";
          $db->query($sql_DeleteAudit2nde);
        }
      }
    }


    // 2nd Tier
    $sql_DeleteAudit3 = "DELETE from `taskd_task_design_audit` WHERE `field_name`='planned_start_datetime_2nd' AND `before_value_string` = `after_value_string` AND `parent_id`='" . $TsdID . "'";
    $db->query($sql_DeleteAudit3);

    $sql_DeleteAudit4 = "DELETE from `taskd_task_design_audit` WHERE `field_name`='planned_end_datetime_2nd' AND `before_value_string` = `after_value_string` AND `parent_id`='" . $TsdID . "'";
    $db->query($sql_DeleteAudit4);

    $sql_del_null2 = "DELETE FROM `taskd_task_design_audit` WHERE `field_name`='planned_start_datetime_2nd' AND `parent_id`='" . $TsdID . "' AND  (`before_value_string` is NULL OR `before_value_string`='') AND  (`after_value_string` is NULL OR `after_value_string`='')";
    $db->query($sql_del_null2);

    $sql_del_null4 = "DELETE FROM `taskd_task_design_audit` WHERE `field_name`='planned_end_datetime_2nd' AND `parent_id`='" . $TsdID . "' AND  (`before_value_string` is NULL OR `before_value_string`='') AND  (`after_value_string` is NULL OR `after_value_string`='')";
    $db->query($sql_del_null4);

    $Tsdtype = $bean->type_2;

    if ($Tsdtype == "Plan SP") {

      $sql_DeleteAuditTsdtype = "DELETE from `taskd_task_design_audit` WHERE `field_name`='planned_start_datetime_1st' AND `parent_id`='" . $TsdID . "'";
      $db->query($sql_DeleteAuditTsdtype);

      $sql_DeleteAuditTsdtype2 = "DELETE from `taskd_task_design_audit` WHERE `field_name`='planned_end_datetime_1st'  AND `parent_id`='" . $TsdID . "'";
      $db->query($sql_DeleteAuditTsdtype2);

      $sql_DeleteAuditTsdtype3 = "DELETE from `taskd_task_design_audit` WHERE `field_name`='planned_start_datetime_2nd' AND `parent_id`='" . $TsdID . "'";
      $db->query($sql_DeleteAuditTsdtype3);

      $sql_DeleteAuditTsdtype4 = "DELETE from `taskd_task_design_audit` WHERE `field_name`='planned_end_datetime_2nd'   AND `parent_id`='" . $TsdID . "'";
      $db->query($sql_DeleteAuditTsdtype4);

      $sql_DeleteAuditTsdtype5 = "DELETE from `taskd_task_design_audit` WHERE `field_name`='single_date_2_c'   AND `parent_id`='" . $TsdID . "'";
      $db->query($sql_DeleteAuditTsdtype5);
      
    }
  }
}
