<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Layoutdefs/rr_regulatory_response_m03_work_product_1_RR_Regulatory_Response.php

 // created: 2019-07-09 11:53:38
$layout_defs["RR_Regulatory_Response"]["subpanel_setup"]['rr_regulatory_response_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'rr_regulatory_response_m03_work_product_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Layoutdefs/rr_regulatory_response_m01_sales_1_RR_Regulatory_Response.php

 // created: 2019-07-09 11:54:56
$layout_defs["RR_Regulatory_Response"]["subpanel_setup"]['rr_regulatory_response_m01_sales_1'] = array (
  'order' => 100,
  'module' => 'M01_Sales',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_M01_SALES_TITLE',
  'get_subpanel_data' => 'rr_regulatory_response_m01_sales_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Layoutdefs/rrd_regulatory_response_doc_rr_regulatory_response_1_RR_Regulatory_Response.php

 // created: 2019-07-09 12:11:34
$layout_defs["RR_Regulatory_Response"]["subpanel_setup"]['rrd_regulatory_response_doc_rr_regulatory_response_1'] = array (
  'order' => 100,
  'module' => 'RRD_Regulatory_Response_Doc',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_RRD_REGULATORY_RESPONSE_DOC_RR_REGULATORY_RESPONSE_1_FROM_RRD_REGULATORY_RESPONSE_DOC_TITLE',
  'get_subpanel_data' => 'rrd_regulatory_response_doc_rr_regulatory_response_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Layoutdefs/_overrideRR_Regulatory_Response_subpanel_rr_regulatory_response_m03_work_product_1.php

//auto-generated file DO NOT EDIT
$layout_defs['RR_Regulatory_Response']['subpanel_setup']['rr_regulatory_response_m03_work_product_1']['override_subpanel_name'] = 'RR_Regulatory_Response_subpanel_rr_regulatory_response_m03_work_product_1';

?>
