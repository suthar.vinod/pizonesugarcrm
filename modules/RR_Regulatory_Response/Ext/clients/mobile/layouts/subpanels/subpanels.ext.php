<?php
// WARNING: The contents of this file are auto-generated.


// created: 2019-07-09 11:54:56
$viewdefs['RR_Regulatory_Response']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_M01_SALES_TITLE',
  'context' => 
  array (
    'link' => 'rr_regulatory_response_m01_sales_1',
  ),
);

// created: 2019-07-09 11:53:38
$viewdefs['RR_Regulatory_Response']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'rr_regulatory_response_m03_work_product_1',
  ),
);

// created: 2019-07-09 12:11:34
$viewdefs['RR_Regulatory_Response']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_RRD_REGULATORY_RESPONSE_DOC_RR_REGULATORY_RESPONSE_1_FROM_RRD_REGULATORY_RESPONSE_DOC_TITLE',
  'context' => 
  array (
    'link' => 'rrd_regulatory_response_doc_rr_regulatory_response_1',
  ),
);