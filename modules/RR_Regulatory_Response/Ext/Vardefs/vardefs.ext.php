<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/sugarfield_account_id_c.php

 // created: 2019-07-09 11:48:15

 
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/sugarfield_company_c.php

 // created: 2019-07-09 11:48:15
$dictionary['RR_Regulatory_Response']['fields']['company_c']['labelValue']='Company';
$dictionary['RR_Regulatory_Response']['fields']['company_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/sugarfield_contact_id_c.php

 // created: 2019-07-09 11:49:01

 
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/sugarfield_contact_c.php

 // created: 2019-07-09 11:49:01
$dictionary['RR_Regulatory_Response']['fields']['contact_c']['labelValue']='Contact';
$dictionary['RR_Regulatory_Response']['fields']['contact_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/sugarfield_regulatory_region_c.php

 // created: 2019-07-09 11:49:59
$dictionary['RR_Regulatory_Response']['fields']['regulatory_region_c']['labelValue']='Regulatory Region';
$dictionary['RR_Regulatory_Response']['fields']['regulatory_region_c']['dependency']='';
$dictionary['RR_Regulatory_Response']['fields']['regulatory_region_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2019-07-09 11:50:55
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['audited']=true;
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['comments']='Date record created';
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['unified_search']=false;
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['calculated']=false;
$dictionary['RR_Regulatory_Response']['fields']['date_entered']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/sugarfield_date_completed_c.php

 // created: 2019-07-09 11:51:20
$dictionary['RR_Regulatory_Response']['fields']['date_completed_c']['labelValue']='Date Completed';
$dictionary['RR_Regulatory_Response']['fields']['date_completed_c']['enforced']='';
$dictionary['RR_Regulatory_Response']['fields']['date_completed_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/sugarfield_response_status_c.php

 // created: 2019-07-09 11:52:50
$dictionary['RR_Regulatory_Response']['fields']['response_status_c']['labelValue']='Response Status';
$dictionary['RR_Regulatory_Response']['fields']['response_status_c']['dependency']='';
$dictionary['RR_Regulatory_Response']['fields']['response_status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/rr_regulatory_response_m03_work_product_1_RR_Regulatory_Response.php

// created: 2019-07-09 11:53:38
$dictionary["RR_Regulatory_Response"]["fields"]["rr_regulatory_response_m03_work_product_1"] = array (
  'name' => 'rr_regulatory_response_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'rr_regulatory_response_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'rr_regulatory_response_m03_work_product_1m03_work_product_idb',
);

?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/rr_regulatory_response_m01_sales_1_RR_Regulatory_Response.php

// created: 2019-07-09 11:54:56
$dictionary["RR_Regulatory_Response"]["fields"]["rr_regulatory_response_m01_sales_1"] = array (
  'name' => 'rr_regulatory_response_m01_sales_1',
  'type' => 'link',
  'relationship' => 'rr_regulatory_response_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_M01_SALES_TITLE',
  'id_name' => 'rr_regulatory_response_m01_sales_1m01_sales_idb',
);

?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/sugarfield_description.php

 // created: 2019-07-09 11:58:19
$dictionary['RR_Regulatory_Response']['fields']['description']['required']=true;
$dictionary['RR_Regulatory_Response']['fields']['description']['audited']=true;
$dictionary['RR_Regulatory_Response']['fields']['description']['massupdate']=false;
$dictionary['RR_Regulatory_Response']['fields']['description']['comments']='Full text of the note';
$dictionary['RR_Regulatory_Response']['fields']['description']['duplicate_merge']='enabled';
$dictionary['RR_Regulatory_Response']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['RR_Regulatory_Response']['fields']['description']['merge_filter']='disabled';
$dictionary['RR_Regulatory_Response']['fields']['description']['unified_search']=false;
$dictionary['RR_Regulatory_Response']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['RR_Regulatory_Response']['fields']['description']['calculated']=false;
$dictionary['RR_Regulatory_Response']['fields']['description']['rows']='6';
$dictionary['RR_Regulatory_Response']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/rrd_regulatory_response_doc_rr_regulatory_response_1_RR_Regulatory_Response.php

// created: 2019-07-09 12:11:34
$dictionary["RR_Regulatory_Response"]["fields"]["rrd_regulatory_response_doc_rr_regulatory_response_1"] = array (
  'name' => 'rrd_regulatory_response_doc_rr_regulatory_response_1',
  'type' => 'link',
  'relationship' => 'rrd_regulatory_response_doc_rr_regulatory_response_1',
  'source' => 'non-db',
  'module' => 'RRD_Regulatory_Response_Doc',
  'bean_name' => 'RRD_Regulatory_Response_Doc',
  'vname' => 'LBL_RRD_REGULATORY_RESPONSE_DOC_RR_REGULATORY_RESPONSE_1_FROM_RRD_REGULATORY_RESPONSE_DOC_TITLE',
  'id_name' => 'rrd_regula67cense_doc_ida',
);

?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/sugarfield_functional_area_c.php

 // created: 2020-05-21 17:10:47
$dictionary['RR_Regulatory_Response']['fields']['functional_area_c']['labelValue']='Functional Area';
$dictionary['RR_Regulatory_Response']['fields']['functional_area_c']['dependency']='';
$dictionary['RR_Regulatory_Response']['fields']['functional_area_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Vardefs/sugarfield_name.php

 // created: 2021-06-22 09:44:58
$dictionary['RR_Regulatory_Response']['fields']['name']['len']='255';
$dictionary['RR_Regulatory_Response']['fields']['name']['required']=false;
$dictionary['RR_Regulatory_Response']['fields']['name']['audited']=true;
$dictionary['RR_Regulatory_Response']['fields']['name']['massupdate']=false;
$dictionary['RR_Regulatory_Response']['fields']['name']['unified_search']=false;
$dictionary['RR_Regulatory_Response']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['RR_Regulatory_Response']['fields']['name']['calculated']=false;
$dictionary['RR_Regulatory_Response']['fields']['name']['hidemassupdate']=false;
$dictionary['RR_Regulatory_Response']['fields']['name']['readonly']=true;

 
?>
