<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Language/en_us.customrr_regulatory_response_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Language/en_us.customrr_regulatory_response_m03_work_product_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_RR_REGULATORY_RESPONSE_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_RR_REGULATORY_RESPONSE_TITLE'] = 'Work Products';


?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_COMPANY_ACCOUNT_ID'] = 'Company (related Company ID)';
$mod_strings['LBL_COMPANY'] = 'Company';
$mod_strings['LBL_CONTACT_CONTACT_ID'] = 'Contact (related Contact ID)';
$mod_strings['LBL_CONTACT'] = 'Contact';
$mod_strings['LBL_REGULATORY_REGION'] = 'Regulatory Region';
$mod_strings['LBL_DATE_COMPLETED'] = 'Date Completed';
$mod_strings['LBL_RESPONSE_STATUS'] = 'Response Status';
$mod_strings['LBL_RECORD_BODY'] = 'Regulatory Response';
$mod_strings['LBL_DESCRIPTION'] = 'Description of Regulatory Response';
$mod_strings['LBL_FUNCTIONAL_AREA'] = 'Functional Area';
$mod_strings['LBL_RR_REGULATORY_RESPONSE_FOCUS_DRAWER_DASHBOARD'] = 'Regulatory Responses Focus Drawer';
$mod_strings['LBL_RR_REGULATORY_RESPONSE_RECORD_DASHBOARD'] = 'Regulatory Responses Record Dashboard';

?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Language/en_us.customrr_regulatory_response_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_RR_REGULATORY_RESPONSE_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/RR_Regulatory_Response/Ext/Language/en_us.customrrd_regulatory_response_doc_rr_regulatory_response_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_RRD_REGULATORY_RESPONSE_DOC_RR_REGULATORY_RESPONSE_1_FROM_RRD_REGULATORY_RESPONSE_DOC_TITLE'] = 'Regulatory Response Documents';
$mod_strings['LBL_RRD_REGULATORY_RESPONSE_DOC_RR_REGULATORY_RESPONSE_1_FROM_RR_REGULATORY_RESPONSE_TITLE'] = 'Regulatory Response Documents';

?>
