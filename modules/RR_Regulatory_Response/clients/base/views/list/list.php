<?php
$module_name = 'RR_Regulatory_Response';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'functional_area_c',
                'label' => 'LBL_FUNCTIONAL_AREA',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'company_c',
                'label' => 'LBL_COMPANY',
                'enabled' => true,
                'id' => 'ACCOUNT_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'contact_c',
                'label' => 'LBL_CONTACT',
                'enabled' => true,
                'id' => 'CONTACT_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'regulatory_region_c',
                'label' => 'LBL_REGULATORY_REGION',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'response_status_c',
                'label' => 'LBL_RESPONSE_STATUS',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'date_completed_c',
                'label' => 'LBL_DATE_COMPLETED',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              9 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
