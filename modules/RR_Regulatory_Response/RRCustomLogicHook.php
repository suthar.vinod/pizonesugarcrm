<?php

class RRCustomLogicHook {

    function generateAutomatedNameForRR($bean, $event, $arguments) {
        global $db;
        $isIdExist = $bean->fetched_row['id'];

        if (!$isIdExist) {

            $year = date("Y");
            $year = substr($year, -2);
            $name = "RR" . $year . "-";

            $queryForGetLatestName = "SELECT name FROM rr_regulatory_response WHERE name like '$name%' AND deleted = 0 "
                    . "ORDER BY date_entered DESC, name DESC LIMIT 0,1";


            $_seqNo = $db->query($queryForGetLatestName);
            $resultForSeqNo = $db->fetchByAssoc($_seqNo);
            $nameRR = $resultForSeqNo['name'];

            if (!empty($nameRR)) {
                $nameDivisionArray = explode('-', $nameRR);
                $number = $nameDivisionArray[1];
                $number = $number + 1;
            } else {
                $number = 0001;
            }
            $number = sprintf('%04d', $number);
            $newName = $name . $number;

            $queryForUpdateName = "UPDATE rr_regulatory_response SET name = '" . $newName . "' "
                    . "WHERE id = '" . $bean->id . "'";
            $result = $db->query($queryForUpdateName);
            if (!$result) {
                $GLOBALS['log']->debug("Name Updation Failed!");
            }
        } else {
            $queryForUpdateName = "UPDATE rr_regulatory_response SET name = '" . $bean->name . "' "
                    . "WHERE id = '" . $bean->id . "'";
            $result = $db->query($queryForUpdateName);
            if (!$result) {
                $GLOBALS['log']->debug("Name Updation Failed!");
            }
        }
    }

}
