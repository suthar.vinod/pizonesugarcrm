<?php
$CopyFromid = $_POST['CopyFromid'];
$CopyToid = $_POST['CopyToid'];
global $db;
$currentGDBean = BeanFactory::retrieveBean('GD_Group_Design', $CopyFromid);
$copytoGDBean = BeanFactory::retrieveBean('GD_Group_Design', $CopyToid);
$newworkProductId = $copytoGDBean->m03_work_product_gd_group_design_1m03_work_product_ida;
$newworkProductname = $copytoGDBean->m03_work_product_gd_group_design_1_name;

$currentGDBean->load_relationship('gd_group_design_taskd_task_design_1');
$relatedTD = $currentGDBean->gd_group_design_taskd_task_design_1->get();

$copyingTaskDesignIds = array();

foreach ($relatedTD as $TDId) {
   $clonedBean = BeanFactory::getBean('TaskD_Task_Design', $TDId);
   $clonedBean->load_relationship("taskd_task_design_taskd_task_design_1");
   $clonedBean->load_relationship("anml_animals_taskd_task_design_1");
   $workProductId = $newworkProductId;
   $groupDesignIds = $CopyToid;
   /* Custom Query to get Task design Ids */
   $sqlGetTaskDesignIds = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_ida as TDId
                                    FROM taskd_task_design_taskd_task_design_1_c 
                                    where taskd_task_design_taskd_task_design_1taskd_task_design_idb='$TDId' and deleted=0";
   $resultsSqlGetTaskDesignIds = $db->query($sqlGetTaskDesignIds);
   $rowSqlGetTaskDesignIds = $db->fetchByAssoc($resultsSqlGetTaskDesignIds);
   $taskDesignIds = $rowSqlGetTaskDesignIds['TDId'];

   $testSystemIds = $clonedBean->anml_animals_taskd_task_design_1->get();

   $clonedBean->id          = create_guid();
   $clonedBean->new_with_id = true;
   $clonedBean->relative = $clonedBean->relative;
   $clonedBean->category = $clonedBean->category;
   $clonedBean->fetched_row = null;

   $copyingTaskDesignIds[] = $clonedBean->id;

   $p1arr[$TDId] = $taskDesignIds;

   $a1arr[$clonedBean->id] = $TDId;
   $b1arr[$TDId] = $clonedBean->id;
   $clonedBean->u_units_id_c = $clonedBean->u_units_id_c;
   $clonedBean->end_integer = $clonedBean->end_integer;
   $type = $clonedBean->type_2;

   $relative = $clonedBean->relative;
   if ($relative == "NA") {
      $clonedBean->planned_end_datetime_1st = null;
      $clonedBean->planned_start_datetime_1st = null;
      $clonedBean->planned_start_datetime_2nd = null;
      $clonedBean->planned_end_datetime_2nd = null;
   } else if ($relative == "1st Tier") {
      $clonedBean->actual_datetime = null;
      $clonedBean->scheduled_start_datetime = null;
      $clonedBean->scheduled_end_datetime = null;
      $clonedBean->planned_start_datetime_2nd = null;
      $clonedBean->planned_end_datetime_2nd = null;
   } else if ($relative == "2nd Tier") {
      $clonedBean->actual_datetime = null;
      $clonedBean->scheduled_start_datetime = null;
      $clonedBean->scheduled_end_datetime = null;
      $clonedBean->planned_start_datetime_1st = null;
      $clonedBean->planned_end_datetime_1st = null;
   }

   if ($type == "Plan") {
      $clonedBean->actual_datetime = null;
      $clonedBean->scheduled_start_datetime = null;
      $clonedBean->scheduled_end_datetime = null;
      $clonedBean->planned_start_datetime_1st = null;
      $clonedBean->planned_end_datetime_1st = null;
      $clonedBean->planned_start_datetime_2nd = null;
      $clonedBean->planned_end_datetime_2nd = null;
   }
   $clonedBean->m03_work_product_taskd_task_design_1->add($workProductId);
   $clonedBean->gd_group_design_taskd_task_design_1->add($CopyToid);
   $clonedBean->taskd_task_design_taskd_task_design_1_right->add($taskDesignIds);
   $clonedBean->anml_animals_taskd_task_design_1->add($testSystemIds);
   $clonedBean->save();

   if ($clonedBean->type_2 == "Plan") {
      $sql_DeleteAudit = "DELETE from `taskd_task_design_audit` 
                        WHERE `field_name` in ('actual_datetime','scheduled_start_datetime','scheduled_end_datetime','planned_start_datetime_1st','planned_end_datetime_1st','planned_start_datetime_2nd','planned_end_datetime_2nd')  
                        AND `parent_id`='" . $clonedBean->id . "'";
      $db->query($sql_DeleteAudit);
   }

   $auditsql = 'update taskd_task_design_audit 
                         set field_name= "u_units_id_c",
                         after_value_string="' . $clonedBean->u_units_id_c . '" 
                         Where parent_id = "' . $clonedBean->id . '"
                         AND field_name="integer_units"';
   $db->query($auditsql);

   $queryAuditLog = "SELECT id,count(*) as NUM FROM `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' 
            AND `parent_id`='" . $clonedBean->id . "' group by before_value_string,after_value_string,date_created";
   $auditLogResult = $db->query($queryAuditLog);
   //$GLOBALS['log']->fatal('sqlGetTaskDesignIds  ===  APA ' . $queryAuditLog);
   if ($auditLogResult->num_rows > 0) {
      while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
         if ($fetchAuditLog['NUM'] > 1) {
            $recordID = $fetchAuditLog['id'];
            $sql_DeleteAudit = "DELETE from `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' AND `parent_id`='" . $clonedBean->id . "' AND id = '" . $recordID . "'";
            $db->query($sql_DeleteAudit);
           // $GLOBALS['log']->fatal('sqlGetTaskDesignIds  ===  APA ' . $sql_DeleteAudit);
         }
      }
   }
   if ($type == "Plan") {
      /*#1353 : Custom name calculation 10 June 2021 */
      $wpBean = BeanFactory::getBean('M03_Work_Product', $workProductId[0]);
      $wpName =  $wpBean->name;
      $gdBean = BeanFactory::getBean('GD_Group_Design', $CopyToid);
      $gdName =  $gdBean->name;
      //$GLOBALS['log']->fatal('******gdName ' .$gdName.' '.$CopyToid);

      $task_type = $clonedBean->task_type;

      $gdNameNumber = "";
      if ($gdName != '') {
         $gdNameNumber = substr($gdName, strrpos($gdName, ' ') + 1);
      }

      $custom_task = $clonedBean->custom_task;
      $standard_task = $clonedBean->standard_task;

      if ($task_type == 'Custom') {
         $standard_task = '';
         $standard_task_val = $custom_task;
      } else if ($task_type == 'Standard') {
         $custom_task = '';
         $standard_task_val = $standard_task;
      }

      $clonedBean->name = $wpName . ' ' . $gdNameNumber . ' ' . $standard_task_val;

      $clonedBean->save();
      /*EOC #1353 : Custom name calculation 10 June 2021 */
   }
}
foreach ($p1arr as $key => $value) {
   $newBeanId = $b1arr[$key];
   $newBean = BeanFactory::getBean('TaskD_Task_Design', $newBeanId);
   $newBean->load_relationship("taskd_task_design_taskd_task_design_1_right");
   $toBeRelateID = array_search($value, $a1arr);
   if ($toBeRelateID != "") {
      $newBean->save();
      $newBean->taskd_task_design_taskd_task_design_1_right->add($toBeRelateID);
   }

   if ($newBean->type_2 == "Plan") {
      $sql_DeleteAudit = "DELETE from `taskd_task_design_audit` 
                        WHERE `field_name` in ('actual_datetime','scheduled_start_datetime','scheduled_end_datetime','planned_start_datetime_1st','planned_end_datetime_1st','planned_start_datetime_2nd','planned_end_datetime_2nd')  
                        AND `parent_id`='" . $newBean->id . "'";
      $db->query($sql_DeleteAudit);
   }
}

$testSystemBean = BeanFactory::retrieveBean('GD_Group_Design', $CopyToid);
$testSystemBean->load_relationship('gd_group_design_taskd_task_design_1');
$testSystemBean->gd_group_design_taskd_task_design_1->add($copyingTaskDesignIds);
if (count($copyingTaskDesignIds) > 0) {
   echo "Task Designs have been copied successfully into selected Group Design.";
} else {
   echo "Error! Something went wrong. Please try again.";
}
