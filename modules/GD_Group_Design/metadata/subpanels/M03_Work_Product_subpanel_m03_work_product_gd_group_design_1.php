<?php
// created: 2021-10-28 09:02:07
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'min_duration_integer' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_MIN_DURATION_INTEGER',
    'width' => 10,
  ),
  'max_duration_integer' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_MAX_DURATION_INTEGER',
    'width' => 10,
  ),
  'duration_unit' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_DURATION_UNIT',
    'id' => 'U_UNITS_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'U_Units',
    'target_record_key' => 'u_units_id_c',
  ),
  'study_article_type' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STUDY_ARTICLE_TYPE',
    'width' => 10,
  ),
  'other_study_article_type_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_OTHER_STUDY_ARTICLE_TYPE',
    'width' => 10,
    'default' => true,
  ),
);