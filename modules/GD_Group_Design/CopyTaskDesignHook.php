<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class CopyTaskDesignHook
{
    static $already_ran = false;
    function copyTaskDesign($bean, $event, $arguments)
    {
        $module         = $arguments['module'];
        $modelId        = $arguments['id'];
        $relatedModule  = $arguments['related_module'];
        $relatedId      = $arguments['related_id'];      
        if ($module === 'GD_Group_Design' && $relatedModule === 'WPE_Work_Product_Enrollment' && $event == "after_relationship_add") {
            self::$already_ran = true;
            $this->copyTD($modelId, $relatedId, $event);
        } else if ($relatedModule === 'GD_Group_Design' && $module === 'WPE_Work_Product_Enrollment' && $event == "after_relationship_add") {
            self::$already_ran = true;
            $this->copyTD($relatedId, $modelId, $event);
        } else  if ($module === 'GD_Group_Design' && $relatedModule === 'TaskD_Task_Design' && $event == "after_relationship_add") {
            /*14 feb 2022 : Bug fix 2104 : To avoid duplicate record on wpa gd link*/
            if (self::$already_ran == false) {
                $this->copyTDtoTS($modelId, $relatedId, $event);
            }
        }
    }
    public function copyTD($id, $rel_id, $event)
    {
        /**#758 : Auto-create Task Designs based on WPA link to GD :  */
        $GLOBALS['log']->fatal('in file copyTaskDesignHook in fun copyTD for Auto-create Task Designs based on WPA link to GD');
        $GLOBALS['log']->fatal('in file copyTaskDesignHook in fun copyTD line 31 :  == ' . $id);
        global $db, $current_user;
        $current_date    = date("Y-m-d"); //current date 
        $currentGDBean  = BeanFactory::retrieveBean('GD_Group_Design', $id);        
        $currentGDBean->load_relationship('gd_group_design_taskd_task_design_1');
        $relatedTD      = $currentGDBean->gd_group_design_taskd_task_design_1->get();
        $gdBeanName     = $currentGDBean->name;
        $GLOBALS['log']->fatal('in file copyTaskDesignHook in fun copyTD line 40 gdBeanName :  == ' . $gdBeanName);
        $tsArr          = array();
        $tsRelationArr  = array();
        $p1arr          = array();
        $copyingTaskDesignIds = array();
        $linkedWPABean  = BeanFactory::retrieveBean('WPE_Work_Product_Enrollment', $rel_id);
        $linkedWPABean->load_relationship('anml_animals_wpe_work_product_enrollment_1');
        $linkedTestSystemId = $linkedWPABean->anml_animals_wpe_work_product_enrollment_1->get();
        $testSystemBean     = BeanFactory::retrieveBean('ANML_Animals', $linkedTestSystemId[0]);
        $testSystemBean->load_relationship('anml_animals_taskd_task_design_1');

        foreach ($relatedTD as $TDId) {
            $clonedBean = \BeanFactory::getBean('TaskD_Task_Design', $TDId);
            $clonedBean->load_relationship("m03_work_product_taskd_task_design_1");
            $clonedBean->load_relationship("gd_group_design_taskd_task_design_1");
            $clonedBean->load_relationship("taskd_task_design_taskd_task_design_1");
            $clonedBean->load_relationship("anml_animals_taskd_task_design_1");
            $workProductId    = $clonedBean->m03_work_product_taskd_task_design_1->get();
            $groupDesignIds = $clonedBean->gd_group_design_taskd_task_design_1->get();
            $date_entered    = date("Y-m-d H:i:s", time());
            /* This line is not working That's why we will get task design Ids by custom query */
            // $taskDesignIds=$clonedBean->taskd_task_design_taskd_task_design_1->get();
            /* Custom Query to get Task design Ids */
            $parentTaskDesignIds    = '';
            $sqlGetTaskDesignIds    = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_ida as parent_TDId
                                    FROM taskd_task_design_taskd_task_design_1_c 
                                    where taskd_task_design_taskd_task_design_1taskd_task_design_idb='" . $TDId . "' and deleted=0";
            $resultTaskDesigns      = $db->query($sqlGetTaskDesignIds);
            $rowTaskDesignIds       = $db->fetchByAssoc($resultTaskDesigns);
            $parentTaskDesignIds    = $rowTaskDesignIds['parent_TDId'];
            $testSystemIds          = $clonedBean->anml_animals_taskd_task_design_1->get();


            $clonedBean->id         = create_guid();
            $tsArr[$clonedBean->id] = $TDId;
            $tsRelationArr[$TDId]   = $parentTaskDesignIds;
            $clonedBean->new_with_id    = true;
            $clonedBean->fetched_row    = null;

            $p1arr[$TDId]               = $parentTaskDesignIds;
            $a1arr[$clonedBean->id]     = $TDId;
            $b1arr[$TDId]               = $clonedBean->id; // New Set of Task Design ID
            $clonedBean->type_2         = "Actual";
            $clonedBean->u_units_id_c   = $clonedBean->u_units_id_c;
            $clonedBean->end_integer    = $clonedBean->end_integer;
            $clonedBean->date_entered   = $date_entered;
            $parent_td_id_c = $TDId;
            $copyingTaskDesignIds[]     = $clonedBean->id;
            // $clonedBean->taskd_task_design_taskd_task_design_1->add($TDId);
            /*24 Jan 2022 : Bug#2033 fixes : Task Design creation*/
            $WPID                       = $workProductId[0];
            $GD_ID                      = $groupDesignIds[0];
            $TS_ID                      = $linkedTestSystemId[0];
            if ($WPID != '' && $GD_ID != '') {
                $clonedBean->save();
            }

            /*EOC 24 Jan 2022 : Bug#2033 fixes : Task Design creation*/
            $clonedBean->m03_work_product_taskd_task_design_1->add($workProductId);
            $clonedBean->gd_group_design_taskd_task_design_1->add($groupDesignIds);
            //$clonedBean->anml_animals_taskd_task_design_1->add($testSystemIds); vin change
            $clonedBean->anml_animals_taskd_task_design_1->add($linkedTestSystemId[0]); //vin Change
            /*#1353 : Custom name calculation 10 June 2021 */
            $wpBean         = BeanFactory::getBean('M03_Work_Product', $workProductId[0]);
            $wpName         =  $wpBean->name;
            //$TSBean = BeanFactory::getBean('ANML_Animals', $testSystemIds[0]);
            $TSBean         = BeanFactory::getBean('ANML_Animals', $linkedTestSystemId[0]);
            $tsName         =  $TSBean->name;
            /* Ticket #1820 14-Dec. 2021 */
            $usda_id_name   = $clonedBean->usda_id_c;
            $task_type      = $clonedBean->task_type;
            $custom_task    = $clonedBean->custom_task;
            $standard_task  = $clonedBean->standard_task;
            $clonedBean->parent_td_id_c = $parent_td_id_c;

            if ($task_type == 'Custom') {
                $standard_task = '';
                $standard_task_val = $custom_task;
            } else if ($task_type == 'Standard') {
                $custom_task = '';
                $standard_task_val = $standard_task;
            }
            /* Ticket #1820 14-Dec. 2021 Update Name*/
            if ($usda_id_name != '') {
                $clonedBean->name = $wpName . ' ' . $tsName . ' ' . $usda_id_name . ' ' . $standard_task_val;
            } else {
                $clonedBean->name = $wpName . ' ' . $tsName . ' ' . $standard_task_val;
            }

            /*EOC #1353 : Custom name calculation 10 June 2021 */
            // Re-saving the same record for creating audit log 
            $clonedBean->gd_group_design_taskd_task_design_1->delete($groupDesignIds[0]);
            $testSystemBean->anml_animals_taskd_task_design_1->add($clonedBean->id);
            /*24 Jan 2022 : Bug#2033 fixes : Task Design creation*/
            if ($WPID != '') {
                $clonedBean->save();
            }
            /*EOC 24 Jan 2022 : Bug#2033 fixes : Task Design creation*/
            $auditsql = 'UPDATE `taskd_task_design_audit`  set `field_name`= "u_units_id_c",`after_value_string`="' . $clonedBean->u_units_id_c . '" 
                         Where parent_id = "' . $clonedBean->id . '" AND field_name="integer_units"';
            $db->query($auditsql);
            $queryAuditLog    = "SELECT id,count(*) as NUM FROM `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' 
            AND `parent_id`='" . $clonedBean->id . "' group by before_value_string,after_value_string,date_created";
            $auditLogResult    = $db->query($queryAuditLog);
            if ($auditLogResult->num_rows > 0) {
                while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
                    if ($fetchAuditLog['NUM'] > 1) {
                        $recordID            = $fetchAuditLog['id'];
                        $sql_DeleteAudit    = "DELETE from `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' AND `parent_id`='" . $clonedBean->id . "' AND id = '" . $recordID . "'";
                        $db->query($sql_DeleteAudit);
                    }
                }
            }

            /**Audit log add */
            $source = '{"subject":{"_type":"logic-hook","class":"CopyTaskDesignToGD","method":"CopyTaskDesignToGD"},"attributes":[]}';
            if ($clonedBean->relative != '') {
                $auditEventid = create_guid();
                $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"relative","datetime","","' . $clonedBean->relative . '")';
                $auditsqlResult_1 = $db->query($auditsql_1);

                $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                $db->query($auditsqlStatus_1);
            }

            if ($clonedBean->category != '') {
                $auditEventid = create_guid();
                $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"category","datetime","","' . $clonedBean->category . '")';
                $auditsqlResult_1 = $db->query($auditsql_1);

                $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                $db->query($auditsqlStatus_1);
            }

            if ($clonedBean->type_2 != '') {
                $auditEventid = create_guid();
                $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"type_2","datetime","","' . $clonedBean->type_2 . '")';
                $auditsqlResult_1 = $db->query($auditsql_1);

                $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                $db->query($auditsqlStatus_1);
            }

            if ($clonedBean->task_type != '') {
                $auditEventid = create_guid();
                $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"task_type","datetime","","' . $clonedBean->task_type . '")';
                $auditsqlResult_1 = $db->query($auditsql_1);

                $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                $db->query($auditsqlStatus_1);
            }

            if ($clonedBean->standard_task != '') {
                $auditEventid = create_guid();
                $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"standard_task","datetime","","' . $clonedBean->standard_task . '")';
                $auditsqlResult_1 = $db->query($auditsql_1);

                $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                $db->query($auditsqlStatus_1);
            }

            if ($clonedBean->start_integer != '') {
                $auditEventid = create_guid();
                $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"start_integer","datetime","","' . $clonedBean->start_integer . '")';
                $auditsqlResult_1 = $db->query($auditsql_1);

                $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                $db->query($auditsqlStatus_1);
            }

            if ($clonedBean->end_integer != '') {
                $auditEventid = create_guid();
                $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"end_integer","datetime","","' . $clonedBean->end_integer . '")';
                $auditsqlResult_1 = $db->query($auditsql_1);
                $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                $db->query($auditsqlStatus_1);
            }

            if ($clonedBean->time_window != '') {
                $auditEventid = create_guid();
                $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"time_window","datetime","","' . $clonedBean->time_window . '")';
                $auditsqlResult_1 = $db->query($auditsql_1);

                $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                $db->query($auditsqlStatus_1);
            }

            if ($clonedBean->custom_task != '') {
                $auditEventid = create_guid();
                $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"custom_task","datetime","","' . $clonedBean->custom_task . '")';
                $auditsqlResult_1 = $db->query($auditsql_1);

                $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                $db->query($auditsqlStatus_1);
            }

            if ($clonedBean->order_2_c != '') {
                $auditEventid = create_guid();
                $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"order_2_c","datetime","","' . $clonedBean->order_2_c . '")';
                $auditsqlResult_1 = $db->query($auditsql_1);

                $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                $db->query($auditsqlStatus_1);
            }
            /**/ /////////////// */
        }

        $counter = 0;
        foreach ($p1arr as $key => $value) {
            $newBeanId        = $b1arr[$key];
            $toBeRelateID    = array_search($value, $a1arr);
            if ($toBeRelateID != "") {
                $uniqId = create_guid();
                $sqlInsertRelationship = "INSERT INTO `taskd_task_design_taskd_task_design_1_c` 
            (`id`, `date_modified`, `deleted`, `taskd_task_design_taskd_task_design_1taskd_task_design_ida`, `taskd_task_design_taskd_task_design_1taskd_task_design_idb`) 
            VALUES ('" . $uniqId . "', now(), '0', '" . $toBeRelateID . "', '" . $newBeanId . "')";
                $resultsSqlGetTaskDesignIds = $db->query($sqlInsertRelationship);
            }
        }
    }

    public function copyTDtoTS($id, $rel_id, $event)
    {
        global $db;
        $GLOBALS['log']->fatal('in file copyTaskDesignHook in fun copyTDtoTS line 273 currentGDBean :  == ' . $id);
        /*26 Sep 2022 : To check the  TD record created from popup window or from GD link from wpa */
        $relTDBean = BeanFactory::retrieveBean('TaskD_Task_Design', $rel_id);
        $creation_mode_c = $relTDBean->creation_mode_c;        
        /** if record is not liked from pop window then it will create basned on wpa linked with gd  */
        if ($creation_mode_c != "CopyGDbyPopup") {            
            /******************************************************************* */
            $GDBean = BeanFactory::retrieveBean('GD_Group_Design', $id);
            $wpid = $GDBean->m03_work_product_gd_group_design_1m03_work_product_ida;
            $wpname = $GDBean->m03_work_product_gd_group_design_1_name;
            $copyingTaskDesignIds = array();
            $tsArr = array();
            $tsRelationArr = array();
            $p1arr = array();
            /**Get the latest wpa record assigned to gd */
            $sql = "SELECT id,gd_group_daf65ollment_idb
        FROM gd_group_design_wpe_work_product_enrollment_1_c
        WHERE gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida='" . $id . "' AND deleted=0 order by date_modified desc";
            $result = $db->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $db->fetchByAssoc($result)) {
                    $relatedWpeId = $row['gd_group_daf65ollment_idb'];
                    /**Get the test system liked in fetched wpa record assigned to gd */
                    $linkedWPABean = BeanFactory::retrieveBean('WPE_Work_Product_Enrollment', $relatedWpeId);
                    $linkedWPABean->load_relationship('anml_animals_wpe_work_product_enrollment_1');
                    $linkedTestSystemId = $linkedWPABean->anml_animals_wpe_work_product_enrollment_1->get();
                    $testSystemBean = BeanFactory::retrieveBean('ANML_Animals', $linkedTestSystemId[0]);
                    $testSystemBean->load_relationship('anml_animals_taskd_task_design_1');
                    $clonedBean = BeanFactory::retrieveBean('TaskD_Task_Design', $rel_id);
                    $other_equipment = $clonedBean->other_equipment_c;
                    $date_entered = date("Y-m-d H:i:s", time());
                    $clonedBean->load_relationship('taskd_task_design_taskd_task_design_1');
                    $type_2 = "Actual";
                    $u_units_id_c = $clonedBean->u_units_id_c;

                    /** Get the test start_integer and end integer from TDs 2189 */

                    $start_day = $clonedBean->start_day_of_recurrence_c;

                    //$GLOBALS['log']->fatal('order_start_integer_c  line 312 ' . $clonedBean->order_start_integer_c);
                    //$GLOBALS['log']->fatal('start_day  line 312 ' . $start_day);
                    if ($start_day != '' || $start_day === 0) {
                        $clonedBean->order_2_c      = $clonedBean->order_start_integer_c;
                        $clonedBean->start_integer  = $start_day;
                        $clonedBean->end_integer    = $start_day;
                        $clonedBean->u_units_id_c   = "ad61bde4-1129-11ea-8f07-06e41dba421a";
                        $u_units_id_c               = $clonedBean->u_units_id_c;
                        $clonedBean->time_window    = 'Day' . " " . $start_day . "-" . $start_day;
                    }

                    $start_integer = $clonedBean->start_integer;
                    $end_integer = $clonedBean->end_integer;
                    $task_type = $clonedBean->task_type;
                    $custom_task = $clonedBean->custom_task;
                    $standard_task = $clonedBean->standard_task;
                    $time_window = $clonedBean->time_window;
                    $actual_datetime = $clonedBean->actual_datetime;
                    $scheduled_start_datetime = $clonedBean->scheduled_start_datetime;
                    $scheduled_end_datetime = $clonedBean->scheduled_end_datetime;
                    $category = $clonedBean->category;
                    $planned_start_datetime_1st = $clonedBean->planned_start_datetime_1st;
                    $planned_end_datetime_1st = $clonedBean->planned_end_datetime_1st;
                    $planned_start_datetime_2nd = $clonedBean->planned_start_datetime_2nd;
                    $planned_end_datetime_2nd = $clonedBean->planned_end_datetime_2nd;
                    $equipment_required = $clonedBean->equipment_required;
                    $description = $clonedBean->description;
                    $order_2_c = $clonedBean->order_2_c;
                    $phase_c = $clonedBean->phase_c;
                    $parent_td_id_c = $clonedBean->id;
                    $relative = $clonedBean->relative;
                    $days_from_initial_task = $clonedBean->days_from_initial_task;
                    $task_design_linked_id_c = $clonedBean->task_design_linked_id_c;
                    $type_of_personnel      = $clonedBean->type_of_personnel_2_c;
                    $duration_integer_min   = $clonedBean->duration_integer_min_c;
                    $integer_units = $clonedBean->integer_units;
                    $usda_id_name = $clonedBean->usda_id_c;
                    /**Get the child record id and save the data from main record into related record*/
                    $childBean = BeanFactory::newBean('TaskD_Task_Design');
                    $tsid = create_guid();
                    $childBean->id = $tsid;
                    $childBean->new_with_id = true;
                    $tsName =  $testSystemBean->name;
                    if ($task_type == 'Custom') {
                        $standard_task = '';
                        $standard_task_val = $custom_task;
                    } else if ($task_type == 'Standard') {
                        $custom_task = '';
                        $standard_task_val = $standard_task;
                    }
                    if ($usda_id_name != '') {
                        $chilbean_name = $wpname . ' ' . $tsName . ' ' . $usda_id_name . ' ' . $standard_task_val;
                    } else {
                        $chilbean_name = $wpname . ' ' . $tsName . ' ' . $standard_task_val;
                    }
                    $childBean->name = $chilbean_name;
                    $childBean->type_2 = $type_2;
                    $childBean->other_equipment_c = $other_equipment;
                    $childBean->date_entered = $date_entered;
                    $childBean->u_units_id_c = $u_units_id_c;
                    $childBean->start_integer = $start_integer;
                    $childBean->end_integer = $end_integer;
                    $childBean->task_type = $task_type;
                    $childBean->custom_task = $custom_task;
                    $childBean->standard_task = $standard_task;
                    $childBean->time_window = $time_window;
                    $childBean->actual_datetime = $actual_datetime;
                    $childBean->scheduled_start_datetime = $scheduled_start_datetime;
                    $childBean->scheduled_end_datetime = $scheduled_end_datetime;
                    $childBean->category = $category;
                    $childBean->equipment_required = $equipment_required;
                    $childBean->description = $description;
                    $childBean->phase_c = $phase_c;
                    $childBean->relative = $relative;
                    $childBean->days_from_initial_task = $days_from_initial_task;
                    $childBean->parent_td_id_c = $parent_td_id_c;
                    $childBean->type_of_personnel_2_c    = $type_of_personnel;
                    $childBean->duration_integer_min_c   = $duration_integer_min;
                    $childBean->integer_units = $integer_units;
                    $childBean->order_2_c = $order_2_c;
                    $childBean->save();
                    /**Relationship add to child record */
                    $childBean->load_relationship("m03_work_product_taskd_task_design_1");
                    $childBean->load_relationship("gd_group_design_taskd_task_design_1");
                    $childBean->load_relationship("taskd_task_design_taskd_task_design_1");
                    $childBean->m03_work_product_taskd_task_design_1->add($wpid);
                    $childBean->anml_animals_taskd_task_design_1->add($linkedTestSystemId[0]);

                    $usda_id_c = $testSystemBean->usda_id_c;

                    $uniqId = create_guid();
                    $taskDesignIds = $task_design_linked_id_c;
                    if ($taskDesignIds != '') {
                        /**Create a new TSD record of task design linked to  linked test system */
                        $clonedBean_linked = BeanFactory::retrieveBean('TaskD_Task_Design', $taskDesignIds);
                        $clonedBean_linked->load_relationship("anml_animals_taskd_task_design_1");
                        /**Get the child record id and save the data from main record into related record*/
                        $childBean_new = BeanFactory::newBean('TaskD_Task_Design');
                        $tsid_new = create_guid();
                        $childBean_new->id = $tsid_new;
                        $childBean_new->new_with_id = true;
                        $custom_task_link = $clonedBean_linked->custom_task;
                        $standard_task_link = $clonedBean_linked->standard_task;
                        $usda_id_name_new = $usda_id_c;

                        if ($clonedBean_linked->task_type == 'Custom') {
                            $standard_task_link = '';
                            $standard_task_val_linked = $custom_task_link;
                        } else if ($clonedBean_linked->task_type == 'Standard') {
                            $custom_task_link = '';
                            $standard_task_val_linked = $standard_task_link;
                        }

                        if ($usda_id_name_new != '') {
                            $chilbean_name_new = $wpname . ' ' . $tsName . ' ' . $usda_id_name_new . ' ' . $standard_task_val_linked;
                        } else {
                            $chilbean_name_new = $wpname . ' ' . $tsName . ' ' . $standard_task_val_linked;
                        }
                        /**Remove all the space from string to match tha name with db record to avoid any space issue in query */
                        $name_without_space = str_replace(' ', '', $chilbean_name_new);
                        $childBean_new->name = $chilbean_name_new;
                        /**Check the record already exist for same name  */
                        $sqlGettsname = "SELECT id,name 
                    FROM taskd_task_design 
                    where REPLACE(name, ' ', '')='$name_without_space' and deleted=0";
                        $resultsSqlGettsid = $db->query($sqlGettsname);
                        $rowSqlGetTsId = $db->fetchByAssoc($resultsSqlGettsid);
                        $Ts_id = $rowSqlGetTsId['id'];
                        $Ts_name = $rowSqlGetTsId['name'];
                        if ($Ts_id != '') {
                            $sqlInsertRelationship = "INSERT INTO `taskd_task_design_taskd_task_design_1_c` 
                        (`id`, `date_modified`, `deleted`, `taskd_task_design_taskd_task_design_1taskd_task_design_ida`, `taskd_task_design_taskd_task_design_1taskd_task_design_idb`) 
                        VALUES ('$uniqId', now(), '0', '$Ts_id','$childBean->id')";
                            $resultsSqlGetTaskDesignIds = $db->query($sqlInsertRelationship);
                            /**18 Nov 2021 : 1551 Bug fix : Can the Planned Date/Time fields calculate at or right after record creation if the necessary fields 
                             * feeding in are populated already? */
                            $tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $Ts_id);
                            if ($tdBean->actual_datetime == "") {
                                $this->SetCustomCalNotActualTime($childBean, $tdBean);
                            } else {
                                $this->SetCustomCalActualTime($childBean, $tdBean);
                            }
                            /******************************************************************************************************
                             * ******************************************************************************************** */
                        }
                    }
                }
            }
        }
    }
    /** Set custom calculation for planed start/end fields for new created record  */
    public function SetCustomCalNotActualTime($childBean, $tdBean)
    {
        global $db;
        $unit_Bean = BeanFactory::getBean('U_Units', $childBean->u_units_id_c);
        $unit_Bean_name = $unit_Bean->name;
        if ($unit_Bean_name == "Day") {
            $integer_u = 'days';
        } elseif ($unit_Bean_name == "Hour") {
            $integer_u = 'hours';
        } else {
            $integer_u = 'minutes';
        }
        if (($childBean->type_2 == "Actual SP" || $childBean->type_2 == "Actual") && $childBean->relative == "1st Tier") {
            if ($tdBean->scheduled_start_datetime != '') {
                $result = date($tdBean->scheduled_start_datetime);
                $result = strtotime($result . '+' . $childBean->start_integer . ' ' . $integer_u);
                $planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
            } else {
                $planned_start_datetime_1st = null;
            }
            if ($tdBean->scheduled_end_datetime != '') {
                $result = date($tdBean->scheduled_end_datetime);
                $result = strtotime($result . '+' . $childBean->end_integer . ' ' . $integer_u);
                $planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
            } else {
                $planned_end_datetime_1st = null;
            }
        }
        if (($childBean->type_2 == "Actual SP" || $childBean->type_2 == "Actual") && $childBean->relative == "2nd Tier") {
            if ($tdBean->planned_start_datetime_1st != "") {
                $result = date($tdBean->planned_start_datetime_1st);
                $result = strtotime($result . '+' . $childBean->start_integer . ' ' . $integer_u);
                $planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
            } else {
                $planned_start_datetime_2nd = null;
            }
            if ($tdBean->planned_end_datetime_1st != "") {
                $result = date($tdBean->planned_end_datetime_1st);
                $result = strtotime($result . '+' . $childBean->end_integer . ' ' . $integer_u);
                $planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
            } else {
                $planned_end_datetime_2nd = null;
            }
        }

        $childBean->planned_start_datetime_1st = $planned_start_datetime_1st;
        $childBean->planned_end_datetime_1st = $planned_end_datetime_1st;
        $childBean->planned_start_datetime_2nd = $planned_start_datetime_2nd;
        $childBean->planned_end_datetime_2nd = $planned_end_datetime_2nd;
        $childBean->save();
    }

    public function SetCustomCalActualTime($childBean, $tdBean)
    {
        global $db;
        $unit_Bean = BeanFactory::getBean('U_Units', $childBean->u_units_id_c);
        $unit_Bean_name = $unit_Bean->name;
        if ($unit_Bean_name == "Day") {
            $integer_u = 'days';
        } elseif ($unit_Bean_name == "Hour") {
            $integer_u = 'hours';
        } else {
            $integer_u = 'minutes';
        }

        if (($childBean->type_2 == "Actual SP" || $childBean->type_2 == "Actual") && $childBean->relative == "1st Tier") {
            if ($tdBean->actual_datetime != '') {
                $result = date($tdBean->actual_datetime);
                $result = strtotime($result . '+' . $childBean->start_integer . ' ' . $integer_u);
                $planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
            } else {
                $planned_start_datetime_1st = null;
            }
            if ($tdBean->actual_datetime != '') {
                $result = date($tdBean->actual_datetime);
                $result = strtotime($result . '+' . $childBean->end_integer . ' ' . $integer_u);
                $planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
            } else {
                $planned_end_datetime_1st = null;
            }
        }
        if (($childBean->type_2 == "Actual SP" || $childBean->type_2 == "Actual") && $childBean->relative == "2nd Tier") {
            if ($tdBean->actual_datetime != '') {
                $result = date($tdBean->actual_datetime);
                $result = strtotime($result . '+' . $childBean->start_integer . ' ' . $integer_u);
                $planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
            } else {
                $planned_start_datetime_2nd = null;
            }
            if ($tdBean->actual_datetime != '') {
                $result = date($tdBean->actual_datetime);
                $result = strtotime($result . '+' . $childBean->end_integer . ' ' . $integer_u);
                $planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
            } else {
                $planned_end_datetime_2nd = null;
            }
        }

        $childBean->planned_start_datetime_1st = $planned_start_datetime_1st;
        $childBean->planned_end_datetime_1st = $planned_end_datetime_1st;
        $childBean->planned_start_datetime_2nd = $planned_start_datetime_2nd;
        $childBean->planned_end_datetime_2nd = $planned_end_datetime_2nd;
        $childBean->save();
    }
}
