<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-06-14 05:27:34
$viewdefs['GD_Group_Design']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'context' => 
  array (
    'link' => 'gd_group_design_taskd_task_design_1',
  ),
);

// created: 2021-08-14 11:12:04
$viewdefs['GD_Group_Design']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'context' => 
  array (
    'link' => 'gd_group_design_wpe_work_product_enrollment_1',
  ),
);