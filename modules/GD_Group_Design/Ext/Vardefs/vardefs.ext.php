<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/Vardefs/m03_work_product_gd_group_design_1_GD_Group_Design.php

// created: 2021-01-19 13:25:53
$dictionary["GD_Group_Design"]["fields"]["m03_work_product_gd_group_design_1"] = array (
  'name' => 'm03_work_product_gd_group_design_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_gd_group_design_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'id_name' => 'm03_work_product_gd_group_design_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["GD_Group_Design"]["fields"]["m03_work_product_gd_group_design_1_name"] = array (
  'name' => 'm03_work_product_gd_group_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_GD_GROUP_DESIGN_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_gd_group_design_1m03_work_product_ida',
  'link' => 'm03_work_product_gd_group_design_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["GD_Group_Design"]["fields"]["m03_work_product_gd_group_design_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_gd_group_design_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE_ID',
  'id_name' => 'm03_work_product_gd_group_design_1m03_work_product_ida',
  'link' => 'm03_work_product_gd_group_design_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/Vardefs/gd_group_design_taskd_task_design_1_GD_Group_Design.php

// created: 2021-01-19 13:36:34
$dictionary["GD_Group_Design"]["fields"]["gd_group_design_taskd_task_design_1"] = array (
  'name' => 'gd_group_design_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'gd_group_design_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'vname' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'id_name' => 'gd_group_design_taskd_task_design_1gd_group_design_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/Vardefs/sugarfield_number_2.php

 // created: 2021-02-03 05:21:02
$dictionary['GD_Group_Design']['fields']['number_2']['calculated']='';
$dictionary['GD_Group_Design']['fields']['number_2']['enforced']='1';

 
?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/Vardefs/sugarfield_number_3_c.php

 // created: 2021-03-31 05:20:02
$dictionary['GD_Group_Design']['fields']['number_3_c']['labelValue']='Number';
$dictionary['GD_Group_Design']['fields']['number_3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['GD_Group_Design']['fields']['number_3_c']['enforced']='false';
$dictionary['GD_Group_Design']['fields']['number_3_c']['dependency']='';
$dictionary['GD_Group_Design']['fields']['number_3_c']['required_formula']='';
$dictionary['GD_Group_Design']['fields']['number_3_c']['readonly']='1';
$dictionary['GD_Group_Design']['fields']['number_3_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/Vardefs/gd_group_design_wpe_work_product_enrollment_1_GD_Group_Design.php

// created: 2021-01-19 13:33:18
$dictionary["GD_Group_Design"]["fields"]["gd_group_design_wpe_work_product_enrollment_1"] = array (
  'name' => 'gd_group_design_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'gd_group_design_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_GD_GROUP_DESIGN_TITLE',
  'id_name' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/Vardefs/sugarfield_study_article_type.php

 // created: 2021-10-28 08:54:06

 
?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/Vardefs/sugarfield_other_study_article_type_c.php

 // created: 2021-10-28 08:56:28
$dictionary['GD_Group_Design']['fields']['other_study_article_type_c']['labelValue']='Other Study Article Type';
$dictionary['GD_Group_Design']['fields']['other_study_article_type_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['GD_Group_Design']['fields']['other_study_article_type_c']['enforced']='';
$dictionary['GD_Group_Design']['fields']['other_study_article_type_c']['dependency']='equal($study_article_type,"Other")';
$dictionary['GD_Group_Design']['fields']['other_study_article_type_c']['required_formula']='equal($study_article_type,"Other")';
$dictionary['GD_Group_Design']['fields']['other_study_article_type_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/Vardefs/sugarfield_status_c.php

 // created: 2022-02-03 09:44:28
$dictionary['GD_Group_Design']['fields']['status_c']['labelValue']='Status';
$dictionary['GD_Group_Design']['fields']['status_c']['dependency']='';
$dictionary['GD_Group_Design']['fields']['status_c']['required_formula']='';
$dictionary['GD_Group_Design']['fields']['status_c']['readonly_formula']='';
$dictionary['GD_Group_Design']['fields']['status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/Vardefs/sugarfield_complete_yes_no_c.php

 // created: 2022-02-08 08:32:38
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['duplicate_merge_dom_value']=0;
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['labelValue']='Complete Yes/No';
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['calculated']='true';
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['formula']='ifElse(equal($status_c,"Complete"),0,1)';
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['enforced']='true';
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['dependency']='';
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['required_formula']='';
$dictionary['GD_Group_Design']['fields']['complete_yes_no_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/Vardefs/bid_batch_id_gd_group_design_1_GD_Group_Design.php

// created: 2022-09-22 05:28:30
$dictionary["GD_Group_Design"]["fields"]["bid_batch_id_gd_group_design_1"] = array (
  'name' => 'bid_batch_id_gd_group_design_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_gd_group_design_1',
  'source' => 'non-db',
  'module' => 'BID_Batch_ID',
  'bean_name' => 'BID_Batch_ID',
  'side' => 'right',
  'vname' => 'LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'id_name' => 'bid_batch_id_gd_group_design_1bid_batch_id_ida',
  'link-type' => 'one',
);
$dictionary["GD_Group_Design"]["fields"]["bid_batch_id_gd_group_design_1_name"] = array (
  'name' => 'bid_batch_id_gd_group_design_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_BID_BATCH_ID_TITLE',
  'save' => true,
  'id_name' => 'bid_batch_id_gd_group_design_1bid_batch_id_ida',
  'link' => 'bid_batch_id_gd_group_design_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'name',
);
$dictionary["GD_Group_Design"]["fields"]["bid_batch_id_gd_group_design_1bid_batch_id_ida"] = array (
  'name' => 'bid_batch_id_gd_group_design_1bid_batch_id_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE_ID',
  'id_name' => 'bid_batch_id_gd_group_design_1bid_batch_id_ida',
  'link' => 'bid_batch_id_gd_group_design_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/Vardefs/sugarfield_name.php

 // created: 2022-09-22 05:42:29
$dictionary['GD_Group_Design']['fields']['name']['hidemassupdate']=false;
$dictionary['GD_Group_Design']['fields']['name']['importable']='false';
$dictionary['GD_Group_Design']['fields']['name']['duplicate_merge']='disabled';
$dictionary['GD_Group_Design']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['GD_Group_Design']['fields']['name']['merge_filter']='disabled';
$dictionary['GD_Group_Design']['fields']['name']['unified_search']=false;
$dictionary['GD_Group_Design']['fields']['name']['calculated']='1';
$dictionary['GD_Group_Design']['fields']['name']['required']=true;
$dictionary['GD_Group_Design']['fields']['name']['readonly']=true;
$dictionary['GD_Group_Design']['fields']['name']['formula']='concat(related($bid_batch_id_gd_group_design_1,"name"),related($m03_work_product_gd_group_design_1,"name")," GRP",getDropdownValue("number_list",$number_2))';
$dictionary['GD_Group_Design']['fields']['name']['enforced']=true;

 
?>
