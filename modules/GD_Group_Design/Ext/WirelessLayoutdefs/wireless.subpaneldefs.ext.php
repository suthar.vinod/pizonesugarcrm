<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/WirelessLayoutdefs/gd_group_design_taskd_task_design_1_GD_Group_Design.php

 // created: 2021-08-14 05:50:46
$layout_defs["GD_Group_Design"]["subpanel_setup"]['gd_group_design_taskd_task_design_1'] = array (
  'order' => 100,
  'module' => 'TaskD_Task_Design',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_GD_GROUP_DESIGN_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'get_subpanel_data' => 'gd_group_design_taskd_task_design_1',
);

?>
<?php
// Merged from custom/Extension/modules/GD_Group_Design/Ext/WirelessLayoutdefs/gd_group_design_wpe_work_product_enrollment_1_GD_Group_Design.php

 // created: 2021-08-14 11:12:04
$layout_defs["GD_Group_Design"]["subpanel_setup"]['gd_group_design_wpe_work_product_enrollment_1'] = array (
  'order' => 100,
  'module' => 'WPE_Work_Product_Enrollment',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'get_subpanel_data' => 'gd_group_design_wpe_work_product_enrollment_1',
);

?>
