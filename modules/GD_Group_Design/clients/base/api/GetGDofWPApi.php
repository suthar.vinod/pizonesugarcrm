<?php
if (!defined('sugarEntry') || !sugarEntry) {
  die('Not A Valid Entry Point');
}
class GetGDofWPApi extends SugarApi
{
  public function registerApiRest()
  {
    return array(
      'MyGetEndpointNum' => array(
        'reqType' => 'GET',
        'noLoginRequired' => false,
        'path' => array('GD_Group_Design', '?', '?', 'get_gd_of_currentWP'),
        'pathVars' => array('module', 'wpname', 'gdname'),
        'method' => 'get_gd_of_currentWP',
        'shortHelp' => '',
        'longHelp' => '',
      ),
    );
  }

  public function get_gd_of_currentWP($api, $args)
  {
    global $db;
    $module = $args['module'];
    $WP = $args['wpname'];
    $GDname = $args['gdname'];
    $html_opt ="";

    $sql = "SELECT tbl_gd.id,tbl_gd.name,tbl_gd.deleted
FROM gd_group_design AS tbl_gd
LEFT JOIN m03_work_product_gd_group_design_1_c AS tbl_wp_gd ON tbl_gd.id = tbl_wp_gd.m03_work_product_gd_group_design_1gd_group_design_idb
WHERE tbl_wp_gd.m03_work_product_gd_group_design_1m03_work_product_ida='" . $WP . "' AND tbl_gd.name!='" . $GDname . "' AND tbl_gd.deleted=0 AND tbl_gd.deleted=0";
    $result = $db->query($sql);
    $alldata = array();
    if ($result->num_rows > 0) {
      while ($row = $db->fetchByassoc($result)) {
        /**Get the tas design linked to group design */
        $gdid = $row['id'];
        $sql_tsd = "SELECT COUNT(id) AS totaltsd                                                                                             
    FROM gd_group_design_taskd_task_design_1_c                                                                       
    WHERE gd_group_design_taskd_task_design_1gd_group_design_ida='" . $gdid . "' AND deleted=0";
        $result_tsd = $db->query($sql_tsd);
        $row_tsd = $db->fetchByAssoc($result_tsd);
        $totaltsd = $row_tsd['totaltsd'];
        /**Show only those gd records into dropdown having at least 1 tsd linked */
        if ($totaltsd > 0) {
          $alldata[] = array('id' => $row['id'], 'name' => $row['name']);
        }
      }
      $Response = array('data' => $alldata);
    }
    $html_opt .= '';
    foreach ($Response as $key => $res) {
      foreach ($res as $reskey => $resval) {
        $html_opt .= '<option value ="' . $resval['id'] . '" name ="' . $resval['name'] . '">' . $resval['name'] . '</option>';
      }
    }
    return $html_opt;
  }
}
