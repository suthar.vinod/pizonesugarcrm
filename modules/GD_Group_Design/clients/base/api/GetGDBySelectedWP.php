<?php
if (!defined('sugarEntry') || !sugarEntry) {
  die('Not A Valid Entry Point');
}
class GetGDBySelectedWP extends SugarApi
{
  public function registerApiRest()
  {
    return array(
      'MyGetEndpointNum' => array(
        'reqType' => 'GET',
        'noLoginRequired' => false,
        'path' => array('GD_Group_Design', '?', 'get_gd_of_selectedWP'),
        'pathVars' => array('module', 'wpname'),
        'method' => 'get_gd_of_selectedWP',
        'shortHelp' => '',
        'longHelp' => '',
      ),
    );
  }
 /** #2428 : Update to Copy All to Batch Design : 04 May 2022 
  *   If the Work Product has any Task Designs linked with Type = Plan - TS, 
  * the Group Design dropdown should be present in the pop up (if not, hide the dropdown). 
 */
  public function get_gd_of_selectedWP($api, $args)
  {
    global $db;
    $tdtype = array();
    $istypeplan = '';
    $module = $args['module'];
    $WP = $args['wpname'];
    $html_opt = "";
    /** Fetch all the tds records from current work product */
    $currentWPBean = BeanFactory::retrieveBean('M03_Work_Product', $WP);
    $currentWPBean->load_relationship('m03_work_product_taskd_task_design_1');
    $relatedTD = $currentWPBean->m03_work_product_taskd_task_design_1->get();
    foreach ($relatedTD as $TDId) {
      $clonedBean = BeanFactory::getBean('TaskD_Task_Design', $TDId);
      $type = $clonedBean->type_2;
      $tdtype[] = $type;
      if($type=='Plan')
      {
        break;
      }
      
    }
    if (in_array('Plan', $tdtype)) {
      $istypeplan = 'yes';
    } else {
      $istypeplan = 'no';
    }
    /****************************************************** */

    $sql = "SELECT tbl_gd.id,tbl_gd.name,tbl_gd.deleted
FROM gd_group_design AS tbl_gd
LEFT JOIN m03_work_product_gd_group_design_1_c AS tbl_wp_gd ON tbl_gd.id = tbl_wp_gd.m03_work_product_gd_group_design_1gd_group_design_idb
WHERE tbl_wp_gd.m03_work_product_gd_group_design_1m03_work_product_ida='" . $WP . "' AND tbl_gd.deleted=0 AND tbl_gd.deleted=0";
    $result = $db->query($sql);
    $alldata = array();
    if ($result->num_rows > 0) {
      while ($row = $db->fetchByassoc($result)) {
        /**Get the tas design linked to group design */
        $gdid = $row['id'];
        $alldata[] = array('id' => $row['id'], 'name' => $row['name']);
      }
      $Response = array('data' => $alldata);
    }

   $html_opt .= '<option value="" name="">Select Group Design</option>';
    foreach ($Response as $key => $res) {
      foreach ($res as $reskey => $resval) {
        $html_opt .= '<option value ="' . $resval['id'] . '" name ="' . $resval['name'] . '">' . $resval['name'] . '</option>';
      }
    }
    if ($istypeplan == 'yes') {
      return $html_opt;
    } else {
      return "";
    }
  }
}
