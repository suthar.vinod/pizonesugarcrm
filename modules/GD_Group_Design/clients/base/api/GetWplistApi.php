<?php
if (!defined('sugarEntry') || !sugarEntry) {
  die('Not A Valid Entry Point');
}
class GetWplistApi extends SugarApi
{
  public function registerApiRest()
  {
    return array(
      'MyGetEndpointNum' => array(
        'reqType' => 'GET',
        'noLoginRequired' => false,
        'path' => array('GD_Group_Design', '?', 'get_wp_of_gdlinked'),
        'pathVars' => array('module', 'record'),
        'method' => 'get_wp_of_gdlinked',
        'shortHelp' => '',
        'longHelp' => '',
      ),
    );
  }

  public function get_wp_of_gdlinked($api, $args)
  {
    global $db;    
    $module = $args['module'];
    $recordId = $args['record'];
    $Gdbean = BeanFactory::retrieveBean('GD_Group_Design', $recordId);
    $gdname = $Gdbean->name;
    $currentwp_name = $Gdbean->m03_work_product_gd_group_design_1_name;
    $currntwpid = $Gdbean->m03_work_product_gd_group_design_1m03_work_product_ida;   

    $sql = "SELECT tbl_wp.id,tbl_wp.name,tbl_wp.deleted
    FROM m03_work_product AS tbl_wp
    LEFT JOIN m03_work_product_gd_group_design_1_c AS tbl_wp_gd ON tbl_wp.id = tbl_wp_gd.m03_work_product_gd_group_design_1m03_work_product_ida
    WHERE tbl_wp_gd.m03_work_product_gd_group_design_1m03_work_product_ida!='' AND tbl_wp.deleted=0 AND tbl_wp_gd.deleted=0 group by tbl_wp_gd.m03_work_product_gd_group_design_1m03_work_product_ida order by tbl_wp.name asc";
    
    $result = $db->query($sql);
    $alldata = array();
    if ($result->num_rows > 0) {
      while ($row = $db->fetchByassoc($result)) {
          $alldata[] = array('id' => $row['id'], 'name' => $row['name'],'currntwpid' => $currntwpid,'currntwpname' => $currentwp_name,'currntgdname' => $gdname);
      }
      $Response = array('data' => $alldata ); 
    }
    return $Response;
  } 
 
}
