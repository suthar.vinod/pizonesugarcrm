<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class CreateGDGroupApi extends SugarApi {

    public function registerApiRest() {
        return array(
            'MyGetEndpoint' => array(
                'reqType' => 'POST',
                'noLoginRequired' => false,
                'path' => array('create_group_design'),
                'pathVars' => array(),
                'method' => 'create_group_design',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'MyGetEndpointBID' => array(
                'reqType' => 'POST',
                'noLoginRequired' => false,
                'path' => array('create_group_design_bid'),
                'pathVars' => array(),
                'method' => 'create_group_design_bid',
                'shortHelp' => '',
                'longHelp' => '',
            ),
        );
    }

    public function create_group_design($api, $args) {
        global $db, $current_user;
        $module = $args['module'];
        $workproduct = $args['workproduct'];	
		$number_2 = $args['number_2'];
        $queryCountAll = "SELECT * FROM gd_group_design WHERE name = '" . $workproduct . " GRP" . $number_2 . "' AND deleted=0";
		$resultAllRecord = $db->query($queryCountAll);
		if ($resultAllRecord->num_rows > 0) {
				return 'already';
		} 		
    }
    public function create_group_design_bid($api, $args) {
        global $db, $current_user;
        $module = $args['module'];
        $batch_id = $args['batch_id'];	
		$number_2 = $args['number_2'];
        $queryCountAll = "SELECT * FROM gd_group_design WHERE name = '" . $batch_id . " GRP" . $number_2 . "' AND deleted=0";
		$resultAllRecord = $db->query($queryCountAll);
		if ($resultAllRecord->num_rows > 0) {
				return 'already';
		} 		
    }

}
