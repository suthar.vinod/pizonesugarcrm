/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.EditablelistbuttonField
 * @alias SUGAR.App.view.fields.BaseEditablelistbuttonField
 * @extends View.Fields.Base.ButtonField
 */
 ({
    events: {
        'click [name=inline-save]' : 'saveClicked',
        'click [name=inline-cancel]' : 'cancelClicked'
    },
    extendsFrom: 'ButtonField',
    initialize: function(options) {
        this._super("initialize", [options]);
        if(this.name === 'inline-save') {
            this.model.off("change", null, this);
            this.model.on("change", function() {
                this.changed = true;
            }, this);
        }
    },
    _loadTemplate: function() {
        app.view.Field.prototype._loadTemplate.call(this);
        if(this.view.action === 'list' && _.indexOf(['edit', 'disabled'], this.action) >= 0 ) {
            //var number2 = self.model.get('date_modified')
            this.model.defaultNumber = this.model.get('number_2');
            console.log("Onload",this.model.defaultNumber); 
            $('input[name="m03_work_product_gd_group_design_1_name"]').prop('disabled', true);
            this.template = app.template.getField('button', 'edit', this.module, 'edit');
        } else {
            this.template = app.template.empty;
        }
    },
    /**
     * Called whenever validation completes on the model being edited
     * @param {boolean} isValid TRUE if model is valid
     * @private
     */
    _validationComplete : function(isValid){
        if (!isValid) {
            this.setDisabled(false);
            return;
        }
        if (!this.changed) {
            this.cancelEdit();
            return;
        }

        this._save();
    },

    /**
     * Called when the model is successfully saved
     *
     * @param {Data.Bean} model The updated model
     * @private
     */
    _onSaveSuccess: function(model) {
        this.changed = false;
        this.view.toggleRow(model.id, false);
    },

    _save: function() {
        var self = this,
            options = {
                success: _.bind(this._onSaveSuccess, this),
                error: function(model, error) {
                    if (error.status === 409) {
                        app.utils.resolve409Conflict(error, self.model, function(model, isDatabaseData) {
                            if (model) {
                                if (isDatabaseData) {
                                    successCallback(model);
                                } else {
                                    self._save();
                                }
                            }
                        });
                    }
                },
                complete: function() {
                    // remove this model from the list if it has been unlinked
                    if (self.model.get('_unlinked')) {
                        self.collection.remove(self.model, { silent: true });
                        self.collection.trigger('reset');
                        self.view.render();
                    } else {
                        self.setDisabled(false);
                    }
                },
                lastModified: self.model.get('date_modified'),
                //Show alerts for this request
                showAlerts: {
                    'process': true,
                    'success': {
                        messages: app.lang.get('LBL_RECORD_SAVED', self.module)
                    }
                },
                relate: this.model.link ? true : false
            };

        options = _.extend({}, options, this.getCustomSaveOptions(options));

        this.model.save({}, options);
    },

    getCustomSaveOptions: function(options) {
        return {};
    },

    /**
     * Initiates validation on the model with fields that the user has edit
     * access to.
     */
    saveModel: function() {
        this.setDisabled(true);

        var fieldsToValidate = this.view.getFields(this.module, this.model);
        var erasedFields = this.model.get('_erased_fields');
        fieldsToValidate = _.pick(fieldsToValidate, function(fieldInfo, fieldName) {
            return app.acl.hasAccessToModel('edit', this.model, fieldName) &&
                (!_.contains(erasedFields, fieldName) || this.model.get(fieldName) || fieldInfo.id_name);
        }, this);
        this.model.doValidate(fieldsToValidate, _.bind(this._validationComplete, this));
    },

    cancelEdit: function() {
        if (this.isDisabled()) {
            this.setDisabled(false);
        }
        this.changed = false;
        this.model.revertAttributes();
        this.view.clearValidationErrors();
        this.view.toggleRow(this.model.id, false);

        // trigger a cancel event across the parent context so listening components
        // know the changes made in this row are being reverted
        if(this.context.parent) {
            this.context.parent.trigger('editablelist:cancel', this.model);
        }
    },
    saveClicked: function(evt) {
        // if (!$(evt.currentTarget).hasClass('disabled')) {
        //     this.saveModel();
        // }
        var self = this;
        var workproduct = self.model.get('m03_work_product_gd_group_design_1_name');
        var number_2 = self.model.get('number_2');
        if (this.model.defaultNumber != undefined && this.model.defaultNumber != this.model.get('number_2')) 
        {
            app.api.call("create", "rest/v10/create_group_design", {
                module: 'GD_Group_Design',
                workproduct: workproduct,
                number_2: number_2,
            }, {
                success: function (result) {
                    console.log('result', result);
                    if (result == 'already') {
                        app.alert.show('link_wps_error', {
                            level: 'error',
                            messages: 'Number already in use for this Work Product. Please pick a unique Number.'
                        });
                    } else {
                        this.saveModel();
                    }
                    //app.alert.dismiss('duplicate_creation1');
                }.bind(self),
                error: function (error) {
                    //console.log('error',error);
                    app.alert.show('link_wps_error', {
                        level: 'error',
                        messages: 'Failed Creating Record!'
                    });
                }
            })
        } else {
            this.saveModel();
        }
    },
    cancelClicked: function(evt) {
        this.cancelEdit();
    }
})
