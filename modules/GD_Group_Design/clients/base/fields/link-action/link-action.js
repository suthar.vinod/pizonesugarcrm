 ({

    extendsFrom: 'LinkActionField',
    initialize: function (options) {
        this._super('initialize', [options]);
    },
    openSelectDrawer: function () {
        if (this.isDisabled()) {
            return;
        }
        if (_.isEqual(this.context.get('parentModel').get('_module'), "M03_Work_Product")) {             
            setTimeout(function(){
                $("button.btn.btn-invisible.btn-dark").css("display", "none");
                $(".search-filter .filter-definition-container").css('pointer-events', 'none');
                $(".choice-filter-clickable").css('pointer-events', 'none');
                $('input[name="m03_work_product_gd_group_design_1_name"]').attr('disabled','disabled');
                $('input[name="m03_work_product_gd_group_design_1_name"]').prop('disabled', true);
                $('input[name="filter_row_operator"]').attr('disabled','disabled');
                $('input[name="filter_row_operator"]').prop('disabled',true);
                $('input[name="filter_row_name"]').attr('disabled','disabled');
                $('input[name="filter_row_name"]').prop('disabled',true);                
            },500);
        }
        console.log(this.context.get('parentModel').get("id"));
        var parentModel = this.context.get('parentModel'),
            linkModule = this.context.get('module'),
            link = this.context.get('link'),
            self = this;
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterWorkProductTemplate',
                'initial_filter_label': 'Work Product',
                'filter_populate': {
                    'm03_work_product_gd_group_design_1_name': [this.context.get('parentModel').get("id")],
                }
            })
            .format();
        var context = {
            module: linkModule,
            recParentModel: parentModel,
            recLink: link,
            recContext: this.context,
            recView: this.view
        }
        //this code will execute for all contact subpanel,to apply the filter only for opportunities we will give a condition   

        if (_.isEqual(this.context.get('parentModel').get('_module'), "M03_Work_Product")) {
            context = _.extend(context, {
                filterOptions: filterOptions
            });
        }
        app.drawer.open({
            layout: 'selection-list',
            context: context,
        }, function (model) {
            if (!model) {
                return;
            }
            var relatedModel = app.data.createRelatedBean(parentModel, model.id, link),
                options = {
                    showAlerts: true,
                    relate: true,
                    success: function (model) {
                        self.context.get('collection').resetPagination();
                        self.context.resetLoadFlag();
                        self.context.set('skipFetch', false);
                        var collectionOptions = self.context.get('collectionOptions') || {};
                        if (collectionOptions.limit) self.context.set('limit', collectionOptions.limit);
                        self.context.loadData({
                            success: function () {
                                self.view.layout.trigger('filter:record:linked');
                            },
                            error: function (error) {
                                app.alert.show('server-error', {
                                    level: 'error',
                                    messages: 'ERR_GENERIC_SERVER_ERROR'
                                });
                            }
                        });
                    },
                    error: function (error) {
                        app.alert.show('server-error', {
                            level: 'error',
                            messages: 'ERR_GENERIC_SERVER_ERROR'
                        });
                    }
                };
            relatedModel.save(null, options);
        });
    },

}) 