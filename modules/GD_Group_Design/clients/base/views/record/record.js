({
    extendsFrom: 'RecordView',
    initialize: function (options) {
        var productCost;
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.onload_function, this);
        this.context.on('button:edit_button:click', this.editClicked, this);
        this.model.addValidationTask('check_deliverable_field', _.bind(this._doValidatedeliverableFiled, this));
    },

    editClicked: function () {
        this.model.defaultNumber = this.model.get('number_2');
        this.model.defaultWP = this.model.get('m03_work_product_gd_group_design_1_name');
        this.model.on('change:m03_work_product_gd_group_design_1m03_work_product_ida', this.function_field_hide_show, this);
        this.model.on('change:bid_batch_id_gd_group_design_1bid_batch_id_ida', this.function_field_hide_show, this);
        this.$el.find('input[name="m03_work_product_gd_group_design_1_name"]').prop('disabled', true);
        this.$el.find('input[name="m03_work_product_gd_group_design_1_name"]').attr('disabled','disabled');
        this.$el.find('span[data-fieldname="m03_work_product_gd_group_design_1_name"] .select2-container').addClass('select2-container-disabled');
        this._super('editClicked');        
    },
    function_field_hide_show: function () {
        // console.log("In change fun");
        var self = this;
        let wp = this.model.get("m03_work_product_gd_group_design_1m03_work_product_ida");
        let batchID = this.model.get("bid_batch_id_gd_group_design_1bid_batch_id_ida");
        if (wp != '') {
            // console.log("In WP CHeck");
            $('[data-name="m03_work_product_gd_group_design_1_name"]').css("visibility", "visible");
            $('[data-name="bid_batch_id_gd_group_design_1_name"]').css("visibility", "hidden");
            $('[data-name="m03_work_product_gd_group_design_1_name"]').css("height", "auto");
        } else if(batchID != '') {
            // console.log("In batch CHeck");
            $('[data-name="m03_work_product_gd_group_design_1_name"]').css("visibility", "hidden");
            $('[data-name="bid_batch_id_gd_group_design_1_name"]').css("visibility", "visible");
            $('[data-name="bid_batch_id_gd_group_design_1_name"]').css("height", "auto");
        }else{
            // console.log("In else CHeck");
            $('[data-name="m03_work_product_gd_group_design_1_name"]').css("visibility", "visible");
            $('[data-name="bid_batch_id_gd_group_design_1_name"]').css("visibility", "visible");
            $('[data-name="bid_batch_id_gd_group_design_1_name"]').css("height", "auto");
            $('[data-name="m03_work_product_gd_group_design_1_name"]').css("height", "auto");
        }
    },
    onload_function: function () {
        this.model.defaultNumber = this.model.get('number_2');
        this.model.defaultWP = this.model.get('m03_work_product_gd_group_design_1_name');
        this.model.defaultBD = this.model.get('bid_batch_id_gd_group_design_1_name');
        this.$el.find('input[name="m03_work_product_gd_group_design_1_name"]').prop('disabled', true);
        this.$el.find('span[data-fieldname="m03_work_product_gd_group_design_1_name"] .select2-container').addClass('select2-container-disabled');
        this.$el.find('input[name="m03_work_product_gd_group_design_1_name"]').attr('disabled','disabled');

        let wp = this.model.get("m03_work_product_gd_group_design_1_name");
        let batchID = this.model.get("bid_batch_id_gd_group_design_1_name");
        if (wp != '') {
            // console.log("In WP CHeck");
            $('[data-name="m03_work_product_gd_group_design_1_name"]').css("visibility", "visible");
            $('[data-name="bid_batch_id_gd_group_design_1_name"]').css("visibility", "hidden");
            $('[data-name="m03_work_product_gd_group_design_1_name"]').css("height", "auto");
        } else if(batchID != '') {
            // console.log("In batch CHeck");
            $('[data-name="m03_work_product_gd_group_design_1_name"]').css("visibility", "hidden");
            $('[data-name="bid_batch_id_gd_group_design_1_name"]').css("visibility", "visible");
            $('[data-name="bid_batch_id_gd_group_design_1_name"]').css("height", "auto");
        }else{
            // console.log("In else CHeck");
            $('[data-name="m03_work_product_gd_group_design_1_name"]').css("visibility", "visible");
            $('[data-name="bid_batch_id_gd_group_design_1_name"]').css("visibility", "visible");
            $('[data-name="bid_batch_id_gd_group_design_1_name"]').css("height", "auto");
            $('[data-name="m03_work_product_gd_group_design_1_name"]').css("height", "auto");
        }


    },
    _doValidatedeliverableFiled: function (fields, errors, callback) {
        // console.log("In validate");
        var self = this;
        let wp = this.model.get("m03_work_product_gd_group_design_1m03_work_product_ida");
        let batchID = this.model.get("bid_batch_id_gd_group_design_1bid_batch_id_ida");
        //validate requirements
        if (wp != '') {
            // errors['bid_batch_id_gd_group_design_1_name'] = errors['bid_batch_id_gd_group_design_1_name'] || {};
            // errors['bid_batch_id_gd_group_design_1_name'].required = false;
        }
        else if (batchID != '') {
            // errors['m03_work_product_gd_group_design_1_name'] = errors['m03_work_product_gd_group_design_1_name'] || {};
            // errors['m03_work_product_gd_group_design_1_name'].required = false;
        }else {
            errors['m03_work_product_gd_group_design_1_name'] = errors['m03_work_product_gd_group_design_1_name'] || {};
            errors['m03_work_product_gd_group_design_1_name'].required = true;
            errors['bid_batch_id_gd_group_design_1_name'] = errors['bid_batch_id_gd_group_design_1_name'] || {};
            errors['bid_batch_id_gd_group_design_1_name'].required = true;
        }
        callback(null, fields, errors);
    },

    handleSave: function () {
        var self = this;
        var workproduct = self.model.get('m03_work_product_gd_group_design_1_name');
        var batch_id    = self.model.get('bid_batch_id_gd_group_design_1_name');
        var number_2 = self.model.get('number_2');
        if(workproduct != '') {
        if (this.model.defaultNumber != undefined && this.model.defaultNumber != this.model.get('number_2') && this.model.defaultWP != this.model.get('m03_work_product_gd_group_design_1_name') && this.model.get('m03_work_product_gd_group_design_1_name') != undefined) 
        {
            app.api.call("create", "rest/v10/create_group_design", {
                module: 'GD_Group_Design',
                workproduct: workproduct,
                number_2: number_2,
            }, {
                success: function (result) {
                    //console.log('result', result);
                    if (result == 'already') {
                        app.alert.show('link_wps_error', {
                            level: 'error',
                            messages: 'Number already in use for this Work Product. Please pick a unique Number.'
                        });
                    } else {
                        self._super('handleSave');
                    }
                    //app.alert.dismiss('duplicate_creation1');
                }.bind(self),
                error: function (error) {
                    //console.log('error',error);
                    app.alert.show('link_wps_error', {
                        level: 'error',
                        messages: 'Failed Creating Record!'
                    });
                }
            })
        } else if (this.model.defaultNumber != undefined && this.model.defaultNumber != this.model.get('number_2')) 
        {
            app.api.call("create", "rest/v10/create_group_design", {
                module: 'GD_Group_Design',
                workproduct: workproduct,
                number_2: number_2,
            }, {
                success: function (result) {
                    //console.log('result', result);
                    if (result == 'already') {
                        app.alert.show('link_wps_error', {
                            level: 'error',
                            messages: 'Number already in use for this Work Product. Please pick a unique Number.'
                        });
                    } else {
                        self._super('handleSave');
                    }
                    //app.alert.dismiss('duplicate_creation1');
                }.bind(self),
                error: function (error) {
                    //console.log('error',error);
                    app.alert.show('link_wps_error', {
                        level: 'error',
                        messages: 'Failed Creating Record!'
                    });
                }
            })
        } else if (this.model.defaultWP != undefined && this.model.defaultWP != this.model.get('m03_work_product_gd_group_design_1_name')) 
        {
            app.api.call("create", "rest/v10/create_group_design", {
                module: 'GD_Group_Design',
                workproduct: workproduct,
                number_2: number_2,
            }, {
                success: function (result) {
                    //console.log('result', result);
                    if (result == 'already') {
                        app.alert.show('link_wps_error', {
                            level: 'error',
                            messages: 'Number already in use for this Work Product. Please pick a unique Number.'
                        });
                    } else {
                        self._super('handleSave');
                    }
                    //app.alert.dismiss('duplicate_creation1');
                }.bind(self),
                error: function (error) {
                    //console.log('error',error);
                    app.alert.show('link_wps_error', {
                        level: 'error',
                        messages: 'Failed Creating Record!'
                    });
                }
            })
        } else {
            self._super('handleSave');
        }
    } else if (batch_id != ''){
        if(batch_id != '') {
            if (this.model.defaultNumber != undefined && this.model.defaultNumber != this.model.get('number_2') && this.model.defaultBD != this.model.get('bid_batch_id_gd_group_design_1_name') && this.model.get('bid_batch_id_gd_group_design_1_name') != undefined) 
            {
                app.api.call("create", "rest/v10/create_group_design_bid", {
                    module: 'GD_Group_Design',
                    batch_id: batch_id,
                    number_2: number_2,
                }, {
                    success: function (result) {
                        //console.log('result', result);
                        if (result == 'already') {
                            app.alert.show('link_wps_error', {
                                level: 'error',
                                messages: 'Number already in use for this Batch ID. Please pick a unique Number.'
                            });
                        } else {
                            self._super('handleSave');
                        }
                        //app.alert.dismiss('duplicate_creation1');
                    }.bind(self),
                    error: function (error) {
                        //console.log('error',error);
                        app.alert.show('link_wps_error', {
                            level: 'error',
                            messages: 'Failed Creating Record!'
                        });
                    }
                })
            } else if (this.model.defaultNumber != undefined && this.model.defaultNumber != this.model.get('number_2')) 
            {
                app.api.call("create", "rest/v10/create_group_design_bid", {
                    module: 'GD_Group_Design',
                    batch_id: batch_id,
                    number_2: number_2,
                }, {
                    success: function (result) {
                        //console.log('result', result);
                        if (result == 'already') {
                            app.alert.show('link_wps_error', {
                                level: 'error',
                                messages: 'Number already in use for this Batch ID. Please pick a unique Number.'
                            });
                        } else {
                            self._super('handleSave');
                        }
                        //app.alert.dismiss('duplicate_creation1');
                    }.bind(self),
                    error: function (error) {
                        //console.log('error',error);
                        app.alert.show('link_wps_error', {
                            level: 'error',
                            messages: 'Failed Creating Record!'
                        });
                    }
                })
            } else if (this.model.defaultBD != undefined && this.model.defaultBD != this.model.get('bid_batch_id_gd_group_design_1_name')) 
            {
                app.api.call("create", "rest/v10/create_group_design_bid", {
                    module: 'GD_Group_Design',
                    batch_id: batch_id,
                    number_2: number_2,
                }, {
                    success: function (result) {
                        //console.log('result', result);
                        if (result == 'already') {
                            app.alert.show('link_wps_error', {
                                level: 'error',
                                messages: 'Number already in use for this Batch ID. Please pick a unique Number.'
                            });
                        } else {
                            self._super('handleSave');
                        }
                        //app.alert.dismiss('duplicate_creation1');
                    }.bind(self),
                    error: function (error) {
                        //console.log('error',error);
                        app.alert.show('link_wps_error', {
                            level: 'error',
                            messages: 'Failed Creating Record!'
                        });
                    }
                })
            } else {
                self._super('handleSave');
            }
        }
    }
    },    
})