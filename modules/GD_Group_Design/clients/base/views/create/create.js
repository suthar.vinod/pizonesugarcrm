({
  extendsFrom: 'CreateView',
  initialize: function (options) {
    this._super('initialize', [options]);
    this.on('render', _.bind(this.onload_function, this));
  },
  onload_function: function () {
    //console.log(JSON.stringify(this.model.link));
    // Check if there's related parent record
    if (!_.isEmpty(this.model.link)) {
      var parent_module = this.model.link.bean.attributes._module;
      // Check if related parent record is GD_Group_Design
      if (parent_module == "M03_Work_Product") {
        // Set the work product value into wp of task design from wp of GD_Group_Design
        this.model.attributes.name = this.model.link.bean.attributes.m03_work_product_gd_group_design_1_name;
        this.model.attributes.id = this.model.link.bean.attributes.m03_work_product_gd_group_design_1m03_work_product_ida;
        var self = this;
        setTimeout(function () {
          self.$el.find('input[name="m03_work_product_gd_group_design_1_name"]').prop('disabled', true);
        }, 500);
      }
      else if (parent_module == "BID_Batch_ID") {
        // Set the work product value into wp of task design from wp of GD_Group_Design
        this.model.attributes.name = this.model.link.bean.attributes.bid_batch_id_gd_group_design_1_name;
        this.model.attributes.id   = this.model.link.bean.attributes.bid_batch_id_gd_group_design_1bid_batch_id_ida;
        var self = this;
        $('[data-name="m03_work_product_gd_group_design_1_name"]').css("visibility", "hidden");
        $('[data-name="bid_batch_id_gd_group_design_1_name"]').css("visibility", "visible");
        setTimeout(function () {
          self.$el.find('input[name="bid_batch_id_gd_group_design_1_name"]').prop('disabled', true);
        }, 500);
      }
      // else {
      //   self.$el.find('input[name="m03_work_product_gd_group_design_1_name"]').prop('disabled', false);
      // }
    }
    else{
      $('[data-name="bid_batch_id_gd_group_design_1_name"]').css("visibility", "hidden");
      $('[data-name="m03_work_product_gd_group_design_1_name"]').css("visibility", "hidden");
      // errors['m03_work_product_gd_group_design_1_name'] = errors['m03_work_product_gd_group_design_1_name'] || {};
      // errors['m03_work_product_gd_group_design_1_name'].required = false;
    }
    this._super("render");
  },
  saveAndClose: function () {
    var self = this;
    var workproduct = self.model.get('m03_work_product_gd_group_design_1_name');
    var batch_id    = self.model.get('bid_batch_id_gd_group_design_1_name');
    var number_2 = self.model.get('number_2');
    var parent_module = this.model.link.bean.attributes._module;
    if (parent_module == "M03_Work_Product" && workproduct != "" && number_2 != "" && self.model.get('number_2') && self.model.get('study_article_type') && self.model.get('min_duration_integer') && self.model.get('max_duration_integer') &&
      self.model.get('duration_unit')) {
      app.api.call("create", "rest/v10/create_group_design", {
        module: 'GD_Group_Design',
        workproduct: workproduct,
        number_2: number_2,
      }, {
        success: function (result) {
          //console.log('result', result);
          if (result == 'already') {
            app.alert.show('link_wps_error', {
              level: 'error',
              messages: 'Number already in use for this Work Product. Please pick a unique Number.'
            });
          } else {
            self.initiateSave(_.bind(function initSave() {
              if (self.closestComponent('drawer')) {
                app.drawer.close(self.context, self.model);
              }
            }, self));
          }
          //app.alert.dismiss('duplicate_creation1');
        }.bind(self),
        error: function (error) {
          //console.log('error',error);
          app.alert.show('link_wps_error', {
            level: 'error',
            messages: 'Failed Creating Record!'
          });
        }
      })
    } else if (parent_module == "BID_Batch_ID" && batch_id != "" && number_2 != "" && self.model.get('number_2') && self.model.get('study_article_type') && self.model.get('min_duration_integer') && self.model.get('max_duration_integer') &&
    self.model.get('duration_unit')) {
    app.api.call("create", "rest/v10/create_group_design_bid", {
      module: 'GD_Group_Design',
      batch_id: batch_id,
      number_2: number_2,
    }, {
      success: function (result) {
        //console.log('result', result);
        if (result == 'already') {
          app.alert.show('link_wps_error', {
            level: 'error',
            messages: 'Number already in use for this Batch ID. Please pick a unique Number.'
          });
        } else {
          self.initiateSave(_.bind(function initSave() {
            if (self.closestComponent('drawer')) {
              app.drawer.close(self.context, self.model);
            }
          }, self));
        }
        //app.alert.dismiss('duplicate_creation1');
      }.bind(self),
      error: function (error) {
        //console.log('error',error);
        app.alert.show('link_wps_error', {
          level: 'error',
          messages: 'Failed Creating Record!'
        });
      }
    })
    } else {
      this.initiateSave(_.bind(function () {
        app.navigate(this.context, this.model);
      }, this));
    }
  },
})