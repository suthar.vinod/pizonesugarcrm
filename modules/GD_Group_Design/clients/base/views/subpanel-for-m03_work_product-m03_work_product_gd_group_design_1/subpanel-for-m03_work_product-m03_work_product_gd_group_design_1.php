<?php
// created: 2021-10-28 09:02:10
$viewdefs['GD_Group_Design']['base']['view']['subpanel-for-m03_work_product-m03_work_product_gd_group_design_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'min_duration_integer',
          'label' => 'LBL_MIN_DURATION_INTEGER',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'max_duration_integer',
          'label' => 'LBL_MAX_DURATION_INTEGER',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'duration_unit',
          'label' => 'LBL_DURATION_UNIT',
          'enabled' => true,
          'id' => 'U_UNITS_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'study_article_type',
          'label' => 'LBL_STUDY_ARTICLE_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'other_study_article_type_c',
          'label' => 'LBL_OTHER_STUDY_ARTICLE_TYPE',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);