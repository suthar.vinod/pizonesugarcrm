({
    extendsFrom: "PreviewView",
    initialize: function(options) {
        this._super("initialize", [options]);
        this.on("render", _.bind(this.onload_volume, this));
        this.context.on("button:save_button:click", this._save_desc, this);
    },
    _save_desc: function() {
        var self = this;
        var workproduct = self.model.get("m03_work_product_gd_group_design_1_name");
        var number_2 = self.model.get("number_2");
        if (
            workproduct != "" &&
            number_2 != "" &&
            self.model.get("number_2") &&
            self.model.get("study_article_type") &&
            self.model.get("min_duration_integer") &&
            self.model.get("max_duration_integer") &&
            self.model.get("duration_unit")
        ) {
            app.api.call(
                "create",
                "rest/v10/create_group_design", {
                    module: "GD_Group_Design",
                    workproduct: workproduct,
                    number_2: number_2,
                }, {
                    success: function(result) {
                        console.log("result", result);
                        if (result == "already") {
                            app.alert.show("link_wps_error", {
                                level: "error",
                                messages: "Number already in use for this Work Product. Please pick a unique Number.",
                            });

                        } else {
                            // self.initiateSave(_.bind(function initSave() {
                            // if (self.closestComponent('drawer')) {
                            //     app.drawer.close(self.context, self.model);
                            // }
                            // }, self));
                            this._saveModel();
                        }
                        //app.alert.dismiss('duplicate_creation1');
                    }.bind(self),
                    error: function(error) {
                        //console.log('error',error);
                        app.alert.show("link_wps_error", {
                            level: "error",
                            messages: "Failed Creating Record!",
                        });
                    },
                }
            );
        }
        // else {
        // this.initiateSave(_.bind(function () {
        //     app.navigate(this.context, this.model);
        // }, this));
        // }
    },

    onload_volume: function() {
        //console.log(JSON.stringify(this.model.link));
        $('.preview-edit-btn').hide();
    },
});