<?php
// created: 2022-09-22 10:13:28
$viewdefs['GD_Group_Design']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'assigned_user_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'duration_unit' => 
    array (
    ),
    'min_duration_integer' => 
    array (
    ),
    'max_duration_integer' => 
    array (
    ),
    'name' => 
    array (
    ),
    'number_2' => 
    array (
    ),
    'other_study_article_type_c' => 
    array (
    ),
    'status_c' => 
    array (
    ),
    'study_article_type' => 
    array (
    ),
    'm03_work_product_gd_group_design_1_name' => 
    array (
    ),
    'bid_batch_id_gd_group_design_1_name' => 
    array (
    ),
  ),
);