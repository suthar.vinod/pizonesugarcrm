<?php
// created: ' . date('Y-m-d H:i:s')
$subpanel_layout['list_fields']['document_name']['name'] = 'document_name';
$subpanel_layout['list_fields']['document_name']['vname'] = 'LBL_LIST_DOCUMENT_NAME';
$subpanel_layout['list_fields']['document_name']['widget_class'] = 'SubPanelDetailViewLink';
$subpanel_layout['list_fields']['document_name']['width'] = '10%';
$subpanel_layout['list_fields']['document_name']['default'] = true;
$subpanel_layout['list_fields']['active_date']['name'] = 'active_date';
$subpanel_layout['list_fields']['active_date']['vname'] = 'LBL_DOC_ACTIVE_DATE';
$subpanel_layout['list_fields']['active_date']['width'] = '10%';
$subpanel_layout['list_fields']['active_date']['default'] = true;
$subpanel_layout['list_fields']['m01_sales_m01_quote_document_1_name']['type'] = 'relate';
$subpanel_layout['list_fields']['m01_sales_m01_quote_document_1_name']['link'] = true;
$subpanel_layout['list_fields']['m01_sales_m01_quote_document_1_name']['vname'] = 'LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_SALES_TITLE';
$subpanel_layout['list_fields']['m01_sales_m01_quote_document_1_name']['id'] = 'M01_SALES_M01_QUOTE_DOCUMENT_1M01_SALES_IDA';
$subpanel_layout['list_fields']['m01_sales_m01_quote_document_1_name']['width'] = '10%';
$subpanel_layout['list_fields']['m01_sales_m01_quote_document_1_name']['default'] = true;
$subpanel_layout['list_fields']['m01_sales_m01_quote_document_1_name']['widget_class'] = 'SubPanelDetailViewLink';
$subpanel_layout['list_fields']['m01_sales_m01_quote_document_1_name']['target_module'] = 'M01_Sales';
$subpanel_layout['list_fields']['m01_sales_m01_quote_document_1_name']['target_record_key'] = 'm01_sales_m01_quote_document_1m01_sales_ida';
