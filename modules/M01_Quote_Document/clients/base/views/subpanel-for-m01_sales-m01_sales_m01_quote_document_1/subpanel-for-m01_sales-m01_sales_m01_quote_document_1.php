<?php
// created: 2020-05-29 11:48:02
$viewdefs['M01_Quote_Document']['base']['view']['subpanel-for-m01_sales-m01_sales_m01_quote_document_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'document_name',
          'label' => 'LBL_NAME',
          'enabled' => true,
          'link' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'uploadfile',
          'label' => 'LBL_FILE_UPLOAD',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'category_id',
          'label' => 'LBL_SF_CATEGORY',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'status_id',
          'label' => 'LBL_DOC_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'quoteco_status_c',
          'label' => 'LBL_QUOTECO_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'quoteco_amount_c',
          'label' => 'LBL_QUOTECO_AMOUNT',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'quoteco_date_c',
          'label' => 'LBL_QUOTECO_DATE',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'quoteco_won_date_c',
          'label' => 'LBL_QUOTECO_WON_DATE',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);