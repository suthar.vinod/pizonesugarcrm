<?php
$module_name = 'M01_Quote_Document';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'document_name',
                'label' => 'LBL_NAME',
                'link' => true,
                'default' => true,
                'enabled' => true,
              ),
              1 => 
              array (
                'name' => 'uploadfile',
                'label' => 'LBL_FILE_UPLOAD',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'category_id',
                'label' => 'LBL_LIST_CATEGORY',
                'default' => true,
                'enabled' => true,
              ),
              3 => 
              array (
                'name' => 'status_id',
                'label' => 'LBL_DOC_STATUS',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'analytical_quoted_amount_c',
                'label' => 'LBL_ANALYTICAL_QUOTED_AMOUNT',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'readonly' => false,
                'currency_format' => true,
                'default' => false,
              ),
              6 => 
              array (
                'name' => 'histopathology_quoted_amount_c',
                'label' => 'LBL_HISTOPATHOLOGY_QUOTED_AMOUNT',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'readonly' => false,
                'currency_format' => true,
                'default' => false,
              ),
              7 => 
              array (
                'name' => 'quoteco_status_c',
                'label' => 'LBL_QUOTECO_STATUS',
                'enabled' => true,
                'default' => false,
              ),
              8 => 
              array (
                'name' => 'quoteco_amount_c',
                'label' => 'LBL_QUOTECO_AMOUNT',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
              ),
              9 => 
              array (
                'name' => 'quoteco_date_c',
                'label' => 'LBL_QUOTECO_DATE',
                'enabled' => true,
                'default' => false,
              ),
              10 => 
              array (
                'name' => 'quoteco_won_date_c',
                'label' => 'LBL_QUOTECO_WON_DATE',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
