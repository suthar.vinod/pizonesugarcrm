<?php
// created: 2017-03-24 14:41:43
$viewdefs['M01_Quote_Document']['base']['view']['subpanel-for-accounts-accounts_m01_quote_document_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'document_name',
          'label' => 'LBL_LIST_DOCUMENT_NAME',
          'enabled' => true,
          'default' => true,
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'document_type_c',
          'label' => 'LBL_DOCUMENT_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'active_date',
          'label' => 'LBL_DOC_ACTIVE_DATE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'quote_amount_c',
          'label' => 'LBL_QUOTE_AMOUNT',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'company_division_c',
          'label' => 'LBL_COMPANY_DIVISION',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'm01_sales_m01_quote_document_1_name',
          'label' => 'LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_SALES_TITLE',
          'enabled' => true,
          'id' => 'M01_SALES_M01_QUOTE_DOCUMENT_1M01_SALES_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);