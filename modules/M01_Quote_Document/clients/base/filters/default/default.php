<?php
// created: 2022-02-22 07:35:06
$viewdefs['M01_Quote_Document']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'analytical_quoted_amount_c' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'category_id' => 
    array (
    ),
    'histopathology_quoted_amount_c' => 
    array (
    ),
    'document_name' => 
    array (
    ),
    'quoteco_amount_c' => 
    array (
    ),
    'quoteco_status_c' => 
    array (
    ),
    'quoteco_date_c' => 
    array (
    ),
    'quoteco_won_date_c' => 
    array (
    ),
    'active_date' => 
    array (
    ),
    'm01_sales_m01_quote_document_1_name' => 
    array (
    ),
  ),
);