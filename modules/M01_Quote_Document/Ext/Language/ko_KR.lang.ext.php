<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Language/ko_KR.customm01_sales_activity_quote_m01_quote_document_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_ACTIVITY_QUOTE_M01_QUOTE_DOCUMENT_1_FROM_M01_SALES_ACTIVITY_QUOTE_TITLE'] = 'Sales Activity Quotes';
$mod_strings['LBL_M01_SALES_ACTIVITY_QUOTE_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE_ID'] = 'Sales Activity Quotes ID';
$mod_strings['LBL_M01_SALES_ACTIVITY_QUOTE_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'Sales Activity Quotes';

?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Language/ko_KR.customm01_sales_m01_quote_document_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE_ID'] = 'Sales ID';
$mod_strings['LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Language/ko_KR.customm01_quote_document_m01_quote_document_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_L_TITLE'] = 'SA Quote Documents';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_R_TITLE'] = 'SA Quote Documents';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_R_TITLE_ID'] = 'M01_Quote_Document ID';

?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Language/ko_KR.customm01_quote_document_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote Documents';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Language/ko_KR.customm01_quote_document_m03_work_product_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_2_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote Documents';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Language/ko_KR.customtasks_m01_quote_document_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TASKS_M01_QUOTE_DOCUMENT_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_TASKS_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE_ID'] = 'Tasks ID';
$mod_strings['LBL_TASKS_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Language/ko_KR.customaccounts_m01_quote_document_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M01_QUOTE_DOCUMENT_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE_ID'] = 'Companies ID';
$mod_strings['LBL_ACCOUNTS_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'Companies';

?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Language/ko_KR.customm01_quote_document_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M01_SALES_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote Documents';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';

?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Language/ko_KR.customtm_tradeshow_management_m01_quote_document_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_M01_QUOTE_DOCUMENT_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE'] = 'Tradeshow Management';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'Tradeshow Management';

?>
