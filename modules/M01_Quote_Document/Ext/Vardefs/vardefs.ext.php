<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/m01_sales_m01_quote_document_1_M01_Quote_Document.php

// created: 2016-01-25 22:19:12
$dictionary["M01_Quote_Document"]["fields"]["m01_sales_m01_quote_document_1"] = array (
  'name' => 'm01_sales_m01_quote_document_1',
  'type' => 'link',
  'relationship' => 'm01_sales_m01_quote_document_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE',
  'id_name' => 'm01_sales_m01_quote_document_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["M01_Quote_Document"]["fields"]["m01_sales_m01_quote_document_1_name"] = array (
  'name' => 'm01_sales_m01_quote_document_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_m01_quote_document_1m01_sales_ida',
  'link' => 'm01_sales_m01_quote_document_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["M01_Quote_Document"]["fields"]["m01_sales_m01_quote_document_1m01_sales_ida"] = array (
  'name' => 'm01_sales_m01_quote_document_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE_ID',
  'id_name' => 'm01_sales_m01_quote_document_1m01_sales_ida',
  'link' => 'm01_sales_m01_quote_document_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:55
$dictionary['M01_Quote_Document']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_description.php

 // created: 2019-04-05 11:27:44
$dictionary['M01_Quote_Document']['fields']['description']['audited']=true;
$dictionary['M01_Quote_Document']['fields']['description']['massupdate']=false;
$dictionary['M01_Quote_Document']['fields']['description']['comments']='Full text of the note';
$dictionary['M01_Quote_Document']['fields']['description']['duplicate_merge']='enabled';
$dictionary['M01_Quote_Document']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['M01_Quote_Document']['fields']['description']['merge_filter']='disabled';
$dictionary['M01_Quote_Document']['fields']['description']['unified_search']=false;
$dictionary['M01_Quote_Document']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['M01_Quote_Document']['fields']['description']['calculated']=false;
$dictionary['M01_Quote_Document']['fields']['description']['rows']='6';
$dictionary['M01_Quote_Document']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_document_name.php

 // created: 2019-04-17 11:59:13
$dictionary['M01_Quote_Document']['fields']['document_name']['required']=true;
$dictionary['M01_Quote_Document']['fields']['document_name']['audited']=true;
$dictionary['M01_Quote_Document']['fields']['document_name']['massupdate']=false;
$dictionary['M01_Quote_Document']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['M01_Quote_Document']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['M01_Quote_Document']['fields']['document_name']['merge_filter']='disabled';
$dictionary['M01_Quote_Document']['fields']['document_name']['unified_search']=false;
$dictionary['M01_Quote_Document']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['M01_Quote_Document']['fields']['document_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_category_id.php

 // created: 2019-06-25 13:00:04
$dictionary['M01_Quote_Document']['fields']['category_id']['audited']=true;
$dictionary['M01_Quote_Document']['fields']['category_id']['massupdate']=true;
$dictionary['M01_Quote_Document']['fields']['category_id']['options']='sale_document_category_list';
$dictionary['M01_Quote_Document']['fields']['category_id']['duplicate_merge']='enabled';
$dictionary['M01_Quote_Document']['fields']['category_id']['duplicate_merge_dom_value']='1';
$dictionary['M01_Quote_Document']['fields']['category_id']['merge_filter']='disabled';
$dictionary['M01_Quote_Document']['fields']['category_id']['unified_search']=false;
$dictionary['M01_Quote_Document']['fields']['category_id']['calculated']=false;
$dictionary['M01_Quote_Document']['fields']['category_id']['dependency']=false;
$dictionary['M01_Quote_Document']['fields']['category_id']['reportable']=true;

 
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_status_id.php

 // created: 2019-06-25 13:01:21
$dictionary['M01_Quote_Document']['fields']['status_id']['default']='Active';
$dictionary['M01_Quote_Document']['fields']['status_id']['audited']=true;
$dictionary['M01_Quote_Document']['fields']['status_id']['massupdate']=true;
$dictionary['M01_Quote_Document']['fields']['status_id']['duplicate_merge']='enabled';
$dictionary['M01_Quote_Document']['fields']['status_id']['duplicate_merge_dom_value']='1';
$dictionary['M01_Quote_Document']['fields']['status_id']['merge_filter']='disabled';
$dictionary['M01_Quote_Document']['fields']['status_id']['unified_search']=false;
$dictionary['M01_Quote_Document']['fields']['status_id']['calculated']=false;
$dictionary['M01_Quote_Document']['fields']['status_id']['dependency']=false;
$dictionary['M01_Quote_Document']['fields']['status_id']['options']='document_status_list';
$dictionary['M01_Quote_Document']['fields']['status_id']['required']=true;
$dictionary['M01_Quote_Document']['fields']['status_id']['reportable']=true;

 
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_currency_id.php

 // created: 2020-06-11 09:22:57

 
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2020-06-11 09:22:57

 
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_quoteco_amount_c.php

 // created: 2021-01-04 09:42:41
$dictionary['M01_Quote_Document']['fields']['quoteco_amount_c']['labelValue']='Quote/CO Amount';
$dictionary['M01_Quote_Document']['fields']['quoteco_amount_c']['enforced']='';
$dictionary['M01_Quote_Document']['fields']['quoteco_amount_c']['dependency']='isInList($category_id,createList("Study Quote","Quote Revision","Change Order","Pricelist Submission"))';
$dictionary['M01_Quote_Document']['fields']['quoteco_amount_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['M01_Quote_Document']['fields']['quoteco_amount_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_quoteco_status_c.php

 // created: 2021-01-04 09:43:55
$dictionary['M01_Quote_Document']['fields']['quoteco_status_c']['labelValue']='Quote/CO Status';
$dictionary['M01_Quote_Document']['fields']['quoteco_status_c']['dependency']='isInList($category_id,createList("Study Quote","Quote Revision","Change Order","Pricelist Submission"))';
$dictionary['M01_Quote_Document']['fields']['quoteco_status_c']['required_formula']='';
$dictionary['M01_Quote_Document']['fields']['quoteco_status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_roll_up_check_c.php

 // created: 2021-01-04 09:48:15
$dictionary['M01_Quote_Document']['fields']['roll_up_check_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Quote_Document']['fields']['roll_up_check_c']['labelValue']='Roll up check';
$dictionary['M01_Quote_Document']['fields']['roll_up_check_c']['calculated']='true';
$dictionary['M01_Quote_Document']['fields']['roll_up_check_c']['formula']='and(equal($status_id,"Active"),isInList($category_id,createList("Pricelist Submission","Study Quote","Change Order")))';
$dictionary['M01_Quote_Document']['fields']['roll_up_check_c']['enforced']='true';
$dictionary['M01_Quote_Document']['fields']['roll_up_check_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_quoteco_date_c.php

 // created: 2021-05-17 04:30:01
$dictionary['M01_Quote_Document']['fields']['quoteco_date_c']['labelValue']='Quote/CO Date';
$dictionary['M01_Quote_Document']['fields']['quoteco_date_c']['enforced']='';
$dictionary['M01_Quote_Document']['fields']['quoteco_date_c']['dependency']='isInList($category_id,createList("Study Quote","Quote Revision","Change Order","Pricelist Submission"))';
$dictionary['M01_Quote_Document']['fields']['quoteco_date_c']['required_formula']='isInList($category_id,createList("Study Quote","Quote Revision","Change Order","Pricelist Submission"))';
$dictionary['M01_Quote_Document']['fields']['quoteco_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_quoteco_won_date_c.php

 // created: 2021-05-19 05:24:04
$dictionary['M01_Quote_Document']['fields']['quoteco_won_date_c']['labelValue']='Quote/CO Won Date';
$dictionary['M01_Quote_Document']['fields']['quoteco_won_date_c']['enforced']='';
$dictionary['M01_Quote_Document']['fields']['quoteco_won_date_c']['dependency']='isInList($category_id,createList("Study Quote","Quote Revision","Change Order","Pricelist Submission"))';
$dictionary['M01_Quote_Document']['fields']['quoteco_won_date_c']['required_formula']='and(equal($quoteco_status_c,"Sponsor Approved"),isInList($category_id,createList("Study Quote","Quote Revision","Change Order","Pricelist Submission")))';
$dictionary['M01_Quote_Document']['fields']['quoteco_won_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_histopathology_quoted_amount_c.php

 // created: 2022-02-22 12:42:21
$dictionary['M01_Quote_Document']['fields']['histopathology_quoted_amount_c']['labelValue']='Histopathology Quoted Amount';
$dictionary['M01_Quote_Document']['fields']['histopathology_quoted_amount_c']['enforced']='';
$dictionary['M01_Quote_Document']['fields']['histopathology_quoted_amount_c']['dependency']='and(
equal(related($m01_sales_m01_quote_document_1,"aps_histopathology_deliv_c"),"Yes")
,isInList($category_id,createList("Change Order","Study Quote")))';
$dictionary['M01_Quote_Document']['fields']['histopathology_quoted_amount_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['M01_Quote_Document']['fields']['histopathology_quoted_amount_c']['required_formula']='';
$dictionary['M01_Quote_Document']['fields']['histopathology_quoted_amount_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Quote_Document/Ext/Vardefs/sugarfield_analytical_quoted_amount_c.php

 // created: 2022-03-03 14:15:26
$dictionary['M01_Quote_Document']['fields']['analytical_quoted_amount_c']['labelValue']='Analytical Quoted Amount';
$dictionary['M01_Quote_Document']['fields']['analytical_quoted_amount_c']['enforced']='';
$dictionary['M01_Quote_Document']['fields']['analytical_quoted_amount_c']['dependency']='and(
equal(related($m01_sales_m01_quote_document_1,"aps_analytical_deliverables_c"),"Yes")
,isInList($category_id,createList("Change Order","Study Quote")))';
$dictionary['M01_Quote_Document']['fields']['analytical_quoted_amount_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['M01_Quote_Document']['fields']['analytical_quoted_amount_c']['required_formula']='';
$dictionary['M01_Quote_Document']['fields']['analytical_quoted_amount_c']['readonly_formula']='';

 
?>
