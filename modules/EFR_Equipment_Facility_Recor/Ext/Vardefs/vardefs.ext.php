<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Vardefs/efs_equipment_facility_servi_efr_equipment_facility_recor_1_EFR_Equipment_Facility_Recor.php

// created: 2019-02-25 15:18:39
$dictionary["EFR_Equipment_Facility_Recor"]["fields"]["efs_equipment_facility_servi_efr_equipment_facility_recor_1"] = array (
  'name' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'type' => 'link',
  'relationship' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'source' => 'non-db',
  'module' => 'EFS_Equipment_Facility_Servi',
  'bean_name' => 'EFS_Equipment_Facility_Servi',
  'side' => 'right',
  'vname' => 'LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE',
  'id_name' => 'efs_equipmd3d1y_servi_ida',
  'link-type' => 'one',
);
$dictionary["EFR_Equipment_Facility_Recor"]["fields"]["efs_equipment_facility_servi_efr_equipment_facility_recor_1_name"] = array (
  'name' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE',
  'save' => true,
  'id_name' => 'efs_equipmd3d1y_servi_ida',
  'link' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'table' => 'efs_equipment_facility_servi',
  'module' => 'EFS_Equipment_Facility_Servi',
  'rname' => 'name',
);
$dictionary["EFR_Equipment_Facility_Recor"]["fields"]["efs_equipmd3d1y_servi_ida"] = array (
  'name' => 'efs_equipmd3d1y_servi_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE_ID',
  'id_name' => 'efs_equipmd3d1y_servi_ida',
  'link' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'table' => 'efs_equipment_facility_servi',
  'module' => 'EFS_Equipment_Facility_Servi',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Vardefs/equip_equipment_efr_equipment_facility_recor_2_EFR_Equipment_Facility_Recor.php

// created: 2019-02-20 15:09:02
$dictionary["EFR_Equipment_Facility_Recor"]["fields"]["equip_equipment_efr_equipment_facility_recor_2"] = array (
  'name' => 'equip_equipment_efr_equipment_facility_recor_2',
  'type' => 'link',
  'relationship' => 'equip_equipment_efr_equipment_facility_recor_2',
  'source' => 'non-db',
  'module' => 'Equip_Equipment',
  'bean_name' => 'Equip_Equipment',
  'side' => 'right',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE',
  'id_name' => 'equip_equi01e2uipment_ida',
  'link-type' => 'one',
);
$dictionary["EFR_Equipment_Facility_Recor"]["fields"]["equip_equipment_efr_equipment_facility_recor_2_name"] = array (
  'name' => 'equip_equipment_efr_equipment_facility_recor_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EQUIP_EQUIPMENT_TITLE',
  'save' => true,
  'id_name' => 'equip_equi01e2uipment_ida',
  'link' => 'equip_equipment_efr_equipment_facility_recor_2',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'name',
);
$dictionary["EFR_Equipment_Facility_Recor"]["fields"]["equip_equi01e2uipment_ida"] = array (
  'name' => 'equip_equi01e2uipment_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE_ID',
  'id_name' => 'equip_equi01e2uipment_ida',
  'link' => 'equip_equipment_efr_equipment_facility_recor_2',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Vardefs/sugarfield_service_date_c.php

 // created: 2019-02-19 17:15:25
$dictionary['EFR_Equipment_Facility_Recor']['fields']['service_date_c']['labelValue']='Service Date';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['service_date_c']['enforced']='';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['service_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Vardefs/sugarfield_software_firmware_c.php

 // created: 2019-02-19 17:28:22
$dictionary['EFR_Equipment_Facility_Recor']['fields']['software_firmware_c']['labelValue']='Software/Firmware';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['software_firmware_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['EFR_Equipment_Facility_Recor']['fields']['software_firmware_c']['enforced']='';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['software_firmware_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Vardefs/sugarfield_document_name.php

 // created: 2019-03-01 18:33:43
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['audited']=true;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['massupdate']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['merge_filter']='disabled';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['unified_search']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['calculated']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['required']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['document_name']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Vardefs/sugarfield_description.php

 // created: 2019-06-05 11:52:53
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['audited']=true;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['massupdate']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['comments']='Full text of the note';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['duplicate_merge']='enabled';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['merge_filter']='disabled';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['unified_search']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['calculated']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['rows']='6';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Vardefs/sugarfield_status_id.php

 // created: 2020-10-06 06:43:39
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['audited']=true;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['massupdate']=true;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['options']='equipment_and_facilities_status_list';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['duplicate_merge']='enabled';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['duplicate_merge_dom_value']='1';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['merge_filter']='disabled';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['unified_search']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['calculated']=false;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['dependency']='or(equal(related($equip_equipment_efr_equipment_facility_recor_2,"classification_check_c"),"1"),and(equal(related($equip_equipment_efr_equipment_facility_recor_2,"department_c"),"Facilities"),not(equal(related($equip_equipment_efr_equipment_facility_recor_2,"qualification_required_c"),"1"))),greaterThan(strlen($status_id),0))';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['required']=true;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['reportable']=true;
$dictionary['EFR_Equipment_Facility_Recor']['fields']['status_id']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Vardefs/sugarfield_type_2_c.php

 // created: 2020-10-06 11:20:21
$dictionary['EFR_Equipment_Facility_Recor']['fields']['type_2_c']['labelValue']='Category';
$dictionary['EFR_Equipment_Facility_Recor']['fields']['type_2_c']['visibility_grid']='';

 
?>
