<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Language/en_us.customequip_equipment_efr_equipment_facility_recor_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Language/en_us.customequip_equipment_efr_equipment_facility_recor_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment';

?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment';


?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SERVICE_DATE'] = 'Service Date';
$mod_strings['LBL_TYPE_2'] = 'Category';
$mod_strings['LBL_SOFTWARE_FIRMWARE'] = 'Software/Firmware';
$mod_strings['LBL_OUT_OF_SERVICE_DATE'] = 'Out of Service Date (Obsolete)';
$mod_strings['LNK_NEW_RECORD'] = 'Create Equipment &amp; Facility Record';
$mod_strings['LNK_LIST'] = 'View Equipment &amp; Facility Records';
$mod_strings['LBL_MODULE_NAME'] = 'Equipment &amp; Facility Records';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Equipment &amp; Facility Record';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Equipment &amp; Facility Record';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Equipment &amp; Facility Record vCard';
$mod_strings['LNK_IMPORT_EFR_EQUIPMENT_FACILITY_RECOR'] = 'Import Equipment &amp; Facility Records';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Equipment &amp; Facility Records List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Equipment &amp; Facility Record';
$mod_strings['LBL_EFR_EQUIPMENT_FACILITY_RECOR_SUBPANEL_TITLE'] = 'Equipment &amp; Facility Records';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Equipment &amp; Facility Records';
$mod_strings['LBL_DOC_STATUS'] = 'Status (Obsolete)';
$mod_strings['LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LBL_STATUS'] = 'StatusOld';
$mod_strings['LBL_DESCRIPTION'] = 'Notes';
$mod_strings['LBL_RECORD_BODY'] = 'Equipment &amp; Facility Record';
$mod_strings['LBL_EFR_EQUIPMENT_FACILITY_RECOR_FOCUS_DRAWER_DASHBOARD'] = 'Equipment &amp; Facility Records Focus Drawer';
$mod_strings['LBL_EFR_EQUIPMENT_FACILITY_RECOR_RECORD_DASHBOARD'] = 'Equipment &amp; Facility Records Record Dashboard';

?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Language/en_us.customefs_equipment_facility_servi_efr_equipment_facility_recor_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE'] = 'Equipment ';
$mod_strings['LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment ';

?>
<?php
// Merged from custom/Extension/modules/EFR_Equipment_Facility_Recor/Ext/Language/en_us.customequip_equipment_efr_equipment_facility_recor_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment ';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment ';

?>
