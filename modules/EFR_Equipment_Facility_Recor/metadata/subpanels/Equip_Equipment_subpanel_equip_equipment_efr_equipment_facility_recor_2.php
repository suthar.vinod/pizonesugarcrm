<?php
// created: 2020-10-06 07:09:46
$subpanel_layout['list_fields'] = array (
  'document_name' => 
  array (
    'name' => 'document_name',
    'vname' => 'LBL_LIST_DOCUMENT_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'software_firmware_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_SOFTWARE_FIRMWARE',
    'width' => 10,
    'default' => true,
  ),
  'uploadfile' => 
  array (
    'type' => 'file',
    'vname' => 'LBL_FILE_UPLOAD',
    'width' => 10,
    'default' => true,
  ),
  'service_date_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SERVICE_DATE',
    'width' => 10,
    'default' => true,
  ),
  'type_2_c' => 
  array (
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_TYPE_2',
    'width' => 10,
  ),
);