<?php
// created: 2019-05-21 19:13:06
$subpanel_layout['list_fields'] = array (
  'document_name' => 
  array (
    'name' => 'document_name',
    'vname' => 'LBL_LIST_DOCUMENT_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'uploadfile' => 
  array (
    'type' => 'file',
    'vname' => 'LBL_FILE_UPLOAD',
    'width' => 10,
    'default' => true,
  ),
  'service_date_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SERVICE_DATE',
    'width' => 10,
    'default' => true,
  ),
);