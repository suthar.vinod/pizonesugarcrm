<?php
$module_name = 'EFR_Equipment_Facility_Recor';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'document_name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'uploadfile',
              ),
              1 => 
              array (
                'name' => 'equip_equipment_efr_equipment_facility_recor_2_name',
              ),
              2 => 
              array (
                'name' => 'service_date_c',
                'label' => 'LBL_SERVICE_DATE',
              ),
              3 => 
              array (
                'name' => 'type_2_c',
                'label' => 'LBL_TYPE_2',
              ),
              4 => 
              array (
                'name' => 'software_firmware_c',
                'label' => 'LBL_SOFTWARE_FIRMWARE',
                'span' => 12,
              ),
              5 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              6 => 
              array (
              ),
              7 => 
              array (
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);
