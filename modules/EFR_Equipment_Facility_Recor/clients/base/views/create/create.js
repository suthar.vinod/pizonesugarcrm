
({
    extendsFrom: 'CreateView',

    /**
     * @inheritdoc
     */
    initialize: function (options) {
        this._super('initialize', [options]);
    },
    render: function (options) {
        this._super("render");
        this.model.set('equip_equi01e2uipment_ida', this.model.link.bean.attributes.equip_equipment_efs_equipment_facility_servi_1.id);
        this.model.set('equip_equipment_efr_equipment_facility_recor_2_name', this.model.link.bean.attributes.equip_equipment_efs_equipment_facility_servi_1.name);
    }
})
