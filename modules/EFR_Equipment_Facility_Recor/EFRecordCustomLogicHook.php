<?php

class EFRecordCustomLogicHook {

    function generateSystemId($bean, $event, $arguments) {

        global $db;
        $url = $_REQUEST[__sugar_url];
        $urlParts = explode('/', $url);
        $module = $urlParts[1];

        $isIdExist = $bean->fetched_row['id'];

        //new record
        if ($bean->id!=$bean->fetched_row['id']) {
            $ID = $bean->id;
            $year = date("Y");
            $year = substr($year, -2);
            $name = "EFR" . $year;

            $queryForUpdateName = "SELECT document_name FROM efr_equipment_facility_recor "
                    . "WHERE deleted = 0 AND document_name LIKE 'EFR%' ORDER BY efr_equipment_facility_recor.date_entered DESC  LIMIT 0,1";

            $_seqNo			= $db->query($queryForUpdateName);
            $resultSeqNo	= $db->fetchByAssoc($_seqNo);
            $nameEFR		= $resultSeqNo['document_name'];

            if (!empty($nameEFR)) {
                $nameDivisionArray = explode('-', $nameEFR);
                $number = $nameDivisionArray[1];

                //check existing year is same as most recent uploaded records's year
                if (substr($nameDivisionArray[0], -2) != $year) {
                    //new year start
                    $number = 0001;
                } else {
                    $number = $number + 1;
                }
            } else {
                $number = 0001;
            }
            $number = sprintf('%04d', $number);
            $newName = $name . '-' . $number;
			
			$query = "UPDATE efr_equipment_facility_recor SET document_name = '" . $newName . "' WHERE id = '" . $bean->id . "'";
            $db->query($query); 
        } 
		
		/*Get Related Service ID*/
		$serviceID = $this->getEquipmentFacilityServiceId($bean->id);
		if ($serviceID!="") { 
			$Equip_Equipment_Service = BeanFactory::retrieveBean('EFS_Equipment_Facility_Servi', $serviceID);
			$Equip_Equipment_Service->save();
		}

		$eeID			 = $this->getEquipmentFacilityId($bean->id); 
		if($eeID!=""){
			$Equip_Equipment = BeanFactory::retrieveBean('Equip_Equipment', $eeID);
			$Equip_Equipment->save();
		}
				
				
    }
	
	/*Function to get Parent Equipment and Facility ID*/
	function getEquipmentFacilityId($id) {
        global $db;
		$pID = "";
		$queryToGetID = "SELECT equip_equipment.id FROM equip_equipment LEFT JOIN equip_equipment_efr_equipment_facility_recor_2_c "
                . "ON equip_equipment.id = equip_equipment_efr_equipment_facility_recor_2_c.equip_equi01e2uipment_ida "
                . "LEFT JOIN efr_equipment_facility_recor ON equip_equipment_efr_equipment_facility_recor_2_c.equip_equiffd2y_recor_idb = efr_equipment_facility_recor.id "
                . "WHERE efr_equipment_facility_recor.id = '" . $id . "' AND equip_equipment_efr_equipment_facility_recor_2_c.deleted = 0 ";

        $parentID = $db->query($queryToGetID);
        if ($resultID = $db->fetchByAssoc($parentID)) {
			$pID = $resultID['id'];
		}
        return $pID;
    }

	/*Function to get Parent Equipment and Facility Service  ID*/
	function getEquipmentFacilityServiceId($EFRid) {
        global $db;
		$serviceID = "";
		$serviceQuery = "SELECT EFSC.id_c   FROM efr_equipment_facility_recor AS EFR 
						INNER JOIN efr_equipment_facility_recor_cstm AS EFRC ON EFR.id = EFRC.id_c 
						INNER JOIN efs_equipment_facility_servi_efr_equipment_facility_recor_1_c AS EFSEFR 
						ON EFRC.id_c = EFSEFR.efs_equipm4bb2y_recor_idb 
						INNER JOIN efs_equipment_facility_servi AS EFS ON EFSEFR.efs_equipmd3d1y_servi_ida = EFS.id 
						INNER JOIN efs_equipment_facility_servi_cstm  AS EFSC ON EFS.id = EFSC.id_c 
						WHERE EFS.deleted = 0  AND EFR.deleted = 0 AND EFR.id = '".$EFRid."'"; //AND EFSEFR.deleted = 0
		$getServiceID = $db->query($serviceQuery);
		if ($rowService = $db->fetchByAssoc($getServiceID)) {
			$serviceID			= $rowService['id_c']; 
		}
        return $serviceID;
    }
	
	/* After Delete the Relationship Update all related records*/
	function setServiceDate($bean, $event, $arguments) {
		global $db;
		
		/*Get Related Service ID*/
		$serviceID = $this->getEquipmentFacilityServiceId($bean->id);
		if ($serviceID!="") { 
			$Equip_Equipment_Service = BeanFactory::retrieveBean('EFS_Equipment_Facility_Servi', $serviceID);
			$Equip_Equipment_Service->save();
		}

		$eeID	 = $this->getEquipmentFacilityId($bean->id); 
		if($eeID!=""){
			$Equip_Equipment = BeanFactory::retrieveBean('Equip_Equipment', $eeID);
			$Equip_Equipment->save();
		}
	}
	 
	 
}
