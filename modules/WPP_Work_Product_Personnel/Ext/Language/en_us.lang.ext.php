<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/WPP_Work_Product_Personnel/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_RECORD'] = 'Create Work Product Person';
$mod_strings['LNK_LIST'] = 'View Work Product Personnel';
$mod_strings['LBL_MODULE_NAME'] = 'Work Product Personnel';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Work Product Person';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Work Product Person';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Work Product Person vCard';
$mod_strings['LNK_IMPORT_WPP_WORK_PRODUCT_PERSONNEL'] = 'Import Work Product Person';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Work Product Personnel  List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Work Product Person';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Work Product Personnel';
$mod_strings['LBL_WPP_WORK_PRODUCT_PERSONNEL_SUBPANEL_TITLE'] = 'Work Product Personnel';
$mod_strings['LBL_WPP_WORK_PRODUCT_PERSONNEL_FOCUS_DRAWER_DASHBOARD'] = 'Work Product Personnel Focus Drawer';
$mod_strings['LBL_WPP_WORK_PRODUCT_PERSONNEL_RECORD_DASHBOARD'] = 'Work Product Personnel Record Dashboard';

?>
<?php
// Merged from custom/Extension/modules/WPP_Work_Product_Personnel/Ext/Language/en_us.customm03_work_product_wpp_work_product_personnel_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_WPP_WORK_PRODUCT_PERSONNEL_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_WPP_WORK_PRODUCT_PERSONNEL_1_FROM_WPP_WORK_PRODUCT_PERSONNEL_TITLE_ID'] = 'Work Products ID';
$mod_strings['LBL_M03_WORK_PRODUCT_WPP_WORK_PRODUCT_PERSONNEL_1_FROM_WPP_WORK_PRODUCT_PERSONNEL_TITLE'] = 'Work Products';

?>
