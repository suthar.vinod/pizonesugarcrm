<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['ERR_DELETE_RECORD'] = 'A record number must be specified to delete the Company.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Company';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Select Company';
$mod_strings['LNK_NEW_ACCOUNT'] = 'New Company';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'New Opportunity';
$mod_strings['LNK_NEW_MEETING'] = 'Schedule APS Activity';

?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Language/en_us.customa1a_critical_phase_inspectio_activities_1_calls.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Language/en_us.customa1a_critical_phase_inspectio_activities_1_calls.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_CALLS_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE'] = 'Critical Phase Inspections';

?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_CALLS_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE'] = 'Critical Phase Inspections';


?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Language/en_us.customm01_sales_activities_1_calls.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_ACTIVITIES_1_CALLS_FROM_M01_SALES_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Language/en_us.customm03_work_product_activities_1_calls.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_CALLS_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
