<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:54
$dictionary['Call']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_calls_Calls.php

// created: 2017-09-13 15:14:51
$dictionary["Call"]["fields"]["a1a_critical_phase_inspectio_activities_1_calls"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_calls',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_calls',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_CALLS_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/m01_sales_activities_1_calls_Calls.php

// created: 2018-12-10 23:01:47
$dictionary["Call"]["fields"]["m01_sales_activities_1_calls"] = array (
  'name' => 'm01_sales_activities_1_calls',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_calls',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_CALLS_FROM_M01_SALES_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/m03_work_product_activities_1_calls_Calls.php

// created: 2019-08-05 12:02:55
$dictionary["Call"]["fields"]["m03_work_product_activities_1_calls"] = array (
  'name' => 'm03_work_product_activities_1_calls',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_calls',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_CALLS_FROM_M03_WORK_PRODUCT_TITLE',
);

?>
