<?php
// created: 2016-01-06 02:12:14
$viewdefs['Calls']['base']['menu']['quickcreate'] = array (
  'layout' => 'create',
  'label' => 'LNK_NEW_CALL',
  'visible' => true,
  'order' => 7,
  'icon' => 'fa-phone',
);