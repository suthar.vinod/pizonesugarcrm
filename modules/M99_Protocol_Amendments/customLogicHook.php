<?php
//AHTSHAM-TL - For Generate the System level Number
//Code for an auto-incrementing the �Name� field in the "Error" module?
//Naming convention will be �E18-xxxx�. This module is on the live site already.
class customLogicHook {

    function generateSystemId($bean, $event, $arguments) {
        global $db;
        $year = date("y"); ///get the year in 2 digit
        //Read Lock
        $sql_lock = "Lock TABLES m99_protocol_amendments READ";
        $bean->db->query($sql_lock);
        if($arguments['isUpdate'] == false){
        // In our Sales Activities Module, we would like to updated the automatic naming function 
        // from E18-XXXX to “APS16-XXXX�?.
        $query = 'SELECT MAX(CAST(SUBSTRING(NAME, 6, LENGTH(NAME)-5) AS UNSIGNED)) AS name_amendments FROM m99_protocol_amendments WHERE NAME LIKE "PA18%" ';
        $result = $db->query($query);
        // Unlock Table
        $sql_unlock = "UNLOCK TABLES";
        $bean->db->query($sql_unlock);
//
        if ($result->num_rows > 0) {
            $row = $db->fetchByAssoc($result);
            $system_id = $row['name_amendments'];
            if (!empty($system_id)) {
                #$number = (int) substr($system_id, -4);    
                $number = (int) $system_id;
                $number = $number + 1;
                $length = strlen($number);
                if(strlen($number)=='1'){
                    $number = '000'.$number;
                }else if(strlen($number)=='2'){
                    $number = '00'.$number;
                }else if(strlen($number)=='3'){
                    $number = '0'.$number;
                }
                $code .= 'PA18-'.$number;
            } else {
                $code = 'PA18-0001';;
            }
        } else {
            $code = 'PA18-0001';;
        }

        if ($code != "") {
            $bean->name = $code;
        }
        }else{
            $query = 'SELECT name FROM m99_protocol_amendments WHERE id = "'.$bean->fetched_row['id'].'"  ';
            $result = $db->query($query);
            // Unlock Table
            $sql_unlock = "UNLOCK TABLES";
            $bean->db->query($sql_unlock);
            //
            if ($result->num_rows > 0) {
                $row = $db->fetchByAssoc($result);
                if($row['name']==''){
                     $bean->name =  $bean->fetched_row['id'];
                }else{
                    $bean->name =  $row['name'];
                }
            }
        }
    }

}
