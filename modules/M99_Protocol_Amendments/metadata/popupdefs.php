<?php
$popupMeta = array (
    'moduleMain' => 'M99_Protocol_Amendments',
    'varName' => 'M99_Protocol_Amendments',
    'orderBy' => 'm99_protocol_amendments.name',
    'whereClauses' => array (
  'name' => 'm99_protocol_amendments.name',
),
    'searchInputs' => array (
  0 => 'm99_protocol_amendments_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
),
);
