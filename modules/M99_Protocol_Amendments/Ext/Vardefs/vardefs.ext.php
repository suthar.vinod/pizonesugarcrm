<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M99_Protocol_Amendments/Ext/Vardefs/m99_protocol_amendments_m03_work_product_M99_Protocol_Amendments.php

// created: 2018-02-05 14:21:02
$dictionary["M99_Protocol_Amendments"]["fields"]["m99_protocol_amendments_m03_work_product"] = array (
  'name' => 'm99_protocol_amendments_m03_work_product',
  'type' => 'link',
  'relationship' => 'm99_protocol_amendments_m03_work_product',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M99_PROTOCOL_AMENDMENTS_M03_WORK_PRODUCT_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm99_protocol_amendments_m03_work_productm03_work_product_idb',
);

?>
<?php
// Merged from custom/Extension/modules/M99_Protocol_Amendments/Ext/Vardefs/disabledname.php

$dictionary['M99_Protocol_Amendments']['fields']['name']['required']=false;
$dictionary['M99_Protocol_Amendments']['fields']['name']['len']='255';
$dictionary['M99_Protocol_Amendments']['fields']['name']['audited']=false;
$dictionary['M99_Protocol_Amendments']['fields']['name']['massupdate']=false;
$dictionary['M99_Protocol_Amendments']['fields']['name']['unified_search']=false;
$dictionary['M99_Protocol_Amendments']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['M99_Protocol_Amendments']['fields']['name']['calculated']='true';
$dictionary['M99_Protocol_Amendments']['fields']['name']['importable']='false';
$dictionary['M99_Protocol_Amendments']['fields']['name']['duplicate_merge']='disabled';
$dictionary['M99_Protocol_Amendments']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['M99_Protocol_Amendments']['fields']['name']['merge_filter']='disabled';
$dictionary['M99_Protocol_Amendments']['fields']['name']['formula']='$animal_id_c';
$dictionary['M99_Protocol_Amendments']['fields']['name']['enforced']=true;

?>
