<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CF_CAPA_Files/Ext/Vardefs/capa_capa_cf_capa_files_1_CF_CAPA_Files.php

// created: 2022-02-03 07:26:23
$dictionary["CF_CAPA_Files"]["fields"]["capa_capa_cf_capa_files_1"] = array (
  'name' => 'capa_capa_cf_capa_files_1',
  'type' => 'link',
  'relationship' => 'capa_capa_cf_capa_files_1',
  'source' => 'non-db',
  'module' => 'CAPA_CAPA',
  'bean_name' => 'CAPA_CAPA',
  'side' => 'right',
  'vname' => 'LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CF_CAPA_FILES_TITLE',
  'id_name' => 'capa_capa_cf_capa_files_1capa_capa_ida',
  'link-type' => 'one',
);
$dictionary["CF_CAPA_Files"]["fields"]["capa_capa_cf_capa_files_1_name"] = array (
  'name' => 'capa_capa_cf_capa_files_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CAPA_CAPA_TITLE',
  'save' => true,
  'id_name' => 'capa_capa_cf_capa_files_1capa_capa_ida',
  'link' => 'capa_capa_cf_capa_files_1',
  'table' => 'capa_capa',
  'module' => 'CAPA_CAPA',
  'rname' => 'name',
);
$dictionary["CF_CAPA_Files"]["fields"]["capa_capa_cf_capa_files_1capa_capa_ida"] = array (
  'name' => 'capa_capa_cf_capa_files_1capa_capa_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CF_CAPA_FILES_TITLE_ID',
  'id_name' => 'capa_capa_cf_capa_files_1capa_capa_ida',
  'link' => 'capa_capa_cf_capa_files_1',
  'table' => 'capa_capa',
  'module' => 'CAPA_CAPA',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/CF_CAPA_Files/Ext/Vardefs/sugarfield_capa_capa_cf_capa_files_1capa_capa_ida.php

 // created: 2022-02-03 07:41:23
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['name']='capa_capa_cf_capa_files_1capa_capa_ida';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['type']='id';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['source']='non-db';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['vname']='LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CF_CAPA_FILES_TITLE_ID';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['id_name']='capa_capa_cf_capa_files_1capa_capa_ida';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['link']='capa_capa_cf_capa_files_1';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['table']='capa_capa';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['module']='CAPA_CAPA';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['rname']='id';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['reportable']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['side']='right';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['massupdate']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['duplicate_merge']='disabled';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['hideacl']=true;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['audited']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1capa_capa_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/CF_CAPA_Files/Ext/Vardefs/sugarfield_capa_capa_cf_capa_files_1_name.php

 // created: 2022-02-03 07:41:25
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['required']=true;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['audited']=true;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['massupdate']=true;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['hidemassupdate']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['duplicate_merge']='enabled';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['duplicate_merge_dom_value']='1';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['merge_filter']='disabled';
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['reportable']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['unified_search']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['calculated']=false;
$dictionary['CF_CAPA_Files']['fields']['capa_capa_cf_capa_files_1_name']['vname']='LBL_CAPA_CAPA_CF_CAPA_FILES_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/CF_CAPA_Files/Ext/Vardefs/sugarfield_document_name.php

 // created: 2022-02-03 09:02:49
$dictionary['CF_CAPA_Files']['fields']['document_name']['unified_search']=false;

 
?>
