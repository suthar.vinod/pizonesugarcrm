<?php
// created: 2021-12-01 12:33:39
$viewdefs['CF_CAPA_Files']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'assigned_user_name' => 
    array (
    ),
    'capa_capa_cf_capa_files_1_name' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'document_name' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'description' => 
    array (
    ),
  ),
);