<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/m06_APS_Communication/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:57
$dictionary['m06_APS_Communication']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/m06_APS_Communication/Ext/Vardefs/sugarfield_communication_date_c.php

 // created: 2016-10-25 03:27:11
$dictionary['m06_APS_Communication']['fields']['communication_date_c']['labelValue'] = 'Communication Date';
$dictionary['m06_APS_Communication']['fields']['communication_date_c']['enforced'] = '';
$dictionary['m06_APS_Communication']['fields']['communication_date_c']['dependency'] = '';
$dictionary['m06_APS_Communication']['fields']['communication_date_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/m06_APS_Communication/Ext/Vardefs/sugarfield_name.php

 // created: 2016-10-25 03:27:11
$dictionary['m06_APS_Communication']['fields']['name']['len'] = '255';
$dictionary['m06_APS_Communication']['fields']['name']['audited'] = false;
$dictionary['m06_APS_Communication']['fields']['name']['massupdate'] = false;
$dictionary['m06_APS_Communication']['fields']['name']['unified_search'] = false;
$dictionary['m06_APS_Communication']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['m06_APS_Communication']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['m06_APS_Communication']['fields']['name']['full_text_search']['boost'] = 1.5500000000000000444089209850062616169452667236328125;
$dictionary['m06_APS_Communication']['fields']['name']['calculated'] = false;


?>
