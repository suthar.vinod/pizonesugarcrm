<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/m06_APS_Communication/Ext/Language/en_us.customm01_sales_m06_aps_communication_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_M06_APS_COMMUNICATION_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_M06_APS_COMMUNICATION_1_FROM_M06_APS_COMMUNICATION_TITLE_ID'] = 'Sales ID';
$mod_strings['LBL_M01_SALES_M06_APS_COMMUNICATION_1_FROM_M06_APS_COMMUNICATION_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/m06_APS_Communication/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_COMMUNICATION_DATE'] = 'Communication Date';
$mod_strings['LBL_NAME'] = 'System ID (Communication)';
$mod_strings['LBL_ASSIGNED_TO'] = 'Communication Owner';
$mod_strings['LNK_NEW_RECORD'] = 'Create APS Communication';
$mod_strings['LNK_LIST'] = 'View APS Communications';
$mod_strings['LBL_MODULE_NAME'] = 'APS Communications';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = '(Inactive) APS Communication';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New (Inactive) APS Communication';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import (Inactive) APS Communication vCard';
$mod_strings['LNK_IMPORT_M06_APS_COMMUNICATION'] = 'Import APS Communication';
$mod_strings['LBL_LIST_FORM_TITLE'] = '(Inactive) APS Communications List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search (Inactive) APS Communication';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My APS Communications';
$mod_strings['LBL_M06_APS_COMMUNICATION_FOCUS_DRAWER_DASHBOARD'] = 'APS Communications Focus Drawer';
$mod_strings['LBL_M06_APS_COMMUNICATION_RECORD_DASHBOARD'] = 'APS Communications Record Dashboard';

?>
<?php
// Merged from custom/Extension/modules/m06_APS_Communication/Ext/Language/en_us.customm03_work_product_m06_aps_communication_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M06_APS_COMMUNICATION_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M06_APS_COMMUNICATION_1_FROM_M06_APS_COMMUNICATION_TITLE_ID'] = 'Work Products ID';
$mod_strings['LBL_M03_WORK_PRODUCT_M06_APS_COMMUNICATION_1_FROM_M06_APS_COMMUNICATION_TITLE'] = 'Work Products';

?>
