<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Vardefs/sugarfield_name.php

 // created: 2019-07-03 12:06:49
$dictionary['CA_Company_Address']['fields']['name']['len']='255';
$dictionary['CA_Company_Address']['fields']['name']['required']=false;
$dictionary['CA_Company_Address']['fields']['name']['audited']=true;
$dictionary['CA_Company_Address']['fields']['name']['massupdate']=false;
$dictionary['CA_Company_Address']['fields']['name']['unified_search']=false;
$dictionary['CA_Company_Address']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['CA_Company_Address']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Vardefs/sugarfield_type_2_c.php

 // created: 2019-07-03 12:08:01
$dictionary['CA_Company_Address']['fields']['type_2_c']['labelValue']='Type';
$dictionary['CA_Company_Address']['fields']['type_2_c']['dependency']='';
$dictionary['CA_Company_Address']['fields']['type_2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Vardefs/accounts_ca_company_address_1_CA_Company_Address.php

// created: 2019-07-03 12:14:01
$dictionary["CA_Company_Address"]["fields"]["accounts_ca_company_address_1"] = array (
  'name' => 'accounts_ca_company_address_1',
  'type' => 'link',
  'relationship' => 'accounts_ca_company_address_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_CA_COMPANY_ADDRESS_TITLE',
  'id_name' => 'accounts_ca_company_address_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["CA_Company_Address"]["fields"]["accounts_ca_company_address_1_name"] = array (
  'name' => 'accounts_ca_company_address_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_ca_company_address_1accounts_ida',
  'link' => 'accounts_ca_company_address_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["CA_Company_Address"]["fields"]["accounts_ca_company_address_1accounts_ida"] = array (
  'name' => 'accounts_ca_company_address_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_CA_COMPANY_ADDRESS_TITLE_ID',
  'id_name' => 'accounts_ca_company_address_1accounts_ida',
  'link' => 'accounts_ca_company_address_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Vardefs/sugarfield_address_postalcode_c.php

 // created: 2019-07-22 12:05:11
$dictionary['CA_Company_Address']['fields']['address_postalcode_c']['labelValue']='PostalCode';
$dictionary['CA_Company_Address']['fields']['address_postalcode_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CA_Company_Address']['fields']['address_postalcode_c']['group']='address_c';
$dictionary['CA_Company_Address']['fields']['address_postalcode_c']['enforced']='';
$dictionary['CA_Company_Address']['fields']['address_postalcode_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Vardefs/sugarfield_address_state_c.php

 // created: 2019-07-22 12:06:08
$dictionary['CA_Company_Address']['fields']['address_state_c']['labelValue']='State/Providence/District';
$dictionary['CA_Company_Address']['fields']['address_state_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CA_Company_Address']['fields']['address_state_c']['group']='address_c';
$dictionary['CA_Company_Address']['fields']['address_state_c']['enforced']='';
$dictionary['CA_Company_Address']['fields']['address_state_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Vardefs/sugarfield_address_suite_floor_c.php

 // created: 2019-07-22 12:07:03
$dictionary['CA_Company_Address']['fields']['address_suite_floor_c']['labelValue']='Suite/Floor';
$dictionary['CA_Company_Address']['fields']['address_suite_floor_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CA_Company_Address']['fields']['address_suite_floor_c']['enforced']='';
$dictionary['CA_Company_Address']['fields']['address_suite_floor_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Vardefs/sugarfield_address_building_c.php

 // created: 2019-07-22 12:07:43
$dictionary['CA_Company_Address']['fields']['address_building_c']['labelValue']='Building';
$dictionary['CA_Company_Address']['fields']['address_building_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CA_Company_Address']['fields']['address_building_c']['enforced']='';
$dictionary['CA_Company_Address']['fields']['address_building_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Vardefs/sugarfield_address_street_c.php

 // created: 2019-07-22 12:08:21
$dictionary['CA_Company_Address']['fields']['address_street_c']['labelValue']='Street';
$dictionary['CA_Company_Address']['fields']['address_street_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CA_Company_Address']['fields']['address_street_c']['group']='address_c';
$dictionary['CA_Company_Address']['fields']['address_street_c']['enforced']='';
$dictionary['CA_Company_Address']['fields']['address_street_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Vardefs/sugarfield_address_city_c.php

 // created: 2019-07-22 12:08:59
$dictionary['CA_Company_Address']['fields']['address_city_c']['labelValue']='City';
$dictionary['CA_Company_Address']['fields']['address_city_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CA_Company_Address']['fields']['address_city_c']['group']='address_c';
$dictionary['CA_Company_Address']['fields']['address_city_c']['enforced']='';
$dictionary['CA_Company_Address']['fields']['address_city_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Vardefs/sugarfield_address_country_c.php

 // created: 2019-07-22 12:09:37
$dictionary['CA_Company_Address']['fields']['address_country_c']['labelValue']='Country';
$dictionary['CA_Company_Address']['fields']['address_country_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CA_Company_Address']['fields']['address_country_c']['group']='address_c';
$dictionary['CA_Company_Address']['fields']['address_country_c']['enforced']='';
$dictionary['CA_Company_Address']['fields']['address_country_c']['dependency']='';

 
?>
