<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Language/en_us.customaccounts_ca_company_address_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Language/en_us.customaccounts_ca_company_address_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_CA_COMPANY_ADDRESS_TITLE'] = 'Companies';

?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_CA_COMPANY_ADDRESS_TITLE'] = 'Companies';


?>
<?php
// Merged from custom/Extension/modules/CA_Company_Address/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TYPE_2'] = 'Type';
$mod_strings['LBL_ADDRESS'] = 'Address';
$mod_strings['LBL_ADDRESS_STREET'] = 'Street';
$mod_strings['LBL_ADDRESS_CITY'] = 'City';
$mod_strings['LBL_ADDRESS_STATE'] = 'State/Providence/District';
$mod_strings['LBL_ADDRESS_POSTALCODE'] = 'PostalCode';
$mod_strings['LBL_ADDRESS_COUNTRY'] = 'Country';
$mod_strings['LBL_ADDRESS_SUITE_FLOOR'] = 'Suite/Floor';
$mod_strings['LBL_ADDRESS_BUILDING'] = 'Building';
$mod_strings['LBL_RECORD_BODY'] = 'Company Address';
$mod_strings['LBL_CA_COMPANY_ADDRESS_FOCUS_DRAWER_DASHBOARD'] = 'Company Addresses Focus Drawer';
$mod_strings['LBL_CA_COMPANY_ADDRESS_RECORD_DASHBOARD'] = 'Company Addresses Record Dashboard';

?>
