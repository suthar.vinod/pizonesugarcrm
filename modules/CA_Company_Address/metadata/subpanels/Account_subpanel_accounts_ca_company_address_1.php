<?php
// created: 2019-07-03 12:17:33
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'type_2_c' => 
  array (
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_TYPE_2',
    'width' => 10,
  ),
);