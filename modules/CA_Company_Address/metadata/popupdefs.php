<?php
$popupMeta = array (
    'moduleMain' => 'CA_Company_Address',
    'varName' => 'CA_Company_Address',
    'orderBy' => 'ca_company_address.name',
    'whereClauses' => array (
  'name' => 'ca_company_address.name',
  'accounts_ca_company_address_1_name' => 'ca_company_address.accounts_ca_company_address_1_name',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'accounts_ca_company_address_1_name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'accounts_ca_company_address_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_CA_COMPANY_ADDRESS_1ACCOUNTS_IDA',
    'width' => 10,
    'name' => 'accounts_ca_company_address_1_name',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'ACCOUNTS_CA_COMPANY_ADDRESS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_CA_COMPANY_ADDRESS_1ACCOUNTS_IDA',
    'width' => 10,
    'default' => true,
    'name' => 'accounts_ca_company_address_1_name',
  ),
),
);
