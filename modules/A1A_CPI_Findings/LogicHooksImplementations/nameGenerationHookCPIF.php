<?php

class HookBeforeSaveForNameGenerationCPIF {

    public function generateName($bean, $event, $arguments) {
        $tableName = 'a1a_cpi_findings';
        $prefix = ' F';
        $fieldName = 'name';
        $relModuleName = 'A1A_Critical_Phase_Inspectio';
        $relModuleIdField = 'a1a_criticbf09spectio_ida';

        //  Get other field's value.
        $prefix2 = $bean->a1a_critical_phase_inspectio_a1a_cpi_findings_1_name;
        if (empty($prefix2) || ($bean->$relModuleIdField != $bean->fetched_row[$relModuleIdField])) {
            $relBean = BeanFactory::getBean($relModuleName, $bean->$relModuleIdField, array('disable_row_level_security' => true));
            $relBean->retrieve($bean->$relModuleIdField);
            $prefix2 = $relBean->name;
        }

        $prefix = $prefix2 . $prefix;

        $code = $this->_getCurrentId($tableName, $prefix, $fieldName);

        if (empty($bean->fetched_row[$fieldName]) || ($bean->fetched_row[$fieldName] != $bean->$fieldName) || ($code != $bean->$fieldName)) {
            $bean->$fieldName = $code;
        }
    }

    private function _getCurrentId($tableName, $prefix, $fieldName) {
        global $db;
        //  Lock Table
        $sql_lock = "Lock TABLES {$tableName} READ";
        $db->query($sql_lock);

        //  In our Sales Activities Module, we would like to updated the automatic naming function from CP{YY}-XXXX.
        $query = "SELECT substring({$fieldName},-4) AS {$fieldName} From {$tableName} "
                . "WHERE {$fieldName} LIKE '%{$prefix}%' "
                . "ORDER BY $fieldName DESC LIMIT 0,1";
        $result = $db->query($query);
//        $GLOBALS['log']->fatal("the Query = " . $query);
        //  Unlock Table
        $sql_unlock = "UNLOCK TABLES";
        $db->query($sql_unlock);

        $row = $db->fetchByAssoc($result);

        $system_id = $row[$fieldName];
//        $GLOBALS['log']->fatal("result = " . $system_id);
        if (!empty($system_id)) {

            $number = (int) $system_id;
            $number = $number + 1;
//            $GLOBALS['log']->fatal("nmber = " . $number);
            $length = strlen($number);
            $allowed_length = 4;

            $left_length = $allowed_length - $length;
            $code = $prefix;

            for ($i = 0; $i < $left_length; $i++) {
                $code .= "0";
            }
            $code .= $number;
        } else {
            $code = $prefix . "0001";
        }

        return $code;
    }

}
