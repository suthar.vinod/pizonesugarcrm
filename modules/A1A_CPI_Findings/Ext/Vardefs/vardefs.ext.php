<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/A1A_CPI_Findings/Ext/Vardefs/a1a_critical_phase_inspectio_a1a_cpi_findings_1_A1A_CPI_Findings.php

// created: 2017-06-07 19:03:44
$dictionary["A1A_CPI_Findings"]["fields"]["a1a_critical_phase_inspectio_a1a_cpi_findings_1"] = array (
  'name' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'side' => 'right',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CPI_FINDINGS_TITLE',
  'id_name' => 'a1a_criticbf09spectio_ida',
  'link-type' => 'one',
);
$dictionary["A1A_CPI_Findings"]["fields"]["a1a_critical_phase_inspectio_a1a_cpi_findings_1_name"] = array (
  'name' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'save' => true,
  'id_name' => 'a1a_criticbf09spectio_ida',
  'link' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'table' => 'a1a_critical_phase_inspectio',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'rname' => 'name',
);
$dictionary["A1A_CPI_Findings"]["fields"]["a1a_criticbf09spectio_ida"] = array (
  'name' => 'a1a_criticbf09spectio_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CPI_FINDINGS_TITLE_ID',
  'id_name' => 'a1a_criticbf09spectio_ida',
  'link' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'table' => 'a1a_critical_phase_inspectio',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/A1A_CPI_Findings/Ext/Vardefs/nameFieldCustomization.php


$dictionary['A1A_CPI_Findings']['fields']['name']['required'] = 'false';
$dictionary['A1A_CPI_Findings']['fields']['name']['readonly'] = 'true';

$dictionary['A1A_CPI_Findings']['fields']['a1a_critical_phase_inspectio_a1a_cpi_findings_1_name']['required'] = 'true';

?>
<?php
// Merged from custom/Extension/modules/A1A_CPI_Findings/Ext/Vardefs/sugarfield_name.php

 // created: 2019-08-26 17:07:15
$dictionary['A1A_CPI_Findings']['fields']['name']['len']='255';
$dictionary['A1A_CPI_Findings']['fields']['name']['required']=false;
$dictionary['A1A_CPI_Findings']['fields']['name']['audited']=true;
$dictionary['A1A_CPI_Findings']['fields']['name']['massupdate']=false;
$dictionary['A1A_CPI_Findings']['fields']['name']['unified_search']=false;
$dictionary['A1A_CPI_Findings']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['A1A_CPI_Findings']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/A1A_CPI_Findings/Ext/Vardefs/sugarfield_cpi_finding_category.php

 // created: 2019-08-26 17:08:45
$dictionary['A1A_CPI_Findings']['fields']['cpi_finding_category']['default']='';
$dictionary['A1A_CPI_Findings']['fields']['cpi_finding_category']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/A1A_CPI_Findings/Ext/Vardefs/sugarfield_cpi_finding_description.php

 // created: 2019-08-26 17:09:57
$dictionary['A1A_CPI_Findings']['fields']['cpi_finding_description']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/A1A_CPI_Findings/Ext/Vardefs/sugarfield_cpi_recommended_action.php

 // created: 2019-08-26 17:11:35
$dictionary['A1A_CPI_Findings']['fields']['cpi_recommended_action']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/A1A_CPI_Findings/Ext/Vardefs/sugarfield_cpi_findings_description_ta_c.php

 // created: 2019-08-26 17:12:47
$dictionary['A1A_CPI_Findings']['fields']['cpi_findings_description_ta_c']['labelValue']='CPI Findings Description';
$dictionary['A1A_CPI_Findings']['fields']['cpi_findings_description_ta_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['A1A_CPI_Findings']['fields']['cpi_findings_description_ta_c']['enforced']='';
$dictionary['A1A_CPI_Findings']['fields']['cpi_findings_description_ta_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/A1A_CPI_Findings/Ext/Vardefs/sugarfield_department_c.php

 // created: 2019-08-26 17:14:17
$dictionary['A1A_CPI_Findings']['fields']['department_c']['labelValue']='Department';
$dictionary['A1A_CPI_Findings']['fields']['department_c']['dependency']='';
$dictionary['A1A_CPI_Findings']['fields']['department_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/A1A_CPI_Findings/Ext/Vardefs/sugarfield_resolution_c.php

 // created: 2019-08-26 17:15:06
$dictionary['A1A_CPI_Findings']['fields']['resolution_c']['labelValue']='Resolution';
$dictionary['A1A_CPI_Findings']['fields']['resolution_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['A1A_CPI_Findings']['fields']['resolution_c']['enforced']='';
$dictionary['A1A_CPI_Findings']['fields']['resolution_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/A1A_CPI_Findings/Ext/Vardefs/sugarfield_cpi_findings_recommended_act_c.php

 // created: 2019-08-26 17:16:20
$dictionary['A1A_CPI_Findings']['fields']['cpi_findings_recommended_act_c']['labelValue']='CPI Findings Recommended Action';
$dictionary['A1A_CPI_Findings']['fields']['cpi_findings_recommended_act_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['A1A_CPI_Findings']['fields']['cpi_findings_recommended_act_c']['enforced']='';
$dictionary['A1A_CPI_Findings']['fields']['cpi_findings_recommended_act_c']['dependency']='';

 
?>
