<?php
$module_name = 'A1A_CPI_Findings';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
                'width' => 'xsmall',
              ),
              1 => 
              array (
                'name' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1_name',
                'label' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
                'enabled' => true,
                'id' => 'A1A_CRITICBF09SPECTIO_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
                'width' => 'xsmall',
              ),
              2 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
                'width' => 'xsmall',
              ),
              3 => 
              array (
                'name' => 'cpi_findings_description_ta_c',
                'label' => 'LBL_CPI_FINDINGS_DESCRIPTION_TA',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'cpi_findings_recommended_act_c',
                'label' => 'LBL_CPI_FINDINGS_RECOMMENDED_ACT',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'resolution_c',
                'label' => 'LBL_RESOLUTION',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => false,
              ),
              7 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => false,
              ),
              8 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
