<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class sendNotificationEmail {
	function sendNotificationRecord($bean, $event, $arguments) {

		//global $db,$current_user; 
		global $db,$sugar_config,$current_user; 
		global $app_list_strings;			
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$newEmailArr = $bean->email;
		$oldEmailArr = $bean->fetched_row['email'];
		$emailDifference = array();

		$newEmailCount = count($newEmailArr);
		$oldEmailCount = count($oldEmailArr);

		if ($newEmailCount >= $oldEmailCount)
			$maxcount = $newEmailCount;
		else
			$maxcount = $oldEmailCount;	
		
		for($cnt=0;$cnt<$maxcount;$cnt++){

			$newEmail  = ($newEmailArr[$cnt]['email_address']!="")?$newEmailArr[$cnt]['email_address']:"Null";
			$oldEmail  = ($oldEmailArr[$cnt]['email_address']!="")?$oldEmailArr[$cnt]['email_address']:"Null";
			if($newEmail!=$oldEmail){
				$emailDifference[] = $oldEmail." to ".$newEmail."; " ;
			}

		} 
        if ($bean->id == $bean->fetched_row['id'] && 
			$bean->portal_account_activated_c == "Yes" &&
			( count($emailDifference)>0 
			|| ($bean->phone_work!=$bean->fetched_row['phone_work']) 
			|| ($bean->phone_mobile!=$bean->fetched_row['phone_mobile']) 
			|| ($bean->ca_company_address_id1_c!=$bean->fetched_row['ca_company_address_id1_c']))) {
				
				$contactData = "";

				$oldPhoneWork	= ($bean->fetched_row['phone_work']!="")?$bean->fetched_row['phone_work']:"Null";
				$oldPhoneMobile = ($bean->fetched_row['phone_mobile']!="")?$bean->fetched_row['phone_mobile']:"Null";
				$newPhoneWork  	= ($bean->phone_work!="")?$bean->phone_work:"Null";
				$newPhoneMobile = ($bean->phone_mobile!="")?$bean->phone_mobile:"Null";
				
				if($bean->ca_company_address_id1_c!=$bean->fetched_row['ca_company_address_id1_c']){

					$newAddressID = $bean->ca_company_address_id1_c;
					$oldAddressID = $bean->fetched_row['ca_company_address_id1_c'];

					$newAddress   = "Null";
					$oldAddress   = "Null";

					if($newAddressID!="")
					{
						$newAddressBean = BeanFactory::getBean('CA_Company_Address', $newAddressID);
						$newAddress 	= $newAddressBean->name;
					}	
					if($oldAddressID!="")
					{
						$oldAddressBean = BeanFactory::getBean('CA_Company_Address', $oldAddressID);
						$oldAddress 	= $oldAddressBean->name;
					}
					$contactData .= "Full Address: ".$oldAddress." to ".$newAddress."<br/>";
				}
				
				if($bean->phone_work!=$bean->fetched_row['phone_work']){
					$contactData .= "Primary Office Phone: ".$oldPhoneWork." to ".$newPhoneWork." <br/>";
				}

				if($bean->phone_mobile!=$bean->fetched_row['phone_mobile']){
					$contactData .= "Mobile Phone: ".$oldPhoneMobile." to ".$newPhoneMobile."<br/>";
				}

				if(count($emailDifference)>0){
					$contactData .= "Email Address: ".implode(" ",$emailDifference)."<br/>";
				}


				$contactID = $bean->id; 
				$contactName = $bean->name; 

				$template = new EmailTemplate();
				$emailObj = new Email();
				$template->retrieve_by_string_fields(array('name' => 'Contact Information Update Notification', 'type' => 'email'));
				
				$contactLink = '<a target="_blank" href="'.$site_url.'/#Contacts/'.$contactID.'">'.$contactName.'</a>';
					 
				$template->body_html = str_replace('[Contact_name]', $contactLink, $template->body_html);
				$template->body_html = str_replace('[Contact_data]', $contactData, $template->body_html);
				
				 
 					$defaults	= $emailObj->getSystemDefaultEmail();
					$mail		= new SugarPHPMailer();
					$mail->setMailerForSystem();
					$mail->IsHTML(true);
					$mail->From		= $defaults['email'];
					$mail->FromName = $defaults['name'];
					$mail->Subject	= $template->subject;
					$mail->Body		= $template->body_html;
							 
					 
					$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';
					//$mail->AddAddress('fxs_mjohnson@apsemail.com');
					$mail->AddAddress('labservicesspecialists@apsemail.com');
									 
					//If their exist some valid email addresses then send email
					if (!empty($wp_emailAdd_dev)) {
						if (!$mail->Send()) {
							$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
						} else {
							$GLOBALS['log']->debug('email sent');
						}
						unset($mail);
					}
				}
    }
}
 