<?php
// created: 2022-12-13 04:43:54
$viewdefs['Contacts']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'email',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'phone_mobile',
          'label' => 'LBL_MOBILE_PHONE',
          'enabled' => true,
          'default' => true,
          'width' => 'small',
        ),
        3 => 
        array (
          'name' => 'phone_work',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
);