<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:54
$dictionary['Contact']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_industry_c.php

 // created: 2017-10-05 01:59:56

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_location_c.php

 // created: 2017-10-05 01:59:56

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_description_c.php

 // created: 2017-10-05 01:59:56

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_size_c.php

 // created: 2017-10-05 01:59:56

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_founded_year_c.php

 // created: 2017-10-05 01:59:57

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_job_2_c.php

 // created: 2017-10-05 01:59:57

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_twitter_c.php

 // created: 2017-10-05 01:59:57

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_facebook_c.php

 // created: 2017-10-05 01:59:57

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_education_2_c.php

 // created: 2017-10-05 01:59:57

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_education_c.php

 // created: 2017-10-05 01:59:57

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_facebook_handle_c.php

 // created: 2017-10-05 01:59:57

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_industry_tags_c.php

 // created: 2017-10-05 01:59:57

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_twitter_handle_c.php

 // created: 2017-10-05 01:59:58

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_logo_c.php

 // created: 2017-10-05 01:59:58

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_website_c.php

 // created: 2017-10-05 01:59:58

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_photo_c.php

 // created: 2017-10-05 01:59:58

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_sic_code_label_c.php

 // created: 2017-10-05 01:59:59

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_naics_code_label_c.php

 // created: 2017-10-05 01:59:58

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_fiscal_year_end_c.php

 // created: 2017-10-05 02:00:00

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_hint_account_annual_revenue_c.php

 // created: 2017-10-05 02:00:00

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/is_send_email.php


$dictionary['Contact']['fields']['is_send_email'] = array(
    'required' => false,
    'studio' => false,
    'name' => 'is_send_email',
    'vname' => 'Send Email',
    'type' => 'bool',
    'massupdate' => '0',
    'default' => '0',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/contacts_ta_tradeshow_activities_1_Contacts.php

// created: 2018-04-23 17:07:52
$dictionary["Contact"]["fields"]["contacts_ta_tradeshow_activities_1"] = array (
  'name' => 'contacts_ta_tradeshow_activities_1',
  'type' => 'link',
  'relationship' => 'contacts_ta_tradeshow_activities_1',
  'source' => 'non-db',
  'module' => 'TA_Tradeshow_Activities',
  'bean_name' => 'TA_Tradeshow_Activities',
  'vname' => 'LBL_CONTACTS_TA_TRADESHOW_ACTIVITIES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_ta_tradeshow_activities_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_last_name.php

 // created: 2018-09-13 20:36:29
$dictionary['Contact']['fields']['last_name']['massupdate']=false;
$dictionary['Contact']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Contact']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['last_name']['duplicate_merge_dom_value']='2';
$dictionary['Contact']['fields']['last_name']['merge_filter']='enabled';
$dictionary['Contact']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.97',
  'searchable' => true,
);
$dictionary['Contact']['fields']['last_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/contacts_m01_sales_1_Contacts.php

// created: 2018-12-13 16:29:00
$dictionary["Contact"]["fields"]["contacts_m01_sales_1"] = array (
  'name' => 'contacts_m01_sales_1',
  'type' => 'link',
  'relationship' => 'contacts_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_CONTACTS_M01_SALES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_m01_sales_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/contacts_m03_work_product_1_Contacts.php

// created: 2018-12-21 23:14:19
$dictionary["Contact"]["fields"]["contacts_m03_work_product_1"] = array (
  'name' => 'contacts_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'contacts_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_m03_work_product_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/contacts_de_deviation_employees_1_Contacts.php

// created: 2019-02-20 19:46:48
$dictionary["Contact"]["fields"]["contacts_de_deviation_employees_1"] = array (
  'name' => 'contacts_de_deviation_employees_1',
  'type' => 'link',
  'relationship' => 'contacts_de_deviation_employees_1',
  'source' => 'non-db',
  'module' => 'DE_Deviation_Employees',
  'bean_name' => 'DE_Deviation_Employees',
  'vname' => 'LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_de_deviation_employees_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_contact_id_c.php

 // created: 2019-02-20 19:40:45

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_email.php

 // created: 2019-02-20 19:26:28
$dictionary['Contact']['fields']['email']['len']='100';
$dictionary['Contact']['fields']['email']['massupdate']=true;
$dictionary['Contact']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['email']['duplicate_merge_dom_value']='2';
$dictionary['Contact']['fields']['email']['merge_filter']='enabled';
$dictionary['Contact']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.0',
  'searchable' => true,
);
$dictionary['Contact']['fields']['email']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_initials_c.php

 // created: 2019-02-20 19:40:06
$dictionary['Contact']['fields']['initials_c']['labelValue']='Initials';
$dictionary['Contact']['fields']['initials_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['initials_c']['enforced']='';
$dictionary['Contact']['fields']['initials_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_manager_c.php

 // created: 2019-02-20 19:40:45
$dictionary['Contact']['fields']['manager_c']['labelValue']='Manager';
$dictionary['Contact']['fields']['manager_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_mobile.php

 // created: 2019-02-20 19:28:15
$dictionary['Contact']['fields']['phone_mobile']['len']='100';
$dictionary['Contact']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Contact']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.0',
  'searchable' => true,
);
$dictionary['Contact']['fields']['phone_mobile']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_work.php

 // created: 2019-02-20 19:27:15
$dictionary['Contact']['fields']['phone_work']['len']='100';
$dictionary['Contact']['fields']['phone_work']['massupdate']=false;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['phone_work']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_work']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.0',
  'searchable' => true,
);
$dictionary['Contact']['fields']['phone_work']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_current_title_c.php

 // created: 2019-06-24 16:19:57
$dictionary['Contact']['fields']['current_title_c']['labelValue']='Title';
$dictionary['Contact']['fields']['current_title_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['current_title_c']['enforced']='';
$dictionary['Contact']['fields']['current_title_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_notes_c.php

 // created: 2019-06-24 16:22:53
$dictionary['Contact']['fields']['notes_c']['labelValue']='Notes';
$dictionary['Contact']['fields']['notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['notes_c']['enforced']='';
$dictionary['Contact']['fields']['notes_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/contacts_maj_contact_documents_1_Contacts.php

// created: 2019-08-15 11:34:01
$dictionary["Contact"]["fields"]["contacts_maj_contact_documents_1"] = array (
  'name' => 'contacts_maj_contact_documents_1',
  'type' => 'link',
  'relationship' => 'contacts_maj_contact_documents_1',
  'source' => 'non-db',
  'module' => 'MAJ_Contact_Documents',
  'bean_name' => 'MAJ_Contact_Documents',
  'vname' => 'LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_maj_contact_documents_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_department_id_c.php

 // created: 2019-11-25 14:08:03
$dictionary['Contact']['fields']['department_id_c']['labelValue']='Department';
$dictionary['Contact']['fields']['department_id_c']['dependency']='';
$dictionary['Contact']['fields']['department_id_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_contact_id1_c.php

 // created: 2020-04-09 14:43:26

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_portal_account_activated_c.php

 // created: 2019-06-24 16:25:03
$dictionary['Contact']['fields']['portal_account_activated_c']['labelValue']='Portal Account Activated';
$dictionary['Contact']['fields']['portal_account_activated_c']['dependency']='';
$dictionary['Contact']['fields']['portal_account_activated_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_functional_area_owner_c.php

 // created: 2020-07-22 06:43:18
$dictionary['Contact']['fields']['functional_area_owner_c']['labelValue']='Functional Area Owner';
$dictionary['Contact']['fields']['functional_area_owner_c']['dependency']='';
$dictionary['Contact']['fields']['functional_area_owner_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_equipment_owner_for_dept_c.php

 // created: 2020-10-20 09:35:37
$dictionary['Contact']['fields']['equipment_owner_for_dept_c']['labelValue']='Equipment Owner for Department';
$dictionary['Contact']['fields']['equipment_owner_for_dept_c']['dependency']='';
$dictionary['Contact']['fields']['equipment_owner_for_dept_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_full_address_c.php

 // created: 2020-11-19 14:06:15
$dictionary['Contact']['fields']['full_address_c']['labelValue']='Full Address';
$dictionary['Contact']['fields']['full_address_c']['dependency']='';
$dictionary['Contact']['fields']['full_address_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_management_c.php

 // created: 2021-01-05 07:42:13
$dictionary['Contact']['fields']['management_c']['labelValue']='Management';
$dictionary['Contact']['fields']['management_c']['enforced']='';
$dictionary['Contact']['fields']['management_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_one_up_manager_c.php

 // created: 2021-01-29 12:07:51
$dictionary['Contact']['fields']['one_up_manager_c']['labelValue']='One Up Manager';
$dictionary['Contact']['fields']['one_up_manager_c']['dependency']='';
$dictionary['Contact']['fields']['one_up_manager_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_ca_company_address_id1_c.php

 // created: 2020-11-13 12:17:07
 $dictionary["Contact"]["fields"]["full_address_c"] = array(
    'studio' => array('searchview' => true, 'visible' => false),
    'filter_relate' => array(
        'account_id' => 'accounts_ca_company_address_1accounts_ida',
       ),
);
 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/capa_capa_contacts_1_Contacts.php

// created: 2022-02-03 07:34:36
$dictionary["Contact"]["fields"]["capa_capa_contacts_1"] = array (
  'name' => 'capa_capa_contacts_1',
  'type' => 'link',
  'relationship' => 'capa_capa_contacts_1',
  'source' => 'non-db',
  'module' => 'CAPA_CAPA',
  'bean_name' => 'CAPA_CAPA',
  'vname' => 'LBL_CAPA_CAPA_CONTACTS_1_FROM_CAPA_CAPA_TITLE',
  'id_name' => 'capa_capa_contacts_1capa_capa_ida',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/capa_capa_contacts_2_Contacts.php

// created: 2022-02-03 07:36:41
$dictionary["Contact"]["fields"]["capa_capa_contacts_2"] = array (
  'name' => 'capa_capa_contacts_2',
  'type' => 'link',
  'relationship' => 'capa_capa_contacts_2',
  'source' => 'non-db',
  'module' => 'CAPA_CAPA',
  'bean_name' => 'CAPA_CAPA',
  'vname' => 'LBL_CAPA_CAPA_CONTACTS_2_FROM_CAPA_CAPA_TITLE',
  'id_name' => 'capa_capa_contacts_2capa_capa_ida',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/contacts_m03_work_product_2_Contacts.php

// created: 2022-07-07 07:02:30
$dictionary["Contact"]["fields"]["contacts_m03_work_product_2"] = array (
  'name' => 'contacts_m03_work_product_2',
  'type' => 'link',
  'relationship' => 'contacts_m03_work_product_2',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_m03_work_product_2contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
