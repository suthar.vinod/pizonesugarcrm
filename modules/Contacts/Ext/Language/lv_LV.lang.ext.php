<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/lv_LV.customcontacts_opportunities_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CONTACTS_OPPORTUNITIES_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE'] = 'Sales Activities';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/lv_LV.customcontacts_tm_tradeshow_management_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTS_TM_TRADESHOW_MANAGEMENT_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE'] = 'Tradeshow Activity';
$mod_strings['LBL_CONTACTS_TM_TRADESHOW_MANAGEMENT_1_FROM_CONTACTS_TITLE'] = 'Tradeshow Activity';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/lv_LV.customcontacts_ta_tradeshow_activities_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE'] = 'Tradeshow Activities';
$mod_strings['LBL_CONTACTS_TA_TRADESHOW_ACTIVITIES_1_FROM_CONTACTS_TITLE'] = 'Tradeshow Activities';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/lv_LV.customcontacts_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTS_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';
$mod_strings['LBL_CONTACTS_M01_SALES_1_FROM_CONTACTS_TITLE'] = 'Sales Activities';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/lv_LV.customcontacts_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/lv_LV.customcontacts_de_deviation_employees_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Deviation Employees';
$mod_strings['LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_CONTACTS_TITLE'] = 'Deviation Employees';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/lv_LV.customcontacts_maj_contact_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_MAJ_CONTACT_DOCUMENTS_TITLE'] = 'Contact Documents';
$mod_strings['LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_CONTACTS_TITLE'] = 'Contact Documents';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/lv_LV.customcapa_capa_contacts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CAPA_CAPA_CONTACTS_1_FROM_CAPA_CAPA_TITLE'] = 'CAPAs';
$mod_strings['LBL_CAPA_CAPA_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'CAPAs';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/lv_LV.customcapa_capa_contacts_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CAPA_CAPA_CONTACTS_2_FROM_CAPA_CAPA_TITLE'] = 'CAPAs';
$mod_strings['LBL_CAPA_CAPA_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'CAPAs';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/lv_LV.customcontacts_m03_work_product_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'Lead Auditor&#039;s Work Products';
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_CONTACTS_TITLE'] = 'Lead Auditor&#039;s Work Products';

?>
