<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/LogicHooks/ContactLogichook.php


$hook_version = 1;
if (!isset($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['before_save'])) {
    $hook_array['before_save'] = array();
}

$hook_array['before_save'][] = array(
    count($hook_array['before_save']),
    'When creating a new contact, send email.',
    'custom/modules/Contacts/ContactHooksImp.php',
    'ContactHooksImp',
    'beforeSave',
);

?>
<?php
// Merged from modules/Contacts/Ext/LogicHooks/RelationshipHook.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$hook_array['after_relationship_delete'][] = [
    1,
    'afterRelationshipDelete',
    'modules/Contacts/ContactsHooks.php',
    'ContactsHooks',
    'afterRelationshipDelete',
];

?>
