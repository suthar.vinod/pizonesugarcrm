<?php
// WARNING: The contents of this file are auto-generated.


// created: 2022-02-03 07:34:36
$viewdefs['Contacts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CAPA_CAPA_CONTACTS_1_FROM_CAPA_CAPA_TITLE',
  'context' => 
  array (
    'link' => 'capa_capa_contacts_1',
  ),
);

// created: 2022-02-03 07:36:41
$viewdefs['Contacts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CAPA_CAPA_CONTACTS_2_FROM_CAPA_CAPA_TITLE',
  'context' => 
  array (
    'link' => 'capa_capa_contacts_2',
  ),
);

// created: 2019-02-20 19:46:48
$viewdefs['Contacts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'context' => 
  array (
    'link' => 'contacts_de_deviation_employees_1',
  ),
);

// created: 2018-12-13 16:29:00
$viewdefs['Contacts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CONTACTS_M01_SALES_1_FROM_M01_SALES_TITLE',
  'context' => 
  array (
    'link' => 'contacts_m01_sales_1',
  ),
);

// created: 2018-12-21 23:14:20
$viewdefs['Contacts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'contacts_m03_work_product_1',
  ),
);

// created: 2022-07-07 07:02:30
$viewdefs['Contacts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'contacts_m03_work_product_2',
  ),
);

// created: 2019-08-15 11:34:01
$viewdefs['Contacts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_MAJ_CONTACT_DOCUMENTS_TITLE',
  'context' => 
  array (
    'link' => 'contacts_maj_contact_documents_1',
  ),
);

// created: 2018-04-23 17:07:52
$viewdefs['Contacts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CONTACTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'context' => 
  array (
    'link' => 'contacts_ta_tradeshow_activities_1',
  ),
);