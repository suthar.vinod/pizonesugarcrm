<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/contacts_ta_tradeshow_activities_1_Contacts.php

 // created: 2018-04-23 17:07:51
$layout_defs["Contacts"]["subpanel_setup"]['contacts_ta_tradeshow_activities_1'] = array (
  'order' => 100,
  'module' => 'TA_Tradeshow_Activities',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'get_subpanel_data' => 'contacts_ta_tradeshow_activities_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/contacts_m01_sales_1_Contacts.php

 // created: 2018-12-13 16:29:00
$layout_defs["Contacts"]["subpanel_setup"]['contacts_m01_sales_1'] = array (
  'order' => 100,
  'module' => 'M01_Sales',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_M01_SALES_1_FROM_M01_SALES_TITLE',
  'get_subpanel_data' => 'contacts_m01_sales_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/contacts_m03_work_product_1_Contacts.php

 // created: 2018-12-21 23:14:19
$layout_defs["Contacts"]["subpanel_setup"]['contacts_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'contacts_m03_work_product_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/contacts_de_deviation_employees_1_Contacts.php

 // created: 2019-02-20 19:46:48
$layout_defs["Contacts"]["subpanel_setup"]['contacts_de_deviation_employees_1'] = array (
  'order' => 100,
  'module' => 'DE_Deviation_Employees',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'get_subpanel_data' => 'contacts_de_deviation_employees_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/contacts_maj_contact_documents_1_Contacts.php

 // created: 2019-08-15 11:34:01
$layout_defs["Contacts"]["subpanel_setup"]['contacts_maj_contact_documents_1'] = array (
  'order' => 100,
  'module' => 'MAJ_Contact_Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_MAJ_CONTACT_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'contacts_maj_contact_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/capa_capa_contacts_1_Contacts.php

 // created: 2022-02-03 07:34:36
$layout_defs["Contacts"]["subpanel_setup"]['capa_capa_contacts_1'] = array (
  'order' => 100,
  'module' => 'CAPA_CAPA',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAPA_CAPA_CONTACTS_1_FROM_CAPA_CAPA_TITLE',
  'get_subpanel_data' => 'capa_capa_contacts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/capa_capa_contacts_2_Contacts.php

 // created: 2022-02-03 07:36:41
$layout_defs["Contacts"]["subpanel_setup"]['capa_capa_contacts_2'] = array (
  'order' => 100,
  'module' => 'CAPA_CAPA',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAPA_CAPA_CONTACTS_2_FROM_CAPA_CAPA_TITLE',
  'get_subpanel_data' => 'capa_capa_contacts_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/contacts_m03_work_product_2_Contacts.php

 // created: 2022-07-07 07:02:30
$layout_defs["Contacts"]["subpanel_setup"]['contacts_m03_work_product_2'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'contacts_m03_work_product_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/_overrideContact_subpanel_contacts_de_deviation_employees_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['contacts_de_deviation_employees_1']['override_subpanel_name'] = 'Contact_subpanel_contacts_de_deviation_employees_1';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/_overrideContact_subpanel_contacts_maj_contact_documents_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['contacts_maj_contact_documents_1']['override_subpanel_name'] = 'Contact_subpanel_contacts_maj_contact_documents_1';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/_overrideContact_subpanel_contacts_m03_work_product_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['contacts_m03_work_product_1']['override_subpanel_name'] = 'Contact_subpanel_contacts_m03_work_product_1';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/_overrideContact_subpanel_capa_capa_contacts_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['capa_capa_contacts_1']['override_subpanel_name'] = 'Contact_subpanel_capa_capa_contacts_1';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Layoutdefs/_overrideContact_subpanel_capa_capa_contacts_2.php

//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['capa_capa_contacts_2']['override_subpanel_name'] = 'Contact_subpanel_capa_capa_contacts_2';

?>
