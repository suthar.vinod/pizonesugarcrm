<?php
global $db;
echo $query = "SELECT CON.id,CON.first_name,CON.last_name,CON.primary_address_street AS Address,
                CON.primary_address_city AS City,
                CON.primary_address_state AS State,
                CON.primary_address_country AS Country,
                CON.primary_address_postalcode AS zip,
                CA.name,ca_company_address_id1_c
            FROM contacts AS CON  
            LEFT JOIN contacts_cstm AS CONS ON CON.id = CONS.id_c
            LEFT JOIN ca_company_address AS CA ON CONS.ca_company_address_id1_c = CA.id
         WHERE CON.deleted=0 AND ca_company_address_id1_c IS NOT NULL";

$con_exec = $db->query($query);
if ($con_exec->num_rows > 0) 
{
    $cnt = 1;
    $emailBody = "<table width='100%' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr.No.</th><th bgcolor='#b3d1ff' align='left'>ID </th><th bgcolor='#b3d1ff' align='left'>Name</th>
            <th bgcolor='#b3d1ff' align='left'>Contact Address</th>
            <th bgcolor='#b3d1ff' align='left'>Company Name</th>
            <th bgcolor='#b3d1ff' align='left'>Company Address ID</th></tr>";	
			
    while($resultCon = $db->fetchByAssoc($con_exec)){

        $id             = $resultCon['id'];
        $conName        = $resultCon['first_name']." ".$resultCon['last_name'];
        $conAddress     = $resultCon['Address']." ".$resultCon['City']." ".$resultCon['State']." ".$resultCon['zip']." ".$resultCon['Country'];
        $compAddressID  = $resultCon['ca_company_address_id1_c'];
        $compAddressName = $resultCon['name'];
        //$id = $resultCon[''];

        $conBean = BeanFactory::retrieveBean("Contacts", $id);//, array('disable_row_level_security' => true)
        //$conBean->account_id;
        $companyName =  $conBean->account_name;

        if($cnt%2==0)
			$stylr = "style='background-color: #e6e6e6;'";
		else
			$stylr = "style='background-color: #f3f3f3;'";
        $emailBody .= "<tr><td ".$stylr.">".$cnt++."</td><td ".$stylr.">".$id."</td><td ".$stylr.">".$conName."</td>
		<td ".$stylr.">".$conAddress."</td><td ".$stylr.">".$companyName."</td>
        <td ".$stylr.">".$compAddressID."</td></tr>";
    }
    echo $emailBody .="</table>";
}