<?php

include_once("include/workflow/alert_utils.php");
    class Contacts_alerts {
    function process_wflow_Contacts1_alert0(&$focus){
            include("custom/modules/Contacts/workflow/alerts_array.php");

	 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "Registration Approval Required&lt;br /&gt;
&lt;br /&gt;
You have registered on American Preclinical Services. Your account will be approved by the administrator.&lt;br /&gt;
&lt;br /&gt;
If you have any questions or need immediate assistance please contact APS business development at 763-717-7990. &lt;br /&gt;
&lt;br /&gt;
Please do not reply to this mail as this is an automated email service. &lt;br /&gt;
&lt;br /&gt;
Visit us at www.americanpreclinical.com"; 

	 $alertshell_array['source_type'] = "Normal Message"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['Contacts1_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
	 }



    //end class
    }

?>