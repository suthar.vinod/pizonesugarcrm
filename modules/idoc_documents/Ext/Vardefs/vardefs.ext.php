<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/idoc_documents/Ext/Vardefs/idoc_documents_idoc_signers_idoc_documents.php

// created: 2015-08-19 15:36:35
$dictionary["idoc_documents"]["fields"]["idoc_documents_idoc_signers"] = array (
  'name' => 'idoc_documents_idoc_signers',
  'type' => 'link',
  'relationship' => 'idoc_documents_idoc_signers',
  'source' => 'non-db',
  'module' => 'idoc_signers',
  'bean_name' => 'idoc_signers',
  'vname' => 'LBL_IDOC_DOCUMENTS_IDOC_SIGNERS_FROM_IDOC_DOCUMENTS_TITLE',
  'id_name' => 'idoc_documents_idoc_signersidoc_documents_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/idoc_documents/Ext/Vardefs/sugarfield_multidoc_c.php

 // created: 2017-08-31 19:59:19
$dictionary['idoc_documents']['fields']['multidoc_c']['duplicate_merge_dom_value']=0;

 
?>
