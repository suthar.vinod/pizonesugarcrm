<?php

require_once 'include/Sugarpdf/sugarpdf/sugarpdf.pdfmanager.php';
require_once 'include/Sugarpdf/sugarpdf/sugarpdf.smarty.php';
//require_once('include/Sugar_Smarty.php');
//ob_start();
class M06_ErrorSugarpdfPdfmanager extends SugarpdfPdfmanager
{
    public function preDisplay()
    {
        global $db, $current_user;
        //$this->ss = new Sugar_Smarty();
		//$GLOBALS['log']->fatal("17may po bean->name ".$this->bean->name);
        parent::preDisplay();
        $this->SetMargins(15, 10, 15);

        $previewMode = false;

        if (!empty($_REQUEST['pdf_preview']) && $_REQUEST['pdf_preview'] == 1) {

            $previewMode = true;

        }
		
		if ($this->module == 'M06_Error' && $previewMode === false) {

            // Select links to add
			//$GLOBALS['log']->fatal('name comm1 === ' . $this->bean->name);
			if (!empty($this->bean->name)) {
                $fields['commName'] = $this->bean->name;
            }
			 
			if (!empty($this->bean->tpr_reference_c)){
                $fields['commTprReference'] = $this->bean->tpr_reference_c;
            }
            $textCount = 0;
			if (!empty($this->bean->subjectiveobjective_exam_c)) {
                $fields['commSubjectiveObjectiveExam'] = $this->bean->subjectiveobjective_exam_c;
                $textCount  += str_word_count($fields['commSubjectiveObjectiveExam']);
            }

			if (!empty($this->bean->vet_assessment_notes_2_c)) {
                $fields['commVetAssessmentNotes'] = $this->bean->vet_assessment_notes_2_c;
                $textCount  += str_word_count($fields['commVetAssessmentNotes']);
            } 
 
			if (!empty($this->bean->refer_to_med_treatment_form_c)) {
                $fields['commReferToMedTreatmentForm'] = 1;
                $fields['commReferToMedTreatmentFormVal'] = $this->bean->refer_to_med_treatment_form_c;
                $textCount  += str_word_count($fields['commReferToMedTreatmentFormVal']);
            }else{
                $fields['commReferToMedTreatmentForm'] = 0;
            }

            if (!empty($this->bean->notify_vet_c )) {
                $fields['commNotifyVet'] = 1;
                $fields['commNotifyVetVal'] = $this->bean->notify_vet_c;
                $textCount  += str_word_count($fields['commNotifyVetVal']);
            }else{
                $fields['commNotifyVet'] = 0;
            }
            
            if (!empty($this->bean->plan_comments_c )) {
                $fields['commPlanComments'] = 1;
                $fields['commPlanCommentsVal'] = $this->bean->plan_comments_c;
                $textCount  += str_word_count($fields['commPlanCommentsVal']);
            }else{
                $fields['commPlanComments'] = 0;
            }

            //Work Product Beans
            $this->bean->load_relationship('m06_error_m03_work_product_1');
            $WpBeanId = $this->bean->m06_error_m03_work_product_1->get();
			$WpBean   = BeanFactory::getBean('M03_Work_Product', $WpBeanId[0]);
			//$GLOBALS['log']->fatal('WpBean Array  === ' . print_r($WpBean->name,1));
            $wpName = $WpBean->name;
			
			if (!empty($wpName)) {
                $fields['commlinkedwp'] = $wpName;
            }

            //TS Beans
            $this->bean->load_relationship('m06_error_anml_animals_1');
            $TsBeanId	= $this->bean->m06_error_anml_animals_1->get();
            $TsBean		= BeanFactory::getBean('ANML_Animals', $TsBeanId[0]);
			$tsName		= $TsBean->name;
			$speciesID	= $TsBean->s_species_id_c;
			if(($speciesID=="5dad1606-1131-11ea-80f4-02fb813964b8"
                || $speciesID=="5de72184-1131-11ea-abb2-02fb813964b8"
                || $speciesID=="5dbadd5e-1131-11ea-b633-02fb813964b8"
                || $speciesID=="5d9c2666-1131-11ea-9421-02fb813964b8"
                || $speciesID=="5d89b5d0-1131-11ea-afac-02fb813964b8"))
            {
                if($TsBean->usda_id_c!="")
					$tsName = $TsBean->usda_id_c;
				else
					$tsName = "";
			}  
			 
			if (!empty($tsName)) {
                $fields['commlinkedts'] = $tsName;
            }
			
            if($textCount<300)
                $objectiveTextSize = '1em';
            if($textCount>300 && $textCount<380)
                $objectiveTextSize = '0.90em';    
            if($textCount>380 && $textCount<450)
                $objectiveTextSize = '0.80em';
            if($textCount>450 && $textCount<525)
                $objectiveTextSize = '0.70em';    
            if($textCount>530)
                $objectiveTextSize = '0.60em';   

            $fields['commSubjectiveCount']		= $textCount; 
            $fields['commSubjectiveFontSize']	= $objectiveTextSize;

            $this->ss->assign('fields', $fields);
        } 
    }
}