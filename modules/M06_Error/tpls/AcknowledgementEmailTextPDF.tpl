<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
</head>
<body>
<table style="width: 600px;" border="1" cellspacing="0" cellpadding="4">
<tbody>
<tr>
<td width="150pt"><span style="font-size:10pt;">COM ID</span></td>
<td width="350pt"><span style="font-size:10pt;"><a href="https://aps.sugarondemand.com/#M06_Error/{$fields.id}">{$fields.name}</a></span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">SD Acknowledgement Date</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.actual_sd_ack_date_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Date Occurred</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.date_error_occurred_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Date Discovered</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.date_error_documented_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Date Time Discovered</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.date_time_discovered_text_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Work Product</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.wp_name_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Study Director</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.study_director_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Compliance</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.compliance_c}</span></td>
</tr>  
</tbody>
</table>
<p><br/><br/><span style="font-size:10pt;font-family:Calibri, sans-serif;">CONFIDENTIALITY NOTICE: This e-mail transmission may contain confidential or legally privileged information that is intended only for the individual or entity named in the e-mail address. Any unauthorized distribution or copying of this transmittal or its attachments, if any, is prohibited. If you have received this e-mail transmission in error, please reply to <a class="email" href="mailto:itsupport@apsemail.com">itsupport@apsemail.com</a>, so that American Preclinical Services can arrange for proper delivery, then please delete the message from your inbox. Thank you.</span></p>
</body>
</html>