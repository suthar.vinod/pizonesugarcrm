<table border="1" cellpadding="2" style="border 1pt solid" width="500pt">
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Work Product</span></td>
        <td width="350pt"><span style="font-size:8pt;">{$data.workProductData}</span></td>
    </tr> 
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Study Director</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.study_directors != null}{foreach from=$data.study_directors key=index item=line_item}{if $line_item.name!= ""}{$line_item.name}{else}{"NA"}{/if}{/foreach}{else}{"NA"}{/if}</span></td>
    </tr> 
	<tr>
        <td width="150pt"><span style="font-size:8pt;">Compliance</span></td>
        <td width="350pt"><span style="font-size:8pt;">{$data.wpComplianceData}</span></td>
    </tr> 
	<tr>
        <td width="150pt"><span style="font-size:8pt;">Submitted By</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.email!= ""}{$data.email}{else}{"NA"} {/if}</span></td>
    </tr>
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Category</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.error_category!= ""}{$data.error_category}{else}{"NA"} {/if}</span></td>
    </tr>
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Type</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.error_type!= ""}{$data.error_type}{else}{"NA"} {/if}</span></td>
    </tr>
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Date Occurred</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.date_error_occurred!= ""}{$data.date_error_occurred}{else}{"NA"} {/if}</span></td>
    </tr>
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Resolution Date</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.Resolution_Date!= ""}{$data.Resolution_Date}{else}{"NA"} {/if}</span></td>
    </tr>
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Date Discovered</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.date_error_discovered!= ""}{$data.date_error_discovered}{else}{"NA"} {/if}</span></td>
    </tr>
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Time Discovered</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.date_time_error_discovered!= ""}{$data.date_time_error_discovered}{else}{"NA"} {/if}</span></td>
    </tr>
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Activities</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.activities!= ""}{$data.activities}{else}{"NA"} {/if}</span></td>
    </tr>
	<tr>
        <td width="150pt"><span style="font-size:8pt;">Expected Event</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.expected_event!= ""}{$data.expected_event}{else}{"NA"} {/if}</span></td>
    </tr>
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Actual Event</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.actual_event!= ""}{$data.actual_event}{else}{"NA"} {/if}</span></td>
    </tr>
	 <tr>
        <td width="150pt"><span style="font-size:8pt;">Test System(s)</span></td>
        <td width="350pt"><span style="font-size:8pt;">{$data.testSystemData}</span></td>
    </tr> 
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Responsible Personnel (Entry)</span></td>
        <td width="350pt"><span style="font-size:8pt;">{$data.EntryEmailData}</span></td>
    </tr> 
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Responsible Personnel (Review)</span></td>
        <td width="350pt"><span style="font-size:8pt;">{$data.ReviewEmailData}</span></td>
    </tr> 
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Department Manager</span></td>
        <td width="350pt"><span style="font-size:8pt;">{$data.ManagerData}</span></td>
    </tr> 
    <tr>
        <td width="150pt"><span style="font-size:8pt;">One up Manager</span></td>
        <td width="350pt"><span style="font-size:8pt;">{$data.OneupManagerData}</span></td>
    </tr>			
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Room(s)</span></td>
        <td width="350pt"><span style="font-size:8pt;">{$data.RMS_RoomData}</span></td>
    </tr>    
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Document(s)</span></td>
        <td width="350pt"><span style="font-size:8pt;">{$data.Erd_Error_DocumentsData}</span></td>
    </tr> 
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Equipment & Facilities</span></td>
        <td width="350pt"><span style="font-size:8pt;">{$data.Equip_EquipmentData}</span></td>
    </tr> 
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Does Submitter know Why it Happened?</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.why_it_happened_c!= ""}{$data.why_it_happened_c}{else}{"NA"}{/if}</span></td>
    </tr> 
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Details on Why it Happened</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.details_why_happened_c!= ""}{$data.details_why_happened_c}{else}{"NA"}{/if}</span></td>
    </tr> 
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Critical Phase Inspection</span></td>
        <td width="350pt"><span style="font-size:8pt;">{$data.A1A_Critical_Phase_InspectioData}</span></td>
    </tr>
 <tr>
        <td width="150pt"><span style="font-size:8pt;">Recommendation</span></td>
        <td width="350pt"><span style="font-size:8pt;">{if $data.recommendation!= ""}{$data.recommendation}{else}{"NA"}{/if}</span></td>
    </tr>	
    <tr>
        <td width="150pt"><span style="font-size:8pt;">Related COM ID</span></td>
        <td width="350pt"><span style="font-size:8pt;">{$data.M06_ErrorData}</span></td>
    </tr> 
</table>

<p><span style="font-size:8pt;">Please review {$data.error_name} and make assessments as warranted in APS' Project Management Tool.</span></p>
<p><br/><br/><span style="font-size:10pt;font-family:Calibri, sans-serif;">CONFIDENTIALITY NOTICE: This e-mail transmission may contain confidential or legally privileged information that is intended only for the individual or entity named in the e-mail address. Any unauthorized distribution or copying of this transmittal or its attachments, if any, is prohibited. If you have received this e-mail transmission in error, please reply to <a class="email" href="mailto:itsupport@apsemail.com">itsupport@apsemail.com</a>, so that American Preclinical Services can arrange for proper delivery, then please delete the message from your inbox. Thank you.</span></p>