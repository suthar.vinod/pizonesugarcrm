<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
</head>
<body>
<table style="width: 600px;" border="1" cellspacing="0" cellpadding="4">
<tbody>
<tr>
<td width="150pt"><span style="font-size:10pt;">Resolved COM</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.resolved_com}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Work Product</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.wp_name_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Study Director</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.study_director_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Compliance</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.compliance_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Submitted By</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.submitter_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Category</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.error_category_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Type</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.error_type_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Date Occurred</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.date_error_occurred_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Resolution Date</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.resolution_date_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Date Discovered</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.date_error_documented_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Time Discovered</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.date_time_discovered_text_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Activities</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.activities}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Expected Event</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.expected_event_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Veterinary Assessment</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.veterinary_assessment}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Test System(s)</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.test_system_name_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Team(s)</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.team}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Responsible Personnel (Entry)</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.EntryEmailData}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Responsible Personnel (Review)</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.ReviewEmailData}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Department Manager</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.ManagerData}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">One Up Manager</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.one_up_manager}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Room(s)</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.room}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Document(s)</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.document}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Equipment & Facilities</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.equipment_facilities}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Does Submitter know Why it Happened?</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.why_it_happened_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Details on Why it Happened</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.details_why_happened_c}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Critical Phase Inspection</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.critical_phase_inspection}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Recommendation</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.recommendation}</span></td>
</tr>
<tr>
<td width="150pt"><span style="font-size:10pt;">Related COM ID</span></td>
<td width="350pt"><span style="font-size:10pt;">{$fields.RelatedCommData}</span></td>
</tr>
</tbody>
</table>
<p><br/><br/><span style="font-size:10pt;font-family:Calibri, sans-serif;">CONFIDENTIALITY NOTICE: This e-mail transmission may contain confidential or legally privileged information that is intended only for the individual or entity named in the e-mail address. Any unauthorized distribution or copying of this transmittal or its attachments, if any, is prohibited. If you have received this e-mail transmission in error, please reply to <a class="email" href="mailto:itsupport@apsemail.com">itsupport@apsemail.com</a>, so that American Preclinical Services can arrange for proper delivery, then please delete the message from your inbox. Thank you.</span></p>
</body>
</html>