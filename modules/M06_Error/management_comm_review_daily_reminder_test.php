<?php

    global $db;
    $current_date = date("Y-m-d"); //current date	
	$entered_date1 = '2020-12-15 00:00:00';
	
	$previous_day	= date('Y-m-d', strtotime('-1 day', strtotime($current_date)));
	$next_day		= date('Y-m-d', strtotime('+1 day', strtotime($current_date)));
	
	
    $wp_emailAdd	= array(); // contains the email addresses of Study Director and its Manager
	$sd_array		= array(); //Contains id For Study Directors
    
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url		= $GLOBALS['sugar_config']['site_url'];
	$domainToSearch = 'apsemail.com';
    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();

   echo "==>".$queryCustom = "SELECT 
                    COMM.id AS communicationID, 
                    COMM.name AS CommunicationName, 
					CONTACTEMPLOYEE.contacts_de_deviation_employees_1contacts_ida AS CONTACTID,
					EMPLOYEE.id,
					EMPLOYEECSTM.deviation_type_c,
					CONTACT.first_name,
					CONTACT.last_name,
					CONTACTCSTM.contact_id_c AS MGTID,
					CONTACTCSTM.contact_id1_c AS oneUpMGTID,
					CONTACTCSTM.management_c AS MGTCheck,
                    COMM_CSTM.target_management_assessment_c, 
					COMM_CSTM.actual_management_assessment_c,   
					COMM_CSTM.error_category_c,
					COMM_WP.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
                    WP.name  AS workProductName
            FROM `m06_error` AS COMM
                    INNER JOIN m06_error_cstm AS COMM_CSTM
                    ON  COMM.id = COMM_CSTM.id_c
					
					LEFT JOIN m06_error_m03_work_product_1_c AS COMM_WP
                    ON  COMM.id = COMM_WP.m06_error_m03_work_product_1m06_error_ida AND COMM_WP.deleted=0
					
					LEFT JOIN m03_work_product AS WP
                    ON  COMM_WP.m06_error_m03_work_product_1m03_work_product_idb=WP.id AND WP.deleted=0 

                    RIGHT JOIN m06_error_de_deviation_employees_2_c AS COMMDEV
                    ON  COMM.id = COMMDEV.m06_error_de_deviation_employees_2m06_error_ida AND COMMDEV.deleted=0 
					
					INNER JOIN de_deviation_employees AS EMPLOYEE
                    ON  COMMDEV.m06_error_de_deviation_employees_2de_deviation_employees_idb=EMPLOYEE.id  AND EMPLOYEE.deleted=0 
					
					INNER JOIN de_deviation_employees_cstm AS EMPLOYEECSTM
                    ON  EMPLOYEE.id=EMPLOYEECSTM.id_c 
					
					INNER JOIN contacts_de_deviation_employees_1_c AS CONTACTEMPLOYEE
                    ON  EMPLOYEE.id =CONTACTEMPLOYEE.contacts_de_deviation_employees_1de_deviation_employees_idb AND CONTACTEMPLOYEE.deleted=0
					
					LEFT JOIN contacts AS CONTACT
					 	ON CONTACT.id=CONTACTEMPLOYEE.contacts_de_deviation_employees_1contacts_ida  AND CONTACT.deleted=0
					LEFT JOIN contacts_cstm AS CONTACTCSTM
					 	ON CONTACT.id=CONTACTCSTM.id_c
					 
		WHERE  
			COMM.deleted = 0
			AND YEAR(COMM_CSTM.target_management_assessment_c) >= 2020 
			AND COMM_CSTM.target_management_assessment_c >'" . $entered_date1 . "'
			AND COMM_CSTM.target_management_assessment_c <='" . $next_day . "'
			AND 
			(
			(
				(COMM_CSTM.error_category_c = 'Critical Phase Inspection' 
					OR	COMM_CSTM.error_category_c = 'Daily QC'
					OR	COMM_CSTM.error_category_c = 'Data Book QC'
					OR	COMM_CSTM.error_category_c = 'Real time study conduct'
					OR	COMM_CSTM.error_category_c = 'Retrospective Data QC'
					OR	COMM_CSTM.error_category_c = 'Internal Feedback'
					OR	COMM_CSTM.error_category_c = 'Process Audit'
					OR	COMM_CSTM.error_category_c = 'Test Failure'
					OR	COMM_CSTM.error_category_c = 'External Audit'
					OR	COMM_CSTM.error_category_c = 'Weekly Sweep'
				)  AND( COMM_CSTM.subtype_c IS NULL )
			) OR (
				(COMM_CSTM.error_category_c = 'Training'  OR COMM_CSTM.error_category_c = 'External Audit')
				AND COMM.date_entered >='" . $entered_date1 . "'  
				AND ( COMM_CSTM.actual_management_assessment_c IS NULL  OR COMM_CSTM.mgt_assessment_c IS NULL )
			)
			)	
		ORDER BY COMM_CSTM.target_management_assessment_c ASC";
		//GROUP BY COMM.id,MGTID

    $queryCommResult = $db->query($queryCustom);
	
	 while($fetchCommResult = $db->fetchByAssoc($queryCommResult)) {

        $communicationID	= $fetchCommResult['communicationID'];
        echo "<br>===>".$communicationName	= $fetchCommResult['CommunicationName'];
		$workProductID		= $fetchCommResult['workProductID'];
		$workProductName	= $fetchCommResult['workProductName'];
        $targetAssessmentDate	= $fetchCommResult['target_management_assessment_c'];
		$EMP_ID				= $fetchCommResult['CONTACTID'];
		$EMP_TYPE			= $fetchCommResult['deviation_type_c'];
		echo "===>".$SD_ID				= $fetchCommResult['MGTID'];
		echo "===>".$Oneup_ID			= $fetchCommResult['oneUpMGTID'];
		$MGTCheck			= $fetchCommResult['MGTCheck'];
		
		echo "<br><br><br>";
        if (($SD_ID != "" || $Oneup_ID != "") && ($communicationID != "" && $communicationName!=""  && $targetAssessmentDate!="") ) {
			
			$emp_bean		= BeanFactory::getBean('Contacts', $EMP_ID); /*Get Employee Bean */
			$empName		= $emp_bean->first_name." ".$emp_bean->last_name;
			$primaryEmailAddress = $emp_bean->emailAddress->getPrimaryAddress($emp_bean);

			$mgt_bean		= BeanFactory::getBean('Contacts', $SD_ID); /*Get Manager Bean */
			$mgtName		= $mgt_bean->first_name." ".$mgt_bean->last_name;
			$mgtEmailAddress = $mgt_bean->emailAddress->getPrimaryAddress($mgt_bean);

			$oneUpManagerBean 	 = BeanFactory::getBean('Contacts', $Oneup_ID); /*Get OneUp Manager Bean */
			$oneUpmanager_name	 = $oneUpManagerBean->first_name." ".$oneUpManagerBean->last_name;
			$oneUpmanagerAddress = $oneUpManagerBean->emailAddress->getPrimaryAddress($oneUpManagerBean);
			$helloName = "";	
			
			if($SD_ID!=""){
				$arrayID = $SD_ID;
				$helloName = $mgtName;
			}else{
				$arrayID = $Oneup_ID;
				$helloName = $oneUpmanager_name;
			}
			if($MGTCheck){
				$arrayID = $EMP_ID;
				$helloName = $empName;
			}

			$sd_array[$arrayID][]	 = array( 
						"communicationID"	=> $communicationID,  
						"communicationName" => $communicationName,
						"workProductID"		=> $workProductID, 
						"workProductName"	=> $workProductName,
						"assessmentDate"	=> date("m-d-Y", strtotime($targetAssessmentDate)),
						"empName"			=> $empName,
						"empType" 			=> $EMP_TYPE,
						"empMail" 			=> $primaryEmailAddress,
						"mgtName" 			=> $helloName,
						"mgtMail" 			=> $mgtEmailAddress,
						"OneupMngName" 		=> $oneUpmanager_name,
						"OneupMngMail" 		=> $oneUpmanagerAddress,
						"mgtCheck" 			=> $MGTCheck,
					);					
		}
	}
	
	
	if(count($sd_array)>0){ 
		$cntr = 1;
		foreach ($sd_array as $key => $value) { 
			$communication_name	 =""; 	
		
			unset($wp_emailAdd); 
            /* Get Email id of Assigned User and report to User */
			$wp_emailAdd = array();
			$arr = array();
			
			//Email Body Header
			$emailBody = "<table width='500px' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Communication ID </th><th bgcolor='#b3d1ff' align='left'>Work Product ID</th><th bgcolor='#b3d1ff' align='left'>Target Subtype Date</th></tr>";	
			
			for($cnt=0;$cnt<count($sd_array[$key]);$cnt++){
				$communication_name = '<a target="_blank"href="'.$site_url.'/#M06_Error/'.$sd_array[$key][$cnt]['communicationID'].'">'.$sd_array[$key][$cnt]['communicationName'] . '</a>';
				$workProduct_name   = "";
				if($sd_array[$key][$cnt]['workProductID']!="" && $sd_array[$key][$cnt]['workProductName']!=""){
					$workProduct_name   = '<a target="_blank" href="'.$site_url.'/#M03_Work_Product/'.$sd_array[$key][$cnt]['workProductID'].'" >'.$sd_array[$key][$cnt]['workProductName'] . '</a>';
				}
				$mgtCheck  = $sd_array[$key][$cnt]['mgtCheck'];
				$empMail  = $sd_array[$key][$cnt]['empMail'];

				$mgtName  = $sd_array[$key][$cnt]['mgtName'];
				$mgtMail  = $sd_array[$key][$cnt]['mgtMail'];
				$OneupMngName  = $sd_array[$key][$cnt]['OneupMngName'];
				$OneupMngMail  = $sd_array[$key][$cnt]['OneupMngMail'];
				$assessment_date  = $sd_array[$key][$cnt]['assessmentDate'];

				if($mgtCheck == 1) {
					$GLOBALS['log']->debug("mgtCheck =1");
					$wp_emailAdd[] = $empMail;	
				}				
				$wp_emailAdd[] = $mgtMail;
				$wp_emailAdd[] = $OneupMngMail;	

				//$emailBody .= "<tr><td>".$communication_name."</td><td>".$workProduct_name."</td><td>".$assessment_date."</td></tr>";
				$arr[$sd_array[$key][$cnt]['communicationName']] = "<tr><td>".$communication_name."</td><td>".$workProduct_name."</td><td>".$assessment_date."</td></tr>";
			}
			$arr =  array_unique($arr);
			foreach($arr as $key=>$value){
				$emailBody .= $value; 
			}
			$emailBody .="</table>";
			
					
			$template->retrieve_by_string_fields(array('name' => 'Management Communication Review Daily Reminder Consolidate', 'type' => 'email'));
			 
			$template->body_html = str_replace('[MGT Name]', $mgtName, $template->body_html);
			echo "<br><br>====>".$template->body_html = str_replace('[Consolidate_Report]', $emailBody, $template->body_html);
			  
			
		}
	}	
	 
?>