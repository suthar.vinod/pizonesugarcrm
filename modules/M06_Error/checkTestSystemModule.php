<?php

//AHTSHAM-TL - For Generate the System level Number
//Code for an auto-incrementing the �Name� field in the "Error" module?
//Naming convention will be �E18-xxxx�. This module is on the live site already.
//Since 2019 prefix has changed to "O" and in future from the year of 2020 it will "C"

class checkTestSystemModule
{
	function checkTestSystem($bean, $event, $arguments)
	{
		global $db, $current_user;
		//$GLOBALS['log']->fatal('M06Error TS first_procedure_date '.$bean->first_procedure_date);
		//$GLOBALS['log']->fatal('M06Error TS Trigger '.$bean->multiple_usda_id);
		//$GLOBALS['log']->fatal('M06Error TS related_id '.$arguments['related_id']);		
		if ($arguments['related_module'] == 'ANML_Animals' && $arguments['link'] == 'm06_error_anml_animals_1') {
			
			$allRecord	=	0;
			$TSId       =  "";
			$queryCountAll = "SELECT TS.name AS TSName, TS.id AS TSId FROM m06_error_anml_animals_1_c AS COMMTS LEFT JOIN anml_animals AS TS ON COMMTS.m06_error_anml_animals_1anml_animals_idb = TS.id WHERE m06_error_anml_animals_1m06_error_ida='" . $bean->id . "' AND COMMTS.deleted=0";

			$resultAllRecord = $db->query($queryCountAll);

			if ($resultAllRecord->num_rows > 0) {
				$allRecord = 1;
				while ($rowAllRecord = $db->fetchByAssoc($resultAllRecord)) {
					$TSId = $rowAllRecord['TSId'];
				}
			}
			if($bean->id == ""){
				$TSId = $arguments['related_id'];
			}

			//$GLOBALS['log']->fatal('M06Error bean Id  '.$bean->id);
			//$GLOBALS['log']->fatal('M06Error Trigger ANML_Animals Id '.$TSId);

			if ($allRecord > 0) {
				$bean->test_system_involved_c = 'Yes';
			} else {
				$bean->test_system_involved_c = 'No';
			}

			$category = $bean->error_category_c;
			$procedure = $bean->procedure_type_c;			
			$testSysBean = BeanFactory::retrieveBean('ANML_Animals', $TSId);

			//$GLOBALS['log']->fatal('M06Error category '.$category);
			//$GLOBALS['log']->fatal('M06Error date_error_occurred_c '.$bean->date_error_occurred_c);

			//$GLOBALS['log']->fatal('M06Error ANML_Animals NAme '.$testSysBean->name);

			if ($category == "Work Product Schedule Outcome") {

				if ($bean->error_type_c == "Performed Per Protocol") {
					$testSysBean->confirmed_on_study_c = 1;
				}
				if ($bean->error_type_c == 'Found Deceased') {
					$dateOccured = date("Y-m-d", strtotime($bean->date_time_discovered_c));
				} else {
					$dateOccured = date("Y-m-d", strtotime($bean->date_error_occurred_c));
				}

				if ($bean->error_type_c == "Sedation for Animal Health" && $bean->date_error_occurred_c != "") {
					if ($testSysBean->abnormality_c == "")
						$testSysBean->abnormality_c = "Sedated " . $dateOccured;
					else
						$testSysBean->abnormality_c = $testSysBean->abnormality_c . ", Sedated " . $dateOccured;
				}

				if ($bean->error_type_c == "Rejected" && $bean->date_error_occurred_c != "") {
					if ($testSysBean->abnormality_c == "")
						$testSysBean->abnormality_c = "Rejected " . $dateOccured;
					else
						$testSysBean->abnormality_c = $testSysBean->abnormality_c . ", Rejected " . $dateOccured;
				}

				if ($procedure == "Acute") {
					if (empty($testSysBean->first_procedure_date_c)) {
						$testSysBean->first_procedure_date_c = $dateOccured;
					}
					if (empty($testSysBean->termination_date_c)) {
						$testSysBean->termination_date_c = $dateOccured;
					}
				}
				if ($procedure == "Initial" || $procedure == "Screening") {
					if (empty($testSysBean->first_procedure_date_c)) {
						$testSysBean->first_procedure_date_c = $dateOccured;						
					}
				}
				if ($procedure == "Termination") {
					if (empty($testSysBean->termination_date_c)) {
						$testSysBean->termination_date_c = $dateOccured;						
					}
				}

				if ($bean->error_type_c == 'Standard Biocomp TS Selection') {
					if($bean->multiple_usda_id != ""){
						$testSysBean->usda_id_c = $bean->multiple_usda_id;
					}
					
					if (empty($testSysBean->first_procedure_date_c)) {
						$testSysBean->first_procedure_date_c = date("Y-m-d", strtotime($bean->first_procedure_date));
					}
				}

				$testSysBean->save();
				
				//$GLOBALS['log']->fatal('M06Error dateOccured '.$dateOccured);
				
				$source = '{"subject":{"_type":"logic-hook","class":"checkTestSystemModule","method":"checkTestSystem"},"attributes":[]}';
				
				//$GLOBALS['log']->fatal('M06Error procedure '.$procedure);
				if ($TSId!="") {
					if ($procedure == "Acute") {						
						if (empty($testSysBean->first_procedure_date_c)) {
							$auditEventid = create_guid();
							//$GLOBALS['log']->fatal('M06Error Trigger ANML_Animals In procedure Acute First Procedure Date With date occured '.$dateOccured);
							 						
							$query_update = "UPDATE anml_animals_cstm SET first_procedure_date_c = '" . $dateOccured . "' WHERE id_c = '" . $TSId . "'";
							 
							$db->query($query_update);				
							$auditsql = 'INSERT INTO anml_animals_audit  (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $TSId . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"first_procedure_date_c","varchar","","' . $dateOccured . '")';
							 
							$auditsqlResult = $db->query($auditsql);
							
							if ($auditsqlResult) {
								$auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" .$auditEventid . "','update', '" . $TSId . "','ANML_Animals','" . $source . "')";
								$db->query($auditsqlStatus);
							}
						}
						if (empty($testSysBean->termination_date_c)) {
							$auditEventid1 = create_guid(); 
							$query_update = "UPDATE anml_animals_cstm SET termination_date_c = '" . $dateOccured . "' WHERE id_c = '" . $TSId . "'";
							 
							$db->query($query_update);				
							$auditsql = 'INSERT INTO anml_animals_audit  (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $TSId . '","' . $auditEventid1 . '",now(),"' . $current_user->id . '",now(),"termination_date_c","varchar","","' . $dateOccured . '")';
							$auditsqlResult = $db->query($auditsql);
		
							if ($auditsqlResult) {
								$auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid1 . "','update', '" . $TSId . "','ANML_Animals','" . $source . "')";
								$db->query($auditsqlStatus);
							}
						}
					}
					if ($procedure == "Initial" || $procedure == "Screening") {
						$auditEventid2 = create_guid();
						if (empty($testSysBean->first_procedure_date_c)) {
							$query_update = "UPDATE anml_animals_cstm SET first_procedure_date_c = '" . $dateOccured . "' WHERE id_c = '" . $TSId . "'";
							 			
							$auditsql = 'INSERT INTO anml_animals_audit  (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $TSId . '","' . $auditEventid2 . '",now(),"' . $current_user->id . '",now(),"first_procedure_date_c","varchar","","' . $dateOccured . '")';
							$auditsqlResult = $db->query($auditsql);
		
							if ($auditsqlResult) {
								$auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid2 . "','update', '" . $TSId . "','ANML_Animals','" . $source . "')";
								$db->query($auditsqlStatus);	
							}				
						}
					}
					if ($procedure == "Termination") {
						$auditEventid3 = create_guid();
						if (empty($testSysBean->termination_date_c)) {
							$query_update = "UPDATE anml_animals_cstm SET termination_date_c = '" . $dateOccured . "' WHERE id_c = '" . $TSId . "'";
							 
							$db->query($query_update);				
							$auditsql = 'INSERT INTO anml_animals_audit  (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $TSId . '","' . $auditEventid3 . '",now(),"' . $current_user->id . '",now(),"termination_date_c","varchar","","' . $dateOccured . '")';
							$auditsqlResult = $db->query($auditsql);
		
							if ($auditsqlResult) {
								$auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid3 . "','update', '" . $TSId . "','ANML_Animals','" . $source . "')";
								$db->query($auditsqlStatus);				
							}		
						}
					}
					if ($bean->error_type_c == 'Standard Biocomp TS Selection') {
						$auditEventid2 = create_guid();
						if (empty($testSysBean->first_procedure_date_c)) {
							$first_procedure_date = date("Y-m-d", strtotime($bean->first_procedure_date));
							$query_update = "UPDATE anml_animals_cstm SET first_procedure_date_c = '" . $first_procedure_date . "' WHERE id_c = '" . $TSId . "'";
							//$GLOBALS['log']->fatal('Procedure Initial/Screening first_procedure_date_c query '.$query_update);
							$db->query($query_update);				
							$auditsql = 'INSERT INTO anml_animals_audit  (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
								values("' . create_guid() . '","' . $TSId . '","' . $auditEventid2 . '",now(),"' . $current_user->id . '",now(),"first_procedure_date_c","varchar","","' . $first_procedure_date . '")';
							$auditsqlResult = $db->query($auditsql);
		
							if ($auditsqlResult) {
								$auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid2 . "','update', '" . $TSId . "','ANML_Animals','" . $source . "')";
								$db->query($auditsqlStatus);	
							}				
						}
					}
				}
			}
		}
	}
}
