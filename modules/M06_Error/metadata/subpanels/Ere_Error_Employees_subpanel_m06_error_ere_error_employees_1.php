<?php
// created: 2018-05-11 15:12:46
$subpanel_layout['list_fields'] = array (
    'name' =>
        array (
            'vname' => 'LBL_NAME',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => 10,
            'default' => true,
        ),
    'date_entered' =>
        array (
            'type' => 'datetime',
            'studio' =>
                array (
                    'portaleditview' => false,
                ),
            'readonly' => true,
            'vname' => 'LBL_DATE_ENTERED',
            'width' => 10,
            'default' => true,
        ),
);