<?php
// created: 2020-01-08 15:32:54
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'error_category_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ERROR_CATEGORY',
    'width' => 10,
  ),
  'error_type_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ERROR_TYPE',
    'width' => 10,
  ),
  'actual_event_c' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_ACTUAL_EVENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'date_error_occurred_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_DATE_ERROR_OCCURRED',
    'width' => 10,
    'default' => true,
  ),
  'date_error_documented_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_DATE_ERROR_DOCUMENTED',
    'width' => 10,
    'default' => true,
  ),
  'date_time_discovered_text_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_DATE_TIME_DISCOVERED',
    'width' => 10,
    'default' => true,
  ),
);