<?php
// created: 2022-02-03 07:57:13
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'error_category_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ERROR_CATEGORY',
    'width' => 10,
  ),
  'error_type_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ERROR_TYPE',
    'width' => 10,
  ),
  'subtype_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_SUBTYPE',
    'width' => 10,
  ),
  'expected_event_c' => 
  array (
    'readonly' => false,
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_EXPECTED_EVENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'actual_event_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_ACTUAL_EVENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'date_error_occurred_c' => 
  array (
    'readonly' => false,
    'type' => 'date',
    'vname' => 'LBL_DATE_ERROR_OCCURRED',
    'width' => 10,
    'default' => true,
  ),
);