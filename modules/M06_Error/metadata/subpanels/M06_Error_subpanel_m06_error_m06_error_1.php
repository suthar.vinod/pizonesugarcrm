<?php
// created: 2020-07-07 08:32:30
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'error_type_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ERROR_TYPE',
    'width' => 10,
  ),
  'resolution_date_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_RESOLUTION_DATE',
    'width' => 10,
    'default' => true,
  ),
  'date_error_occurred_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_DATE_ERROR_OCCURRED',
    'width' => 10,
    'default' => true,
  ),
  'actual_event_c' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_ACTUAL_EVENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
);