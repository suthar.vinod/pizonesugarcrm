<?php
// created: 2022-03-03 07:21:40
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'expected_event_c' => 
  array (
    'readonly' => false,
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_EXPECTED_EVENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'actual_event_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_ACTUAL_EVENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'error_classification_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ERROR_CLASSIFICATION',
    'width' => 10,
  ),
  'error_type_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ERROR_TYPE',
    'width' => 10,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);