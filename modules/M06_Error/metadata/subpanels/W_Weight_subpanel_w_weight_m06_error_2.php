<?php
// created: 2021-09-14 09:25:34
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'submitter_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_SUBMITTER',
    'width' => 10,
    'default' => true,
  ),
  'error_category_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ERROR_CATEGORY',
    'width' => 10,
  ),
  'error_type_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ERROR_TYPE',
    'width' => 10,
  ),
  'expected_event_c' => 
  array (
    'readonly' => false,
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_EXPECTED_EVENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'actual_event_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_ACTUAL_EVENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'date_error_occurred_c' => 
  array (
    'readonly' => false,
    'type' => 'date',
    'vname' => 'LBL_DATE_ERROR_OCCURRED',
    'width' => 10,
    'default' => true,
  ),
);