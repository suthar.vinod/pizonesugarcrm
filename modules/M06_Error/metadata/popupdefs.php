<?php
$popupMeta = array (
    'moduleMain' => 'M06_Error',
    'varName' => 'M06_Error',
    'orderBy' => 'm06_error.name',
    'whereClauses' => array (
  'name' => 'm06_error.name',
),
    'searchInputs' => array (
  0 => 'm06_error_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
),
);
