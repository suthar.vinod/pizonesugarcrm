<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class create_usda_id_record
{
    static $already_ran = false;

    function create_usda_id_record_after_save($bean, $event, $arguments)
    {
        if (self::$already_ran == true)
            return;   //So that hook will only trigger once
        self::$already_ran = true;

        global $db, $current_user;

        $type = $bean->error_type_c;
        $com_usda_id = $bean->com_usda_id_c;
        
        $date_error_occurred = date("Y-m-d", strtotime($bean->date_error_occurred_c));
        $actual_event = $bean->actual_event_c; 

        $related_data = prepareRelatedRecordData($bean->related_data);
        
        if(count($related_data['ANML_Animals'])>0)
        {
            $testSystem_ID = $related_data['ANML_Animals'][0]['id'];
            $testSystemID = str_replace('"', '', $testSystem_ID);
            
            $TsBean = BeanFactory::retrieveBean('ANML_Animals', $testSystemID);
            $usda_id = $TsBean->usda_id_c;
            $TsBean->com_usda_id = $com_usda_id;
            $GLOBALS['log']->fatal('TsBean->usda_id_d ' . $TsBean->usda_id_c);
            
        }

        if ($type == 'Re_identification' && $arguments['isUpdate'] == false && !empty($testSystemID)) {

            $NewUsdaBean = BeanFactory::newBean('USDA_Historical_USDA_ID');
            $new_bean_id = create_guid();
            $NewUsdaBean->id = $new_bean_id; 
            $NewUsdaBean->new_with_id = true;
            $NewUsdaBean->fetched_row = null;

            $NewUsdaBean->name =  $com_usda_id;       
            $NewUsdaBean->old_usda_id_c =  $usda_id;       
            $NewUsdaBean->date_of_reidentification =  $date_error_occurred;       
            $NewUsdaBean->description =  $actual_event; 
            $NewUsdaBean->save();

            $NewUsdaBean->load_relationship('anml_animals_usda_historical_usda_id_1'); 
            $NewUsdaBean->anml_animals_usda_historical_usda_id_1->add($testSystemID);

        }
    }
}


function prepareRelatedRecordData($related_data)
{
    global $db;
    $data = array();
    foreach ($related_data as $module => $value) {
        $data[$module] = array();
        //$ids = jsonDecode($value);
        $ids = $value;
        if (gettype($ids) == 'array') {
            foreach ($ids as $id) {
                if (!empty($id) && $id != "null" && $id != "[]") {
                    if ($module != "ANML_Animals") {
                        $SugarQuery = new SugarQuery();
                        $SugarQuery->from(BeanFactory::newBean($module));
                        $SugarQuery->where()->equals('id', $id);
                        $SugarQuery->select(array('name'));
                        $query = $SugarQuery->compile();
                        $result = $SugarQuery->execute();
                        $data[$module][] = array("name" => $result[0]['name'], "id" => $id);
                    } else {
                        /*$SugarQuery = new SugarQuery();
                        $SugarQuery->from(BeanFactory::newBean($module), array('alias' => 'aa'));
                        $SugarQuery->joinTable('anml_animals_cstm', array(
                            'joinType' => 'INNER',
                            'alias' => 'aa_cstm',
                            'linkingTable' => true,
                        ))->on()->equalsField('aa_cstm.id_c', 'aa.id');
                        $SugarQuery->where()->equals('aa.id', $id);
                        $SugarQuery->select(array('aa.name', 'aa_cstm.usda_id_c'));
                        $query = $SugarQuery->compile();
                        $result = $SugarQuery->execute();*/

                        $ts_sql = "SELECT aa.name,aac.usda_id_c FROM anml_animals AS aa INNER JOIN anml_animals_cstm as aac ON aa.id = aac.id_c WHERE aa.id ='".$id."'";
                        $ts_exec = $db->query($ts_sql);
                        $result = $db->fetchByAssoc($ts_exec); 
                        $data[$module][] = array("name" => $result['name'], "usda_id_c" => $result['usda_id_c'], "id" => $id);
                    }
                }
            }
        }
    }

    return $data;
}

function jsonDecode($arr)
{
    $result = str_replace("\\\"", '', $arr);
    $result = str_replace("[", '', $result);
    $result = str_replace("]", '', $result);
    $result = explode(",", $result);
    return $result;
}
