<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class UpdatedeviationCDUonunlink
{
    function UpdatedeviationCDUonunlinkRecord($bean, $event, $arguments)
    {
        global $db,$current_user;        

        $module = $arguments['module'];
        $related_module = $arguments['related_module'];

        /*Get the month from month end without leading zero */       
        $erd_doc_id = $arguments['related_id'];
        //die();
        if (($module == 'M06_Error') && ($related_module == 'Erd_Error_Documents'))
        {

            $related_erd_id = $erd_doc_id;
            /*Get all the CDU record based on related erd record*/

            $sqlgetallCDU = "SELECT T2.*,T1.id AS cduid,T1.name,T1.date_entered, T1.month_end,T1.controlled_document_uti,T3.* FROM erd_error_documents_cdu_cd_utilization_1_c AS T2

                    LEFT JOIN cdu_cd_utilization AS T1 ON T1.id = T2.erd_error_documents_cdu_cd_utilization_1cdu_cd_utilization_idb

                    LEFT JOIN cdu_cd_utilization_cstm AS T3 ON T1.id = T3.id_c

                    WHERE T2.erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida='" . $related_erd_id . "' AND T2.deleted=0 AND T1.deleted=0 ORDER BY T1.date_entered desc";

            $resultCDU = $db->query($sqlgetallCDU);
           
            if ($resultCDU->num_rows > 0)
            {
                while ($rowCDU = $db->fetchByAssoc($resultCDU))
                {
                    $cduid = $rowCDU['cduid'];
                    $name = $rowCDU['name'];
                    $month_end = $rowCDU['month_end'];
                    $olddeviation_rate_c = $rowCDU['deviation_rate_c'];
                    /*Get the month from month end without leading zero */
                    $CDU_MonthEnd = date('n', strtotime($month_end));
                    $CDU_year = date('Y', strtotime($month_end));
                    $controlled_document_uti = $rowCDU['controlled_document_uti'];                  

                    /*Get all the CDU record from erd record*/
                    $sqlComm = "SELECT count(*) as NUM, T2.*,T1.id,T1.name,T3.date_error_occurred_c,T3.error_classification_c,MONTH(T3.date_error_occurred_c) as dateoccmonth 
            
                FROM m06_error_erd_error_documents_1_c AS T2

                LEFT JOIN m06_error AS T1 ON T2.m06_error_erd_error_documents_1m06_error_ida = T1.id

                LEFT JOIN m06_error_cstm AS T3 ON T1.id = T3.id_c

                WHERE T2.m06_error_erd_error_documents_1erd_error_documents_idb='" . $related_erd_id . "' AND T3.error_classification_c='error' AND MONTH(T3.date_error_occurred_c)='" . $CDU_MonthEnd . "' AND YEAR(T3.date_error_occurred_c)='" . $CDU_year . "' AND T2.deleted=0";

                    $resultComm = $db->query($sqlComm);

                    $rowComm = $db->fetchByAssoc($resultComm);

                    $NoofCommRecord = $rowComm['NUM'];
                    // if ($NoofCommRecord > 0)
                    //{
                   

                    $controlled_document_uti = $controlled_document_uti;
                    
                    //$controlled_document_uti = "96.25";

                  

                    if ($controlled_document_uti != '')
                    {
                        $final_cal = $NoofCommRecord / $controlled_document_uti * 100;

                        $deviation_rate_c = number_format($final_cal, 2);

                    }
                    else
                    {
                        $deviation_rate_c = "0.00";
                    }

                    $bean->deviation_rate_c = $deviation_rate_c;

                  

                    $sqlUpdate = "UPDATE `cdu_cd_utilization_cstm` SET `deviation_rate_c`='" . $bean->deviation_rate_c . "' WHERE id_c='" . $cduid . "'";

                    $db->query($sqlUpdate);

                 
                    $auditEventid = create_guid();

                    /** Insert into audit log */
                     $auditsqlCDU = 'INSERT INTO cdu_cd_utilization_audit 
                     (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string)
                     values("' . create_guid() . '","' . $cduid . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"deviation_rate_c","decimal","' . $olddeviation_rate_c . '","' . $deviation_rate_c . '")';
                     
                     $db->query($auditsqlCDU);

                    /*Delete from audit where befor and after value having same value */

                    $deleteaudit = 'DELETE FROM cdu_cd_utilization_audit WHERE before_value_string = after_value_string AND parent_id = "' . $cduid . '"';
                    
                    $db->query($deleteaudit);

                    

                    //}
                    
                }

            }
        }

    }

}

