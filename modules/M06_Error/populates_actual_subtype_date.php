<?php
/* Ticket#1833 Autopopulate Actual Subtype Date - mgt assessment redo */
class populates_actual_subtype_date
{
	function populates_actual_subtype_date_save($bean, $event, $arguments)
	{
		global $db;
		global $app_list_strings;
		$current_date = date("Y-m-d"); //current date
		$subtype_dom	 	= $app_list_strings['error_type_list'];

		if($bean->subtype_c!="")
		{
			$subtypeArr = explode(",",str_replace("^","",$bean->subtype_c));					
			for ($k = 0; $k < count($subtypeArr); $k++) {
				$subtypeArr2[] =  $subtype_dom[$subtypeArr[$k]]; 				
			}
			if(count($subtypeArr2)>0){
				$subtypeList = implode(", ",$subtypeArr2);				
			}
		}
		if($bean->fetched_row['subtype_c']!="")
		{
			$subtypeArrf = explode(",",str_replace("^","",$bean->fetched_row['subtype_c']));					
			for ($k = 0; $k < count($subtypeArrf); $k++) {
				$subtypeArr3[] =  $subtype_dom[$subtypeArrf[$k]]; 				
			}
			if(count($subtypeArr3)>0){
				$subtypeList2 = implode(", ",$subtypeArr3);				
			}
		}
		if ($bean->subtype_hidden_field_c == "" && $subtypeList != $subtypeList2) {
            if ($bean->subtype_hidden_field_c == ""){
                $bean->subtype_hidden_field_c = $subtypeList;
				$bean->actual_management_assessment_c = $current_date;				
            }
		}
	}
}
