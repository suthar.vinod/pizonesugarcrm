<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class saveNotificationEmail {
	function saveNotificationRecord($bean, $event, $arguments) {
		//global $db,$current_user; 
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$domainToSearch = 'apsemail.com';
	
		global $db,$sugar_config,$current_user; 
		global $app_list_strings;
		
		$subtype_dom	 	= $app_list_strings['error_type_list'];
        $category_dom	 	= $app_list_strings['category_list'];
		//$enrollment_dom = $app_list_strings['enrollment_activities_list'];
		$activities_dom 	= $app_list_strings['wps_outcome_activities_list'];
		$classification_dom = $app_list_strings['error_classification_c_list'];
		$whyHappened_dom 	= $app_list_strings['why_it_happened_c_list'];
		$type_dom 			= $app_list_strings['error_category_list'];
		$impactful_dom 			= $app_list_strings['impactful_list'];

		if($bean->id == $bean->fetched_row['id'] &&( 
			($bean->resolution_c != $bean->fetched_row['resolution_c']) ||
			($bean->error_classification_c != $bean->fetched_row['error_classification_c'] && ($bean->error_classification_c != "" || $bean->fetched_row['error_classification_c'] != "Choose One")) ||
			($bean->expected_event_c != $bean->fetched_row['expected_event_c']) ||
			($bean->actual_event_c != $bean->fetched_row['actual_event_c'])  ||
			($bean->impactful_dd_c != $bean->fetched_row['impactful_dd_c']  && $bean->fetched_row['impactful_dd_c'] != "" )) 
			) {
			$workProductId = "";			
			$commID = $bean->id;
			$commName = $bean->name;
			$departmentName   = "Scientific";


			/*Get Study Director and Email*/
			$wp_sql = "SELECT m06_error_m03_work_product_1m03_work_product_idb AS WPID  FROM `m06_error_m03_work_product_1_c` AS COMM_WP 
						WHERE COMM_WP.`m06_error_m03_work_product_1m06_error_ida`='".$commID."' 
						AND deleted=0 ORDER BY `COMM_WP`.`date_modified` DESC limit 1";
			$wp_exec = $db->query($wp_sql);
			if ($wp_exec->num_rows > 0) 
			{
				$result = $db->fetchByAssoc($wp_exec);
				$workProductId = $result['WPID']; 
				
				$WP_bean = BeanFactory::getBean('M03_Work_Product', $workProductId);			
				$stdy_id = $WP_bean->contact_id_c;
				$wp_compliance = $WP_bean->work_product_compliance_c;
				$wp_name_c = $WP_bean->name;
				$stdy_bean = BeanFactory::getBean('Contacts', $stdy_id);
				$studyDirectorName = $stdy_bean->name;
				$SDEmailAddress = $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
				
				$wp_codeId		= $WP_bean->m03_work_product_code_id1_c;
				$WPC_bean		= BeanFactory::getBean('M03_Work_Product_Code', $wp_codeId);
				$WPC_Name       = $WPC_bean->name;
			}

			
			/* End Study Director Email */

				$ComBean = BeanFactory::retrieveBean('M06_Error',$commID);
				
				/* Get Related Comm ID */
				$RelatedCommData = $this->getRelateComID($commID);

				/* Get Related Room(s) */
				$RelatedRoomsData = $this->getRelateRoomID($commID);

				/* Get Related Document(s) */
				$RelatedDocumentData = $this->getRelateDocumentID($commID);

				/* Get Related Equipment & Facilities(s) */
				$RelatedEquipmentData = $this->getRelateEquipmentID($commID);

				/* Get Test System Data */
				$RelatedTestSysData = $this->getRelateTestID($commID);

				/* Get Test System Name Data */
				$RelatedTestSysNameData = $this->getRelateTestSysID($commID);
				
				/* Get Responsible Entry Review Data */
				$RelateComIDList = $this->getResponsibleComData($commID);
				
				$managerData 		= $RelateComIDList['manager_name_list'];
				$ReviewEmailData	= $RelateComIDList['reviewEmails'];
				$EntryEmailData		= $RelateComIDList['entryEmails']; 
				
				$oneUpManagerData	= $RelateComIDList['oneUpmanager_name']; 
				$departmentData		= $RelateComIDList['contact_dept']; 
				
				/* End to get Responsible_Personnel_Entry and Responsible_Personnel_Review*/

				// $fields = $ComBean->fetched_row;					
				$actual_sd_ack_date_c = "";
				$date_error_occurred_c = "";
				$date_error_documented_c = "";
				$date_time_discovered_text_c = "";
				$date_time_discovered_c = "";
				$resolution_date_c = "";
				$reinspection_date_c = "";

				//$date_discovered				 = $bean->date_error_documented_c;

				if($bean->actual_sd_ack_date_c!=""){
					$actual_sd_ack_date_c = date("m/d/Y",strtotime($bean->actual_sd_ack_date_c));
				}
				if($bean->resolution_date_c!=""){
					$resolution_date_c = date("m/d/Y",strtotime($bean->resolution_date_c));
				}
				if($bean->date_error_occurred_c!=""){
					$date_error_occurred_c = date("m/d/Y",strtotime($bean->date_error_occurred_c));
				}
				if($bean->date_error_documented_c!=""){
					$date_error_documented_c = date("m/d/Y",strtotime($bean->date_error_documented_c));
				}
				if($bean->reinspection_date_c!=""){
					$reinspection_date_c = date("m/d/Y",strtotime($bean->reinspection_date_c));
				}
				
				if(empty($date_error_documented_c) && $bean->date_time_discovered_c!="")
				{
					$offset1 =  -18000;
					//$offset1 =  -21600; //daylight Saving
					$offset_time = strtotime($bean->date_time_discovered_c) + $offset1;
					$date_time_discovered_c = date("m/d/Y H:i:s",$offset_time);

					$sep_date_time = explode(" ", $date_time_discovered_c);
                    if (count($sep_date_time) > 1) {
                        $date_error_documented_c = $sep_date_time[0];                         
                    }
				}
				
				$subtypeList = "";
				if($bean->subtype_c!="")
				{
					$subtypeArr = explode(",",str_replace("^","",$bean->subtype_c));
					
					for ($k = 0; $k < count($subtypeArr); $k++) {
						$subtypeArr2[] =  $subtype_dom[$subtypeArr[$k]]; 
					}
					if(count($subtypeArr2)>0){
						$subtypeList = implode(", ",$subtypeArr2);
					}						
					
				}
				
				$arrFind =  array("<",">");
				$arrReplace =  array("&lt;","&gt;");
				
				$expected_event_c = str_replace($arrFind,$arrReplace,$bean->expected_event_c); //strip_tags($bean->expected_event_c);
				$actual_event_c   = str_replace($arrFind,$arrReplace,$bean->actual_event_c);  //strip_tags($bean->actual_event_c);		
				$category 		  = $category_dom[$bean->error_category_c];	
				$studyDirector    = $studyDirectorName."(".$SDEmailAddress.")";
				$error_classification_c = $classification_dom[$bean->error_classification_c];
				$impactful = $impactful_dom[$bean->impactful_dd_c];
				$id = $bean->id;
				$name = $bean->name;
				$resolution_c 		= str_replace($arrFind,$arrReplace,$bean->resolution_c);  
				$error_type_c 		= $type_dom[$bean->error_type_c];				
				$test_system_name_c = $bean->test_system_name_c;
				$submitter_c 	  	= $bean->submitter_c;	
				$why_it_happened_c 	= $whyHappened_dom[$bean->why_it_happened_c];
				$details_why_happened_c = str_replace($arrFind,$arrReplace,$bean->details_why_happened_c);  
				$recommendation 	= $bean->recommendation_c;
				$critical_phase_inspection = $bean->a1a_critical_phase_inspectio_m06_error_1_name;
				/*Get Enrollment Activities*/
				$activitiesData = "NA";
				
				$activitiesArr = explode(",",str_replace("^","",$bean->activities_c));
				foreach ($activitiesArr as $idx => $actRecord) {
					$activitiesDisplayArr[] = $activities_dom[$actRecord];
				} 
				$activitiesData		 = !empty($activitiesDisplayArr)?implode(", ",$activitiesDisplayArr):"NA";

				$fields = array(
								'id'=>$id,
								'name'=>$name,
								'actual_sd_assessment_date_c'=>($reinspection_date_c!="")?$reinspection_date_c:"",
								'resolution_c'=>($resolution_c!="")?$resolution_c:"",
								'error_classification_c'=>($error_classification_c!="")?$error_classification_c:"",
								'impactful'=>($impactful!="")?$impactful:"",
								'error_type_c'=>($error_type_c!="")?$error_type_c:"NA",
								'subtype_c'=>($subtypeList!="")?$subtypeList:"NA", 
								'wp_name_c'=>($wp_name_c!="")?$wp_name_c:"NA",
								'study_director_c'=>($studyDirector!="")?$studyDirector:"NA",
								'test_system_name_c'=>$RelatedTestSysData,
								'test_system_name_data'=>$RelatedTestSysNameData,
								'compliance_c'=>($wp_compliance!="")?$wp_compliance:"NA",
								'submitter_c'=>($submitter_c!="")?$submitter_c:"NA",
								'error_category_c'=>($category!="")?$category:"NA",
								'date_error_occurred_c'=>($date_error_occurred_c!="")?$date_error_occurred_c:"NA",
								'date_error_documented_c'=>($date_error_documented_c!="")?$date_error_documented_c:"NA",
								'date_time_discovered_text_c'=>($date_time_discovered_c!="")?$date_time_discovered_c:"NA",
								'resolution_date_c'=>($resolution_date_c!="")?$resolution_date_c:"NA",
								'EntryEmailData'=>($EntryEmailData!="")?$EntryEmailData:"NA",
								'ReviewEmailData'=>($ReviewEmailData!="")?$ReviewEmailData:"NA",
								'ManagerData'=>($managerData!="")?$managerData:"NA",
								'activities'=>($activitiesData!="")?$activitiesData:"NA",
								'why_it_happened_c'=>($why_it_happened_c!="")?$why_it_happened_c:"NA",
								'details_why_happened_c'=>($details_why_happened_c!="")?$details_why_happened_c:"NA",
								'critical_phase_inspection'=>($critical_phase_inspection!="")?$critical_phase_inspection:"NA",
								'expected_event_c'=>($expected_event_c!="")?$expected_event_c:"NA",
								'actual_event_c'=>($actual_event_c!="")?$actual_event_c:"NA",
								'RelatedCommData'=>($RelatedCommData!="")?$RelatedCommData:"NA",
								'one_up_manager'=>($oneUpManagerData!="")?$oneUpManagerData:"NA",
								'room'=>$RelatedRoomsData,
								'document'=>$RelatedDocumentData,
								'equipment_facilities'=>$RelatedEquipmentData,								
								'recommendation'=>($recommendation!="")?$recommendation:"NA",
							);
							
							
				/** Code for sending email to Study Director**/
				if ($SDEmailAddress != "") 
				{
				
					$communication_name = '<a target="_blank"href="' . $site_url . '/#M06_Error/' . $bean->id . '">' . $bean->name . '</a>';

					$template = new EmailTemplate();
					$emailObj = new Email();
					/*Get Email Subject*/
					if($bean->resolution_c=="" && (
						($bean->expected_event_c != $bean->fetched_row['expected_event_c']) ||
						($bean->actual_event_c != $bean->fetched_row['actual_event_c']))){
							$getEmailSubject = $this->getEmailSubjectEdited($fields);
						}else{
							$getEmailSubject = $this->getEmailSubject($fields);
						}

					if ($error_classification_c == "Notification")
					{
						$template->retrieve_by_string_fields(array('name' => 'SD Assessment Notification Template for Classification', 'type' => 'email'));
					} else {
						$template->retrieve_by_string_fields(array('name' => 'SD Assessment Notification Template', 'type' => 'email'));
					}
					$template->body_html = str_replace('[COM_ID]', $communication_name, $template->body_html); 					
					$template->body_html = str_replace('[actual_sd_assessment_date_c]', $fields['actual_sd_assessment_date_c'], $template->body_html);
					$template->body_html = str_replace('[SD_Assessment]', $fields['resolution_c'], $template->body_html);
					$template->body_html = str_replace('[Classification]', $fields['error_classification_c'], $template->body_html);
					$template->body_html = str_replace('[Impactful]', $fields['impactful'], $template->body_html);
					$template->body_html = str_replace('[Subtype]', $fields['subtype_c'], $template->body_html);
					$template->body_html = str_replace('[Work_Product]', $fields['wp_name_c'], $template->body_html);
					$template->body_html = str_replace('[Study_Director]', $fields['study_director_c'], $template->body_html);
					$template->body_html = str_replace('[Compliance]', $fields['compliance_c'], $template->body_html);
					$template->body_html = str_replace('[Submitted_By]', $fields['submitter_c'], $template->body_html);
					$template->body_html = str_replace('[Category]', $fields['error_category_c'], $template->body_html);
					$template->body_html = str_replace('[Type]', $fields['error_type_c'], $template->body_html);
					$template->body_html = str_replace('[Date_Occurred]', $fields['date_error_occurred_c'], $template->body_html);
					$template->body_html = str_replace('[Resolution_Date]', $fields['resolution_date_c'], $template->body_html);
					$template->body_html = str_replace('[date_error_documented_c]', $fields['date_error_documented_c'], $template->body_html);
					$template->body_html = str_replace('[Date_Time_Discovered]', $fields['date_time_discovered_text_c'], $template->body_html);	
					$template->body_html = str_replace('[activities]', $fields['activities'], $template->body_html);				
					$template->body_html = str_replace('[Expected_Event]', $fields['expected_event_c'], $template->body_html);
					$template->body_html = str_replace('[Actual_Event]', $fields['actual_event_c'], $template->body_html);
					$template->body_html = str_replace('[Test_System]', $fields['test_system_name_c'], $template->body_html);
					$template->body_html = str_replace('[Responsible_Personnel_Entry]', $fields['EntryEmailData'], $template->body_html);
					$template->body_html = str_replace('[Responsible_Personnel_Review]', $fields['ReviewEmailData'], $template->body_html);
					$template->body_html = str_replace('[Department_Manager]', $fields['ManagerData'], $template->body_html);					
					$template->body_html = str_replace('[One_Up_Manager]', $fields['one_up_manager'], $template->body_html);
					$template->body_html = str_replace('[Room]', $fields['room'], $template->body_html);
					$template->body_html = str_replace('[Document]', $fields['document'], $template->body_html);
					$template->body_html = str_replace('[Equipment_Facilities]', $fields['equipment_facilities'], $template->body_html);		
					$template->body_html = str_replace('[Why_it_Happened]', $fields['why_it_happened_c'], $template->body_html);
					$template->body_html = str_replace('[Details_on_Why_it_Happened]', $fields['details_why_happened_c'], $template->body_html);
					$template->body_html = str_replace('[Critical_Phase_Inspection]', $fields['critical_phase_inspection'], $template->body_html);
					$template->body_html = str_replace('[Recommendation]', $fields['recommendation'], $template->body_html);
					$template->body_html = str_replace('[Related_COM_ID]', $fields['RelatedCommData'], $template->body_html);
					
					$defaults	= $emailObj->getSystemDefaultEmail();
					$mail		= new SugarPHPMailer();
					$mail->setMailerForSystem();
					$mail->IsHTML(true);
					$mail->From		= $defaults['email'];
					$mail->FromName = $defaults['name'];
					$mail->Subject	= $getEmailSubject;
					$mail->Body		= $template->body_html;						 
					
					$domainToSearch = 'apsemail.com';
					$domain_name = substr(strrchr($SDEmailAddress, "@"), 1);
					if (trim(strtolower($domain_name) == $domainToSearch)) {
						$mail->AddAddress($SDEmailAddress);
					}
					
					 

					$wpcode = "TX";
					$wpcodestring = $WPC_Name;
					// 748_Add_Harriet_on_TX_SD_Assessment_emails if Work Product Code begins with "TX."
					if(strpos($wpcodestring, $wpcode) !== false){
						$mail->AddAddress( 'hkamendi@apsemail.com');
					}

					//$mail->AddAddress('fxs_mjohnson@apsemail.com');

					//$mail->AddAddress('mconforti@apsemail.com');
					$mail->AddAddress('cleet@apsemail.com');
					$mail->AddAddress('ablakstvedt@apsemail.com');
					$mail->AddAddress('hackerson@apsemail.com');
					$mail->AddAddress('jpomonis@apsemail.com'); 
					$mail->AddAddress('cvaleri@apsemail.com');  

					if (!$mail->Send()) {
							$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
						} else {
							$GLOBALS['log']->debug('email sent');
						}
						unset($mail);

					/*Save Document after sending email if Work product compliance is GLP*/
					if($wp_compliance == "GLP")
					{		    
						
						$lastNumber = 0;
						$emailDocId  = "";
						$edoc_sql = "SELECT `id`,`type_2`,`document_name` FROM `edoc_email_documents` AS EDOC 
									 WHERE EDOC.`document_name` LIKE '".$commName."%.pdf' 									
									 AND deleted=0 ORDER BY `EDOC`.`date_entered` ASC";
						
						$edoc_exec = $db->query($edoc_sql);
						while($resultEdoc = $db->fetchByAssoc($edoc_exec)){
							$type_2		= $resultEdoc['type_2'];
							$eDocName	= $resultEdoc['document_name'];
							
							if($type_2=="SD Assessment Confirmation"){
								$eDocNewName = $eDocName;	
								$emailDocId  = $resultEdoc['id'];
							}else{
								$eDocNameStr = str_replace(".pdf", "", $eDocName);
								$eDocNameArr = explode(" ",$eDocNameStr);
								$lastNumber  = (int)trim($eDocNameArr[1]);
							}
						}
						
						if($emailDocId==""){
							$nextNumber = $lastNumber+1;
							$nextNumber  = str_pad($nextNumber,2,"0",STR_PAD_LEFT);
							$eDocNewName = $commName." ".$nextNumber.".pdf";
							
							//Create bean
							$emailDocBean = BeanFactory::newBean('EDoc_Email_Documents');
							$emailDocBean->document_name	= $eDocNewName; 
							$emailDocBean->department		= $departmentName;//'Scientific'; 
							$emailDocBean->type_2			= "SD Assessment Confirmation"; 
							$emailDocBean->assigned_user_id	= $current_user->id;
							
							$emailDocBean->filename	= $eDocNewName;
							$emailDocBean->file_ext	= 'pdf';
							$emailDocBean->file_mime_type	= 'application/pdf';	
							
							$emailDocBean->save();
							$emailDocId = $emailDocBean->id;

							if($workProductId!="" && $emailDocId!="" ){
								$gid = create_guid();//strtolower($GUID);
								$sql_msb = "INSERT INTO `m03_work_product_edoc_email_documents_1_c` (id,m03_work_product_edoc_email_documents_1m03_work_product_ida, m03_work_product_edoc_email_documents_1edoc_email_documents_idb) VALUES ('".$gid."','".$workProductId."', '".$emailDocId."')";
								$db->query($sql_msb);
							} 
						}
						
						 
							$ss = new Sugar_Smarty();
							$ss->security = true;
							$ss->security_settings['PHP_TAGS'] = false;
							if (defined('SUGAR_SHADOW_PATH')) {
								$ss->secure_dir[] = SUGAR_SHADOW_PATH;
							}
							$ss->assign('MOD', $GLOBALS['mod_strings']);
							$ss->assign('APP', $GLOBALS['app_strings']);
							
							$arrFind =  array("<",">");
							$arrReplace =  array("&lt;","&gt;");
							$fields['resolution_c'] 			=	str_replace($arrFind,$arrReplace,$fields['resolution_c']);
							$fields['expected_event_c'] 			=	str_replace($arrFind,$arrReplace,$fields['expected_event_c']);
							$fields['actual_event_c'] 				=	str_replace($arrFind,$arrReplace,$fields['actual_event_c']);
							$fields['details_why_happened_c'] 	=	str_replace($arrFind,$arrReplace,$fields['details_why_happened_c']);
									
							$ss->assign('fields', $fields);

							if ($error_classification_c == "Notification")
							{								
								$html2 = $ss->fetch('custom/modules/M06_Error/tpls/ClassificationEmailTextPDF.tpl');
							} else {
								$html2 = $ss->fetch('custom/modules/M06_Error/tpls/EmailTextPDF.tpl');
							}
							
							$top_html = "";  
							
							$offset =  -18000;
							//$offset =  -21600; //daylight Saving
							$offset_time = time() + $offset +1;
							$formattedDate = date("D m/d/Y g:i a",$offset_time);

							$to_Arr = array();			

							$to_Arr[] = $studyDirector;
							$to_Arr[] =  "Michael Conforti(mconforti@apsemail.com)";
							$to_Arr[] =  "Corey Leet(cleet@apsemail.com)";
							$to_Arr[] =  "Adam Blakstvedt(ablakstvedt@apsemail.com)";
							$to_Arr[] =  "HeatherAckerson(hackerson@apsemail.com)";
							$to_Arr[] =  "James Pomonis(jpomonis@apsemail.com)";
							
							$top_html = '<span style="font-size:8pt;">'.$formattedDate.'</span>';
							$top_html .= '<br/><span style="font-size:8pt;">From: PMT (do_not_reply@apsemail.com)</span>';
							$top_html .= '<br/><span style="font-size:8pt;">Subject: '.$getEmailSubject.'</span>';
							$top_html .= '<br/><span style="font-size:8pt;">To: '.implode(",", $to_Arr).'</span>';
							
							$borderHtml = '<p><span style="font-size:10pt;">----------------------------------------------------------------------------------------------------------------------------------------------------------------</span></p>';
							$body_html_new = $top_html.$borderHtml.$html2;
							
							$fileName = $eDocNewName;
							$fileDocID = $emailDocId; 
							$fileInfo = $this->create_PDF($body_html_new, $fileName);
							$objFile = new UploadFile('uploadfile');
							// Set the filename
							$objFile->file_ext = 'pdf';
							$objFile->set_for_soap($fileName, (sugar_file_get_contents($fileInfo['file_path'])));
							//$objFile->set_for_soap($fileName, $html2);
							$objFile->create_stored_filename();
							$objFile->final_move($fileDocID);
						
					}				
					/*ENd Email document creation*/					
				}
			}
		}	
	
	function get_PDF_html($pdf_id)
    {
        $html = '';
          $page_break = '<div style="page-break-before:always">&nbsp;</div>';
          $is_first_page = true;
          $template = array();
          $pdf_template = new PdfManager();
          $pdf_template->retrieve($pdf_id);
          $template['id'] = $pdf_template->id;
          $template['name'] = $pdf_template->name;
          $template['type'] = $pdf_template->base_module;
          $template['html'] = $pdf_template->body_html;
          return $template;
    }
	function create_PDF($html, $name)
     {
          require_once('vendor/tcpdf/tcpdf.php');
          $tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
          $tcpdf->SetCreator(PDF_CREATOR);
          $name = str_replace(' ', '_', $name);
          $date = date('Y-m-d');		  
		  $file_info['file_path'] = "upload/".$name;
		  $file_info['file_name'] = $name;  
          
          $tcpdf->AddPage();
          $tcpdf->writeHTML($html, true, false, true, false, '');
          $tcpdf->Output($file_info['file_path'], "F");
          return $file_info;
    }

	/*Get Related COmm ID*/
	function getRelateComID($commID){
		global $db;
		$related_name_list = array();
		$queryComm = "SELECT 
							COMM.id AS communicationID,
							COMM.name AS CommunicationName,
							relComm.m06_error_m06_error_1m06_error_idb AS ReletedCommID
							FROM m06_error AS COMM		
							RiGHT JOIN m06_error_m06_error_1_c AS relComm ON  relComm.m06_error_m06_error_1m06_error_ida = COMM.id
							WHERE COMM.id = '".$commID."' AND relComm.deleted=0";

		$related_comm_exec = $db->query($queryComm);
		if ($related_comm_exec->num_rows > 0)
		{
			while($resultrelatedcomm = $db->fetchByAssoc($related_comm_exec)){
				$relatedCommBean = BeanFactory::getBean('M06_Error', $resultrelatedcomm['ReletedCommID']);
				$related_name_list[] = $relatedCommBean->name;
			}						
		}
					
		return !empty($related_name_list)?implode(", ",$related_name_list):"NA";
	}


	/*Get Related Rooms*/
	function getRelateRoomID($commID){
		global $db;
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$related_room_list = array();
		
		$room_sql = "SELECT m06_error_rms_room_1rms_room_idb AS ROOMID  FROM `m06_error_rms_room_1_c` AS COMM_ROOM 
					 WHERE COMM_ROOM.`m06_error_rms_room_1m06_error_ida`='".$commID."' AND deleted=0";	
		
		$related_room_exec = $db->query($room_sql);
		if ($related_room_exec->num_rows > 0)
		{
			while($resultrelatedroom = $db->fetchByAssoc($related_room_exec)){
				$relatedRoomBean = BeanFactory::getBean('RMS_Room', $resultrelatedroom['ROOMID']);
				$related_room_list[] = '<a target="_blank" href="' . $site_url . '/#RMS_Room/' . $resultrelatedroom['ROOMID'] . '">' .$relatedRoomBean->name . '</a>';
			}						
		}
					
		return !empty($related_room_list)?implode(", ",$related_room_list):"NA";
	}
	
	/*Get Related Document(s)*/
	function getRelateDocumentID($commID){
		global $db;
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$related_document_list = array();
		
		$document_sql = "SELECT m06_error_erd_error_documents_1erd_error_documents_idb AS DOCID  FROM `m06_error_erd_error_documents_1_c` AS COMM_DOC 
						 WHERE COMM_DOC.`m06_error_erd_error_documents_1m06_error_ida`='".$commID."' AND deleted=0";	
		
		$related_document_exec = $db->query($document_sql);
		if ($related_document_exec->num_rows > 0)
		{
			while($resultrelateddocument = $db->fetchByAssoc($related_document_exec)){
				$relatedDocumentBean = BeanFactory::getBean('Erd_Error_Documents', $resultrelateddocument['DOCID']);
				$related_document_list[] = '<a target="_blank" href="' . $site_url . '/#Erd_Error_Documents/' . $resultrelateddocument['DOCID'] . '">' .$relatedDocumentBean->name . '</a>';	
			}						
		}					
		return !empty($related_document_list)?implode(", ",$related_document_list):"NA";
	}

	/*Get Related Equipment & Facilities(s)*/
	function getRelateEquipmentID($commID){
		global $db;
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$related_equipment_list = array();
		
		$equipment_sql = "SELECT m06_error_equip_equipment_1equip_equipment_idb AS EQUID  FROM `m06_error_equip_equipment_1_c` AS COMM_EQU
						  WHERE COMM_EQU.`m06_error_equip_equipment_1m06_error_ida`='".$commID."' AND deleted=0";	
		
		$related_equipment_exec = $db->query($equipment_sql);
		if ($related_equipment_exec->num_rows > 0)
		{
			while($resultrelatedequipment = $db->fetchByAssoc($related_equipment_exec)){
				$relatedEquipmentBean = BeanFactory::getBean('Equip_Equipment', $resultrelatedequipment['EQUID']);
				$related_equipment_list[] = '<a target="_blank" href="' . $site_url . '/#Equip_Equipment/' . $resultrelatedequipment['EQUID'] . '">' .$relatedEquipmentBean->name . '</a>';				
			}						
		}
					
		return !empty($related_equipment_list)?implode(", ",$related_equipment_list):"NA";
	}
		
	/*Get Related Test Systems*/
	function getRelateTestID($commID){
		global $db;
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$related_test_sys_list = array();
		
		$test_sys_sql = "SELECT m06_error_anml_animals_1anml_animals_idb AS TESTID  FROM `m06_error_anml_animals_1_c` AS COMM_TEST
		WHERE COMM_TEST.`m06_error_anml_animals_1m06_error_ida`='".$commID."' AND deleted=0";	
		
		$related_test_sys_sql_exec = $db->query($test_sys_sql);
		if ($related_test_sys_sql_exec->num_rows > 0)
		{
			while($resultrelatedtestsys = $db->fetchByAssoc($related_test_sys_sql_exec)){
				$relatedTestsysBean = BeanFactory::getBean('ANML_Animals', $resultrelatedtestsys['TESTID']);
				$related_test_sys_list[] =  '<a target="_blank" href="' . $site_url . '/#ANML_Animals/' . $resultrelatedtestsys['TESTID'] . '">' .$relatedTestsysBean->name . '</a>';
			}						
		}
					
		return !empty($related_test_sys_list)?implode(", ",$related_test_sys_list):"NA";
	}

	/*Get Related Test Systems without link*/
	function getRelateTestSysID($commID){
		global $db;
		$related_test_sys_name_list = array();
		
		$test_sys_name_sql = "SELECT m06_error_anml_animals_1anml_animals_idb AS TESTID  FROM `m06_error_anml_animals_1_c` AS COMM_TEST
		WHERE COMM_TEST.`m06_error_anml_animals_1m06_error_ida`='".$commID."' AND deleted=0";	
		
		$related_test_sys_name_sql_exec = $db->query($test_sys_name_sql);
		if ($related_test_sys_name_sql_exec->num_rows > 0)
		{
			while($resultrelatedtestsysname = $db->fetchByAssoc($related_test_sys_name_sql_exec)){
				$relatedTestsystemBean = BeanFactory::getBean('ANML_Animals', $resultrelatedtestsysname['TESTID']);
				$related_test_sys_name_list[] =  $relatedTestsystemBean->name;
			}						
		}
					
		return !empty($related_test_sys_name_list)?implode(", ",$related_test_sys_name_list):"NA";
	}

	/* Get Responsible_Personnel_Entry and Responsible_Personnel_Review */
	function getResponsibleComData($commID){
		global $db;
		$responsibleData = array();
		$reviewEmails = array();
		$entryEmails = array();
		$manager_name_list = array(); 
		$oneUpmanager_name = array();
		
		$queryResponsible = "SELECT
								COMM.id AS communicationID,
								COMM.name AS CommunicationName,
								CONTACTEMPLOYEE.contacts_de_deviation_employees_1contacts_ida AS CONTACTID,
								EMPLOYEE.id,
								EMPLOYEECSTM.deviation_type_c,
								CONTACT.first_name,
								CONTACT.last_name,
								CONTACTCSTM.contact_id_c AS MGTID,
								CONTACTCSTM.contact_id1_c AS oneUpMGTID,
								CONTACTCSTM.department_id_c AS department
								FROM `m06_error` AS COMM
								INNER JOIN m06_error_cstm AS COMM_CSTM ON COMM.id = COMM_CSTM.id_c
								RIGHT JOIN m06_error_de_deviation_employees_2_c AS COMMDEV
								ON COMM.id = COMMDEV.m06_error_de_deviation_employees_2m06_error_ida
								INNER JOIN de_deviation_employees AS EMPLOYEE
								ON COMMDEV.m06_error_de_deviation_employees_2de_deviation_employees_idb=EMPLOYEE.id
								INNER JOIN contacts_de_deviation_employees_1_c AS CONTACTEMPLOYEE
								ON EMPLOYEE.id =CONTACTEMPLOYEE.contacts_de_deviation_employees_1de_deviation_employees_idb
								INNER JOIN de_deviation_employees_cstm AS EMPLOYEECSTM
								ON EMPLOYEE.id=EMPLOYEECSTM.id_c
								LEFT JOIN contacts AS CONTACT
								ON CONTACT.id=CONTACTEMPLOYEE.contacts_de_deviation_employees_1contacts_ida
								LEFT JOIN contacts_cstm AS CONTACTCSTM ON CONTACT.id=CONTACTCSTM.id_c
								WHERE COMM.deleted = 0 AND COMM.id = '".$commID."' ";

				$responsible_exec = $db->query($queryResponsible);
				if ($responsible_exec->num_rows > 0)
				{
					while ($resultResponsible = $db->fetchByAssoc($responsible_exec)) {
						$ResType = $resultResponsible['deviation_type_c'];						
						if($ResType=="Entry")
							$entryResponsibleArr[] = $resultResponsible['CONTACTID'];
						if($ResType=="Review")
							$reviewResponsibleArr[] = $resultResponsible['CONTACTID'];
						$managersArr[] = $resultResponsible['MGTID']; //manager id	
						$onupmanagersArr[] 	= $resultResponsible['oneUpMGTID']; //oneUp manager id
						$contact_dept[]		= $resultResponsible['department']; //department
						
					}
				}

				//Get Entry emails
				if (!empty($entryResponsibleArr)) {
					foreach ($entryResponsibleArr as $entry_id) {
						$entryResponsible = BeanFactory::getBean('Contacts', $entry_id);
						$primaryEmailAddress = $entryResponsible->emailAddress->getPrimaryAddress($entryResponsible);
							if ($primaryEmailAddress != false) {
								$entryEmails[] = $primaryEmailAddress;
							}
					}
				} 
				//Get Review emails
				if (!empty($reviewResponsibleArr)) {
					foreach ($reviewResponsibleArr as $review_id) {
						$reviewResponsible = BeanFactory::getBean('Contacts', $review_id);
						$primaryEmailAddress = $reviewResponsible->emailAddress->getPrimaryAddress($reviewResponsible);
						if ($primaryEmailAddress != false) {
							$reviewEmails[] = $primaryEmailAddress;
						}
					}
				} 
				//Get Manager
				if (!empty($managersArr)) {
					foreach ($managersArr as $manager_id) {
						$managerBean = BeanFactory::getBean('Contacts', $manager_id);
						$manager_name_list[] = $managerBean->name;
					}
				} 
				
				if (!empty($onupmanagersArr)) {
					foreach ($onupmanagersArr as $oneupmanager_id) {
						$oneUpManagerBean = BeanFactory::getBean('Contacts', $oneupmanager_id);
						$oneUpmanager_name[] = $oneUpManagerBean->name;                      
					}
           		}
				
				
				$responsibleData['entryEmails'] 		= !empty($entryEmails)?implode(", ",array_unique($entryEmails)):"NA";
				$responsibleData['reviewEmails'] 		= !empty($reviewEmails)?implode(", ",array_unique($reviewEmails)):"NA";
				$responsibleData['manager_name_list'] 	= !empty($manager_name_list)?implode(", ",array_unique($manager_name_list)):"NA";
				$responsibleData['oneUpmanager_name']	= !empty($oneUpmanager_name)?implode(", ",array_unique($oneUpmanager_name)):"NA";
				$responsibleData['contact_dept'] 		= !empty($contact_dept)?implode(", ",array_unique($contact_dept)):"NA";
				
				return $responsibleData;
			}
	
	/*Get Email Subject */
	function getEmailSubject($data) {
		if(!empty($data['error_category_c']) && $data['error_category_c']!="NA") {
            $subject .= ' : '.$data['error_category_c'];
		}
		
		if(!empty($data['wp_name_c']) && $data['wp_name_c']!="NA") {
            $subject .= ' : ' . $data['wp_name_c'];
		}
		
        if(!empty($data['test_system_name_data']) && $data['test_system_name_data']!="NA") {
            $subject .= ' : ' . $data['test_system_name_data'];
		}
		
		if(!empty($data['name'])) {
            $subject .= ' : ' .$data['name'];
        }
		
        return 'SD Assessment '.$subject;
	}
	
	/*Get Email Subject Edit line */
	function getEmailSubjectEdited($data) {
		$subject = '';
		if(!empty($data['name'])) {
            $subject .= ' : ' .$data['name'];
        }		
        return 'COM Edited'.$subject;
	}
	
}