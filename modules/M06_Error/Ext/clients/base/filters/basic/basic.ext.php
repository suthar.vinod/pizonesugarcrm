<?php
// WARNING: The contents of this file are auto-generated.


$viewdefs['M06_Error']['base']['filter']['basic']['filters'][] = array(
    'id' => 'filterObservations',
    'name' => 'LBL_FILTER_OBSERVATIONS_TEMPLATE',
    'filter_definition' => array(
        array(
            'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida'=> array(
              '$in'=>array(),
            ),
            'id'=> array(
              '$in'=>array(),
            ),

        )
    ),
    'editable' => true,
    'is_template' => true,
); 
