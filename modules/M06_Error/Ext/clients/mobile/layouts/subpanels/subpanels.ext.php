<?php
// WARNING: The contents of this file are auto-generated.


// created: 2022-04-26 07:03:59
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_BID_BATCH_ID_M06_ERROR_1_FROM_BID_BATCH_ID_TITLE',
  'context' => 
  array (
    'link' => 'bid_batch_id_m06_error_1',
  ),
);

// created: 2018-06-01 21:24:07
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_ERQC_ERROR_QC_EMPLOYEES_TITLE',
  'context' => 
  array (
    'link' => 'erqc_error_qc_employees_m06_error_1',
  ),
);

// created: 2018-01-31 16:00:43
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_anml_animals_1',
  ),
);

// created: 2018-07-30 20:17:15
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_de_deviation_employees_1',
  ),
);

// created: 2022-01-03 10:55:30
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_2_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_de_deviation_employees_2',
  ),
);

// created: 2018-02-22 13:58:40
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_ED_ERRORS_DEPARTMENT_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_ed_errors_department_1',
  ),
);

// created: 2018-01-31 16:07:58
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_equip_equipment_1',
  ),
);

// created: 2018-01-31 21:10:46
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_erd_error_documents_1',
  ),
);

// created: 2018-01-31 21:12:49
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_ere_error_employees_1',
  ),
);

// created: 2018-01-31 15:57:04
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_m03_work_product_1',
  ),
);

// created: 2020-07-07 08:16:55
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_R_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_m06_error_1',
  ),
);

// created: 2019-02-21 20:21:32
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_RMS_ROOM_1_FROM_RMS_ROOM_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_rms_room_1',
  ),
);

// created: 2019-05-19 21:27:08
$viewdefs['M06_Error']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_TASKS_1_FROM_TASKS_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_tasks_1',
  ),
);