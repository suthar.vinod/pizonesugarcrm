<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us..php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ERROR_TYPE'] = 'Type';
$mod_strings['LBL_SOP_NUMBER'] = 'SOP Number (if applicable)';
$mod_strings['LBL_FORM_NUMBER'] = 'Form Number (if applicable)';
$mod_strings['LBL_DATE_ERROR_DOCUMENTED'] = 'Date Discovered';
$mod_strings['LBL_DATE_ERROR_OCCURRED'] = 'Date Occurred';
$mod_strings['LBL_EQUIPMENT_ID'] = 'Equipment ID (if applicable)';
$mod_strings['LBL_EXPECTED_EVENT'] = 'Expected Event';
$mod_strings['LBL_ACTUAL_EVENT'] = 'Actual Event';
$mod_strings['LBL_DEPARTMENT_ID'] = 'Department ID';
$mod_strings['LBL_ERROR_CATEGORY'] = 'Category';
$mod_strings['LBL_ERROR_CLASSIFICATION'] = 'Classification';
$mod_strings['LBL_ERROR_OCCURED_ON_WEEKEND'] = 'Deviation Occurred on Weekend';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Submission Info';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Assessment Info';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'New Panel 4';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Deviation Documents';
$mod_strings['LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LNK_NEW_RECORD'] = 'Create Deviation';
$mod_strings['LNK_LIST'] = 'View Observations';
$mod_strings['LBL_MODULE_NAME'] = 'Observations';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Deviation';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Deviation';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Deviation vCard';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Observation List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Deviation';
$mod_strings['LBL_M06_ERROR_SUBPANEL_TITLE'] = 'Observations';
$mod_strings['LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE'] = 'Documents';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Observations';
$mod_strings['LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_ED_ERRORS_DEPARTMENT_TITLE'] = 'Departments';
$mod_strings['LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE'] = 'APS Employees';
$mod_strings['LBL_EMPLOYEE_RELATE'] = 'Employee Created By';
$mod_strings['LBL_ERR_CREATED_DATE'] = 'Date Created';
$mod_strings['LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_ERQC_ERROR_QC_EMPLOYEES_TITLE'] = 'Deviation QC Employees';
$mod_strings['LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Deviation QC Employees';
$mod_strings['LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_M06_ERROR_TITLE'] = 'Deviation Departments';
$mod_strings['LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_M06_ERROR_TITLE'] = 'Deviation Documents';
$mod_strings['LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_M06_ERROR_TITLE'] = 'Deviation Employees';
$mod_strings['LBL_SUBMITTER'] = 'Submitter';
$mod_strings['LBL_DATE_TIME_DISCOVERED'] = 'Date Time Discovered';
$mod_strings['LBL_SUBTYPE'] = 'Subtype';
$mod_strings['LBL_DEPARTMENT'] = 'Department';
$mod_strings['LBL_RECOMMENDATION'] = 'Recommendation';
$mod_strings['LBL_RESOLUTION'] = 'Impact Assessment';
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Observation Employee';
$mod_strings['LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_M06_ERROR_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LBL_NOTES'] = 'Notes';
$mod_strings['LBL_TARGET_REINSPECTION_DATE'] = 'Target Reinspection Date';
$mod_strings['LBL_REINSPECTION_DATE'] = 'Reinspection Date';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.observations_filter.php


$mod_strings['LBL_FILTER_OBSERVATIONS_TEMPLATE'] = "Filter Communications";


?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_m03_work_product_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M06_ERROR_TITLE'] = 'Communications';
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M06_ERROR_TITLE'] = 'Work Products';
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M06_ERROR_TITLE'] = 'Work Products';
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_anml_animals_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE'] = 'Animals';
$mod_strings['LBL_M06_ERROR_ANML_ANIMALS_1_FROM_M06_ERROR_TITLE'] = 'Animals';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_docs_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_DOCS_DOCUMENTS_1_FROM_DOCS_DOCUMENTS_TITLE'] = 'Documents';
$mod_strings['LBL_M06_ERROR_DOCS_DOCUMENTS_1_FROM_M06_ERROR_TITLE'] = 'Documents';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Documents';
$mod_strings['LBL_M06_ERROR_DOCUMENTS_1_FROM_M06_ERROR_TITLE'] = 'Documents';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customerqc_error_qc_employees_m06_error_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_ERQC_ERROR_QC_EMPLOYEES_TITLE'] = 'Errors QC Employees';
$mod_strings['LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Errors QC Employees';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_de_deviation_employees_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Deviation Employees';
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_M06_ERROR_TITLE'] = 'Deviation Employees';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customa1a_critical_phase_inspectio_m06_error_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE'] = 'Critical Phase Inspections';
$mod_strings['LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Critical Phase Inspections';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customcie_clinical_issue_exam_m06_error_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE'] = 'Clinical Issues & Exams';
$mod_strings['LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Clinical Issues & Exams';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customco_clinical_observation_m06_error_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_CO_CLINICAL_OBSERVATION_TITLE'] = 'Clinical Observations';
$mod_strings['LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Clinical Observations';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_m05_employee_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_M05_EMPLOYEE_1_FROM_M05_EMPLOYEE_TITLE'] = 'Employees';
$mod_strings['LBL_M06_ERROR_M05_EMPLOYEE_1_FROM_M06_ERROR_TITLE'] = 'Employees';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_equip_equipment_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment';
$mod_strings['LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_M06_ERROR_TITLE'] = 'Equipment';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_erd_error_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE'] = 'Errors Documents';
$mod_strings['LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_M06_ERROR_TITLE'] = 'Errors Documents';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_ere_error_employees_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE'] = 'Errors Employees';
$mod_strings['LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_M06_ERROR_TITLE'] = 'Errors Employees';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_ed_errors_department_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_ED_ERRORS_DEPARTMENT_TITLE'] = 'Errors Departments';
$mod_strings['LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_M06_ERROR_TITLE'] = 'Errors Departments';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_tasks_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_TASKS_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_M06_ERROR_TASKS_1_FROM_M06_ERROR_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_rms_room_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_RMS_ROOM_1_FROM_RMS_ROOM_TITLE'] = 'Rooms';
$mod_strings['LBL_M06_ERROR_RMS_ROOM_1_FROM_M06_ERROR_TITLE'] = 'Rooms';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_edoc_email_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE'] = 'Email Documents';
$mod_strings['LBL_M06_ERROR_EDOC_EMAIL_DOCUMENTS_1_FROM_M06_ERROR_TITLE'] = 'Email Documents';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customw_weight_m06_error_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_W_WEIGHT_M06_ERROR_1_FROM_W_WEIGHT_TITLE'] = 'Weights';
$mod_strings['LBL_W_WEIGHT_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Weights';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_m06_error_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_R_TITLE'] = 'Communications';
$mod_strings['LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_L_TITLE'] = 'Communications';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customw_weight_m06_error_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_W_WEIGHT_M06_ERROR_2_FROM_W_WEIGHT_TITLE'] = 'Weights';
$mod_strings['LBL_W_WEIGHT_M06_ERROR_2_FROM_M06_ERROR_TITLE'] = 'Weights';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customm06_error_de_deviation_employees_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_2_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Responsible Personnel M2M';
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_2_FROM_M06_ERROR_TITLE'] = 'Responsible Personnel M2M';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.customcapa_capa_m06_error_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CAPA_CAPA_M06_ERROR_1_FROM_CAPA_CAPA_TITLE'] = 'CAPAs';
$mod_strings['LBL_CAPA_CAPA_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'CAPAs';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.custombid_batch_id_m06_error_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_BID_BATCH_ID_M06_ERROR_1_FROM_BID_BATCH_ID_TITLE'] = 'Batch IDs';
$mod_strings['LBL_BID_BATCH_ID_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Batch IDs';

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ERROR_TYPE'] = 'Type';
$mod_strings['LBL_SOP_NUMBER'] = 'SOP Number (if applicable)';
$mod_strings['LBL_FORM_NUMBER'] = 'Form Number (if applicable)';
$mod_strings['LBL_DATE_ERROR_DOCUMENTED'] = 'Date Discovered';
$mod_strings['LBL_DATE_ERROR_OCCURRED'] = 'Date Occurred';
$mod_strings['LBL_EQUIPMENT_ID'] = 'Equipment ID (if applicable)';
$mod_strings['LBL_EXPECTED_EVENT'] = 'Expected Event';
$mod_strings['LBL_ACTUAL_EVENT'] = 'Actual Event';
$mod_strings['LBL_DEPARTMENT_ID'] = 'Department ID';
$mod_strings['LBL_ERROR_CATEGORY'] = 'Category';
$mod_strings['LBL_ERROR_CLASSIFICATION'] = 'Classification';
$mod_strings['LBL_ERROR_OCCURED_ON_WEEKEND'] = 'Deviation Occurred on Weekend (obsolete)';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Submission Info';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Study Director';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Management';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Quality Assurance';
$mod_strings['LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LNK_NEW_RECORD'] = 'Create Communication';
$mod_strings['LNK_LIST'] = 'View Communications';
$mod_strings['LBL_MODULE_NAME'] = 'Communications';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Communication';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Deviation';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Deviation vCard';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Communication List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Communication';
$mod_strings['LBL_M06_ERROR_SUBPANEL_TITLE'] = 'Communications';
$mod_strings['LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE'] = 'Controlled Documents';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Communications';
$mod_strings['LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_ED_ERRORS_DEPARTMENT_TITLE'] = 'Departments';
$mod_strings['LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE'] = 'APS Employees';
$mod_strings['LBL_EMPLOYEE_RELATE'] = 'Employee Created By';
$mod_strings['LBL_ERR_CREATED_DATE'] = 'Date Created';
$mod_strings['LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_ERQC_ERROR_QC_EMPLOYEES_TITLE'] = 'Deviation QC Employees';
$mod_strings['LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Deviation QC Employees';
$mod_strings['LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_M06_ERROR_TITLE'] = 'Deviation Departments';
$mod_strings['LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_M06_ERROR_TITLE'] = 'Deviation Documents';
$mod_strings['LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_M06_ERROR_TITLE'] = 'Observation Employees';
$mod_strings['LBL_SUBMITTER'] = 'Submitter';
$mod_strings['LBL_DATE_TIME_DISCOVERED'] = 'Date Time Discovered';
$mod_strings['LBL_SUBTYPE'] = 'Subtype';
$mod_strings['LBL_DEPARTMENT'] = 'Department';
$mod_strings['LBL_RECOMMENDATION'] = 'Recommendation';
$mod_strings['LBL_RESOLUTION'] = 'SD Assessment';
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Responsible Personnel (Obsolete 3Jan2022)';
$mod_strings['LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_M06_ERROR_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LBL_NOTES'] = 'Pending Classification Notes';
$mod_strings['LBL_TARGET_REINSPECTION_DATE'] = 'Target SD Assessment Date';
$mod_strings['LBL_REINSPECTION_DATE'] = 'Actual SD Assessment Date';
$mod_strings['LBL_OCCURED_ON_WEEKEND'] = 'Occurred on Weekend';
$mod_strings['LBL_M06_ERROR_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE'] = 'Test Systems';
$mod_strings['LBL_TARGET_QA_REASSESSMENT_DATE'] = 'Target QA Verification of Completion  Date';
$mod_strings['LBL_ACTUAL_QA_REASSESSMENT_DATE'] = 'Actual QA Verification of Completion Date';
$mod_strings['LBL_TARGET_MANAGEMENT_ASSESSMENT'] = 'Target Subtype Date';
$mod_strings['LBL_ACTUAL_MANAGEMENT_ASSESSMENT'] = 'Actual Subtype Date';
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_M06_ERROR_TITLE'] = 'Observation Employees';
$mod_strings['LBL_MAINTENANCE_REQUEST_NOTES'] = 'Facilities Assessment';
$mod_strings['LBL_DUPLICATE_M06_ERROR_ID'] = 'Duplicate (related Observation ID)';
$mod_strings['LBL_DUPLICATE'] = 'Duplicate';
$mod_strings['LBL_SD_ASSESSMENT_DURATION'] = 'SD Assessment Duration';
$mod_strings['LBL_REASSESSMENT_REQUIRED'] = 'SD Reassessment Required';
$mod_strings['LBL_REASON_FOR_REASSESSMENT'] = 'SD Reason for Reassessment';
$mod_strings['LBL_ACTUAL_SD_REASSESSMENT_DATE'] = 'Actual SD Reassessment Date';
$mod_strings['LBL_QA_ASSESSMENT_DURATION'] = 'QA Verification of Completion Duration';
$mod_strings['LBL_VET_ASSESSMENT_DATE'] = 'Vet Assessment Date';
$mod_strings['LBL_VET_ASSESSMENT_NOTES'] = 'Vet Assessment Notes (obsolete)';
$mod_strings['LBL_VET_REASSESSMENT_REQUIRED'] = 'Vet Reassessment Required';
$mod_strings['LBL_VET_REASSESSMENT_DATE'] = 'Actual Vet Reassessment Date';
$mod_strings['LBL_TARGET_VET_REASSESSMENT_DATE'] = 'Target Vet Reassessment Date';
$mod_strings['LBL_ORGAN_SYSTEM'] = 'Organ System';
$mod_strings['LBL_CONDITION_2'] = 'Clinical Symptoms';
$mod_strings['LBL_FEEDBACK_ASSESSMENT_NOTES'] = 'Feedback Assessment Notes';
$mod_strings['LBL_ACTUAL_FEEDBACK_REVIEW_DATE'] = 'Actual PI Assessment Date';
$mod_strings['LBL_RECORDVIEW_PANEL5'] = 'Veterinarian';
$mod_strings['LBL_RECORDVIEW_PANEL6'] = 'Process Improvement';
$mod_strings['LBL_CAPA_INITIATION'] = 'CAPA Initiation';
$mod_strings['LBL_CAPA_ID'] = 'CAPA ID';
$mod_strings['LBL_JUSTIFICATION_FOR_NO_CAPA'] = 'Justification for no CAPA';
$mod_strings['LBL_FEEDBACK_ASSESSMENT_NOTES_2'] = 'PI Assessment';
$mod_strings['LBL_VET_ASSESSMENT_NOTES_2'] = 'Vet Assessment';
$mod_strings['LBL_RECORDVIEW_PANEL7'] = 'General Assessment';
$mod_strings['LBL_TARGET_SD_REASSESSMENT_DATE'] = 'Target SD Reassessment Date';
$mod_strings['LBL_MANAGEMENT_REASSESSMENT_REQ'] = 'Management Reassessment Required';
$mod_strings['LBL_TARGET_MGT_REASSESS_DATE'] = 'Target Management Reassessment Date';
$mod_strings['LBL_ACTUAL_MGT_REASSESSMENT_DATE'] = 'Actual Management Reassessment Date';
$mod_strings['LBL_MGT_REASON_FOR_REASSESSMENT'] = 'Management Reason for Reassessment';
$mod_strings['LBL_MGT_ASSESSMENT'] = 'Management Assessment';
$mod_strings['LBL_TARGET_VET_ASSESSMENT_DATE'] = 'Target Vet Assessment Date';
$mod_strings['LBL_VET_REASON_FOR_REASSESSMENT'] = 'Vet Reason for Reassessment';
$mod_strings['LBL_TARGET_PI_ASSESSMENT_DATE'] = 'Target PI Assessment Date';
$mod_strings['LBL_ACTUAL_PI_ASSESSMENT_DATE'] = 'Actual PI Assessment Date';
$mod_strings['LBL_PI_REASSESSMENT_REQUIRED'] = 'PI Reassessment Required';
$mod_strings['LBL_TARGET_PI_REASSESSMENT_DATE'] = 'Target PI Reassessment Date';
$mod_strings['LBL_ACTUAL_PI_REASSESSMENT_DATE'] = 'Actual PI Reassessment Date';
$mod_strings['LBL_PI_REASON_FOR_REASSESSMENT'] = 'PI Reason for Reassessment';
$mod_strings['LBL_TARGET_FAC_ASSESSMENT_DATE'] = 'Target Facilities Assessment Date';
$mod_strings['LBL_ACTUAL_FAC_ASSESSMENT_DATE'] = 'Actual Facilities Assessment Date';
$mod_strings['LBL_FAC_REASSESSMENT_REQUIRED'] = 'Facilities Reassessment Required';
$mod_strings['LBL_TARGET_FAC_REASSESSMENT_DATE'] = 'Target Facilities Reassessment Date';
$mod_strings['LBL_ACTUAL_FAC_REASSESSMENT_DATE'] = 'Actual Facilities Reassessment Date';
$mod_strings['LBL_FAC_REASON_FOR_REASSESSMENT'] = 'Facilities Reason for Reassessment';
$mod_strings['LBL_RECORDVIEW_PANEL8'] = 'Facilities';
$mod_strings['LBL_MISSING_DATE_INITIALS_QUAN'] = 'Missing Date/Initials Quantity';
$mod_strings['LBL_VET_REASSESSMENT_NOTES'] = 'Vet Reassessment';
$mod_strings['LBL_SD_REASSESSMENT_NOTES'] = 'SD Reassessment';
$mod_strings['LBL_FAC_REASSESSMENT_NOTES'] = 'Facilities Reassessment';
$mod_strings['LBL_MGT_REASSESSMENT_NOTES'] = 'Management Reassessment';
$mod_strings['LBL_PI_REASSESSMENT_NOTES'] = 'PI Reassessment';
$mod_strings['LBL_CORRECTIVE_ACTIONS_WITHIN_ST'] = 'Corrective Action(s) Within the Study (obsolete 11/11/2019)';
$mod_strings['LBL_RELATED_TO'] = 'Related to';
$mod_strings['LBL_CORRECTIVE_ACTION_1'] = 'Corrective Action Within the Study';
$mod_strings['LBL_CORRECTIVE_ACTION_2'] = 'Corrective Action 2';
$mod_strings['LBL_CORRECTIVE_ACTION_3'] = 'Corrective Action 3';
$mod_strings['LBL_AMENDMENT_NUMBER'] = 'Amendment Number';
$mod_strings['LBL_SOP_UPDATED'] = 'Controlled Document Updated (Controlled Document Number)';
$mod_strings['LBL_DATES_OF_PROTOCOL_TRAINING'] = 'Date(s) of Protocol Specific Training';
$mod_strings['LBL_RATIONALE_FOR_NO_CA'] = 'Corrective Action Details';
$mod_strings['LBL_NOTES_FOR_OTHER_CA'] = 'Notes for Other Corrective Action';
$mod_strings['LBL_REPEAT_DEFICIENCY'] = 'Repeat Deficiency';
$mod_strings['LBL_PLAN_FOR_CORRECTION'] = 'Plan for Correction';
$mod_strings['LBL_DUE_DATE'] = 'Due Date';
$mod_strings['LBL_STATUS_PROGRESS_NOTES'] = 'Status/Progress Notes';
$mod_strings['LBL_COMPLETED_DATE'] = 'Completed Date';
$mod_strings['LBL_RECORDVIEW_PANEL9'] = 'IACUC Deficiency';
$mod_strings['LBL_IMPACTFUL'] = 'Impactful';
$mod_strings['LBL_WP_NAME'] = 'WP name';
$mod_strings['LBL_TEST_SYSTEM_NAME'] = 'Test System Name';
$mod_strings['LBL_RECORDVIEW_PANEL10'] = 'Equipment';
$mod_strings['LBL_SD_REASSESSMENT_REQUIRED'] = 'SD Reassessment Required?';
$mod_strings['LBL_COMPLIANCE'] = 'Compliance';
$mod_strings['LBL_TARGET_EQUIPMENT_ASSESS_DATE'] = 'Target Equipment Assessment Date';
$mod_strings['LBL_ACTUAL_EQUIPMENT_ASSESS_DATE'] = 'Actual Equipment Assessment Date';
$mod_strings['LBL_EQUIPMENT_ASSESSMENT'] = 'Equipment Assessment';
$mod_strings['LBL_EQUIPMENT_REASSESSMENT_REQ'] = 'Equipment Reassessment Required';
$mod_strings['LBL_EQUIPMENT_REASON_FOR_REASSES'] = 'Equipment Reason for Reassessment';
$mod_strings['LBL_TARGET_EQ_REASSESS_DATE'] = 'Target Equipment Reassessment Date';
$mod_strings['LBL_ACTUAL_EQ_REASSESS_DATE'] = 'Actual Equipment Reassessment Date';
$mod_strings['LBL_EQUIPMENT_REASSESSMENT'] = 'Equipment Reassessment';
$mod_strings['LBL_DIAGNOSIS'] = 'Diagnosis';
$mod_strings['LBL_STUDY_DIRECTOR'] = 'Study Director';
$mod_strings['LBL_DESCRIPTION'] = 'Notes';
$mod_strings['LBL_RECORDVIEW_PANEL11'] = 'Notes';
$mod_strings['DESCRIPTION'] = 'Notes';
$mod_strings['LBL_WHY_IT_HAPPENED_C'] = 'Does Submitter know Why it Happened?';
$mod_strings['LBL_DETAILS_WHY_HAPPENED_C'] = 'Details for Why it Happened';
$mod_strings['LBL_RESOLUTION_DATE'] = 'Resolution Date';
$mod_strings['M06_ERROR_M06_ERROR_1_NAME'] = 'Related COM ID';
$mod_strings['LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_L_TITLE'] = 'Related COM ID';
$mod_strings['LBL_TIME_TO_DISCOVERY_DAYS'] = 'Time to Discovery (Days)';
$mod_strings['LBL_TEST_SYSTEM_INVOLVED'] = 'Test System Involved?';
$mod_strings['LBL_HIGH_LEVEL_ROOT_CAUSE_ACTION'] = 'High Level Root Cause/Action';
$mod_strings['LBL_REPEAT_ISSUE_WITHIN_STUDY'] = 'Repeat Issue Within Study?';
$mod_strings['LBL_TARGET_SD_ACK_DATE'] = 'Target SD Acknowledgement Date';
$mod_strings['LBL_ACTUAL_SD_ACK_DATE'] = 'Actual SD Acknowledgement Date';
$mod_strings['LBL_IMPACTFUL_DD'] = 'Impactful?';
$mod_strings['LBL_MANAGEMENT_ASSESSMENT_TRAIN'] = 'Management Assessment for Training';
$mod_strings['LBL_IACUC_DEFICIENCY_CLASS'] = 'IACUC Deficiency Classification';
$mod_strings['LBL_RESOLVED'] = 'Resolved';
$mod_strings['LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE'] = 'Clinical Issues &amp; Exams';
$mod_strings['LBL_ACTIVITIES'] = 'Activities';
$mod_strings['LBL_M06_ERROR_FOCUS_DRAWER_DASHBOARD'] = 'Communications Focus Drawer';
$mod_strings['LBL_TPR_REFERENCE'] = 'TPR Reference';
$mod_strings['LBL_SUBJECTIVEOBJECTIVE_EXAM'] = 'Subjective/Objective Examination';
$mod_strings['LBL_REFER_TO_MED_TREATMENT_FORM'] = 'Refer to F-S-IL-GN-OP-010-01 – Medical Treatments Form';
$mod_strings['LBL_NOTIFY_VET'] = 'Notify a veterinarian if the following clinical signs are observed';
$mod_strings['LBL_PLAN_COMMENTS'] = 'Plan Comments';
$mod_strings['LBL_RECORDVIEW_PANEL12'] = 'S/O';
$mod_strings['LBL_RECORDVIEW_PANEL13'] = 'A';
$mod_strings['LBL_RECORDVIEW_PANEL14'] = 'P';
$mod_strings['LBL_RELATED_TEXT_C'] = 'Related Comm Data';
$mod_strings['LBL_COMMAPP_MISMATCH_C'] = 'Commapp Mismatch Update';
$mod_strings['LBL_PROCEDURE_TYPE'] = 'Procedure Type';
$mod_strings['LBL_MANAGER_ASSESSMENT_DURATION'] = 'Manager Assessment Duration';
$mod_strings['LBL_COM_USDA_ID'] = 'Com USDA Id';
$mod_strings['LBL_M06_ERROR_ANML_ANIMALS_1_FROM_M06_ERROR_TITLE'] = 'Test Systems';
$mod_strings['LBL_SUBTYPE_HIDDEN_FIELD_C'] = 'Subtype Hidden Field';
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_2_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Responsible Personnel';
$mod_strings['LBL_SEDATED'] = 'Sedated';
$mod_strings['LBL_SUBSTANCE_ADMINISTRATION'] = 'Substance Administration';
$mod_strings['LBL_BLOOD_COLLECTION'] = 'Blood Collected';
$mod_strings['LBL_SURGICAL_ACCESS'] = 'Surgical Access';
$mod_strings['LBL_SURGICAL_ACCESS_DETAILS'] = 'Surgical Access Details';
$mod_strings['LBL_TEST_ARTICLE_EXPOSURE'] = 'Test Article Exposure';
$mod_strings['LBL_TEST_ARTICLE_EXPOSURE_DETAIL'] = 'Test Article Exposure Details';
$mod_strings['LBL_ON_BID_LIST_C'] = 'On BID list';
$mod_strings['LBL_DATA_REVIEWED_BY_C'] = 'Data Reviewed By';
$mod_strings['LBL_TWENTYFOUR_ANIMAL_QUANTITY'] = '24hr: Quantity of animals that scored 1 or greater';
$mod_strings['LBL_FORTYEIGHT_ANIMAL_QUANTITY'] = '48hr: Quantity of animals that scored 1 or greater';

?>
