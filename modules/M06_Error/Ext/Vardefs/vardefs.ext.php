<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:56
$dictionary['M06_Error']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/m06_error_m03_work_product_1_M06_Error.php

// created: 2018-01-31 15:57:04
$dictionary["M06_Error"]["fields"]["m06_error_m03_work_product_1"] = array (
  'name' => 'm06_error_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'm06_error_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm06_error_m03_work_product_1m03_work_product_idb',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/m06_error_anml_animals_1_M06_Error.php

// created: 2018-01-31 16:00:43
$dictionary["M06_Error"]["fields"]["m06_error_anml_animals_1"] = array (
  'name' => 'm06_error_anml_animals_1',
  'type' => 'link',
  'relationship' => 'm06_error_anml_animals_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'vname' => 'LBL_M06_ERROR_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'm06_error_anml_animals_1anml_animals_idb',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/m06_error_equip_equipment_1_M06_Error.php

// created: 2018-01-31 16:07:58
$dictionary["M06_Error"]["fields"]["m06_error_equip_equipment_1"] = array (
  'name' => 'm06_error_equip_equipment_1',
  'type' => 'link',
  'relationship' => 'm06_error_equip_equipment_1',
  'source' => 'non-db',
  'module' => 'Equip_Equipment',
  'bean_name' => 'Equip_Equipment',
  'vname' => 'LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'id_name' => 'm06_error_equip_equipment_1equip_equipment_idb',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/m06_error_erd_error_documents_1_M06_Error.php

// created: 2018-01-31 21:10:46
$dictionary["M06_Error"]["fields"]["m06_error_erd_error_documents_1"] = array (
  'name' => 'm06_error_erd_error_documents_1',
  'type' => 'link',
  'relationship' => 'm06_error_erd_error_documents_1',
  'source' => 'non-db',
  'module' => 'Erd_Error_Documents',
  'bean_name' => 'Erd_Error_Documents',
  'vname' => 'LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'id_name' => 'm06_error_erd_error_documents_1erd_error_documents_idb',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/m06_error_ere_error_employees_1_M06_Error.php

// created: 2018-01-31 21:12:49
$dictionary["M06_Error"]["fields"]["m06_error_ere_error_employees_1"] = array (
  'name' => 'm06_error_ere_error_employees_1',
  'type' => 'link',
  'relationship' => 'm06_error_ere_error_employees_1',
  'source' => 'non-db',
  'module' => 'Ere_Error_Employees',
  'bean_name' => 'Ere_Error_Employees',
  'vname' => 'LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE',
  'id_name' => 'm06_error_ere_error_employees_1ere_error_employees_idb',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/m06_error_ed_errors_department_1_M06_Error.php

// created: 2018-02-22 13:58:39
$dictionary["M06_Error"]["fields"]["m06_error_ed_errors_department_1"] = array (
  'name' => 'm06_error_ed_errors_department_1',
  'type' => 'link',
  'relationship' => 'm06_error_ed_errors_department_1',
  'source' => 'non-db',
  'module' => 'ED_Errors_Department',
  'bean_name' => 'ED_Errors_Department',
  'vname' => 'LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_ED_ERRORS_DEPARTMENT_TITLE',
  'id_name' => 'm06_error_ed_errors_department_1ed_errors_department_idb',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/erqc_error_qc_employees_m06_error_1_M06_Error.php

// created: 2018-06-01 21:24:07
$dictionary["M06_Error"]["fields"]["erqc_error_qc_employees_m06_error_1"] = array (
  'name' => 'erqc_error_qc_employees_m06_error_1',
  'type' => 'link',
  'relationship' => 'erqc_error_qc_employees_m06_error_1',
  'source' => 'non-db',
  'module' => 'ErQC_Error_QC_Employees',
  'bean_name' => 'ErQC_Error_QC_Employees',
  'vname' => 'LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_ERQC_ERROR_QC_EMPLOYEES_TITLE',
  'id_name' => 'erqc_error_qc_employees_m06_error_1erqc_error_qc_employees_ida',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_rp_review_emails_dept.php

/**
 * Created by PhpStorm.
 * User: nabeel.alvi
 * Date: 7/23/18
 * Time: 5:23 PM
 */

$dictionary["M06_Error"]["fields"]["rp_review_email_dept"] = array(
    'name' => 'rp_review_email_dept',
    'label' => 'LBL_REVIEW_EMAIL_DEPT',
    'type' => 'text',
    'dbType' => 'blob',
    'module' => 'M06_Error',
    'audited' => false,
    'mass_update' => false,
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
    'source' => 'db'
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/m06_error_de_deviation_employees_1_M06_Error.php

// created: 2018-07-30 20:17:15
$dictionary["M06_Error"]["fields"]["m06_error_de_deviation_employees_1"] = array (
  'name' => 'm06_error_de_deviation_employees_1',
  'type' => 'link',
  'relationship' => 'm06_error_de_deviation_employees_1',
  'source' => 'non-db',
  'module' => 'DE_Deviation_Employees',
  'bean_name' => 'DE_Deviation_Employees',
  'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_de_deviation_employees_1m06_error_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_employee.php


$dictionary["M06_Error"]["fields"]['employee_relate'] = array (
      'name' => 'employee_relate',
      'source' => 'non-db',
      'vname' => 'LBL_EMPLOYEE_RELATE',
      'type' => 'relate',
      'id_name' => 'employee_id',
      'module' => 'Employees',
      'rname' => 'name',
      'studio' => 'visible',
    );


$dictionary["M06_Error"]["fields"]['employee_id'] = array (
      'name' => 'employee_id',
      'type' => 'id',
      'vname' => 'LBL_EMPLOYEE_ID',
    );

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_rp_entry_emails_dept.php

/**
 * Created by PhpStorm.
 * User: nabeel.alvi
 * Date: 7/23/18
 * Time: 5:23 PM
 */

$dictionary["M06_Error"]["fields"]["rp_entry_email_dept"] = array(
    'name' => 'rp_entry_email_dept',
    'label' => 'LBL_ENTRY_EMAIL_DEPT',
    'type' => 'text',
    'dbType' => 'blob',
    'audited' => false,
    'mass_update' => false,
    'module' => 'M06_Error',
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
    'source' => 'db'
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_rp_review.php

/**
 * Created by PhpStorm.
 * User: nabeel.alvi
 * Date: 7/23/18
 * Time: 5:23 PM
 */

$dictionary["M06_Error"]["fields"]["rp_review"] = array(
    'name' => 'rp_review',
    'label' => 'LBL_ENTRY',
    'type' => 'text',
    'dbType' => 'blob',
    'default_value' => '',
    'audited' => false,
    'mass_update' => false,
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
    'source' => 'db',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_rp_entry.php

/**
 * Created by PhpStorm.
 * User: nabeel.alvi
 * Date: 7/20/18
 * Time: 6:58 PM
 */

$dictionary["M06_Error"]["fields"]["rp_entry"] = array(
    'name' => 'rp_entry',
    'label' => 'LBL_ENTRY',
    'type' => 'text',
    'dbType' => 'blob',
    'default_value' => '',
    'audited' => false,
    'mass_update' => false,
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
    'source' => 'db',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugar_field_work_products.php

/**
 * Created by PhpStorm.
 * User: nabeel.alvi
 * Date: 7/23/18
 * Time: 5:23 PM
 */

$dictionary["M06_Error"]["fields"]["work_products"] = array(
    'name' => 'work_products',
    'label' => 'LBL_ENTRY',
    'type' => 'text',
    'dbType' => 'blob',
    'default_value' => '',
    'audited' => false,
    'mass_update' => false,
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
    'source' => 'db',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_date_created.php


$dictionary["M06_Error"]["fields"]["err_created_date"] = array(
    'name' => 'err_created_date',
    'label' => 'LBL_ERR_CREATED_DATE',
    'type' => 'date',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_wordpress_flag.php


$dictionary["M06_Error"]["fields"]["wordpress_flag"] = array(
    'name' => 'wordpress_flag',
    'label' => 'LBL_WORDPRESS_FLAG',
    'type' => 'bool',
    'module' => 'M06_Error',
    'default_value' => false,
    'audited' => false,
    'mass_update' => false,
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_related_data.php

$dictionary["M06_Error"]["fields"]["related_data"] = array(
    'name' => 'related_data',
    'label' => 'LBL_RELATED_DATA',
    'type' => 'text',
    'dbType' => 'blob',
    'default_value' => '',
    'audited' => false,
    'mass_update' => false,
    'duplicate_merge' => false,
    'reportable' => true,
    'importable' => 'true',
    'studio_visible' => 'false',
    'source' => 'db',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/m06_error_tasks_1_M06_Error.php

// created: 2019-05-19 21:27:08
$dictionary["M06_Error"]["fields"]["m06_error_tasks_1"] = array (
  'name' => 'm06_error_tasks_1',
  'type' => 'link',
  'relationship' => 'm06_error_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_M06_ERROR_TASKS_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_tasks_1m06_error_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/a1a_critical_phase_inspectio_m06_error_1_M06_Error.php

// created: 2019-02-21 13:44:24
$dictionary["M06_Error"]["fields"]["a1a_critical_phase_inspectio_m06_error_1"] = array (
  'name' => 'a1a_critical_phase_inspectio_m06_error_1',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_m06_error_1',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'side' => 'right',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'a1a_critic9e89spectio_ida',
  'link-type' => 'one',
);
$dictionary["M06_Error"]["fields"]["a1a_critical_phase_inspectio_m06_error_1_name"] = array (
  'name' => 'a1a_critical_phase_inspectio_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'save' => true,
  'id_name' => 'a1a_critic9e89spectio_ida',
  'link' => 'a1a_critical_phase_inspectio_m06_error_1',
  'table' => 'a1a_critical_phase_inspectio',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["a1a_critic9e89spectio_ida"] = array (
  'name' => 'a1a_critic9e89spectio_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_M06_ERROR_TITLE_ID',
  'id_name' => 'a1a_critic9e89spectio_ida',
  'link' => 'a1a_critical_phase_inspectio_m06_error_1',
  'table' => 'a1a_critical_phase_inspectio',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/m06_error_rms_room_1_M06_Error.php

// created: 2019-02-21 20:21:31
$dictionary["M06_Error"]["fields"]["m06_error_rms_room_1"] = array (
  'name' => 'm06_error_rms_room_1',
  'type' => 'link',
  'relationship' => 'm06_error_rms_room_1',
  'source' => 'non-db',
  'module' => 'RMS_Room',
  'bean_name' => 'RMS_Room',
  'vname' => 'LBL_M06_ERROR_RMS_ROOM_1_FROM_RMS_ROOM_TITLE',
  'id_name' => 'm06_error_rms_room_1rms_room_idb',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_name.php

 // created: 2019-05-22 13:07:32
$dictionary['M06_Error']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['M06_Error']['fields']['name']['calculated']='1';
$dictionary['M06_Error']['fields']['name']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_submitter_c.php

 // created: 2019-05-22 13:08:35
$dictionary['M06_Error']['fields']['submitter_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['submitter_c']['required']=false;
$dictionary['M06_Error']['fields']['submitter_c']['name']='submitter_c';
$dictionary['M06_Error']['fields']['submitter_c']['vname']='LBL_SUBMITTER';
$dictionary['M06_Error']['fields']['submitter_c']['type']='varchar';
$dictionary['M06_Error']['fields']['submitter_c']['massupdate']=false;
$dictionary['M06_Error']['fields']['submitter_c']['no_default']=false;
$dictionary['M06_Error']['fields']['submitter_c']['comments']='';
$dictionary['M06_Error']['fields']['submitter_c']['help']='';
$dictionary['M06_Error']['fields']['submitter_c']['importable']='false';
$dictionary['M06_Error']['fields']['submitter_c']['duplicate_merge']='disabled';
$dictionary['M06_Error']['fields']['submitter_c']['duplicate_merge_dom_value']='0';
$dictionary['M06_Error']['fields']['submitter_c']['audited']=true;
$dictionary['M06_Error']['fields']['submitter_c']['reportable']=true;
$dictionary['M06_Error']['fields']['submitter_c']['unified_search']=false;
$dictionary['M06_Error']['fields']['submitter_c']['merge_filter']='disabled';
$dictionary['M06_Error']['fields']['submitter_c']['pii']=false;
$dictionary['M06_Error']['fields']['submitter_c']['calculated']=false;
$dictionary['M06_Error']['fields']['submitter_c']['len']='255';
$dictionary['M06_Error']['fields']['submitter_c']['size']='20';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_occured_on_weekend_c.php

 // created: 2019-05-22 13:14:46
$dictionary['M06_Error']['fields']['occured_on_weekend_c']['labelValue']='Occurred on Weekend';
$dictionary['M06_Error']['fields']['occured_on_weekend_c']['enforced']='';
$dictionary['M06_Error']['fields']['occured_on_weekend_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_department_c.php

 // created: 2019-05-22 13:28:29
$dictionary['M06_Error']['fields']['department_c']['labelValue']='Department';
$dictionary['M06_Error']['fields']['department_c']['dependency']='';
$dictionary['M06_Error']['fields']['department_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_m06_error_id_c.php

 // created: 2019-05-28 12:21:00

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_duplicate_c.php

 // created: 2019-05-28 12:21:00
$dictionary['M06_Error']['fields']['duplicate_c']['labelValue']='Duplicate';
$dictionary['M06_Error']['fields']['duplicate_c']['dependency']='equal($error_classification_c,"Duplicate")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_actual_qa_reassessment_date_c.php

 // created: 2019-06-13 15:40:29
$dictionary['M06_Error']['fields']['actual_qa_reassessment_date_c']['labelValue']='Actual QA Verification of Completion Date';
$dictionary['M06_Error']['fields']['actual_qa_reassessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_qa_reassessment_date_c']['dependency']='and(equal($error_category_c,"Critical Phase Inspection"),not(equal($error_classification_c,"Duplicate")))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_vet_reassessment_date_c.php

 // created: 2019-06-25 16:19:47
$dictionary['M06_Error']['fields']['target_vet_reassessment_date_c']['labelValue']='Target Vet Reassessment Date';
$dictionary['M06_Error']['fields']['target_vet_reassessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['target_vet_reassessment_date_c']['dependency']='equal($vet_reassessment_required_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_qa_reassessment_date_c.php

 // created: 2019-06-27 14:20:12
$dictionary['M06_Error']['fields']['target_qa_reassessment_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_qa_reassessment_date_c']['labelValue']='Target QA Verification of Completion  Date';
$dictionary['M06_Error']['fields']['target_qa_reassessment_date_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_qa_reassessment_date_c']['formula']='addDays($date_entered,21)';
$dictionary['M06_Error']['fields']['target_qa_reassessment_date_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_qa_reassessment_date_c']['dependency']='and(equal($error_category_c,"Critical Phase Inspection"),not(equal($error_classification_c,"Duplicate")))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_mgt_reassess_date_c.php

 // created: 2019-10-02 12:36:16
$dictionary['M06_Error']['fields']['target_mgt_reassess_date_c']['labelValue']='Target Management Reassessment Date';
$dictionary['M06_Error']['fields']['target_mgt_reassess_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['target_mgt_reassess_date_c']['dependency']='equal($management_reassessment_req_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_actual_mgt_reassessment_date_c.php

 // created: 2019-10-02 12:38:35
$dictionary['M06_Error']['fields']['actual_mgt_reassessment_date_c']['labelValue']='Actual Management Reassessment Date';
$dictionary['M06_Error']['fields']['actual_mgt_reassessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_mgt_reassessment_date_c']['dependency']='equal($management_reassessment_req_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_pi_assessment_date_c.php

 // created: 2019-10-02 12:56:43
$dictionary['M06_Error']['fields']['target_pi_assessment_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_pi_assessment_date_c']['labelValue']='Target PI Assessment Date';
$dictionary['M06_Error']['fields']['target_pi_assessment_date_c']['calculated']='true';
$dictionary['M06_Error']['fields']['target_pi_assessment_date_c']['formula']='addDays($date_entered,14)';
$dictionary['M06_Error']['fields']['target_pi_assessment_date_c']['enforced']='true';
$dictionary['M06_Error']['fields']['target_pi_assessment_date_c']['dependency']='equal($error_type_c,"Complaint")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_pi_reassessment_required_c.php

 // created: 2019-10-02 13:00:13
$dictionary['M06_Error']['fields']['pi_reassessment_required_c']['labelValue']='PI Reassessment Required';
$dictionary['M06_Error']['fields']['pi_reassessment_required_c']['enforced']='';
$dictionary['M06_Error']['fields']['pi_reassessment_required_c']['dependency']='equal($error_type_c,"Complaint")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_pi_reassessment_date_c.php

 // created: 2019-10-02 13:01:26
$dictionary['M06_Error']['fields']['target_pi_reassessment_date_c']['labelValue']='Target PI Reassessment Date';
$dictionary['M06_Error']['fields']['target_pi_reassessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['target_pi_reassessment_date_c']['dependency']='equal($pi_reassessment_required_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_actual_pi_reassessment_date_c.php

 // created: 2019-10-02 13:07:48
$dictionary['M06_Error']['fields']['actual_pi_reassessment_date_c']['labelValue']='Actual PI Reassessment Date';
$dictionary['M06_Error']['fields']['actual_pi_reassessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_pi_reassessment_date_c']['dependency']='equal($pi_reassessment_required_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_actual_fac_reassessment_date_c.php

 // created: 2019-10-02 13:24:39
$dictionary['M06_Error']['fields']['actual_fac_reassessment_date_c']['labelValue']='Actual Facilities Reassessment Date';
$dictionary['M06_Error']['fields']['actual_fac_reassessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_fac_reassessment_date_c']['dependency']='equal($fac_reassessment_required_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_maintenance_request_notes_c.php

 // created: 2019-10-02 13:48:14
$dictionary['M06_Error']['fields']['maintenance_request_notes_c']['labelValue']='Facilities Assessment';
$dictionary['M06_Error']['fields']['maintenance_request_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['maintenance_request_notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['maintenance_request_notes_c']['dependency']='or(equal($error_category_c,"Maintenance Request"),equal($error_type_c,"Facility"))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_fac_assessment_date_c.php

 // created: 2019-10-02 13:48:44
$dictionary['M06_Error']['fields']['target_fac_assessment_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_fac_assessment_date_c']['labelValue']='Target Facilities Assessment Date';
$dictionary['M06_Error']['fields']['target_fac_assessment_date_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_fac_assessment_date_c']['formula']='addDays($date_entered,14)';
$dictionary['M06_Error']['fields']['target_fac_assessment_date_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_fac_assessment_date_c']['dependency']='or(equal($error_category_c,"Maintenance Request"),equal($error_type_c,"Facility"))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_fac_reassessment_required_c.php

 // created: 2019-10-02 13:49:11
$dictionary['M06_Error']['fields']['fac_reassessment_required_c']['labelValue']='Facilities Reassessment Required';
$dictionary['M06_Error']['fields']['fac_reassessment_required_c']['enforced']='';
$dictionary['M06_Error']['fields']['fac_reassessment_required_c']['dependency']='or(equal($error_category_c,"Maintenance Request"),equal($error_type_c,"Facility"))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_actual_fac_assessment_date_c.php

 // created: 2019-10-02 13:49:59
$dictionary['M06_Error']['fields']['actual_fac_assessment_date_c']['labelValue']='Actual Facilities Assessment Date';
$dictionary['M06_Error']['fields']['actual_fac_assessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_fac_assessment_date_c']['dependency']='or(equal($error_category_c,"Maintenance Request"),equal($error_type_c,"Facility"))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_fac_reassessment_date_c.php

 // created: 2019-10-02 13:50:27
$dictionary['M06_Error']['fields']['target_fac_reassessment_date_c']['labelValue']='Target Facilities Reassessment Date';
$dictionary['M06_Error']['fields']['target_fac_reassessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['target_fac_reassessment_date_c']['dependency']='equal($fac_reassessment_required_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_vet_reason_for_reassessment_c.php

 // created: 2019-10-10 12:01:00
$dictionary['M06_Error']['fields']['vet_reason_for_reassessment_c']['labelValue']='Vet Reason for Reassessment';
$dictionary['M06_Error']['fields']['vet_reason_for_reassessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['vet_reason_for_reassessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['vet_reason_for_reassessment_c']['dependency']='equal($vet_reassessment_required_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_fac_reason_for_reassessment_c.php

 // created: 2019-10-10 12:01:59
$dictionary['M06_Error']['fields']['fac_reason_for_reassessment_c']['labelValue']='Facilities Reason for Reassessment';
$dictionary['M06_Error']['fields']['fac_reason_for_reassessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['fac_reason_for_reassessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['fac_reason_for_reassessment_c']['dependency']='equal($fac_reassessment_required_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_mgt_reason_for_reassessment_c.php

 // created: 2019-10-10 12:02:48
$dictionary['M06_Error']['fields']['mgt_reason_for_reassessment_c']['labelValue']='Management Reason for Reassessment';
$dictionary['M06_Error']['fields']['mgt_reason_for_reassessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['mgt_reason_for_reassessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['mgt_reason_for_reassessment_c']['dependency']='equal($management_reassessment_req_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_pi_reason_for_reassessment_c.php

 // created: 2019-10-10 12:03:33
$dictionary['M06_Error']['fields']['pi_reason_for_reassessment_c']['labelValue']='PI Reason for Reassessment';
$dictionary['M06_Error']['fields']['pi_reason_for_reassessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['pi_reason_for_reassessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['pi_reason_for_reassessment_c']['dependency']='equal($pi_reassessment_required_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_fac_reassessment_notes_c.php

 // created: 2019-10-10 12:31:56
$dictionary['M06_Error']['fields']['fac_reassessment_notes_c']['labelValue']='Facilities Reassessment';
$dictionary['M06_Error']['fields']['fac_reassessment_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['fac_reassessment_notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['fac_reassessment_notes_c']['dependency']='equal($fac_reassessment_required_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_pi_reassessment_notes_c.php

 // created: 2019-10-10 12:39:26
$dictionary['M06_Error']['fields']['pi_reassessment_notes_c']['labelValue']='PI Reassessment';
$dictionary['M06_Error']['fields']['pi_reassessment_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['pi_reassessment_notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['pi_reassessment_notes_c']['dependency']='equal($pi_reassessment_required_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_repeat_deficiency_c.php

 // created: 2019-11-14 12:36:13
$dictionary['M06_Error']['fields']['repeat_deficiency_c']['labelValue']='Repeat Deficiency';
$dictionary['M06_Error']['fields']['repeat_deficiency_c']['enforced']='';
$dictionary['M06_Error']['fields']['repeat_deficiency_c']['dependency']='equal($error_category_c,"IACUC Deficiency")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_completed_date_c.php

 // created: 2019-11-14 12:42:21
$dictionary['M06_Error']['fields']['completed_date_c']['labelValue']='Completed Date';
$dictionary['M06_Error']['fields']['completed_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['completed_date_c']['dependency']='equal($error_category_c,"IACUC Deficiency")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_impactful_c.php

 // created: 2019-11-22 13:17:01
$dictionary['M06_Error']['fields']['impactful_c']['labelValue']='Impactful';
$dictionary['M06_Error']['fields']['impactful_c']['enforced']='';
$dictionary['M06_Error']['fields']['impactful_c']['dependency']='equal($error_classification_c,"Error")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_reassessment_required_c.php

 // created: 2019-12-05 12:07:48
$dictionary['M06_Error']['fields']['reassessment_required_c']['labelValue']='SD Reassessment Required';
$dictionary['M06_Error']['fields']['reassessment_required_c']['enforced']='';
$dictionary['M06_Error']['fields']['reassessment_required_c']['dependency']='not(equal(related($m06_error_m03_work_product_1,"name"),""))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/w_weight_m06_error_1_M06_Error.php

// created: 2020-01-07 13:27:00
$dictionary["M06_Error"]["fields"]["w_weight_m06_error_1"] = array (
  'name' => 'w_weight_m06_error_1',
  'type' => 'link',
  'relationship' => 'w_weight_m06_error_1',
  'source' => 'non-db',
  'module' => 'W_Weight',
  'bean_name' => 'W_Weight',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_1_FROM_W_WEIGHT_TITLE',
  'id_name' => 'w_weight_m06_error_1w_weight_ida',
);
$dictionary["M06_Error"]["fields"]["w_weight_m06_error_1_name"] = array (
  'name' => 'w_weight_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_1_FROM_W_WEIGHT_TITLE',
  'save' => true,
  'id_name' => 'w_weight_m06_error_1w_weight_ida',
  'link' => 'w_weight_m06_error_1',
  'table' => 'w_weight',
  'module' => 'W_Weight',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["w_weight_m06_error_1w_weight_ida"] = array (
  'name' => 'w_weight_m06_error_1w_weight_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_1_FROM_W_WEIGHT_TITLE_ID',
  'id_name' => 'w_weight_m06_error_1w_weight_ida',
  'link' => 'w_weight_m06_error_1',
  'table' => 'w_weight',
  'module' => 'W_Weight',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/cie_clinical_issue_exam_m06_error_1_M06_Error.php

// created: 2020-01-07 13:48:18
$dictionary["M06_Error"]["fields"]["cie_clinical_issue_exam_m06_error_1"] = array (
  'name' => 'cie_clinical_issue_exam_m06_error_1',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_m06_error_1',
  'source' => 'non-db',
  'module' => 'CIE_Clinical_Issue_Exam',
  'bean_name' => 'CIE_Clinical_Issue_Exam',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'id_name' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
);
$dictionary["M06_Error"]["fields"]["cie_clinical_issue_exam_m06_error_1_name"] = array (
  'name' => 'cie_clinical_issue_exam_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'save' => true,
  'id_name' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
  'link' => 'cie_clinical_issue_exam_m06_error_1',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida"] = array (
  'name' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE_ID',
  'id_name' => 'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida',
  'link' => 'cie_clinical_issue_exam_m06_error_1',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/co_clinical_observation_m06_error_1_M06_Error.php

// created: 2020-01-07 20:01:08
$dictionary["M06_Error"]["fields"]["co_clinical_observation_m06_error_1"] = array (
  'name' => 'co_clinical_observation_m06_error_1',
  'type' => 'link',
  'relationship' => 'co_clinical_observation_m06_error_1',
  'source' => 'non-db',
  'module' => 'CO_Clinical_Observation',
  'bean_name' => 'CO_Clinical_Observation',
  'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'id_name' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
);
$dictionary["M06_Error"]["fields"]["co_clinical_observation_m06_error_1_name"] = array (
  'name' => 'co_clinical_observation_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'save' => true,
  'id_name' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
  'link' => 'co_clinical_observation_m06_error_1',
  'table' => 'co_clinical_observation',
  'module' => 'CO_Clinical_Observation',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["co_clinical_observation_m06_error_1co_clinical_observation_ida"] = array (
  'name' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_CO_CLINICAL_OBSERVATION_TITLE_ID',
  'id_name' => 'co_clinical_observation_m06_error_1co_clinical_observation_ida',
  'link' => 'co_clinical_observation_m06_error_1',
  'table' => 'co_clinical_observation',
  'module' => 'CO_Clinical_Observation',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_test_system_name_c.php

 // created: 2020-01-13 15:09:39
$dictionary['M06_Error']['fields']['test_system_name_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['test_system_name_c']['labelValue']='Test System Name';
$dictionary['M06_Error']['fields']['test_system_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['test_system_name_c']['calculated']='1';
$dictionary['M06_Error']['fields']['test_system_name_c']['formula']='related($m06_error_anml_animals_1,"name")';
$dictionary['M06_Error']['fields']['test_system_name_c']['enforced']='1';
$dictionary['M06_Error']['fields']['test_system_name_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_wp_name_c.php

 // created: 2020-01-13 15:10:16
$dictionary['M06_Error']['fields']['wp_name_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['wp_name_c']['labelValue']='WP name';
$dictionary['M06_Error']['fields']['wp_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['wp_name_c']['calculated']='1';
$dictionary['M06_Error']['fields']['wp_name_c']['formula']='related($m06_error_m03_work_product_1,"name")';
$dictionary['M06_Error']['fields']['wp_name_c']['enforced']='1';
$dictionary['M06_Error']['fields']['wp_name_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_compliance_c.php

 // created: 2020-01-13 15:11:06
$dictionary['M06_Error']['fields']['compliance_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['compliance_c']['labelValue']='Compliance';
$dictionary['M06_Error']['fields']['compliance_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['compliance_c']['calculated']='1';
$dictionary['M06_Error']['fields']['compliance_c']['formula']='related($m06_error_m03_work_product_1,"work_product_compliance_c")';
$dictionary['M06_Error']['fields']['compliance_c']['enforced']='1';
$dictionary['M06_Error']['fields']['compliance_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_date_error_documented_c.php

 // created: 2020-01-16 18:01:03
$dictionary['M06_Error']['fields']['date_error_documented_c']['labelValue']='Date Discovered';
$dictionary['M06_Error']['fields']['date_error_documented_c']['enforced']='';
$dictionary['M06_Error']['fields']['date_error_documented_c']['dependency']='isInList($error_category_c,createList("Critical Phase Inspection","Daily QC","Data Book QC","External Audit","Maintenance Request","Process Audit","Real time study conduct","Weekly Sweep","Internal Feedback","IACUC Deficiency","Equipment Maintenance Request","Retrospective Data QC","Test Failure"))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_equipment_assess_date_c.php

 // created: 2020-01-24 13:02:04
$dictionary['M06_Error']['fields']['target_equipment_assess_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_equipment_assess_date_c']['labelValue']='Target Equipment Assessment Date';
$dictionary['M06_Error']['fields']['target_equipment_assess_date_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_equipment_assess_date_c']['formula']='addDays($date_entered,14)';
$dictionary['M06_Error']['fields']['target_equipment_assess_date_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_equipment_assess_date_c']['dependency']='equal($error_category_c,"Equipment Maintenance Request")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_eq_reassess_date_c.php

 // created: 2020-01-24 13:10:20
$dictionary['M06_Error']['fields']['target_eq_reassess_date_c']['labelValue']='Target Equipment Reassessment Date';
$dictionary['M06_Error']['fields']['target_eq_reassess_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['target_eq_reassess_date_c']['dependency']='equal($equipment_reassessment_req_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_actual_eq_reassess_date_c.php

 // created: 2020-01-24 13:12:17
$dictionary['M06_Error']['fields']['actual_eq_reassess_date_c']['labelValue']='Actual Equipment Reassessment Date';
$dictionary['M06_Error']['fields']['actual_eq_reassess_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_eq_reassess_date_c']['dependency']='equal($equipment_reassessment_req_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_equipment_reassessment_c.php

 // created: 2020-01-24 13:13:32
$dictionary['M06_Error']['fields']['equipment_reassessment_c']['labelValue']='Equipment Reassessment';
$dictionary['M06_Error']['fields']['equipment_reassessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['equipment_reassessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['equipment_reassessment_c']['dependency']='equal($equipment_reassessment_req_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_equipment_reassessment_req_c.php

 // created: 2020-01-24 13:15:48
$dictionary['M06_Error']['fields']['equipment_reassessment_req_c']['labelValue']='Equipment Reassessment Required';
$dictionary['M06_Error']['fields']['equipment_reassessment_req_c']['enforced']='';
$dictionary['M06_Error']['fields']['equipment_reassessment_req_c']['dependency']='equal($error_category_c,"Equipment Maintenance Request")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_equipment_reason_for_reasses_c.php

 // created: 2020-01-24 13:16:17
$dictionary['M06_Error']['fields']['equipment_reason_for_reasses_c']['labelValue']='Equipment Reason for Reassessment';
$dictionary['M06_Error']['fields']['equipment_reason_for_reasses_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['equipment_reason_for_reasses_c']['enforced']='';
$dictionary['M06_Error']['fields']['equipment_reason_for_reasses_c']['dependency']='equal($equipment_reassessment_req_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_actual_sd_reassessment_date_c.php

 // created: 2020-01-27 12:56:22
$dictionary['M06_Error']['fields']['actual_sd_reassessment_date_c']['labelValue']='Actual SD Reassessment Date';
$dictionary['M06_Error']['fields']['actual_sd_reassessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_sd_reassessment_date_c']['dependency']='equal($sd_reassessment_required_c,"Yes")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_sd_reassessment_required_c.php

 // created: 2020-01-27 12:59:10
$dictionary['M06_Error']['fields']['sd_reassessment_required_c']['labelValue']='SD Reassessment Required?';
$dictionary['M06_Error']['fields']['sd_reassessment_required_c']['dependency']='or(not(equal($resolution_c,"")),not(equal($reinspection_date_c,"")))';
$dictionary['M06_Error']['fields']['sd_reassessment_required_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/disabledname.php


$dictionary['M06_Error']['fields']['name']['readonly']=true;
$dictionary['M06_Error']['fields']['name']['required']=false;
$dictionary['M06_Error']['fields']['name']['len']='255';
$dictionary['M06_Error']['fields']['name']['audited']=true;
$dictionary['M06_Error']['fields']['name']['massupdate']=false;
$dictionary['M06_Error']['fields']['name']['unified_search']=false;
$dictionary['M06_Error']['fields']['name']['full_text_search']=array (
    'enabled' => true,
    'boost' => '1.55',
    'searchable' => true,
);

$dictionary['M06_Error']['fields']['name']['importable']='false';
$dictionary['M06_Error']['fields']['name']['duplicate_merge']='disabled';
$dictionary['M06_Error']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['name']['merge_filter']='disabled';
$dictionary['M06_Error']['fields']['name']['enforced']=true;

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_actual_feedback_review_date_c.php

 // created: 2020-02-21 13:08:24
$dictionary['M06_Error']['fields']['actual_feedback_review_date_c']['labelValue']='Actual PI Assessment Date';
$dictionary['M06_Error']['fields']['actual_feedback_review_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_feedback_review_date_c']['dependency']='equal($error_type_c,"Complaint")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_feedback_assessment_notes_2_c.php

 // created: 2020-02-21 13:09:49
$dictionary['M06_Error']['fields']['feedback_assessment_notes_2_c']['labelValue']='PI Assessment';
$dictionary['M06_Error']['fields']['feedback_assessment_notes_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['feedback_assessment_notes_2_c']['enforced']='';
$dictionary['M06_Error']['fields']['feedback_assessment_notes_2_c']['dependency']='equal($error_type_c,"Complaint")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_capa_id_c.php

 // created: 2020-02-21 13:11:45
$dictionary['M06_Error']['fields']['capa_id_c']['labelValue']='CAPA ID';
$dictionary['M06_Error']['fields']['capa_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['capa_id_c']['enforced']='';
$dictionary['M06_Error']['fields']['capa_id_c']['dependency']='equal($capa_initiation_c,"Yes")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_justification_for_no_capa_c.php

 // created: 2020-02-21 13:12:28
$dictionary['M06_Error']['fields']['justification_for_no_capa_c']['labelValue']='Justification for no CAPA';
$dictionary['M06_Error']['fields']['justification_for_no_capa_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['justification_for_no_capa_c']['enforced']='';
$dictionary['M06_Error']['fields']['justification_for_no_capa_c']['dependency']='equal($capa_initiation_c,"No")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_study_director_c.php

 // created: 2020-05-01 10:48:14
$dictionary['M06_Error']['fields']['study_director_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['study_director_c']['labelValue']='Study Director';
$dictionary['M06_Error']['fields']['study_director_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['study_director_c']['calculated']='1';
$dictionary['M06_Error']['fields']['study_director_c']['formula']='related($m06_error_m03_work_product_1,"assigned_to_for_coms_c")';
$dictionary['M06_Error']['fields']['study_director_c']['enforced']='1';
$dictionary['M06_Error']['fields']['study_director_c']['dependency']='and(not(equal($wp_name_c,"")),not(isInList($error_category_c,createList("Feedback","Internal Feedback"))))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_plan_for_correction_c.php

 // created: 2020-05-06 18:08:06
$dictionary['M06_Error']['fields']['plan_for_correction_c']['labelValue']='Plan for Correction';
$dictionary['M06_Error']['fields']['plan_for_correction_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['plan_for_correction_c']['enforced']='';
$dictionary['M06_Error']['fields']['plan_for_correction_c']['required']=false;
$dictionary['M06_Error']['fields']['plan_for_correction_c']['dependency']='equal($error_category_c,"IACUC Deficiency")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_due_date_c.php

 // created: 2020-05-06 18:09:02
$dictionary['M06_Error']['fields']['due_date_c']['labelValue']='Due Date';
$dictionary['M06_Error']['fields']['due_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['due_date_c']['required']=false;
$dictionary['M06_Error']['fields']['due_date_c']['dependency']='equal($error_category_c,"IACUC Deficiency")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_status_progress_notes_c.php

 // created: 2020-05-06 18:09:46
$dictionary['M06_Error']['fields']['status_progress_notes_c']['labelValue']='Status/Progress Notes';
$dictionary['M06_Error']['fields']['status_progress_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['status_progress_notes_c']['required']=false;
$dictionary['M06_Error']['fields']['status_progress_notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['status_progress_notes_c']['dependency']='equal($error_category_c,"IACUC Deficiency")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_mgt_reassessment_notes_c.php

 // created: 2019-10-10 12:37:43
$dictionary['M06_Error']['fields']['mgt_reassessment_notes_c']['labelValue']='Management Reassessment';
$dictionary['M06_Error']['fields']['mgt_reassessment_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['mgt_reassessment_notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['mgt_reassessment_notes_c']['dependency']='equal($management_reassessment_req_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_vet_assessment_notes_2_c.php

 // created: 2020-05-19 18:33:36
$dictionary['M06_Error']['fields']['vet_assessment_notes_2_c']['labelValue']='Vet Assessment';
$dictionary['M06_Error']['fields']['vet_assessment_notes_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['vet_assessment_notes_2_c']['enforced']='';
$dictionary['M06_Error']['fields']['vet_assessment_notes_2_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal"))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_vet_reassessment_required_c.php

 // created: 2020-05-19 18:34:12
$dictionary['M06_Error']['fields']['vet_reassessment_required_c']['labelValue']='Vet Reassessment Required';
$dictionary['M06_Error']['fields']['vet_reassessment_required_c']['enforced']='';
$dictionary['M06_Error']['fields']['vet_reassessment_required_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal"))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_vet_assessment_date_c.php

 // created: 2020-05-19 18:31:04
$dictionary['M06_Error']['fields']['target_vet_assessment_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_vet_assessment_date_c']['labelValue']='Target Vet Assessment Date';
$dictionary['M06_Error']['fields']['target_vet_assessment_date_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_vet_assessment_date_c']['formula']='$date_entered';
$dictionary['M06_Error']['fields']['target_vet_assessment_date_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_vet_assessment_date_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal"))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_missing_date_initials_quan_c.php

 // created: 2020-06-05 08:46:34
$dictionary['M06_Error']['fields']['missing_date_initials_quan_c']['labelValue']='Missing Date/Initials Quantity';
$dictionary['M06_Error']['fields']['missing_date_initials_quan_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['missing_date_initials_quan_c']['enforced']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_sd_assessment_duration_c.php

 // created: 2020-06-12 17:18:39
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['labelValue']='SD Assessment Duration';
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['calculated']='1';
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['formula']='ifElse(or(equal($reinspection_date_c,""),equal($target_reinspection_date_c,"")),"",subtract(daysUntil($reinspection_date_c),daysUntil($target_reinspection_date_c)))';
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['enforced']='1';
$dictionary['M06_Error']['fields']['sd_assessment_duration_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_qa_assessment_duration_c.php

 // created: 2020-06-12 17:17:45
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['labelValue']='QA Verification of Completion Duration';
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['calculated']='1';
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['formula']='ifElse(or(equal($actual_qa_reassessment_date_c,""),equal($target_qa_reassessment_date_c,"")),"",subtract(daysUntil($actual_qa_reassessment_date_c),daysUntil($target_qa_reassessment_date_c)))';
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['enforced']='1';
$dictionary['M06_Error']['fields']['qa_assessment_duration_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_details_why_happened_c.php

 // created: 2020-06-17 04:03:19
$dictionary['M06_Error']['fields']['details_why_happened_c']['labelValue']='Details for Why it Happened';
$dictionary['M06_Error']['fields']['details_why_happened_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['details_why_happened_c']['enforced']='';
$dictionary['M06_Error']['fields']['details_why_happened_c']['dependency']='equal($why_it_happened_c,"Known")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_description.php

 // created: 2020-06-04 15:10:16
$dictionary['M06_Error']['fields']['description']['audited']=true;
$dictionary['M06_Error']['fields']['description']['massupdate']=false;
$dictionary['M06_Error']['fields']['description']['comments']='Full text of the note';
$dictionary['M06_Error']['fields']['description']['duplicate_merge']='enabled';
$dictionary['M06_Error']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['M06_Error']['fields']['description']['merge_filter']='disabled';
$dictionary['M06_Error']['fields']['description']['unified_search']=false;
$dictionary['M06_Error']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['M06_Error']['fields']['description']['calculated']=false;
$dictionary['M06_Error']['fields']['description']['rows']='6';
$dictionary['M06_Error']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_notes_c.php

 // created: 2020-06-04 15:09:20
$dictionary['M06_Error']['fields']['notes_c']['labelValue']='Pending Classification Notes';
$dictionary['M06_Error']['fields']['notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['notes_c']['dependency']='equal($error_classification_c,"Pending")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_recommendation_c.php

 // created: 2020-06-30 12:20:14
$dictionary['M06_Error']['fields']['recommendation_c']['labelValue']='Recommendation';
$dictionary['M06_Error']['fields']['recommendation_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['recommendation_c']['enforced']='';
$dictionary['M06_Error']['fields']['recommendation_c']['dependency']='isInList($error_category_c,createList("Critical Phase Inspection","Process Audit"))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_management_reassessment_req_c.php

 // created: 2020-06-30 18:43:37
$dictionary['M06_Error']['fields']['management_reassessment_req_c']['labelValue']='Management Reassessment Required';
$dictionary['M06_Error']['fields']['management_reassessment_req_c']['enforced']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/m06_error_m06_error_1_M06_Error.php

// created: 2020-07-07 08:16:55
$dictionary["M06_Error"]["fields"]["m06_error_m06_error_1"] = array (
  'name' => 'm06_error_m06_error_1',
  'type' => 'link',
  'relationship' => 'm06_error_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_L_TITLE',
  'id_name' => 'm06_error_m06_error_1m06_error_idb',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["M06_Error"]["fields"]["m06_error_m06_error_1_right"] = array (
  'name' => 'm06_error_m06_error_1_right',
  'type' => 'link',
  'relationship' => 'm06_error_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'side' => 'right',
  'vname' => 'LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_R_TITLE',
  'id_name' => 'm06_error_m06_error_1m06_error_ida',
  'link-type' => 'one',
);
$dictionary["M06_Error"]["fields"]["m06_error_m06_error_1_name"] = array (
  'name' => 'm06_error_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_L_TITLE',
  'save' => true,
  'id_name' => 'm06_error_m06_error_1m06_error_ida',
  'link' => 'm06_error_m06_error_1_right',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["m06_error_m06_error_1m06_error_ida"] = array (
  'name' => 'm06_error_m06_error_1m06_error_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_R_TITLE_ID',
  'id_name' => 'm06_error_m06_error_1m06_error_ida',
  'link' => 'm06_error_m06_error_1_right',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_resolution_date_c.php

 // created: 2020-07-07 05:52:30
$dictionary['M06_Error']['fields']['resolution_date_c']['labelValue']='Resolution Date';
$dictionary['M06_Error']['fields']['resolution_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['resolution_date_c']['dependency']='equal($error_type_c,"Resolution Notification")';
$dictionary['M06_Error']['fields']['resolution_date_c']['readonly']='true';
 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_actual_equipment_assess_date_c.php

 // created: 2020-08-25 06:50:29
$dictionary['M06_Error']['fields']['actual_equipment_assess_date_c']['labelValue']='Actual Equipment Assessment Date';
$dictionary['M06_Error']['fields']['actual_equipment_assess_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_equipment_assess_date_c']['dependency']='equal($error_category_c,"Equipment Maintenance Request")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_equipment_assessment_c.php

 // created: 2020-08-25 06:51:23
$dictionary['M06_Error']['fields']['equipment_assessment_c']['labelValue']='Equipment Assessment';
$dictionary['M06_Error']['fields']['equipment_assessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['equipment_assessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['equipment_assessment_c']['dependency']='equal($error_category_c,"Equipment Maintenance Request")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_date_error_occurred_c.php

 // created: 2020-09-10 08:37:12
$dictionary['M06_Error']['fields']['date_error_occurred_c']['labelValue']='Date Occurred';
$dictionary['M06_Error']['fields']['date_error_occurred_c']['enforced']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_test_system_involved_c.php

 // created: 2020-09-24 07:56:34
$dictionary['M06_Error']['fields']['test_system_involved_c']['labelValue']='Test System Involved?';
$dictionary['M06_Error']['fields']['test_system_involved_c']['dependency']='';
$dictionary['M06_Error']['fields']['test_system_involved_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_why_it_happened_c.php

 // created: 2020-11-19 11:44:37
$dictionary['M06_Error']['fields']['why_it_happened_c']['labelValue']='Does Submitter know Why it Happened?';
$dictionary['M06_Error']['fields']['why_it_happened_c']['dependency']='isInList($error_category_c,createList("Daily QC","Data Book QC","Real time study conduct","Retrospective Data QC","Training","Weekly Sweep"))';
$dictionary['M06_Error']['fields']['why_it_happened_c']['required_formula']='';
$dictionary['M06_Error']['fields']['why_it_happened_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_actual_sd_ack_date_c.php

 // created: 2020-12-15 04:59:22
$dictionary['M06_Error']['fields']['actual_sd_ack_date_c']['labelValue']='Actual SD Acknowledgement Date';
$dictionary['M06_Error']['fields']['actual_sd_ack_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_sd_ack_date_c']['dependency']='';
$dictionary['M06_Error']['fields']['actual_sd_ack_date_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_management_assessment_train_c.php

 // created: 2020-12-15 05:08:20
$dictionary['M06_Error']['fields']['management_assessment_train_c']['labelValue']='Management Assessment for Training';
$dictionary['M06_Error']['fields']['management_assessment_train_c']['dependency']='equal($error_category_c,"Training")';
$dictionary['M06_Error']['fields']['management_assessment_train_c']['required_formula']='';
$dictionary['M06_Error']['fields']['management_assessment_train_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_iacuc_deficiency_class_c.php

 // created: 2020-12-15 05:10:55
$dictionary['M06_Error']['fields']['iacuc_deficiency_class_c']['labelValue']='IACUC Deficiency Classification';
$dictionary['M06_Error']['fields']['iacuc_deficiency_class_c']['dependency']='equal($error_category_c,"IACUC Deficiency")';
$dictionary['M06_Error']['fields']['iacuc_deficiency_class_c']['required_formula']='';
$dictionary['M06_Error']['fields']['iacuc_deficiency_class_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_impactful_dd_c.php

 // created: 2020-11-02 17:44:57
$dictionary['M06_Error']['fields']['impactful_dd_c']['labelValue']='Impactful?';
$dictionary['M06_Error']['fields']['impactful_dd_c']['dependency']='';
$dictionary['M06_Error']['fields']['impactful_dd_c']['required_formula']='';
$dictionary['M06_Error']['fields']['impactful_dd_c']['visibility_grid']=array (
  'trigger' => 'error_classification_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Adverse Event' => 
    array (
      0 => '',
      1 => 'This Adverse Event was impactful',
      2 => 'This Adverse Event was NOT impactful',
      3 => 'Pending needs further assessment before finalizing',
    ),
    'Error' => 
    array (
      0 => '',
      1 => 'This Deviation was impactful',
      2 => 'This Deviation was NOT impactful',
      3 => 'Pending needs further assessment before finalizing',
    ),
    'Duplicate' => 
    array (
    ),
    'Job well done' => 
    array (
    ),
    'Notification' => 
    array (
    ),
    'Observation' => 
    array (
    ),
    'Opportunity for Improvement' => 
    array (
    ),
    'Pending' => 
    array (
    ),
    'Impactful Deviation' => 
    array (
    ),
    'Non Impactful Deviation' => 
    array (
    ),
    'Unforeseen Circumstance Unanticipated Response' => 
    array (
    ),
    'Withdrawn Error' => 
    array (
    ),
    'A' => 
    array (
    ),
    'M' => 
    array (
    ),
    'S' => 
    array (
    ),
    'C' => 
    array (
    ),
    'NA' => 
    array (
    ),
    'Major Change or New' => 
    array (
    ),
    'Revision Change or Minor Change' => 
    array (
    ),
    'Employee did NOT perform SOP tasks while overdue' => 
    array (
    ),
    'The Controlled Document is a standalone document. No Controlled Document activities are associated' => 
    array (
    ),
    'Overdue refresher training' => 
    array (
    ),
    'Adverse Event Study Article Related' => 
    array (
      0 => '',
      1 => 'Pending needs further assessment before finalizing',
      2 => 'This Adverse Event was impactful',
      3 => 'This Adverse Event was NOT impactful',
    ),
    'Adverse Event Procedure Related' => 
    array (
      0 => '',
      1 => 'Pending needs further assessment before finalizing',
      2 => 'This Adverse Event was impactful',
      3 => 'This Adverse Event was NOT impactful',
    ),
    'Adverse Event Test System Model Related' => 
    array (
      0 => '',
      1 => 'Pending needs further assessment before finalizing',
      2 => 'This Adverse Event was impactful',
      3 => 'This Adverse Event was NOT impactful',
    ),
    'Adverse Event Etiology Unknown' => 
    array (
      0 => '',
      1 => 'Pending needs further assessment before finalizing',
      2 => 'This Adverse Event was impactful',
      3 => 'This Adverse Event was NOT impactful',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_capa_initiation_c.php

 // created: 2020-12-15 08:24:02
$dictionary['M06_Error']['fields']['capa_initiation_c']['labelValue']='CAPA Initiation';
$dictionary['M06_Error']['fields']['capa_initiation_c']['dependency']='or(equal($error_type_c,"Complaint"),equal($impactful_c,true),isInList($impactful_dd_c,createList("This Deviation was impactful","This Adverse Event was impactful")))';
$dictionary['M06_Error']['fields']['capa_initiation_c']['required_formula']='';
$dictionary['M06_Error']['fields']['capa_initiation_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_sd_ack_date_c.php

 // created: 2020-12-15 08:26:51
$dictionary['M06_Error']['fields']['target_sd_ack_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_sd_ack_date_c']['labelValue']='Target SD Acknowledgement Date';
$dictionary['M06_Error']['fields']['target_sd_ack_date_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_sd_ack_date_c']['formula']='addDays($date_entered,3)';
$dictionary['M06_Error']['fields']['target_sd_ack_date_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_sd_ack_date_c']['dependency']='and(not(equal($wp_name_c,"")),not(isInList($error_category_c,createList("Feedback","Internal Feedback"))))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_resolved_c.php

 // created: 2020-12-15 08:29:49
$dictionary['M06_Error']['fields']['resolved_c']['labelValue']='Resolved';
$dictionary['M06_Error']['fields']['resolved_c']['enforced']='';
$dictionary['M06_Error']['fields']['resolved_c']['dependency']='isInList($error_type_c,createList("Initial Issue","Observation at Transfer"))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_error_category_c.php

 // created: 2020-12-15 11:46:55
$dictionary['M06_Error']['fields']['error_category_c']['labelValue']='Category';
$dictionary['M06_Error']['fields']['error_category_c']['dependency']='';
$dictionary['M06_Error']['fields']['error_category_c']['required_formula']='';
$dictionary['M06_Error']['fields']['error_category_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_time_to_discovery_days_c.php

 // created: 2020-12-15 11:52:33
$dictionary['M06_Error']['fields']['time_to_discovery_days_c']['labelValue']='Time to Discovery (Days)';
$dictionary['M06_Error']['fields']['time_to_discovery_days_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['time_to_discovery_days_c']['formula']='ifElse(not(equal($date_error_documented_c,"")),abs(subtract(daysUntil($date_error_occurred_c),daysUntil($date_error_documented_c))),abs(subtract(daysUntil($date_error_occurred_c),daysUntil($date_time_discovered_c))))';
$dictionary['M06_Error']['fields']['time_to_discovery_days_c']['enforced']='false';
$dictionary['M06_Error']['fields']['time_to_discovery_days_c']['dependency']='';
$dictionary['M06_Error']['fields']['time_to_discovery_days_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_activities_c.php

 // created: 2020-12-16 13:44:29
$dictionary['M06_Error']['fields']['activities_c']['labelValue']='Activities';
$dictionary['M06_Error']['fields']['activities_c']['dependency']='equal($error_category_c,"Work Product Schedule Outcome")';
$dictionary['M06_Error']['fields']['activities_c']['required_formula']='';
$dictionary['M06_Error']['fields']['activities_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_corrective_action_1_c.php

 // created: 2020-12-18 13:30:08
$dictionary['M06_Error']['fields']['corrective_action_1_c']['labelValue']='Corrective Action Within the Study';
$dictionary['M06_Error']['fields']['corrective_action_1_c']['dependency']='';
$dictionary['M06_Error']['fields']['corrective_action_1_c']['required_formula']='';
$dictionary['M06_Error']['fields']['corrective_action_1_c']['visibility_grid']=array (
  'trigger' => 'error_classification_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Adverse Event' => 
    array (
      0 => 'Further Investigation of Event',
    ),
    'Error' => 
    array (
      0 => '',
      1 => 'SOP Update',
      2 => 'Further Investigation of Event',
      3 => 'None',
      4 => 'Other',
      5 => 'Protocol Amendment',
      6 => 'Protocol Requirement Review',
      7 => 'Report Amendment',
      8 => 'Protocol Specific Training',
    ),
    'Duplicate' => 
    array (
    ),
    'Impactful Deviation' => 
    array (
      0 => '',
      1 => 'SOP Update',
      2 => 'Further Investigation of Event',
      3 => 'None',
      4 => 'Other',
      5 => 'Protocol Amendment',
      6 => 'Protocol Requirement Review',
      7 => 'Report Amendment',
      8 => 'Protocol Specific Training',
    ),
    'Job well done' => 
    array (
    ),
    'Notification' => 
    array (
    ),
    'Observation' => 
    array (
    ),
    'Opportunity for Improvement' => 
    array (
    ),
    'Pending' => 
    array (
    ),
    'Non Impactful Deviation' => 
    array (
    ),
    'Unforeseen Circumstance Unanticipated Response' => 
    array (
    ),
    'Withdrawn Error' => 
    array (
    ),
    'Choose One' => 
    array (
    ),
    'A' => 
    array (
    ),
    'M' => 
    array (
    ),
    'S' => 
    array (
    ),
    'C' => 
    array (
    ),
    'NA' => 
    array (
    ),
    'Major Change or New' => 
    array (
    ),
    'Revision Change or Minor Change' => 
    array (
    ),
    'Employee did NOT perform SOP tasks while overdue' => 
    array (
    ),
    'The Controlled Document is a standalone document. No Controlled Document activities are associated' => 
    array (
    ),
    'Overdue refresher training' => 
    array (
    ),
    'Adverse Event Study Article Related' => 
    array (
      0 => 'Further Investigation of Event',
    ),
    'Adverse Event Procedure Related' => 
    array (
      0 => 'Further Investigation of Event',
    ),
    'Adverse Event Test System Model Related' => 
    array (
      0 => 'Further Investigation of Event',
    ),
    'Adverse Event Etiology Unknown' => 
    array (
      0 => 'Further Investigation of Event',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_rationale_for_no_ca_c.php

 // created: 2020-12-22 09:17:23
$dictionary['M06_Error']['fields']['rationale_for_no_ca_c']['labelValue']='Corrective Action Details';
$dictionary['M06_Error']['fields']['rationale_for_no_ca_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['rationale_for_no_ca_c']['enforced']='';
$dictionary['M06_Error']['fields']['rationale_for_no_ca_c']['dependency']='isInList($error_classification_c,createList("Error","Impactful Deviation","Adverse Event","Adverse Event Study Article Related","Adverse Event Procedure Related","Adverse Event Test System Model Related","Adverse Event Etiology Unknown"))';
$dictionary['M06_Error']['fields']['rationale_for_no_ca_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_sd_reassessment_date_c.php

 // created: 2021-01-12 17:07:14
$dictionary['M06_Error']['fields']['target_sd_reassessment_date_c']['labelValue']='Target SD Reassessment Date';
$dictionary['M06_Error']['fields']['target_sd_reassessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['target_sd_reassessment_date_c']['dependency']='equal($sd_reassessment_required_c,"Yes")';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_high_level_root_cause_action_c.php

 // created: 2021-01-26 09:11:54
$dictionary['M06_Error']['fields']['high_level_root_cause_action_c']['labelValue']='High Level Root Cause/Action';
$dictionary['M06_Error']['fields']['high_level_root_cause_action_c']['dependency']='not(equal($error_category_c,"IACUC Deficiency"))';
$dictionary['M06_Error']['fields']['high_level_root_cause_action_c']['required_formula']='not(equal($mgt_assessment_c,""))';
$dictionary['M06_Error']['fields']['high_level_root_cause_action_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_expected_event_c.php

 // created: 2021-01-25 14:07:00
$dictionary['M06_Error']['fields']['expected_event_c']['labelValue']='Expected Event';
$dictionary['M06_Error']['fields']['expected_event_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['expected_event_c']['enforced']='';
$dictionary['M06_Error']['fields']['expected_event_c']['dependency']='isInList($error_category_c,createList("Critical Phase Inspection","Daily QC","Data Book QC","Process Audit","Real time study conduct","Weekly Sweep","Retrospective Data QC","Training","Test Failure","Internal Feedback","IACUC Deficiency"))';
$dictionary['M06_Error']['fields']['expected_event_c']['required_formula']='isInList($error_category_c,createList("Critical Phase Inspection","Daily QC","Data Book QC","Process Audit","Real time study conduct","Weekly Sweep","Retrospective Data QC","Training","Test Failure","Internal Feedback"))';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_error_classification_c.php

 // created: 2021-01-26 19:12:17
$dictionary['M06_Error']['fields']['error_classification_c']['labelValue']='Classification';
$dictionary['M06_Error']['fields']['error_classification_c']['dependency']='';
$dictionary['M06_Error']['fields']['error_classification_c']['required_formula']='and(not(equal($resolution_c,"")),not(isInList($error_category_c,createList("IACUC Deficiency","Training","Work Product Schedule Outcome","Equipment Maintenance Request","External Audit","Feedback","Maintenance Request","Internal Feedback","Process Audit","Weekly Sweep"))))';
$dictionary['M06_Error']['fields']['error_classification_c']['visibility_grid']=array (
  'trigger' => 'error_category_c',
  'values' => 
  array (
    'Sttudy' => 
    array (
      0 => 'Choose One',
      1 => 'Error',
      2 => 'Impactful Deviation',
      3 => 'Observation',
      4 => 'Pending',
    ),
    'Facility' => 
    array (
      0 => 'Choose One',
      1 => 'Error',
      2 => 'Impactful Deviation',
      3 => 'Observation',
      4 => 'Pending',
    ),
    'Choose One' => 
    array (
      0 => '',
      1 => 'Adverse Event',
      2 => 'Error',
      3 => 'Duplicate',
      4 => 'Notification',
      5 => 'Opportunity for Improvement',
      6 => 'Pending',
      7 => 'Impactful Deviation',
      8 => 'Observation',
      9 => 'Non Impactful Deviation',
      10 => 'Unforeseen Circumstance Unanticipated Response',
      11 => 'Withdrawn Error',
    ),
    'Critical Phase Inspection' => 
    array (
      0 => '',
      1 => 'Addressed CPI COM',
      2 => 'Adverse Event Study Article Related',
      3 => 'Adverse Event Procedure Related',
      4 => 'Adverse Event Test System Model Related',
      5 => 'Adverse Event Etiology Unknown',
      6 => 'Error',
      7 => 'Duplicate',
      8 => 'Notification',
      9 => 'Opportunity for Improvement',
      10 => 'Pending',
      11 => 'Impactful Deviation',
      12 => 'Observation',
      13 => 'Non Impactful Deviation',
      14 => 'Unforeseen Circumstance Unanticipated Response',
      15 => 'Withdrawn Error',
      16 => 'Adverse Event',
    ),
    'Deceased Animal' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Duplicate',
      6 => 'Adverse Event',
    ),
    'Maintenance Request' => 
    array (
    ),
    'Observation' => 
    array (
      0 => 'Choose One',
      1 => 'Error',
      2 => 'Impactful Deviation',
      3 => 'Observation',
      4 => 'Pending',
    ),
    'Rejected Animal' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Impactful Deviation',
      11 => 'Observation',
      12 => 'Non Impactful Deviation',
      13 => 'Unforeseen Circumstance Unanticipated Response',
      14 => 'Withdrawn Error',
      15 => 'Adverse Event',
    ),
    'Vet Check' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Impactful Deviation',
      11 => 'Observation',
      12 => 'Non Impactful Deviation',
      13 => 'Unforeseen Circumstance Unanticipated Response',
      14 => 'Withdrawn Error',
      15 => 'Adverse Event',
    ),
    '' => 
    array (
    ),
    'Daily QC' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Impactful Deviation',
      11 => 'Observation',
      12 => 'Non Impactful Deviation',
      13 => 'Unforeseen Circumstance Unanticipated Response',
      14 => 'Withdrawn Error',
      15 => 'Adverse Event',
    ),
    'Data Book QC' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Impactful Deviation',
      11 => 'Observation',
      12 => 'Non Impactful Deviation',
      13 => 'Unforeseen Circumstance Unanticipated Response',
      14 => 'Withdrawn Error',
      15 => 'Adverse Event',
    ),
    'Real time study conduct' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Impactful Deviation',
      11 => 'Observation',
      12 => 'Non Impactful Deviation',
      13 => 'Unforeseen Circumstance Unanticipated Response',
      14 => 'Withdrawn Error',
      15 => 'Adverse Event',
    ),
    'Weekly Sweep' => 
    array (
    ),
    'External Audit' => 
    array (
    ),
    'Feedback' => 
    array (
      0 => '',
      1 => 'Job well done',
      2 => 'Opportunity for Improvement',
    ),
    'Process Audit' => 
    array (
    ),
    'Project Management' => 
    array (
      0 => 'Notification',
    ),
    'Gross Pathology' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Impactful Deviation',
      11 => 'Observation',
      12 => 'Non Impactful Deviation',
      13 => 'Unforeseen Circumstance Unanticipated Response',
      14 => 'Withdrawn Error',
      15 => 'Adverse Event',
    ),
    'Internal Feedback' => 
    array (
    ),
    'IACUC Deficiency' => 
    array (
    ),
    'Equipment Maintenance Request' => 
    array (
    ),
    'Retrospective Data QC' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Adverse Event',
    ),
    'Training' => 
    array (
    ),
    'Test Failure' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
      10 => 'Adverse Event',
    ),
    'Work Product Schedule Outcome' => 
    array (
      0 => '',
      1 => 'Adverse Event Study Article Related',
      2 => 'Adverse Event Procedure Related',
      3 => 'Adverse Event Test System Model Related',
      4 => 'Adverse Event Etiology Unknown',
      5 => 'Error',
      6 => 'Duplicate',
      7 => 'Notification',
      8 => 'Opportunity for Improvement',
      9 => 'Pending',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_date_time_discovered_text_c.php

 // created: 2021-02-11 14:46:56
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['required']=true;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['name']='date_time_discovered_text_c';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['vname']='LBL_DATE_TIME_DISCOVERED';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['type']='varchar';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['massupdate']=false;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['no_default']=false;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['comments']='';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['help']='';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['importable']='false';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['duplicate_merge']='disabled';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['duplicate_merge_dom_value']='0';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['audited']=true;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['reportable']=true;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['unified_search']=false;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['merge_filter']='disabled';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['calculated']=false;
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['len']='255';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['size']='20';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['source']='custom_fields';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['dependency']='or(equal($error_category_c,"Deceased Animal"),equal($error_type_c,"Found Deceased"))';
$dictionary['M06_Error']['fields']['date_time_discovered_text_c']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_date_time_discovered_c.php

 // created: 2021-02-11 14:47:52
$dictionary['M06_Error']['fields']['date_time_discovered_c']['labelValue']='Date Time Discovered1';
$dictionary['M06_Error']['fields']['date_time_discovered_c']['enforced']='';
$dictionary['M06_Error']['fields']['date_time_discovered_c']['dependency']='or(equal($error_category_c,"Deceased Animal"),equal($error_type_c,"Found Deceased"))';
$dictionary['M06_Error']['fields']['date_time_discovered_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_tpr_reference_c.php

 // created: 2021-05-25 07:11:12
$dictionary['M06_Error']['fields']['tpr_reference_c']['labelValue']='TPR Reference';
$dictionary['M06_Error']['fields']['tpr_reference_c']['dependency']='';
$dictionary['M06_Error']['fields']['tpr_reference_c']['required_formula']='';
$dictionary['M06_Error']['fields']['tpr_reference_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['tpr_reference_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_subjectiveobjective_exam_c.php

 // created: 2021-05-25 07:14:02
$dictionary['M06_Error']['fields']['subjectiveobjective_exam_c']['labelValue']='Subjective/Objective Examination';
$dictionary['M06_Error']['fields']['subjectiveobjective_exam_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['subjectiveobjective_exam_c']['enforced']='';
$dictionary['M06_Error']['fields']['subjectiveobjective_exam_c']['dependency']='';
$dictionary['M06_Error']['fields']['subjectiveobjective_exam_c']['required_formula']='';
$dictionary['M06_Error']['fields']['subjectiveobjective_exam_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_notify_vet_c.php

 // created: 2021-05-25 07:17:20
$dictionary['M06_Error']['fields']['notify_vet_c']['labelValue']='Notify a veterinarian if the following clinical signs are observed';
$dictionary['M06_Error']['fields']['notify_vet_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['notify_vet_c']['enforced']='';
$dictionary['M06_Error']['fields']['notify_vet_c']['dependency']='';
$dictionary['M06_Error']['fields']['notify_vet_c']['required_formula']='';
$dictionary['M06_Error']['fields']['notify_vet_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_plan_comments_c.php

 // created: 2021-05-25 07:19:13
$dictionary['M06_Error']['fields']['plan_comments_c']['labelValue']='Plan Comments';
$dictionary['M06_Error']['fields']['plan_comments_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['plan_comments_c']['enforced']='';
$dictionary['M06_Error']['fields']['plan_comments_c']['dependency']='';
$dictionary['M06_Error']['fields']['plan_comments_c']['required_formula']='';
$dictionary['M06_Error']['fields']['plan_comments_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_refer_to_med_treatment_form_c.php

 // created: 2021-05-25 07:22:28
$dictionary['M06_Error']['fields']['refer_to_med_treatment_form_c']['labelValue']='Refer to F-S-IL-GN-OP-010-01 – Medical Treatments Form';
$dictionary['M06_Error']['fields']['refer_to_med_treatment_form_c']['enforced']='';
$dictionary['M06_Error']['fields']['refer_to_med_treatment_form_c']['dependency']='';
$dictionary['M06_Error']['fields']['refer_to_med_treatment_form_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_related_to_c.php

 // created: 2021-06-01 10:02:36
$dictionary['M06_Error']['fields']['related_to_c']['labelValue']='Related to';
$dictionary['M06_Error']['fields']['related_to_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal","Work Product Schedule Outcome"))';
$dictionary['M06_Error']['fields']['related_to_c']['required_formula']='';
$dictionary['M06_Error']['fields']['related_to_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['related_to_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_organ_system_c.php

 // created: 2021-06-01 10:04:23
$dictionary['M06_Error']['fields']['organ_system_c']['labelValue']='Organ System';
$dictionary['M06_Error']['fields']['organ_system_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal","Work Product Schedule Outcome"))';
$dictionary['M06_Error']['fields']['organ_system_c']['required_formula']='';
$dictionary['M06_Error']['fields']['organ_system_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['organ_system_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_diagnosis_c.php

 // created: 2021-06-01 10:06:29
$dictionary['M06_Error']['fields']['diagnosis_c']['labelValue']='Diagnosis';
$dictionary['M06_Error']['fields']['diagnosis_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal","Work Product Schedule Outcome"))';
$dictionary['M06_Error']['fields']['diagnosis_c']['required_formula']='';
$dictionary['M06_Error']['fields']['diagnosis_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['diagnosis_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_related_text_c.php

 // created: 2021-06-03 10:18:21
$dictionary['M06_Error']['fields']['related_text_c']['labelValue']='Related Comm Data';
$dictionary['M06_Error']['fields']['related_text_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['related_text_c']['enforced']='';
$dictionary['M06_Error']['fields']['related_text_c']['dependency']='';
$dictionary['M06_Error']['fields']['related_text_c']['required_formula']='';
$dictionary['M06_Error']['fields']['related_text_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_commapp_mismatch_c.php

 // created: 2021-06-03 10:19:44
$dictionary['M06_Error']['fields']['commapp_mismatch_c']['labelValue']='Commapp Mismatch Update';
$dictionary['M06_Error']['fields']['commapp_mismatch_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['commapp_mismatch_c']['enforced']='';
$dictionary['M06_Error']['fields']['commapp_mismatch_c']['dependency']='';
$dictionary['M06_Error']['fields']['commapp_mismatch_c']['required_formula']='';
$dictionary['M06_Error']['fields']['commapp_mismatch_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_vet_assessment_date_c.php

 // created: 2021-06-10 11:44:55
$dictionary['M06_Error']['fields']['vet_assessment_date_c']['labelValue']='Vet Assessment Date';
$dictionary['M06_Error']['fields']['vet_assessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['vet_assessment_date_c']['dependency']='isInList($error_category_c,createList("Vet Check","Gross Pathology","Deceased Animal"))';
$dictionary['M06_Error']['fields']['vet_assessment_date_c']['required_formula']='';
$dictionary['M06_Error']['fields']['vet_assessment_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_vet_reassessment_date_c.php

 // created: 2021-06-15 11:35:43
$dictionary['M06_Error']['fields']['vet_reassessment_date_c']['labelValue']='Actual Vet Reassessment Date';
$dictionary['M06_Error']['fields']['vet_reassessment_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['vet_reassessment_date_c']['dependency']='and(equal($vet_reassessment_required_c,true),isBefore($date_entered,date("6/7/2021")))';
$dictionary['M06_Error']['fields']['vet_reassessment_date_c']['required_formula']='';
$dictionary['M06_Error']['fields']['vet_reassessment_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_vet_reassessment_notes_c.php

 // created: 2021-06-15 11:36:18
$dictionary['M06_Error']['fields']['vet_reassessment_notes_c']['labelValue']='Vet Reassessment';
$dictionary['M06_Error']['fields']['vet_reassessment_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['vet_reassessment_notes_c']['enforced']='';
$dictionary['M06_Error']['fields']['vet_reassessment_notes_c']['dependency']='and(equal($vet_reassessment_required_c,true),isBefore($date_entered,date("6/7/2021")))';
$dictionary['M06_Error']['fields']['vet_reassessment_notes_c']['required_formula']='';
$dictionary['M06_Error']['fields']['vet_reassessment_notes_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_procedure_type_c.php

 // created: 2021-07-01 09:27:01
$dictionary['M06_Error']['fields']['procedure_type_c']['labelValue']='Procedure Type';
$dictionary['M06_Error']['fields']['procedure_type_c']['dependency']='equal($error_category_c,"Work Product Schedule Outcome")';
$dictionary['M06_Error']['fields']['procedure_type_c']['required_formula']='';
$dictionary['M06_Error']['fields']['procedure_type_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['procedure_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_actual_event_c.php

 // created: 2021-08-05 06:53:27
$dictionary['M06_Error']['fields']['actual_event_c']['labelValue']='Actual Event';
$dictionary['M06_Error']['fields']['actual_event_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['actual_event_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_event_c']['dependency']='isInList($error_category_c,createList("Critical Phase Inspection","Daily QC","Data Book QC","Deceased Animal","External Audit","Feedback","Maintenance Request","Process Audit","Real time study conduct","Rejected Animal","Vet Check","Weekly Sweep","Internal Feedback","Gross Pathology","IACUC Deficiency","Equipment Maintenance Request","Retrospective Data QC","Training","Test Failure","Work Product Schedule Outcome","Study Specific Charge"))';
$dictionary['M06_Error']['fields']['actual_event_c']['required_formula']='isInList($error_category_c,createList("Critical Phase Inspection","Daily QC","Data Book QC","Deceased Animal","External Audit","Feedback","Maintenance Request","Process Audit","Real time study conduct","Rejected Animal","Vet Check","Weekly Sweep","Internal Feedback","Gross Pathology","IACUC Deficiency","Equipment Maintenance Request","Retrospective Data QC","Training","Test Failure","Study Specific Charge"))';
$dictionary['M06_Error']['fields']['actual_event_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_manager_assessment_duration_c.php

 // created: 2021-08-12 08:52:13
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['labelValue']='Manager Assessment Duration';
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['calculated']='true';
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['formula']='ifElse(equal($actual_management_assessment_c,""),"",subtract(daysUntil($actual_management_assessment_c),daysUntil($date_entered)))';
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['enforced']='true';
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['dependency']='';
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['required_formula']='';
$dictionary['M06_Error']['fields']['manager_assessment_duration_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_com_usda_id_c.php

 // created: 2021-08-12 13:19:10
$dictionary['M06_Error']['fields']['com_usda_id_c']['labelValue']='Com USDA Id';
$dictionary['M06_Error']['fields']['com_usda_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['com_usda_id_c']['enforced']='';
$dictionary['M06_Error']['fields']['com_usda_id_c']['dependency']='';
$dictionary['M06_Error']['fields']['com_usda_id_c']['required_formula']='';
$dictionary['M06_Error']['fields']['com_usda_id_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_reinspection_date_c.php

 // created: 2021-09-09 07:58:01
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['labelValue']='Target SD Assessment Date';
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['formula']='addDays($date_entered,7)';
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['dependency']='and(not(equal($actual_sd_ack_date_c,"")),not(equal($error_category_c,"Study Specific Charge")))';
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['required_formula']='';
$dictionary['M06_Error']['fields']['target_reinspection_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_reinspection_date_c.php

 // created: 2021-09-09 07:59:51
$dictionary['M06_Error']['fields']['reinspection_date_c']['labelValue']='Actual SD Assessment Date';
$dictionary['M06_Error']['fields']['reinspection_date_c']['enforced']='';
$dictionary['M06_Error']['fields']['reinspection_date_c']['dependency']='and(not(equal($actual_sd_ack_date_c,"")),not(equal($error_category_c,"Study Specific Charge")))';
$dictionary['M06_Error']['fields']['reinspection_date_c']['required_formula']='not(equal($resolution_c,""))';
$dictionary['M06_Error']['fields']['reinspection_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_resolution_c.php

 // created: 2021-09-09 08:01:55
$dictionary['M06_Error']['fields']['resolution_c']['labelValue']='SD Assessment';
$dictionary['M06_Error']['fields']['resolution_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['resolution_c']['enforced']='';
$dictionary['M06_Error']['fields']['resolution_c']['dependency']='and(not(equal($actual_sd_ack_date_c,"")),not(equal($error_category_c,"Study Specific Charge")))';
$dictionary['M06_Error']['fields']['resolution_c']['required_formula']='';
$dictionary['M06_Error']['fields']['resolution_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/w_weight_m06_error_2_M06_Error.php

// created: 2021-09-14 09:20:50
$dictionary["M06_Error"]["fields"]["w_weight_m06_error_2"] = array (
  'name' => 'w_weight_m06_error_2',
  'type' => 'link',
  'relationship' => 'w_weight_m06_error_2',
  'source' => 'non-db',
  'module' => 'W_Weight',
  'bean_name' => 'W_Weight',
  'side' => 'right',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_2_FROM_M06_ERROR_TITLE',
  'id_name' => 'w_weight_m06_error_2w_weight_ida',
  'link-type' => 'one',
);
$dictionary["M06_Error"]["fields"]["w_weight_m06_error_2_name"] = array (
  'name' => 'w_weight_m06_error_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_2_FROM_W_WEIGHT_TITLE',
  'save' => true,
  'id_name' => 'w_weight_m06_error_2w_weight_ida',
  'link' => 'w_weight_m06_error_2',
  'table' => 'w_weight',
  'module' => 'W_Weight',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["w_weight_m06_error_2w_weight_ida"] = array (
  'name' => 'w_weight_m06_error_2w_weight_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_2_FROM_M06_ERROR_TITLE_ID',
  'id_name' => 'w_weight_m06_error_2w_weight_ida',
  'link' => 'w_weight_m06_error_2',
  'table' => 'w_weight',
  'module' => 'W_Weight',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_mgt_assessment_c.php

 // created: 2021-12-21 08:34:57
$dictionary['M06_Error']['fields']['mgt_assessment_c']['labelValue']='Management Assessment';
$dictionary['M06_Error']['fields']['mgt_assessment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['mgt_assessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['mgt_assessment_c']['dependency']='not(equal($error_category_c,"IACUC Deficiency"))';
$dictionary['M06_Error']['fields']['mgt_assessment_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_actual_management_assessment_c.php

 // created: 2021-12-21 08:38:52
$dictionary['M06_Error']['fields']['actual_management_assessment_c']['labelValue']='Actual Subtype Date';
$dictionary['M06_Error']['fields']['actual_management_assessment_c']['enforced']='';
$dictionary['M06_Error']['fields']['actual_management_assessment_c']['dependency']='not(equal($error_category_c,"IACUC Deficiency"))';
$dictionary['M06_Error']['fields']['actual_management_assessment_c']['required_formula']='';
$dictionary['M06_Error']['fields']['actual_management_assessment_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_target_management_assessment_c.php

 // created: 2021-12-21 08:45:49
$dictionary['M06_Error']['fields']['target_management_assessment_c']['duplicate_merge_dom_value']=0;
$dictionary['M06_Error']['fields']['target_management_assessment_c']['labelValue']='Target Subtype Date';
$dictionary['M06_Error']['fields']['target_management_assessment_c']['calculated']='1';
$dictionary['M06_Error']['fields']['target_management_assessment_c']['formula']='addDays($date_entered,3)';
$dictionary['M06_Error']['fields']['target_management_assessment_c']['enforced']='1';
$dictionary['M06_Error']['fields']['target_management_assessment_c']['dependency']='not(equal($error_category_c,"IACUC Deficiency"))';
$dictionary['M06_Error']['fields']['target_management_assessment_c']['required_formula']='';
$dictionary['M06_Error']['fields']['target_management_assessment_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_subtype_hidden_field_c.php

 // created: 2021-12-28 05:21:30
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['labelValue']='Subtype Hidden Field';
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['enforced']='';
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['dependency']='';
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['required_formula']='';
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['readonly']='1';
$dictionary['M06_Error']['fields']['subtype_hidden_field_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/m06_error_de_deviation_employees_2_M06_Error.php

// created: 2022-01-03 10:55:30
$dictionary["M06_Error"]["fields"]["m06_error_de_deviation_employees_2"] = array (
  'name' => 'm06_error_de_deviation_employees_2',
  'type' => 'link',
  'relationship' => 'm06_error_de_deviation_employees_2',
  'source' => 'non-db',
  'module' => 'DE_Deviation_Employees',
  'bean_name' => 'DE_Deviation_Employees',
  'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_2_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'id_name' => 'm06_error_de_deviation_employees_2de_deviation_employees_idb',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_sedated_c.php

 // created: 2022-01-11 06:47:34
$dictionary['M06_Error']['fields']['sedated_c']['labelValue']='Sedated';
$dictionary['M06_Error']['fields']['sedated_c']['dependency']='isInList($error_type_c,createList("Not Used","Not used shared BU","Rejected"))';
$dictionary['M06_Error']['fields']['sedated_c']['required_formula']='';
$dictionary['M06_Error']['fields']['sedated_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['sedated_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_substance_administration_c.php

 // created: 2022-01-11 06:49:22
$dictionary['M06_Error']['fields']['substance_administration_c']['labelValue']='Substance Administration';
$dictionary['M06_Error']['fields']['substance_administration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['substance_administration_c']['enforced']='';
$dictionary['M06_Error']['fields']['substance_administration_c']['dependency']='isInList($error_type_c,createList("Not Used","Not used shared BU","Rejected"))';
$dictionary['M06_Error']['fields']['substance_administration_c']['required_formula']='';
$dictionary['M06_Error']['fields']['substance_administration_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_blood_collection_c.php

 // created: 2022-01-11 06:52:01
$dictionary['M06_Error']['fields']['blood_collection_c']['labelValue']='Blood Collected';
$dictionary['M06_Error']['fields']['blood_collection_c']['dependency']='isInList($error_type_c,createList("Not Used","Not used shared BU","Rejected"))';
$dictionary['M06_Error']['fields']['blood_collection_c']['required_formula']='';
$dictionary['M06_Error']['fields']['blood_collection_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['blood_collection_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_surgical_access_c.php

 // created: 2022-01-11 06:55:19
$dictionary['M06_Error']['fields']['surgical_access_c']['labelValue']='Surgical Access';
$dictionary['M06_Error']['fields']['surgical_access_c']['dependency']='isInList($error_type_c,createList("Not Used","Not used shared BU","Rejected"))';
$dictionary['M06_Error']['fields']['surgical_access_c']['required_formula']='';
$dictionary['M06_Error']['fields']['surgical_access_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['surgical_access_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_surgical_access_details_c.php

 // created: 2022-01-11 06:57:02
$dictionary['M06_Error']['fields']['surgical_access_details_c']['labelValue']='Surgical Access Details';
$dictionary['M06_Error']['fields']['surgical_access_details_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['surgical_access_details_c']['enforced']='';
$dictionary['M06_Error']['fields']['surgical_access_details_c']['dependency']='equal($surgical_access_c,"Yes")';
$dictionary['M06_Error']['fields']['surgical_access_details_c']['required_formula']='equal($surgical_access_c,"Yes")';
$dictionary['M06_Error']['fields']['surgical_access_details_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_test_article_exposure_c.php

 // created: 2022-01-11 07:01:37
$dictionary['M06_Error']['fields']['test_article_exposure_c']['labelValue']='Test Article Exposure';
$dictionary['M06_Error']['fields']['test_article_exposure_c']['dependency']='isInList($error_type_c,createList("Not Used","Not used shared BU","Rejected"))';
$dictionary['M06_Error']['fields']['test_article_exposure_c']['required_formula']='';
$dictionary['M06_Error']['fields']['test_article_exposure_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['test_article_exposure_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_test_article_exposure_detail_c.php

 // created: 2022-01-11 07:03:42
$dictionary['M06_Error']['fields']['test_article_exposure_detail_c']['labelValue']='Test Article Exposure Details';
$dictionary['M06_Error']['fields']['test_article_exposure_detail_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['test_article_exposure_detail_c']['enforced']='';
$dictionary['M06_Error']['fields']['test_article_exposure_detail_c']['dependency']='equal($test_article_exposure_c,"Yes")';
$dictionary['M06_Error']['fields']['test_article_exposure_detail_c']['required_formula']='equal($test_article_exposure_c,"Yes")';
$dictionary['M06_Error']['fields']['test_article_exposure_detail_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/capa_capa_m06_error_1_M06_Error.php

// created: 2022-02-03 07:30:32
$dictionary["M06_Error"]["fields"]["capa_capa_m06_error_1"] = array (
  'name' => 'capa_capa_m06_error_1',
  'type' => 'link',
  'relationship' => 'capa_capa_m06_error_1',
  'source' => 'non-db',
  'module' => 'CAPA_CAPA',
  'bean_name' => 'CAPA_CAPA',
  'side' => 'right',
  'vname' => 'LBL_CAPA_CAPA_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'capa_capa_m06_error_1capa_capa_ida',
  'link-type' => 'one',
);
$dictionary["M06_Error"]["fields"]["capa_capa_m06_error_1_name"] = array (
  'name' => 'capa_capa_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CAPA_CAPA_M06_ERROR_1_FROM_CAPA_CAPA_TITLE',
  'save' => true,
  'id_name' => 'capa_capa_m06_error_1capa_capa_ida',
  'link' => 'capa_capa_m06_error_1',
  'table' => 'capa_capa',
  'module' => 'CAPA_CAPA',
  'rname' => 'name',
);
$dictionary["M06_Error"]["fields"]["capa_capa_m06_error_1capa_capa_ida"] = array (
  'name' => 'capa_capa_m06_error_1capa_capa_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CAPA_CAPA_M06_ERROR_1_FROM_M06_ERROR_TITLE_ID',
  'id_name' => 'capa_capa_m06_error_1capa_capa_ida',
  'link' => 'capa_capa_m06_error_1',
  'table' => 'capa_capa',
  'module' => 'CAPA_CAPA',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_condition_2_c.php

 // created: 2022-02-11 19:50:59
$dictionary['M06_Error']['fields']['condition_2_c']['labelValue']='Clinical Symptoms';
$dictionary['M06_Error']['fields']['condition_2_c']['dependency']='';
$dictionary['M06_Error']['fields']['condition_2_c']['required_formula']='';
$dictionary['M06_Error']['fields']['condition_2_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['condition_2_c']['visibility_grid']=array (
  'trigger' => 'organ_system_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Cardiovascular' => 
    array (
      0 => '',
      1 => 'Arrhythmia',
      2 => 'Bradycardia',
      3 => 'Heart block',
      4 => 'Heart murmur',
      5 => 'Hypoxia',
      6 => 'Muffled heart sounds',
      7 => 'Other',
      8 => 'Pericardial effusion',
      9 => 'Tachycardia',
      10 => 'Prolonged CRT Cyanosis',
    ),
    'GI' => 
    array (
      0 => '',
      1 => 'Abdominal distension',
      2 => 'Bloat',
      3 => 'Choke',
      4 => 'Constipation',
      5 => 'Diarrhea',
      6 => 'Gingivitis',
      7 => 'Hematemesis',
      8 => 'Hematochezia',
      9 => 'Inappetence',
      10 => 'Internal Parasites',
      11 => 'Melena',
      12 => 'Nausea',
      13 => 'Other',
      14 => 'Peritoneal effusionAscites',
      15 => 'Ptyalism',
      16 => 'Rectal Bleeding',
      17 => 'Rectal Prolapse',
      18 => 'Regurgitation',
      19 => 'Ulcer',
      20 => 'Vomiting',
      21 => 'Dental disease',
      22 => 'Icteric',
      23 => 'Ileus',
    ),
    'Integumentary' => 
    array (
      0 => '',
      1 => 'Abrasion',
      2 => 'Alopecia',
      3 => 'Blisters',
      4 => 'Bruising',
      5 => 'Discharge',
      6 => 'Ectoparasite',
      7 => 'Edema',
      8 => 'Erythema',
      9 => 'Hoof crack',
      10 => 'Hyperpigmentation',
      11 => 'Laceration',
      12 => 'Mass',
      13 => 'Nodule',
      14 => 'Other',
      15 => 'PapulesPustules',
      16 => 'Pruritis',
      17 => 'Pustules',
      18 => 'Rash',
      19 => 'Scabbing',
      20 => 'Swelling',
      21 => 'Urticaria',
      22 => 'Ulceration',
      23 => 'Wound',
      24 => 'Otitis Externa',
    ),
    'Musculoskeletal' => 
    array (
      0 => '',
      1 => 'Atrophy',
      2 => 'Conformation',
      3 => 'Joint Swelling',
      4 => 'Lame',
      5 => 'Other',
      6 => 'Poor Confirmation',
    ),
    'Neurologic' => 
    array (
      0 => '',
      1 => 'Ataxia',
      2 => 'Deafness',
      3 => 'Head Tilt',
      4 => 'Nystagmus',
      5 => 'Other',
      6 => 'Paresis',
      7 => 'Paralysis',
      8 => 'Seizure',
      9 => 'Syncope',
    ),
    'Ophthalmic' => 
    array (
      0 => '',
      1 => 'Blindness',
      2 => 'Blepharospasm',
      3 => 'Chemosis',
      4 => 'Conjunctivits',
      5 => 'Corneal Edema',
      6 => 'Corneal Opacity',
      7 => 'Discharge',
      8 => 'Ectropion',
      9 => 'Enophthalmos',
      10 => 'Entropion',
      11 => 'Horners',
      12 => 'Hyphema',
      13 => 'Microphthalmia',
      14 => 'NeovascularizationPannus',
      15 => 'Other',
      16 => 'Scleral Injection',
    ),
    'Respiratory' => 
    array (
      0 => '',
      1 => 'Apnea',
      2 => 'Coughing',
      3 => 'Dyspnea',
      4 => 'Hemoptysis',
      5 => 'Lung lesions',
      6 => 'Nasal discharge',
      7 => 'Other',
      8 => 'Pleural effusion',
      9 => 'Respiratory Stridor',
      10 => 'Tachypnea',
      11 => 'Respiratory distress',
    ),
    'Surgical' => 
    array (
      0 => '',
      1 => 'Bandage abnormality',
      2 => 'Bruising',
      3 => 'Catheter Site Swelling',
      4 => 'Dehiscence',
      5 => 'Discharge',
      6 => 'Hemorrhage',
      7 => 'Incisional swelling',
      8 => 'Other',
      9 => 'Recumbency',
      10 => 'Self mutilation',
      11 => 'Swelling',
      12 => 'Anesthesia',
      13 => 'Infection',
      14 => 'Intubation',
      15 => 'Mass',
      16 => 'Post op Swelling',
    ),
    'Systemic' => 
    array (
      0 => '',
      1 => 'Anaphylaxis',
      2 => 'Dehydration',
      3 => 'Hemorrhage',
      4 => 'Hypertension',
      5 => 'Hypoglycemia',
      6 => 'Hypotension',
      7 => 'Hypothermia',
      8 => 'Icterus',
      9 => 'Malaise',
      10 => 'Obesity',
      11 => 'Other',
      12 => 'Pallor',
      13 => 'PetechiaEcchyomoses',
      14 => 'Pyrexia',
      15 => 'Shock',
      16 => 'Underweight',
      17 => 'Weight loss',
      18 => 'Lymphadenopathy',
      19 => 'Icteric',
    ),
    'Unknown ADR' => 
    array (
      0 => '',
      1 => 'ADR',
      2 => 'Other',
    ),
    'Urogential' => 
    array (
      0 => '',
      1 => 'Bilirubinuria',
      2 => 'Discolored urine',
      3 => 'Hematuria',
      4 => 'Other',
      5 => 'Paraphimosis',
      6 => 'Porphyrinuria',
      7 => 'Preputial Swelling',
      8 => 'Pyuria',
      9 => 'StrainingDysuria',
      10 => 'Trauma',
      11 => 'Udder Enlargement',
      12 => 'Urinary obstruction',
      13 => 'Vulvar discharge',
      14 => 'Vulvar Swelling',
      15 => 'Azotemia',
    ),
    'Lymphatic' => 
    array (
      0 => '',
      1 => 'Abscess',
      2 => 'Other',
      3 => 'Lymphadenopathy',
      4 => 'Necropsy Finding',
    ),
    'Nervous System' => 
    array (
    ),
    'Integument' => 
    array (
    ),
    'Urogenital' => 
    array (
    ),
    'Clin Path' => 
    array (
    ),
    'Not Applicable' => 
    array (
      0 => '',
      1 => 'Normal Exam',
      2 => 'QAcclimation Activity',
      3 => 'Post Op Exam',
      4 => 'Routine Exam',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/bid_batch_id_m06_error_1_M06_Error.php

// created: 2022-04-26 07:03:59
$dictionary["M06_Error"]["fields"]["bid_batch_id_m06_error_1"] = array (
  'name' => 'bid_batch_id_m06_error_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_m06_error_1',
  'source' => 'non-db',
  'module' => 'BID_Batch_ID',
  'bean_name' => 'BID_Batch_ID',
  'vname' => 'LBL_BID_BATCH_ID_M06_ERROR_1_FROM_BID_BATCH_ID_TITLE',
  'id_name' => 'bid_batch_id_m06_error_1bid_batch_id_ida',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_on_bid_list_c.php

 // created: 2022-09-08 05:51:51
$dictionary['M06_Error']['fields']['on_bid_list_c']['labelValue']='On BID list';
$dictionary['M06_Error']['fields']['on_bid_list_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['on_bid_list_c']['enforced']='false';
$dictionary['M06_Error']['fields']['on_bid_list_c']['dependency']='';
$dictionary['M06_Error']['fields']['on_bid_list_c']['required_formula']='';
$dictionary['M06_Error']['fields']['on_bid_list_c']['readonly']='1';
$dictionary['M06_Error']['fields']['on_bid_list_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_data_reviewed_by_c.php

 // created: 2022-09-08 05:53:47
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['labelValue']='Data Reviewed By';
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['enforced']='false';
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['dependency']='';
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['required_formula']='';
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['readonly']='1';
$dictionary['M06_Error']['fields']['data_reviewed_by_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_error_type_c.php

 // created: 2022-10-04 05:48:48
$dictionary['M06_Error']['fields']['error_type_c']['labelValue']='Type';
$dictionary['M06_Error']['fields']['error_type_c']['dependency']='';
$dictionary['M06_Error']['fields']['error_type_c']['required_formula']='';
$dictionary['M06_Error']['fields']['error_type_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['error_type_c']['visibility_grid']=array (
  'trigger' => 'error_category_c',
  'values' => 
  array (
    'Choose One' => 
    array (
    ),
    'Critical Phase Inspection' => 
    array (
      0 => '',
      1 => 'Sttudy',
      2 => 'Facility',
    ),
    'Deceased Animal' => 
    array (
      0 => '',
      1 => 'Found Dead',
      2 => 'Early Term',
    ),
    'Maintenance Request' => 
    array (
    ),
    'Observation' => 
    array (
      0 => 'Sttudy',
      1 => 'Facility',
    ),
    'Rejected Animal' => 
    array (
      0 => '',
      1 => 'Not Sedated Study Article NOT Administered Surgical Access NOT Performed',
      2 => 'Study Article Administered',
      3 => 'Study Article NOT Administered Surgical Access NOT Performed',
      4 => 'Study Article NOT Administered Surgical Access Performed',
      5 => 'Unused_Medication Administered',
      6 => 'Unused_No Medication',
    ),
    'Vet Check' => 
    array (
      0 => '',
      1 => 'Initial Issue',
      2 => 'Repeat Issue',
      3 => 'Resolution Notification',
      4 => 'Observation at Transfer',
      5 => 'Clinical Pathology Review',
    ),
    '' => 
    array (
    ),
    'Daily QC' => 
    array (
      0 => '',
      1 => 'Sttudy',
      2 => 'Facility',
    ),
    'Data Book QC' => 
    array (
      0 => '',
      1 => 'Sttudy',
      2 => 'Facility',
    ),
    'Real time study conduct' => 
    array (
      0 => '',
      1 => 'Sttudy',
      2 => 'Facility',
      3 => 'Re Challenge',
    ),
    'Weekly Sweep' => 
    array (
      0 => '',
      1 => 'Sttudy',
      2 => 'Facility',
    ),
    'Feedback' => 
    array (
      0 => '',
      1 => 'Complaint',
      2 => 'Kudos',
    ),
    'Process Audit' => 
    array (
      0 => '',
      1 => 'OFI',
      2 => 'Minor',
      3 => 'Major',
    ),
    'External Audit' => 
    array (
      0 => '',
      1 => 'Sttudy',
      2 => 'Facility',
    ),
    'Gross Pathology' => 
    array (
      0 => '',
      1 => 'Deceased Animal',
      2 => 'Herd Health',
      3 => 'Necropsy Findings Notification',
    ),
    'Internal Feedback' => 
    array (
      0 => '',
      1 => 'Operations',
      2 => 'Analytical Operations',
      3 => 'Inlife Operations',
      4 => 'Laboratory Operations',
      5 => 'Large Animal Operations',
      6 => 'Operations Support',
      7 => 'Pathology Operations',
      8 => 'Re_identification',
      9 => 'Remaining Departments',
      10 => 'Scientific',
      11 => 'Small Animal Operations',
      12 => 'SPA Bug',
    ),
    'IACUC Deficiency' => 
    array (
      0 => '',
      1 => 'Facility',
      2 => 'Program',
    ),
    'Equipment Maintenance Request' => 
    array (
    ),
    'Retrospective Data QC' => 
    array (
    ),
    'Training' => 
    array (
      0 => '',
      1 => 'Trainee Initial Training',
      2 => 'Trainee Refresher Training',
      3 => 'Trainer Initial Training',
      4 => 'Trainer Refresher Training',
      5 => 'Trainee',
      6 => 'Trainer',
    ),
    'Test Failure' => 
    array (
      0 => '',
      1 => 'Expanded Study',
      2 => 'Failed Study',
      3 => 'Aborted Study',
      4 => 'CAB',
      5 => 'Invalid Study',
    ),
    'Work Product Schedule Outcome' => 
    array (
      0 => '',
      1 => 'Early Death',
      2 => 'Early Term Outcome',
      3 => 'Failed Sham',
      4 => 'Failed Pyrogen',
      5 => 'Found Deceased',
      6 => 'Not Used',
      7 => 'Not used shared BU',
      8 => 'Passed Sham',
      9 => 'Passed Pyrogen',
      10 => 'Performed Per Protocol',
      11 => 'Procedure Death',
      12 => 'Rejected',
      13 => 'Sedation for Animal Health',
      14 => 'Standard Biocomp TS Selection',
      15 => 'Unsuccessful Procedure and Survived',
    ),
    'Study Specific Charge' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_twentyfour_animal_quantity_c.php

 // created: 2022-10-04 05:54:48
$dictionary['M06_Error']['fields']['twentyfour_animal_quantity_c']['labelValue']='24hr: Quantity of animals that scored 1 or greater';
$dictionary['M06_Error']['fields']['twentyfour_animal_quantity_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['twentyfour_animal_quantity_c']['enforced']='';
$dictionary['M06_Error']['fields']['twentyfour_animal_quantity_c']['dependency']='and(equal($error_category_c,"Real time study conduct"),equal($error_type_c,"Re Challenge"))';
$dictionary['M06_Error']['fields']['twentyfour_animal_quantity_c']['required_formula']='and(equal($error_category_c,"Real time study conduct"),equal($error_type_c,"Re Challenge"))';
$dictionary['M06_Error']['fields']['twentyfour_animal_quantity_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_fortyeight_animal_quantity_c.php

 // created: 2022-10-04 05:58:16
$dictionary['M06_Error']['fields']['fortyeight_animal_quantity_c']['labelValue']='48hr: Quantity of animals that scored 1 or greater';
$dictionary['M06_Error']['fields']['fortyeight_animal_quantity_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M06_Error']['fields']['fortyeight_animal_quantity_c']['enforced']='';
$dictionary['M06_Error']['fields']['fortyeight_animal_quantity_c']['dependency']='and(equal($error_category_c,"Real time study conduct"),equal($error_type_c,"Re Challenge"))';
$dictionary['M06_Error']['fields']['fortyeight_animal_quantity_c']['required_formula']='and(equal($error_category_c,"Real time study conduct"),equal($error_type_c,"Re Challenge"))';
$dictionary['M06_Error']['fields']['fortyeight_animal_quantity_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/Vardefs/sugarfield_subtype_c.php

 // created: 2022-12-01 09:56:24
$dictionary['M06_Error']['fields']['subtype_c']['labelValue']='Subtype';
$dictionary['M06_Error']['fields']['subtype_c']['dependency']='';
$dictionary['M06_Error']['fields']['subtype_c']['required_formula']='';
$dictionary['M06_Error']['fields']['subtype_c']['readonly_formula']='';
$dictionary['M06_Error']['fields']['subtype_c']['visibility_grid']=array (
  'trigger' => 'error_category_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Critical Phase Inspection' => 
    array (
      0 => '',
      1 => 'ACT iSTAT error code',
      2 => 'ACT iSTAT starout',
      3 => 'ACT time interval missed',
      4 => 'ACT value out of range 1000 sec',
      5 => 'ACT value out of range high',
      6 => 'ACT value out of range low',
      7 => 'Additional activity performed',
      8 => 'Analytical Method Criteria Failure',
      9 => 'Analytical standards noncompliance',
      10 => 'Anesthesia Change Without Comment',
      11 => 'Anesthesia Incorrect Change',
      12 => 'Anesthesia Monitor Interval Missed',
      13 => 'Animal Escape',
      14 => 'Animal Housing Inappropriately Group Housed',
      15 => 'Animal Identification incorrect documentation',
      16 => 'Animal Identification incorrect identification',
      17 => 'Animal Inadequate Enrichment',
      18 => 'Animal Weight out of range',
      19 => 'Animal Weight suspect data',
      20 => 'Clinical Pathology Insufficient Volume',
      21 => 'Clinical Pathology missing analyte',
      22 => 'Clinical Pathology missing vet review',
      23 => 'Clinical Pathology sample quality issue',
      24 => 'Controlled Documents Incorrect Document Used',
      25 => 'Controlled Documents Not Archived',
      26 => 'Utilized noncurrent controlled doc',
      27 => 'Data Transfer not performed',
      28 => 'Disregard issue correctable',
      29 => 'Disregard unnecessary COM',
      30 => 'Documentation illegiblewriteover',
      31 => 'Documentation invalid entry',
      32 => 'Documentation missing data',
      33 => 'Documentation Error',
      34 => 'Documentation missing footnote',
      35 => 'Documentation missingincomplete verification',
      36 => 'Duplicate',
      37 => 'Equipment Malfunction',
      38 => 'Equipment OOS',
      39 => 'Equipment PMCal Overdue',
      40 => 'Equipment Tag Missing',
      41 => 'Chemical Reagent Expired',
      42 => 'Cleaning Supply Expired',
      43 => 'Drug Expired',
      44 => 'Lab Supply Expired',
      45 => 'GLP Study Article Expired',
      46 => 'Study Article Abnormality',
      47 => 'Extract Abnormality',
      48 => 'Fluid Administration',
      49 => 'Humidity OOR not OOS',
      50 => 'Chemical Reagent Label Error',
      51 => 'Cleaning Supply Label Error',
      52 => 'Drug Label Error',
      53 => 'Label Missing',
      54 => 'Specimen Label Error',
      55 => 'GLP Study Article Label Error',
      56 => 'MedicationDosing Wrong Dose',
      57 => 'MedicationDosing Wrong route',
      58 => 'Missed Exam scheduling',
      59 => 'Missed Task Acclimation',
      60 => 'Missed Task Bandaging',
      61 => 'Missed Task Cage Cleaning',
      62 => 'Missed Task Clinical Pathology',
      63 => 'Missed Deliverable Generation',
      64 => 'Missed Task Exam',
      65 => 'Missed Task Exercise',
      66 => 'Missed Task Food',
      67 => 'Missed Task Histology',
      68 => 'Missed Task Imaging',
      69 => 'Missed Task Microbiology',
      70 => 'Missed Task Necropsy',
      71 => 'Missed Task Observation',
      72 => 'Missed Task Prep',
      73 => 'Missed Task Preventive Health',
      74 => 'Missed Task Protocol Activity',
      75 => 'Missed Task QuarantineIsolation Release',
      76 => 'Missed Task Re Marking',
      77 => 'Missed Task Specimen Collection',
      78 => 'Missed Task Specimen Processing',
      79 => 'Missed Task Substance Administration',
      80 => 'Missed Task Urinary Management',
      81 => 'Missed Task Vet Order',
      82 => 'Missed Task Vitals Monitor',
      83 => 'Missed Task Water',
      84 => 'Missed Task Weight',
      85 => 'Missed Task Wound Care',
      86 => 'Not Applicable',
      87 => 'Photo Nonconformance Image Quality',
      88 => 'Photo Nonconformance Labeling Issue',
      89 => 'Procedure Time Interval Missed',
      90 => 'Report Compilation Error',
      91 => 'Sample Prep Error Analytical',
      92 => 'Sample Prep Error Extraction Duration',
      93 => 'Sample Prep Error Extraction Ratio',
      94 => 'Sample Prep Error Extraction Temperature',
      95 => 'Sample Prep Error Extract Manipulation',
      96 => 'Sample Prep Error Loss of Vehicle Glassware',
      97 => 'Sample Prep Error Loss of Vehicle WhirlPak',
      98 => 'Sample Prep Error Wrong components',
      99 => 'Sample Prep Error Wrong storage',
      100 => 'Sample Prep Error Wrong TACA concentration',
      101 => 'Sample Prep Error Wrong vehicle',
      102 => 'Standards Noncompliance',
      103 => 'Sterilization Failed Indicator',
      104 => 'Storage Conditions chemicalreagent',
      105 => 'Storage Conditions cleaning supply',
      106 => 'Storage Conditions specimen',
      107 => 'Storage Conditions study article',
      108 => 'Temp OOR not OOS',
      109 => 'Training task performed with inadequate training',
      110 => 'Training task performed without training',
      111 => 'Unsatisfactory Study Article Receipt',
      112 => 'Water testing missed retest',
    ),
    'Daily QC' => 
    array (
      0 => '',
      1 => 'ACT iSTAT error code',
      2 => 'ACT iSTAT starout',
      3 => 'ACT time interval missed',
      4 => 'ACT value out of range 1000 sec',
      5 => 'ACT value out of range high',
      6 => 'ACT value out of range low',
      7 => 'Additional activity performed',
      8 => 'Analytical Method Criteria Failure',
      9 => 'Analytical standards noncompliance',
      10 => 'Anesthesia Change Without Comment',
      11 => 'Anesthesia Incorrect Change',
      12 => 'Anesthesia Monitor Interval Missed',
      13 => 'Animal Escape',
      14 => 'Animal Housing Inappropriately Group Housed',
      15 => 'Animal Identification incorrect documentation',
      16 => 'Animal Identification incorrect identification',
      17 => 'Animal Inadequate Enrichment',
      18 => 'Animal Weight out of range',
      19 => 'Animal Weight suspect data',
      20 => 'Clinical Pathology Insufficient Volume',
      21 => 'Clinical Pathology missing analyte',
      22 => 'Clinical Pathology missing vet review',
      23 => 'Clinical Pathology sample quality issue',
      24 => 'Controlled Documents Incorrect Document Used',
      25 => 'Controlled Documents Not Archived',
      26 => 'Utilized noncurrent controlled doc',
      27 => 'Data Transfer not performed',
      28 => 'Disregard issue correctable',
      29 => 'Disregard unnecessary COM',
      30 => 'Documentation illegiblewriteover',
      31 => 'Documentation invalid entry',
      32 => 'Documentation missing data',
      33 => 'Documentation Error',
      34 => 'Documentation missing footnote',
      35 => 'Documentation missingincomplete verification',
      36 => 'Duplicate',
      37 => 'Equipment Malfunction',
      38 => 'Equipment OOS',
      39 => 'Equipment PMCal Overdue',
      40 => 'Equipment Tag Missing',
      41 => 'Chemical Reagent Expired',
      42 => 'Cleaning Supply Expired',
      43 => 'Drug Expired',
      44 => 'Lab Supply Expired',
      45 => 'GLP Study Article Expired',
      46 => 'Study Article Abnormality',
      47 => 'Extract Abnormality',
      48 => 'Fluid Administration',
      49 => 'Humidity OOR not OOS',
      50 => 'Chemical Reagent Label Error',
      51 => 'Cleaning Supply Label Error',
      52 => 'Drug Label Error',
      53 => 'Label Missing',
      54 => 'Specimen Label Error',
      55 => 'GLP Study Article Label Error',
      56 => 'MedicationDosing Wrong Dose',
      57 => 'MedicationDosing Wrong route',
      58 => 'Missed Exam scheduling',
      59 => 'Missed Task Acclimation',
      60 => 'Missed Task Bandaging',
      61 => 'Missed Task Cage Cleaning',
      62 => 'Missed Task Clinical Pathology',
      63 => 'Missed Deliverable Generation',
      64 => 'Missed Task Exam',
      65 => 'Missed Task Exercise',
      66 => 'Missed Task Food',
      67 => 'Missed Task Histology',
      68 => 'Missed Task Imaging',
      69 => 'Missed Task Microbiology',
      70 => 'Missed Task Necropsy',
      71 => 'Missed Task Observation',
      72 => 'Missed Task Prep',
      73 => 'Missed Task Preventive Health',
      74 => 'Missed Task Protocol Activity',
      75 => 'Missed Task QuarantineIsolation Release',
      76 => 'Missed Task Re Marking',
      77 => 'Missed Task Specimen Collection',
      78 => 'Missed Task Specimen Processing',
      79 => 'Missed Task Substance Administration',
      80 => 'Missed Task Urinary Management',
      81 => 'Missed Task Vet Order',
      82 => 'Missed Task Vitals Monitor',
      83 => 'Missed Task Water',
      84 => 'Missed Task Weight',
      85 => 'Missed Task Wound Care',
      86 => 'Not Applicable',
      87 => 'Photo Nonconformance Image Quality',
      88 => 'Photo Nonconformance Labeling Issue',
      89 => 'Procedure Time Interval Missed',
      90 => 'Report Compilation Error',
      91 => 'Sample Prep Error Analytical',
      92 => 'Sample Prep Error Extraction Duration',
      93 => 'Sample Prep Error Extraction Ratio',
      94 => 'Sample Prep Error Extraction Temperature',
      95 => 'Sample Prep Error Extract Manipulation',
      96 => 'Sample Prep Error Loss of Vehicle Glassware',
      97 => 'Sample Prep Error Loss of Vehicle WhirlPak',
      98 => 'Sample Prep Error Wrong components',
      99 => 'Sample Prep Error Wrong storage',
      100 => 'Sample Prep Error Wrong TACA concentration',
      101 => 'Sample Prep Error Wrong vehicle',
      102 => 'Standards Noncompliance',
      103 => 'Sterilization Failed Indicator',
      104 => 'Storage Conditions chemicalreagent',
      105 => 'Storage Conditions cleaning supply',
      106 => 'Storage Conditions specimen',
      107 => 'Storage Conditions study article',
      108 => 'Temp OOR not OOS',
      109 => 'Training task performed with inadequate training',
      110 => 'Training task performed without training',
      111 => 'Unsatisfactory Study Article Receipt',
      112 => 'Water testing missed retest',
    ),
    'Data Book QC' => 
    array (
      0 => '',
      1 => 'ACT iSTAT error code',
      2 => 'ACT iSTAT starout',
      3 => 'ACT time interval missed',
      4 => 'ACT value out of range 1000 sec',
      5 => 'ACT value out of range high',
      6 => 'ACT value out of range low',
      7 => 'Additional activity performed',
      8 => 'Analytical Method Criteria Failure',
      9 => 'Analytical standards noncompliance',
      10 => 'Anesthesia Change Without Comment',
      11 => 'Anesthesia Incorrect Change',
      12 => 'Anesthesia Monitor Interval Missed',
      13 => 'Animal Escape',
      14 => 'Animal Housing Inappropriately Group Housed',
      15 => 'Animal Identification incorrect documentation',
      16 => 'Animal Identification incorrect identification',
      17 => 'Animal Inadequate Enrichment',
      18 => 'Animal Weight out of range',
      19 => 'Animal Weight suspect data',
      20 => 'Clinical Pathology Insufficient Volume',
      21 => 'Clinical Pathology missing analyte',
      22 => 'Clinical Pathology missing vet review',
      23 => 'Clinical Pathology sample quality issue',
      24 => 'Controlled Documents Incorrect Document Used',
      25 => 'Controlled Documents Not Archived',
      26 => 'Utilized noncurrent controlled doc',
      27 => 'Data Transfer not performed',
      28 => 'Disregard issue correctable',
      29 => 'Disregard unnecessary COM',
      30 => 'Documentation illegiblewriteover',
      31 => 'Documentation invalid entry',
      32 => 'Documentation missing data',
      33 => 'Documentation Error',
      34 => 'Documentation missing footnote',
      35 => 'Documentation missingincomplete verification',
      36 => 'Duplicate',
      37 => 'Equipment Malfunction',
      38 => 'Equipment OOS',
      39 => 'Equipment PMCal Overdue',
      40 => 'Equipment Tag Missing',
      41 => 'Chemical Reagent Expired',
      42 => 'Cleaning Supply Expired',
      43 => 'Drug Expired',
      44 => 'Lab Supply Expired',
      45 => 'GLP Study Article Expired',
      46 => 'Study Article Abnormality',
      47 => 'Extract Abnormality',
      48 => 'Fluid Administration',
      49 => 'Humidity OOR not OOS',
      50 => 'Chemical Reagent Label Error',
      51 => 'Cleaning Supply Label Error',
      52 => 'Drug Label Error',
      53 => 'Label Missing',
      54 => 'Specimen Label Error',
      55 => 'GLP Study Article Label Error',
      56 => 'MedicationDosing Wrong Dose',
      57 => 'MedicationDosing Wrong route',
      58 => 'Missed Exam scheduling',
      59 => 'Missed Task Acclimation',
      60 => 'Missed Task Bandaging',
      61 => 'Missed Task Cage Cleaning',
      62 => 'Missed Task Clinical Pathology',
      63 => 'Missed Deliverable Generation',
      64 => 'Missed Task Exam',
      65 => 'Missed Task Exercise',
      66 => 'Missed Task Food',
      67 => 'Missed Task Histology',
      68 => 'Missed Task Imaging',
      69 => 'Missed Task Microbiology',
      70 => 'Missed Task Necropsy',
      71 => 'Missed Task Observation',
      72 => 'Missed Task Prep',
      73 => 'Missed Task Preventive Health',
      74 => 'Missed Task Protocol Activity',
      75 => 'Missed Task QuarantineIsolation Release',
      76 => 'Missed Task Re Marking',
      77 => 'Missed Task Specimen Collection',
      78 => 'Missed Task Specimen Processing',
      79 => 'Missed Task Substance Administration',
      80 => 'Missed Task Urinary Management',
      81 => 'Missed Task Vet Order',
      82 => 'Missed Task Vitals Monitor',
      83 => 'Missed Task Water',
      84 => 'Missed Task Weight',
      85 => 'Missed Task Wound Care',
      86 => 'Not Applicable',
      87 => 'Photo Nonconformance Image Quality',
      88 => 'Photo Nonconformance Labeling Issue',
      89 => 'Procedure Time Interval Missed',
      90 => 'Report Compilation Error',
      91 => 'Sample Prep Error Analytical',
      92 => 'Sample Prep Error Extraction Duration',
      93 => 'Sample Prep Error Extraction Ratio',
      94 => 'Sample Prep Error Extraction Temperature',
      95 => 'Sample Prep Error Extract Manipulation',
      96 => 'Sample Prep Error Loss of Vehicle Glassware',
      97 => 'Sample Prep Error Loss of Vehicle WhirlPak',
      98 => 'Sample Prep Error Wrong components',
      99 => 'Sample Prep Error Wrong storage',
      100 => 'Sample Prep Error Wrong TACA concentration',
      101 => 'Sample Prep Error Wrong vehicle',
      102 => 'Standards Noncompliance',
      103 => 'Sterilization Failed Indicator',
      104 => 'Storage Conditions chemicalreagent',
      105 => 'Storage Conditions cleaning supply',
      106 => 'Storage Conditions specimen',
      107 => 'Storage Conditions study article',
      108 => 'Temp OOR not OOS',
      109 => 'Training task performed with inadequate training',
      110 => 'Training task performed without training',
      111 => 'Unsatisfactory Study Article Receipt',
      112 => 'Water testing missed retest',
    ),
    'Deceased Animal' => 
    array (
    ),
    'Equipment Maintenance Request' => 
    array (
    ),
    'External Audit' => 
    array (
      0 => '',
      1 => 'ACT iSTAT error code',
      2 => 'ACT iSTAT starout',
      3 => 'ACT time interval missed',
      4 => 'ACT value out of range 1000 sec',
      5 => 'ACT value out of range high',
      6 => 'ACT value out of range low',
      7 => 'Additional activity performed',
      8 => 'Analytical Method Criteria Failure',
      9 => 'Analytical standards noncompliance',
      10 => 'Anesthesia Change Without Comment',
      11 => 'Anesthesia Incorrect Change',
      12 => 'Anesthesia Monitor Interval Missed',
      13 => 'Animal Escape',
      14 => 'Animal Housing Inappropriately Group Housed',
      15 => 'Animal Identification incorrect documentation',
      16 => 'Animal Identification incorrect identification',
      17 => 'Animal Inadequate Enrichment',
      18 => 'Animal Weight out of range',
      19 => 'Animal Weight suspect data',
      20 => 'Clinical Pathology Insufficient Volume',
      21 => 'Clinical Pathology missing analyte',
      22 => 'Clinical Pathology missing vet review',
      23 => 'Clinical Pathology sample quality issue',
      24 => 'Controlled Documents Incorrect Document Used',
      25 => 'Controlled Documents Not Archived',
      26 => 'Utilized noncurrent controlled doc',
      27 => 'Data Transfer not performed',
      28 => 'Disregard issue correctable',
      29 => 'Disregard unnecessary COM',
      30 => 'Documentation illegiblewriteover',
      31 => 'Documentation invalid entry',
      32 => 'Documentation missing data',
      33 => 'Documentation Error',
      34 => 'Documentation missing footnote',
      35 => 'Documentation missingincomplete verification',
      36 => 'Duplicate',
      37 => 'Equipment Malfunction',
      38 => 'Equipment OOS',
      39 => 'Equipment PMCal Overdue',
      40 => 'Equipment Tag Missing',
      41 => 'Chemical Reagent Expired',
      42 => 'Cleaning Supply Expired',
      43 => 'Drug Expired',
      44 => 'Lab Supply Expired',
      45 => 'GLP Study Article Expired',
      46 => 'Study Article Abnormality',
      47 => 'Extract Abnormality',
      48 => 'Fluid Administration',
      49 => 'Humidity OOR not OOS',
      50 => 'Chemical Reagent Label Error',
      51 => 'Cleaning Supply Label Error',
      52 => 'Drug Label Error',
      53 => 'Label Missing',
      54 => 'Specimen Label Error',
      55 => 'GLP Study Article Label Error',
      56 => 'MedicationDosing Wrong Dose',
      57 => 'MedicationDosing Wrong route',
      58 => 'Missed Exam scheduling',
      59 => 'Missed Task Acclimation',
      60 => 'Missed Task Bandaging',
      61 => 'Missed Task Cage Cleaning',
      62 => 'Missed Deliverable Generation',
      63 => 'Missed Task Exam',
      64 => 'Missed Task Exercise',
      65 => 'Missed Task Food',
      66 => 'Missed Task Histology',
      67 => 'Missed Task Imaging',
      68 => 'Missed Task Microbiology',
      69 => 'Missed Task Necropsy',
      70 => 'Missed Task Observation',
      71 => 'Missed Task Prep',
      72 => 'Missed Task Preventive Health',
      73 => 'Missed Task Protocol Activity',
      74 => 'Missed Task QuarantineIsolation Release',
      75 => 'Missed Task Specimen Collection',
      76 => 'Missed Task Specimen Processing',
      77 => 'Missed Task Substance Administration',
      78 => 'Missed Task Urinary Management',
      79 => 'Missed Task Vet Order',
      80 => 'Missed Task Vitals Monitor',
      81 => 'Missed Task Water',
      82 => 'Missed Task Weight',
      83 => 'Missed Task Wound Care',
      84 => 'Not Applicable',
      85 => 'Photo Nonconformance Image Quality',
      86 => 'Photo Nonconformance Labeling Issue',
      87 => 'Procedure Time Interval Missed',
      88 => 'Report Compilation Error',
      89 => 'Sample Prep Error Analytical',
      90 => 'Sample Prep Error Extraction Duration',
      91 => 'Sample Prep Error Extraction Ratio',
      92 => 'Sample Prep Error Extraction Temperature',
      93 => 'Sample Prep Error Extract Manipulation',
      94 => 'Sample Prep Error Loss of Vehicle Glassware',
      95 => 'Sample Prep Error Loss of Vehicle WhirlPak',
      96 => 'Sample Prep Error Wrong components',
      97 => 'Sample Prep Error Wrong storage',
      98 => 'Sample Prep Error Wrong TACA concentration',
      99 => 'Sample Prep Error Wrong vehicle',
      100 => 'Standards Noncompliance',
      101 => 'Sterilization Failed Indicator',
      102 => 'Storage Conditions chemicalreagent',
      103 => 'Storage Conditions cleaning supply',
      104 => 'Storage Conditions specimen',
      105 => 'Storage Conditions study article',
      106 => 'Temp OOR not OOS',
      107 => 'Training task performed with inadequate training',
      108 => 'Training task performed without training',
      109 => 'Unsatisfactory Study Article Receipt',
      110 => 'Water testing missed retest',
    ),
    'Feedback' => 
    array (
    ),
    'Maintenance Request' => 
    array (
    ),
    'Gross Pathology' => 
    array (
    ),
    'IACUC Deficiency' => 
    array (
    ),
    'Internal Feedback' => 
    array (
      0 => '',
      1 => 'Animal Escape',
      2 => 'Animal Housing Inappropriately Group Housed',
      3 => 'Animal Identification incorrect identification',
      4 => 'Animal Inadequate Enrichment',
      5 => 'Animal Incorrect Assignment',
      6 => 'Animal Re identification',
      7 => 'Animal Room Incorrect',
      8 => 'Animal Procurement Incorrect',
      9 => 'Catheter Concerns',
      10 => 'Disregard unnecessary COM',
      11 => 'FastTrack Glitch',
      12 => 'FastTrack Incorrect',
      13 => 'Incomplete or missed prep task',
      14 => 'Incorrect labels',
      15 => 'Intubation Concerns',
      16 => 'Label Missing',
      17 => 'Missed Task Re Marking',
      18 => 'Missed task scheduling',
      19 => 'Missing forms',
      20 => 'Not Applicable',
      21 => 'Post op Form incorrect',
      22 => 'Pre op Form incorrect',
      23 => 'Sample Prep Error Loss of Vehicle Glassware',
      24 => 'Sample Prep Error Loss of Vehicle WhirlPak',
      25 => 'Sample Prep Near Miss Prep',
      26 => 'Sample Prep Near Miss Extraction',
      27 => 'Scheduling Matrix not updated',
      28 => 'SPA Bug',
      29 => 'Specimen Collection form incorrect',
      30 => 'Startups worksheet incorrect',
      31 => 'Sterilization Failed Indicator',
      32 => 'Surgical Site Concerns',
      33 => 'Task Incorrect Scheduling',
      34 => 'Water testing missed retest',
      35 => 'WPO Not Submitted',
    ),
    'Process Audit' => 
    array (
      0 => '',
      1 => 'ACT iSTAT error code',
      2 => 'ACT iSTAT starout',
      3 => 'ACT time interval missed',
      4 => 'ACT value out of range 1000 sec',
      5 => 'ACT value out of range high',
      6 => 'ACT value out of range low',
      7 => 'Additional activity performed',
      8 => 'Animal Procurement Incorrect',
      9 => 'Analytical Method Criteria Failure',
      10 => 'Analytical standards noncompliance',
      11 => 'Anesthesia Change Without Comment',
      12 => 'Anesthesia Incorrect Change',
      13 => 'Anesthesia Monitor Interval Missed',
      14 => 'Animal Escape',
      15 => 'Animal Housing Inappropriately Group Housed',
      16 => 'Animal Identification incorrect documentation',
      17 => 'Animal Identification incorrect identification',
      18 => 'Animal Inadequate Enrichment',
      19 => 'Animal Weight out of range',
      20 => 'Animal Weight suspect data',
      21 => 'Clinical Pathology Insufficient Volume',
      22 => 'Clinical Pathology missing analyte',
      23 => 'Clinical Pathology missing vet review',
      24 => 'Clinical Pathology sample quality issue',
      25 => 'Controlled Documents Incorrect Document Used',
      26 => 'Controlled Documents Not Archived',
      27 => 'Utilized noncurrent controlled doc',
      28 => 'Data Transfer not performed',
      29 => 'Disregard issue correctable',
      30 => 'Disregard unnecessary COM',
      31 => 'Documentation illegiblewriteover',
      32 => 'Documentation invalid entry',
      33 => 'Documentation missing data',
      34 => 'Documentation Error',
      35 => 'Documentation missing footnote',
      36 => 'Documentation missingincomplete verification',
      37 => 'Duplicate',
      38 => 'Equipment Malfunction',
      39 => 'Equipment OOS',
      40 => 'Equipment PMCal Overdue',
      41 => 'Equipment Tag Missing',
      42 => 'Chemical Reagent Expired',
      43 => 'Cleaning Supply Expired',
      44 => 'Drug Expired',
      45 => 'Lab Supply Expired',
      46 => 'GLP Study Article Expired',
      47 => 'Study Article Abnormality',
      48 => 'Extract Abnormality',
      49 => 'Fluid Administration',
      50 => 'Humidity OOR not OOS',
      51 => 'Chemical Reagent Label Error',
      52 => 'Cleaning Supply Label Error',
      53 => 'Drug Label Error',
      54 => 'Label Missing',
      55 => 'Specimen Label Error',
      56 => 'GLP Study Article Label Error',
      57 => 'MedicationDosing Wrong Dose',
      58 => 'MedicationDosing Wrong route',
      59 => 'Missed Exam scheduling',
      60 => 'Missed Task Acclimation',
      61 => 'Missed Task Bandaging',
      62 => 'Missed Task Cage Cleaning',
      63 => 'Missed Task Clinical Pathology',
      64 => 'Missed Deliverable Generation',
      65 => 'Missed Task Exam',
      66 => 'Missed Task Exercise',
      67 => 'Missed Task Food',
      68 => 'Missed Task Histology',
      69 => 'Missed Task Imaging',
      70 => 'Missed Task Microbiology',
      71 => 'Missed Task Necropsy',
      72 => 'Missed Task Observation',
      73 => 'Missed Task Prep',
      74 => 'Missed Task Preventive Health',
      75 => 'Missed Task Protocol Activity',
      76 => 'Missed Task QuarantineIsolation Release',
      77 => 'Missed Task Re Marking',
      78 => 'Missed Task Specimen Collection',
      79 => 'Missed Task Specimen Processing',
      80 => 'Missed Task Substance Administration',
      81 => 'Missed Task Urinary Management',
      82 => 'Missed Task Vet Order',
      83 => 'Missed Task Vitals Monitor',
      84 => 'Missed Task Water',
      85 => 'Missed Task Weight',
      86 => 'Missed Task Wound Care',
      87 => 'Not Applicable',
      88 => 'Photo Nonconformance Image Quality',
      89 => 'Photo Nonconformance Labeling Issue',
      90 => 'Procedure Time Interval Missed',
      91 => 'Report Compilation Error',
      92 => 'Sample Prep Error Analytical',
      93 => 'Sample Prep Error Extraction Duration',
      94 => 'Sample Prep Error Extraction Ratio',
      95 => 'Sample Prep Error Extraction Temperature',
      96 => 'Sample Prep Error Extract Manipulation',
      97 => 'Sample Prep Error Loss of Vehicle Glassware',
      98 => 'Sample Prep Error Loss of Vehicle WhirlPak',
      99 => 'Sample Prep Error Wrong components',
      100 => 'Sample Prep Error Wrong storage',
      101 => 'Sample Prep Error Wrong TACA concentration',
      102 => 'Sample Prep Error Wrong vehicle',
      103 => 'Standards Noncompliance',
      104 => 'Sterilization Failed Indicator',
      105 => 'Storage Conditions chemicalreagent',
      106 => 'Storage Conditions cleaning supply',
      107 => 'Storage Conditions specimen',
      108 => 'Storage Conditions study article',
      109 => 'Temp OOR not OOS',
      110 => 'Training task performed with inadequate training',
      111 => 'Training task performed without training',
      112 => 'Unsatisfactory Study Article Receipt',
      113 => 'Water testing missed retest',
    ),
    'Real time study conduct' => 
    array (
      0 => '',
      1 => 'ACT iSTAT error code',
      2 => 'ACT iSTAT starout',
      3 => 'ACT time interval missed',
      4 => 'ACT value out of range 1000 sec',
      5 => 'ACT value out of range high',
      6 => 'ACT value out of range low',
      7 => 'Additional activity performed',
      8 => 'Analytical Method Criteria Failure',
      9 => 'Analytical standards noncompliance',
      10 => 'Anesthesia Change Without Comment',
      11 => 'Anesthesia Incorrect Change',
      12 => 'Anesthesia Monitor Interval Missed',
      13 => 'Animal Escape',
      14 => 'Animal Housing Inappropriately Group Housed',
      15 => 'Animal Identification incorrect documentation',
      16 => 'Animal Identification incorrect identification',
      17 => 'Animal Inadequate Enrichment',
      18 => 'Animal Weight out of range',
      19 => 'Animal Weight suspect data',
      20 => 'Clinical Pathology Insufficient Volume',
      21 => 'Clinical Pathology missing analyte',
      22 => 'Clinical Pathology missing vet review',
      23 => 'Clinical Pathology sample quality issue',
      24 => 'Controlled Documents Incorrect Document Used',
      25 => 'Controlled Documents Not Archived',
      26 => 'Utilized noncurrent controlled doc',
      27 => 'Data Transfer not performed',
      28 => 'Disregard issue correctable',
      29 => 'Disregard unnecessary COM',
      30 => 'Documentation illegiblewriteover',
      31 => 'Documentation invalid entry',
      32 => 'Documentation missing data',
      33 => 'Documentation Error',
      34 => 'Documentation missing footnote',
      35 => 'Documentation missingincomplete verification',
      36 => 'Duplicate',
      37 => 'Equipment Malfunction',
      38 => 'Equipment OOS',
      39 => 'Equipment PMCal Overdue',
      40 => 'Equipment Tag Missing',
      41 => 'Chemical Reagent Expired',
      42 => 'Cleaning Supply Expired',
      43 => 'Drug Expired',
      44 => 'Lab Supply Expired',
      45 => 'GLP Study Article Expired',
      46 => 'Study Article Abnormality',
      47 => 'Extract Abnormality',
      48 => 'Fluid Administration',
      49 => 'Humidity OOR not OOS',
      50 => 'Chemical Reagent Label Error',
      51 => 'Cleaning Supply Label Error',
      52 => 'Drug Label Error',
      53 => 'Label Missing',
      54 => 'Specimen Label Error',
      55 => 'GLP Study Article Label Error',
      56 => 'MedicationDosing Wrong Dose',
      57 => 'MedicationDosing Wrong route',
      58 => 'Missed Exam scheduling',
      59 => 'Missed Task Acclimation',
      60 => 'Missed Task Bandaging',
      61 => 'Missed Task Cage Cleaning',
      62 => 'Missed Task Clinical Pathology',
      63 => 'Missed Deliverable Generation',
      64 => 'Missed Task Exam',
      65 => 'Missed Task Exercise',
      66 => 'Missed Task Food',
      67 => 'Missed Task Histology',
      68 => 'Missed Task Imaging',
      69 => 'Missed Task Microbiology',
      70 => 'Missed Task Necropsy',
      71 => 'Missed Task Observation',
      72 => 'Missed Task Prep',
      73 => 'Missed Task Preventive Health',
      74 => 'Missed Task Protocol Activity',
      75 => 'Missed Task QuarantineIsolation Release',
      76 => 'Missed Task Re Marking',
      77 => 'Missed Task Specimen Collection',
      78 => 'Missed Task Specimen Processing',
      79 => 'Missed Task Substance Administration',
      80 => 'Missed Task Urinary Management',
      81 => 'Missed Task Vet Order',
      82 => 'Missed Task Vitals Monitor',
      83 => 'Missed Task Water',
      84 => 'Missed Task Weight',
      85 => 'Missed Task Wound Care',
      86 => 'Not Applicable',
      87 => 'Photo Nonconformance Image Quality',
      88 => 'Photo Nonconformance Labeling Issue',
      89 => 'Procedure Time Interval Missed',
      90 => 'Report Compilation Error',
      91 => 'Sample Prep Error Analytical',
      92 => 'Sample Prep Error Extraction Duration',
      93 => 'Sample Prep Error Extraction Ratio',
      94 => 'Sample Prep Error Extraction Temperature',
      95 => 'Sample Prep Error Extract Manipulation',
      96 => 'Sample Prep Error Loss of Vehicle Glassware',
      97 => 'Sample Prep Error Loss of Vehicle WhirlPak',
      98 => 'Sample Prep Error Study article became unsuitable for use',
      99 => 'Sample Prep Error Wrong components',
      100 => 'Sample Prep Error Wrong storage',
      101 => 'Sample Prep Error Wrong TACA concentration',
      102 => 'Sample Prep Error Wrong vehicle',
      103 => 'Standards Noncompliance',
      104 => 'Sterilization Failed Indicator',
      105 => 'Storage Conditions chemicalreagent',
      106 => 'Storage Conditions cleaning supply',
      107 => 'Storage Conditions specimen',
      108 => 'Storage Conditions study article',
      109 => 'Temp OOR not OOS',
      110 => 'Training task performed with inadequate training',
      111 => 'Training task performed without training',
      112 => 'Unsatisfactory Study Article Receipt',
      113 => 'Water testing missed retest',
    ),
    'Rejected Animal' => 
    array (
    ),
    'Retrospective Data QC' => 
    array (
      0 => '',
      1 => 'ACT iSTAT error code',
      2 => 'ACT iSTAT starout',
      3 => 'ACT time interval missed',
      4 => 'ACT value out of range 1000 sec',
      5 => 'ACT value out of range high',
      6 => 'ACT value out of range low',
      7 => 'Additional activity performed',
      8 => 'Analytical Method Criteria Failure',
      9 => 'Analytical standards noncompliance',
      10 => 'Anesthesia Change Without Comment',
      11 => 'Anesthesia Incorrect Change',
      12 => 'Anesthesia Monitor Interval Missed',
      13 => 'Animal Escape',
      14 => 'Animal Housing Inappropriately Group Housed',
      15 => 'Animal Identification incorrect documentation',
      16 => 'Animal Identification incorrect identification',
      17 => 'Animal Inadequate Enrichment',
      18 => 'Animal Weight out of range',
      19 => 'Animal Weight suspect data',
      20 => 'Clinical Pathology Insufficient Volume',
      21 => 'Clinical Pathology missing analyte',
      22 => 'Clinical Pathology missing vet review',
      23 => 'Clinical Pathology sample quality issue',
      24 => 'Controlled Documents Incorrect Document Used',
      25 => 'Controlled Documents Not Archived',
      26 => 'Utilized noncurrent controlled doc',
      27 => 'Data Transfer not performed',
      28 => 'Disregard issue correctable',
      29 => 'Disregard unnecessary COM',
      30 => 'Documentation illegiblewriteover',
      31 => 'Documentation invalid entry',
      32 => 'Documentation missing data',
      33 => 'Documentation Error',
      34 => 'Documentation missing footnote',
      35 => 'Documentation missingincomplete verification',
      36 => 'Duplicate',
      37 => 'Equipment Malfunction',
      38 => 'Equipment OOS',
      39 => 'Equipment PMCal Overdue',
      40 => 'Equipment Tag Missing',
      41 => 'Chemical Reagent Expired',
      42 => 'Cleaning Supply Expired',
      43 => 'Drug Expired',
      44 => 'Lab Supply Expired',
      45 => 'GLP Study Article Expired',
      46 => 'Study Article Abnormality',
      47 => 'Extract Abnormality',
      48 => 'Fluid Administration',
      49 => 'Humidity OOR not OOS',
      50 => 'Chemical Reagent Label Error',
      51 => 'Cleaning Supply Label Error',
      52 => 'Drug Label Error',
      53 => 'Label Missing',
      54 => 'Specimen Label Error',
      55 => 'GLP Study Article Label Error',
      56 => 'MedicationDosing Wrong Dose',
      57 => 'MedicationDosing Wrong route',
      58 => 'Missed Exam scheduling',
      59 => 'Missed Task Acclimation',
      60 => 'Missed Task Bandaging',
      61 => 'Missed Task Cage Cleaning',
      62 => 'Missed Task Clinical Pathology',
      63 => 'Missed Deliverable Generation',
      64 => 'Missed Task Exam',
      65 => 'Missed Task Exercise',
      66 => 'Missed Task Food',
      67 => 'Missed Task Histology',
      68 => 'Missed Task Imaging',
      69 => 'Missed Task Microbiology',
      70 => 'Missed Task Necropsy',
      71 => 'Missed Task Observation',
      72 => 'Missed Task Prep',
      73 => 'Missed Task Preventive Health',
      74 => 'Missed Task Protocol Activity',
      75 => 'Missed Task QuarantineIsolation Release',
      76 => 'Missed Task Re Marking',
      77 => 'Missed Task Specimen Collection',
      78 => 'Missed Task Specimen Processing',
      79 => 'Missed Task Substance Administration',
      80 => 'Missed Task Urinary Management',
      81 => 'Missed Task Vet Order',
      82 => 'Missed Task Vitals Monitor',
      83 => 'Missed Task Water',
      84 => 'Missed Task Weight',
      85 => 'Missed Task Wound Care',
      86 => 'Not Applicable',
      87 => 'Photo Nonconformance Image Quality',
      88 => 'Photo Nonconformance Labeling Issue',
      89 => 'Procedure Time Interval Missed',
      90 => 'Report Compilation Error',
      91 => 'Sample Prep Error Analytical',
      92 => 'Sample Prep Error Extraction Duration',
      93 => 'Sample Prep Error Extraction Ratio',
      94 => 'Sample Prep Error Extraction Temperature',
      95 => 'Sample Prep Error Extract Manipulation',
      96 => 'Sample Prep Error Loss of Vehicle Glassware',
      97 => 'Sample Prep Error Loss of Vehicle WhirlPak',
      98 => 'Sample Prep Error Wrong components',
      99 => 'Sample Prep Error Wrong storage',
      100 => 'Sample Prep Error Wrong TACA concentration',
      101 => 'Sample Prep Error Wrong vehicle',
      102 => 'Standards Noncompliance',
      103 => 'Sterilization Failed Indicator',
      104 => 'Storage Conditions chemicalreagent',
      105 => 'Storage Conditions cleaning supply',
      106 => 'Storage Conditions specimen',
      107 => 'Storage Conditions study article',
      108 => 'Temp OOR not OOS',
      109 => 'Training task performed with inadequate training',
      110 => 'Training task performed without training',
      111 => 'Unsatisfactory Study Article Receipt',
      112 => 'Water testing missed retest',
    ),
    'Test Failure' => 
    array (
      0 => '',
      1 => 'ACT iSTAT error code',
      2 => 'ACT iSTAT starout',
      3 => 'ACT time interval missed',
      4 => 'ACT value out of range 1000 sec',
      5 => 'ACT value out of range high',
      6 => 'ACT value out of range low',
      7 => 'Additional activity performed',
      8 => 'Analytical Method Criteria Failure',
      9 => 'Analytical standards noncompliance',
      10 => 'Anesthesia Change Without Comment',
      11 => 'Anesthesia Incorrect Change',
      12 => 'Anesthesia Monitor Interval Missed',
      13 => 'Animal Escape',
      14 => 'Animal Housing Inappropriately Group Housed',
      15 => 'Animal Identification incorrect documentation',
      16 => 'Animal Identification incorrect identification',
      17 => 'Animal Inadequate Enrichment',
      18 => 'Animal Weight out of range',
      19 => 'Animal Weight suspect data',
      20 => 'Clinical Pathology Insufficient Volume',
      21 => 'Clinical Pathology missing analyte',
      22 => 'Clinical Pathology missing vet review',
      23 => 'Clinical Pathology sample quality issue',
      24 => 'Controlled Documents Incorrect Document Used',
      25 => 'Controlled Documents Not Archived',
      26 => 'Utilized noncurrent controlled doc',
      27 => 'Data Transfer not performed',
      28 => 'Disregard issue correctable',
      29 => 'Disregard unnecessary COM',
      30 => 'Documentation illegiblewriteover',
      31 => 'Documentation invalid entry',
      32 => 'Documentation missing data',
      33 => 'Documentation Error',
      34 => 'Documentation missing footnote',
      35 => 'Documentation missingincomplete verification',
      36 => 'Duplicate',
      37 => 'Equipment Malfunction',
      38 => 'Equipment OOS',
      39 => 'Equipment PMCal Overdue',
      40 => 'Equipment Tag Missing',
      41 => 'Chemical Reagent Expired',
      42 => 'Cleaning Supply Expired',
      43 => 'Drug Expired',
      44 => 'Lab Supply Expired',
      45 => 'GLP Study Article Expired',
      46 => 'Study Article Abnormality',
      47 => 'Extract Abnormality',
      48 => 'Fluid Administration',
      49 => 'Humidity OOR not OOS',
      50 => 'Chemical Reagent Label Error',
      51 => 'Cleaning Supply Label Error',
      52 => 'Drug Label Error',
      53 => 'Label Missing',
      54 => 'Specimen Label Error',
      55 => 'GLP Study Article Label Error',
      56 => 'MedicationDosing Wrong Dose',
      57 => 'MedicationDosing Wrong route',
      58 => 'Missed Exam scheduling',
      59 => 'Missed Task Acclimation',
      60 => 'Missed Task Bandaging',
      61 => 'Missed Task Cage Cleaning',
      62 => 'Missed Task Clinical Pathology',
      63 => 'Missed Deliverable Generation',
      64 => 'Missed Task Exam',
      65 => 'Missed Task Exercise',
      66 => 'Missed Task Food',
      67 => 'Missed Task Histology',
      68 => 'Missed Task Imaging',
      69 => 'Missed Task Microbiology',
      70 => 'Missed Task Necropsy',
      71 => 'Missed Task Observation',
      72 => 'Missed Task Prep',
      73 => 'Missed Task Preventive Health',
      74 => 'Missed Task Protocol Activity',
      75 => 'Missed Task QuarantineIsolation Release',
      76 => 'Missed Task Specimen Collection',
      77 => 'Missed Task Specimen Processing',
      78 => 'Missed Task Substance Administration',
      79 => 'Missed Task Urinary Management',
      80 => 'Missed Task Vet Order',
      81 => 'Missed Task Vitals Monitor',
      82 => 'Missed Task Water',
      83 => 'Missed Task Weight',
      84 => 'Missed Task Wound Care',
      85 => 'Not Applicable',
      86 => 'Photo Nonconformance Image Quality',
      87 => 'Photo Nonconformance Labeling Issue',
      88 => 'Procedure Time Interval Missed',
      89 => 'Report Compilation Error',
      90 => 'Sample Prep Error Analytical',
      91 => 'Sample Prep Error Extraction Duration',
      92 => 'Sample Prep Error Extraction Ratio',
      93 => 'Sample Prep Error Extraction Temperature',
      94 => 'Sample Prep Error Extract Manipulation',
      95 => 'Sample Prep Error Loss of Vehicle Glassware',
      96 => 'Sample Prep Error Loss of Vehicle WhirlPak',
      97 => 'Sample Prep Error Wrong components',
      98 => 'Sample Prep Error Wrong storage',
      99 => 'Sample Prep Error Wrong TACA concentration',
      100 => 'Sample Prep Error Wrong vehicle',
      101 => 'Standards Noncompliance',
      102 => 'Sterilization Failed Indicator',
      103 => 'Storage Conditions chemicalreagent',
      104 => 'Storage Conditions cleaning supply',
      105 => 'Storage Conditions specimen',
      106 => 'Storage Conditions study article',
      107 => 'Temp OOR not OOS',
      108 => 'Training task performed with inadequate training',
      109 => 'Training task performed without training',
      110 => 'Unsatisfactory Study Article Receipt',
      111 => 'Water testing missed retest',
    ),
    'Training' => 
    array (
      0 => '',
      1 => 'Associate did not perform before due date',
      2 => 'Associate Out of Office',
      3 => 'Wrong Training assignment to associate',
    ),
    'Vet Check' => 
    array (
    ),
    'Weekly Sweep' => 
    array (
      0 => '',
      1 => 'ACT iSTAT error code',
      2 => 'ACT iSTAT starout',
      3 => 'ACT time interval missed',
      4 => 'ACT value out of range 1000 sec',
      5 => 'ACT value out of range high',
      6 => 'ACT value out of range low',
      7 => 'Additional activity performed',
      8 => 'Analytical Method Criteria Failure',
      9 => 'Analytical standards noncompliance',
      10 => 'Anesthesia Change Without Comment',
      11 => 'Anesthesia Incorrect Change',
      12 => 'Anesthesia Monitor Interval Missed',
      13 => 'Animal Escape',
      14 => 'Animal Housing Inappropriately Group Housed',
      15 => 'Animal Identification incorrect documentation',
      16 => 'Animal Identification incorrect identification',
      17 => 'Animal Inadequate Enrichment',
      18 => 'Animal Weight out of range',
      19 => 'Animal Weight suspect data',
      20 => 'Clinical Pathology Insufficient Volume',
      21 => 'Clinical Pathology missing analyte',
      22 => 'Clinical Pathology missing vet review',
      23 => 'Clinical Pathology sample quality issue',
      24 => 'Controlled Documents Incorrect Document Used',
      25 => 'Controlled Documents Not Archived',
      26 => 'Utilized noncurrent controlled doc',
      27 => 'Data Transfer not performed',
      28 => 'Disregard issue correctable',
      29 => 'Disregard unnecessary COM',
      30 => 'Documentation illegiblewriteover',
      31 => 'Documentation invalid entry',
      32 => 'Documentation missing data',
      33 => 'Documentation Error',
      34 => 'Documentation missing footnote',
      35 => 'Documentation missingincomplete verification',
      36 => 'Duplicate',
      37 => 'Equipment Malfunction',
      38 => 'Equipment OOS',
      39 => 'Equipment PMCal Overdue',
      40 => 'Equipment Tag Missing',
      41 => 'Chemical Reagent Expired',
      42 => 'Cleaning Supply Expired',
      43 => 'Drug Expired',
      44 => 'Lab Supply Expired',
      45 => 'GLP Study Article Expired',
      46 => 'Study Article Abnormality',
      47 => 'Extract Abnormality',
      48 => 'Fluid Administration',
      49 => 'Humidity OOR not OOS',
      50 => 'Chemical Reagent Label Error',
      51 => 'Cleaning Supply Label Error',
      52 => 'Drug Label Error',
      53 => 'Label Missing',
      54 => 'Specimen Label Error',
      55 => 'GLP Study Article Label Error',
      56 => 'MedicationDosing Wrong Dose',
      57 => 'MedicationDosing Wrong route',
      58 => 'Missed Exam scheduling',
      59 => 'Missed Task Acclimation',
      60 => 'Missed Task Bandaging',
      61 => 'Missed Task Cage Cleaning',
      62 => 'Missed Task Clinical Pathology',
      63 => 'Missed Deliverable Generation',
      64 => 'Missed Task Exam',
      65 => 'Missed Task Exercise',
      66 => 'Missed Task Food',
      67 => 'Missed Task Histology',
      68 => 'Missed Task Imaging',
      69 => 'Missed Task Microbiology',
      70 => 'Missed Task Necropsy',
      71 => 'Missed Task Observation',
      72 => 'Missed Task Prep',
      73 => 'Missed Task Preventive Health',
      74 => 'Missed Task Protocol Activity',
      75 => 'Missed Task QuarantineIsolation Release',
      76 => 'Missed Task Specimen Collection',
      77 => 'Missed Task Specimen Processing',
      78 => 'Missed Task Urinary Management',
      79 => 'Missed Task Vet Order',
      80 => 'Missed Task Vitals Monitor',
      81 => 'Missed Task Water',
      82 => 'Missed Task Weight',
      83 => 'Missed Task Wound Care',
      84 => 'Not Applicable',
      85 => 'Photo Nonconformance Image Quality',
      86 => 'Photo Nonconformance Labeling Issue',
      87 => 'Procedure Time Interval Missed',
      88 => 'Report Compilation Error',
      89 => 'Sample Prep Error Analytical',
      90 => 'Sample Prep Error Extraction Duration',
      91 => 'Sample Prep Error Extraction Ratio',
      92 => 'Sample Prep Error Extraction Temperature',
      93 => 'Sample Prep Error Extract Manipulation',
      94 => 'Sample Prep Error Loss of Vehicle Glassware',
      95 => 'Sample Prep Error Loss of Vehicle WhirlPak',
      96 => 'Sample Prep Error Wrong components',
      97 => 'Sample Prep Error Wrong storage',
      98 => 'Sample Prep Error Wrong TACA concentration',
      99 => 'Sample Prep Error Wrong vehicle',
      100 => 'Standards Noncompliance',
      101 => 'Sterilization Failed Indicator',
      102 => 'Storage Conditions chemicalreagent',
      103 => 'Storage Conditions cleaning supply',
      104 => 'Storage Conditions specimen',
      105 => 'Storage Conditions study article',
      106 => 'Temp OOR not OOS',
      107 => 'Training task performed with inadequate training',
      108 => 'Training task performed without training',
      109 => 'Unsatisfactory Study Article Receipt',
      110 => 'Water testing missed retest',
    ),
    'Work Product Schedule Outcome' => 
    array (
    ),
    'Study Specific Charge' => 
    array (
    ),
  ),
);

 
?>
