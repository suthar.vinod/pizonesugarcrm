<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/m06_error_m03_work_product_1_M06_Error.php

 // created: 2018-01-31 15:57:04
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'm06_error_m03_work_product_1',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/m06_error_anml_animals_1_M06_Error.php

 // created: 2018-01-31 16:00:43
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_anml_animals_1'] = array (
  'order' => 100,
  'module' => 'ANML_Animals',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE',
  'get_subpanel_data' => 'm06_error_anml_animals_1',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/m06_error_equip_equipment_1_M06_Error.php

 // created: 2018-01-31 16:07:58
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_equip_equipment_1'] = array (
  'order' => 100,
  'module' => 'Equip_Equipment',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'get_subpanel_data' => 'm06_error_equip_equipment_1',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/m06_error_erd_error_documents_1_M06_Error.php

 // created: 2018-01-31 21:10:46
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_erd_error_documents_1'] = array (
  'order' => 100,
  'module' => 'Erd_Error_Documents',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'm06_error_erd_error_documents_1',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/m06_error_ere_error_employees_1_M06_Error.php

 // created: 2018-01-31 21:12:49
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_ere_error_employees_1'] = array (
  'order' => 100,
  'module' => 'Ere_Error_Employees',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE',
  'get_subpanel_data' => 'm06_error_ere_error_employees_1',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/m06_error_ed_errors_department_1_M06_Error.php

 // created: 2018-02-22 13:58:40
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_ed_errors_department_1'] = array (
  'order' => 100,
  'module' => 'ED_Errors_Department',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_ED_ERRORS_DEPARTMENT_TITLE',
  'get_subpanel_data' => 'm06_error_ed_errors_department_1',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/erqc_error_qc_employees_m06_error_1_M06_Error.php

 // created: 2018-06-01 21:24:07
$layout_defs["M06_Error"]["subpanel_setup"]['erqc_error_qc_employees_m06_error_1'] = array (
  'order' => 100,
  'module' => 'ErQC_Error_QC_Employees',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_ERQC_ERROR_QC_EMPLOYEES_TITLE',
  'get_subpanel_data' => 'erqc_error_qc_employees_m06_error_1',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/m06_error_de_deviation_employees_1_M06_Error.php

 // created: 2018-07-30 20:17:15
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_de_deviation_employees_1'] = array (
  'order' => 100,
  'module' => 'DE_Deviation_Employees',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'get_subpanel_data' => 'm06_error_de_deviation_employees_1',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/m06_error_tasks_1_M06_Error.php

 // created: 2019-05-19 21:27:08
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'm06_error_tasks_1',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/m06_error_rms_room_1_M06_Error.php

 // created: 2019-02-21 20:21:31
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_rms_room_1'] = array (
  'order' => 100,
  'module' => 'RMS_Room',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_RMS_ROOM_1_FROM_RMS_ROOM_TITLE',
  'get_subpanel_data' => 'm06_error_rms_room_1',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/m06_error_m06_error_1_M06_Error.php

 // created: 2020-07-07 08:16:55
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_m06_error_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_M06_ERROR_1_FROM_M06_ERROR_R_TITLE',
  'get_subpanel_data' => 'm06_error_m06_error_1',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/m06_error_de_deviation_employees_2_M06_Error.php

 // created: 2022-01-03 10:55:30
$layout_defs["M06_Error"]["subpanel_setup"]['m06_error_de_deviation_employees_2'] = array (
  'order' => 100,
  'module' => 'DE_Deviation_Employees',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_2_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'get_subpanel_data' => 'm06_error_de_deviation_employees_2',
);

?>
<?php
// Merged from custom/Extension/modules/M06_Error/Ext/WirelessLayoutdefs/bid_batch_id_m06_error_1_M06_Error.php

 // created: 2022-04-26 07:03:59
$layout_defs["M06_Error"]["subpanel_setup"]['bid_batch_id_m06_error_1'] = array (
  'order' => 100,
  'module' => 'BID_Batch_ID',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_BID_BATCH_ID_M06_ERROR_1_FROM_BID_BATCH_ID_TITLE',
  'get_subpanel_data' => 'bid_batch_id_m06_error_1',
);

?>
