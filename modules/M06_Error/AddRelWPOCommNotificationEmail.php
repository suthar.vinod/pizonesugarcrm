<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class AddRelWPOCommNotificationEmail
{
    function AddRelWPOCommNotificationRecord($bean, $event, $arguments)
    {
        $site_url = $GLOBALS['sugar_config']['site_url'];
        $domainToSearch = 'apsemail.com';
        $module = $arguments['module'];
        $related_module = $arguments['related_module'];
        global $db, $sugar_config, $current_user;
        
        if (($module == 'M06_Error') && ($related_module == 'ANML_Animals' || $related_module == 'M03_Work_Product') && ($bean->error_category_c == 'Work Product Schedule Outcome'))
        {

            /*Query to get number of records linked of test system*/
            
                $sqlTS = "SELECT count(*) as NUM FROM m06_error_anml_animals_1_c WHERE m06_error_anml_animals_1m06_error_ida='" . $bean->id . "'  ORDER BY date_modified desc";
                $resultTS = $db->query($sqlTS);
                $rowTS = $db->fetchByAssoc($resultTS);
                $NoofTSRecord = $rowTS['NUM'];

            /*Query to get number of records linked of work product*/
            
                $sqlWP = "SELECT count(*) as NUM FROM m06_error_m03_work_product_1_c WHERE m06_error_m03_work_product_1m06_error_ida='" . $bean->id . "' ORDER BY date_modified desc";
                $resultWP = $db->query($sqlWP);
                $rowWP = $db->fetchByAssoc($resultWP);
                $NoofWPRecord = $rowWP['NUM'];
                

            if ($NoofTSRecord > 1 || $NoofWPRecord > 1)
            {
                /** Code for sending email **/
                $communication_name = '<a target="_blank"href="' . $site_url . '/#M06_Error/' . $bean->id . '">' . $bean->name . '</a>';

                $template = new EmailTemplate();
                $emailObj = new Email();
                $EmailSubject = "Notification of WPO COM update";
                $template->retrieve_by_string_fields(array(
                    'name' => 'Notification of WPO COM update',
                    'type' => 'email'
                ));

                $template->body_html = str_replace('[comm_name]', $communication_name, $template->body_html);
                
                $defaults = $emailObj->getSystemDefaultEmail();
                $mail = new SugarPHPMailer();
                $mail->setMailerForSystem();
                $mail->IsHTML(true);
                $mail->From = $defaults['email'];
                $mail->FromName = $defaults['name'];
                $mail->Subject = $EmailSubject;
                $mail->Body = $template->body_html;

                /*send mail to team  operation support */
                $team_name = 'Operations Support';
                $company = 'american preclinical services';
                $email_addresses = $this->get_related_teams($team_name);
                if (!empty($email_addresses))
                {
                    foreach ($email_addresses as $email)
                    {
                        if ($this->verifyContactForNotification($company, $email))
                        {
                            //$mail->AddAddress($email);
                            
                        }
                    }
                }

                $con_email_addresses = $this->get_related_contacts_email_addresses($team_name);
                if (!empty($con_email_addresses))
                {
                    foreach ($con_email_addresses as $email)
                    {
                        if ($this->verifyContactForNotification($company, $email))
                        {
                            //$mail->AddAddress($email);
                            
                        }
                    }
                }

                $mail->AddAddress('calcox@apsemail.com');
                $mail->AddAddress('lzugschwert@apsemail.com');
                $mail->AddAddress('lwalz@apsemail.com');
                $mail->AddAddress('ehollen@apsemail.com');
                $mail->AddAddress('dlmccarty@apsemail.com');
                $mail->AddAddress('IACUCSubmissions@apsemail.com');

                if (!$mail->Send())
                {
                    $GLOBALS['log']->fatal("Fail to Send Email, Please check settings file relationship");
                }
                else
                {
                    $GLOBALS['log']->fatal('email sent');
                    //$GLOBALS['log']->fatal("Template : ".print_r($template->body_html,1));
                    
                }
                unset($mail);
            }
        }
    }

    /**
     * Members which are associated with team, return there email addresses
     * @param type $team_name
     * @return type array
     */
    function get_related_teams($team_name)
    {
        $email_addresses = array();

        $get_team_members = "SELECT email_addresses.email_address FROM teams " . "INNER JOIN team_memberships ON teams.id = team_memberships.team_id " . "INNER JOIN users ON team_memberships.user_id = users.id " . "INNER JOIN email_addr_bean_rel ON users.id = email_addr_bean_rel.bean_id " . "INNER JOIN email_addresses ON email_addr_bean_rel.email_address_id = email_addresses.id " . "WHERE teams.name = '" . $team_name . "' AND teams.deleted = 0 AND team_memberships.deleted = 0 AND users.deleted = 0 ";

        $team_members = $GLOBALS['db']->query($get_team_members);
        while ($team_member_address = $GLOBALS["db"]->fetchByAssoc($team_members))
        {
            if (!empty($team_member_address['email_address']))
            {
                $email_addresses[] = $team_member_address['email_address'];
            }
        }

        return $email_addresses;
    }

    /**
     * Contacts which are associated with team, return there email addresses
     * @param type $team_name
     * @return type array
     */
    function get_related_contacts_email_addresses($team_name)
    {

        $contacts_email_addreses = array();

        $query = new SugarQuery();
        $query->from(BeanFactory::newBean('Teams') , array(
            'alias' => 'teams'
        ));
        $query->joinTable('team_sets_teams', array(
            'joinType' => 'INNER',
            'alias' => 'tst',
            'linkingTable' => true,
        ))
            ->on()
            ->equalsField('teams.id', 'tst.team_id');
        $query->joinTable('contacts', array(
            'joinType' => 'INNER',
            'alias' => 'c',
            'linkingTable' => true,
        ))
            ->on()
            ->equalsField('tst.team_set_id', 'c.team_set_id');
        $query->joinTable('email_addr_bean_rel', array(
            'joinType' => 'INNER',
            'alias' => 'eabr',
            'linkingTable' => true,
        ))
            ->on()
            ->equalsField('c.id', 'eabr.bean_id');
        $query->joinTable('email_addresses', array(
            'joinType' => 'INNER',
            'alias' => 'ea',
            'linkingTable' => true,
        ))
            ->on()
            ->equalsField('eabr.email_address_id', 'ea.id');

        $query->where()
            ->equals('teams.name', $team_name)->equals('teams.deleted', 0)
            ->equals('tst.deleted', 0)
            ->equals('c.deleted', 0)
            ->equals('eabr.deleted', 0);
        $query->select('ea.email_address');
        $compile_query = $query->compile();
        $result_query = $query->execute();

        foreach ($result_query as $key => $innerArray)
        {
            foreach ($innerArray as $key => $email_address)
            {
                $contacts_email_addreses[] = $email_address;
            }
        }

        return $contacts_email_addreses;
    }

    function verifyContactForNotification($account_name, $email)
    {
        $companyToSearch = 'american preclinical services';
        $domainToSearch = 'apsemail.com';
        $domain_name = substr(strrchr($email, "@") , 1);
        if (trim(strtolower($account_name)) == $companyToSearch && strtolower($domain_name) == $domainToSearch)
        {
            return true;
        }
        return false;
    }

}