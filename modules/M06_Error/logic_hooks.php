<?php

$hook_version = 1;
$hook_array = Array();
// position, file, function 
//$hook_array['before_save'] = Array(); 
$hook_array['before_save'][] = Array('1', 'workflow', 'include/workflow/WorkFlowHandler.php', 'WorkFlowHandler', 'WorkFlowHandler');
$hook_array['before_save'][] = Array('100', 'Save Email Document Record', 'custom/modules/M06_Error/saveNotificationEmail.php', 'saveNotificationEmail', 'saveNotificationRecord');
$hook_array['before_save'][] = Array('10', 'Copy Date Time Discovered  to Date Time Discovered when Category is Deceased Animal', 'custom/modules/M06_Error/copyDateDiscoveredToDateOccurred.php', 'copyDateDiscoveredToDateOccurred', 'copyDateValue',);

$hook_array['after_relationship_add'][] = Array(
    56,
    'Check Test System Module',
    'custom/modules/M06_Error/checkTestSystemModule.php',
    'checkTestSystemModule',
    'checkTestSystem',
);
/**13 July 2022 : To fix bug #2644 : Null TS record creation
 * We have commented below logic hook to avoid blank TS recrod creation */

/* $hook_array['after_relationship_delete'][] = Array(
    57,
    'Check Test System Module',
    'custom/modules/M06_Error/checkTestSystemModule.php',
    'checkTestSystemModule',
    'checkTestSystem',
); */
/**EOC ** 13 July 2022 : To fix bug #2644 : Null TS record creation */

$hook_array['before_save'][] = Array(
    '101',
    'Save SD Acknowledgement Email Document Record',
    'custom/modules/M06_Error/saveAcknowledgementNotificationEmail.php',
    'saveAcknowledgementNotificationEmail',
    'saveAcknowledgementNotificationRecord'
);

$hook_array['before_save'][] = Array(
    '102',
    'Save Resolution Email Document Record',
    'custom/modules/M06_Error/saveResolutionNotificationEmail.php',
    'saveResolutionNotificationEmail',
    'saveResolutionNotificationRecord'
);

$hook_array['after_retrieve'][] = array(
    '11',
    'Delete Date Time Discovered in Audit',
    'custom/modules/M06_Error/deleteAuditLogDateTimeDiscovered.php',
    'deleteAuditLogDateTimeDiscovered',
    'deleteDateTimeDiscovered'
);
/* #931 : 29 March 2021 */
$hook_array['before_save'][] = Array(
    '103',
    'Save WPO Comm Update Record',
    'custom/modules/M06_Error/saveWPOCommNotificationEmail.php',
    'saveWPOCommNotificationEmail',
    'saveWPOCommNotificationRecord'
    );
    
$hook_array['after_relationship_add'][] = array(
    105,
    'Add relation test sys/wp WPO Comm Update Record',
    'custom/modules/M06_Error/AddRelWPOCommNotificationEmail.php',
    'AddRelWPOCommNotificationEmail',
    'AddRelWPOCommNotificationRecord',
);
$hook_array['after_relationship_delete'][] = array(
    106,
    'Add relation test sys/wp WPO Comm Update Record',
    'custom/modules/M06_Error/AddRelWPOCommNotificationEmail.php',
    'AddRelWPOCommNotificationEmail',
    'AddRelWPOCommNotificationRecord',
);
/*28 July 2021 : bug#1249 */
$hook_array['after_relationship_add'][] = Array(
    111,
    'Update Activity and Test System after saving communication module record',
    'custom/modules/M06_Error/AutoUpdateWPA.php',
    'AutoUpdateWPA',
    'updateWPA'
);
/*21 june 2021  */
$hook_array['process_record'][] = array(
    '107',
    'delete null Audit log Record for activity field',
    'custom/modules/M06_Error/deleteAuditLog.php',
    'deleteAuditLog',
    'deleteAudit'
 );

 /*#1145 */
$hook_array['after_save'][] = Array(
    '107',
    'Update Deviation Rate in CDU',
    'custom/modules/M06_Error/UpdatedeviationCDU.php',
    'UpdatedeviationCDU',
    'UpdatedeviationCDURecord'
);
$hook_array['after_relationship_delete'][] = Array(
    '108',
    'Update Deviation Rate in CDU',
    'custom/modules/M06_Error/UpdatedeviationCDUonunlink.php',
    'UpdatedeviationCDUonunlink',
    'UpdatedeviationCDUonunlinkRecord'
);

$hook_array['before_save'][] = array(
    '107',
    'Re-identification auto-creation of USDA ID record',
    'custom/modules/M06_Error/create_usda_id_record.php',
    'create_usda_id_record',
    'create_usda_id_record_after_save'
);

// $hook_array['after_relationship_add'][] = array(
//     '110',
//     'Communication pull from app',
//     'custom/modules/M06_Error/date_comm_to_ts.php',
//     'update_ts_date_record',
//     'update_ts_date_record_after_save'
// );

/* Ticket#1833 Autopopulate Actual Subtype Date - mgt assessment redo */
$hook_array['before_save'][] = array(
    '114',
    'Autopopulate Actual Subtype Date - mgt assessment redo',
    'custom/modules/M06_Error/populates_actual_subtype_date.php',
    'populates_actual_subtype_date',
    'populates_actual_subtype_date_save'
);
?>