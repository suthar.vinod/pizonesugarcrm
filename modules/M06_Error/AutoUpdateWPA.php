<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class AutoUpdateWPA
{
    public function updateWPA($bean, $event, $arguments)
    {
        global $db;
        //Cureent Communication category name
        $category = $bean->error_category_c;
        if ($category == "Work Product Schedule Outcome") {

            $bean->load_relationship("m06_error_m03_work_product_1");
            $bean->load_relationship("m06_error_anml_animals_1");
            $workProductIDArray = $bean->m06_error_m03_work_product_1->get();            
            if(!empty($workProductIDArray))
            {
                $workProductID = $workProductIDArray[0];               
            }
            else
            {
                $workProductID =  preg_replace('/[^A-Za-z0-9\-]/', '', $bean->work_products);                
            }      
            $testSystemIdsArray = $bean->m06_error_anml_animals_1->get();
            $testSysids = join("','", $testSystemIdsArray);            
            $finalWPEIdsAfterMathingWP = "SELECT m03_work_p9bf5ollment_idb as WPAID, WPE.date_entered 
                                      FROM   m03_work_product_wpe_work_product_enrollment_1_c 
                                      LEFT JOIN wpe_work_product_enrollment As WPE
                                      ON WPE.id = m03_work_p9bf5ollment_idb 
                                      WHERE  m03_work_p9bf5ollment_idb 
                                      IN (SELECT anml_anima9941ollment_idb 
                                          FROM anml_animals_wpe_work_product_enrollment_1_c 
                                          WHERE  anml_animals_wpe_work_product_enrollment_1anml_animals_ida IN ('$testSysids') 
                                          AND anml_animals_wpe_work_product_enrollment_1_c.deleted = 0) 
                                      AND m03_work_p7d13product_ida = '$workProductID' 
                                      AND m03_work_product_wpe_work_product_enrollment_1_c.deleted = 0 
                                      AND WPE.deleted = 0 
                                      ORDER  BY WPE.date_entered DESC LIMIT  1";            
            $resultFinalId = $db->query($finalWPEIdsAfterMathingWP);
            if ($resultFinalId->num_rows > 0) {
                $rowFinalId = $db->fetchByAssoc($resultFinalId);
                //Final WPE id that will be updated
                $finalWPEID = $rowFinalId['WPAID'];                
                if ($finalWPEID != "") {
                    //Bean of WPE that will be updated (Activities)
                    $WPEBean = BeanFactory::retrieveBean('WPE_Work_Product_Enrollment', $finalWPEID);
                    $oldActivityString = $WPEBean->activities_c;
                    $oldWPEActivityArr = explode(",", $oldActivityString);                    
                    $activity = $bean->activities_c;                   
                    $activity = str_replace("^Only Other Protocol Activities^", "^Other Activities^", $activity);                    
                    $WPActivityArr = array();

                    if (strlen($WPEBean->activities_c)) {
                        if ($WPEBean->activities_c != '^^') {
                            $WPActivityArr = explode(",", $WPEBean->activities_c);
                        }
                    }

                    $CommActivityArr = array();

                    if (strlen($activity)) {
                        $CommActivityArr = explode(",", $activity);
                    }

                    $CombinedArr = array_unique(array_merge($WPActivityArr, $CommActivityArr));

                    if (array_diff($CombinedArr, $oldWPEActivityArr)) {
                        $activityString = implode(",", $CombinedArr);
                        $activityString = trim($activityString, ",");                        
                        if ($activityString != '' || $activityString != '^^') {
                            $WPEBean->activities_c = $activityString;                            
                            $WPEBean->save();
                        }
                    }
                }
            }
            /* 14 April 2021 : #1141 By Gsingh */
            /*the system searches for the most recent (by Date Created) Work Product Assignment with the
                 matching Test System and (Work Product or Blanket Work Product). */

            /**Fetch  the latest WPE record with matching Blankat WP */
            $latestWPEId = "SELECT m03_work_p90c4ollment_idb as wpaID, WPE.date_entered
                                FROM   m03_work_product_wpe_work_product_enrollment_2_c 
                                LEFT JOIN wpe_work_product_enrollment As WPE
                                ON WPE.id = m03_work_p90c4ollment_idb 
                                WHERE  m03_work_p90c4ollment_idb 
                                IN (SELECT anml_anima9941ollment_idb 
                                    FROM anml_animals_wpe_work_product_enrollment_1_c 
                                    WHERE  anml_animals_wpe_work_product_enrollment_1anml_animals_ida IN ('$testSysids') 
                                    AND anml_animals_wpe_work_product_enrollment_1_c.deleted = 0) 
                                AND m03_work_p9f23product_ida = '$workProductID' 
                                AND m03_work_product_wpe_work_product_enrollment_2_c.deleted = 0 
                                AND WPE.deleted = 0 
                                ORDER  BY WPE.date_entered DESC LIMIT  1";
            $resultLatestWPEId = $db->query($latestWPEId);
            if ($resultLatestWPEId->num_rows > 0) {
                $rowLatestWPEId = $db->fetchByAssoc($resultLatestWPEId);
                //Final WPE id that will be updated
                $latest_WPEID = $rowLatestWPEId['wpaID'];                
                if ($latest_WPEID != "") {
                    //Bean of WPE that will be updated (Activities)
                    $wpeBean = BeanFactory::retrieveBean('WPE_Work_Product_Enrollment', $latest_WPEID);
                    $old_ActivityString = $wpeBean->activities_c;
                    $old_WPEActivityArr = explode(",", $old_ActivityString); 
                    $Activity = $bean->activities_c;
                    $Activity = str_replace("^Only Other Protocol Activities^", "^Other Activities^", $Activity);                    
                    $wpActivityArr = array();

                    if (strlen($wpeBean->activities_c)) {
                        if ($wpeBean->activities_c != '^^') {
                            $wpActivityArr = explode(",", $wpeBean->activities_c);
                        }
                    }
                    $COMMActivityArr = array();

                    if (strlen($Activity)) {
                        $COMMActivityArr = explode(",", $Activity);
                    }
                    $Combined_Arr = array_unique(array_merge($wpActivityArr, $COMMActivityArr));

                    if (array_diff($Combined_Arr, $old_WPEActivityArr)) {
                        $activity_String = implode(",", $Combined_Arr);
                        $activity_String = trim($activity_String, ",");                        
                        if ($activity_String != '' || $activity_String != '^^') {
                            $wpeBean->activities_c = $activity_String;                            
                            $wpeBean->save();
                        }
                    }
                }
            }
        }
    }
}