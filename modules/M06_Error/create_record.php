<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class create_record
{
    static $already_ran = false;
    /**
     * Create all the related records which we are not creating in wordpress app side like
     * with Observation Employee and Vet Observation Module
     * @param $bean
     * @param $event
     * @param $arguments
     */
    function create_record_after_save($bean, $event, $arguments)
    {

        if (self::$already_ran == true)
            return;   //So that hook will only trigger once
        self::$already_ran = true;

        if ($bean->id !== $bean->fetched_row['id'] && $bean->wordpress_flag == 1) {

            $module = 'DE_Deviation_Employees';
            $module2 = 'VO_Vet_Observations';
            $vet_observation_data = $this->jsonDecode($bean->vet_observation_data_c);

            if ($bean->rp_entry != '') {
                $rp_entry = $this->jsonDecode($bean->rp_entry);
                if ($bean->rp_entry !== 'null') {
                    $this->setType($bean, $rp_entry, $module, 'Entry', $arguments);
                }
            }

            if ($bean->rp_review != '') {
                $rp_review = $this->jsonDecode($bean->rp_review);

                if ($bean->rp_review !== 'null') {
                    $this->setType($bean, $rp_review, $module, 'Review', $arguments);
                }
            }
        }
    }

    /**
     * @param $arr
     * @return array|mixed
     */
    function jsonDecode($arr)
    {

        $result = str_replace("\\\"", '', $arr);
        $result = str_replace("[", '', $result);
        $result = str_replace("]", '', $result);
        $result = explode(",", $result);
        return $result;
    }

    function setType($bean, $arr, $module, $type, $arguments)
    {

        for ($i = 0; $i < count($arr); $i++) {
            $new_bean = BeanFactory::newBean($module);
            $new_bean->name = $this->nameCreation($bean, $arguments, $arr[$i]);
            $new_bean->ere_error_ee6cployees_ida = $arr[$i];
            $new_bean->contacts_de_deviation_employees_1contacts_ida = $arr[$i];
            $new_bean->deviation_type_c = $type;
            $new_bean->save();

            $bean->load_relationship('m06_error_de_deviation_employees_2');
            $bean->m06_error_de_deviation_employees_2->add($new_bean->id);
        }
    }

    /**
     * Create the relationship of Vet Observations module with Observation module
     * @param type $bean
     * @param type $arr
     * @param type $module
     * @param type $type
     * @param type $arguments
     */
    function add_relation_of_vet_observation($bean, $data, $module, $arguments)
    {

        $new_bean = BeanFactory::newBean($module);
        $new_bean->name = $this->nameCreationForVetObservation();
        $new_bean->temperature = $data[0];
        $new_bean->heart_rate = $data[1];
        $new_bean->respiratory_rate = $data[2];
        $new_bean->save();

        //Add the relationship with Vet Observation Module
        $bean->load_relationship('m06_error_vo_vet_observations_1');
        $bean->m06_error_vo_vet_observations_1->add($new_bean->id);

        //Add the relationship between Vet Observation and Test System Modules
        $new_bean->load_relationship('anml_animals_vo_vet_observations_1');
        $new_bean->anml_animals_vo_vet_observations_1->add($bean->is_one_animal_c);
    }

    /**
     * Generate the auto naming sequence for Vet Observation Module
     */
    function nameCreationForVetObservation()
    {
        $GLOBALS['log']->fatal("nameCreationForVetObservation");

        global $db;
        $year = date("Y");
        $year = substr($year, -2);
        $initial = "VO" . $year . '-';

        $query = "SELECT name FROM vo_vet_observations ORDER BY vo_vet_observations.date_entered DESC "
            . "LIMIT 0,1";

        $deleted = '0';
        //        $query_test = "SELECT name FROM vo_vet_observation WHERE deleted = ? ORDER BY vo_vet_observation.date_entered DESC "
        //                . "LIMIT 0,1"; 
        //        $conn = $db->getConnection();
        //        $stmt = $conn->executeQuery($query_test, array($deleted));
        //   $compiled = $query->compile();
        // $GLOBALS['log']->fatal("compiled query is ". $compiled);
        // $GLOBALS['log']->fatal("parameterized sql is ". $compiled->getSQL());
        // $GLOBALS['log']->fatal("parameters are ". $compiled->getParameters());
        $result = $db->query($query);
        if ($row = $db->fetchByAssoc($result)) {
            $name = $row['name'];
            $name_array = explode('-', $name);
            if ($name_array[1]) {

                //check this is same year or next year
                $pre_year = $name_array[0];
                $pre_year = substr($pre_year, -2);

                //we are in the same year
                if ($pre_year == $year) {
                    $number = filter_var($name_array[1], FILTER_SANITIZE_NUMBER_INT);
                    $new_number = $number + 1;
                } else {
                    $new_number = 1;
                }
            }
            //Creating first time
            else {
                $new_number = 1;
            }
        }
        $new_number = sprintf('%05d', $new_number);
        $new_name = $initial . $new_number;
        return $new_name;
    }

    function nameCreation($bean, $arguments, $data)
    {

        global $db;

        $date_error_occurred_c = strtotime($bean->date_error_occurred_c);
        $day = date("D", $date_error_occurred_c); //get day of "Date Error Occured" 2 digit
        if ($day == "Sat" or $day == "Sun") {
            $bean->occured_on_weekend_c = 1;
        } else {
            $bean->occured_on_weekend_c = 0;
        }

        $curr_year = date("y", $date_error_occurred_c); //get year of "Date Error Occured" 2 digit
        $py = 'CE' . $curr_year;

        //Read Lock
        /* $sql_lock = "Lock TABLES ere_error_employees_de_deviation_employees_1_c READ, de_deviation_employees READ";
          $bean->db->query( $sql_lock ); */

        if ($arguments['isUpdate'] == false) {

            $query = "SELECT max(de_deviation_employees.name) AS name FROM `de_deviation_employees`
                    INNER JOIN contacts_de_deviation_employees_1_c
                        ON de_deviation_employees.id = contacts_de_deviation_employees_1_c.contacts_de_deviation_employees_1de_deviation_employees_idb
                        AND contacts_de_deviation_employees_1_c.deleted = 0
                    WHERE contacts_de_deviation_employees_1_c.contacts_de_deviation_employees_1contacts_ida = '{$data}'
                        AND de_deviation_employees.deleted = 0 AND de_deviation_employees.name LIKE '%{$curr_year}%'
                    ORDER BY contacts_de_deviation_employees_1_c.date_modified DESC;";

            $result = $db->query($query);

            // Unlock Table
            /* $sql_unlock = "UNLOCK TABLES";
              $bean->db->query( $sql_unlock ); */

            if ($result->num_rows > 0) {
                $row = $db->fetchByAssoc($result);

                if (!empty($row['name'])) {
                    $system_id = $row['name'];
                    $system_id = explode('-', $system_id);

                    $prev_year = str_split($system_id[0]);

                    $system_id = str_split($system_id[1]);
                    $system_id = $system_id[0] . $system_id[1] . $system_id[2] . $system_id[3];

                    if ($prev_year[0] == 'D') {
                        $prev_year = $prev_year[1] . $prev_year[2];
                    } else {
                        $prev_year = $prev_year[2] . $prev_year[3];
                    }
                } else {
                    $prev_year = 0;
                    $system_id = 1;
                }


                if (!empty($system_id)) {
                    if ($prev_year == $curr_year) {
                        // we are in same year
                        $GLOBALS['log']->fatal('we are in same year as yesterday');
                        $code = $this->generate_code($system_id, $curr_year);
                    } else {
                        $code .= 'CE' . $curr_year . '-0001';
                    }
                }
            } else {
                $code = 'CE' . $curr_year . '-0001';
            }
        } else {
            $code = 'CE' . $curr_year . '-0001';
        }

        if ($code != "") {
            return $code;
        }
    }

    function generate_code($system_id, $curr_year)
    {
        #$number = (int) substr($system_id, -4);
        $number = (int) $system_id;
        $number = $number + 1;
        $length = strlen($number);
        if ($length == 1) {
            $number = '000' . $number;
        } else if ($length == 2) {
            $number = '00' . $number;
        } else if ($length == 3) {
            $number = '0' . $number;
        }
        $code .= 'CE' . $curr_year . '-' . $number;

        return $code;
    }
}
