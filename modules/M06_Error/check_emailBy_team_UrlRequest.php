<?php


	global $db;
	 
	$related_dataArr		= array(); 
	  
	echo "<pre>";
    if(isset($_REQUEST['team_name'])){
        $team_name = $_REQUEST['team_name'];
    }else{
        $team_name = "APS Feedback";
    }
	
	$con_email_addresses = get_related_contacts_email_addresses($team_name);
    if (!empty($con_email_addresses)) {
		
		print_r($con_email_addresses);
    }
					
	 
	 function get_related_contacts_email_addresses($team_name)
    {
        $contacts_email_addreses = array();
        $query = new SugarQuery();
        $query->from(BeanFactory::newBean('Teams'), array('alias' => 'teams'));
        $query->joinTable('team_sets_teams', array(
            'joinType' => 'INNER',
            'alias' => 'tst',
            'linkingTable' => true,
        ))->on()->equalsField('teams.id', 'tst.team_id');
        $query->joinTable('contacts', array(
            'joinType' => 'INNER',
            'alias' => 'c',
            'linkingTable' => true,
        ))->on()->equalsField('tst.team_set_id', 'c.team_set_id');
        $query->joinTable('email_addr_bean_rel', array(
            'joinType' => 'INNER',
            'alias' => 'eabr',
            'linkingTable' => true,
        ))->on()->equalsField('c.id', 'eabr.bean_id');
        $query->joinTable('email_addresses', array(
            'joinType' => 'INNER',
            'alias' => 'ea',
            'linkingTable' => true,
        ))->on()->equalsField('eabr.email_address_id', 'ea.id');

        $query->where()
            ->equals('teams.name', $team_name)
            ->equals('teams.deleted', 0)
            ->equals('tst.deleted', 0)
            ->equals('c.deleted', 0)
            ->equals('eabr.deleted', 0);
        $query->select('ea.email_address');
        $compile_query = $query->compile();
        $result_query = $query->execute();

        foreach ($result_query as $key => $innerArray) {
            foreach ($innerArray as $key => $email_address) {
                $contacts_email_addreses[] = $email_address;
            }
        }

        return $contacts_email_addreses;
    }
