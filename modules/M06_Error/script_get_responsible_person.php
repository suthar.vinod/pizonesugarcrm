<?php 
	global $db; 
	$site_url = $GLOBALS['sugar_config']['site_url'];
    $query = "Select * FROM `m06_error_de_deviation_employees_2_c` WHERE m06_error_de_deviation_employees_2m06_error_ida = '461e6d36-1e59-11ed-adac-02fb813964b8'";

	$queryResult = $db->query($query);		 
	$srno = 1;
	
	$tableBody = "<table width='1050px' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr>
			<th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th bgcolor='#b3d1ff' align='left'>M06 Error Id</th>
			<th bgcolor='#b3d1ff' align='left'>Responsible Person ID</th>
			<th bgcolor='#b3d1ff' align='left'>Status</th>
			<th bgcolor='#b3d1ff' align='left'>Modified Date</th>
			</tr>";
			
			
    while ($fetchResult = $db->fetchByAssoc($queryResult)) {

		if($srno%2==0)
			$stylr = "style='background-color: #e6e6e6;'";
		else
			$stylr = "style='background-color: #f3f3f3;'";
		
		$comID = $fetchResult['m06_error_de_deviation_employees_2m06_error_ida'];
		$devID = $fetchResult['m06_error_de_deviation_employees_2de_deviation_employees_idb'];
		
		$comBean = BeanFactory::getBean('M06_Error', $comID);
		$devBean = BeanFactory::getBean('DE_Deviation_Employees', $devID);
		
		$comLink =  '<a target="_blank"href="' . $site_url . '/#M06_Error/' . $comID . '">' . $comBean->name . '</a>';
		$devLink =  '<a target="_blank"href="' . $site_url . '/#DE_Deviation_Employees/' . $devID . '">' . $devBean->name . '</a>';

		$tableBody .= "<tr><td ".$stylr.">".$srno++."</td>
							<td ".$stylr.">".$comLink."</td> 
							<td ".$stylr.">".$devLink."</td>		
							<td ".$stylr.">".$fetchResult['deleted']."</td>		
							<td ".$stylr.">".$fetchResult['date_modified']."</td>		
							</tr>";		
	}	 
	echo "<br><br>".$tableBody .="</table>";
