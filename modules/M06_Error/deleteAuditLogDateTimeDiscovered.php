<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class deleteAuditLogDateTimeDiscovered {

    function deleteDateTimeDiscovered($bean, $event, $arguments) {
        global $db;

        $sql_DeleteAudit = "DELETE from `m06_error_audit` WHERE `field_name`='date_time_discovered_c' AND `parent_id`='" . $bean->id . "' AND `created_by`='943a2d38-7bd3-11e9-99fb-06e41dba421a' AND  `before_value_string` IS NULL AND `after_value_string` IS NOT NULL LIMIT 1";
        $DeleteAudit = $db->query($sql_DeleteAudit); 


        $queryAuditLogUpdate = "UPDATE `m06_error_audit` SET before_value_string = '' WHERE `field_name`='date_time_discovered_c' AND `parent_id`='" . $bean->id . "' AND `created_by`='943a2d38-7bd3-11e9-99fb-06e41dba421a'";
        $db->query($queryAuditLogUpdate);

        /* 1014 Delete Test Systems involved UI Issue */
            $allRecord	=	0;			
			$queryCountAll = "SELECT TS.name AS TSName, TS.id AS TSId FROM m06_error_anml_animals_1_c AS COMMTS
							  LEFT JOIN anml_animals AS TS ON COMMTS.m06_error_anml_animals_1anml_animals_idb = TS.id 
			 				  WHERE m06_error_anml_animals_1m06_error_ida='" . $bean->id . "' AND COMMTS.deleted=0 ";

			$resultAllRecord = $db->query($queryCountAll);

			if ($resultAllRecord->num_rows > 0) {
				$allRecord = 1;
			}

			if ($allRecord > 0) {
				$test_system_involved_c = 'Yes';
				//$bean->test_system_name_c = $TSNameString;
			} else {
				$test_system_involved_c = 'No';
				//$bean->test_system_name_c = $TSNameString;
			}
            if($bean->test_system_involved_c != $test_system_involved_c) {
                $queryFiledUpdate = "UPDATE `m06_error_cstm` SET test_system_involved_c = '" .$test_system_involved_c. "' WHERE `id_c`='" . $bean->id . "'";
                $db->query($queryFiledUpdate);
            }
    }

}

?>