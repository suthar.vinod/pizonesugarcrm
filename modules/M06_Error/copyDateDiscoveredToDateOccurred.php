<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class copyDateDiscoveredToDateOccurred{
	function copyDateValue($bean, $event, $arguments){
		global $db,$current_user;
		/* Copy date_time_discovered to Date Occurred while edit the record*/
		if(($bean->id == $bean->fetched_row['id']) && $bean->error_category_c=="Deceased Animal"){
			$bean->date_error_occurred_c  = $bean->date_time_discovered_c;				
		}			 
	}
}
?>