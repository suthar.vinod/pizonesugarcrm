<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class saveResolutionNotificationEmail {
	function saveResolutionNotificationRecord($bean, $event, $arguments) {
		//global $db,$current_user; 
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$domainToSearch = 'apsemail.com';
	
		global $db,$sugar_config,$current_user; 
		global $app_list_strings;
		
        $category_dom	 = $app_list_strings['category_list'];
		$type_dom 		 = $app_list_strings['error_category_list'];

		 if($bean->id == $bean->fetched_row['id']
		 	&& ($bean->resolved_c != $bean->fetched_row['resolved_c']) 
		 	&& ($bean->fetched_row['resolved_c'] == 0)
		   )
		 {
			
			$workProductId = "";			
			$commID = $bean->id;
			$commName = $bean->name;
			$departmentName   = "Veterinary Services";


			/*Get Study Director and Email*/
			$wp_sql = "SELECT m06_error_m03_work_product_1m03_work_product_idb AS WPID  FROM `m06_error_m03_work_product_1_c` AS COMM_WP 
						WHERE COMM_WP.`m06_error_m03_work_product_1m06_error_ida`='".$commID."' 
						AND deleted=0 ORDER BY `COMM_WP`.`date_modified` DESC limit 1";
			$wp_exec = $db->query($wp_sql);
			if ($wp_exec->num_rows > 0) 
			{
				$result = $db->fetchByAssoc($wp_exec);
				$workProductId = $result['WPID'];
				$WP_bean = BeanFactory::getBean('M03_Work_Product', $workProductId);			
				$stdy_id = $WP_bean->contact_id_c;
				$wp_name_c = $WP_bean->name;
				$wp_name_c_list =  '<a target="_blank" href="' . $site_url . '/#M03_Work_Product/' . $workProductId . '">' .$wp_name_c . '</a>';
				$stdy_bean = BeanFactory::getBean('Contacts', $stdy_id);
				$studyDirectorName = $stdy_bean->name;
				$SDEmailAddress = $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
			}
			
			//Adding selected team members for notification email
			$user_email = array();
			$selected_teams = 'Clinical Vet';
			$company = 'american preclinical services';

			//Adding all the contact's email addresses of selected teams:
			$con_email_addresses = $this->get_related_contacts_email_addresses($selected_teams);

			if (!empty($con_email_addresses)) {
			foreach ($con_email_addresses as $email) {
				if ($this->verifyContactForNotification($company, $email)) {
					array_push($user_email, $SDEmailAddress);
					
					}
				}
			}

			$cv_team_id = 'f019638e-5b95-11e9-9bd1-06036fae356c';
			$team = BeanFactory::getBean('Teams', $cv_team_id);
			$team_members = $team->get_team_members(true); //Only get active users
			if (!empty($team_members)) {
				foreach ($team_members as $member) {
					$memberPrimaryEmailAddress = $member->emailAddress->getPrimaryAddress($member);
					if ($memberPrimaryEmailAddress != false) {
						if ($this->verifyContactForNotification($company, $memberPrimaryEmailAddress))
							array_push($user_email, $memberPrimaryEmailAddress);
						}
				}
			} else {
				
			}

			
			/* End Study Director Email */

				$ComBean = BeanFactory::retrieveBean('M06_Error',$commID);
				
				/* Get Related Comm ID */
				//$RelatedCommData = $this->getRelateComID($commID);
				
				if($bean->m06_error_m06_error_1_name!="")
				{
					$RelatedCommData = '<a target="_blank" href="' . $site_url . '/#M06_Error/' . $bean->m06_error_m06_error_1m06_error_ida . '">' .$bean->m06_error_m06_error_1_name . '</a>';
				}
				else
				{
					$RelatedCommData = "NA";
				}
				$Resolved_Com = '<a target="_blank"href="' . $site_url . '/#M06_Error/' . $bean->id . '">' . $bean->name . '</a>';
				/* Get Test System Data */
				$RelatedTestSysData = $this->getRelateTestID($commID);

				/* Get Test System Name Data */
				$RelatedTestSysNameData = $this->getRelateTestSysID($commID);
				
				 //Getting submitter email
				 $submitter_email = $this->getRecordEmail("bean_id = '{$bean->employee_id}'");
				 $submitter = BeanFactory::getBean('Contacts', $bean->employee_id);
				 $submitter_c = $submitter_email[0];

				// $fields = $ComBean->fetched_row;					
				$resolution_date_c = "";
				$resolution_date_c = date("m/d/Y");											

				$id = $bean->id;
				$name = $bean->name;
				$category 		  = $category_dom[$bean->error_category_c];	
				$studyDirector    = ($studyDirectorName!="")?$studyDirectorName."(".$SDEmailAddress.")":"NA";
				$error_type_c 	  = $type_dom[$bean->error_type_c];				
				//$submitter_c 	  = $bean->submitter_c;	
				$veterinary_assessment = $bean->vet_assessment_notes_2_c;
				
				$fields = array(
								'id'=>$id,
								'name'=>$name,
								'resolved_com'=>$Resolved_Com,
								'wp_name_c'=>($wp_name_c!="")?$wp_name_c:"NA",
								'wp_name_c_list'=>($wp_name_c_list!="")?$wp_name_c_list:"NA",
								'study_director_c'=>$studyDirector,								
								'submitter_c'=>($submitter_c!="")?$submitter_c:"NA",
								'error_category_c'=>($category!="")?$category:"NA",								
								'error_type_c'=>"Resolution Notification",	
								'resolution_date_c'=>$resolution_date_c,
								'team'=>"Clinical Vet",
								'test_system_name_c'=>$RelatedTestSysData,
								'RelatedCommData'=>$RelatedCommData,
								'veterinary_assessment'=>($veterinary_assessment!="")?$veterinary_assessment:"NA",								
								'date_time_discovered_text_c'=>"NA",
								'actual_sd_assessment_date_c'=>"NA",
								'resolution_c'=>"NA",
								'error_classification_c'=>"NA",
								'subtype_c'=>"NA",							 
								'compliance_c'=>"NA",
								'test_system_name_data'=>$RelatedTestSysNameData,
								'date_error_occurred_c'=>"NA",
								'date_error_documented_c'=>"NA",								
								'EntryEmailData'=>"NA",
								'ReviewEmailData'=>"NA",
								'ManagerData'=>"NA",
								'activities'=>"NA",
								'why_it_happened_c'=>"NA",
								'details_why_happened_c'=>"NA",
								'critical_phase_inspection'=>"NA",
								'expected_event_c'=>"NA",
								'actual_event_c'=>"NA",								
								'one_up_manager'=>"NA",
								'room'=>"NA",
								'document'=>"NA",
								'equipment_facilities'=>"NA",								
								'recommendation'=>"NA",
							);
							
							
				/** Code for sending email to Study Director**/
				if (!empty($user_email)) 
				{
				
					$template = new EmailTemplate();
					$emailObj = new Email();
					/*Get Email Subject*/
					$getEmailSubject = $this->getEmailSubject($fields);

					$template->retrieve_by_string_fields(array('name' => 'Resolution Notification Template', 'type' => 'email'));
					$template->body_html = str_replace('[Resolved_Com]', $fields['resolved_com'], $template->body_html); 
					$template->body_html = str_replace('[Work_Product]', $fields['wp_name_c_list'], $template->body_html);
					$template->body_html = str_replace('[Study_Director]', $fields['study_director_c'], $template->body_html);
					$template->body_html = str_replace('[Compliance]', $fields['compliance_c'], $template->body_html);
					$template->body_html = str_replace('[Submitted_By]', $fields['submitter_c'], $template->body_html);
					$template->body_html = str_replace('[Category]', $fields['error_category_c'], $template->body_html);
					$template->body_html = str_replace('[Type]', $fields['error_type_c'], $template->body_html);
					$template->body_html = str_replace('[Date_Occurred]', $fields['date_error_occurred_c'], $template->body_html);
					$template->body_html = str_replace('[Resolution_Date]', $fields['resolution_date_c'], $template->body_html);
					$template->body_html = str_replace('[Date_Time_Discovered]', $fields['date_time_discovered_text_c'], $template->body_html);
					$template->body_html = str_replace('[date_error_documented_c]', $fields['date_error_documented_c'], $template->body_html);
					$template->body_html = str_replace('[activities]', $fields['activities'], $template->body_html);
					$template->body_html = str_replace('[Expected_Event]', $fields['expected_event_c'], $template->body_html);
					$template->body_html = str_replace('[Veterinary_Assessment]', $fields['veterinary_assessment'], $template->body_html);
					$template->body_html = str_replace('[Test_System]', $fields['test_system_name_c'], $template->body_html);
					$template->body_html = str_replace('[team]', $fields['team'], $template->body_html);
					$template->body_html = str_replace('[Responsible_Personnel_Entry]', $fields['EntryEmailData'], $template->body_html);
					$template->body_html = str_replace('[Responsible_Personnel_Review]', $fields['ReviewEmailData'], $template->body_html);
					$template->body_html = str_replace('[Department_Manager]', $fields['ManagerData'], $template->body_html);		
					$template->body_html = str_replace('[One_Up_Manager]', $fields['one_up_manager'], $template->body_html);
					$template->body_html = str_replace('[Room]', $fields['room'], $template->body_html);
					$template->body_html = str_replace('[Document]', $fields['document'], $template->body_html);
					$template->body_html = str_replace('[Equipment_Facilities]', $fields['equipment_facilities'], $template->body_html);		
					$template->body_html = str_replace('[Why_it_Happened]', $fields['why_it_happened_c'], $template->body_html);
					$template->body_html = str_replace('[Details_on_Why_it_Happened]', $fields['details_why_happened_c'], $template->body_html);
					$template->body_html = str_replace('[Critical_Phase_Inspection]', $fields['critical_phase_inspection'], $template->body_html);
					$template->body_html = str_replace('[Recommendation]', $fields['recommendation'], $template->body_html);
					$template->body_html = str_replace('[Related_COM_ID]', $fields['RelatedCommData'], $template->body_html);
					
					
					$defaults	= $emailObj->getSystemDefaultEmail();
					$mail		= new SugarPHPMailer();
					$mail->setMailerForSystem();
					$mail->IsHTML(true);
					$mail->From		= $defaults['email'];
					$mail->FromName = $defaults['name'];
					$mail->Subject	= $getEmailSubject;
					$mail->Body		= $template->body_html;
					 

					$userepdfmail = array();

					foreach ($user_email as $umail) {
						$mail->AddAddress($umail);
						$userepdfmail[] = $umail;
					}
					$userpdfemail_list  = array_unique($userepdfmail);
					$userpdfemail_list = implode(", ",$userpdfemail_list);					

					
					//If their exist some valid email addresses then send email
					if (!$mail->Send()) {
						if (!$mail->Send()) {
							
						} else {
							
						}
						unset($mail);
					}

					/*Save Document after sending email*/
					if(1==0){ /*Stop Creation of email documents for ticket #726*/ 

						$edoc_sql = "SELECT `document_name` FROM `edoc_email_documents` AS EDOC 
							WHERE EDOC.`document_name` LIKE '".$commName."%.pdf'
							AND EDOC.`type_2` = 'COM email'
							AND EDOC.`department` = 'Veterinary Services'
							AND deleted=0 ORDER BY `EDOC`.`date_modified` DESC limit 1";
			
							$edoc_exec = $db->query($edoc_sql);
							if ($edoc_exec->num_rows > 0) 
							{
								$resultEdoc = $db->fetchByAssoc($edoc_exec);
								$eDocName = $resultEdoc['document_name'];
								$eDocNameStr = str_replace(".pdf", "", $eDocName);
								$eDocNameArr = explode(" ",$eDocNameStr);
								$lastNumber = (int)trim($eDocNameArr[1]);
								$nextNumber = $lastNumber+1;
								$nextNumber  = str_pad($nextNumber,2,"0",STR_PAD_LEFT);
								$eDocNewName = $eDocNameArr[0]." ".$nextNumber.".pdf";
							}else{
								$eDocNewName = $commName." 01.pdf";
							}							
							//Create bean
							$emailDocBean = BeanFactory::newBean('EDoc_Email_Documents');
							$emailDocBean->document_name	= $eDocNewName; 
							$emailDocBean->department		= $departmentName;//'Scientific'; 
							$emailDocBean->type_2			= "COM email"; 
							$emailDocBean->assigned_user_id	= $current_user->id;
							
							$emailDocBean->filename	= $eDocNewName;
							$emailDocBean->file_ext	= 'pdf';
							$emailDocBean->file_mime_type	= 'application/pdf';	
							
							$emailDocBean->save();
							$emailDocId = $emailDocBean->id;

							if($workProductId!="" && $emailDocId!="" ){
								$gid = create_guid();//strtolower($GUID);
								$sql_msb = "INSERT INTO `m03_work_product_edoc_email_documents_1_c` (id,m03_work_product_edoc_email_documents_1m03_work_product_ida, m03_work_product_edoc_email_documents_1edoc_email_documents_idb) VALUES ('".$gid."','".$workProductId."', '".$emailDocId."')";
								$db->query($sql_msb);
							} 
												 
							$ss = new Sugar_Smarty();
							$ss->security = true;
							$ss->security_settings['PHP_TAGS'] = false;
							if (defined('SUGAR_SHADOW_PATH')) {
								$ss->secure_dir[] = SUGAR_SHADOW_PATH;
							}
							$ss->assign('MOD', $GLOBALS['mod_strings']);
							$ss->assign('APP', $GLOBALS['app_strings']);
									
							$ss->assign('fields', $fields);
							
							$arrFind =  array("<",">");
							$arrReplace =  array("&lt;","&gt;");
							$fields['resolution_c'] 			=	str_replace($arrFind,$arrReplace,$fields['resolution_c']);
							$fields['expected_event_c'] 			=	str_replace($arrFind,$arrReplace,$fields['expected_event_c']);
							$fields['actual_event_c'] 				=	str_replace($arrFind,$arrReplace,$fields['actual_event_c']);
							$fields['details_why_happened_c'] 	=	str_replace($arrFind,$arrReplace,$fields['details_why_happened_c']);
							
							$html2 = $ss->fetch('custom/modules/M06_Error/tpls/ResolutionEmailTextPDF.tpl');
							
							$top_html = "";  
							
							$offset =  -18000;
							//$offset =  -21600; //daylight Saving
							$offset_time = time() + $offset +1;
							$formattedDate = date("D m/d/Y g:i a",$offset_time);
							$top_html = '<span style="font-size:8pt;">'.$formattedDate.'</span>';
							$top_html .= '<br/><span style="font-size:8pt;">From: PMT (do_not_reply@apsemail.com)</span>';							
							$top_html .= '<br/><span style="font-size:8pt;">Subject: '.$getEmailSubject.'</span>';
							$top_html .= '<br/><span style="font-size:8pt;">To: '.$userpdfemail_list.'</span>';
							$borderHtml = '<p><span style="font-size:10pt;">----------------------------------------------------------------------------------------------------------------------------------------------------------------</span></p>';
							$body_html_new = $top_html.$borderHtml.$html2;
							
							$fileName = $eDocNewName;
							$fileDocID = $emailDocId; 
							$fileInfo = $this->create_PDF($body_html_new, $fileName);
							$objFile = new UploadFile('uploadfile');
							// Set the filename
							$objFile->file_ext = 'pdf';
							$objFile->set_for_soap($fileName, (sugar_file_get_contents($fileInfo['file_path'])));
							//$objFile->set_for_soap($fileName, $html2);
							$objFile->create_stored_filename();
							$objFile->final_move($fileDocID);
					}
							
						
						
					/*ENd Email document creation*/					
				}
			}
		}	
	
	function get_PDF_html($pdf_id)
    {
        $html = '';
          $page_break = '<div style="page-break-before:always">&nbsp;</div>';
          $is_first_page = true;
          $template = array();
          $pdf_template = new PdfManager();
          $pdf_template->retrieve($pdf_id);
          $template['id'] = $pdf_template->id;
          $template['name'] = $pdf_template->name;
          $template['type'] = $pdf_template->base_module;
          $template['html'] = $pdf_template->body_html;		 ;
          return $template;
    }
	function create_PDF($html, $name)
     {
          require_once('vendor/tcpdf/tcpdf.php');
          $tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
          $tcpdf->SetCreator(PDF_CREATOR);
          $name = str_replace(' ', '_', $name);
          $date = date('Y-m-d');
		  
		  $file_info['file_path'] = "upload/".$name;
		  $file_info['file_name'] = $name;
		  
          
          $tcpdf->AddPage();
          $tcpdf->writeHTML($html, true, false, true, false, '');
          $tcpdf->Output($file_info['file_path'], "F");
          return $file_info;
    }

	/*Get Related COmm ID*/
	function getRelateComID($commID){
		global $db;
		$related_name_list = array();
		$queryComm = "SELECT 
							COMM.id AS communicationID,
							COMM.name AS CommunicationName,
							relComm.m06_error_m06_error_1m06_error_idb AS ReletedCommID
							FROM m06_error AS COMM		
							RiGHT JOIN m06_error_m06_error_1_c AS relComm ON  relComm.m06_error_m06_error_1m06_error_ida = COMM.id
							WHERE COMM.id = '".$commID."' AND relComm.deleted=0";

		$related_comm_exec = $db->query($queryComm);
		if ($related_comm_exec->num_rows > 0)
		{
			while($resultrelatedcomm = $db->fetchByAssoc($related_comm_exec)){
				$relatedCommBean = BeanFactory::getBean('M06_Error', $resultrelatedcomm['ReletedCommID']);
				$related_name_list[] = $relatedCommBean->name;
			}						
		}
		
		return !empty($related_name_list)?implode(", ",$related_name_list):"NA";
	}

		
	/*Get Related Test Systems*/
	function getRelateTestID($commID){
		global $db;
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$related_test_sys_list = array();
		
		$test_sys_sql = "SELECT m06_error_anml_animals_1anml_animals_idb AS TESTID  FROM `m06_error_anml_animals_1_c` AS COMM_TEST
		WHERE COMM_TEST.`m06_error_anml_animals_1m06_error_ida`='".$commID."' AND deleted=0";	
		
		$related_test_sys_sql_exec = $db->query($test_sys_sql);
		if ($related_test_sys_sql_exec->num_rows > 0)
		{
			while($resultrelatedtestsys = $db->fetchByAssoc($related_test_sys_sql_exec)){
				$relatedTestsysBean = BeanFactory::getBean('ANML_Animals', $resultrelatedtestsys['TESTID']);
				$related_test_sys_list[] =  '<a target="_blank" href="' . $site_url . '/#ANML_Animals/' . $resultrelatedtestsys['TESTID'] . '">' .$relatedTestsysBean->name . '</a>';
			}						
		}
					
		return !empty($related_test_sys_list)?implode(", ",$related_test_sys_list):"NA";
	}

	/*Get Related Test Systems without link*/
	function getRelateTestSysID($commID){
		global $db;
		$related_test_sys_name_list = array();
		
		$test_sys_name_sql = "SELECT m06_error_anml_animals_1anml_animals_idb AS TESTID  FROM `m06_error_anml_animals_1_c` AS COMM_TEST
		WHERE COMM_TEST.`m06_error_anml_animals_1m06_error_ida`='".$commID."' AND deleted=0";	
		
		$related_test_sys_name_sql_exec = $db->query($test_sys_name_sql);
		if ($related_test_sys_name_sql_exec->num_rows > 0)
		{
			while($resultrelatedtestsysname = $db->fetchByAssoc($related_test_sys_name_sql_exec)){
				$relatedTestsystemBean = BeanFactory::getBean('ANML_Animals', $resultrelatedtestsysname['TESTID']);
				$related_test_sys_name_list[] =  $relatedTestsystemBean->name;
			}						
		}
					
		return !empty($related_test_sys_name_list)?implode(", ",$related_test_sys_name_list):"NA";
	}

	 /**
     * Retrieves email address form bean id from email related bean table
     * @param string $record
     * @return string email_address|null
     */
	 function getRecordEmail($where = '') {
        $email_addresses = array();
        $query = "
        SELECT
            email_addresses.email_address
        FROM
            `email_addresses`
        INNER JOIN
            `email_addr_bean_rel` ON email_addr_bean_rel.email_address_id = email_addresses.id
        WHERE email_addr_bean_rel.deleted = 0 AND email_addresses.deleted='0' AND email_addresses.invalid_email='0' AND email_addresses.opt_out='0'
        AND email_addr_bean_rel." . $where;
        $res = $GLOBALS['db']->query($query);
        if ($res->num_rows >= 1) {
            while ($row = $GLOBALS["db"]->fetchByAssoc($res)) {
                $email_address[] = $row['email_address'];
            }
            return $email_address;
        }
		return '';
    }
	/**
	* Contacts which are associated with team, return there email addresses
	* @param type $team_name
	* @return type array
	*/
	function get_related_contacts_email_addresses($team_name) {

		$contacts_email_addreses = array();
		
		$query = new SugarQuery();
		$query->from(BeanFactory::newBean('Teams'), array('alias' => 'teams'));
		$query->joinTable('team_sets_teams', array(
		'joinType' => 'INNER',
		'alias' => 'tst',
		'linkingTable' => true,
		))->on()->equalsField('teams.id', 'tst.team_id');
		$query->joinTable('contacts', array(
		'joinType' => 'INNER',
		'alias' => 'c',
		'linkingTable' => true,
		))->on()->equalsField('tst.team_set_id', 'c.team_set_id');
		$query->joinTable('email_addr_bean_rel', array(
		'joinType' => 'INNER',
		'alias' => 'eabr',
		'linkingTable' => true,
		))->on()->equalsField('c.id', 'eabr.bean_id');
		$query->joinTable('email_addresses', array(
		'joinType' => 'INNER',
		'alias' => 'ea',
		'linkingTable' => true,
		))->on()->equalsField('eabr.email_address_id', 'ea.id');
		
		$query->where()
		->equals('teams.name', $team_name)
		->equals('teams.deleted', 0)
		->equals('tst.deleted', 0)
		->equals('c.deleted', 0)
		->equals('eabr.deleted', 0);
		$query->select('ea.email_address');
		$compile_query = $query->compile();
		$result_query = $query->execute();
		
		foreach ($result_query as $key => $innerArray) {
		foreach ($innerArray as $key => $email_address) {
		$contacts_email_addreses[] = $email_address;
		}
		}
		
		return $contacts_email_addreses;
	}
	
	/**
	* Verify if contact account name is 'american preclinical services'
	* and email address domain is 'apsemail.com' otherwise return false
	* @param string $account_name
	* @param string $email
	* @return bool true|false
	*/
	function verifyContactForNotification($account_name, $email) {
		$companyToSearch = 'american preclinical services';
		$domainToSearch = 'apsemail.com';
		$domain_name = substr(strrchr($email, "@"), 1);
		if (trim(strtolower($account_name)) == $companyToSearch && strtolower($domain_name) == $domainToSearch) {
		return true;
		}
		return false;
	}

	/*Get Email Subject */
	function getEmailSubject($data) {
		
		$subject .= 'Vet Check Communication';       
				
		if(!empty($data['wp_name_c']) && $data['wp_name_c']!="NA") {
            $subject .= ' : ' . $data['wp_name_c'];
		}
		
        if(!empty($data['test_system_name_data']) && $data['test_system_name_data']!="NA") {
            $subject .= ' : ' . $data['test_system_name_data'];
        }		
		
        return $subject;
    }
	
}