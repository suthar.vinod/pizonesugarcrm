<?php

//AHTSHAM-TL - For Generate the System level Number
//Code for an auto-incrementing the �Name� field in the "Error" module?
//Naming convention will be �E18-xxxx�. This module is on the live site already.
//Since 2019 prefix has changed to "O" and in future from the year of 2020 it will "C"

class customLogicHook {

    static $already_ran = false;  // To make sure LogicHooks will execute just onces

    function generateSystemId($bean, $event, $arguments) {
        if (self::$already_ran == true)
            return;   //So that hook will only trigger once
        self::$already_ran = true;
        global $db;

        if ($bean->id !== $bean->fetched_row['id']) {
            $bean->related_text_c  = json_encode($bean->related_data);  
        }
        
        //If category = Training then set the default values of Expected Event and Actual Event
        if ($bean->error_category_c == 'Training') {
            $bean->expected_event_c = "On time training";
            $bean->actual_event_c = "Overdue training";
        }
        $bean->expected_event_c = str_replace('\\', '', $bean->expected_event_c);
        $bean->actual_event_c = str_replace('\\', '', $bean->actual_event_c);

        $date_error_occurred_c = strtotime($bean->date_error_occurred_c);
        $date_time_discovered_c = strtotime($bean->date_time_discovered_c);
        $date_error_documented_c = strtotime($bean->date_error_documented_c);
        $resolution_date_c = strtotime($bean->resolution_date_c);
		
		/*Ticket 429 : Copy DateTimeDiscovered to Date Occurred*/
		if($bean->date_time_discovered_c!="" && $bean->error_category_c=="Deceased Animal"){
			$bean->date_error_occurred_c  = $bean->date_time_discovered_c;
		}	
		
		 		
		if($bean->date_time_discovered_c!="" && ($bean->id != $bean->fetched_row['id'])){
			$arr = explode("+",$bean->date_time_discovered_c);
			$dateTimeDiscovered = $arr[0]; //date("m/d/Y H:i:s",$bean->date_time_discovered_c); //strtotime($bean->date_time_discovered_c);
			$dateTimeDiscovered_c = strtotime($dateTimeDiscovered)+18000;
			$bean->date_time_discovered_c = date("Y-m-d H:i:s",$dateTimeDiscovered_c);
		}
        
        $day = date("D", $date_error_occurred_c); //get day of "Date Error Occured" 2 digit
        if ($day == "Sat" OR $day == "Sun") {
            $bean->occured_on_weekend_c = 1;
        } else {
            $bean->occured_on_weekend_c = 0;
        }

        $year = null;
        
        
        if ($bean->error_category_c == 'Vet Check' && $bean->error_type_c == 'Resolution Notification') {
            $year = date("y", $resolution_date_c);
            $capitalYear = date("Y", $resolution_date_c);
        }else if ($bean->error_category_c == 'Vet Check' && $bean->error_type_c == 'Repeat Issue') {
            $year = date("y", $date_error_occurred_c);
            $capitalYear = date("Y", $date_error_occurred_c);
        }else{
            //When Category = Maintenance Request 
            if (($date_error_occurred_c == NULL || $date_error_occurred_c == '') && ($date_time_discovered_c == NULL || $date_time_discovered_c == '')) {
                $year = date("y", $date_error_documented_c);
                $capitalYear = date("Y", $date_error_documented_c);
            }
            //When Category = Deceased Animal
            else if ($date_error_occurred_c == NULL || $date_error_occurred_c == '') {
                $year = date("y", $date_time_discovered_c);
                $capitalYear = date("Y", $date_time_discovered_c);
            } else {
                $year = date("y", $date_error_occurred_c); //get year of "Date Error Occured" 2 digit
                $capitalYear = date("Y", $date_error_occurred_c);
            }
        }
		
		if($capitalYear==70)
			$capitalYear = date("Y");

        //$Current_year = date("Y");
        //if ($arguments['isUpdate'] == false) {
        if ($bean->name == '') {

            $query = "SELECT * FROM custom_modules_sequence WHERE module_name = 'Communications' AND sequence_year_short_name = '" . $year . "' ORDER BY id DESC LIMIT 0,1";
            $result = $db->query($query);

            if ($rows = $db->fetchByAssoc($result)) {
                $id = $rows['id'];
                $running_number = $rows['running_number'] + 1;
                $query_update = "UPDATE custom_modules_sequence SET running_number = '" . $running_number . "' WHERE id = '" . $id . "'";
                $db->query($query_update);
            } else {

                $nameCheck = "C" . $year . "%";
                $queryGetLastName = "SELECT MAX(CAST(SUBSTRING(NAME, 5, LENGTH(NAME)-4) AS UNSIGNED)) AS `name_error` FROM `m06_error` WHERE NAME LIKE '" . $nameCheck . "' and deleted = 0 ";
                $resultGetLastName = $db->query($queryGetLastName);
                if ($rowGetLastName = $db->fetchByAssoc($resultGetLastName)) {
                    $lastNumber = $rowGetLastName['name_error'];
                    $running_number = $lastNumber + 1;
                } else {
                    $running_number = 1;
                }
                $queryInsert = "INSERT INTO `custom_modules_sequence` (`module_name`, `module_short_name`, `sequence_year`, `sequence_year_short_name`, `start_number`, `lpad_string`, `sequence_number_length`, `running_number`, `active_status`) VALUES ('Communications', 'C', '" . $capitalYear . "', '" . $year . "', '1', '0', '3', '" . $running_number . "', 1)";
                $db->query($queryInsert);
            }

            if ($year < '20') {
                $number_len = 4;
            } else {
                $number_len = 5;
            }
            $running_number = str_pad(strval($running_number), $number_len, "0", STR_PAD_LEFT);

            $code = 'C' . $year . '-' . $running_number;
            $bean->name = $code;
            sleep(1);
        }

        //Save null value for target_reinspection_date_c if it does not have any value or empty/NULL
        if ($bean->target_reinspection_date_c == NULL || trim($bean->target_reinspection_date_c) == '') {
            $bean->target_reinspection_date_c = null;
        }
    }

}