({
    extendsFrom:'EditablelistbuttonField',
    initialize: function(options) {
     this._super("initialize", [options]);
    // this.model.on('change: error_category_c', this.function_error_category_c,this);
     
    },
    _loadTemplate: function() {
		this._super('_loadTemplate'); 
		var error_category_c = this.model.get('error_category_c');
        if (error_category_c=='Deceased Animal') {            
            $('input[name="date_error_occurred_c"]').prop('disabled', true);
            $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'none');
            $('[name="date_error_occurred_c"]').attr('readOnly',true);
        }else{
            $('input[name="date_error_occurred_c"]').prop('disabled', false);
            $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'unset');
            $('[name="date_error_occurred_c"]').attr('readOnly',false);
        }
    }, 
})