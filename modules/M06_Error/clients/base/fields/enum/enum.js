({
    extendsFrom: 'EnumField',
    initialize: function(opts) {
        this._super('initialize', [opts]);
    },
    /**
     * Updating catgeory dom with copy of it's dom without empty key value pair.
     * @private
     */
    _render: function() {
        var self = this;
        self._super('_render');
        if(self.name == 'error_category_c') {
          var options = self.items;
          for (var key in options) {
            if (options[key] === '' || key === '') {
              delete self.items[key];
            }
          }
        }
    },
})
