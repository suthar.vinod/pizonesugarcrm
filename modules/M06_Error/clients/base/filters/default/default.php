<?php
// created: 2022-11-08 03:22:51
$viewdefs['M06_Error']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'activities_c' => 
    array (
    ),
    'actual_event_c' => 
    array (
    ),
    'actual_equipment_assess_date_c' => 
    array (
    ),
    'actual_eq_reassess_date_c' => 
    array (
    ),
    'actual_fac_assessment_date_c' => 
    array (
    ),
    'actual_fac_reassessment_date_c' => 
    array (
    ),
    'actual_management_assessment_c' => 
    array (
    ),
    'actual_feedback_review_date_c' => 
    array (
    ),
    'actual_pi_reassessment_date_c' => 
    array (
    ),
    'actual_qa_reassessment_date_c' => 
    array (
    ),
    'actual_sd_ack_date_c' => 
    array (
    ),
    'reinspection_date_c' => 
    array (
    ),
    'vet_reassessment_date_c' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'blood_collection_c' => 
    array (
    ),
    'capa_capa_m06_error_1_name' => 
    array (
    ),
    'capa_id_c' => 
    array (
    ),
    'capa_initiation_c' => 
    array (
    ),
    'error_category_c' => 
    array (
    ),
    'error_classification_c' => 
    array (
    ),
    'condition_2_c' => 
    array (
    ),
    'completed_date_c' => 
    array (
    ),
    'rationale_for_no_ca_c' => 
    array (
    ),
    'corrective_action_1_c' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'a1a_critical_phase_inspectio_m06_error_1_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_error_documented_c' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'date_error_occurred_c' => 
    array (
    ),
    'date_time_discovered_c' => 
    array (
    ),
    'department_c' => 
    array (
    ),
    'details_why_happened_c' => 
    array (
    ),
    'diagnosis_c' => 
    array (
    ),
    'why_it_happened_c' => 
    array (
    ),
    'due_date_c' => 
    array (
    ),
    'duplicate_c' => 
    array (
    ),
    'equipment_assessment_c' => 
    array (
    ),
    'equipment_reason_for_reasses_c' => 
    array (
    ),
    'equipment_reassessment_c' => 
    array (
    ),
    'equipment_reassessment_req_c' => 
    array (
    ),
    'expected_event_c' => 
    array (
    ),
    'maintenance_request_notes_c' => 
    array (
    ),
    'fac_reason_for_reassessment_c' => 
    array (
    ),
    'fac_reassessment_notes_c' => 
    array (
    ),
    'fac_reassessment_required_c' => 
    array (
    ),
    'iacuc_deficiency_class_c' => 
    array (
    ),
    'impactful_c' => 
    array (
    ),
    'impactful_dd_c' => 
    array (
    ),
    'justification_for_no_capa_c' => 
    array (
    ),
    'mgt_assessment_c' => 
    array (
    ),
    'management_assessment_train_c' => 
    array (
    ),
    'missing_date_initials_quan_c' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'name' => 
    array (
    ),
    'description' => 
    array (
    ),
    'notify_vet_c' => 
    array (
    ),
    'occured_on_weekend_c' => 
    array (
    ),
    'organ_system_c' => 
    array (
    ),
    'notes_c' => 
    array (
    ),
    'feedback_assessment_notes_2_c' => 
    array (
    ),
    'pi_reason_for_reassessment_c' => 
    array (
    ),
    'pi_reassessment_notes_c' => 
    array (
    ),
    'pi_reassessment_required_c' => 
    array (
    ),
    'plan_comments_c' => 
    array (
    ),
    'plan_for_correction_c' => 
    array (
    ),
    'procedure_type_c' => 
    array (
    ),
    'qa_assessment_duration_c' => 
    array (
    ),
    'related_text_c' => 
    array (
    ),
    'recommendation_c' => 
    array (
    ),
    'refer_to_med_treatment_form_c' => 
    array (
    ),
    'm06_error_m06_error_1_name' => 
    array (
    ),
    'related_to_c' => 
    array (
    ),
    'repeat_deficiency_c' => 
    array (
    ),
    'resolution_date_c' => 
    array (
    ),
    'resolved_c' => 
    array (
    ),
    'resolution_c' => 
    array (
    ),
    'sd_assessment_duration_c' => 
    array (
    ),
    'sedated_c' => 
    array (
    ),
    'status_progress_notes_c' => 
    array (
    ),
    'study_director_c' => 
    array (
    ),
    'subjectiveobjective_exam_c' => 
    array (
    ),
    'submitter_c' => 
    array (
    ),
    'substance_administration_c' => 
    array (
    ),
    'subtype_c' => 
    array (
    ),
    'target_equipment_assess_date_c' => 
    array (
    ),
    'target_eq_reassess_date_c' => 
    array (
    ),
    'target_fac_assessment_date_c' => 
    array (
    ),
    'target_fac_reassessment_date_c' => 
    array (
    ),
    'target_management_assessment_c' => 
    array (
    ),
    'target_pi_assessment_date_c' => 
    array (
    ),
    'target_pi_reassessment_date_c' => 
    array (
    ),
    'target_qa_reassessment_date_c' => 
    array (
    ),
    'target_sd_ack_date_c' => 
    array (
    ),
    'target_reinspection_date_c' => 
    array (
    ),
    'target_vet_assessment_date_c' => 
    array (
    ),
    'target_vet_reassessment_date_c' => 
    array (
    ),
    'test_article_exposure_c' => 
    array (
    ),
    'test_article_exposure_detail_c' => 
    array (
    ),
    'test_system_involved_c' => 
    array (
    ),
    'time_to_discovery_days_c' => 
    array (
    ),
    'tpr_reference_c' => 
    array (
    ),
    'error_type_c' => 
    array (
    ),
    'vet_assessment_notes_2_c' => 
    array (
    ),
    'vet_assessment_date_c' => 
    array (
    ),
    'vet_reassessment_notes_c' => 
    array (
    ),
    'vet_reassessment_required_c' => 
    array (
    ),
    'vet_reason_for_reassessment_c' => 
    array (
    ),
    'w_weight_m06_error_2_name' => 
    array (
    ),
  ),
);