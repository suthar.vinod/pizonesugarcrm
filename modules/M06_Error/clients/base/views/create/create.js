({
    extendsFrom: 'CreateView',

    initialize: function (options) {
        this._super('initialize', [options]);
        this.model.addValidationTask('check_subtype_c', _.bind(this._doValidateMissingDate, this));
        /*16 July 2020 : Ticket 354 : Copy Resolution Date to Date Occurred field in one case*/
        this.model.on("change:resolution_date_c", this.function_get_resolution_date_c, this);
        this.model.on("change:error_type_c", this.function_set_date_error_occurred_c_on_error_type, this);
		 this.model.on("change:date_time_discovered_c", this.function_set_date_error_occurred, this);
        this.model.on("change:error_category_c", this.function_disable_date_error_occurred, this);
    },

    //429 : Copy Date discovered in Date occurred Field
    function_disable_date_error_occurred: function () {
        var error_category_c = this.model.get('error_category_c');
        if (error_category_c=='Deceased Animal') {  
            $('input[name="date_error_occurred_c"]').prop('disabled', true);
            $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'none');
        }else{
            $('input[name="date_error_occurred_c"]').prop('disabled', false);
            $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'unset');
        }
        
    },

	function_set_date_error_occurred: function () {
			var error_category_c = this.model.get('error_category_c');
			if (error_category_c=='Deceased Animal') {
				var discoverDate = this.model.get('date_time_discovered_c');
                this.model.set('date_error_occurred_c',discoverDate);
                $('input[name="date_error_occurred_c"]').prop('disabled', true);
                $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'none');
			}else{
                $('input[name="date_error_occurred_c"]').prop('disabled', false);
                $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'unset');
            }
	},

 
		
		
    /*16 July 2020 : Ticket 354 : Copy Resolution Date to Date Occurred field in one case*/
   function_get_resolution_date_c: function () {
    var resolution_date_c = this.model.get('resolution_date_c');
    var error_category_c = this.model.get('error_category_c');
    var error_type_c = this.model.get('error_type_c');
    if (resolution_date_c != '' && (error_category_c !='' && error_category_c=='Vet Check' && error_type_c!='' && error_type_c=='Resolution Notification')) {
        this.model.set('date_error_occurred_c',resolution_date_c);
        $('input[name="date_error_occurred_c"]').prop('disabled', true);
    }
    else
    {
        $('input[name="date_error_occurred_c"]').prop('disabled', false);
    }
},

function_set_date_error_occurred_c_on_error_type: function () {
    var error_type_c = this.model.get('error_type_c');
    var error_category_c = this.model.get('error_category_c');
    if (error_category_c=='Vet Check' && error_type_c=='Resolution Notification') {
        $('input[name="date_error_occurred_c"]').prop('disabled', true);
    }
    else
    {
        //this.model.set('date_error_occurred_c','');
        //$("input[name='date_error_occurred_c']").val("");
        $('input[name="date_error_occurred_c"]').prop('disabled', false);
    }
    
},
 /*End of code :16 July 2020 : Ticket 354 : Copy Resolution Date to Date Occurred field in one case*/

    _doValidateMissingDate: function (fields, errors, callback) {
        //validate type requirements
        var value = this.model.get('missing_date_initials_quan_c');
        if (_.contains(this.model.get('subtype_c'), "Documentation Error") && (typeof value === "undefined" || value.length == 0))
        {
            errors['missing_date_initials_quan_c'] = errors['missing_date_initials_quan_c'] || {};
            errors['missing_date_initials_quan_c'].required = true;
        }

        callback(null, fields, errors);
    },
    /**
     * Make the Missing Data Initials Quantity visible and required based on subtype_c
     * if subtype_c contains Documentation (Initials/Date)
     */
    _render: function () {
        var self = this;
        self.model.on("change", function () {
			if (_.contains(self.model.get('subtype_c'), "Documentation Error")) {
                self.$('span[data-fieldname="missing_date_initials_quan_c"]').show();
                self.$('.record-label[data-name="missing_date_initials_quan_c"]').show();
				self.$('div[data-name="missing_date_initials_quan_c"]').show();
            } else {
                self.$('span[data-fieldname="missing_date_initials_quan_c"]').hide();
                self.$('.record-label[data-name="missing_date_initials_quan_c"]').hide();
				self.$('div[data-name="missing_date_initials_quan_c"]').hide();
            }

        })

        self._super('_render');
    }
})


 