<?php
// created: 2022-03-03 07:21:41
$viewdefs['M06_Error']['base']['view']['subpanel-for-a1a_critical_phase_inspectio-a1a_critical_phase_inspectio_m06_error_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'expected_event_c',
          'label' => 'LBL_EXPECTED_EVENT',
          'enabled' => true,
          'readonly' => false,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'actual_event_c',
          'label' => 'LBL_ACTUAL_EVENT',
          'enabled' => true,
          'readonly' => false,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'error_classification_c',
          'label' => 'LBL_ERROR_CLASSIFICATION',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'error_type_c',
          'label' => 'LBL_ERROR_TYPE',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        5 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);