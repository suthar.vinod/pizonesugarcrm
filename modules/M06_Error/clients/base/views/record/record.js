({
    extendsFrom: 'RecordView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.setFieldDateTimeDsicovered, this);
        //this.model.addValidationTask('check_subtype_c', _.bind(this._doValidateMissingDate, this));
        /*16 July 2020 : Ticket 354 : Copy Resolution Date to Date Occurred field in one case*/
        this.model.on("change:resolution_date_c", this.function_get_resolution_date_c, this);
        this.model.on("change:error_type_c", this.function_set_date_error_occurred_c_on_error_type, this);
        this.model.on("change:date_time_discovered_c", this.function_set_date_error_occurred, this);
    },

    /*1334_M06_Error: Auto-create COM upon Target Vet Reassessment Date*/
    handleSave: function () {
        var vet_assessment_notes_2_c = this.model.get('vet_assessment_notes_2_c');
        var target_vet_reassessment_date_c = this.model.get('target_vet_reassessment_date_c');
        var moduleId = this.model.get('id');
        var self = this;

        if (target_vet_reassessment_date_c != '') {
            console.log('1');
            var url = app.api.buildURL("autocreateCommTVRD");
            var method = "create";
            var data = {
                module: 'M06_Error',
                recordId: moduleId,
                target_vet_reassessment_date_c: target_vet_reassessment_date_c,
                vet_assessment_notes_2_c: vet_assessment_notes_2_c
            };

            var callback = {
                success: _.bind(function successCB(res) {
                    console.log(res);
                }, this)
            };
            app.api.call(method, url, data, callback);

            setTimeout(function () {
                self._super('handleSave');
            }, 500);
        } else {
            console.log('2');
            this._super('handleSave');
        }

    },

    //429 : Copy Date discovered in Date occurred Field
    function_set_date_error_occurred: function () {
        var error_category_c = this.model.get('error_category_c');
        if (error_category_c == 'Deceased Animal') {
            var discoverDate = this.model.get('date_time_discovered_c');
            this.model.set('date_error_occurred_c', discoverDate);
            $('input[name="date_error_occurred_c"]').prop('disabled', true);
        } else {
            $('input[name="date_error_occurred_c"]').prop('disabled', false);
        }
    },
    /*16 July 2020 : Ticket 354 : Copy Resolution Date to Date Occurred field in one case*/
    function_get_resolution_date_c: function () {
        var resolution_date_c = this.model.get('resolution_date_c');
        var error_category_c = this.model.get('error_category_c');
        var error_type_c = this.model.get('error_type_c');
        if (resolution_date_c != '' && (error_category_c != '' && error_category_c == 'Vet Check' && error_type_c != '' && error_type_c == 'Resolution Notification')) {
            this.model.set('date_error_occurred_c', resolution_date_c);
            $('input[name="date_error_occurred_c"]').prop('disabled', true);
            $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'none');
        }
        else {
            $('input[name="date_error_occurred_c"]').prop('disabled', false);
            $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'unset');
        }
    },

    function_set_date_error_occurred_c_on_error_type: function () {
        var error_type_c = this.model.get('error_type_c');
        var error_category_c = this.model.get('error_category_c');
        if (error_category_c == 'Vet Check' && error_type_c == 'Resolution Notification') {
            $('input[name="date_error_occurred_c"]').prop('disabled', true);
            $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'none');
        }
        else {
            //this.model.set('date_error_occurred_c','');
            //$("input[name='date_error_occurred_c']").val("");
            $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'unset');
            $('input[name="date_error_occurred_c"]').prop('disabled', false);
        }

    },
    /*End of code :16 July 2020 : Ticket 354 : Copy Resolution Date to Date Occurred field in one case*/
    // _doValidateMissingDate: function (fields, errors, callback) {
    //     //validate type requirements
    //     var value = this.model.get('missing_date_initials_quan_c');
    //     if (_.contains(this.model.get('subtype_c'), "Documentation Error") && (typeof value === "undefined" || value == null))
    //     {
    //         errors['missing_date_initials_quan_c'] = errors['missing_date_initials_quan_c'] || {};
    //         //errors['missing_date_initials_quan_c'].required = true;
    //     }

    //     callback(null, fields, errors);
    // },
    /**
     * Check if record is submitted from wordpress and have value for date_time_discovered
     * then display text field with actual database date instead of datetime picker field.
     * This will avoid time zone differences for different users in CRM when record is
     * pushed from Wordpress app.
     */
    setFieldDateTimeDsicovered: function () {
        var self = this;
        var date_time_discovered = self.model.get('date_time_discovered_c');
        var wp_flag = self.model.get('wordpress_flag');
        /*if (!_.isUndefined(wp_flag) && wp_flag == true && !_.isEmpty(date_time_discovered)) {
            _.each(self.meta.panels, function (panel, i) {
                _.each(panel.fields, function (field, j) {
                    if (field.name == 'date_time_discovered_c') {
                        self.meta.panels[i].fields[j].type = 'text';
                        self.meta.panels[i].fields[j].name = 'date_time_discovered_text_c';
                    }
                }, self);
            }, self);
            self.render();
        }*/


        if (_.contains(self.model.get('subtype_c'), "Documentation Error")) {
            self.$('span[data-fieldname="missing_date_initials_quan_c"]').show();
            self.$('.record-label[data-name="missing_date_initials_quan_c"]').show();
            self.$('div[data-name="missing_date_initials_quan_c"]').show();
        }
        if (_.contains(self.model.get('subtype_c'), "Documentation Error") === false) {
            self.$('span[data-fieldname="missing_date_initials_quan_c"]').hide();
            self.$('.record-label[data-name="missing_date_initials_quan_c"]').hide();
            self.$('div[data-name="missing_date_initials_quan_c"]').hide();
        }
        /*16 July 2020 : Ticket 354 : Copy Resolution Date to Date Occurred field in one case*/
        var resolution_date_c = this.model.get('resolution_date_c');
        var error_category_c = this.model.get('error_category_c');
        var error_type_c = this.model.get('error_type_c');
        if (error_category_c == 'Vet Check' && error_type_c == 'Resolution Notification') {
            $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'none');
        }
        else {
            $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'unset');
        }
        /*End of code 16 July 2020 : Ticket 354 : Copy Resolution Date to Date Occurred field in one case*/
    },
    /**
     * Hide empty type or sub type fields if category is also empty.
     */
    _render: function () {
        var self = this;
        var displayType = true;
        var displaySubType = true;
        self.model.on("change", function () {

            // 429 : make readonly date Occurred when Category is Deceased Animal
            var error_category_c = self.model.get('error_category_c');
            if (error_category_c == 'Deceased Animal') {
                var discoverDate = self.model.get('date_time_discovered_c');
                self.model.set('date_error_occurred_c', discoverDate);
                $('input[name="date_error_occurred_c"]').prop('disabled', true);
                $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'none');
            } else {
                $('input[name="date_error_occurred_c"]').prop('disabled', false);
                $('div[data-name="date_error_occurred_c"]').css('pointer-events', 'unset');
            }
            //end

            if (_.contains(self.model.get('subtype_c'), "Documentation Error")) {
                self.$('span[data-fieldname="missing_date_initials_quan_c"]').show();
                self.$('.record-label[data-name="missing_date_initials_quan_c"]').show();
                self.$('div[data-name="missing_date_initials_quan_c"]').show();
            }
            if (_.contains(self.model.get('subtype_c'), "Documentation Error") === false) {
                self.$('span[data-fieldname="missing_date_initials_quan_c"]').hide();
                self.$('.record-label[data-name="missing_date_initials_quan_c"]').hide();
                self.$('div[data-name="missing_date_initials_quan_c"]').hide();
            }

            if (self.model.get('error_category_c') == '') {
                if (self.model.get('error_type_c') == '') {
                    self.$('span[data-fieldname="error_type_c"]').hide();
                    self.$('.record-label[data-name="error_type_c"]').hide();
                    displayType = false;
                }
                if (self.model.get('subtype_c') == '') {
                    self.$('span[data-fieldname="subtype_c"]').hide();
                    self.$('.record-label[data-name="subtype_c"]').hide();
                    displaySubType = false;
                }
            } else {
                if (!displayType) {
                    self.$('span[data-fieldname="error_type_c"]').show();
                    self.$('.record-label[data-name="error_type_c"]').show();
                    displayType = true;
                }
                if (!displaySubType) {
                    self.$('span[data-fieldname="subtype_c"]').show();
                    self.$('.record-label[data-name="subtype_c"]').show();
                    displaySubType = true;
                }
            }
        })

        self._super('_render');
    }
})