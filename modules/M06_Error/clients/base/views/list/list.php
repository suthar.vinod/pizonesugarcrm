<?php
$module_name = 'M06_Error';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'date_error_occurred_c',
                'label' => 'LBL_DATE_ERROR_OCCURRED',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'error_category_c',
                'label' => 'LBL_ERROR_CATEGORY',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'error_type_c',
                'label' => 'LBL_ERROR_TYPE',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'subtype_c',
                'label' => 'LBL_SUBTYPE',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'date_error_documented_c',
                'label' => 'LBL_DATE_ERROR_DOCUMENTED',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'submitter_c',
                'label' => 'LBL_SUBMITTER',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'error_classification_c',
                'label' => 'LBL_ERROR_CLASSIFICATION',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'actual_event_c',
                'label' => 'LBL_ACTUAL_EVENT',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
                'readonly' => true,
              ),
              9 => 
              array (
                'name' => 'expected_event_c',
                'label' => 'LBL_EXPECTED_EVENT',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
                'readonly' => true,
              ),
              10 => 
              array (
                'name' => 'impactful_dd_c',
                'label' => 'LBL_IMPACTFUL_DD',
                'enabled' => true,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'study_director_c',
                'label' => 'LBL_STUDY_DIRECTOR',
                'enabled' => true,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
