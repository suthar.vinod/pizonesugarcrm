<?php
// created: 2021-09-14 09:25:35
$viewdefs['M06_Error']['base']['view']['subpanel-for-w_weight-w_weight_m06_error_2'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'submitter_c',
          'label' => 'LBL_SUBMITTER',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'error_category_c',
          'label' => 'LBL_ERROR_CATEGORY',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'error_type_c',
          'label' => 'LBL_ERROR_TYPE',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'expected_event_c',
          'label' => 'LBL_EXPECTED_EVENT',
          'enabled' => true,
          'readonly' => false,
          'sortable' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'actual_event_c',
          'label' => 'LBL_ACTUAL_EVENT',
          'enabled' => true,
          'readonly' => false,
          'sortable' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'date_error_occurred_c',
          'label' => 'LBL_DATE_ERROR_OCCURRED',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);