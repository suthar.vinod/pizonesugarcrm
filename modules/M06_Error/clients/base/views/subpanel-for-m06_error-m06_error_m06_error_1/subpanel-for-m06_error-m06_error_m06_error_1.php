<?php
// created: 2020-07-07 08:32:31
$viewdefs['M06_Error']['base']['view']['subpanel-for-m06_error-m06_error_m06_error_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'error_type_c',
          'label' => 'LBL_ERROR_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'resolution_date_c',
          'label' => 'LBL_RESOLUTION_DATE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'date_error_occurred_c',
          'label' => 'LBL_DATE_ERROR_OCCURRED',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'actual_event_c',
          'label' => 'LBL_ACTUAL_EVENT',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);