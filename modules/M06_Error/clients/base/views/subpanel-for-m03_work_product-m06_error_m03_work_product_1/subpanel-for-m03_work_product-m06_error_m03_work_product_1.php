<?php
// created: 2021-09-07 18:34:40
$viewdefs['M06_Error']['base']['view']['subpanel-for-m03_work_product-m06_error_m03_work_product_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'date_error_occurred_c',
          'label' => 'LBL_DATE_ERROR_OCCURRED',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'date_error_documented_c',
          'label' => 'LBL_DATE_ERROR_DOCUMENTED',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'error_category_c',
          'label' => 'LBL_ERROR_CATEGORY',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'error_type_c',
          'label' => 'LBL_ERROR_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'error_classification_c',
          'label' => 'LBL_ERROR_CLASSIFICATION',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'actual_event_c',
          'label' => 'LBL_ACTUAL_EVENT',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);