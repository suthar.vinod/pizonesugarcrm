<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class autocreateCommOnChangeTVRDApi extends SugarApi {

    public function registerApiRest() {
        return array(
            'autocreateCommTVRD' => array(
                'reqType' => 'POST',
                'noLoginRequired' => false,
                'path' => array('autocreateCommTVRD'),
                'pathVars' => array('autocreateCommTVRD'),
                'method' => 'autocreateCommTVRD',
                'shortHelp' => '',
                'longHelp' => '',
            ),
        );
    }

    
    function autocreateCommTVRD($api, $args) {
		global $db,$current_user;
		 
		$module = $args['module'];
		$recordId = $args['recordId'];
		$target_vet_reassessment_date_c = $args['target_vet_reassessment_date_c'];
		$vet_assessment_notes_2_c = $args['vet_assessment_notes_2_c'];
		$bean = BeanFactory::retrieveBean($module, $recordId);
		
		
        if($target_vet_reassessment_date_c != "" && ($bean->target_vet_reassessment_date_c != $target_vet_reassessment_date_c)) {
 
			$bean->load_relationship("m06_error_anml_animals_1");
			$TSIDS = $bean->m06_error_anml_animals_1->get();
			
			$bean->load_relationship("m06_error_m03_work_product_1");
			$WPIDS = $bean->m06_error_m03_work_product_1->get();
			
			$NewCommBean = BeanFactory::newBean('M06_Error');
			$new_bean_id = create_guid();
			
			$NewCommBean->id = $new_bean_id;
			$NewCommBean->new_with_id = true;
			$NewCommBean->fetched_row = null;
			
			$NewCommBean->name             = $this->generateSystemId($bean);
			$NewCommBean->submitter_c      = $current_user->name;;
			$NewCommBean->error_category_c = "Vet Check";
			$NewCommBean->error_type_c     = "Repeat Issue";
			$NewCommBean->date_error_occurred_c = $target_vet_reassessment_date_c;
			$NewCommBean->actual_event_c        = 'Vet Reassessment - '.$vet_assessment_notes_2_c;
			$NewCommBean->m06_error_m06_error_1m06_error_ida = $bean->id;
			$NewCommBean->m06_error_m06_error_1_name         = $bean->name;
						 
			if(!empty($TSIDS)){
				$NewCommBean->test_system_involved_c = 'Yes';
			}else{
				$NewCommBean->test_system_involved_c = 'No';
			}

			$NewCommBean->employee_id    = $current_user->id;
			
			$NewCommBean->wordpress_flag = 1;
			$NewCommBean->related_data   = array(
											"Erd_Error_Documents" => "",
											"ANML_Animals" => array($TSIDS[0]),
											"Equip_Equipment" => "",
											"RMS_Room" => "",
											"A1A_Critical_Phase_Inspectio" => "",
											"Teams" => "",
											"M06_Error" => array($bean->id),
										); 
			$NewCommBean->work_products  = '['.$WPIDS[0].']';
			$NewCommBean->save();
			
			if (!empty($TSIDS)) {
				foreach ($TSIDS as $TSID) {
					//$NewCommBean->m06_error_anml_animals_1->add($TSID);
					$relationid = create_guid();
					$tssql = 'INSERT INTO m06_error_anml_animals_1_c  (id,date_modified,deleted,m06_error_anml_animals_1m06_error_ida,m06_error_anml_animals_1anml_animals_idb) 
						values("' . $relationid . '",now(),0,"' . $NewCommBean->id . '","' . $TSID . '")';
						
					$db->query($tssql);
				}
			}

			if (!empty($WPIDS)) {
				foreach ($WPIDS as $WPID) {
					//$NewCommBean->m06_error_m03_work_product_1->add($WPID);
					$relationid1 = create_guid();
					$wpsql = 'INSERT INTO m06_error_m03_work_product_1_c  (id,date_modified,deleted,m06_error_m03_work_product_1m06_error_ida,m06_error_m03_work_product_1m03_work_product_idb) 
						values("' . $relationid1 . '",now(),0,"' . $NewCommBean->id . '","' . $WPID . '")';
						
					$db->query($wpsql);
				}
			} 
			return 1;
			
        }else{
			return 0;
		}
    }
	
	function generateSystemId($bean) {
        
        global $db; 
         
		 
        $year = date("y");
        $capitalYear = date("Y");
		
		if($capitalYear==70){
			$year = date("y");
			$capitalYear = date("Y");
		}

		$query = "SELECT * FROM custom_modules_sequence WHERE module_name = 'Communications' AND sequence_year_short_name = '" . $year . "' ORDER BY id DESC LIMIT 0,1";
		$result = $db->query($query);

		if ($rows = $db->fetchByAssoc($result)) {
			$id = $rows['id'];
			$running_number = $rows['running_number'] + 1;
			$query_update = "UPDATE custom_modules_sequence SET running_number = '" . $running_number . "' WHERE id = '" . $id . "'";
			$db->query($query_update);
		} else {

			$nameCheck = "C" . $year . "%";
			$queryGetLastName = "SELECT MAX(CAST(SUBSTRING(NAME, 5, LENGTH(NAME)-4) AS UNSIGNED)) AS `name_error` FROM `m06_error` WHERE NAME LIKE '" . $nameCheck . "' and deleted = 0 ";
			$resultGetLastName = $db->query($queryGetLastName);
			if ($rowGetLastName = $db->fetchByAssoc($resultGetLastName)) {
				$lastNumber = $rowGetLastName['name_error'];
				$running_number = $lastNumber + 1;
			} else {
				$running_number = 1;
			}
			$queryInsert = "INSERT INTO `custom_modules_sequence` (`module_name`, `module_short_name`, `sequence_year`, `sequence_year_short_name`, `start_number`, `lpad_string`, `sequence_number_length`, `running_number`, `active_status`) VALUES ('Communications', 'C', '" . $capitalYear . "', '" . $year . "', '1', '0', '3', '" . $running_number . "', 1)";
			$db->query($queryInsert);
		}

		if ($year < '20') {
			$number_len = 4;
		} else {
			$number_len = 5;
		}
		$running_number = str_pad(strval($running_number), $number_len, "0", STR_PAD_LEFT);

		$code = 'C' . $year . '-' . $running_number;
		
		return $code;
    }


}

