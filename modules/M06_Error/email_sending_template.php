<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once("modules/Emails/Email.php");
require_once('modules/EmailTemplates/EmailTemplate.php');
require_once 'custom/include/Helpers/sendEmailTemplates.php';

class email_sending_template
{

    /**
     * Initiates email notification process for APS wordpress application upon data submission.
     * @param object $bean
     * @param string $event
     * @param array $arguments
     */
    function after_relationship_add_email_sending_template($bean, $event, $arguments)
    {
        global $current_user;
        global $timedate;
        global $db;
        global $app_list_strings;
        if ($bean->id !== $bean->fetched_row['id']) {

            $beanrelated_data = $bean->related_data;    
            $bean->retrieve($bean->id);
            $bean->related_data = $beanrelated_data;
            
            if ($bean->wordpress_flag == 1) {

                $sendEmailTemplates		= new sendEmailTemplates();
                $template_data			= array();
                $user_email				= array(); //Selected Responsible Personal and related Team users emails
                $wp_email				= array(); //Work product assigned user emails
                $company				= 'american preclinical services';
                $oneupmanager_email		= array();
                $team_name				= '';
                $team1_name				= '';
                $team2_name				= '';
                //Section for both error types (Facility and Sttudy)
                //Observation Employees......
                //Identifying entry and review emails
                $entryEmails			= array(); //Emails of selected responsible personal (Entry)
                $reviewEmails			= array(); //Emails of selected responsible personal (Review)
                $e_array				= array(); //temporary array to store raw data of responsible personal (Entry)
                $r_array				= array(); //temporary array to store raw data of responsible personal (Review)
                $contact_dept			= array(); //Names of selected responsible personal departments
                $managers				= array(); //Manager id's of selected responsible personals
                $oneupmanagers			= array(); //One Up Manager id's of selected responsible personals
                $manager_name_list		= array(); //Manager name's of selected responsible personals
                $oneupmanager_name_list = array(); //One Up Manager name's of selected responsible personals
                $manager_dept			= array(); //Department names of manager's
                $wproducts				= array();
                $workProductID			= "";
                $study_director			= array();
                $study_coordinator		= array();
                $activitiesDisplayArr	= array();
                $type_dom             	= $app_list_strings['error_category_list'];
                $category_dom         	= $app_list_strings['category_list'];
                $classification_dom 	= $app_list_strings['error_classification_c_list'];
                $activities_dom     	= $app_list_strings['wps_outcome_activities_list'];
                $whyHappened_dom     	= $app_list_strings['why_it_happened_c_list'];

                //Decode the vet observation data
                $vet_observation_data = trim($bean->vet_observation_data_c, '\[]"');
                $vet_observation_data = explode(",", $vet_observation_data);


                // Populate date_time_discovered_c into field date_time_discovered_text_c to avoid time zone offsets.
                if (!empty($bean->date_time_discovered_c)) {
                    $this->updateTableData('date_time_discovered_text_c', date("m/d/Y H:i:s", strtotime($bean->date_time_discovered_c)), $bean->id);
                }

                // $GLOBALS['log']->debug('Observation submitted from APS wordpress application.');            

                //Set vet_observation_data in template so that it can show in email notifications
                $template_data['temparature'] = $vet_observation_data[0];
                $template_data['heart_rate'] = $vet_observation_data[1];
                $template_data['respiratory_rate'] = $vet_observation_data[2];

                //Preparing list of Work Product assigned users.
                $configuratorObj = new Configurator();
                $configuratorObj->loadConfig();
                //$wpCount = $configuratorObj->config['wps'];
                $wpCount = 0;
                if ($bean->error_type_c == 'Sttudy') {
                    if ($bean->work_products != '[]' && $bean->work_products != 'null') {
                        $work_products = $this->jsonDecode($bean->work_products);
                        $wp_id = $work_products[$wpCount];

                        $strQuery4 = "
                               SELECT
                                   users.id
                               FROM
                                   `users`
                               INNER JOIN
                                   `m03_work_product` ON m03_work_product.assigned_user_id = users.id
                               WHERE users.deleted = 0 AND m03_work_product.id IN ('{$wp_id}')";
                        $res4 = $GLOBALS['db']->query($strQuery4);
                        $wproduct_users = array();
                        while ($user = $GLOBALS["db"]->fetchByAssoc($res4)) {
                            $wproduct_users[] = $user['id'];
                        }
                        if (!empty($wproduct_users)) {
                            $user_id = implode("', '", $wproduct_users);
                            $emails = $this->getRecordEmail("bean_id IN ('{$user_id}')");
                            if (!empty($emails)) {
                                foreach ($emails as $email) {
                                    if ($this->verifyContactForNotification($company, $email))
                                        array_push($wp_email, $email);
                                }
                            } else {
                                $GLOBALS['log']->debug('No email found against work product user.');
                            }
                        }
                    }
                }
                //For Vet Check Category
                if ($bean->error_category_c == 'Vet Check') {

                    $team_name = 'Clinical Vet';

                    //get the email addresses of team members
                    $email_addresses = $this->get_related_teams($team_name);
                    if (!empty($email_addresses)) {
                        foreach ($email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email))
                                array_push($user_email, $email);
                        }
                    } else {
                        $GLOBALS['log']->debug('No email found for Vet Check Team.');
                    }

                    //get the email addresses of contacts associated with teams
                    $con_email_addresses = $this->get_related_contacts_email_addresses($team_name);
                    if (!empty($con_email_addresses)) {
                        foreach ($con_email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    }
                } else if ($bean->error_category_c == 'Gross Pathology') {

                    $team1_name = 'Pathologist';
                    $team2_name = 'Clinical Vet';

                    //get the email addresses of Pathologist team members
                    $email_addresses = $this->get_related_teams($team1_name);
                    if (!empty($email_addresses)) {
                        foreach ($email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email))
                                array_push($user_email, $email);
                        }
                    } else {
                        $GLOBALS['log']->debug('No email found for Pathologist Team.');
                    }

                    //get the email addresses of Clinical Vet team members
                    $email_addresses = $this->get_related_teams($team2_name);
                    if (!empty($email_addresses)) {
                        foreach ($email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email))
                                array_push($user_email, $email);
                        }
                    } else {
                        $GLOBALS['log']->debug('No email found for Pathologist Team.');
                    }

                    //get the email addresses of contacts associated with Pathologist team
                    $con_email_addresses = $this->get_related_contacts_email_addresses($team1_name);
                    if (!empty($con_email_addresses)) {
                        foreach ($con_email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    }

                    //get the email addresses of contacts associated with Clinical Vet team
                    $con_email_addresses = $this->get_related_contacts_email_addresses($team2_name);
                    if (!empty($con_email_addresses)) {
                        foreach ($con_email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    }
                } else if ($bean->error_category_c == 'Maintenance Request') {

                    $team_name = 'Facilities';


                    //get the email addresses of team members
                    $email_addresses = $this->get_related_teams($team_name);
                    if (!empty($email_addresses)) {
                        foreach ($email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    } else {
                        $GLOBALS['log']->debug('No email found for Maintenance Request Team.');
                    }

                    //get the email addresses of contacts associated with teams
                    $con_email_addresses = $this->get_related_contacts_email_addresses($team_name);
                    if (!empty($con_email_addresses)) {
                        foreach ($con_email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    }
                } else if ($bean->error_category_c == 'Test Failure') { //If Category = 'Test Failure' then send email to "Failed Study"
                    // team members and contacts
                    $team_name = 'Failed Study';

                    //get the email addresses of team members
                    $email_addresses = $this->get_related_teams($team_name);
                    if (!empty($email_addresses)) {
                        foreach ($email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    } else {
                        $GLOBALS['log']->debug('No email found for Maintenance Request Team.');
                    }

                    //get the email addresses of contacts associated with teams
                    $con_email_addresses = $this->get_related_contacts_email_addresses($team_name);
                    if (!empty($con_email_addresses)) {
                        foreach ($con_email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    }
                } else if ($bean->error_category_c == 'Equipment Maintenance Request') {

                    $team_name = 'Equipment';


                    //get the email addresses of team members
                    $email_addresses = $this->get_related_teams($team_name);
                    if (!empty($email_addresses)) {
                        foreach ($email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    } else {
                        $GLOBALS['log']->debug('No email found for Maintenance Request Team.');
                    }

                    //get the email addresses of contacts associated with teams
                    $con_email_addresses = $this->get_related_contacts_email_addresses($team_name);
                    if (!empty($con_email_addresses)) {
                        foreach ($con_email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    }
                } else if ($bean->error_category_c == 'IACUC Deficiency') { //when category is 'IACUC Deficiency'
                    //then send email to IACUC Committee
                    $team_name = 'IACUC Committee';

                    //get the email addresses of team members
                    $email_addresses = $this->get_related_teams($team_name);
                    if (!empty($email_addresses)) {
                        foreach ($email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    } else {
                        $GLOBALS['log']->debug('No email address found for IACUC Committee Team Members.');
                    }

                    //get the email addresses of contacts associated with teams
                    $con_email_addresses = $this->get_related_contacts_email_addresses($team_name);
                    if (!empty($con_email_addresses)) {
                        foreach ($con_email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    }
                } else if ($bean->error_category_c == 'Rejected Animal') {
                    //|| $bean->error_category_c == 'Internal Feedback'
                    $team_name = $bean->error_category_c;

                    //get the email addresses of team members
                    $email_addresses = $this->get_related_teams($team_name);
                    if (!empty($email_addresses)) {
                        foreach ($email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email))
                                array_push($user_email, $email);
                        }
                    } else {
                        $GLOBALS['log']->debug('No email found for Rejected Animal Team.');
                    }

                    //get the email addresses of contacts associated with teams
                    $con_email_addresses = $this->get_related_contacts_email_addresses($team_name);
                    if (!empty($con_email_addresses)) {
                        foreach ($con_email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    }
                } else if ($bean->error_category_c == 'Deceased Animal') {

                    $team_name = 'Deceased Animal Group';

                    //get the email addresses of team members
                    $email_addresses = $this->get_related_teams($team_name);
                    if (!empty($email_addresses)) {
                        foreach ($email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email))
                                array_push($user_email, $email);
                        }
                    } else {
                        $GLOBALS['log']->debug('No email found for Deased Animal Team.');
                    }

                    //get the email addresses of contacts associated with teams
                    $con_email_addresses = $this->get_related_contacts_email_addresses($team_name);
                    if (!empty($con_email_addresses)) {
                        foreach ($con_email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    }
                    if ($bean->work_products != '[]' && $bean->work_products != 'null') {
                        $work_products = $this->jsonDecode($bean->work_products);
                        $wp_id = $work_products[$wpCount];
                        $strQuery4 = "
                               SELECT
                                   users.id
                               FROM
                                   `users`
                               INNER JOIN
                                   `m03_work_product` ON m03_work_product.assigned_user_id = users.id
                               WHERE users.deleted = 0 AND m03_work_product.id IN ('{$wp_id}')";
                        $res4 = $GLOBALS['db']->query($strQuery4);
                        $wproduct_users = array();
                        while ($user = $GLOBALS["db"]->fetchByAssoc($res4)) {
                            $wproduct_users[] = $user['id'];
                        }
                        if (!empty($wproduct_users)) {
                            $user_id = implode("', '", $wproduct_users);
                            $emails = $this->getRecordEmail("bean_id IN ('{$user_id}')");
                            if (!empty($emails)) {
                                foreach ($emails as $email) {
                                    if ($this->verifyContactForNotification($company, $email))
                                        array_push($wp_email, $email);
                                }
                            } else {
                                $GLOBALS['log']->debug('No email found against work product user.');
                            }
                        }
                    }
                }

                if ($bean->rp_entry_email_dept != '[]') {
                    //$e_array = $this->jsonDecode($bean->rp_entry_email_dept);
                    $e_array = json_decode($bean->rp_entry_email_dept);
                }
                if ($bean->rp_review_email_dept != '[]') {
                    //$r_array = $this->jsonDecode($bean->rp_review_email_dept);
                    $r_array = json_decode($bean->rp_review_email_dept);
                }

                //Selected Responsible Personal (Entry)
                for ($i = 0; $i < sizeof($e_array); $i++) {
                    $e = explode('--', $e_array[$i]);
                    $entryEmails[] = $e[0]; //contact email
                    $e_info = $this->getContactRelatedInformation($e[1]);
                    if (!empty($e_info['manager_id'])) {
                        $managers[] = $e_info['manager_id']; //manager id
                    }
                    if (!empty($e_info['oneupmanager_id'])) {
                        $oneupmanagers[] = $e_info['oneupmanager_id']; //oneupmanager_id id
                    }
                    if (!empty($e_info['department'])) {
                        $contact_dept[] = $e_info['department']; //department
                    }
					
					if($e_info['department']=="Pathology Services"){
						array_push($oneupmanager_email, "esteinmetz@apsemail.com");
					}

                    //$e[0] -> Contact email, $e[1] -> Contact id
                    if ($this->verifyContactForNotification($e_info['company'], $e[0])) {
                        array_push($user_email, $e[0]);
                    }
                }

                //Selected Responsible Personal (Review)
                for ($i = 0; $i < sizeof($r_array); $i++) {
                    $r = explode('--', $r_array[$i]);
                    $reviewEmails[] = $r[0]; //contact email
                    $r_info = $this->getContactRelatedInformation($r[1]);
                    if (!empty($r_info['manager_id'])) {
                        $managers[] = $r_info['manager_id']; //manager id
                    }
                    if (!empty($r_info['oneupmanager_id'])) {
                        $oneupmanagers[] = $r_info['oneupmanager_id']; //oneupmanager id
                    }
                    if (!empty($r_info['department'])) {
                        $contact_dept[] = $r_info['department']; //department
                    }
					if($r_info['department']=="Pathology Services"){
						array_push($oneupmanager_email, "esteinmetz@apsemail.com");
					}
                    //$r[0] -> Contact email, $r[1] -> Contact id
                    if ($this->verifyContactForNotification($r_info['company'], $r[0])) {
                        array_push($user_email, $r[0]);
                    }
                }
                $template_data['entryEmails'] = $entryEmails;
                $template_data['reviewEmails'] = $reviewEmails;

                //Adding Manager's email of selected Responsible Personals (Entry and Review)
                $managers = array_unique($managers);
                if (!empty($managers)) {
                    foreach ($managers as $manager_id) {
                        $manager = BeanFactory::getBean('Contacts', $manager_id);
                        $manager_name_list[$manager->id] = $manager->name;
                        $account_name = $manager->account_name;
                        if (!empty($manager->department_id_c)) {
                            $manager_dept[] = $manager->department_id_c;
                        }
                        $primaryEmailAddress = $manager->emailAddress->getPrimaryAddress($manager);
                        if ($primaryEmailAddress != false) {
                            if ($this->verifyContactForNotification($account_name, $primaryEmailAddress)) {
                                array_push($user_email, $primaryEmailAddress);
                            } else {
                                $GLOBALS['log']->debug('Account or domain does not match for Manager: ' . $manager_id);
                            }
                        } else {
                            $GLOBALS['log']->debug('No email found against Manager: ' . $manager_id);
                        }
                    }
                } else {
                    $GLOBALS['log']->debug('No managers found against select Responsible Personals (Entry and Review)');
                }

                //Adding OneUp Manager's email of selected Responsible Personals (Entry and Review)
                $oneupmanagers = array_unique($oneupmanagers);
                if (!empty($oneupmanagers)) {
                    foreach ($oneupmanagers as $oneupmanager_id) {
                        $oneupmanager = BeanFactory::getBean('Contacts', $oneupmanager_id);
                        $oneupmanager_name_list[$oneupmanager->id] = $oneupmanager->name;
                        $oneupAccount_name = $oneupmanager->account_name;

                        $primaryEmailAddress = $oneupmanager->emailAddress->getPrimaryAddress($oneupmanager);
                        if ($primaryEmailAddress != false) {
                            if ($this->verifyContactForNotification($oneupAccount_name, $primaryEmailAddress)) {
                                array_push($oneupmanager_email, $primaryEmailAddress);
                            } else {
                                $GLOBALS['log']->debug('Account or domain does not match for One Up Manager: ' . $oneupmanager_id);
                            }
                        } else {
                            $GLOBALS['log']->debug('No email found against One Up Manager: ' . $oneupmanager_id);
                        }
                    }
                } else {
                    $GLOBALS['log']->debug('No one up managers found against select Responsible Personals (Entry and Review)');
                }

                $manager_dept = array_unique($manager_dept);
                $contact_dept = array_unique($contact_dept); //Responsible Personal (Entry and Review) departments
                $bean->department_c = "^" . implode("^,^", $contact_dept) . "^";
                $this->updateTableData('department_c', "^" . implode("^,^", $contact_dept) . "^", $bean->id);

                /**Department for Email Document **/
                $departmentName   = $contact_dept[0];
                /****/

                //Getting submitter email
                $submitter_email = $this->getRecordEmail("bean_id = '{$bean->employee_id}'");
                $submitter = BeanFactory::getBean('Contacts', $bean->employee_id);
                $template_data['email'] = $submitter_email[0];
                $template_data['submittedBy'] = $submitter_email[0];
                $template_data['submitter_domain_check'] = $this->verifyContactForNotification($submitter->account_name, $submitter_email[0]);

                // Geting Manages based of Submitter //

                if ($submitter->contact_id_c != "") {
                    $submitter_manager_email	= "";
                    $submitter_manager_id		= $submitter->contact_id_c;
                    $submitter_manager_email	= $this->getRecordEmail("bean_id = '{$submitter_manager_id}'");
                    $submitter_manager_bean		= BeanFactory::getBean('Contacts', $submitter_manager_id);
                    //$GLOBALS['log']->fatal('submitter_manager_email ==>' . print_r($submitter_manager_email, 1));
                    if ($this->verifyContactForNotification($submitter_manager_bean->account_name, $submitter_manager_email[0])) {
                        array_push($user_email, $submitter_manager_email[0]);
                    }
                }
                //Getting One Up Manager of Submitter //
                if ($submitter->contact_id1_c != "") {
                    $submitter_oneupmanager_email = "";
                    $submitter_oneupmanager_id = $submitter->contact_id1_c;
                    $submitter_oneupmanager_email = $this->getRecordEmail("bean_id = '{$submitter_oneupmanager_id}'");
                    $submitter_oneupmanager_bean = BeanFactory::getBean('Contacts', $submitter_oneupmanager_id);
                    //$GLOBALS['log']->fatal('submitter_oneupmanager_email ==>'.print_r($submitter_oneupmanager_email,1));

                    if ($this->verifyContactForNotification($submitter_oneupmanager_bean->account_name, $submitter_oneupmanager_email[0])) {
                        array_push($user_email, $submitter_oneupmanager_email[0]);
                    }
                }

                //$template_data['submitter_manager_email'] = $oneupmanager_email[0];
                // $template_data['submitter_manager_domain_check'] = $this->verifyContactForNotification($submitter_manager_bean->account_name, $submitter_manager_email[0]);
                //Preparing list of selected work product , their names and related study directors

                $k = 0;
                if ($bean->work_products != '[]' && $bean->work_products != 'null') {
                    $work_products = $this->jsonDecode($bean->work_products);
                    foreach ($work_products as $product_id) {
                        $workProductID = $product_id;
                        $product = BeanFactory::getBean('M03_Work_Product', $product_id);
                        $wproducts[$product_id] = array(
                            "name" => $product->name,
                            "account_name" => $product->accounts_m03_work_product_1_name,
                            "account_id" => $product->accounts_m03_work_product_1accounts_ida,
                            "wp_compliance" => $product->work_product_compliance_c
                        );


                        if ($product->functional_area_c != "") {
                            $functionalArea = $product->functional_area_c;

                            $functionalUserQuery = "select c.id as functionalUserID from contacts c join contacts_cstm cc on cc.id_c = c.id where cc.functional_area_owner_c LIKE '%" . $functionalArea . "%' AND c.deleted=0";
                            $functionalUserResult = $GLOBALS['db']->query($functionalUserQuery);

                            if ($functionalUserResult->num_rows > 0) {
                                while ($rowFunctionalUser = $GLOBALS['db']->fetchByAssoc($functionalUserResult)) {

                                    $functionalUserID		= $rowFunctionalUser['functionalUserID'];
									$cnt_bean				= BeanFactory::getBean('Contacts', $functionalUserID);
                                    $account_name			= $cnt_bean->account_name;
                                    $primaryEmailAddress 	= $cnt_bean->emailAddress->getPrimaryAddress($cnt_bean);
                                    if ($primaryEmailAddress != false && $this->verifyContactForNotification($account_name, $primaryEmailAddress)) {
										//$GLOBALS['log']->fatal('Functional Owner Email : ' . $primaryEmailAddress);
                                        array_push($user_email, $primaryEmailAddress);
                                    }
                                }
                            }
                        }



                        //$GLOBALS['log']->fatal('$k '.$k.'==$wpCount='.$wpCount);
                        $study_director[$product->contact_id_c]		= array("name" => $product->study_director_script_c);
                        $study_coordinator[$product->contact_id2_c] = array("name" => $product->study_coordinator_relate_c);
                        $k++;
                    }
                }
                //preparing list of work product study directors emails to send notification
                if (!empty($study_director)) {
                    foreach ($study_director as $stdy_id => $value) {
                        $stdy_bean = BeanFactory::getBean('Contacts', $stdy_id);
                        $account_name = $stdy_bean->account_name;
                        $primaryEmailAddress = $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
                        if ($primaryEmailAddress != false && $this->verifyContactForNotification($account_name, $primaryEmailAddress)) {

                            //$GLOBALS['log']->fatal('Study Director Email : ' . $primaryEmailAddress);
                            array_push($user_email, $primaryEmailAddress);
                        }
                    }
                }
                //Add following email to all the notifications if type is facility.
                if ($bean->error_type_c == 'Facility') {
                    global $sugar_config;
                    if (isset($sugar_config['observation_notification_email']) && !empty($sugar_config['observation_notification_email'])) {
                        array_push($user_email, trim($sugar_config['observation_notification_email']));
                    }
                }

                //Get Deceased Animal COM ID/Clinical Issue COM ID
                /*$deceasedAnimalComID = $bean->m06_error_id2_c;
                $deceasedAnimalComBean = BeanFactory::getBean('M06_Error', $deceasedAnimalComID);
                $deceasedAnimalComName = $deceasedAnimalComBean->name;

                $clinicalIssueComID = $bean->m06_error_id1_c;
                $clinicalIssueComBean = BeanFactory::getBean('M06_Error', $clinicalIssueComID);
                $clinicalIssueComName = $clinicalIssueComBean->name;*/

                //Preparing template data array to populate variables in APS Deviation email template              			 

                $commID			= $bean->id;
                $commName		= $bean->name;
				$arrFind		=  array("<", ">");
                $arrReplace		=  array("&lt;", "&gt;");

                $template_data['site_url']			= $GLOBALS['sugar_config']['site_url'];
                $template_data['id']            	= $commID;
                $template_data['Title']            	= $wproducts;
                $template_data['error_name']    	= $commName;
                $template_data['expected_event'] 	= str_replace($arrFind, $arrReplace, $bean->expected_event_c);
                $template_data['actual_event']     	= str_replace($arrFind, $arrReplace, $bean->actual_event_c);
                $template_data['error_type']     	= $type_dom[$bean->error_type_c];
                $template_data['error_category'] 	= $category_dom[$bean->error_category_c];
                $date_discovered                 	= $bean->date_error_documented_c;
                $date_time_discovered             	= $bean->date_time_discovered_c;

                $template_data['date_error_occurred']         = $bean->date_error_occurred_c;
                $template_data['why_it_happened_c']           = $whyHappened_dom[$bean->why_it_happened_c];
                $template_data['details_why_happened_c']      = str_replace($arrFind, $arrReplace, $bean->details_why_happened_c);
                $template_data['error_classification']        = $classification_dom[$bean->error_classification_c];

                /* Change Logic for #502*/
                $activitiesArr = explode(",", str_replace("^", "", $bean->activities_c));
                foreach ($activitiesArr as $idx => $actRecord) {
                    $activitiesDisplayArr[] = $activities_dom[$actRecord];
                }
                $template_data['activities']         = implode(",", $activitiesDisplayArr);

                $template_data['Resolution_Date'] = $bean->resolution_date_c;


                //If category = Training then set the default values of Expected Event and Actual Event
                if ($bean->error_category_c == 'Training') {
                    $template_data['expected_event'] = "On time training";
                    $template_data['actual_event'] = "Overdue training";
                }
                $sep_date_time1970 = explode(" ", $date_time_discovered);
                if (empty($date_discovered) && !empty($date_time_discovered) && $sep_date_time1970[0] != '1970-01-01') {

                    $offset1 =  -18000;
                    //$offset1 =  -21600; //daylight Saving
                    $offset_time = strtotime($date_time_discovered) + $offset1;
                    $date_time_discovered = date("Y-m-d H:i:s", $offset_time);
                    //$GLOBALS['log']->fatal('date_time_discovered==2>'.$date_time_discovered);

                    $sep_date_time = explode(" ", $date_time_discovered);
                    if (count($sep_date_time) > 1) {
                        $date_discovered = $sep_date_time[0];
                        $date_time_discovered = $sep_date_time[1];
                    }else{
                        $date_time_discovered = "NA";
                    }
                }else{
                    $date_time_discovered = "NA";
                }                
                $template_data['date_error_discovered']         = $date_discovered;
                $template_data['date_time_error_discovered']    = $date_time_discovered;
                $template_data['manager_departments']           = $manager_name_list;
                $template_data['oneupmanager_departments']      = $oneupmanager_name_list;
                $template_data['study_directors']               = $study_director;
                $template_data['related_data']                  = $this->prepareRelatedRecordData($bean->related_data);

                /*Ticket #495 : If Equipment & Facility Data is there then get Its Owner and add in User email*/
                /*Ticket #594 : Add E&F Manager & One Up Manger's in User email*/

                if (count($template_data['related_data']['Equip_Equipment']) > 0) {
                    $equipData = $template_data['related_data']['Equip_Equipment'];
                    //$GLOBALS['log']->fatal('643 == EF Equip_Equipment : ' . print_r($equipData,1));
                    for ($efCtr = 0; $efCtr < count($equipData); $efCtr++) {
                        $equipmentID		= $equipData[$efCtr]['id'];
                        //Get Equipment Data
                        $ef_bean            = BeanFactory::getBean('Equip_Equipment', $equipmentID);
                        $ef_department_c    = $ef_bean->department_c;
						
                        $SugarQuery = new SugarQuery();
                        $SugarQuery->from(BeanFactory::newBean('Contacts'), array('alias' => 'cont'));
                        $SugarQuery->where()->contains('equipment_owner_for_dept_c', $ef_department_c);
                        $SugarQuery->select(array('id', 'name', 'contact_id_c', 'contact_id1_c'));
                        $queryContact   = $SugarQuery->compile();
                        $resultContact  = $SugarQuery->execute();

                        //$GLOBALS['log']->fatal('EF resultContact : ' . print_r($resultContact,1));
                        foreach ($resultContact as $key => $value) {
                            $contactID          = $value['id'];
                            $managerContactID   = $value['contact_id_c']; //Manager 
                            $oneUpContactID     = $value['contact_id1_c']; //One Up Manager
                            $cnct_bean          = BeanFactory::getBean('Contacts', $contactID);
                            $EF_owner_name      = $cnct_bean->account_name;
                            $EF_primaryEmailAddress = $cnct_bean->emailAddress->getPrimaryAddress($cnct_bean);
                            if ($EF_primaryEmailAddress != false && $this->verifyContactForNotification($EF_owner_name, $EF_primaryEmailAddress)) {
                                //$GLOBALS['log']->fatal('EF Owner Email : ' . $EF_primaryEmailAddress);
                                array_push($user_email, $EF_primaryEmailAddress);
                            }
                            /*Add Manager Email for Equip_Equipment department*/
                            $cnctManager_bean		= BeanFactory::getBean('Contacts', $managerContactID);
                            $EF_Manager_name		= $cnctManager_bean->account_name;
                            $EF_ManagerEmailAddress = $cnctManager_bean->emailAddress->getPrimaryAddress($cnctManager_bean);
                            if ($EF_ManagerEmailAddress != false && $this->verifyContactForNotification($EF_Manager_name, $EF_ManagerEmailAddress)) {
                                array_push($user_email, $EF_ManagerEmailAddress);
                            }
                            /*Add One UpManager Email for Equip_Equipment department*/
                            $cnctOneUp_bean		= BeanFactory::getBean('Contacts', $oneUpContactID);
                            $EF_OneUp_name		= $cnctOneUp_bean->account_name;
                            $EF_OneUpEmailAddress = $cnctOneUp_bean->emailAddress->getPrimaryAddress($cnctOneUp_bean);
                            if ($EF_OneUpEmailAddress != false && $this->verifyContactForNotification($EF_OneUp_name, $EF_OneUpEmailAddress)) {
                                array_push($user_email, $EF_OneUpEmailAddress);
                            }
                        }
                    }
                }
                /*End logic for Ticket #495, #594*/

                if ($bean->error_category_c == 'Internal Feedback') {

                    /**Get Email for Study Coordinator*/
                    if (!empty($study_coordinator)) {
                        foreach ($study_coordinator as $stdyCrd_id => $value) {
                            $stdyCrd_bean = BeanFactory::getBean('Contacts', $stdyCrd_id);
                            $account_name = $stdyCrd_bean->account_name;
                            $primaryEmailAddress = $stdyCrd_bean->emailAddress->getPrimaryAddress($stdyCrd_bean);
                            if ($primaryEmailAddress != false && $this->verifyContactForNotification($account_name, $primaryEmailAddress)) {
                                array_push($user_email, $primaryEmailAddress);
                            }
                        }
                    }

                    if ($bean->error_type_c == 'Scientific') {
                        array_push($user_email, 'comm_feedback_scientific@apsemail.com');
                    }
                    if ($bean->error_type_c == 'Operations') {
                        array_push($user_email, 'comm_feedback_operations@apsemail.com');
                    }
                    if ($bean->error_type_c == 'Remaining Departments') {
                        array_push($user_email, 'comm_feedback_general@apsemail.com');
                    }
                    if ($bean->error_type_c == 'Re_identification') {
                        array_push($user_email, 'comm_feedback_re-identification@apsemail.com');
                    }
                    
                    /*Ticket #1437 : Added New email*/
                    
                    if ($bean->error_type_c == 'Laboratory Operations') {
                        array_push($user_email, 'comm_feedback_labops@apsemail.com');
                    }
                    if ($bean->error_type_c == 'Operations Support') {
                        array_push($user_email, 'comm_feedback_opssup@apsemail.com');
                    }
                    if ($bean->error_type_c == 'Large Animal Operations') {
                        array_push($user_email, 'comm_feedback_laops@apsemail.com');
                    }
                    if ($bean->error_type_c == 'Small Animal Operations') {
                        array_push($user_email, 'comm_feedback_saops@apsemail.com');
                    }
                    if ($bean->error_type_c == 'Analytical Operations') {
                        array_push($user_email, 'comm_feedback_alops@apsemail.com');
                    }
                    if ($bean->error_type_c == 'Inlife Operations') {
                        array_push($user_email, 'comm_feedback_ilops@apsemail.com');
                    }
                    if ($bean->error_type_c == 'Pathology Operations') {
                        array_push($user_email, 'comm_feedback_pathops@apsemail.com');
                    }
                    /*Added New mail when Type = SPA Bug*/
                    if ($bean->error_type_c == 'SPA Bug') {
                        array_push($user_email, 'comm_feedback_spabugs@apsemail.com');
                    }
                }

                if ($bean->error_category_c == 'Gross Pathology') {
                    if ($bean->error_type_c == 'Deceased Animal') {
                        array_push($user_email, 'mmarion@apsemail.com');
                        array_push($user_email, 'jbranstad@apsemail.com'); //#for Ticket#737 jbranstad@apsemail.com
                    }
                }

                //$GLOBALS['log']->fatal('PMT_08112022 Record Name 773 : ' .  $commName);
                //Adding selected team members for notification email
                $selected_teams = $template_data['related_data']['Teams'];
                $company = 'american preclinical services';
                foreach ($selected_teams as $index => $sTeam) {
                    //Adding all the contact's email addresses of selected teams:
                    //$GLOBALS['log']->fatal('PMT_08112022_Team Name 780 : ' . $sTeam['name']);
                    $con_email_addresses = $this->get_related_contacts_email_addresses($sTeam['name']);
					//$GLOBALS['log']->fatal('PMT_08112022_Teams EmailIds : ' . print_r($con_email_addresses,1));

                    if (!empty($con_email_addresses)) {
                        foreach ($con_email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    }
                    
                    $team = BeanFactory::getBean('Teams', $sTeam['id']);
                    $team_members = $team->get_team_members(true); //Only get active users
                    if (!empty($team_members)) {
                        foreach ($team_members as $member) {
                            $memberPrimaryEmailAddress = $member->emailAddress->getPrimaryAddress($member);
                            if ($memberPrimaryEmailAddress != false) {
                                if ($this->verifyContactForNotification($company, $memberPrimaryEmailAddress))
                                    array_push($user_email, $memberPrimaryEmailAddress);
                            }
                        }
                    } else {
                        $GLOBALS['log']->debug('No team members found against Team Set id ' . $bean->team_set_id);
                    }
                }

                $template_data['recommendation'] = $bean->recommendation_c;
                $user_email = array_unique($user_email);
				
				
				

                if ($bean->error_category_c == "Work Product Schedule Outcome") {
					
					//$GLOBALS['log']->fatal('PMT_08112022_WP Outcome before EmailIds : ' . print_r($user_email,1));
                    $team_name          = 'Operations Support';

                    if ($bean->error_type_c == "Performed Per Protocol") {
                        $user_email         =  array();
                        $user_email         =  array();
                        $oneupmanager_email =  array();
                        $wp_email           = array();
                        array_push($user_email, $template_data['submittedBy']);
                    }



                    if (($bean->error_type_c == "Not Used"
                        || $bean->error_type_c == "Passed Sham"
                        || $bean->error_type_c == "Passed Pyrogen"
                        || $bean->error_type_c == "Not used shared BU")) {

                        $user_email         =  array();
                        $oneupmanager_email =  array();
                        $wp_email           = array();
                        $template_data['email'] = "";
                        //get the email addresses of team members


                    }

                    /**Get Email for Study Coordinator*/
                    if (!empty($study_coordinator)) {
                        foreach ($study_coordinator as $stdyCrd_id => $value) {
                            $stdyCrd_bean = BeanFactory::getBean('Contacts', $stdyCrd_id);
                            $account_name = $stdyCrd_bean->account_name;
                            $primaryEmailAddress = $stdyCrd_bean->emailAddress->getPrimaryAddress($stdyCrd_bean);
                            if ($primaryEmailAddress != false && $this->verifyContactForNotification($account_name, $primaryEmailAddress)) {
                                array_push($user_email, $primaryEmailAddress);
                            }
                        }
                    }


                    $email_addresses = $this->get_related_teams($team_name);
                    if (!empty($email_addresses)) {
                        foreach ($email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    }
                    $con_email_addresses = $this->get_related_contacts_email_addresses($team_name);
                    if (!empty($con_email_addresses)) {
                        foreach ($con_email_addresses as $email) {
                            if ($this->verifyContactForNotification($company, $email)) {
                                array_push($user_email, $email);
                            }
                        }
                    }
                }

                if ($bean->error_category_c == 'Study Specific Charge') {
					$user_email         =  array();
					$oneupmanager_email =  array();
					$wp_email           = array();	
                    array_push($user_email, 'ar@apsemail.com');
                    array_push($user_email, 'npatel@namsa.com');
                }

                /**********************************/
                /**Send Test System Notification Email **/

                $sendTestSystemNotification = false;
                if (count($template_data['related_data']['ANML_Animals']) > 0) {

                    foreach ($template_data['related_data']['ANML_Animals'] as $index => $record) {
                        $testName        = $record['name'];
                        $testSystemID[] = $record['id'];
                        if ($record['name'] == 'Rat' || $record['name'] == 'Mouse' || $record['name'] == 'Hamster' || $record['name'] == 'GP' || $record['name'] == 'Porcine' || $record['name'] == 'Ovine' || $record['name'] == 'Caprine' || $record['name'] == 'Canine' || $record['name'] == 'Bovine') {
                            $sendTestSystemNotification = true;
                        }
                    }

                    /*Add Additional email id in comm Notification */
                    /*According to WPA*/
                    $testSysids = join("','", $testSystemID);
                    $sql_latestWPA = "SELECT m03_work_p90c4ollment_idb as WPAID, date_entered 
											  FROM   m03_work_product_wpe_work_product_enrollment_2_c 
											  LEFT JOIN wpe_work_product_enrollment 
											  ON wpe_work_product_enrollment.id = m03_work_p90c4ollment_idb 
											  WHERE  m03_work_p90c4ollment_idb 
											  IN (SELECT anml_anima9941ollment_idb 
												  FROM anml_animals_wpe_work_product_enrollment_1_c 
												  WHERE  anml_animals_wpe_work_product_enrollment_1anml_animals_ida IN ('" . $testSysids . "') 
												  AND anml_animals_wpe_work_product_enrollment_1_c.deleted = 0) 
											  AND m03_work_p9f23product_ida = '" . $workProductID . "' 
											  AND m03_work_product_wpe_work_product_enrollment_2_c.deleted = 0 
											  AND wpe_work_product_enrollment.deleted = 0 
											  ORDER  BY date_entered DESC LIMIT  1";

                    $resultWPAId = $db->query($sql_latestWPA);
                    if ($resultWPAId->num_rows > 0) {
                        $rowWPAId = $db->fetchByAssoc($resultWPAId);
                        $finalWPEID = $rowWPAId['WPAID'];

                        /*Get WP of latest WPAID*/
                        $sql_WP = "SELECT m03_work_p7d13product_ida as WPID 
										FROM   m03_work_product_wpe_work_product_enrollment_1_c AS WP_WPA 
									WHERE m03_work_p9bf5ollment_idb = '" . $finalWPEID . "'  AND WP_WPA.deleted = 0 
									ORDER  BY date_modified DESC LIMIT  1";
                        $resultWPId = $db->query($sql_WP);
                        if ($resultWPId->num_rows > 0) {
                            $rowWPId = $db->fetchByAssoc($resultWPId);
                            $wpID = $rowWPId['WPID'];

                            $newproduct        = BeanFactory::getBean('M03_Work_Product', $wpID);
                            $study_director_id  = $newproduct->contact_id_c;
                            //$GLOBALS['log']->fatal('new WPA Study Director id : ' . $study_director_id);
                            $stdy_bean = BeanFactory::getBean('Contacts', $study_director_id);
                            $account_name = $stdy_bean->account_name;
                            $primaryEmailAddress = $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
                            if ($primaryEmailAddress != false && $this->verifyContactForNotification($account_name, $primaryEmailAddress)) {
                                //$GLOBALS['log']->fatal('new WPA Study Director Email : ' . $primaryEmailAddress);
                                array_push($user_email, $primaryEmailAddress);
                            }
                        }
                    }
                }
                /**********************************/

                if($bean->error_type_c=="Standard Biocomp TS Selection"){
                    $user_email         =  array();
                    $oneupmanager_email =  array();
                    $wp_email           =  array();
                    $template_data['email'] = "";
                } 
                // $GLOBALS['log']->fatal('Error Category =='.$bean->error_category_c);
                // $GLOBALS['log']->fatal('Error Type =='.$bean->error_type_c);
                if ($bean->error_category_c == 'Real time study conduct') {
                    if ($bean->error_type_c == 'Re Challenge') {
						// $GLOBALS['log']->fatal('Get ReChallenge Condition');
                        array_push($user_email, 'hackerson@apsemail.com');
                        array_push($user_email, 'cvaleri@apsemail.com');
                        array_push($user_email, 'ablakstvedt@apsemail.com');
                        array_push($user_email, 'cjimenez@apsemail.com');
                        array_push($user_email, 'jgeist@apsemail.com');
                        array_push($user_email, 'eclark@apsemail.com');
                        array_push($user_email, 'dzehowski@apsemail.com');
                        array_push($user_email, 'ar@apsemail.com');
                        // array_push($user_email, 'comm_feedback_opssup@apsemail.com');
                        // array_push($user_email, 'vsuthar@apsemail.com');
                        // array_push($user_email, 'gsingh@apsemail.com'); //#for Ticket#737 jbranstad@apsemail.com
                        
                        // Mail to send study Director
                        if (!empty($study_director)) {
                            foreach ($study_director as $stdy_id => $value) {
                                $stdy_bean = BeanFactory::getBean('Contacts', $stdy_id);
                                $account_name = $stdy_bean->account_name;
                                $primaryEmailAddress = $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
                                if ($primaryEmailAddress != false && $this->verifyContactForNotification($account_name, $primaryEmailAddress)) {
        
                                    //email_sending_template.php
                                    array_push($user_email, $primaryEmailAddress);
                                }
                            }
                        }
                        // Add Operation support Emails 
                        $team_name          = 'Operations Support';
                        $email_addresses    = $this->get_related_teams($team_name);
                        if (!empty($email_addresses)) {
                            foreach ($email_addresses as $email) {
                                if ($this->verifyContactForNotification($company, $email)) {
                                    array_push($user_email, $email);
                                }
                            }
                        } 
                        $con_email_addresses = $this->get_related_contacts_email_addresses($team_name);
                        if (!empty($con_email_addresses)) {
                            foreach ($con_email_addresses as $email) {
                                if ($this->verifyContactForNotification($company, $email)) {
                                    array_push($user_email, $email);
                                }
                            }
                        } 
                    }
                }
				
                //$GLOBALS['log']->fatal('PMT_08112022_CommApp Email Users ==> ' . print_r($user_email, 1));
                if (!empty($user_email) || !empty($template_data['email'])) {
                    if (empty($wp_email)) {
                        $sendEmailTemplates->sendemailWithoutWP($template_data, $user_email, $oneupmanager_email);
                    } else {
                        $sendEmailTemplates->sendemail($template_data, $user_email, $wp_email, $oneupmanager_email);
                    }
                }





                /**********************************/
                /**Send Test System Notification Email **/
                if ($sendTestSystemNotification) {
                    $TStemplate = new EmailTemplate();
                    $TStemplate->retrieve_by_string_fields(array('name' => 'Generic TS Use Notification', 'type' => 'email'));
                    $commLink =  '<a target="_blank"href="' . $template_data['site_url'] . '/#M06_Error/' . $commID . '">' . $commName . '</a>';
                    $TStemplate->body_html = str_replace('[M06_Error_Name]', $commLink, $TStemplate->body_html);

                    $TSemail = "IACUCSubmissions@apsemail.com";

                    $sendEmailTemplates->sendMailForTSUseNotification($TStemplate, $TSemail);
                }
                /**********************************/


                /*Create Email Document records*/
                /*******************************/
                //$GLOBALS['log']->fatal('Create Email Document records');
                $wp_compliance = "";
                if ($workProductID != "") {
                    $WP_bean = BeanFactory::getBean('M03_Work_Product', $workProductID);
                    $wp_compliance = $WP_bean->work_product_compliance_c;
                    $template_data['wpComplianceData'] = $wp_compliance;
                }
                /* Add new condition for #570, Work Product Compliance should be GLP*/

                if ($workProductID != "" && $wp_compliance == "GLP" && 1 == 0) {
                    //Create bean
                    $emailDocBean = BeanFactory::newBean('EDoc_Email_Documents');

                    $eDocNewName                    =  $commName . ' 01.pdf';
                    $emailDocBean->document_name    = $eDocNewName;
                    $emailDocBean->department        = $departmentName; //'Information_Technology'; 
                    $emailDocBean->type_2            = 'COM email';
                    $emailDocBean->assigned_user_id    = $current_user->id;
                    $emailDocBean->filename            = $eDocNewName;
                    $emailDocBean->file_ext            = 'pdf';
                    $emailDocBean->file_mime_type    = 'application/pdf';

                    $emailDocBean->save();
                    $emailDocId = $emailDocBean->id;

                    $args = array(
                        'module' => 'M06_Error',
                        'record' => $commID,
                        'email_document_id' => $emailDocId,
                        'email_document_name' => $eDocNewName,
                    );

                    $html2 = $this->getCustomEmailDocument($args, $template_data);

                    /*Create Top Part*/

                    $getEmailSubject = $this->getEmailSubject($template_data);

                    $toAddressArr = array_unique(array_merge($user_email, $wp_email));

                    for ($cnt = 0; $cnt < count($toAddressArr); $cnt++) {
                        if ($toAddressArr[$cnt] == "spa_testing@apsemail.com") {
                            $toAddressArr[$cnt] =  "SPA Testing(" . $toAddressArr[$cnt] . ")";
                        } else {
                            $userEmailQuery = "SELECT  CONCAT_WS(' ', contacts.first_name ,contacts.last_name) as name  FROM  `email_addresses`
                                INNER JOIN `email_addr_bean_rel` ON email_addr_bean_rel.email_address_id = email_addresses.id
                                INNER JOIN `contacts` ON contacts.id = email_addr_bean_rel.bean_id
                                WHERE email_addr_bean_rel.deleted = 0 AND email_addresses.email_address like '" . $toAddressArr[$cnt] . "' limit 1";
                            $userEmailResult = $GLOBALS['db']->query($userEmailQuery);
                            while ($fetchResult = $GLOBALS["db"]->fetchByAssoc($userEmailResult)) {
                                if ($fetchResult['name'] != "")
                                    $toAddressArr[$cnt] = $fetchResult['name'] . "(" . $toAddressArr[$cnt] . ")";
                            }
                        }
                    }
                    $toAddress = implode(",", $toAddressArr);

                    $ccEmailArr[] = $template_data['email'];

                    $ccEmailArr1 = array_diff(array_unique(array_merge($ccEmailArr, $oneupmanager_email)), array_unique(array_merge($user_email, $wp_email)));

                    for ($cnt = 0; $cnt < count($ccEmailArr1); $cnt++) {
                        if ($ccEmailArr1[$cnt] == "spa_testing@apsemail.com") {
                            $ccEmailArr1[$cnt] =  "SPA Testing(" . $ccEmailArr1[$cnt] . ")";
                        } else {
                            $userEmailQuery = "SELECT  CONCAT_WS(' ', contacts.first_name ,contacts.last_name) as name  FROM  `email_addresses`
                                    INNER JOIN `email_addr_bean_rel` ON email_addr_bean_rel.email_address_id = email_addresses.id
                                    INNER JOIN `contacts` ON contacts.id = email_addr_bean_rel.bean_id
                                    WHERE email_addr_bean_rel.deleted = 0 AND email_addresses.email_address like '" . $ccEmailArr1[$cnt] . "' limit 1";
                            $userEmailResult = $GLOBALS['db']->query($userEmailQuery);
                            while ($fetchResult = $GLOBALS["db"]->fetchByAssoc($userEmailResult)) {
                                if ($fetchResult['name'] != "")
                                    $ccEmailArr1[$cnt] = $fetchResult['name'] . "(" . $ccEmailArr1[$cnt] . ")";
                            }
                        }
                    }

                    $ccEmail = implode(",", $ccEmailArr1);
                    //$ccEmail = implode(",",array_unique(array_merge($ccEmailArr, $oneupmanager_email)));



                    $offset =  -18000;
                    //$offset =  -21600; //daylight Saving
                    $offset_time = time() + $offset + 1;
                    $formattedDate = date("D m/d/Y g:i a", $offset_time);

                    $top_html = '<span style="font-size:8pt;">' . $formattedDate . '</span>';
                    $top_html .= '<br/><span style="font-size:8pt;">From: PMT (do_not_reply@apsemail.com)</span>';
                    $top_html .= '<br/><span style="font-size:8pt;">Subject: ' . $getEmailSubject . '</span>';
                    $top_html .= '<br/><span style="font-size:8pt;">To: ' . $toAddress . '</span>';
                    if ($ccEmail != "")
                        $top_html .= '<br/><span style="font-size:8pt;">CC: ' . $ccEmail . '</span>';

                    $borderHtml = '<p><span style="font-size:10pt;">----------------------------------------------------------------------------------------------------------------------------------------------------------------</span></p>';
                    $body_html_new = $top_html . $borderHtml . $html2;


                    $fileName = $eDocNewName;
                    $fileDocID = $emailDocId;
                    $fileInfo = $this->create_PDF($body_html_new, $fileName);
                    //$GLOBALS['log']->fatal('pdfAfter == '.print_r($fileInfo,1));
                    $objFile = new UploadFile('uploadfile');
                    // Set the filename
                    $objFile->file_ext = 'pdf';
                    $objFile->set_for_soap($fileName, (sugar_file_get_contents($fileInfo['file_path'])));
                    $objFile->create_stored_filename();
                    $objFile->final_move($fileDocID);
                    //$GLOBALS['log']->fatal('workProductId == '.$workProductID);

                    if ($workProductID != "" && $emailDocId != "") {
                        $gid = create_guid(); //strtolower($GUID);
                        $sql_msb = "INSERT INTO `m03_work_product_edoc_email_documents_1_c` (id,m03_work_product_edoc_email_documents_1m03_work_product_ida, m03_work_product_edoc_email_documents_1edoc_email_documents_idb) VALUES ('" . $gid . "','" . $workProductID . "', '" . $emailDocId . "')";
                        $GLOBALS['db']->query($sql_msb);
                    }
                }

                /*******************************/
            }
        } else {
            $GLOBALS['log']->debug("Observation is in edit mode.");
        }
    }

    function create_PDF($html, $name)
    {
        require_once('vendor/tcpdf/tcpdf.php');
        $tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $tcpdf->SetCreator(PDF_CREATOR);
        $name = str_replace(' ', '_', $name);
        $date = date('Y-m-d');
        //$file_info['file_path'] = "upload/".$name.".pdf";
        //$file_info['file_name'] = $name.".pdf";
        $file_info['file_path'] = "upload/" . $name;
        $file_info['file_name'] = $name;
        //$GLOBALS['log']->fatal('file Name pdf file== '.$file_info['file_path']);

        $tcpdf->AddPage();
        $tcpdf->writeHTML($html, true, false, true, false, '');
        $tcpdf->Output($file_info['file_path'], "F");
        return $file_info;
    }

    /**
     * @param $arr
     * @return array|mixed
     */
    function jsonDecode($arr)
    {
        $result = str_replace("\\\"", '', $arr);
        $result = str_replace("[", '', $result);
        $result = str_replace("]", '', $result);
        $result = explode(",", $result);
        return $result;
    }

    /**
     * Prepares array of emails from $dept list and return array for matching department name
     * @param string $dept_name
     * @param string $dept
     * @param string $email
     * @return array
     */
    function get_email_array($dept_name, $dept, $email)
    {
        $j = 0;
        $arr = array();
        for ($i = 0; $i < sizeof($dept); $i++) {
            if ($dept_name == $dept[$i]) {
                $arr[$j] = $email[$i];
                $j++;
            }
        }
        return $arr;
    }

    /**
     * Retrieves email address form bean id from email related bean table
     * @param string $record
     * @return string email_address|null
     */
    function getRecordEmail($where = '')
    {
        $email_addresses = array();
        $query = "SELECT email_addresses.email_address
                    FROM `email_addresses`
                    INNER JOIN
                        `email_addr_bean_rel` ON email_addr_bean_rel.email_address_id = email_addresses.id
                    WHERE email_addr_bean_rel.deleted = 0 AND email_addresses.deleted='0' AND email_addresses.invalid_email='0' AND email_addresses.opt_out='0'
                    AND email_addr_bean_rel." . $where;
        $res = $GLOBALS['db']->query($query);
        if ($res->num_rows >= 1) {
            while ($row = $GLOBALS["db"]->fetchByAssoc($res)) {
                $email_address[] = $row['email_address'];
            }
            return $email_address;
        }
        //        else if ($res->num_rows == 1) {
        //            $row = $GLOBALS["db"]->fetchByAssoc($res);
        //            return $row['email_address'];
        //        }
        return '';
    }

    /**
     * Prepares list of related records name and id to add and make clickable link for email template.
     * Return related record information for rooms, animals, teams,
     * documents, equipment & facility, critical phase inspection
     * @param array $related_data
     * @return array $data
     */
    function prepareRelatedRecordData($related_data)
    {
        $data = array();
        foreach ($related_data as $module => $value) {
            $data[$module] = array();
            //$ids = $this->jsonDecode($value);
            $ids = $value;
            if (gettype($ids) == 'array') {
                foreach ($ids as $id) {
                    if (!empty($id) && $id != "null" && $id != "[]") {
                        if ($module != "ANML_Animals") {
                            $SugarQuery = new SugarQuery();
                            $SugarQuery->from(BeanFactory::newBean($module));
                            $SugarQuery->where()->equals('id', $id);
                            $SugarQuery->select(array('name'));
                            $query = $SugarQuery->compile();
                            $result = $SugarQuery->execute();
                            $data[$module][] = array("name" => $result[0]['name'], "id" => $id);
                        } else {
                            $SugarQuery = new SugarQuery();
                            $SugarQuery->from(BeanFactory::newBean($module), array('alias' => 'aa'));
                            $SugarQuery->joinTable('anml_animals_cstm', array(
                                'joinType' => 'INNER',
                                'alias' => 'aa_cstm',
                                'linkingTable' => true,
                            ))->on()->equalsField('aa_cstm.id_c', 'aa.id');
                            $SugarQuery->where()->equals('aa.id', $id);
                            $SugarQuery->select(array('aa.name', 'aa_cstm.usda_id_c'));
                            $query = $SugarQuery->compile();
                            $result = $SugarQuery->execute();
                            $data[$module][] = array("name" => $result[0]['name'], "usda_id_c" => $result[0]['usda_id_c'], "id" => $id);
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Verify if contact account name is 'american preclinical services'
     * and email address domain is 'apsemail.com' otherwise return false
     * @param string $account_name
     * @param string $email
     * @return bool true|false
     */
    function verifyContactForNotification($account_name, $email)
    {
        $companyToSearch = array('american preclinical services','NAMSA');
        $domainToSearch = array('apsemail.com', 'namsa.com');
        if (in_array(substr($email, strrpos($email, '@') + 1), $domainToSearch) && in_array($account_name,$companyToSearch) || in_array(strtolower($account_name),$companyToSearch)) {
            return true;
        }
        return false;
    }

    /**
     * Update specific field data in module table, can be extended to update multiple fields at a time.
     * @param string $field
     * @param string $value
     * @param string $id
     */
    function updateTableData($field, $value, $id)
    {
        $query = "UPDATE m06_error_cstm set $field ='" . $value . "' where id_c = '" . $id . "'";
        $GLOBALS['db']->query($query);
    }

    /**
     * Members which are associated with team, return there email addresses
     * @param type $team_name
     * @return type array
     */
    function get_related_teams($team_name)
    {
        $email_addresses = array();

        $get_team_members = "SELECT email_addresses.email_address FROM teams "
            . "INNER JOIN team_memberships ON teams.id = team_memberships.team_id "
            . "INNER JOIN users ON team_memberships.user_id = users.id "
            . "INNER JOIN email_addr_bean_rel ON users.id = email_addr_bean_rel.bean_id "
            . "INNER JOIN email_addresses ON email_addr_bean_rel.email_address_id = email_addresses.id "
            . "WHERE teams.name = '" . $team_name . "' AND teams.deleted = 0 AND team_memberships.deleted = 0 AND users.deleted = 0 ";

        $team_members = $GLOBALS['db']->query($get_team_members);
        while ($team_member_address = $GLOBALS["db"]->fetchByAssoc($team_members)) {
            if (!empty($team_member_address['email_address'])) {
                $email_addresses[] = $team_member_address['email_address'];
            }
        }

        return $email_addresses;
    }

    /**
     * Contacts which are associated with team, return there email addresses
     * @param type $team_name
     * @return type array
     */
    function get_related_contacts_email_addresses($team_name)
    {

        $contacts_email_addreses = array();

        $query = new SugarQuery();
        $query->from(BeanFactory::newBean('Teams'), array('alias' => 'teams'));
        $query->joinTable('team_sets_teams', array(
            'joinType' => 'INNER',
            'alias' => 'tst',
            'linkingTable' => true,
        ))->on()->equalsField('teams.id', 'tst.team_id');
        $query->joinTable('contacts', array(
            'joinType' => 'INNER',
            'alias' => 'c',
            'linkingTable' => true,
        ))->on()->equalsField('tst.team_set_id', 'c.team_set_id');
        $query->joinTable('email_addr_bean_rel', array(
            'joinType' => 'INNER',
            'alias' => 'eabr',
            'linkingTable' => true,
        ))->on()->equalsField('c.id', 'eabr.bean_id');
        $query->joinTable('email_addresses', array(
            'joinType' => 'INNER',
            'alias' => 'ea',
            'linkingTable' => true,
        ))->on()->equalsField('eabr.email_address_id', 'ea.id');

        $query->where()
            ->equals('teams.name', $team_name)
            ->equals('teams.deleted', 0)
            ->equals('tst.deleted', 0)
            ->equals('c.deleted', 0)
            ->equals('eabr.deleted', 0)
			->equals('eabr.primary_address', 1);
        $query->select('ea.email_address');
        $compile_query = $query->compile();
        $result_query = $query->execute();

        foreach ($result_query as $key => $innerArray) {
            foreach ($innerArray as $key => $email_address) {
                $contacts_email_addreses[] = $email_address;
            }
        }

        return $contacts_email_addreses;
    }

    /**
     * Fetch responsible personal department and manager
     * @param string $contact_id
     * @return array
     */
    function getContactRelatedInformation($contact_id)
    {

        $SugarQuery = new SugarQuery();
        $SugarQuery->from(BeanFactory::newBean('Contacts'), array('alias' => 'cont'));
        $SugarQuery->joinTable('accounts_contacts', array('alias' => 'ac', 'joinType' => 'LEFT', 'linkingTable' => true))
            ->on()
            ->equalsField('ac.contact_id', 'id')
            ->equals('ac.deleted', 0);

        $SugarQuery->joinTable('accounts', array('alias' => 'a', 'joinType' => 'LEFT', 'linkingTable' => true))
            ->on()
            ->equalsField('a.id', 'ac.account_id')
            ->equals('a.deleted', 0);

        $SugarQuery->where()->equals('id', $contact_id);
        $SugarQuery->select(array('department_id_c', 'contact_id_c', 'contact_id1_c', 'a.name'));
        $SugarQuery->limit(1);
        $query = $SugarQuery->compile();
        $result = $SugarQuery->execute();
        return array(
            "department" => $result[0]['department_id_c'],
            "manager_id" => $result[0]['contact_id_c'],
            "oneupmanager_id" => $result[0]['contact_id1_c'],
            "company" => $result[0]['name'],
        );
    }

    /**
     * Create TXT file of email which has sent to users
     * @param string $args,$template_data
     */
    function getCustomEmailDocument($args, $template_data)
    {
        if ($args['module'] && $args['record']) {
            global $sugar_config;
            $ss = new Sugar_Smarty();
            $ss->security = true;
            $ss->security_settings['PHP_TAGS'] = false;
            if (defined('SUGAR_SHADOW_PATH')) {
                $ss->secure_dir[] = SUGAR_SHADOW_PATH;
            }
            $ss->assign('MOD', $GLOBALS['mod_strings']);
            $ss->assign('APP', $GLOBALS['app_strings']);


            $RMS_RoomData  = "NA";
            $testSystemData  = "NA";
            $TeamsData  = "NA";
            $Erd_Error_DocumentsData  = "NA";
            $Equip_EquipmentData  = "NA";
            $A1A_Critical_Phase_InspectioData  = "NA";
            $M06_ErrorData  = "NA";

            $EntryEmailData  = "NA";
            $ReviewEmailData  = "NA";
            $ManagerData  = "NA";
            $OneupManagerData  = "NA";

            $workProductData  = "NA";

            /*Get Work Product*/
            if (count($template_data['Title']) > 0) {
                foreach ($template_data['Title'] as $index => $record) {
                    $workProductArr[] = $record['name'] . "(" . $record['account_name'] . ")";
                }
                $workProductData = implode(",", $workProductArr);
            }


            /*get responsible personnel Review*/
            if (count($template_data['reviewEmails']) > 0) {
                foreach ($template_data['reviewEmails'] as $index => $record) {
                    $ReviewEmailArr[] = $record;
                }
                $ReviewEmailData = implode(",", $ReviewEmailArr);
            }

            /*get responsible personnel Entry*/
            if (count($template_data['entryEmails']) > 0) {
                foreach ($template_data['entryEmails'] as $index => $record) {
                    $EntryEmailArr[] = $record;
                }
                $EntryEmailData = implode(",", $EntryEmailArr);
            }


            /*get Manager_departments*/
            if (count($template_data['manager_departments']) > 0) {
                foreach ($template_data['manager_departments'] as $index => $record) {
                    $ManagerArr[] = $record;
                }
                $ManagerData = implode(",", $ManagerArr);
            }

            /*get oneupmanager_departments*/
            if (count($template_data['oneupmanager_departments']) > 0) {
                foreach ($template_data['oneupmanager_departments'] as $index => $record) {
                    $oneupmanagerArr[] = $record;
                }
                $OneupManagerData = implode(",", $oneupmanagerArr);
            }

            if (count($template_data['related_data']['RMS_Room']) > 0) {
                foreach ($template_data['related_data']['RMS_Room'] as $index => $record) {
                    $RMS_RoomDataArr[] = $record['name'];
                }
                $RMS_RoomData = implode(",", $RMS_RoomDataArr);
            }

            if (count($template_data['related_data']['ANML_Animals']) > 0) {
                foreach ($template_data['related_data']['ANML_Animals'] as $index => $record) {
                    $testName = $record['name'];
                    if ($record['usda_id_c'] != "") {
                        $testName .= "-" . $record['usda_id_c'];
                    }

                    $ANML_AnimalsArr[] = $testName;
                }
                $testSystemData = implode(",", $ANML_AnimalsArr);
            }

            if (count($template_data['related_data']['Teams']) > 0) {

                foreach ($template_data['related_data']['Teams'] as $index => $record) {
                    $TeamsArr[] = $record['name'];
                }
                $TeamsData = implode(",", $TeamsArr);
            }


            if (count($template_data['related_data']['Erd_Error_Documents']) > 0) {
                foreach ($template_data['related_data']['Erd_Error_Documents'] as $index => $record) {
                    $Erd_Error_DocumentsArr[] = $record['name'];
                }
                $Erd_Error_DocumentsData = implode(",", $Erd_Error_DocumentsArr);
            }


            if (count($template_data['related_data']['Equip_Equipment']) > 0) {
                foreach ($template_data['related_data']['Equip_Equipment'] as $index => $record) {
                    $Equip_EquipmentArr[] = $record['name'];
                }
                $Equip_EquipmentData = implode(",", $Equip_EquipmentArr);
            }


            if (count($template_data['related_data']['A1A_Critical_Phase_Inspectio']) > 0) {
                foreach ($template_data['related_data']['A1A_Critical_Phase_Inspectio'] as $index => $record) {
                    $A1A_Critical_Phase_InspectioArr[] = $record['name'];
                }
                $A1A_Critical_Phase_InspectioData = implode(",", $A1A_Critical_Phase_InspectioArr);
            }

            if (count($template_data['related_data']['M06_Error']) > 0) {
                foreach ($template_data['related_data']['M06_Error'] as $index => $record) {
                    $M06_ErrorArr[] = $record['name'];
                }
                $M06_ErrorData = implode(",", $M06_ErrorArr);
            }

            $arrFind         =  array("<", ">");
            $arrReplace     =  array("&lt;", "&gt;");

            $template_data['workProductData']         = $workProductData;
            $template_data['EntryEmailData']         = $EntryEmailData;
            $template_data['ReviewEmailData']         = $ReviewEmailData;
            $template_data['ManagerData']         = $ManagerData;
            $template_data['OneupManagerData']         = $OneupManagerData;

            $template_data['RMS_RoomData']         = $RMS_RoomData;
            $template_data['testSystemData']     = $testSystemData;
            $template_data['TeamsData']             = $TeamsData;
            $template_data['Equip_EquipmentData']         = $Equip_EquipmentData;
            $template_data['Erd_Error_DocumentsData']             = $Erd_Error_DocumentsData;
            $template_data['A1A_Critical_Phase_InspectioData']     = $A1A_Critical_Phase_InspectioData;
            $template_data['M06_ErrorData']         = $M06_ErrorData;

            $template_data['expected_event']             =    str_replace($arrFind, $arrReplace, $template_data['expected_event']);
            $template_data['actual_event']                 =    str_replace($arrFind, $arrReplace, $template_data['actual_event']);
            $template_data['details_why_happened_c']     =    str_replace($arrFind, $arrReplace, $template_data['details_why_happened_c']);

            /*Date Formatting*/


            if ($template_data['date_error_occurred'] != "")
                $template_data['date_error_occurred'] = date("m/d/Y", strtotime($template_data['date_error_occurred']));


            if ($template_data['date_error_discovered'] != "")
                $template_data['date_error_discovered'] = date("m/d/Y", strtotime($template_data['date_error_discovered']));

            if ($template_data['Resolution_Date'] != "")
                $template_data['Resolution_Date'] = date("m/d/Y", strtotime($template_data['Resolution_Date']));



            $ss->assign('data', $template_data);
            return $html2 = $ss->fetch('custom/modules/M06_Error/tpls/CommAppEmailFile.tpl');
        }
    }

    /*Get Email Subject */
    function getEmailSubject($data)
    {
        $error_category = $data['error_category'];
        if (!empty($error_category)) {
            $subject .= $error_category . ' Communication';
        }

        $work_products = array();
        foreach ($data['Title'] as $key => $value) {
            if ($value['name'] != "")
                $work_products[] = $value['name'];
        }
        if (!empty($work_products)) {
            //$subject .= ' - Work Product(s)['.implode(",",$work_products).']';
            $subject .= ' : ' . implode(",", $work_products) . '';
        }

        $anml_animals = array();
        foreach ($data['related_data']['ANML_Animals'] as $key => $value) {
            $testName = "";
            if ($value['name'] != "" && $value['usda_id_c'] != "")
                $testName = $value['name'] . '-' . $value['usda_id_c'];
            else if ($value['name'] != "")
                $testName = $value['name'];
            if ($testName != "")
                $anml_animals[] = $testName;
        }
        if (!empty($anml_animals)) {
            //$subject .= ' - Test System(s)['.implode(",",$anml_animals).']';
            $subject .= " : " . implode(",", $anml_animals);
        }
        return $subject;
    }
}
