<?php

//AHTSHAM-TL - For Generate the System level Number
//Code for an auto-incrementing the �Name� field in the "Error" module?
//Naming convention will be �E18-xxxx�. This module is on the live site already.
//Since 2019 prefix has changed to "O" and in future from the year of 2020 it will "C"

class update_ts_date_record
{
	function update_ts_date_record_after_save($bean, $event, $arguments)
	{
		global $db;

		if ($arguments['related_module'] == 'ANML_Animals' && $arguments['link'] == 'm06_error_anml_animals_1') {
			$allRecord	=	0;
			$TSName		=	array();
			$TSId       =  array();
			$TSNameString	= "";
			$queryCountAll = "SELECT TS.name AS TSName, TS.id AS TSId FROM m06_error_anml_animals_1_c AS COMMTS
								LEFT JOIN anml_animals AS TS ON COMMTS.m06_error_anml_animals_1anml_animals_idb = TS.id 
			 				WHERE m06_error_anml_animals_1m06_error_ida='" . $bean->id . "' AND COMMTS.deleted=0";

			$resultAllRecord = $db->query($queryCountAll);

			if ($resultAllRecord->num_rows > 0) {
				$allRecord = 1;

				while ($rowAllRecord = $db->fetchByAssoc($resultAllRecord)) {
					$TSId = $rowAllRecord['TSId'];
				}
			}

			$procedure = $bean->procedure_type_c;
			$dateOccured = date("Y-m-d", strtotime($bean->date_error_occurred_c));
			$testSysBean = BeanFactory::retrieveBean('ANML_Animals', $TSId);

			if ($procedure == "Acute") {
				if (empty($testSysBean->first_procedure_date_c) && empty($testSysBean->termination_date_c)) {
					$testSysBean->first_procedure_date_c = $dateOccured;
					$testSysBean->termination_date_c = $dateOccured;
					$testSysBean->save();
				}
			}
			if ($procedure == "Initial") {
				if (empty($testSysBean->first_procedure_date_c)) {
					$testSysBean->first_procedure_date_c = $dateOccured;
					$testSysBean->save();
				}
			}
			if ($procedure == "Termination") {
				if (empty($testSysBean->termination_date_c)) {
					$testSysBean->termination_date_c = $dateOccured;
					$testSysBean->save();
				}
			}
		}
	}
}
