<?php
require_once 'include/Sugarpdf/sugarpdf/sugarpdf.pdfmanager.php';
require_once 'include/Sugarpdf/sugarpdf/sugarpdf.smarty.php';
//require_once('include/Sugar_Smarty.php');
//ob_start();
class IM_Inventory_ManagementSugarpdfPdfmanager extends SugarpdfPdfmanager
{
    public function preDisplay()
    {
        global $db, $current_user, $app_list_strings;
        parent::preDisplay();
        $this->SetMargins(15, 10, 15);
        $previewMode = false;

        if (!empty($_REQUEST['pdf_preview']) && $_REQUEST['pdf_preview'] == 1) {
            $previewMode = true;
        }
        if ($this->module == 'IM_Inventory_Management' && $previewMode === false) {
            $timepoint_type_list_dom = $app_list_strings['timepoint_type_list'];
            $inventory_item_type_list_dom = $app_list_strings['inventory_item_type_list'];
            $beanId            = $this->bean->id;
            $roomId            = $this->bean->rms_room_id_c;
            $equipmentId       = $this->bean->equip_equipment_id_c;
            $locationBuilding  = $this->bean->location_building;
            // $endCondition      = $this->bean->end_condition;

            //condition to get full Name of location building
            if ($locationBuilding == '780')
                $locationBuildingName = '780 86th Ave.';
            else if ($locationBuilding == '8945')
                $locationBuildingName = '8945 Evergreen Blvd.';
            else if ($locationBuilding == '8960')
                $locationBuildingName = '8960 Evergreen Blvd.';
            else if ($locationBuilding == '9055')
                $locationBuildingName = '9055 Evergreen Blvd.';
            else
                $locationBuildingName = $locationBuilding;

            $wpId      = $this->bean->m03_work_product_im_inventory_management_1m03_work_product_ida; //get WP Id
            $beanWP    = BeanFactory::retrieveBean('M03_Work_Product', $wpId); //get WP bean
            $beanRoom  = BeanFactory::retrieveBean('RMS_Room', $roomId); //get Room bean
            $beanEquip = BeanFactory::retrieveBean('Equip_Equipment', $equipmentId); //get equipment Bean

            $location  = $locationBuildingName . '' . $beanRoom->name . '' . $beanEquip->name;

            //fetch Records from Inventory Item(Subpanel)
            $allDataArr = array();
            $count = 0;
            if ($this->bean->load_relationship('ii_inventory_item_im_inventory_management_1')) {
                //Fetch related record IDs
                $relatedIIIds = $this->bean->ii_inventory_item_im_inventory_management_1->get();
                foreach ($relatedIIIds as $IIIds) {
                    $beanII       = BeanFactory::retrieveBean('II_Inventory_Item', $IIIds); //get II bean
                    $testSysId    = $beanII->anml_animals_ii_inventory_item_1anml_animals_ida;
                    $beanAnimal   = BeanFactory::retrieveBean('ANML_Animals', $testSysId); //get Animal bean               
                    $speciesId    = $beanAnimal->s_species_id_c;
                    $species      = BeanFactory::retrieveBean('S_Species', $speciesId); //get Animal bean
                    $speciesName  = $species->name;
                    // $GLOBALS['log']->fatal("species Name===>". $speciesName);
                    if($speciesName == 'Lagomorph' || $speciesName == 'Rat' ||$speciesName == 'Mouse' ||$speciesName == 'Hamster' ||$speciesName == 'Guinea Pig'){
                        $allDataArr[$count]['testSystemId'] = $beanAnimal->usda_id_c;
                        // $GLOBALS['log']->fatal("USDA Name===>". $beanAnimal->usda_id_c);
                    }else{
                        $allDataArr[$count]['testSystemId'] = $beanAnimal->name;
                        // $GLOBALS['log']->fatal("testSystemId===>". $beanAnimal->name);
                    }
                    if ($beanII->timepoint_type != 'Defined') {
                        $allDataArr[$count]['timepoint'] = $timepoint_type_list_dom[$beanII->timepoint_type];
                    } else {
                        $allDataArr[$count]['timepoint'] = $beanII->timepoint_name_c;
                    }
                    if ($beanII->collection_date_time_type == 'Date at Record Creation') {
                        $formatedDate = explode("-", $beanII->collection_date);
                        $newformatedDate = $formatedDate[1] . '/' . $formatedDate[2] . '/' . $formatedDate[0]; // mm/dd/yy format
                        $allDataArr[$count]['collectiondate'] = $newformatedDate;
                    }
                    if ($beanII->type_2 != 'Other') {
                        $allDataArr[$count]['specimentype'] = $inventory_item_type_list_dom[$beanII->type_2];
                    } else {
                        $allDataArr[$count]['specimentype'] = $beanII->other_type_c;
                    }
                    $allDataArr[$count]['actionName'] = $this->bean->action;
                    $count++;
                }
            }
            $allDataArrCount = count($allDataArr);
            $forStaticRows = 20 - $allDataArrCount;
            for ($i = 0; $i < $forStaticRows; $i++) {
                $rowArr[] = $i;
            }

            $this->ss->assign('work_product', $beanWP->name);
            $this->ss->assign('location', $location);
            // $this->ss->assign('storage_condition', $endCondition);
            $this->ss->assign('allDataArr', $allDataArr);
            $this->ss->assign('forStaticRows', $forStaticRows);
            $this->ss->assign('rowArr', $rowArr);
        }
    }

    /**
     * PDF manager specific Footer function
     */
    public function Footer()
    {
        $cur_y = $this->GetY();
        $ormargins = $this->getOriginalMargins();
        $this->SetTextColor(0, 0, 0);
        //set style for cell border
        $line_width = 0.85 / $this->getScaleFactor();
        $this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        //print document barcode
        $barcode = $this->getBarcode();
        if (!empty($barcode)) {
            $this->Ln($line_width);
            $barcode_width = round(($this->getPageWidth() - $ormargins['left'] - $ormargins['right']) / 3);
            $this->write1DBarcode($barcode, 'C128B', $this->GetX(), $cur_y + $line_width, $barcode_width, (($this->getFooterMargin() / 3) - $line_width), 0.3, '', '');
        }
        if (empty($this->pagegroups)) {
            $pagenumtxt = $this->l['w_page'] . ' ' . $this->getAliasNumPage() . ' OF ' . $this->getAliasNbPages();
            $pagenumtxt = 'PAGE' . $pagenumtxt;
        } else {
            $pagenumtxt = $this->l['w_page'] . ' ' . $this->getPageNumGroupAlias() . ' OF ' . $this->getPageGroupAlias();
            $pagenumtxt = 'PAGE' . $pagenumtxt;
        }
        $this->SetY($cur_y);

        if ($this->getRTL()) {
            $this->SetX($ormargins['right']);
            if ($this->footerText) {
                // footer text and page number
                $this->Cell(0, 0, $this->footerText, 'T', 0, 'R');
                $this->Cell(0, 0, $pagenumtxt, 0, 0, 'L');
            } else {
                // page number only
                $this->Cell(0, 0, $pagenumtxt, 'T', 0, 'L');
            }
        } else {
            $this->SetX($ormargins['left']);
            if ($this->footerText) {
                // footer text and page number
                $this->Cell(0, 0, $this->footerText, 'T', 0, 'L');
                $this->Cell(0, 0, $pagenumtxt, 0, 0, 'R');
            } else {
                // page number only
                $this->Cell(0, 0, $pagenumtxt, 'T', 0, 'R');
            }
        }
    }
}
