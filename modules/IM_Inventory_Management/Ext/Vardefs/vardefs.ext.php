<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/ii_inventory_item_im_inventory_management_1_IM_Inventory_Management.php

// created: 2020-07-31 13:40:17
$dictionary["IM_Inventory_Management"]["fields"]["ii_inventory_item_im_inventory_management_1"] = array (
  'name' => 'ii_inventory_item_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'ii_inventory_item_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'ii_inventory_item_im_inventory_management_1ii_inventory_item_ida',
);

?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/m03_work_product_im_inventory_management_1_IM_Inventory_Management.php

// created: 2021-02-26 13:12:35
$dictionary["IM_Inventory_Management"]["fields"]["m03_work_product_im_inventory_management_1"] = array (
  'name' => 'm03_work_product_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'id_name' => 'm03_work_product_im_inventory_management_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["IM_Inventory_Management"]["fields"]["m03_work_product_im_inventory_management_1_name"] = array (
  'name' => 'm03_work_product_im_inventory_management_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_im_inventory_management_1m03_work_product_ida',
  'link' => 'm03_work_product_im_inventory_management_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["IM_Inventory_Management"]["fields"]["m03_work_product_im_inventory_management_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_im_inventory_management_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE_ID',
  'id_name' => 'm03_work_product_im_inventory_management_1m03_work_product_ida',
  'link' => 'm03_work_product_im_inventory_management_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/ic_inventory_collection_im_inventory_management_1_IM_Inventory_Management.php

// created: 2020-07-31 13:42:07
$dictionary["IM_Inventory_Management"]["fields"]["ic_inventory_collection_im_inventory_management_1"] = array (
  'name' => 'ic_inventory_collection_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'ic_inventory_collection_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'IC_Inventory_Collection',
  'bean_name' => 'IC_Inventory_Collection',
  'side' => 'right',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'id_name' => 'ic_inventoe24election_ida',
  'link-type' => 'one',
);
$dictionary["IM_Inventory_Management"]["fields"]["ic_inventory_collection_im_inventory_management_1_name"] = array (
  'name' => 'ic_inventory_collection_im_inventory_management_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'save' => true,
  'id_name' => 'ic_inventoe24election_ida',
  'link' => 'ic_inventory_collection_im_inventory_management_1',
  'table' => 'ic_inventory_collection',
  'module' => 'IC_Inventory_Collection',
  'rname' => 'name',
);
$dictionary["IM_Inventory_Management"]["fields"]["ic_inventoe24election_ida"] = array (
  'name' => 'ic_inventoe24election_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE_ID',
  'id_name' => 'ic_inventoe24election_ida',
  'link' => 'ic_inventory_collection_im_inventory_management_1',
  'table' => 'ic_inventory_collection',
  'module' => 'IC_Inventory_Collection',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/m01_sales_im_inventory_management_1_IM_Inventory_Management.php

// created: 2020-07-31 13:33:19
$dictionary["IM_Inventory_Management"]["fields"]["m01_sales_im_inventory_management_1"] = array (
  'name' => 'm01_sales_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'm01_sales_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'id_name' => 'm01_sales_im_inventory_management_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["IM_Inventory_Management"]["fields"]["m01_sales_im_inventory_management_1_name"] = array (
  'name' => 'm01_sales_im_inventory_management_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_im_inventory_management_1m01_sales_ida',
  'link' => 'm01_sales_im_inventory_management_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["IM_Inventory_Management"]["fields"]["m01_sales_im_inventory_management_1m01_sales_ida"] = array (
  'name' => 'm01_sales_im_inventory_management_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE_ID',
  'id_name' => 'm01_sales_im_inventory_management_1m01_sales_ida',
  'link' => 'm01_sales_im_inventory_management_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_ii_ids_hidden_c.php

 // created: 2020-12-16 11:34:00
$dictionary['IM_Inventory_Management']['fields']['ii_ids_hidden_c']['labelValue']='II Ids Hidden';
$dictionary['IM_Inventory_Management']['fields']['ii_ids_hidden_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['IM_Inventory_Management']['fields']['ii_ids_hidden_c']['enforced']='';
$dictionary['IM_Inventory_Management']['fields']['ii_ids_hidden_c']['dependency']='';
$dictionary['IM_Inventory_Management']['fields']['ii_ids_hidden_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_location_equi_serial_no_hide_c.php

 // created: 2021-04-12 09:34:31
$dictionary['IM_Inventory_Management']['fields']['location_equi_serial_no_hide_c']['labelValue']='Location Equi Sr No Hidden';
$dictionary['IM_Inventory_Management']['fields']['location_equi_serial_no_hide_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['IM_Inventory_Management']['fields']['location_equi_serial_no_hide_c']['enforced']='';
$dictionary['IM_Inventory_Management']['fields']['location_equi_serial_no_hide_c']['dependency']='';
$dictionary['IM_Inventory_Management']['fields']['location_equi_serial_no_hide_c']['required_formula']='';
$dictionary['IM_Inventory_Management']['fields']['location_equi_serial_no_hide_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_reviewed_record_c.php

 // created: 2021-04-23 18:55:27
$dictionary['IM_Inventory_Management']['fields']['reviewed_record_c']['labelValue']='Reviewed Record';
$dictionary['IM_Inventory_Management']['fields']['reviewed_record_c']['dependency']='';
$dictionary['IM_Inventory_Management']['fields']['reviewed_record_c']['required_formula']='';
$dictionary['IM_Inventory_Management']['fields']['reviewed_record_c']['readonly_formula']='';
$dictionary['IM_Inventory_Management']['fields']['reviewed_record_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_processed_per_protocol_c.php

 // created: 2021-07-09 12:37:43
$dictionary['IM_Inventory_Management']['fields']['processed_per_protocol_c']['labelValue']='Processed Per Protocol';
$dictionary['IM_Inventory_Management']['fields']['processed_per_protocol_c']['required_formula']='';
$dictionary['IM_Inventory_Management']['fields']['processed_per_protocol_c']['readonly_formula']='';
$dictionary['IM_Inventory_Management']['fields']['processed_per_protocol_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_composition.php

 // created: 2021-02-10 10:41:26
$dictionary['IM_Inventory_Management']['fields']['composition']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['composition']['visibility_grid']=array (
  'trigger' => 'related_to',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Internal Use' => 
    array (
      0 => 'Inventory Item',
    ),
    'Sales' => 
    array (
      0 => 'Inventory Item',
    ),
    'Work Product' => 
    array (
      0 => '',
      1 => 'Inventory Item',
      2 => 'Inventory Collection',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/InventoryManagementCustomizations.php


$dictionary['IM_Inventory_Management']['fields']['name']['readonly'] = true;
$dictionary['IM_Inventory_Management']['fields']['name']['required'] = false;

$dictionary['IM_Inventory_Management']['fields']['duration_hours']['readonly'] = true;

$dictionary['IM_Inventory_Management']['fields']['im_inactive_c'] = array(
    'labelValue' => 'LBL_IM_INACTIVE',
    'source'     => 'custom_fields',
    'name'       => 'im_inactive_c',
    'vname'      => 'LBL_IM_INACTIVE',
    'type'       => 'bool',
    'importable' => 'true',
    'audited'    => false,
    'reportable' => true,
    'default'    => false,
    'size'       => '20',
);

$dictionary['IM_Inventory_Management']['fields']['inventory_management_num_c'] = array(
    'labelValue'                => 'LBL_INVENTORY_MANAGEMENT_NUM',
    'required'                  => false,
    'source'                    => 'custom_fields',
    'name'                      => 'inventory_management_num_c',
    'vname'                     => 'LBL_INVENTORY_MANAGEMENT_NUM',
    'type'                      => 'int',
    'massupdate'                => false,
    'importable'                => 'true',
    'duplicate_merge'           => 'enabled',
    'duplicate_merge_dom_value' => 1,
    'reportable'                => true,
    'len'                       => 11,
    'size'                      => '20',
    'min'                       => false,
    'max'                       => false,
);

?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_end_datetime.php

 // created: 2022-04-05 08:50:16
$dictionary['IM_Inventory_Management']['fields']['end_datetime']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['end_datetime']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_duration_hours.php

 // created: 2022-04-05 08:50:57
$dictionary['IM_Inventory_Management']['fields']['duration_hours']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['duration_hours']['readonly']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_end_condition_acceptable.php

 // created: 2022-04-05 08:52:10
$dictionary['IM_Inventory_Management']['fields']['end_condition_acceptable']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['end_condition_acceptable']['dependency']='isInList($action,createList("Archive Offsite","Discard","External Transfer"))';
$dictionary['IM_Inventory_Management']['fields']['end_condition_acceptable']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_end_condition_comments.php

 // created: 2022-04-05 08:53:36
$dictionary['IM_Inventory_Management']['fields']['end_condition_comments']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['end_condition_comments']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_location_type.php

 // created: 2022-04-05 08:54:19
$dictionary['IM_Inventory_Management']['fields']['location_type']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['location_type']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_location_building.php

 // created: 2022-04-05 08:55:07
$dictionary['IM_Inventory_Management']['fields']['location_building']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['location_building']['visibility_grid']=array (
  'trigger' => 'action',
  'values' => 
  array (
    'Initial Storage' => 
    array (
      0 => '',
      1 => '8945',
      2 => '8960',
      3 => '9055',
    ),
    'In Use' => 
    array (
      0 => '',
      1 => '780',
      2 => '8945',
      3 => '8960',
      4 => '9055',
    ),
    'Processing' => 
    array (
    ),
    'Internal Transfer' => 
    array (
      0 => '',
      1 => '780',
      2 => '8945',
      3 => '8960',
      4 => '9055',
    ),
    'External Transfer' => 
    array (
    ),
    'Archive Offsite' => 
    array (
    ),
    'Archive Onsite' => 
    array (
      0 => '',
      1 => '780',
    ),
    'Discard' => 
    array (
    ),
    'Obsolete' => 
    array (
    ),
    '' => 
    array (
    ),
    'Separate Inventory Items' => 
    array (
      0 => '',
      1 => '780',
      2 => '8945',
      3 => '8960',
      4 => '9055',
    ),
    'Create Inventory Collection' => 
    array (
    ),
  ),
);
$dictionary['IM_Inventory_Management']['fields']['location_building']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_equip_equipment_id_c.php

 // created: 2022-04-05 08:58:10
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['name']='equip_equipment_id_c';
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['vname']='LBL_LOCATION_EQUIPMENT_EQUIP_EQUIPMENT_ID';
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['type']='id';
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['massupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['no_default']=false;
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['comments']='';
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['help']='';
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['importable']='true';
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['duplicate_merge']='enabled';
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['duplicate_merge_dom_value']=1;
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['audited']=false;
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['reportable']=false;
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['unified_search']=false;
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['merge_filter']='disabled';
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['pii']=false;
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['calculated']=false;
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['len']=36;
$dictionary['IM_Inventory_Management']['fields']['equip_equipment_id_c']['size']='20';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_location_equipment.php

 // created: 2022-04-05 08:58:10
$dictionary['IM_Inventory_Management']['fields']['location_equipment']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['location_equipment']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['location_equipment']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_rms_room_id_c.php

 // created: 2022-04-05 08:58:50
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['name']='rms_room_id_c';
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['vname']='LBL_LOCATION_ROOM_RMS_ROOM_ID';
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['type']='id';
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['massupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['no_default']=false;
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['comments']='';
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['help']='';
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['importable']='true';
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['duplicate_merge']='enabled';
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['duplicate_merge_dom_value']=1;
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['audited']=false;
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['reportable']=false;
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['unified_search']=false;
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['merge_filter']='disabled';
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['pii']=false;
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['calculated']=false;
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['len']=36;
$dictionary['IM_Inventory_Management']['fields']['rms_room_id_c']['size']='20';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_location_room.php

 // created: 2022-04-05 08:58:50
$dictionary['IM_Inventory_Management']['fields']['location_room']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['location_room']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['location_room']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_location_shelf.php

 // created: 2022-04-05 08:59:56
$dictionary['IM_Inventory_Management']['fields']['location_shelf']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['location_shelf']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_location_cabinet.php

 // created: 2022-04-05 09:00:25
$dictionary['IM_Inventory_Management']['fields']['location_cabinet']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['location_cabinet']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_ship_to_contact.php

 // created: 2022-04-05 09:01:08
$dictionary['IM_Inventory_Management']['fields']['ship_to_contact']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['ship_to_contact']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['ship_to_contact']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_contact_id_c.php

 // created: 2022-04-05 09:01:08
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['name']='contact_id_c';
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['vname']='LBL_SHIP_TO_CONTACT_CONTACT_ID';
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['type']='id';
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['massupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['no_default']=false;
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['comments']='';
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['help']='';
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['importable']='true';
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['duplicate_merge']='enabled';
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['duplicate_merge_dom_value']=1;
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['audited']=false;
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['reportable']=false;
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['unified_search']=false;
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['merge_filter']='disabled';
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['pii']=false;
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['calculated']=false;
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['len']=36;
$dictionary['IM_Inventory_Management']['fields']['contact_id_c']['size']='20';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_account_id_c.php

 // created: 2022-04-05 09:01:45
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['name']='account_id_c';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['vname']='LBL_SHIP_TO_COMPANY_ACCOUNT_ID';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['type']='id';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['massupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['no_default']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['comments']='';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['help']='';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['importable']='true';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['duplicate_merge']='enabled';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['duplicate_merge_dom_value']=1;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['audited']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['reportable']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['unified_search']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['merge_filter']='disabled';
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['pii']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['calculated']=false;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['len']=36;
$dictionary['IM_Inventory_Management']['fields']['account_id_c']['size']='20';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_ship_to_company.php

 // created: 2022-04-05 09:01:45
$dictionary['IM_Inventory_Management']['fields']['ship_to_company']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['ship_to_company']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['ship_to_company']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_ca_company_address_id_c.php

 // created: 2022-04-05 09:02:19
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['name']='ca_company_address_id_c';
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['vname']='LBL_SHIP_TO_ADDRESS_CA_COMPANY_ADDRESS_ID';
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['type']='id';
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['massupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['no_default']=false;
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['comments']='';
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['help']='';
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['importable']='true';
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['duplicate_merge']='enabled';
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['duplicate_merge_dom_value']=1;
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['audited']=false;
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['reportable']=false;
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['unified_search']=false;
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['merge_filter']='disabled';
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['pii']=false;
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['calculated']=false;
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['len']=36;
$dictionary['IM_Inventory_Management']['fields']['ca_company_address_id_c']['size']='20';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_ship_to_address.php

 // created: 2022-04-05 09:02:19
$dictionary['IM_Inventory_Management']['fields']['ship_to_address']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['ship_to_address']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['ship_to_address']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_category.php

 // created: 2022-04-05 09:04:22
$dictionary['IM_Inventory_Management']['fields']['category']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['category']['visibility_grid']=array (
  'trigger' => 'related_to',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Internal Use' => 
    array (
      0 => '',
      1 => 'Product',
      2 => 'Record',
      3 => 'Specimen',
      4 => 'Study Article',
    ),
    'Sales' => 
    array (
      0 => '',
      1 => 'Product',
      2 => 'Record',
      3 => 'Specimen',
      4 => 'Study Article',
    ),
    'Work Product' => 
    array (
      0 => '',
      1 => 'Product',
      2 => 'Record',
      3 => 'Specimen',
      4 => 'Study Article',
    ),
    'Inventory Collection' => 
    array (
      0 => 'Specimen',
    ),
  ),
);
$dictionary['IM_Inventory_Management']['fields']['category']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_type_2.php

 // created: 2022-04-05 09:04:51
$dictionary['IM_Inventory_Management']['fields']['type_2']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['type_2']['dependency']=false;
$dictionary['IM_Inventory_Management']['fields']['type_2']['options']='universal_inventory_management_type_list';
$dictionary['IM_Inventory_Management']['fields']['type_2']['visibility_grid']=array (
  'trigger' => 'category',
  'values' => 
  array (
    'Product' => 
    array (
      0 => '',
      1 => 'Autoclave supplies',
      2 => 'Balloon Catheter',
      3 => 'Bandaging',
      4 => 'Blood draw supply',
      5 => 'Cannula',
      6 => 'Catheter',
      7 => 'Chemical',
      8 => 'Cleaning Agent',
      9 => 'Cleaning Supplies',
      10 => 'NA Heparin Plasma',
      11 => 'Contrast',
      12 => 'Drug',
      13 => 'Endotracheal TubeSupplies',
      14 => 'Enrichment Toy',
      15 => 'Equipment',
      16 => 'ETO supplies',
      17 => 'Feeding supplies',
      18 => 'Graft',
      19 => 'Inflation Device',
      20 => 'Office Supply',
      21 => 'Other',
      22 => 'Reagent',
      23 => 'Sheath',
      24 => 'Shipping supplies',
      25 => 'Solution',
      26 => 'Stent',
      27 => 'Surgical Instrument',
      28 => 'Surgical site prep',
      29 => 'Suture',
      30 => 'Tubing',
      31 => 'Wire',
    ),
    'Record' => 
    array (
      0 => '',
      1 => 'Data Book',
      2 => 'Data Sheet',
      3 => 'Equipment Facility Record',
      4 => 'Hard Drive',
      5 => 'Protocol Book',
    ),
    'Specimen' => 
    array (
    ),
    'Study Article' => 
    array (
      0 => '',
      1 => 'Accessory Article',
      2 => 'Control Article',
      3 => 'Test Article',
    ),
    '' => 
    array (
    ),
  ),
);
$dictionary['IM_Inventory_Management']['fields']['type_2']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_processing_method.php

 // created: 2022-04-05 09:05:41
$dictionary['IM_Inventory_Management']['fields']['processing_method']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['processing_method']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_external_receipt_date.php

 // created: 2022-04-05 09:06:41
$dictionary['IM_Inventory_Management']['fields']['external_receipt_date']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_start_datetime.php

 // created: 2022-04-05 09:10:01
$dictionary['IM_Inventory_Management']['fields']['start_datetime']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['start_datetime']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_m03_work_product_im_inventory_management_1m03_work_product_ida.php

 // created: 2022-04-05 09:14:18
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['name']='m03_work_product_im_inventory_management_1m03_work_product_ida';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['type']='id';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['source']='non-db';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['vname']='LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE_ID';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['id_name']='m03_work_product_im_inventory_management_1m03_work_product_ida';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['link']='m03_work_product_im_inventory_management_1';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['table']='m03_work_product';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['module']='M03_Work_Product';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['rname']='id';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['reportable']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['side']='right';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['massupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['duplicate_merge']='disabled';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['hideacl']=true;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['audited']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1m03_work_product_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_m03_work_product_im_inventory_management_1_name.php

 // created: 2022-04-05 09:14:20
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['audited']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['massupdate']=true;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['duplicate_merge']='enabled';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['duplicate_merge_dom_value']='1';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['merge_filter']='disabled';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['reportable']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['unified_search']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['calculated']=false;
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['dependency']='or(equal($related_to,"Work Product"),equal($related_to,"Inventory Collection"))';
$dictionary['IM_Inventory_Management']['fields']['m03_work_product_im_inventory_management_1_name']['vname']='LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_location_rack_c.php

 // created: 2022-04-05 09:17:08
$dictionary['IM_Inventory_Management']['fields']['location_rack_c']['labelValue']='Location (Freezer Rack #)';
$dictionary['IM_Inventory_Management']['fields']['location_rack_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['IM_Inventory_Management']['fields']['location_rack_c']['enforced']='';
$dictionary['IM_Inventory_Management']['fields']['location_rack_c']['dependency']='equal($location_equi_serial_no_hide_c,"51014222")';
$dictionary['IM_Inventory_Management']['fields']['location_rack_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_location_bin_c.php

 // created: 2022-04-05 09:17:52
$dictionary['IM_Inventory_Management']['fields']['location_bin_c']['labelValue']='Location (Bin #)';
$dictionary['IM_Inventory_Management']['fields']['location_bin_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['IM_Inventory_Management']['fields']['location_bin_c']['enforced']='';
$dictionary['IM_Inventory_Management']['fields']['location_bin_c']['dependency']='equal($location_equi_serial_no_hide_c,"51014222")';
$dictionary['IM_Inventory_Management']['fields']['location_bin_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_type_specimen_c.php

 // created: 2022-04-05 09:18:48
$dictionary['IM_Inventory_Management']['fields']['type_specimen_c']['labelValue']='Type (Specimen)';
$dictionary['IM_Inventory_Management']['fields']['type_specimen_c']['dependency']='equal($category,"Specimen")';
$dictionary['IM_Inventory_Management']['fields']['type_specimen_c']['readonly_formula']='';
$dictionary['IM_Inventory_Management']['fields']['type_specimen_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_location_cubby_c.php

 // created: 2022-04-05 09:19:28
$dictionary['IM_Inventory_Management']['fields']['location_cubby_c']['labelValue']='Location (Slot #)';
$dictionary['IM_Inventory_Management']['fields']['location_cubby_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['IM_Inventory_Management']['fields']['location_cubby_c']['enforced']='';
$dictionary['IM_Inventory_Management']['fields']['location_cubby_c']['dependency']='equal($location_equi_serial_no_hide_c,"51014222")';
$dictionary['IM_Inventory_Management']['fields']['location_cubby_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_processing_transfer_condition.php

 // created: 2022-04-05 09:21:23
$dictionary['IM_Inventory_Management']['fields']['processing_transfer_condition']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['processing_transfer_condition']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_end_condition.php

 // created: 2022-04-05 09:22:20
$dictionary['IM_Inventory_Management']['fields']['end_condition']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['end_condition']['options']='inventory_item_storage_condition_list';
$dictionary['IM_Inventory_Management']['fields']['end_condition']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_ending_storage_media.php

 // created: 2022-04-05 10:07:29
$dictionary['IM_Inventory_Management']['fields']['ending_storage_media']['required']=false;
$dictionary['IM_Inventory_Management']['fields']['ending_storage_media']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['ending_storage_media']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Vardefs/sugarfield_action.php

 // created: 2022-05-19 07:22:22
$dictionary['IM_Inventory_Management']['fields']['action']['hidemassupdate']=false;
$dictionary['IM_Inventory_Management']['fields']['action']['visibility_grid']=array (
  'trigger' => 'composition',
  'values' => 
  array (
    'Inventory Collection' => 
    array (
      0 => '',
      1 => 'Archive Offsite',
      2 => 'Archive Onsite',
      3 => 'Create Inventory Collection',
      4 => 'Discard',
      5 => 'Exhausted',
      6 => 'External Transfer',
      7 => 'Internal Transfer Analysis',
      8 => 'Internal Transfer',
      9 => 'Missing',
      10 => 'Separate Inventory Items',
    ),
    'Inventory Item' => 
    array (
      0 => '',
      1 => 'Archive Offsite',
      2 => 'Archive Onsite',
      3 => 'Discard',
      4 => 'Exhausted',
      5 => 'External Transfer',
      6 => 'Internal Transfer Analysis',
      7 => 'Internal Transfer',
      8 => 'Missing',
      9 => 'Obsolete',
    ),
    '' => 
    array (
    ),
  ),
);

 
?>
