<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Dependencies/InventoryManagementCustomizations.php


$dependencies['IM_Inventory_Management']['related_to_visibility_dep'] = array(
    'hooks'         => array("edit"),
    'triggerFields' => array('related_to'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'm01_sales_im_inventory_management_1_name',
                'value'  => 'equal($related_to, "Sales")',
            ),
        ),
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_im_inventory_management_1_name',
                'value'  => 'or(equal($related_to, "Work Product"),equal($related_to, "Inventory Collection"))',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_im_inventory_management_1_name',
                'value'  => 'or(equal($related_to, "Work Product"),equal($related_to, "Inventory Collection"))',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'm01_sales_im_inventory_management_1_name',
                'value'  => 'equal($related_to, "Sales")',
            ),
        ),
    ),
);

$dependencies['IM_Inventory_Management']['ship_to_fields_visibility_dep'] = array(
    'hooks'         => array("all"),
    'triggerFields' => array('action'),
    'onload'        => true,
    'actions'       => array(        
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ship_to_contact',
                'value'  => 'isInList($action, createList("Archive Offsite","External Transfer"))',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ship_to_company',
                'value'  => 'isInList($action, createList("Archive Offsite","External Transfer"))',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ship_to_address',
                'value'  => 'isInList($action, createList("Archive Offsite","External Transfer"))',
            ),
        ),
    ),
);

$dependencies['IM_Inventory_Management']['location_equipment_visibility_dep'] = array(
    'hooks'         => array("edit"),
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => 'equal($location_type,"Equipment")',
            ),
        ),
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'location_room',
                'value'  => 'isInList($location_type,createList("Room","RoomCabinet","RoomShelf"))',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => 'equal($location_type,"Equipment")',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'location_room',
                'value'  => 'isInList($location_type,createList("Room","RoomCabinet","RoomShelf"))',
            ),
        ),
    ),
);

$dependencies['IM_Inventory_Management']['inventory_collection'] = array(
    'hooks'         => array("all"),
    'triggerFields' => array('composition','action','ic_inventory_collection_im_inventory_management_1_name'),
    'onload'        => true,
    'actions'       => array(
		array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ic_inventory_collection_im_inventory_management_1_name',
                'value'  => 'and(equal($composition, "Inventory Collection"), isInList($action, createList("Archive Offsite", "Archive Onsite", "External Transfer", "Discard", "Internal Transfer", "Separate Inventory Items","Exhausted")))'
            ),
        ),   
		array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'ic_inventory_collection_im_inventory_management_1_name',
                'value'  => 'and(equal($composition, "Inventory Collection"), isInList($action, createList("Archive Offsite", "Archive Onsite", "External Transfer", "Discard", "Internal Transfer", "Separate Inventory Items","Exhausted")))'
            ),
        ),		
				
	),
); 

$dependencies['IM_Inventory_Management']['inventory_collection_visibility_dep'] = array(
    'hooks'         => array("edit"),
    'triggerFields' => array('id'),
    'onload'        => true,
    'actions'       => array(     
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ic_inventory_collection_im_inventory_management_1_name',
                'value'  => 'not(equal($id, ""))',
            ),
        ),
     ),
);
$dependencies['IM_Inventory_Management']['inventory_mgmt_fields_blank'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'and(not(equal($action,"Archive Offsite")),not(equal($action,"External Transfer")))',
    'triggerFields' => array('action'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'contact_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'ca_company_address_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'account_id_c',
                'value'  => '',
            ),
        ),    
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'ship_to_contact',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'ship_to_company',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'ship_to_address',
                'value'  => '',
            ),
        ),
       
     ),
);

$dependencies['IM_Inventory_Management']['inventory_mgmt_Locations_fields_blank'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($action, createList("Archive Offsite"))',
    'triggerFields' => array('action'),
    'onload'        => true,
    'actions'       => array( 
         /*array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_type',
                'value'  => '',
            ),
        ),*/
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'rms_room_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),    
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_room',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
       
     ),
);

/*#391 : 08 March 2021 */
$dependencies['IM_Inventory_Management']['cabinet_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Cabinet"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'rms_room_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_room',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['IM_Inventory_Management']['equipement_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Equipment"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'rms_room_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_room',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['IM_Inventory_Management']['shelf_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Shelf"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'rms_room_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_room',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['IM_Inventory_Management']['room_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Room"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['IM_Inventory_Management']['roomshelf_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("RoomShelf"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['IM_Inventory_Management']['roomscabinet_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("RoomCabinet"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
       
     ),
);


?>
