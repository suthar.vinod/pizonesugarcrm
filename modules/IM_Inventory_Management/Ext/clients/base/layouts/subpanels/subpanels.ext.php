<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-07-31 13:40:17
$viewdefs['IM_Inventory_Management']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_II_INVENTORY_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'ii_inventory_item_im_inventory_management_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['IM_Inventory_Management']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'ii_inventory_item_im_inventory_management_1',
  'view' => 'subpanel-for-im_inventory_management-ii_inventory_item_im_inventory_management_1',
);
