<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Layoutdefs/ii_inventory_item_im_inventory_management_1_IM_Inventory_Management.php

 // created: 2021-11-09 09:39:34
$layout_defs["IM_Inventory_Management"]["subpanel_setup"]['ii_inventory_item_im_inventory_management_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'ii_inventory_item_im_inventory_management_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Layoutdefs/_overrideIM_Inventory_Management_subpanel_ii_inventory_item_im_inventory_management_1.php

//auto-generated file DO NOT EDIT
$layout_defs['IM_Inventory_Management']['subpanel_setup']['ii_inventory_item_im_inventory_management_1']['override_subpanel_name'] = 'IM_Inventory_Management_subpanel_ii_inventory_item_im_inventory_management_1';

?>
