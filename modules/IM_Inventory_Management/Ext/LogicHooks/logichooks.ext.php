<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/LogicHooks/InventoryManagementCustomizationsHooks.php


$hook_array['before_save'][] = array(
    77,
    'Inventory Management Record Customizations',
    null,
    'Sugarcrm\\Sugarcrm\\custom\\src\\wsystems\\InventoryManagementCustomizations\\LogicHooks\\InventoryManagementCustomizationsHook',
    'beforeSaveMethod',
);

$hook_array['after_relationship_add'][] = array(
    78,
    'Inventory Management Record Customizations',
    null,
    'Sugarcrm\\Sugarcrm\\custom\\src\\wsystems\\InventoryManagementCustomizations\\LogicHooks\\InventoryManagementCustomizationsHook',
    'afterRelationshipAddMethod',
);

$hook_array['before_filter'][] = array(
    78,
    'Before Filter Inventory Management Records',
    null,
    'Sugarcrm\\Sugarcrm\\custom\\src\\wsystems\\InventoryManagementCustomizations\\LogicHooks\\InventoryManagementCustomizationsHook',
    'beforeFilterMethod',
);

?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/LogicHooks/CalculateItemStatus.php


$hook_array['after_save'][] = array(
    //Processing index. For sorting the array.
    1,
    //Label. A string value to identify the hook.
    'After Save Change Status',

    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateMgtStatus.php',

    //The class the method is in.
    'CalculateMgtStatus',

    //The method to call.
    'calculateAfterSave',
);
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/LogicHooks/PopulateInventorySubpanelsFromWorkProduct.php


$hook_array['after_relationship_delete'][] = array(
    1,
    'Delete record on unlink action from m03_work_product_im_inventory_management_2 relationship ',
    null,
    'Sugarcrm\\Sugarcrm\\custom\\wsystems\\PopulateInventorySubpanelsFromWorkProduct\\LogicHooks\\IMDeleteRelationshipHook',
    'deleteIMSubpanelRelationship',
);

?>
