<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Language/en_us.customii_inventory_item_im_inventory_management_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE'] = 'Inventory Items';

?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Language/en_us.customm03_work_product_im_inventory_management_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Language/en_us.customic_inventory_collection_im_inventory_management_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IC_INVENTORY_COLLECTION_TITLE'] = 'Inventory Collections';
$mod_strings['LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE'] = 'Inventory Collections';

?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Language/en_us.customm01_sales_im_inventory_management_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Language/en_us.InventoryManagementCustomizations.php


$mod_strings['LBL_INVENTORY_MANAGEMENT_NUM'] = 'Inventory Management #';
$mod_strings['LBL_IM_INACTIVE']              = 'Inactive';

?>
<?php
// Merged from custom/Extension/modules/IM_Inventory_Management/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_II_IDS_HIDDEN_C'] = 'II Ids Hidden';
$mod_strings['LBL_LOCATION_EQUI_SERIAL_NO_HIDE'] = 'Location Equi Sr No Hidden';
$mod_strings['LBL_LOCATION_BIN'] = 'Location (Bin #)';
$mod_strings['LBL_REVIEWED_RECORD'] = 'Reviewed Record';
$mod_strings['LBL_LOCATION_CUBBY'] = 'Location (Slot #)';
$mod_strings['LBL_PROCESSED_PER_PROTOCOL'] = 'Processed Per Protocol';
$mod_strings['LBL_LOCATION_RACK'] = 'Location (Freezer Rack #)';
$mod_strings['LBL_END_CONDITION_ACCEPTABLE'] = 'Physical Condition Acceptable';
$mod_strings['LBL_LOCATION_SHELF'] = 'Location (Room Shelf)';
$mod_strings['LBL_TYPE_2'] = 'Type (non-Specimen)';
$mod_strings['LBL_TYPE_SPECIMEN'] = 'Type (Specimen)';
$mod_strings['LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_NAME_FIELD_TITLE'] = 'Work Products';

?>
