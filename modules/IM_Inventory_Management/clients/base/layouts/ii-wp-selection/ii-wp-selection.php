<?php

$viewdefs['IM_Inventory_Management']['base']['layout']['ii-wp-selection'] = array(
    'type' => 'selection-list',
    'components' => array(
        array(
            'layout' => array(
                'type' => 'base',
                'name' => 'main-pane',
                'css_class' => 'main-pane span12',
                'components' => array(
                    /* array(
                      'view' => 'selection-list-context',
                      ), */
                    array(
                        'layout' => array(
                            'type' => 'filterpanel',
                            'availableToggles' => array(),
                            'filter_options' => array(
                                'stickiness' => false,
                            ),
                            'components' => array(
                                 array(
                                  'layout'     => 'filter',
                                  'loadModule' => 'Filters',
                                  ),
                                  array(
                                  'view' => 'filter-rows',
                                  ),
                                  array(
                                  'view' => 'filter-actions',
                                  ), 
                                array(
                                    'view' => 'ii-wp-selection',
                                    'context'    => array(
                                        'module' => 'II_Inventory_Item',
                                    ),
                                    'loadModule' => 'IM_Inventory_Management',
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);
