({
    extendsFrom: 'RelateField',
    initialize: function (options) {
        this._super('initialize', [options]);

    },
    openSelectDrawer: function () {
        var action = this.model.get('action');
        if (this.model.get('location_type') == "Equipment" && !_.isEmpty(this.model.get('location_building')) && this.model.get('location_building') != null) {
            this.selectLocation();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else if (this.getSearchModule() == "IC_Inventory_Collection" && this.model.get('m03_work_product_im_inventory_management_1_name') != "" && this.model.get('m03_work_product_im_inventory_management_1_name') != null && (action == "Processing" || action == "Internal Transfer" || action == "External Transfer" || action == "Archive Offsite" || action == "Archive Onsite" || action == "Discard" || action == "Obsolete" || action == "Separate Inventory Items" || action == "Exhausted")) {
            this.selectInventory_Collection();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else if (this.getSearchModule() == "RMS_Room" && this.model.get('location_building') != "" && this.model.get('location_building') != null) {
            this.selectRooms();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else {
            this._super('openSelectDrawer');
            $("button.btn.btn-invisible.btn-dark").css("display", "block");
            $(".filter-definition-container").css('pointer-events', 'unset');
            $(".choice-filter-clickable").css('pointer-events', 'unset');

        }
    },
    selectInventory_Collection: function () {
        var work_product_id = this.model.get('m03_work_product_im_inventory_management_1m03_work_product_ida');
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterInventoryCollectionTemplate',
                'initial_filter_label': 'Inventory Collection',
                'filter_populate': {
                    'm03_work_product_ic_inventory_collection_1_name': [work_product_id],
                },
                'filter_relate': {
                    'm03_work_product_ic_inventory_collection_1_name': [work_product_id],
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "IC_Inventory_Collection") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    selectRooms: function () {
        var location_building = this.model.get('location_building');
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterRMS_RoomTemplate',
                'initial_filter_label': 'Location(Rooms)',
                'filter_populate': {
                    'building_c': location_building,
                },
                'filter_relate': {
                    'building_c': location_building,
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "RMS_Room") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    selectLocation: function () {
        var location_building = this.model.get('location_building');
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterEquipmentTemplateLocation',
                'initial_filter_label': 'Location(Building)',
                'filter_populate': {
                    'building_c': location_building,
                },
                'filter_relate': {
                    'building_c': location_building,
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "Equip_Equipment") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    /*To disable search text box filter of Test System */
    search: _.debounce(function (query) {
        var term = query.term || '',
            self = this,
            searchModule = this.getSearchModule(),
            params = {},
            limit = self.def.limit || 5,
            relatedModuleField = this.getRelatedModuleField();

        if (query.context) {
            params.offset = this.searchCollection.next_offset;
        }

        params.filter = this.buildFilterDefinition(term);

        this.searchCollection.fetch({
            //Don't show alerts for this request
            showAlerts: false,
            update: true,
            remove: _.isUndefined(params.offset),
            fields: this.getSearchFields(),
            context: self,
            params: params,
            //limit: limit,
            success: function (data) {
                var fetch = { results: [], more: data.next_offset > 0, context: data };
                if (fetch.more) {
                    var fieldEl = self.$(self.fieldTag),
                        //For teamset widget, we should specify which index element to be filled in
                        plugin = (fieldEl.length > 1) ? $(fieldEl.get(self.currentIndex)).data("select2") : fieldEl.data("select2"),
                        height = plugin.searchmore.children("li:first").children(":first").outerHeight(),
                        //0.2 makes scroll not to touch the bottom line which avoid fetching next record set
                        maxHeight = height * (limit - .2);
                    plugin.results.css("max-height", maxHeight);
                }
                _.each(data.models, function (model, index) {
                    if (params.offset && index < params.offset) {
                        return;
                    }
                    fetch.results.push({
                        id: model.id,
                        text: model.get(relatedModuleField) + ''
                    });
                });
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback(fetch);
                }
            },
            error: function () {
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback({ results: [] });
                }
                app.logger.error("Unable to fetch the bean collection.");
            }
        });

    }, app.config.requiredElapsed || 500),
    _getEquipEquipment: function () {
        var location_building = this.model.get('location_building');
        return [{
            '$or': [
                { 'building_c': location_building }
            ]
        }];
    },
    _getCustomFilterRoom: function () {
        var location_building = this.model.get('location_building');
        return [{
            '$or': [
                { 'building_c': location_building }
            ]
        }];
    },
    _getCustomFilterIC: function (searchTerm) {
        var work_product_id = this.model.get('m03_work_product_im_inventory_management_1_name');
        if (searchTerm.startsWith("APS IC")) {
            app.api.call(
                "read",
                "rest/v10/IC_Inventory_Collection?fields=id,name,internal_barcode&filter[0][internal_barcode][$equals]=" + searchTerm,
                null,
                {
                    success: function successFunc(data) {
                        if (data.records != "") {
                            this.model.set("ic_inventoe24election_ida", data.records[0].id);
                            this.model.set('ic_inventory_collection_im_inventory_management_1_name', data.records[0].name);
                        } else {
                            this.model.set("ic_inventoe24election_ida", "");
                            this.model.set('ic_inventory_collection_im_inventory_management_1_name', "");

                        }

                    }.bind(this),
                    error: function (error) {

                    }
                });
            return [];

        } else {
            return [{
                '$or': [
                    { 'm03_work_product_ic_inventory_collection_1_name': work_product_id }

                ]
            }];
        }
    },
    /**location_equipment
     * @override
     */
    buildFilterDefinition: function (searchTerm) {
        var action = this.model.get('action');
        var parentFilter = this._super('buildFilterDefinition', [searchTerm]);
        if (this.getSearchModule() == 'Equip_Equipment' && this.model.get('location_building') != "" && this.model.get('location_building') != null) {
            return parentFilter.concat(this._getEquipEquipment());
        } else if (this.getSearchModule() == 'RMS_Room' && this.model.get('location_building') != "" && this.model.get('location_building') != null) {
            return parentFilter.concat(this._getCustomFilterRoom());
        } else if (this.getSearchModule() == "IC_Inventory_Collection" && this.model.get('m03_work_product_im_inventory_management_1_name') != "" && this.model.get('m03_work_product_im_inventory_management_1_name') != null && (action == "Processing" || action == "Internal Transfer" || action == "External Transfer" || action == "Archive Offsite" || action == "Archive Onsite" || action == "Discard" || action == "Obsolete" || action == "Separate Inventory Items" || action == "Exhausted")) {
            return parentFilter.concat(this._getCustomFilterIC(searchTerm));
        } else {
            return parentFilter;
        }
    },
    /**EOC Disable search filter code */
})