({
  extendsFrom: 'CreateView',
  initialize: function (options) {

    this._super('initialize', [options]);
    this.model.on('change:location_type', this.function_location_type_change, this);
    this.model.on('change:location_building', this.function_location_building_change, this);
    this.model.on('change:location_equipment', this.function_location_equipment, this);
    this.model.addValidationTask('check_im_field', _.bind(this._doValidateimFiled, this));  
    this.model.on('change:related_to change:category change:type_2 change:action change:location_type change:location_equi_serial_no_hide_c change:type_specimen_c', this.function_im_fields, this);
    this.on('render', _.bind(this.onload_function, this));
  },

  onload_function: function () {
    var self = this;
    var type_specimen = this.model.get('type_specimen_c');    
    console.log('type_specimen : OUT Create', type_specimen);            
    if (_.contains(type_specimen, "Tissue") || _.contains(this.model.get('type_specimen_c'), "Tissue")) {
        console.log('type_specimen : IN Create', type_specimen);
        $('[data-name="ending_storage_media"]').css("visibility", "visible"); 
        $('[data-name="ending_storage_media"]').css("height", "auto"); 
    } else {
        $('[data-name="ending_storage_media"]').css("visibility", "hidden"); 
        $('[data-name="ending_storage_media"]').css("height", "0"); 
        setTimeout(function () {
          self.$el.find('input[name="ending_storage_media"]').prop('disabled', true);
        }, 1000);
    }
    this._render();
  },

  _doValidateimFiled: function(fields, errors, callback) {
		var self = this;
		var related_to = this.model.get('related_to');
        var category = this.model.get('category');  
        var type_2 = this.model.get('type_2');
        var type_specimen = this.model.get('type_specimen_c');
        var action = this.model.get('action');
        var location_type = this.model.get('location_type');
        var location_equi_serial_no_hide_c = this.model.get('location_equi_serial_no_hide_c');
        var duration_hours = this.model.get('duration_hours');
        var end_condition_acceptable = this.model.get('end_condition_acceptable');
        var wp_name = this.model.get('m03_work_product_im_inventory_management_1_name')
        var sales_name = this.model.get('m01_sales_im_inventory_management_1_name')
        console.log('sales_name : ', sales_name);
        console.log('wp_name : ', wp_name);
		//validate requirements
		    
    if (related_to == 'Sales'  && ( sales_name == ''  || sales_name == null || sales_name == undefined)) {
        errors['m01_sales_im_inventory_management_1_name'] = errors['m01_sales_im_inventory_management_1_name'] || {};
        errors['m01_sales_im_inventory_management_1_name'].required = true;
    } 
    if (related_to == 'Work Product'  && ( wp_name == '' || wp_name == null || wp_name == undefined)) {
      errors['m03_work_product_im_inventory_management_1_name'] = errors['m03_work_product_im_inventory_management_1_name'] || {};
	    errors['m03_work_product_im_inventory_management_1_name'].required = true;
    }    

    if (category != 'Specimen' && _.isEmpty(this.model.get('type_2'))) {              
    	errors['type_2'] = errors['type_2'] || {};
		  errors['type_2'].required = true;
    }
    if (category == 'Specimen' && _.isEmpty(this.model.get('type_specimen_c'))) {
        errors['type_specimen_c'] = errors['type_specimen_c'] || {};
		    errors['type_specimen_c'].required = true;
    }
    if (category != 'Specimen' && _.isEmpty(this.model.get('type_2'))) {              
    	errors['type_2'] = errors['type_2'] || {};
		  errors['type_2'].required = true;
    } 
    console.log('type_specimen : ', type_specimen);
    if (_.contains(type_specimen, "Tissue") && _.isEmpty(this.model.get('ending_storage_media'))) {
        console.log('type_specimen in: ', type_specimen);
        errors['ending_storage_media'] = errors['ending_storage_media'] || {};
		    errors['ending_storage_media'].required = true;
    }   
    /* if ((action == 'Archive Offsite' || action == 'External Transfer') && _.isEmpty(this.model.get('ship_to_contact'))) {              
    	errors['ship_to_contact'] = errors['ship_to_contact'] || {};
		errors['ship_to_contact'].required = true;
    } 
    if ((action == 'Archive Offsite' || action == 'External Transfer') && _.isEmpty(this.model.get('ship_to_address'))) {              
    	errors['ship_to_address'] = errors['ship_to_address'] || {};
		errors['ship_to_address'].required = true;
    } 
    if ((action == 'Archive Offsite' || action == 'External Transfer') && _.isEmpty(this.model.get('ship_to_company'))) {              
    	errors['ship_to_company'] = errors['ship_to_company'] || {};
		errors['ship_to_company'].required = true;
    }  */
     /**Bug #525 fixes : 19 oct 2022 : These fields are not in record view so it was showing required field error on save */  
    /* if ((action == 'Archive Offsite' || action == 'Discard' || action == 'External Transfer') && _.isEmpty(this.model.get('end_condition_acceptable'))) {              
    	errors['end_condition_acceptable'] = errors['end_condition_acceptable'] || {};
		errors['end_condition_acceptable'].required = true;
    }  */
    if ((action == 'Archive Onsite' || action == 'Internal Transfer Storage' || action == 'Separate Inventory Items') && _.isEmpty(this.model.get('location_building'))) {              
    	errors['location_building'] = errors['location_building'] || {};
		errors['location_building'].required = true;
    } 
    if ((action == 'Archive Onsite' || action == 'Internal Transfer Storage' || action == 'Separate Inventory Items') && _.isEmpty(this.model.get('location_type'))) {              
    	errors['location_type'] = errors['location_type'] || {};
		errors['location_type'].required = true;
    } 
    if ((action == 'Processing' || action == 'Internal Transfer') && (duration_hours == "" || duration_hours == null)) {
        errors['duration_hours'] = errors['duration_hours'] || {};
		errors['duration_hours'].required = true;
    } 
    if ((action == 'Processing' || action == 'Internal Transfer') && _.isEmpty(this.model.get('start_datetime'))) {
        errors['start_datetime'] = errors['start_datetime'] || {};
		errors['start_datetime'].required = true;
    } 
    if ((action == 'Processing' || action == 'Internal Transfer') && _.isEmpty(this.model.get('end_datetime'))) {
        errors['end_datetime'] = errors['end_datetime'] || {};
		errors['end_datetime'].required = true;
    }
    if ((action == 'Processing') && _.isEmpty(this.model.get('processing_method'))) {
        errors['processing_method'] = errors['processing_method'] || {};
		errors['processing_method'].required = true;
    }
    if ((action == 'Archive Onsite' || action == 'External Transfer' || action == 'Internal Transfer' || action == 'Processing') && _.isEmpty(this.model.get('processing_transfer_condition'))) {
        errors['processing_transfer_condition'] = errors['processing_transfer_condition'] || {};
		errors['processing_transfer_condition'].required = true;
    }
    if ((action == 'Archive Onsite' || action == 'Create Inventory Collection' || action == 'Internal Transfer' || action == 'Processing' || action == 'Separate Inventory Items')  && _.isEmpty(this.model.get('end_condition'))) {
        errors['end_condition'] = errors['end_condition'] || {};
		errors['end_condition'].required = true;
    }
    if ((action == 'Archive Onsite' || action == 'Internal Transfer Storage' || action == 'Separate Inventory Items')  && _.isEmpty(this.model.get('location_type'))) {             
        errors['location_type'] = errors['location_type'] || {};
		errors['location_type'].required = true;
    } 
    if ((location_type == 'Room' || location_type == 'RoomCabinet' || location_type == 'RoomShelf') && _.isEmpty(this.model.get('location_room'))) {              
    	errors['location_room'] = errors['location_room'] || {};
		errors['location_room'].required = true;
    }
    if ((location_type == 'Shelf' || location_type == 'RoomShelf') && _.isEmpty(this.model.get('location_shelf'))) {              
    	errors['location_shelf'] = errors['location_shelf'] || {};
		errors['location_shelf'].required = true;
    }                   
    if ((location_type == 'RoomCabinet' || location_type == 'Cabinet') && _.isEmpty(this.model.get('location_cabinet'))) {              
    	errors['location_cabinet'] = errors['location_cabinet'] || {};
		errors['location_cabinet'].required = true;
    }
    if ((location_type == 'Equipment') && _.isEmpty(this.model.get('location_equipment'))) {              
    	errors['location_equipment'] = errors['location_equipment'] || {};
		errors['location_equipment'].required = true;
    }   
   
    if ((location_equi_serial_no_hide_c == '51014222') && _.isEmpty(this.model.get('location_rack_c'))) {              
    	errors['location_rack_c'] = errors['location_rack_c'] || {};
		errors['location_rack_c'].required = true;
    }
    if ((location_equi_serial_no_hide_c == '51014222') && _.isEmpty(this.model.get('location_bin_c'))) {              
    	errors['location_bin_c'] = errors['location_bin_c'] || {};
		errors['location_bin_c'].required = true;
    }
    if ((location_equi_serial_no_hide_c == '51014222') && _.isEmpty(this.model.get('location_cubby_c'))) {              
    	errors['location_cubby_c'] = errors['location_cubby_c'] || {};
		errors['location_cubby_c'].required = true;
    }  
    /**Bug #525 fixes : 19 oct 2022 : These fields are not in record view so it was showing required field error on save */  
   /*  if ((end_condition_acceptable == 'No' || end_condition_acceptable == 'Yes with Comments' ) && _.isEmpty(this.model.get('end_condition_comments'))) {              
    	errors['end_condition_comments'] = errors['end_condition_comments'] || {};
		errors['end_condition_comments'].required = true;
    }  */
	callback(null, fields, errors);
	},

  function_location_type_change: function () {
    this.model.set('location_building', null);
    this.model.set('equip_equipment_id_c', null);
    this.model.set('location_equipment', null);
    this.model.set('rms_room_id_c', null);
    this.model.set('location_room', null);
    this.model.set('location_shelf', null);
    this.model.set('location_cabinet', null);
  },
  function_location_building_change: function () {
    this.model.set('equip_equipment_id_c', null);
    this.model.set('location_equipment', null);
    this.model.set('rms_room_id_c', null);
    this.model.set('location_room', null);
    this.model.set('location_shelf', null);
    this.model.set('location_cabinet', null);

    if (this.model.get('location_type') == "Equipment" && !_.isEmpty(this.model.get('location_building'))) {
      //$(".select2-input").prop("readonly", true);
    } else {
      //$(".select2-input").prop("readonly", false);
    }

  },
  function_location_equipment: function () {
    var self = this;
    var equip_equipment_id = this.model.get('equip_equipment_id_c');
    if (equip_equipment_id) {
      App.api.call("get", "rest/v10/Equip_Equipment/" + equip_equipment_id + "?fields=serial_number_c", null, {
        success: function (data) {
          self.model.set("location_equi_serial_no_hide_c",data.serial_number_c);
        }
      });
    }

  },

  function_im_fields: function() {
		var self = this;	
                
    var action = this.model.get('action');    
    if (action == 'Processing' || action == 'Internal Transfer') {
        $('[data-name="duration_hours"]').css("visibility", "visible"); 
        $('[data-name="duration_hours"]').css("height", "auto");        
    } else {
        $('[data-name="duration_hours"]').css("visibility", "hidden");            
        $('[data-name="duration_hours"]').css("height", "0");       
    }
    var type_specimen = this.model.get('type_specimen_c'); 
    console.log('type_specimen : OUT ', type_specimen);               
    if (_.contains(type_specimen, "Tissue") || _.contains(this.model.get('type_specimen_c'), "Tissue")) {
      console.log('type_specimen : In ', type_specimen);
        $('[data-name="ending_storage_media"]').css("visibility", "visible"); 
        $('[data-name="ending_storage_media"]').css("height", "auto"); 
        setTimeout(function () {
          self.$el.find('input[name="ending_storage_media"]').prop('disabled', false);
        }, 1000);
    } else {
        $('[data-name="ending_storage_media"]').css("visibility", "hidden"); 
        $('[data-name="ending_storage_media"]').css("height", "0"); 
    }          
	},
})


