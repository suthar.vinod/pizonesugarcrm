({
    extendsFrom: "MultiSelectionListView",
    allowedFields: ["name", "work_product_status_c", "date_modified"],
    initialize: function () {
        this._super("initialize", arguments);
        this.collection.reset();
    },
    render: function () {
        this._super("render", arguments);
        this.layout.$el.parent().addClass("span12 wp_selection");
    },
    addActions: function () {
        this._super("addActions");
        this.rightColumns = [];
        this.meta.panels[0].fields = _.filter(this.meta.panels[0].fields, function filterFields(field) {
            return this.allowedFields.indexOf(field.name) !== -1;
        }.bind(this));
    }
});