<?php
// created: 2021-04-08 11:54:19
$viewdefs['IM_Inventory_Management']['base']['view']['subpanel-for-ii_inventory_item-ii_inventory_item_im_inventory_management_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'action',
          'label' => 'LBL_ACTION',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'action_date',
          'label' => 'LBL_ACTION_DATE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'location_type',
          'label' => 'LBL_LOCATION_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'location_building',
          'label' => 'LBL_LOCATION_BUILDING',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'location_room',
          'label' => 'LBL_LOCATION_ROOM',
          'enabled' => true,
          'id' => 'RMS_ROOM_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'location_equipment',
          'label' => 'LBL_LOCATION_EQUIPMENT',
          'enabled' => true,
          'id' => 'EQUIP_EQUIPMENT_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'location_rack_c',
          'label' => 'LBL_LOCATION_RACK',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'location_cubby_c',
          'label' => 'LBL_LOCATION_CUBBY',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'location_bin_c',
          'label' => 'LBL_LOCATION_BIN',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'location_shelf',
          'label' => 'LBL_LOCATION_SHELF',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'location_cabinet',
          'label' => 'LBL_LOCATION_CABINET',
          'enabled' => true,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'ship_to_contact',
          'label' => 'LBL_SHIP_TO_CONTACT',
          'enabled' => true,
          'id' => 'CONTACT_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        13 => 
        array (
          'name' => 'ship_to_company',
          'label' => 'LBL_SHIP_TO_COMPANY',
          'enabled' => true,
          'id' => 'ACCOUNT_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        14 => 
        array (
          'name' => 'ship_to_address',
          'label' => 'LBL_SHIP_TO_ADDRESS',
          'enabled' => true,
          'id' => 'CA_COMPANY_ADDRESS_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);