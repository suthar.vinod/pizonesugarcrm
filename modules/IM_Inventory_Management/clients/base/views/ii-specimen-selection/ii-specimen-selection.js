({
    extendsFrom: "MultiSelectionListView",
    allowedFields: ["name","collection_date"],
    initialize: function () {
        this._super("initialize", arguments);
        this.collection.reset();
    },
    render: function () {
        this._super("render", arguments);
        /** Set the label name as custom label for given fields */
        /* this.$('[data-fieldname="anml_animals_ii_inventory_item_1_name"]').text('TS');
        this.$('[data-fieldname="type_2"]').text('Specimen Type');
        this.$('[data-fieldname="test_type_c"]').text('Test Type');
        this.$('[data-fieldname="timepoint_type"]').text('Timepoint');
        this.$('[data-fieldname="collection_date"]').text('Collection Date'); */

        this.layout.$el.parent().addClass("span12 ii_specimen_selection");
    },
    addActions: function () {
        this._super("addActions");
        this.rightColumns = [];
        this.meta.panels[0].fields = _.filter(this.meta.panels[0].fields, function filterFields(field) {
            return this.allowedFields.indexOf(field.name) !== -1;
        }.bind(this));
    }
});