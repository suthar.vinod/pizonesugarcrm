<?php
$module_name = 'IM_Inventory_Management';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'IM_Inventory_Management',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'related_to',
                'label' => 'LBL_RELATED_TO',
              ),
              1 => 
              array (
                'name' => 'composition',
                'label' => 'LBL_COMPOSITION',
              ),
              2 => 
              array (
                'name' => 'action',
                'label' => 'LBL_ACTION',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'name' => 'm01_sales_im_inventory_management_1_name',
              ),
              5 => 
              array (
              ),
              6 => 
              array (
                'name' => 'm03_work_product_im_inventory_management_1_name',
              ),
              7 => 
              array (
              ),
              8 => 
              array (
                'name' => 'category',
                'label' => 'LBL_CATEGORY',
              ),
              9 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
              ),
              10 => 
              array (
                'readonly' => false,
                'name' => 'type_specimen_c',
                'label' => 'LBL_TYPE_SPECIMEN',
              ),
              11 => 
              array (
              ),
              12 => 
              array (
                'name' => 'current_storage_verification',
                'label' => 'LBL_CURRENT_STORAGE_VERIFICATION',
              ),
              13 => 
              array (
                'name' => 'current_location_verification',
                'label' => 'LBL_CURRENT_LOCATION_VERIFICATION',
              ),
              14 => 
              array (
                'name' => 'ic_inventory_collection_im_inventory_management_1_name',
              ),
              15 => 
              array (
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'action_date',
                'label' => 'LBL_ACTION_DATE',
              ),
              1 => 
              array (
              ),
              2 => 
              array (
                'name' => 'start_datetime',
                'label' => 'LBL_START_DATETIME',
              ),
              3 => 
              array (
                'name' => 'end_datetime',
                'label' => 'LBL_END_DATETIME',
              ),
              4 => 
              array (
                'name' => 'duration_hours',
                'label' => 'LBL_DURATION_HOURS',
              ),
              5 => 
              array (
              ),
              6 => 
              array (
                'name' => 'processing_transfer_condition',
                'label' => 'LBL_PROCESSING_TRANSFER_CONDITION',
              ),
              7 => 
              array (
              ),
              8 => 
              array (
                'name' => 'end_condition',
                'label' => 'LBL_END_CONDITION',
              ),
              9 => 
              array (
              ),
              10 => 
              array (
                'name' => 'ending_storage_media',
                'label' => 'LBL_ENDING_STORAGE_MEDIA',
              ),
              11 => 
              array (
              ),
              12 => 
              array (
                'readonly' => false,
                'name' => 'processed_per_protocol_c',
                'label' => 'LBL_PROCESSED_PER_PROTOCOL',
              ),
              13 => 
              array (
              ),
              14 => 
              array (
                'name' => 'external_receipt_date',
                'label' => 'LBL_EXTERNAL_RECEIPT_DATE',
              ),
              15 => 
              array (
              ),
              16 => 
              array (
                'name' => 'record_status',
                'label' => 'LBL_RECORD_STATUS',
              ),
              17 => 
              array (
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL4',
            'label' => 'LBL_RECORDVIEW_PANEL4',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'location_type',
                'label' => 'LBL_LOCATION_TYPE',
              ),
              1 => 
              array (
                'name' => 'location_building',
                'label' => 'LBL_LOCATION_BUILDING',
              ),
              2 => 
              array (
                'name' => 'location_equipment',
                'studio' => 'visible',
                'label' => 'LBL_LOCATION_EQUIPMENT',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'location_rack_c',
                'label' => 'LBL_LOCATION_RACK',
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'location_cubby_c',
                'label' => 'LBL_LOCATION_CUBBY',
              ),
              6 => 
              array (
                'readonly' => false,
                'name' => 'location_bin_c',
                'label' => 'LBL_LOCATION_BIN',
              ),
              7 => 
              array (
              ),
              8 => 
              array (
                'name' => 'location_room',
                'studio' => 'visible',
                'label' => 'LBL_LOCATION_ROOM',
              ),
              9 => 
              array (
              ),
              10 => 
              array (
                'name' => 'location_shelf',
                'label' => 'LBL_LOCATION_SHELF',
              ),
              11 => 
              array (
              ),
              12 => 
              array (
                'name' => 'location_cabinet',
                'label' => 'LBL_LOCATION_CABINET',
              ),
              13 => 
              array (
              ),
              14 => 
              array (
                'name' => 'ship_to_contact',
                'studio' => 'visible',
                'label' => 'LBL_SHIP_TO_CONTACT',
              ),
              15 => 
              array (
                'name' => 'ship_to_company',
                'studio' => 'visible',
                'label' => 'LBL_SHIP_TO_COMPANY',
              ),
              16 => 
              array (
                'name' => 'ship_to_address',
                'studio' => 'visible',
                'label' => 'LBL_SHIP_TO_ADDRESS',
              ),
              17 => 
              array (
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL5',
            'label' => 'LBL_RECORDVIEW_PANEL5',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'reviewed_record_c',
                'label' => 'LBL_REVIEWED_RECORD',
              ),
              1 => 
              array (
              ),
              2 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
