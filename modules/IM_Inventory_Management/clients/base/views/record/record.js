({
    extendsFrom: 'RecordView',
    initialize: function(options) {
        this._super('initialize', [options]);

        this.model.once("sync",function() {this.model.on("change:location_building",this.function_location_building_change,this);},this);
        this.model.once("sync",function() {this.model.on("change:location_equipment",this.function_location_equipment,this);},this);
        this.model.once("sync",function() {this.model.on("change:location_type",this.function_location_type_change,this);},this);
        this.model.once("sync",function() {this.model.on("change:action",this.function_action_change,this);},this);        
        this.model.addValidationTask('check_im_field', _.bind(this._doValidateimFiled, this));
        this.model.on('change:related_to change:category change:type_2 change:action change:location_type change:location_equi_serial_no_hide_c change:type_specimen_c', this.function_im_fields, this);
    },

    _doValidateimFiled: function(fields, errors, callback) {
		var self = this;
		var related_to = this.model.get('related_to');
        var category = this.model.get('category');  
        var type_2 = this.model.get('type_2');
        var type_specimen = this.model.get('type_specimen_c');
        var action = this.model.get('action');
        var location_type = this.model.get('location_type');
        var location_equi_serial_no_hide_c = this.model.get('location_equi_serial_no_hide_c');
        var duration_hours = this.model.get('duration_hours');
        var end_condition_acceptable = this.model.get('end_condition_acceptable');
        var wp_name = this.model.get('m03_work_product_im_inventory_management_1_name')
        var sales_name = this.model.get('m01_sales_im_inventory_management_1_name')
        console.log('sales_name : ', sales_name);
        console.log('wp_name : ', wp_name);
		//validate requirements
		    
    if (related_to == 'Sales'  && ( sales_name == ''  || sales_name == null || sales_name == undefined)) {
        errors['m01_sales_im_inventory_management_1_name'] = errors['m01_sales_im_inventory_management_1_name'] || {};
        errors['m01_sales_im_inventory_management_1_name'].required = true;
    } 
    if (related_to == 'Work Product'  && ( wp_name == '' || wp_name == null || wp_name == undefined)) {
      errors['m03_work_product_im_inventory_management_1_name'] = errors['m03_work_product_im_inventory_management_1_name'] || {};
	  errors['m03_work_product_im_inventory_management_1_name'].required = true;
    }    

    if (category != 'Specimen' && _.isEmpty(this.model.get('type_2'))) {              
    	errors['type_2'] = errors['type_2'] || {};
		errors['type_2'].required = true;
    }
    if (category == 'Specimen' && _.isEmpty(this.model.get('type_specimen_c'))) {
        errors['type_specimen_c'] = errors['type_specimen_c'] || {};
		errors['type_specimen_c'].required = true;
    }
    if (category != 'Specimen' && _.isEmpty(this.model.get('type_2'))) {              
    	errors['type_2'] = errors['type_2'] || {};
		errors['type_2'].required = true;
    } 
    console.log('type_specimen : ', type_specimen);
    if (_.contains(type_specimen, "Tissue") && _.isEmpty(this.model.get('ending_storage_media'))) {
        console.log('type_specimen in: ', type_specimen);
        errors['ending_storage_media'] = errors['ending_storage_media'] || {};
		errors['ending_storage_media'].required = true;
    }    
   /*  if ((action == 'Archive Offsite' || action == 'External Transfer') && _.isEmpty(this.model.get('ship_to_contact'))) {              
    	errors['ship_to_contact'] = errors['ship_to_contact'] || {};
		errors['ship_to_contact'].required = true;
    } 
    if ((action == 'Archive Offsite' || action == 'External Transfer') && _.isEmpty(this.model.get('ship_to_address'))) {              
    	errors['ship_to_address'] = errors['ship_to_address'] || {};
		errors['ship_to_address'].required = true;
    } 
    if ((action == 'Archive Offsite' || action == 'External Transfer') && _.isEmpty(this.model.get('ship_to_company'))) {              
    	errors['ship_to_company'] = errors['ship_to_company'] || {};
		errors['ship_to_company'].required = true;
    }  */
    /**Bug #525 fixes : 19 oct 2022 : These fields are not in record view so it was showing required field error on save */
    /* if ((action == 'Archive Offsite' || action == 'Discard' || action == 'External Transfer') && _.isEmpty(this.model.get('end_condition_acceptable'))) {              
    	errors['end_condition_acceptable'] = errors['end_condition_acceptable'] || {};
		errors['end_condition_acceptable'].required = true;
    } */ 
    if ((action == 'Archive Onsite' || action == 'Internal Transfer Storage' || action == 'Separate Inventory Items') && _.isEmpty(this.model.get('location_building'))) {              
    	errors['location_building'] = errors['location_building'] || {};
		errors['location_building'].required = true;
    } 
    if ((action == 'Archive Onsite' || action == 'Internal Transfer Storage' || action == 'Separate Inventory Items') && _.isEmpty(this.model.get('location_type'))) {              
    	errors['location_type'] = errors['location_type'] || {};
		errors['location_type'].required = true;
    } 
    if ((action == 'Processing' || action == 'Internal Transfer') && (duration_hours == "" || duration_hours == null)) {
        errors['duration_hours'] = errors['duration_hours'] || {};
		errors['duration_hours'].required = true;
    } 
    if ((action == 'Processing' || action == 'Internal Transfer') && _.isEmpty(this.model.get('start_datetime'))) {
        errors['start_datetime'] = errors['start_datetime'] || {};
		errors['start_datetime'].required = true;
    } 
    if ((action == 'Processing' || action == 'Internal Transfer') && _.isEmpty(this.model.get('end_datetime'))) {
        errors['end_datetime'] = errors['end_datetime'] || {};
		errors['end_datetime'].required = true;
    }
    if ((action == 'Processing') && _.isEmpty(this.model.get('processing_method'))) {
        errors['processing_method'] = errors['processing_method'] || {};
		errors['processing_method'].required = true;
    }
    if ((action == 'Archive Onsite' || action == 'External Transfer' || action == 'Internal Transfer' || action == 'Processing') && _.isEmpty(this.model.get('processing_transfer_condition'))) {
        errors['processing_transfer_condition'] = errors['processing_transfer_condition'] || {};
		errors['processing_transfer_condition'].required = true;
    }
    if ((action == 'Archive Onsite' || action == 'Create Inventory Collection' || action == 'Internal Transfer' || action == 'Processing' || action == 'Separate Inventory Items')  && _.isEmpty(this.model.get('end_condition'))) {
        errors['end_condition'] = errors['end_condition'] || {};
		errors['end_condition'].required = true;
    }
    if ((action == 'Archive Onsite' || action == 'Internal Transfer Storage' || action == 'Separate Inventory Items')  && _.isEmpty(this.model.get('location_type'))) {             
        errors['location_type'] = errors['location_type'] || {};
		errors['location_type'].required = true;
    } 
    if ((location_type == 'Room' || location_type == 'RoomCabinet' || location_type == 'RoomShelf') && _.isEmpty(this.model.get('location_room'))) {              
    	errors['location_room'] = errors['location_room'] || {};
		errors['location_room'].required = true;
    }
    if ((location_type == 'Shelf' || location_type == 'RoomShelf') && _.isEmpty(this.model.get('location_shelf'))) {              
    	errors['location_shelf'] = errors['location_shelf'] || {};
		errors['location_shelf'].required = true;
    }                   
    if ((location_type == 'RoomCabinet' || location_type == 'Cabinet') && _.isEmpty(this.model.get('location_cabinet'))) {              
    	errors['location_cabinet'] = errors['location_cabinet'] || {};
		errors['location_cabinet'].required = true;
    }
    if ((location_type == 'Equipment') && _.isEmpty(this.model.get('location_equipment'))) {              
    	errors['location_equipment'] = errors['location_equipment'] || {};
		errors['location_equipment'].required = true;
    }   
   
    if ((location_equi_serial_no_hide_c == '51014222') && _.isEmpty(this.model.get('location_rack_c'))) {              
    	errors['location_rack_c'] = errors['location_rack_c'] || {};
		errors['location_rack_c'].required = true;
    }
    if ((location_equi_serial_no_hide_c == '51014222') && _.isEmpty(this.model.get('location_bin_c'))) {              
    	errors['location_bin_c'] = errors['location_bin_c'] || {};
		errors['location_bin_c'].required = true;
    }
    if ((location_equi_serial_no_hide_c == '51014222') && _.isEmpty(this.model.get('location_cubby_c'))) {              
    	errors['location_cubby_c'] = errors['location_cubby_c'] || {};
		errors['location_cubby_c'].required = true;
    }
    /**Bug #525 fixes : 19 oct 2022 : These fields are not in record view so it was showing required field error on save */
    /* if ((end_condition_acceptable == 'No' || end_condition_acceptable == 'Yes with Comments' ) && _.isEmpty(this.model.get('end_condition_comments'))) {              
    	errors['end_condition_comments'] = errors['end_condition_comments'] || {};
		errors['end_condition_comments'].required = true;
    }  */
	callback(null, fields, errors);
	},
    function_location_equipment: function() {
        var self = this;
        var equip_equipment_id = this.model.get('equip_equipment_id_c');
        if (equip_equipment_id) {
            App.api.call("get", "rest/v10/Equip_Equipment/" + equip_equipment_id + "?fields=serial_number_c", null, {
                success: function(data) {
                    self.model.set("location_equi_serial_no_hide_c", data.serial_number_c);
                }
            });
        }

    },

    function_location_type_change: function() {
        this.model.set('location_building', '');
        this.model.set('equip_equipment_id_c', '');
        this.model.set('location_equipment', '');
        this.model.set('rms_room_id_c', '');
        this.model.set('location_room', '');
        this.model.set('location_shelf', '');
        this.model.set('location_cabinet', '');

    },
    function_location_building_change: function() {
        this.model.set('equip_equipment_id_c', '');
        this.model.set('location_equipment', '');
        this.model.set('rms_room_id_c', '');
        this.model.set('location_room', '');
        this.model.set('location_shelf', '');
        this.model.set('location_cabinet', '');

    },

    function_action_change: function() {
        //alert('hi');
        var action = this.model.get('action');
        //alert(action);
        if (action == 'Archive Offsite' || action == 'External Transfer') {
            //alert('No Ship To');
            this.model.set('location_type', '');
            this.model.set('rms_room_id_c', '');
            this.model.set('location_room', '');
            this.model.set('location_shelf', '');
            this.model.set('location_cabinet', '');
            this.model.set('location_equipment', '');
            this.model.set('equip_equipment_id_c', '');

        } else {
            //alert('Ship To');
            this.model.set('ship_to_contact', '');
            this.model.set('contact_id_c', '');
            this.model.set('ship_to_address', '');
            this.model.set('ca_company_address_id_c', '');
            this.model.set('ship_to_company', '');
            this.model.set('account_id_c', '');

        }
        //this._super('save');
    },

    function_im_fields: function() {
		var self = this;
		var related_to = this.model.get('related_to');
                if (related_to == 'Sales') {
                    $('[data-name="m01_sales_im_inventory_management_1_name"]').css("visibility", "visible"); 
                    $('[data-name="m01_sales_im_inventory_management_1_name"]').css("height", "auto"); 

                } else {
                    $('[data-name="m01_sales_im_inventory_management_1_name"]').css("visibility", "hidden"); 
                    $('[data-name="m01_sales_im_inventory_management_1_name"]').css("height", "0"); 
                }
                if (related_to == 'Work Product') {
                    $('[data-name="m03_work_product_im_inventory_management_1_name"]').css("visibility", "visible"); 
                    $('[data-name="m03_work_product_im_inventory_management_1_name"]').css("height", "auto"); 

                } else {
                    $('[data-name="m03_work_product_im_inventory_management_1_name"]').css("visibility", "hidden"); 
                    $('[data-name="m03_work_product_im_inventory_management_1_name"]').css("height", "0"); 
                }
               
                var category = this.model.get('category');
                                
                if (category != 'Specimen') {
                    $('[data-name="type_2"]').css("visibility", "visible"); 
                    $('[data-name="type_2"]').css("height", "auto"); 

                } else {
                    $('[data-name="type_2"]').css("visibility", "hidden"); 
                    $('[data-name="type_2"]').css("height", "0"); 
                }
                if (category == 'Specimen') {
                    $('[data-name="type_specimen_c"]').css("visibility", "visible"); 
                    $('[data-name="type_specimen_c"]').css("height", "auto"); 

                } else {
                    $('[data-name="type_specimen_c"]').css("visibility", "hidden"); 
                    $('[data-name="type_specimen_c"]').css("height", "0"); 
                }
                var type_specimen = this.model.get('type_specimen_c');    
                console.log('type_specimen : OUT', type_specimen);            
                if (_.contains(type_specimen, "Tissue") || _.contains(this.model.get('type_specimen_c'), "Tissue")) {
                    console.log('type_specimen : IN', type_specimen);
                    $('[data-name="ending_storage_media"]').css("visibility", "visible"); 
                    $('[data-name="ending_storage_media"]').css("height", "auto"); 
                } else {
                    $('[data-name="ending_storage_media"]').css("visibility", "hidden"); 
                    $('[data-name="ending_storage_media"]').css("height", "0"); 
                }
                
                var action = this.model.get('action');
                if (action == 'Archive Offsite' || action == 'External Transfer') {
                    $('[data-name="external_receipt_date"]').css("visibility", "visible"); 
                    $('[data-name="ship_to_contact"]').css("visibility", "visible");
                    $('[data-name="ship_to_address"]').css("visibility", "visible");
                    $('[data-name="ship_to_company"]').css("visibility", "visible");

                    $('[data-name="external_receipt_date"]').css("height", "auto");
                    $('[data-name="ship_to_contact"]').css("height", "auto");
                    $('[data-name="ship_to_address"]').css("height", "auto");
                    $('[data-name="ship_to_company"]').css("height", "auto");
                } else {
                    $('[data-name="external_receipt_date"]').css("visibility", "hidden");
                    $('[data-name="ship_to_contact"]').css("visibility", "hidden"); 
                    $('[data-name="ship_to_address"]').css("visibility", "hidden"); 
                    $('[data-name="ship_to_company"]').css("visibility", "hidden"); 
                    
                    $('[data-name="external_receipt_date"]').css("height", "0");
                    $('[data-name="ship_to_contact"]').css("height", "0");
                    $('[data-name="ship_to_address"]').css("height", "0");
                    $('[data-name="ship_to_company"]').css("height", "0");
                }
                if (action == 'Archive Onsite' || action == 'Separate Inventory Items') {
                    $('[data-name="location_building"]').css("visibility", "visible"); 
                    $('[data-name="location_building"]').css("height", "auto");
                } else {
                    $('[data-name="location_building"]').css("visibility", "hidden");            
                    $('[data-name="location_building"]').css("height", "0");
                }
                if (action == 'Processing' || action == 'Internal Transfer') {
                    $('[data-name="start_datetime"]').css("visibility", "visible"); 
                    $('[data-name="start_datetime"]').css("height", "auto");
                    $('[data-name="end_datetime"]').css("visibility", "visible"); 
                    $('[data-name="end_datetime"]').css("height", "auto");
                    $('[data-name="duration_hours"]').css("visibility", "visible"); 
                    $('[data-name="duration_hours"]').css("height", "auto"); 
                } else {
                    $('[data-name="start_datetime"]').css("visibility", "hidden");            
                    $('[data-name="start_datetime"]').css("height", "0");
                    $('[data-name="end_datetime"]').css("visibility", "hidden");            
                    $('[data-name="end_datetime"]').css("height", "0");
                    $('[data-name="duration_hours"]').css("visibility", "hidden");            
                    $('[data-name="duration_hours"]').css("height", "0");    
                }
                if (action == 'Archive Onsite' || action == 'External Transfer' || action == 'Internal Transfer' || action == 'Processing') {
                    $('[data-name="processing_transfer_condition"]').css("visibility", "visible"); 
                    $('[data-name="processing_transfer_condition"]').css("height", "auto");
                } else {
                    $('[data-name="processing_transfer_condition"]').css("visibility", "hidden");            
                    $('[data-name="processing_transfer_condition"]').css("height", "0");
                }
                if (action == 'Archive Onsite' || action == 'Create Inventory Collection' || action == 'Internal Transfer' || action == 'Processing' || action == 'Separate Inventory Items') {
                    $('[data-name="end_condition"]').css("visibility", "visible"); 
                    $('[data-name="end_condition"]').css("height", "auto");
                } else {
                    $('[data-name="end_condition"]').css("visibility", "hidden");            
                    $('[data-name="end_condition"]').css("height", "0");
                }
                if (action == 'Archive Onsite' || action == 'Internal Transfer Storage' || action == 'Separate Inventory Items') {              
                    $('[data-name="location_type"]').css("visibility", "visible"); 
                    $('[data-name="location_type"]').css("height", "auto");
                } else {
                    $('[data-name="location_type"]').css("visibility", "hidden");            
                    $('[data-name="location_type"]').css("height", "0");
                }

                var location_type = this.model.get('location_type');
                if (location_type == 'Room' || location_type == 'RoomCabinet' || location_type == 'RoomShelf') {
                    $('[data-name="location_room"]').css("visibility", "visible"); 
                    $('[data-name="location_room"]').css("height", "auto"); 

                } else {
                    $('[data-name="location_room"]').css("visibility", "hidden"); 
                    $('[data-name="location_room"]').css("height", "0"); 
                }                
                if (location_type == 'Shelf' || location_type == 'RoomShelf') {
                    $('[data-name="location_shelf"]').css("visibility", "visible"); 
                    $('[data-name="location_shelf"]').css("height", "auto"); 

                } else {
                    $('[data-name="location_shelf"]').css("visibility", "hidden"); 
                    $('[data-name="location_shelf"]').css("height", "0"); 
                }
                if (location_type == 'RoomCabinet' || location_type == 'Cabinet') {
                    $('[data-name="location_cabinet"]').css("visibility", "visible"); 
                    $('[data-name="location_cabinet"]').css("height", "auto"); 

                } else {
                    $('[data-name="location_cabinet"]').css("visibility", "hidden"); 
                    $('[data-name="location_cabinet"]').css("height", "0"); 
                }
                if (location_type == 'Equipment') {
                    $('[data-name="location_equipment"]').css("visibility", "visible"); 
                    $('[data-name="location_equipment"]').css("height", "auto"); 

                } else {
                    $('[data-name="location_equipment"]').css("visibility", "hidden"); 
                    $('[data-name="location_equipment"]').css("height", "0"); 
                }
                var location_equi_serial_no_hide_c = this.model.get('location_equi_serial_no_hide_c');
                if (location_equi_serial_no_hide_c == '51014222') {
                    $('[data-name="location_rack_c"]').css("visibility", "visible"); 
                    $('[data-name="location_rack_c"]').css("height", "auto");
                    $('[data-name="location_bin_c"]').css("visibility", "visible"); 
                    $('[data-name="location_bin_c"]').css("height", "auto");  
                    $('[data-name="location_cubby_c"]').css("visibility", "visible"); 
                    $('[data-name="location_cubby_c"]').css("height", "auto");
                } else {
                    $('[data-name="location_rack_c"]').css("visibility", "hidden"); 
                    $('[data-name="location_rack_c"]').css("height", "0"); 
                    $('[data-name="location_bin_c"]').css("visibility", "hidden"); 
                    $('[data-name="location_bin_c"]').css("height", "0"); 
                    $('[data-name="location_cubby_c"]').css("visibility", "hidden"); 
                    $('[data-name="location_cubby_c"]').css("height", "0"); 
                }
               
	},

})