({
    extendsFrom: "MultiSelectionListView",
    allowedFields: ["name", "internal_barcode","storage_condition","allowed_storage_conditions","allowed_storage_medium","location_building","location_room","location_equipment","location_shelf","location_cabinet"],
    initialize: function () {
        this._super("initialize", arguments);
        this.collection.reset();
    },
    render: function () {
        this._super("render", arguments);
        this.layout.$el.parent().addClass("span12 ii_wp_selection");
    },
    addActions: function () {
        this._super("addActions");
        this.rightColumns = [];
        this.meta.panels[0].fields = _.filter(this.meta.panels[0].fields, function filterFields(field) {
            return this.allowedFields.indexOf(field.name) !== -1;
        }.bind(this));
    }
});