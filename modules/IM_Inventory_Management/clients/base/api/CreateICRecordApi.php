<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class CreateICRecordApi extends SugarApi {

    public function registerApiRest() {
        return array(
            'MyGetEndpointIC' => array(
                'reqType' => 'POST',
                'noLoginRequired' => false,
                'path' => array('create_ic_record'),
                'pathVars' => array(),
                'method' => 'create_ic_record',
                'shortHelp' => '',
                'longHelp' => '',
            ),
        );
    }

    public function create_ic_record($api, $args) {
        global $db;

        $module = $args['module'];
        $IM_id = $args['IM_id'];
        $wpId = $args['wpId'];
        $II_ids = $args['II_ids'];
 
        $sqlWP = 'SELECT name FROM m03_work_product WHERE id = "' . $wpId . '"';
        $resultWP = $db->query($sqlWP);
        if ($resultWP->num_rows > 0) {
            $rowWP = $db->fetchByAssoc($resultWP);
            $WPName = $rowWP['name'];
        }

        $bean = BeanFactory::newBean($module);

        $bean->load_relationship("ic_inventory_collection_ii_inventory_item_1");
        $bean->load_relationship("ic_inventory_collection_im_inventory_management_1");

        $bid = create_guid();
        $bean->id = $bid;
        $bean->new_with_id = true;

        $bean->m03_work_product_ic_inventory_collection_1_name = $WPName;
        $bean->m03_work_product_ic_inventory_collection_1m03_work_product_ida = $wpId;

        $bean->save();
        foreach ($II_ids as $IIids) {
            $bean->ic_inventory_collection_ii_inventory_item_1->add($IIids);
        }

        $bean->ic_inventory_collection_im_inventory_management_1->add($IM_id);

        return true;
    }

}
