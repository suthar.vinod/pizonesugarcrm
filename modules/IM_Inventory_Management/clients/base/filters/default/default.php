<?php
// created: 2022-01-25 09:21:36
$viewdefs['IM_Inventory_Management']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'action' => 
    array (
    ),
    'action_date' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'category' => 
    array (
    ),
    'composition' => 
    array (
    ),
    'current_location_verification' => 
    array (
    ),
    'current_storage_verification' => 
    array (
    ),
    'duration_hours' => 
    array (
    ),
    'end_condition' => 
    array (
    ),
    'end_datetime' => 
    array (
    ),
    'ending_storage_media' => 
    array (
    ),
    'external_receipt_date' => 
    array (
    ),
    'ic_inventory_collection_im_inventory_management_1_name' => 
    array (
    ),
    'location_bin_c' => 
    array (
    ),
    'location_building' => 
    array (
    ),
    'location_cabinet' => 
    array (
    ),
    'location_cubby_c' => 
    array (
    ),
    'location_equipment' => 
    array (
    ),
    'location_rack_c' => 
    array (
    ),
    'location_room' => 
    array (
    ),
    'location_shelf' => 
    array (
    ),
    'location_type' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'name' => 
    array (
    ),
    'description' => 
    array (
    ),
    'processing_transfer_condition' => 
    array (
    ),
    'processed_per_protocol_c' => 
    array (
    ),
    'record_status' => 
    array (
    ),
    'related_to' => 
    array (
    ),
    'reviewed_record_c' => 
    array (
    ),
    'm01_sales_im_inventory_management_1_name' => 
    array (
    ),
    'ship_to_address' => 
    array (
    ),
    'ship_to_company' => 
    array (
    ),
    'ship_to_contact' => 
    array (
    ),
    'start_datetime' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'type_2' => 
    array (
    ),
    'type_specimen_c' => 
    array (
    ),
    'm03_work_product_im_inventory_management_1_name' => 
    array (
    ),
  ),
);