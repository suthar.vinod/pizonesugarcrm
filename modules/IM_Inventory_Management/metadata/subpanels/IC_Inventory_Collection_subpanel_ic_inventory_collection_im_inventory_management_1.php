<?php
// created: 2021-02-25 17:10:00
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'action' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ACTION',
    'width' => 10,
  ),
  'action_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_ACTION_DATE',
    'width' => 10,
    'default' => true,
  ),
  'location_type' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_LOCATION_TYPE',
    'width' => 10,
  ),
  'location_building' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_LOCATION_BUILDING',
    'width' => 10,
  ),
  'location_equipment' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_LOCATION_EQUIPMENT',
    'id' => 'EQUIP_EQUIPMENT_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Equip_Equipment',
    'target_record_key' => 'equip_equipment_id_c',
  ),
  'location_room' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_LOCATION_ROOM',
    'id' => 'RMS_ROOM_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'RMS_Room',
    'target_record_key' => 'rms_room_id_c',
  ),
  'location_shelf' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_LOCATION_SHELF',
    'width' => 10,
  ),
  'location_cabinet' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_LOCATION_CABINET',
    'width' => 10,
  ),
  'ship_to_contact' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_SHIP_TO_CONTACT',
    'id' => 'CONTACT_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Contacts',
    'target_record_key' => 'contact_id_c',
  ),
  'ship_to_company' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_SHIP_TO_COMPANY',
    'id' => 'ACCOUNT_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Accounts',
    'target_record_key' => 'account_id_c',
  ),
  'ship_to_address' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_SHIP_TO_ADDRESS',
    'id' => 'CA_COMPANY_ADDRESS_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'CA_Company_Address',
    'target_record_key' => 'ca_company_address_id_c',
  ),
);