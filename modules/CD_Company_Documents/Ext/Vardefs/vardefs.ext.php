<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CD_Company_Documents/Ext/Vardefs/accounts_cd_company_documents_1_CD_Company_Documents.php

// created: 2019-02-06 20:01:35
$dictionary["CD_Company_Documents"]["fields"]["accounts_cd_company_documents_1"] = array (
  'name' => 'accounts_cd_company_documents_1',
  'type' => 'link',
  'relationship' => 'accounts_cd_company_documents_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_CD_COMPANY_DOCUMENTS_1_FROM_CD_COMPANY_DOCUMENTS_TITLE',
  'id_name' => 'accounts_cd_company_documents_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["CD_Company_Documents"]["fields"]["accounts_cd_company_documents_1_name"] = array (
  'name' => 'accounts_cd_company_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_CD_COMPANY_DOCUMENTS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_cd_company_documents_1accounts_ida',
  'link' => 'accounts_cd_company_documents_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["CD_Company_Documents"]["fields"]["accounts_cd_company_documents_1accounts_ida"] = array (
  'name' => 'accounts_cd_company_documents_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_CD_COMPANY_DOCUMENTS_1_FROM_CD_COMPANY_DOCUMENTS_TITLE_ID',
  'id_name' => 'accounts_cd_company_documents_1accounts_ida',
  'link' => 'accounts_cd_company_documents_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/CD_Company_Documents/Ext/Vardefs/sugarfield_date_of_qualification_c.php

 // created: 2019-02-20 13:57:52
$dictionary['CD_Company_Documents']['fields']['date_of_qualification_c']['labelValue']='Qualified Date';
$dictionary['CD_Company_Documents']['fields']['date_of_qualification_c']['enforced']='';
$dictionary['CD_Company_Documents']['fields']['date_of_qualification_c']['dependency']='equal($category_id,"Qualification")';

 
?>
<?php
// Merged from custom/Extension/modules/CD_Company_Documents/Ext/Vardefs/sugarfield_method_of_qualification_c.php

 // created: 2019-02-20 13:50:01
$dictionary['CD_Company_Documents']['fields']['method_of_qualification_c']['labelValue']='Method of Qualification';
$dictionary['CD_Company_Documents']['fields']['method_of_qualification_c']['visibility_grid']=array (
  'trigger' => 'category_id',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Material Transfer Agreement' => 
    array (
    ),
    'MSA' => 
    array (
    ),
    'MSA Amendment' => 
    array (
    ),
    'NDA_CDA' => 
    array (
    ),
    'Qualification' => 
    array (
      0 => '',
      1 => 'APS Management Approval',
      2 => 'CV',
      3 => 'Questionnaire',
      4 => 'Site Visit',
    ),
    'Quality Agreement' => 
    array (
    ),
    'Supplemental' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/CD_Company_Documents/Ext/Vardefs/sugarfield_vendor_status_c.php

 // created: 2019-02-20 13:43:28
$dictionary['CD_Company_Documents']['fields']['vendor_status_c']['labelValue']='Vendor Status';
$dictionary['CD_Company_Documents']['fields']['vendor_status_c']['visibility_grid']=array (
  'trigger' => 'category_id',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Material Transfer Agreement' => 
    array (
    ),
    'MSA' => 
    array (
    ),
    'MSA Amendment' => 
    array (
    ),
    'NDA_CDA' => 
    array (
    ),
    'Qualification' => 
    array (
      0 => '',
      1 => 'Approved',
      2 => 'Disapproved',
      3 => 'Pending Approval',
      4 => 'Retired',
    ),
    'Quality Agreement' => 
    array (
    ),
    'Supplemental' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/CD_Company_Documents/Ext/Vardefs/sugarfield_category_id.php

 // created: 2019-05-23 16:46:01
$dictionary['CD_Company_Documents']['fields']['category_id']['audited']=true;
$dictionary['CD_Company_Documents']['fields']['category_id']['massupdate']=true;
$dictionary['CD_Company_Documents']['fields']['category_id']['options']='category_id_list';
$dictionary['CD_Company_Documents']['fields']['category_id']['duplicate_merge']='enabled';
$dictionary['CD_Company_Documents']['fields']['category_id']['duplicate_merge_dom_value']='1';
$dictionary['CD_Company_Documents']['fields']['category_id']['merge_filter']='disabled';
$dictionary['CD_Company_Documents']['fields']['category_id']['unified_search']=false;
$dictionary['CD_Company_Documents']['fields']['category_id']['calculated']=false;
$dictionary['CD_Company_Documents']['fields']['category_id']['dependency']=false;
$dictionary['CD_Company_Documents']['fields']['category_id']['reportable']=true;

 
?>
<?php
// Merged from custom/Extension/modules/CD_Company_Documents/Ext/Vardefs/sugarfield_document_name.php

 // created: 2019-06-19 16:05:29
$dictionary['CD_Company_Documents']['fields']['document_name']['audited']=true;
$dictionary['CD_Company_Documents']['fields']['document_name']['massupdate']=false;
$dictionary['CD_Company_Documents']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['CD_Company_Documents']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['CD_Company_Documents']['fields']['document_name']['merge_filter']='disabled';
$dictionary['CD_Company_Documents']['fields']['document_name']['unified_search']=false;
$dictionary['CD_Company_Documents']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['CD_Company_Documents']['fields']['document_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/CD_Company_Documents/Ext/Vardefs/sugarfield_active_date.php

 // created: 2019-06-27 13:22:40
$dictionary['CD_Company_Documents']['fields']['active_date']['audited']=true;
$dictionary['CD_Company_Documents']['fields']['active_date']['massupdate']=true;
$dictionary['CD_Company_Documents']['fields']['active_date']['duplicate_merge']='enabled';
$dictionary['CD_Company_Documents']['fields']['active_date']['duplicate_merge_dom_value']='1';
$dictionary['CD_Company_Documents']['fields']['active_date']['merge_filter']='disabled';
$dictionary['CD_Company_Documents']['fields']['active_date']['unified_search']=false;
$dictionary['CD_Company_Documents']['fields']['active_date']['calculated']=false;
$dictionary['CD_Company_Documents']['fields']['active_date']['enable_range_search']=false;
$dictionary['CD_Company_Documents']['fields']['active_date']['dependency']='isInList($category_id,createList("MSA","MSA Amendment","NDA_CDA","Quality Agreement","Material Transfer Agreement","Supplemental"))';

 
?>
<?php
// Merged from custom/Extension/modules/CD_Company_Documents/Ext/Vardefs/sugarfield_exp_date.php

 // created: 2019-06-27 13:23:18
$dictionary['CD_Company_Documents']['fields']['exp_date']['audited']=true;
$dictionary['CD_Company_Documents']['fields']['exp_date']['massupdate']=true;
$dictionary['CD_Company_Documents']['fields']['exp_date']['duplicate_merge']='enabled';
$dictionary['CD_Company_Documents']['fields']['exp_date']['duplicate_merge_dom_value']='1';
$dictionary['CD_Company_Documents']['fields']['exp_date']['merge_filter']='disabled';
$dictionary['CD_Company_Documents']['fields']['exp_date']['unified_search']=false;
$dictionary['CD_Company_Documents']['fields']['exp_date']['calculated']=false;
$dictionary['CD_Company_Documents']['fields']['exp_date']['enable_range_search']=false;
$dictionary['CD_Company_Documents']['fields']['exp_date']['dependency']='isInList($category_id,createList("MSA","MSA Amendment","NDA_CDA","Quality Agreement","Material Transfer Agreement","Supplemental"))';

 
?>
