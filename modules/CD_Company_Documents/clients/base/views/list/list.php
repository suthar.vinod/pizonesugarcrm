<?php
$module_name = 'CD_Company_Documents';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'document_name',
                'label' => 'LBL_NAME',
                'link' => true,
                'default' => true,
                'enabled' => true,
              ),
              1 => 
              array (
                'name' => 'active_date',
                'label' => 'LBL_LIST_ACTIVE_DATE',
                'default' => true,
                'enabled' => true,
              ),
              2 => 
              array (
                'name' => 'exp_date',
                'label' => 'LBL_LIST_EXP_DATE',
                'default' => true,
                'enabled' => true,
              ),
              3 => 
              array (
                'name' => 'date_of_qualification_c',
                'label' => 'LBL_DATE_OF_QUALIFICATION',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'method_of_qualification_c',
                'label' => 'LBL_METHOD_OF_QUALIFICATION',
                'enabled' => true,
                'default' => true,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
