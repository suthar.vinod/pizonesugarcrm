<?php
// created: 2019-06-27 13:34:14
$subpanel_layout['list_fields'] = array (
  'document_name' => 
  array (
    'name' => 'document_name',
    'vname' => 'LBL_LIST_DOCUMENT_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'uploadfile' => 
  array (
    'type' => 'file',
    'vname' => 'LBL_FILE_UPLOAD',
    'width' => 10,
    'default' => true,
  ),
  'category_id' => 
  array (
    'name' => 'category_id',
    'usage' => 'query_only',
  ),
  'active_date' => 
  array (
    'name' => 'active_date',
    'vname' => 'LBL_DOC_ACTIVE_DATE',
    'width' => 10,
    'sortable' => false,
    'default' => true,
  ),
  'exp_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_DOC_EXP_DATE',
    'width' => 10,
    'default' => true,
  ),
  'date_of_qualification_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_DATE_OF_QUALIFICATION',
    'width' => 10,
    'default' => true,
  ),
  'method_of_qualification_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_METHOD_OF_QUALIFICATION',
    'width' => 10,
  ),
  'vendor_status_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_VENDOR_STATUS',
    'width' => 10,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
);