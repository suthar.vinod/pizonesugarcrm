<?php

class customLogicHook {

    function setQualificationMethod($bean, $event, $arguments) {
        global $db;
      
        $isIdExist = $bean->fetched_row['id'];

        //I am writing this query because may be this record will not create by the subpanel
        //It is directly creating by the create view of Company Document Module
        $queryForGetIdOfAccounts = "SELECT accounts_cstm.id_c , cd_company_documents_cstm.method_of_qualification_c, cd_company_documents_cstm.vendor_status_c, cd_company_documents.category_id, cd_company_documents_cstm.date_of_qualification_c FROM accounts_cstm LEFT JOIN accounts_cd_company_documents_1_c "
                . "ON accounts_cstm.id_c = accounts_cd_company_documents_1_c.accounts_cd_company_documents_1accounts_ida "
                . "LEFT JOIN cd_company_documents ON accounts_cd_company_documents_1_c.accounts_cd_company_documents_1cd_company_documents_idb = cd_company_documents.id "
                . "LEFT JOIN cd_company_documents_cstm ON cd_company_documents.id = cd_company_documents_cstm.id_c "
                . "WHERE cd_company_documents.id = '" . $bean->id . "'";

        $qualificationMethod = $db->query($queryForGetIdOfAccounts);
        $resultQualification = $db->fetchByAssoc($qualificationMethod);

        $accountsID = $resultQualification['id_c'];
        $methodValue = $resultQualification['method_of_qualification_c'];
        $statusValue = $resultQualification['vendor_status_c'];
        $category = $resultQualification['category_id'];
        $qualification_date = $resultQualification['date_of_qualification_c'];

        //If related Companies record exist
        if (!empty($accountsID)) {

            //Company Document record already exist. In Edit mode
            if (!empty($isIdExist)) {

                //Query to Get the most recent created relate record 
                $queryToGetMostRecentQualification = "SELECT cd_company_documents_cstm.method_of_qualification_c, cd_company_documents_cstm.vendor_status_c, cd_company_documents.id, cd_company_documents_cstm.date_of_qualification_c FROM cd_company_documents "
                        . "LEFT JOIN cd_company_documents_cstm ON cd_company_documents.id = cd_company_documents_cstm.id_c "
                        . "LEFT JOIN accounts_cd_company_documents_1_c ON cd_company_documents_cstm.id_c = accounts_cd_company_documents_1_c.accounts_cd_company_documents_1cd_company_documents_idb "
                        . "LEFT JOIN accounts_cstm ON accounts_cd_company_documents_1_c.accounts_cd_company_documents_1accounts_ida = accounts_cstm.id_c "
                        . "WHERE accounts_cstm.id_c = '" . $accountsID . "' AND cd_company_documents.deleted = 0 AND accounts_cd_company_documents_1_c.deleted = 0 AND cd_company_documents.category_id = 'Qualification' "
                        . "ORDER BY cd_company_documents.date_entered DESC LIMIT 0,1";

                $qualificationMethod = $db->query($queryToGetMostRecentQualification);
                $resultQualification = $db->fetchByAssoc($qualificationMethod);
                $documentID = $resultQualification['id'];
                $methodValue = $resultQualification['method_of_qualification_c'];
                $statusValue = $resultQualification['vendor_status_c'];
                $qualification_date = $resultQualification['date_of_qualification_c'];

                //No related record exist hence populate parent with default values
                if (empty($methodValue) && empty($statusValue)) {
                    $methodValue = "TBD";
                    $statusValue = "Pending Approval";
                }

                $queryForUpdateQualificationMethod = "UPDATE accounts_cstm SET method_of_qualification_c = '" . $methodValue . "', current_vendor_status_c = '" . $statusValue . "' "
                        . ", approval_date_c = '".$qualification_date."' "
                        . "WHERE id_c = '" . $accountsID . "'";
                $db->query($queryForUpdateQualificationMethod);
            } else { //New record is creating
                if ($category != "Qualification") {
                    $methodAndStatus = $this->getMethodAndStatusForCategoryNotQualification($accountsID);
                    $methodValue = $methodAndStatus[0];
                    $statusValue = $methodAndStatus[1];
                    $qualification_date = $methodAndStatus[2];
                }

                $queryForUpdateQualificationMethod = "UPDATE accounts_cstm SET method_of_qualification_c = '" . $methodValue . "', current_vendor_status_c = '" . $statusValue . "' "
                        . ", approval_date_c = '" . $qualification_date . "' "
                        . "WHERE id_c = '" . $accountsID . "'";
                $db->query($queryForUpdateQualificationMethod);
            }
        }
    }

    /**
     * This method with return the "Method of Qualification", "Vendor Status" and "Qualification Date"
     * for the most recently created record which has the Category equal to Qualification
     * @global type $db
     * @param type $accountID
     * @return type
     */
    function getMethodAndStatusForCategoryNotQualification($accountID) {
        global $db;

        $queryForMethodAndStatus = "SELECT cd_company_documents_cstm.method_of_qualification_c, cd_company_documents_cstm.vendor_status_c, cd_company_documents.id, cd_company_documents_cstm.date_of_qualification_c FROM accounts_cstm LEFT JOIN accounts_cd_company_documents_1_c "
                . "ON accounts_cstm.id_c = accounts_cd_company_documents_1_c.accounts_cd_company_documents_1accounts_ida "
                . "LEFT JOIN cd_company_documents ON accounts_cd_company_documents_1_c.accounts_cd_company_documents_1cd_company_documents_idb = cd_company_documents.id "
                . "LEFT JOIN cd_company_documents_cstm ON cd_company_documents.id = cd_company_documents_cstm.id_c "
                . "WHERE accounts_cstm.id_c = '" . $accountID . "' AND cd_company_documents.category_id = 'Qualification' AND accounts_cd_company_documents_1_c.deleted = 0 "
                . "ORDER BY cd_company_documents.date_entered DESC "
                . "LIMIT 0,1";

        $methodAndStatus = $db->query($queryForMethodAndStatus);
        $result = $db->fetchByAssoc($methodAndStatus);

        $idCD = $result['id'];
        $methodValue = $result['method_of_qualification_c'];
        $statusValue = $result['vendor_status_c'];
        $qualification_date = $result['date_of_qualification_c'];

        if (empty($methodValue) && empty($statusValue)) {
            $methodValue = "TBD";
            $statusValue = "Pending Approval";
        }

        return array($methodValue, $statusValue, $qualification_date);
    }

    /**
     * When record unlink or delete , populate the qualification method field value with the latest uploaded company record
     */
    function updateQualificationMethod($bean, $event, $arguments) {
        global $db;

        if ($arguments['related_module'] == "Accounts") {

            //Get the related Companies' record
            $queryForGetIdOfAccounts = "SELECT accounts_cstm.id_c FROM accounts_cstm LEFT JOIN accounts_cd_company_documents_1_c "
                    . "ON accounts_cstm.id_c = accounts_cd_company_documents_1_c.accounts_cd_company_documents_1accounts_ida "
                    . "LEFT JOIN cd_company_documents ON accounts_cd_company_documents_1_c.accounts_cd_company_documents_1cd_company_documents_idb = cd_company_documents.id "
                    . "WHERE cd_company_documents.id = '" . $bean->id . "' ";
            $accountsIdQuery = $db->query($queryForGetIdOfAccounts);
            $result = $db->fetchByAssoc($accountsIdQuery);
            $accountsID = $result['id_c'];

            if (!empty($accountsID)) {
                //Get the most recent related Company Document Record
                $queryToGetMostRecentQualification = "SELECT cd_company_documents.category_id,  cd_company_documents_cstm.method_of_qualification_c, cd_company_documents_cstm.vendor_status_c, cd_company_documents_cstm.date_of_qualification_c FROM cd_company_documents "
                        . "LEFT JOIN cd_company_documents_cstm ON cd_company_documents.id = cd_company_documents_cstm.id_c "
                        . "LEFT JOIN accounts_cd_company_documents_1_c ON cd_company_documents_cstm.id_c = accounts_cd_company_documents_1_c.accounts_cd_company_documents_1cd_company_documents_idb "
                        . "LEFT JOIN accounts_cstm ON accounts_cd_company_documents_1_c.accounts_cd_company_documents_1accounts_ida = accounts_cstm.id_c "
                        . "WHERE accounts_cstm.id_c = '" . $accountsID . "' AND cd_company_documents.deleted = 0 AND cd_company_documents.id <> '" . $bean->id . "' AND accounts_cd_company_documents_1_c.deleted = 0 AND cd_company_documents.category_id = 'Qualification'"
                        . "ORDER BY cd_company_documents.date_entered DESC LIMIT 0,1";

                $qualificationMethod = $db->query($queryToGetMostRecentQualification);
                $resultQualification = $db->fetchByAssoc($qualificationMethod);
                $methodValue = $resultQualification['method_of_qualification_c'];
                $statusValue = $resultQualification['vendor_status_c'];
                $qualification_date = $resultQualification['date_of_qualification_c'];
                if (empty($methodValue) && empty($statusValue)) {
                    $methodValue = "TBD";
                    $statusValue = "Pending Approval";
                }

                $queryForUpdateQualificationMethod = "UPDATE accounts_cstm SET method_of_qualification_c = '" . $methodValue . "' , current_vendor_status_c = '" . $statusValue . "' "
                        . ", approval_date_c = '" . $qualification_date . "' WHERE id_c = '" . $accountsID . "'";
                $result = $db->query($queryForUpdateQualificationMethod);
                if ($result) {
                    $GLOBALS['log']->debug("updated successfully");
                }
            }
        }
    }

}
