<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CDU_CD_Utilization/Ext/Dependencies/controlled_document_uti_readonly_dep.php


$dependencies['CDU_CD_Utilization']['controlled_document_uti_readonly_dep'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('controlled_document_uti','deviation_rate_c'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'controlled_document_uti',
                'value'  => 'true',  
            ),
        ), 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'deviation_rate_c',
                'value'  => 'true',  
            ),
        ), 
    ),
);
?>
