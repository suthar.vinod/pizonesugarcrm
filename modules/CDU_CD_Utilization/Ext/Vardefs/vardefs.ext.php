<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CDU_CD_Utilization/Ext/Vardefs/sugarfield_name.php

 // created: 2021-04-13 12:29:09
$dictionary['CDU_CD_Utilization']['fields']['name']['importable']='false';
$dictionary['CDU_CD_Utilization']['fields']['name']['duplicate_merge']='disabled';
$dictionary['CDU_CD_Utilization']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['CDU_CD_Utilization']['fields']['name']['merge_filter']='disabled';
$dictionary['CDU_CD_Utilization']['fields']['name']['unified_search']=false;
$dictionary['CDU_CD_Utilization']['fields']['name']['calculated']='true';
$dictionary['CDU_CD_Utilization']['fields']['name']['formula']='concat(related($erd_error_documents_cdu_cd_utilization_1,"name")," CUD ",toString($month_end))';
$dictionary['CDU_CD_Utilization']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/CDU_CD_Utilization/Ext/Vardefs/sugarfield_deviation_rate_c.php

 // created: 2021-04-30 05:06:12
$dictionary['CDU_CD_Utilization']['fields']['deviation_rate_c']['labelValue']='Deviation Rate';
$dictionary['CDU_CD_Utilization']['fields']['deviation_rate_c']['enforced']='';
$dictionary['CDU_CD_Utilization']['fields']['deviation_rate_c']['dependency']='';
$dictionary['CDU_CD_Utilization']['fields']['deviation_rate_c']['required_formula']='';
$dictionary['CDU_CD_Utilization']['fields']['deviation_rate_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CDU_CD_Utilization/Ext/Vardefs/sugarfield_controlled_document_uti.php

 // created: 2021-06-24 10:32:22
$dictionary['CDU_CD_Utilization']['fields']['controlled_document_uti']['precision']=1;

 
?>
<?php
// Merged from custom/Extension/modules/CDU_CD_Utilization/Ext/Vardefs/erd_error_documents_cdu_cd_utilization_1_CDU_CD_Utilization.php

// created: 2021-06-29 08:02:02
$dictionary["CDU_CD_Utilization"]["fields"]["erd_error_documents_cdu_cd_utilization_1"] = array (
  'name' => 'erd_error_documents_cdu_cd_utilization_1',
  'type' => 'link',
  'relationship' => 'erd_error_documents_cdu_cd_utilization_1',
  'source' => 'non-db',
  'module' => 'Erd_Error_Documents',
  'bean_name' => 'Erd_Error_Documents',
  'side' => 'right',
  'vname' => 'LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_CDU_CD_UTILIZATION_TITLE',
  'id_name' => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
  'link-type' => 'one',
);
$dictionary["CDU_CD_Utilization"]["fields"]["erd_error_documents_cdu_cd_utilization_1_name"] = array (
  'name' => 'erd_error_documents_cdu_cd_utilization_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'save' => true,
  'id_name' => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
  'link' => 'erd_error_documents_cdu_cd_utilization_1',
  'table' => 'erd_error_documents',
  'module' => 'Erd_Error_Documents',
  'rname' => 'name',
);
$dictionary["CDU_CD_Utilization"]["fields"]["erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida"] = array (
  'name' => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_CDU_CD_UTILIZATION_TITLE_ID',
  'id_name' => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
  'link' => 'erd_error_documents_cdu_cd_utilization_1',
  'table' => 'erd_error_documents',
  'module' => 'Erd_Error_Documents',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/CDU_CD_Utilization/Ext/Vardefs/sugarfield_number_of_procedures_2.php

 // created: 2021-07-08 09:24:50
$dictionary['CDU_CD_Utilization']['fields']['number_of_procedures_2']['required']=true;
$dictionary['CDU_CD_Utilization']['fields']['number_of_procedures_2']['precision']=1;
$dictionary['CDU_CD_Utilization']['fields']['number_of_procedures_2']['required_formula']='equal(related($erd_error_documents_cdu_cd_utilization_1,"deviation_rate_basis_c"),"Procedures")';

 
?>
<?php
// Merged from custom/Extension/modules/CDU_CD_Utilization/Ext/Vardefs/sugarfield_number_of_tests_2.php

 // created: 2021-07-08 09:25:26
$dictionary['CDU_CD_Utilization']['fields']['number_of_tests_2']['required']=true;
$dictionary['CDU_CD_Utilization']['fields']['number_of_tests_2']['importable']='true';
$dictionary['CDU_CD_Utilization']['fields']['number_of_tests_2']['duplicate_merge']='enabled';
$dictionary['CDU_CD_Utilization']['fields']['number_of_tests_2']['duplicate_merge_dom_value']='1';
$dictionary['CDU_CD_Utilization']['fields']['number_of_tests_2']['precision']=1;
$dictionary['CDU_CD_Utilization']['fields']['number_of_tests_2']['required_formula']='equal(related($erd_error_documents_cdu_cd_utilization_1,"deviation_rate_basis_c"),"Studies")';

 
?>
<?php
// Merged from custom/Extension/modules/CDU_CD_Utilization/Ext/Vardefs/sugarfield_number_of_animals_2.php

 // created: 2021-07-08 09:26:02
$dictionary['CDU_CD_Utilization']['fields']['number_of_animals_2']['required']=true;
$dictionary['CDU_CD_Utilization']['fields']['number_of_animals_2']['precision']=1;
$dictionary['CDU_CD_Utilization']['fields']['number_of_animals_2']['required_formula']='equal(related($erd_error_documents_cdu_cd_utilization_1,"deviation_rate_basis_c"),"Animal Days")';

 
?>
