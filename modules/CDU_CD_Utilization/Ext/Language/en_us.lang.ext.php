<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CDU_CD_Utilization/Ext/Language/en_us.customerd_error_documents_cdu_cd_utilization_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_ERD_ERROR_DOCUMENTS_TITLE'] = 'Controlled Documents';
$mod_strings['LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_CDU_CD_UTILIZATION_TITLE'] = 'Controlled Documents';

?>
<?php
// Merged from custom/Extension/modules/CDU_CD_Utilization/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTROLLED_DOCUMENT_UTI'] = 'CD Utilizations/Deviation Rates';
$mod_strings['LNK_LIST'] = 'View CD Utilizations/Deviation Rates';
$mod_strings['LBL_MODULE_NAME'] = 'CD Utilizations/Deviation Rates';
$mod_strings['LNK_IMPORT_CDU_CD_UTILIZATION'] = 'Import CD Utilizations/Deviation Rates';
$mod_strings['LBL_CDU_CD_UTILIZATION_SUBPANEL_TITLE'] = 'CD Utilizations/Deviation Rates';
$mod_strings['LBL_NUMBER_OF_TESTS_2'] = 'Number of Studies';
$mod_strings['LBL_DEVIATION_RATE'] = 'Deviation Rate';

?>
