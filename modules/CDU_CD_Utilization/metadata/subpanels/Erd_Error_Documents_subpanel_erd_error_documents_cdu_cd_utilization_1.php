<?php
// created: 2021-06-24 10:28:38
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'controlled_document_uti' => 
  array (
    'readonly' => false,
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_CONTROLLED_DOCUMENT_UTI',
    'width' => 10,
  ),
  'month_end' => 
  array (
    'readonly' => false,
    'type' => 'date',
    'vname' => 'LBL_MONTH_END',
    'width' => 10,
    'default' => true,
  ),
  'deviation_rate_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_DEVIATION_RATE',
    'width' => 10,
  ),
  'number_of_procedures_2' => 
  array (
    'readonly' => false,
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_NUMBER_OF_PROCEDURES_2',
    'width' => 10,
  ),
  'number_of_tests_2' => 
  array (
    'readonly' => false,
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_NUMBER_OF_TESTS_2',
    'width' => 10,
  ),
  'number_of_animals_2' => 
  array (
    'readonly' => false,
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_NUMBER_OF_ANIMALS_2',
    'width' => 10,
  ),
);