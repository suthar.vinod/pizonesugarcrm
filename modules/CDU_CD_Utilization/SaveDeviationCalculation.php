<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class SaveDeviationCalculation
{
    function SaveDeviationCalculationRecord($bean, $event, $arguments)
    {
        global $db;

        $sqlgetrelbean = "select * from erd_error_documents_cdu_cd_utilization_1_c where erd_error_documents_cdu_cd_utilization_1cdu_cd_utilization_idb='" . $bean->id . "' and deleted=0";

        $resultrelbean = $db->query($sqlgetrelbean);

        $rowrelbean = $db->fetchByAssoc($resultrelbean);

        $related_erd_id = $rowrelbean['erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida'];

        $month_end = $bean->month_end;
        /*Get the month from month end without leading zero */
        $CDU_MonthEnd = date('n', strtotime($month_end));
        $CDU_year = date('Y', strtotime($month_end));

        $sqlComm = "SELECT count(*) as NUM, T2.*,T1.id,T1.name,T3.date_error_occurred_c,T3.error_classification_c,MONTH(T3.date_error_occurred_c) as dateoccmonth 
                
                    FROM m06_error_erd_error_documents_1_c AS T2
    
                    LEFT JOIN m06_error AS T1 ON T2.m06_error_erd_error_documents_1m06_error_ida = T1.id
    
                    LEFT JOIN m06_error_cstm AS T3 ON T1.id = T3.id_c
    
                    WHERE T2.m06_error_erd_error_documents_1erd_error_documents_idb='" . $related_erd_id . "' AND T3.error_classification_c='error' AND MONTH(T3.date_error_occurred_c)='" . $CDU_MonthEnd . "' AND YEAR(T3.date_error_occurred_c)='" . $CDU_year . "' AND T2.deleted=0";

        $resultComm = $db->query($sqlComm);

        $rowComm = $db->fetchByAssoc($resultComm);

        $NoofCommRecord = $rowComm['NUM'];

        $controlled_document_uti = $bean->controlled_document_uti;


        if ($controlled_document_uti != '') {
            $final_cal = $NoofCommRecord / $controlled_document_uti * 100;

            $deviation_rate_c = number_format($final_cal, 2);
        } else {
            $deviation_rate_c = "0.00";
        }

        $bean->deviation_rate_c = $deviation_rate_c;
        $bean->save();
    }
}
