({
    extendsFrom: 'CreateView',
    initialize: function(options) {
        this._super('initialize', [options]);
        this.on('render', _.bind(this.onload_function, this));
    },
    saveAndClose: function() {
        var self = this;
        var parent_module = this.model.link.bean.attributes._module;
        var data_cells_per_cd = this.model.link.bean.attributes.data_cells_per_cd_c;
        var average_cd_pages_per_basis = this.model.link.bean.attributes.average_cd_pages_per_basis_c;
        var number_of_animals = this.model.get("number_of_animals_2") == "" ? 1 : this.model.get("number_of_animals_2");
        var number_of_procedures = this.model.get("number_of_procedures_2") == "" ? 1 : this.model.get("number_of_procedures_2");
        var number_of_tests = this.model.get("number_of_tests_2") == "" ? 1 : this.model.get("number_of_tests_2");
        // Check if  related parent record is Erd_Error_Documents
        if (parent_module == "Erd_Error_Documents" && (number_of_animals != "" || number_of_procedures != "" || number_of_tests != "")) {
            var formula = data_cells_per_cd * average_cd_pages_per_basis * number_of_animals * number_of_procedures * number_of_tests * 30;
            self.model.set("controlled_document_uti", formula);
            self.initiateSave(_.bind(function initSave() {
                if (self.closestComponent('drawer')) {
                    app.drawer.close(self.context, self.model);
                }
            }, self));
        } else {
            this.initiateSave(_.bind(function() {
                app.navigate(this.context, this.model);
            }, this));
        }
    },
    onload_function: function() {

        // Check if there's related parent record
        if (!_.isEmpty(this.model.link)) {
            var parent_module = this.model.link.bean.attributes._module;
            var deviation_rate = this.model.link.bean.attributes.deviation_rate_basis_c;

            // Check if related parent record is Erd_Error_Documents
            if (parent_module == "Erd_Error_Documents") {
                if (deviation_rate == 'Animal Days') {
                    $('[data-name="number_of_procedures_2"]').css('visibility', 'hidden');
                    $('[data-name="number_of_tests_2"]').css('visibility', 'hidden');
                } else if (deviation_rate == 'Procedures') {
                    $('[data-name="number_of_animals_2"]').css('visibility', 'hidden');
                    $('[data-name="number_of_tests_2"]').css('visibility', 'hidden');
                } else if (deviation_rate == 'Studies') {
                    $('[data-name="number_of_procedures_2"]').css('visibility', 'hidden');
                    $('[data-name="number_of_animals_2"]').css('visibility', 'hidden');
                } else {
                    $('[data-name="number_of_tests_2"]').css('visibility', 'hidden');
                    $('[data-name="number_of_procedures_2"]').css('visibility', 'hidden');
                    $('[data-name="number_of_animals_2"]').css('visibility', 'hidden');
                }
            }
        }
        this._super("render");
    },
})