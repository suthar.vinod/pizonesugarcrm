({
    extendsFrom: 'RecordView',
    initialize: function(options) {
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.onload_function, this);
        this.model.on('change:erd_error_documents_cdu_cd_utilization_1_name', this.onload_function, this);
    },

    onload_function: function() {
        var CDId = this.model.get("erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida");
        console.log(CDId);

        if (CDId != undefined) {
            App.api.call("get", "rest/v10/Erd_Error_Documents/" + CDId + "?fields=deviation_rate_basis_c", null, {
                success: function(data) {
                    console.log('data', data);
                    var deviation_rate = data.deviation_rate_basis_c;
                    console.log('deviation_rate', deviation_rate);
                    if (deviation_rate == 'Animal Days') {
                        $('[data-name="number_of_procedures_2"]').css('visibility', 'hidden');
                        $('[data-name="number_of_tests_2"]').css('visibility', 'hidden');
                    } else if (deviation_rate == 'Procedures') {
                        $('[data-name="number_of_animals_2"]').css('visibility', 'hidden');
                        $('[data-name="number_of_tests_2"]').css('visibility', 'hidden');
                    } else if (deviation_rate == 'Studies') {
                        $('[data-name="number_of_procedures_2"]').css('visibility', 'hidden');
                        $('[data-name="number_of_animals_2"]').css('visibility', 'hidden');
                    } else {
                        $('[data-name="number_of_tests_2"]').css('visibility', 'hidden');
                        $('[data-name="number_of_procedures_2"]').css('visibility', 'hidden');
                        $('[data-name="number_of_animals_2"]').css('visibility', 'hidden');
                    }
                }
            });
        }
        this._super("render");
    },
})