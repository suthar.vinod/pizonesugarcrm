<?php
// created: 2021-06-24 10:28:40
$viewdefs['CDU_CD_Utilization']['base']['view']['subpanel-for-erd_error_documents-erd_error_documents_cdu_cd_utilization_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'controlled_document_uti',
          'label' => 'LBL_CONTROLLED_DOCUMENT_UTI',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'month_end',
          'label' => 'LBL_MONTH_END',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'deviation_rate_c',
          'label' => 'LBL_DEVIATION_RATE',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'number_of_procedures_2',
          'label' => 'LBL_NUMBER_OF_PROCEDURES_2',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'number_of_tests_2',
          'label' => 'LBL_NUMBER_OF_TESTS_2',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'number_of_animals_2',
          'label' => 'LBL_NUMBER_OF_ANIMALS_2',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);