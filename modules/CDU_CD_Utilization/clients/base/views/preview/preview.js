({
    extendsFrom: 'PreviewView',
    initialize: function(options) {
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.onload_function, this);
    },

    onload_function: function() {
        $('.preview-edit-btn').hide();
    },

})