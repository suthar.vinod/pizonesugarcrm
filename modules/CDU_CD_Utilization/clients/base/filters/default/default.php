<?php
// created: 2021-04-13 12:48:46
$viewdefs['CDU_CD_Utilization']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'assigned_user_name' => 
    array (
    ),
    'erd_error_documents_cdu_cd_utilization_1_name' => 
    array (
    ),
    'controlled_document_uti' => 
    array (
    ),
    'month_end' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'name' => 
    array (
    ),
    'number_of_animals_2' => 
    array (
    ),
    'number_of_procedures_2' => 
    array (
    ),
    'number_of_tests_2' => 
    array (
    ),
    'tag' => 
    array (
    ),
  ),
);