({
    extendsFrom: 'EditablelistbuttonField',
    initialize: function(options) {
        this._super("initialize", [options]);
        // this.model.on('change: error_category_c', this.function_error_category_c,this);

    },
    _loadTemplate: function() {
        this._super('_loadTemplate');
        $('input[name="number_of_procedures_2"]').prop('disabled', true);
        $('[name="number_of_procedures_2"]').attr('readOnly', true);
        $('input[name="number_of_tests_2"]').prop('disabled', true);
        $('[name="number_of_tests_2"]').attr('readOnly', true);
        $('input[name="number_of_animals_2"]').prop('disabled', true);
        $('[name="number_of_animals_2"]').attr('readOnly', true);
    },
})