<?php

$hook_version = 1;
$hook_array = Array();

$hook_array['after_save'][] = array(
    1,
    'Deviation Rate field addition',
    'custom/modules/CDU_CD_Utilization/SaveDeviationCalculation.php',
    'SaveDeviationCalculation',
    'SaveDeviationCalculationRecord',
);

$hook_array['after_relationship_add'][] = array(
    2,
    'Deviation Rate field Updation',
    'custom/modules/CDU_CD_Utilization/AddRelDeviationCalculation.php',
    'AddRelDeviationCalculation',
    'AddRelDeviationCalculationRecord',
);


?>