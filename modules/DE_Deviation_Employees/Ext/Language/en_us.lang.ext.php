<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Language/en_us.customm06_error_de_deviation_employees_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Language/en_us.customm06_error_de_deviation_employees_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_M06_ERROR_TITLE'] = 'Deviations';
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Deviations';

?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_M06_ERROR_TITLE'] = 'Deviations';
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Deviations';


?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Language/en_us.customcontacts_de_deviation_employees_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Language/en_us.customcontacts_de_deviation_employees_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Contacts';

?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Contacts';


?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DEVIATION_TYPE'] = 'Type';
$mod_strings['LBL_APS_EMPLOYEE'] = 'APS Employee';
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_M06_ERROR_TITLE'] = 'Communication';
$mod_strings['LNK_NEW_RECORD'] = 'Create Responsible Personnel';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Responsible Personnel';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Responsible Personnel';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Responsible Personnel vCard';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Responsible Personnel List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Responsible Personnel';
$mod_strings['LNK_LIST'] = 'View Responsible Personnel';
$mod_strings['LBL_MODULE_NAME'] = 'Responsible Personnel';
$mod_strings['LNK_IMPORT_DE_DEVIATION_EMPLOYEES'] = 'Import Responsible Personnel';
$mod_strings['LBL_DE_DEVIATION_EMPLOYEES_SUBPANEL_TITLE'] = 'Responsible Personnel';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Responsible Personnel';
$mod_strings['LBL_DE_DEVIATION_EMPLOYEES_FOCUS_DRAWER_DASHBOARD'] = 'Responsible Personnel Focus Drawer';
$mod_strings['LBL_DE_DEVIATION_EMPLOYEES_RECORD_DASHBOARD'] = 'Responsible Personnel Record Dashboard';

?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Language/en_us.customere_error_employees_de_deviation_employees_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ERE_ERROR_EMPLOYEES_DE_DEVIATION_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE'] = 'APS Employees';
$mod_strings['LBL_ERE_ERROR_EMPLOYEES_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'APS Employees';

?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Language/en_us.customde_deviation_employees_ere_error_employees_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DE_DEVIATION_EMPLOYEES_ERE_ERROR_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE'] = 'APS Employees';
$mod_strings['LBL_DE_DEVIATION_EMPLOYEES_ERE_ERROR_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'APS Employees';

?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Language/en_us.customm06_error_de_deviation_employees_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_2_FROM_M06_ERROR_TITLE'] = 'Communications';
$mod_strings['LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_2_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Communications';

?>
