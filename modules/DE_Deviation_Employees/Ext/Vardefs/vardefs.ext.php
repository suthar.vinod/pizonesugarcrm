<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Vardefs/m06_error_de_deviation_employees_1_DE_Deviation_Employees.php

// created: 2018-07-30 20:17:15
$dictionary["DE_Deviation_Employees"]["fields"]["m06_error_de_deviation_employees_1"] = array (
  'name' => 'm06_error_de_deviation_employees_1',
  'type' => 'link',
  'relationship' => 'm06_error_de_deviation_employees_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'side' => 'right',
  'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'id_name' => 'm06_error_de_deviation_employees_1m06_error_ida',
  'link-type' => 'one',
);
$dictionary["DE_Deviation_Employees"]["fields"]["m06_error_de_deviation_employees_1_name"] = array (
  'name' => 'm06_error_de_deviation_employees_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_M06_ERROR_TITLE',
  'save' => true,
  'id_name' => 'm06_error_de_deviation_employees_1m06_error_ida',
  'link' => 'm06_error_de_deviation_employees_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'name',
);
$dictionary["DE_Deviation_Employees"]["fields"]["m06_error_de_deviation_employees_1m06_error_ida"] = array (
  'name' => 'm06_error_de_deviation_employees_1m06_error_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE_ID',
  'id_name' => 'm06_error_de_deviation_employees_1m06_error_ida',
  'link' => 'm06_error_de_deviation_employees_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Vardefs/disablefields.php

/**
 * Created by PhpStorm.
 * User: nabeel.alvi
 * Date: 10/2/18
 * Time: 10:55 AM
 */

$dictionary['DE_Deviation_Employees']['fields']['name']['required']=false;
$dictionary['DE_Deviation_Employees']['fields']['name']['readonly']=true;

?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Vardefs/contacts_de_deviation_employees_1_DE_Deviation_Employees.php

// created: 2019-02-20 19:46:48
$dictionary["DE_Deviation_Employees"]["fields"]["contacts_de_deviation_employees_1"] = array (
  'name' => 'contacts_de_deviation_employees_1',
  'type' => 'link',
  'relationship' => 'contacts_de_deviation_employees_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE',
  'id_name' => 'contacts_de_deviation_employees_1contacts_ida',
  'link-type' => 'one',
);
$dictionary["DE_Deviation_Employees"]["fields"]["contacts_de_deviation_employees_1_name"] = array (
  'name' => 'contacts_de_deviation_employees_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_de_deviation_employees_1contacts_ida',
  'link' => 'contacts_de_deviation_employees_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["DE_Deviation_Employees"]["fields"]["contacts_de_deviation_employees_1contacts_ida"] = array (
  'name' => 'contacts_de_deviation_employees_1contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE_ID',
  'id_name' => 'contacts_de_deviation_employees_1contacts_ida',
  'link' => 'contacts_de_deviation_employees_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Vardefs/sugarfield_name.php

 // created: 2019-05-23 18:07:26
$dictionary['DE_Deviation_Employees']['fields']['name']['len']='255';
$dictionary['DE_Deviation_Employees']['fields']['name']['audited']=true;
$dictionary['DE_Deviation_Employees']['fields']['name']['massupdate']=false;
$dictionary['DE_Deviation_Employees']['fields']['name']['unified_search']=false;
$dictionary['DE_Deviation_Employees']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['DE_Deviation_Employees']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Vardefs/sugarfield_deviation_type_c.php

 // created: 2019-05-23 18:08:22
$dictionary['DE_Deviation_Employees']['fields']['deviation_type_c']['labelValue']='Type';
$dictionary['DE_Deviation_Employees']['fields']['deviation_type_c']['dependency']='';
$dictionary['DE_Deviation_Employees']['fields']['deviation_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/DE_Deviation_Employees/Ext/Vardefs/m06_error_de_deviation_employees_2_DE_Deviation_Employees.php

// created: 2022-01-03 10:55:30
$dictionary["DE_Deviation_Employees"]["fields"]["m06_error_de_deviation_employees_2"] = array (
  'name' => 'm06_error_de_deviation_employees_2',
  'type' => 'link',
  'relationship' => 'm06_error_de_deviation_employees_2',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_2_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_de_deviation_employees_2m06_error_ida',
);

?>
