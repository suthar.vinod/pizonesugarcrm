<?php
// created: 2020-01-24 16:45:48
$viewdefs['DE_Deviation_Employees']['base']['view']['subpanel-for-contacts-contacts_de_deviation_employees_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'deviation_type_c',
          'label' => 'LBL_DEVIATION_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'm06_error_de_deviation_employees_1_name',
          'label' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_M06_ERROR_TITLE',
          'enabled' => true,
          'id' => 'M06_ERROR_DE_DEVIATION_EMPLOYEES_1M06_ERROR_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);