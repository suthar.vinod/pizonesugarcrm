<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class create_record_on_save
{
    /**
     * @param $bean
     * @param $event
     * @param $arguments
     */
    function update_record_name_on_contact_change($bean, $event, $arguments){
        // This hook will run on new record creation with a linked contact and on contact change)
        if ($arguments['related_module'] == "Contacts" && !empty($arguments['related_id'])  && !empty($bean->deviation_type_c)) {

            $contact_id = $arguments['related_id'];
            $year = date("y");

            if($bean->m06_error_de_deviation_employees_2m06_error_ida) {
                $error_bean = BeanFactory::getBean( 'M06_Error', $bean->m06_error_de_deviation_employees_2m06_error_ida);
                $date_error_occurred_c = strtotime($error_bean->date_error_occurred_c);
                $year = date("y", $date_error_occurred_c);
            }
              
            // need to get all linked names and find gaps to fillup
            $conn = $GLOBALS['db']->getConnection();
            $sql = "SELECT de_deviation_employees.name AS name FROM de_deviation_employees
                    LEFT JOIN contacts_de_deviation_employees_1_c
                        ON de_deviation_employees.id = contacts_de_deviation_employees_1_c.contacts_de_deviation_employees_1de_deviation_employees_idb
                        AND contacts_de_deviation_employees_1_c.deleted = 0
                    WHERE contacts_de_deviation_employees_1_c.contacts_de_deviation_employees_1contacts_ida = '{$contact_id}'
                        AND de_deviation_employees.deleted = 0 AND de_deviation_employees.name LIKE '%{$year}%' AND de_deviation_employees.id != '{$bean->id}'
                    ORDER BY contacts_de_deviation_employees_1_c.date_modified DESC";
            $rslt = $conn->executeQuery($sql);
            $existingNames = $rslt->fetchAll();
            $Numbers = array();
            foreach ($existingNames as $key => $names) {
                $Numbers[] = (int)explode("-", $names['name'])[1];
            }

            sort($Numbers, SORT_NUMERIC);

            if (!empty($Numbers)) {
                if (in_array(1, $Numbers) === false) {
                    $nextNumber = 1;
                }else{
                    foreach ($Numbers as $key => $number) {
                        if (in_array($number+1, $Numbers) === false) {
                            $nextNumber = $number+1;
                            break;
                        }
                    }
                }
            }else{
                $nextNumber = 1;
            }

            $length = strlen($nextNumber);
            if ($length == 1) {
                $nextNumber = '000' . $nextNumber;
            } else if ($length == 2) {
                $nextNumber = '00' . $nextNumber;
            } else if ($length == 3) {
                $nextNumber = '0' . $nextNumber;
            }
            $code .= 'CE' . $year . '-' . $nextNumber;
            $bean->name = $code;
        }
    }
}

