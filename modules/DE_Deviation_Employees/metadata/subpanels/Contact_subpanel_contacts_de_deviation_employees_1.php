<?php
// created: 2020-01-24 16:45:47
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'deviation_type_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_DEVIATION_TYPE',
    'width' => 10,
  ),
  'm06_error_de_deviation_employees_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M06_ERROR_DE_DEVIATION_EMPLOYEES_1_FROM_M06_ERROR_TITLE',
    'id' => 'M06_ERROR_DE_DEVIATION_EMPLOYEES_1M06_ERROR_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M06_Error',
    'target_record_key' => 'm06_error_de_deviation_employees_1m06_error_ida',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
);