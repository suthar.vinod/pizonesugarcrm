<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ABC12_Work_Product_Activities/Ext/Vardefs/m03_work_product_abc12_work_product_activities_1_ABC12_Work_Product_Activities.php

// created: 2017-02-17 17:50:46
$dictionary["ABC12_Work_Product_Activities"]["fields"]["m03_work_product_abc12_work_product_activities_1"] = array (
  'name' => 'm03_work_product_abc12_work_product_activities_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_abc12_work_product_activities_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_ABC12_WORK_PRODUCT_ACTIVITIES_TITLE',
  'id_name' => 'm03_work_p4898product_ida',
  'link-type' => 'one',
);
$dictionary["ABC12_Work_Product_Activities"]["fields"]["m03_work_product_abc12_work_product_activities_1_name"] = array (
  'name' => 'm03_work_product_abc12_work_product_activities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p4898product_ida',
  'link' => 'm03_work_product_abc12_work_product_activities_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["ABC12_Work_Product_Activities"]["fields"]["m03_work_p4898product_ida"] = array (
  'name' => 'm03_work_p4898product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_ABC12_WORK_PRODUCT_ACTIVITIES_TITLE_ID',
  'id_name' => 'm03_work_p4898product_ida',
  'link' => 'm03_work_product_abc12_work_product_activities_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/ABC12_Work_Product_Activities/Ext/Vardefs/sugarfield_activity_date.php

 // created: 2017-02-17 18:08:50
$dictionary['ABC12_Work_Product_Activities']['fields']['activity_date']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/ABC12_Work_Product_Activities/Ext/Vardefs/sugarfield_activity_time.php

 // created: 2017-02-17 18:09:34
$dictionary['ABC12_Work_Product_Activities']['fields']['activity_time']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/ABC12_Work_Product_Activities/Ext/Vardefs/nameFieldCustomization.php


$dictionary['ABC12_Work_Product_Activities']['fields']['name']['required'] = 'false';
$dictionary['ABC12_Work_Product_Activities']['fields']['name']['readonly'] = 'true';

?>
<?php
// Merged from custom/Extension/modules/ABC12_Work_Product_Activities/Ext/Vardefs/sugarfield_activity_time_quarter_hour_c.php

 // created: 2017-03-10 17:50:25
$dictionary['ABC12_Work_Product_Activities']['fields']['activity_time_quarter_hour_c']['labelValue']='Activity Time (By Quarter Hour)';
$dictionary['ABC12_Work_Product_Activities']['fields']['activity_time_quarter_hour_c']['enforced']='';
$dictionary['ABC12_Work_Product_Activities']['fields']['activity_time_quarter_hour_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ABC12_Work_Product_Activities/Ext/Vardefs/sugarfield_activity.php

 // created: 2017-03-20 21:11:23
$dictionary['ABC12_Work_Product_Activities']['fields']['activity']['required']=true;

 
?>
