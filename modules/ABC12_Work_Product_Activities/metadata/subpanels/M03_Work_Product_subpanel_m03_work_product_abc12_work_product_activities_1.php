<?php
// created: 2017-03-13 16:33:20
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'activity' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ACTIVITY',
    'width' => '10%',
  ),
  'activity_time_quarter_hour_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_ACTIVITY_TIME_QUARTER_HOUR',
    'width' => '10%',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
);