<?php
// created: 2017-03-13 16:33:24
$viewdefs['ABC12_Work_Product_Activities']['base']['view']['subpanel-for-m03_work_product-m03_work_product_abc12_work_product_activities_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'activity',
          'label' => 'LBL_ACTIVITY',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'activity_time_quarter_hour_c',
          'label' => 'LBL_ACTIVITY_TIME_QUARTER_HOUR',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);