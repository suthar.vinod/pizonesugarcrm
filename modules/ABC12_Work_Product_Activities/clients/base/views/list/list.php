<?php
$module_name = 'ABC12_Work_Product_Activities';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'm03_work_product_abc12_work_product_activities_1_name',
                'label' => 'LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_M03_WORK_PRODUCT_TITLE',
                'enabled' => true,
                'id' => 'M03_WORK_P4898PRODUCT_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'activity_time_quarter_hour_c',
                'label' => 'LBL_ACTIVITY_TIME_QUARTER_HOUR',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'activity_time',
                'label' => 'LBL_ACTIVITY_TIME',
                'enabled' => true,
                'default' => false,
              ),
              5 => 
              array (
                'name' => 'workproduct',
                'label' => 'LBL_WORKPRODUCT',
                'enabled' => true,
                'id' => 'M03_WORK_PRODUCT_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              6 => 
              array (
                'name' => 'activity',
                'label' => 'LBL_ACTIVITY',
                'enabled' => true,
                'default' => false,
              ),
              7 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
