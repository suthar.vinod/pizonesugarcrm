<?php
// WARNING: The contents of this file are auto-generated.


$viewdefs['CO_Clinical_Observation']['base']['filter']['basic']['filters'][] = array(
    'id' => 'filterCObss',
    'name' => 'LBL_FILTER_CO_TEMPLATE',
    'filter_definition' => array(

        array(
            'anml_animals_co_clinical_observation_1anml_animals_ida'=> array(
              '$in'=>array(),
            ),
            'cie_clinicb9a6ue_exam_ida'=> array(
              '$in'=>array(),
            ),
            'vet_check_submitted'=> array(
              '$in'=>array(),
            ),

        )
    ),
    'editable' => true,
    'is_template' => true,
);
