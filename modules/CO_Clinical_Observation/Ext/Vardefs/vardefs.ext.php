<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/cie_clinical_issue_exam_co_clinical_observation_1_CO_Clinical_Observation.php

// created: 2020-01-07 13:43:54
$dictionary["CO_Clinical_Observation"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_1"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'source' => 'non-db',
  'module' => 'CIE_Clinical_Issue_Exam',
  'bean_name' => 'CIE_Clinical_Issue_Exam',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'id_name' => 'cie_clinicb9a6ue_exam_ida',
);
$dictionary["CO_Clinical_Observation"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_1_name"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'save' => true,
  'id_name' => 'cie_clinicb9a6ue_exam_ida',
  'link' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'name',
);
$dictionary["CO_Clinical_Observation"]["fields"]["cie_clinicb9a6ue_exam_ida"] = array (
  'name' => 'cie_clinicb9a6ue_exam_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE_ID',
  'id_name' => 'cie_clinicb9a6ue_exam_ida',
  'link' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/cie_clinical_issue_exam_co_clinical_observation_2_CO_Clinical_Observation.php

// created: 2020-01-07 13:50:42
$dictionary["CO_Clinical_Observation"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_2"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  'source' => 'non-db',
  'module' => 'CIE_Clinical_Issue_Exam',
  'bean_name' => 'CIE_Clinical_Issue_Exam',
  'side' => 'right',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_2_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'id_name' => 'cie_clinicddcbue_exam_ida',
  'link-type' => 'one',
);
$dictionary["CO_Clinical_Observation"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_2_name"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_2_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'save' => true,
  'id_name' => 'cie_clinicddcbue_exam_ida',
  'link' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'name',
);
$dictionary["CO_Clinical_Observation"]["fields"]["cie_clinicddcbue_exam_ida"] = array (
  'name' => 'cie_clinicddcbue_exam_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_2_FROM_CO_CLINICAL_OBSERVATION_TITLE_ID',
  'id_name' => 'cie_clinicddcbue_exam_ida',
  'link' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/anml_animals_co_clinical_observation_1_CO_Clinical_Observation.php

// created: 2020-01-07 14:17:22
$dictionary["CO_Clinical_Observation"]["fields"]["anml_animals_co_clinical_observation_1"] = array (
  'name' => 'anml_animals_co_clinical_observation_1',
  'type' => 'link',
  'relationship' => 'anml_animals_co_clinical_observation_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'id_name' => 'anml_animals_co_clinical_observation_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["CO_Clinical_Observation"]["fields"]["anml_animals_co_clinical_observation_1_name"] = array (
  'name' => 'anml_animals_co_clinical_observation_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_co_clinical_observation_1anml_animals_ida',
  'link' => 'anml_animals_co_clinical_observation_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["CO_Clinical_Observation"]["fields"]["anml_animals_co_clinical_observation_1anml_animals_ida"] = array (
  'name' => 'anml_animals_co_clinical_observation_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE_ID',
  'id_name' => 'anml_animals_co_clinical_observation_1anml_animals_ida',
  'link' => 'anml_animals_co_clinical_observation_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/co_clinical_observation_m06_error_1_CO_Clinical_Observation.php

// created: 2020-01-07 20:01:08
$dictionary["CO_Clinical_Observation"]["fields"]["co_clinical_observation_m06_error_1"] = array (
  'name' => 'co_clinical_observation_m06_error_1',
  'type' => 'link',
  'relationship' => 'co_clinical_observation_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'co_clinical_observation_m06_error_1m06_error_idb',
);
$dictionary["CO_Clinical_Observation"]["fields"]["co_clinical_observation_m06_error_1_name"] = array (
  'name' => 'co_clinical_observation_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'save' => true,
  'id_name' => 'co_clinical_observation_m06_error_1m06_error_idb',
  'link' => 'co_clinical_observation_m06_error_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'name',
);
$dictionary["CO_Clinical_Observation"]["fields"]["co_clinical_observation_m06_error_1m06_error_idb"] = array (
  'name' => 'co_clinical_observation_m06_error_1m06_error_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_M06_ERROR_TITLE_ID',
  'id_name' => 'co_clinical_observation_m06_error_1m06_error_idb',
  'link' => 'co_clinical_observation_m06_error_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/sugarfield_test_system_name_c.php

 // created: 2020-01-07 20:06:12
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['duplicate_merge_dom_value']=0;
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['labelValue']='Test System Name';
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['calculated']='true';
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['formula']='related($anml_animals_co_clinical_observation_1,"name")';
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['enforced']='true';
$dictionary['CO_Clinical_Observation']['fields']['test_system_name_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/sugarfield_testsystem_relate.php

$dictionary['CO_Clinical_Observation']['fields']['anml_animals_co_clinical_observation_1_name']['required'] = true;
?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/sugarfield_temperature_data.php

 // created: 2020-01-15 05:14:37
$dictionary['CO_Clinical_Observation']['fields']['temperature_data']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/sugarfield_pulse_data.php

 // created: 2020-01-15 05:15:02
$dictionary['CO_Clinical_Observation']['fields']['pulse_data']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/sugarfield_respiration_rate_data.php

 // created: 2020-01-15 05:15:25
$dictionary['CO_Clinical_Observation']['fields']['respiration_rate_data']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/co_clinical_observation_m06_error_1_name.php


$dictionary['CO_Clinical_Observation']['fields']['co_clinical_observation_m06_error_1_name']['readonly'] = true;


?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/sugarfield_appetite.php

 // created: 2020-01-27 09:40:47
$dictionary['CO_Clinical_Observation']['fields']['appetite']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/sugarfield_feces.php

 // created: 2020-01-27 09:41:40
$dictionary['CO_Clinical_Observation']['fields']['feces']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/sugarfield_mentation.php

 // created: 2020-01-27 09:42:02
$dictionary['CO_Clinical_Observation']['fields']['mentation']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Vardefs/sugarfield_name.php

 // created: 2020-03-17 11:33:49
$dictionary['CO_Clinical_Observation']['fields']['name']['required']=false;
$dictionary['CO_Clinical_Observation']['fields']['name']['unified_search']=false;
$dictionary['CO_Clinical_Observation']['fields']['name']['importable']='false';
$dictionary['CO_Clinical_Observation']['fields']['name']['duplicate_merge']='disabled';
$dictionary['CO_Clinical_Observation']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['CO_Clinical_Observation']['fields']['name']['merge_filter']='disabled';
$dictionary['CO_Clinical_Observation']['fields']['name']['calculated']='true';
$dictionary['CO_Clinical_Observation']['fields']['name']['formula']='concat($test_system_name_c," ",toString($datetime)," Clinical Observation")';
$dictionary['CO_Clinical_Observation']['fields']['name']['enforced']=true;

 
?>
