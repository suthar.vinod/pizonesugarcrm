<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Dependencies/rt_type_obs_co_dep.php


$dependencies['CO_Clinical_Observation']['setoptions_type_obs_update1'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2, createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc")),equal($observation,"Normal"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => 'true',
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
    ),
);

$dependencies['CO_Clinical_Observation']['setoptions_type_obs_update_null1'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'equal($type_2,"")',
    'triggerFields' => array('type_2'),
    'onload' => 'true',
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
    ),
);


?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Dependencies/sugardependency_setoptions_CO.php


$dependencies['CO_Clinical_Observation']['setoptions_type_observation1'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2, createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc")),equal($observation,"See Observation Text"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => 'true',
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_taken_list")',
                'labels' => 'getDropdownValueSet("rt_taken_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
    ),
);

$dependencies['CO_Clinical_Observation']['setoptions_morbidity1'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'equal($type_2,"Morbidity and Mortality")',
    'triggerFields' => array('type_2','observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
    ),


);

$dependencies['CO_Clinical_Observation']['setoptions_studio_type1'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'and(isInList($type_2,createList("Ad Hoc","Post Operative AM","Post Operative PM","Standard")),equal($observation,""))',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_taken_list")',
                'labels' => 'getDropdownValueSet("rt_taken_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_taken_both_list")',
                'labels' => 'getDropdownValueSet("rt_taken_both_list")'
            ),
        ),
    ),
);

$dependencies['CO_Clinical_Observation']['setoptions_pain_assessment1'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'equal($type_2,"Pain Assessment")',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_not_taken_list")',
                'labels' => 'getDropdownValueSet("rt_not_taken_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_not_taken_list")',
                'labels' => 'getDropdownValueSet("rt_not_taken_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_not_taken_list")',
                'labels' => 'getDropdownValueSet("rt_not_taken_list")'
            ),
        ),
    ),
);

$dependencies['CO_Clinical_Observation']['setoptions_post_day_11'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'equal($type_2,"Post Operative Day 1 AM")',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'temperature_data',
                'keys' => 'getDropdownKeySet("rt_taken_list")',
                'labels' => 'getDropdownValueSet("rt_taken_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'pulse_data',
                'keys' => 'getDropdownKeySet("rt_taken_list")',
                'labels' => 'getDropdownValueSet("rt_taken_list")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiration_rate_data',
                'keys' => 'getDropdownKeySet("rt_taken_list")',
                'labels' => 'getDropdownValueSet("rt_taken_list")'
            ),
        ),
    ),
);



?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Dependencies/hideActiveClinicalIssue.php

$dependencies['CO_Clinical_Observation']['CO_observation_readonly_field'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'true',
    //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array('active_clinical_issue','observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                
                'target' => 'cie_clinical_issue_exam_co_clinical_observation_2_name',
                'value' => 'equal($active_clinical_issue,"Yes")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                
                'target' => 'cie_clinical_issue_exam_co_clinical_observation_2_name',
                'value' => 'equal($active_clinical_issue,"Yes")',
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Dependencies/appetite_dependency.php


$dependencies['CO_Clinical_Observation']['appetite_dep'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2,createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc","Morbidity and Mortality")),equal($observation,"See Observation Text"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'appetite',
                'keys' => 'getDropdownKeySet("appetite_dd_list")',
                'labels' => 'getDropdownValueSet("appetite_dd_list")'
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['appetite_dep4'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($type_2,createList("Post Operative Day 1 AM"))',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'appetite',
                'keys' => 'getDropdownKeySet("appetite_dd_list")',
                'labels' => 'getDropdownValueSet("appetite_dd_list")'
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['appetite_dep3'] = array(
    'hooks' => array("edit", "save"),
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'appetite',
                'value' => 'not(equal($type_2,"Pain Assessment"))',
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['appetite_dep2'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2,createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc","Morbidity and Mortality")),equal($observation,"Normal"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'appetite',
                'keys' => 'getDropdownKeySet("app_feces_normal_list")',
                'labels' => 'getDropdownValueSet("app_feces_normal_list")'
            ),
        ),
    ),
);


?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Dependencies/feces_dependency.php


$dependencies['CO_Clinical_Observation']['feces_dep'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2,createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc","Morbidity and Mortality")),equal($observation,"See Observation Text"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'feces',
                'keys' => 'getDropdownKeySet("feces_dd_list")',
                'labels' => 'getDropdownValueSet("feces_dd_list")'
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['feces_dep4'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($type_2,createList("Post Operative Day 1 AM"))',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'feces',
                'keys' => 'getDropdownKeySet("feces_dd_list")',
                'labels' => 'getDropdownValueSet("feces_dd_list")'
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['feces_dep3'] = array(
    'hooks' => array("edit", "save"),
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'feces',
                'value' => 'not(equal($type_2,"Pain Assessment"))',
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['feces_dep2'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2,createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc","Morbidity and Mortality")),equal($observation,"Normal"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'feces',
                'keys' => 'getDropdownKeySet("app_feces_normal_list")',
                'labels' => 'getDropdownValueSet("app_feces_normal_list")'
            ),
        ),
    ),
);


?>
<?php
// Merged from custom/Extension/modules/CO_Clinical_Observation/Ext/Dependencies/mentation_dependency.php


$dependencies['CO_Clinical_Observation']['mentation_dep'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2,createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc","Morbidity and Mortality")),equal($observation,"See Observation Text"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'mentation',
                'keys' => 'getDropdownKeySet("mentation_dd_list")',
                'labels' => 'getDropdownValueSet("mentation_dd_list")'
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['mentation_dep4'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($type_2,createList("Post Operative Day 1 AM"))',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'mentation',
                'keys' => 'getDropdownKeySet("mentation_dd_list")',
                'labels' => 'getDropdownValueSet("mentation_dd_list")'
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['mentation_dep3'] = array(
    'hooks' => array("edit", "save"),
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'mentation',
                'value' => 'not(equal($type_2,"Pain Assessment"))',
            ),
        ),
    ),
);
$dependencies['CO_Clinical_Observation']['mentation_dep2'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($type_2,createList("Standard","Post Operative AM","Post Operative PM","Ad Hoc","Morbidity and Mortality")),equal($observation,"Normal"))',
    'triggerFields' => array('type_2', 'observation'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'mentation',
                'keys' => 'getDropdownKeySet("mentation_bar_list")',
                'labels' => 'getDropdownValueSet("mentation_bar_list")'
            ),
        ),
    ),
);


?>
