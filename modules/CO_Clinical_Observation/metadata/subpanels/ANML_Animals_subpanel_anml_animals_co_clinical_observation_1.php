<?php
// created: 2020-02-07 13:51:32
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'type_2' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TYPE_2',
    'width' => 10,
  ),
  'co_clinical_observation_m06_error_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'readonly' => true,
    'vname' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_M06_ERROR_TITLE',
    'id' => 'CO_CLINICAL_OBSERVATION_M06_ERROR_1M06_ERROR_IDB',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M06_Error',
    'target_record_key' => 'co_clinical_observation_m06_error_1m06_error_idb',
  ),
  'vet_check_submitted' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_VET_CHECK_SUBMITTED',
    'width' => 10,
  ),
  'observation' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_OBSERVATION',
    'width' => 10,
  ),
  'temperature' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_TEMPERATURE',
    'width' => 10,
  ),
  'pulse' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_PULSE',
    'width' => 10,
  ),
  'respiration_rate' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_RESPIRATION_RATE',
    'width' => 10,
  ),
  'appetite' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_APPETITE',
    'width' => 10,
  ),
  'feces' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_FECES',
    'width' => 10,
  ),
  'mentation' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_MENTATION',
    'width' => 10,
  ),
);