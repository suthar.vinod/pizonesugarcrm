<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('modules/Emails/Email.php');
require_once('modules/EmailTemplates/EmailTemplate.php');
require_once('custom/include/Helpers/sendEmailTemplates.php');
require_once('custom/modules/M06_Error/email_sending_template.php');


class customCOLogicHook {

    function createCommunicationRecord($bean, $event, $arguments) {

        global $db;
        global $current_user;
        $user_email = array(); //Related Team users and submitter emails
        $wp_email = array(); //Work product assigned user emails
        $template_data = array(); //set all the data in this array for email sending
        $email_sending_template = new email_sending_template();
        $sendEmailTemplates = new sendEmailTemplates();
        $wp_id = null; // It will store the work product id so that it can pass in template data

        $template_data['entryEmails'] = '';
        $template_data['reviewEmails'] = '';

        //If it is a new record
        if (empty($arguments['isUpdate']) && empty($bean->fetched_row['id'])) {

            //If Vet Check Submitted = Yes then create a Communication record
            if ($bean->vet_check_submitted == 'Yes') {
               
                //Create a bean of Communications/Observations
                $beanObser = BeanFactory::newBean('M06_Error');
                $email_address = $this->getRecordEmail("bean_id = '{$current_user->id}'");

                if (!empty($email_address)) {

                    foreach ($email_address as $email) {
                        if ($this->verifyEmailAddForNotification($email)) {

                            //Set the current user email address in the submitter field of Communication
                            //record if email address belongs to valid domain
                            $beanObser->submitter_c = $email;
                            array_push($user_email, $email);
                            $template_data['email'] = $email; //set submitter email address in template
                        }
                    }
                } else {
                    $GLOBALS['log']->debug('No email found against work product user.');
                }

                $beanObser->error_category_c = "Vet Check"; //Set the Vet Check as a Category
                $beanObser->date_error_occurred_c = date('Y-m-d', strtotime($bean->datetime)); // Set the Date Occured = Date on the Clinical Onservation
                $beanObser->actual_event_c = $bean->observation_text; //Set the Actual Event = Observation Text field value
                $beanObser->team_set_id = 'bc97fdae-7af8-11e9-b473-029395602a62'; //Set team_set_id for teams Global and Clinical Vet
              //  $GLOBALS['log']->fatal("before save call");
                $beanObser->save(); //save the Communication bean
              //  $GLOBALS['log']->fatal("after save call id is ". $beanObser->id);
              //  $GLOBALS['log']->fatal("after save call name is ". $beanObser->name);
               
                //Load Test System
                $beanAnimals = BeanFactory::getBean('ANML_Animals', $bean->anml_animals_co_clinical_observation_1anml_animals_ida);

                //If Test System relationship with Communication is loaded 
                if ($beanObser->load_relationship('m06_error_anml_animals_1')) {

                    // Add the relationship with Communication/Observations
                    $beanObser->m06_error_anml_animals_1->add($bean->anml_animals_co_clinical_observation_1anml_animals_ida);
                }

                //If the "Clinical Issues & Examms" relationship with Communication is loaded
                if ($beanObser->load_relationship('cie_clinical_issue_exam_m06_error_1')) {

                    // Add the relationship with Communication/Observations
                    $beanObser->cie_clinical_issue_exam_m06_error_1->add($bean->cie_clinicb9a6ue_exam_ida);
                }

                //If relationship Test System relationship with Work Product Enrollment is loaded
                if ($beanAnimals->load_relationship('anml_animals_wpe_work_product_enrollment_1')) {

                    //Fetch related Work Product Enrollment(s) IDs
                    $relatedWPE = $beanAnimals->anml_animals_wpe_work_product_enrollment_1->get();

                    //Coverting in the comma seperated form so that it can directly used in sql query
                    $relatedWPE_array = implode("', '", $relatedWPE);

                    //Get the id of Work Product Enrollment which was recently created.
                    $wpe_id_query = "SELECT id FROM `wpe_work_product_enrollment` WHERE id IN ('{$relatedWPE_array}') ORDER BY date_entered DESC";
                    $result_id = $db->query($wpe_id_query);
                    if ($row = $db->fetchByAssoc($result_id)) {
                        $id_desc = $row['id']; //Most recent Work Product Enrollment ID

                        //Create the Work Product Enrollment bean based on the ID.
                        $beanWPE = BeanFactory::getBean('WPE_Work_Product_Enrollment', $id_desc);

                        //If relationship is loaded
                        if ($beanWPE->load_relationship('m03_work_product_wpe_work_product_enrollment_1')) {

                            //Fetch related Work Product IDs
                            $relatedWP = $beanWPE->m03_work_product_wpe_work_product_enrollment_1->get();

                            //Add the Work Product record under the subpanel of Communication/Observation
                            $beanObser->load_relationship('m06_error_m03_work_product_1');
                            $beanObser->m06_error_m03_work_product_1->add($relatedWP[0]);
                            $wp_id = $relatedWP[0];
                        }
                    }
                }

                //if the Clinical Observation relationship with Communication is loaded
                if ($beanObser->load_relationship('co_clinical_observation_m06_error_1')) {

                    // Add the relationship with Communication/Observations
                    $beanObser->co_clinical_observation_m06_error_1->add($bean->id);
                }

                //Send Email to "Clinical Vet" team members and contacts

                $team_name = 'Clinical Vet';

                //get the email addresses of team members
                $team_email_addresses = $email_sending_template->get_related_teams($team_name);
                if (!empty($team_email_addresses)) {
                    foreach ($team_email_addresses as $email) {
                        if ($this->verifyEmailAddForNotification($email))
                            array_push($user_email, $email);
                    }
                } else {
                    $GLOBALS['log']->debug('No email found for Vet Check Team.');
                }

                //get the email addresses of contacts associated with teams
                $con_email_addresses = $email_sending_template->get_related_contacts_email_addresses($team_name);
                if (!empty($con_email_addresses)) {
                    foreach ($con_email_addresses as $email) {
                        if ($this->verifyEmailAddForNotification($email)) {
                            array_push($user_email, $email);
                        }
                    }
                }


                //Preparing list of selected work product , their names and related study directors
                $wproducts = array();
                $study_director = array();
                $product = BeanFactory::getBean('M03_Work_Product', $wp_id);
                $wproducts[$wp_id] = array(
                    "name" => $product->name,
                    "account_name" => $product->accounts_m03_work_product_1_name,
                    "account_id" => $product->accounts_m03_work_product_1accounts_ida,
                    "wp_compliance" => $product->work_product_compliance_c
                );
                $study_director[$product->contact_id_c] = array("name" => $product->study_director_script_c);


                $strQuery4 = "
                               SELECT
                                   users.id
                               FROM
                                   `users`
                               INNER JOIN
                                   `m03_work_product` ON m03_work_product.assigned_user_id = users.id
                               WHERE users.deleted = 0 AND m03_work_product.id IN ('{$wp_id}')";
                $res4 = $GLOBALS['db']->query($strQuery4);
                $wproduct_users = array();
                while ($user = $GLOBALS["db"]->fetchByAssoc($res4)) {
                    $wproduct_users[] = $user['id'];
                }
                if (!empty($wproduct_users)) {
                    $user_id = implode("', '", $wproduct_users);
                    $emails = $this->getRecordEmail("bean_id IN ('{$user_id}')");
                    if (!empty($emails)) {
                        foreach ($emails as $email) {
                            if ($this->verifyEmailAddForNotification($email))
                                array_push($wp_email, $email);
                        }
                    } else {
                        $GLOBALS['log']->debug('No email found against work product user.');
                    }
                }

                //Prepare the related data:
                $related_data = array();
                $related_data[Erd_Error_Documents] = null;
                $related_data[ANML_Animals] = $bean->anml_animals_co_clinical_observation_1anml_animals_ida;
                $related_data[Equip_Equipment] = null;
                $related_data[RMS_Room] = null;
                $related_data[A1A_Critical_Phase_Inspectio] = null;
                $related_data[Teams] = "f019638e-5b95-11e9-9bd1-06036fae356c";

                $template_data['site_url'] = $GLOBALS['sugar_config']['site_url'];
                $template_data['id'] = $beanObser->id;
                $template_data['Title'] = $wproducts;
                $template_data['error_name'] = $beanObser->name;
                $template_data['error_classification'] = $beanObser->error_classification_c;
                $template_data['expected_event'] = $beanObser->expected_event_c;
                $template_data['actual_event'] = $beanObser->actual_event_c;
                $template_data['error_type'] = '';
                $template_data['error_category'] = 'Vet Check';
                $template_data['date_error_occurred'] = $beanObser->date_error_occurred_c;
                $template_data['date_error_discovered'] = '';
                $template_data['date_time_error_discovered'] = '';
                $template_data['manager_departments'] = '';
                $template_data['study_directors'] = $study_director;
                $template_data['related_data'] = $email_sending_template->prepareRelatedRecordData($related_data);

                $user_email = array_unique($user_email);

                $sendEmailTemplates->sendemail($template_data, $user_email, $wp_email);

            }
        }
    }

    /**
     * Retrieves email address form bean id from email related bean table
     * @param string $record
     * @return string email_address|null
     */
    function getRecordEmail($where = '') {
        $email_addresses = array();
        $query = "
        SELECT
            email_addresses.email_address
        FROM
            `email_addresses`
        INNER JOIN
            `email_addr_bean_rel` ON email_addr_bean_rel.email_address_id = email_addresses.id
        WHERE email_addr_bean_rel.deleted = 0 AND email_addresses.deleted='0' AND email_addresses.invalid_email='0' AND email_addresses.opt_out='0'
        AND email_addr_bean_rel." . $where;
        $res = $GLOBALS['db']->query($query);
        if ($res->num_rows >= 1) {
            while ($row = $GLOBALS["db"]->fetchByAssoc($res)) {
                $email_address[] = $row['email_address'];
            }
            return $email_address;
        }

        return '';
    }

    /**
     * It will use just for the submitter email address or Team members because user normally didn't belong to any account
     * Verify if email address domain is 'apsemail.com' otherwise return false
     * @param string $email
     * @return bool true|false
     */
    function verifyEmailAddForNotification($email) {
        $domainToSearch = 'apsemail.com';
        $domain_name = substr(strrchr($email, "@"), 1);
        if (trim(strtolower($domain_name) == $domainToSearch)) {
            return true;
        }
        return false;
    }

}


