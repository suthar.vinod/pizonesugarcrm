<?php
// created: 2019-12-31 16:48:46
$viewdefs['CO_Clinical_Observation']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'anml_animals_co_clinical_observation_1_name' => 
    array (
    ),
    'vet_check_submitted' => 
    array (
    ),
    'cie_clinical_issue_exam_co_clinical_observation_1_name' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);