({
   extendsFrom: 'RelateField',

    /**
     * @inheritdoc
     */
    initialize: function(options) {        
        this._super('initialize', [options]);
        this.type = 'relate';
    },


    /**
     * function: _getCustomFilter
     *
     * return: custom filter def
     */


    _getCustomFilter: function () {
        return [{
            '$and': [
                {'anml_animals_cie_clinical_issue_exam_1anml_animals_ida': this.model.get('anml_animals_co_clinical_observation_1anml_animals_ida')},
                {'resolution_date': {'$empty': ''}}
            ]
        }];
    },

    /**
     * @override
     */
    buildFilterDefinition: function (searchTerm) {
        var parentFilter = this._super('buildFilterDefinition', [searchTerm]);
        if(this.getSearchModule() == 'CIE_Clinical_Issue_Exam') {
            return parentFilter.concat(this._getCustomFilter());
        } else {
            return parentFilter;
        }
    },

    /**
     * @override
     */
    openSelectDrawer: function () {
         if(this.getSearchModule() != 'CIE_Clinical_Issue_Exam') {
            this._super('openSelectDrawer');
        }

        var layout = 'selection-list';

     var filterOptions = new app.utils.FilterOptions()
    .config({
        'initial_filter': 'filterByTestSystem',
        'initial_filter_label': 'LBL_TEST_FILTER_TEMPLATE',
        'filter_populate': {
            'anml_animals_cie_clinical_issue_exam_1anml_animals_ida': [this.model.get('anml_animals_co_clinical_observation_1anml_animals_ida')],
           'resolution_date' : ''
        }
    })
    .format();

        var context = {
            module: this.getSearchModule(),
            fields: this.getSearchFields(),
            filterOptions: filterOptions
        };
        if (!_.isUndefined(layout) && !_.isUndefined(context)) {
            app.drawer.open({
                layout: layout,
                context: context
            }, _.bind(this.setValue, this));
        }
    }
})
