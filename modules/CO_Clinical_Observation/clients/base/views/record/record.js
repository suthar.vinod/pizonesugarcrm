/**
 * @class View.Views.Base.RecordView
 * @alias SUGAR.App.view.views.RecordView
 * @extends View.Views.Base.RecordView
 */
({
	extendsFrom: 'RecordView',

	/**
	 * Initialize the view and prepare the model with default button metadata
	 * for the current layout.
	 */
	initialize: function (options) {
		this._super('initialize', [options]);
		this.model.on("change:observation_selection", this.setGaitOptions, this);
		this.model.on("change:type_2", this.setGaitOptions, this);
		this.model.on("change:observation", this._setValue, this);
		this.model.addValidationTask('check_observation_text', _.bind(this._doValidateObservation, this));

	},

    editClicked: function() {
        this.setButtonStates(this.STATE.EDIT);
        this.action = 'edit';
        this.toggleEdit(true);
        this.setRoute('edit');
        var ObsField = this.getField('observation');
        var ObsTextField = this.getField('observation_text');
        var value = this.model.get('observation');
        if(value == 'Normal')
         {
          if(ObsTextField){
              this.model.set('observation_text', 'None');
              ObsTextField.setDisabled(true);
              ObsTextField.render();
            }
         }
     },
     _doValidateObservation: function(fields, errors, callback) {
         //validate type requirements
         if (this.model.get('observation') == 'See Observation Text' && !this.model.get('observation_text'))
         {
             errors['observation_text'] = errors['observation_text'] || {};
             errors['observation_text'].required = true;
         }

         callback(null, fields, errors);
     },
     _setValue:function(){

        var ObsTextField = this.getField('observation_text');
        var ObsField = this.getField('observation');
        if(ObsField)
        {
          var value = this.model.get('observation');
          var ObsValue = this.model.get('observation_text');

           if (value == 'Normal' && ObsTextField) {
                 this.model.set('observation_text', 'None');
                 ObsTextField.setDisabled(true);
                 ObsTextField.render();
         } else if(ObsTextField  && value == 'See Observation Text')
          {
             if(ObsValue == 'None')
             {
                 this.model.set('observation_text', '');
             }
             ObsTextField.setDisabled(false);
             ObsTextField.render();

          }
        }
     },

    /**
     * function to set custom dropdown list to gait dropdown on basis of type_2 and observation_selection field value
     */
	setGaitOptions: function () {
		var type = this.model.get('type_2');
		var selectionList = this.model.get('observation_selection');
		var gaitField = this.getField('gait');
		var reqTypes = ["Standard", "Post Operative Day 1 AM", "Post Operative Day AM", "Post Operative PM", "Ad Hoc", "Morbidity and Mortality"];
		var options = {};
		if (type == ""||type=="Pain Assessment")
		{
			this._hideGait();
		}
		if (!selectionList.includes("Lameness") && reqTypes.includes(type)) {
			options = _.extend(options, app.lang.getAppListStrings("gait_normal_list"));
			this.model.set("gait", "Normal");
			this._showGait();
		} else if (selectionList.includes("Lameness") && reqTypes.includes(type)) {
			options = _.extend(options, app.lang.getAppListStrings("gait_abnormal_list"));
			this.model.set("gait", "Abnormal");
			this._showGait();
		}
		if (gaitField)
		{

			gaitField.items = options;
			gaitField.render();
		}

	},
	/**
     * function to set visibility hidden to gait dropdown field
     */
	_hideGait: function () {
		_.each(this.fields, function (field) {
			if (_.isEqual(field.def.name, 'gait')) {
				this.$('[data-name=gait]').css('visibility', 'hidden');
			}
		}, this);
	},
    /**
     * function to set visibility visible to gait dropdown field
     */
	_showGait: function () {
		_.each(this.fields, function (field) {
			if (_.isEqual(field.def.name, 'gait')) {
				this.$('[data-name=gait]').css('visibility', 'visible');
			}
		}, this);
	},
	_renderHtml: function () {
		this.noEditFields.push('active_clinical_issue');
		this._super('_renderHtml');
	},
	_dispose: function () {
		this._super('_dispose');
	},
})
