<?php
$module_name = 'CO_Clinical_Observation';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'CO_Clinical_Observation',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'active_clinical_issue',
                'label' => 'LBL_ACTIVE_CLINICAL_ISSUE',
                'span' => 6,
              ),
              1 => 
              array (
                'name' => 'cie_clinical_issue_exam_co_clinical_observation_2_name',
                'span' => 6,
              ),
              2 => 
              array (
                'name' => 'datetime',
                'label' => 'LBL_DATETIME',
              ),
              3 => 
              array (
                'name' => 'test_system_id_verified',
                'label' => 'LBL_TEST_SYSTEM_ID_VERIFIED',
              ),
              4 => 
              array (
                'name' => 'housing',
                'label' => 'LBL_HOUSING',
              ),
              5 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
              ),
              6 => 
              array (
                'name' => 'observation',
                'label' => 'LBL_OBSERVATION',
              ),
              7 => 
              array (
                'name' => 'observation_selection',
                'label' => 'LBL_OBSERVATION_SELECTION',
              ),
              8 => 
              array (
                'name' => 'observation_text',
                'studio' => 'visible',
                'label' => 'LBL_OBSERVATION_TEXT',
                'span' => 12,
              ),
              9 => 
              array (
                'name' => 'post_operative_pain_level',
                'label' => 'LBL_POST_OPERATIVE_PAIN_LEVEL',
              ),
              10 => 
              array (
                'name' => 'pain_level_observation',
                'label' => 'LBL_PAIN_LEVEL_OBSERVATION',
              ),
              11 => 
              array (
                'name' => 'temperature_data',
                'label' => 'LBL_TEMPERATURE_DATA',
              ),
              12 => 
              array (
                'name' => 'temperature',
                'label' => 'LBL_TEMPERATURE',
              ),
              13 => 
              array (
                'name' => 'pulse_data',
                'label' => 'LBL_PULSE_DATA',
              ),
              14 => 
              array (
                'name' => 'pulse',
                'label' => 'LBL_PULSE',
              ),
              15 => 
              array (
                'name' => 'respiration_rate_data',
                'label' => 'LBL_RESPIRATION_RATE_DATA',
              ),
              16 => 
              array (
                'name' => 'respiration_rate',
                'label' => 'LBL_RESPIRATION_RATE',
              ),
              17 => 
              array (
                'name' => 'appetite',
                'label' => 'LBL_APPETITE',
              ),
              18 => 
              array (
                'name' => 'feces',
                'label' => 'LBL_FECES',
              ),
              19 => 
              array (
                'name' => 'mentation',
                'label' => 'LBL_MENTATION',
              ),
              20 => 
              array (
                'name' => 'gait',
                'label' => 'LBL_GAIT',
              ),
              21 => 
              array (
                'name' => 'lameness_location',
                'label' => 'LBL_LAMENESS_LOCATION',
              ),
              22 => 
              array (
                'name' => 'lameness_severity',
                'label' => 'LBL_LAMENESS_SEVERITY',
              ),
              23 => 
              array (
                'name' => 'vet_check_submitted',
                'label' => 'LBL_VET_CHECK_SUBMITTED',
                'span' => 12,
              ),
              24 => 
              array (
                'name' => 'anml_animals_co_clinical_observation_1_name',
              ),
              25 => 
              array (
                'name' => 'co_clinical_observation_m06_error_1_name',
              ),
              26 => 
              array (
                'name' => 'cie_clinical_issue_exam_co_clinical_observation_1_name',
              ),
              27 => 
              array (
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
