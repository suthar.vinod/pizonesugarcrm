<?php
// created: 2020-02-07 13:51:32
$viewdefs['CO_Clinical_Observation']['base']['view']['subpanel-for-anml_animals-anml_animals_co_clinical_observation_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'type_2',
          'label' => 'LBL_TYPE_2',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'co_clinical_observation_m06_error_1_name',
          'label' => 'LBL_CO_CLINICAL_OBSERVATION_M06_ERROR_1_FROM_M06_ERROR_TITLE',
          'enabled' => true,
          'readonly' => true,
          'id' => 'CO_CLINICAL_OBSERVATION_M06_ERROR_1M06_ERROR_IDB',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'vet_check_submitted',
          'label' => 'LBL_VET_CHECK_SUBMITTED',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'observation',
          'label' => 'LBL_OBSERVATION',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'temperature',
          'label' => 'LBL_TEMPERATURE',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'pulse',
          'label' => 'LBL_PULSE',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'respiration_rate',
          'label' => 'LBL_RESPIRATION_RATE',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'appetite',
          'label' => 'LBL_APPETITE',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'feces',
          'label' => 'LBL_FECES',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'mentation',
          'label' => 'LBL_MENTATION',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);