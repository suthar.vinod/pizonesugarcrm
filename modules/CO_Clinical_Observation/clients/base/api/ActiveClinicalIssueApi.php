<?php
/**
* @class FilterObservationsApi
* @extends SugarApi
*/
class ActiveClinicalIssueApi extends SugarApi
{
    /**
    * registerApiRest default method to register custom endpoints
    * @return registered api array object
    */
    public function registerApiRest()
    {
        return array(
            'ActiveClinicalIssue' => array(
                'reqType' => 'GET',
                'path' => array('CO_Clinical_Observation','?', 'ActiveClinicalIssue'),
                'pathVars' => array('module','id',''),
                'method' => 'ActiveClinicalIssueMethod',
                'shortHelp' => 'GET related CIES to TEST System for Active Clinical Issue?',
            ),
        );
    }
    /**
    * GET related CIES to TEST System for Active Clinical Issue?
    * @param $api contains api basic info
    * @param $args contains arguments passed when this api called
    * @return $record_found check CIE record with resolution_date = null found or not
    */
    public function ActiveClinicalIssueMethod($api, $args)
    {
        $this->requireArgs($args,array('id'));
        $CIEQuery = "SELECT resolution_date FROM cie_clinical_issue_exam  CIE INNER JOIN "; 
        $CIEQuery .= " anml_animals_cie_clinical_issue_exam_1_c  ANMLCIE  ON CIE.id = ";
        $CIEQuery .= " ANMLCIE.anml_anima4710ue_exam_idb WHERE resolution_date IS NULL AND anml_animals_cie_clinical_issue_exam_1anml_animals_ida  =".$GLOBALS['db']->quoted($args['id'])." AND CIE.deleted=0 ;";
        $results = $GLOBALS['db']->query($CIEQuery);
        $recordFound = false;
        if($GLOBALS['db']->fetchByAssoc($results))
        { 
            $recordFound = true; 
        }
        else
        {
            $recordFound = false;
        }
        return $recordFound;
    }
}
