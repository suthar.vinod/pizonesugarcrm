<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TD_Tradeshow_Documents/Ext/Vardefs/tm_tradeshow_management_td_tradeshow_documents_1_TD_Tradeshow_Documents.php

// created: 2019-02-13 23:29:07
$dictionary["TD_Tradeshow_Documents"]["fields"]["tm_tradeshow_management_td_tradeshow_documents_1"] = array (
  'name' => 'tm_tradeshow_management_td_tradeshow_documents_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_td_tradeshow_documents_1',
  'source' => 'non-db',
  'module' => 'TM_Tradeshow_Management',
  'bean_name' => 'TM_Tradeshow_Management',
  'side' => 'right',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TD_TRADESHOW_DOCUMENTS_1_FROM_TD_TRADESHOW_DOCUMENTS_TITLE',
  'id_name' => 'tm_tradeshb931agement_ida',
  'link-type' => 'one',
);
$dictionary["TD_Tradeshow_Documents"]["fields"]["tm_tradeshow_management_td_tradeshow_documents_1_name"] = array (
  'name' => 'tm_tradeshow_management_td_tradeshow_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TD_TRADESHOW_DOCUMENTS_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'save' => true,
  'id_name' => 'tm_tradeshb931agement_ida',
  'link' => 'tm_tradeshow_management_td_tradeshow_documents_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'name',
);
$dictionary["TD_Tradeshow_Documents"]["fields"]["tm_tradeshb931agement_ida"] = array (
  'name' => 'tm_tradeshb931agement_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TD_TRADESHOW_DOCUMENTS_1_FROM_TD_TRADESHOW_DOCUMENTS_TITLE_ID',
  'id_name' => 'tm_tradeshb931agement_ida',
  'link' => 'tm_tradeshow_management_td_tradeshow_documents_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/TD_Tradeshow_Documents/Ext/Vardefs/sugarfield_document_name.php

 // created: 2020-03-02 13:01:40
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['audited']=true;
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['massupdate']=false;
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['merge_filter']='disabled';
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['unified_search']=false;
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['TD_Tradeshow_Documents']['fields']['document_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/TD_Tradeshow_Documents/Ext/Vardefs/sugarfield_description.php

 // created: 2021-01-14 09:52:20
$dictionary['TD_Tradeshow_Documents']['fields']['description']['audited']=true;
$dictionary['TD_Tradeshow_Documents']['fields']['description']['massupdate']=false;
$dictionary['TD_Tradeshow_Documents']['fields']['description']['hidemassupdate']=false;
$dictionary['TD_Tradeshow_Documents']['fields']['description']['comments']='Full text of the note';
$dictionary['TD_Tradeshow_Documents']['fields']['description']['duplicate_merge']='enabled';
$dictionary['TD_Tradeshow_Documents']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['TD_Tradeshow_Documents']['fields']['description']['merge_filter']='disabled';
$dictionary['TD_Tradeshow_Documents']['fields']['description']['unified_search']=false;
$dictionary['TD_Tradeshow_Documents']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['TD_Tradeshow_Documents']['fields']['description']['calculated']=false;
$dictionary['TD_Tradeshow_Documents']['fields']['description']['rows']='6';
$dictionary['TD_Tradeshow_Documents']['fields']['description']['cols']='80';

 
?>
