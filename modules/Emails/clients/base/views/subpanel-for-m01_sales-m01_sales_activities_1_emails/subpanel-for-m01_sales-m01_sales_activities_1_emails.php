<?php
// created: 2018-12-13 17:06:28
$viewdefs['Emails']['base']['view']['subpanel-for-m01_sales-m01_sales_activities_1_emails'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'from_collection',
          'type' => 'from',
          'label' => 'LBL_LIST_FROM_ADDR',
          'enabled' => true,
          'default' => true,
          'readonly' => true,
          'fields' => 
          array (
            0 => 'email_address_id',
            1 => 'email_address',
            2 => 'parent_type',
            3 => 'parent_id',
            4 => 'parent_name',
            5 => 'invalid_email',
            6 => 'opt_out',
          ),
        ),
        1 => 
        array (
          'name' => 'name',
          'enabled' => true,
          'default' => true,
          'link' => 'true',
          'related_fields' => 
          array (
            0 => 'total_attachments',
            1 => 'state',
          ),
        ),
        2 => 
        array (
          'name' => 'state',
          'label' => 'LBL_LIST_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'date_sent',
          'label' => 'LBL_LIST_DATE_COLUMN',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'rowactions' => 
  array (
    'actions' => 
    array (
      0 => 
      array (
        'type' => 'rowaction',
        'css_class' => 'btn',
        'tooltip' => 'LBL_PREVIEW',
        'event' => 'list:preview:fire',
        'icon' => 'fa-eye',
        'acl_action' => 'view',
        'allow_bwc' => false,
      ),
      1 => 
      array (
        'type' => 'unlink-action',
        'icon' => 'fa-chain-broken',
        'label' => 'LBL_UNLINK_BUTTON',
      ),
    ),
  ),
  'type' => 'subpanel-list',
);