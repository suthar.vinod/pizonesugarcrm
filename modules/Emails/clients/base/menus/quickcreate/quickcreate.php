<?php
// created: 2016-01-06 02:12:14
$viewdefs['Emails']['base']['menu']['quickcreate'] = array (
  'layout' => 'compose',
  'label' => 'LBL_COMPOSE_MODULE_NAME_SINGULAR',
  'visible' => true,
  'order' => 6,
  'icon' => 'fa-plus',
);