<?php
// created: 2018-12-13 17:06:28
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'name' => 'name',
    'vname' => 'LBL_LIST_SUBJECT',
    'width' => 10,
    'default' => true,
  ),
  'state' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_EMAIL_STATE',
    'width' => 10,
  ),
  'date_sent' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_SENT',
    'width' => 10,
    'default' => true,
  ),
);