<?php
   $hook_version = 1;
   $hook_array = Array();
   $hook_array['after_save'] = Array();
  
   $hook_array['after_save'][] = Array(
      1010, 
      'Update Animal Countdown in Work Product', 
      'custom/modules/WPE_Work_Product_Enrollment/WPE_animal_countdown_Hook.php', 
      'WPE_animalCountHook', 
      'UpdateAnimalCountdownValues'
   );
   
   $hook_array['after_relationship_delete'][] = Array(
      1012, 
      'Update Animal Countdown in Work Product After Relationship Delete', 
      'custom/modules/WPE_Work_Product_Enrollment/WPE_animal_countdown_afterRelationDeleteHook.php', 
      'WPE_animalCountAfterRelationDeleteHook', 
      'UpdateAnimalCountdownValues'
   );
   
   $hook_array['after_relationship_add'][] = Array(
      1011, 
      'Update Animal Countdown in Work Product After Relationship Add', 
      'custom/modules/WPE_Work_Product_Enrollment/WPE_animal_countdown_afterRelationAddHook.php', 
      'WPE_animalCountAfterRelationAddHook', 
      'UpdateAnimalCountdownValues'
   );

   $hook_array['process_record'][] = array(
      11,
      'delete duplicate Activities Audit log Record',
      'custom/modules/WPE_Work_Product_Enrollment/deleteActivitiesAuditLog.php',
      'deleteActivitiesAuditLog',
      'deleteActivitiesAudit'
   );
   
    /*$hook_array['after_relationship_add'][] = Array(
      1099, 
      'Send Email for Animal Assignment Notification in Work Product After Relationship Add', 
      'custom/modules/WPE_Work_Product_Enrollment/WPE_animal_assignment_notification.php', 
      'WPE_animalNotificationAfterRelationAddHook', 
      'SendAnimalAssignmentNotification'
   );*/

?>