<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class WPE_animalCountAfterRelationDeleteHook{
	function UpdateAnimalCountdownValues($bean, $event, $arguments){
		global $db;
		$workProductId	= "";
		$onStudyCount	= 0;
		$onStudyBackupCount = 0;
		if($bean->id!=""){
			$wpe_id =$bean->id; // Work Product Enrollment ID
			//Query to get the Work Product ID	
			$wpe_sql = "SELECT * FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE  
					WHERE WP_WPE.`m03_work_p9bf5ollment_idb`='".$wpe_id."' ORDER BY `WP_WPE`.`date_modified` DESC limit 1";
			//$GLOBALS['log']->fatal($wpe_sql);		
			$wpe_exec = $db->query($wpe_sql);
			if ($wpe_exec->num_rows > 0) 
			{
				$result = $db->fetchByAssoc($wpe_exec);
				$workProductId = $result['m03_work_p7d13product_ida'];
				if($workProductId!="4a529aae-5165-3149-6db7-5702c7b98a5e" && $workProductId!="811ae58e-9281-2755-25d5-5702c7252696")
				{
					//Update Work_Product Bean to update the Animal Countdown Value
					$wp_bean = BeanFactory::getBean('M03_Work_Product', $workProductId);
					$wp_bean->save();
					 
				}
			}


			/* Countdown for Blanket Work Product*/
			

			$wpe_sql = "SELECT * FROM `m03_work_product_wpe_work_product_enrollment_2_c` AS WP_WPE  
					WHERE WP_WPE.`m03_work_p90c4ollment_idb`='".$wpe_id."' ORDER BY `WP_WPE`.`date_modified` DESC limit 1";
			$wpe_exec = $db->query($wpe_sql);
			if ($wpe_exec->num_rows > 0) 
			{
				$result = $db->fetchByAssoc($wpe_exec);
				$workProductId = $result['m03_work_p9f23product_ida'];
				
				//Update Work_Product Bean to update the Animal Countdown Value
				if($workProductId!="4a529aae-5165-3149-6db7-5702c7b98a5e" && $workProductId!="811ae58e-9281-2755-25d5-5702c7252696")
				{
					//sleep(1);
					$wp_bean = BeanFactory::getBean('M03_Work_Product', $workProductId);
					$wp_bean->save();
					
				}
			}

			/********************** */ 
		}
	} 
 
}
?>