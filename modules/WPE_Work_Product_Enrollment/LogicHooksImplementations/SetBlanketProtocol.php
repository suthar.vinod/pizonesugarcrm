<?php
/* Before Save Logic Hook */
class SetBlanketProtocol
{
    public function setProtocol($bean, $event, $arguments)
    {
        //Set Blanket Protocol Manually Only While Creating Record.
        if (isset($arguments['isUpdate']) && $arguments['isUpdate'] == false) {
            //$GLOBALS['log']->fatal('In Create 10');
            $bean->load_relationship('m03_work_product_wpe_work_product_enrollment_1');
            $workProductId = $bean->m03_work_product_wpe_work_product_enrollment_1->get();

            if ($workProductId != "") {
				$wpBean = BeanFactory::getBean('M03_Work_Product', $workProductId[0]);
				if ($wpBean->m03_work_product_id_c != "") {
                    //Assigning Blanket Protocol.
                    $bean->load_relationship('m03_work_product_wpe_work_product_enrollment_2');
                    $bean->m03_work_product_wpe_work_product_enrollment_2->add($wpBean->m03_work_product_id_c);
                }
            }
        }
    }
}