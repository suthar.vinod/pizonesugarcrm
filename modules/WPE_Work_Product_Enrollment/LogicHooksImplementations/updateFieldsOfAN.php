<?php

class HookWPERelManagement {

    /**
     * 
      Animal Module
      -  We would like the USDA Category pulled from the WPE module; however, it needs to pull in the highest category (E>D>C>B) associated to that animal
      o    e.g. WPE on 4/10 has USDA category D, but WPE on 5/10 has USDA category C; the animal module would then show the USDA category as a D
      -  We would like the enrollment status in the animal module to mirror of the most recent, non-deleted (active) Enrollment Status in the Work Product Enrollment record associated to that animal
      o    e.g. WPE on 4/10 states Back-up Used, and WPE on 5/10 states �Non-na�ve Stock�; the enrollment status within the animal module for that animal would then state �non-na�ve stock�
     *
     */
    public function manageFieldsDel($bean, $event, $arguments) {
        if ($arguments['related_module'] == 'ANML_Animals' && $arguments['link'] == 'anml_animals_wpe_work_product_enrollment_1') {
            $bean->formRelDel = "Yes";
            $animalId = $arguments['related_id'];
            //  Fetch that animal.
            if (!empty($animalId)) {
                //  Get its latest WPE
                global $db;

                /*$query = "SELECT wpe.id FROM wpe_work_product_enrollment wpe "
                        . "JOIN anml_animals_wpe_work_product_enrollment_1_c wpe_anml "
                        . "ON wpe.id = wpe_anml.anml_anima9941ollment_idb "
                        . "AND wpe_anml.anml_animals_wpe_work_product_enrollment_1anml_animals_ida = '{$animalId}' "
                        . "AND wpe_anml.deleted = '0' "
                        . "WHERE wpe.deleted = '0' "
                        . "ORDER BY wpe.date_modified DESC";*/
                $query = "SELECT wpe.id FROM wpe_work_product_enrollment wpe "
                        . "JOIN anml_animals_wpe_work_product_enrollment_1_c wpe_anml "
                        . "ON wpe.id = wpe_anml.anml_anima9941ollment_idb "
                        . "AND wpe_anml.anml_animals_wpe_work_product_enrollment_1anml_animals_ida = '{$animalId}' "
                        . "AND wpe_anml.deleted = '0' "
                        . "WHERE wpe.deleted = '0' "
                        . "ORDER BY wpe.date_entered DESC";

                $result = $db->query($query, true);
                $row = $db->fetchByAssoc($result);

                $wpe = BeanFactory::getBean('WPE_Work_Product_Enrollment', $row['id'], array('disable_row_level_security' => true));

                //  Get its Animal
                $animalBean = BeanFactory::getBean("ANML_Animals", $animalId, array('disable_row_level_security' => true));
                $category = $this->_getValues($animalBean, $event);

                //  Check if its latest wpe and animal STATUS are same ?
                if ($wpe->enrollment_status_c != $animalBean->enrollment_status) {
                    $animalBean->enrollment_status = $wpe->enrollment_status_c;
                    $doSave = true;
                }

                //$GLOBALS['log']->fatal('uPDATE from updateFieldsofAN  ==='.$category);


                //  Check if its latest CATEGORY and animal CATEGORY are same ?
                if ($category != $animalBean->usda_category_c) {
                    $animalBean->usda_category_c = $category;
                    $doSave = true;
                }

                //  Check if its latest wpe and animal STATUS are same ?
                if ($wpe->enrollment_status_c != $animalBean->enrollment_status) {
                    $animalBean->enrollment_status = $wpe->enrollment_status_c;
                    $doSave = true;
                }

                //  Check if its latest wpe WP and animal WP are same ?
                $wpName = $wpe->m03_work_product_wpe_work_product_enrollment_1_name;
                $wpName = '';
                if (empty($wpName)) {
                    if (!empty($wpe->m03_work_p7d13product_ida)) {
                        $wpBean = BeanFactory::getBean("M03_Work_Product", $wpe->m03_work_p7d13product_ida, array('disable_row_level_security' => true));
                        $wpName = $wpBean->name;
                    } else if (!empty($bean->bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida)){
                        $bidBean = BeanFactory::getBean("BID_Batch_ID", $bean->bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida, array('disable_row_level_security' => true));
                        $wpName = $bidBean->name;
                    }else {
                        $wpName = '';
                    }
                }
                if ($wpName != $animalBean->assigned_to_wp_c) {
                    $animalBean->assigned_to_wp_c = $wpName;
                    if($wpName!="APS001-AH01"){
                        $animalBean->allocated_work_product_c = '';
                        $animalBean->m03_work_product_id_c = '';
                    }
                    $doSave = true;
                }

                //  If there is a change then save Animal.
                if ($doSave) {
                  //  $animalBean->save();
                }
            }
        }
    }

    public function manageFieldsAdd($bean, $event, $arguments) {
        //  If its Animal changed
        if ($arguments['related_module'] == 'ANML_Animals' && $arguments['link'] == 'anml_animals_wpe_work_product_enrollment_1') {
            $bean->formRelAdd = "Yes";
            $animalId = $arguments['related_id'];

            //  Fetch that animal.
            if (!empty($animalId)) {
                $animalBean = BeanFactory::getBean("ANML_Animals", $animalId, array('disable_row_level_security' => true));
                $category = $this->_getValues($animalBean, $event, $bean);

                //  Check if wpe and animal STATUS are same ?
                if ($bean->enrollment_status_c != $animalBean->enrollment_status) {
                    $animalBean->enrollment_status = $bean->enrollment_status_c;
                    $doSave = true;
                }
                //$GLOBALS['log']->fatal('uPDATE from updateFieldsofAN1  ==='.$category);
                //  Check if its latest CATEGORY and animal CATEGORY are same ?
                if ($category != $animalBean->usda_category_c) {
                    $animalBean->usda_category_c = $category;
                    //$GLOBALS['log']->fatal("Testing category == ".$category);
                    $doSave = true;
                }

                //  Check if wpe and animal STATUS are same ?
                $wpName = $bean->m03_work_product_wpe_work_product_enrollment_1_name;
                $wpName = '';
                if (empty($wpName)) {
                    if (!empty($bean->m03_work_p7d13product_ida)) {
                        $wpBean = BeanFactory::getBean("M03_Work_Product", $bean->m03_work_p7d13product_ida, array('disable_row_level_security' => true));
                        $wpName = $wpBean->name;
                    } else if (!empty($bean->bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida)){
                        $bidBean = BeanFactory::getBean("BID_Batch_ID", $bean->bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida, array('disable_row_level_security' => true));
                        $wpName = $bidBean->name;
                    }else {
                        $wpName = '';
                    }
                }
                if ($wpName != $animalBean->assigned_to_wp_c) {
                    $animalBean->assigned_to_wp_c = $wpName;
                    
                    if($wpName!="APS001-AH01"){
                        $animalBean->allocated_work_product_c = '';
                        $animalBean->m03_work_product_id_c = '';
                    }
                    $doSave = true;
                }

                //  If there is a change then save Animal.
                if ($doSave) {
                    //$GLOBALS['log']->fatal("Testing 131");
                    $animalBean->save();
                }
            }
        }
    }

    private function _getValues(&$bean, $event, $wpeBean = '') {
        //  Fetch related Beans
        $bean->load_relationship('anml_animals_wpe_work_product_enrollment_1');
        /*$wpeRecords = $bean->anml_animals_wpe_work_product_enrollment_1->getBeans(array('order_by' => 'date_modified', 'orderby' => 'date_modified'));*/
        $wpeRecords = $bean->anml_animals_wpe_work_product_enrollment_1->getBeans(array('order_by' => 'date_entered', 'orderby' => 'date_entered'));

        //$GLOBALS['log']->fatal('sss');
        if ($event == 'after_relationship_add') {
            if (!array_key_exists($wpeBean->id, $wpeRecords)) {
                if (!empty($wpeBean)) {
                    $wpeRecords[$wpeBean->id] = $wpeBean;
                }
            }
        }

        $value = '';
        foreach ($wpeRecords as $wpeRecord) {
            //  Work for Category
            //$value = $wpeRecord->usda_category_c;
            if($wpeRecord->enrollment_status_c=="On Study Transferred" || $wpeRecord->enrollment_status_c=="On Study" || $wpeRecord->enrollment_status_c=="On Study Backup"){
               //  Work for Category

                //$value = $wpeRecord->usda_category_c;
                if (empty($value)) {
                    $value = ord($wpeRecord->usda_category_c);
                }

                $newValue = ord($wpeRecord->usda_category_c);
                if ($newValue > $value) {
                    $value = $newValue;
                } 
            }
        }

        if (!empty($value)) {
            $value = chr($value);
        }else{
            $value = "B";
        }

        return $value;
    }

}