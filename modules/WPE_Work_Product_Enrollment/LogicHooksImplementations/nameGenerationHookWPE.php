<?php

class HookBeforeSaveForNameGenerationWPE {
    /**
     * 
      We would like the Name to be automatically generated with: Work Product ID space �WPE� next sequential
      three digit number for that WP
      e.g. Work Product APS001-AH01, first WPE
      APS001-AH01 WPE001
     */
    public function generateName($bean, $event, $arguments) {
        $tableName	= 'wpe_work_product_enrollment';
        $prefix 	= 'WPA';
        $fieldName	= 'name';

        
        //$GLOBALS['log']->fatal("WPE_autoWPA_TS===>".$bean->autoWPA_TS);

        //  Get related Record Name
        $wpName = $this->_getWPName($bean);
        //$prefix = $wpName . ' ' . $prefix;
		$prefix = $wpName;

        if (empty($bean->fetched_row[$fieldName]) || strpos($bean->$fieldName, $prefix) === FALSE) {

            if($bean->autoWPA_TS=="Yes"){

                $wpName         = $this->_getWPName($bean);
                $autoWPA_TSName = $bean->autoWPA_TSName;
                $autoWPA_usdaId = $bean->usda_id_c;    
                $bean->$fieldName = $wpName." ".$autoWPA_TSName."-".$autoWPA_usdaId;
                //$bean->usda_id_c = $this->_getTestUSDAID($bean);;    
            }
            
            //$code = $this->_getCurrentId($tableName, $prefix, $fieldName);
            //$bean->$fieldName = $code;

            //$GLOBALS['log']->fatal("WPE_BeanCheck===>".$bean->anml_animals_wpe_work_product_enrollment_1_name);
           // $GLOBALS['log']->fatal("WPE_BeanCheckf===>".$bean->anml_animals_wpe_work_product_enrollment_1anml_animals_ida);
            if($bean->anml_animals_wpe_work_product_enrollment_1anml_animals_ida!="")
                $bean->usda_id_c = $this->_getTestUSDAID($bean);;
        }
    }

    private function _getTestUSDAID(&$bean) {

        //$GLOBALS['log']->fatal("WPE_Bean1===>".$bean->anml_animals_wpe_work_product_enrollment_1_name);
        //$GLOBALS['log']->fatal("WPE_Bean2===>".$bean->anml_animals_wpe_work_product_enrollment_1anml_animals_ida);
        $usda_id_c = '';
        if (empty($bean->anml_animals_wpe_work_product_enrollment_1_name)) {
            $wpB = BeanFactory::getBean("ANML_Animals", $bean->anml_animals_wpe_work_product_enrollment_1anml_animals_ida, array('disable_row_level_security' => true));
            $usda_id_c = $wpB->usda_id_c;
        } else {
            if ($bean->anml_animals_wpe_work_product_enrollment_1anml_animals_ida != $bean->fetched_row['anml_animals_wpe_work_product_enrollment_1anml_animals_ida']) {
                $wpB = BeanFactory::getBean("ANML_Animals", $bean->anml_animals_wpe_work_product_enrollment_1anml_animals_ida, array('disable_row_level_security' => true));
                $usda_id_c = $wpB->usda_id_c;
            } else {
                $usda_id_c = $bean->usda_id_c;
            }
        }    
        //$GLOBALS['log']->fatal("usda_id_c===>".$usda_id_c);    
        return $usda_id_c;
    }


    private function _getWPName(&$bean) {
        $name = '';
        if (empty($bean->m03_work_product_wpe_work_product_enrollment_1_name)) {
            $wpB = BeanFactory::getBean("M03_Work_Product", $bean->m03_work_p7d13product_ida, array('disable_row_level_security' => true));
            $name = $wpB->name;
        } else {
            if ($bean->m03_work_p7d13product_ida != $bean->fetched_row['m03_work_p7d13product_ida']) {
                $wpB = BeanFactory::getBean("M03_Work_Product", $bean->m03_work_p7d13product_ida, array('disable_row_level_security' => true));
                $name = $wpB->name;
            } else {
                $name = $bean->m03_work_product_wpe_work_product_enrollment_1_name;
            }
        }        
        return $name;
    }

    private function _getCurrentId($tableName, $prefix, $fieldName) {
        global $db;
        //   get the year in 2 digits
        $year = date("y");
        //  Lock Table
        $sql_lock = "Lock TABLES {$tableName} READ";
        $db->query($sql_lock);

        //  In our Sales Activities Module, we would like to updated the automatic naming function from CP{YY}-XXXX.
        //$query = "SELECT CONVERT(SUBSTRING_INDEX({$fieldName},'WPE',-1), UNSIGNED INTEGER) AS {$fieldName} From {$tableName} "
        //        . "WHERE {$fieldName} LIKE '%{$prefix}%' "
        //        . "ORDER BY {$fieldName} DESC LIMIT 0,1";
				
		$queryNew = "SELECT {$fieldName} FROM {$tableName} WHERE {$fieldName} LIKE '{$prefix}%' AND `deleted`=0 ORDER BY date_entered DESC LIMIT 1";


        $result = $db->query($queryNew);

        //  Unlock Table
        $sql_unlock = "UNLOCK TABLES";
        $db->query($sql_unlock);

        $row 		= $db->fetchByAssoc($result);
		$system_id	= trim($row[$fieldName]);
		$nextNumber = 1;
        if (!empty($system_id)) {
			
			$pos = strpos($system_id, " ");
			if ($pos === false) {
				 $lastNumber = substr($system_id, 3); 
				  
			} else {
				$nameStr = trim(str_replace($prefix,"",$system_id));
				$lastNumber = substr($nameStr, 3); 
			} 
            $lastNumber = (int)$lastNumber;
            $nextNumber = $lastNumber + 1;
            
            $length = strlen($number);
            $allowed_length = 4;
			
			$nextNumber = str_pad(strval($nextNumber), $allowed_length, "0", STR_PAD_LEFT);
			 
            $code  = $prefix." WPA".$nextNumber;
        } else {
            $code = $prefix." WPA0001";
        }

        return $code;
    }

}