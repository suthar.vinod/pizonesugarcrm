<?php

class HookAnimalRelManagement {

    /**
     * 
      Animal Module
      -  We would like the USDA Category pulled from the WPE module; however, it needs to pull in the highest category (E>D>C>B) associated to that animal
      o    e.g. WPE on 4/10 has USDA category D, but WPE on 5/10 has USDA category C; the animal module would then show the USDA category as a D
      -  We would like the enrollment status in the animal module to mirror of the most recent, non-deleted (active) Enrollment Status in the Work Product Enrollment record associated to that animal
      o    e.g. WPE on 4/10 states Back-up Used, and WPE on 5/10 states �Non-na�ve Stock�; the enrollment status within the animal module for that animal would then state �non-na�ve stock�
     *
     */
    public function manageCategoryAndStatus($bean, $event, $arguments) {
        //  If updated Status and WP then process
        
        if (!((isset($bean->formRelDel) && $bean->formRelDel == "Yes") || (isset($bean->formRelAdd) && $bean->formRelAdd == "Yes"))) 
        {
            $animalId = $bean->anml_animals_wpe_work_product_enrollment_1anml_animals_ida;
            $doSave = false;
            $doSave = true;

            if (!empty($animalId)) 
            {
                $animalBean = BeanFactory::getBean("ANML_Animals", $animalId, array('disable_row_level_security' => true));
                $maxWPE = $this->_getLatestWPE($animalBean);
                if ($maxWPE == $bean->id) 
                {
                    if ($bean->enrollment_status_c != $animalBean->enrollment_status) 
                    {
                        $animalBean->enrollment_status = $bean->enrollment_status_c;
                        $doSave = true;
                    }

                    $wpName = $bean->m03_work_product_wpe_work_product_enrollment_1_name;
                    if (empty($wpName)) 
                    {
                        if (!empty($bean->m03_work_p7d13product_ida)) 
                        {
                            $wpBean = BeanFactory::getBean("M03_Work_Product", $bean->m03_work_p7d13product_ida, array('disable_row_level_security' => true));
                            $wpName = $wpBean->name;
                        } else if (!empty($bean->bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida)){
                            $bidBean = BeanFactory::getBean("BID_Batch_ID", $bean->bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida, array('disable_row_level_security' => true));
                            $wpName = $bidBean->name;
                        }else {
                            $wpName = '';
                        }
                    }
                    if ($wpName != $animalBean->assigned_to_wp_c) 
                    {
                        
                        $animalBean->assigned_to_wp_c = $wpName;
                        if($wpName!="APS001-AH01"){
                            $animalBean->allocated_work_product_c = '';
                            $animalBean->m03_work_product_id_c = '';
                        }
                        $doSave = true;
                    }
                }

                 //  Check if the value is different from Animal.
                $category = $this->_getValues($animalBean);

                //  If there is a change in Category
                if ($category != $animalBean->usda_category_c) 
                {
                   
                    $animalBean->usda_category_c = $category;
                    $doSave = true;
                }
                if ($doSave) 
                {
                    //$GLOBALS['log']->fatal("Testing 5");
                    $animalBean->save();
                }
            }
        }  else {
            //$GLOBALS['log']->fatal("should not run");
        }
    }

    private function _getValues(&$animalBean) {        
        //  Fetch related Beans
        $animalBean->load_relationship('anml_animals_wpe_work_product_enrollment_1');
        /*$wpeRecords = $animalBean->anml_animals_wpe_work_product_enrollment_1->getBeans(array('order_by' => 'date_modified', 'orderby' => 'date_modified'));*/
        $wpeRecords = $animalBean->anml_animals_wpe_work_product_enrollment_1->getBeans(array('order_by' => 'date_entered', 'orderby' => 'date_entered'));

        $value = '';
        foreach ($wpeRecords as $wpeRecord) {
            if($wpeRecord->enrollment_status_c=="On Study Transferred" || $wpeRecord->enrollment_status_c=="On Study"|| $wpeRecord->enrollment_status_c=="On Study Backup"){
                //  Work for Category
                if (empty($value)) {
                    $value = ord($wpeRecord->usda_category_c);
                }

                $newValue = ord($wpeRecord->usda_category_c);
                if ($newValue > $value) {
                    $value = $newValue;
                } 
            }            
        }

        if (!empty($value)) {
            $value = chr($value);
        }else{
            $value = "B";
        }

        return $value;
    }

    private function _getLatestWPE($animalBean) 
    {
        //  Fetch related Beans
        global $db;
        $id = '';
        $sql = "SELECT id FROM wpe_work_product_enrollment WHERE id IN (SELECT anml_anima9941ollment_idb FROM anml_animals_wpe_work_product_enrollment_1_c WHERE deleted = 0 AND anml_animals_wpe_work_product_enrollment_1anml_animals_ida = '{$animalBean->id}') ORDER BY date_entered DESC LIMIT 1";
        $exec = $db->query($sql);
        if ($exec->num_rows > 0) 
        {
            $result = $db->fetchByAssoc($exec);
            $id = $result['id'];
        }
        return $id;
    }
}