<?php

global $db;
$i = 1;
if (isset($_GET['update'])) {
  $check = $_GET['update'];
  $WPAreportSql = "SELECT IFNULL(wpe_work_product_enrollment.id,'') primaryid,
                IFNULL(wpe_work_product_enrollment.name,'') WPE_WORK_PRODUCT_ENROL972FC8,
                IFNULL(l2.id,'') l2_id,
                IFNULL(l2.name,'') l2_name,
                IFNULL(l1.id,'') l1_id,
                IFNULL(l1.name,'') l1_name,
                l1_cstm.m03_work_product_id_c L1_CSTM_M03_WORK_PRODU479074,m03_work_product1.name m03_work_product1_name
                FROM wpe_work_product_enrollment
                LEFT JOIN  m03_work_product_wpe_work_product_enrollment_1_c l1_1 
                ON wpe_work_product_enrollment.id=l1_1.m03_work_p9bf5ollment_idb AND l1_1.deleted=0
                LEFT JOIN  m03_work_product l1 
                ON l1.id=l1_1.m03_work_p7d13product_ida AND l1.deleted=0
                LEFT JOIN  m03_work_product_wpe_work_product_enrollment_2_c l2_1 
                ON wpe_work_product_enrollment.id=l2_1.m03_work_p90c4ollment_idb AND l2_1.deleted=0
                LEFT JOIN  m03_work_product l2 
                ON l2.id=l2_1.m03_work_p9f23product_ida AND l2.deleted=0
                LEFT JOIN m03_work_product_cstm l1_cstm 
                ON l1.id = l1_cstm.id_c
                LEFT JOIN m03_work_product m03_work_product1 
                ON m03_work_product1.id = l1_cstm.m03_work_product_id_c AND IFNULL(m03_work_product1.deleted,0)=0 
                WHERE ((((coalesce(LENGTH(l2.id), 0) = 0)) AND ((coalesce(LENGTH(l1_cstm.m03_work_product_id_c), 0) <> 0)) AND (wpe_work_product_enrollment.date_entered > '2021-01-12 00:00:00'))) 
                AND  wpe_work_product_enrollment.deleted=0 
                AND IFNULL(m03_work_product1.deleted,0)=0";


  $result = $GLOBALS['db']->query($WPAreportSql);

  while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
    $wpaID = $row['primaryid'];
    $blanketID = $row['L1_CSTM_M03_WORK_PRODU479074'];
    $wpaName = $row['WPE_WORK_PRODUCT_ENROL972FC8'];

    $unique_id = create_guid();

    //Inerting Relationship in the in the form of new row
    $insertSql = "INSERT INTO `m03_work_product_wpe_work_product_enrollment_2_c` (`id`, `date_modified`, `deleted`, `m03_work_p9f23product_ida`, `m03_work_p90c4ollment_idb`) VALUES ('$unique_id', now(), '0', '$blanketID', '$wpaID')";

    $resultsUpdateBlankletInWPA = $GLOBALS['db']->query($insertSql);
    if ($resultsUpdateBlankletInWPA) {
      echo "<br>" . $i . ".) " . $wpaName . " >>> Updated Successfully";
    } else {
      echo "<br>" . $i . ".) " . $wpaName . " >>> Updated Failed";
    }
    if ($check == "test_mode" && $i == 3)
      break;
    $i++;
  }
} else {
  echo "Invalid Request";
}
