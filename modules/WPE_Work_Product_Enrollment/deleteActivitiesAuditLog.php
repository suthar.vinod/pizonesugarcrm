<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class deleteActivitiesAuditLog{
	function deleteActivitiesAudit($bean, $event, $arguments) {
		global $db;
		$wpeID = $bean->id;		
		$queryAuditLog = "SELECT id,count(*) as NUM FROM `wpe_work_product_enrollment_audit` WHERE `field_name`='activities_c' AND `parent_id`='".$wpeID."' group by before_value_string,after_value_string,date_created";
		$auditLogResult = $db->query($queryAuditLog);
		
		if($auditLogResult->num_rows > 0 ){
			while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
				if($fetchAuditLog['NUM']>1){
					$recordID = $fetchAuditLog['id'];
					$sql_DeleteAudit = "DELETE from `wpe_work_product_enrollment_audit` WHERE `field_name`='activities_c' AND `parent_id`='".$wpeID."' AND id = '".$recordID."'";
					$db->query($sql_DeleteAudit);
				}				
			}
		}
		/* To delete null entry for activity_c field */
		$sql_del_null = "DELETE FROM `wpe_work_product_enrollment_audit` WHERE `field_name`='activities_c' AND `parent_id`='".$wpeID."' AND  (`before_value_string` is NULL OR `before_value_string`='' OR `before_value_string`='^^') AND  (`after_value_string` is NULL OR `after_value_string`='' OR `after_value_string`='^^')";
		$db->query($sql_del_null);
	}
}
?>