<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Dependencies/read_only_field.php


$dependencies['WPE_Work_Product_Enrollment']['make_read_only_enroll'] = array(
    'hooks' => array("all"),   
    'trigger' => 'true',
    //Optional, the trigger for the dependency. Defaults to 'true'.
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'm03_work_product_wpe_work_product_enrollment_2_name',
                'value'=>'true'
            ),
        ),
    ),
);


?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Dependencies/workProduct_readonly_dep.php

$dependencies['WPE_Work_Product_Enrollment']['readonly_fields'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('id'),
    'onload'        => true,
    'actions'       => array(
        // array(
        //     'name' => 'ReadOnly',
        //     'params' => array(
        //         'target' => 'm03_work_product_wpe_work_product_enrollment_1_name',
        //         'value' => 'not(equal($id,""))',                
        //     ),             
        // ), 
    ),
);

$dependencies['WPE_Work_Product_Enrollment']['WP_required_dep'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_wpe_work_product_enrollment_1_name','bid_batch_id_wpe_work_product_enrollment_1_name'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_wpe_work_product_enrollment_1_name',
                //'label' => 'wp_required_label',
                'value' => 'or(and(equal($bid_batch_id_wpe_work_product_enrollment_1_name,""),not(equal($m03_work_product_wpe_work_product_enrollment_1_name,""))),and(equal($bid_batch_id_wpe_work_product_enrollment_1_name,""),equal($m03_work_product_wpe_work_product_enrollment_1_name,"")))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'bid_batch_id_wpe_work_product_enrollment_1_name',
                //'label' => 'wp_required_label',
                'value' => 'or(and(equal($m03_work_product_wpe_work_product_enrollment_1_name,""),not(equal($bid_batch_id_wpe_work_product_enrollment_1_name,""))),and(equal($m03_work_product_wpe_work_product_enrollment_1_name,""),equal($bid_batch_id_wpe_work_product_enrollment_1_name,"")))',
            ),
        ),
    ),
);
$dependencies['WPE_Work_Product_Enrollment']['WP_Visibility_dep'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_wpe_work_product_enrollment_1_name','bid_batch_id_wpe_work_product_enrollment_1_name'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(        
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_wpe_work_product_enrollment_1_name',
                'value' => 'or(and(equal($bid_batch_id_wpe_work_product_enrollment_1_name,""),not(equal($m03_work_product_wpe_work_product_enrollment_1_name,""))),and(equal($bid_batch_id_wpe_work_product_enrollment_1_name,""),equal($m03_work_product_wpe_work_product_enrollment_1_name,"")))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'bid_batch_id_wpe_work_product_enrollment_1_name',
                'value' => 'or(and(equal($m03_work_product_wpe_work_product_enrollment_1_name,""),not(equal($bid_batch_id_wpe_work_product_enrollment_1_name,""))),and(equal($m03_work_product_wpe_work_product_enrollment_1_name,""),equal($bid_batch_id_wpe_work_product_enrollment_1_name,"")))',
            ),
        ),
    ),
);
?>
