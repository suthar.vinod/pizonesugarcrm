<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/LogicHooks/autoGenHookRegistration.php


$hook_version = 1;
$hook_array['before_save'][] = array(
    1001,
    'Auto Generating Id',
    'custom/modules/WPE_Work_Product_Enrollment/LogicHooksImplementations/nameGenerationHookWPE.php',
    'HookBeforeSaveForNameGenerationWPE',
    'generateName'
);

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/LogicHooks/animalLogicHookRegistration.php


$hook_version = 1;
$hook_array['after_save'][] = array(
    1001,
    'Manage Category and status for Related Animal',
    'custom/modules/WPE_Work_Product_Enrollment/LogicHooksImplementations/updateCategoryAndStatusHookForAN.php',
    'HookAnimalRelManagement',
    'manageCategoryAndStatus'
);
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/LogicHooks/relAddDel.php


$hook_version = 1;
$hook_array['after_relationship_add'][] = array(
    1001,
    'Manage Category and status of Hook',
    'custom/modules/WPE_Work_Product_Enrollment/LogicHooksImplementations/updateFieldsOfAN.php',
    'HookWPERelManagement',
    'manageFieldsAdd'
);
$hook_array['after_relationship_delete'][] = array(
    1001,
    'Manage Category and status of Hook',
    'custom/modules/WPE_Work_Product_Enrollment/LogicHooksImplementations/updateFieldsOfAN.php',
    'HookWPERelManagement',
    'manageFieldsDel'
);
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/LogicHooks/setBlankletProtolWPAHook.php


$hook_version = 1;
$hook_array['before_save'][] = array(
    22,
    'Set Blanket Protocol While creation of WPA',
    'custom/modules/WPE_Work_Product_Enrollment/LogicHooksImplementations/SetBlanketProtocol.php',
    'SetBlanketProtocol',
    'setProtocol'
);
?>
