<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Language/en_us.WPEsModuleCustomizations.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TRANSFER_DATE'] = 'Transfer Date';
$mod_strings['LBL_COMMENTS'] = 'Comments';
$mod_strings['LBL_ENROLLMENT_STATUS'] = 'Enrollment Status';
$mod_strings['LBL_USDA_CATEGORY'] = 'USDA Category';
$mod_strings['LBL_USDA_ID'] = 'USDA ID';

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Language/en_us.customanml_animals_wpe_work_product_enrollment_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Language/en_us.customanml_animals_wpe_work_product_enrollment_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE'] = 'Animals';
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Animals';

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE'] = 'Animals';
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Animals';


?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Language/en_us.wpe_mass_create.php.WPEsModuleCustomizations.lang.php

$mod_strings['LBL_WPE_MASS_CREATION'] = 'WPAs Mass Creation';
$mod_strings['LBL_CREATE_WPES_BUTTON_LABEL'] = 'Create WPAs';

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Language/en_us.customwpe_work_product_enrollment_wpe_work_product_enrollment_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_R_TITLE'] = 'Work Product Enrollment';
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_L_TITLE'] = 'Work Product Enrollment';

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Language/en_us.customwpe_work_product_enrollment_anml_animals_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE'] = 'Animals';
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Animals';

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Language/en_us.customm03_work_product_wpe_work_product_enrollment_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Language/en_us.customm03_work_product_wpe_work_product_enrollment_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Language/en_us.customgd_group_design_wpe_work_product_enrollment_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_GD_GROUP_DESIGN_TITLE'] = 'Group Designs';
$mod_strings['LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Group Designs';

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Language/en_us.custombid_batch_id_wpe_work_product_enrollment_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_BID_BATCH_ID_TITLE'] = 'Batch IDs';
$mod_strings['LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Batch IDs';

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DATE_ENTERED'] = 'Date Created';
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE'] = 'Test Systems';
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE'] = 'Test Systems';
$mod_strings['LBL_ACTIVITIES'] = 'Activities';
$mod_strings['LNK_NEW_RECORD'] = 'Create Work Product Assignment';
$mod_strings['LNK_LIST'] = 'View Work Product Assignments';
$mod_strings['LBL_MODULE_NAME'] = 'Work Product Assignments';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Work Product Assignment';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Work Product Assignment';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Work Product Assignment vCard';
$mod_strings['LNK_IMPORT_WPE_WORK_PRODUCT_ENROLLMENT'] = 'Import Work Product Assignments';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Work Product Assignment List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Work Product Assignment';
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_SUBPANEL_TITLE'] = 'Work Product Assignments';
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_L_TITLE'] = 'Work Product Assignment';
$mod_strings['LBL_NAME'] = 'WPA ID';
$mod_strings['LBL_TESTSYSTEM_HIDDEN_C'] = 'testsystem hidden c';
$mod_strings['LBL_SELECT_APS001_AH01_C'] = 'Select APS001-AH01';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'Blanket Work Product';
$mod_strings['LBL_ENROLLMENT_STATUS'] = 'Status';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Blanket Work Product';
$mod_strings['LBL_SELECT_APS177_AH01_C'] = 'Select APS177-AH15';
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_FOCUS_DRAWER_DASHBOARD'] = 'Work Product Assignments Focus Drawer';
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_RECORD_DASHBOARD'] = 'Work Product Assignments Record Dashboard';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_NAME_M03_WORK_PRODUCT_ID'] = 'Blanket Work Product (related Work Product ID)';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_NAME_FIELD_TITLE'] = 'Blanket Work Product';
$mod_strings['LBL_TRANSFER_DATE'] = 'Assignment Date';
$mod_strings['LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_NAME_FIELD_TITLE'] = 'Group Designs';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_NAME_FIELD_TITLE'] = 'Work Products';

?>
