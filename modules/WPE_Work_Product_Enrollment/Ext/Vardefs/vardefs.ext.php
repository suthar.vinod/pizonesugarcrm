<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/nameFieldCustomization.php


$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['required'] = 'false';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['readonly'] = 'true';

$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['required'] = 'true';

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_transfer_date_c.php

 // created: 2017-09-12 14:57:45
$dictionary['WPE_Work_Product_Enrollment']['fields']['transfer_date_c']['labelValue']='Transfer Date';
$dictionary['WPE_Work_Product_Enrollment']['fields']['transfer_date_c']['enforced']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['transfer_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/anml_animals_wpe_work_product_enrollment_1_WPE_Work_Product_Enrollment.php

// created: 2017-09-12 15:03:53
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["anml_animals_wpe_work_product_enrollment_1"] = array (
  'name' => 'anml_animals_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'anml_animals_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'id_name' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["anml_animals_wpe_work_product_enrollment_1_name"] = array (
  'name' => 'anml_animals_wpe_work_product_enrollment_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
  'link' => 'anml_animals_wpe_work_product_enrollment_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["anml_animals_wpe_work_product_enrollment_1anml_animals_ida"] = array (
  'name' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID',
  'id_name' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
  'link' => 'anml_animals_wpe_work_product_enrollment_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/wpe_work_product_enrollment_wpe_work_product_enrollment_1_WPE_Work_Product_Enrollment.php

// created: 2017-09-12 15:06:13
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["wpe_work_product_enrollment_wpe_work_product_enrollment_1"] = array (
  'name' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_L_TITLE',
  'id_name' => 'wpe_work_pd83eollment_ida',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["wpe_work_product_enrollment_wpe_work_product_enrollment_1"] = array (
  'name' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_R_TITLE',
  'id_name' => 'wpe_work_pd83eollment_ida',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["wpe_work_product_enrollment_wpe_work_product_enrollment_1_name"] = array (
  'name' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_R_TITLE',
  'save' => true,
  'id_name' => 'wpe_work_pd83eollment_ida',
  'link' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1',
  'table' => 'wpe_work_product_enrollment',
  'module' => 'WPE_Work_Product_Enrollment',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["wpe_work_product_enrollment_wpe_work_product_enrollment_1_name"] = array (
  'name' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_L_TITLE',
  'save' => true,
  'id_name' => 'wpe_work_pd83eollment_ida',
  'link' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1',
  'table' => 'wpe_work_product_enrollment',
  'module' => 'WPE_Work_Product_Enrollment',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["wpe_work_pd83eollment_ida"] = array (
  'name' => 'wpe_work_pd83eollment_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_R_TITLE_ID',
  'id_name' => 'wpe_work_pd83eollment_ida',
  'link' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1',
  'table' => 'wpe_work_product_enrollment',
  'module' => 'WPE_Work_Product_Enrollment',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["wpe_work_pd83eollment_ida"] = array (
  'name' => 'wpe_work_pd83eollment_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_L_TITLE_ID',
  'id_name' => 'wpe_work_pd83eollment_ida',
  'link' => 'wpe_work_product_enrollment_wpe_work_product_enrollment_1',
  'table' => 'wpe_work_product_enrollment',
  'module' => 'WPE_Work_Product_Enrollment',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/wpe_work_product_enrollment_anml_animals_1_WPE_Work_Product_Enrollment.php

// created: 2017-09-12 15:08:34
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["wpe_work_product_enrollment_anml_animals_1"] = array (
  'name' => 'wpe_work_product_enrollment_anml_animals_1',
  'type' => 'link',
  'relationship' => 'wpe_work_product_enrollment_anml_animals_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["wpe_work_product_enrollment_anml_animals_1_name"] = array (
  'name' => 'wpe_work_product_enrollment_anml_animals_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
  'link' => 'wpe_work_product_enrollment_anml_animals_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["wpe_work_product_enrollment_anml_animals_1anml_animals_idb"] = array (
  'name' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE_ID',
  'id_name' => 'wpe_work_product_enrollment_anml_animals_1anml_animals_idb',
  'link' => 'wpe_work_product_enrollment_anml_animals_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/m03_work_product_wpe_work_product_enrollment_1_WPE_Work_Product_Enrollment.php

// created: 2017-09-12 15:10:30
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["m03_work_product_wpe_work_product_enrollment_1"] = array (
  'name' => 'm03_work_product_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'id_name' => 'm03_work_p7d13product_ida',
  'link-type' => 'one',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["m03_work_product_wpe_work_product_enrollment_1_name"] = array (
  'name' => 'm03_work_product_wpe_work_product_enrollment_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p7d13product_ida',
  'link' => 'm03_work_product_wpe_work_product_enrollment_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["m03_work_p7d13product_ida"] = array (
  'name' => 'm03_work_p7d13product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID',
  'id_name' => 'm03_work_p7d13product_ida',
  'link' => 'm03_work_product_wpe_work_product_enrollment_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_comments_c.php

 // created: 2019-04-17 12:43:55
$dictionary['WPE_Work_Product_Enrollment']['fields']['comments_c']['labelValue']='Comments';
$dictionary['WPE_Work_Product_Enrollment']['fields']['comments_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['WPE_Work_Product_Enrollment']['fields']['comments_c']['enforced']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['comments_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_usda_category_c.php

 // created: 2020-02-04 13:53:58
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_category_c']['labelValue']='USDA Category';
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_category_c']['dependency']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_category_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_activities_c.php

 // created: 2020-12-15 06:26:51
$dictionary['WPE_Work_Product_Enrollment']['fields']['activities_c']['labelValue']='Activities';
$dictionary['WPE_Work_Product_Enrollment']['fields']['activities_c']['dependency']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['activities_c']['required_formula']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['activities_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/m03_work_product_wpe_work_product_enrollment_2_WPE_Work_Product_Enrollment.php

// created: 2020-12-15 06:35:00
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["m03_work_product_wpe_work_product_enrollment_2"] = array (
  'name' => 'm03_work_product_wpe_work_product_enrollment_2',
  'type' => 'link',
  'relationship' => 'm03_work_product_wpe_work_product_enrollment_2',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'id_name' => 'm03_work_p9f23product_ida',
  'link-type' => 'one',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["m03_work_product_wpe_work_product_enrollment_2_name"] = array (
  'name' => 'm03_work_product_wpe_work_product_enrollment_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p9f23product_ida',
  'link' => 'm03_work_product_wpe_work_product_enrollment_2',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["m03_work_p9f23product_ida"] = array (
  'name' => 'm03_work_p9f23product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID',
  'id_name' => 'm03_work_p9f23product_ida',
  'link' => 'm03_work_product_wpe_work_product_enrollment_2',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_testsystem_hidden_c.php

 // created: 2020-12-15 08:58:03
$dictionary['WPE_Work_Product_Enrollment']['fields']['testsystem_hidden_c']['labelValue']='WPA Hidden Field';
$dictionary['WPE_Work_Product_Enrollment']['fields']['testsystem_hidden_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['WPE_Work_Product_Enrollment']['fields']['testsystem_hidden_c']['enforced']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['testsystem_hidden_c']['dependency']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['testsystem_hidden_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_select_aps001_ah01_c.php

 // created: 2020-12-15 08:59:03
$dictionary['WPE_Work_Product_Enrollment']['fields']['select_aps001_ah01_c']['labelValue']='Select APS001-AH01';
$dictionary['WPE_Work_Product_Enrollment']['fields']['select_aps001_ah01_c']['enforced']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['select_aps001_ah01_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_anml_animals_wpe_work_product_enrollment_1_name.php

 // created: 2020-11-04 14:11:31
$dictionary['WPE_Work_Product_Enrollment']['fields']['anml_animals_wpe_work_product_enrollment_1_name']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_enrollment_status_c.php

 // created: 2020-12-15 12:58:42
$dictionary['WPE_Work_Product_Enrollment']['fields']['enrollment_status_c']['labelValue']='Status';
$dictionary['WPE_Work_Product_Enrollment']['fields']['enrollment_status_c']['dependency']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['enrollment_status_c']['required_formula']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['enrollment_status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2021-01-05 07:45:53
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['audited']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['comments']='Date record created';
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['unified_search']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['calculated']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['enable_range_search']='1';
$dictionary['WPE_Work_Product_Enrollment']['fields']['date_entered']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_select_aps177_ah01_c.php

 // created: 2021-02-04 06:58:51
$dictionary['WPE_Work_Product_Enrollment']['fields']['select_aps177_ah01_c']['labelValue']='Select APS177-AH15';
$dictionary['WPE_Work_Product_Enrollment']['fields']['select_aps177_ah01_c']['enforced']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['select_aps177_ah01_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_usda_id_c.php

 // created: 2021-04-07 14:01:02
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['labelValue']='USDA ID';
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['formula']='related($anml_animals_wpe_work_product_enrollment_1,"usda_id_c")';
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['enforced']='false';
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['dependency']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['required_formula']='';
$dictionary['WPE_Work_Product_Enrollment']['fields']['usda_id_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/gd_group_design_wpe_work_product_enrollment_1_WPE_Work_Product_Enrollment.php

// created: 2021-06-14 05:42:43
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["gd_group_design_wpe_work_product_enrollment_1"] = array (
  'name' => 'gd_group_design_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'gd_group_design_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'GD_Group_Design',
  'bean_name' => 'GD_Group_Design',
  'side' => 'right',
  'vname' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'id_name' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
  'link-type' => 'one',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["gd_group_design_wpe_work_product_enrollment_1_name"] = array (
  'name' => 'gd_group_design_wpe_work_product_enrollment_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_GD_GROUP_DESIGN_TITLE',
  'save' => true,
  'id_name' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
  'link' => 'gd_group_design_wpe_work_product_enrollment_1',
  'table' => 'gd_group_design',
  'module' => 'GD_Group_Design',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida"] = array (
  'name' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID',
  'id_name' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
  'link' => 'gd_group_design_wpe_work_product_enrollment_1',
  'table' => 'gd_group_design',
  'module' => 'GD_Group_Design',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_m03_work_p9f23product_ida.php

 // created: 2021-11-30 09:24:54
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['name']='m03_work_p9f23product_ida';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['type']='id';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['source']='non-db';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['vname']='LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['id_name']='m03_work_p9f23product_ida';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['link']='m03_work_product_wpe_work_product_enrollment_2';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['table']='m03_work_product';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['module']='M03_Work_Product';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['rname']='id';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['reportable']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['side']='right';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['massupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['duplicate_merge']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['hideacl']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['audited']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p9f23product_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_m03_work_product_wpe_work_product_enrollment_2_name.php

 // created: 2021-11-30 09:24:54
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['audited']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['hidemassupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['duplicate_merge']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['duplicate_merge_dom_value']='0';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['merge_filter']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['calculated']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['vname']='LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_NAME_FIELD_TITLE';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['massupdate']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['reportable']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_2_name']['unified_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida.php

 // created: 2022-02-01 14:19:08
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['name']='gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['type']='id';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['source']='non-db';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['vname']='LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['id_name']='gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['link']='gd_group_design_wpe_work_product_enrollment_1';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['table']='gd_group_design';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['module']='GD_Group_Design';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['rname']='id';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['reportable']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['side']='right';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['massupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['duplicate_merge']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['hideacl']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['audited']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_gd_group_design_wpe_work_product_enrollment_1_name.php

 // created: 2022-02-01 14:19:08
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['audited']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['massupdate']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['hidemassupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['duplicate_merge']='enabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['duplicate_merge_dom_value']='1';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['merge_filter']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['reportable']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['unified_search']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['calculated']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['gd_group_design_wpe_work_product_enrollment_1_name']['vname']='LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/bid_batch_id_wpe_work_product_enrollment_1_WPE_Work_Product_Enrollment.php

// created: 2022-04-26 06:50:11
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["bid_batch_id_wpe_work_product_enrollment_1"] = array (
  'name' => 'bid_batch_id_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'BID_Batch_ID',
  'bean_name' => 'BID_Batch_ID',
  'side' => 'right',
  'vname' => 'LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'id_name' => 'bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida',
  'link-type' => 'one',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["bid_batch_id_wpe_work_product_enrollment_1_name"] = array (
  'name' => 'bid_batch_id_wpe_work_product_enrollment_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_BID_BATCH_ID_TITLE',
  'save' => true,
  'id_name' => 'bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida',
  'link' => 'bid_batch_id_wpe_work_product_enrollment_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'name',
);
$dictionary["WPE_Work_Product_Enrollment"]["fields"]["bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida"] = array (
  'name' => 'bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID',
  'id_name' => 'bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida',
  'link' => 'bid_batch_id_wpe_work_product_enrollment_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_m03_work_p7d13product_ida.php

 // created: 2022-04-26 09:16:58
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['name']='m03_work_p7d13product_ida';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['type']='id';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['source']='non-db';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['vname']='LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['id_name']='m03_work_p7d13product_ida';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['link']='m03_work_product_wpe_work_product_enrollment_1';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['table']='m03_work_product';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['module']='M03_Work_Product';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['rname']='id';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['reportable']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['side']='right';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['massupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['duplicate_merge']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['hideacl']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['audited']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_p7d13product_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_m03_work_product_wpe_work_product_enrollment_1_name.php

 // created: 2022-04-26 09:17:00
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['required']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['audited']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['massupdate']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['hidemassupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['duplicate_merge']='enabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['duplicate_merge_dom_value']='1';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['merge_filter']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['reportable']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['unified_search']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['calculated']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['m03_work_product_wpe_work_product_enrollment_1_name']['vname']='LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/WPE_Work_Product_Enrollment/Ext/Vardefs/sugarfield_name.php

 // created: 2022-04-26 11:48:58
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['len']='255';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['required']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['audited']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['massupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['unified_search']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['calculated']='1';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['hidemassupdate']=false;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['importable']='false';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['duplicate_merge']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['merge_filter']='disabled';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['formula']='concat(related($bid_batch_id_wpe_work_product_enrollment_1,"name"),related($m03_work_product_wpe_work_product_enrollment_1,"name")," ",related($anml_animals_wpe_work_product_enrollment_1,"animal_id_c"),"-",related($anml_animals_wpe_work_product_enrollment_1,"usda_id_c"))';
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['enforced']=true;
$dictionary['WPE_Work_Product_Enrollment']['fields']['name']['readonly']=true;

 
?>
