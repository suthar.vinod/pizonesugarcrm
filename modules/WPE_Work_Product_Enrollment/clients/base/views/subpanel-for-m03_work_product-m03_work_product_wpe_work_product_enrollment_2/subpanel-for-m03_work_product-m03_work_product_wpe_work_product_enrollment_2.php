<?php
// created: 2021-08-14 12:38:02
$viewdefs['WPE_Work_Product_Enrollment']['base']['view']['subpanel-for-m03_work_product-m03_work_product_wpe_work_product_enrollment_2'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'anml_animals_wpe_work_product_enrollment_1_name',
          'label' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE',
          'enabled' => true,
          'id' => 'ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1ANML_ANIMALS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'usda_id_c',
          'label' => 'LBL_USDA_ID',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'enrollment_status_c',
          'label' => 'LBL_ENROLLMENT_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'usda_category_c',
          'label' => 'LBL_USDA_CATEGORY',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
        6 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);