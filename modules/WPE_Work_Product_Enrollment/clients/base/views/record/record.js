
    ({
        extendsFrom: 'RecordView',
        initialize: function (options) {
        this._super('initialize', [options]);
        this.context.on('record:view:save', this.handleSave, this);
        this.model.on('data:sync:complete', this.makedisableButton, this);
        //this.model.on("change:m03_work_product_wpe_work_product_enrollment_1_name", this.getWorkProduct, this)
        this.model.on('change:bid_batch_id_wpe_work_product_enrollment_1_name', this.setUSDACategory, this);
        this.model.once("sync",
                function () {
                    this.model.on(
                        "change:m03_work_product_wpe_work_product_enrollment_1_name",
                        this.getWorkProduct,
                        this
                    );
                },
                this
            );
        },

        setUSDACategory: function () {
            var batchId = this.model.get("bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida");
            var wpid = this.model.get('m03_work_p7d13product_ida');
            // console.log("batchId===>", batchId);
            if ((wpid == "" || wpid == null) && (batchId != "" || batchId != null)) {
                var url = app.api.buildURL("getUSDAHighestLetterFromWP");
                var method = "create";
                var data = {
                    module: 'BID_Batch_ID',
                    batchId: batchId
                };
                var callback = {
                    success: _.bind(function successCB(res) {
                        // console.log("Respoonse===>", res);
                        this.model.set("usda_category_c", res);
                    }, this)
                };
                app.api.call(method, url, data, callback);
            }
        },
        handleSave: function() {
            var testsys_id = this.model.get("anml_animals_wpe_work_product_enrollment_1anml_animals_ida");
            var TestSysAPIURL = "rest/v10/ANML_Animals/" + testsys_id + "?fields=termination_date_c&max_num=1&order_by=date_entered:desc";
            // console.log('testsys_id:', TestSysAPIURL);
            var self = this;
                App.api.call("get", TestSysAPIURL, null, {
                    success: function (Data) {
                        // console.log('TestSysAPIURL Data :', Data);
                        var termination_date_c = Data.termination_date_c;
                            if (termination_date_c != '') {
                                app.alert.show("no_wpa_error",
                                    {
                                        level: "error",
                                        autoClose: true,
                                        messages: "Test System is deceased (Termination Date is not null). WPA cannot be created."
                                    });
                                    return false;
                            }
                          else
                            {
                                self._super('handleSave');                                   
                            }
                    }
                });
            
        },

        getWorkProduct: function(model) {
            var self = this;
            if (self.model.get('m03_work_product_wpe_work_product_enrollment_1_name') != "") {
                selectedProductId = model.changed.m03_work_product_wpe_work_product_enrollment_1.id;
                app.api.call("GET", app.api.buildURL("M03_Work_Product/" + selectedProductId + "/get_blanket_product"), null, {
                    success: function(data) {
                        self.model.set('m03_work_p9f23product_ida', data.workID);
                        self.model.set('m03_work_product_wpe_work_product_enrollment_2_name', data.blanket_protocol_c);
                    },
                    error: function(err) {
                        // console.log(err);
                    }
                });
            } else {
                self.model.set('m03_work_p9f23product_ida', null);
                self.model.set('m03_work_product_wpe_work_product_enrollment_2_name', null);
            }
        },

        makedisableButton:function(){ 
            $('div[data-name="select_aps001_ah01_c"]').hide();
            $('div[data-name="select_aps177_ah01_c"]').hide();
            var testsys_id = this.model.get("anml_animals_wpe_work_product_enrollment_1anml_animals_ida");
            var editButton;
            editButton = this.getField('edit_button'); // passing argument as name of button
            var TestSysAPIURL = "rest/v10/ANML_Animals/" + testsys_id + "?fields=termination_date_c&max_num=1&order_by=date_entered:desc";
            console.log('testsys_id:', TestSysAPIURL);
            var self = this;
                App.api.call("get", TestSysAPIURL, null, {
                    success: function (Data) {
                        // console.log('TestSysAPIURL Data :', Data);
                        var termination_date_c = Data.termination_date_c;
                         if (termination_date_c!='') {
                            editButton.setDisabled(true);
                          }                         
                    }
                });
          }, 
})