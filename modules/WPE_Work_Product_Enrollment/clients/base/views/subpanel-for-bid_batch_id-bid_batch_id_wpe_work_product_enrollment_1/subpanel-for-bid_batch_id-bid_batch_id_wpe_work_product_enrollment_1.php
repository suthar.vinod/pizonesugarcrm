<?php
// created: 2022-04-26 11:07:01
$viewdefs['WPE_Work_Product_Enrollment']['base']['view']['subpanel-for-bid_batch_id-bid_batch_id_wpe_work_product_enrollment_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'anml_animals_wpe_work_product_enrollment_1_name',
          'label' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE',
          'enabled' => true,
          'id' => 'ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1ANML_ANIMALS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'usda_id_c',
          'label' => 'LBL_USDA_ID',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'enrollment_status_c',
          'label' => 'LBL_ENROLLMENT_STATUS',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'comments_c',
          'label' => 'LBL_COMMENTS',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'activities_c',
          'label' => 'LBL_ACTIVITIES',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);