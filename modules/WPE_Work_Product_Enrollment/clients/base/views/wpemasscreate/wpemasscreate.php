<?php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$viewdefs['WPE_Work_Product_Enrollment']['base']['view']['wpemasscreate'] = array(
    'buttons' => array(
        array(
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'events' => array(
                'click' => 'button:cancel_button:click',
            ),
        ),
        array(
            'type' => 'rowaction',
            'event' => 'button:wpe_mass_update_button:click',
            'name' => 'save_button',
            'label' => 'LBL_CREATE_WPES_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
    'panels' => array(
        array(
            'name' => 'panel_header',
            'header' => true,
            'fields' => array(
                array(
                    'name' => 'viewtitle',
                    'type' => 'varchar',
                    'dismiss_label' => true,
                    'readonly' => true,
                ),
            )
        ),
        array(
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'fields' => array(
                'm03_work_product_wpe_work_product_enrollment_1_name', 
                'm03_work_product_wpe_work_product_enrollment_2_name',
                'select_aps001_ah01_c',
                'select_aps177_ah01_c',
                'usda_category_c',
                'enrollment_status_c',
                'bid_batch_id_wpe_work_product_enrollment_1_name',
                'comments_c',
            ),
        ),
    ),
);
