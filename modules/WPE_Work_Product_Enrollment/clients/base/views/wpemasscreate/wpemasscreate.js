/*
* Your installation or use of this SugarCRM file is subject to the applicable
* terms available at
* http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
* If you do not agree to all of the applicable terms or do not have the
* authority to bind the entity as an authorized representative, then do not
* install or use this SugarCRM file.
*
* Copyright (C) SugarCRM Inc. All rights reserved.
*/
/**
 * @extends View.Views.Base.CreateView
 */
 ({
    extendsFrom: 'CreateView',

    /**
     * Initialize the view and prepare the model with default button metadata
     * for the current layout.
     */
    initialize: function (options) {
        var self = this;
        self._super("initialize", [options]);
        self.model.set('viewtitle', app.lang.get('LBL_WPE_MASS_CREATION', 'WPE_Work_Product_Enrollment'));
        self.context.on('button:wpe_mass_update_button:click', this.wpeMassCreate, this);
        this.on('render', _.bind(this.massOnload_function, this));
        this.model.on('change:select_aps001_ah01_c', this.function_APS001, this);
        this.model.on('change:select_aps177_ah01_c', this.function_APS177, this);
        this.model.on('change:bid_batch_id_wpe_work_product_enrollment_1_name', this.setUSDACategory, this);
        this.model.addValidationTask('check_WPBatch_field', _.bind(this._doValidateWPBatchFiled, this));
    },

    function_APS001: function () {
        if (this.model.get('select_aps001_ah01_c')) {
            this.model.set('m03_work_p7d13product_ida', "4a529aae-5165-3149-6db7-5702c7b98a5e");
            this.model.set('m03_work_product_wpe_work_product_enrollment_1_name', "APS001-AH01");
        } else {
            this.model.set('m03_work_p7d13product_ida', "");
            this.model.set('m03_work_product_wpe_work_product_enrollment_1_name', "");
        }
    },
    function_APS177: function () {
        if (this.model.get('select_aps177_ah01_c')) {
            this.model.set('m03_work_p7d13product_ida', "99ac34ca-e074-c580-0c73-5702c7e2f7c0");
            this.model.set('m03_work_product_wpe_work_product_enrollment_1_name', "APS177-AH15");
        } else {
            this.model.set('m03_work_p7d13product_ida', "");
            this.model.set('m03_work_product_wpe_work_product_enrollment_1_name', "");
        }
    },
    _doValidateWPBatchFiled: function(fields, errors, callback) {
        var self = this;
        var WP_Field = this.model.get('m03_work_product_wpe_work_product_enrollment_1_name');
        var Batch_id = this.model.get('bid_batch_id_wpe_work_product_enrollment_1_name');
        // console.log('WP_Field V : ', WP_Field);
        // console.log('Batch_id WP_Field: ', Batch_id);
        if ((WP_Field == "" || WP_Field == undefined) && (Batch_id == "" || Batch_id == undefined)) {      
            errors['m03_work_product_wpe_work_product_enrollment_1_name'] = errors['m03_work_product_wpe_work_product_enrollment_1_name'] || {};
            errors['m03_work_product_wpe_work_product_enrollment_1_name'].required = true;
            errors['bid_batch_id_wpe_work_product_enrollment_1_name'] = errors['bid_batch_id_wpe_work_product_enrollment_1_name'] || {};
            errors['bid_batch_id_wpe_work_product_enrollment_1_name'].required = true;
        } 
        callback(null, fields, errors);
},
setUSDACategory: function () {
    var batchId = this.model.get("bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida");
    var wpid = this.model.get('m03_work_p7d13product_ida');
    // console.log("batchId===>", batchId);
    if ((wpid == "" || wpid == null) && (batchId != "" || batchId != null)) {
      var url = app.api.buildURL("getUSDAHighestLetterFromWP");
      var method = "create";
      var data = {
        module: 'BID_Batch_ID',
        batchId: batchId
      };
      var callback = {
        success: _.bind(function successCB(res) {
          // console.log("Respoonse===>", res);
          this.model.set("usda_category_c", res);
        }, this)
      };
      app.api.call(method, url, data, callback);
    }
  },

    massOnload_function: function () {
        var self = this;
        setTimeout(function () {
            self.$el.find('[data-name="select_aps001_ah01_c"]').css('visibility', 'hidden');
            self.$el.find('[data-name="select_aps177_ah01_c"]').css('visibility', 'hidden');
        }, 300);

        var awpArr1 = [];
        var awpArr2 = [];
        var awpArr3 = [];
        var tsAssignedWPArr = [];
        var tsEnrollmentStatusArr = [];
        var totalCount = 0;
        var models = self.context.parent.get('mass_collection').models;
        _.each(models, function (selectedModel) {
            var tsName = selectedModel.get('name');
            var tsAssignedWP = selectedModel.get('assigned_to_wp_c');
            var tsAllocatedWP = selectedModel.get('m03_work_product_id_c');
            var tsEnrollmentStatus = selectedModel.get('enrollment_status');
            var tsTerminationDate = selectedModel.get('termination_date_c');
            tsEnrollmentStatusArr.push(tsEnrollmentStatus);
            tsAssignedWPArr.push(tsAssignedWP);
        });
        var uniqueArr = tsAssignedWPArr.filter(function (itm, i, tsAssignedWPArr) {
            return i == tsAssignedWPArr.indexOf(itm);
        });
        var uniqueEnrollmentStatusArr = tsEnrollmentStatusArr.filter(function (itm, i, tsEnrollmentStatusArr) {
            return i == tsEnrollmentStatusArr.indexOf(itm);
        });
        var currentwp_name = uniqueArr.toString();
        var EnrollmentStatus = uniqueEnrollmentStatusArr.toString();
        if (EnrollmentStatus == "On Study Transferred") {
            var WP_SponsorAPI = "rest/v10/M03_Work_Product?fields=accounts_m03_work_product_1_name,work_product_code_2_c&max_num=1&order_by=date_entered:desc&filter[0][name][$starts]=" + currentwp_name;
            var self = this;
            setTimeout(function () {
                App.api.call("get", WP_SponsorAPI, null, {
                    success: function (Data) {
                        if (Data.records.length > 0) {
                            var WPCode = Data.records[0].work_product_code_2_c;

                            if (WPCode == 'ST01' || WPCode == 'ST01a' || WPCode == 'ST02' || WPCode == 'ST09') {
                                self.$el.find('[data-name="select_aps001_ah01_c"]').css('visibility', 'hidden');
                                self.$el.find('[data-name="select_aps177_ah01_c"]').css('visibility', 'unset');
                            } else {
                                self.$el.find('[data-name="select_aps001_ah01_c"]').css('visibility', 'unset');
                                self.$el.find('[data-name="select_aps177_ah01_c"]').css('visibility', 'hidden');
                            }
                        }
                    }
                });
            }, 500);
        }

    },

    wpeMassCreate: function () {
        var self = this;
        var allFields = self.getFields(self.module, self.model);
        var fieldsToValidate = {};
        for (var fieldKey in allFields) {
            if (app.acl.hasAccessToModel('edit', self.model, fieldKey)) {
                _.extend(fieldsToValidate, _.pick(allFields, fieldKey));
            }
        }
        self.model.doValidate(fieldsToValidate, _.bind(self.processWPEMassCreate, self));
    },
    processWPEMassCreate: function (isValid) {
        var self = this;

        if (isValid) {
             /**17 nov 2021 : WPA mass create double click record create issue fix */
             $('a.rowaction.btn.btn-primary ').addClass('disabled');
            //  Preparing Data
            animalIdsNotToSend = [];
            animalIdsToSend = [];
            var awpErrorMsg = [];
            var tdErrorMsg = [];
            models = self.context.parent.get('mass_collection').models;

            var wpid = self.model.get('m03_work_p7d13product_ida');
            var tsPromises = []
            var tsidsArr = []

            _.each(models, function (selectedModel) {

                var tsid = selectedModel.get('id');
                var tsName = selectedModel.get('name');
                var tsAssignedWP = selectedModel.get('assigned_to_wp_c');
                var tsAllocatedWP = selectedModel.get('allocated_work_product_c');
                var tsTerminationDate = selectedModel.get('termination_date_c');

                /*Start  1235 by Laxmichand*/
                tsidsArr.push(tsid);

                var imCheck = self.checkForWPACreation(wpid, tsid, tsName, tsTerminationDate);
                tsPromises.push(imCheck);
            });

            var url = app.api.buildURL("getTsRelatedComm");
            var method = "create";
            var data = {
                ts_Id: tsidsArr
            };

            var callback = {
                success: _.bind(function successCB(res) {
                    if (res.status == "Rejected" && res.tsName != "") {
                        var alltsname = res.tsName.replace(/,/g, '<br>');
                        app.alert.show('Comm_record_check_pop_up', {
                            level: 'confirmation',
                            messages: 'This animal was previously rejected.  Are you sure you want to assign? <br><b>' + alltsname + '</b>',
                            autoClose: false,
                            confirm: {
                                label: "Yes"
                            },
                            cancel: {
                                label: "No"
                            },
                            onCancel: function () {
                                return;
                            },
                            onConfirm: function () {
                                //alert('continue');
                                Promise.all(tsPromises).then(function (results) {
                                    //console.log(animalIdsToSend);
                                    if (awpErrorMsg.length > 0) {
                                        /*app.alert.show("NotAlloted_WP", {
                                            level: "error",
                                            messages: "Allocated Work Product is not null for "+awpErrorMsg+". Please clear the Allocated Work Product before creating a WPA."
                                        });*/
                                    }
                                    if (tdErrorMsg.length > 0) {
                                        app.alert.show("NotTerminationDate", {
                                            level: "error",
                                            //messages: "Test System ("+tdErrorMsg+") is deceased (Termination Date is not null). WPA cannot be created."
                                            messages: "Some of Test System either blank or deceased. WPA cannot be created."
                                        });
                                        app.drawer.close();
                                    }
                                    // console.log("NotToSend: ", animalIdsNotToSend);
                                    // console.log("ToSend: ", animalIdsToSend);
                                    if (animalIdsToSend.length > 0) {
                                        //  Get this model's values
                                        var batchid = self.model.get('bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida');
                                        console.log("batchid====>",batchid);
                                        var valuesToSave = {};
                                        var fieldsToIterate = self.meta.panels[1].fields;
                                        _.each(fieldsToIterate, function (field) {
                                            valuesToSave[field.name] = (_.isUndefined(self.model.get(field.name))) ? '' : self.model.get(field.name);
                                        });
                                        valuesToSave['m03_work_p7d13product_ida'] = (_.isUndefined(self.model.get('m03_work_p7d13product_ida'))) ? '' : self.model.get('m03_work_p7d13product_ida');
                                        valuesToSave['m03_work_p9f23product_ida'] = (_.isUndefined(self.model.get('m03_work_p9f23product_ida'))) ? '' : self.model.get('m03_work_p9f23product_ida');
                                        valuesToSave['bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida'] = (_.isUndefined(self.model.get('bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida'))) ? '' : self.model.get('bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida');
                                        //alert(valuesToSave['m03_work_p9f23product_ida']);
                                        var dataToSend = { ids: animalIdsToSend, values: valuesToSave };
                                        console.log(dataToSend);
                                        self.saveWPEs(dataToSend);
                                    } else {
                                        app.alert.show("NotSexMatchAlert", {
                                            level: "error",
                                            //messages: "Test System ("+tdErrorMsg+") is deceased (Termination Date is not null). WPA cannot be created."
                                            messages: "Sex of chosen Animal(s) not approved per Work Product's Test System Design(s).  Please review."
                                        });
                                        // app.drawer.close();
                                    }
                                }.bind(this))
                            }
                        });
                    } else {
                        Promise.all(tsPromises).then(function (results) {
                            //console.log(animalIdsToSend);
                            if (awpErrorMsg.length > 0) {
                                /*app.alert.show("NotAlloted_WP", {
                                    level: "error",
                                    messages: "Allocated Work Product is not null for "+awpErrorMsg+". Please clear the Allocated Work Product before creating a WPA."
                                });*/
                            }
                            if (tdErrorMsg.length > 0) {
                                app.alert.show("NotTerminationDate", {
                                    level: "error",
                                    //messages: "Test System ("+tdErrorMsg+") is deceased (Termination Date is not null). WPA cannot be created."
                                    messages: "Some of Test System either blank or deceased. WPA cannot be created."
                                });
                                app.drawer.close();
                            }
                            // console.log("NotToSend: ", animalIdsNotToSend);
                            // console.log("ToSend: ", animalIdsToSend);
                            if (animalIdsToSend.length > 0) {
                                //  Get this model's values
                                var batchid = self.model.get('bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida');
                                console.log("batchid====>",batchid);
                                var valuesToSave = {};
                                var fieldsToIterate = self.meta.panels[1].fields;
                                _.each(fieldsToIterate, function (field) {
                                    valuesToSave[field.name] = (_.isUndefined(self.model.get(field.name))) ? '' : self.model.get(field.name);
                                });
                                valuesToSave['m03_work_p7d13product_ida'] = (_.isUndefined(self.model.get('m03_work_p7d13product_ida'))) ? '' : self.model.get('m03_work_p7d13product_ida');
                                valuesToSave['m03_work_p9f23product_ida'] = (_.isUndefined(self.model.get('m03_work_p9f23product_ida'))) ? '' : self.model.get('m03_work_p9f23product_ida');
                                valuesToSave['bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida'] = (_.isUndefined(self.model.get('bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida'))) ? '' : self.model.get('bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida');
                                //alert(valuesToSave['m03_work_p9f23product_ida']);
                                var dataToSend = { ids: animalIdsToSend, values: valuesToSave };
                                console.log(dataToSend);
                                self.saveWPEs(dataToSend);
                            } else {
                                app.alert.show("NotSexMatchAlert", {
                                    level: "error",
                                    //messages: "Test System ("+tdErrorMsg+") is deceased (Termination Date is not null). WPA cannot be created."
                                    messages: "Sex of chosen Animal(s) not approved per Work Product's Test System Design(s).  Please review."
                                });
                                // app.drawer.close();
                            }
                        }.bind(this))
                    }
                }, this)
            };
            app.api.call(method, url, data, callback);


        }
    },
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    checkForWPACreation: function (wpid, tsid, tsName, tsTerminationDate) {
        return new Promise(function (resolve, reject) {
            if (tsName != "" && tsTerminationDate == "") {

                app.api.call("GET", app.api.buildURL("M03_Work_Product/" + wpid + "/" + tsid + "/get_test_system_design"), {
                }, {
                    success: function (data) {
                        // console.log("98");

                        // console.log(JSON.stringify(data));
                        // return;
                        if (data == 0) {
                            allowCreation = 0;
                            animalIdsNotToSend.push(tsName);
                            resolve({
                                notAllowedTS: tsid
                            })
                        } else {
                            allowCreation = 1;
                            console.log("110");
                            animalIdsToSend.push(tsid);
                            resolve({
                                allowedTS: tsid
                            })
                            //   animalIdsToSend.push(selectedModel.get('id'));
                        }
                    },
                    error: function (err) {
                        allowCreation = 1;
                        resolve({
                            allowedTS: tsid
                        })
                    }
                });
            } else {
                resolve({
                    // tdErrorMsg.push(tsName);
                    tdErrorMsg: tsName
                })
            }
        });
    },

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    saveWPEs: function (dataToSend) {
        var self = this;
        if (animalIdsNotToSend.length > 0) {
            app.alert.show("NotSexMatchAlert", {
                level: "error",
                //messages: "Test System ("+tdErrorMsg+") is deceased (Termination Date is not null). WPA cannot be created."
                messages: "Sex of chosen Animal(s) not approved per Work Product's Test System Design(s).  Please review."
            });
            return;
        } else {
            self.toggleButtons(false);
        }
        //  Building URL
        var url = app.api.buildURL('wpe_mass_create/');
        //  Calling to save with Parameters/data.
        app.alert.show("wpeMassCreation", {
            level: 'process',
            title: "Saving",
            autoClose: false,
        });
        app.api.call('create', url, { data: dataToSend }, {
            success: function (serverData) {
                self.toggleButtons(true);
                if (serverData.status) {
                    var messageLevel = 'success';
                } else {
                    var messageLevel = 'error';
                }
                app.alert.dismiss('wpeMassCreation');
                app.alert.show("wpeMassCreation", {
                    level: messageLevel,
                    messages: serverData.Message,
                    autoClose: false,
                    //                    autoCloseDelay: 5000
                });
                app.drawer.close();
            },
            error: function (serverData) {
                self.toggleButtons(true);
                app.alert.dismiss('wpeMassCreation');
                // console.log("In Error wpeMassCreation ", serverData);
                app.alert.show("wpeMassCreation", {
                    level: 'error',
                    messages: 'Saving failed',
                    autoClose: false,
                });
            },
        });
    }
})
