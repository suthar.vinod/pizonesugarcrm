<?php
// created: 2022-03-08 05:49:12
$viewdefs['WPE_Work_Product_Enrollment']['base']['view']['subpanel-for-anml_animals-anml_animals_wpe_work_product_enrollment_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'm03_work_product_wpe_work_product_enrollment_1_name',
          'label' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_M03_WORK_PRODUCT_TITLE',
          'enabled' => true,
          'id' => 'M03_WORK_P7D13PRODUCT_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'enrollment_status_c',
          'label' => 'LBL_ENROLLMENT_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'gd_group_design_wpe_work_product_enrollment_1_name',
          'label' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_NAME_FIELD_TITLE',
          'enabled' => true,
          'id' => 'GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1GD_GROUP_DESIGN_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'comments_c',
          'label' => 'LBL_COMMENTS',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'usda_category_c',
          'label' => 'LBL_USDA_CATEGORY',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'm03_work_product_wpe_work_product_enrollment_2_name',
          'label' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_M03_WORK_PRODUCT_TITLE',
          'enabled' => true,
          'id' => 'M03_WORK_P9F23PRODUCT_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'activities_c',
          'label' => 'LBL_ACTIVITIES',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);