({
  extendsFrom: 'CreateView',
  initialize: function (options) {

    this._super('initialize', [options]);
    this.wpRenderEvent();
    //this.model.on('change:m03_work_product_wpe_work_product_enrollment_1_name', this.function_usda, this);
    this.on('render', _.bind(this.onload_function, this));
  },
  onload_function: function () {
    var self = this;
    var WPE_Status_List = app.lang.getAppListStrings('enrollment_status_list');
    //console.log('WPE_Status_List',WPE_Status_List);
    Object.keys(WPE_Status_List).forEach(function (key) {
      delete WPE_Status_List[key];
    });
    //WPE_Status_List[''] = "";
    WPE_Status_List['On Study'] = "On Study";
    this.model.fields['enrollment_status_c'].options = WPE_Status_List;
    this.model.set('enrollment_status_c', "On Study");
    this._render();
  },

  wpRenderEvent: function () {
    var wpid = this.model.get('m03_work_p7d13product_ida');
    if (wpid) {
      var self = this;
      App.api.call("get", "rest/v10/M03_Work_Product/" + wpid + "?fields=usda_category_multiselect_c", null, {
        success: function (Data) {
          if (Data.usda_category_multiselect_c != "") {
            var ch1 = '';
            var catArr = Data.usda_category_multiselect_c;
            catArr.sort();
            ch = catArr[catArr.length - 1];
            res = ch.replace("^", "");
            ch1 = res.replace("^", "");
            self.model.set('usda_category_c', ch1);
          } else {
            self.model.set('usda_category_c', "B");
          }
        }
      });
    }
  },
  setUSDACategory: function () {
    var batchId = this.model.get("bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida");
    var wpid = this.model.get('m03_work_p7d13product_ida');
    // console.log("batchId===>", batchId);
    if ((wpid == "" || wpid == null) && (batchId != "" || batchId != null)) {
      var url = app.api.buildURL("getUSDAHighestLetterFromWP");
      var method = "create";
      var data = {
        module: 'BID_Batch_ID',
        batchId: batchId
      };
      var callback = {
        success: _.bind(function successCB(res) {
          // console.log("Respoonse===>", res);
          this.model.set("usda_category_c", res);
        }, this)
      };
      app.api.call(method, url, data, callback);
    }
  },
  function_usda: function () {
    if (this.model.get('m03_work_product_wpe_work_product_enrollment_1_name') != "") {

      var wpid = this.model.get('m03_work_p7d13product_ida');
      var self = this;
      App.api.call("get", "rest/v10/M03_Work_Product/" + wpid + "?fields=usda_category_multiselect_c", null, {
        success: function (Data) {

          if (Data.usda_category_multiselect_c != "") {
            var ch1 = '';
            var catArr = Data.usda_category_multiselect_c;
            catArr.sort();
            ch = catArr[catArr.length - 1];
            res = ch.replace("^", "");
            ch1 = res.replace("^", "");
            self.model.set('usda_category_c', ch1);
          } else {
            self.model.set('usda_category_c', "B");
          }
        }
      });

      //selectedProductId = model.changed.m03_work_product_wpe_work_product_enrollment_1.id;
      app.api.call("GET", app.api.buildURL("M03_Work_Product/" + wpid + "/get_blanket_product"), null, {
        success: function (data) {
          self.model.set('m03_work_p9f23product_ida', data.workID);
          self.model.set('m03_work_product_wpe_work_product_enrollment_2_name', data.blanket_protocol_c);
        },
        error: function (err) {
          // console.log(err);
        }
      });
    } else {
      this.model.set('m03_work_p9f23product_ida', null);
      this.model.set('m03_work_product_wpe_work_product_enrollment_2_name', null);
    }
  },

})