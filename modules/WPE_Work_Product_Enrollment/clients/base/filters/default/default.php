<?php
// created: 2022-04-26 11:46:27
$viewdefs['WPE_Work_Product_Enrollment']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'activities_c' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'bid_batch_id_wpe_work_product_enrollment_1_name' => 
    array (
    ),
    'm03_work_product_wpe_work_product_enrollment_2_name' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'gd_group_design_wpe_work_product_enrollment_1_name' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'enrollment_status_c' => 
    array (
    ),
    'anml_animals_wpe_work_product_enrollment_1_name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'usda_category_c' => 
    array (
    ),
    'usda_id_c' => 
    array (
    ),
    'm03_work_product_wpe_work_product_enrollment_1_name' => 
    array (
    ),
    'name' => 
    array (
    ),
  ),
);