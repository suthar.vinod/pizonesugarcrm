({
    extendsFrom: 'RelateField',
    initialize: function(options) {
        this._super('initialize', [options]);
        $('select').select2({
            minimumResultsForSearch: -1
        });

    },
    openSelectDrawer: function() {

        if (this.getSearchModule() == "M03_Work_Product" && this.def.name == "m03_work_product_wpe_work_product_enrollment_1_name") {
            var self = this;
            setTimeout(function() {
                var tsid = self.model.get('anml_animals_wpe_work_product_enrollment_1anml_animals_ida');
                //alert(tsid);
                if (tsid != undefined) {
                    var APIURL = "rest/v10/ANML_Animals/" + tsid + "/link/anml_animals_wpe_work_product_enrollment_1?erased_fields=true&max_num=1&order_by=date_entered:desc";
                    var lastwp = "";
                    var lastStatus = "";
                    console.log('APIURL', APIURL);
                    App.api.call("get", APIURL, null, {
                        success: function(Data) {
                            console.log('WPE Last Record Data :', Data);
                            if (Data.records.length > 0) {
                                $.each(Data.records, function(key, item) {
                                    var lastwp = item.m03_work_product_wpe_work_product_enrollment_1_name;
                                    var lastwp_id = item.m03_work_p7d13product_ida;
                                    var lastStatus = item.enrollment_status_c;
                                    if (lastwp == "APS001-AH01") {
                                        console.log('selectWorkProductPhase');
                                        self.selectWorkProductPhase();
                                    } else if (lastwp != "APS001-AH01" && lastStatus == 'On Study Transferred') {
                                        console.log('selectWorkProductPhaseBySponsor');
                                        self.selectWorkProductPhaseBySponsor(lastwp_id);
                                    }
                                    /*751 : 05 Jan 2021 : gsingh */
                                    else if (lastwp == "APS177-AH15") {
                                        console.log('selectWorkProductPhaseNotAH15');
                                        self.selectWorkProductPhaseNotAH15();
                                    } else if ((lastwp != "APS001-AH01" || lastwp != "APS177-AH15") && (lastStatus == 'Not Used')) {

                                        console.log('selectWorkProductPhaseLastWP_NotAH01');
                                        //self.selectWorkProductPhaseLastWP_NotAH01();
                                        /*#751 : New changes */
                                        self.selectWorkProductPhaseLastWP_NotAH01(lastwp_id);

                                    }
                                    /*#751 : New changes */
                                    else if ((lastwp != "APS001-AH01" || lastwp != "APS177-AH15") && (lastStatus == 'On Study')) {

                                        console.log('selectWorkProductPhaseLastWP_NotAH01_OnStudy');
                                        self.selectWorkProductPhaseLastWP_NotAH01_OnStudy(lastwp_id);
                                    }
                                    /////***********/////
                                    else {
                                        console.log('selectWorkProductPhase2');
                                        self.selectWorkProductPhase2();
                                    }
                                    $("button.btn.btn-invisible.btn-dark").css("display", "none");
                                    $(".filter-definition-container").css('pointer-events', 'none');
                                    $(".choice-filter-clickable").css('pointer-events', 'none');
                                });
                            } else {
                                self.selectWorkProductPhase2();
                                $("button.btn.btn-invisible.btn-dark").css("display", "none");
                                $(".filter-definition-container").css('pointer-events', 'none');
                                $(".choice-filter-clickable").css('pointer-events', 'none');
                            }

                        }
                    });
                } else {
                    //self.selectWorkProductPhase2();
                    if (self.context.parent) {
                        var awpArr1 = [];
                        var awpArr2 = [];
                        var awpArr3 = [];
                        var awpArr4 = [];
                        var awpArr20 = [];

                        var tsAllocatedWPArr = [];
                        var totalCount = 0;

                        var models = self.context.parent.get('mass_collection').models;
                        //console.log('model===>',models);
                        _.each(models, function(selectedModel) {

                            console.log(selectedModel);
                            var tsAssignedWP = selectedModel.get('assigned_to_wp_c');
                            var tsAllocatedWP = selectedModel.get('allocated_work_product_c');
                            var tsEnrollmentStatus = selectedModel.get('enrollment_status');


                            if (tsAssignedWP == "APS001-AH01") {

                                if ((tsAllocatedWP == undefined || tsAllocatedWP == "")) {
                                    awpArr1.push(tsAssignedWP);
                                } else {
                                    tsAllocatedWPArr.push(tsAllocatedWP);
                                    awpArr4.push(tsAllocatedWP);
                                }
                            } else if (tsAssignedWP != "APS001-AH01" && tsEnrollmentStatus == "On Study Transferred") {
                                awpArr20.push(tsAssignedWP);
                            } else if (tsAssignedWP != "APS001-AH01" && tsEnrollmentStatus == "Not Used") {
                                awpArr2.push(tsAssignedWP);
                            } else {
                                awpArr3.push(tsAssignedWP);
                            }

                            totalCount++;
                        });

                        if (awpArr1.every((val, i, arr) => val === "APS001-AH01") && awpArr1.length == totalCount) {
                            self.selectWorkProductPhase();
                        } else if (awpArr20.every((val, i, arr) => val === arr[0]) && awpArr20.length == totalCount) {
                            var uniqueArr = awpArr20.filter(function(itm, i, awpArr20) {
                                return i == awpArr20.indexOf(itm);
                            });
                            var cwpName = uniqueArr.toString();
                            self.selectWorkProductStatusTranferred(cwpName);
                        } else if (awpArr2.every((val, i, arr) => val === arr[0]) && awpArr2.length == totalCount) {
                            //self.selectWorkProductPhaseLastWP_NotAH01();
                            self.selectWorkProductPhaseLastWP_NotAH01_Masscreate(awpArr2[0]);
                        } else if (awpArr3.every((val, i, arr) => val === arr[0]) && awpArr3.length == totalCount) {
                            self.selectWorkProductPhase();
                        } else if (awpArr4.every((val, i, arr) => val === arr[0])) {
                            self.selectSameWorkProduct(awpArr4[0]);
                        } else {
                            self.selectWorkProductPhase2();
                        } // true

                        $("button.btn.btn-invisible.btn-dark").css("display", "none");
                        $(".filter-definition-container").css('pointer-events', 'none');
                        $(".choice-filter-clickable").css('pointer-events', 'none');
                    } else {
                        //alert("Select click");
                        self.selectWorkProductDefault();
                    }
                }

            }, 500);

        } else if (this.getSearchModule() == "M03_Work_Product" && this.def.name == "m03_work_product_wpe_work_product_enrollment_2_name") {
            this.selectWorkProductCode();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else if (this.getSearchModule() == "GD_Group_Design" && this.model.get("m03_work_product_wpe_work_product_enrollment_1_name") != '' && this.model.get("m03_work_product_wpe_work_product_enrollment_1_name") != undefined) {
            console.log('in wpa gd field filter line 150');
            console.log('in wpa gd field filter line 151',this.model.get("m03_work_product_wpe_work_product_enrollment_1_name"));
            this.selectWorkProductGD();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        }
        /**01 Aug 2022 : #2691 : Update Group Design filter on WPAs (TDs for Batch IDs) */
        else if (this.getSearchModule() == "GD_Group_Design" && this.model.get("bid_batch_id_wpe_work_product_enrollment_1_name") != '' && this.model.get("bid_batch_id_wpe_work_product_enrollment_1_name") != undefined) {
            console.log('in wpa bd field filter line 158');
            this.selectBatchIDGD();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        }
        /** **************EOC of #2691 ***************** */
        else if (this.getSearchModule() == "BID_Batch_ID") {
            console.log('in wpa batchid field filter line 156');
            this.selectBID_Batch_ID();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
            $(".filter .filter-view .search-filter").css('pointer-events', 'none');
            $('.filter-definition-container input[value="status_c"]').prop('disabled', true);
            $('.filter-definition-container input[name="filter_row_name"]').prop('disabled', true);
            $('.filter-definition-container input[name="filter_row_operator"]').prop('disabled', true);
            $('.filter-definition-container input[name="status_c"]').prop('disabled', true);
        } else {
            console.log('in else of wpa gd field filter line 167');
            this._super('openSelectDrawer');
            $("button.btn.btn-invisible.btn-dark").css("display", "block");
            $(".filter-definition-container").css('pointer-events', 'unset');
            $(".choice-filter-clickable").css('pointer-events', 'unset');
            $(".filter .filter-view .search-filter").css('pointer-events', 'unset');
            $('.filter-definition-container input[name="filter_row_name"]').prop('disabled', false);
            $('.filter-definition-container input[name="filter_row_operator"]').prop('disabled', false);
            $('.filter-definition-container input[name="status_c"]').prop('disabled', false);
        }
    },

    selectBID_Batch_ID: function () {
        var self = this;
        var filterOptions = new app.utils.FilterOptions().config({
            'initial_filter': 'FilterBID_Batch_IDStatus',
            'initial_filter_label': 'Batch ID Filter',
            'filter_populate': {'status_c': ['Testing','Open']},
        })
               .format();
        filterOptions = (this.getSearchModule() == "BID_Batch_ID") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },

    selectWorkProductPhaseLastWP_NotAH01_Masscreate: function (lastwp_name) {
        var lastwp_name = lastwp_name;
        
        var url = app.api.buildURL("getWPDataByName");
        var method = "create";
        var data = {
            module: 'M03_Work_Product',
            lastwp_name: lastwp_name
        };

        var callback = {
            success: _.bind(function successCB(Data) {
                console.log('WP_CodeAPI Data :', Data);
                var WPCode = Data;
                this.selectWorkProductPhaseLastWP_NotAH01Filter(WPCode);
            }, this)
        };
        app.api.call(method, url, data, callback);

    },

    selectWorkProductStatusTranferred: function(currentwp_name) {
        var WP_SponsorAPI = "rest/v10/M03_Work_Product?fields=accounts_m03_work_product_1_name,work_product_code_2_c&max_num=1&order_by=date_entered:desc&filter[0][name][$starts]=" + currentwp_name;
        var self = this;
        App.api.call("get", WP_SponsorAPI, null, {
            success: function(Data) {
                if (Data.records.length > 0) {
                    var sponsorname = Data.records[0].accounts_m03_work_product_1_name;
                    var sponsorid = Data.records[0].accounts_m03_work_product_1.id;
                    var WPCode = Data.records[0].work_product_code_2_c;
                    var wp_id = Data.records[0].id;

                    setTimeout(function() {
                        self.selectWorkProductStatusTranferredTemplate(sponsorid, sponsorname, WPCode, wp_id);
                    }, 300);

                }
            }
        });

    },

    selectWorkProductStatusTranferredTemplate: function(sponsorid, sponsorname, WPCode, wp_id) {
        var TemplateName = '';
        var Name = '';
        if (WPCode == 'ST01' || WPCode == 'ST01a' || WPCode == 'ST02' || WPCode == 'ST09') {
            TemplateName = 'FilterWorkProductPhaseTemplateTestSystemAPS';
            Name = 'APS177-AH15';
        } else {
            TemplateName = 'FilterWorkProductPhaseTemplateTestSystems_In_APS';
            Name = 'APS001-AH01';
        }
        var phase = 'Testing';
        var self = this;
        var filterOptions = new app.utils.FilterOptions().config({
                'initial_filter': TemplateName,
                'initial_filter_label': 'Work Product Phase Testing',
                //'filter_populate': {'work_product_status_c': [phase]}, 
                'filter_populate': { 'work_product_status_c': [phase], 'iacuc_closure_date_c': '0', 'accounts_m03_work_product_1_name': [sponsorid] },
            })
            //.populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "M03_Work_Product") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
        $("button.btn.btn-invisible.btn-dark").css("display", "none");
        $(".filter-definition-container").css('pointer-events', 'none');
        $(".choice-filter-clickable").css('pointer-events', 'none');
    },

    selectWorkProductPhase: function() {
        var phase = 'Testing';
        var self = this;
        var filterOptions = new app.utils.FilterOptions().config({
                'initial_filter': 'FilterWorkProductPhaseTemplate',
                'initial_filter_label': 'Work Product Phase(Testing)',
                'filter_populate': { 'work_product_status_c': [phase], 'iacuc_closure_date_c': '0', 'name': 'APS001-AH01' },
            })
            //.populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "M03_Work_Product") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    /* 751 : 05 Jan 2021 : Gsingh */
    selectWorkProductPhaseNotAH15: function() {
        var phase = 'Testing';
        var self = this;
        var filterOptions = new app.utils.FilterOptions().config({
                'initial_filter': 'FilterWorkProductPhaseTemplateNotAH15',
                'initial_filter_label': 'Work Product Phase(Testing)',
                'filter_populate': { 'work_product_status_c': [phase], 'iacuc_closure_date_c': '0', 'name': 'APS177-AH15' },
            })
            //.populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "M03_Work_Product") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
        $("button.btn.btn-invisible.btn-dark").css("display", "none");
        $(".filter-definition-container").css('pointer-events', 'none');
        $(".choice-filter-clickable").css('pointer-events', 'none');
    },

    /*Ticket#725 Should search only selected work product */
    selectSameWorkProduct: function(wpid) {
        var phase = 'Testing';
        var WP = wpid;
        var self = this;
        var filterOptions = new app.utils.FilterOptions().config({
            'initial_filter': 'FilterSameWorkProductPhaseTemplate',
            'initial_filter_label': 'Filter Same Work Product',
            'filter_populate': { 'work_product_status_c': [phase], 'iacuc_closure_date_c': '0', 'name': WP },
        }).format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "M03_Work_Product") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    /*577 : 10 Nov 2020 :Work Product Assignment - Status/Controls on record creation  */
    selectWorkProductDefault: function() {
        var phase = 'Testing';
        var self = this;
        var filterOptions = new app.utils.FilterOptions().config({
            'initial_filter': '',
            'initial_filter_label': '',
            'filter_populate': {},
        }).format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "M03_Work_Product") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    _getWorkProductDefault: function() {
        return [{}];
    },
    selectWorkProductPhase2: function() {
        var phase = 'Testing';
        var self = this;
        var filterOptions = new app.utils.FilterOptions().config({
                'initial_filter': 'FilterWorkProductPhaseTemplateNotAH01',
                'initial_filter_label': 'Work Product Phase(Testing)',
                //'filter_populate': {'work_product_status_c': [phase]}, 
                'filter_populate': { 'work_product_status_c': [phase], 'iacuc_closure_date_c': '0' },
            })
            //.populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "M03_Work_Product") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
        $("button.btn.btn-invisible.btn-dark").css("display", "none");
        $(".filter-definition-container").css('pointer-events', 'none');
        $(".choice-filter-clickable").css('pointer-events', 'none');
    },

    selectWorkProductPhaseLastWP_NotAH01: function(lastwp_id) {
        var lastwp_id = lastwp_id;
        console.log('selectWorkProductPhaseBySponsor lastwp_id :', lastwp_id);
        var WP_CodeAPI = "rest/v10/M03_Work_Product/" + lastwp_id + "?fields=work_product_code_2_c&max_num=1&order_by=date_entered:desc";
        var self = this;
        console.log('WP_Code API:', WP_CodeAPI);
        App.api.call("get", WP_CodeAPI, null, {
            success: function(Data) {
                console.log('WP_CodeAPI Data :', Data);
                var WPCode = Data.work_product_code_2_c;
                console.log('WPCode name :', WPCode);
                self.selectWorkProductPhaseLastWP_NotAH01Filter(WPCode);

            }
        });

    },
    selectWorkProductPhaseLastWP_NotAH01Filter: function(WPCode) {
        var WPCode = WPCode;
        console.log('WPCode new :', WPCode);
        var phase = 'Testing';
        var TemplateName = '';
        var self = this;
        if (WPCode == 'ST01' || WPCode == 'ST01a' || WPCode == 'ST02' || WPCode == 'ST09') {
            TemplateName = 'FilterWorkProductPhaseTemplateLastNotAH01_In_AH15';
        } else {
            TemplateName = 'FilterWorkProductPhaseTemplateLastNotAH01';
        }
        var filterOptions = new app.utils.FilterOptions().config({

                'initial_filter': TemplateName,
                'initial_filter_label': 'Work Product Phase(Testing)',
                //'filter_populate': {'work_product_status_c': [phase]}, 
                'filter_populate': { 'work_product_status_c': [phase], 'iacuc_closure_date_c': '0', 'name': 'APS001-AH01' },
            })
            //.populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "M03_Work_Product") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
        $("button.btn.btn-invisible.btn-dark").css("display", "none");
        $(".filter-definition-container").css('pointer-events', 'none');
        $(".choice-filter-clickable").css('pointer-events', 'none');
    },
    /*751 : the WPC on the WP is ST01, ST01a, ST02, or ST09.  In these cases, allow a new WPA record to be created filtering WP = APS177-AH15. */
    selectWorkProductPhaseLastWP_NotAH01_OnStudy: function(lastwp_id) {
        var lastwp_id = lastwp_id;
        console.log('selectWorkProductPhaseLastWP_NotAH01_OnStudy lastwp_id :', lastwp_id);
        var WP_CodeAPI = "rest/v10/M03_Work_Product/" + lastwp_id + "?fields=work_product_code_2_c&max_num=1&order_by=date_entered:desc";
        var self = this;
        console.log('selectWorkProductPhaseLastWP_NotAH01_OnStudy WP_Code API:', WP_CodeAPI);
        App.api.call("get", WP_CodeAPI, null, {
            success: function(Data) {
                console.log('selectWorkProductPhaseLastWP_NotAH01_OnStudy WP_CodeAPI Data :', Data);
                var WPCode = Data.work_product_code_2_c;
                console.log('selectWorkProductPhaseLastWP_NotAH01_OnStudy WPCode name :', WPCode);
                self.selectWorkProductPhaseLastWP_NotAH01Filter_OnStudy(WPCode);

            }
        });

    },
    selectWorkProductPhaseLastWP_NotAH01Filter_OnStudy: function(WPCode) {
        var WPCode = WPCode;
        console.log('selectWorkProductPhaseLastWP_NotAH01_OnStudy WPCode new :', WPCode);
        var phase = 'Testing';
        var TemplateName = '';
        var self = this;
        if (WPCode == 'ST01' || WPCode == 'ST01a' || WPCode == 'ST02' || WPCode == 'ST09') {
            TemplateName = 'FilterWorkProductPhaseTemplateLastNotAH01_OnStudy_In_AH15';
        } else {
            TemplateName = 'FilterWorkProductPhaseTemplateNotAH01';
        }
        var filterOptions = new app.utils.FilterOptions().config({
                'initial_filter': TemplateName,
                //'initial_filter': 'FilterWorkProductPhaseTemplateLastNotAH01_OnStudy_In_AH15',
                'initial_filter_label': 'Work Product Phase(Testing)',
                //'filter_populate': {'work_product_status_c': [phase]}, 
                'filter_populate': { 'work_product_status_c': [phase], 'iacuc_closure_date_c': '0', 'name': 'APS177-AH15' },
            })
            //.populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "M03_Work_Product") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
        $("button.btn.btn-invisible.btn-dark").css("display", "none");
        $(".filter-definition-container").css('pointer-events', 'none');
        $(".choice-filter-clickable").css('pointer-events', 'none');
    },
    /*///////////// */
    selectWorkProductPhaseBySponsor: function(lastwp_id) {
        var lastwp_id = lastwp_id;
        console.log('selectWorkProductPhaseBySponsor lastwp_id :', lastwp_id);
        var WP_SponsorAPI = "rest/v10/M03_Work_Product/" + lastwp_id + "?fields=accounts_m03_work_product_1_name,work_product_code_2_c&max_num=1&order_by=date_entered:desc";
        var self = this;
        console.log('WP_Sponsor:', WP_SponsorAPI);
        App.api.call("get", WP_SponsorAPI, null, {
            success: function(Data) {
                console.log('WP_SponsorAPI Data :', Data);
                var sponsorname = Data.accounts_m03_work_product_1.id;
                var WPCode = Data.work_product_code_2_c;
                console.log('sponsor name :', sponsorname);
                console.log('WPCode :', WPCode);
                self.selectWorkProductPhase_new(sponsorname, WPCode);

            }
        });

    },
    selectWorkProductPhase_new: function(sponsorname, WPCode) {
        var sponsorname = sponsorname;
        var WPCode = WPCode;
        var TemplateName = '';
        var Name = '';
        console.log('sponsor name new :', sponsorname);
        console.log('WPCode name new :', WPCode);
        if (WPCode == 'ST01' || WPCode == 'ST01a' || WPCode == 'ST02' || WPCode == 'ST09') {
            TemplateName = 'FilterWorkProductPhaseTemplateTestSystems_In_AH15';
            Name = 'APS001-AH01';
        } else {
            TemplateName = 'FilterWorkProductPhaseTemplateTestSystem';
            Name = 'APS177-AH15';
        }
        var phase = 'Testing';
        var self = this;
        var filterOptions = new app.utils.FilterOptions().config({
                'initial_filter': TemplateName,
                'initial_filter_label': 'Work Product Phase(Testing)',
                //'filter_populate': {'work_product_status_c': [phase]}, 
                'filter_populate': { 'work_product_status_c': [phase], 'iacuc_closure_date_c': '0', 'accounts_m03_work_product_1_name': [sponsorname], 'name': [Name] },
            })
            //.populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "M03_Work_Product") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
        $("button.btn.btn-invisible.btn-dark").css("display", "none");
        $(".filter-definition-container").css('pointer-events', 'none');
        $(".choice-filter-clickable").css('pointer-events', 'none');
    },

    selectWorkProductCode: function() {
        var phase = 'Testing';
        var filterOptions = new app.utils.FilterOptions().config({
                'initial_filter': 'FilterWorkProductCodeTemplate',
                'initial_filter_label': 'Work Product Code',
                'filter_populate': { 'm03_work_product_code_id1_c': '' },
            })
            //.populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "M03_Work_Product") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    selectWorkProductGD: function() {
        var wp_id = this.model.get("m03_work_p7d13product_ida");
		console.log('wp_id==', wp_id);
		var filterOptions = new app.utils.FilterOptions()
            .config({
                //'name' : 'accounts_ca_company_address_1_name',
                'initial_filter': 'FilterWorkProductTemplate',
                'initial_filter_label': 'Work Product',
                'filter_populate': {
                    'm03_work_product_gd_group_design_1_name': [wp_id],
                },
                'filter_relate': {
                    'm03_work_product_gd_group_design_1_name': [wp_id],
                },
            })
            .populateRelate(this.model)
            .format();
			console.log('filterOptions', filterOptions);
			//this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
			filterOptions = (this.getSearchModule() == "GD_Group_Design" && wp_id != null) ? filterOptions : this.getFilterOptions();
			app.drawer.open({
				layout: 'selection-list',
				context: {
					module: this.getSearchModule(),
					fields: this.getSearchFields(),
					filterOptions: filterOptions,
				}
			}, _.bind(this.setValue, this));
			
		
        
    },
    /** 01 Aug 2022 : #2691 :  If Batch ID on the WPA is not null, 
     * Group Designs that have the same Batch ID as the Work Product Assignment ** */
    selectBatchIDGD: function() {
        var batch_id = this.model.get("bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida");
		console.log('batch_id line no 620 ==> ', batch_id);
		var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterBatchIDTemplate',
                'initial_filter_label': 'Batch ID',
                'filter_populate': {
                    'bid_batch_id_gd_group_design_1_name': [batch_id],
                },
                'filter_relate': {
                    'bid_batch_id_gd_group_design_1_name': [batch_id],
                },
            })
            .populateRelate(this.model)
            .format();
			console.log('filterOptions', filterOptions);
			//this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
			filterOptions = (this.getSearchModule() == "GD_Group_Design" && batch_id != null) ? filterOptions : this.getFilterOptions();
			app.drawer.open({
				layout: 'selection-list',
				context: {
					module: this.getSearchModule(),
					fields: this.getSearchFields(),
					filterOptions: filterOptions,
				}
			}, _.bind(this.setValue, this));
			
		
        
    },
    /************* EOC #2691  GD field filter on batch id selection of WPA ************* */
    /*To disable search text box filter of Test System */
    search: _.debounce(function(query) {
        var term = query.term || '',
            self = this,
            searchModule = this.getSearchModule(),
            params = {},
            limit = self.def.limit || 5,
            relatedModuleField = this.getRelatedModuleField();

        if (query.context) {
            params.offset = this.searchCollection.next_offset;
        }

        //console.log(params);
        if (this.getSearchModule() == "M03_Work_Product") { //alert("dfgfd");

        }
        params.filter = this.buildFilterDefinition(term);
        this.searchCollection.fetch({
            //Don't show alerts for this request
            showAlerts: false,
            update: true,
            remove: _.isUndefined(params.offset),
            fields: this.getSearchFields(),
            context: self,
            params: params,
            //limit: limit,
            success: function(data) {
                var fetch = { results: [], more: data.next_offset > 0, context: data };
                if (fetch.more) {
                    var fieldEl = self.$(self.fieldTag),
                        //For teamset widget, we should specify which index element to be filled in
                        plugin = (fieldEl.length > 1) ? $(fieldEl.get(self.currentIndex)).data("select2") : fieldEl.data("select2"),
                        height = plugin.searchmore.children("li:first").children(":first").outerHeight(),
                        //0.2 makes scroll not to touch the bottom line which avoid fetching next record set
                        maxHeight = height * (limit - .2);
                    plugin.results.css("max-height", maxHeight);
                }
                _.each(data.models, function(model, index) {
                    if (params.offset && index < params.offset) {
                        return;
                    }
                    fetch.results.push({
                        id: model.id,
                        text: model.get(relatedModuleField) + ''
                    });
                });
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback(fetch);
                }
            },
            error: function() {
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback({ results: [] });
                }
                app.logger.error("Unable to fetch the bean collection.");
            }
        });

    }, app.config.requiredElapsed || 500),
    _getCustomFilter1: function() {
        var WP_name = this.model.get("m03_work_product_wpe_work_product_enrollment_1_name");
        console.log('WP_name', WP_name);
        return [{
            '$or': [
                { 'm03_work_product_gd_group_design_1_name': [WP_name] }
            ]
        }];
    },
    _getCustomFilterbatchid: function() {
        var BatchID_name = this.model.get("bid_batch_id_wpe_work_product_enrollment_1_name");
        console.log('Batchid_name', BatchID_name);
        return [{
            '$or': [
                { 'bid_batch_id_gd_group_design_1_name': [BatchID_name] }
            ]
        }];
    },    
    _getWorkProductPhase: function() {
        var phase = 'Testing';
        return [{
            'work_product_status_c': {
                '$in': ['Testing'],
            },
            'iacuc_closure_date_c': {
                '$empty': "",
            },
            'name': {
                '$not_equals': 'APS001-AH01',
            },
            'primary_animals_countdown_c': {
                '$gt': '0',
            },
        }];
    },
    _getWorkProductPhase2: function() {
        var phase = 'Testing';
        return [{
            'work_product_status_c': {
                '$in': ['Testing'],
            },
            'iacuc_closure_date_c': {
                '$empty': "",
            },
            'primary_animals_countdown_c': {
                '$gt': '0',
            },
        }];
    },

    _selectWorkProductPhase_new: function(sponsorname, wpcode) {
        console.log('testgurpret', sponsorname);
        console.log('wpcode value', wpcode);
        var sponsorname = sponsorname;
        var wpcodeVal = wpcode;
        var WpfilterName = '';
        if (wpcodeVal == 'ST01' || wpcodeVal == 'ST01a' || wpcodeVal == 'ST02' || wpcodeVal == 'ST09') {
            var WpfilterName = 'APS001-AH01';
        } else {
            var WpfilterName = 'APS177-AH15';
        }
        return [{
            'work_product_status_c': {
                '$in': ['Testing'],
            },
            'iacuc_closure_date_c': {
                '$empty': "",
            },
            'primary_animals_countdown_c': {
                '$gt': '0',
            },
            'accounts_m03_work_product_1_name': {
                '$in': [sponsorname],
            },
            'name': {
                '$not_equals': WpfilterName,
            },
        }];
    },
    /*Ticket #751 */
    _selectWorkProductPhaseLastWP_NotAH01: function(wpcode) {
        var phase = 'Testing';
        var wpcodeVal = wpcode;
        var WpfilterName = '';
        if (wpcodeVal == 'ST01' || wpcodeVal == 'ST01a' || wpcodeVal == 'ST02' || wpcodeVal == 'ST09') {
            var WpfilterName = 'APS177-AH15';
        } else {
            var WpfilterName = 'APS001-AH01';
        }
        return [{
            'work_product_status_c': {
                '$in': ['Testing'],
            },
            'iacuc_closure_date_c': {
                '$empty': "",
            },
            'name': {
                '$equals': WpfilterName,
            },
            'primary_animals_countdown_c': {
                '$gt': '0',
            },
        }];
    },

    _getWorkProductPhaseNotAH15: function() {
        console.log('_getWorkProductPhaseNotAH15');
        var phase = 'Testing';
        return [{
            '$or': [
                { 'm03_work_product_code_id1_c': '426d29a9-0810-54b0-1834-5788f6505070' },
                { 'm03_work_product_code_id1_c': 'c81c2577-bd7c-16e3-1248-5785ac111a73' },
                { 'm03_work_product_code_id1_c': 'f6b39288-96f1-11e7-b45e-02feb3423f0d' },
                { 'm03_work_product_code_id1_c': '00535bde-96f2-11e7-a240-02feb3423f0d' },
            ],
            'work_product_status_c': {
                '$in': ['Testing'],
            },
            'iacuc_closure_date_c': {
                '$empty': "",
            },
            'name': {
                '$not_equals': 'APS177-AH15',
            },
            'primary_animals_countdown_c': {
                '$gt': '0',
            },

        }];
    },
    _selectWorkProductPhaseLastWP_NotAH01_OnStudy: function(wpcode) {
        var phase = 'Testing';
        var wpcodeVal = wpcode;
        var WpfilterName = '';
        if (wpcodeVal == 'ST01' || wpcodeVal == 'ST01a' || wpcodeVal == 'ST02' || wpcodeVal == 'ST09') {
            var WpfilterName = 'APS177-AH15';
        } else {
            var WpfilterName = '';
        }
        return [{
            'work_product_status_c': {
                '$in': ['Testing'],
            },
            'iacuc_closure_date_c': {
                '$empty': "",
            },
            'name': {
                '$equals': WpfilterName,
            },
            'primary_animals_countdown_c': {
                '$gt': '0',
            },
        }];
    },
    /*/////// EOC : Ticket #751 ////// */

    /*For Ticket #725 */
    _getSameWorkProduct2: function(WP) {
        var phase = 'Testing';
        return [{
            'work_product_status_c': {
                '$in': ['Testing'],
            },
            'iacuc_closure_date_c': {
                '$empty': "",
            },
            'name': {
                '$equals': WP,
            },
            'primary_animals_countdown_c': {
                '$gt': '0',
            },
        }];
    },

    /*1453*/
    _selectWorkProductPhaseLastWPTransferred_NotAH01ABC: function(searchTerm, currentwp_name) {

        return [{
            'work_product_status_c': {
                '$in': ['Testing'],
            },
            'iacuc_closure_date_c': {
                '$empty': "",
            },
            'name': {
                '$equals': 'NANA',
            },
            'primary_animals_countdown_c': {
                '$gt': '0',
            },
            /*'accounts_m03_work_product_1_name': {
                 '$in': [sponsor_name],
             },*/
        }];

    },

    _getWorkProductCode: function() {
        return [{
            '$or': [
                { 'm03_work_product_code_id1_c': '0527341e-d0d4-11e9-8661-06eddc549468' },
                { 'm03_work_product_code_id1_c': '9634c914-fe07-11e6-a663-02feb3423f0d' },
                { 'm03_work_product_code_id1_c': '7c730588-20f7-11eb-b881-069273903c1e' }
            ],
        }];
    },

    _getBID_Batch_ID: function (WP) {
        return [{
                'status_c': {
                    '$in': ['Testing','Open'],
                },
            }];
    },
    /**location_equipment
     * @override
     */
    buildFilterDefinition: function(searchTerm) {
        var parentFilter = this._super('buildFilterDefinition', [searchTerm]);
        if (this.getSearchModule() == "M03_Work_Product" && this.def.name == "m03_work_product_wpe_work_product_enrollment_1_name") {
            var self = this;
            var tsid = this.model.get('anml_animals_wpe_work_product_enrollment_1anml_animals_ida');
            var tsParameter = this.model.get('testsystem_hidden_c');

            // alert(tsid);

            if (tsid != undefined) {
                tsArr = tsParameter.split("$$");
                var lastwp = tsArr[0];
                var lastStatus = tsArr[1];
                var sponsorname = tsArr[2];
                var wpcode = tsArr[3];



                if (lastwp == "APS001-AH01") {
                    console.log('1');
                    return parentFilter.concat(this._getWorkProductPhase());
                } else if (lastwp != "APS001-AH01" && lastStatus == 'On Study Transferred') {
                    console.log('2');
                    return parentFilter.concat(this._selectWorkProductPhase_new(sponsorname, wpcode));
                } else if (lastwp != "APS001-AH01" && lastStatus == 'Not Used') {
                    //return parentFilter.concat(this._selectWorkProductPhaseLastWP_NotAH01());
                    /*751 : New changes : 18 Jan 2021 */
                    console.log('3');
                    return parentFilter.concat(this._selectWorkProductPhaseLastWP_NotAH01(wpcode));
                } else if (lastwp == "APS177-AH15") {
                    console.log('4');
                    //console.log('_getWorkProductPhaseNotAH15 Search',parentFilter.concat(this._getWorkProductPhaseNotAH15()));
                    return parentFilter.concat(this._getWorkProductPhaseNotAH15());
                } else if (lastwp != "APS001-AH01" && lastStatus == 'On Study') {
                    console.log('5');
                    return parentFilter.concat(this._selectWorkProductPhaseLastWP_NotAH01_OnStudy(wpcode));
                } else {
                    //
                    console.log('6');
                    return parentFilter.concat(this._getWorkProductPhase2());
                }


            } else {
                if (self.context.parent) {
                    var awpArr1 = [];
                    var awpArr2 = [];
                    var awpArr3 = [];
                    var awpArr4 = [];
                    var awpArr20 = [];
                    var tsAllocatedWPArr = [];
                    var totalCount = 0;


                    var models = self.context.parent.get('mass_collection').models;
                    ///console.log('model===>',models);
                    _.each(models, function(selectedModel) {
                        var tsAssignedWP = selectedModel.get('assigned_to_wp_c');
                        var tsAllocatedWP = selectedModel.get('allocated_work_product_c');
                        var tsEnrollmentStatus = selectedModel.get('enrollment_status');

                        //alert(tsAllocatedWP);

                        if (tsAssignedWP == "APS001-AH01") {

                            if ((tsAllocatedWP == undefined || tsAllocatedWP == "")) {
                                awpArr1.push(tsAssignedWP);
                            } else {
                                tsAllocatedWPArr.push(tsAllocatedWP);
                                awpArr4.push(tsAllocatedWP);
                            }

                        } else if (tsAssignedWP != "APS001-AH01" && tsEnrollmentStatus == "On Study Transferred") {
                            awpArr20.push(tsAssignedWP);
                        } else if (tsAssignedWP != "APS001-AH01" && tsEnrollmentStatus == "Not Used") {
                            awpArr2.push(tsAssignedWP);
                        } else {
                            awpArr3.push(tsAssignedWP);
                        }

                        totalCount++;
                    });

                    if (awpArr1.every((val, i, arr) => val === "APS001-AH01") && awpArr1.length == totalCount) {
                        //console.log('_getWorkProductPhase 1');
                        return parentFilter.concat(this._getWorkProductPhase());
                    } else if (awpArr20.every((val, i, arr) => val === arr[0]) && awpArr20.length == totalCount) {
                        var uniqueArr = awpArr20.filter(function(itm, i, awpArr20) {
                            return i == awpArr20.indexOf(itm);
                        });
                        var currentwp_name = uniqueArr.toString();

                        return parentFilter.concat(this._selectWorkProductPhaseLastWPTransferred_NotAH01ABC());



                    } else if (awpArr2.every((val, i, arr) => val === arr[0]) && awpArr2.length == totalCount) {
                        //console.log('_selectWorkProductPhaseLastWP_NotAH01 2');
                        return parentFilter.concat(this._selectWorkProductPhaseLastWP_NotAH01());
                    } else if (awpArr3.every((val, i, arr) => val === arr[0]) && awpArr3.length == totalCount) {
                        //console.log('_getWorkProductPhase2 1');
                        return parentFilter.concat(this._getWorkProductPhase2());
                    } else if (awpArr4.every((val, i, arr) => val === arr[0])) {
                        // self.selectSameWorkProduct(awpArr4[0]);
                        return parentFilter.concat(this._getSameWorkProduct2(awpArr4[0]));
                    } else {
                        //console.log('_getWorkProductPhase2 2');
                        return parentFilter.concat(this._getWorkProductPhase2());
                    } // true
                } else {
                    return parentFilter.concat(this._getWorkProductDefault());
                }

            }

        } else if (this.getSearchModule() == "M03_Work_Product" && this.def.name == "m03_work_product_wpe_work_product_enrollment_2_name") {
            return parentFilter.concat(this._getWorkProductCode());
        } else if (this.getSearchModule() == "GD_Group_Design" && this.model.get("m03_work_product_wpe_work_product_enrollment_1_name") != '') {
            return parentFilter.concat(this._getCustomFilter1());
        }else if (this.getSearchModule() == "GD_Group_Design" && this.model.get("bid_batch_id_wpe_work_product_enrollment_1_name") != '' ){
            return parentFilter.concat(this._getCustomFilterbatchid());
        }else if (this.getSearchModule() == "BID_Batch_ID") {
            return parentFilter.concat(this._getBID_Batch_ID());
        } else {
            return parentFilter;
        }
    },
    /**EOC Disable search filter code */
})