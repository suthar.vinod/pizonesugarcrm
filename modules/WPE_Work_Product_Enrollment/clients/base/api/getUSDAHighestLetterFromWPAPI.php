<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class getUSDAHighestLetterFromWPAPI extends SugarApi
{

    public function registerApiRest()
    {
        return array(
            'getUSDAHighestLetterFromWP' => array(
                'reqType'   => 'POST',
                'path'      => array('getUSDAHighestLetterFromWP'),
                'pathVars'  => array('getUSDAHighestLetterFromWP'),
                'method'    => 'getUSDAHighestLetterFromWP',
                'shortHelp' => '',
                'longHelp'  => '',
            ),
        );
    }

    public function getUSDAHighestLetterFromWP($api, $args)
    {
        $batchID = $args['batchId'];
        $module  = $args['module'];
        $batchBean = BeanFactory::getBean($module, $batchID);
        //Load relationship to get the Related WP Ids
        $batchBean->load_relationship("bid_batch_id_m03_work_product_1");
        $linkedWP = $batchBean->bid_batch_id_m03_work_product_1->get();
        // $GLOBALS['log']->fatal("linkedWP====>", print_r($linkedWP, 1));
        foreach ($linkedWP as $relatedWP) {
            $WPBean = BeanFactory::getBean('M03_Work_Product', $relatedWP);
            $USDAVal = $WPBean->usda_category_multiselect_c;
            $trunctedData = str_replace("^", "", $USDAVal);
            $allArrayElements = explode(',',$trunctedData); //after explode $allArrayElements array become multidimensional
            $USDACatgory[] = $allArrayElements;
        }
        // get all multi-dimensional array Elements into single dimension array
        for ($i=0; $i < count($USDACatgory); $i++) { 
            for ($j=0; $j < count($USDACatgory[$i]); $j++) { 
                $getAllCharVal[] = $USDACatgory[$i][$j];
            }
        }
        // $GLOBALS['log']->fatal("without sort====>", print_r($getAllCharVal, 1));
        sort($getAllCharVal);
        // $GLOBALS['log']->fatal("sorted array====>", print_r($getAllCharVal, 1));
        $getUniqueLetter = array_unique($getAllCharVal); // get the different char from the array
        // $GLOBALS['log']->fatal("Unique Elements====>", print_r($getUniqueLetter, 1));
        $lastElement = end($getUniqueLetter);
        return $lastElement;
    }
}
