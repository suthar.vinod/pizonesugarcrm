<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class WPEMassCreateAPI extends SugarApi {

    public function registerApiRest() {
        return array(
            'wpeMassCreate' => array(
                'reqType' => 'POST',
                'path' => array('wpe_mass_create'),
                'pathVars' => array(''),
                'method' => 'wpeMassCreate',
                'shortHelp' => 'Cretes WPEs from the Animals sent in this request',
                'longHelp' => 'Cretes WPEs from the Animals sent in this request',
            ),
        );
    }

    public function wpeMassCreate($api, $args) {
        $response = array();
        $response['status'] = FALSE;
        $response['Message'] = "Saving failed";

        try {
            //  Save it to DB
            $this->_wpeMassCreateToDB($args);

            $response['status'] = TRUE;
            $response['Message'] = "Saved successfully";
        } catch (Exception $exc) {
            $response['status'] = FALSE;
            $response['Message'] = "Saving failed";
        }

        return $response;
    }

    private function _wpeMassCreateToDB($args) {
        try {
            $valuesToSet = $args['data']['values'];
            $ids = $args['data']['ids'];
            $count = 1;
            foreach ($ids as $id) {
                if (!empty($valuesToSet)) {
                    $parentBean = BeanFactory::getBean("ANML_Animals", $id, array('disable_row_level_security' => true));

                    $childBean = BeanFactory::newBean("WPE_Work_Product_Enrollment", null, array('disable_row_level_security' => true));
                    foreach ($valuesToSet as $fieldName => $value) {
                        $childBean->$fieldName = $value;
                    }

                    $childBean->anml_animals_wpe_work_product_enrollment_1anml_animals_ida = $id;
                    $childBean->usda_id_c = $parentBean->usda_id_c;
                    $childBean->save();

                    //  Could we modify the code so that when the animal(s) are mass enrolled in the WP Enrollment module 
                    //  (using the WPE Mass Create), the field [Assigned to WP] in the Animals module would automatically populate 
                    //  with the current Work Product? See attached image. Let me know if you have any questions.
                    $parentBean = BeanFactory::getBean("ANML_Animals", $id, array('disable_row_level_security' => true));
                    if(!empty($childBean->m03_work_product_wpe_work_product_enrollment_1_name)){
                        $parentBean->assigned_to_wp_c = $childBean->m03_work_product_wpe_work_product_enrollment_1_name;
                    }else if(!empty($childBean->bid_batch_id_wpe_work_product_enrollment_1_name)){
                        $parentBean->assigned_to_wp_c = $childBean->bid_batch_id_wpe_work_product_enrollment_1_name;
                    }
                    //  Shahid
                    //  According to the new change request for APS, once the assigned to WP field populated with the 
                    //  current work product, now what the client wants is also to update the 
                    //  current enrollment status in the animals module.
                    $parentBean->enrollment_status = $childBean->enrollment_status_c;
                    $parentBean->save();
                }
                $count++;
            }
        } catch (Exception $exc) {
           
        }
    }

}
