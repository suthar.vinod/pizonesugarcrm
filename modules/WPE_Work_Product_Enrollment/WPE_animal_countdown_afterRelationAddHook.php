<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class WPE_animalCountAfterRelationAddHook{
	function UpdateAnimalCountdownValues($bean, $event, $arguments){
		global $db;
		$workProductId	= "";
		$onStudyCount	= 0;
		$onStudyBackupCount = 0;
		if($bean->id!="" && 1==0){
			$wpe_id =$bean->id; // Work Product Enrollment ID
			//Query to get the Work Product ID	
			$wpe_sql = "SELECT * FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE WHERE WP_WPE.`m03_work_p9bf5ollment_idb`='".$wpe_id."' ORDER BY `WP_WPE`.`date_modified` DESC limit 1";
			//$GLOBALS['log']->fatal($wpe_sql);		
			$wpe_exec = $db->query($wpe_sql);
			if ($wpe_exec->num_rows > 0) 
			{
				$result = $db->fetchByAssoc($wpe_exec);
				$workProductId = $result['m03_work_p7d13product_ida'];
				if($workProductId!="4a529aae-5165-3149-6db7-5702c7b98a5e" && $workProductId!="811ae58e-9281-2755-25d5-5702c7252696")
				{
					//Update Work_Product Bean to update the Animal Countdown Value
					$wp_bean = BeanFactory::getBean('M03_Work_Product', $workProductId);
					//$wp_bean->save();
					///////////////////////////////////
					//////////////////////////////////////////////////////////////////////
					$previousAnimalCount 		= $wp_bean->primary_animals_countdown_c;
					$previousBlanketAnimalCount = $wp_bean->blanket_animals_remaining_c;
					
					//$GLOBALS['log']->fatal('WPE After relationship Add :');
					//$GLOBALS['log']->fatal('previousAnimalCount :' . $previousAnimalCount);
					//$GLOBALS['log']->fatal('previousBlanketAnimalCount :' . $previousBlanketAnimalCount);
					$animalCountValue  = $this->calculateAnimal($wp_bean,$previousAnimalCount,$previousBlanketAnimalCount);
				}
			}

			/* Countdown for Blanket Work Product*/
			
			$wpe_sql = "SELECT * FROM `m03_work_product_wpe_work_product_enrollment_2_c` AS WP_WPE WHERE WP_WPE.`m03_work_p90c4ollment_idb`='".$wpe_id."' ORDER BY `WP_WPE`.`date_modified` DESC limit 1";
			$wpe_exec = $db->query($wpe_sql);
			if ($wpe_exec->num_rows > 0) 
			{
				$result = $db->fetchByAssoc($wpe_exec);
				$workProductId = $result['m03_work_p9f23product_ida'];
				
				//Update Work_Product Bean to update the Animal Countdown Value
				if($workProductId!="4a529aae-5165-3149-6db7-5702c7b98a5e" && $workProductId!="811ae58e-9281-2755-25d5-5702c7252696")
				{
					sleep(1);
					$wp_bean = BeanFactory::getBean('M03_Work_Product', $workProductId);
					//$wp_bean->save();
					///////////////////////////////////
					//////////////////////////////////////////////////////////////////////
					$previousAnimalCount 		= $wp_bean->primary_animals_countdown_c;
					$previousBlanketAnimalCount = $wp_bean->blanket_animals_remaining_c;
					
					//$GLOBALS['log']->fatal('WPE After relationship Add Blanket :');
					//$GLOBALS['log']->fatal('previousAnimalCount :' . $previousAnimalCount);
					//$GLOBALS['log']->fatal('previousBlanketAnimalCount :' . $previousBlanketAnimalCount);
					
					$animalCountValue  = $this->calculateAnimal($wp_bean,$previousAnimalCount,$previousBlanketAnimalCount);
				}
			}
			/********************** */ 
		}
		
		 $related_module = $arguments['related_module'];
		if($bean->id!="" && $related_module == 'ANML_Animals'){
			$wpe_id =$bean->id; // Work Product Enrollment ID
			
			
			$wpe_animal_sql = "SELECT * FROM `anml_animals_wpe_work_product_enrollment_1_c` AS ANML_WPE  
					WHERE ANML_WPE.`anml_anima9941ollment_idb`='".$wpe_id."' AND deleted=0 ORDER BY `ANML_WPE`.`date_modified` DESC limit 1";
			 
			$wpe_animal_exec = $db->query($wpe_animal_sql);
			if ($wpe_animal_exec->num_rows > 0) 
			{
				$result = $db->fetchByAssoc($wpe_animal_exec);
				$testSystemAnimalId = $result['anml_animals_wpe_work_product_enrollment_1anml_animals_ida'];
				/*Get animal id, animal name, room id from Test System */
				$ANML_Animals = BeanFactory::getBean('ANML_Animals', $testSystemAnimalId);
				 
				$animal_id		= $ANML_Animals->id;
				$animal_name	= $ANML_Animals->name;
				$animal_roomid  = $ANML_Animals->rms_room_id_c;
				
				//$GLOBALS['log']->fatal('animal room id:=>' . $animal_roomid);
				if($animal_roomid=='11633546-7965-11e9-9c7a-029395602a62' || $animal_roomid=='117c10ca-7965-11e9-b77d-029395602a62' || $animal_roomid=='118f9064-7965-11e9-a677-029395602a62' || $animal_roomid=='11a312ec-7965-11e9-b6a3-029395602a62' || $animal_roomid=='11b67a3a-7965-11e9-8bc7-029395602a62' )
				{
					
					$roomBean 	= BeanFactory::getBean('RMS_Room', $animal_roomid);
					$room_name	= $roomBean->name; // Get Animal Assignment Notification Template
					$template 	= new EmailTemplate();
					
					$template->retrieve_by_string_fields(array('name' => 'Animal Assignment Notification', 'type' => 'email'));
					// Set values in Template
					$site_url		= $GLOBALS['sugar_config']['site_url'];
					$animal_link	= '<a target="_blank" href="'.$site_url.'/#ANML_Animals/'.$animal_id.'">'.$animal_name.'</a>';
					$room_link 		= '<a target="_blank" href="'.$site_url.'/#RMS_Room/'.$animal_roomid.'">'.$room_name.'</a>';
					
					$template->body_html = str_replace('[ANIMAL_NAME]', $animal_link, $template->body_html);
					$template->body_html = str_replace('[ANIMAL_ROOM]', $room_link, $template->body_html);
					//$GLOBALS['log']->fatal('Notification Email:=>' . $template->body_html);
					// Send Email
					$emailObj	= new Email();
					$defaults	= $emailObj->getSystemDefaultEmail();
					$mail 		= new SugarPHPMailer();
					$mail->setMailerForSystem();
					$mail->IsHTML(true);
					$mail->From = $defaults['email'];
					$mail->FromName = $defaults['name'];
					if(!empty($template->subject))
						$mail->Subject = $template->subject;
					else
						$mail->Subject = "";
			
					if (!empty($template->body_html)) {
						$mail->Body = $template->body_html;
					} else {
						$mail->Body = "";
					}
					$mail->AddAddress('nuitermarkt@apsemail.com'); //to be deleted
					//$mail->AddAddress('fxs_mjohnson@apsemail.com'); //to be deleted
					$mail->AddBCC('vsuthar@apsemail.com'); //to be deleted
					if (!$mail->Send()) {
						$GLOBALS['log']->fatal("247: Fail to Send Email, Please check settings");
					} else {
						$GLOBALS['log']->fatal('247: email sent');
					}

				}				
			}
		}	
	} 
}
?>