<?php
// created: 2022-03-08 05:49:09
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'm03_work_product_wpe_work_product_enrollment_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_M03_WORK_PRODUCT_TITLE',
    'id' => 'M03_WORK_P7D13PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M03_Work_Product',
    'target_record_key' => 'm03_work_p7d13product_ida',
  ),
  'enrollment_status_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ENROLLMENT_STATUS',
    'width' => 10,
  ),
  'gd_group_design_wpe_work_product_enrollment_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1_NAME_FIELD_TITLE',
    'id' => 'GD_GROUP_DESIGN_WPE_WORK_PRODUCT_ENROLLMENT_1GD_GROUP_DESIGN_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'GD_Group_Design',
    'target_record_key' => 'gd_group_design_wpe_work_product_enrollment_1gd_group_design_ida',
  ),
  'comments_c' => 
  array (
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_COMMENTS',
    'width' => 10,
    'default' => true,
  ),
  'usda_category_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_USDA_CATEGORY',
    'width' => 10,
  ),
  'm03_work_product_wpe_work_product_enrollment_2_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_NAME_FIELD_TITLE',
    'id' => 'M03_WORK_P9F23PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M03_Work_Product',
    'target_record_key' => 'm03_work_p9f23product_ida',
  ),
  'activities_c' => 
  array (
    'readonly' => false,
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_ACTIVITIES',
    'width' => 10,
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
);