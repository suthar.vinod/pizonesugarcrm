<?php
// created: 2021-08-14 15:53:07
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'anml_animals_wpe_work_product_enrollment_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE',
    'id' => 'ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1ANML_ANIMALS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'ANML_Animals',
    'target_record_key' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
  ),
  'usda_id_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_USDA_ID',
    'width' => 10,
    'default' => true,
  ),
  'enrollment_status_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ENROLLMENT_STATUS',
    'width' => 10,
  ),
  'usda_category_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_USDA_CATEGORY',
    'width' => 10,
  ),
  'activities_c' => 
  array (
    'readonly' => false,
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_ACTIVITIES',
    'width' => 10,
  ),
  'comments_c' => 
  array (
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_COMMENTS',
    'width' => 10,
    'default' => true,
  ),
  'm03_work_product_wpe_work_product_enrollment_2_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_NAME_FIELD_TITLE',
    'id' => 'M03_WORK_P9F23PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M03_Work_Product',
    'target_record_key' => 'm03_work_p9f23product_ida',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
);