<?php
// created: 2022-04-26 11:06:59
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'anml_animals_wpe_work_product_enrollment_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE',
    'id' => 'ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1ANML_ANIMALS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'ANML_Animals',
    'target_record_key' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
  ),
  'usda_id_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_USDA_ID',
    'width' => 10,
    'default' => true,
  ),
  'enrollment_status_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ENROLLMENT_STATUS',
    'width' => 10,
  ),
  'comments_c' => 
  array (
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_COMMENTS',
    'width' => 10,
    'default' => true,
  ),
  'activities_c' => 
  array (
    'readonly' => false,
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_ACTIVITIES',
    'width' => 10,
  ),
);