<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once("modules/Emails/Email.php");
require_once('modules/EmailTemplates/EmailTemplate.php');
require_once('include/SugarPHPMailer.php');
class WPE_animalNotificationAfterRelationAddHook{
	function SendAnimalAssignmentNotification($bean, $event, $arguments){
		global $db;
		$workProductId	= "";
		$onStudyCount	= 0;
		$onStudyBackupCount = 0;
		if($bean->id!=""){
			$wpe_id =$bean->id; // Work Product Enrollment ID
			 
			//Query to get the Work Product ID	
			//$GLOBALS['log']->fatal('AfterRelationAddHook Notification.'.$wpe_id);
			$wpe_animal_sql = "SELECT * FROM `anml_animals_wpe_work_product_enrollment_1_c` AS ANML_WPE  
					WHERE ANML_WPE.`anml_anima9941ollment_idb`='".$wpe_id."' AND deleted=0 ORDER BY `ANML_WPE`.`date_modified` DESC limit 1";
			//$GLOBALS['log']->fatal($wpe_sql);		
			$wpe_animal_exec = $db->query($wpe_animal_sql);
			if ($wpe_animal_exec->num_rows > 0) 
			{
				$result = $db->fetchByAssoc($wpe_animal_exec);
				$testSystemAnimalId = $result['anml_animals_wpe_work_product_enrollment_1anml_animals_ida'];
				/*Get animal id, animal name, room id from Test System */
				$ANML_Animals = BeanFactory::getBean('ANML_Animals', $testSystemAnimalId);
				//$GLOBALS['log']->fatal('test System Animal Id:=>' . $testSystemAnimalId);
				//Get Animal Name ===== from table : anml_animals --> name
				$animal_id =$ANML_Animals->id;
				$animal_name =$ANML_Animals->name;
				//$GLOBALS['log']->fatal('animal id:=>' . $animal_id);
				//$GLOBALS['log']->fatal('animal name:=>' . $animal_name);
				// $animal_roomid =$ANML_Animals->id_c;
				// Get Animal Room ID  === from table : anml_animals_cstm --> 
				$animal_roomid =$ANML_Animals->rms_room_id_c;
				//$GLOBALS['log']->fatal('animal room id:=>' . $animal_roomid);
				if($animal_roomid=='11633546-7965-11e9-9c7a-029395602a62' || $animal_roomid=='117c10ca-7965-11e9-b77d-029395602a62' || $animal_roomid=='118f9064-7965-11e9-a677-029395602a62' || $animal_roomid=='11a312ec-7965-11e9-b6a3-029395602a62' || $animal_roomid=='11b67a3a-7965-11e9-8bc7-029395602a62' )
				{
					
					//Get  room Name from == > table - rms_room 
					$wpe_roomname_sql = "SELECT * FROM `rms_room` AS ROOM_WPE  WHERE ROOM_WPE.`id`='".$animal_roomid."' ";
					$wpe_roomname_sql_exec = $db->query($wpe_roomname_sql);
					$result_roomname = $db->fetchByAssoc($wpe_roomname_sql_exec);
					$room_name = $result_roomname['name'];
					//$GLOBALS['log']->fatal('room name:=>' . $room_name);
					// Get Animal Assignment Notification Template
					$template = new EmailTemplate();
					$template->retrieve_by_string_fields(array('name' => 'Animal Assignment Notification', 'type' => 'email'));
					// Set values in Template
					$site_url = $GLOBALS['sugar_config']['site_url'];
					$animal_name_withlink = '<a target="_blank" href="' . $site_url . '/#ANML_Animals/' . $animal_id . '">' . $animal_name . '</a>';
					$room_name_withlink = '<a target="_blank" href="' . $site_url . '/#RMS_Room/' . $animal_roomid . '">' . $room_name . '</a>';
					$template->body_html = str_replace('[ANIMAL_NAME]', $animal_name_withlink, $template->body_html);
					$template->body_html = str_replace('[ANIMAL_ROOM]', $room_name_withlink, $template->body_html);
					//$GLOBALS['log']->fatal('Notification Email:=>' . $template->body_html);
					// Send Email
					$emailObj = new Email();
					$defaults = $emailObj->getSystemDefaultEmail();
					$mail = new SugarPHPMailer();
					$mail->setMailerForSystem();
					$mail->IsHTML(true);
					$mail->From = $defaults['email'];
					$mail->FromName = $defaults['name'];
					if(!empty($template->subject))
						$mail->Subject = $template->subject;
					else
						$mail->Subject = "";
			
					if (!empty($template->body_html)) {
						$mail->Body = $template->body_html;
					} else {
						$mail->Body = "";
					}
					$mail->AddAddress('nuitermarkt@apsemail.com'); //to be deleted
					$mail->AddBCC('vsuthar@apsemail.com'); //to be deleted
					if (!$mail->Send()) {
						$GLOBALS['log']->fatal("247: Fail to Send Email, Please check settings");
					} else {
						$GLOBALS['log']->fatal('247: email sent');
					}

				}
				
			}
		}
	}
}
?>