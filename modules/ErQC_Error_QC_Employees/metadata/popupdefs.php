<?php
$popupMeta = array (
    'moduleMain' => 'ErQC_Error_QC_Employees',
    'varName' => 'ErQC_Error_QC_Employees',
    'orderBy' => 'erqc_error_qc_employees.name',
    'whereClauses' => array (
  'name' => 'erqc_error_qc_employees.name',
  'initials_of_record_c' => 'erqc_error_qc_employees_cstm.initials_of_record_c',
  'department_c' => 'erqc_error_qc_employees_cstm.department_c',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'initials_of_record_c',
  5 => 'department_c',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'initials_of_record_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_INITIALS_OF_RECORD',
    'width' => 10,
    'name' => 'initials_of_record_c',
  ),
  'department_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_DEPARTMENT',
    'width' => 10,
    'name' => 'department_c',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'INITIALS_OF_RECORD_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_INITIALS_OF_RECORD',
    'width' => 10,
    'default' => true,
  ),
  'DEPARTMENT_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_DEPARTMENT',
    'width' => 10,
  ),
),
);
