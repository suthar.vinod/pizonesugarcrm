<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ErQC_Error_QC_Employees/Ext/Vardefs/sugarfield_department_c.php

 // created: 2018-06-01 21:12:44
$dictionary['ErQC_Error_QC_Employees']['fields']['department_c']['labelValue']='Department';
$dictionary['ErQC_Error_QC_Employees']['fields']['department_c']['dependency']='';
$dictionary['ErQC_Error_QC_Employees']['fields']['department_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/ErQC_Error_QC_Employees/Ext/Vardefs/sugarfield_employee_number_c.php

 // created: 2018-06-01 21:13:36
$dictionary['ErQC_Error_QC_Employees']['fields']['employee_number_c']['labelValue']='Employee Number';
$dictionary['ErQC_Error_QC_Employees']['fields']['employee_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ErQC_Error_QC_Employees']['fields']['employee_number_c']['enforced']='';
$dictionary['ErQC_Error_QC_Employees']['fields']['employee_number_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ErQC_Error_QC_Employees/Ext/Vardefs/sugarfield_initials_of_record_c.php

 // created: 2018-06-01 21:14:21
$dictionary['ErQC_Error_QC_Employees']['fields']['initials_of_record_c']['labelValue']='Initials of Record';
$dictionary['ErQC_Error_QC_Employees']['fields']['initials_of_record_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ErQC_Error_QC_Employees']['fields']['initials_of_record_c']['enforced']='';
$dictionary['ErQC_Error_QC_Employees']['fields']['initials_of_record_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ErQC_Error_QC_Employees/Ext/Vardefs/erqc_error_qc_employees_m06_error_1_ErQC_Error_QC_Employees.php

// created: 2018-06-01 21:24:07
$dictionary["ErQC_Error_QC_Employees"]["fields"]["erqc_error_qc_employees_m06_error_1"] = array (
  'name' => 'erqc_error_qc_employees_m06_error_1',
  'type' => 'link',
  'relationship' => 'erqc_error_qc_employees_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_ERQC_ERROR_QC_EMPLOYEES_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'erqc_error_qc_employees_m06_error_1m06_error_idb',
);

?>
