<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/NSC_NAMSA_Sub_Companies/Ext/Vardefs/accounts_nsc_namsa_sub_companies_1_NSC_NAMSA_Sub_Companies.php

// created: 2021-12-07 12:21:22
$dictionary["NSC_NAMSA_Sub_Companies"]["fields"]["accounts_nsc_namsa_sub_companies_1"] = array (
  'name' => 'accounts_nsc_namsa_sub_companies_1',
  'type' => 'link',
  'relationship' => 'accounts_nsc_namsa_sub_companies_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE',
  'id_name' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["NSC_NAMSA_Sub_Companies"]["fields"]["accounts_nsc_namsa_sub_companies_1_name"] = array (
  'name' => 'accounts_nsc_namsa_sub_companies_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
  'link' => 'accounts_nsc_namsa_sub_companies_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["NSC_NAMSA_Sub_Companies"]["fields"]["accounts_nsc_namsa_sub_companies_1accounts_ida"] = array (
  'name' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE_ID',
  'id_name' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
  'link' => 'accounts_nsc_namsa_sub_companies_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/NSC_NAMSA_Sub_Companies/Ext/Vardefs/sugarfield_accounts_nsc_namsa_sub_companies_1accounts_ida.php

 // created: 2021-12-07 12:23:18
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['name']='accounts_nsc_namsa_sub_companies_1accounts_ida';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['type']='id';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['source']='non-db';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['vname']='LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE_ID';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['id_name']='accounts_nsc_namsa_sub_companies_1accounts_ida';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['link']='accounts_nsc_namsa_sub_companies_1';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['table']='accounts';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['module']='Accounts';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['rname']='id';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['reportable']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['side']='right';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['massupdate']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['duplicate_merge']='disabled';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['hideacl']=true;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['audited']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1accounts_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/NSC_NAMSA_Sub_Companies/Ext/Vardefs/sugarfield_accounts_nsc_namsa_sub_companies_1_name.php

 // created: 2021-12-07 12:23:20
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['audited']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['massupdate']=true;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['hidemassupdate']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['duplicate_merge']='enabled';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['duplicate_merge_dom_value']='1';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['merge_filter']='disabled';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['reportable']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['unified_search']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['calculated']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['accounts_nsc_namsa_sub_companies_1_name']['vname']='LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/NSC_NAMSA_Sub_Companies/Ext/Vardefs/sugarfield_name.php

 // created: 2021-12-07 12:25:29
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['importable']='false';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['duplicate_merge']='disabled';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['merge_filter']='disabled';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['unified_search']=false;
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['calculated']='true';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['formula']='$company_name';
$dictionary['NSC_NAMSA_Sub_Companies']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/NSC_NAMSA_Sub_Companies/Ext/Vardefs/nsc_namsa_sub_companies_m01_sales_1_NSC_NAMSA_Sub_Companies.php

// created: 2021-12-07 12:33:38
$dictionary["NSC_NAMSA_Sub_Companies"]["fields"]["nsc_namsa_sub_companies_m01_sales_1"] = array (
  'name' => 'nsc_namsa_sub_companies_m01_sales_1',
  'type' => 'link',
  'relationship' => 'nsc_namsa_sub_companies_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE',
  'id_name' => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
