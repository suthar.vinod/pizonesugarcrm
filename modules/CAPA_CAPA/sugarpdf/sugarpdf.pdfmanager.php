<?php
require_once 'include/Sugarpdf/sugarpdf/sugarpdf.pdfmanager.php';
require_once 'include/Sugarpdf/sugarpdf/sugarpdf.smarty.php';
//require_once('include/Sugar_Smarty.php');
//ob_start();
class CAPA_CAPASugarpdfPdfmanager extends SugarpdfPdfmanager
{
    public function preDisplay()
    {
        global $db, $current_user;
        //$this->ss = new Sugar_Smarty();
        parent::preDisplay();
        $this->SetMargins(15, 10, 15);

        $previewMode = false;

        if (!empty($_REQUEST['pdf_preview']) && $_REQUEST['pdf_preview'] == 1) {
            $previewMode = true;
		}	
			
        if ($this->module == 'CAPA_CAPA' && $previewMode === false) {
             
            $beanid = $this->bean->id;
			// $wp		= $this->bean->m03_work_p33edproduct_ida;
			
            $CAPAQuery	        = "SELECT * FROM capa_capa where id='".$beanid."' and deleted=0 ORDER BY date_modified";
            $resultCAPA1		= $db->query($CAPAQuery);
            $rowCAPA            = $db->fetchByAssoc($resultCAPA1);
			

			//CAPA Owner(Contact) bean
            $this->bean->load_relationship('capa_capa_contacts_1');
            $CommBeanId		= $this->bean->capa_capa_contacts_1->get();//?
			
            $commData	= array();
            foreach ($CommBeanId as $CommId)
            {
				$commBean    = BeanFactory::getBean('Contacts', $CommId);
                array_push($commData, $commBean->name);
            }
            $commaSepOwner = "";
            for ($i=0; $i < count($commData); $i++) { 
                $commaSepOwner = implode(', ',$commData);
            }


            //Study Director(Contact) bean
            $this->bean->load_relationship('capa_capa_contacts_2');
            $StudyDirectorBeanId		= $this->bean->capa_capa_contacts_2->get();//?
			
            $studyDirectorData	= array();
            foreach ($StudyDirectorBeanId as $studyDirectorId)
            {
				$studyDirectorBean    = BeanFactory::getBean('Contacts', $studyDirectorId);
                array_push($studyDirectorData, $studyDirectorBean->name);
            }
            $commaSepStudyDirector = "";
            for ($i=0; $i<count($studyDirectorData); $i++) { 
                $commaSepStudyDirector = implode(', ',$studyDirectorData);
            }

            //check if any record linked in the communication Panel
            $deviationAttech = "";
            $this->bean->load_relationship('capa_capa_m06_error_1');
            $communicationBeanId		= $this->bean->capa_capa_m06_error_1->get();
			if(!empty($communicationBeanId))
                $deviationAttech = 1;
            else
                $deviationAttech = 0;

            

            //For Preventative Action Plan Custom Date
            if(!empty($rowCAPA['effectiveness_verification_pa'])){
                $setCustomDate = explode('-',$rowCAPA['effectiveness_verification_pa']);
                $newCustomDate = $setCustomDate[1].'/'.$setCustomDate[2].'/'.$setCustomDate[0];
            }
            if(!empty($rowCAPA['preventative_action_target_due'])){
                $setCustomDatePreventative = explode('-',$rowCAPA['preventative_action_target_due']);
                $setCustomDatePreventativeDate = $setCustomDatePreventative[1].'/'.$setCustomDatePreventative[2].'/'.$setCustomDatePreventative[0];
            }

            //For Corrective Action Plan Custom Date
            if(!empty($rowCAPA['corrective_action_target_due'])){
                $setCustomDateCorrective = explode('-',$rowCAPA['corrective_action_target_due']);
                $setCustomDateCorrectiveDate = $setCustomDateCorrective[1].'/'.$setCustomDateCorrective[2].'/'.$setCustomDateCorrective[0];
            }
            if(!empty($rowCAPA['effectiveness_verification_due'])){
                $setCustomDateEffectiveVarification = explode('-',$rowCAPA['effectiveness_verification_due']);
                $setCustomDateEffectiveVarificationDate = $setCustomDateEffectiveVarification[1].'/'.$setCustomDateEffectiveVarification[2].'/'.$setCustomDateEffectiveVarification[0];
            }

            //For CAPA Initiation Custom Custom Date
            if(!empty($rowCAPA['date_initiated'])){
                $setCustomDateInitiation = explode('-',$rowCAPA['date_initiated']);
                $setCustomDateInitiationDate = $setCustomDateInitiation[1].'/'.$setCustomDateInitiation[2].'/'.$setCustomDateInitiation[0];
            }

            //For CAPA Initiation Form
            if(!empty($rowCAPA['non_conforming_event']))
                $countNonConfEvent = str_word_count($rowCAPA['non_conforming_event']);
            else
                $countNonConfEvent = 0;
            if(!empty($rowCAPA['initial_scope']))
                $countInitial_scope = str_word_count($rowCAPA['initial_scope']);
            else
                $countInitial_scope = 0;
            if(!empty($rowCAPA['correction']))
                $countCorrection = str_word_count($rowCAPA['correction']);
            else
                $countCorrection = 0;
            
            $textCount = $countNonConfEvent + $countInitial_scope +  $countCorrection;
            if($textCount<= 241)
                $objectiveTextSize = '1.13em';//16px
            if($textCount>241 && $textCount<=380)
                $objectiveTextSize = '1em';  //14.4px  
            if($textCount>380 && $textCount<=450)
                $objectiveTextSize = '0.90em'; //12.8px
            if($textCount>450 && $textCount<=583)
                $objectiveTextSize = '0.75em';   //11.2px 
            if($textCount>583)
                $objectiveTextSize = '0.70em'; //9.6px

            //For Investigation Report
            if(!empty($rowCAPA['root_cause_investigation']))
                $root_cause_investigation = str_word_count($rowCAPA['root_cause_investigation']);
            else
                $root_cause_investigation = 0;
            
            $textCountInvestigation = $root_cause_investigation;
            if($textCountInvestigation< 583)
                $objectiveInvestigation = '1.2em';//16px
            if($textCountInvestigation>300 && $textCountInvestigation<380)
                $objectiveInvestigation = '1.1em';  //14.4px  
            if($textCountInvestigation>380 && $textCountInvestigation<450)
                $objectiveInvestigation = '1em'; //12.8px
            if($textCountInvestigation>450 && $textCountInvestigation<525)
                $objectiveInvestigation = '0.90em';   //11.2px 
            if($textCountInvestigation>583)
                $objectiveInvestigation = '0.80em'; //9.6px


            //For Corrective Report
            if(!empty($rowCAPA['corrective_actions_proposed']))
                $corrective_actions_proposed = str_word_count($rowCAPA['corrective_actions_proposed']);
            else
                $corrective_actions_proposed = 0;
            if(!empty($rowCAPA['proposed_verification_effectiv']))
                $proposed_verification_effectiv = str_word_count($rowCAPA['proposed_verification_effectiv']);
            else
                $proposed_verification_effectiv  = 0;
            
            $textCountCorrective = $corrective_actions_proposed + $proposed_verification_effectiv;
            if($textCountCorrective< 583)
                $objectiveCorrective = '1.2em';//16px
            if($textCountCorrective>300 && $textCountCorrective<380)
                $objectiveCorrective = '1.1em';  //14.4px  
            if($textCountCorrective>380 && $textCountCorrective<450)
                $objectiveCorrective = '1em'; //12.8px
            if($textCountCorrective>450 && $textCountCorrective<525)
                $objectiveCorrective = '0.90em';   //11.2px 
            if($textCountCorrective>583)
                $objectiveCorrective = '0.80em'; //9.6px


             //For Preventative Report
            if(!empty($rowCAPA['preventative_actions_proposed']))
                $preventative_actions_proposed = str_word_count($rowCAPA['preventative_actions_proposed']);
            else
                $preventative_actions_proposed = 0;
            if(!empty($rowCAPA['rationale_or_discussion_of_pa']))
                $rationale_or_discussion_of_pa = str_word_count($rowCAPA['rationale_or_discussion_of_pa']);
            else
                $rationale_or_discussion_of_pa  = 0;
            if(!empty($rowCAPA['proposed_verification_pa']))
                $proposed_verification_pa = str_word_count($rowCAPA['proposed_verification_pa']);
            else
                $proposed_verification_pa  = 0;
            
            $textCountPreventative = $preventative_actions_proposed + $rationale_or_discussion_of_pa + $proposed_verification_pa;
            if($textCountPreventative< 583)
                $objectivePreventative = '1.2em';//16px
            if($textCountPreventative>300 && $textCountPreventative<380)
                $objectivePreventative = '1.1em';  //14.4px  
            if($textCountPreventative>380 && $textCountPreventative<450)
                $objectivePreventative = '1em'; //12.8px
            if($textCountPreventative>450 && $textCountPreventative<525)
                $objectivePreventative = '0.90em';   //11.2px 
            if($textCountPreventative>583)
                $objectivePreventative = '0.80em'; //9.6px

             //For CAPA Review
            if(!empty($rowCAPA['corrective_action_effective']))
                $corrective_action_effective = str_word_count($rowCAPA['corrective_action_effective']);
            else
                $corrective_action_effective = 0;
            if(!empty($rowCAPA['preventative_action_effective']))
                $preventative_action_effective = str_word_count($rowCAPA['preventative_action_effective']);
            else
                $preventative_action_effective  = 0;
            if(!empty($rowCAPA['capa_cancellation_justificatio']))
                $capa_cancellation_justificatio = str_word_count($rowCAPA['capa_cancellation_justificatio']);
            else
                $capa_cancellation_justificatio  = 0;
            
            $textCountReview = $corrective_action_effective + $preventative_action_effective + $capa_cancellation_justificatio;
            if($textCountReview< 583)
                $objectiveReview = '1.2em';//16px
            if($textCountReview>300 && $textCountReview<380)
                $objectiveReview = '1.1em';  //14.4px  
            if($textCountReview>380 && $textCountReview<450)
                $objectiveReview = '1em'; //12.8px
            if($textCountReview>450 && $textCountReview<525)
                $objectiveReview = '0.90em';   //11.2px 
            if($textCountReview>583)
                $objectiveReview = '0.80em'; //9.6px


            $this->ss->assign('name', $rowCAPA['name']);
            $this->ss->assign('risk_assessment', $rowCAPA['risk_assessment']);
            $this->ss->assign('capa_owner', $commaSepOwner);
            $this->ss->assign('Study_director', $commaSepStudyDirector);
            $this->ss->assign('setCustomDatePreventativeDate', $setCustomDatePreventativeDate);
            $this->ss->assign('newCustomDate', $newCustomDate);
            $this->ss->assign('CorrectiveActionTargetDueDate', $setCustomDateCorrectiveDate);
            $this->ss->assign('effectiveVarificationDueDate', $setCustomDateEffectiveVarificationDate);
            $this->ss->assign('date_initiated', $setCustomDateInitiationDate);
            $this->ss->assign('deviationAttech', $deviationAttech);
            $this->ss->assign('objectiveTextSize', $objectiveTextSize);
            $this->ss->assign('objectiveInvestigation', $objectiveInvestigation);
            $this->ss->assign('objectiveCorrective', $objectiveCorrective);
            $this->ss->assign('objectivePreventative', $objectivePreventative);
            $this->ss->assign('objectiveReview', $objectiveReview);
        }  
    }

    /**
     * PDF manager specific Footer function
     */
    public function Footer() {
        $cur_y = $this->GetY();
        $ormargins = $this->getOriginalMargins();
        $this->SetTextColor(0, 0, 0);
        //set style for cell border
        $line_width = 0.85 / $this->getScaleFactor();
        $this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        //print document barcode
        $barcode = $this->getBarcode();
        if (!empty($barcode)) {
            $this->Ln($line_width);
            $barcode_width = round(($this->getPageWidth() - $ormargins['left'] - $ormargins['right'])/3);
            $this->write1DBarcode($barcode, 'C128B', $this->GetX(), $cur_y + $line_width, $barcode_width, (($this->getFooterMargin() / 3) - $line_width), 0.3, '', '');
        }
        if (empty($this->pagegroups)) {
            $pagenumtxt = $this->l['w_page'].' '.$this->getAliasNumPage().' OF '.$this->getAliasNbPages();
            $pagenumtxt = 'PAGE'. $pagenumtxt;
        } else {
            $pagenumtxt = $this->l['w_page'].' '.$this->getPageNumGroupAlias().' OF '.$this->getPageGroupAlias();
            $pagenumtxt = 'PAGE'. $pagenumtxt;
        }
        $this->SetY($cur_y);

        if ($this->getRTL()) {
            $this->SetX($ormargins['right']);
            if ($this->footerText) {
                // footer text and page number
                $this->Cell(0, 0, $this->footerText, 'T', 0, 'R');
                $this->Cell(0, 0, $pagenumtxt, 0, 0, 'L');
            } else {
                // page number only
                $this->Cell(0, 0, $pagenumtxt, 'T', 0, 'L');
            }
        } else {
            $this->SetX($ormargins['left']);
            if ($this->footerText) {
                // footer text and page number
                $this->Cell(0, 0, $this->footerText, 'T', 0, 'L');
                $this->Cell(0, 0, $pagenumtxt, 0, 0, 'R');
            } else {
                // page number only
                $this->Cell(0, 0, $pagenumtxt, 'T', 0, 'R');
            }
        }
    }
}