<?php
// created: 2022-02-03 09:01:14
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'date_initiated' => 
  array (
    'readonly' => false,
    'type' => 'date',
    'vname' => 'LBL_DATE_INITIATED',
    'width' => 10,
    'default' => true,
  ),
  'issue' => 
  array (
    'readonly' => false,
    'type' => 'text',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_ISSUE',
    'sortable' => false,
    'width' => 10,
  ),
  'non_conforming_event' => 
  array (
    'readonly' => false,
    'type' => 'text',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_NON_CONFORMING_EVENT',
    'sortable' => false,
    'width' => 10,
  ),
  'initial_scope' => 
  array (
    'readonly' => false,
    'type' => 'text',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_INITIAL_SCOPE',
    'sortable' => false,
    'width' => 10,
  ),
);