<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/capa_capa_cf_capa_files_1_CAPA_CAPA.php

// created: 2022-02-03 07:26:23
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_cf_capa_files_1"] = array (
  'name' => 'capa_capa_cf_capa_files_1',
  'type' => 'link',
  'relationship' => 'capa_capa_cf_capa_files_1',
  'source' => 'non-db',
  'module' => 'CF_CAPA_Files',
  'bean_name' => 'CF_CAPA_Files',
  'vname' => 'LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CAPA_CAPA_TITLE',
  'id_name' => 'capa_capa_cf_capa_files_1capa_capa_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/capa_capa_m06_error_1_CAPA_CAPA.php

// created: 2022-02-03 07:30:32
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_m06_error_1"] = array (
  'name' => 'capa_capa_m06_error_1',
  'type' => 'link',
  'relationship' => 'capa_capa_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_CAPA_CAPA_M06_ERROR_1_FROM_CAPA_CAPA_TITLE',
  'id_name' => 'capa_capa_m06_error_1capa_capa_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/capa_capa_capa_capa_1_CAPA_CAPA.php

// created: 2022-02-03 07:32:44
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_capa_capa_1"] = array (
  'name' => 'capa_capa_capa_capa_1',
  'type' => 'link',
  'relationship' => 'capa_capa_capa_capa_1',
  'source' => 'non-db',
  'module' => 'CAPA_CAPA',
  'bean_name' => 'CAPA_CAPA',
  'vname' => 'LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_L_TITLE',
  'id_name' => 'capa_capa_capa_capa_1capa_capa_idb',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_capa_capa_1_right"] = array (
  'name' => 'capa_capa_capa_capa_1_right',
  'type' => 'link',
  'relationship' => 'capa_capa_capa_capa_1',
  'source' => 'non-db',
  'module' => 'CAPA_CAPA',
  'bean_name' => 'CAPA_CAPA',
  'side' => 'right',
  'vname' => 'LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_R_TITLE',
  'id_name' => 'capa_capa_capa_capa_1capa_capa_ida',
  'link-type' => 'one',
);
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_capa_capa_1_name"] = array (
  'name' => 'capa_capa_capa_capa_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_L_TITLE',
  'save' => true,
  'id_name' => 'capa_capa_capa_capa_1capa_capa_ida',
  'link' => 'capa_capa_capa_capa_1_right',
  'table' => 'capa_capa',
  'module' => 'CAPA_CAPA',
  'rname' => 'name',
);
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_capa_capa_1capa_capa_ida"] = array (
  'name' => 'capa_capa_capa_capa_1capa_capa_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_R_TITLE_ID',
  'id_name' => 'capa_capa_capa_capa_1capa_capa_ida',
  'link' => 'capa_capa_capa_capa_1_right',
  'table' => 'capa_capa',
  'module' => 'CAPA_CAPA',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/capa_capa_contacts_1_CAPA_CAPA.php

// created: 2022-02-03 07:34:36
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_contacts_1"] = array (
  'name' => 'capa_capa_contacts_1',
  'type' => 'link',
  'relationship' => 'capa_capa_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CAPA_CAPA_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'capa_capa_contacts_1contacts_idb',
);

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/capa_capa_contacts_2_CAPA_CAPA.php

// created: 2022-02-03 07:36:41
$dictionary["CAPA_CAPA"]["fields"]["capa_capa_contacts_2"] = array (
  'name' => 'capa_capa_contacts_2',
  'type' => 'link',
  'relationship' => 'capa_capa_contacts_2',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CAPA_CAPA_CONTACTS_2_FROM_CONTACTS_TITLE',
  'id_name' => 'capa_capa_contacts_2contacts_idb',
);

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_ca_andor_pa_dd_c.php

 // created: 2022-02-03 08:10:15
$dictionary['CAPA_CAPA']['fields']['ca_andor_pa_dd_c']['labelValue']='CAPA Type';
$dictionary['CAPA_CAPA']['fields']['ca_andor_pa_dd_c']['dependency']='';
$dictionary['CAPA_CAPA']['fields']['ca_andor_pa_dd_c']['required_formula']='';
$dictionary['CAPA_CAPA']['fields']['ca_andor_pa_dd_c']['readonly_formula']='';
$dictionary['CAPA_CAPA']['fields']['ca_andor_pa_dd_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_ca_mgmt_ack_na_c.php

 // created: 2022-02-03 08:11:14
$dictionary['CAPA_CAPA']['fields']['ca_mgmt_ack_na_c']['labelValue']='CA Affected Management Acknowledgement Not Applicable';
$dictionary['CAPA_CAPA']['fields']['ca_mgmt_ack_na_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['ca_mgmt_ack_na_c']['dependency']='and(equal($ca_andor_pa_dd_c,"CA"),not(equal($risk_assessment,"Minor")))';
$dictionary['CAPA_CAPA']['fields']['ca_mgmt_ack_na_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_pa_mgmt_ack_na_c.php

 // created: 2022-02-03 08:16:51
$dictionary['CAPA_CAPA']['fields']['pa_mgmt_ack_na_c']['labelValue']='PA Affected Management Acknowledgement Not Applicable';
$dictionary['CAPA_CAPA']['fields']['pa_mgmt_ack_na_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['pa_mgmt_ack_na_c']['dependency']='isInList($ca_andor_pa_dd_c,createList("PA","CA and PA"))';
$dictionary['CAPA_CAPA']['fields']['pa_mgmt_ack_na_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_frequency_score_dd_c.php

 // created: 2022-02-03 08:25:23
$dictionary['CAPA_CAPA']['fields']['frequency_score_dd_c']['labelValue']='Frequency Score';
$dictionary['CAPA_CAPA']['fields']['frequency_score_dd_c']['dependency']='';
$dictionary['CAPA_CAPA']['fields']['frequency_score_dd_c']['required_formula']='';
$dictionary['CAPA_CAPA']['fields']['frequency_score_dd_c']['readonly_formula']='';
$dictionary['CAPA_CAPA']['fields']['frequency_score_dd_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_severity_score_dd_c.php

 // created: 2022-02-03 08:27:45
$dictionary['CAPA_CAPA']['fields']['severity_score_dd_c']['labelValue']='Severity Score';
$dictionary['CAPA_CAPA']['fields']['severity_score_dd_c']['dependency']='';
$dictionary['CAPA_CAPA']['fields']['severity_score_dd_c']['required_formula']='';
$dictionary['CAPA_CAPA']['fields']['severity_score_dd_c']['readonly_formula']='';
$dictionary['CAPA_CAPA']['fields']['severity_score_dd_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_iacuc_yes_c.php

 // created: 2022-02-03 08:29:13
$dictionary['CAPA_CAPA']['fields']['iacuc_yes_c']['labelValue']='IACUC Yes';
$dictionary['CAPA_CAPA']['fields']['iacuc_yes_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['iacuc_yes_c']['dependency']='';
$dictionary['CAPA_CAPA']['fields']['iacuc_yes_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_iacuc_na_c.php

 // created: 2022-02-03 08:32:25
$dictionary['CAPA_CAPA']['fields']['iacuc_na_c']['labelValue']='IACUC NA';
$dictionary['CAPA_CAPA']['fields']['iacuc_na_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['iacuc_na_c']['dependency']='';
$dictionary['CAPA_CAPA']['fields']['iacuc_na_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_initiation_form_completion_c.php

 // created: 2022-02-03 08:33:45
$dictionary['CAPA_CAPA']['fields']['initiation_form_completion_c']['labelValue']='Initiation Form Completion Date';
$dictionary['CAPA_CAPA']['fields']['initiation_form_completion_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['initiation_form_completion_c']['dependency']='';
$dictionary['CAPA_CAPA']['fields']['initiation_form_completion_c']['required_formula']='';
$dictionary['CAPA_CAPA']['fields']['initiation_form_completion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_investigation_report_na_c.php

 // created: 2022-02-03 08:36:25
$dictionary['CAPA_CAPA']['fields']['investigation_report_na_c']['labelValue']='Investigation Report NA';
$dictionary['CAPA_CAPA']['fields']['investigation_report_na_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['investigation_report_na_c']['dependency']='';
$dictionary['CAPA_CAPA']['fields']['investigation_report_na_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_investigation_report_complet_c.php

 // created: 2022-02-03 08:37:10
$dictionary['CAPA_CAPA']['fields']['investigation_report_complet_c']['labelValue']='Investigation Report Completion Date';
$dictionary['CAPA_CAPA']['fields']['investigation_report_complet_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['investigation_report_complet_c']['dependency']='equal($investigation_report_na_c,false)';
$dictionary['CAPA_CAPA']['fields']['investigation_report_complet_c']['required_formula']='';
$dictionary['CAPA_CAPA']['fields']['investigation_report_complet_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_capa_plan_na_c.php

 // created: 2022-02-03 08:38:49
$dictionary['CAPA_CAPA']['fields']['capa_plan_na_c']['labelValue']='CAPA Plan NA';
$dictionary['CAPA_CAPA']['fields']['capa_plan_na_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['capa_plan_na_c']['dependency']='';
$dictionary['CAPA_CAPA']['fields']['capa_plan_na_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_capa_plan_completion_date_c.php

 // created: 2022-02-03 08:40:32
$dictionary['CAPA_CAPA']['fields']['capa_plan_completion_date_c']['labelValue']='CA/PA Plan Completion Date';
$dictionary['CAPA_CAPA']['fields']['capa_plan_completion_date_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['capa_plan_completion_date_c']['dependency']='equal($capa_plan_na_c,false)';
$dictionary['CAPA_CAPA']['fields']['capa_plan_completion_date_c']['required_formula']='';
$dictionary['CAPA_CAPA']['fields']['capa_plan_completion_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_review_completion_date_c.php

 // created: 2022-02-03 08:41:40
$dictionary['CAPA_CAPA']['fields']['review_completion_date_c']['labelValue']='Review Completion Date';
$dictionary['CAPA_CAPA']['fields']['review_completion_date_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['review_completion_date_c']['dependency']='';
$dictionary['CAPA_CAPA']['fields']['review_completion_date_c']['required_formula']='';
$dictionary['CAPA_CAPA']['fields']['review_completion_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_closed_date_c.php

 // created: 2022-02-03 08:42:47
$dictionary['CAPA_CAPA']['fields']['closed_date_c']['labelValue']='Closed Date';
$dictionary['CAPA_CAPA']['fields']['closed_date_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['closed_date_c']['dependency']='';
$dictionary['CAPA_CAPA']['fields']['closed_date_c']['required_formula']='';
$dictionary['CAPA_CAPA']['fields']['closed_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_frequency_score.php

 // created: 2022-02-03 08:44:02

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_severity_score.php

 // created: 2022-02-03 08:45:02

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_contact_id_c.php

 // created: 2022-02-03 08:45:34
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['required']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['readonly']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['name']='contact_id_c';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['vname']='LBL_CAPA_OWNER_CONTACT_ID';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['type']='id';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['massupdate']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['hidemassupdate']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['no_default']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['comments']='';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['help']='';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['importable']='true';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['duplicate_merge']='enabled';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['duplicate_merge_dom_value']=1;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['audited']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['reportable']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['unified_search']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['merge_filter']='disabled';
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['pii']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['calculated']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['len']=36;
$dictionary['CAPA_CAPA']['fields']['contact_id_c']['size']='20';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_capa_owner.php

 // created: 2022-02-03 08:45:34
$dictionary['CAPA_CAPA']['fields']['capa_owner']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_contact_id1_c.php

 // created: 2022-02-03 08:46:02
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['required']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['readonly']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['name']='contact_id1_c';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['vname']='LBL_STUDY_DIRECTOR_CONTACT_ID';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['type']='id';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['massupdate']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['hidemassupdate']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['no_default']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['comments']='';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['help']='';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['importable']='true';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['duplicate_merge']='enabled';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['duplicate_merge_dom_value']=1;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['audited']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['reportable']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['unified_search']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['merge_filter']='disabled';
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['pii']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['calculated']=false;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['len']=36;
$dictionary['CAPA_CAPA']['fields']['contact_id1_c']['size']='20';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_study_director.php

 // created: 2022-02-03 08:46:02
$dictionary['CAPA_CAPA']['fields']['study_director']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_ca_andor_pa.php

 // created: 2022-02-03 08:46:30
$dictionary['CAPA_CAPA']['fields']['ca_andor_pa']['default']='^^';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_corrective_actions_proposed.php

 // created: 2022-02-03 08:47:19
$dictionary['CAPA_CAPA']['fields']['corrective_actions_proposed']['dependency']='and(equal($ca_andor_pa_dd_c,"CA"),not(equal($risk_assessment,"Minor")))';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_corrective_action_target_due.php

 // created: 2022-02-03 08:47:59
$dictionary['CAPA_CAPA']['fields']['corrective_action_target_due']['dependency']='and(equal($ca_andor_pa_dd_c,"CA"),not(equal($risk_assessment,"Minor")))';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_proposed_verification_effectiv.php

 // created: 2022-02-03 08:48:40
$dictionary['CAPA_CAPA']['fields']['proposed_verification_effectiv']['dependency']='and(equal($ca_andor_pa_dd_c,"CA"),not(equal($risk_assessment,"Minor")))';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_effectiveness_verification_due.php

 // created: 2022-02-03 08:49:14
$dictionary['CAPA_CAPA']['fields']['effectiveness_verification_due']['dependency']='and(equal($ca_andor_pa_dd_c,"CA"),not(equal($risk_assessment,"Minor")))';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_preventative_actions_proposed.php

 // created: 2022-02-03 08:49:52
$dictionary['CAPA_CAPA']['fields']['preventative_actions_proposed']['dependency']='isInList($ca_andor_pa_dd_c,createList("PA","CA and PA"))';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_preventative_action_target_due.php

 // created: 2022-02-03 08:50:28
$dictionary['CAPA_CAPA']['fields']['preventative_action_target_due']['dependency']='isInList($ca_andor_pa_dd_c,createList("PA","CA and PA"))';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_rationale_or_discussion_of_pa.php

 // created: 2022-02-03 08:51:04
$dictionary['CAPA_CAPA']['fields']['rationale_or_discussion_of_pa']['dependency']='isInList($ca_andor_pa_dd_c,createList("PA","CA and PA"))';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_proposed_verification_pa.php

 // created: 2022-02-03 08:51:38
$dictionary['CAPA_CAPA']['fields']['proposed_verification_pa']['dependency']='isInList($ca_andor_pa_dd_c,createList("PA","CA and PA"))';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_effectiveness_verification_pa.php

 // created: 2022-02-03 08:52:16
$dictionary['CAPA_CAPA']['fields']['effectiveness_verification_pa']['dependency']='isInList($ca_andor_pa_dd_c,createList("PA","CA and PA"))';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_risk_assessment_score.php

 // created: 2022-02-03 08:52:50

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_risk_assessment_score_text_c.php

 // created: 2022-02-03 08:53:34
$dictionary['CAPA_CAPA']['fields']['risk_assessment_score_text_c']['labelValue']='Risk Assessment Score';
$dictionary['CAPA_CAPA']['fields']['risk_assessment_score_text_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CAPA_CAPA']['fields']['risk_assessment_score_text_c']['enforced']='';
$dictionary['CAPA_CAPA']['fields']['risk_assessment_score_text_c']['dependency']='';
$dictionary['CAPA_CAPA']['fields']['risk_assessment_score_text_c']['required_formula']='';
$dictionary['CAPA_CAPA']['fields']['risk_assessment_score_text_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Vardefs/sugarfield_root_cause_investigation.php

 // created: 2022-02-03 08:54:17
$dictionary['CAPA_CAPA']['fields']['root_cause_investigation']['dependency']='and(equal($ca_andor_pa_dd_c,"CA"),not(equal($risk_assessment,"Minor")))';

 
?>
