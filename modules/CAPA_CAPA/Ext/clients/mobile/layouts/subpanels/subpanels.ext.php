<?php
// WARNING: The contents of this file are auto-generated.


// created: 2022-02-03 07:32:44
$viewdefs['CAPA_CAPA']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_R_TITLE',
  'context' => 
  array (
    'link' => 'capa_capa_capa_capa_1',
  ),
);

// created: 2022-02-03 07:26:23
$viewdefs['CAPA_CAPA']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CF_CAPA_FILES_TITLE',
  'context' => 
  array (
    'link' => 'capa_capa_cf_capa_files_1',
  ),
);

// created: 2022-02-03 07:34:36
$viewdefs['CAPA_CAPA']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CAPA_CAPA_CONTACTS_1_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'capa_capa_contacts_1',
  ),
);

// created: 2022-02-03 07:36:41
$viewdefs['CAPA_CAPA']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CAPA_CAPA_CONTACTS_2_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'capa_capa_contacts_2',
  ),
);

// created: 2022-02-03 07:30:32
$viewdefs['CAPA_CAPA']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CAPA_CAPA_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'context' => 
  array (
    'link' => 'capa_capa_m06_error_1',
  ),
);