<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Layoutdefs/capa_capa_cf_capa_files_1_CAPA_CAPA.php

 // created: 2022-02-03 07:26:23
$layout_defs["CAPA_CAPA"]["subpanel_setup"]['capa_capa_cf_capa_files_1'] = array (
  'order' => 100,
  'module' => 'CF_CAPA_Files',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CF_CAPA_FILES_TITLE',
  'get_subpanel_data' => 'capa_capa_cf_capa_files_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Layoutdefs/capa_capa_m06_error_1_CAPA_CAPA.php

 // created: 2022-02-03 07:30:32
$layout_defs["CAPA_CAPA"]["subpanel_setup"]['capa_capa_m06_error_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAPA_CAPA_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'capa_capa_m06_error_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Layoutdefs/capa_capa_capa_capa_1_CAPA_CAPA.php

 // created: 2022-02-03 07:32:44
$layout_defs["CAPA_CAPA"]["subpanel_setup"]['capa_capa_capa_capa_1'] = array (
  'order' => 100,
  'module' => 'CAPA_CAPA',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_R_TITLE',
  'get_subpanel_data' => 'capa_capa_capa_capa_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Layoutdefs/capa_capa_contacts_1_CAPA_CAPA.php

 // created: 2022-02-03 07:34:36
$layout_defs["CAPA_CAPA"]["subpanel_setup"]['capa_capa_contacts_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAPA_CAPA_CONTACTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'capa_capa_contacts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Layoutdefs/capa_capa_contacts_2_CAPA_CAPA.php

 // created: 2022-02-03 07:36:41
$layout_defs["CAPA_CAPA"]["subpanel_setup"]['capa_capa_contacts_2'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CAPA_CAPA_CONTACTS_2_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'capa_capa_contacts_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Layoutdefs/_overrideCAPA_CAPA_subpanel_capa_capa_capa_capa_1.php

//auto-generated file DO NOT EDIT
$layout_defs['CAPA_CAPA']['subpanel_setup']['capa_capa_capa_capa_1']['override_subpanel_name'] = 'CAPA_CAPA_subpanel_capa_capa_capa_capa_1';

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Layoutdefs/_overrideCAPA_CAPA_subpanel_capa_capa_cf_capa_files_1.php

//auto-generated file DO NOT EDIT
$layout_defs['CAPA_CAPA']['subpanel_setup']['capa_capa_cf_capa_files_1']['override_subpanel_name'] = 'CAPA_CAPA_subpanel_capa_capa_cf_capa_files_1';

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Layoutdefs/_overrideCAPA_CAPA_subpanel_capa_capa_contacts_1.php

//auto-generated file DO NOT EDIT
$layout_defs['CAPA_CAPA']['subpanel_setup']['capa_capa_contacts_1']['override_subpanel_name'] = 'CAPA_CAPA_subpanel_capa_capa_contacts_1';

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Layoutdefs/_overrideCAPA_CAPA_subpanel_capa_capa_contacts_2.php

//auto-generated file DO NOT EDIT
$layout_defs['CAPA_CAPA']['subpanel_setup']['capa_capa_contacts_2']['override_subpanel_name'] = 'CAPA_CAPA_subpanel_capa_capa_contacts_2';

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Layoutdefs/_overrideCAPA_CAPA_subpanel_capa_capa_m06_error_1.php

//auto-generated file DO NOT EDIT
$layout_defs['CAPA_CAPA']['subpanel_setup']['capa_capa_m06_error_1']['override_subpanel_name'] = 'CAPA_CAPA_subpanel_capa_capa_m06_error_1';

?>
