<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Language/en_us.customcapa_capa_cf_capa_files_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CF_CAPA_FILES_TITLE'] = 'CAPA Files';
$mod_strings['LBL_CAPA_CAPA_CF_CAPA_FILES_1_FROM_CAPA_CAPA_TITLE'] = 'CAPA Files';

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Language/en_us.customcapa_capa_m06_error_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CAPA_CAPA_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Communications';
$mod_strings['LBL_CAPA_CAPA_M06_ERROR_1_FROM_CAPA_CAPA_TITLE'] = 'Communications';

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Language/en_us.customcapa_capa_capa_capa_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_R_TITLE'] = 'CAPAs';
$mod_strings['LBL_CAPA_CAPA_CAPA_CAPA_1_FROM_CAPA_CAPA_L_TITLE'] = 'CAPAs';

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Language/en_us.customcapa_capa_contacts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CAPA_CAPA_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_CAPA_CAPA_CONTACTS_1_FROM_CAPA_CAPA_TITLE'] = 'Contacts';

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Language/en_us.customcapa_capa_contacts_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CAPA_CAPA_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_CAPA_CAPA_CONTACTS_2_FROM_CAPA_CAPA_TITLE'] = 'Contacts';

?>
<?php
// Merged from custom/Extension/modules/CAPA_CAPA/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CAPA_CAPA_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'CAPA Owner(s)';
$mod_strings['LBL_CAPA_CAPA_CONTACTS_2_FROM_CONTACTS_TITLE'] = 'Study Director(s)';
$mod_strings['LBL_CA_MGMT_ACK_NA'] = 'CA Affected Management Acknowledgement Not Applicable';
$mod_strings['LBL_CA_ANDOR_PA_DD'] = 'CAPA Type';
$mod_strings['LBL_PA_MGMT_ACK_NA'] = 'PA Affected Management Acknowledgement Not Applicable';
$mod_strings['LBL_FREQUENCY_SCORE_DD'] = 'Frequency Score';
$mod_strings['LBL_SEVERITY_SCORE_DD'] = 'Severity Score';
$mod_strings['LBL_IACUC_YES'] = 'IACUC Yes';
$mod_strings['LBL_IACUC_NA'] = 'IACUC NA';
$mod_strings['LBL_INITIATION_FORM_COMPLETION'] = 'Initiation Form Completion Date';
$mod_strings['LBL_INVESTIGATION_REPORT_COMPLET'] = 'Investigation Report Completion Date';
$mod_strings['LBL_INVESTIGATION_REPORT_NA'] = 'Investigation Report NA';
$mod_strings['LBL_CAPA_PLAN_NA'] = 'CAPA Plan NA';
$mod_strings['LBL_CAPA_PLAN_COMPLETION_DATE'] = 'CA/PA Plan Completion Date';
$mod_strings['LBL_REVIEW_COMPLETION_DATE'] = 'Review Completion Date';
$mod_strings['LBL_CLOSED_DATE'] = 'Closed Date';
$mod_strings['LBL_FREQUENCY_SCORE'] = 'Frequency Score (Obsolete)';
$mod_strings['LBL_SEVERITY_SCORE'] = 'Severity Score (Obsolete)';
$mod_strings['LBL_CAPA_OWNER'] = 'CAPA Owner (Obsolete)';
$mod_strings['LBL_STUDY_DIRECTOR'] = 'Study Director (Obsolete)';
$mod_strings['LBL_CA_ANDOR_PA'] = 'CA and/or PA (Obsolete)';
$mod_strings['LBL_RISK_ASSESSMENT_SCORE'] = 'Risk Assessment Score (Obsolete)';
$mod_strings['LBL_RISK_ASSESSMENT_SCORE_TEXT'] = 'Risk Assessment Score';
$mod_strings['LBL_RECORDVIEW_PANEL6'] = 'Progress';

?>
