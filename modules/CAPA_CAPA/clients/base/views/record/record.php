<?php
$module_name = 'CAPA_CAPA';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'CAPA_CAPA',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
                'size' => 'large',
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'date_initiated',
                'label' => 'LBL_DATE_INITIATED',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'ca_andor_pa_dd_c',
                'label' => 'LBL_CA_ANDOR_PA_DD',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'issue',
                'studio' => 'visible',
                'label' => 'LBL_ISSUE',
                'span' => 12,
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'iacuc_yes_c',
                'label' => 'LBL_IACUC_YES',
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'iacuc_na_c',
                'label' => 'LBL_IACUC_NA',
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'non_conforming_event',
                'studio' => 'visible',
                'label' => 'LBL_NON_CONFORMING_EVENT',
                'span' => 12,
              ),
              6 => 
              array (
                'readonly' => false,
                'name' => 'initial_scope',
                'studio' => 'visible',
                'label' => 'LBL_INITIAL_SCOPE',
                'span' => 12,
              ),
              7 => 
              array (
                'readonly' => false,
                'name' => 'correction',
                'studio' => 'visible',
                'label' => 'LBL_CORRECTION',
                'span' => 12,
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'frequency_score_dd_c',
                'label' => 'LBL_FREQUENCY_SCORE_DD',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'severity_score_dd_c',
                'label' => 'LBL_SEVERITY_SCORE_DD',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'risk_assessment_score_text_c',
                'label' => 'LBL_RISK_ASSESSMENT_SCORE_TEXT',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'risk_assessment',
                'label' => 'LBL_RISK_ASSESSMENT',
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL6',
            'label' => 'LBL_RECORDVIEW_PANEL6',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'initiation_form_completion_c',
                'label' => 'LBL_INITIATION_FORM_COMPLETION',
              ),
              1 => 
              array (
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'investigation_report_na_c',
                'label' => 'LBL_INVESTIGATION_REPORT_NA',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'investigation_report_complet_c',
                'label' => 'LBL_INVESTIGATION_REPORT_COMPLET',
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'capa_plan_na_c',
                'label' => 'LBL_CAPA_PLAN_NA',
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'capa_plan_completion_date_c',
                'label' => 'LBL_CAPA_PLAN_COMPLETION_DATE',
              ),
              6 => 
              array (
                'readonly' => false,
                'name' => 'review_completion_date_c',
                'label' => 'LBL_REVIEW_COMPLETION_DATE',
              ),
              7 => 
              array (
                'readonly' => false,
                'name' => 'closed_date_c',
                'label' => 'LBL_CLOSED_DATE',
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'root_cause_investigation',
                'studio' => 'visible',
                'label' => 'LBL_ROOT_CAUSE_INVESTIGATION',
                'span' => 12,
              ),
            ),
          ),
          5 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'ca_mgmt_ack_na_c',
                'label' => 'LBL_CA_MGMT_ACK_NA',
                'span' => 12,
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'corrective_actions_proposed',
                'studio' => 'visible',
                'label' => 'LBL_CORRECTIVE_ACTIONS_PROPOSED',
                'span' => 12,
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'corrective_action_target_due',
                'label' => 'LBL_CORRECTIVE_ACTION_TARGET_DUE',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'proposed_verification_effectiv',
                'studio' => 'visible',
                'label' => 'LBL_PROPOSED_VERIFICATION_EFFECTIV',
                'span' => 12,
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'effectiveness_verification_due',
                'label' => 'LBL_EFFECTIVENESS_VERIFICATION_DUE',
              ),
              6 => 
              array (
              ),
            ),
          ),
          6 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL4',
            'label' => 'LBL_RECORDVIEW_PANEL4',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'pa_mgmt_ack_na_c',
                'label' => 'LBL_PA_MGMT_ACK_NA',
                'span' => 12,
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'preventative_actions_proposed',
                'studio' => 'visible',
                'label' => 'LBL_PREVENTATIVE_ACTIONS_PROPOSED',
                'span' => 12,
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'preventative_action_target_due',
                'label' => 'LBL_PREVENTATIVE_ACTION_TARGET_DUE',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'rationale_or_discussion_of_pa',
                'studio' => 'visible',
                'label' => 'LBL_RATIONALE_OR_DISCUSSION_OF_PA',
                'span' => 12,
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'proposed_verification_pa',
                'studio' => 'visible',
                'label' => 'LBL_PROPOSED_VERIFICATION_PA',
                'span' => 12,
              ),
              6 => 
              array (
                'readonly' => false,
                'name' => 'effectiveness_verification_pa',
                'label' => 'LBL_EFFECTIVENESS_VERIFICATION_PA',
              ),
              7 => 
              array (
              ),
            ),
          ),
          7 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL5',
            'label' => 'LBL_RECORDVIEW_PANEL5',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'corrective_action_effective',
                'studio' => 'visible',
                'label' => 'LBL_CORRECTIVE_ACTION_EFFECTIVE',
                'span' => 12,
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'progress_update_ca',
                'label' => 'LBL_PROGRESS_UPDATE_CA',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'effectiveness_review_ca',
                'label' => 'LBL_EFFECTIVENESS_REVIEW_CA',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'further_corrective_action_requ',
                'label' => 'LBL_FURTHER_CORRECTIVE_ACTION_REQU',
              ),
              4 => 
              array (
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'preventative_action_effective',
                'studio' => 'visible',
                'label' => 'LBL_PREVENTATIVE_ACTION_EFFECTIVE',
                'span' => 12,
              ),
              6 => 
              array (
                'readonly' => false,
                'name' => 'progress_update_pa',
                'label' => 'LBL_PROGRESS_UPDATE_PA',
              ),
              7 => 
              array (
                'readonly' => false,
                'name' => 'effectiveness_review_pa',
                'label' => 'LBL_EFFECTIVENESS_REVIEW_PA',
              ),
              8 => 
              array (
                'readonly' => false,
                'name' => 'further_preventative_action_re',
                'label' => 'LBL_FURTHER_PREVENTATIVE_ACTION_RE',
              ),
              9 => 
              array (
              ),
              10 => 
              array (
                'readonly' => false,
                'name' => 'capa_cancellation_justificatio',
                'studio' => 'visible',
                'label' => 'LBL_CAPA_CANCELLATION_JUSTIFICATIO',
                'span' => 12,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
