<?php
// created: 2022-02-03 09:01:14
$viewdefs['CAPA_CAPA']['base']['view']['subpanel-for-contacts-capa_capa_contacts_2'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'date_initiated',
          'label' => 'LBL_DATE_INITIATED',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'issue',
          'label' => 'LBL_ISSUE',
          'enabled' => true,
          'readonly' => false,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'non_conforming_event',
          'label' => 'LBL_NON_CONFORMING_EVENT',
          'enabled' => true,
          'readonly' => false,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'initial_scope',
          'label' => 'LBL_INITIAL_SCOPE',
          'enabled' => true,
          'readonly' => false,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);