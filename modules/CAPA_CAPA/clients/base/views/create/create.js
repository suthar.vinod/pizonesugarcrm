({
    extendsFrom: 'CreateView',
    initialize: function (options) {

        this._super('initialize', [options]);        
        this.model.on("change:ca_andor_pa_dd_c", this.function_correction, this);        
    },

     /*2043 Custom Default Value*/
     function_correction: function () {
        var ca_andor_pa_dd = this.model.get('ca_andor_pa_dd_c');
        console.log('ca_andor_pa_dd ', ca_andor_pa_dd);
        if (ca_andor_pa_dd == 'PA') {
            this.model.set("correction", 'NA');
        }
    },
})