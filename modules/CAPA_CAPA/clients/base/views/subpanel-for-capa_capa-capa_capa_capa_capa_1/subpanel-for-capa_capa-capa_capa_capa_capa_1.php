<?php
// created: 2022-02-03 07:50:36
$viewdefs['CAPA_CAPA']['base']['view']['subpanel-for-capa_capa-capa_capa_capa_capa_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'date_initiated',
          'label' => 'LBL_DATE_INITIATED',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'issue',
          'label' => 'LBL_ISSUE',
          'enabled' => true,
          'readonly' => false,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);