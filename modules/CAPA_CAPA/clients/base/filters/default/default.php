<?php
// created: 2022-01-21 06:48:30
$viewdefs['CAPA_CAPA']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'ca_andor_pa_dd_c' => 
    array (
    ),
    'ca_mgmt_ack_na_c' => 
    array (
    ),
    'capa_cancellation_justificatio' => 
    array (
    ),
    'name' => 
    array (
    ),
    'capa_plan_completion_date_c' => 
    array (
    ),
    'capa_plan_na_c' => 
    array (
    ),
    'closed_date_c' => 
    array (
    ),
    'correction' => 
    array (
    ),
    'corrective_action_effective' => 
    array (
    ),
    'corrective_actions_proposed' => 
    array (
    ),
    'corrective_action_target_due' => 
    array (
    ),
    'effectiveness_review_ca' => 
    array (
    ),
    'effectiveness_review_pa' => 
    array (
    ),
    'effectiveness_verification_due' => 
    array (
    ),
    'effectiveness_verification_pa' => 
    array (
    ),
    'frequency_score_dd_c' => 
    array (
    ),
    'further_corrective_action_requ' => 
    array (
    ),
    'further_preventative_action_re' => 
    array (
    ),
    'iacuc_na_c' => 
    array (
    ),
    'iacuc_yes_c' => 
    array (
    ),
    'initial_scope' => 
    array (
    ),
    'initiation_form_completion_c' => 
    array (
    ),
    'investigation_report_complet_c' => 
    array (
    ),
    'investigation_report_na_c' => 
    array (
    ),
    'non_conforming_event' => 
    array (
    ),
    'pa_mgmt_ack_na_c' => 
    array (
    ),
    'preventative_action_effective' => 
    array (
    ),
    'preventative_actions_proposed' => 
    array (
    ),
    'preventative_action_target_due' => 
    array (
    ),
    'progress_update_ca' => 
    array (
    ),
    'progress_update_pa' => 
    array (
    ),
    'proposed_verification_effectiv' => 
    array (
    ),
    'proposed_verification_pa' => 
    array (
    ),
    'rationale_or_discussion_of_pa' => 
    array (
    ),
    'review_completion_date_c' => 
    array (
    ),
    'risk_assessment' => 
    array (
    ),
    'risk_assessment_score_text_c' => 
    array (
    ),
    'severity_score_dd_c' => 
    array (
    ),
  ),
);