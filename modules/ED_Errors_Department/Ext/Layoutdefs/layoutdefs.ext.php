<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ED_Errors_Department/Ext/Layoutdefs/m06_error_ed_errors_department_1_ED_Errors_Department.php

 // created: 2018-02-22 13:58:39
$layout_defs["ED_Errors_Department"]["subpanel_setup"]['m06_error_ed_errors_department_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'm06_error_ed_errors_department_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
