<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ED_Errors_Department/Ext/Vardefs/m06_error_ed_errors_department_1_ED_Errors_Department.php

// created: 2018-02-22 13:58:39
$dictionary["ED_Errors_Department"]["fields"]["m06_error_ed_errors_department_1"] = array (
  'name' => 'm06_error_ed_errors_department_1',
  'type' => 'link',
  'relationship' => 'm06_error_ed_errors_department_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_ED_ERRORS_DEPARTMENT_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_ed_errors_department_1m06_error_ida',
);

?>
<?php
// Merged from custom/Extension/modules/ED_Errors_Department/Ext/Vardefs/sugarfield_manager_c.php

 // created: 2018-05-29 18:03:08
$dictionary['ED_Errors_Department']['fields']['manager_c']['labelValue']='Manager';
$dictionary['ED_Errors_Department']['fields']['manager_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ED_Errors_Department']['fields']['manager_c']['enforced']='';
$dictionary['ED_Errors_Department']['fields']['manager_c']['dependency']='';

 
?>
