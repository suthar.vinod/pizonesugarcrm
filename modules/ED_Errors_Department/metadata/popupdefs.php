<?php
$popupMeta = array (
    'moduleMain' => 'ED_Errors_Department',
    'varName' => 'ED_Errors_Department',
    'orderBy' => 'ed_errors_department.name',
    'whereClauses' => array (
  'name' => 'ed_errors_department.name',
),
    'searchInputs' => array (
  1 => 'name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
),
);
