<?php
class CustomConfiguratorController extends ConfiguratorController
{
    public function action_saveconfig()
    {
      $this->allowKeysSaveConfig[] = "observation_notification_email";
      parent::action_saveconfig();
    }
}
