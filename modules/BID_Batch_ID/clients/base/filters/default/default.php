<?php
// created: 2022-05-05 09:17:35
$viewdefs['BID_Batch_ID']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'assigned_user_name' => 
    array (
    ),
    'control_quantity_c' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'dose_route_c' => 
    array (
    ),
    'extract_duration_hours_c' => 
    array (
    ),
    'extract_condition_temp_c' => 
    array (
    ),
    'in_life_duration_c' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'name' => 
    array (
    ),
    'description' => 
    array (
    ),
    'other_dose_route_c' => 
    array (
    ),
    'scheduled_date_c' => 
    array (
    ),
    'status_c' => 
    array (
    ),
  ),
);