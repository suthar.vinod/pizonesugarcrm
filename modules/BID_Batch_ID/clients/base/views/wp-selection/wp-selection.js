({
    extendsFrom: "MultiSelectionListView",
    allowedFields: ["name"],
    initialize: function () {
        this._super("initialize", arguments);
        this.collection.reset();
    },
    render: function () {
        this._super("render", arguments);
        this.layout.$el.parent().addClass("span14 wp-selection");
        this.layout.$el.parent().css("width","100%");
    },
    addActions: function () {
        this._super("addActions");
        this.rightColumns = [];
        this.meta.panels[0].fields = _.filter(this.meta.panels[0].fields, function filterFields(field) {
            return this.allowedFields.indexOf(field.name) !== -1;
        }.bind(this));
    }
});