({
    extendsFrom: 'RecordView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.onload_function, this);
    },

    onload_function: function () {
        var status = this.model.get('status_c');
        var batchId = this.model.get('id');
        // console.log("Status===>", status);
        if (status !== "") {
            var url = app.api.buildURL("setStatusReadOnly");
            var method = "create";
            var data = {
                module: 'BID_Batch_ID',
                batchId: batchId
            };
            var callback = {
                success: _.bind(function successCB(res) {
                    if(res !== 0){
                        var setVal = 'Closed';
                        this.model.set('status_c',setVal);
                    }
                    this.model.set('status_readonly_c',res);
                    console.log("status_readonly_c===>",this.model.get("status_readonly_c"));
                }, this)
            };
            app.api.call(method, url, data, callback);
        }
    },
})