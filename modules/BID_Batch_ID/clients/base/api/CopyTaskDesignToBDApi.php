<?php
if (!defined('sugarEntry') || !sugarEntry) {
  die('Not A Valid Entry Point');
}
class CopyTaskDesignToBDApi extends SugarApi
{
  public function registerApiRest()
  {
    return array(
      'MyGetEndpointNum' => array(
        'reqType' => 'GET',
        'noLoginRequired' => false,
        'path' => array('BID_Batch_ID', '?', '?', '?', 'copy_task_design_to_bd'),
        'pathVars' => array('module', 'CopyFromwpid', 'CopyTobdid', 'SelectedGD'),
        'method' => 'copy_task_design_to_bd',
        'shortHelp' => '',
        'longHelp' => '',
      ),
    );
  }
  /**#2265 : 29March 2022 : Below code will copy the task design records from Work product and create new records basend on copy tsd's into selected batch design */
  public function copy_task_design_to_bd($api, $args)
  {
    global $db, $current_user;
    $module = $args['module'];
    /**Copy from current wp **/
    $CopyFromwpid = $args['CopyFromwpid'];
    /**Copy to selected bd **/
    $CopyTobdid = $args['CopyTobdid'];
    $SelectedGD = $args['SelectedGD'];
    $currentWPBean = BeanFactory::retrieveBean('M03_Work_Product', $CopyFromwpid);
    $copytoBDBean = BeanFactory::retrieveBean('BID_Batch_ID', $CopyTobdid);
    $coytoBDname = $copytoBDBean->name;
    $newworkProductId = $copytoBDBean->bid_batch_id_m03_work_product_1bid_batch_id_ida;
    $currentWPBean->load_relationship('m03_work_product_taskd_task_design_1');
    $relatedTD = $currentWPBean->m03_work_product_taskd_task_design_1->get();

    /**Get all the tsd id's using loop and will assign the values from existing into new created records */
    foreach ($relatedTD as $TDId) {
      $clonedBean = BeanFactory::getBean('TaskD_Task_Design', $TDId);
      $type = $clonedBean->type_2;
      $clonedBean->load_relationship("gd_group_design_taskd_task_design_1");
      /**Get the group design record id from task design record */
      $GDIds = $clonedBean->gd_group_design_taskd_task_design_1->get();
      $GDId = $GDIds[0];
       /** #2428 : 04 May 2022In addition, if Group Design was not null in the pop up at the time of paste, all of the TDs with Type = Plan - TS and Group Design matching what was chosen in the pop up should also copy to the Batch ID.  */
      if (($type == "Actual SP") || ($SelectedGD!='00' && $GDId!='' && $GDId == $SelectedGD)) {
        $other_equipment = $clonedBean->other_equipment_c;
        $clonedBean->load_relationship("taskd_task_design_taskd_task_design_1");
        $clonedBean->load_relationship("bid_batch_id_taskd_task_design_1");
        $date_entered = date("Y-m-d H:i:s", time());
        /* Custom Query to get Task design Ids */
        $sqlGetTaskDesignIds = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_ida as TDId
                                    FROM taskd_task_design_taskd_task_design_1_c 
                                    where taskd_task_design_taskd_task_design_1taskd_task_design_idb='$TDId' and deleted=0";
        $resultsSqlGetTaskDesignIds = $db->query($sqlGetTaskDesignIds);
        $rowSqlGetTaskDesignIds = $db->fetchByAssoc($resultsSqlGetTaskDesignIds);
        $taskDesignIds = $rowSqlGetTaskDesignIds['TDId'];
        $clonedBean->id  = create_guid();
        $clonedBean->new_with_id = true;
        $clonedBean->parent_td_id_c = $TDId;
        $clonedBean->relative = $clonedBean->relative;
        $clonedBean->category = $clonedBean->category;
        $clonedBean->order_2_c = $clonedBean->order_2_c;
        $clonedBean->equipment_required = $clonedBean->equipment_required;
        $clonedBean->task_type = $clonedBean->task_type;
        $clonedBean->type_of_personnel_2_c = $clonedBean->type_of_personnel_2_c;
        $clonedBean->duration_integer_min_c = $clonedBean->duration_integer_min_c;
        $clonedBean->description = $clonedBean->description;
        $clonedBean->other_equipment_c = $other_equipment;
        $clonedBean->date_entered = $date_entered;
        $copyingTaskDesignIds[] = $clonedBean->id;
        $p1arr[$TDId] = $taskDesignIds;
        $a1arr[$clonedBean->id] = $TDId;
        $b1arr[$TDId] = $clonedBean->id;
        $clonedBean->u_units_id_c = $clonedBean->u_units_id_c;
        $clonedBean->start_integer = $clonedBean->start_integer;
        $clonedBean->end_integer = $clonedBean->end_integer;
        $clonedBean->phase_c = $clonedBean->phase_c;
        $clonedBean->custom_task = $clonedBean->custom_task;
        $clonedBean->standard_task = $clonedBean->standard_task;
        $clonedBean->type_2 = "Actual SP";
        $phase = $clonedBean->phase_c;
        $unit_Bean = BeanFactory::getBean('U_Units', $clonedBean->u_units_id_c);
        $unit_Bean_name = $unit_Bean->name;
        if ($phase != '') {
          $phaseVal = ' ' . $phase;
        } else {
          $phaseVal = '';
        }

        $clonedBean->save();
        if ($CopyTobdid != '') {
          $clonedBean->bid_batch_id_taskd_task_design_1->add($CopyTobdid);
        }

        if ($type == "Actual SP" || $type == "Plan") {
          $task_type = $clonedBean->task_type;
          $custom_task = $clonedBean->custom_task;
          $standard_task = $clonedBean->standard_task;

          if ($task_type == 'Custom') {
            $standard_task = '';
            $standard_task_val = $custom_task;
          } else if ($task_type == 'Standard') {
            $custom_task = '';
            $standard_task_val = $standard_task;
          }
          // $clonedBean->name = $wpName . ' ' . $coytoBDname . $phaseVal . ' ' . $standard_task_val;
          $clonedBean->name = $coytoBDname . $phaseVal . ' ' . $standard_task_val;
          if ($copytoBDBean->scheduled_date_c != '' && $clonedBean->relative == "NA") {
            $scheduled_date       = strtotime($copytoBDBean->scheduled_date_c);
            $clonedBean->actual_datetime = date("Y-m-d 17:00:00", $scheduled_date);
            $copytoBDSDdate       =  $clonedBean->actual_datetime;
            //$clonedBean->save();             
          }
          if ($copytoBDSDdate != "") {
            if ($unit_Bean_name == "Day") {
              if ($clonedBean->relative == "1st Tier") {
                $result = date($copytoBDSDdate);
                $result = strtotime($result . '+' . $clonedBean->start_integer . ' days');
                $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);

                $result = date($copytoBDSDdate);
                $result = strtotime($result . '+' . $clonedBean->end_integer . ' days');
                $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);               
              }
            } else if ($unit_Bean_name == "Hour") {
              if ($clonedBean->relative == "1st Tier") {
                $result = date($copytoBDSDdate);
                $result = strtotime($result . '+' . $clonedBean->start_integer . ' hours');
                $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);                
                $result = date($copytoBDSDdate);
                $result = strtotime($result . '+' . $clonedBean->end_integer . ' hours');
                $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
              }
            } else if ($unit_Bean_name == "Minute") {

              if ($clonedBean->relative == "1st Tier") {
                $result = date($copytoBDSDdate);
                $result = strtotime($result . '+' . $clonedBean->start_integer . ' minutes');
                $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);

                $result = date($copytoBDSDdate);
                $result = strtotime($result . '+' . $clonedBean->end_integer . ' minutes');
                $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
              }
            }

            if (($clonedBean->relative == "1st Tier" || $clonedBean->relative == "NA")) {
              $clonedBean->planned_start_datetime_2nd = null;
              $clonedBean->planned_end_datetime_2nd = null;
            }
            if (($clonedBean->relative == "2nd Tier" || $clonedBean->relative == "NA")) {
              $clonedBean->planned_start_datetime_1st = null;
              $clonedBean->planned_end_datetime_1st = null;
            }
          }
          $clonedBean->save();
        }
        /** Save audit log for all updated fields*/
        $source = '{"subject":{"_type":"logic-hook","class":"CopyTaskDesignToGD","method":"CopyTaskDesignToGD"},"attributes":[]}';
        if ($clonedBean->name != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"name","datetime","","' . $clonedBean->name . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->type_2 != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"type_2","datetime","","' . $clonedBean->type_2 . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->relative != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"relative","datetime","","' . $clonedBean->relative . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->category != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"category","datetime","","' . $clonedBean->category . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->type_2 != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"type_2","datetime","","' . $clonedBean->type_2 . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->task_type != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"task_type","datetime","","' . $clonedBean->task_type . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->standard_task != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"standard_task","datetime","","' . $clonedBean->standard_task . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->start_integer != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"start_integer","datetime","","' . $clonedBean->start_integer . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->end_integer != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"end_integer","datetime","","' . $clonedBean->end_integer . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);
          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->time_window != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"time_window","datetime","","' . $clonedBean->time_window . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->integer_units != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"integer_units","datetime","","' . $clonedBean->integer_units . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->planned_start_datetime_1st != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_start_datetime_1st","datetime","","' . $clonedBean->planned_start_datetime_1st . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }
        
        if ($clonedBean->bid_batch_id_taskd_task_design_1_name != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"bid_batch_id_taskd_task_design_1_name","datetime","","' . $clonedBean->bid_batch_id_taskd_task_design_1_name . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->custom_task != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"custom_task","datetime","","' . $clonedBean->custom_task . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->order_2_c != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"order_2_c","datetime","","' . $clonedBean->order_2_c . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->equipment_required != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"equipment_required","datetime","","' . $clonedBean->equipment_required . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->other_equipment_c != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"other_equipment_c","datetime","","' . $clonedBean->other_equipment_c . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->phase_c != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"phase_c","datetime","","' . $clonedBean->phase_c . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->description != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"description","datetime","","' . $clonedBean->description . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->type_of_personnel_2_c != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"type_of_personnel_2_c","datetime","","' . $clonedBean->type_of_personnel_2_c . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->duration_integer_min_c != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"duration_integer_min_c","datetime","","' . $clonedBean->duration_integer_min_c . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }
      }
    }
     /** Delete the audit log if name field having same value in before and after string */
     $del_query = "Select * from `taskd_task_design_audit` WHERE `field_name`='name' AND `parent_id`='" . $clonedBean->id . "' ";
     $sql_DeleteAudit = $db->query($del_query);
     if ($sql_DeleteAudit->num_rows > 0) {
       while ($fetchAuditLog = $db->fetchByAssoc($sql_DeleteAudit)) {
         $before_val = $fetchAuditLog['before_value_string'];
         $after_val = $fetchAuditLog['after_value_string'];
         /** To ignore space we are using str replace so exact matching record will be delete */
         $before_val_st = str_replace(' ', '', $before_val);
         $after_val_st = str_replace(' ', '', $after_val);

         if ($before_val_st == $after_val_st) {
           $recordID = $fetchAuditLog['id'];
           $sql_Delete = "DELETE from `taskd_task_design_audit` WHERE `field_name`='name' AND `parent_id`='" . $clonedBean->id . "' AND id = '" . $recordID . "'";
           $db->query($sql_Delete);
         }
       }
     }
     /** delete audit log for integer units if both are haveing blank value */
     $del_query_int = "Select * from `taskd_task_design_audit` WHERE `field_name`='integer_units' AND `parent_id`='" . $clonedBean->id . "' ";
     $sql_DeleteAudit_int = $db->query($del_query_int);
     if ($sql_DeleteAudit_int->num_rows > 0) {
       while ($fetchAuditLog = $db->fetchByAssoc($sql_DeleteAudit_int)) {
         $before_val_int = $fetchAuditLog['before_value_string'];
         $after_val_int = $fetchAuditLog['after_value_string'];
         /** To ignore space we are using str replace so exact matching record will be delete */
         $before_val_st_int = str_replace(' ', '', $before_val_int);
         $after_val_st_int = str_replace(' ', '', $after_val_int);

         if ($before_val_st_int == $after_val_st_int) {
           $recordID = $fetchAuditLog['id'];
           $sql_Delete = "DELETE from `taskd_task_design_audit` WHERE `field_name`='integer_units' AND `parent_id`='" . $clonedBean->id . "' AND id = '" . $recordID . "'";
           $db->query($sql_Delete);
         }
       }
     }
    /**Relationship of TSD with TSD */
    if (count($p1arr) > 0) {
      foreach ($p1arr as $key => $value) {
        $newBeanId = $b1arr[$key];
        $newBean = BeanFactory::getBean('TaskD_Task_Design', $newBeanId);
        $newBean->load_relationship("taskd_task_design_taskd_task_design_1_right");
        $toBeRelateID = array_search($value, $a1arr);

        if ($toBeRelateID != "") {
          $uniqId = create_guid();
          $sqlInsertRelationship = "INSERT INTO `taskd_task_design_taskd_task_design_1_c` 
              (`id`, `date_modified`, `deleted`, `taskd_task_design_taskd_task_design_1taskd_task_design_ida`, `taskd_task_design_taskd_task_design_1taskd_task_design_idb`) 
              VALUES ('" . $uniqId . "', now(), '0', '" . $toBeRelateID . "', '" . $newBeanId . "')";
          $resultsSqlGetTaskDesignIds = $db->query($sqlInsertRelationship);
        }
      }
    }

    $currentWPBean2 = BeanFactory::retrieveBean('M03_Work_Product', $CopyFromwpid);
    $currentWPBean2->load_relationship('m03_work_product_taskd_task_design_1');
    $relatedTD1 = $currentWPBean2->m03_work_product_taskd_task_design_1->get();


    foreach ($relatedTD1 as $TDId1) {
      $tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $TDId1);
      $u_units = $tdBean->u_units_id_c;
      $unit_Bean = BeanFactory::getBean('U_Units', $u_units);
      $unit_Bean_name = $unit_Bean->name;      
      if ($unit_Bean_name == "Day") {
        $integer_u = 'days';
      } elseif ($unit_Bean_name == "Hour") {
        $integer_u = 'hours';
      } else {
        $integer_u = 'minutes';
      }      
      if ($tdBean->relative == "1st Tier") {
        $tdBean1 = BeanFactory::retrieveBean('TaskD_Task_Design', $tdBean->id);
        $tdBean1->load_relationship('taskd_task_design_taskd_task_design_1');
        $taskDesignId1 = $tdBean1->taskd_task_design_taskd_task_design_1->get();

        $Get_2nd_tier = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_idb FROM  `taskd_task_design_taskd_task_design_1_c` WHERE `taskd_task_design_taskd_task_design_1taskd_task_design_ida` = '" . $tdBean->id . "' AND deleted = 0;";
        $Get_Result = $db->query($Get_2nd_tier);

        if ($Get_Result->num_rows > 0) {
          while ($fetchResult = $db->fetchByAssoc($Get_Result)) {
            $recordID = $fetchResult['taskd_task_design_taskd_task_design_1taskd_task_design_idb'];            
            $tdBean2 = BeanFactory::retrieveBean('TaskD_Task_Design', $recordID);           
            if ($tdBean->planned_start_datetime_1st != "") {
              $result = date($tdBean->planned_start_datetime_1st);
              $result = strtotime($result . '+' . $tdBean2->start_integer . ' ' . $integer_u);
              $tdBean2->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
              if (date("Y", $result) != "1970") {
                $tdBean2->single_date_c = date("Y-m-d", $result);
              }
            } else {
              $tdBean2->planned_start_datetime_2nd = null;
            }
            
            if ($tdBean->planned_end_datetime_1st != "") {
              $result = date($tdBean->planned_end_datetime_1st);
              $result = strtotime($result . '+' . $tdBean2->end_integer . ' ' . $integer_u);
              $tdBean2->planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean2->planned_end_datetime_2nd = null;
            }
            $tdBean2->save();
          }
        }
      }
    }
    if (count($copyingTaskDesignIds) > 0) {
      return "Task Designs have been copied successfully to the selected Batch ID.";
    } else {
      return "Error! There is no task designs linked to selected work product. Please link it to a work product prior to copying.";
    }
  }
}
