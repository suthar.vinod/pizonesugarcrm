<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class setStatusReadOnlyApi extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'MyGetEndpoint' => array(
                'reqType'         => 'POST',
                'noLoginRequired' => false,
                'path'            => array('setStatusReadOnly'),
                'pathVars'        => array('setStatusReadOnly'),
                'method'          => 'setStatusReadOnly',
                'shortHelp'       => '',
                'longHelp'        => '',
            ),
        );
    }

    public function setStatusReadOnly($api, $args)
    {
        global $current_user, $db;
        $module  = $args['module'];
        $batchId = $args['batchId'];

        $batchIdBean = BeanFactory::getBean($module, $batchId);
        // If relationship is loaded
        $batchIdBean->load_relationship('bid_batch_id_m03_work_product_1');
        $linkedWP     = $batchIdBean->bid_batch_id_m03_work_product_1->get();
        // $GLOBALS['log']->fatal("linkedWP from api====>". print_r($linkedWP,1));
        $count = 0;
        foreach ($linkedWP as $relatedWP) {
            $WPBean = BeanFactory::getBean('M03_Work_Product', $relatedWP);
            if($WPBean->work_product_status_c == 'Archived'){
                $count++;
                // $GLOBALS['log']->fatal("count if". $count);
            }else{
                $count = 0;
                break;
            }
        }
        // if($count !== 0){
        //     $batchIdBean->status_c = "Closed";
        // }
        // $batchIdBean->status_readonly_c = $count;
        // $batchIdBean->save();
        return $count;
    }
}
