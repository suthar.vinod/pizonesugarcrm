<?php
$viewdefs['BID_Batch_ID']['base']['layout']['wp-selection'] = array(
    'type' => 'selection-list',
    'components' => array(
        array(
            'layout' => array(
                'type' => 'base',
                'name' => 'main-pane',
                'css_class' => 'main-pane span12',
                'components' => array(
                    array(
                        'layout' => array(
                            'type' => 'filterpanel',
                            'availableToggles' => array(),
                            'filter_options' => array(
                                'stickiness' => false,
                            ),
                            'components' => array(
                                array(
                                    'layout'     => 'filter',
                                    'loadModule' => 'Filters',
                                ),
                                array(
                                    'view' => 'filter-rows',
                                ),
                                array(
                                    'view' => 'filter-actions',
                                ),
                                array(
                                    'view' => 'wp-selection',
                                    'context'    => array(
                                        'module' => 'M03_Work_Product',
                                    ),
                                    'loadModule' => 'BID_Batch_ID',
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);
