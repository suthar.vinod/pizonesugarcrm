<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class BID_animalCountAfterRelationDeleteHook{
	function UpdateAnimalCountdownValues($bean, $event, $arguments){
		global $db;
		$workProductId	= "";
		$onStudyCount	= 0;
		$onStudyBackupCount = 0;
		if($bean->id!=""){
			$bid_id =$bean->id; // Work Product Enrollment ID
			//Query to get the Work Product ID	
			$bid_sql = "SELECT * FROM `bid_batch_id_m03_work_product_1_c` AS BID_WP  
					WHERE BID_WP.`bid_batch_id_m03_work_product_1bid_batch_id_ida`='".$bid_id."' ORDER BY `BID_WP`.`date_modified` DESC limit 1";
			$bid_exec = $db->query($bid_sql);
			if ($bid_exec->num_rows > 0) 
			{
				$result = $db->fetchByAssoc($bid_exec);
				$workProductId = $result['bid_batch_id_m03_work_product_1m03_work_product_idb'];
				if($workProductId!="4a529aae-5165-3149-6db7-5702c7b98a5e" && $workProductId!="811ae58e-9281-2755-25d5-5702c7252696")
				{
					//Update Work_Product Bean to update the Animal Countdown Value
					$wp_bean = BeanFactory::getBean('M03_Work_Product', $workProductId);
					$GLOBALS['log']->fatal('WP BID animal Count Down delete=='.$workProductId);
					//$wp_bean->save();
					$previousCount 		= $wp_bean->primary_animals_countdown_c;
					$previousBlanketCount = $wp_bean->blanket_animals_remaining_c;
					$animalCountValue  = $this->calculateAnimal($bid_id,$wp_bean,$previousCount,$previousBlanketCount,$event,$arguments['related_id']);
					 
				}
			} 
		}
	}
	
	function calculateAnimal($wp_bid,$wpBean,$previousAnimalCount,$previousBlanketAnimalCount,$event,$related_id){
		
		global $db,$current_user;
		$wpBeanID = $wpBean->id;
		if($wpBeanID!="" && ($wpBeanID!="4a529aae-5165-3149-6db7-5702c7b98a5e" && $wpBeanID!="811ae58e-9281-2755-25d5-5702c7252696")){
			//$GLOBALS['log']->fatal('WP:Update Animal countdown.');
			$workProductId 				= $wpBeanID;
			$onStudyWithoutTSCount		= 0;
			$onStudyWithTSCount			= 0;
			$totalOnStudyCount			= 0;
			$onStudyBackupWithTSCount	= 0;
			$onStudyBackupWithoutTSCount = 0;
			$totalOnStudyBackypCount	=  0;
			$WPTS 						= array();
			// Query to get On_Study/On_Study Transferred Count
			 
			
			/*Query to get total number of onStudy/onStudyTransferred Enrollment status Without Test System*/
			$studyWithoutTS_sql = "SELECT  WP_WPE.`m03_work_p9bf5ollment_idb` 
			FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE 
			LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p9bf5ollment_idb`=WPE_CUST.id_c 
			LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p9bf5ollment_idb`=TS.anml_anima9941ollment_idb 
			WHERE WP_WPE.`m03_work_p7d13product_ida`='".$workProductId."' AND 
			(WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') AND WP_WPE.deleted=0 AND (TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NULL )"; 
			//TS.deleted=1 OR TS.deleted IS NULL OR
			
			$studyWithoutTS_exec = $db->query($studyWithoutTS_sql);
             
			if ($studyWithoutTS_exec->num_rows > 0) 
			{
				$onStudyWithoutTSCount = $studyWithoutTS_exec->num_rows; //total rows without TS

				while($fetchWOTS = $db->fetchByAssoc($studyWithoutTS_exec)) {
					$WPTS[]	= $fetchWOTS['m03_work_p9bf5ollment_idb'];
				}
			}
			
			/*Query to get total number of onStudy/onStudyTransferred Enrollment status With Test System*/
			$studyWithTS_sql = "SELECT  WP_WPE.`m03_work_p9bf5ollment_idb` 
			FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE 
			LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p9bf5ollment_idb`=WPE_CUST.id_c 
			LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p9bf5ollment_idb`=TS.anml_anima9941ollment_idb 
			WHERE WP_WPE.`m03_work_p7d13product_ida`='".$workProductId."' 
			AND (WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') AND TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NOT NULL  AND WP_WPE.deleted=0 AND TS.deleted=0 GROUP BY TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida";
                        
			$studyWithTS_exec = $db->query($studyWithTS_sql);
                        
			if ($studyWithTS_exec->num_rows > 0) 
			{
				$onStudyWithTSCount = $studyWithTS_exec->num_rows; //total rows with TS
				while($fetchWTS = $db->fetchByAssoc($studyWithTS_exec)) {
					$WPTS[]	= $fetchWTS['m03_work_p9bf5ollment_idb'];
				}
			}
 

			/*=========================================================*/
			/*=========================================================*/			

			/*Query to get total number of onStudy/onStudyTransferred Enrollment status Without Test System*/
			$blanket_sql = "SELECT  WP_WPE.`m03_work_p90c4ollment_idb` 
						FROM `m03_work_product_wpe_work_product_enrollment_2_c` AS WP_WPE 
						LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p90c4ollment_idb`=WPE_CUST.id_c 
						LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p90c4ollment_idb`=TS.anml_anima9941ollment_idb 
					WHERE WP_WPE.`m03_work_p9f23product_ida`='".$workProductId."' AND 
						(WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') 
						AND WP_WPE.deleted=0 AND (TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NULL )"; 
			//TS.deleted=1 OR TS.deleted IS NULL OR
			
			$blanket_exec = $db->query($blanket_sql);
             
			if ($blanket_exec->num_rows > 0) 
			{
				//$onStudyWithoutTSCount = $studyWithoutTS_exec->num_rows; //total rows without TS

				while($fetchB1 = $db->fetchByAssoc($blanket_exec)) {
					//$GLOBALS['log']->fatal('WP===>:'.$fetchB1['m03_work_p90c4ollment_idb']);
					$WPTS[]	= $fetchB1['m03_work_p90c4ollment_idb'];
				}
			}
			
			/*Query to get total number of onStudy/onStudyTransferred Enrollment status With Test System*/
			$blanket2_sql = "SELECT  WP_WPE.`m03_work_p90c4ollment_idb` 
							FROM `m03_work_product_wpe_work_product_enrollment_2_c` AS WP_WPE 
								LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p90c4ollment_idb`=WPE_CUST.id_c 
								LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p90c4ollment_idb`=TS.anml_anima9941ollment_idb 
							WHERE WP_WPE.`m03_work_p9f23product_ida`='".$workProductId."' 
								AND (WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred')
								AND TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NOT NULL  AND WP_WPE.deleted=0 AND TS.deleted=0 
							GROUP BY TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida";
                        
			$blanket2_exec = $db->query($blanket2_sql);
                        
			if ($blanket2_exec->num_rows > 0) 
			{
				//$onStudyWithTSCount = $studyWithTS_exec->num_rows; //total rows with TS
				while($fetchB2 = $db->fetchByAssoc($blanket2_exec)) {
					//$GLOBALS['log']->fatal('WP22===>:'.$fetchB2['m03_work_p90c4ollment_idb']);
					$WPTS[]	= $fetchB2['m03_work_p90c4ollment_idb'];
				}
			}


				/*Changes according to Ticket # 2269 */
				if($wp_bid!=""){
					/*Query to get total number of onStudy/onStudyTransferred Enrollment status Without Test System*/
					$studyWithoutTS_BIDsql = "SELECT  BID_WPE.bid_batch_f386ollment_idb
					FROM `bid_batch_id_wpe_work_product_enrollment_1_c` AS BID_WPE 
						LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON BID_WPE.`bid_batch_f386ollment_idb`=WPE_CUST.id_c 
						LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON BID_WPE.`bid_batch_f386ollment_idb`=TS.anml_anima9941ollment_idb 
					WHERE BID_WPE.`bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida`='".$wp_bid."' 
						AND  (WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') 
						AND BID_WPE.deleted=0 AND (TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NULL ) "; 
					//TS.deleted=1 OR TS.deleted IS NULL OR
					$GLOBALS['log']->fatal('151 SQL :' .$studyWithoutTS_BIDsql); 
					$studyWithoutTS_BIDexec = $db->query($studyWithoutTS_BIDsql);
	
					if ($studyWithoutTS_BIDexec->num_rows > 0) 
					{
						$onStudyWithoutTSBIDCount = $studyWithoutTS_BIDexec->num_rows; //total rows without TS
						while($fetchWOTS_BID = $db->fetchByAssoc($studyWithoutTS_BIDexec)) {
							$WPTS[]	= $fetchWOTS_BID['bid_batch_f386ollment_idb'];
						}
					}
	
					/*Query to get total number of onStudy/onStudyTransferred Enrollment status With Test System*/
					$studyWithTS_BIDsql = "SELECT BID_WPE.bid_batch_f386ollment_idb
					FROM `bid_batch_id_wpe_work_product_enrollment_1_c` AS BID_WPE 
					LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON BID_WPE.`bid_batch_f386ollment_idb`=WPE_CUST.id_c 
					LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON BID_WPE.`bid_batch_f386ollment_idb`=TS.anml_anima9941ollment_idb 
					WHERE BID_WPE.`bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida`='".$wp_bid."' 
					AND  (WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') AND TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NOT NULL  AND BID_WPE.deleted=0 AND TS.deleted=0 GROUP BY TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida";
					
					$studyWithTS_BIDexec = $db->query($studyWithTS_BIDsql);
	
					if ($studyWithTS_BIDexec->num_rows > 0) 
					{
						$onStudyWithTSBIDCount = $studyWithTS_BIDexec->num_rows; //total rows with TS
						while($fetchWTS_BID = $db->fetchByAssoc($studyWithTS_BIDexec)) {
							$GLOBALS['log']->fatal('btid== :' .$fetchWTS_BID['bid_batch_f386ollment_idb']); 

							$WPTS[]	= $fetchWTS_BID['bid_batch_f386ollment_idb'];
						}
					}
	
				}			
				$GLOBALS['log']->fatal('WPTS :' . print_r($WPTS,1)); 
	 
				if (($key = array_search($related_id, $WPTS)) !== false) {
					unset($WPTS[$key]);
				}
				 
			/*=========================================================*/
			/*=========================================================*/ 
			$totalOnStudyCount  = count(array_unique($WPTS)); //$onStudyWithTSCount+$onStudyWithoutTSCount; //Total Count of OnStudy/OnStudyTransferred
			//$totalOnStudyCount  = $onStudyWithTSCount+$onStudyWithoutTSCount; //Total Count of OnStudy/OnStudyTransferred
	
	
			// Query to get On_Backup Study Count
			/*Query to get total number of onStudyBackup Enrollment status Without Test System*/            
           $studyBackupWithoutTSsql = "SELECT  WP_WPE.`m03_work_p9bf5ollment_idb` 
							FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE 
							LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p9bf5ollment_idb`=WPE_CUST.id_c 
							LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p9bf5ollment_idb`=TS.anml_anima9941ollment_idb 
							WHERE WP_WPE.`m03_work_p7d13product_ida`='".$workProductId."' AND 
							(WPE_CUST.enrollment_status_c='On Study Backup') AND WP_WPE.deleted=0 AND 
							(TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NULL )"; 
            //TS.deleted=1 OR TS.deleted IS NULL OR             
			$StudyBckpWithoutTS_exec = $db->query($studyBackupWithoutTSsql);
            $onStudyBackupCount = '0';
			if ($StudyBckpWithoutTS_exec->num_rows > 0) 
			{
				$onStudyBackupWithoutTSCount = $StudyBckpWithoutTS_exec->num_rows; //total Backup rows without TS
			}
		   
		   /*Query to get total number of onStudyBackup Enrollment status With Test System*/   
		   $studyBackupWithTSsql = "SELECT  WP_WPE.`m03_work_p9bf5ollment_idb` 
									FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE 
									LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p9bf5ollment_idb`=WPE_CUST.id_c 
									LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p9bf5ollment_idb`=TS.anml_anima9941ollment_idb 
									WHERE WP_WPE.`m03_work_p7d13product_ida`='".$workProductId."' AND (WPE_CUST.enrollment_status_c='On Study Backup') 
									AND  TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NOT NULL 
									AND WP_WPE.deleted=0 AND TS.deleted=0 GROUP BY TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida";
                        
			$StudyBckpWithTS_exec = $db->query($studyBackupWithTSsql);
            
			if ($StudyBckpWithTS_exec->num_rows > 0) 
			{
				$onStudyBackupWithTSCount = $StudyBckpWithTS_exec->num_rows; //total Backup rows with TS
			}
			$totalOnStudyBackypCount  = $onStudyBackupWithTSCount+$onStudyBackupWithoutTSCount; //Total Count of OnStudyBackup
			
			// Get Total value from Bean
			$totalPrimary	= $wpBean->total_animals_used_primary_c;
			$totalBackup	= $wpBean->total_animals_used_backup_c;
			$GLOBALS['log']->fatal('totalPrimary :' . $totalPrimary);
			$GLOBALS['log']->fatal('totalOnStudyCount :' . $totalOnStudyCount);
			//Calculate countdown Value by subtracting onStudy/Backup value from Total value 
			$Primary_Animals_Countdown	= $totalPrimary-$totalOnStudyCount;
			//$Backup_Animals_Countdown	= $totalBackup-$totalOnStudyBackypCount;
			
			$GLOBALS['log']->fatal('New Primary_Animals_Countdown :' . $Primary_Animals_Countdown);
			//$GLOBALS['log']->fatal('New Primary_Animals_Countdown :' . $Primary_Animals_Countdown);
			
			$selectcheckQuery = "SELECT `primary_animals_countdown_c`,`blanket_animals_remaining_c` FROM m03_work_product_cstm WHERE id_c='".$workProductId."'";
			$resultCheckPrimary =  $db->query($selectcheckQuery);
			if ($resultCheckPrimary->num_rows > 0) 
			{
				while($fetchPrimary = $db->fetchByAssoc($resultCheckPrimary)) {
					$previousAnimalCount	= $fetchPrimary['primary_animals_countdown_c'];
					$previousBlanketAnimalCount	= $fetchPrimary['blanket_animals_remaining_c'];
					
				}
			}
			
			
			if ($previousAnimalCount != $Primary_Animals_Countdown) {
				
				$updateCountQuery = "UPDATE m03_work_product_cstm SET `primary_animals_countdown_c`='".$Primary_Animals_Countdown."' WHERE id_c='".$workProductId."'";
				$db->query($updateCountQuery);
				 
				$source = '{"subject":{"_type":"logic-hook","class":"WPE_animalCountHook","method":"UpdateAnimalCountdownValues"},"attributes":[]}';
				$auditEventid = create_guid();
                $auditsql = 'INSERT INTO m03_work_product_audit  (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $workProductId . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"primary_animals_countdown_c","integer","' . $previousAnimalCount . '","' . $Primary_Animals_Countdown . '")';
                $auditsqlResult = $db->query($auditsql);
				$auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $workProductId . "','M03_Work_Product','" . $source . "')";
                $db->query($auditsqlStatus);
            } 

			if($wpBean->m03_work_product_id_c=="" && $wpBean->fetched_row['m03_work_product_id_c']!=""){
				$wpBean->blanket_protocol_hidden_c  = $wpBean->fetched_row['m03_work_product_id_c'];
			}
			
			$wpCode = $wpBean->m03_work_product_code_id1_c;
			if($wpBean->iacuc_approval_date_c!="" && ($wpCode=='9634c914-fe07-11e6-a663-02feb3423f0d' || $wpCode=='0527341e-d0d4-11e9-8661-06eddc549468')){
				$iacucDate =  $wpBean->iacuc_approval_date_c;
				$blnkQuery = "SELECT sum(IFNULL(WPCSTM.primary_animals_countdown_c,0)) PrimaryAnimalRemaining,
								sum(IFNULL(WPCSTM.back_up_animals_countdown_c,0)) BackupAnimalRemaining
								FROM m03_work_product AS WP
								LEFT JOIN  m03_work_product_erd_error_documents_1_c AS WPERD  ON 
								WP.id=WPERD.m03_work_product_erd_error_documents_1m03_work_product_ida AND WPERD.deleted=0

								LEFT JOIN  erd_error_documents ERD  
								ON ERD.id=WPERD.m03_work_product_erd_error_documents_1erd_error_documents_idb AND ERD.deleted=0
								
								LEFT JOIN m03_work_product_cstm WPCSTM  ON WP.id = WPCSTM.id_c
								LEFT JOIN m03_work_product WP1 ON WP1.id = WPCSTM.m03_work_product_id_c AND IFNULL(WP1.deleted,0)=0 

							WHERE 
								WPCSTM.m03_work_product_id_c='".$wpBean->id."'
								AND 
								WPCSTM.work_product_compliance_c IN ('NonGLP Discovery','NonGLP Structured','GLP','GLP Discontinued','nonGLP')
								AND 
								WPCSTM.first_procedure_c >= '".$iacucDate."' 
								AND  
								WP.deleted=0 AND IFNULL(WP1.deleted,0)=0";
				$blnkQuery_exec = $db->query($blnkQuery);
				$blnkSum	= 0;
				$primarySum = 0;
				$backupSum	= 0;
				if ($blnkQuery_exec->num_rows > 0) 
				{
					while($fetchBlnk = $db->fetchByAssoc($blnkQuery_exec)) {
						$primarySum	= $fetchBlnk['PrimaryAnimalRemaining'];
						$backupSum	= $fetchBlnk['BackupAnimalRemaining']; 
					}
					$blnkSum = $primarySum+$backupSum;
					$blanket_animals_remaining_c = ($wpBean->total_animals_used_primary_c + $blnkSum);
					
					//$GLOBALS['log']->fatal('new blanket_animals_remaining_c ===>' . $blanket_animals_remaining_c);

					if ($previousBlanketAnimalCount != $blanket_animals_remaining_c) {
				
						$updateCountQuery = "UPDATE m03_work_product_cstm SET `blanket_animals_remaining_c`='".$blanket_animals_remaining_c."' WHERE id_c='".$workProductId."'";
						
						//$GLOBALS['log']->fatal('update Count Query ===>' . $updateCountQuery);
						
						$db->query($updateCountQuery);
						 
						$source = '{"subject":{"_type":"logic-hook","class":"WPE_animalCountHook","method":"UpdateAnimalCountdownValues"},"attributes":[]}';
						$auditEventid = create_guid();
						$auditsql = 'INSERT INTO m03_work_product_audit  (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $workProductId . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"blanket_animals_remaining_c","integer","' . $previousBlanketAnimalCount . '","' . $blanket_animals_remaining_c . '")';
						$auditsqlResult = $db->query($auditsql);
						$auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $workProductId . "','M03_Work_Product','" . $source . "')";
						$db->query($auditsqlStatus);
					}
				}
				
			}
			
		}else{
			$GLOBALS['log']->fatal('WP:Not Updating Animal countdown for :'.$bean->id);
		} 
	}
 
}
?>