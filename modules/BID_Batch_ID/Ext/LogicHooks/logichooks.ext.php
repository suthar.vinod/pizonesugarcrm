<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/LogicHooks/addBatchIDHook.php

$hook_version = 1;
$hook_array['before_save'][] = array(
     1,
    'Auto Generate Name',
    'custom/modules/BID_Batch_ID/nameGenerationHookBID.php',
    'HookBeforeSaveForNameGenerationBID',
    'generateNameBID'
);
$hook_array['after_save'][] = array(
    6,
   'Set the Schedule date to the Linked work Product',
   'custom/modules/BID_Batch_ID/setScheduleDateToFirstProcedureWPHook.php',
   'setScheduleDateToFirstProcedureWPHook',
   'setScheduleDate'
);
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/LogicHooks/WP_animal_countdown_Hook.php

	$hook_array['after_relationship_delete'][] = Array(
      1012, 
      'Update Animal Countdown in Work Product After Relationship Delete', 
      'custom/modules/BID_Batch_ID/BID_WP_animal_countdown_afterRelationDeleteHook.php', 
      'BID_animalCountAfterRelationDeleteHook', 
      'UpdateAnimalCountdownValues'
   );
   
   $hook_array['after_relationship_add'][] = Array(
      1011, 
      'Update Animal Countdown in Work Product After relation add', 
      'custom/modules/BID_Batch_ID/BID_WP_animal_countdown_Hook.php', 
      'BID_animalCountAfterRelationAddHook', 
      'UpdateAnimalCountdownValues'
   );   

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/LogicHooks/setStatusReadOnlyHook.php

// $hook_version = 1;
$hook_array['after_relationship_add'][] = array(
     111,
    'Set status field readonly',
    'custom/modules/BID_Batch_ID/setStatusReadOnlyAddHook.php',
    'setStatusReadOnlyAddHook',
    'setBIDStatus'
);
$hook_array['after_relationship_delete'][] = array(
    211,
   'Set status field readonly',
   'custom/modules/BID_Batch_ID/setStatusReadOnlyAddHook.php',
    'setStatusReadOnlyAddHook',
    'setBIDStatus'
);
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/LogicHooks/updateBatchIDTDsDateHook.php

//$hook_version = 1;
$hook_array['before_save'][] = array(
     101,
    'Auto Generate Name',
    'custom/modules/BID_Batch_ID/UpdateCalCopyedTaskDesignToBD.php',
    'HookBeforeSaveForTDsCalculation',
    'generateTDsCalculation'
);

?>
