<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/WirelessLayoutdefs/bid_batch_id_m03_work_product_1_BID_Batch_ID.php

 // created: 2022-04-26 06:44:22
$layout_defs["BID_Batch_ID"]["subpanel_setup"]['bid_batch_id_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'bid_batch_id_m03_work_product_1',
);

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/WirelessLayoutdefs/bid_batch_id_taskd_task_design_1_BID_Batch_ID.php

 // created: 2022-04-26 06:47:59
$layout_defs["BID_Batch_ID"]["subpanel_setup"]['bid_batch_id_taskd_task_design_1'] = array (
  'order' => 100,
  'module' => 'TaskD_Task_Design',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'get_subpanel_data' => 'bid_batch_id_taskd_task_design_1',
);

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/WirelessLayoutdefs/bid_batch_id_wpe_work_product_enrollment_1_BID_Batch_ID.php

 // created: 2022-04-26 06:50:11
$layout_defs["BID_Batch_ID"]["subpanel_setup"]['bid_batch_id_wpe_work_product_enrollment_1'] = array (
  'order' => 100,
  'module' => 'WPE_Work_Product_Enrollment',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'get_subpanel_data' => 'bid_batch_id_wpe_work_product_enrollment_1',
);

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/WirelessLayoutdefs/bid_batch_id_m06_error_1_BID_Batch_ID.php

 // created: 2022-04-26 07:03:59
$layout_defs["BID_Batch_ID"]["subpanel_setup"]['bid_batch_id_m06_error_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_BID_BATCH_ID_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'bid_batch_id_m06_error_1',
);

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/WirelessLayoutdefs/bid_batch_id_gd_group_design_1_BID_Batch_ID.php

 // created: 2022-09-22 05:28:30
$layout_defs["BID_Batch_ID"]["subpanel_setup"]['bid_batch_id_gd_group_design_1'] = array (
  'order' => 100,
  'module' => 'GD_Group_Design',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'get_subpanel_data' => 'bid_batch_id_gd_group_design_1',
);

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/WirelessLayoutdefs/bid_batch_id_tasks_1_BID_Batch_ID.php

 // created: 2022-09-22 05:31:46
$layout_defs["BID_Batch_ID"]["subpanel_setup"]['bid_batch_id_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_BID_BATCH_ID_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'bid_batch_id_tasks_1',
);

?>
