<?php
// WARNING: The contents of this file are auto-generated.


// created: 2022-09-22 05:28:30
$viewdefs['BID_Batch_ID']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'context' => 
  array (
    'link' => 'bid_batch_id_gd_group_design_1',
  ),
);

// created: 2022-04-26 06:44:22
$viewdefs['BID_Batch_ID']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'bid_batch_id_m03_work_product_1',
  ),
);

// created: 2022-04-26 07:03:59
$viewdefs['BID_Batch_ID']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_BID_BATCH_ID_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'context' => 
  array (
    'link' => 'bid_batch_id_m06_error_1',
  ),
);

// created: 2022-04-26 06:47:59
$viewdefs['BID_Batch_ID']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'context' => 
  array (
    'link' => 'bid_batch_id_taskd_task_design_1',
  ),
);

// created: 2022-09-22 05:31:46
$viewdefs['BID_Batch_ID']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_BID_BATCH_ID_TASKS_1_FROM_TASKS_TITLE',
  'context' => 
  array (
    'link' => 'bid_batch_id_tasks_1',
  ),
);

// created: 2022-04-26 06:50:11
$viewdefs['BID_Batch_ID']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'context' => 
  array (
    'link' => 'bid_batch_id_wpe_work_product_enrollment_1',
  ),
);