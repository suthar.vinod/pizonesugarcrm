<?php
// WARNING: The contents of this file are auto-generated.


$module = 'BID_Batch_ID';
$components = $viewdefs[$module]['base']['layout']['create']['components'] ?? [];
$components = \Sugarcrm\Sugarcrm\custom\src\wsystems\InventoryManagementCustomizations\IMUtils::injectAfterSidebarMainpane(
    $components,
    [
        'layout' => [
            'type' => 'base',
            'name' => 'dashboard-pane',
            'css_class' => 'dashboard-pane',
            'components' => [
                [
                    'layout' => 'wp-selection',
                ],
            ],
        ],
    ],
    'preview-pane'
);
$viewdefs[$module]['base']['layout']['create']['components'] = $components;
