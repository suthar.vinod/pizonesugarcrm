<?php
// WARNING: The contents of this file are auto-generated.


// created: 2022-04-07 
$viewdefs['BID_Batch_ID']['base']['filter']['basic']['filters'][] = array(
  'id' => 'FilterBID_Batch_IDStatus',
  'name' => 'Batch ID Filter',
  'filter_definition' => array(
    array(
      'status_c'  => array(
        '$in' => '',
      ),
    ),
  ),
  'editable' => true,
  'is_template' => true,
);
