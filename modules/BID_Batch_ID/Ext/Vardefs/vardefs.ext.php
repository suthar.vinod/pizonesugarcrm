<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/bid_batch_id_m03_work_product_1_BID_Batch_ID.php

// created: 2022-04-26 06:44:22
$dictionary["BID_Batch_ID"]["fields"]["bid_batch_id_m03_work_product_1"] = array (
  'name' => 'bid_batch_id_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_BID_BATCH_ID_TITLE',
  'id_name' => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/bid_batch_id_taskd_task_design_1_BID_Batch_ID.php

// created: 2022-04-26 06:47:59
$dictionary["BID_Batch_ID"]["fields"]["bid_batch_id_taskd_task_design_1"] = array (
  'name' => 'bid_batch_id_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'vname' => 'LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_BID_BATCH_ID_TITLE',
  'id_name' => 'bid_batch_id_taskd_task_design_1bid_batch_id_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/bid_batch_id_wpe_work_product_enrollment_1_BID_Batch_ID.php

// created: 2022-04-26 06:50:11
$dictionary["BID_Batch_ID"]["fields"]["bid_batch_id_wpe_work_product_enrollment_1"] = array (
  'name' => 'bid_batch_id_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_BID_BATCH_ID_TITLE',
  'id_name' => 'bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/bid_batch_id_m06_error_1_BID_Batch_ID.php

// created: 2022-04-26 07:03:59
$dictionary["BID_Batch_ID"]["fields"]["bid_batch_id_m06_error_1"] = array (
  'name' => 'bid_batch_id_m06_error_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_BID_BATCH_ID_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'bid_batch_id_m06_error_1m06_error_idb',
);

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/sugarfield_scheduled_date_c.php

 // created: 2022-04-26 07:06:54
$dictionary['BID_Batch_ID']['fields']['scheduled_date_c']['labelValue']='Scheduled Date';
$dictionary['BID_Batch_ID']['fields']['scheduled_date_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['scheduled_date_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['scheduled_date_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['scheduled_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/sugarfield_status_c.php

 // created: 2022-04-26 07:09:41
$dictionary['BID_Batch_ID']['fields']['status_c']['labelValue']='Status';
$dictionary['BID_Batch_ID']['fields']['status_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['status_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['status_c']['readonly_formula']='';
$dictionary['BID_Batch_ID']['fields']['status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/sugarfield_control_quantity_c.php

 // created: 2022-04-26 07:13:57
$dictionary['BID_Batch_ID']['fields']['control_quantity_c']['labelValue']='Control Quantity';
$dictionary['BID_Batch_ID']['fields']['control_quantity_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['control_quantity_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['control_quantity_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['control_quantity_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['control_quantity_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/sugarfield_in_life_duration_c.php

 // created: 2022-04-26 07:15:18
$dictionary['BID_Batch_ID']['fields']['in_life_duration_c']['labelValue']='In-life Duration (days)';
$dictionary['BID_Batch_ID']['fields']['in_life_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['in_life_duration_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['in_life_duration_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['in_life_duration_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['in_life_duration_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/sugarfield_dose_route_c.php

 // created: 2022-04-26 07:18:01
$dictionary['BID_Batch_ID']['fields']['dose_route_c']['labelValue']='Dose Route';
$dictionary['BID_Batch_ID']['fields']['dose_route_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['dose_route_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['dose_route_c']['readonly_formula']='';
$dictionary['BID_Batch_ID']['fields']['dose_route_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/sugarfield_other_dose_route_c.php

 // created: 2022-04-26 07:19:39
$dictionary['BID_Batch_ID']['fields']['other_dose_route_c']['labelValue']='Other Dose Route';
$dictionary['BID_Batch_ID']['fields']['other_dose_route_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['other_dose_route_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['other_dose_route_c']['dependency']='equal($dose_route_c,"Other")';
$dictionary['BID_Batch_ID']['fields']['other_dose_route_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['other_dose_route_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/sugarfield_status_readonly_c.php

 // created: 2022-04-26 07:20:58
$dictionary['BID_Batch_ID']['fields']['status_readonly_c']['labelValue']='Status Readonly';
$dictionary['BID_Batch_ID']['fields']['status_readonly_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['status_readonly_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['status_readonly_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['status_readonly_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['status_readonly_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/sugarfield_description.php

 // created: 2022-04-26 11:39:24
$dictionary['BID_Batch_ID']['fields']['description']['audited']=true;
$dictionary['BID_Batch_ID']['fields']['description']['massupdate']=false;
$dictionary['BID_Batch_ID']['fields']['description']['hidemassupdate']=false;
$dictionary['BID_Batch_ID']['fields']['description']['comments']='Full text of the note';
$dictionary['BID_Batch_ID']['fields']['description']['duplicate_merge']='enabled';
$dictionary['BID_Batch_ID']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['BID_Batch_ID']['fields']['description']['merge_filter']='disabled';
$dictionary['BID_Batch_ID']['fields']['description']['unified_search']=false;
$dictionary['BID_Batch_ID']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['BID_Batch_ID']['fields']['description']['calculated']=false;
$dictionary['BID_Batch_ID']['fields']['description']['rows']='6';
$dictionary['BID_Batch_ID']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/sugarfield_extract_duration_hours_c.php

 // created: 2022-05-05 09:08:56
$dictionary['BID_Batch_ID']['fields']['extract_duration_hours_c']['labelValue']='Extract Condition (Hours)';
$dictionary['BID_Batch_ID']['fields']['extract_duration_hours_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['extract_duration_hours_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['extract_duration_hours_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['extract_duration_hours_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['extract_duration_hours_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/sugarfield_extract_condition_temp_c.php

 // created: 2022-05-05 09:11:14
$dictionary['BID_Batch_ID']['fields']['extract_condition_temp_c']['labelValue']='Extract Condition (Temperature)';
$dictionary['BID_Batch_ID']['fields']['extract_condition_temp_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['extract_condition_temp_c']['enforced']='';
$dictionary['BID_Batch_ID']['fields']['extract_condition_temp_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['extract_condition_temp_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['extract_condition_temp_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/bid_batch_id_gd_group_design_1_BID_Batch_ID.php

// created: 2022-09-22 05:28:30
$dictionary["BID_Batch_ID"]["fields"]["bid_batch_id_gd_group_design_1"] = array (
  'name' => 'bid_batch_id_gd_group_design_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_gd_group_design_1',
  'source' => 'non-db',
  'module' => 'GD_Group_Design',
  'bean_name' => 'GD_Group_Design',
  'vname' => 'LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_BID_BATCH_ID_TITLE',
  'id_name' => 'bid_batch_id_gd_group_design_1bid_batch_id_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/bid_batch_id_tasks_1_BID_Batch_ID.php

// created: 2022-09-22 05:31:46
$dictionary["BID_Batch_ID"]["fields"]["bid_batch_id_tasks_1"] = array (
  'name' => 'bid_batch_id_tasks_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_BID_BATCH_ID_TASKS_1_FROM_BID_BATCH_ID_TITLE',
  'id_name' => 'bid_batch_id_tasks_1bid_batch_id_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Vardefs/sugarfield_gd_complete_c.php

 // created: 2022-09-22 05:39:56
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['duplicate_merge_dom_value']=0;
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['labelValue']='GD Complete';
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['calculated']='true';
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['formula']='rollupSum($bid_batch_id_gd_group_design_1,"complete_yes_no_c")';
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['enforced']='true';
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['dependency']='';
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['required_formula']='';
$dictionary['BID_Batch_ID']['fields']['gd_complete_c']['readonly_formula']='';

 
?>
