<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Language/fi_FI.custombid_batch_id_m03_work_product_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_BID_BATCH_ID_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Language/fi_FI.custombid_batch_id_taskd_task_design_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE'] = 'Task Designs';
$mod_strings['LBL_BID_BATCH_ID_TASKD_TASK_DESIGN_1_FROM_BID_BATCH_ID_TITLE'] = 'Task Designs';

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Language/fi_FI.custombid_batch_id_wpe_work_product_enrollment_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Product Assignments';
$mod_strings['LBL_BID_BATCH_ID_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_BID_BATCH_ID_TITLE'] = 'Work Product Assignments';

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Language/fi_FI.custombid_batch_id_m06_error_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_BID_BATCH_ID_M06_ERROR_1_FROM_M06_ERROR_TITLE'] = 'Communications';
$mod_strings['LBL_BID_BATCH_ID_M06_ERROR_1_FROM_BID_BATCH_ID_TITLE'] = 'Communications';

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Language/fi_FI.custombid_batch_id_gd_group_design_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE'] = 'Group Designs';
$mod_strings['LBL_BID_BATCH_ID_GD_GROUP_DESIGN_1_FROM_BID_BATCH_ID_TITLE'] = 'Group Designs';

?>
<?php
// Merged from custom/Extension/modules/BID_Batch_ID/Ext/Language/fi_FI.custombid_batch_id_tasks_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_BID_BATCH_ID_TASKS_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_BID_BATCH_ID_TASKS_1_FROM_BID_BATCH_ID_TITLE'] = 'Tasks';

?>
