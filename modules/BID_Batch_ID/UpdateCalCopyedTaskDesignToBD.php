<?php

if (!defined('sugarEntry') || !sugarEntry)
  die('Not A Valid Entry Point');

class HookBeforeSaveForTDsCalculation
{

  function generateTDsCalculation($bean, $event, $arguments)
  {

    global $db, $current_user;

    $bean->load_relationship("bid_batch_id_taskd_task_design_1");
    $TaskDesignId = $bean->bid_batch_id_taskd_task_design_1->get();

    if ($bean->scheduled_date_c != $bean->fetched_row['scheduled_date_c']) {
      $datefirst_procedure_c = strtotime($bean->scheduled_date_c);

      foreach ($TaskDesignId as $tdID) {
        $tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $tdID);
        $type   = $tdBean->type_2;
        $relative = $tdBean->relative;
        if ($type == 'Actual SP' && $relative == 'NA') {
          $tdBean->actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
          //$GLOBALS['log']->fatal(' tdBean->actual_datetime ' . $tdBean->actual_datetime);
          $tdBean->save();
        }

        /* Code for updation of single date 2nd tier */
        $u_units = $tdBean->u_units_id_c;
        $unit_Bean = BeanFactory::getBean('U_Units', $u_units);
        $unit_Bean_name = $unit_Bean->name;
        //$GLOBALS['log']->fatal(' unit_Bean_name ' . $unit_Bean_name);
        if ($unit_Bean_name == "Day") {
          $integer_u = 'days';
        } elseif ($unit_Bean_name == "Hour") {
          $integer_u = 'hours';
        } else {
          $integer_u = 'minutes';
        }
        //$GLOBALS['log']->fatal(' tdID tdBean->relative ' . $tdBean->relative);
        if ($tdBean->relative == "1st Tier") {
          $tdBean1 = BeanFactory::retrieveBean('TaskD_Task_Design', $tdBean->id);
          $tdBean1->load_relationship('taskd_task_design_taskd_task_design_1');
          $taskDesignId1 = $tdBean1->taskd_task_design_taskd_task_design_1->get();
          $Get_2nd_tier = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_idb FROM  `taskd_task_design_taskd_task_design_1_c` WHERE `taskd_task_design_taskd_task_design_1taskd_task_design_ida` = '" . $tdBean->id . "' AND deleted = 0;";
          $Get_Result = $db->query($Get_2nd_tier);

          if ($Get_Result->num_rows > 0) {
            while ($fetchResult = $db->fetchByAssoc($Get_Result)) {
              $recordID = $fetchResult['taskd_task_design_taskd_task_design_1taskd_task_design_idb'];
              //$GLOBALS['log']->fatal(' recordID ' . $recordID);
              $tdBean2 = BeanFactory::retrieveBean('TaskD_Task_Design', $recordID);
              //$GLOBALS['log']->fatal(' tdBean->planned_start_datetime_1st ' . $tdBean->planned_start_datetime_1st);
              if ($tdBean->planned_start_datetime_1st != "") {
                $result = date($tdBean->planned_start_datetime_1st);
                $result = strtotime($result . '+' . $tdBean2->start_integer . ' ' . $integer_u);
                // $tdBean2->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
                if (date("Y", $result) != "1970") {
                  $tdBean2->single_date_c = date("Y-m-d", $result);
                  $GLOBALS['log']->fatal(' single_date_c WP 2nd Tier ' . $tdBean2->single_date_c);
                }
              }
              $tdBean2->save();
            }
          }
        }
      }
    }
  }
}
