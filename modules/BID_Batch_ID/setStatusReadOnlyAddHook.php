<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
class setStatusReadOnlyAddHook
{
    public function setBIDStatus($bean, $event, $arguments)
    {
        if($arguments['module'] == "BID_Batch_ID" && $arguments['related_module'] == "M03_Work_Product" && $arguments['relationship']=="bid_batch_id_m03_work_product_1"){
            $count = 0;
            $statusCheck = array();
            // If relationship is loaded
            $bean->load_relationship('bid_batch_id_m03_work_product_1');
            $scheduleDate = $bean->scheduled_date_c;
            
            $linkedWP     = $bean->bid_batch_id_m03_work_product_1->get();
            foreach ($linkedWP as $relatedWP) {
                $WPBean  = BeanFactory::getBean('M03_Work_Product', $relatedWP);
                // $batchId = $WPBean->bid_batch_id_m03_work_product_1bid_batch_id_ida;
                array_push($statusCheck,$WPBean->work_product_status_c); // push value into the status array
                if (count(array_unique($statusCheck)) === 1 && end($statusCheck) === 'Archived') {
                    $count = count($statusCheck);
                }else{
                    $count = 0;
                }
                $WPBean->first_procedure_c = $scheduleDate;
                $WPBean->save();
            }
            if($count !== 0){
                $bean->status_c = "Closed";
            }
            $bean->status_readonly_c = $count;
            //  $GLOBALS['log']->fatal("count hook lax==>". $count);
            $bean->save();
        }
        
    }
}