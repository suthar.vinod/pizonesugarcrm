<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
class setScheduleDateToFirstProcedureWPHook
{
    public function setScheduleDate($bean, $event, $arguments){
        // $GLOBALS['log']->fatal("<=========In hook after save========>");
        $scheduleDate = $bean->scheduled_date_c;
        //Load Relationship
        $bean->load_relationship('bid_batch_id_m03_work_product_1');
        $linkedWP     = $bean->bid_batch_id_m03_work_product_1->get(); // get all the Ids of linked WP
        foreach ($linkedWP as $relatedWp) {
            $WPBean = BeanFactory::getBean('M03_Work_Product', $relatedWp);
            $batchId = $WPBean->bid_batch_id_m03_work_product_1bid_batch_id_ida;
            if(!empty($batchId)){
                $WPBean->first_procedure_c = $scheduleDate;
                $WPBean->save();
            }
        }
    }
}