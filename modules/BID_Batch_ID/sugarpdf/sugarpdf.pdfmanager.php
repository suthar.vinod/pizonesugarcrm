<?php
require_once 'include/Sugarpdf/sugarpdf/sugarpdf.pdfmanager.php';
require_once 'include/Sugarpdf/sugarpdf/sugarpdf.smarty.php';
//require_once('include/Sugar_Smarty.php');
//ob_start();
class BID_Batch_IDSugarpdfPdfmanager extends SugarpdfPdfmanager
{
    public function preDisplay()
    {
        global $db, $current_user, $app_list_strings;
        parent::preDisplay();
        $this->SetMargins(15, 10, 15);
        $previewMode = false;

        if (!empty($_REQUEST['pdf_preview']) && $_REQUEST['pdf_preview'] == 1) {
            $previewMode = true;
        }
        if ($this->module == 'BID_Batch_ID' && $previewMode === false) {
            $batchIdName = $this->bean->name;
            // $GLOBALS['log']->fatal("batchIdName===> " . $batchIdName);
            $allDataArr = array();
            $parentValcheck = array();
            $count = 1;
            if ($this->bean->load_relationship('bid_batch_id_m03_work_product_1')) {
                $linkedWPIds = $this->bean->bid_batch_id_m03_work_product_1->get();
                // $GLOBALS['log']->fatal("Linked WPS" . print_r($linkedWPIds , 1));
                foreach ($linkedWPIds as $relatedWPs) {
                    $WPbean = BeanFactory::retrieveBean('M03_Work_Product', $relatedWPs); //get WP bean
                    $allDataArr[$count]['WPId'] = $WPbean->name;
                    //get the parent WP Field val
                    $allDataArr[$count]['parentWPVal'] = $WPbean->parent_work_product_c; // store all the checked and non checked value;
                    //$GLOBALS['log']->fatal("parent_work_product_c===> " . $WPbean->parent_work_product_c);
                    if(!empty($WPbean->parent_work_product_c) && $WPbean->parent_work_product_c == 1){
                        $parentValcheck[] = $allDataArr[$count]['WPId'];
                    }
                    $count++;
                }
                $commaSepParentVal = implode(",",$parentValcheck);
            }
            $totalRecordCount = count($allDataArr);
            $staticRow = 7 - $totalRecordCount;
            for ($i = 0; $i < $staticRow; $i++) {
                $rowArr[] = $i;
            }
            $count = 1;
           
            
            $this->ss->assign('allDataArr', $allDataArr);
            $this->ss->assign('parentValcheck', $commaSepParentVal);
            $this->ss->assign('totalRecordCount', $totalRecordCount);
            $this->ss->assign('rowArr', $rowArr);
            $this->ss->assign('staticRow', $staticRow);
            $this->ss->assign('row', $row);
            $this->ss->assign('batchIdName', $batchIdName);
            $this->ss->assign('count', $count);
        }
    }

    /**
     * PDF manager specific Footer function
     */
    public function Footer()
    {
        $cur_y = $this->GetY();
        $ormargins = $this->getOriginalMargins();
        $this->SetTextColor(0, 0, 0);
        //set style for cell border
        $line_width = 0.85 / $this->getScaleFactor();
        $this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        //print document barcode
        $barcode = $this->getBarcode();
        if (!empty($barcode)) {
            $this->Ln($line_width);
            $barcode_width = round(($this->getPageWidth() - $ormargins['left'] - $ormargins['right']) / 3);
            $this->write1DBarcode($barcode, 'C128B', $this->GetX(), $cur_y + $line_width, $barcode_width, (($this->getFooterMargin() / 3) - $line_width), 0.3, '', '');
        }
        if (empty($this->pagegroups)) {
            $pagenumtxt = $this->l['w_page'] . ' ' . $this->getAliasNumPage() . ' OF ' . $this->getAliasNbPages();
            $pagenumtxt = 'PAGE' . $pagenumtxt;
        } else {
            $pagenumtxt = $this->l['w_page'] . ' ' . $this->getPageNumGroupAlias() . ' OF ' . $this->getPageGroupAlias();
            $pagenumtxt = 'PAGE' . $pagenumtxt;
        }
        $this->SetY($cur_y);

        if ($this->getRTL()) {
            $this->SetX($ormargins['right']);
            if ($this->footerText) {
                // footer text and page number
                $this->Cell(0, 0, $this->footerText, 'T', 0, 'R');
                $this->Cell(0, 0, $pagenumtxt, 0, 0, 'L');
            } else {
                // page number only
                $this->Cell(0, 0, $pagenumtxt, 'T', 0, 'L');
            }
        } else {
            $this->SetX($ormargins['left']);
            if ($this->footerText) {
                // footer text and page number
                $this->Cell(0, 0, $this->footerText, 'T', 0, 'L');
                $this->Cell(0, 0, $pagenumtxt, 0, 0, 'R');
            } else {
                // page number only
                $this->Cell(0, 0, $pagenumtxt, 'T', 0, 'R');
            }
        }
    }
}
