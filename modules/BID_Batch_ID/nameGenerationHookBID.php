<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
class HookBeforeSaveForNameGenerationBID
{
    public function generateNameBID($bean, $event, $arguments)
    {
        global $db;
        if ($arguments['isUpdate'] == false) {
            $currentYear = date('y'); //current Year last two digit if 2022 => 22
            $fixedPrefix = 'B';
            $sql         = 'SELECT name FROM bid_batch_id WHERE name LIKE "%' . $currentYear . '%" AND deleted <> 1 ORDER BY name DESC LIMIT 1';
            $result      = $db->query($sql);
            $resultSet   = $db->fetchByAssoc($result);
            $year        = substr($resultSet['name'], 1, 2);
            if (!empty($resultSet) && $year === $currentYear) {
                $existingSequence = substr($resultSet['name'], -3);
                if (!is_numeric($existingSequence) === true) {
                    $existingSequence = null;
                }
                if ($existingSequence >= 100) {
                    $seq = $this->getSequence($bean, $existingSequence, $currentYear);
                    $seqNo = trim($bean->name);
                    if ($existingSequence === null) {
                        $seqNo .= $seq;
                    } else if ($existingSequence !== $seq) {
                        if ($seq >= 1000) {
                            $seqNo = substr($bean->name, 0, -4) . $seq;
                        } else {
                            $seqNo = substr($bean->name, 0, -3) . $seq;
                        }
                    } else if ($existingSequence === $seq) {
                        $eseq = substr($bean->name, -3);
                        if (is_numeric($eseq) === true) {
                            $seqNo = substr($bean->name, 0, -3) . $seq;
                        } else {
                            $seqNo .= $seq;
                        }
                    }
                } else {
                    $seq = $this->getSequence($bean, $existingSequence, $currentYear);
                    $seqNo = trim($bean->name);
                    if ($existingSequence === null) {
                        $seqNo .= $seq;
                    } else if ($existingSequence !== $seq) {
                        $seqNo = substr($bean->name, 0, -2) . $seq;
                    } else if ($existingSequence === $seq) {
                        $eseq = substr($bean->name, -2);
                        if (is_numeric($eseq) === true) {
                            $seqNo = substr($bean->name, 0, -2) . $seq;
                        } else {
                            $seqNo .= $seq;
                        }
                    }
                }
                $bean->name = $fixedPrefix . '' . $currentYear . '-' . $seqNo;
            } else {
                $bean->name = $fixedPrefix . '' . $currentYear . '-001';
            }
        }
    }
    private function getSequence($bean, $existingSequence, $currentYear)
    {
        $sugarQuery = new SugarQuery();
        $sugarQuery->from(BeanFactory::newBean($bean->module_dir));
        $sugarQuery->select(array("id", "name"));
        $sugarQuery->where()->contains("name", $currentYear);
        // $sugarQuery->orderBy('date_entered');
        $resultSet = $sugarQuery->execute();
        $sequences = array();
        foreach ($resultSet as $row) {
            $row_name    = $row["name"];
            if ($row_seq  = substr($row_name, 4) >= 1000) {
                $row_seq  = substr($row_name, 4);
            } else {
                $row_seq  = substr($row_name, 4);
            }
            if (is_numeric($row_seq)) {
                $sequences[] = intval($row_seq);
            }
        }
        if (count($sequences) > 0) {
            $max_seq = max($sequences);
            if ($existingSequence === null) {
                $max_seq += 1;
            }
            if ($max_seq > 9) {
                if ($max_seq >= 99 && $max_seq < 999) {
                    $seq = ++$max_seq;
                } else if ($max_seq >= 999) {
                    $seq = ++$max_seq;
                } else {
                    $seq = ++$max_seq;
                    $seq = '0' . $seq;
                }
            } else if ($max_seq <= 9) {
                $seq = ++$max_seq;
                if ($seq == 10) {
                    $seq = '010';
                } else {
                    $seq = "00" . $max_seq;
                }
            }
        } else {
            $seq = "01";
        }
        // $GLOBALS['log']->fatal('Final Sequence No==>: ' . $seq);
        return $seq;
    }
}
