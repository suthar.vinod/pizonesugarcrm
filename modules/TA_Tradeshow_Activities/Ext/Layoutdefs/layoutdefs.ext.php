<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TA_Tradeshow_Activities/Ext/Layoutdefs/ta_tradeshow_activities_tasks_1_TA_Tradeshow_Activities.php

 // created: 2019-10-31 11:25:05
$layout_defs["TA_Tradeshow_Activities"]["subpanel_setup"]['ta_tradeshow_activities_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'ta_tradeshow_activities_tasks_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
