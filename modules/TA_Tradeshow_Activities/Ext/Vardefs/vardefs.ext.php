<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TA_Tradeshow_Activities/Ext/Vardefs/contacts_ta_tradeshow_activities_1_TA_Tradeshow_Activities.php

// created: 2018-04-23 17:07:52
$dictionary["TA_Tradeshow_Activities"]["fields"]["contacts_ta_tradeshow_activities_1"] = array (
  'name' => 'contacts_ta_tradeshow_activities_1',
  'type' => 'link',
  'relationship' => 'contacts_ta_tradeshow_activities_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'id_name' => 'contacts_ta_tradeshow_activities_1contacts_ida',
  'link-type' => 'one',
);
$dictionary["TA_Tradeshow_Activities"]["fields"]["contacts_ta_tradeshow_activities_1_name"] = array (
  'name' => 'contacts_ta_tradeshow_activities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_TA_TRADESHOW_ACTIVITIES_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_ta_tradeshow_activities_1contacts_ida',
  'link' => 'contacts_ta_tradeshow_activities_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["TA_Tradeshow_Activities"]["fields"]["contacts_ta_tradeshow_activities_1contacts_ida"] = array (
  'name' => 'contacts_ta_tradeshow_activities_1contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE_ID',
  'id_name' => 'contacts_ta_tradeshow_activities_1contacts_ida',
  'link' => 'contacts_ta_tradeshow_activities_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/TA_Tradeshow_Activities/Ext/Vardefs/tm_tradeshow_management_ta_tradeshow_activities_1_TA_Tradeshow_Activities.php

// created: 2018-04-23 17:10:34
$dictionary["TA_Tradeshow_Activities"]["fields"]["tm_tradeshow_management_ta_tradeshow_activities_1"] = array (
  'name' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'source' => 'non-db',
  'module' => 'TM_Tradeshow_Management',
  'bean_name' => 'TM_Tradeshow_Management',
  'side' => 'right',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'id_name' => 'tm_tradesh0f8eagement_ida',
  'link-type' => 'one',
);
$dictionary["TA_Tradeshow_Activities"]["fields"]["tm_tradeshow_management_ta_tradeshow_activities_1_name"] = array (
  'name' => 'tm_tradeshow_management_ta_tradeshow_activities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'save' => true,
  'id_name' => 'tm_tradesh0f8eagement_ida',
  'link' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'name',
);
$dictionary["TA_Tradeshow_Activities"]["fields"]["tm_tradesh0f8eagement_ida"] = array (
  'name' => 'tm_tradesh0f8eagement_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE_ID',
  'id_name' => 'tm_tradesh0f8eagement_ida',
  'link' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/TA_Tradeshow_Activities/Ext/Vardefs/accounts_ta_tradeshow_activities_1_TA_Tradeshow_Activities.php

// created: 2018-04-23 17:12:12
$dictionary["TA_Tradeshow_Activities"]["fields"]["accounts_ta_tradeshow_activities_1"] = array (
  'name' => 'accounts_ta_tradeshow_activities_1',
  'type' => 'link',
  'relationship' => 'accounts_ta_tradeshow_activities_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'id_name' => 'accounts_ta_tradeshow_activities_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["TA_Tradeshow_Activities"]["fields"]["accounts_ta_tradeshow_activities_1_name"] = array (
  'name' => 'accounts_ta_tradeshow_activities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_ta_tradeshow_activities_1accounts_ida',
  'link' => 'accounts_ta_tradeshow_activities_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["TA_Tradeshow_Activities"]["fields"]["accounts_ta_tradeshow_activities_1accounts_ida"] = array (
  'name' => 'accounts_ta_tradeshow_activities_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE_ID',
  'id_name' => 'accounts_ta_tradeshow_activities_1accounts_ida',
  'link' => 'accounts_ta_tradeshow_activities_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/TA_Tradeshow_Activities/Ext/Vardefs/sugarfield_name.php

 // created: 2019-04-23 12:14:38
$dictionary['TA_Tradeshow_Activities']['fields']['name']['len']='255';
$dictionary['TA_Tradeshow_Activities']['fields']['name']['audited']=true;
$dictionary['TA_Tradeshow_Activities']['fields']['name']['massupdate']=false;
$dictionary['TA_Tradeshow_Activities']['fields']['name']['unified_search']=false;
$dictionary['TA_Tradeshow_Activities']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['TA_Tradeshow_Activities']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/TA_Tradeshow_Activities/Ext/Vardefs/sugarfield_contact_name_c.php

 // created: 2019-04-23 12:15:16
$dictionary['TA_Tradeshow_Activities']['fields']['contact_name_c']['labelValue']='Contact Name';
$dictionary['TA_Tradeshow_Activities']['fields']['contact_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TA_Tradeshow_Activities']['fields']['contact_name_c']['enforced']='';
$dictionary['TA_Tradeshow_Activities']['fields']['contact_name_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TA_Tradeshow_Activities/Ext/Vardefs/sugarfield_company_name_c.php

 // created: 2019-04-23 12:15:59
$dictionary['TA_Tradeshow_Activities']['fields']['company_name_c']['labelValue']='Company Name';
$dictionary['TA_Tradeshow_Activities']['fields']['company_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TA_Tradeshow_Activities']['fields']['company_name_c']['enforced']='';
$dictionary['TA_Tradeshow_Activities']['fields']['company_name_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TA_Tradeshow_Activities/Ext/Vardefs/sugarfield_lead_quality_c.php

 // created: 2019-04-23 12:16:41
$dictionary['TA_Tradeshow_Activities']['fields']['lead_quality_c']['labelValue']='Lead Quality';
$dictionary['TA_Tradeshow_Activities']['fields']['lead_quality_c']['dependency']='';
$dictionary['TA_Tradeshow_Activities']['fields']['lead_quality_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TA_Tradeshow_Activities/Ext/Vardefs/sugarfield_follow_up_completed_c.php

 // created: 2019-04-23 12:17:19
$dictionary['TA_Tradeshow_Activities']['fields']['follow_up_completed_c']['labelValue']='Follow-Up Completed';
$dictionary['TA_Tradeshow_Activities']['fields']['follow_up_completed_c']['enforced']='';
$dictionary['TA_Tradeshow_Activities']['fields']['follow_up_completed_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TA_Tradeshow_Activities/Ext/Vardefs/ta_tradeshow_activities_tasks_1_TA_Tradeshow_Activities.php

// created: 2019-10-31 11:25:05
$dictionary["TA_Tradeshow_Activities"]["fields"]["ta_tradeshow_activities_tasks_1"] = array (
  'name' => 'ta_tradeshow_activities_tasks_1',
  'type' => 'link',
  'relationship' => 'ta_tradeshow_activities_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'id_name' => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/TA_Tradeshow_Activities/Ext/Vardefs/sugarfield_description.php

 // created: 2021-01-14 09:51:40
$dictionary['TA_Tradeshow_Activities']['fields']['description']['audited']=true;
$dictionary['TA_Tradeshow_Activities']['fields']['description']['massupdate']=false;
$dictionary['TA_Tradeshow_Activities']['fields']['description']['hidemassupdate']=false;
$dictionary['TA_Tradeshow_Activities']['fields']['description']['comments']='Full text of the note';
$dictionary['TA_Tradeshow_Activities']['fields']['description']['duplicate_merge']='enabled';
$dictionary['TA_Tradeshow_Activities']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['TA_Tradeshow_Activities']['fields']['description']['merge_filter']='disabled';
$dictionary['TA_Tradeshow_Activities']['fields']['description']['unified_search']=false;
$dictionary['TA_Tradeshow_Activities']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['TA_Tradeshow_Activities']['fields']['description']['calculated']=false;
$dictionary['TA_Tradeshow_Activities']['fields']['description']['rows']='6';
$dictionary['TA_Tradeshow_Activities']['fields']['description']['cols']='80';

 
?>
