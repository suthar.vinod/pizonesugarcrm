<?php
// created: 2018-04-23 17:50:10
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'contact_name_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_CONTACT_NAME',
    'width' => 10,
    'default' => true,
  ),
  'company_name_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_COMPANY_NAME',
    'width' => 10,
    'default' => true,
  ),
  'lead_quality_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_LEAD_QUALITY',
    'width' => 10,
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
);