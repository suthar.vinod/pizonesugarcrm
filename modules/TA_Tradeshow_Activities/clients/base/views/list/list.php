<?php
$module_name = 'TA_Tradeshow_Activities';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'tm_tradeshow_management_ta_tradeshow_activities_1_name',
                'label' => 'LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
                'enabled' => true,
                'id' => 'TM_TRADESH0F8EAGEMENT_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'contact_name_c',
                'label' => 'LBL_CONTACT_NAME',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'company_name_c',
                'label' => 'LBL_COMPANY_NAME',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'lead_quality_c',
                'label' => 'LBL_LEAD_QUALITY',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              7 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => false,
              ),
              9 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
