<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class SetStatusForORIHook
{

    function setORIEstimatearrivalDate($bean)
    {
        global $db, $app_list_strings;
        $productsDepartmentString = $app_list_strings['department_list'];

        $products_department_c = str_replace("^", "", $bean->products_department_c);
        $products_department = explode(',', $products_department_c);
        $product_Department = '';
        for ($x = 0; $x <= count($products_department); $x++) {
            $product_Department .= $productsDepartmentString[$products_department[$x]] . ',';
        }
        $product_Department = rtrim($product_Department, ',');
        $bean->products_department_c = $product_Department;

        $this->checkORIStatusAndSetPoStatus($bean);
    }

    function checkORIStatusAndSetPoStatus($order_request_item)
    {
        if ($order_request_item->po_purchase_order_ori_order_request_item_1po_purchase_order_ida != "") {

            $poid = $order_request_item->po_purchase_order_ori_order_request_item_1po_purchase_order_ida;

            $currentPOBean = BeanFactory::retrieveBean('PO_Purchase_Order', $poid);
            $currentPOBean->load_relationship('po_purchase_order_poi_purchase_order_item_1');
            $currentPOBean->load_relationship('po_purchase_order_ori_order_request_item_1');
            /** Load poi and ori bean to get all the linked record of poi and ori */
            $relatedPOI = $currentPOBean->po_purchase_order_poi_purchase_order_item_1->get();
            $relatedORI = $currentPOBean->po_purchase_order_ori_order_request_item_1->get();
            /**linkedrecstatus array : To store the status valye of all the linked poi and ori records  */
            $linkedrecstatus = array();

            /*find all the lined poi record and get the status value */
            foreach ($relatedPOI as $POIid) {
                $clonedBeanPOI = BeanFactory::getBean('POI_Purchase_Order_Item', $POIid);
                $poi_status = $clonedBeanPOI->status;
                if ($poi_status != 'Inventory') {
                    $linkedrecstatus[] = $poi_status;
                }
            }
            /*find all the lined ori record and get the status value */
            foreach ($relatedORI as $ORIid) {
                $clonedBeanORI = BeanFactory::getBean('ORI_Order_Request_Item', $ORIid);
                $ori_status = $clonedBeanORI->status;
                if ($ori_status != 'Inventory') {
                    $linkedrecstatus[] = $ori_status;
                }
            }
            /**25 nov 2021 : #1778 : Status field on Purchase Orders to automatically update to read only 
             * Complete if all of the ORIs and POIs linked to the PO have Status = Fully Received. */
            if ((count($linkedrecstatus) == 0) && (count($relatedPOI) != 0 || count($relatedORI) != 0)) {
                /**To store the old value of status field , we are using status_old_c field */
                $currentPOBean->status_old_c = $currentPOBean->fetched_row['status_c'];
                $currentPOBean->status_c = "Complete";
                $currentPOBean->save();
            } else {
                /**In case status valye changed from full recevied to other then it will pull the old value to status field */
                if ($currentPOBean->status_old_c != '') {
                    $currentPOBean->status_c = $currentPOBean->status_old_c;
                    $currentPOBean->save();
                }
            }
        }
    }
}
