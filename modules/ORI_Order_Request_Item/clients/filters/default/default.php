<?php
// created: 2022-03-24 13:33:01
$viewdefs['ORI_Order_Request_Item']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'date_entered' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'client_cost_c' => 
    array (
    ),
    'concentration' => 
    array (
    ),
    'concentration_unit' => 
    array (
    ),
    'cost_per_unit' => 
    array (
    ),
    'department_new_c' => 
    array (
    ),
    'estimated_arrival_date' => 
    array (
    ),
    'manufacturer' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'name' => 
    array (
    ),
    'notes_c' => 
    array (
    ),
    'or_order_request_ori_order_request_item_1_name' => 
    array (
    ),
    'owner' => 
    array (
    ),
    'prod_product_ori_order_request_item_1_name' => 
    array (
    ),
    'product_id_2' => 
    array (
    ),
    'product_name' => 
    array (
    ),
    'product_selection' => 
    array (
    ),
    'po_purchase_order_ori_order_request_item_1_name' => 
    array (
    ),
    'purchase_unit_2_c' => 
    array (
    ),
    'quality_requirement' => 
    array (
    ),
    'related_to' => 
    array (
    ),
    'request_date' => 
    array (
    ),
    'required_by_date' => 
    array (
    ),
    'm01_sales_ori_order_request_item_1_name' => 
    array (
    ),
    'status' => 
    array (
    ),
    'subtype' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'type_2' => 
    array (
    ),
    'unit_quantity_c' => 
    array (
    ),
    'vendor_name' => 
    array (
    ),
    'm03_work_product_ori_order_request_item_1_name' => 
    array (
    ),
  ),
);