({
    extendsFrom: 'EditablelistbuttonField',
    initialize: function (options) {
        this._super("initialize", [options]);
        this.model.on('change: error_category_c', this.function_error_category_c,this);
       
    },
      
    _loadTemplate: function () {
        this._super('_loadTemplate');
        var status = this.model.get('status');
        if (status == "Requested" || status == "Inventory") {
            $('input[name="status"]').prop('disabled', true);
            $('[name="status"]').attr('readOnly', true);
        } else {
            $('input[name="status"]').prop('disabled', false);
            $('[name="status"]').attr('readOnly', false);
        }
    },
})
