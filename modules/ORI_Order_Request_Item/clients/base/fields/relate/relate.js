({
    extendsFrom: 'RelateField',
    initialize: function (options) {
        this._super('initialize', [options]);
        //alert('laxlaxlax');
    },
    openSelectDrawer: function () {
        if (!_.isEmpty(this.model.get('department_new_c')) && this.model.get('department_new_c') != null && this.model.get('type_2') != null) {
            //console.log('lllax');
            this.selectProd_Product_nosubtype();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
		}
        
        else {
            this._super('openSelectDrawer');
            $("button.btn.btn-invisible.btn-dark").css("display", "block");
            $(".filter-definition-container").css('pointer-events', 'unset');
            $(".choice-filter-clickable").css('pointer-events', 'unset');

        }
    }, 
		
	selectProd_Product_nosubtype: function () {
        var department = this.model.get('department_new_c');
        var type_2 = this.model.get('type_2');
		//console.log('lllaxtype_2 ',type_2);
        var filterOptions = new app.utils.FilterOptions()
                .config({
                    'initial_filter': 'FilterProd_ProductNSBTemplate',
                    'initial_filter_label': 'Product(Department)',
                    'filter_populate': {
                        'department': [department],
						'type_2': [type_2],
                        'status_c': ['Out of Production']
                    }, 
                })
                //.populateRelate(this.model)
                .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "Prod_Product") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
 
	/*To disable search text box filter of Test System */
    search: _.debounce(function (query) {
        var term = query.term || '',
                self = this,
                searchModule = this.getSearchModule(),
                params = {},
                limit = self.def.limit || 5,
                relatedModuleField = this.getRelatedModuleField();

        if (query.context) {
            params.offset = this.searchCollection.next_offset;
        }

        //console.log(params);
        params.filter = this.buildFilterDefinition(term);

        this.searchCollection.fetch({
            //Don't show alerts for this request
            showAlerts: false,
            update: true,
            remove: _.isUndefined(params.offset),
            fields: this.getSearchFields(),
            context: self,
            params: params,
            //limit: limit,
            success: function (data) {
                var fetch = {results: [], more: data.next_offset > 0, context: data};
                if (fetch.more) {
                    var fieldEl = self.$(self.fieldTag),
                            //For teamset widget, we should specify which index element to be filled in
                            plugin = (fieldEl.length > 1) ? $(fieldEl.get(self.currentIndex)).data("select2") : fieldEl.data("select2"),
                            height = plugin.searchmore.children("li:first").children(":first").outerHeight(),
                            //0.2 makes scroll not to touch the bottom line which avoid fetching next record set
                            maxHeight = height * (limit - .2);
                    plugin.results.css("max-height", maxHeight);
                }
                _.each(data.models, function (model, index) {
                    if (params.offset && index < params.offset) {
                        return;
                    }
                    fetch.results.push({
                        id: model.id,
                        text: model.get(relatedModuleField) + ''
                    });
                });
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback(fetch);
                }
            },
            error: function () {
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback({results: []});
                }
                app.logger.error("Unable to fetch the bean collection.");
            }
        });

    }, app.config.requiredElapsed || 500),
    
	_getProd_ProductNoSubtype: function (searchTerm) {
       var department = this.model.get('department_new_c');
       var type_2 = this.model.get('type_2');
       return [{
        'type_2': {
          '$in': [type_2],
        },
        'department': {
          '$contains': [department],
        },
        'name': {
          '$contains': [searchTerm],
        },
        'status_c': {
            '$not_in': ['Out of Production'],
        }
      }];
    },
  
    /**location_equipment
     * @override
     */
    buildFilterDefinition: function (searchTerm) {
        var parentFilter = this._super('buildFilterDefinition', [searchTerm]);
        
        if (this.getSearchModule() == 'Prod_Product' && this.model.get('department_new_c') != "" && this.model.get('department_new_c') != null && this.model.get('type_2') != null) {
            console.log('get product subtype : ',this._getProd_ProductNoSubtype());
            return this._getProd_ProductNoSubtype(searchTerm);       
        }else {
            return parentFilter;
        }
    },
    /**EOC Disable search filter code */
})