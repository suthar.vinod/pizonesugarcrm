<?php
/*
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class DuplicateORIItemApi extends SugarApi {

    public function registerApiRest() {
        return array(
            'MyGetEndpoint' => array(
                'reqType' => 'POST',
                'noLoginRequired' => false,
                'path' => array('create_ori_duplicate'),
                'pathVars' => array(),
                'method' => 'create_ori_duplicate',
                'shortHelp' => '',
                'longHelp' => '',
            ),
        );
    }

    public function create_ori_duplicate($api, $args) {
        global $db, $current_user;
        $module = $args['module'];
        $recordId = $args['record'];
        //$orderDate = $args['order_date'];
        $GLOBALS['log']->fatal('ORI API args ' . print_r($args,1));
        $clonedBean = BeanFactory::retrieveBean($module, $recordId);

        $clonedBean->load_relationship("prod_product_ori_order_request_item_1");
        $clonedBean->load_relationship("po_order_request_ori_order_request_item_1");
        $clonedBean->load_relationship("po_purchase_order_ori_order_request_item_1");
        $clonedBean->load_relationship("m03_work_product_ori_order_request_item_1");
        $clonedBean->load_relationship("m01_sales_ori_order_request_item_1");

        $clonedBean->load_relationship('ori_order_request_item_ii_inventory_item_1');

        if (is_numeric($clonedBean->unit_quantity_requested)) {
            $quantity = intval($clonedBean->unit_quantity_requested);

            $product_id = $clonedBean->prod_product_ori_order_request_item_1->get();
            $or_id = $clonedBean->or_order_request_ori_order_request_item_1->get();
            $po_id = $clonedBean->po_purchase_order_ori_order_request_item_1->get();
            $wp_id = $clonedBean->m03_work_product_ori_order_request_item_1->get();
            $sale_id = $clonedBean->m01_sales_ori_order_request_item_1->get();
            $relatedII = $clonedBean->ori_order_request_item_ii_inventory_item_1->get();

            $or_id1 = $clonedBean->or_order_request_ori_order_request_item_1or_order_request_ida;
            $clonedBean->Duplicate_ORI = 'Duplicate ORI';
            for ($i = 0; $i < $quantity - 1; $i++) {

                $orBean = BeanFactory::newBean('ORI_Order_Request_Item');
                $bid = create_guid();
                $orBean->id = $bid;
                $orBean->new_with_id = true;
                $orBean->fetched_row = null;



                $orBean->or_order_request_ori_order_request_item_1or_order_request_ida = $or_id1;
                //$orBean->name   = $clonedBean->name;    
                $orBean->request_date = $clonedBean->request_date;                
                $orBean->status = $clonedBean->status;
                $orBean->department_new_c = $clonedBean->department_new_c;
                $orBean->date_entered = $clonedBean->date_entered;
                $orBean->date_modified = $clonedBean->date_modified;
                $orBean->description = $clonedBean->description;
                $orBean->owner = $clonedBean->owner;
                $orBean->type_2 = $clonedBean->type_2;
                $orBean->product_name = $clonedBean->product_name;
                $orBean->product_id_2 = $clonedBean->product_id_2;
                $orBean->vendor_name = $clonedBean->vendor_name;
                $orBean->purchase_unit = $clonedBean->purchase_unit;
                $orBean->cost_per_unit = $clonedBean->cost_per_unit;
                $orBean->required_by_date = $clonedBean->required_by_date;                
                $orBean->related_to = $clonedBean->related_to;
                $orBean->product_selection = $clonedBean->product_selection;
                $orBean->subtype = $clonedBean->subtype;
                $orBean->manufacturer = $clonedBean->manufacturer;
                $orBean->concentration = $clonedBean->concentration;
                $orBean->concentration_unit = $clonedBean->concentration_unit;
                $orBean->quality_requirement = $clonedBean->quality_requirement;
                $orBean->estimated_arrival_date = $clonedBean->estimated_arrival_date;
                $orBean->unit_quantity_requested = $clonedBean->unit_quantity_requested;
                $orBean->u_units_id_c = $clonedBean->u_units_id_c;

                $orBean->Duplicate_ORI = 'Duplicate ORI';

                $orBean->save();
                $orBean->prod_product_ori_order_request_item_1->add($product_id);
                $orBean->or_order_request_ori_order_request_item_1->add($or_id);
                $orBean->po_purchase_order_ori_order_request_item_1->add($po_id);
                $orBean->m03_work_product_ori_order_request_item_1->add($wp_id);
                $orBean->m01_sales_ori_order_request_item_1->add($sale_id);

                $orBean->purchase_unit_2_c = $clonedBean->purchase_unit_2_c;
                $orBean->unit_quantity_c = $clonedBean->unit_quantity_c;
                $orBean->client_cost_c = $clonedBean->client_cost_c;
                $orBean->save();
            }
        }

        return true;
    }

}
*/