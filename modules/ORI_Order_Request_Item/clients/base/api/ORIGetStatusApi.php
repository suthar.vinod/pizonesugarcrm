<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class ORIGetStatusApi extends SugarApi
{

    public function registerApiRest()
    {
        return array(
            'MyGetEndpoint' => array(
                'reqType' => 'POST',
                'noLoginRequired' => false,
                'path' => array('get_status_data'),
                'pathVars' => array(),
                'method' => 'get_status_data',
                'shortHelp' => '',
                'longHelp' => '',
            ),
        );
    }

    public function get_status_data($api, $args)
    {
        global $db, $current_user;
        $moduleId = $args['moduleId'];
        $PO_ID = $args['PO_ID'];
        $GLOBALS['log']->fatal('API  args '.print_r($args,1));
        $oriBean = BeanFactory::retrieveBean("ORI_Order_Request_Item", $moduleId);
 
        $oriBean->load_relationship('ori_order_request_item_ri_received_items_1');
        $relatedRI = $oriBean->ori_order_request_item_ri_received_items_1->get();

        $GLOBALS['log']->fatal('API relatedRI '.print_r($relatedRI,1));

        $total = 0;
        for($a=0;$a<count($relatedRI);$a++){
            $RI_Received_Items = BeanFactory::retrieveBean('RI_Received_Items', $relatedRI[$a]);
            $quantity_received = $RI_Received_Items->quantity_received;             
            $total = $total+$quantity_received;            
        }    
        $unit_quantity_requested = $oriBean->unit_quantity_requested;

        $poBean = BeanFactory::retrieveBean("PO_Purchase_Order", $PO_ID);
        $POStatus = $poBean->status_c;
        $POname = $poBean->name;
        $GLOBALS['log']->fatal('API POStatus '.$POStatus);
        $GLOBALS['log']->fatal('API POname '.$POname);

        $GLOBALS['log']->fatal('$API equest_date '.$oriBean->request_date);
        $uniqueArr =  array();
        $uniqueArr["Ordered"] = "Ordered";
        $uniqueArr["Backordered"] = "Backordered";
        
        $status = "";

        if ($oriBean->request_date != "" && $POname == "" && count($relatedRI)<= 0) {
            $status = "Requested";
            $GLOBALS['log']->fatal('API Requested1 '.$status);
        }else if ($oriBean->request_date != "" && $POname != "" && $POStatus == "Pending" && count($relatedRI)<= 0) {
            $status = "Requested";
            $GLOBALS['log']->fatal('API Requested2 '.$status);
        } else if ($oriBean->request_date != "" && $POname !="" && $POStatus == "Submitted" && count($relatedRI) <= 0) {
            if($oriBean->status == "Ordered" || $oriBean->status == "Backordered"){
                $status = $uniqueArr[$oriBean->status];
            }else{
                $status = "Ordered";
            }
             
            $GLOBALS['log']->fatal('API Ordered '.$status);
        }  else  if(count($relatedRI) > 0 && $unit_quantity_requested == $total){
                $status = "Inventory";
                $GLOBALS['log']->fatal('API Inventory '.$status);
        } else if(count($relatedRI) > 0 && $unit_quantity_requested != $total) {                   
              $status = "Partially Received";
              $GLOBALS['log']->fatal('API Received '.$status);
         }  
         

        return $status;
    }
}
