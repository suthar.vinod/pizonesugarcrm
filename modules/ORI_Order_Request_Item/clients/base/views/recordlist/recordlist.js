/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @extends View.Views.Base.RecordlistView
 */
({
    extendsFrom: 'RecordlistView',
    initialize: function (options) {
        console.log('This is Quotes RecordlistView');
        this._super('initialize', [options]);
    },
    /* #1274 : 10 May 2021 : gsingh */
    /* override method addActions in the js controller of RecordListView  */
    addActions: function () {
        console.log('addActions');
        this._super("addActions");
    },
    /** * EOC #1274 : 10 May 2021 : gsingh */
    _bindEvents: function () {
        var self = this;

        self._super("_bindEvents");
        if (self.layout) {
            self.layout.on('list:rimasscreaterecords:fire', self.rimasscreaterecordsClicked, self);
        }
    },
    rimasscreaterecordsClicked: function () {
        var self = this;
        var openDrawer = 1;
        var oriStatusArr = []; 
        _.each(self.fields, function (field) {  //alert("dsfdd");
            if (field.$el.find('input[data-check="one"]:checked').length > 0) {

                var oriName = field.model.get('name');
                var oriStatus = field.model.get('status');
                oriStatusArr.push(oriStatus);
                
            }
        }); 
        console.log('oriStatusArr',oriStatusArr);
        if ($.inArray('Inventory', oriStatusArr) == -1 && $.inArray('Requested', oriStatusArr) == -1) {
           openDrawer = 1;
           app.alert.dismiss('rieMassCreationCon');
        }else{
            app.alert.show("rieMassCreationCon", {
                level: 'info',
                messages: 'Received Item Mass Create Not Allowed.',
                autoClose: false,
            });
            openDrawer = 0;
        }
        console.log('openDrawer',openDrawer);      
        if (openDrawer) {
            app.drawer.open({
                layout: 'rimasscreate',
                context: {
                    create: true,
                    module: 'RI_Received_Items',
                }
            });
        } else {
            return false;
        }
    },
     

})