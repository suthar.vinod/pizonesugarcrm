({
	extendsFrom: 'RecordView',
	/**
	 * @inheritdoc
	 */
	initialize: function (options) {

		this._super('initialize', [options]);

		this.model.on('sync', _.bind(this._readonlyFields, this));
		this.model.addValidationTask('check_account_type', _.bind(this._doValidateProduct, this));
		this.context.on('button:edit_button:click', this.editClicked, this);
		
		this.model.once("sync",
            function () {
                this.model.on(
                    "change:product_selection",
                    this.emptyFieldOnChangePS,
                    this
                );
            },
            this
        );

		this.model.once("sync",
			function () {
				this.model.on(
					"change:change:type_2",
					this.type_2FieldOnChange,
					this
				);
			},
			this
		);

        this.model.once("sync",
            function () {
                this.model.on(
                    "change:prod_product_ori_order_request_item_1_name",
                    this.setSubtype,
                    this
                );
            },
            this
        );

	},

	type_2FieldOnChange: function () {
		var self = this;
		setTimeout(function () {
			var product_selection = self.model.get("product_selection");
			if (product_selection == "In System Vendor Non Specific" || product_selection == "In System Vendor Specific") {
				self.$el.find('input[name="subtype"]').prop('disabled', true);
			} else {
				self.$el.find('input[name="subtype"]').prop('disabled', false);
			}
		}, 300);
	},
	
    setSubtype: function () {
        var Product_ID = this.model.get('prod_product_ori_order_request_item_1prod_product_ida');
        //console.log('setSubtype Product_ID',Product_ID);
        if (Product_ID != "" && Product_ID != null) {
            var self = this;
            App.api.call("get", "rest/v10/Prod_Product/" + Product_ID + "?fields=subtype", null, {
                success: function (Data) {
                    self.model.set('subtype', Data.subtype);
                }
            });
        }else{
            this.model.set('subtype', '');
        }
    },
	render: function () {
		var superResult = this._super("render", arguments);
		//this.$el.find('[data-name="unit_quantity_requested"]').css('visibility', 'hidden');
		this.noEditFields.push('name');
		//this.$el.find('.record-edit-link-wrapper[data-name=status]').remove();

	},
	emptyFieldOnChangePS: function () {
        var self = this;
        self.model.set("product_name", null);
        self.model.set("product_id_2", null);
        self.model.set("manufacturer", null);
        self.model.set("vendor_name", null);

        if (this.model.get('po_purchase_order_ori_order_request_item_1_name') == "" || this.model.get('po_purchase_order_ori_order_request_item_1_name') == undefined) {
            this.$el.find('.record-edit-link-wrapper[data-name=cost_per_unit]').hide();
            this.$el.find('input[name="cost_per_unit"]').prop('disabled', true);
        } else {
            this.$el.find('.record-edit-link-wrapper[data-name=cost_per_unit]').show();
            this.$el.find('input[name="cost_per_unit"]').prop('disabled', false);
        }

		setTimeout(function () {
			var product_selection = self.model.get("product_selection");
			if (product_selection == "In System Vendor Non Specific" || product_selection == "In System Vendor Specific") {
				self.$el.find('input[name="subtype"]').prop('disabled', true);
			} else {
				self.$el.find('input[name="subtype"]').prop('disabled', false);
			}
		}, 300);
    },
	_readonlyFields: function () {
		this.model.defaultCostReord = this.model.get('cost_per_unit');
		if (this.model.get('status') == "Requested" || this.model.get('status') == "Inventory") {
			this.noEditFields.push('status');
			//this.$el.find('.record-edit-link-wrapper[data-name=status]').remove();
		}

		if ((this.model.get('product_selection') == 'In System Vendor Non Specific' || this.model.get('product_selection') == 'In System Vendor Specific') && this.model.get('type_2') != '' && this.model.get('department_new_c') != '') {
			this.$el.find('[data-name="prod_product_ori_order_request_item_1_name"]').css('visibility', 'visible');
		}else {
			this.$el.find('[data-name="prod_product_ori_order_request_item_1_name"]').css('visibility', 'hidden');
		}

		this.$el.find('.record-edit-link-wrapper[data-name=po_purchase_order_ori_order_request_item_1_name]').remove();
		this.$el.find('.record-edit-link-wrapper[data-name=request_date]').remove();
		//this.$el.find('.record-edit-link-wrapper[data-name=received_date]').remove();

		this.model.on('change:po_purchase_order_ori_order_request_item_1_name', this.setArrivalDateRecord, this);
		this.model.on('change:prod_product_ori_order_request_item_1_name', this.setCostUnitRecord, this);
		this.model.on('change:request_date change:po_purchase_order_ori_order_request_item_1po_purchase_order_ida change:received_date', this.setStatusRecord, this);

		this.model.on('change:product_selection', this.setProductVisibilityRecord, this);
		this.model.on('change:type_2', this.setProductVisibilityRecord, this);
		this.model.on('change:department_new_c', this.setProductVisibilityRecord, this);
		this.model.on('change:subtype', this.setProductVisibilityRecord, this);

	},
	setStatusRecord: function () {

		if ((this.model.get('request_date') != '' || this.model.get('request_date') != undefined) && (this.model.get('received_date') == '' || this.model.get('received_date') == undefined) && ((this.model.get('po_purchase_order_ori_order_request_item_1po_purchase_order_ida') == undefined) || this.model.get('po_purchase_order_ori_order_request_item_1po_purchase_order_ida') == '') && (this.model._relatedCollections.ori_order_request_item_ii_inventory_item_1.length <= 0)) {
			this.model.set('status', 'Requested');
		} else if (this.model.get('request_date') != '' && (this.model.get('received_date') == '' || this.model.get('received_date') == undefined) && this.model.get('po_purchase_order_ori_order_request_item_1po_purchase_order_ida') != '' && this.model._relatedCollections.ori_order_request_item_ii_inventory_item_1.length <= 0) {
			this.model.set('status', 'Ordered');
		} else if (this.model.get('request_date') != '' && this.model.get('received_date') != '' && this.model.get('po_purchase_order_ori_order_request_item_1po_purchase_order_ida') != '' && this.model._relatedCollections.ori_order_request_item_ii_inventory_item_1.length > 0) {
			this.model.set('status', 'Inventory');
		} else {
			//this.model.set('status', '');
		}
	},
	_doValidateProduct: function (fields, errors, callback) {
		if (this.model.get('product_selection') == "In System Vendor Non Specific" || this.model.get('product_selection') == "In System Vendor Specific") {
			if (_.isEmpty(this.model.get('prod_product_ori_order_request_item_1_name'))) {
				errors['prod_product_ori_order_request_item_1_name'] = errors['prod_product_ori_order_request_item_1_name'] || {};
				errors['prod_product_ori_order_request_item_1_name'].required = true;
			}
		}
		callback(null, fields, errors);
	},

	setArrivalDateRecord: function (model) {
		if (this.model._previousAttributes.po_purchase_order_ori_order_request_item_1po_purchase_order_ida != this.model.get('po_purchase_order_ori_order_request_item_1po_purchase_order_ida')) {
			var url = app.api.buildURL("getArrivalDateValue");
			var method = "create";
			var data = {
				po_id: this.model.get('po_purchase_order_ori_order_request_item_1po_purchase_order_ida')
			};
			var callback = {
				success: _.bind(function successCB(res) {
					this.model.set('estimated_arrival_date', res);

				}, this)
			};
			app.api.call(method, url, data, callback);
		}
	},

	setCostUnitRecord: function (model) {
		if (this.model._previousAttributes.prod_product_ori_order_request_item_1prod_product_ida != this.model.get('prod_product_ori_order_request_item_1prod_product_ida')) {

			var url = app.api.buildURL("getCostPerUnit");
			var method = "create";
			var data = {
				p_id: this.model.get('prod_product_ori_order_request_item_1prod_product_ida')
			};
			var callback = {
				success: _.bind(function successCB(res) {
					this.model.set('cost_per_unit', res);

				}, this)
			};
			app.api.call(method, url, data, callback);
		}
	},

	setProductVisibilityRecord: function (model) {

		if (this.model._previousAttributes.type_2 != '' && this.model._previousAttributes.type_2 != undefined && this.model._previousAttributes.type_2 != this.model.get('type_2')) {
			this.model.unset('prod_product_ori_order_request_item_1prod_product_ida');
			this.model.unset('prod_product_ori_order_request_item_1_name');
			this.model.set('prod_product_ori_order_request_item_1prod_product_ida','');
			this.model.set('prod_product_ori_order_request_item_1_name','');
			fieldRef = this.getField('prod_product_ori_order_request_item_1_name');
			fieldRef.render();
		}
		if (this.model._previousAttributes.department_new_c != '' && this.model._previousAttributes.department_new_c != undefined && this.model._previousAttributes.department_new_c != this.model.get('department_new_c')) {
			this.model.unset('prod_product_ori_order_request_item_1prod_product_ida');
			this.model.unset('prod_product_ori_order_request_item_1_name');
			this.model.set('prod_product_ori_order_request_item_1prod_product_ida','');
			this.model.set('prod_product_ori_order_request_item_1_name','');
			fieldRef = this.getField('prod_product_ori_order_request_item_1_name');
			fieldRef.render();
		}
		if (this.model._previousAttributes.subtype != undefined && this.model._previousAttributes.subtype != this.model.get('subtype')) {
			this.model.unset('prod_product_ori_order_request_item_1prod_product_ida');
			this.model.unset('prod_product_ori_order_request_item_1_name');
			this.model.set('prod_product_ori_order_request_item_1prod_product_ida','');
			this.model.set('prod_product_ori_order_request_item_1_name','');
			fieldRef = this.getField('prod_product_ori_order_request_item_1_name');
			fieldRef.render();
		}
		var inSystemList = ['In System Vendor Non Specific', 'In System Vendor Specific'];

		if ((inSystemList.includes(this.model._previousAttributes.product_selection) == false) && inSystemList.includes(this.model.get('product_selection'))) {
			this.model.unset('prod_product_ori_order_request_item_1prod_product_ida');
			this.model.unset('prod_product_ori_order_request_item_1_name');
			this.model.set('prod_product_ori_order_request_item_1prod_product_ida','');
			this.model.set('prod_product_ori_order_request_item_1_name','');
			fieldRef = this.getField('prod_product_ori_order_request_item_1_name');
			fieldRef.render();
		}

		if ((this.model.get('product_selection') == 'In System Vendor Non Specific' || this.model.get('product_selection') == 'In System Vendor Specific') && this.model.get('type_2') != '' && this.model.get('department_new_c') != '') {
			
				this.model.set('prod_product_ori_order_request_item_1_name', '');
				this.model.set('attributes.prod_product_ori_order_request_item_1prod_product_ida', '');
				this.$el.find('[data-name="prod_product_ori_order_request_item_1_name"]').css('visibility', 'hidden');
		} else if ((this.model.get('product_selection') == 'In System Vendor Non Specific' || this.model.get('product_selection') == 'In System Vendor Specific') && this.model.get('type_2') != '' && this.model.get('department_new_c') != '') {
			this.$el.find('[data-name="prod_product_ori_order_request_item_1_name"]').css('visibility', 'visible');
		} else {
			this.model.set('prod_product_ori_order_request_item_1_name', '');
			this.model.set('prod_product_ori_order_request_item_1prod_product_ida', '');
			this.$el.find('[data-name="prod_product_ori_order_request_item_1_name"]').css('visibility', 'hidden');
		}
	},
	editClicked: function () {
		this.model.defaultCostReord = this.model.get('cost_per_unit');
		this._super('editClicked');
	},
	handleSave: function () {
		if (this.model.defaultCostReord != undefined && this.model.defaultCostReord != this.model.get('cost_per_unit') && this.model.get('prod_product_ori_order_request_item_1_name') != '' && this.model.get('prod_product_ori_order_request_item_1_name') != undefined) {
			var productID = this.model.get('prod_product_ori_order_request_item_1prod_product_ida');
			var costPerUnit = this.model.get('cost_per_unit');
			var self = this;
			app.alert.show('message-id', {
				level: 'confirmation',
				messages: 'Cost Per Unit field was updated. Click Confirm to edit corresponding Product record.',
				autoClose: false,
				onConfirm: function () {
					app.api.call("create", "rest/v10/update_product_cost_unit", {
						module: 'Prod_Product',
						product: productID,
						cost_per_unit: costPerUnit,
					}, {
						success: function (result) {
							self._super('handleSave');
						},
						error: function (error) {
							app.alert.show('link_wps_error', {
								level: 'error',
								messages: 'Failed Creating Duplicates!'
							});
						}
					});
				},
				onCancel: function () {
					self._super('handleSave');
				}
			});
		} else {
			this._super('handleSave');
		}
	},
})