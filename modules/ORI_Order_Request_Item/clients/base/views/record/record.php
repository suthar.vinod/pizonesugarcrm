<?php
$module_name = 'ORI_Order_Request_Item';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'ORI_Order_Request_Item',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'related_to',
                'label' => 'LBL_RELATED_TO',
              ),
              1 => 
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
              ),
              2 => 
              array (
                'name' => 'm03_work_product_ori_order_request_item_1_name',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'name' => 'm01_sales_ori_order_request_item_1_name',
              ),
              5 => 
              array (
              ),
              6 => 
              array (
                'name' => 'owner',
                'label' => 'LBL_OWNER',
                'span' => 6,
              ),
              7 => 
              array (
                'span' => 6,
              ),
              8 => 
              array (
                'name' => 'department_new_c',
                'label' => 'LBL_DEPARTMENT_NEW',
              ),
              9 => 
              array (
              ),
              10 => 
              array (
                'name' => 'product_selection',
                'label' => 'LBL_PRODUCT_SELECTION',
              ),
              11 => 
              array (
              ),
              12 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
              ),
              13 => 
              array (
              ),
              14 => 
              array (
                'name' => 'prod_product_ori_order_request_item_1_name',
              ),
              15 => 
              array (
                'name' => 'subtype',
                'label' => 'LBL_SUBTYPE',
              ),
              16 => 
              array (
                'name' => 'product_name',
                'label' => 'LBL_PRODUCT_NAME',
              ),
              17 => 
              array (
                'name' => 'manufacturer',
                'label' => 'LBL_MANUFACTURER',
              ),
              18 => 
              array (
                'name' => 'product_id_2',
                'label' => 'LBL_PRODUCT_ID_2',
              ),
              19 => 
              array (
                'name' => 'vendor_name',
                'label' => 'LBL_VENDOR_NAME',
              ),
              20 => 
              array (
                'name' => 'description',
                'comment' => 'Full text of the note',
                'studio' => 'visible',
                'label' => 'LBL_DESCRIPTION',
                'span' => 12,
              ),
              21 => 
              array (
                'name' => 'concentration',
                'label' => 'LBL_CONCENTRATION',
              ),
              22 => 
              array (
                'name' => 'concentration_unit',
                'studio' => 'visible',
                'label' => 'LBL_CONCENTRATION_UNIT',
              ),
              23 => 
              array (
                'name' => 'quality_requirement',
                'label' => 'LBL_QUALITY_REQUIREMENT',
              ),
              24 => 
              array (
              ),
              25 => 
              array (
                'name' => 'purchase_unit_2_c',
                'label' => 'LBL_PURCHASE_UNIT_2',
              ),
              26 => 
              array (
                'name' => 'unit_quantity_c',
                'label' => 'LBL_UNIT_QUANTITY',
              ),
              27 => 
              array (
                'name' => 'cost_per_unit',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_COST_PER_UNIT',
              ),
              28 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'client_cost_c',
                'label' => 'LBL_CLIENT_COST',
              ),
              29 => 
              array (
                'name' => 'unit_quantity_requested',
                'label' => 'LBL_UNIT_QUANTITY_REQUESTED',
              ),
              30 => 
              array (
                'name' => 'request_date',
                'label' => 'LBL_REQUEST_DATE',
              ),
              31 => 
              array (
                'name' => 'required_by_date',
                'label' => 'LBL_REQUIRED_BY_DATE',
              ),
              32 => 
              array (
                'name' => 'estimated_arrival_date',
                'label' => 'LBL_ESTIMATED_ARRIVAL_DATE',
              ),
              33 => 
              array (
                'name' => 'po_purchase_order_ori_order_request_item_1_name',
              ),
              34 => 
              array (
              ),
              35 => 
              array (
                'name' => 'or_order_request_ori_order_request_item_1_name',
              ),
              36 => 
              array (
              ),
              37 => 
              array (
                'readonly' => false,
                'name' => 'notes_c',
                'studio' => 'visible',
                'label' => 'LBL_NOTES',
                'span' => 12,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
