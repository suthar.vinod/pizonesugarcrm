<?php
// created: 2021-10-21 05:06:09
$viewdefs['ORI_Order_Request_Item']['base']['view']['subpanel-for-prod_product-prod_product_ori_order_request_item_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'related_to',
          'label' => 'LBL_RELATED_TO',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'department_new_c',
          'label' => 'LBL_DEPARTMENT_NEW',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'owner',
          'label' => 'LBL_OWNER',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'request_date',
          'label' => 'LBL_REQUEST_DATE',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'estimated_arrival_date',
          'label' => 'LBL_ESTIMATED_ARRIVAL_DATE',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'required_by_date',
          'label' => 'LBL_REQUIRED_BY_DATE',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'received_date',
          'label' => 'LBL_RECEIVED_DATE',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'm01_sales_ori_order_request_item_1_name',
          'label' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_M01_SALES_TITLE',
          'enabled' => true,
          'id' => 'M01_SALES_ORI_ORDER_REQUEST_ITEM_1M01_SALES_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'm03_work_product_ori_order_request_item_1_name',
          'label' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
          'enabled' => true,
          'id' => 'M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1M03_WORK_PRODUCT_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);