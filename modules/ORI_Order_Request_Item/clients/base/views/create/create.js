({
    extendsFrom: 'CreateView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.on('render', _.bind(this.onload_function, this));
        this.model.on("change:product_selection", this.emptyFieldOnChangePS, this);
        this.model.on("change:type_2", this.type_2FieldOnChangePS, this);
        this.model.on('change:prod_product_ori_order_request_item_1_name', this.setSubtype, this);
        this.model.on('change:department_new_c', this.setProductnameVisibility, this);
        this.model.on('change:type_2', this.setProductnameVisibility, this);
    },

    setSubtype: function () {
        //console.log('setSubtype');
        var Product_ID = this.model.get('prod_product_ori_order_request_item_1prod_product_ida');
        console.log('Product_ID',Product_ID);
        if (Product_ID != "" && Product_ID != null) {
            var self = this;
            App.api.call("get", "rest/v10/Prod_Product/" + Product_ID + "?fields=subtype", null, {
                success: function (Data) {
                    //console.log('Data.subtyp',Data.subtype);
                    self.model.set('subtype', Data.subtype);
                }
            });
        }else{
            this.model.set('subtype', '');
        }
    },

    onload_function: function () {
        var self = this;
        setTimeout(function () {
            self.$el.find('input[name="cost_per_unit"]').prop('disabled', true);
        }, 1000);
    },

    type_2FieldOnChangePS: function () {
        var self = this;
        setTimeout(function () {
            var product_selection = self.model.get("product_selection");
            if (product_selection == "In System Vendor Non Specific" || product_selection == "In System Vendor Specific") {
                self.$el.find('input[name="subtype"]').prop('disabled', true);
            } else {
                self.$el.find('input[name="subtype"]').prop('disabled', false);
            }
        }, 300);
    },

    emptyFieldOnChangePS: function () {
        var self = this;
        self.model.set("product_name", null);
        self.model.set("product_id_2", null);
        self.model.set("manufacturer", null);
        self.model.set("vendor_name", null);
        
        setTimeout(function () {
            var product_selection = self.model.get("product_selection");
            if (product_selection == "In System Vendor Non Specific" || product_selection == "In System Vendor Specific") {
                self.$el.find('input[name="subtype"]').prop('disabled', true);
            } else {
                self.$el.find('input[name="subtype"]').prop('disabled', false);
            }
        }, 300);

        setTimeout(function () {
            self.$el.find('input[name="cost_per_unit"]').prop('disabled', true);
        }, 1000);
    },
    /**18 July 2022 : #2662 bug fix */
    setProductnameVisibility: function () {
        console.log('file ori create js line 71');        
        var self = this;
		    self.model.unset('prod_product_ori_order_request_item_1prod_product_ida');
			self.model.unset('prod_product_ori_order_request_item_1_name');
			self.model.set('prod_product_ori_order_request_item_1prod_product_ida','');
			self.model.set('prod_product_ori_order_request_item_1_name','');
	},
    /***************************** */
})