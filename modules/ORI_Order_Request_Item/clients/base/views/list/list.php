<?php
$module_name = 'ORI_Order_Request_Item';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'prod_product_ori_order_request_item_1_name',
                'label' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_PROD_PRODUCT_TITLE',
                'enabled' => true,
                'id' => 'PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1PROD_PRODUCT_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'owner',
                'label' => 'LBL_OWNER',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              5 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'subtype',
                'label' => 'LBL_SUBTYPE',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'request_date',
                'label' => 'LBL_REQUEST_DATE',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'required_by_date',
                'label' => 'LBL_REQUIRED_BY_DATE',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'po_purchase_order_ori_order_request_item_1_name',
                'label' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE',
                'enabled' => true,
                'id' => 'PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1PO_PURCHASE_ORDER_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
