<?php
// created: 2022-03-31 11:42:59
$viewdefs['ORI_Order_Request_Item']['base']['view']['subpanel-for-or_order_request-or_order_request_ori_order_request_item_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'purchase_unit_2_c',
          'label' => 'LBL_PURCHASE_UNIT_2',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'po_purchase_order_ori_order_request_item_1_name',
          'label' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE',
          'enabled' => true,
          'id' => 'PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1PO_PURCHASE_ORDER_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'prod_product_ori_order_request_item_1_name',
          'label' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_PROD_PRODUCT_TITLE',
          'enabled' => true,
          'id' => 'PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1PROD_PRODUCT_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'in_system_product_id_c',
          'label' => 'LBL_IN_SYSTEM_PRODUCT_ID_C',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'product_name',
          'label' => 'LBL_PRODUCT_NAME',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'product_id_2',
          'label' => 'LBL_PRODUCT_ID_2',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'unit_quantity_requested',
          'label' => 'LBL_UNIT_QUANTITY_REQUESTED',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'request_date',
          'label' => 'LBL_REQUEST_DATE',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'required_by_date',
          'label' => 'LBL_REQUIRED_BY_DATE',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'estimated_arrival_date',
          'label' => 'LBL_ESTIMATED_ARRIVAL_DATE',
          'enabled' => true,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'notes_c',
          'label' => 'LBL_NOTES_C',
          'enabled' => true,
          'readonly' => false,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);