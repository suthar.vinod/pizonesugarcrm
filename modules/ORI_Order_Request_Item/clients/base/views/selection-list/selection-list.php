<?php
$module_name = 'ORI_Order_Request_Item';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'prod_product_ori_order_request_item_1_name',
                'label' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE',
                'enabled' => true,
                'id' => 'PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1PROD_PRODUCT_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              1 => 
              array (
                'name' => 'product_name',
                'label' => 'LBL_PRODUCT_NAME',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'vendor_name',
                'label' => 'LBL_VENDOR_NAME',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'product_id_2',
                'label' => 'LBL_PRODUCT_ID_2',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'unit_quantity_requested',
                'label' => 'LBL_UNIT_QUANTITY_REQUESTED',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'quality_requirement',
                'label' => 'LBL_QUALITY_REQUIREMENT',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'department_new_c',
                'label' => 'LBL_DEPARTMENT_NEW',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              8 => 
              array (
                'name' => 'or_request_by_c',
                'label' => 'LBL_OR_REQUEST_BY_C',
                'enabled' => true,
                'readonly' => false,
                'id' => 'USER_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'po_purchase_order_ori_order_request_item_1_name',
                'label' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE',
                'enabled' => true,
                'id' => 'PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1PO_PURCHASE_ORDER_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              11 => 
              array (
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'default' => false,
                'name' => 'date_modified',
                'readonly' => true,
              ),
              12 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
