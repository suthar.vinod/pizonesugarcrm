<?php
$popupMeta = array (
    'moduleMain' => 'ORI_Order_Request_Item',
    'varName' => 'ORI_Order_Request_Item',
    'orderBy' => 'ori_order_request_item.name',
    'whereClauses' => array (
  'name' => 'ori_order_request_item.name',
),
    'searchInputs' => array (
  0 => 'ori_order_request_item_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_PROD_PRODUCT_TITLE',
    'id' => 'PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1PROD_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'name' => 'prod_product_ori_order_request_item_1_name',
  ),
  'PRODUCT_NAME' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_PRODUCT_NAME',
    'width' => 10,
    'name' => 'product_name',
  ),
  'VENDOR_NAME' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_VENDOR_NAME',
    'width' => 10,
    'name' => 'vendor_name',
  ),
  'PRODUCT_ID_2' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_PRODUCT_ID_2',
    'width' => 10,
    'name' => 'product_id_2',
  ),
  'UNIT_QUANTITY_REQUESTED' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_UNIT_QUANTITY_REQUESTED',
    'width' => 10,
    'name' => 'unit_quantity_requested',
  ),
  'QUALITY_REQUIREMENT' => 
  array (
    'type' => 'multienum',
    'default' => true,
    'label' => 'LBL_QUALITY_REQUIREMENT',
    'width' => 10,
    'name' => 'quality_requirement',
  ),
  'DEPARTMENT_NEW_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_DEPARTMENT_NEW',
    'width' => 10,
    'name' => 'department_new_c',
  ),
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'OR_REQUEST_BY_C' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_OR_REQUEST_BY_C',
    'id' => 'USER_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'name' => 'or_request_by_c',
  ),
  'PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE',
    'id' => 'PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1PO_PURCHASE_ORDER_IDA',
    'width' => 10,
    'default' => true,
    'name' => 'po_purchase_order_ori_order_request_item_1_name',
  ),
),
);
