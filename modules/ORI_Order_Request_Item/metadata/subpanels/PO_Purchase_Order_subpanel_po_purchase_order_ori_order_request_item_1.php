<?php
// created: 2022-03-31 11:44:43
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STATUS',
    'width' => 10,
  ),
  'prod_product_ori_order_request_item_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_PROD_PRODUCT_TITLE',
    'id' => 'PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1PROD_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Prod_Product',
    'target_record_key' => 'prod_product_ori_order_request_item_1prod_product_ida',
  ),
  'products_department_c' => 
  array (
    'readonly' => '1',
    'readonly_formula' => '',
    'type' => 'varchar',
    'vname' => 'LBL_PRODUCTS_DEPARTMENT',
    'width' => 10,
    'default' => true,
  ),
  'in_system_product_id_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_IN_SYSTEM_PRODUCT_ID_C',
    'width' => 10,
    'default' => true,
  ),
  'product_name' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_PRODUCT_NAME',
    'width' => 10,
  ),
  'product_id_2' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_PRODUCT_ID_2',
    'width' => 10,
  ),
  'unit_quantity_requested' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_UNIT_QUANTITY_REQUESTED',
    'width' => 10,
  ),
  'cost_per_unit' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_COST_PER_UNIT',
    'currency_format' => true,
    'width' => 10,
  ),
  'request_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_REQUEST_DATE',
    'width' => 10,
    'default' => true,
  ),
  'required_by_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_REQUIRED_BY_DATE',
    'width' => 10,
    'default' => true,
  ),
  'estimated_arrival_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_ESTIMATED_ARRIVAL_DATE',
    'width' => 10,
    'default' => true,
  ),
  'm01_sales_ori_order_request_item_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_M01_SALES_TITLE',
    'id' => 'M01_SALES_ORI_ORDER_REQUEST_ITEM_1M01_SALES_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M01_Sales',
    'target_record_key' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
  ),
  'm03_work_product_ori_order_request_item_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
    'id' => 'M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1M03_WORK_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M03_Work_Product',
    'target_record_key' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
  ),
  'or_request_by_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_OR_REQUEST_BY_C',
    'id' => 'USER_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'user_id_c',
  ),
  'notes_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_NOTES_C',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
);