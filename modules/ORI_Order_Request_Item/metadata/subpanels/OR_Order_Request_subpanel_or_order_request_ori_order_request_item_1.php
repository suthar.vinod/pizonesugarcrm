<?php
// created: 2022-03-31 11:42:57
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STATUS',
    'width' => 10,
  ),
  'purchase_unit_2_c' => 
  array (
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_PURCHASE_UNIT_2',
    'width' => 10,
    'default' => true,
  ),
  'po_purchase_order_ori_order_request_item_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE',
    'id' => 'PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1PO_PURCHASE_ORDER_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'PO_Purchase_Order',
    'target_record_key' => 'po_purchase_order_ori_order_request_item_1po_purchase_order_ida',
  ),
  'prod_product_ori_order_request_item_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_PROD_PRODUCT_TITLE',
    'id' => 'PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1PROD_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Prod_Product',
    'target_record_key' => 'prod_product_ori_order_request_item_1prod_product_ida',
  ),
  'in_system_product_id_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_IN_SYSTEM_PRODUCT_ID_C',
    'width' => 10,
    'default' => true,
  ),
  'product_name' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_PRODUCT_NAME',
    'width' => 10,
  ),
  'product_id_2' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_PRODUCT_ID_2',
    'width' => 10,
  ),
  'unit_quantity_requested' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_UNIT_QUANTITY_REQUESTED',
    'width' => 10,
  ),
  'request_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_REQUEST_DATE',
    'width' => 10,
    'default' => true,
  ),
  'required_by_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_REQUIRED_BY_DATE',
    'width' => 10,
    'default' => true,
  ),
  'estimated_arrival_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_ESTIMATED_ARRIVAL_DATE',
    'width' => 10,
    'default' => true,
  ),
  'notes_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_NOTES_C',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
);