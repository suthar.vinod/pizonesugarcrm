<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class UpdateORIForORIHook
{

    function UpdateORIStatusOnQuanitityChange($bean, $event, $arguments)
    {
        global $db;
        $unit_quantity_requested = $bean->unit_quantity_requested;        
        $bean->load_relationship('ori_order_request_item_ri_received_items_1');
        $relatedRI = $bean->ori_order_request_item_ri_received_items_1->get();
        $relatedPO = $bean->po_purchase_order_ori_order_request_item_1->get();
        if ($bean->unit_quantity_requested != $bean->fetched_row['unit_quantity_requested'] || $bean->request_date != $bean->fetched_row['request_date']) {
            //$order_request_item = BeanFactory::retrieveBean('ORI_Order_Request_Item', $bean->id);        
            
            $relatedRIIds = $relatedRI;
            $total = 0;
            for ($a = 0; $a < count($relatedRIIds); $a++) {
                $RI_Received_Items = BeanFactory::retrieveBean('RI_Received_Items', $relatedRIIds[$a]);
                $quantity_received = $RI_Received_Items->quantity_received;
                $total = $total + $quantity_received;
            }
            $request_date = $bean->request_date;

            if ($request_date != ""  && count($relatedPO) > 0 && count($relatedRIIds) <= 0) { 
                $bean->status = "Ordered";
            } else if ($request_date != ""  && count($relatedPO) <= 0 && count($relatedRIIds) <= 0) { 
                $bean->status = "Requested";
            } else  if (count($relatedRIIds) > 0 && $unit_quantity_requested <= $total) {  
                $bean->status = "Inventory";
            } else if (count($relatedRIIds) > 0 && $unit_quantity_requested != "") {   
                $bean->status = "Partially Received";
            }
        }
    }
}
