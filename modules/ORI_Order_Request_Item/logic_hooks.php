<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
$hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['before_save'] = Array(); 
$hook_array['before_save'][] = Array('1','workflow','include/workflow/WorkFlowHandler.php','WorkFlowHandler','WorkFlowHandler',);

$hook_array['before_save'][] = Array(
    '17',
    'After relating a ORI record resave the bean so the status field is calculated',
    'custom/modules/ORI_Order_Request_Item/SetORIStatusNew.php',
    'SetStatusForORIHook',
    'setORIEstimatearrivalDate',
);
$hook_array['process_record'][] = array(
      '1',
      'delete duplicate Status Audit log Record',
      'custom/modules/ORI_Order_Request_Item/deleteStatusAuditLog.php',
      'deleteStatusAuditLog',
      'deleteStatusAudit'
   );
   $hook_array['after_save'][] = Array(
    '9',
    'After relating a ORI record resave the bean so the status field is calculated',
    'custom/modules/ORI_Order_Request_Item/SetORStatus.php',
    'SetORStatusHook1',
    'setORStatusAfterORISave',
);
/**24 June 2022 : Bug #2601 : ORI Incorrect status */
$hook_array['before_save'][] = Array(
    '18',
    'ORI Status update on edit quantity from ori record',
    'custom/modules/ORI_Order_Request_Item/UpdateORIStatus.php',
    'UpdateORIForORIHook',
    'UpdateORIStatusOnQuanitityChange',
);

?>