<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class deleteStatusAuditLog {

    function deleteStatusAudit($bean, $event, $arguments) {
        global $db;
        $oriID = $bean->id;
        $queryAuditLog = "SELECT id,count(*) as NUM FROM `ori_order_request_item_audit` WHERE `field_name`='status' AND `parent_id`='" . $oriID . "' group by before_value_string,after_value_string,date(date_created)";
        $auditLogResult = $db->query($queryAuditLog);

        if ($auditLogResult->num_rows > 0) {
            while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {

                if ($fetchAuditLog['NUM'] > 1) {
                    $recordID = $fetchAuditLog['id'];					
                    $sql_DeleteAudit = "DELETE from `ori_order_request_item_audit` WHERE `field_name`='status' AND `parent_id`='" . $oriID . "' AND id = '" . $recordID . "'";
                    $db->query($sql_DeleteAudit);
                }
            }
        }
        
        /*Manage duplicate value in ORI auditlog */
        $queryAuditLog = "SELECT id,count(*) as NUM FROM `ori_order_request_item_audit` WHERE `field_name`='Order Requests' AND `parent_id`='".$oriID."' AND (before_value_string='' OR before_value_string is null)  group by before_value_string,after_value_string,date(date_created)";
        $auditLogResult = $db->query($queryAuditLog);

        if($auditLogResult->num_rows > 0 ){
            while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
                if($fetchAuditLog['NUM']>1){
                    $recordID = $fetchAuditLog['id'];
                    $sql_DeleteAudit = "DELETE from `ori_order_request_item_audit` WHERE `field_name`='Order Requests' AND `parent_id`='".$oriID."' AND id = '".$recordID."'";
                    $db->query($sql_DeleteAudit);
                }				
            }
        } 

    }

}

?>