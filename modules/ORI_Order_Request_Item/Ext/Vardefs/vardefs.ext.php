<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_m01_sales_ori_order_request_item_1m01_sales_ida.php

 // created: 2021-10-19 11:34:56
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['name']='m01_sales_ori_order_request_item_1m01_sales_ida';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['type']='id';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['source']='non-db';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['vname']='LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['id_name']='m01_sales_ori_order_request_item_1m01_sales_ida';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['link']='m01_sales_ori_order_request_item_1';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['table']='m01_sales';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['module']='M01_Sales';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['rname']='id';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['side']='right';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['massupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['duplicate_merge']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['hideacl']=true;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1m01_sales_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_m01_sales_ori_order_request_item_1_name.php

 // created: 2021-10-19 11:34:57
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1_name']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1_name']['massupdate']=true;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1_name']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1_name']['duplicate_merge']='enabled';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1_name']['duplicate_merge_dom_value']='1';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1_name']['merge_filter']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1_name']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1_name']['unified_search']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1_name']['calculated']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m01_sales_ori_order_request_item_1_name']['vname']='LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_m03_work_product_ori_order_request_item_1m03_work_product_ida.php

 // created: 2021-10-19 11:35:30
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['name']='m03_work_product_ori_order_request_item_1m03_work_product_ida';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['type']='id';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['source']='non-db';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['vname']='LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['id_name']='m03_work_product_ori_order_request_item_1m03_work_product_ida';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['link']='m03_work_product_ori_order_request_item_1';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['table']='m03_work_product';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['module']='M03_Work_Product';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['rname']='id';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['side']='right';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['massupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['duplicate_merge']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['hideacl']=true;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1m03_work_product_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_m03_work_product_ori_order_request_item_1_name.php

 // created: 2021-10-19 11:35:33
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1_name']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1_name']['massupdate']=true;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1_name']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1_name']['duplicate_merge']='enabled';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1_name']['duplicate_merge_dom_value']='1';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1_name']['merge_filter']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1_name']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1_name']['unified_search']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1_name']['calculated']=false;
$dictionary['ORI_Order_Request_Item']['fields']['m03_work_product_ori_order_request_item_1_name']['vname']='LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_or_order_request_ori_order_request_item_1or_order_request_ida.php

 // created: 2021-10-19 11:36:20
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['name']='or_order_request_ori_order_request_item_1or_order_request_ida';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['type']='id';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['source']='non-db';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['vname']='LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['id_name']='or_order_request_ori_order_request_item_1or_order_request_ida';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['link']='or_order_request_ori_order_request_item_1';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['table']='or_order_request';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['module']='OR_Order_Request';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['rname']='id';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['side']='right';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['massupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['duplicate_merge']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['hideacl']=true;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1or_order_request_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_or_order_request_ori_order_request_item_1_name.php

 // created: 2021-10-19 11:36:22
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1_name']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1_name']['massupdate']=true;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1_name']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1_name']['duplicate_merge']='enabled';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1_name']['duplicate_merge_dom_value']='1';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1_name']['merge_filter']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1_name']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1_name']['unified_search']=false;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1_name']['calculated']=false;
$dictionary['ORI_Order_Request_Item']['fields']['or_order_request_ori_order_request_item_1_name']['vname']='LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_prod_product_ori_order_request_item_1prod_product_ida.php

 // created: 2021-10-19 11:37:05
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['name']='prod_product_ori_order_request_item_1prod_product_ida';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['type']='id';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['source']='non-db';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['vname']='LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['id_name']='prod_product_ori_order_request_item_1prod_product_ida';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['link']='prod_product_ori_order_request_item_1';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['table']='prod_product';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['module']='Prod_Product';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['rname']='id';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['side']='right';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['massupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['duplicate_merge']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['hideacl']=true;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1prod_product_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_prod_product_ori_order_request_item_1_name.php

 // created: 2021-10-19 11:37:06
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['audited']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['massupdate']=true;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['duplicate_merge']='enabled';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['duplicate_merge_dom_value']='1';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['merge_filter']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['reportable']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['unified_search']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['calculated']=false;
$dictionary['ORI_Order_Request_Item']['fields']['prod_product_ori_order_request_item_1_name']['vname']='LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/ori_order_request_item_ri_received_items_1_ORI_Order_Request_Item.php

// created: 2021-08-04 19:53:25
$dictionary["ORI_Order_Request_Item"]["fields"]["ori_order_request_item_ri_received_items_1"] = array (
  'name' => 'ori_order_request_item_ri_received_items_1',
  'type' => 'link',
  'relationship' => 'ori_order_request_item_ri_received_items_1',
  'source' => 'non-db',
  'module' => 'RI_Received_Items',
  'bean_name' => 'RI_Received_Items',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'ori_order_6b32st_item_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/or_order_request_ori_order_request_item_1_ORI_Order_Request_Item.php

// created: 2021-03-03 11:30:08
$dictionary["ORI_Order_Request_Item"]["fields"]["or_order_request_ori_order_request_item_1"] = array (
  'name' => 'or_order_request_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'or_order_request_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'OR_Order_Request',
  'bean_name' => 'OR_Order_Request',
  'side' => 'right',
  'vname' => 'LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'or_order_request_ori_order_request_item_1or_order_request_ida',
  'link-type' => 'one',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["or_order_request_ori_order_request_item_1_name"] = array (
  'name' => 'or_order_request_ori_order_request_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_OR_ORDER_REQUEST_TITLE',
  'save' => true,
  'id_name' => 'or_order_request_ori_order_request_item_1or_order_request_ida',
  'link' => 'or_order_request_ori_order_request_item_1',
  'table' => 'or_order_request',
  'module' => 'OR_Order_Request',
  'rname' => 'name',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["or_order_request_ori_order_request_item_1or_order_request_ida"] = array (
  'name' => 'or_order_request_ori_order_request_item_1or_order_request_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID',
  'id_name' => 'or_order_request_ori_order_request_item_1or_order_request_ida',
  'link' => 'or_order_request_ori_order_request_item_1',
  'table' => 'or_order_request',
  'module' => 'OR_Order_Request',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/po_purchase_order_ori_order_request_item_1_ORI_Order_Request_Item.php

// created: 2021-02-25 12:36:42
$dictionary["ORI_Order_Request_Item"]["fields"]["po_purchase_order_ori_order_request_item_1"] = array (
  'name' => 'po_purchase_order_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'po_purchase_order_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'PO_Purchase_Order',
  'bean_name' => 'PO_Purchase_Order',
  'side' => 'right',
  'vname' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'po_purchase_order_ori_order_request_item_1po_purchase_order_ida',
  'link-type' => 'one',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["po_purchase_order_ori_order_request_item_1_name"] = array (
  'name' => 'po_purchase_order_ori_order_request_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE',
  'save' => true,
  'id_name' => 'po_purchase_order_ori_order_request_item_1po_purchase_order_ida',
  'link' => 'po_purchase_order_ori_order_request_item_1',
  'table' => 'po_purchase_order',
  'module' => 'PO_Purchase_Order',
  'rname' => 'name',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["po_purchase_order_ori_order_request_item_1po_purchase_order_ida"] = array (
  'name' => 'po_purchase_order_ori_order_request_item_1po_purchase_order_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID',
  'id_name' => 'po_purchase_order_ori_order_request_item_1po_purchase_order_ida',
  'link' => 'po_purchase_order_ori_order_request_item_1',
  'table' => 'po_purchase_order',
  'module' => 'PO_Purchase_Order',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/prod_product_ori_order_request_item_1_ORI_Order_Request_Item.php

// created: 2020-07-31 12:47:27
$dictionary["ORI_Order_Request_Item"]["fields"]["prod_product_ori_order_request_item_1"] = array (
  'name' => 'prod_product_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'prod_product_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'Prod_Product',
  'bean_name' => 'Prod_Product',
  'side' => 'right',
  'vname' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'prod_product_ori_order_request_item_1prod_product_ida',
  'link-type' => 'one',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["prod_product_ori_order_request_item_1_name"] = array (
  'name' => 'prod_product_ori_order_request_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_PROD_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'prod_product_ori_order_request_item_1prod_product_ida',
  'link' => 'prod_product_ori_order_request_item_1',
  'table' => 'prod_product',
  'module' => 'Prod_Product',
  'rname' => 'name',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["prod_product_ori_order_request_item_1prod_product_ida"] = array (
  'name' => 'prod_product_ori_order_request_item_1prod_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID',
  'id_name' => 'prod_product_ori_order_request_item_1prod_product_ida',
  'link' => 'prod_product_ori_order_request_item_1',
  'table' => 'prod_product',
  'module' => 'Prod_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_client_cost_c.php

 // created: 2021-02-16 19:18:10
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['labelValue']='Client Cost';
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['calculated']='1';
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['formula']='related($prod_product_ori_order_request_item_1,"client_cost_c")';
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['enforced']='1';
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['dependency']='and(equal($owner,"APS Supplied Client Owned"),not(isInList($product_selection,createList("Not In System Vendor Non Specific","Not In System Vendor Specific"))))';
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['ORI_Order_Request_Item']['fields']['client_cost_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_department_new_c.php

 // created: 2021-02-15 08:32:01
$dictionary['ORI_Order_Request_Item']['fields']['department_new_c']['labelValue']='Department';
$dictionary['ORI_Order_Request_Item']['fields']['department_new_c']['dependency']='';
$dictionary['ORI_Order_Request_Item']['fields']['department_new_c']['required_formula']='';
$dictionary['ORI_Order_Request_Item']['fields']['department_new_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_purchase_unit_2_c.php

 // created: 2021-02-26 12:07:46
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['labelValue']='Purchase Unit';
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['calculated']='1';
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['formula']='getDropdownValue("purchase_unit_list",related($prod_product_ori_order_request_item_1,"purchase_unit"))';
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['enforced']='1';
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['dependency']='not(isInList($product_selection,createList("Not In System Vendor Non Specific","Not In System Vendor Specific")))';
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit_2_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_unit_quantity_c.php

 // created: 2021-02-16 18:39:35
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['labelValue']='Unit Quantity';
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['calculated']='true';
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['formula']='related($prod_product_ori_order_request_item_1,"unit_quantity")';
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['enforced']='true';
$dictionary['ORI_Order_Request_Item']['fields']['unit_quantity_c']['dependency']='not(isInList($product_selection,createList("Not In System Vendor Non Specific","Not In System Vendor Specific")))';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_orderrequesttotal_c.php

 // created: 2021-08-10 21:20:15
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['labelValue']='orderrequesttotal';
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['calculated']='true';
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['formula']='multiply($cost_per_unit,$unit_quantity_requested)';
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['enforced']='true';
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['dependency']='';
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['required_formula']='';
$dictionary['ORI_Order_Request_Item']['fields']['orderrequesttotal_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/m01_sales_ori_order_request_item_1_ORI_Order_Request_Item.php

// created: 2020-07-31 12:44:16
$dictionary["ORI_Order_Request_Item"]["fields"]["m01_sales_ori_order_request_item_1"] = array (
  'name' => 'm01_sales_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'm01_sales_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["m01_sales_ori_order_request_item_1_name"] = array (
  'name' => 'm01_sales_ori_order_request_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
  'link' => 'm01_sales_ori_order_request_item_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["m01_sales_ori_order_request_item_1m01_sales_ida"] = array (
  'name' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID',
  'id_name' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
  'link' => 'm01_sales_ori_order_request_item_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/m03_work_product_ori_order_request_item_1_ORI_Order_Request_Item.php

// created: 2020-07-31 12:46:07
$dictionary["ORI_Order_Request_Item"]["fields"]["m03_work_product_ori_order_request_item_1"] = array (
  'name' => 'm03_work_product_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["m03_work_product_ori_order_request_item_1_name"] = array (
  'name' => 'm03_work_product_ori_order_request_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
  'link' => 'm03_work_product_ori_order_request_item_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["ORI_Order_Request_Item"]["fields"]["m03_work_product_ori_order_request_item_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE_ID',
  'id_name' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
  'link' => 'm03_work_product_ori_order_request_item_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_purchase_unit.php

 // created: 2021-02-16 19:22:15
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['purchase_unit']['dependency']='not(isInList($product_selection,createList("Not In System Vendor Non Specific","Not In System Vendor Specific")))';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_received_date.php

 // created: 2020-12-08 10:22:35
$dictionary['ORI_Order_Request_Item']['fields']['received_date']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['received_date']['readonly']=true;
$dictionary['ORI_Order_Request_Item']['fields']['received_date']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_related_to.php

 // created: 2021-02-16 18:31:50
$dictionary['ORI_Order_Request_Item']['fields']['related_to']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_cost_per_unit.php

 // created: 2021-07-30 04:58:51
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['required']=true;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['name']='cost_per_unit';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['vname']='LBL_COST_PER_UNIT';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['type']='currency';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['massupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['no_default']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['comments']='';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['help']='';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['importable']='true';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['duplicate_merge']='enabled';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['duplicate_merge_dom_value']='1';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['audited']=true;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['reportable']=true;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['unified_search']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['merge_filter']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['pii']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['default']='';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['calculated']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['dependency']='not(isInList($product_selection,createList("Not In System Vendor Non Specific","Not In System Vendor Specific")))';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['len']=26;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['size']='20';
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['enable_range_search']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['precision']=6;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['convertToBase']=true;
$dictionary['ORI_Order_Request_Item']['fields']['cost_per_unit']['showTransactionalAmount']=true;

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_name.php

 // created: 2021-08-12 11:22:24
$dictionary['ORI_Order_Request_Item']['fields']['name']['required']=false;
$dictionary['ORI_Order_Request_Item']['fields']['name']['unified_search']=false;
$dictionary['ORI_Order_Request_Item']['fields']['name']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['name']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_status.php

 // created: 2021-04-05 06:25:28
$dictionary['ORI_Order_Request_Item']['fields']['status']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_request_date.php

 // created: 2020-07-31 13:04:48
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['massupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['importable']='false';
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['duplicate_merge']='disabled';
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['calculated']='true';
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['formula']='related($or_order_request_ori_order_request_item_1,"request_date")';
$dictionary['ORI_Order_Request_Item']['fields']['request_date']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/ori_order_request_item_ii_inventory_item_1_ORI_Order_Request_Item.php

// created: 2021-11-09 09:46:03
$dictionary["ORI_Order_Request_Item"]["fields"]["ori_order_request_item_ii_inventory_item_1"] = array (
  'name' => 'ori_order_request_item_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'ori_order_request_item_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'id_name' => 'ori_order_2123st_item_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_in_system_product_id_c.php

 // created: 2021-12-02 06:29:40
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['labelValue']='In System Product ID';
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['calculated']='true';
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['formula']='related($prod_product_ori_order_request_item_1,"product_id_2")';
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['enforced']='true';
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['dependency']='';
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['required_formula']='';
$dictionary['ORI_Order_Request_Item']['fields']['in_system_product_id_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_products_department_c.php

 // created: 2021-12-14 06:04:40
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['labelValue']='Product\'s Department(s)';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['calculated']='true';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['formula']='related($prod_product_ori_order_request_item_1,"department")';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['enforced']='true';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['dependency']='';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['required_formula']='';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['readonly']='1';
$dictionary['ORI_Order_Request_Item']['fields']['products_department_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_products_name_c.php

 // created: 2022-02-22 08:11:36
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['duplicate_merge_dom_value']=0;
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['labelValue']='Product\'s Name';
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['calculated']='true';
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['formula']='concat($product_name,related($prod_product_ori_order_request_item_1,"name"))';
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['enforced']='true';
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['dependency']='';
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['required_formula']='';
$dictionary['ORI_Order_Request_Item']['fields']['products_name_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_user_id_c.php

 // created: 2022-02-24 07:17:10

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_or_request_by_c.php

 // created: 2022-02-24 07:17:10
$dictionary['ORI_Order_Request_Item']['fields']['or_request_by_c']['labelValue']='Request By';
$dictionary['ORI_Order_Request_Item']['fields']['or_request_by_c']['dependency']='';
$dictionary['ORI_Order_Request_Item']['fields']['or_request_by_c']['required_formula']='';
$dictionary['ORI_Order_Request_Item']['fields']['or_request_by_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_a_subtype.php

 // created: 2021-11-09 07:23:36
 //$dictionary['ORI_Order_Request_Item']['fields']['subtype']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_notes_c.php

 // created: 2022-03-31 11:40:06
$dictionary['ORI_Order_Request_Item']['fields']['notes_c']['labelValue']='Notes';
$dictionary['ORI_Order_Request_Item']['fields']['notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ORI_Order_Request_Item']['fields']['notes_c']['enforced']='';
$dictionary['ORI_Order_Request_Item']['fields']['notes_c']['dependency']='';
$dictionary['ORI_Order_Request_Item']['fields']['notes_c']['required_formula']='';
$dictionary['ORI_Order_Request_Item']['fields']['notes_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Vardefs/sugarfield_subtype.php

 // created: 2022-03-23 12:35:46
$dictionary['ORI_Order_Request_Item']['fields']['subtype']['hidemassupdate']=false;
$dictionary['ORI_Order_Request_Item']['fields']['subtype']['visibility_grid']=array (
  'trigger' => 'type_2',
  'values' => 
  array (
    'Balloon Catheter' => 
    array (
      0 => '',
      1 => 'Angioplasty Balloon',
      2 => 'Cutting Balloon',
      3 => 'Drug Coated Balloon',
      4 => 'Other',
      5 => 'OTW over the wire',
      6 => 'POBA Balloon',
      7 => 'PTA Balloon',
    ),
    'Cannula' => 
    array (
      0 => '',
      1 => 'Aortic Root Cannula',
      2 => 'Arterial Cannula',
      3 => 'Other',
      4 => 'Pediatric Cannula',
      5 => 'Venous Cannula',
      6 => 'Vessel Cannula',
    ),
    'Catheter' => 
    array (
      0 => '',
      1 => 'Access Catheter',
      2 => 'Butterfly Catheter',
      3 => 'Central Venous Catheter',
      4 => 'Diagnostic Catheter',
      5 => 'EP Catheter',
      6 => 'Extension Catheter',
      7 => 'Foley Catheter',
      8 => 'Guide Catheter',
      9 => 'Imaging Catheter',
      10 => 'IV Catheter',
      11 => 'Mapping Catheter',
      12 => 'Micro Catheter',
      13 => 'Mila Catheter',
      14 => 'Other',
      15 => 'Pressure Catheter',
      16 => 'Support Catheter',
      17 => 'Transseptal Catheter',
    ),
    'Chemical' => 
    array (
      0 => '',
      1 => 'Analytes',
      2 => 'Control Matrix',
      3 => 'Corrosive Solution',
      4 => 'Flammable',
      5 => 'Flammable Solution',
      6 => 'Non Flammable',
      7 => 'Solvents Aqueous',
      8 => 'Solvents Organic',
      9 => 'Toxic Solution',
    ),
    'Cleaning Agent' => 
    array (
    ),
    'Contrast' => 
    array (
      0 => '',
      1 => 'Barium Contrast',
      2 => 'Iodinated Contrast',
      3 => 'Other',
    ),
    'Drug' => 
    array (
      0 => '',
      1 => 'Inhalant',
      2 => 'Injectable',
      3 => 'Intranasal',
      4 => 'IV Solution',
      5 => 'Oral',
      6 => 'Other',
      7 => 'Topical',
    ),
    'Graft' => 
    array (
      0 => '',
      1 => 'Coated',
      2 => 'ePTFE',
      3 => 'Knitted',
      4 => 'Other',
      5 => 'Patches',
      6 => 'Woven',
    ),
    'Inflation Device' => 
    array (
      0 => '',
      1 => 'High Pressure',
      2 => 'Other',
    ),
    'Reagent' => 
    array (
      0 => '',
      1 => 'Flammable',
      2 => 'Non Flammable',
    ),
    'Sheath' => 
    array (
      0 => '',
      1 => 'Introducer Sheath',
      2 => 'Other',
      3 => 'Peel Away',
      4 => 'Steerable Sheath',
    ),
    'Stent' => 
    array (
      0 => '',
      1 => 'Bare Metal Stent',
      2 => 'Drug Coated Stent',
      3 => 'Other',
    ),
    'Suture' => 
    array (
      0 => '',
      1 => 'Ethibond',
      2 => 'Ethilon',
      3 => 'MonoAdjustable Loop',
      4 => 'Monocryl',
      5 => 'Monoderm',
      6 => 'Other',
      7 => 'PDO',
      8 => 'PDS',
      9 => 'Pledget',
      10 => 'Prolene',
      11 => 'Silk',
      12 => 'Ti Cron',
      13 => 'Vicryl',
    ),
    'Wire' => 
    array (
      0 => '',
      1 => 'Exchange Wire',
      2 => 'Guide Wire',
      3 => 'Marking Wire',
      4 => 'Other',
      5 => 'Support Wire',
    ),
    '' => 
    array (
    ),
    'Autoclave supplies' => 
    array (
    ),
    'Bandaging' => 
    array (
      0 => '',
      1 => 'Dressing',
      2 => 'Sub layer wrap',
      3 => 'Tape',
      4 => 'Top layer wrap',
      5 => 'Other',
    ),
    'Blood draw supply' => 
    array (
      0 => '',
      1 => 'Cryovial',
      2 => 'Extension line',
      3 => 'Injection port',
      4 => 'Needle',
      5 => 'Needless port',
      6 => 'Other',
      7 => 'Syringe',
      8 => 'Vacutainer',
    ),
    'Cleaning Supplies' => 
    array (
      0 => '',
      1 => 'Bottle',
      2 => 'Brush',
      3 => 'Floor scrubber supplies',
      4 => 'Other',
      5 => 'SpongePad',
      6 => 'Trash bag',
    ),
    'Endotracheal TubeSupplies' => 
    array (
      0 => '',
      1 => 'Adaptor',
      2 => 'Breathing circuit',
      3 => 'Brush',
      4 => 'ET Tube',
      5 => 'Other',
      6 => 'Stylet',
    ),
    'Enrichment Toy' => 
    array (
    ),
    'Equipment' => 
    array (
      0 => '',
      1 => 'GCMS Columns',
      2 => 'GCMS Parts',
      3 => 'ICPMS Parts',
      4 => 'LCMS Columns',
      5 => 'LCMS Parts',
      6 => 'Machine',
      7 => 'Other',
      8 => 'Replacement Part',
      9 => 'Software',
    ),
    'ETO supplies' => 
    array (
    ),
    'Feeding supplies' => 
    array (
      0 => '',
      1 => 'Enrichment treats',
      2 => 'Feeding Tube',
      3 => 'Other',
      4 => 'Primary diet',
      5 => 'Supplement diet',
    ),
    'Office Supply' => 
    array (
      0 => '',
      1 => 'Binders and supplies',
      2 => 'Label',
      3 => 'Laminating Sheets',
      4 => 'Other',
      5 => 'Paper',
    ),
    'Other' => 
    array (
      0 => '',
      1 => 'Contrast Spike',
      2 => 'Dressing',
      3 => 'Essentials Kit',
      4 => 'Invasive Blood Pressure Supplies',
      5 => 'Other',
      6 => 'Perc Needle',
      7 => 'Probe Cover',
      8 => 'Stopcock',
      9 => 'Torque Device',
      10 => 'Tuohy',
    ),
    'Shipping supplies' => 
    array (
      0 => '',
      1 => 'CD case',
      2 => 'Container',
      3 => 'Cold Pack',
      4 => 'Corrugated box',
      5 => 'Dry Ice',
      6 => 'Indicator label',
      7 => 'Insulated shipper',
      8 => 'Other',
      9 => 'Padding',
      10 => 'Pallet',
      11 => 'Tape',
      12 => 'Wrap',
    ),
    'Solution' => 
    array (
    ),
    'Surgical Instrument' => 
    array (
    ),
    'Surgical site prep' => 
    array (
      0 => '',
      1 => 'Incision site cover',
      2 => 'Other',
      3 => 'Surgical scrub',
    ),
    'Tubing' => 
    array (
      0 => '',
      1 => 'Adaptor',
      2 => 'Connector',
      3 => 'Extension Tubing',
      4 => 'High Pressure Tubing',
      5 => 'Other',
      6 => 'Reducer',
      7 => 'WYE',
    ),
    'Bypass supply' => 
    array (
      0 => '',
      1 => 'Hemoconcentrator',
      2 => 'Other',
      3 => 'Oxygenator',
      4 => 'Pump pack',
      5 => 'Reservoir',
      6 => 'Tubing',
    ),
    'PPE supply' => 
    array (
      0 => '',
      1 => 'Boots',
      2 => 'Bouffanthair net',
      3 => 'Coveralloverall',
      4 => 'Ear protection',
      5 => 'Face shield',
      6 => 'Gloves non sterile',
      7 => 'Gloves sterile',
      8 => 'Mask',
      9 => 'Other',
      10 => 'Safety glasses',
      11 => 'Shoe covers',
      12 => 'Surgical gown non sterile',
      13 => 'Surgical gown sterile',
    ),
    'Control Matrix' => 
    array (
      0 => '',
      1 => 'Fluid',
      2 => 'Other',
      3 => 'Tissue',
    ),
    'Interventional Supplies' => 
    array (
      0 => '',
      1 => 'Other',
    ),
    'Surgical Supplies' => 
    array (
      0 => '',
      1 => 'Aortic Punch',
      2 => 'Biopsy Punch',
      3 => 'Blade',
      4 => 'Cautery Supply',
      5 => 'CT supply',
      6 => 'Drainage supply',
      7 => 'Drape',
      8 => 'ECG supply',
      9 => 'Gauze',
      10 => 'Hand Scrub',
      11 => 'Other',
      12 => 'Patient Drape',
      13 => 'SharpsBiohazard',
      14 => 'Sponge',
      15 => 'Table drape',
    ),
    'Glassware' => 
    array (
      0 => '',
      1 => 'Beaker',
      2 => 'Beakers',
      3 => 'Bottle',
      4 => 'Consumable',
      5 => 'Flask',
      6 => 'Grade A Glassware',
      7 => 'Other',
    ),
    'Plastic Ware' => 
    array (
      0 => '',
      1 => 'Beaker',
      2 => 'Bottle',
      3 => 'Consumable',
      4 => 'Flask',
      5 => 'Other',
      6 => 'Pipette Tips',
      7 => 'Tubes',
      8 => 'Vial Closures',
    ),
    'Lab Supplies' => 
    array (
      0 => '',
      1 => 'Misc',
      2 => 'Storage container',
      3 => 'Storage Racks',
    ),
  ),
);
$dictionary['ORI_Order_Request_Item']['fields']['subtype']['required']=false;

 
?>
