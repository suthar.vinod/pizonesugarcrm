<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Language/en_us.customori_order_request_item_ri_received_items_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE'] = 'Received Items';
$mod_strings['LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Received Items';

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Language/en_us.customor_order_request_ori_order_request_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_OR_ORDER_REQUEST_TITLE'] = 'Order Requests';
$mod_strings['LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Order Requests';

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Language/en_us.custompo_purchase_order_ori_order_request_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE'] = 'Purchase Order';
$mod_strings['LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Purchase Order';

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Language/en_us.customprod_product_ori_order_request_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_PROD_PRODUCT_TITLE'] = 'Products';
$mod_strings['LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Products';

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Language/en_us.customm01_sales_ori_order_request_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Language/en_us.customm03_work_product_ori_order_request_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Language/en_us.customori_order_request_item_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Inventory Items';

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Language/en_us.ri_mass_update.php.ORIModuleCustomizations.lang.php


$mod_strings['LBL_RI_MASS_CREATE_BUTTON'] = 'RI Mass Create';
$mod_strings['LBL_RI_MASS_CREATION'] = 'RIs Mass Creation';
$mod_strings['LBL_CREATE_RIS_BUTTON_LABEL'] = 'Create RIs';
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_CLIENT_COST'] = 'Client Cost';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_DEPARTMENT_NEW'] = 'Department';
$mod_strings['LBL_PURCHASE_UNIT_2'] = 'Purchase Unit';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_ORDERREQUESTTOTAL'] = 'orderrequesttotal';
$mod_strings['LBL_PURCHASE_UNIT'] = 'Purchase Unit (Obsolete)';
$mod_strings['LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE'] = 'Sales Activity';
$mod_strings['LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_M01_SALES_TITLE'] = 'Sales Activity';
$mod_strings['LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE'] = 'Work Product';
$mod_strings['LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Product';
$mod_strings['LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE'] = 'Order Request';
$mod_strings['LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_OR_ORDER_REQUEST_TITLE'] = 'Order Request';
$mod_strings['LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_NAME_FIELD_TITLE'] = 'Product';
$mod_strings['LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_PROD_PRODUCT_TITLE'] = 'Product';
$mod_strings['LBL_IN_SYSTEM_PRODUCT_ID_C'] = 'In System Product ID';
$mod_strings['LBL_PRODUCTS_DEPARTMENT'] = 'Product&#039;s Department(s)';
$mod_strings['LBL_PRODUCTS_NAME'] = 'Product&#039;s Name';
$mod_strings['LBL_OR_REQUEST_BY_C_USER_ID'] = 'Request By (related User ID)';
$mod_strings['LBL_OR_REQUEST_BY_C'] = 'Request By';
$mod_strings['LBL_NOTES_C'] = 'Notes';

?>
