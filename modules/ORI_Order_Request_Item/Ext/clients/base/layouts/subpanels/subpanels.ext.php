<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-11-09 09:46:03
$viewdefs['ORI_Order_Request_Item']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'ori_order_request_item_ii_inventory_item_1',
  ),
);

// created: 2021-08-04 19:53:25
$viewdefs['ORI_Order_Request_Item']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'context' => 
  array (
    'link' => 'ori_order_request_item_ri_received_items_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['ORI_Order_Request_Item']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'ori_order_request_item_ri_received_items_1',
  'view' => 'subpanel-for-ori_order_request_item-ori_order_request_item_ri_received_items_1',
);
