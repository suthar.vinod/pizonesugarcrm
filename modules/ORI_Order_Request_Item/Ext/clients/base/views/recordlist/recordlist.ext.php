<?php
// WARNING: The contents of this file are auto-generated.



$viewdefs['ORI_Order_Request_Item']['base']['view']['recordlist']['selection']['actions'][] = array(
    'name' => 'ri_mass_create_button',
    'label' => 'LBL_RI_MASS_CREATE_BUTTON',
    'type' => 'button',
    'primary' => true,
    'events' => array(
        'click' => 'list:rimasscreaterecords:fire',
    ),
    'acl_action' => 'massupdate',
);
