<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-08-26 
$viewdefs['ORI_Order_Request_Item']['base']['filter']['basic']['filters'][] = array(
  'id' => 'FilterORI_Order_Request_Item',
  'name' => 'ORI Status',
  'filter_definition' => array(
    array(
      'status'  => array(
        '$in' => '',
      ),
    ),
  ),
  'editable' => true,
  'is_template' => true,
);
