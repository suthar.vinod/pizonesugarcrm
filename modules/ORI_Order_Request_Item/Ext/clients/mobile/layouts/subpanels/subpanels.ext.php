<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-11-09 09:46:03
$viewdefs['ORI_Order_Request_Item']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'ori_order_request_item_ii_inventory_item_1',
  ),
);

// created: 2021-10-19 09:15:00
$viewdefs['ORI_Order_Request_Item']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'context' => 
  array (
    'link' => 'ori_order_request_item_ri_received_items_1',
  ),
);