<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Dependencies/or_required_dep.php


$dependencies['ORI_Order_Request_Item']['set_or_required12'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('or_order_request_ori_order_request_item_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'or_order_request_ori_order_request_item_1_name',
                'value' => 'true',
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Dependencies/name_value_dep.php


$dependencies['ORI_Order_Request_Item']['set_visibility'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('related_to'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm01_sales_ori_order_request_item_1_name',
                'value' => 'equal($related_to,"Sales")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_ori_order_request_item_1_name',
                'value' => 'equal($related_to,"Work Product")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm01_sales_ori_order_request_item_1_name',
                'value' => 'equal($related_to, "Sales")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_ori_order_request_item_1_name',
                'value' => 'equal($related_to, "Work Product")',
            ),
        ),
    ),
);

$dependencies['ORI_Order_Request_Item']['set_visibility_re'] = array(
    'hooks' => array("edit", "view"),
    'trigger' => 'true',
    'triggerFields' => array('product_selection', 'department_new_c', 'type_2'),
    'onload' => false,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'prod_product_ori_order_request_item_1_name',
                'value' => 'and(isInList($product_selection, createList("In System Vendor Non Specific", "In System Vendor Specific")), greaterThan(strlen($department_new_c),0), greaterThan(strlen($type_2),0))',
            ),
        ),
    ),
);


$dependencies['ORI_Order_Request_Item']['setStatus'] = array(
    'hooks' => array("edit", "view"),
    'trigger' => 'true',
    'triggerFields' => array(
        'status',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'status',
                'keys' => 'ifElse(isInList($status, createList("Ordered", "Backordered")), getDropdownKeySet("ORI_or_status_list"), getDropdownKeySet("ORI_status_list"))',
                'labels' => 'ifElse(isInList($status, createList("Ordered", "Backordered")), getDropdownKeySet("ORI_or_status_list"), getDropdownKeySet("ORI_status_list"))',
            ),
        ),
    ),
);

$dependencies['ORI_Order_Request_Item']['status_read'] = array(
    'hooks' => array("edit", "view"),
    'trigger' => 'true',
    'triggerFields' => array(
        'status',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'ifElse(isInList($status, createList("Requested", "Inventory")),true,false)',
            ),
        ),
    ),
);
$dependencies['ORI_Order_Request_Item']['statusasasasa_read'] = array(
    'hooks' => array("edit", "view"),
    'trigger' => 'true',
    'triggerFields' => array(
        'status',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'ifElse(isInList($status, createList("Ordered", "Backordered")),false,true)',
            ),
        ),
    ),
);


/* 
$dependencies['ORI_Order_Request_Item']['read_only_type_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'cost_per_unit',
                'value' => 'ifElse(equal($po_purchase_order_ori_order_request_item_1_name,""),true,false)',
            ),
        ),
    ),
); */
?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Dependencies/status_field_dep.php



$dependencies['ORI_Order_Request_Item']['status_reada11123'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_ori_order_request_item_1_name',
        'order_date',
        'status'
    ),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    // You could list multiple fields here each in their own array under 'actions'
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'status',
                'keys' => 'ifElse(and(
                    not(equal($request_date,"")),
                    not(equal(related($po_purchase_order_ori_order_request_item_1,"name"),"")),
                    equal(related($po_purchase_order_ori_order_request_item_1,"status_c"),"Submitted"),
                    equal(related($ori_order_request_item_ri_received_items_1,"name"),"")),getDropdownKeySet("ORI_or_status_list"),getDropdownKeySet("ORI_status_list"))',
                'labels' => 'ifElse(and(
                    not(equal($request_date,"")),
                    not(equal(related($po_purchase_order_ori_order_request_item_1,"name"),"")),
                    equal(related($po_purchase_order_ori_order_request_item_1,"status_c"),"Submitted"),
                    equal(related($ori_order_request_item_ri_received_items_1,"name"),"")),getDropdownValueSet("ORI_or_status_list"),getDropdownValueSet("ORI_status_list"))'
            ),
        ),
        
    ),
);

$dependencies['ORI_Order_Request_Item']['status3_reada11jkk1245a1hh'] = array(
    'hooks' => array("all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'ifElse(
        and(
        not(equal($request_date,"")),
        equal(related($po_purchase_order_ori_order_request_item_1,"name"),""),
        equal(related($ori_order_request_item_ri_received_items_1,"name"),"")),
        "true","false")', //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'received_date',
        'status'
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true'
            ),
        ),
    )
);

$dependencies['ORI_Order_Request_Item']['status_r2eada11jkk1245a1hh'] = array(
    'hooks' => array("all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'ifElse(
        and(
            equal($status,"Requested"),
            not(equal($request_date,"")),
            not(equal(related($po_purchase_order_ori_order_request_item_1,"name"),"")),
            equal(related($po_purchase_order_ori_order_request_item_1,"status_c"),"Pending"),            
            equal(related($ori_order_request_item_ri_received_items_1,"name"),"")),
        "true","false")', //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'received_date',
        'status'
    ),
    'onload' => true,
    'actions' => array(

        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true'
            ),
        ),
    )
);

$dependencies['ORI_Order_Request_Item']['statusg1_r2eada11jkk1245a1hh'] = array(
    'hooks' => array("all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'ifElse(and(
            not(equal($request_date,"")),
            not(equal(related($po_purchase_order_ori_order_request_item_1,"name"),"")),
            equal(related($po_purchase_order_ori_order_request_item_1,"status_c"),"Submitted"),          
            equal(related($ori_order_request_item_ri_received_items_1,"name"),"")),"false","true")', //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'received_date',
        'status'
    ),
    'onload' => true,
    'actions' => array(

        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'false'
            ),
        ),
    )
);

$dependencies['ORI_Order_Request_Item']['stgatusg1_r2eadafsd11jkk1245a1hh'] = array(
    'hooks' => array("all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'ifElse(and(
        not(equal($request_date,"")),
        not(equal(related($po_purchase_order_ori_order_request_item_1,"name"),"")),
        equal(related($po_purchase_order_ori_order_request_item_1,"status_c"),"Submitted"),
        not(equal(related($ori_order_request_item_ri_received_items_1,"name"),""))),"true","false")', //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'received_date',
        'status'
    ),
    'onload' => true,
    'actions' => array(

        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true'
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'ifElse(or(equal($status,"Ordered"),equal($status,"Backordered")),false,true)'
            ),
        ),
    )
);


/* 06-April */

$dependencies['ORI_Order_Request_Item']['status_readLux'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    //Optional, the trigger for the dependency. Defaults to 'true'.
    'triggerFields' => array(
        'po_purchase_order_poi_purchase_order_item_1_name',
        'order_date',
        'received_date',
        'status',
    ),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    // You could list multiple fields here each in their own array under 'actions'
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'status',
                'keys' => 'ifElse(or(equal($status,"Ordered"),equal($status,"Backordered")),getDropdownKeySet("ORI_or_status_list"),getDropdownKeySet("ORI_status_list"))',
                'labels' => 'ifElse(or(equal($status,"Ordered"),equal($status,"Backordered")),getDropdownValueSet("ORI_or_status_list"),getDropdownValueSet("ORI_status_list"))'
            ),
        ),
    ),
);

$dependencies['ORI_Order_Request_Item']['fr33status_reada11a1245'] = array(
    'hooks' => array("edit","all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'and(equal($status,"Ordered"),equal($status,"Backordered"))',
    'triggerFields' => array(
        'status',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'false'
            ),
        ),
    )
);


$dependencies['ORI_Order_Request_Item']['fr33status_reada11a1245'] = array(
    'hooks' => array("edit","all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'and(equal($status,"Ordered"),equal($status,"Backordered"))',
    'triggerFields' => array(
        'status',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'false'
            ),
        ),
    )
);



$dependencies['ORI_Order_Request_Item']['fr33status_reada111d245sd'] = array(
    'hooks' => array("edit","all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'equal($status,"Inventory")',
    'triggerFields' => array(
        'status',
        'id'
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'setValue',
            'params' => array(
                'target' => 'status',
                'value' => 'Inventory'
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true'
            ),
        ),
        
    )
);

$dependencies['ORI_Order_Request_Item']['fr33status_reada11v1245sdaaaaa'] = array(
    'hooks' => array("edit","all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'equal($status,"Partially Received")',
    'triggerFields' => array(
        'status',
        'id'
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'setValue',
            'params' => array(
                'target' => 'status',
                'value' => 'Partially Received'
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value' => 'true'
            ),
        ),
    )
);


?>
<?php
// Merged from custom/Extension/modules/ORI_Order_Request_Item/Ext/Dependencies/readonly_field_dep.php


$dependencies['ORI_Order_Request_Item']['cost_per_unit_readonly'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array(
        'cost_per_unit',
       // 'id',
        //'po_purchase_order_ori_order_request_item_1_name',
    ),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'cost_per_unit',
                'value' => 'equal($po_purchase_order_ori_order_request_item_1_name,"")',
            ),
        ),
    ),
);

$dependencies['ORI_Order_Request_Item']['subtype_field_readonly01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('product_selection','type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'subtype',
                'value' => 'ifElse(or(equal($product_selection,"In System Vendor Non Specific"),equal($product_selection,"In System Vendor Specific")),true,false)',
            ),
        ),
    ),
);
?>
