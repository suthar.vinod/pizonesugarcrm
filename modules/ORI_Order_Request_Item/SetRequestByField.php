<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class SetORRequestBy
{
    function SetRequestBy($bean, $event, $arguments) {
        global $db; 
        $orID   = $bean->or_order_request_ori_order_request_item_1or_order_request_ida;
        $orBean = BeanFactory::getBean('OR_Order_Request', $orID);
        $orRequestByID  = $orBean->user_id_c;
        $bean->user_id_c = $orRequestByID;
   }
}