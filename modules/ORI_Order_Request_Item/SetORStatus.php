<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class SetORStatusHook1
{    
    public function setORStatusAfterORISave($bean, $event, $arguments) {
        global $db;
        
        if (isset($arguments['isUpdate']) && $arguments['isUpdate'] == 1 && isset($arguments['dataChanges']['po_purchase_order_ori_order_request_item_1po_purchase_order_ida'])) {  
 
            $OR_Order_Request = BeanFactory::retrieveBean('OR_Order_Request', $bean->or_order_request_ori_order_request_item_1or_order_request_ida);

            $OR_Order_Request->load_relationship('or_order_request_ori_order_request_item_1');
            $ORIids = $OR_Order_Request->or_order_request_ori_order_request_item_1->get();
 
 
            $po_id = "";
            $POArray = array();
            for($a=0;$a<count($ORIids);$a++){
                $ORI_Order_Request_Item = BeanFactory::retrieveBean('ORI_Order_Request_Item', $ORIids[$a]);
                $po_id = $ORI_Order_Request_Item->po_purchase_order_ori_order_request_item_1po_purchase_order_ida; 
                if($po_id){                   
                    array_push($POArray, $po_id);
                }else{
                    array_push($POArray, "");
                }
                
            }
            $POArray1 = array_unique($POArray);
            //$GLOBALS['log']->fatal('In ORI save  POArray : '.print_r($POArray1,1));
            if(!empty($POArray1)){
                if(in_array("",$POArray1)) {
                    $POArrayString = implode(" ",$POArray1);
                    if( trim( $POArrayString ) == "" ){
                        $OR_Order_Request->status_c = "Open";
                        $OR_Order_Request->save();
                    }else {
                        $OR_Order_Request->status_c = "Partially Submitted";
                        $OR_Order_Request->save();
                    }               
                } else {
                    $OR_Order_Request->status_c = "Fully Submitted";
                    $OR_Order_Request->save();
                }
            }else{
                $OR_Order_Request->status_c = "Open";
                $OR_Order_Request->save();
            }
        }
         
    }
}
