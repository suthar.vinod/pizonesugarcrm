<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M01_SA_Division_Department/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_RECORD'] = 'Create SA_Division_Department';
$mod_strings['LNK_LIST'] = 'View SA_Division_Departments';
$mod_strings['LBL_MODULE_NAME'] = 'SA_Division_Departments';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = '(Inactive) SA Division Department';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New (Inactive) SA Division Department';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import (Inactive) SA Division Department vCard';
$mod_strings['LNK_IMPORT_M01_SA_DIVISION_DEPARTMENT'] = 'Import SA_Division_Department';
$mod_strings['LBL_LIST_FORM_TITLE'] = '(Inactive) SA Division Departments List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search (Inactive) SA Division Department';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My SA_Division_Departments';
$mod_strings['LBL_M01_SA_DIVISION_DEPARTMENT_FOCUS_DRAWER_DASHBOARD'] = 'SA_Division_Departments Focus Drawer';
$mod_strings['LBL_M01_SA_DIVISION_DEPARTMENT_RECORD_DASHBOARD'] = 'SA_Division_Departments Record Dashboard';

?>
