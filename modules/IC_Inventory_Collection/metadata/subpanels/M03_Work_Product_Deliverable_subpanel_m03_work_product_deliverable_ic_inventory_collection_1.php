<?php
// created: 2022-02-22 07:52:41
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'internal_barcode' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'readonly' => true,
    'vname' => 'LBL_INTERNAL_BARCODE',
    'width' => 10,
  ),
  'm03_work_product_ic_inventory_collection_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_TITLE',
    'id' => 'M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1M03_WORK_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M03_Work_Product',
    'target_record_key' => 'm03_work_product_ic_inventory_collection_1m03_work_product_ida',
  ),
  'test_system_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_TEST_SYSTEM_C',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
);