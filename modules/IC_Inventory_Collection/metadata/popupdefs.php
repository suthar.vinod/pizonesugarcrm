<?php
$popupMeta = array (
    'moduleMain' => 'IC_Inventory_Collection',
    'varName' => 'IC_Inventory_Collection',
    'orderBy' => 'ic_inventory_collection.name',
    'whereClauses' => array (
  'name' => 'ic_inventory_collection.name',
  'm03_work_product_ic_inventory_collection_1_name' => 'ic_inventory_collection.m03_work_product_ic_inventory_collection_1_name',
  'internal_barcode' => 'ic_inventory_collection.internal_barcode',
  'assigned_user_id' => 'ic_inventory_collection.assigned_user_id',
  'favorites_only' => 'ic_inventory_collection.favorites_only',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'm03_work_product_ic_inventory_collection_1_name',
  5 => 'internal_barcode',
  6 => 'assigned_user_id',
  7 => 'favorites_only',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10',
  ),
  'm03_work_product_ic_inventory_collection_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_TITLE',
    'id' => 'M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1M03_WORK_PRODUCT_IDA',
    'width' => '10',
    'name' => 'm03_work_product_ic_inventory_collection_1_name',
  ),
  'internal_barcode' => 
  array (
    'type' => 'varchar',
    'readonly' => true,
    'label' => 'LBL_INTERNAL_BARCODE',
    'width' => '10',
    'name' => 'internal_barcode',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10',
  ),
  'favorites_only' => 
  array (
    'name' => 'favorites_only',
    'label' => 'LBL_FAVORITES_FILTER',
    'type' => 'bool',
    'width' => '10',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_TITLE',
    'id' => 'M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1M03_WORK_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'name' => 'm03_work_product_ic_inventory_collection_1_name',
  ),
  'TEST_SYSTEM_C' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'LBL_TEST_SYSTEM_C',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'INTERNAL_BARCODE' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'readonly' => true,
    'label' => 'LBL_INTERNAL_BARCODE',
    'width' => 10,
    'name' => 'internal_barcode',
  ),
  'TEAM_NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_TEAM',
    'default' => true,
    'name' => 'team_name',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
    'name' => 'date_modified',
  ),
),
);
