<?php
// created: 2022-02-22 08:09:53
$viewdefs['IC_Inventory_Collection']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'assigned_user_name' => 
    array (
    ),
    'internal_barcode' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'test_system_c' => 
    array (
    ),
    'm03_work_product_ic_inventory_collection_1_name' => 
    array (
    ),
    'm03_work_product_deliverable_ic_inventory_collection_1_name' => 
    array (
    ),
  ),
);