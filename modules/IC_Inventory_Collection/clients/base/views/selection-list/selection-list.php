<?php
$module_name = 'IC_Inventory_Collection';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'm03_work_product_ic_inventory_collection_1_name',
                'label' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_TITLE',
                'enabled' => true,
                'id' => 'M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1M03_WORK_PRODUCT_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'test_system_c',
                'label' => 'LBL_TEST_SYSTEM_C',
                'enabled' => true,
                'readonly' => false,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'internal_barcode',
                'label' => 'LBL_INTERNAL_BARCODE',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              5 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              6 => 
              array (
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'default' => true,
                'name' => 'date_modified',
                'readonly' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
