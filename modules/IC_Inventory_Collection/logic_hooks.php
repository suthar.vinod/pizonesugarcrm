<?php
$hook_version = 1;
$hook_array = Array();
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(
    '1',
    'After Save Change Inventory Collection',

    //The PHP file where your class is located.
    'custom/modules/IC_Inventory_Collection/customLogicICNameHook.php',

    //The class the method is in.
    'customLogicICNameHook',

    //The method to call.
    'generateICName',
);

$hook_array['before_save'][] = Array(
    '2',
    'After Save Change Internal Barcode',

    //The PHP file where your class is located.
    'custom/modules/IC_Inventory_Collection/customLogicInternalBarcodeHook.php',

    //The class the method is in.
    'customLogicInternalBarcodeHook',

    //The method to call.
    'generateInternalBarcodeId',
);

$hook_array['before_save'][] = Array(
    '3',
    'append a sequence of numbers at the name of the Item.',
    'custom/modules/IC_Inventory_Collection/SetNameSequence.php',
    'SetNameSequence',
    'setName'
);

$hook_array['after_save'][] = Array(
    '10',
    'check Test System.',
    'custom/modules/IC_Inventory_Collection/checkTestSystem.php',
    'checkTestSystem',
    'updateTestSystem'
);
$hook_array['after_relationship_add'][] = Array(
    '12',
    'check Test System after relation add',
    'custom/modules/IC_Inventory_Collection/checkTestSystem.php',
    'checkTestSystem',
    'updateTestSystem'
);
$hook_array['after_relationship_delete'][] = Array(
    '13',
    'check Test System after relation delete',
    'custom/modules/IC_Inventory_Collection/checkTestSystem.php',
    'checkTestSystem',
    'updateTestSystem'
);

?>