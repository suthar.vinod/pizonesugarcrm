<?php
/**
 * When a record is saved, identify the next number sequence and append it to the name
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class SetNameSequence
{
    public function setName($bean, $event, $arguments)
    {
        //remove extra spaces
        $bean->name = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $bean->name)));

        $existingSequence = null;
        if ($bean->fetched_row) {
            $previous_name = $bean->fetched_row['name'];

            $existingSequence = substr($previous_name, -3);
            if (!is_numeric($existingSequence) === true) {
                $existingSequence = null;
            }
        }

        $seq        = $this->getSequence($bean, $existingSequence);
        $bean->name = trim($bean->name);

        if ($existingSequence === null) {
            $bean->name .= $seq;
        } else if ($existingSequence !== $seq) {
            $bean->name = substr($bean->name, 0, -3) . $seq;
        } else if ($existingSequence === $seq) {
            $eseq = substr($bean->name, -3);
            if (is_numeric($eseq) === true) {
                $bean->name = substr($bean->name, 0, -3) . $seq;
            } else {
                $bean->name .= $seq;
            }
        }

        $bean->name = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $bean->name)));
    }

    private function getSequence($bean, $existingSequence)
    {
        $sugarQuery = new SugarQuery();
        $sugarQuery->from(BeanFactory::newBean($bean->module_dir));
        $sugarQuery->select(array("id", "name"));
        $sugarQuery->where()->contains("name", $bean->name);
        $resultSet = $sugarQuery->execute();

        $sequences = array();
        foreach ($resultSet as $row) {
            $row_name = $row["name"];
            $row_seq  = substr($row_name, -3);
            if (is_numeric($row_seq)) {
                $sequences[] = intval($row_seq);
            }
        }

        if (count($sequences) > 0) {
            $max_seq = max($sequences);

            if ($existingSequence === null) {
                $max_seq += 1;
            }

            if ($max_seq < 100 && $max_seq > 9) {
                $seq = "0" . $max_seq;
            } else if ($max_seq <= 9) {
                $seq = "00" . $max_seq;
            } else {
                $seq = $max_seq;
            }
        } else {
            $seq = "001";
        }
        return $seq;
    }
}
