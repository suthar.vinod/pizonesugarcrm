<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/IC_Inventory_Collection/Ext/Language/en_us.customic_inventory_collection_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_IC_INVENTORY_COLLECTION_TITLE'] = 'Inventory Items';

?>
<?php
// Merged from custom/Extension/modules/IC_Inventory_Collection/Ext/Language/en_us.customic_inventory_collection_im_inventory_management_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE'] = 'Inventory Management';
$mod_strings['LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IC_INVENTORY_COLLECTION_TITLE'] = 'Inventory Management';

?>
<?php
// Merged from custom/Extension/modules/IC_Inventory_Collection/Ext/Language/en_us.customm03_work_product_ic_inventory_collection_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/IC_Inventory_Collection/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TEST_SYSTEM_C'] = 'Test System(s)';

?>
<?php
// Merged from custom/Extension/modules/IC_Inventory_Collection/Ext/Language/en_us.customm03_work_product_deliverable_ic_inventory_collection_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE'] = 'Work Product Deliverables';

?>
