<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-07-31 13:29:35
$viewdefs['IC_Inventory_Collection']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'ic_inventory_collection_ii_inventory_item_1',
  ),
);

// created: 2020-07-31 13:42:07
$viewdefs['IC_Inventory_Collection']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'context' => 
  array (
    'link' => 'ic_inventory_collection_im_inventory_management_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['IC_Inventory_Collection']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'ic_inventory_collection_ii_inventory_item_1',
  'view' => 'subpanel-for-ic_inventory_collection-ic_inventory_collection_ii_inventory_item_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['IC_Inventory_Collection']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'ic_inventory_collection_im_inventory_management_1',
  'view' => 'subpanel-for-ic_inventory_collection-ic_inventory_collection_im_inventory_management_1',
);
