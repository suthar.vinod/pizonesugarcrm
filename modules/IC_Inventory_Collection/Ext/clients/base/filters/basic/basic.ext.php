<?php
// WARNING: The contents of this file are auto-generated.


$viewdefs['IC_Inventory_Collection']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterInventoryCollectionTemplate',
    'name' => 'Filter Inventory Collection',
    'filter_definition' => array(
        array(
            'm03_work_product_ic_inventory_collection_1_name' => array(
                '$in' => array(),
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);

