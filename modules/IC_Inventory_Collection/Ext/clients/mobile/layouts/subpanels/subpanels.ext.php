<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-11-09 09:43:46
$viewdefs['IC_Inventory_Collection']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'ic_inventory_collection_ii_inventory_item_1',
  ),
);

// created: 2021-11-09 10:19:23
$viewdefs['IC_Inventory_Collection']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'context' => 
  array (
    'link' => 'ic_inventory_collection_im_inventory_management_1',
  ),
);