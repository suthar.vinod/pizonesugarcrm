<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/IC_Inventory_Collection/Ext/Vardefs/ic_inventory_collection_ii_inventory_item_1_IC_Inventory_Collection.php

// created: 2020-07-31 13:29:35
$dictionary["IC_Inventory_Collection"]["fields"]["ic_inventory_collection_ii_inventory_item_1"] = array (
  'name' => 'ic_inventory_collection_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'ic_inventory_collection_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'id_name' => 'ic_invento128flection_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/IC_Inventory_Collection/Ext/Vardefs/m03_work_product_ic_inventory_collection_1_IC_Inventory_Collection.php

// created: 2020-07-31 13:31:34
$dictionary["IC_Inventory_Collection"]["fields"]["m03_work_product_ic_inventory_collection_1"] = array (
  'name' => 'm03_work_product_ic_inventory_collection_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_ic_inventory_collection_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'id_name' => 'm03_work_product_ic_inventory_collection_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["IC_Inventory_Collection"]["fields"]["m03_work_product_ic_inventory_collection_1_name"] = array (
  'name' => 'm03_work_product_ic_inventory_collection_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_ic_inventory_collection_1m03_work_product_ida',
  'link' => 'm03_work_product_ic_inventory_collection_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["IC_Inventory_Collection"]["fields"]["m03_work_product_ic_inventory_collection_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_ic_inventory_collection_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE_ID',
  'id_name' => 'm03_work_product_ic_inventory_collection_1m03_work_product_ida',
  'link' => 'm03_work_product_ic_inventory_collection_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/IC_Inventory_Collection/Ext/Vardefs/ic_inventory_collection_im_inventory_management_1_IC_Inventory_Collection.php

// created: 2020-07-31 13:42:07
$dictionary["IC_Inventory_Collection"]["fields"]["ic_inventory_collection_im_inventory_management_1"] = array (
  'name' => 'ic_inventory_collection_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'ic_inventory_collection_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'IM_Inventory_Management',
  'bean_name' => 'IM_Inventory_Management',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_IM_INVENTORY_MANAGEMENT_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'id_name' => 'ic_inventoe24election_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/IC_Inventory_Collection/Ext/Vardefs/sugarfield_internal_barcode.php

 // created: 2020-10-09 10:17:16
$dictionary['IC_Inventory_Collection']['fields']['internal_barcode']['required']=false;
$dictionary['IC_Inventory_Collection']['fields']['internal_barcode']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/IC_Inventory_Collection/Ext/Vardefs/sugarfield_name.php

 // created: 2020-10-09 08:51:27
$dictionary['IC_Inventory_Collection']['fields']['name']['required']=false;
$dictionary['IC_Inventory_Collection']['fields']['name']['unified_search']=false;
$dictionary['IC_Inventory_Collection']['fields']['name']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/IC_Inventory_Collection/Ext/Vardefs/sugarfield_test_system_c.php

 // created: 2021-11-10 02:26:10
$dictionary['IC_Inventory_Collection']['fields']['test_system_c']['labelValue']='Test System(s)';
$dictionary['IC_Inventory_Collection']['fields']['test_system_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['IC_Inventory_Collection']['fields']['test_system_c']['enforced']='';
$dictionary['IC_Inventory_Collection']['fields']['test_system_c']['dependency']='';
$dictionary['IC_Inventory_Collection']['fields']['test_system_c']['required_formula']='';
$dictionary['IC_Inventory_Collection']['fields']['test_system_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/IC_Inventory_Collection/Ext/Vardefs/m03_work_product_deliverable_ic_inventory_collection_1_IC_Inventory_Collection.php

// created: 2022-02-22 07:48:02
$dictionary["IC_Inventory_Collection"]["fields"]["m03_work_product_deliverable_ic_inventory_collection_1"] = array (
  'name' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Deliverable',
  'bean_name' => 'M03_Work_Product_Deliverable',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'id_name' => 'm03_work_p2592verable_ida',
  'link-type' => 'one',
);
$dictionary["IC_Inventory_Collection"]["fields"]["m03_work_product_deliverable_ic_inventory_collection_1_name"] = array (
  'name' => 'm03_work_product_deliverable_ic_inventory_collection_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p2592verable_ida',
  'link' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'table' => 'm03_work_product_deliverable',
  'module' => 'M03_Work_Product_Deliverable',
  'rname' => 'name',
);
$dictionary["IC_Inventory_Collection"]["fields"]["m03_work_p2592verable_ida"] = array (
  'name' => 'm03_work_p2592verable_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE_ID',
  'id_name' => 'm03_work_p2592verable_ida',
  'link' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'table' => 'm03_work_product_deliverable',
  'module' => 'M03_Work_Product_Deliverable',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
