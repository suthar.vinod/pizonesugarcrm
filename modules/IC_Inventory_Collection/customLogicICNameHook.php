<?php

class customLogicICNameHook {

    function generateICName($bean, $event, $arguments) {

        global $db;

        if ($arguments['isUpdate'] == false) {

            $wp_id = $bean->m03_work_product_ic_inventory_collection_1m03_work_product_ida;

            $sqlWP = 'SELECT name FROM m03_work_product WHERE id = "' . $wp_id . '"';
            $resultWP = $db->query($sqlWP);
            if ($resultWP->num_rows > 0) {
                $rowWP = $db->fetchByAssoc($resultWP);
                $WPName = $rowWP['name'];
            }

            $bean->name = $WPName . ' IC-';
        }
    }

}
