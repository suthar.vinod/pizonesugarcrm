<?php

class customLogicInternalBarcodeHook {

    function generateInternalBarcodeId($bean, $event, $arguments) {

        global $db;

        if ($arguments['isUpdate'] == false) {
            $next_number = 1;
            $number_len = 4; 
            $year = gmdate('y');

            $sql = "SELECT MAX(substring(internal_barcode,-4)) as last_number FROM ic_inventory_collection WHERE  internal_barcode like 'APS IC{$year}-%' and deleted=0";


            $result = $db->query($sql);

            if ($result->num_rows > 0) {
                $row = $db->fetchByAssoc($result);

                $lastBarCode = $row['last_number'];
                $next_number = intval($lastBarCode) + 1;
            }
            $next_number = str_pad(strval($next_number), $number_len, "0", STR_PAD_LEFT);

            $icBarCode = 'APS IC' . $year . '-' . $next_number;

            $bean->internal_barcode = $icBarCode;
        }
    }

}
