<?php

class checkTestSystem {

    function updateTestSystem($bean, $event, $arguments) {

        global $db;
        $icID = $bean->id;
        $tsString = "";
		$tsArr = array();
        $sql = "SELECT ic_invento128flection_ida AS IC_ID,
                    IC_II.ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb AS Inventory_ID, 
                    TS_II.anml_animals_ii_inventory_item_1anml_animals_ida AS TSID,
                    TS.name AS TSName 
            FROM ic_inventory_collection_ii_inventory_item_1_c AS IC_II
        LEFT JOIN anml_animals_ii_inventory_item_1_c AS TS_II
        ON TS_II.anml_animals_ii_inventory_item_1ii_inventory_item_idb=IC_II.ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb AND TS_II.deleted=0
        LEFT JOIN anml_animals AS TS
        ON TS_II.anml_animals_ii_inventory_item_1anml_animals_ida= TS.id
        WHERE ic_invento128flection_ida = '".$icID."' AND IC_II.deleted=0";
 
        $result = $db->query($sql);

        if ($result->num_rows > 0) {
            while($row = $db->fetchByAssoc($result)){
                $tsArr[] = $row['TSName'];
            }
			$tsArr1 = array_unique($tsArr);
            $tsString = implode(",",$tsArr1);
            $updateICSql = "UPDATE ic_inventory_collection_cstm SET `test_system_c`='".$tsString."' WHERE id_c='".$icID."'";
            $db->query($updateICSql);            
        }
    }
}