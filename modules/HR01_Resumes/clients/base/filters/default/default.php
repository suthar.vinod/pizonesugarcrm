<?php
// created: 2017-07-20 17:15:44
$viewdefs['HR01_Resumes']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'assigned_user_name' => 
    array (
    ),
    'job_category_c' => 
    array (
    ),
    'document_name' => 
    array (
    ),
    'resume_reviewed_c' => 
    array (
    ),
    'interview_setup_requested_c' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);