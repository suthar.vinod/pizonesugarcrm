<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/HR01_Resumes/Ext/Vardefs/sugarfield_candidate_name_c.php

 // created: 2017-07-20 15:30:40
$dictionary['HR01_Resumes']['fields']['candidate_name_c']['labelValue']='Candidate Name';
$dictionary['HR01_Resumes']['fields']['candidate_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['HR01_Resumes']['fields']['candidate_name_c']['enforced']='';
$dictionary['HR01_Resumes']['fields']['candidate_name_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/HR01_Resumes/Ext/Vardefs/sugarfield_job_title_c.php

 // created: 2017-07-20 15:31:23
$dictionary['HR01_Resumes']['fields']['job_title_c']['labelValue']='Job Title';
$dictionary['HR01_Resumes']['fields']['job_title_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['HR01_Resumes']['fields']['job_title_c']['enforced']='';
$dictionary['HR01_Resumes']['fields']['job_title_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/HR01_Resumes/Ext/Vardefs/sugarfield_interview_setup_requested_c.php

 // created: 2017-07-20 15:58:08
$dictionary['HR01_Resumes']['fields']['interview_setup_requested_c']['labelValue']='Interview Setup Requested';
$dictionary['HR01_Resumes']['fields']['interview_setup_requested_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/HR01_Resumes/Ext/Vardefs/sugarfield_job_category_c.php

 // created: 2017-07-20 16:45:31
$dictionary['HR01_Resumes']['fields']['job_category_c']['labelValue']='Job Category';
$dictionary['HR01_Resumes']['fields']['job_category_c']['dependency']='';
$dictionary['HR01_Resumes']['fields']['job_category_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/HR01_Resumes/Ext/Vardefs/sugarfield_current_employee_c.php

 // created: 2017-07-20 16:46:30
$dictionary['HR01_Resumes']['fields']['current_employee_c']['labelValue']='Current Employee';
$dictionary['HR01_Resumes']['fields']['current_employee_c']['enforced']='';
$dictionary['HR01_Resumes']['fields']['current_employee_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/HR01_Resumes/Ext/Vardefs/sugarfield_resume_reviewed_c.php

 // created: 2017-07-21 13:51:33
$dictionary['HR01_Resumes']['fields']['resume_reviewed_c']['labelValue']='Resume Reviewed';
$dictionary['HR01_Resumes']['fields']['resume_reviewed_c']['dependency']='';

 
?>
