<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Language/en_us.customm01_sales_quotes_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Language/en_us.customm01_sales_quotes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_QUOTES_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';
$mod_strings['LBL_M01_SALES_QUOTES_1_FROM_QUOTES_TITLE'] = 'Sales Activities';

?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_QUOTES_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';
$mod_strings['LBL_M01_SALES_QUOTES_1_FROM_QUOTES_TITLE'] = 'Sales Activities';


?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_PROJECTS_SUBPANEL_TITLE'] = 'Projects';
$mod_strings['LBL_SHIPPING_ACCOUNT_NAME'] = 'Shipping Company Name:';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Company Name:';
$mod_strings['LBL_BILLING_ACCOUNT_NAME'] = 'Company Name';
$mod_strings['LBL_OPPORTUNITY_NAME'] = 'Opportunity Name:';
$mod_strings['LBL_ACCOUNT_ID'] = 'Company Id';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Company Name';
$mod_strings['EXCEPTION_QUOTE_ALREADY_CONVERTED'] = 'Quote Already Converted To Opportunity';
$mod_strings['LBL_STUDY_COMPLIANCE'] = 'Study Compliance';
$mod_strings['LBL_PRODUCT_NAME'] = 'Product Name';
$mod_strings['LBL_QUOTE_DATE'] = 'Quote Date';
$mod_strings['LBL_AMOUNT'] = 'Quote Amount';
$mod_strings['LBL_BILLING_CONTACT_NAME'] = 'Contact Name';

?>
