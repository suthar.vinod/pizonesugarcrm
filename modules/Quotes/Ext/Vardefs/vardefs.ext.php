<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:57
$dictionary['Quote']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Vardefs/sugarfield_name.php

 // created: 2019-08-26 17:25:08
$dictionary['Quote']['fields']['name']['audited']=true;
$dictionary['Quote']['fields']['name']['massupdate']=false;
$dictionary['Quote']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Quote']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Quote']['fields']['name']['merge_filter']='disabled';
$dictionary['Quote']['fields']['name']['unified_search']=false;
$dictionary['Quote']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.61',
  'searchable' => true,
);
$dictionary['Quote']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Vardefs/sugarfield_study_compliance_c.php

 // created: 2019-08-26 17:26:35
$dictionary['Quote']['fields']['study_compliance_c']['labelValue']='Study Compliance';
$dictionary['Quote']['fields']['study_compliance_c']['dependency']='';
$dictionary['Quote']['fields']['study_compliance_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Vardefs/sugarfield_product_name_c.php

 // created: 2019-08-26 17:27:41
$dictionary['Quote']['fields']['product_name_c']['labelValue']='Product Name';
$dictionary['Quote']['fields']['product_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Quote']['fields']['product_name_c']['enforced']='';
$dictionary['Quote']['fields']['product_name_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Vardefs/sugarfield_quote_date_c.php

 // created: 2019-08-26 17:28:34
$dictionary['Quote']['fields']['quote_date_c']['labelValue']='Quote Date';
$dictionary['Quote']['fields']['quote_date_c']['enforced']='';
$dictionary['Quote']['fields']['quote_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Vardefs/sugarfield_description.php

 // created: 2019-08-26 17:31:50
$dictionary['Quote']['fields']['description']['audited']=true;
$dictionary['Quote']['fields']['description']['massupdate']=false;
$dictionary['Quote']['fields']['description']['comments']='Full text of the note';
$dictionary['Quote']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Quote']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Quote']['fields']['description']['merge_filter']='disabled';
$dictionary['Quote']['fields']['description']['unified_search']=false;
$dictionary['Quote']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.57',
  'searchable' => true,
);
$dictionary['Quote']['fields']['description']['calculated']=false;
$dictionary['Quote']['fields']['description']['rows']='6';
$dictionary['Quote']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Vardefs/denorm_billing_account_name.php


// 'billing_account_name'
$dictionary['Quote']['fields']['billing_account_name']['is_denormalized'] = true;
$dictionary['Quote']['fields']['billing_account_name']['denormalized_field_name'] = 'denorm_billing_account_name';

// 'denorm_billing_account_name'
$dictionary['Quote']['fields']['denorm_billing_account_name']['name'] = 'denorm_billing_account_name';
$dictionary['Quote']['fields']['denorm_billing_account_name']['type'] = 'varchar';
$dictionary['Quote']['fields']['denorm_billing_account_name']['dbType'] = 'varchar';
$dictionary['Quote']['fields']['denorm_billing_account_name']['vname'] = 'LBL_BILLING_ACCOUNT_NAME';
$dictionary['Quote']['fields']['denorm_billing_account_name']['len'] = 255;
$dictionary['Quote']['fields']['denorm_billing_account_name']['comment'] = 'Name of the Company';
$dictionary['Quote']['fields']['denorm_billing_account_name']['unified_search'] = true;
$dictionary['Quote']['fields']['denorm_billing_account_name']['full_text_search'] = array (
  'enabled' => true,
  'searchable' => true,
  'boost' => 1.91,
);
$dictionary['Quote']['fields']['denorm_billing_account_name']['audited'] = true;
$dictionary['Quote']['fields']['denorm_billing_account_name']['required'] = false;
$dictionary['Quote']['fields']['denorm_billing_account_name']['importable'] = 'required';
$dictionary['Quote']['fields']['denorm_billing_account_name']['duplicate_on_record_copy'] = 'always';
$dictionary['Quote']['fields']['denorm_billing_account_name']['merge_filter'] = 'selected';
$dictionary['Quote']['fields']['denorm_billing_account_name']['denorm_from_module'] = 'Accounts';
$dictionary['Quote']['fields']['denorm_billing_account_name']['studio'] = false;

?>
