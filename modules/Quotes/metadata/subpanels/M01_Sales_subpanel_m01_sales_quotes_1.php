<?php
// created: 2018-04-24 16:38:12
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_LIST_QUOTE_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'quote_date_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_QUOTE_DATE',
    'width' => 10,
    'default' => true,
  ),
  'product_name_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_PRODUCT_NAME',
    'width' => 10,
    'default' => true,
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'study_compliance_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STUDY_COMPLIANCE',
    'width' => 10,
  ),
  'total' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_TOTAL',
    'currency_format' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'width' => 10,
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'name' => 'assigned_user_name',
    'vname' => 'LBL_LIST_ASSIGNED_TO_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'target_record_key' => 'assigned_user_id',
    'target_module' => 'Employees',
    'width' => 10,
    'default' => true,
  ),
  'currency_id' => 
  array (
    'usage' => 'query_only',
  ),
);