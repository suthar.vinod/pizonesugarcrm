<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class send_email_status_change
{
	function send_email_status_change_Record($bean, $event, $arguments)
	{

		global $db;
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$workProductId	= "";
		$current_date = date("Y-m-d"); //current date
		$company = array();

		if ($bean->deliverable_status_c != $bean->fetched_row['deliverable_status_c'] && $bean->deliverable_status_c == 'Ready for Study Director') {			
			
			$WPD_ID 		= $bean->id;
			$WPD_name 		= $bean->name;
			$workProductId	= $bean->m03_work_p0b66product_ida;
			$wp_bean = BeanFactory::getBean('M03_Work_Product', $workProductId);
			$wp_name  = $wp_bean->name;
			$LUID  = $wp_bean->contact_id4_c;

			$WPD_author_id 		= $bean->contact_id_c;			
			$domainToSearch 	= 'apsemail.com';
			
			//Send Email/
			$template = new EmailTemplate();
			$emailObj = new Email();
			
			$Contact_bean = BeanFactory::getBean('Contacts', $WPD_author_id);
			//$company[] = $Contact_bean->account_name;			
			//$GLOBALS['log']->fatal("===company==>".print_r($company,1));
			$AuditorEmailAddress = $Contact_bean->emailAddress->getPrimaryAddress($Contact_bean);
			//$GLOBALS['log']->fatal("=====Contactemail Address====>" . $AuditorEmailAddress);
			if ($bean->assigned_user_id) {
				$assign_id		= $bean->assigned_user_id;
				$emp_bean 		= BeanFactory::getBean('Employees', $assign_id);
				
				$emp_bean_email	= $emp_bean->emailAddress->getPrimaryAddress($emp_bean);				
				$domain_name = substr(strrchr($emp_bean_email, "@"), 1);
				if (trim(strtolower($domain_name) != $domainToSearch)) {
					$emp_bean_email = "";				
				}
			}
			//$GLOBALS['log']->fatal("=====assigned_user_id email Address====>" . $emp_bean_email);

			$template->retrieve_by_string_fields(array('name' => 'Notification of Report Ready for Study Director', 'type' => 'email'));

			$WPDLink = '<a target="_blank"href="' . $site_url . '/#M03_Work_Product/' . $workProductId . '">' . $wp_name . '</a>';
			//$GLOBALS['log']->fatal("=====WPD Link====>" . print_r($WPDLink, 1));
			$template->body_html = str_replace('[WPD_Name]', $WPDLink, $template->body_html);
			// You have been assigned as the Author for [insert hyperlink to Work Product Deliverable record using WPD name].  Please review.

			//$GLOBALS['log']->fatal('Email Template:=>' . $template->body_html);

			$defaults	= $emailObj->getSystemDefaultEmail();
			$mail		= new SugarPHPMailer();
			$mail->setMailerForSystem();
			$mail->IsHTML(true);
			$mail->From		= $defaults['email'];
			$mail->FromName = $defaults['name'];
			$mail->Subject	= $template->subject;
			$mail->Body		= $template->body_html;
			$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';

			//$mail->AddAddress('fxs_mjohnson@apsemail.com');
			//$mail->AddAddress('vsuthar@apsemail.com');
			//$company 			= array('american preclinical services','NAMSA');
			
			$domain_name = substr(strrchr($AuditorEmailAddress, "@"), 1);
			if (trim(strtolower($domain_name) == $domainToSearch)) {
				$mail->AddAddress($AuditorEmailAddress);
			}
			$mail->AddAddress($emp_bean_email);
			$mail->AddAddress('cleet@apsemail.com');
			//If their exist some valid email addresses then send email
			if (!empty($wp_emailAdd_dev)) {
				if (!$mail->Send()) {
					$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
				} else {
					$GLOBALS['log']->debug('email sent to Lead Auditor');
				}
				unset($mail);
			}
		}
	}
	

}
