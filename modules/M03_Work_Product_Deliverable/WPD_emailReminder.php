<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class WPD_emailReminder{
	static $already_ran = false;
	function sendEmailReminder($bean, $event, $arguments){
		if(self::$already_ran == true) return;
        self::$already_ran = true;
		
		global $db;
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$workProductId	= ""; 
		$current_date = date("Y-m-d"); //current date
		
		
		if (($bean->id != $bean->fetched_row['id']) && $bean->type_2_c != 'Paraffin Blocks' && $bean->type_2_c != 'Slide' && ($bean->filename == "" || ($bean->type_3_c == "Shipping" && ($bean->attn_to_name_c == "" || $bean->address_street_c == "" || $bean->address_city_c == "" || $bean->address_state_c == "" || $bean->address_postalcode_c == "" || $bean->address_country_c == "" || $bean->phone_number_c == "" || $bean->address_confirmed_c == "" || $bean->receiving_party_expecting_c == "" || $bean->faxitron_required_c == ""))) && $bean->deliverable_c == "SpecimenSample Disposition") {
			$condition_true = 1;
		}
		if (($bean->id != $bean->fetched_row['id']) && ($bean->type_2_c == 'Paraffin Blocks' || $bean->type_2_c == 'Slide') &&   ($bean->type_3_c == "Shipping" && ($bean->attn_to_name_c == "" || $bean->address_street_c == "" || $bean->address_city_c == "" || $bean->address_state_c == "" || $bean->address_postalcode_c == "" || $bean->address_country_c == "" || $bean->phone_number_c == "" || $bean->address_confirmed_c == "" || $bean->receiving_party_expecting_c == "" || $bean->faxitron_required_c == "")) && $bean->deliverable_c == "SpecimenSample Disposition") {
			$condition_true = 1;
		}

		if ($condition_true == 1) {
			$wpd_id		= $bean->id; // Work Product Deliverable ID
			$wpdName	= $bean->name;
			$UserName	= "";
			$studyDirectorName = "";

			/*Get WPD Assigned User*/
			$wpd_assignedUser_id	= $bean->assigned_user_id;
			$user_bean1	 			= BeanFactory::getBean('Employees', $wpd_assignedUser_id);
			$WPD_UserName			= $user_bean1->name;
			$WPD_UserAddress		= $user_bean1->emailAddress->getPrimaryAddress($user_bean1);

			//Query to get the Work Product ID	
			$wpd_sql = "SELECT * FROM `m03_work_product_m03_work_product_deliverable_1_c` AS WP_WPD  
			WHERE WP_WPD.`m03_work_pe584verable_idb`='".$wpd_id."' AND deleted=0 ORDER BY `WP_WPD`.`date_modified` DESC limit 1";
			$wpd_exec = $db->query($wpd_sql);
			if ($wpd_exec->num_rows > 0) 
			{
				$result			= $db->fetchByAssoc($wpd_exec);
				$workProductId	= $result['m03_work_p0b66product_ida'];
				//Update Work_Product Bean to update the Animal Countdown Value
				$wp_bean = BeanFactory::getBean('M03_Work_Product', $workProductId);
				
				//$firstProcedureDate = $wp_bean->first_procedure_c;
				
				$dueDraftDate = $bean->due_date_c;
				
				/*Get Assigned User*/
				$assignedUser_id	= $wp_bean->assigned_user_id;
				$user_bean	 		= BeanFactory::getBean('Employees', $assignedUser_id);
				$UserName			= $user_bean->name;
				$UserAddress		= $user_bean->emailAddress->getPrimaryAddress($user_bean);				
				
				/*Get Study Director*/
				$stdy_id	 		= $wp_bean->contact_id_c;
				$stdy_bean	 		= BeanFactory::getBean('Contacts', $stdy_id);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
				
				$date1 = strtotime($dueDraftDate);
				$date2 = strtotime($current_date);

				$diff = abs($date1-$date2);
				$days = floor(($diff)/ (60*60*24)); 

				if((($date1>$date2 && $days<14) || $date1<$date2 || $days==0)  &&  ($UserAddress!="" || $SDEmailAddress!="" || $WPD_UserAddress!="" )){
					//Send Email/
					///Upload Document Reminder Template 
					$template = new EmailTemplate();
					$emailObj = new Email();
					
					$template->retrieve_by_string_fields(array('name' => 'Upload Document Reminder Template', 'type' => 'email'));
					
					$wpdLink = '<a target="_blank"href="'.$site_url.'/#M03_Work_Product_Deliverable/'.$wpd_id.'">'.$wpdName.'</a>';
					 
					$WP_MGT = ($UserName!="")?$UserName:$studyDirectorName;
					$template->body_html = str_replace('[WP_MGT]', $WP_MGT, $template->body_html);
					$template->body_html = str_replace('[WPD_ID]', $wpdLink, $template->body_html);
					
					$defaults	= $emailObj->getSystemDefaultEmail();
					$mail		= new SugarPHPMailer();
					$mail->setMailerForSystem();
					$mail->IsHTML(true);
					$mail->From		= $defaults['email'];
					$mail->FromName = $defaults['name'];
					$mail->Subject	= "Reminder - Complete fields on ".$wpdName;
					$mail->Body		= $template->body_html;
							 
					 
					$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';
					
					if(!empty($UserAddress))
						$mail->AddAddress($UserAddress);

					if(!empty($SDEmailAddress))
						$mail->AddAddress($SDEmailAddress);

					if(!empty($WPD_UserAddress))
						$mail->AddAddress($WPD_UserAddress);

					//$mail->AddAddress('mjohnson@apsemail.com');
					//$mail->AddAddress('vsuthar@apsemail.com');
					/** 13 Jan 2022 : #1954 : Remove Ops Support from custom email */
				/* 	$team_name      	= 'Operations Support';
					$company 			= 'american preclinical services';
					$email_addresses 	= $this->get_related_teams($team_name);
                        if (!empty($email_addresses)) {
                            foreach ($email_addresses as $email) {
                                if ($this->verifyContactForNotification($company, $email)) {
                                    $mail->AddAddress($email);
                                }
                            }
                        }
						
					$con_email_addresses = $this->get_related_contacts_email_addresses($team_name);
					if (!empty($con_email_addresses)) {
							foreach ($con_email_addresses as $email) {
								if ($this->verifyContactForNotification($company, $email)) {
									$mail->AddAddress($email);
								}
							}
						} */
					 
					//If their exist some valid email addresses then send email
					if (!empty($wp_emailAdd_dev)) {
						if (!$mail->Send()) {
							$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
						} else {
							$GLOBALS['log']->debug('email sent');
						}
						unset($mail);
					}
				} 
			}
		}
	}
	
	/**
     * Members which are associated with team, return there email addresses
     * @param type $team_name
     * @return type array
     */
	/** 13 Jan 2022 : #1954 : Remove Ops Support from custom email */
   /*  function get_related_teams($team_name) {
        $email_addresses = array();

        $get_team_members = "SELECT email_addresses.email_address FROM teams "
                . "INNER JOIN team_memberships ON teams.id = team_memberships.team_id "
                . "INNER JOIN users ON team_memberships.user_id = users.id "
                . "INNER JOIN email_addr_bean_rel ON users.id = email_addr_bean_rel.bean_id "
                . "INNER JOIN email_addresses ON email_addr_bean_rel.email_address_id = email_addresses.id "
                . "WHERE teams.name = '" . $team_name . "' AND teams.deleted = 0 AND team_memberships.deleted = 0 AND users.deleted = 0 ";

        $team_members = $GLOBALS['db']->query($get_team_members);
        while ($team_member_address = $GLOBALS["db"]->fetchByAssoc($team_members)) {
            if (!empty($team_member_address['email_address'])) {
                $email_addresses[] = $team_member_address['email_address'];
            }
        }

        return $email_addresses;
    } */

    /**
     * Contacts which are associated with team, return there email addresses
     * @param type $team_name
     * @return type array
     */
	/** 13 Jan 2022 : #1954 : Remove Ops Support from custom email */
    /* function get_related_contacts_email_addresses($team_name) {

        $contacts_email_addreses = array();

        $query = new SugarQuery();
        $query->from(BeanFactory::newBean('Teams'), array('alias' => 'teams'));
        $query->joinTable('team_sets_teams', array(
            'joinType' => 'INNER',
            'alias' => 'tst',
            'linkingTable' => true,
        ))->on()->equalsField('teams.id', 'tst.team_id');
        $query->joinTable('contacts', array(
            'joinType' => 'INNER',
            'alias' => 'c',
            'linkingTable' => true,
        ))->on()->equalsField('tst.team_set_id', 'c.team_set_id');
        $query->joinTable('email_addr_bean_rel', array(
            'joinType' => 'INNER',
            'alias' => 'eabr',
            'linkingTable' => true,
        ))->on()->equalsField('c.id', 'eabr.bean_id');
        $query->joinTable('email_addresses', array(
            'joinType' => 'INNER',
            'alias' => 'ea',
            'linkingTable' => true,
        ))->on()->equalsField('eabr.email_address_id', 'ea.id');

        $query->where()
                ->equals('teams.name', $team_name)
                ->equals('teams.deleted', 0)
                ->equals('tst.deleted', 0)
                ->equals('c.deleted', 0)
                ->equals('eabr.deleted', 0);
        $query->select('ea.email_address');
        $compile_query = $query->compile();
        $result_query = $query->execute();

        foreach ($result_query as $key => $innerArray) {
            foreach ($innerArray as $key => $email_address) {
                $contacts_email_addreses[] = $email_address;
            }
        }

        return $contacts_email_addreses;
    }
	
	function verifyContactForNotification($account_name, $email) {
        $companyToSearch = 'american preclinical services';
        $domainToSearch = 'apsemail.com';
        $domain_name = substr(strrchr($email, "@"), 1);
        if (trim(strtolower($account_name)) == $companyToSearch && strtolower($domain_name) == $domainToSearch) {
            return true;
        }
        return false;
    } */


}
?>