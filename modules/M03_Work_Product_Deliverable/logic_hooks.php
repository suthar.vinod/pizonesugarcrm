<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['before_save'] = Array(); 
$hook_array['before_save'][] = Array('1','workflow','include/workflow/WorkFlowHandler.php','WorkFlowHandler','WorkFlowHandler',);

$hook_array['after_relationship_add'][] = Array(
    1011, 
    'Send email notification if Current date is with in 14 date from First Procedure date', 
    'custom/modules/M03_Work_Product_Deliverable/WPD_emailReminder.php', 
    'WPD_emailReminder', 
    'sendEmailReminder'
);

$hook_array['after_relationship_delete'][] = Array(
   1012, 
   'Update BC Deliverable date calculations After delete',
   'custom/modules/M03_Work_Product_Deliverable/update_WPD_draft_date.php',
   'update_WPD_draft_dateHook',
   'update_WPD_draft_dateHook_Method',
);

 $hook_array['before_save'][] = Array(
    1033, 
    'Send email notification if Deliverable = QC Data Review and Deliverable Status changes to Completed', 
    'custom/modules/M03_Work_Product_Deliverable/WPD_emailWorkFlow.php', 
    'WPD_emailWorkFlow', 
    'sendEmailWorkFlow'
 );
 $hook_array['before_save'][] = Array(
   102,
   'Save Change by and date',
   'custom/modules/M03_Work_Product_Deliverable/savechangebytime.php',
   'savechangebytime',
   'savechangebytimeRecord'
);
/* $hook_array['before_save'][] = array(
   107,
   'After Save Change ORI Status',
   'custom/modules/M03_Work_Product_Deliverable/get_WPD_cus_cal.php',
   'get_WPD_cus_calHook',
   'get_WPD_cus_calMethod',
); */
$hook_array['after_save'][] = array(
   108,
   'Update BC Deliverable date calculations',
   'custom/modules/M03_Work_Product_Deliverable/get_WPD_cus_date_cal.php',
   'get_WPD_cus_date_calHook',
   'get_WPD_cus_date_calMethod',
);
$hook_array['after_save'][] = array(
   109,
   'After Save Get Author',
   'custom/modules/M03_Work_Product_Deliverable/get_WPD_author.php',
   'get_WPD_author_Hook',
   'get_WPD_author_Method',
);
$hook_array['before_save'][] = array(
   110,
   'After Save Send Mail to the Author',
   'custom/modules/M03_Work_Product_Deliverable/authorChangeEmailRemainder.php',
   'saveAuthorChangeEmail',
   'saveAuthorChangeEmailRecord',
);
$hook_array['before_save'][] = array(
   111,
   'Custom email workflow for Lead Auditor',
   'custom/modules/M03_Work_Product_Deliverable/send_email_lead_auditor.php',
   'send_email_lead_auditor',
   'send_email_lead_auditorRecord',
);
$hook_array['before_save'][] = array(
   112,
   'Custom workflow for WPD status change',
   'custom/modules/M03_Work_Product_Deliverable/send_email_status_change.php',
   'send_email_status_change',
   'send_email_status_change_Record',
);
/* $hook_array['before_save'][] = Array(
   114, 
   'Send email notification if Deliverable Status changes to Not Performed', 
   'custom/modules/M03_Work_Product_Deliverable/WPD_Not_Performed_emailWorkFlow.php', 
   'WPD_Not_Performed_emailWorkFlow', 
   'sendNotPerformedEmailWorkFlow'
); */
?>