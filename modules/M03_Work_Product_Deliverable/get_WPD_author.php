<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class get_WPD_author_Hook
{
	static $already_ran = false;
	function get_WPD_author_Method($bean, $event, $arguments)
	{
		if (self::$already_ran == true) return;
		self::$already_ran = true;

		global $db;
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$workProductId	= "";
		$current_date = date("Y-m-d"); //current date
		$wpd_id		= $bean->id; // Work Product Deliverable ID			
		//$GLOBALS['log']->fatal("wpd_id: author_id " . $wpd_id);

		$workProductId	= $bean->m03_work_p0b66product_ida;
		$wp_bean = BeanFactory::getBean('M03_Work_Product', $workProductId);
		$wpname = $wp_bean->name;
		//$GLOBALS['log']->fatal("author_id:wpname " . $wpname);
		if ($workProductId != '') {
			$deliverable_c = $bean->deliverable_c;
			//$GLOBALS['log']->fatal("deliverable_c: author_id " . $deliverable_c);
			$auth_id = $this->get_author_wpd($wpname);
			$queryCustomupdate2 = "SELECT  
				WPD.id AS id, 
				WPD.name AS name				          
				FROM `m03_work_product_deliverable` AS WPD
					LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
						ON WPD.id= WPDCSTM.id_c
					RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
						ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
					LEFT JOIN m03_work_product AS WP
						ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
					LEFT JOIN m03_work_product_cstm AS WPCSTM
						ON WP.id=WPCSTM.id_c
				WHERE  WP.name = '" . $wpname . "' 				
				AND WPD.deleted = 0";
			$queryResult2 = $db->query($queryCustomupdate2);
			while ($fetchResult2 = $db->fetchByAssoc($queryResult2)) {
				$id		= $fetchResult2['id'];

				$wpdUpdateSQL = "UPDATE `m03_work_product_deliverable_cstm` SET `contact_id1_c`='".$auth_id."' WHERE id_c = '".$id."'";
				$db->query($wpdUpdateSQL);
				
				//$conBean = BeanFactory::retrieveBean('Contacts',  $auth_id);
				//$WPDBean = BeanFactory::retrieveBean('M03_Work_Product_Deliverable',  $id);
				//$WPDBean->author_report_c = $conBean->name;
				//$WPDBean->contact_id1_c = $auth_id;
				//$WPDBean->save();
				//$GLOBALS['log']->fatal("author_id:" . $auth_id);
				//$GLOBALS['log']->fatal("author_id: conBean->name " . $conBean->name);
				//$GLOBALS['log']->fatal("author_id: fetchResult2 " . print_r($fetchResult2, 1));
			}
		}
	}
	function get_author_wpd($wpname)
	{
		//$GLOBALS['log']->fatal("num_rows2:WPD " . $wpname);
		global $db, $current_user;
		$queryCustomupdate = "SELECT  
				WPD.id AS id, 
				WPD.name AS name, 
				WPDCSTM.contact_id_c AS author           
				FROM `m03_work_product_deliverable` AS WPD
					LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
						ON WPD.id= WPDCSTM.id_c
					RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
						ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
					LEFT JOIN m03_work_product AS WP
						ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
					LEFT JOIN m03_work_product_cstm AS WPCSTM
						ON WP.id=WPCSTM.id_c
				WHERE  WP.name = '" . $wpname . "' 
				AND (WPDCSTM.deliverable_c = 'Histopathology Report' OR WPDCSTM.deliverable_c = 'Interim Histopathology Report' 
				OR WPDCSTM.deliverable_c ='Histopathology Report2' OR WPDCSTM.deliverable_c ='Interim Histopathology Report2' 
                OR WPDCSTM.deliverable_c ='Histopathology Notes')
				AND WPD.deleted = 0";
		$queryResult1 = $db->query($queryCustomupdate);
		while ($fetchResult1 = $db->fetchByAssoc($queryResult1)) {
			$id		= $fetchResult1['id'];
			$author_id		= $fetchResult1['author'];
			//$GLOBALS['log']->fatal("author_id:" . $author_id);
			//$GLOBALS['log']->fatal("author_id: id " . $id);
			//$GLOBALS['log']->fatal("author_id: queryResult1 " . print_r($fetchResult1, 1));
		}
		return $author_id;
	}
}
