<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class get_WPD_cus_date_calHook
{
	static $already_ran = false;
	function get_WPD_cus_date_calMethod($bean, $event, $arguments)
	{
		if (self::$already_ran == true) return;
		self::$already_ran = true;

		global $db;
		$site_url			= $GLOBALS['sugar_config']['site_url'];
		$workProductId		= "";
		$current_date		= date("Y-m-d"); //current date
		$wpd_id				= $bean->id; // Work Product Deliverable ID

		//Query to get the Work Product ID
		$workProductId		= $bean->m03_work_p0b66product_ida;
		//Update Work_Product Bean to update the Animal Countdown Value
		$wp_bean			= BeanFactory::getBean('M03_Work_Product', $workProductId);
		$wpname				= $wp_bean->name;

		if($bean->manual_datechange == 0) {
			$deliverable_c = $bean->deliverable_c;			
			$num_rows = $this->get_record_wpd($wpname);

			$queryCustomupdate = "SELECT  
				WPD.id AS id, 
				WPD.name AS name				
				FROM `m03_work_product_deliverable` AS WPD
				LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
				ON WPD.id= WPDCSTM.id_c
				RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
				ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
				LEFT JOIN m03_work_product AS WP
				ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
				LEFT JOIN m03_work_product_cstm AS WPCSTM
				ON WP.id=WPCSTM.id_c
				WHERE  WP.name = '" . $wpname . "' 
				AND WPDCSTM.deliverable_c = 'Final Report'
				AND WPD.deleted = 0
				ORDER BY WPDCSTM.due_date_c DESC";

			$queryResult1 = $db->query($queryCustomupdate);
			while ($fetchResult1 = $db->fetchByAssoc($queryResult1)) {
				$id = $fetchResult1['id'];

				$WPDBean = BeanFactory::retrieveBean('M03_Work_Product_Deliverable', $id);

				if ($wp_bean->functional_area_c == 'Standard Biocompatibility' && $wp_bean->report_timeline_type_c == 'By First Procedure' && ($wp_bean->first_procedure_c != "" || $wp_bean->first_procedure_c != null)) {

					if ($WPDBean->deliverable_c == "Final Report") {
						if ($wp_bean->final_report_timeline_c == '' || $wp_bean->final_report_timeline_c == null) {
							$Days = ' + 0 days';
							$finalDueDate = date('Y-m-d', strtotime($wp_bean->first_procedure_c . $Days));
						} else {
							$Days = 7 * $wp_bean->final_report_timeline_c;
							$finalDueDate = date('Y-m-d', strtotime($wp_bean->first_procedure_c . ' + ' . $Days . ' days'));
						}

						if ($num_rows > 0) {
							if ($wp_bean->final_report_timeline_c == '' || $wp_bean->final_report_timeline_c == null) {
								//$DueDays = ' - 5 days';
								$minusDays = 5; 
								$DueDate = $this->getDaysCalculation($wp_bean->first_procedure_c,$minusDays);
								
							} else {
								$DueDays	= (7 * $wp_bean->final_report_timeline_c);// - 5
								$DueDate1	= date('Y-m-d', strtotime($wp_bean->first_procedure_c . ' + ' . $DueDays . ' days'));
								$minusDays	= 5;
								$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
							}
						} else {
							if ($wp_bean->final_report_timeline_c == '' || $wp_bean->final_report_timeline_c == null) {
								//$DueDays = ' - 3 days';
								//$DueDate = date('Y-m-d', strtotime($wp_bean->first_procedure_c . $DueDays));
								$minusDays	= 3;
								$DueDate	= $this->getDaysCalculation($wp_bean->first_procedure_c,$minusDays);
								
							} else {
								$DueDays	= (7 * $wp_bean->final_report_timeline_c); //-3
								$DueDate	= date('Y-m-d', strtotime($wp_bean->first_procedure_c . ' + ' . $DueDays . ' days'));
								
								$minusDays	= 3;
								$DueDate	= $this->getDaysCalculation($DueDate,$minusDays);
							}
						}
						$WPDBean->final_due_date_c	= $finalDueDate;
						$WPDBean->due_date_c		= $DueDate;
						$WPDBean->save();
					} else if ($WPDBean->deliverable_c == "Histopathology Report") {

						if ($wp_bean->final_report_timeline_c == '' || $wp_bean->final_report_timeline_c == null) {
							$Days = ' + 0 days';
							//$DueDays = ' - 6 days';

							$finalDueDate = date('Y-m-d', strtotime($wp_bean->first_procedure_c . $Days));
							$minusDays	= 6;
							$DueDate	= $this->getDaysCalculation($wp_bean->first_procedure_c,$minusDays);
							//$DueDate = date('Y-m-d', strtotime($wp_bean->first_procedure_c . $DueDays));
						} else {
							$Days = 7 * $wp_bean->final_report_timeline_c;
							$finalDueDate = date('Y-m-d', strtotime($wp_bean->first_procedure_c . ' + ' . $Days . ' days'));

							if ($wp_bean->final_report_timeline_c == 1) {
								//$DueDays = ' - 1 days';
								//$DueDate = date('Y-m-d', strtotime($wp_bean->first_procedure_c . $DueDays));
								$minusDays	= 1;
								$DueDate	= $this->getDaysCalculation($wp_bean->first_procedure_c,$minusDays);
							
							
							} else {
								$DueDays = (7 * $wp_bean->final_report_timeline_c) ;//- 6
								$DueDate1 = date('Y-m-d', strtotime($wp_bean->first_procedure_c . ' + ' . $DueDays . ' days'));
								$minusDays	= 6;
								$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
								
							}
						}

						$WPDBean->final_due_date_c = $finalDueDate;
						$WPDBean->due_date_c = $DueDate;
						$WPDBean->save();
					}
				} else if ($wp_bean->functional_area_c == 'Standard Biocompatibility' && $wp_bean->report_timeline_type_c == 'By SPA Reconciliation' && ($wp_bean->bc_spa_reconciled_date_c != "" || $wp_bean->bc_spa_reconciled_date_c != null)) {

					if ($WPDBean->deliverable_c == "Final Report") {
						if ($wp_bean->final_report_timeline_c == '' || $wp_bean->final_report_timeline_c == null) {
							$Days = ' + 0 days';
							$finalDueDate = date('Y-m-d', strtotime($wp_bean->bc_spa_reconciled_date_c . $Days));
						} else {
							$Days = 7 * $wp_bean->final_report_timeline_c;
							$finalDueDate = date('Y-m-d', strtotime($wp_bean->bc_spa_reconciled_date_c . ' + ' . $Days . ' days'));
						}

						if ($num_rows > 0) {
							if ($wp_bean->final_report_timeline_c == '' || $wp_bean->final_report_timeline_c == null) {
								//$DueDays = ' - 5 days';
								//$DueDate = date('Y-m-d', strtotime($wp_bean->bc_spa_reconciled_date_c . $DueDays));
								
								$minusDays	= 5;
								$DueDate	= $this->getDaysCalculation($wp_bean->bc_spa_reconciled_date_c,$minusDays);
								
							} else {
								$DueDays = (7 * $wp_bean->final_report_timeline_c) ;//- 5
								$DueDate1 = date('Y-m-d', strtotime($wp_bean->bc_spa_reconciled_date_c . ' + ' . $DueDays . ' days'));
								
								$minusDays	= 5;
								$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
							}
						} else {
							if ($wp_bean->final_report_timeline_c == '' || $wp_bean->final_report_timeline_c == null) {
								//$DueDays = ' - 3 days';
								//$DueDate1 = date('Y-m-d', strtotime($wp_bean->bc_spa_reconciled_date_c . $DueDays));
								
								$minusDays	= 3;
								$DueDate	= $this->getDaysCalculation($wp_bean->bc_spa_reconciled_date_c,$minusDays);
								
								
							} else {
								$DueDays = (7 * $wp_bean->final_report_timeline_c) ;//- 3
								$DueDate1 = date('Y-m-d', strtotime($wp_bean->bc_spa_reconciled_date_c . ' + ' . $DueDays . ' days'));
								$minusDays	= 3;
								$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
								
							}
						}

						$WPDBean->final_due_date_c = $finalDueDate;
						$WPDBean->due_date_c = $DueDate;
						$WPDBean->save();
					} else if ($WPDBean->deliverable_c == "Histopathology Report") {
						if ($wp_bean->final_report_timeline_c == '' || $wp_bean->final_report_timeline_c == null) {
							$Days = ' + 0 days';
							$DueDays = ' - 6 days';

							$finalDueDate = date('Y-m-d', strtotime($wp_bean->bc_spa_reconciled_date_c . $Days));
							//$DueDate = date('Y-m-d', strtotime($wp_bean->bc_spa_reconciled_date_c . $DueDays));
							$minusDays	= 6;
							$DueDate	= $this->getDaysCalculation($wp_bean->bc_spa_reconciled_date_c,$minusDays);
								
						} else {
							$Days = 7 * $wp_bean->final_report_timeline_c;
							$finalDueDate = date('Y-m-d', strtotime($wp_bean->bc_spa_reconciled_date_c . ' + ' . $Days . ' days'));

							if ($wp_bean->final_report_timeline_c == 1) {
								//$DueDays = ' - 1 days';
								//$DueDate = date('Y-m-d', strtotime($wp_bean->bc_spa_reconciled_date_c . $DueDays));
								$minusDays	= 1;
								$DueDate	= $this->getDaysCalculation($wp_bean->bc_spa_reconciled_date_c,$minusDays);
								
							} else {
								$DueDays	= (7 * $wp_bean->final_report_timeline_c) ;//- 6
								$DueDate1	= date('Y-m-d', strtotime($wp_bean->bc_spa_reconciled_date_c . '+'.$DueDays. ' days'));
								$minusDays	= 6;
								$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
							}
						}

						$WPDBean->final_due_date_c = $finalDueDate;
						$WPDBean->due_date_c = $DueDate;
						$WPDBean->save();
					}
				}
			}
		}
	}
	
	
	function getDaysCalculation($actualDate,$subDays){
		$t = strtotime($actualDate);
 
		for($i=$subDays; $i>0; $i--){
			$oneDay = 86400;// add 1 day to timestamp
			$nextDay = date('w', ($t-$oneDay));// get what day it is next day
			// if it's Saturday or Sunday get $i-1
			if($nextDay == 0 || $nextDay == 6) {
				$i++;
			}
			
			$t = $t-$oneDay;// modify timestamp, add 1 day
		}

		return date('Y-m-d',$t);		
	}
	
	function get_record_wpd($wpname)
	{

		global $db, $current_user;
		$queryCustom = "SELECT  
					WPD.id AS id		
					FROM `m03_work_product_deliverable` AS WPD
						LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
							ON WPD.id= WPDCSTM.id_c
						RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
							ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
						LEFT JOIN m03_work_product AS WP
							ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
						LEFT JOIN m03_work_product_cstm AS WPCSTM
							ON WP.id=WPCSTM.id_c
					WHERE  WP.name = '" . $wpname . "' 
					AND WPDCSTM.deliverable_c ='Histopathology Report' AND WPD.deleted = 0
					ORDER BY WPDCSTM.due_date_c DESC";
		$queryResult = $db->query($queryCustom);
		$fetchResult = $db->fetchByAssoc($queryResult);
		$num_rows = $queryResult->num_rows;

		return $num_rows;
	}
}
