<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/WirelessLayoutdefs/m03_work_product_deliverable_tasks_1_M03_Work_Product_Deliverable.php

 // created: 2016-02-29 19:53:08
$layout_defs["M03_Work_Product_Deliverable"]["subpanel_setup"]['m03_work_product_deliverable_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'm03_work_product_deliverable_tasks_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/WirelessLayoutdefs/m03_work_product_deliverable_ii_inventory_item_1_M03_Work_Product_Deliverable.php

 // created: 2022-02-22 07:44:29
$layout_defs["M03_Work_Product_Deliverable"]["subpanel_setup"]['m03_work_product_deliverable_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'm03_work_product_deliverable_ii_inventory_item_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/WirelessLayoutdefs/m03_work_product_deliverable_ic_inventory_collection_1_M03_Work_Product_Deliverable.php

 // created: 2022-02-22 07:48:02
$layout_defs["M03_Work_Product_Deliverable"]["subpanel_setup"]['m03_work_product_deliverable_ic_inventory_collection_1'] = array (
  'order' => 100,
  'module' => 'IC_Inventory_Collection',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'get_subpanel_data' => 'm03_work_product_deliverable_ic_inventory_collection_1',
);

?>
