<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/m03_work_product_m03_work_product_deliverable_1_M03_Work_Product_Deliverable.php

// created: 2016-02-01 20:37:08
$dictionary["M03_Work_Product_Deliverable"]["fields"]["m03_work_product_m03_work_product_deliverable_1"] = array (
  'name' => 'm03_work_product_m03_work_product_deliverable_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_m03_work_product_deliverable_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'id_name' => 'm03_work_p0b66product_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product_Deliverable"]["fields"]["m03_work_product_m03_work_product_deliverable_1_name"] = array (
  'name' => 'm03_work_product_m03_work_product_deliverable_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p0b66product_ida',
  'link' => 'm03_work_product_m03_work_product_deliverable_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["M03_Work_Product_Deliverable"]["fields"]["m03_work_p0b66product_ida"] = array (
  'name' => 'm03_work_p0b66product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE_ID',
  'id_name' => 'm03_work_p0b66product_ida',
  'link' => 'm03_work_product_m03_work_product_deliverable_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/tasks_m03_work_product_deliverable_1_M03_Work_Product_Deliverable.php

// created: 2016-02-29 15:01:15
$dictionary["M03_Work_Product_Deliverable"]["fields"]["tasks_m03_work_product_deliverable_1"] = array (
  'name' => 'tasks_m03_work_product_deliverable_1',
  'type' => 'link',
  'relationship' => 'tasks_m03_work_product_deliverable_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'id_name' => 'tasks_m03_work_product_deliverable_1tasks_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product_Deliverable"]["fields"]["tasks_m03_work_product_deliverable_1_name"] = array (
  'name' => 'tasks_m03_work_product_deliverable_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_TASKS_TITLE',
  'save' => true,
  'id_name' => 'tasks_m03_work_product_deliverable_1tasks_ida',
  'link' => 'tasks_m03_work_product_deliverable_1',
  'table' => 'tasks',
  'module' => 'Tasks',
  'rname' => 'name',
);
$dictionary["M03_Work_Product_Deliverable"]["fields"]["tasks_m03_work_product_deliverable_1tasks_ida"] = array (
  'name' => 'tasks_m03_work_product_deliverable_1tasks_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE_ID',
  'id_name' => 'tasks_m03_work_product_deliverable_1tasks_ida',
  'link' => 'tasks_m03_work_product_deliverable_1',
  'table' => 'tasks',
  'module' => 'Tasks',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/m03_work_product_deliverable_tasks_1_M03_Work_Product_Deliverable.php

// created: 2016-02-29 19:53:08
$dictionary["M03_Work_Product_Deliverable"]["fields"]["m03_work_product_deliverable_tasks_1"] = array (
  'name' => 'm03_work_product_deliverable_tasks_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_deliverable_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'id_name' => 'm03_work_pbb72verable_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:56
$dictionary['M03_Work_Product_Deliverable']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_date_qa_received_c.php

 // created: 2019-01-17 16:28:27
$dictionary['M03_Work_Product_Deliverable']['fields']['date_qa_received_c']['labelValue']='Date QA Received';
$dictionary['M03_Work_Product_Deliverable']['fields']['date_qa_received_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['date_qa_received_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_date_qa_edits_received_c.php

 // created: 2019-01-25 19:57:35
$dictionary['M03_Work_Product_Deliverable']['fields']['date_qa_edits_received_c']['labelValue']='Date QA Edits Submitted to SD';
$dictionary['M03_Work_Product_Deliverable']['fields']['date_qa_edits_received_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['date_qa_edits_received_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_amendment_responsibility_c.php

 // created: 2019-04-18 12:42:08
$dictionary['M03_Work_Product_Deliverable']['fields']['amendment_responsibility_c']['labelValue']='Amendment Responsibility';
$dictionary['M03_Work_Product_Deliverable']['fields']['amendment_responsibility_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['amendment_responsibility_c']['visibility_grid']=array (
  'trigger' => 'deliverable_c',
  'values' => 
  array (
    'Choose One' => 
    array (
    ),
    'QA Data Review' => 
    array (
    ),
    'Critical Phase Audit' => 
    array (
    ),
    'Protocol Development' => 
    array (
    ),
    'Final Report' => 
    array (
    ),
    'Final Report Amendment' => 
    array (
      0 => '',
      1 => 'APS Error',
      2 => 'Sponsor Error',
    ),
    'Gross Pathology Report' => 
    array (
    ),
    'Histopathology Notes' => 
    array (
    ),
    'Histopathology Report' => 
    array (
    ),
    'Histopathology and Histomorphometry Report' => 
    array (
    ),
    'Animal Health Report' => 
    array (
    ),
    'In Life Report' => 
    array (
    ),
    'Tissue Shipping' => 
    array (
    ),
    'Tissue Trimming' => 
    array (
    ),
    'SEM Prep' => 
    array (
    ),
    'SEM' => 
    array (
    ),
    'SEM Report' => 
    array (
    ),
    'Slide Completion' => 
    array (
    ),
    'Recuts' => 
    array (
    ),
    'Slide Shipping' => 
    array (
    ),
    'Histomorphometry' => 
    array (
    ),
    'Tissue Receipt' => 
    array (
    ),
    'Faxitron' => 
    array (
    ),
    'Interim Report' => 
    array (
    ),
    'Interim Gross Pathology Report' => 
    array (
    ),
    'Interim Histopathology Report' => 
    array (
    ),
    'Interim Animal Health Report' => 
    array (
    ),
    'Interim In Life Report' => 
    array (
    ),
    'Pharmacology Data Summary' => 
    array (
    ),
    'Waiting on external test site' => 
    array (
    ),
    'Bioanalytical Method Development' => 
    array (
    ),
    'Bioanalytical Validation Report' => 
    array (
    ),
    'Bioanalytical Specimen Analysis' => 
    array (
    ),
    'Bioanalytical Sample Prep' => 
    array (
    ),
    'Bioanalytical Stability Analysis' => 
    array (
    ),
    'Bioanalytical Data Table' => 
    array (
    ),
    'Analytical Report' => 
    array (
    ),
    'Bioanalytical Protocol Dvelopment' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_work_product_deliverable_not_c.php

 // created: 2019-04-22 12:15:35
$dictionary['M03_Work_Product_Deliverable']['fields']['work_product_deliverable_not_c']['labelValue']='Notes';
$dictionary['M03_Work_Product_Deliverable']['fields']['work_product_deliverable_not_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['work_product_deliverable_not_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['work_product_deliverable_not_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_department_manager_approval_c.php

 // created: 2019-04-22 12:17:29
$dictionary['M03_Work_Product_Deliverable']['fields']['department_manager_approval_c']['labelValue']='Departmental Approval';
$dictionary['M03_Work_Product_Deliverable']['fields']['department_manager_approval_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['department_manager_approval_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_integer_estimated_owner_c.php

 // created: 2019-04-22 12:23:27
$dictionary['M03_Work_Product_Deliverable']['fields']['integer_estimated_owner_c']['labelValue']='Decimal-Estimated Hours (Deliverable Owner)';
$dictionary['M03_Work_Product_Deliverable']['fields']['integer_estimated_owner_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['integer_estimated_owner_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_estimated_report_hours_c.php

 // created: 2019-04-22 12:24:23
$dictionary['M03_Work_Product_Deliverable']['fields']['estimated_report_hours_c']['labelValue']='Estimated Hours (Deliverable Owner)';
$dictionary['M03_Work_Product_Deliverable']['fields']['estimated_report_hours_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['estimated_report_hours_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['estimated_report_hours_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_actual_hours_owner_c.php

 // created: 2019-04-22 12:25:00
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_hours_owner_c']['labelValue']='Actual Hours (Deliverable Owner)';
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_hours_owner_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_hours_owner_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_hours_owner_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_integer_actual_owner_c.php

 // created: 2019-04-22 12:25:46
$dictionary['M03_Work_Product_Deliverable']['fields']['integer_actual_owner_c']['labelValue']='Decimal-Actual Hours (Deliverable Owner)';
$dictionary['M03_Work_Product_Deliverable']['fields']['integer_actual_owner_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['integer_actual_owner_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_contact_id_c.php

 // created: 2019-04-29 11:51:33

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_author_c.php

 // created: 2019-04-29 11:51:33
$dictionary['M03_Work_Product_Deliverable']['fields']['author_c']['labelValue']='Author';
$dictionary['M03_Work_Product_Deliverable']['fields']['author_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_embedded_c.php

 // created: 2020-01-16 12:43:22
$dictionary['M03_Work_Product_Deliverable']['fields']['embedded_c']['labelValue']='Embedded';
$dictionary['M03_Work_Product_Deliverable']['fields']['embedded_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['embedded_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['embedded_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_cut_c.php

 // created: 2020-01-16 12:44:59
$dictionary['M03_Work_Product_Deliverable']['fields']['cut_c']['labelValue']='Cut';
$dictionary['M03_Work_Product_Deliverable']['fields']['cut_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['cut_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['cut_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_qcd_c.php

 // created: 2020-01-16 12:46:07
$dictionary['M03_Work_Product_Deliverable']['fields']['qcd_c']['labelValue']='QC\'d';
$dictionary['M03_Work_Product_Deliverable']['fields']['qcd_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['qcd_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['qcd_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_charge_sheet_submitted_c.php

 // created: 2020-01-16 12:46:48
$dictionary['M03_Work_Product_Deliverable']['fields']['charge_sheet_submitted_c']['labelValue']='Charge Sheet Submitted';
$dictionary['M03_Work_Product_Deliverable']['fields']['charge_sheet_submitted_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['charge_sheet_submitted_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['charge_sheet_submitted_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_stained_c.php

 // created: 2020-01-16 12:52:32
$dictionary['M03_Work_Product_Deliverable']['fields']['stained_c']['labelValue']='Stained';
$dictionary['M03_Work_Product_Deliverable']['fields']['stained_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['stained_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['stained_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_controls_submitted_c.php

 // created: 2020-01-17 15:27:58
$dictionary['M03_Work_Product_Deliverable']['fields']['controls_submitted_c']['labelValue']='Controls Submitted';
$dictionary['M03_Work_Product_Deliverable']['fields']['controls_submitted_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['controls_submitted_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['controls_submitted_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_name.php

 // created: 2020-03-16 12:37:00
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['len']='255';
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['audited']=true;
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['massupdate']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['importable']='false';
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['merge_filter']='disabled';
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['unified_search']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['required']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['duplicate_merge']='disabled';
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['formula']='concat($wp_name_c," ",$deliverable_text_c)';
$dictionary['M03_Work_Product_Deliverable']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_reason_for_amendment_c.php

 // created: 2020-05-21 18:10:41
$dictionary['M03_Work_Product_Deliverable']['fields']['reason_for_amendment_c']['labelValue']='Reason for Amendment';
$dictionary['M03_Work_Product_Deliverable']['fields']['reason_for_amendment_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['reason_for_amendment_c']['visibility_grid']=array (
  'trigger' => 'deliverable_c',
  'values' => 
  array (
    'Choose One' => 
    array (
    ),
    'QA Data Review' => 
    array (
    ),
    'Critical Phase Audit' => 
    array (
    ),
    'Protocol Development' => 
    array (
    ),
    'Final Report' => 
    array (
    ),
    'Final Report Amendment' => 
    array (
      0 => '',
      1 => 'Typographical Error',
      2 => 'Data Entry Error',
      3 => 'Error in Final Report PDF',
      4 => 'Sponsor Request',
      5 => 'Sponsor Request for Additional Analysis',
    ),
    'Gross Pathology Report' => 
    array (
    ),
    'Histopathology Notes' => 
    array (
    ),
    'Histopathology Report' => 
    array (
    ),
    'Histopathology and Histomorphometry Report' => 
    array (
    ),
    'Animal Health Report' => 
    array (
    ),
    'In Life Report' => 
    array (
    ),
    'Tissue Shipping' => 
    array (
    ),
    'Tissue Trimming' => 
    array (
    ),
    'SEM Prep' => 
    array (
    ),
    'SEM' => 
    array (
    ),
    'SEM Report' => 
    array (
    ),
    'Slide Completion' => 
    array (
    ),
    'Recuts' => 
    array (
    ),
    'Slide Shipping' => 
    array (
    ),
    'Histomorphometry' => 
    array (
    ),
    'Tissue Receipt' => 
    array (
    ),
    'Faxitron' => 
    array (
    ),
    'Interim Report' => 
    array (
    ),
    'Interim Gross Pathology Report' => 
    array (
    ),
    'Interim Histopathology Report' => 
    array (
    ),
    'Interim Animal Health Report' => 
    array (
    ),
    'Interim In Life Report' => 
    array (
    ),
    'Pharmacology Data Summary' => 
    array (
    ),
    'Waiting on external test site' => 
    array (
    ),
    'Bioanalytical Method Development' => 
    array (
    ),
    'Bioanalytical Validation Report' => 
    array (
    ),
    'Bioanalytical Specimen Analysis' => 
    array (
    ),
    'Bioanalytical Sample Prep' => 
    array (
    ),
    'Bioanalytical Stability Analysis' => 
    array (
    ),
    'Bioanalytical Data Table' => 
    array (
    ),
    'Analytical Report' => 
    array (
    ),
    'Bioanalytical Protocol Dvelopment' => 
    array (
    ),
    '' => 
    array (
    ),
    'Abnormal Extract' => 
    array (
    ),
    'Analytical ExhaustiveExaggerated Extraction' => 
    array (
    ),
    'Analytical Headspace Analysis' => 
    array (
    ),
    'Analytical Interim Validation Report' => 
    array (
    ),
    'Analytical Method' => 
    array (
    ),
    'Analytical Nickel Ion Release Extraction' => 
    array (
    ),
    'Analytical Pre testing' => 
    array (
    ),
    'BC Abnormal Study Article' => 
    array (
    ),
    'Analytical Simulated Use Extraction' => 
    array (
    ),
    'Biological Evaluation Plan' => 
    array (
    ),
    'Chemical and Physical Characterization of Particulates' => 
    array (
    ),
    'Contributing Scientist Report Amendment' => 
    array (
    ),
    'Decalcification' => 
    array (
    ),
    'Frozen Slide Completion' => 
    array (
    ),
    'GLP Discontinuation Report' => 
    array (
    ),
    'Gross Morphometry' => 
    array (
    ),
    'Histology Controls' => 
    array (
    ),
    'Histopathology Methods Report' => 
    array (
    ),
    'Paraffin Slide Completion' => 
    array (
    ),
    'Pathology Unknown' => 
    array (
    ),
    'EXAKT Slide Completion' => 
    array (
    ),
    'Plastic Microtome Slide Completion' => 
    array (
    ),
    'Regulatory Response' => 
    array (
    ),
    'Risk Assessment Report' => 
    array (
    ),
    'Risk Assessment Report Review' => 
    array (
    ),
    'SEND Report' => 
    array (
    ),
    'Slide Scanning' => 
    array (
    ),
    'SpecimenSample Disposition' => 
    array (
    ),
    'Sponsor Contracted Contributing Scientist Report' => 
    array (
    ),
    'Statistical Summary' => 
    array (
    ),
    'Statistics Report' => 
    array (
    ),
    'Shipping Request' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_deliverable_c.php

 // created: 2020-05-21 18:08:15
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_c']['labelValue']='Deliverable';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_pathologist_workload_c.php

 // created: 2020-05-21 18:09:49
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_workload_c']['labelValue']='Pathologist Workload';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_workload_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_workload_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_deliverable_lead_time_c.php

 // created: 2020-06-12 17:16:13
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['labelValue']='Deliverable Lead Time';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['formula']='ifElse(or(equal($due_date_c,""),equal(related($m03_work_product_m03_work_product_deliverable_1,"last_procedure_c"),"")),"",subtract(daysUntil($due_date_c),daysUntil(related($m03_work_product_m03_work_product_deliverable_1,"last_procedure_c"))))';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['enforced']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_lead_time_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_filename.php

 // created: 2020-10-16 16:09:35
/* $dictionary['M03_Work_Product_Deliverable']['fields']['filename']['audited']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['massupdate']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['comments']='File name associated with the note (attachment)';
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['importable']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['duplicate_merge']='enabled';
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['duplicate_merge_dom_value']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['merge_filter']='disabled';
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['unified_search']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['calculated']=false;
$dictionary['M03_Work_Product_Deliverable']['fields']['filename']['dependency']='equal($deliverable_c,"SpecimenSample Disposition")'; */
$GLOBALS['dictionary']['M03_Work_Product_Deliverable']['fields']['filename'] = array (
    'name' => 'filename',
    'vname' => 'LBL_FILENAME',
    'type' => 'file',
    'dbType' => 'varchar',
    'len' => '255',
    'comments' => 'File name associated with the note (attachment)',
    'reportable' => true,
    'comment' => 'File name associated with the note (attachment)',
    'importable' => false,
    'dependency' => 'equal($deliverable_c,"SpecimenSample Disposition")',
	'audited' => false,
	'massupdate' => false,
	'unified_search' => false,
	'calculated' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '1',
	'merge_filter' => 'disabled',
);
 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_file_mime_type.php

 // created: 2020-10-16 16:09:35
$GLOBALS['dictionary']['M03_Work_Product_Deliverable']['fields']['file_mime_type'] = array(

     'name' => 'file_mime_type',
     'vname' => 'LBL_FILE_MIME_TYPE',
     'type' => 'varchar',
     'len' => '100',
     'comment' => 'Attachment MIME type',
     'importable' => false,
);

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_file_url.php

 // created: 2020-10-16 16:09:35
$GLOBALS['dictionary']['M03_Work_Product_Deliverable']['fields']['file_url'] = array (

     'name' => 'file_url',
     'vname' => 'LBL_FILE_URL',
     'type' => 'varchar',
     'source' => 'non-db',
     'reportable' => false,
     'comment' => 'Path to file (can be URL)',
     'importable' => false,

);

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_for_bc_goal_internal_draft_c.php

 // created: 2021-02-18 08:52:44
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['labelValue']='Internal BC Draft Due Date Goal';
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['formula']='addDays($final_due_date_c,-14)';
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['enforced']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['dependency']='not(isInList($deliverable_c,createList("Animal Selection","SpecimenSample Disposition")))';
$dictionary['M03_Work_Product_Deliverable']['fields']['for_bc_goal_internal_draft_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_due_to_complete_timeframe_c.php

 // created: 2021-02-25 05:55:20
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['labelValue']='Due to Complete Timeframe (Days)';
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['calculated']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['formula']='ifElse(or(equal($final_due_date_c,""),equal($deliverable_completed_date_c,"")),"",subtract(daysUntil($final_due_date_c),daysUntil($deliverable_completed_date_c)))';
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['enforced']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['due_to_complete_timeframe_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_standard_unstained_slides_c.php

 // created: 2021-04-06 09:06:39
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_unstained_slides_c']['labelValue']='# of Standard Unstained Slides';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_unstained_slides_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_unstained_slides_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_unstained_slides_c']['dependency']='isInList($deliverable_c,createList("Paraffin Slide Completion","Plastic Microtome Slide Completion","Frozen Slide Completion"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_unstained_slides_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_unstained_slides_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_slide_type_c.php

 // created: 2021-04-06 09:19:53
$dictionary['M03_Work_Product_Deliverable']['fields']['slide_type_c']['labelValue']='Slide Type';
$dictionary['M03_Work_Product_Deliverable']['fields']['slide_type_c']['dependency']='equal($deliverable_c,"Slide Scanning")';
$dictionary['M03_Work_Product_Deliverable']['fields']['slide_type_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['slide_type_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['slide_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_off_test_to_final_report_c.php

 // created: 2021-06-17 08:50:23
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['labelValue']='Off Test to Final Report';
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['calculated']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['formula']='ifElse(and(equal($deliverable_c,"Final Report"),not(equal($deliverable_completed_date_c,"")),not(equal(related($m03_work_product_m03_work_product_deliverable_1,"last_procedure_c"),""))),subtract(daysUntil($deliverable_completed_date_c),daysUntil(related($m03_work_product_m03_work_product_deliverable_1,"last_procedure_c"))),"")';
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['enforced']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['off_test_to_final_report_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_due_date_c.php

 // created: 2021-07-22 08:58:52
$dictionary['M03_Work_Product_Deliverable']['fields']['due_date_c']['labelValue']='Draft Due Date';
$dictionary['M03_Work_Product_Deliverable']['fields']['due_date_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['due_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_change_by_c.php

 // created: 2021-07-29 09:29:54
$dictionary['M03_Work_Product_Deliverable']['fields']['change_by_c']['labelValue']='Change By';
$dictionary['M03_Work_Product_Deliverable']['fields']['change_by_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['change_by_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['change_by_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['change_by_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['change_by_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_change_timestamp_c.php

 // created: 2021-07-29 09:31:12
$dictionary['M03_Work_Product_Deliverable']['fields']['change_timestamp_c']['labelValue']='Change Timestamp';
$dictionary['M03_Work_Product_Deliverable']['fields']['change_timestamp_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['change_timestamp_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['change_timestamp_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['change_timestamp_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_pathologist_participation_c.php

 // created: 2021-08-12 08:58:03
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_participation_c']['labelValue']='Pathologist Participation at Necropsy Needed';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_participation_c']['dependency']='isInList($deliverable_c,createList("Histopathology Report2","Interim Histopathology Report2","Histopathology Report","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_participation_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_participation_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_participation_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_minutes_per_slide_c.php

 // created: 2021-08-12 09:03:05
$dictionary['M03_Work_Product_Deliverable']['fields']['minutes_per_slide_c']['labelValue']='Minutes per Slide';
$dictionary['M03_Work_Product_Deliverable']['fields']['minutes_per_slide_c']['dependency']='isInList($deliverable_c,createList("Histopathology Report2","Histopathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['minutes_per_slide_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['minutes_per_slide_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['minutes_per_slide_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_total_number_of_slides_c.php

 // created: 2021-08-12 09:04:23
$dictionary['M03_Work_Product_Deliverable']['fields']['total_number_of_slides_c']['labelValue']='Total Number of Slides';
$dictionary['M03_Work_Product_Deliverable']['fields']['total_number_of_slides_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['total_number_of_slides_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['total_number_of_slides_c']['dependency']='isInList($deliverable_c,createList("Histopathology Report2","Histopathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['total_number_of_slides_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['total_number_of_slides_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_planned_bd_to_draft_c.php

 // created: 2021-08-12 09:07:15
$dictionary['M03_Work_Product_Deliverable']['fields']['planned_bd_to_draft_c']['labelValue']='Planned Business Days to Draft Report';
$dictionary['M03_Work_Product_Deliverable']['fields']['planned_bd_to_draft_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['planned_bd_to_draft_c']['dependency']='isInList($deliverable_c,createList("Gross Pathology Report","Histopathology Report2","Histopathology Report","Interim Gross Pathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['planned_bd_to_draft_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['planned_bd_to_draft_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_actual_bc_to_complete_draft_c.php

 // created: 2021-08-12 09:09:03
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_bc_to_complete_draft_c']['labelValue']='Actual Business Days to Complete Draft';
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_bc_to_complete_draft_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_bc_to_complete_draft_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_bc_to_complete_draft_c']['dependency']='isInList($deliverable_c,createList("Gross Pathology Report","Histopathology Report2","Histopathology Report","Interim Gross Pathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_bc_to_complete_draft_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['actual_bc_to_complete_draft_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_report_writing_days_c.php

 // created: 2021-08-12 09:11:25
$dictionary['M03_Work_Product_Deliverable']['fields']['report_writing_days_c']['labelValue']='Report Writing Days';
$dictionary['M03_Work_Product_Deliverable']['fields']['report_writing_days_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['report_writing_days_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['report_writing_days_c']['dependency']='isInList($deliverable_c,createList("Histopathology Report2","Histopathology Report","Interim Gross Pathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['report_writing_days_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['report_writing_days_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_pathologist_on_call_c.php

 // created: 2021-08-12 09:14:13
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_on_call_c']['labelValue']='Pathologist on Call During Reporting Window';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_on_call_c']['dependency']='isInList($deliverable_c,createList("Histopathology Report2","Histopathology Report","Interim Gross Pathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_on_call_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_on_call_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['pathologist_on_call_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_standard_he_slides_c.php

 // created: 2021-08-12 11:46:51
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_he_slides_c']['labelValue']='# of Standard H&E Slides';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_he_slides_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_he_slides_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_he_slides_c']['dependency']='isInList($deliverable_c,createList("Paraffin Slide Completion","EXAKT Slide Completion","Plastic Microtome Slide Completion","Frozen Slide Completion","Slide Scanning","Histopathology Report2","Interim Histopathology Report2","Histopathology Report","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_he_slides_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_he_slides_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_standard_special_stain_c.php

 // created: 2021-08-12 11:48:03
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_special_stain_c']['labelValue']='# of Standard Special Stain Slides';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_special_stain_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_special_stain_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_special_stain_c']['dependency']='isInList($deliverable_c,createList("Paraffin Slide Completion","EXAKT Slide Completion","Plastic Microtome Slide Completion","Frozen Slide Completion","Slide Scanning","Histopathology Report2","Interim Histopathology Report2","Histopathology Report","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_special_stain_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['standard_special_stain_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_large_he_slides_c.php

 // created: 2021-08-12 11:49:34
$dictionary['M03_Work_Product_Deliverable']['fields']['large_he_slides_c']['labelValue']='# of Large H&E Slides';
$dictionary['M03_Work_Product_Deliverable']['fields']['large_he_slides_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['large_he_slides_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['large_he_slides_c']['dependency']='isInList($deliverable_c,createList("Paraffin Slide Completion","EXAKT Slide Completion","Plastic Microtome Slide Completion","Frozen Slide Completion","Slide Scanning","Histopathology Report2","Interim Histopathology Report2","Histopathology Report","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['large_he_slides_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['large_he_slides_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_large_special_stain_c.php

 // created: 2021-08-12 11:51:04
$dictionary['M03_Work_Product_Deliverable']['fields']['large_special_stain_c']['labelValue']='# of Large Special Stain Slides';
$dictionary['M03_Work_Product_Deliverable']['fields']['large_special_stain_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['large_special_stain_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['large_special_stain_c']['dependency']='isInList($deliverable_c,createList("Paraffin Slide Completion","EXAKT Slide Completion","Plastic Microtome Slide Completion","Frozen Slide Completion","Slide Scanning","Histopathology Report2","Interim Histopathology Report2","Histopathology Report","Interim Histopathology Report","Histopathology Notes"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['large_special_stain_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['large_special_stain_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_aps_target_due_date_c.php

 // created: 2021-09-14 08:56:02
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['labelValue']='Internal BC Final Due Date Goal';
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['formula']='addDays($final_due_date_c,-3)';
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['enforced']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['dependency']='not(isInList($deliverable_c,createList("Animal Selection","SpecimenSample Disposition")))';
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['aps_target_due_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_within_timeframe_c.php

 // created: 2021-09-14 08:58:15
$dictionary['M03_Work_Product_Deliverable']['fields']['within_timeframe_c']['labelValue']='Within Timeframe';
$dictionary['M03_Work_Product_Deliverable']['fields']['within_timeframe_c']['dependency']='equal(related($m03_work_product_m03_work_product_deliverable_1,"functional_area_c"),"Standard Biocompatibility")';
$dictionary['M03_Work_Product_Deliverable']['fields']['within_timeframe_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['within_timeframe_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['within_timeframe_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_attn_to_name_c.php

 // created: 2021-10-21 05:53:52
$dictionary['M03_Work_Product_Deliverable']['fields']['attn_to_name_c']['labelValue']='Attn. To Name';
$dictionary['M03_Work_Product_Deliverable']['fields']['attn_to_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['attn_to_name_c']['enforced']='false';
$dictionary['M03_Work_Product_Deliverable']['fields']['attn_to_name_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['attn_to_name_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['attn_to_name_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_address_street_c.php

 // created: 2021-10-21 06:55:28
$dictionary['M03_Work_Product_Deliverable']['fields']['address_street_c']['labelValue']='Street';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_street_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['address_street_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_street_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_street_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_street_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_address_city_c.php

 // created: 2021-10-21 06:57:08
$dictionary['M03_Work_Product_Deliverable']['fields']['address_city_c']['labelValue']='City';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_city_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['address_city_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_city_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_city_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_city_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_address_state_c.php

 // created: 2021-10-21 06:58:56
$dictionary['M03_Work_Product_Deliverable']['fields']['address_state_c']['labelValue']='State';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_state_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['address_state_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_state_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_state_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_state_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_address_postalcode_c.php

 // created: 2021-10-21 07:00:54
$dictionary['M03_Work_Product_Deliverable']['fields']['address_postalcode_c']['labelValue']='Postal Code';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_postalcode_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['address_postalcode_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_postalcode_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_postalcode_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_postalcode_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_address_country_c.php

 // created: 2021-10-21 07:06:54
$dictionary['M03_Work_Product_Deliverable']['fields']['address_country_c']['labelValue']='Country';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_country_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['address_country_c']['enforced']='false';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_country_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_country_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_country_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_phone_number_c.php

 // created: 2021-10-21 07:08:03
$dictionary['M03_Work_Product_Deliverable']['fields']['phone_number_c']['labelValue']='Phone Number';
$dictionary['M03_Work_Product_Deliverable']['fields']['phone_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['phone_number_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['phone_number_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['phone_number_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['phone_number_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_address_confirmed_c.php

 // created: 2021-10-21 07:09:35
$dictionary['M03_Work_Product_Deliverable']['fields']['address_confirmed_c']['labelValue']='Address Confirmed';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_confirmed_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_confirmed_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_confirmed_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['address_confirmed_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_receiving_party_expecting_c.php

 // created: 2021-10-21 07:13:02
$dictionary['M03_Work_Product_Deliverable']['fields']['receiving_party_expecting_c']['labelValue']='Receiving Party Expecting Shipment';
$dictionary['M03_Work_Product_Deliverable']['fields']['receiving_party_expecting_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['receiving_party_expecting_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['receiving_party_expecting_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['receiving_party_expecting_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_faxitron_required_c.php

 // created: 2021-10-21 07:18:20
$dictionary['M03_Work_Product_Deliverable']['fields']['faxitron_required_c']['labelValue']='Faxitron Required Prior to Shipment';
$dictionary['M03_Work_Product_Deliverable']['fields']['faxitron_required_c']['dependency']='equal($type_3_c,"Shipping")';
$dictionary['M03_Work_Product_Deliverable']['fields']['faxitron_required_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['faxitron_required_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['faxitron_required_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_contact_id1_c.php

 // created: 2021-11-16 09:54:11

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_author_report_name_c.php

 // created: 2021-11-16 09:54:11
$dictionary['M03_Work_Product_Deliverable']['fields']['author_report_name_c']['labelValue']='Author Report Name';
$dictionary['M03_Work_Product_Deliverable']['fields']['author_report_name_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['author_report_name_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['author_report_name_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/m03_work_product_deliverable_ii_inventory_item_1_M03_Work_Product_Deliverable.php

// created: 2022-02-22 07:44:29
$dictionary["M03_Work_Product_Deliverable"]["fields"]["m03_work_product_deliverable_ii_inventory_item_1"] = array (
  'name' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'id_name' => 'm03_work_p7e14verable_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/m03_work_product_deliverable_ic_inventory_collection_1_M03_Work_Product_Deliverable.php

// created: 2022-02-22 07:48:02
$dictionary["M03_Work_Product_Deliverable"]["fields"]["m03_work_product_deliverable_ic_inventory_collection_1"] = array (
  'name' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'source' => 'non-db',
  'module' => 'IC_Inventory_Collection',
  'bean_name' => 'IC_Inventory_Collection',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'id_name' => 'm03_work_p2592verable_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_deliverable_com_date_test_c.php

 // created: 2022-03-09 09:03:53
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_com_date_test_c']['labelValue']='Deliverable Com Date Test';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_com_date_test_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_com_date_test_c']['required_formula']='equal($deliverable_status_c,"Completed")';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_com_date_test_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_shipping_delivery_c.php

 // created: 2022-03-23 05:33:16
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_delivery_c']['labelValue']='Shipping Delivery';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_delivery_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_delivery_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_delivery_c']['visibility_grid']=array (
  'trigger' => 'shipping_company_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Fed Ex' => 
    array (
      0 => '',
      1 => 'International Priority',
      2 => 'First Priority Overnight',
      3 => 'Priority Overnight',
      4 => '2 Day',
      5 => 'Ground',
    ),
    'On Time' => 
    array (
      0 => '',
      1 => 'Direct',
      2 => 'Same Day',
      3 => '1 HR',
      4 => '2 HR',
      5 => '3 HR',
    ),
    'World Courier' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_wp_name_c.php

 // created: 2022-03-23 05:34:45
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['labelValue']='WP Name';
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['formula']='related($m03_work_product_m03_work_product_deliverable_1,"name")';
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['enforced']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['wp_name_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_deliverable_text_c.php

 // created: 2022-03-23 05:36:09
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['labelValue']='Deliverable Text';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['formula']='getDropdownValue("deliverable_list",$deliverable_c)';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['enforced']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_text_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_shipping_conditions_c.php

 // created: 2022-03-23 05:37:27
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_conditions_c']['labelValue']='Shipping Conditions';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_conditions_c']['dependency']='or(isInList($type_3_c,createList("Shipping","Archive")),equal($deliverable_c,"Shipping Request"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_conditions_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_conditions_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_type_3_c.php

 // created: 2022-03-23 05:40:48
$dictionary['M03_Work_Product_Deliverable']['fields']['type_3_c']['labelValue']='Type';
$dictionary['M03_Work_Product_Deliverable']['fields']['type_3_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['type_3_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['type_3_c']['visibility_grid']=array (
  'trigger' => 'deliverable_c',
  'values' => 
  array (
    'Choose One' => 
    array (
    ),
    'Abnormal Extract' => 
    array (
    ),
    'Bioanalytical Data Table' => 
    array (
    ),
    'Analytical ExhaustiveExaggerated Extraction' => 
    array (
    ),
    'Analytical Report' => 
    array (
    ),
    'Analytical Headspace Analysis' => 
    array (
    ),
    'Analytical Interim Validation Report' => 
    array (
    ),
    'Bioanalytical Method Development' => 
    array (
    ),
    'Analytical Nickel Ion Release Extraction' => 
    array (
    ),
    'Analytical Pre testing' => 
    array (
    ),
    'BC Abnormal Study Article' => 
    array (
    ),
    'Bioanalytical Protocol Dvelopment' => 
    array (
    ),
    'Bioanalytical Specimen Analysis' => 
    array (
    ),
    'Bioanalytical Sample Prep' => 
    array (
    ),
    'Analytical Simulated Use Extraction' => 
    array (
    ),
    'Bioanalytical Stability Analysis' => 
    array (
    ),
    'Bioanalytical Validation Report' => 
    array (
    ),
    'Animal Health Report' => 
    array (
    ),
    'Biological Evaluation Plan' => 
    array (
    ),
    'Chemical and Physical Characterization of Particulates' => 
    array (
    ),
    'Critical Phase Audit' => 
    array (
    ),
    'Faxitron' => 
    array (
    ),
    'Final Report' => 
    array (
    ),
    'Final Report Amendment' => 
    array (
    ),
    'Frozen Slide Completion' => 
    array (
    ),
    'GLP Discontinuation Report' => 
    array (
    ),
    'Gross Morphometry' => 
    array (
    ),
    'Gross Pathology Report' => 
    array (
    ),
    'Histology Controls' => 
    array (
    ),
    'Histomorphometry' => 
    array (
    ),
    'Histopathology Methods Report' => 
    array (
    ),
    'In Life Report' => 
    array (
    ),
    'Interim Animal Health Report' => 
    array (
    ),
    'Interim Gross Pathology Report' => 
    array (
    ),
    'Interim Histopathology Report' => 
    array (
    ),
    'Interim In Life Report' => 
    array (
    ),
    'Interim Report' => 
    array (
    ),
    'Paraffin Slide Completion' => 
    array (
    ),
    'Pathology Unknown' => 
    array (
    ),
    'Histopathology Notes' => 
    array (
    ),
    'Histopathology Report' => 
    array (
    ),
    'Pharmacology Data Summary' => 
    array (
    ),
    'EXAKT Slide Completion' => 
    array (
    ),
    'Plastic Microtome Slide Completion' => 
    array (
    ),
    'Protocol Development' => 
    array (
    ),
    'QA Data Review' => 
    array (
    ),
    'Recuts' => 
    array (
    ),
    'Regulatory Response' => 
    array (
    ),
    'Risk Assessment Report' => 
    array (
    ),
    'Risk Assessment Report Review' => 
    array (
    ),
    'SEM' => 
    array (
    ),
    'Slide Scanning' => 
    array (
    ),
    'SpecimenSample Disposition' => 
    array (
      0 => '',
      1 => 'Shipping',
      2 => 'Transfer',
      3 => 'Discard',
      4 => 'Archive',
    ),
    'Statistical Summary' => 
    array (
    ),
    'Statistics Report' => 
    array (
    ),
    'Tissue Receipt' => 
    array (
    ),
    'Tissue Trimming' => 
    array (
    ),
    'Waiting on external test site' => 
    array (
    ),
    'Histopathology and Histomorphometry Report' => 
    array (
    ),
    'SEM Prep' => 
    array (
    ),
    'SEM Report' => 
    array (
    ),
    'Shipping Request' => 
    array (
    ),
    'Slide Completion' => 
    array (
    ),
    'Slide Shipping' => 
    array (
    ),
    'Tissue Shipping' => 
    array (
    ),
    '' => 
    array (
    ),
    'Analytical Method' => 
    array (
    ),
    'Contributing Scientist Report Amendment' => 
    array (
    ),
    'Decalcification' => 
    array (
    ),
    'Early DeathTermination Investigation Report' => 
    array (
    ),
    'IACUC Submission' => 
    array (
    ),
    'Pathology Protocol Review' => 
    array (
    ),
    'Pharmacokinetic Report' => 
    array (
    ),
    'SEND Report' => 
    array (
    ),
    'Sponsor Contracted Contributing Scientist Report' => 
    array (
    ),
    'TWT Protocol Review' => 
    array (
    ),
    'TWT Report Review' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_stability_considerations_c.php

 // created: 2022-03-23 05:42:32
$dictionary['M03_Work_Product_Deliverable']['fields']['stability_considerations_c']['labelValue']='Stability Considerations';
$dictionary['M03_Work_Product_Deliverable']['fields']['stability_considerations_c']['dependency']='isInList($type_3_c,createList("Shipping","Transfer","Archive"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['stability_considerations_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['stability_considerations_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_current_location_c.php

 // created: 2022-03-23 05:43:20
$dictionary['M03_Work_Product_Deliverable']['fields']['current_location_c']['labelValue']='Current Location';
$dictionary['M03_Work_Product_Deliverable']['fields']['current_location_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['current_location_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['current_location_c']['dependency']='equal($type_3_c,"Transfer")';
$dictionary['M03_Work_Product_Deliverable']['fields']['current_location_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_datetime_desired_c.php

 // created: 2022-03-23 05:44:43
$dictionary['M03_Work_Product_Deliverable']['fields']['datetime_desired_c']['labelValue']='Date/Time Desired';
$dictionary['M03_Work_Product_Deliverable']['fields']['datetime_desired_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['datetime_desired_c']['dependency']='equal($type_3_c,"Transfer")';
$dictionary['M03_Work_Product_Deliverable']['fields']['datetime_desired_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_transfer_to_location_c.php

 // created: 2022-03-23 05:45:43
$dictionary['M03_Work_Product_Deliverable']['fields']['transfer_to_location_c']['labelValue']='Transfer to Location';
$dictionary['M03_Work_Product_Deliverable']['fields']['transfer_to_location_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['transfer_to_location_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['transfer_to_location_c']['dependency']='equal($type_3_c,"Transfer")';
$dictionary['M03_Work_Product_Deliverable']['fields']['transfer_to_location_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_hazardous_contents_c.php

 // created: 2022-03-23 05:49:09
$dictionary['M03_Work_Product_Deliverable']['fields']['hazardous_contents_c']['labelValue']='Hazardous Contents';
$dictionary['M03_Work_Product_Deliverable']['fields']['hazardous_contents_c']['dependency']='or(isInList($type_3_c,createList("Shipping","Archive")),equal($deliverable_c,"Shipping Request"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['hazardous_contents_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['hazardous_contents_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_shipping_company_c.php

 // created: 2022-03-23 05:50:17
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_company_c']['labelValue']='Shipping Company';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_company_c']['dependency']='or(isInList($type_3_c,createList("Shipping","Archive")),equal($deliverable_c,"Shipping Request"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_company_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['shipping_company_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_type_2_c.php

 // created: 2022-03-23 05:51:00
$dictionary['M03_Work_Product_Deliverable']['fields']['type_2_c']['labelValue']='Subtype';
$dictionary['M03_Work_Product_Deliverable']['fields']['type_2_c']['dependency']='or(isInList($type_3_c,createList("Shipping","Archive")),equal($deliverable_c,"Shipping Request"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['type_2_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['type_2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_analyze_by_date_c.php

 // created: 2022-03-23 05:51:50
$dictionary['M03_Work_Product_Deliverable']['fields']['analyze_by_date_c']['labelValue']='Analyze-by Date';
$dictionary['M03_Work_Product_Deliverable']['fields']['analyze_by_date_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['analyze_by_date_c']['dependency']='equal($stability_considerations_c,"Yes")';
$dictionary['M03_Work_Product_Deliverable']['fields']['analyze_by_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_last_procedure_c.php

 // created: 2022-03-24 11:22:59
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['labelValue']='Last Procedure';
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['calculated']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['formula']='related($m03_work_product_m03_work_product_deliverable_1,"last_procedure_c")';
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['enforced']='true';
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['last_procedure_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_same_day_prep_c.php

 // created: 2022-05-24 08:45:17
$dictionary['M03_Work_Product_Deliverable']['fields']['same_day_prep_c']['labelValue']='Same Day Prep Required?';
$dictionary['M03_Work_Product_Deliverable']['fields']['same_day_prep_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['same_day_prep_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['same_day_prep_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['same_day_prep_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_neat_preparation_c.php

 // created: 2022-05-24 08:48:33
$dictionary['M03_Work_Product_Deliverable']['fields']['neat_preparation_c']['labelValue']='Neat Preparation?';
$dictionary['M03_Work_Product_Deliverable']['fields']['neat_preparation_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['neat_preparation_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['neat_preparation_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['neat_preparation_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_hdpe_eto_c.php

 // created: 2022-05-24 08:51:14
$dictionary['M03_Work_Product_Deliverable']['fields']['hdpe_eto_c']['labelValue']='HDPE Comparison ETO Required?';
$dictionary['M03_Work_Product_Deliverable']['fields']['hdpe_eto_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['hdpe_eto_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['hdpe_eto_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['hdpe_eto_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_number_test_implants_c.php

 // created: 2022-05-24 08:53:23
$dictionary['M03_Work_Product_Deliverable']['fields']['number_test_implants_c']['labelValue']='Number of implant articles needed per procedure date (Test)';
$dictionary['M03_Work_Product_Deliverable']['fields']['number_test_implants_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['number_test_implants_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['number_test_implants_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['number_test_implants_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['number_test_implants_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_sample_prep_time_estimate_c.php

 // created: 2022-05-24 08:56:32
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_time_estimate_c']['labelValue']='Sample Prep Time Estimate (min)';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_time_estimate_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_time_estimate_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_time_estimate_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_time_estimate_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_time_estimate_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_additional_supplies_required_c.php

 // created: 2022-05-24 08:59:30
$dictionary['M03_Work_Product_Deliverable']['fields']['additional_supplies_required_c']['labelValue']='Additional Supplies Required for Implant';
$dictionary['M03_Work_Product_Deliverable']['fields']['additional_supplies_required_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['additional_supplies_required_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['additional_supplies_required_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['additional_supplies_required_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['additional_supplies_required_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_sample_prep_design_notes_c.php

 // created: 2022-05-24 09:03:05
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_design_notes_c']['labelValue']='Sample Prep Design Notes';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_design_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_design_notes_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_design_notes_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_design_notes_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['sample_prep_design_notes_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_number_control_implants_c.php

 // created: 2022-05-24 09:11:22
$dictionary['M03_Work_Product_Deliverable']['fields']['number_control_implants_c']['labelValue']='Number of implant articles needed per procedure date (Control)';
$dictionary['M03_Work_Product_Deliverable']['fields']['number_control_implants_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Deliverable']['fields']['number_control_implants_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['number_control_implants_c']['dependency']='equal($deliverable_c,"Sample Prep Design")';
$dictionary['M03_Work_Product_Deliverable']['fields']['number_control_implants_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['number_control_implants_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/nameFieldCustomization.php


$dictionary['M03_Work_Product_Deliverable']['fields']['name']['required'] = 'false';
//$dictionary['M03_Work_Product_Deliverable']['fields']['name']['readonly'] = 'true';

//  Other Fieldss
$dictionary['M03_Work_Product_Deliverable']['fields']['m03_work_product_m03_work_product_deliverable_1_name']['required'] = 'true';
//$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_status_c']['required'] = 'true';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_timeline_type_c.php

 // created: 2022-05-24 10:13:23
$dictionary['M03_Work_Product_Deliverable']['fields']['timeline_type_c']['labelValue']='Timeline';
$dictionary['M03_Work_Product_Deliverable']['fields']['timeline_type_c']['dependency']='not(equal($deliverable_c,"Sample Prep Design"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['timeline_type_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['timeline_type_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['timeline_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_work_type_c.php

 // created: 2022-05-24 10:15:35
$dictionary['M03_Work_Product_Deliverable']['fields']['work_type_c']['labelValue']='Work Type';
$dictionary['M03_Work_Product_Deliverable']['fields']['work_type_c']['dependency']='not(equal($deliverable_c,"Sample Prep Design"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['work_type_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['work_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_internal_final_due_date_c.php

 // created: 2022-05-24 10:17:30
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['labelValue']='Internal Final Due Date';
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['calculated']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['formula']='addDays($final_due_date_c,-1)';
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['enforced']='1';
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['dependency']='not(isInList($deliverable_c,createList("Animal Selection","Animal Selection Populate TSD","Animal Selection Procedure Use","Sample Prep Design")))';
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['internal_final_due_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_bc_send_draft_report_c.php

 // created: 2022-05-24 10:18:11
$dictionary['M03_Work_Product_Deliverable']['fields']['bc_send_draft_report_c']['labelValue']='Send BC Draft Report?';
$dictionary['M03_Work_Product_Deliverable']['fields']['bc_send_draft_report_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['bc_send_draft_report_c']['dependency']='not(equal($deliverable_c,"Sample Prep Design"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['bc_send_draft_report_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_redlines_c.php

 // created: 2022-05-24 10:19:03
$dictionary['M03_Work_Product_Deliverable']['fields']['redlines_c']['labelValue']='Audit After Redlines?';
$dictionary['M03_Work_Product_Deliverable']['fields']['redlines_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['redlines_c']['dependency']='not(equal($deliverable_c,"Sample Prep Design"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['redlines_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_draft_deliverable_sent_date_c.php

 // created: 2022-05-24 10:19:50
$dictionary['M03_Work_Product_Deliverable']['fields']['draft_deliverable_sent_date_c']['labelValue']='Draft Deliverable Sent Date';
$dictionary['M03_Work_Product_Deliverable']['fields']['draft_deliverable_sent_date_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['draft_deliverable_sent_date_c']['dependency']='not(equal($deliverable_c,"Sample Prep Design"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['draft_deliverable_sent_date_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['draft_deliverable_sent_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_overdue_risk_level_c.php

 // created: 2022-05-24 10:21:02
$dictionary['M03_Work_Product_Deliverable']['fields']['overdue_risk_level_c']['labelValue']='Overdue Risk Level';
$dictionary['M03_Work_Product_Deliverable']['fields']['overdue_risk_level_c']['dependency']='not(equal($deliverable_c,"Sample Prep Design"))';
$dictionary['M03_Work_Product_Deliverable']['fields']['overdue_risk_level_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['overdue_risk_level_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_deliverable_completed_date_c.php

 // created: 2022-05-24 12:02:55
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_completed_date_c']['labelValue']='Deliverable Completed Date';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_completed_date_c']['enforced']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_completed_date_c']['dependency']='equal($deliverable_status_c,"Completed")';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_completed_date_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_completed_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_deliverable_status_c.php

 // created: 2022-10-25 12:44:52
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_status_c']['labelValue']='Deliverable Status';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_status_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_status_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_status_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['deliverable_status_c']['visibility_grid']=array (
  'trigger' => 'deliverable_c',
  'values' => 
  array (
    'Choose One' => 
    array (
    ),
    'Abnormal Extract' => 
    array (
      0 => 'In Progress',
      1 => 'Completed',
    ),
    'Bioanalytical Data Table' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Analytical ExhaustiveExaggerated Extraction' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Analytical Report' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sent to Study DirectorPrinciple Investigator',
      9 => 'Sponsor Retracted',
      10 => 'None',
      11 => 'Not Performed',
      12 => 'Completed Account on Hold',
    ),
    'Analytical Headspace Analysis' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Analytical Interim Validation Report' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sent to Study DirectorPrinciple Investigator',
      9 => 'Sponsor Retracted',
      10 => 'None',
      11 => 'Not Performed',
      12 => 'Completed Account on Hold',
    ),
    'Bioanalytical Method Development' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Analytical Nickel Ion Release Extraction' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Analytical Pre testing' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Bioanalytical Protocol Dvelopment' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Bioanalytical Specimen Analysis' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Bioanalytical Sample Prep' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Analytical Simulated Use Extraction' => 
    array (
      0 => '',
      1 => 'None',
      2 => 'In Progress',
      3 => 'In Data Review',
      4 => 'Completed',
      5 => 'Not Performed',
    ),
    'Bioanalytical Stability Analysis' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Bioanalytical Validation Report' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sent to Study DirectorPrinciple Investigator',
      9 => 'Sponsor Retracted',
      10 => 'None',
      11 => 'Not Performed',
      12 => 'Completed Account on Hold',
    ),
    'Animal Health Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Biological Evaluation Plan' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Critical Phase Audit' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Faxitron' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Final Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
      11 => 'Ready for Study Director',
    ),
    'Final Report Amendment' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
      11 => 'Ready for Study Director',
    ),
    'Frozen Slide Completion' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'GLP Discontinuation Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
      12 => 'Ready for Study Director',
    ),
    'Gross Morphometry' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Ready for Pathologist',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Gross Pathology Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Ready for Pathologist',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sent to Study DirectorPrinciple Investigator',
      9 => 'Sponsor Retracted',
      10 => 'None',
      11 => 'Not Performed',
      12 => 'Completed Account on Hold',
    ),
    'Histomorphometry' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Ready for Pathologist',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Histopathology Methods Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Ready for Pathologist',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sent to Study DirectorPrinciple Investigator',
      9 => 'Sponsor Retracted',
      10 => 'None',
      11 => 'Not Performed',
      12 => 'Completed Account on Hold',
    ),
    'In Life Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
      12 => 'Ready for Study Director',
    ),
    'Interim Animal Health Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Interim Gross Pathology Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Ready for Pathologist',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sent to Study DirectorPrinciple Investigator',
      9 => 'Sponsor Retracted',
      10 => 'None',
      11 => 'Not Performed',
      12 => 'Completed Account on Hold',
    ),
    'Interim Histopathology Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Ready for Pathologist',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sent to Study DirectorPrinciple Investigator',
      9 => 'Sponsor Retracted',
      10 => 'None',
      11 => 'Not Performed',
      12 => 'Completed Account on Hold',
    ),
    'Interim In Life Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
      12 => 'Ready for Study Director',
    ),
    'Interim Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
      12 => 'Ready for Study Director',
    ),
    'Paraffin Slide Completion' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Pathology Unknown' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Histopathology Notes' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Ready for Pathologist',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Histopathology Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Ready for Pathologist',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sent to Study DirectorPrinciple Investigator',
      9 => 'Sponsor Retracted',
      10 => 'None',
      11 => 'Not Performed',
      12 => 'Completed Account on Hold',
    ),
    'Pharmacology Data Summary' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'EXAKT Slide Completion' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Plastic Microtome Slide Completion' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Protocol Development' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'QA Data Review' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Recuts' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Regulatory Response' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Risk Assessment Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Risk Assessment Report Review' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'SEM' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Slide Scanning' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Slide Shipping' => 
    array (
      0 => 'In Progress',
      1 => 'Waiting on Sponsor',
      2 => 'Waiting_on_Subcontracted_Report',
      3 => 'Waiting On Sponsor Ready to Audit',
      4 => 'Completed',
      5 => 'Overdue',
      6 => 'Sponsor Retracted',
      7 => 'None',
      8 => 'Not Performed',
      9 => 'Completed Account on Hold',
    ),
    'Statistical Summary' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Statistics Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Tissue Receipt' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Tissue Shipping' => 
    array (
      0 => 'In Progress',
      1 => 'Waiting on Sponsor',
      2 => 'Waiting_on_Subcontracted_Report',
      3 => 'Waiting On Sponsor Ready to Audit',
      4 => 'Completed',
      5 => 'Overdue',
      6 => 'Sponsor Retracted',
      7 => 'None',
      8 => 'Not Performed',
      9 => 'Completed Account on Hold',
    ),
    'Tissue Trimming' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Waiting on external test site' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Histopathology and Histomorphometry Report' => 
    array (
      0 => 'In Progress',
      1 => 'Waiting on Sponsor',
      2 => 'Waiting_on_Subcontracted_Report',
      3 => 'Waiting On Sponsor Ready to Audit',
      4 => 'Completed',
      5 => 'Overdue',
      6 => 'Sponsor Retracted',
      7 => 'None',
      8 => 'Not Performed',
      9 => 'Completed Account on Hold',
    ),
    'SEM Prep' => 
    array (
      0 => 'In Progress',
      1 => 'Waiting on Sponsor',
      2 => 'Waiting_on_Subcontracted_Report',
      3 => 'Waiting On Sponsor Ready to Audit',
      4 => 'Completed',
      5 => 'Overdue',
      6 => 'Sponsor Retracted',
      7 => 'None',
      8 => 'Not Performed',
      9 => 'Completed Account on Hold',
    ),
    'SEM Report' => 
    array (
      0 => 'In Progress',
      1 => 'Waiting on Sponsor',
      2 => 'Waiting_on_Subcontracted_Report',
      3 => 'Waiting On Sponsor Ready to Audit',
      4 => 'Completed',
      5 => 'Overdue',
      6 => 'Sponsor Retracted',
      7 => 'None',
      8 => 'Not Performed',
      9 => 'Completed Account on Hold',
    ),
    'Slide Completion' => 
    array (
      0 => 'In Progress',
      1 => 'Waiting on Sponsor',
      2 => 'Waiting_on_Subcontracted_Report',
      3 => 'Waiting On Sponsor Ready to Audit',
      4 => 'Completed',
      5 => 'Overdue',
      6 => 'Sponsor Retracted',
      7 => 'None',
      8 => 'Not Performed',
      9 => 'Completed Account on Hold',
    ),
    'SpecimenSample Disposition' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'BC Abnormal Study Article' => 
    array (
      0 => 'In Progress',
      1 => 'Completed',
    ),
    'Chemical and Physical Characterization of Particulates' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Histology Controls' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Shipping Request' => 
    array (
      0 => 'In Progress',
      1 => 'Completed',
      2 => 'Overdue',
      3 => 'Not Performed',
    ),
    'Contributing Scientist Report Amendment' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Decalcification' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Sponsor Contracted Contributing Scientist Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'SEND Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Analytical Method' => 
    array (
      0 => '',
      1 => 'Pending method development',
      2 => 'In Progress',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    '' => 
    array (
    ),
    'Pharmacokinetic Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Early DeathTermination Investigation Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'TWT Protocol Review' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
      11 => 'Out to Study Director Waiting on Redlines',
    ),
    'TWT Report Review' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
      11 => 'Out to Study Director Waiting on Redlines',
    ),
    'IACUC Submission' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Pathology Protocol Review' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
      11 => 'Out to Study Director Waiting on Redlines',
    ),
    'Histopathology Report2' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Ready for Pathologist',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sent to Study DirectorPrinciple Investigator',
      9 => 'Sponsor Retracted',
      10 => 'None',
      11 => 'Not Performed',
      12 => 'Completed Account on Hold',
    ),
    'Interim Histopathology Report2' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Ready for Pathologist',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sent to Study DirectorPrinciple Investigator',
      9 => 'Sponsor Retracted',
      10 => 'None',
      11 => 'Not Performed',
      12 => 'Completed Account on Hold',
    ),
    'Pathology Misc' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Animal Selection' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Statistical Review' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'QC Data Review' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'In Data Review',
      3 => 'Waiting on Sponsor',
      4 => 'Waiting_on_Subcontracted_Report',
      5 => 'Waiting On Sponsor Ready to Audit',
      6 => 'Completed',
      7 => 'Overdue',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Animal Selection Populate TSD' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Animal Selection Procedure Use' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Summary Certificate of ISO 10993' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sent to Study DirectorPrinciple Investigator',
      8 => 'Sponsor Retracted',
      9 => 'None',
      10 => 'Not Performed',
      11 => 'Completed Account on Hold',
    ),
    'Summary Certificate of USP Class VI Testing' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Waiting_on_Subcontracted_Report',
      4 => 'Waiting On Sponsor Ready to Audit',
      5 => 'Completed',
      6 => 'Overdue',
      7 => 'Sponsor Retracted',
      8 => 'None',
      9 => 'Not Performed',
      10 => 'Completed Account on Hold',
    ),
    'Histomorphometry Report' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Waiting on Sponsor',
      3 => 'Ready for Pathologist',
      4 => 'Completed',
      5 => 'Sponsor Retracted',
      6 => 'Not Performed',
      7 => 'Completed Account on Hold',
    ),
    'Slide Imaging' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'Completed',
      3 => 'Sponsor Retracted',
      4 => 'Not Performed',
      5 => 'Completed Account on Hold',
    ),
    'Sample Prep Design' => 
    array (
    ),
    'Analytical Exhaustive Extraction' => 
    array (
      0 => '',
      1 => 'None',
      2 => 'In Progress',
      3 => 'In Data Review',
      4 => 'Completed',
      5 => 'Not Performed',
    ),
    'Analytical Exaggerated Extraction' => 
    array (
      0 => '',
      1 => 'None',
      2 => 'In Progress',
      3 => 'In Data Review',
      4 => 'Completed',
      5 => 'Not Performed',
    ),
    'Analytical Headspace GCMS Analysis' => 
    array (
      0 => '',
      1 => 'None',
      2 => 'In Progress',
      3 => 'In Data Review',
      4 => 'Completed',
      5 => 'Not Performed',
    ),
    'Analytical Direct Inject GCMS Analysis' => 
    array (
      0 => '',
      1 => 'None',
      2 => 'In Progress',
      3 => 'In Data Review',
      4 => 'Completed',
      5 => 'Not Performed',
    ),
    'Analytical LCMS Analysis' => 
    array (
      0 => '',
      1 => 'None',
      2 => 'In Progress',
      3 => 'In Data Review',
      4 => 'Completed',
      5 => 'Not Performed',
    ),
    'Analytical ICPMS Analysis' => 
    array (
      0 => '',
      1 => 'None',
      2 => 'In Progress',
      3 => 'In Data Review',
      4 => 'Completed',
      5 => 'Not Performed',
    ),
    'Analytical Headspace GCMS Interpretation' => 
    array (
      0 => '',
      1 => 'None',
      2 => 'In Progress',
      3 => 'In Data Review',
      4 => 'Completed',
      5 => 'Not Performed',
    ),
    'Analytical Direct Inject GCMS Interpretation' => 
    array (
      0 => '',
      1 => 'None',
      2 => 'In Progress',
      3 => 'In Data Review',
      4 => 'Completed',
      5 => 'Not Performed',
    ),
    'Analytical LCMS Interpretation' => 
    array (
      0 => '',
      1 => 'None',
      2 => 'In Progress',
      3 => 'In Data Review',
      4 => 'Completed',
      5 => 'Not Performed',
    ),
    'Analytical ICPMS Interpretation' => 
    array (
      0 => '',
      1 => 'None',
      2 => 'In Progress',
      3 => 'In Data Review',
      4 => 'Completed',
      5 => 'Not Performed',
    ),
    'Analytical GCMS Sample Preparation' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'In Data Review',
      3 => 'Completed',
      4 => 'Not Performed',
    ),
    'Analytical LCMS Sample Preparation' => 
    array (
      0 => '',
      1 => 'In Progress',
      2 => 'In Data Review',
      3 => 'Completed',
      4 => 'Not Performed',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Vardefs/sugarfield_final_due_date_c.php

 // created: 2022-12-08 09:24:38
$dictionary['M03_Work_Product_Deliverable']['fields']['final_due_date_c']['labelValue']='External Final Due Date';
$dictionary['M03_Work_Product_Deliverable']['fields']['final_due_date_c']['formula']='addDays($due_date_c,14)';
$dictionary['M03_Work_Product_Deliverable']['fields']['final_due_date_c']['enforced']='false';
$dictionary['M03_Work_Product_Deliverable']['fields']['final_due_date_c']['dependency']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['final_due_date_c']['required_formula']='';
$dictionary['M03_Work_Product_Deliverable']['fields']['final_due_date_c']['readonly_formula']='';

 
?>
