<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Dependencies/custom_setVisibility_on_fields.php

/* $dependencies['M03_Work_Product_Deliverable']['visibility_work_product_deliverable_fields_dep1111'] = array(
    'hooks'         => array("all","save"),
	'trigger'       => 'true',
    'triggerFields' => array('relative_template_c','deliverable_c','m03_work_product_m03_work_product_deliverable_1','select_template_c','relative_c','due_date_c','deliverable_status_c','type_3_c','work_type_c','timeline_type_c','relative_c','final_due_date_c','internal_final_due_date_c','bc_send_draft_report_c','redlines_c','draft_deliverable_sent_date_c','deliverable_completed_date_c','planned_bd_to_draft_c','overdue_risk_level_c','assigned_user_name','related_deliverable_c','template_name_c','due_date_c'),
    'onload'        => true,
	'actions' => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'due_date_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,ifElse(equal($relative_template_c,"No"),true,false))',
            ),
        ),
		array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'deliverable_status_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),or(equal($select_template_c,""),equal($select_template_c,"undefined"))),true,false)',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'deliverable_status_c',
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),or(equal($select_template_c,""),equal($select_template_c,"undefined"))),true,false)',
            ),
        ),
		array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'type_3_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($deliverable_c,"SpecimenSample Disposition")),true,false)',
            ),
        ),
		array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'type_3_c',              
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'work_type_c',                
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No"),or(not(greaterThan(strlen(toString($date_entered)),0)),isAfter($date_entered,"2020-06-10"))),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'work_type_c',                
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'timeline_type_c',                
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'relative_c',                
                'value'  => 'ifElse(equal($select_template_c,""),true,false)',
            ),
        ),
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'final_due_date_c',                
                // 'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No"),not(equal(related($m03_work_product_m03_work_product_deliverable_1,"functional_area_c"),"Standard Biocompatibility"))),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'final_due_date_c',                
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'deliverable_c',
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'internal_final_due_date_c',
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'bc_send_draft_report_c',
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'redlines_c',
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'draft_deliverable_sent_date_c',
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'deliverable_completed_date_c',
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'planned_bd_to_draft_c',
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'overdue_risk_level_c',                
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'overdue_risk_level_c',
                'value'  => 'ifElse(and(equal($select_template_c,""),equal($relative_template_c,"No")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'assigned_user_name',
                'value'  => 'ifElse(equal($relative_template_c,"Yes"),true,ifElse(equal($select_template_c,""),true,false))',
            ),
        ),
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'related_deliverable_c',
                'value'  => 'ifElse(and(and(equal($relative_template_c,"No"),equal($select_template_c,"")),equal($relative_c,"To Other Work Product Deliverable")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'related_deliverable_c',
                'value'  => 'ifElse(equal($relative_c,"To Other Work Product Deliverable"),true,false)',
            ),
        ),
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'template_name_c',
                'value'  => 'ifElse(and(equal($relative_template_c,"Yes"),equal($select_template_c,"")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'due_date_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,""),not(isInList($deliverable_c,createList("Animal Selection","Animal Selection Populate TSD","Animal Selection Procedure Use")))),true,false)',
                // 'value'  => 'ifElse(not(isInList($deliverable_c,createList("Animal Selection","Animal Selection Populate TSD","Animal Selection Procedure Use"))),true,ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,""),not(isInList($deliverable_c,createList("Animal Selection","Animal Selection Populate TSD","Animal Selection Procedure Use")))),true,ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,false))))',
            ),
        ),
    ),
); */
/* Department Info Time info and Notes Tab*/
/* $dependencies['M03_Work_Product_Deliverable']['visibility_work_product_deliverable_fields'] = array(
    'hooks'         => array("all","save"),
	'trigger'       => 'true',
    'triggerFields' => array('relative_template_c','select_template_c','author_report_name_c','work_product_deliverable_not_c'),
    'onload'        => true,
	'actions' => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'author_report_name_c',                
                'value'  => 'ifElse(or(equal($select_template_c,""),equal($select_template_c,"undefined")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'work_product_deliverable_not_c',                
                'value'  => 'ifElse(or(equal($select_template_c,""),equal($select_template_c,"undefined")),true,false)',
            ),
        ),
    ),
);
$dependencies['M03_Work_Product_Deliverable']['visibility_work_product_deliverable_fields_department'] = array(
    'hooks'         => array("all","save"),
	'trigger'       => 'true',
    'triggerFields' => array('relative_template_c','select_template_c','author_c','pathologist_workload_c','department_manager_approval_c','integer_estimated_owner_c','estimated_report_hours_c','integer_actual_owner_c','actual_hours_owner_c','date_qa_received_c','date_qa_edits_received_c','deliverable_lead_time_c','report_writing_days_c'),
    'onload'        => true,
	'actions' => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'author_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'pathologist_workload_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'department_manager_approval_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'integer_estimated_owner_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'estimated_report_hours_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'integer_actual_owner_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'actual_hours_owner_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'date_qa_received_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'date_qa_edits_received_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'deliverable_lead_time_c',                
                'value'  => 'ifElse(and(equal($relative_template_c,"No"),equal($select_template_c,"")),true,false)',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'report_writing_days_c',                
                'value'  => 'ifElse(and(equal($select_template_c,""),isInList($deliverable_c,createList("Histopathology Report2","Histopathology Report","Interim Gross Pathology Report","Interim Histopathology Report2","Interim Histopathology Report","Histopathology Notes"))),true,false)',
            ),
        ),
    ),
); */

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Dependencies/required_fields.php

$dependencies['M03_Work_Product_Deliverable']['required_fields'] = array(
	'hooks' => array("all"),
	'trigger' => 'true',
	'triggerFields' => array('deliverable_c','date_entered','id',),
	'onload' => true,
	//Actions is a list of actions to fire when the trigger is true
	'actions' => array(
		array(
			'name' => 'SetRequired',
			'params' => array(
				'target' => 'work_type_c',
				'value' => 'and(or(not(greaterThan(strlen(toString($date_entered)),0)),isAfter($date_entered,"2020-06-10")),not(equal($deliverable_c,"Sample Prep Design")))',
			),
		),		
	)
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Dependencies/required_due_date.php


$dependencies['M03_Work_Product_Deliverable']['duedate_required_123'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('deliverable_c','id'),
	'onload'        => true,
    'actions'       => array(
		
       array( 
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'due_date_c',
                 'value' => 'not(isInList($deliverable_c,createList("Animal Selection","Animal Selection Populate TSD","Animal Selection Procedure Use","Sample Prep Design")))',
            ),
        ), 
		// array( 
        //     'name' => 'SetRequired',
        //     'params' => array(
        //         'target' => 'due_date_c',
        //         //'value' => 'and(not(equal(related($m03_work_product_m03_work_product_deliverable_1,"functional_area_c"),"Standard Biocompatibility")),not(isInList($deliverable_c,createList("Animal Selection"))))',
        //         'value' => 'not(equal(related($m03_work_product_m03_work_product_deliverable_1,"functional_area_c"),"Standard Biocompatibility"))',
        //     ),
        // ),
    ),
);

$dependencies['M03_Work_Product_Deliverable']['final_due_date_required'] = array(
    'hooks' => array("edit", "view"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_m03_work_product_deliverable_1','final_due_date_c','functional_area_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'final_due_date_c',
                'value' => 'not(equal(related($m03_work_product_m03_work_product_deliverable_1,"functional_area_c"),"Standard Biocompatibility"))',
            ),
        ),    
    ),
);
  

?>
