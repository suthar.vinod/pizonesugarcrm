<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/sk_SK.customm03_work_product_m03_work_product_deliverable_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE_ID'] = 'Work Products ID';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/sk_SK.customtasks_m03_work_product_deliverable_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE_ID'] = 'Tasks ID';
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/sk_SK.customm03_work_product_deliverable_tasks_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/sk_SK.customm03_work_product_deliverable_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/sk_SK.customm03_work_product_deliverable_meetings_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Activities';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_MEETINGS_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Activities';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/sk_SK.customm03_work_product_deliverable_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Inventory Items';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/sk_SK.customm03_work_product_deliverable_ic_inventory_collection_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE'] = 'Inventory Collections';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Inventory Collections';

?>
