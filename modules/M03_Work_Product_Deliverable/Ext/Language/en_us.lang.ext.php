<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/en_us.customm03_work_product_m03_work_product_deliverable_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE_ID'] = 'Work Products ID';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/filename.en_us.lang.php


$mod_strings['LBL_FILE_MIME_TYPE'] = 'File Mime Type';
$mod_strings['LBL_FILE_URL'] = 'File URL';
$mod_strings['LBL_FILENAME'] = 'Document';

 

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/en_us.customtasks_m03_work_product_deliverable_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE_ID'] = 'Tasks ID';
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/en_us.customm03_work_product_deliverable_tasks_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/en_us.customm03_work_product_deliverable_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/en_us.customm03_work_product_deliverable_meetings_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Activities';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_MEETINGS_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Activities';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/en_us.customm03_work_product_deliverable_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Inventory Items';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/en_us.customm03_work_product_deliverable_ic_inventory_collection_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE'] = 'Inventory Collections';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Inventory Collections';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TIMELINE_TYPE'] = 'Timeline';
$mod_strings['LBL_DUE_DATE'] = 'Draft Due Date';
$mod_strings['LBL_DELIVERABLE_OWNER'] = 'Deliverable Owner';
$mod_strings['LBL_DELIVERABLE'] = 'Deliverable';
$mod_strings['LBL_DRAFT_DELIVERABLE_SENT_DATE'] = 'Draft Deliverable Sent Date';
$mod_strings['LBL_DELIVERABLE_STATUS'] = 'Deliverable Status';
$mod_strings['LBL_DELIVERABLE_COMPLETED_DATE'] = 'Deliverable Completed Date';
$mod_strings['LBL_NAME'] = 'Deliverable Name';
$mod_strings['LBL_DEPARTMENT_MANAGER_APPROVAL'] = 'Departmental Approval';
$mod_strings['LBL_INTERNAL_DEPARTMENT'] = 'Department';
$mod_strings['LBL_BC_TA_DISPOSAL_DATE'] = 'BC TA Disposal Date';
$mod_strings['LBL_WORK_PRODUCT_IMPORT'] = 'Work Product Import';
$mod_strings['LBL_PATHOLOGIST'] = 'Study Pathologist';
$mod_strings['LBL_STUDY_VETERINARIAN'] = 'Study Veterinarian';
$mod_strings['LBL_PATHOLOGIST_WORKLOAD'] = 'Pathologist Workload';
$mod_strings['LBL_REPORTING_HOURS'] = 'Reporting Hours';
$mod_strings['LBL_FINAL_DUE_DATE'] = 'External Final Due Date';
$mod_strings['LBL_REDLINES'] = 'Audit After Redlines?';
$mod_strings['LBL_COMPANY_STATUS'] = 'Account Status';
$mod_strings['LBL_BC_SEND_DRAFT_REPORT'] = 'Send BC Draft Report?';
$mod_strings['LBL_ESTIMATED_HOURS'] = 'Estimated Hours (Deliverable Owner)';
$mod_strings['LBL_ACTUAL_HOURS'] = 'Actual Hours (Deliverable Owner)';
$mod_strings['LBL_RECORD_BODY'] = 'General Info';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Department Info';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Time Info';
$mod_strings['LBL_ESTIMATED_HOURS_QA'] = 'Estimated Hours (QA)';
$mod_strings['LBL_ACTUAL_HOURS_QA'] = 'Actual Hours (QA)';
$mod_strings['LBL_ESTIMATED_REPORT_HOURS'] = 'Estimated Hours (Deliverable Owner)';
$mod_strings['LBL_ACTUAL_HOURS_OWNER'] = 'Actual Hours (Deliverable Owner)';
$mod_strings['LBL_INTEGER_ESTIMATED_OWNER'] = 'Decimal-Estimated Hours (Deliverable Owner)';
$mod_strings['LBL_INTEGER_ACTUAL_OWNER'] = 'Decimal-Actual Hours (Deliverable Owner)';
$mod_strings['LBL_INTEGER_ESTIMATED_QA'] = 'Decimal-Estimated Hours (QA)';
$mod_strings['LBL_INTEGER_ESTIMATE_QA'] = 'Integer-Estimated Hours (QA)';
$mod_strings['LBL_INTEGER_ACTUAL_QA'] = 'Decimal-Actual Hours (QA)';
$mod_strings['LBL_APS_TARGET_DUE_DATE'] = 'Internal BC Final Due Date Goal';
$mod_strings['LBL_DELIVERABLE_LEAD_TIME'] = 'Deliverable Lead Time';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Notes';
$mod_strings['LBL_WORK_PRODUCT_DELIVERABLE_NOT'] = 'Notes';
$mod_strings['LBL_REASON_FOR_AMENDMENT'] = 'Reason for Amendment';
$mod_strings['LBL_AMENDMENT_RESPONSIBILITY'] = 'Amendment Responsibility';
$mod_strings['LBL_FOR_BC_GOAL_INTERNAL_DRAFT'] = 'Internal BC Draft Due Date Goal';
$mod_strings['LBL_INTERNAL_FINAL_DUE_DATE'] = 'Internal Final Due Date';
$mod_strings['LBL_DATE_QA_RECEIVED'] = 'Date QA Received';
$mod_strings['LBL_DATE_QA_EDITS_RECEIVED'] = 'Date QA Edits Submitted to SD';
$mod_strings['LBL_AUTHOR_CONTACT_ID'] = 'Author (related Contact ID)';
$mod_strings['LBL_AUTHOR'] = 'Author';
$mod_strings['LBL_EMBEDDED'] = 'Embedded';
$mod_strings['LBL_CUT'] = 'Cut';
$mod_strings['LBL_QCD'] = 'QC&#039;d';
$mod_strings['LBL_CHARGE_SHEET_SUBMITTED'] = 'Charge Sheet Submitted';
$mod_strings['LBL_STAINED'] = 'Stained';
$mod_strings['LBL_CONTROLS_SUBMITTED'] = 'Controls Submitted';
$mod_strings['LBL_TYPE_2'] = 'Subtype';
$mod_strings['LBL_SHIPPING_COMPANY'] = 'Shipping Company';
$mod_strings['LBL_SHIPPING_DELIVERY'] = 'Shipping Delivery';
$mod_strings['LBL_SHIPPING_CONDITIONS'] = 'Shipping Conditions';
$mod_strings['LBL_HAZARDOUS_CONTENTS'] = 'Hazardous Contents';
$mod_strings['LBL_MATERIAL_CATEGORY'] = 'Material Category';
$mod_strings['LBL_WP_NAME'] = 'WP Name';
$mod_strings['LBL_DELIVERABLE_TEXT'] = 'Deliverable Text';
$mod_strings['LBL_TYPE_3'] = 'Type';
$mod_strings['LBL_WORK_TYPE'] = 'Work Type';
$mod_strings['LBL_CURRENT_LOCATION'] = 'Current Location';
$mod_strings['LBL_DATETIME_DESIRED'] = 'Date/Time Desired';
$mod_strings['LBL_TRANSFER_TO_LOCATION'] = 'Transfer to Location';
$mod_strings['LBL_STABILITY_CONSIDERATIONS'] = 'Stability Considerations';
$mod_strings['LBL_ANALYZE_BY_DATE'] = 'Analyze-by Date';
$mod_strings['LBL_DUE_TO_COMPLETE_TIMEFRAME'] = 'Due to Complete Timeframe (Days)';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_FOCUS_DRAWER_DASHBOARD'] = 'Work Product Deliverables Focus Drawer';
$mod_strings['LBL_STANDARD_HE_SLIDES'] = '# of Standard H&amp;E Slides';
$mod_strings['LBL_STANDARD_SPECIAL_STAIN'] = '# of Standard Special Stain Slides';
$mod_strings['LBL_STANDARD_UNSTAINED_SLIDES'] = '# of Standard Unstained Slides';
$mod_strings['LBL_LARGE_HE_SLIDES'] = '# of Large H&amp;E Slides';
$mod_strings['LBL_LARGE_SPECIAL_STAIN'] = '# of Large Special Stain Slides';
$mod_strings['LBL_SLIDE_TYPE'] = 'Slide Type';
$mod_strings['LBL_OFF_TEST_TO_FINAL_REPORT'] = 'Off Test to Final Report';
$mod_strings['LBL_OVERDUE_RISK_LEVEL'] = 'Overdue Risk Level';
$mod_strings['LBL_CHANGE_BY'] = 'Change By';
$mod_strings['LBL_CHANGE_TIMESTAMP_C'] = 'Change Timestamp';
$mod_strings['LBL_PATHOLOGIST_PARTICIPATION'] = 'Pathologist Participation at Necropsy Needed';
$mod_strings['LBL_MINUTES_PER_SLIDE'] = 'Minutes per Slide';
$mod_strings['LBL_TOTAL_NUMBER_OF_SLIDES'] = 'Total Number of Slides';
$mod_strings['LBL_PLANNED_BD_TO_DRAFT'] = 'Planned Business Days to Draft Report';
$mod_strings['LBL_ACTUAL_BC_TO_COMPLETE_DRAFT'] = 'Actual Business Days to Complete Draft';
$mod_strings['LBL_REPORT_WRITING_DAYS'] = 'Report Writing Days';
$mod_strings['LBL_PATHOLOGIST_ON_CALL'] = 'Pathologist on Call During Reporting Window';
$mod_strings['LBL_RECORDVIEW_PANEL5'] = 'Slide Information';
$mod_strings['LBL_WITHIN_TIMEFRAME'] = 'Within Timeframe';
$mod_strings['LBL_ATTN_TO_NAME'] = 'Attn. To Name';
$mod_strings['LBL_ADDRESS_STREET'] = 'Street';
$mod_strings['LBL_ADDRESS_CITY'] = 'City';
$mod_strings['LBL_ADDRESS_STATE'] = 'State';
$mod_strings['LBL_ADDRESS_POSTALCODE'] = 'Postal Code';
$mod_strings['LBL_ADDRESS_COUNTRY'] = 'Country';
$mod_strings['LBL_PHONE_NUMBER'] = 'Phone Number';
$mod_strings['LBL_ADDRESS_CONFIRMED'] = 'Address Confirmed';
$mod_strings['LBL_RECEIVING_PARTY_EXPECTING'] = 'Receiving Party Expecting Shipment';
$mod_strings['LBL_FAXITRON_REQUIRED'] = 'Faxitron Required Prior to Shipment';
$mod_strings['LBL_AUTHOR_REPORT_NAME_C_CONTACT_ID'] = 'Author Report Name (related Contact ID)';
$mod_strings['LBL_AUTHOR_REPORT_NAME'] = 'Author Report Name';
$mod_strings['LBL_DELIVERABLE_COM_DATE_TEST'] = 'Deliverable Com Date Test';
$mod_strings['LBL_LAST_PROCEDURE'] = 'Last Procedure';
$mod_strings['LBL_SAME_DAY_PREP_C'] = 'Same Day Prep Required?';
$mod_strings['LBL_NEAT_PREPARATION'] = 'Neat Preparation?';
$mod_strings['LBL_HDPE_ETO'] = 'HDPE Comparison ETO Required?';
$mod_strings['LBL_NUMBER_TEST_IMPLANTS'] = 'Number of implant articles needed per procedure date (Test)';
$mod_strings['LBL_SAMPLE_PREP_TIME_ESTIMATE'] = 'Sample Prep Time Estimate (min)';
$mod_strings['LBL_ADDITIONAL_SUPPLIES_REQUIRED'] = 'Additional Supplies Required for Implant';
$mod_strings['LBL_SAMPLE_PREP_DESIGN_NOTES'] = 'Sample Prep Design Notes';
$mod_strings['LBL_RECORDVIEW_PANEL6'] = 'Sample Prep Design';
$mod_strings['LBL_NUMBER_CONTROL_IMPLANTS'] = 'Number of implant articles needed per procedure date (Control)';

?>
