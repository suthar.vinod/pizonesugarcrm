<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Layoutdefs/m03_work_product_deliverable_tasks_1_M03_Work_Product_Deliverable.php

 // created: 2016-02-29 19:53:08
$layout_defs["M03_Work_Product_Deliverable"]["subpanel_setup"]['m03_work_product_deliverable_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'm03_work_product_deliverable_tasks_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Layoutdefs/m03_work_product_deliverable_ii_inventory_item_1_M03_Work_Product_Deliverable.php

 // created: 2022-02-22 07:44:29
$layout_defs["M03_Work_Product_Deliverable"]["subpanel_setup"]['m03_work_product_deliverable_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Layoutdefs/m03_work_product_deliverable_ic_inventory_collection_1_M03_Work_Product_Deliverable.php

 // created: 2022-02-22 07:48:02
$layout_defs["M03_Work_Product_Deliverable"]["subpanel_setup"]['m03_work_product_deliverable_ic_inventory_collection_1'] = array (
  'order' => 100,
  'module' => 'IC_Inventory_Collection',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'get_subpanel_data' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Layoutdefs/_overrideM03_Work_Product_Deliverable_subpanel_m03_work_product_deliverable_ic_inventory_collection_1.php

//auto-generated file DO NOT EDIT
$layout_defs['M03_Work_Product_Deliverable']['subpanel_setup']['m03_work_product_deliverable_ic_inventory_collection_1']['override_subpanel_name'] = 'M03_Work_Product_Deliverable_subpanel_m03_work_product_deliverable_ic_inventory_collection_1';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Deliverable/Ext/Layoutdefs/_overrideM03_Work_Product_Deliverable_subpanel_m03_work_product_deliverable_ii_inventory_item_1.php

//auto-generated file DO NOT EDIT
$layout_defs['M03_Work_Product_Deliverable']['subpanel_setup']['m03_work_product_deliverable_ii_inventory_item_1']['override_subpanel_name'] = 'M03_Work_Product_Deliverable_subpanel_m03_work_product_deliverable_ii_inventory_item_1';

?>
