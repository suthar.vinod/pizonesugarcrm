<?php
// WARNING: The contents of this file are auto-generated.


// created: 2022-02-22 07:48:02
$viewdefs['M03_Work_Product_Deliverable']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  ),
);

// created: 2022-02-22 07:44:29
$viewdefs['M03_Work_Product_Deliverable']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_deliverable_ii_inventory_item_1',
  ),
);

// created: 2016-02-29 19:53:08
$viewdefs['M03_Work_Product_Deliverable']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_deliverable_tasks_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product_Deliverable']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_deliverable_ic_inventory_collection_1',
  'view' => 'subpanel-for-m03_work_product_deliverable-m03_work_product_deliverable_ic_inventory_collection_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product_Deliverable']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'view' => 'subpanel-for-m03_work_product_deliverable-m03_work_product_deliverable_ii_inventory_item_1',
);
