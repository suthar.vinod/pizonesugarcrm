<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class update_WPD_draft_dateHook
{
	static $already_ran = false;
	function update_WPD_draft_dateHook_Method($bean, $event, $arguments)
	{
		if (self::$already_ran == true) return;
		self::$already_ran = true;

		global $db;
		if( $arguments['related_module']=='M03_Work_Product'){

			$wpID = $arguments['related_id'];
			$WPBean = BeanFactory::retrieveBean('M03_Work_Product',  $wpID);
			
			if($WPBean->functional_area_c == 'Standard Biocompatibility' && $WPBean->report_timeline_type_c == 'By First Procedure' && ($WPBean->first_procedure_c != "" || $WPBean->first_procedure_c != null)) {

				$WPBean->load_relationship("m03_work_product_m03_work_product_deliverable_1");
				$WPDIDs = $WPBean->m03_work_product_m03_work_product_deliverable_1->get();

				foreach ($WPDIDs as $wpdId) {
					$WPDBean = BeanFactory::retrieveBean('M03_Work_Product_Deliverable',  $wpdId);
	
						if ($WPDBean->deliverable_c == "Final Report") {
							if ($WPBean->final_report_timeline_c == '' || $WPBean->final_report_timeline_c == null) {
								$Days = ' + 0 days';
								$finalDueDate = date('Y-m-d', strtotime($WPBean->first_procedure_c . $Days));
								
							} else {
								$Days = 7 * $WPBean->final_report_timeline_c;
								$finalDueDate = date('Y-m-d', strtotime($WPBean->first_procedure_c . ' + ' . $Days . ' days'));
							   
							}
	
							if ($num_rows > 0) {
								if ($WPBean->final_report_timeline_c == '' || $WPBean->final_report_timeline_c == null) {
									$minusDays = 5; 
									$DueDate = $this->getDaysCalculation($WPBean->first_procedure_c,$minusDays);
							
								} else {
									$DueDays = (7 * $WPBean->final_report_timeline_c);// - 5
									$DueDate1 = date('Y-m-d', strtotime($WPBean->first_procedure_c . ' + ' . $DueDays . ' days'));
									$minusDays = 5; 
									$DueDate = $this->getDaysCalculation($DueDate1,$minusDays);
								}
							} else {
								if ($WPBean->final_report_timeline_c == '' || $WPBean->final_report_timeline_c == null) {
									$minusDays = 3; 
									$DueDate = $this->getDaysCalculation($WPBean->first_procedure_c,$minusDays);
									
								} else {
									$DueDays = (7 * $WPBean->final_report_timeline_c);// - 3
									$DueDate1 = date('Y-m-d', strtotime($WPBean->first_procedure_c . ' + ' . $DueDays . ' days'));
									$minusDays = 3; 
									$DueDate = $this->getDaysCalculation($DueDate1,$minusDays);
								}
							}
	
							$WPDBean->final_due_date_c = $finalDueDate;
							$WPDBean->due_date_c = $DueDate;
							$WPDBean->save();
						} else if ($WPDBean->deliverable_c == "Histopathology Report") {
							if ($WPBean->final_report_timeline_c == '' || $WPBean->final_report_timeline_c == null) {
								$Days = ' + 0 days';
								$DueDays = ' - 6 days';
								$finalDueDate = date('Y-m-d', strtotime($WPBean->first_procedure_c . $Days));
								$minusDays = 6; 
								$DueDate = $this->getDaysCalculation($WPBean->first_procedure_c,$minusDays);
							} else {
								$Days = 7 * $WPBean->final_report_timeline_c;
								$finalDueDate = date('Y-m-d', strtotime($WPBean->first_procedure_c . ' + ' . $Days . ' days'));
	
								if ($bean->final_report_timeline_c == 1) {
									$minusDays = 1; 
									$DueDate = $this->getDaysCalculation($bean->first_procedure_c,$minusDays);
								} else {
									$DueDays	= (7 * $WPBean->final_report_timeline_c);// - 6
									$DueDate1	= date('Y-m-d', strtotime($WPBean->first_procedure_c . ' + ' . $DueDays . ' days'));
									$minusDays	= 6; 
									$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
								}
							}
	
							$WPDBean->final_due_date_c = $finalDueDate;
							$WPDBean->due_date_c = $DueDate;
							$WPDBean->save();
						}
					 
 
				}
			} else if ($WPBean->report_timeline_type_c == 'By SPA Reconciliation' && ($WPBean->bc_spa_reconciled_date_c != "" || $WPBean->bc_spa_reconciled_date_c != null)) {

				$WPBean->load_relationship("m03_work_product_m03_work_product_deliverable_1");
				$WPDIDs = $WPBean->m03_work_product_m03_work_product_deliverable_1->get();

				foreach ($WPDIDs as $wpdId) {
					$WPDBean = BeanFactory::retrieveBean('M03_Work_Product_Deliverable',  $wpdId);
					
					if ($WPDBean->deliverable_c == "Final Report") {
						if ($WPBean->final_report_timeline_c == '' || $WPBean->final_report_timeline_c == null) {
							$Days = ' + 0 days';
							$finalDueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $Days));
						} else {
							$Days = 7 * $WPBean->final_report_timeline_c;
							$finalDueDate = date('Y-m-d', strtotime($WPBean->bc_spa_reconciled_date_c . ' + ' . $Days . ' days'));
						}

						if ($num_rows > 0) {
							if ($WPBean->final_report_timeline_c == '' || $WPBean->final_report_timeline_c == null) {
								$minusDays	= 5; 
								$DueDate	= $this->getDaysCalculation($WPBean->bc_spa_reconciled_date_c,$minusDays);
								
							} else {
								$DueDays = (7 * $WPBean->final_report_timeline_c) ;//- 5
								$DueDate1 = date('Y-m-d', strtotime($WPBean->bc_spa_reconciled_date_c . ' + ' . $DueDays . ' days'));
								$minusDays	= 5; 
								$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
							}
						} else {
							if ($WPBean->final_report_timeline_c == '' || $WPBean->final_report_timeline_c == null) {
								$minusDays	= 3; 
								$DueDate	= $this->getDaysCalculation($WPBean->bc_spa_reconciled_date_c,$minusDays);
							} else {
								$DueDays = (7 * $WPBean->final_report_timeline_c) ;//- 3
								$DueDate1 = date('Y-m-d', strtotime($WPBean->bc_spa_reconciled_date_c . ' + ' . $DueDays . ' days'));
								$minusDays	= 3; 
								$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
							}
						}

						$WPDBean->final_due_date_c	= $finalDueDate;
						$WPDBean->due_date_c		= $DueDate;
						$WPDBean->save();
					} else if ($WPDBean->deliverable_c == "Histopathology Report") {
						if ($WPBean->final_report_timeline_c == '' || $WPBean->final_report_timeline_c == null) {
							$Days		= ' + 0 days';
							$DueDays	= ' - 6 days';
							$finalDueDate	= date('Y-m-d', strtotime($WPBean->bc_spa_reconciled_date_c . $Days));
							$minusDays		= 6; 
							$DueDate		= $this->getDaysCalculation($WPBean->bc_spa_reconciled_date_c,$minusDays);
						} else {
							$Days = 7 * $WPBean->final_report_timeline_c;
							$finalDueDate = date('Y-m-d', strtotime($WPBean->bc_spa_reconciled_date_c . ' + ' . $Days . ' days'));

							if ($WPBean->final_report_timeline_c == 1) {
								$DueDays	= ' - 1 days';
								$DueDate	= date('Y-m-d', strtotime($WPBean->bc_spa_reconciled_date_c . $DueDays));
								$minusDays	= 1; 
								$DueDate	= $this->getDaysCalculation($WPBean->bc_spa_reconciled_date_c,$minusDays);
							} else {
								$DueDays	= (7 * $WPBean->final_report_timeline_c) ;//- 6
								$DueDate1	= date('Y-m-d', strtotime($WPBean->bc_spa_reconciled_date_c . '+' . $DueDays . ' days'));
								$minusDays	= 6; 
								$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
							}
						}

						$WPDBean->final_due_date_c = $finalDueDate;
						$WPDBean->due_date_c = $DueDate;
						$WPDBean->save();
					}
				}
			}

			 	
		}
	}

	function getDaysCalculation($actualDate,$subDays){
		$t = strtotime($actualDate);
 		for($i=$subDays; $i>0; $i--){
			$oneDay = 86400;// add 1 day to timestamp
			$nextDay = date('w', ($t-$oneDay));// get what day it is next day
			// if it's Saturday or Sunday get $i-1
			if($nextDay == 0 || $nextDay == 6) {
				$i++;
			}
			$t = $t-$oneDay;// modify timestamp, add 1 day
		}
		return date('Y-m-d',$t);		
	}
}