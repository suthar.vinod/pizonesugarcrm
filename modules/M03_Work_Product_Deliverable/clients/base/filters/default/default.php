<?php
// created: 2022-05-24 09:53:01
$viewdefs['M03_Work_Product_Deliverable']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'actual_bc_to_complete_draft_c' => 
    array (
    ),
    'additional_supplies_required_c' => 
    array (
    ),
    'address_confirmed_c' => 
    array (
    ),
    'analyze_by_date_c' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'attn_to_name_c' => 
    array (
    ),
    'address_city_c' => 
    array (
    ),
    'address_country_c' => 
    array (
    ),
    'current_location_c' => 
    array (
    ),
    'datetime_desired_c' => 
    array (
    ),
    'deliverable_c' => 
    array (
    ),
    'name' => 
    array (
    ),
    'deliverable_status_c' => 
    array (
    ),
    'due_date_c' => 
    array (
    ),
    'faxitron_required_c' => 
    array (
    ),
    'hdpe_eto_c' => 
    array (
    ),
    'minutes_per_slide_c' => 
    array (
    ),
    'neat_preparation_c' => 
    array (
    ),
    'number_control_implants_c' => 
    array (
    ),
    'number_test_implants_c' => 
    array (
    ),
    'standard_he_slides_c' => 
    array (
    ),
    'standard_special_stain_c' => 
    array (
    ),
    'standard_unstained_slides_c' => 
    array (
    ),
    'large_he_slides_c' => 
    array (
    ),
    'large_special_stain_c' => 
    array (
    ),
    'off_test_to_final_report_c' => 
    array (
    ),
    'overdue_risk_level_c' => 
    array (
    ),
    'pathologist_on_call_c' => 
    array (
    ),
    'pathologist_participation_c' => 
    array (
    ),
    'phone_number_c' => 
    array (
    ),
    'planned_bd_to_draft_c' => 
    array (
    ),
    'address_postalcode_c' => 
    array (
    ),
    'receiving_party_expecting_c' => 
    array (
    ),
    'report_writing_days_c' => 
    array (
    ),
    'same_day_prep_c' => 
    array (
    ),
    'sample_prep_design_notes_c' => 
    array (
    ),
    'sample_prep_time_estimate_c' => 
    array (
    ),
    'slide_type_c' => 
    array (
    ),
    'stability_considerations_c' => 
    array (
    ),
    'address_state_c' => 
    array (
    ),
    'address_street_c' => 
    array (
    ),
    'type_2_c' => 
    array (
    ),
    'total_number_of_slides_c' => 
    array (
    ),
    'transfer_to_location_c' => 
    array (
    ),
    'type_3_c' => 
    array (
    ),
    'within_timeframe_c' => 
    array (
    ),
    'work_type_c' => 
    array (
    ),
    'final_due_date_c' => 
    array (
    ),
    'last_procedure_c' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
  ),
);