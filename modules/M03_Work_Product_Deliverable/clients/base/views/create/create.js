({
    extendsFrom: 'CreateView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.model.on('change:interval_days_2_c change:relative_2_c change:related_deliverable_2_c change:deliverable_c change:m03_work_product_m03_work_product_deliverable_1_name', this.function_work_product_deliverable_change, this);
        //this.model.on('change:deliverable_c', this.function_field_hide_show, this);
        //this.model.on('change:dt_deliverable_template_m03_work_product_deliverable_1_name', this.getRelativeFromDT, this);
        //this.model.on('change:deliverable_c', this.function_deliverable, this);
        this.model.addValidationTask('check_due_date_c', _.bind(this._doValidateDueDate, this));
        this.model.addValidationTask('check_deliverable_completed_date_c', _.bind(this._doValidateCompleteDate, this));
        this.model.addValidationTask('check_deliverable_field', _.bind(this._doValidatedeliverableFiled, this));
        this.events['keyup input[name=interval_days_2_c]'] = 'fun_interval_days_2';
    },

    fun_interval_days_2: function(){
		//var interval_days_2_c = $('[data-name="interval_days_2_c"] input[name="interval_days_2_c"]').val();
		 var interval_days_2_c = this.model.get('interval_days_2_c');
        console.log('interval_days_2_c',interval_days_2_c);

		if (interval_days_2_c != "" && interval_days_2_c != "-")
		{			
			var relative_2_c = this.model.get('relative_2_c');
			if(relative_2_c == 'To First Procedure Date' || relative_2_c == 'To Last Term Date' || relative_2_c == 'To Other Work Product Deliverable'){
				this.function_work_product_deliverable_change();
			}
		}
	},

    function_field_hide_show: function () {
        var self = this;
        var deliverable_c = this.model.get('deliverable_c');
        if (deliverable_c == 'Sample Prep Design') {
            $('[data-name="assigned_user_name"]').css("visibility", "visible");
            $('[data-name="assigned_user_name"]').css("height", "auto");
        } else {
            $('[data-name="assigned_user_name"]').css("visibility", "hidden");
            $('[data-name="assigned_user_name"]').css("height", "0");
        }
    },
    _doValidatedeliverableFiled: function (fields, errors, callback) {
        var self = this;
        var type_3_c = this.model.get('type_3_c');
        var type_2_c = this.model.get('type_2_c');
        var shipping_delivery_c = this.model.get('shipping_delivery_c');
        var deliverable_c = this.model.get('deliverable_c');
        var deliverable_status_c = this.model.get('deliverable_status_c');
        var shipping_company_c = this.model.get('shipping_company_c');
        var stability_considerations_c = this.model.get('stability_considerations_c');
        var same_day_prep_c = this.model.get('same_day_prep_c');
        var neat_preparation_c = this.model.get('neat_preparation_c');
        var hdpe_eto_c = this.model.get('hdpe_eto_c');
        var number_test_implants_c = this.model.get('number_test_implants_c');
        var number_control_implants_c = this.model.get('number_control_implants_c');
        var sample_prep_time_estimate_c = this.model.get('sample_prep_time_estimate_c');
        var additional_supplies_required_c = this.model.get('additional_supplies_required_c');
        var sample_prep_design_notes_c = this.model.get('sample_prep_design_notes_c');
        var work_type_c = this.model.get('work_type_c');
        console.log('number_control_implants_c', number_control_implants_c);
        console.log('work_type_c', work_type_c);
        //validate requirements
        if (type_3_c == 'Transfer' && _.isEmpty(this.model.get('datetime_desired_c'))) {
            errors['datetime_desired_c'] = errors['datetime_desired_c'] || {};
            errors['datetime_desired_c'].required = true;
        }
        if (type_3_c == 'Transfer' && _.isEmpty(this.model.get('transfer_to_location_c'))) {
            errors['transfer_to_location_c'] = errors['transfer_to_location_c'] || {};
            errors['transfer_to_location_c'].required = true;
        }
        if (type_3_c == 'Transfer' && _.isEmpty(this.model.get('current_location_c'))) {
            errors['current_location_c'] = errors['current_location_c'] || {};
            errors['current_location_c'].required = true;
        }

        if ((type_3_c == 'Shipping' || type_3_c == 'Archive' || deliverable_c == 'Shipping Request') && _.isEmpty(this.model.get('shipping_conditions_c'))) {
            errors['shipping_conditions_c'] = errors['shipping_conditions_c'] || {};
            errors['shipping_conditions_c'].required = true;
        }
        if ((type_3_c == 'Shipping' || type_3_c == 'Archive' || deliverable_c == 'Shipping Request') && _.isEmpty(this.model.get('hazardous_contents_c'))) {

            errors['hazardous_contents_c'] = errors['hazardous_contents_c'] || {};
            errors['hazardous_contents_c'].required = true;
        }
        if ((type_3_c == 'Shipping' || type_3_c == 'Archive' || type_3_c == 'Transfer') && (_.isEmpty(this.model.get('stability_considerations_c')))) {
            errors['stability_considerations_c'] = errors['stability_considerations_c'] || {};
            errors['stability_considerations_c'].required = true;
        }
        if ((type_3_c == 'Shipping' || type_3_c == 'Archive' || deliverable_c == 'Shipping Request') && (_.isEmpty(this.model.get('shipping_company_c')) || shipping_company_c == null)) {
            errors['shipping_company_c'] = errors['shipping_company_c'] || {};
            errors['shipping_company_c'].required = true;
        }
        if ((shipping_company_c == 'Fed Ex' || shipping_company_c == 'On Time') && (_.isEmpty(this.model.get('shipping_delivery_c')) || shipping_delivery_c == null)) {
            errors['shipping_delivery_c'] = errors['shipping_delivery_c'] || {};
            errors['shipping_delivery_c'].required = true;
        }
        if (stability_considerations_c == 'Yes' && _.isEmpty(this.model.get('analyze_by_date_c'))) {
            errors['analyze_by_date_c'] = errors['analyze_by_date_c'] || {};
            errors['analyze_by_date_c'].required = true;
        }
        if ((type_3_c == 'Shipping' || type_3_c == 'Archive') && (_.isEmpty(this.model.get('type_2_c')) || type_2_c == null)) {
            errors['type_2_c'] = errors['type_2_c'] || {};
            errors['type_2_c'].required = true;
        }

        if (deliverable_c == 'SpecimenSample Disposition' && (_.isEmpty(this.model.get('type_3_c')) || type_3_c == null)) {
            errors['type_3_c'] = errors['type_3_c'] || {};
            errors['type_3_c'].required = true;
        }

        if (_.isEmpty(this.model.get('same_day_prep_c')) && (deliverable_c == 'Sample Prep Design') && (same_day_prep_c == '' || same_day_prep_c == null)) {
            errors['same_day_prep_c'] = errors['same_day_prep_c'] || {};
            errors['same_day_prep_c'].required = true;
        }
        if (_.isEmpty(this.model.get('neat_preparation_c')) && (deliverable_c == 'Sample Prep Design') && (neat_preparation_c == '' || neat_preparation_c == null)) {
            errors['neat_preparation_c'] = errors['neat_preparation_c'] || {};
            errors['neat_preparation_c'].required = true;
        }
        if (_.isEmpty(this.model.get('hdpe_eto_c')) && (deliverable_c == 'Sample Prep Design') && (hdpe_eto_c == '' || hdpe_eto_c == null)) {
            errors['hdpe_eto_c'] = errors['hdpe_eto_c'] || {};
            errors['hdpe_eto_c'].required = true;
        }
        /* if (_.isEmpty(this.model.get('number_test_implants_c')) && (deliverable_c == 'Sample Prep Design') && (number_test_implants_c == '' || number_test_implants_c == undefined)) {
            errors['number_test_implants_c'] = errors['number_test_implants_c'] || {};
            errors['number_test_implants_c'].required = true;
        }
        if (_.isEmpty(this.model.get('number_control_implants_c')) && (deliverable_c == 'Sample Prep Design') && (number_control_implants_c == '' || number_control_implants_c == undefined)) {
            errors['number_control_implants_c'] = errors['number_control_implants_c'] || {};
            errors['number_control_implants_c'].required = true;
        }        
        if (_.isEmpty(this.model.get('additional_supplies_required_c')) && (deliverable_c == 'Sample Prep Design')) {
            errors['additional_supplies_required_c'] = errors['additional_supplies_required_c'] || {};
            errors['additional_supplies_required_c'].required = true;
        }
        if (_.isEmpty(this.model.get('sample_prep_design_notes_c')) && (deliverable_c == 'Sample Prep Design')) {
            errors['sample_prep_design_notes_c'] = errors['sample_prep_design_notes_c'] || {};
            errors['sample_prep_design_notes_c'].required = true;
        }*/
        if (_.isEmpty(this.model.get('sample_prep_time_estimate_c')) && (deliverable_c == 'Sample Prep Design') && (sample_prep_time_estimate_c == '' || sample_prep_time_estimate_c == undefined)) {
            errors['sample_prep_time_estimate_c'] = errors['sample_prep_time_estimate_c'] || {};
            errors['sample_prep_time_estimate_c'].required = true;
        } 
        /* if (_.isEmpty(this.model.get('m03_work_product_m03_work_product_deliverable_1_name')) && (deliverable_c == 'Sample Prep Design')) {
            errors['m03_work_product_m03_work_product_deliverable_1_name'] = errors['m03_work_product_m03_work_product_deliverable_1_name'] || {};
            errors['m03_work_product_m03_work_product_deliverable_1_name'].required = true;
        } */
        if (_.isEmpty(this.model.get('work_type_c')) && (deliverable_c != 'Sample Prep Design') && (work_type_c == '' || work_type_c == null)) {
            console.log('work_type_c IN', work_type_c);
            errors['work_type_c'] = errors['work_type_c'] || {};
            errors['work_type_c'].required = true;
        }
        if (_.isEmpty(this.model.get('deliverable_status_c')) && (deliverable_c != 'Sample Prep Design' && deliverable_c != 'Analytical Exhaustive Extraction' && deliverable_c != 'Analytical Exaggerated Extraction' && deliverable_c != 'Analytical Headspace GCMS Analysis' && deliverable_c != 'Analytical Direct Inject GCMS Analysis' && deliverable_c != 'Analytical LCMS Analysis' && deliverable_c != 'Analytical ICPMS Analysis' && deliverable_c != 'Analytical Headspace GCMS Interpretation' && deliverable_c != 'Analytical Direct Inject GCMS Interpretation'  && deliverable_c != 'Analytical LCMS Interpretation' && deliverable_c != 'Analytical ICPMS Interpretation') && (deliverable_status_c == '' || deliverable_status_c == null)) {
            console.log('deliverable_status_c IN Create', deliverable_status_c);
            errors['deliverable_status_c'] = errors['deliverable_status_c'] || {};
            errors['deliverable_status_c'].required = true;
        }
        if (_.isEmpty(this.model.get('overdue_risk_level_c')) && (deliverable_c != 'Sample Prep Design')) {
            errors['overdue_risk_level_c'] = errors['overdue_risk_level_c'] || {};
            errors['overdue_risk_level_c'].required = true;
        }

        callback(null, fields, errors);
    },
    _doValidateCompleteDate: function (fields, errors, callback) {
        var deliverable_status_c = this.model.get('deliverable_status_c');
        //validate requirements
        if (deliverable_status_c == 'Completed' && _.isEmpty(this.model.get('deliverable_completed_date_c'))) {
            errors['deliverable_completed_date_c'] = errors['deliverable_completed_date_c'] || {};
            errors['deliverable_completed_date_c'].required = true;
        }

        callback(null, fields, errors);
    },

    _doValidateDueDate: function (fields, errors, callback) {
        var due_date_c = this.model.get('due_date_c');
        var work_product_id = this.model.get('m03_work_p0b66product_ida');
        var deliverable_c = this.model.get('deliverable_c');
        if (work_product_id != '' && work_product_id != undefined) {
            var WP_API = "rest/v10/M03_Work_Product/" + work_product_id + "?fields=functional_area_c";
            App.api.call("get", WP_API, null, {
                success: function (WP_Data) {
                    var new_allocated_wp_name = WP_Data.functional_area_c;
                    console.log('WP_API data : name : ', new_allocated_wp_name);
                    if (new_allocated_wp_name != 'Standard Biocompatibility' && deliverable_c != 'Animal Selection' && deliverable_c != 'Animal Selection Populate TSD' && deliverable_c != 'Animal Selection Procedure Use' && deliverable_c != 'Sample Prep Design' && (due_date_c == '' || due_date_c == undefined)) {
                        console.log('contacts', new_allocated_wp_name);
                        errors['due_date_c'] = errors['due_date_c'] || {};
                        errors['due_date_c'].required = true;
                    }
                    callback(null, fields, errors);
                }
            });
        } else {
            callback(null, fields, errors);
        }
    },
 
    function_work_product_deliverable_change: function () {
        var self = this;
        console.log('function_work_product_deliverable_change');
        setTimeout(function() { 
            var work_product_id = self.model.get('m03_work_p0b66product_ida');
            var deliverable_c = self.model.get('deliverable_c');
            var timeline_type_c = self.model.get('timeline_type_c');
            var relative_2 = self.model.get('relative_2_c');
            var interval_days_2 = self.model.get('interval_days_2_c');
            if (work_product_id) {
                //var contacts = self.model.getRelatedCollection('m03_work_product');
                //contacts.fetch({ relate: true });
                //console.log('contacts', contacts);
                var url = app.api.buildURL("getWPDDueDates");
                var method = "create";
                var data = {
                    work_product_id: work_product_id,
                    deliverable_c: deliverable_c,
                    timeline_type_c: timeline_type_c,
                    relative_2 :relative_2,
                    interval_days_2:interval_days_2,
                    wpdId:self.model.get('id'),
                    related_deliverable_2_id:self.model.get("m03_work_product_deliverable_id_c")
                };
                console.log('data11122',data);
                var callback = {
                    success: _.bind(function successCB(res) {
                        console.log('resData', res);
                        if (res != null) {
                            if ((res.final_due_date_c != null || res.final_due_date_c != '') && (res.due_date_c != null || res.due_date_c != '')) {
                                self.model.set("final_due_date_c", res.final_due_date_c);
                                if(res.due_date_c != undefined){
                                    self.model.set("due_date_c", res.due_date_c);
                                }
                            }

                            console.log('res final_due_date_c',res.final_due_date_c);
                            console.log('relative_2',relative_2);
                            if((res.final_due_date_c != null || res.final_due_date_c != '') && relative_2 == 'To First Procedure Date' || relative_2 == 'To Last Term Date' || relative_2 == 'To Other Work Product Deliverable'){
                                self.model.set("final_due_date_c", res.final_due_date_c);	
                            }
                        }
                    }, self)
                };
                app.api.call(method, url, data, callback);
            }
        }, 500);
    },
    //This function set the value into Relative and Interval days from the Selected Deliverable Template
    getRelativeFromDT: function () {
        var deliverableTemplateId = this.model.get('dt_delivercf79emplate_ida');
        var relativeValue = this.model.get('relative_2_c');
        // console.log("relativeValue==>",relativeValue);
        // console.log("DeliverableTempId====>",deliverableTemplateId);
        if (deliverableTemplateId == "") {
            this.model.set("interval_days_2_c", ""); // if the deliverable template value blank thn interval days must be blank
        }
        if (deliverableTemplateId !== "") {
            var url = app.api.buildURL("getValuesFromDT");
            var method = "create";
            var data = {
                deliverableTemplateId: deliverableTemplateId,
                relativeValue: relativeValue
            };
            var callback = {
                success: _.bind(function successCB(res) {
                    // console.log('res', res);
                    if (res != null) {
                        this.model.set("relative_2_c", res.relative);
                        if ((res.relative == 'To First Procedure Date' || res.relative == 'To Last Term Date' || res.relative == 'To Other Work Product Deliverable') && deliverableTemplateId !== null) {
                            // console.log("interval_days_2_c",res.intervalDaysValue);
                            this.model.set("interval_days_2_c", res.intervalDaysValue);
                        }
                    }
                }, this)
            };
            app.api.call(method, url, data, callback);
        }
    },

})