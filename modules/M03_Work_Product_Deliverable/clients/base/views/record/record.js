({
    extendsFrom: 'RecordView',
    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.onload_function, this);
        //this.context.on('button:cancel_button:click', this.cancel_button, this);
        //this.context.on('button:edit_button:click', this.edit_button, this);
		this.events['keyup input[name=interval_days_2_c]'] = 'fun_interval_days_2';
        this.model.once("sync",
            function() {
                this.model.on("change:interval_days_2_c change:related_deliverable_2_c change:deliverable_c change:m03_work_product_m03_work_product_deliverable_1_name",this.function_work_product_deliverable,this);
                this.model.addValidationTask('check_due_date_c', _.bind(this._doValidateDueDate, this));
                this.model.addValidationTask('check_deliverable_completed_date_c', _.bind(this._doValidateCompleteDate, this));
                this.model.addValidationTask('check_deliverable_field', _.bind(this._doValidatedeliverableFiled, this));
                this.model.on('change:relative_2_c change:relative_template_c change:deliverable_c change:type_3_c change:deliverable_status_c change:stability_considerations_c change:shipping_company_c', this.function_select_template, this);
            },
            this
        );
        this.model.once("sync",
            function () {
                this.model.on("change:dt_deliverable_template_m03_work_product_deliverable_1_name",this.getRelativeFromDT,this);
            },
            this
        );  
    },
	onload_function: function () {
		console.log("In onload30");
		let deliverableTemplateId = this.model.get('dt_delivercf79emplate_ida');
		let relativeValue         = this.model.get('relative_2_c');
		let intervalDays          = this.model.get('interval_days_2_c');
		console.log("deliverableTemplateId", deliverableTemplateId);
		console.log("relativeValue", relativeValue);
		console.log("intervalDays", intervalDays);
		if(deliverableTemplateId != "" || deliverableTemplateId != undefined || deliverableTemplateId != null){
			$('input[name="relative_2_c"]').prop('disabled', true);
            $('div[data-name="relative_2_c"]').css('pointer-events', 'none');
		}else{
			$('input[name="relative_2_c"]').prop('disabled', false);
            $('div[data-name="relative_2_c"]').css('pointer-events', '');
		}
		if((relativeValue == "To First Procedure Date" || relativeValue == "To Last Term Date" || relativeValue == "To Other Work Product Deliverable") &&(deliverableTemplateId != "" || deliverableTemplateId != undefined || deliverableTemplateId != null)){
			$('input[name="interval_days_2_c"]').prop('disabled', true);
            $('div[data-name="interval_days_2_c"]').css('pointer-events', 'none');
		}else{
			$('input[name="interval_days_2_c"]').prop('disabled', false);
            $('div[data-name="interval_days_2_c"]').css('pointer-events', '');
		}
		if((relativeValue == "To Other Work Product Deliverable") &&(deliverableTemplateId != "" || deliverableTemplateId != undefined || deliverableTemplateId != null)){
			$('input[name="related_deliverable_2_c"]').prop('disabled', true);
            $('div[data-name="related_deliverable_2_c"]').css('pointer-events', 'none');
		}else{
			$('input[name="related_deliverable_2_c"]').prop('disabled', false);
            $('div[data-name="related_deliverable_2_c"]').css('pointer-events', '');
		}
	},
	fun_interval_days_2: function(model){
		var interval_days_2_c = $('[data-name="interval_days_2_c"] input[name="interval_days_2_c"]').val();
		 
        console.log('interval_days_2_c',interval_days_2_c);

		if (interval_days_2_c != "" && interval_days_2_c != "-")
		{			
			var relative_2_c = this.model.get('relative_2_c');
			if(relative_2_c == 'To First Procedure Date' || relative_2_c == 'To Last Term Date' || relative_2_c == 'To Other Work Product Deliverable'){
				console.log('interval_days_2_c condition ');
				this.function_work_product_deliverable(model,interval_days_2_c);
			}
		}
	},
   
	_doValidatedeliverableFiled: function(fields, errors, callback) {
		var self = this;
		var type_3_c 					= this.model.get('type_3_c');
        var type_2_c 					= this.model.get('type_2_c');
        var shipping_delivery_c 		= this.model.get('shipping_delivery_c');
		var deliverable_c 				= this.model.get('deliverable_c');
		var deliverable_status_c 		= this.model.get('deliverable_status_c');
		var shipping_company_c 			= this.model.get('shipping_company_c');
		var stability_considerations_c  = this.model.get('stability_considerations_c');
		var same_day_prep_c 			= this.model.get('same_day_prep_c');
        var neat_preparation_c 			= this.model.get('neat_preparation_c');
        var hdpe_eto_c 					= this.model.get('hdpe_eto_c');
        var number_test_implants_c 		= this.model.get('number_test_implants_c');
        var number_control_implants_c 	= this.model.get('number_control_implants_c');
        var sample_prep_time_estimate_c = this.model.get('sample_prep_time_estimate_c');
        var additional_supplies_required_c = this.model.get('additional_supplies_required_c');
        var work_type_c 	= this.model.get('work_type_c');
 
		//validate requirements
		if (type_3_c == 'Transfer' && _.isEmpty(this.model.get('datetime_desired_c'))) {
			errors['datetime_desired_c'] = errors['datetime_desired_c'] || {};
			errors['datetime_desired_c'].required = true;
		}
		if (type_3_c == 'Transfer' && _.isEmpty(this.model.get('transfer_to_location_c'))) {			
			errors['transfer_to_location_c'] = errors['transfer_to_location_c'] || {};
			errors['transfer_to_location_c'].required = true;
		}
		if (type_3_c == 'Transfer' && _.isEmpty(this.model.get('current_location_c'))) {
			errors['current_location_c'] = errors['current_location_c'] || {};
			errors['current_location_c'].required = true;
		}

		if ((type_3_c == 'Shipping' || type_3_c == 'Archive' || deliverable_c == 'Shipping Request') && _.isEmpty(this.model.get('shipping_conditions_c'))) {
			errors['shipping_conditions_c'] = errors['shipping_conditions_c'] || {};
			errors['shipping_conditions_c'].required = true;
		}
		if ((type_3_c == 'Shipping' || type_3_c == 'Archive' || deliverable_c == 'Shipping Request') &&  _.isEmpty(this.model.get('hazardous_contents_c'))) {
			
			errors['hazardous_contents_c'] = errors['hazardous_contents_c'] || {};
			errors['hazardous_contents_c'].required = true;
		}
		if ((type_3_c == 'Shipping' || type_3_c == 'Archive' || type_3_c == 'Transfer') && (_.isEmpty(this.model.get('stability_considerations_c')))) {
			errors['stability_considerations_c'] = errors['stability_considerations_c'] || {};
			errors['stability_considerations_c'].required = true;
		}
        if ((type_3_c == 'Shipping' || type_3_c == 'Archive' || deliverable_c =='Shipping Request') && (_.isEmpty(this.model.get('shipping_company_c')) || shipping_company_c == null )) 
        {
            errors['shipping_company_c'] = errors['shipping_company_c'] || {};
			errors['shipping_company_c'].required = true;           
        }
		if ((shipping_company_c == 'Fed Ex' || shipping_company_c == 'On Time') && (_.isEmpty(this.model.get('shipping_delivery_c')) || shipping_delivery_c == null )) {
			errors['shipping_delivery_c'] = errors['shipping_delivery_c'] || {};
			errors['shipping_delivery_c'].required = true;
		}
		if (stability_considerations_c == 'Yes' && _.isEmpty(this.model.get('analyze_by_date_c'))) {
			errors['analyze_by_date_c'] = errors['analyze_by_date_c'] || {};
			errors['analyze_by_date_c'].required = true;
		}
		if ((type_3_c == 'Shipping' || type_3_c == 'Archive') && (_.isEmpty(this.model.get('type_2_c'))|| type_2_c == null)) {
			errors['type_2_c'] = errors['type_2_c'] || {};
			errors['type_2_c'].required = true;
		}
        
        if (deliverable_c == 'SpecimenSample Disposition'  && (_.isEmpty(this.model.get('type_3_c')) || type_3_c == null)) {
            errors['type_3_c'] = errors['type_3_c'] || {};
			errors['type_3_c'].required = true;
        }
		
		if (_.isEmpty(this.model.get('same_day_prep_c')) && (deliverable_c == 'Sample Prep Design') && (same_day_prep_c == '' || same_day_prep_c == null)) {
            errors['same_day_prep_c'] = errors['same_day_prep_c'] || {};
            errors['same_day_prep_c'].required = true;
        }
        if (_.isEmpty(this.model.get('neat_preparation_c')) && (deliverable_c == 'Sample Prep Design') && (neat_preparation_c == '' || neat_preparation_c == null)) {
            errors['neat_preparation_c'] = errors['neat_preparation_c'] || {};
            errors['neat_preparation_c'].required = true;
        }
        if (_.isEmpty(this.model.get('hdpe_eto_c')) && (deliverable_c == 'Sample Prep Design') && (hdpe_eto_c == '' || hdpe_eto_c == null)) {
            errors['hdpe_eto_c'] = errors['hdpe_eto_c'] || {};
            errors['hdpe_eto_c'].required = true;
        }
        /* if (_.isEmpty(this.model.get('number_test_implants_c')) && (deliverable_c == 'Sample Prep Design') && (number_test_implants_c == '' || number_test_implants_c == undefined)) {
            errors['number_test_implants_c'] = errors['number_test_implants_c'] || {};
            errors['number_test_implants_c'].required = true;
        }
        if (_.isEmpty(this.model.get('number_control_implants_c')) && (deliverable_c == 'Sample Prep Design') && (number_control_implants_c == '' || number_control_implants_c == undefined)) {
            errors['number_control_implants_c'] = errors['number_control_implants_c'] || {};
            errors['number_control_implants_c'].required = true;
        }        
        if (_.isEmpty(this.model.get('additional_supplies_required_c')) && (deliverable_c == 'Sample Prep Design')) {
            errors['additional_supplies_required_c'] = errors['additional_supplies_required_c'] || {};
            errors['additional_supplies_required_c'].required = true;
        }
        if (_.isEmpty(this.model.get('sample_prep_design_notes_c')) && (deliverable_c == 'Sample Prep Design')) {
            errors['sample_prep_design_notes_c'] = errors['sample_prep_design_notes_c'] || {};
            errors['sample_prep_design_notes_c'].required = true;
        }*/
        if (_.isEmpty(this.model.get('sample_prep_time_estimate_c')) && (deliverable_c == 'Sample Prep Design') && (sample_prep_time_estimate_c == '' || sample_prep_time_estimate_c == undefined)) {
            errors['sample_prep_time_estimate_c'] = errors['sample_prep_time_estimate_c'] || {};
            errors['sample_prep_time_estimate_c'].required = true;
        } 
		/* if (_.isEmpty(this.model.get('m03_work_product_m03_work_product_deliverable_1_name')) && (deliverable_c == 'Sample Prep Design')) {
            errors['m03_work_product_m03_work_product_deliverable_1_name'] = errors['m03_work_product_m03_work_product_deliverable_1_name'] || {};
            errors['m03_work_product_m03_work_product_deliverable_1_name'].required = true;
        } */
		if (_.isEmpty(this.model.get('work_type_c')) && (deliverable_c != 'Sample Prep Design') && (work_type_c == '' || work_type_c == null)) {
            console.log('work_type_c IN', work_type_c);
            errors['work_type_c'] = errors['work_type_c'] || {};
            errors['work_type_c'].required = true;
        }
		console.log('deliverable_status_c OUT', deliverable_status_c);
		/* if (_.isEmpty(this.model.get('deliverable_status_c')) && ((deliverable_c == 'Sample Prep Design') || (deliverable_c == ''))) {
            console.log('deliverable_status_c IN', deliverable_status_c);
            errors['deliverable_status_c'] = errors['deliverable_status_c'] || {};
            errors['deliverable_status_c'].required = false;
        } */
        if (_.isEmpty(this.model.get('deliverable_status_c')) && (deliverable_c != 'Sample Prep Design' ) && (deliverable_status_c == '' || deliverable_status_c == null)) {
			console.log('deliverable_status_c IN', deliverable_status_c);
			errors['deliverable_status_c'] = errors['deliverable_status_c'] || {};
			errors['deliverable_status_c'].required = true;
		}
		if (_.isEmpty(this.model.get('overdue_risk_level_c')) && (deliverable_c != 'Sample Prep Design')) {
            errors['overdue_risk_level_c'] = errors['overdue_risk_level_c'] || {};
            errors['overdue_risk_level_c'].required = true;
        }

		callback(null, fields, errors);
	},

	_doValidateCompleteDate: function(fields, errors, callback) {
		var deliverable_status_c = this.model.get('deliverable_status_c');
		var deliverable_c = this.model.get('deliverable_c');
		//validate requirements
		if (deliverable_status_c == 'Completed' && _.isEmpty(this.model.get('deliverable_completed_date_c'))) {
			errors['deliverable_completed_date_c'] = errors['deliverable_completed_date_c'] || {};
			errors['deliverable_completed_date_c'].required = true;
		}

		callback(null, fields, errors);
	},
	_doValidateDueDate: function(fields, errors, callback) {
		var due_date_c = this.model.get('due_date_c');
		var work_product_id = this.model.get('m03_work_p0b66product_ida');
		var deliverable_c = this.model.get('deliverable_c');
		if (work_product_id != '' && work_product_id != undefined) {
			var WP_API = "rest/v10/M03_Work_Product/" + work_product_id + "?fields=functional_area_c";
			App.api.call("get", WP_API, null, {
				success: function(WP_Data) {
					var new_allocated_wp_name = WP_Data.functional_area_c;
					// console.log('WP_API data : name : ', new_allocated_wp_name);
					if (new_allocated_wp_name != 'Standard Biocompatibility' && deliverable_c != 'Animal Selection' && deliverable_c != 'Animal Selection Populate TSD' && deliverable_c != 'Animal Selection Procedure Use' && deliverable_c != 'Sample Prep Design' && (due_date_c == '' || due_date_c == undefined)) {
						// console.log('contacts', new_allocated_wp_name);
						errors['due_date_c'] = errors['due_date_c'] || {};
						errors['due_date_c'].required = true;
					}
					callback(null, fields, errors);
				}
			});
		} else {
			callback(null, fields, errors);
		}
	},

	function_work_product_deliverable: function(model,interval_days) {
		var self = this;
		var work_product_id = this.model.get('m03_work_p0b66product_ida');
        var deliverable_c = this.model.get('deliverable_c');

        var relative_2 = this.model.get('relative_2_c');
		//var interval_days_2 = this.model.get('interval_days_2_c');
		console.log('interval_days condition ',interval_days);
		if(interval_days == undefined || interval_days == ""){
			console.log('interval_days condition 1 ');
			var interval_days_2 = this.model.get('interval_days_2_c');
		}else{
			console.log('interval_days condition 2 ');
			var interval_days_2 = $('[data-name="interval_days_2_c"] input[name="interval_days_2_c"]').val();
		}
		console.log('interval_days_2 condition 3 ',interval_days_2);
		console.log('m03_work_product_deliverable_id_c ',this.model.get("m03_work_product_deliverable_id_c"));

		if (work_product_id) {
			var url = app.api.buildURL("getWPDDueDates");
			var method = "create";
			var data = {
				work_product_id: work_product_id,
                deliverable_c: deliverable_c,
                relative_2 :relative_2,
				interval_days_2:interval_days_2,
				wpdId:this.model.get('id'),
                related_deliverable_2_id:this.model.get("m03_work_product_deliverable_id_c")
			};
			console.log('data res', data);
			var callback = {
				success: _.bind(function successCB(res) {
					console.log('res', res);
					if (res != null) {
						if ((res.final_due_date_c != null || res.final_due_date_c != '') && (res.due_date_c != null || res.due_date_c != '')) {
							this.model.set("final_due_date_c", res.final_due_date_c);
							if(res.due_date_c != undefined){
								this.model.set("due_date_c", res.due_date_c);
							}
						}

						 
						if((res.final_due_date_c != null || res.final_due_date_c != '') && relative_2 == 'To First Procedure Date' || relative_2 == 'To Last Term Date' || relative_2 == 'To Other Work Product Deliverable'){
							this.model.set("final_due_date_c", res.final_due_date_c);	
						}
					}
				}, this)
			};
			app.api.call(method, url, data, callback);
		}

	},
	//This function set the value into Relative and Interval days from the Selected Deliverable Template
	getRelativeFromDT: function() {
		var deliverableTemplateId = this.model.get('dt_delivercf79emplate_ida');
		var relativeValue = this.model.get('relative_2_c');
		if (deliverableTemplateId == "") {
			this.model.set("interval_days_2_c", ""); // if the deliverable template value blank thn interval days must be blank
		}
		if (deliverableTemplateId !== "") {
			var url = app.api.buildURL("getValuesFromDT");
			var method = "create";
			var data = {
				deliverableTemplateId: deliverableTemplateId,
				relativeValue: relativeValue
			};
			var callback = {
				success: _.bind(function successCB(res) {
					if (res != null) {
						this.model.set("relative_2_c", res.relative);
						if ((res.relative == 'To First Procedure Date' || res.relative == 'To Last Term Date' || res.relative == 'To Other Work Product Deliverable') && deliverableTemplateId !== null) {
							this.model.set("interval_days_2_c", res.intervalDaysValue);
						}
					}
				}, this)
			};
			app.api.call(method, url, data, callback);
		}
	},

	edit_button: function() {
		var deliverableTemplate = this.model.get('dt_deliverable_template_m03_work_product_deliverable_1_name');
		var relativeVal = this.model.get('relative_2_c');
		if (relativeVal == 'To Other Work Product Deliverable' && deliverableTemplate != null && deliverableTemplate != "") {
			$('input[name="interval_days_2_c"]').prop('disabled', true);
			$('div[data-name="interval_days_2_c"]').css('pointer-events', 'none');
			$('input[name="related_deliverable_2_c"]').prop('disabled', true);
			$('div[data-name="related_deliverable_2_c"]').css('pointer-events', 'none');
		}
		if ((relativeVal == 'To First Procedure Date' || relativeVal == 'To Last Term Date' || relativeVal == 'To Other Work Product Deliverable') && deliverableTemplate != null && deliverableTemplate != "") {
			$('input[name="relative_2_c"]').prop('disabled', true);
			$('div[data-name="relative_2_c"]').css('pointer-events', 'none');
			$('input[name="interval_days_2_c"]').prop('disabled', true);
			$('div[data-name="interval_days_2_c"]').css('pointer-events', 'none');
		}
	},
	cancel_button: function() {
		var deliverableTemplate = this.model.get('dt_deliverable_template_m03_work_product_deliverable_1_name');
		var relativeVal = this.model.get('relative_2_c');
		if (relativeVal == 'To Other Work Product Deliverable' && deliverableTemplate != null && deliverableTemplate != "") {
			$('input[name="interval_days_2_c"]').prop('disabled', true);
			$('div[data-name="interval_days_2_c"]').css('pointer-events', 'none');
			$('input[name="related_deliverable_2_c"]').prop('disabled', true);
			$('div[data-name="related_deliverable_2_c"]').css('pointer-events', 'none');
		}
		if ((relativeVal == 'To First Procedure Date' || relativeVal == 'To Last Term Date' || relativeVal == 'To Other Work Product Deliverable') && deliverableTemplate != null && deliverableTemplate != "") {
			$('input[name="relative_2_c"]').prop('disabled', true);
			$('div[data-name="relative_2_c"]').css('pointer-events', 'none');
			$('input[name="interval_days_2_c"]').prop('disabled', true);
			$('div[data-name="interval_days_2_c"]').css('pointer-events', 'none');
		}
	},

	function_select_template: function() {
		var self = this;
		var relative_2_c = this.model.get('relative_2_c');
        console.log(relative_2_c);
		 
		if(relative_2_c == 'To First Procedure Date' || relative_2_c == 'To Last Term Date' || relative_2_c == 'To Other Work Product Deliverable'){
           this.function_work_product_deliverable();
		}
		var relative_template_c = this.model.get('relative_template_c');
		if (relative_2_c == 'To Other Work Product Deliverable') {
			$('[data-name="related_deliverable_2_c"]').css("visibility", "visible"); 
			$('[data-name="related_deliverable_2_c"]').css("height", "auto"); 

		} else {
			$('[data-name="related_deliverable_2_c"]').css("visibility", "hidden"); 
			$('[data-name="related_deliverable_2_c"]').css("height", "0"); 
		}
		if (relative_2_c == 'No') {
			$('[data-name="interval_days_2_c"]').css("visibility", "hidden"); 
			$('[data-name="interval_days_2_c"]').css("height", "0");
		} else {
			$('[data-name="interval_days_2_c"]').css("visibility", "visible"); 
			$('[data-name="interval_days_2_c"]').css("height", "auto");
		}

		var deliverable_c = this.model.get('deliverable_c');
		if (deliverable_c == 'SpecimenSample Disposition') {
			$('[data-name="type_3_c"]').css("visibility", "visible"); 
			$('[data-name="filename"]').css("visibility", "visible");
			$('[data-name="type_3_c"]').css("height", "auto");
			$('[data-name="filename"]').css("height", "auto");                                   
		} else {
			$('[data-name="type_3_c"]').css("visibility", "hidden");
			$('[data-name="filename"]').css("visibility", "hidden"); 
			$('[data-name="type_3_c"]').css("height", "0");
			$('[data-name="filename"]').css("height", "0"); 
		}
		if (deliverable_c == 'Slide Scanning') {
			$('[data-name="slide_type_c"]').css("visibility", "visible"); 
			$('[data-name="slide_type_c"]').css("height", "auto");                                                        
		} else {
			$('[data-name="slide_type_c"]').css("visibility", "hidden");
			$('[data-name="slide_type_c"]').css("height", "0"); 
		}
		if (deliverable_c == 'Histopathology Report2' || deliverable_c == 'Histopathology Report' || deliverable_c == 'Interim Gross Pathology Report' || deliverable_c == 'Interim Histopathology Report2' || deliverable_c == 'Interim Histopathology Report' || deliverable_c == 'Histopathology Notes') {
			$('[data-name="pathologist_on_call_c"]').css("visibility", "visible");   
			$('[data-name="pathologist_on_call_c"]').css("height", "auto");                                    
		} else {
			$('[data-name="pathologist_on_call_c"]').css("visibility", "hidden"); 
			$('[data-name="pathologist_on_call_c"]').css("height", "0");  
		}                
		if (deliverable_c == 'Gross Pathology Report' || deliverable_c == 'Histopathology Report2' || deliverable_c == 'Histopathology Report' || deliverable_c == 'Interim Gross Pathology Report' || deliverable_c == 'Interim Histopathology Report2' || deliverable_c == 'Interim Histopathology Report' || deliverable_c == 'Histopathology Notes') {
			$('[data-name="planned_bd_to_draft_c"]').css("visibility", "visible");  
			$('[data-name="actual_bc_to_complete_draft_c"]').css("visibility", "visible");  
			$('[data-name="actual_bc_to_complete_draft_c"]').css("height", "auto");                                    
		} else {
			$('[data-name="planned_bd_to_draft_c"]').css("visibility", "hidden"); 
			$('[data-name="actual_bc_to_complete_draft_c"]').css("visibility", "hidden");
			$('[data-name="actual_bc_to_complete_draft_c"]').css("height", "0"); 
		}
		if (deliverable_c == 'Final Report Amendment') {
			$('[data-name="amendment_responsibility_c"]').css("visibility", "visible");   
			$('[data-name="reason_for_amendment_c"]').css("visibility", "visible");
			$('[data-name="amendment_responsibility_c"]').css("height", "auto"); 
			$('[data-name="reason_for_amendment_c"]').css("height", "auto");                                    
		} else {
			$('[data-name="amendment_responsibility_c"]').css("visibility", "hidden"); 
			$('[data-name="reason_for_amendment_c"]').css("visibility", "hidden");
			$('[data-name="amendment_responsibility_c"]').css("height", "0"); 
			$('[data-name="reason_for_amendment_c"]').css("height", "0");  
		}
		if (deliverable_c != 'Animal Selection' && deliverable_c != 'Animal Selection Populate TSD' && deliverable_c != 'Animal Selection Procedure Use' && deliverable_c != 'Sample Prep Design') {
			$('[data-name="due_date_c"]').css("visibility", "visible"); 
			$('[data-name="due_date_c"]').css("height", "auto");   
			$('[data-name="internal_final_due_date_c"]').css("visibility", "visible");  
			$('[data-name="internal_final_due_date_c"]').css("height", "auto");                                     
		} else {
			$('[data-name="due_date_c"]').css("visibility", "hidden"); 
			$('[data-name="due_date_c"]').css("height", "0"); 
			$('[data-name="internal_final_due_date_c"]').css("visibility", "hidden"); 
			$('[data-name="internal_final_due_date_c"]').css("height", "0"); 
		}
		
		if (deliverable_c == 'Paraffin Slide Completion' || deliverable_c == 'EXAKT Slide Completion' || deliverable_c == 'Plastic Microtome Slide Completion' || deliverable_c == 'Frozen Slide Completion' || deliverable_c == 'Slide Scanning' || deliverable_c == 'Histopathology Report2' || deliverable_c == 'Interim Histopathology Report2' || deliverable_c == 'Histopathology Report' || deliverable_c == 'Interim Histopathology Report' || deliverable_c == 'Histopathology Notes') {
			$('[data-name="standard_he_slides_c"]').css("visibility", "visible"); 
			$('[data-name="standard_special_stain_c"]').css("visibility", "visible"); 
			$('[data-name="large_he_slides_c"]').css("visibility", "visible");      
			$('[data-name="large_special_stain_c"]').css("visibility", "visible");
			
			$('[data-name="standard_he_slides_c"]').css("height", "auto");
			$('[data-name="standard_special_stain_c"]').css("height", "auto");
			$('[data-name="large_he_slides_c"]').css("height", "auto");
			$('[data-name="large_special_stain_c"]').css("height", "auto");

		} else {
			$('[data-name="standard_he_slides_c"]').css("visibility", "hidden"); 
			$('[data-name="standard_special_stain_c"]').css("visibility", "hidden"); 
			 $('[data-name="large_he_slides_c"]').css("visibility", "hidden"); 
			 $('[data-name="large_special_stain_c"]').css("visibility", "hidden"); 

			 $('[data-name="standard_he_slides_c"]').css("height", "0");
			 $('[data-name="standard_special_stain_c"]').css("height", "0");
			 $('[data-name="large_he_slides_c"]').css("height", "0");
			 $('[data-name="large_special_stain_c"]').css("height", "0");
		}
		if (deliverable_c == 'Paraffin Slide Completion' || deliverable_c == 'Plastic Microtome Slide Completion' || deliverable_c == 'Frozen Slide Completion') {
			$('[data-name="standard_unstained_slides_c"]').css("visibility", "visible"); 
			$('[data-name="standard_unstained_slides_c"]').css("height", "auto");
		  
		} else {
			$('[data-name="standard_unstained_slides_c"]').css("visibility", "hidden"); 
			$('[data-name="standard_unstained_slides_c"]').css("height", "0");
		}
		if (deliverable_c == 'Histopathology Report2' || deliverable_c == 'Histopathology Report' || deliverable_c == 'Interim Histopathology Report2' || deliverable_c == 'Interim Histopathology Report' || deliverable_c == 'Histopathology Notes') {
			$('[data-name="minutes_per_slide_c"]').css("visibility", "visible"); 
			$('[data-name="total_number_of_slides_c"]').css("visibility", "visible"); 
			$('[data-name="report_writing_days_c"]').css("visibility", "visible"); 
			$('[data-name="minutes_per_slide_c"]').css("height", "auto");
			$('[data-name="total_number_of_slides_c"]').css("height", "auto");
			$('[data-name="report_writing_days_c"]').css("height", "auto");
		} else {
			$('[data-name="minutes_per_slide_c"]').css("visibility", "hidden"); 
			$('[data-name="total_number_of_slides_c"]').css("visibility", "hidden"); 
			$('[data-name="report_writing_days_c"]').css("visibility", "hidden"); 

			$('[data-name="minutes_per_slide_c"]').css("height", "0");
			$('[data-name="total_number_of_slides_c"]').css("height", "0");
			$('[data-name="report_writing_days_c"]').css("height", "0");
		}
		if (deliverable_c == 'Histopathology Report2' || deliverable_c == 'Histopathology Report' || deliverable_c == 'Interim Histopathology Report2' || deliverable_c == 'Interim Histopathology Report' || deliverable_c == 'Histopathology Notes') {
			$('[data-name="pathologist_participation_c"]').css("visibility", "visible");
			$('[data-name="pathologist_participation_c"]').css("height", "auto");
		} else {
			$('[data-name="pathologist_participation_c"]').css("visibility", "hidden");
			$('[data-name="pathologist_participation_c"]').css("height", "0");

		}
		if(deliverable_c == '' ){
			console.log("On line 195");
			$('[data-name="deliverable_status_c"]').css("visibility", "hidden");
			$('[data-name="deliverable_status_c"]').css("height", "0");
		}else if (deliverable_c == 'Sample Prep Design') {
			$('[data-name="same_day_prep_c"]').css("visibility", "visible");   
			$('[data-name="neat_preparation_c"]').css("visibility", "visible");
			$('[data-name="hdpe_eto_c"]').css("visibility", "visible");   
			$('[data-name="number_test_implants_c"]').css("visibility", "visible");
			$('[data-name="number_control_implants_c"]').css("visibility", "visible");   
			$('[data-name="sample_prep_time_estimate_c"]').css("visibility", "visible");
			$('[data-name="additional_supplies_required_c"]').css("visibility", "visible");   
			$('[data-name="sample_prep_design_notes_c"]').css("visibility", "visible"); 
			
			//$('[data-name="assigned_user_name"]').css("visibility", "visible"); 
			//$('[data-name="m03_work_product_m03_work_product_deliverable_1_name"]').css("visibility", "visible");
			$('[data-name="same_day_prep_c"]').css("height", "auto"); 
			$('[data-name="neat_preparation_c"]').css("height", "auto"); 
			$('[data-name="hdpe_eto_c"]').css("height", "auto"); 
			$('[data-name="number_test_implants_c"]').css("height", "auto"); 
			$('[data-name="number_control_implants_c"]').css("height", "auto"); 
			$('[data-name="sample_prep_time_estimate_c"]').css("height", "auto"); 
			$('[data-name="additional_supplies_required_c"]').css("height", "auto"); 
			$('[data-name="sample_prep_design_notes_c"]').css("height", "auto"); 
			
			//$('[data-name="assigned_user_name"]').css("height", "auto");
			//$('[data-name="m03_work_product_m03_work_product_deliverable_1_name"]').css("height", "auto");   

			$('[data-name="timeline_type_c"]').css("visibility", "hidden"); 
			$('[data-name="work_type_c"]').css("visibility", "hidden");
			$('[data-name="bc_send_draft_report_c"]').css("visibility", "hidden");
			$('[data-name="redlines_c"]').css("visibility", "hidden");
			$('[data-name="draft_deliverable_sent_date_c"]').css("visibility", "hidden");
			$('[data-name="deliverable_status_c"]').css("visibility", "hidden");
			$('[data-name="overdue_risk_level_c"]').css("visibility", "hidden");
			 
			$('[data-name="work_type_c"]').css("height", "0"); 
			$('[data-name="bc_send_draft_report_c"]').css("height", "0"); 
			$('[data-name="redlines_c"]').css("height", "0"); 
			$('[data-name="draft_deliverable_sent_date_c"]').css("height", "0"); 
			$('[data-name="deliverable_status_c"]').css("height", "0");
			$('[data-name="overdue_risk_level_c"]').css("height", "0");  

		} else {
			$('[data-name="same_day_prep_c"]').css("visibility", "hidden"); 
			$('[data-name="neat_preparation_c"]').css("visibility", "hidden");
			$('[data-name="hdpe_eto_c"]').css("visibility", "hidden");   
			$('[data-name="number_test_implants_c"]').css("visibility", "hidden");
			$('[data-name="number_control_implants_c"]').css("visibility", "hidden");   
			$('[data-name="sample_prep_time_estimate_c"]').css("visibility", "hidden");
			$('[data-name="additional_supplies_required_c"]').css("visibility", "hidden");   
			$('[data-name="sample_prep_design_notes_c"]').css("visibility", "hidden");
			
			//$('[data-name="assigned_user_name"]').css("visibility", "hidden");
			//$('[data-name="m03_work_product_m03_work_product_deliverable_1_name"]').css("visibility", "hidden");
			$('[data-name="same_day_prep_c"]').css("height", "0"); 
			$('[data-name="neat_preparation_c"]').css("height", "0"); 
			$('[data-name="hdpe_eto_c"]').css("height", "0"); 
			$('[data-name="number_test_implants_c"]').css("height", "0"); 
			$('[data-name="number_control_implants_c"]').css("height", "0"); 
			$('[data-name="sample_prep_time_estimate_c"]').css("height", "0"); 
			$('[data-name="additional_supplies_required_c"]').css("height", "0"); 
			$('[data-name="sample_prep_design_notes_c"]').css("height", "0");
			  
			//$('[data-name="assigned_user_name"]').css("height", "0");  
			//$('[data-name="m03_work_product_m03_work_product_deliverable_1_name"]').css("height", "0");
			$('[data-name="deliverable_status_c"]').css("visibility", "visible");
			$('[data-name="overdue_risk_level_c"]').css("visibility", "visible");
			$('[data-name="timeline_type_c"]').css("visibility", "visible");
			$('[data-name="work_type_c"]').css("visibility", "visible"); 
			$('[data-name="bc_send_draft_report_c"]').css("visibility", "visible"); 
			$('[data-name="redlines_c"]').css("visibility", "visible"); 
			$('[data-name="draft_deliverable_sent_date_c"]').css("visibility", "visible");  
			
			$('[data-name="deliverable_status_c"]').css("height", "auto"); 
			$('[data-name="work_type_c"]').css("height", "auto");
			$('[data-name="bc_send_draft_report_c"]').css("height", "auto");
			$('[data-name="redlines_c"]').css("height", "auto");
			$('[data-name="draft_deliverable_sent_date_c"]').css("height", "auto");
			$('[data-name="overdue_risk_level_c"]').css("height", "auto");
		}
		var type_3_c = this.model.get('type_3_c');
		if (type_3_c == 'Transfer') {
			$('[data-name="datetime_desired_c"]').css("visibility", "visible"); 
			$('[data-name="current_location_c"]').css("visibility", "visible");  
			$('[data-name="transfer_to_location_c"]').css("visibility", "visible");   
			
			$('[data-name="datetime_desired_c"]').css("height", "auto");
			$('[data-name="current_location_c"]').css("height", "auto");
			$('[data-name="transfer_to_location_c"]').css("height", "auto");

		} else {
			$('[data-name="datetime_desired_c"]').css("visibility", "hidden"); 
			$('[data-name="current_location_c"]').css("visibility", "hidden"); 
			$('[data-name="transfer_to_location_c"]').css("visibility", "hidden");

			$('[data-name="datetime_desired_c"]').css("height", "0");
			$('[data-name="current_location_c"]').css("height", "0");
			$('[data-name="transfer_to_location_c"]').css("height", "0");
		}
		if (type_3_c == 'Shipping' || type_3_c == 'Archive' || deliverable_c =='Shipping Request' ) {
			$('[data-name="shipping_conditions_c"]').css("visibility", "visible");  
			$('[data-name="type_2_c"]').css("visibility", "visible"); 
			$('[data-name="shipping_company_c"]').css("visibility", "visible");  
			$('[data-name="hazardous_contents_c"]').css("visibility", "visible");
			
			$('[data-name="shipping_conditions_c"]').css("height", "auto");
			$('[data-name="type_2_c"]').css("height", "auto");
			$('[data-name="shipping_company_c"]').css("height", "auto");
			$('[data-name="hazardous_contents_c"]').css("height", "auto");

		} else {
			$('[data-name="shipping_conditions_c"]').css("visibility", "hidden");
			$('[data-name="type_2_c"]').css("visibility", "hidden"); 
			$('[data-name="shipping_company_c"]').css("visibility", "hidden"); 
			$('[data-name="hazardous_contents_c"]').css("visibility", "hidden");
			
			$('[data-name="shipping_conditions_c"]').css("height", "0");
			$('[data-name="type_2_c"]').css("height", "0");
			$('[data-name="shipping_company_c"]').css("height", "0");
			$('[data-name="hazardous_contents_c"]').css("height", "0");
		}
		if (type_3_c == 'Shipping' || type_3_c == 'Archive' || type_3_c == 'Transfer') {
			$('[data-name="stability_considerations_c"]').css("visibility", "visible");
			$('[data-name="stability_considerations_c"]').css("height", "auto");                               
		} else {
			$('[data-name="stability_considerations_c"]').css("visibility", "hidden"); 
			$('[data-name="stability_considerations_c"]').css("height", "0");
		}
		if (type_3_c == 'Shipping') {
			$('[data-name="receiving_party_expecting_c"]').css("visibility", "visible");
			$('[data-name="attn_to_name_c"]').css("visibility", "visible");
			$('[data-name="address_street_c"]').css("visibility", "visible");
			$('[data-name="address_city_c"]').css("visibility", "visible");
			$('[data-name="address_state_c"]').css("visibility", "visible");
			$('[data-name="address_postalcode_c"]').css("visibility", "visible");
			$('[data-name="phone_number_c"]').css("visibility", "visible");
			$('[data-name="address_confirmed_c"]').css("visibility", "visible");
			$('[data-name="faxitron_required_c"]').css("visibility", "visible");
			$('[data-name="address_country_c"]').css("visibility", "visible");

			$('[data-name="receiving_party_expecting_c"]').css("height", "auto");
			$('[data-name="attn_to_name_c"]').css("height", "auto");
			$('[data-name="address_street_c"]').css("height", "auto");
			$('[data-name="address_city_c"]').css("height", "auto");
			$('[data-name="address_state_c"]').css("height", "auto");
			$('[data-name="address_postalcode_c"]').css("height", "auto");
			$('[data-name="phone_number_c"]').css("height", "auto");
			$('[data-name="address_confirmed_c"]').css("height", "auto");
			$('[data-name="faxitron_required_c"]').css("height", "auto");
			$('[data-name="address_country_c"]').css("height", "auto");
		} else {
			$('[data-name="receiving_party_expecting_c"]').css("visibility", "hidden");
			$('[data-name="attn_to_name_c"]').css("visibility", "hidden");
			$('[data-name="address_street_c"]').css("visibility", "hidden");
			$('[data-name="address_city_c"]').css("visibility", "hidden");
			$('[data-name="address_state_c"]').css("visibility", "hidden");
			$('[data-name="address_postalcode_c"]').css("visibility", "hidden");
			$('[data-name="phone_number_c"]').css("visibility", "hidden");
			$('[data-name="address_confirmed_c"]').css("visibility", "hidden");
			$('[data-name="faxitron_required_c"]').css("visibility", "hidden");
			$('[data-name="address_country_c"]').css("visibility", "hidden");

			$('[data-name="receiving_party_expecting_c"]').css("height", "0");
			$('[data-name="attn_to_name_c"]').css("height", "0");
			$('[data-name="address_street_c"]').css("height", "0");
			$('[data-name="address_city_c"]').css("height", "0");
			$('[data-name="address_state_c"]').css("height", "0");
			$('[data-name="address_postalcode_c"]').css("height", "0");
			$('[data-name="phone_number_c"]').css("height", "0");
			$('[data-name="address_confirmed_c"]').css("height", "0");
			$('[data-name="faxitron_required_c"]').css("height", "0");
			$('[data-name="address_country_c"]').css("height", "0");
		}

		var deliverable_status_c = this.model.get('deliverable_status_c');
		if (deliverable_status_c == 'Completed') {
			$('[data-name="deliverable_completed_date_c"]').css("visibility", "visible");   
			$('[data-name="deliverable_completed_date_c"]').css("height", "auto");                                  
		} else {
			$('[data-name="deliverable_completed_date_c"]').css("visibility", "hidden"); 
			$('[data-name="deliverable_completed_date_c"]').css("height", "0");
		}

		var stability_considerations_c = this.model.get('stability_considerations_c');
		if (stability_considerations_c == 'Yes') {
			$('[data-name="analyze_by_date_c"]').css("visibility", "visible"); 
			$('[data-name="analyze_by_date_c"]').css("height", "auto");
		} else {
			$('[data-name="analyze_by_date_c"]').css("visibility", "hidden"); 
			$('[data-name="analyze_by_date_c"]').css("height", "0");
		}

		var shipping_company_c = this.model.get('shipping_company_c');
		if (shipping_company_c == 'Fed Ex' || shipping_company_c == 'On Time') {
			$('[data-name="shipping_delivery_c"]').css("visibility", "visible");
			$('[data-name="shipping_delivery_c"]').css("height", "auto");
		} else {
			$('[data-name="shipping_delivery_c"]').css("visibility", "hidden");
			$('[data-name="shipping_delivery_c"]').css("height", "0");
		}
	},

})