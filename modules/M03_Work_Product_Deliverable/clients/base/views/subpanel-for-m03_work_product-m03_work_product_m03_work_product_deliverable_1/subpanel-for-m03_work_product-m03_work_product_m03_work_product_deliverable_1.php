<?php
// created: 2022-07-14 07:23:44
$viewdefs['M03_Work_Product_Deliverable']['base']['view']['subpanel-for-m03_work_product-m03_work_product_m03_work_product_deliverable_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'enabled' => true,
          'link' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'timeline_type_c',
          'label' => 'LBL_TIMELINE_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'deliverable_c',
          'label' => 'LBL_DELIVERABLE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
          'enabled' => true,
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'due_date_c',
          'label' => 'LBL_DUE_DATE',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'final_due_date_c',
          'label' => 'LBL_FINAL_DUE_DATE',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'department_manager_approval_c',
          'label' => 'LBL_DEPARTMENT_MANAGER_APPROVAL',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'draft_deliverable_sent_date_c',
          'label' => 'LBL_DRAFT_DELIVERABLE_SENT_DATE',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'deliverable_status_c',
          'label' => 'LBL_DELIVERABLE_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'deliverable_completed_date_c',
          'label' => 'LBL_DELIVERABLE_COMPLETED_DATE',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'author_c',
          'label' => 'LBL_AUTHOR',
          'enabled' => true,
          'readonly' => false,
          'id' => 'CONTACT_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);