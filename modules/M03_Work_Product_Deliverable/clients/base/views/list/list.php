<?php
$module_name = 'M03_Work_Product_Deliverable';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              2 => 
              array (
                'name' => 'type_2_c',
                'label' => 'LBL_TYPE_2',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'address_country_c',
                'label' => 'LBL_ADDRESS_COUNTRY',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'deliverable_status_c',
                'label' => 'LBL_DELIVERABLE_STATUS',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'final_due_date_c',
                'label' => 'LBL_FINAL_DUE_DATE',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'timeline_type_c',
                'label' => 'LBL_TIMELINE_TYPE',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'deliverable_completed_date_c',
                'label' => 'LBL_DELIVERABLE_COMPLETED_DATE',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'due_date_c',
                'label' => 'LBL_DUE_DATE',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'draft_deliverable_sent_date_c',
                'label' => 'LBL_DRAFT_DELIVERABLE_SENT_DATE',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'author_c',
                'label' => 'LBL_AUTHOR',
                'enabled' => true,
                'id' => 'CONTACT_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'bc_send_draft_report_c',
                'label' => 'LBL_BC_SEND_DRAFT_REPORT',
                'enabled' => true,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'internal_final_due_date_c',
                'label' => 'LBL_INTERNAL_FINAL_DUE_DATE',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
