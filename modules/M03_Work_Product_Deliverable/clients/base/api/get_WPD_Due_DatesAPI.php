<?php

if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

class get_WPD_Due_DatesAPI extends SugarApi
{

	public function registerApiRest()
	{
		return array(
			'getWPDDueDates' => array(
				'reqType' => 'POST',
				'noLoginRequired' => false,
				'path' => array('getWPDDueDates'),
				'pathVars' => array('getWPDDueDates'),
				'method' => 'getWPDDueDates',
				'shortHelp' => '',
				'longHelp' => '',
			),
		);
	}


	function getWPDDueDates($api, $args)
	{
		global $db, $current_user;

		$work_product_id	= $args['work_product_id'];
		$wpd_id				= $args['wpd_id'];
		$deliverable_c		= $args['deliverable_c'];
		$data				= array();

		$bean		= BeanFactory::retrieveBean('M03_Work_Product', $work_product_id);
		$wpname		= $bean->name;
		$num_rows	= $this->get_record_wpd($wpname);
		
		if ($bean->functional_area_c == 'Standard Biocompatibility' && $bean->report_timeline_type_c == 'By First Procedure' && ($bean->first_procedure_c != "" || $bean->first_procedure_c != null)) {
			if ($deliverable_c == "Final Report") {
				//$GLOBALS['log']->fatal("Final Report " . $deliverable_c);
				if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
					$Days = ' + 0 days';
					//$DueDays = ' - 7 days';
					$finalDueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $Days));
					//$DueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $DueDays));
				} else {
					$Days = 7 * $bean->final_report_timeline_c;
					//$DueDays = (7 * $bean->final_report_timeline_c) - 7;

					$finalDueDate = date('Y-m-d', strtotime($bean->first_procedure_c . ' + ' . $Days . ' days'));
					//$DueDate = date('Y-m-d', strtotime($bean->first_procedure_c . ' + ' . $DueDays . ' days'));
				}

				if ($num_rows > 0) {
					if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
						//$DueDays = ' - 5 days';
						//$DueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $DueDays));
						$minusDays = 5; 
						$DueDate = $this->getDaysCalculation($bean->first_procedure_c,$minusDays);
								
					} else {
						$DueDays = (7 * $bean->final_report_timeline_c) ;//- 5
						$DueDate1 = date('Y-m-d', strtotime($bean->first_procedure_c . ' + ' . $DueDays . ' days'));
						
						$minusDays = 5; 
						$DueDate = $this->getDaysCalculation($DueDate1,$minusDays);
					}
				} else {
					if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
						//$DueDays = ' - 3 days';
						//$DueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $DueDays));
						
						$minusDays	= 3; 
						$DueDate	= $this->getDaysCalculation($bean->first_procedure_c,$minusDays);
						
					} else {
						$DueDays = (7 * $bean->final_report_timeline_c) ;//- 3
						$DueDate1 = date('Y-m-d', strtotime($bean->first_procedure_c . ' + ' . $DueDays . ' days'));
						
						$minusDays	= 3; 
						$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
					}
				}

				$final_due_date_c = $finalDueDate;
				$due_date_c = $DueDate;
			} else if ($deliverable_c == "Histopathology Report") {
				if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
					$Days = ' + 0 days';
					$DueDays = ' - 6 days';

					$finalDueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $Days));
					//$DueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $DueDays));
					$minusDays	= 6; 
					$DueDate	= $this->getDaysCalculation($bean->first_procedure_c,$minusDays);
						
				} else {
					$Days = 7 * $bean->final_report_timeline_c;
					$finalDueDate = date('Y-m-d', strtotime($bean->first_procedure_c . ' + ' . $Days . ' days'));

					if ($bean->final_report_timeline_c == 1) {
						//$DueDays = ' - 1 days';
						//$DueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $DueDays));
						$minusDays	= 1; 
						$DueDate	= $this->getDaysCalculation($bean->first_procedure_c,$minusDays);
					
					
					} else {
						$DueDays	= (7 * $bean->final_report_timeline_c) ;//- 6
						$DueDate1	= date('Y-m-d', strtotime($bean->first_procedure_c . ' + ' . $DueDays . ' days'));
						$minusDays	= 6; 
						$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
					
					}
				}

				$final_due_date_c = $finalDueDate;
				$due_date_c = $DueDate;
			}
			$data  = array("final_due_date_c" => $final_due_date_c, 'due_date_c' => $due_date_c);
			return $data;
		} else if ($bean->functional_area_c == 'Standard Biocompatibility' && $bean->report_timeline_type_c == 'By SPA Reconciliation' && ($bean->bc_spa_reconciled_date_c != "" || $bean->bc_spa_reconciled_date_c != null)) {
			if ($deliverable_c == "Final Report") {

				if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
					$Days = ' + 0 days';
					//$DueDays = ' - 7 days';

					$finalDueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $Days));
					//$DueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $DueDays));
				} else {
					$Days = 7 * $bean->final_report_timeline_c;
					//$DueDays = (7 * $bean->final_report_timeline_c) - 7;

					$finalDueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . ' + ' . $Days . ' days'));
					//$DueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . ' + ' . $DueDays . ' days'));
				}

				if ($num_rows > 0) {
					if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
						//$DueDays = ' - 5 days';
						//$DueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $DueDays));
						
						$minusDays	= 5; 
						$DueDate	= $this->getDaysCalculation($bean->bc_spa_reconciled_date_c,$minusDays);
						
					} else {
						$DueDays	= (7 * $bean->final_report_timeline_c); //- 5
						$DueDate1	= date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . ' + ' . $DueDays . ' days'));
						$minusDays	= 5; 
						$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
						
					}
				} else {
					if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
						//$DueDays = ' - 3 days';
						//$DueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $DueDays));
						$minusDays	= 3; 
						$DueDate	= $this->getDaysCalculation($bean->bc_spa_reconciled_date_c,$minusDays);
						
					} else {
						$DueDays = (7 * $bean->final_report_timeline_c); //- 3
						$DueDate1 = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . ' + ' . $DueDays . ' days'));
						$minusDays	= 3; 
						$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
					}
				}

				$final_due_date_c = $finalDueDate;
				$due_date_c = $DueDate;
			} else if ($deliverable_c == "Histopathology Report") {
				if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
					$Days = ' + 0 days';
					$DueDays = ' - 6 days';
					$finalDueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $Days));
					//$DueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $DueDays));
					$minusDays	= 6; 
					$DueDate	= $this->getDaysCalculation($bean->bc_spa_reconciled_date_c,$minusDays);
				} else {
					$Days = 7 * $bean->final_report_timeline_c;
					$finalDueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . ' + ' . $Days . ' days'));

					if ($bean->final_report_timeline_c == 1) {
						//$DueDays = ' - 1 days';
						//$DueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $DueDays));
						$minusDays	= 1; 
						$DueDate	= $this->getDaysCalculation($bean->bc_spa_reconciled_date_c,$minusDays);
					} else {
						$DueDays = (7 * $bean->final_report_timeline_c) ;//- 6
						$DueDate1 = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . ' + ' . $DueDays . ' days'));
						$minusDays	= 6; 
						$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
					}
				}

				$final_due_date_c = $finalDueDate;
				$due_date_c = $DueDate;
			}
			$data  = array("final_due_date_c" => $final_due_date_c, 'due_date_c' => $due_date_c);
			return $data;
		}
	}
	
	function getDaysCalculation($actualDate,$subDays){
		$t = strtotime($actualDate);
 
		for($i=$subDays; $i>0; $i--){
			$oneDay = 86400;// add 1 day to timestamp
			$nextDay = date('w', ($t-$oneDay));// get what day it is next day
			// if it's Saturday or Sunday get $i-1
			if($nextDay == 0 || $nextDay == 6) {
				$i++;
			}
			
			$t = $t-$oneDay;// modify timestamp, add 1 day
		}
		//$GLOBALS['log']->fatal('getDaysCalculation:' . date('Y-m-d',$t));		
		return date('Y-m-d',$t);		
	}

	function get_record_wpd($wpname)
	{
		//$GLOBALS['log']->fatal("num_rows2:WPD " . $wpname);
		global $db, $current_user;
		$queryCustom = "SELECT  
		WPD.id AS id		
		FROM `m03_work_product_deliverable` AS WPD
			LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
				ON WPD.id= WPDCSTM.id_c
			RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
				ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
			LEFT JOIN m03_work_product AS WP
				ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
			LEFT JOIN m03_work_product_cstm AS WPCSTM
				ON WP.id=WPCSTM.id_c
		WHERE  WP.name = '" . $wpname . "' 
		AND WPDCSTM.deliverable_c ='Histopathology Report' AND WPD.deleted = 0
		ORDER BY WPDCSTM.due_date_c DESC";
		$queryResult = $db->query($queryCustom);
		$fetchResult = $db->fetchByAssoc($queryResult);
		$num_rows = $queryResult->num_rows;
		//$GLOBALS['log']->fatal("num_rows:WPD " . $num_rows);
		return $num_rows;
	}
}
