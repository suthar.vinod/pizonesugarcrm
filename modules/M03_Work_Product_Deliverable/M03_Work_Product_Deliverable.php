<?PHP
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * THIS CLASS IS FOR DEVELOPERS TO MAKE CUSTOMIZATIONS IN
 */
require_once('modules/M03_Work_Product_Deliverable/M03_Work_Product_Deliverable_sugar.php');
require_once('include/upload_file.php');
class M03_Work_Product_Deliverable extends M03_Work_Product_Deliverable_sugar {

	/**
	 * This is a depreciated method, please start using __construct() as this method will be removed in a future version
     *
     * @see __construct
     * @depreciated
	 */
	function M03_Work_Product_Deliverable(){
		self::__construct();
	}

	public function __construct(){
		parent::__construct();
	}
	
	public function deleteAttachment($isduplicate="false"){

       if($this->ACLAccess('edit')){
             if($isduplicate=="true"){
                     return true;
            }

            $removeFile = "upload/{$this->id}";  //upload/{$this->id}

        }
		  if(SugarAutoloader::fileExists($removeFile)) { 
			//UploadFile::unlink($removeFile);
			return true;

        }else{
			$GLOBALS['log']->fatal("File not Exist"); 
			return false;
        }
        return false;
   }
	
}