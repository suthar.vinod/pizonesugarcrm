<?php
// created: 2022-07-14 07:23:44
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'timeline_type_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TIMELINE_TYPE',
    'width' => 10,
  ),
  'deliverable_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_DELIVERABLE',
    'width' => 10,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'related_fields' => 
    array (
      0 => 'assigned_user_id',
    ),
    'vname' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'due_date_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'date',
    'vname' => 'LBL_DUE_DATE',
    'width' => 10,
    'default' => true,
  ),
  'final_due_date_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'date',
    'vname' => 'LBL_FINAL_DUE_DATE',
    'width' => 10,
    'default' => true,
  ),
  'department_manager_approval_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_DEPARTMENT_MANAGER_APPROVAL',
    'width' => 10,
  ),
  'draft_deliverable_sent_date_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'date',
    'vname' => 'LBL_DRAFT_DELIVERABLE_SENT_DATE',
    'width' => 10,
    'default' => true,
  ),
  'deliverable_status_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_DELIVERABLE_STATUS',
    'width' => 10,
  ),
  'deliverable_completed_date_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'date',
    'vname' => 'LBL_DELIVERABLE_COMPLETED_DATE',
    'width' => 10,
    'default' => true,
  ),
  'author_c' => 
  array (
    'readonly' => false,
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_AUTHOR',
    'id' => 'CONTACT_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Contacts',
    'target_record_key' => 'contact_id_c',
  ),
);