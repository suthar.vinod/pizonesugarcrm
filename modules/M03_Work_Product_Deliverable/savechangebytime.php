<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class savechangebytime
{

    function savechangebytimeRecord($bean, $event, $arguments)
    {
        global $db, $current_user;
        $current_date = date("Y-m-d H:i:s");
        if ($bean->id == $bean->fetched_row['id'] && ($bean->overdue_risk_level_c != $bean->fetched_row['overdue_risk_level_c'])) {
            $user_Name =  $current_user->name;
            $bean->change_by_c = $user_Name;
            $bean->change_timestamp_c = $current_date;            
        }
        
        if ($bean->deliverable_c != $bean->fetched_row['deliverable_c'] || $bean->name != $bean->fetched_row['name'] || $bean->fetched_row['deliverable_c'] == "") {
			$bean->manual_datechange = 0;
		} else {
			$bean->manual_datechange = 1;
		}
    }
}
