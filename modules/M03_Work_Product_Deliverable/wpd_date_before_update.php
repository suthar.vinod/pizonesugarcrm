<?php 
global $db; 
$entered_date1 = '2021-11-02 00:00:00';
	echo "<br>===>".$query = "SELECT  WPD.id AS wpd_id,
                WPD.name AS wpd_name,
            WPD.date_entered,
            WPDCSTM.due_date_c,						
            WPDCSTM.final_due_date_c,	
            WPDCSTM.internal_final_due_date_c,
            WPDCSTM.aps_target_due_date_c,
            WPDCSTM.for_bc_goal_internal_draft_c,						
            WPD.date_modified
            FROM `m03_work_product_deliverable` AS WPD
            LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
            ON WPD.id= WPDCSTM.id_c
            RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
            ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
            LEFT JOIN m03_work_product AS WP
            ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
            LEFT JOIN m03_work_product_cstm AS WPCSTM
            ON WP.id=WPCSTM.id_c
            WHERE ( WPDCSTM.due_date_c IS NULL AND WPDCSTM.final_due_date_c IS NULL)
            AND WPD.deleted = 0 
            AND WPD.date_modified>'".$entered_date1."'
            ORDER BY WPD.date_entered ASC";

	$queryResult = $db->query($query);
		 
	$srno = 1;
	
	$tableBody = "<table width='1050px' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th bgcolor='#b3d1ff' align='left'> ID </th>
			<th bgcolor='#b3d1ff' align='left'>Due Date</th>
			<th bgcolor='#b3d1ff' align='left'>Final Due Date</th>
            <th bgcolor='#b3d1ff' align='left'>internal Due Date</th>
            <th bgcolor='#b3d1ff' align='left'>APS target Due Date</th>
            <th bgcolor='#b3d1ff' align='left'>Internal BC Draft Due Date Goal</th>
            <th bgcolor='#b3d1ff' align='left'>Due Date Audit</th>
			<th bgcolor='#b3d1ff' align='left'>Final Due Date Audit</th>
            <th bgcolor='#b3d1ff' align='left'>internal Due Date Audit</th>
            <th bgcolor='#b3d1ff' align='left'>APS target Due Date Audit</th>
            <th bgcolor='#b3d1ff' align='left'>Internal BC Draft Due Date Goal Audit</th>
			</tr>";
			
			
    while ($fetchResult = $db->fetchByAssoc($queryResult)) {
        $id	 						= $fetchResult['wpd_id'];
	    $due_date                   = $fetchResult['due_date_c'];
		$final_due_date   		    = $fetchResult['final_due_date_c'];
        $internal_final_due_date   		    = $fetchResult['internal_final_due_date_c'];
        $aps_target_due_date   		    = $fetchResult['aps_target_due_date_c'];
        $for_bc_goal_internal_draft   		    = $fetchResult['for_bc_goal_internal_draft'];     
		
		 
			if($srno%2==0)
				$stylr = "style='background-color: #e6e6e6;'";
			else
				$stylr = "style='background-color: #f3f3f3;'";					
			

        $sqlGetdate = "SELECT before_value_string FROM `m03_work_product_deliverable_audit` WHERE field_name = 'due_date_c' 
        AND before_value_string != '' AND parent_id =  '".$id."' AND date_created>'".$entered_date1."' order by date_created DESC LIMIT 1";
            $resultsqlGetdate = $db->query($sqlGetdate);
            $rowsqldate = $db->fetchByAssoc($resultsqlGetdate);
            $date_new = $rowsqldate['before_value_string'];                      
           
        $sqlGetdate1 = "SELECT before_value_string FROM `m03_work_product_deliverable_audit` WHERE field_name = 'final_due_date_c' 
       AND before_value_string != '' AND parent_id =  '".$id."' AND date_created>'".$entered_date1."' order by date_created DESC LIMIT 1";
            $resultsqlGetdate1 = $db->query($sqlGetdate1);
            $rowsqldate1 = $db->fetchByAssoc($resultsqlGetdate1);
            $date_new1 = $rowsqldate1['before_value_string']; 
           

            $sqlGetdate2 = "SELECT before_value_string FROM `m03_work_product_deliverable_audit` WHERE field_name = 'internal_final_due_date_c' 
        AND before_value_string != '' AND parent_id =  '".$id."' AND date_created>'".$entered_date1."' order by date_created DESC LIMIT 1";
            $resultsqlGetdate2 = $db->query($sqlGetdate2);
            $rowsqldate2 = $db->fetchByAssoc($resultsqlGetdate2);
            $date_new2 = $rowsqldate2['before_value_string'];           

            $sqlGetdate3 = "SELECT before_value_string FROM `m03_work_product_deliverable_audit` WHERE field_name = 'aps_target_due_date_c' 
        AND before_value_string != '' AND parent_id =  '".$id."' AND date_created>'".$entered_date1."' order by date_created DESC LIMIT 1";
            $resultsqlGetdate3 = $db->query($sqlGetdate3);
            $rowsqldate3 = $db->fetchByAssoc($resultsqlGetdate3);
            $date_new3 = $rowsqldate3['before_value_string'];            

             $sqlGetdate4 = "SELECT before_value_string FROM `m03_work_product_deliverable_audit` WHERE field_name = 'for_bc_goal_internal_draft_c' 
       AND before_value_string != '' AND parent_id =  '".$id."' AND date_created>'".$entered_date1."' order by date_created DESC LIMIT 1";
            $resultsqlGetdate4 = $db->query($sqlGetdate4);
            $rowsqldate4 = $db->fetchByAssoc($resultsqlGetdate4);
            $date_new4 = $rowsqldate4['before_value_string']; 
           

            $tableBody .= "<tr><td ".$stylr.">".$srno++."</td>
							<td ".$stylr.">".$id."</td> 
							<td ".$stylr.">".$due_date."</td>	
							<td ".$stylr.">".$final_due_date."</td>	
                            <td ".$stylr.">".$internal_final_due_date."</td>
                            <td ".$stylr.">".$aps_target_due_date."</td>
                            <td ".$stylr.">".$for_bc_goal_internal_draft."</td>
                            <td ".$stylr.">".$date_new."</td>	
							<td ".$stylr.">".$date_new1."</td>	
                            <td ".$stylr.">".$date_new2."</td>
                            <td ".$stylr.">".$date_new3."</td>
                            <td ".$stylr.">".$date_new4."</td>				
							</tr>";

	 }
	 
	 echo "<br><br>".$tableBody .="</table>";
?>