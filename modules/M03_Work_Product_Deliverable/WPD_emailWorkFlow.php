<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class WPD_emailWorkFlow{
	static $already_ran = false;
	function sendEmailWorkFlow($bean, $event, $arguments){
		if(self::$already_ran == true) return;
        self::$already_ran = true;
		
		global $db;
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$workProductId	= ""; 
		$current_date = date("Y-m-d"); //current date
		
		if(($bean->id == $bean->fetched_row['id']) && $bean->deliverable_status_c=="Completed" && $bean->deliverable_c=="QC Data Review"){

			$wpd_id		= $bean->id; // Work Product Deliverable ID
			$wpdName	= $bean->name;
			$UserName	= "";
			$leadAuditorName = "";

			//Query to get the Work Product ID	
			$wpd_sql = "SELECT * FROM `m03_work_product_m03_work_product_deliverable_1_c` AS WP_WPD  
			WHERE WP_WPD.`m03_work_pe584verable_idb`='".$wpd_id."' AND deleted=0 ORDER BY `WP_WPD`.`date_modified` DESC limit 1";
			$wpd_exec = $db->query($wpd_sql);
			if ($wpd_exec->num_rows > 0) 
			{
				$result			= $db->fetchByAssoc($wpd_exec);
				$workProductId	= $result['m03_work_p0b66product_ida'];
				//Update Work_Product Bean to update the Animal Countdown Value
				$wp_bean = BeanFactory::getBean('M03_Work_Product', $workProductId);
								
				/*Get Study Director*/
				$leadaudi_id	 		= $wp_bean->contact_id4_c;
				$leadaudi_bean	 		= BeanFactory::getBean('Contacts', $leadaudi_id);
				$leadAuditorName		= $leadaudi_bean->name;
				$LAEmailAddress			= $leadaudi_bean->emailAddress->getPrimaryAddress($leadaudi_bean);
				

				if($LAEmailAddress!=""){
					//Send Email/
					///Upload Document Reminder Template 
					$template = new EmailTemplate();
					$emailObj = new Email();
					
					$template->retrieve_by_string_fields(array('name' => 'Notification of Completed QC Data Review', 'type' => 'email'));
					
					$wpdLink = '<a target="_blank"href="'.$site_url.'/#M03_Work_Product_Deliverable/'.$wpd_id.'">'.$wpdName.'</a>';
					 
					$template->body_html = str_replace('[WPD_ID]', $wpdLink, $template->body_html);
					
					$defaults	= $emailObj->getSystemDefaultEmail();
					$mail		= new SugarPHPMailer();
					$mail->setMailerForSystem();
					$mail->IsHTML(true);
					$mail->From		= $defaults['email'];
					$mail->FromName = $defaults['name'];
					$mail->Subject	= "Notification of Completed QC Data Review";
					$mail->Body		= $template->body_html;					 

					$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';					

					if(!empty($LAEmailAddress))
						$mail->AddAddress($LAEmailAddress);
					//$mail->AddAddress('fxs_mjohnson@apsemail.com');

					//If their exist some valid email addresses then send email
					if (!empty($wp_emailAdd_dev)) {
						if (!$mail->Send()) {
							$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
						} else {
							$GLOBALS['log']->debug('email sent');
						}
						unset($mail);
					}
				} 
			}
		}
	}
}
?>