<?php
require_once 'include/Sugarpdf/sugarpdf/sugarpdf.pdfmanager.php';
require_once 'include/Sugarpdf/sugarpdf/sugarpdf.smarty.php';
//require_once('include/Sugar_Smarty.php');
// ob_start();
class M03_Work_Product_DeliverableSugarpdfPdfmanager extends SugarpdfPdfmanager
{
    public function preDisplay()
    {
        global $db, $current_user;
        parent::preDisplay();
        $this->SetMargins(15, 10, 15);
        $previewMode = false;
        if (!empty($_REQUEST['pdf_preview']) && $_REQUEST['pdf_preview'] == 1) {
            $previewMode = true;
        }

        if ($this->module == 'M03_Work_Product_Deliverable' && $previewMode == false) {
            $wpdId   = $this->bean->id;
            $beanWpd = BeanFactory::retrieveBean('M03_Work_Product_Deliverable', $wpdId);
            //Fetch Inventory Items record IDs
            $allDataArr = array();
            $count = 0;
            if ($beanWpd->load_relationship('m03_work_product_deliverable_ii_inventory_item_1')) {
                $inventoryItemsIds = $beanWpd->m03_work_product_deliverable_ii_inventory_item_1->get();
                for ($i = 0; $i < count($inventoryItemsIds); $i++) {
                    $sql = "SELECT ii_inventory_item_cstm.timepoint_name_c,
                                ii_inventory_item.name,
                                ii_inventory_item.collection_date,
                                ii_inventory_item.storage_condition,
                                ii_inventory_item.timepoint_type,
                                ii_inventory_item.collection_date_time,
                                ii_inventory_item.collection_date_time_type
                            FROM ii_inventory_item
                            INNER JOIN m03_work_product_deliverable_ii_inventory_item_1_c ON m03_work_product_deliverable_ii_inventory_item_1_c.m03_work_p3c4fry_item_idb = '" . $inventoryItemsIds[$i] . "'
                            INNER JOIN ii_inventory_item_cstm ON ii_inventory_item_cstm.id_c = m03_work_product_deliverable_ii_inventory_item_1_c.m03_work_p3c4fry_item_idb 
                            WHERE ii_inventory_item.id = ii_inventory_item_cstm.id_c AND ii_inventory_item.deleted = 0";
                    $query          = $db->query($sql);
                    $result         = $db->fetchByAssoc($query);
                    $allDataArr[$count]['Name']    = $result['name'];
                    $timepointType  = $result['timepoint_type'];
                    $collectionDate = $result['collection_date'];
                    //get value of timepointtype field
                    if ($timepointType !== 'Defined') {
                        if ($result['timepoint_type'] == 'AcuteBaseline') {
                            $allDataArr[$count]['timepoint_type'] = 'Acute/Baseline';
                        } else if ($result['timepoint_type'] == 'AcuteTermination') {
                            $allDataArr[$count]['timepoint_type'] = 'Acute/Termination';
                        } else if ($result['timepoint_type'] == 'ChronicBaseline') {
                            $allDataArr[$count]['timepoint_type'] = 'Chronic/Baseline';
                        } else if ($result['timepoint_type'] == 'ChronicTreatment') {
                            $allDataArr[$count]['timepoint_type'] = 'Chronic/Treatment';
                        } else if ($result['timepoint_type'] == 'Model CreationBaseline') {
                            $allDataArr[$count]['timepoint_type'] = 'Model Creation/Baseline';
                        } else {
                            $allDataArr[$count]['timepoint_type'] = $result['timepoint_type'];
                        }
                    } else {
                        $allDataArr[$count]['timepoint_type'] = $result['timepoint_name_c'];
                    }
                    //get value of collection date
                    if ($result['collection_date_time_type'] == 'Date at Record Creation' && $collectionDate !== NULL) {
                        $formatedDate = explode("-", $collectionDate);
                        $newformatedDate = $formatedDate[1] . '/' . $formatedDate[2] . '/' . $formatedDate[0];
                        $allDataArr[$count]['collection_date'] = $newformatedDate;
                    } else {
                        if ($result['collection_date_time_type'] == 'Date Time at Record Creation') {
                            $formatedDate = explode(" ", $result['collection_date_time']);
                            $newformatedDate = $formatedDate[0];
                            $newFormatedDate1 = explode("-", $newformatedDate);
                            $newformatedDate2 = $newFormatedDate1[1] . '/' . $newFormatedDate1[2] . '/' . $newFormatedDate1[0];
                            $allDataArr[$count]['collection_date'] = $newformatedDate2;
                        }
                    }
                    $allDataArr[$count]['storage_condition']   = $result['storage_condition']; //get the value of current storage condition field
                    $count++;
                }
            }

            //get the inventory Collection Record Ids
            if ($beanWpd->load_relationship('m03_work_product_deliverable_ic_inventory_collection_1')) {
                $inventoryCollectionIds = $beanWpd->m03_work_product_deliverable_ic_inventory_collection_1->get();
                for ($j = 0; $j < count($inventoryCollectionIds); $j++) {
                    //Get Inventory Item Bean
                    $sqlIC = "SELECT id FROM ic_inventory_collection WHERE id = '" . $inventoryCollectionIds[$j] . "' AND deleted = 0";
                    $queryIC   = $db->query($sqlIC);
                    $resultIC  = $db->fetchByAssoc($queryIC);
                    $sqlRelICII = "SELECT ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb AS II_Id FROM ic_inventory_collection_ii_inventory_item_1_c WHERE ic_invento128flection_ida = '" . $resultIC['id'] . "' AND deleted = 0";
                    $queryRelICII      = $db->query($sqlRelICII);
                    while ($rowRelICII = $db->fetchByAssoc($queryRelICII)) {
                        $sqlII2 = "SELECT ii_inventory_item_cstm.timepoint_name_c,
                                        ii_inventory_item.name,
                                        ii_inventory_item.collection_date,
                                        ii_inventory_item.storage_condition,
                                        ii_inventory_item.timepoint_type,
                                        ii_inventory_item.collection_date_time,
                                        ii_inventory_item.collection_date_time_type
                                    FROM ii_inventory_item
                                    INNER JOIN ic_inventory_collection_ii_inventory_item_1_c ON ic_inventory_collection_ii_inventory_item_1_c.ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb = '" . $rowRelICII['II_Id'] . "'
                                    INNER JOIN ii_inventory_item_cstm ON ii_inventory_item_cstm.id_c = ic_inventory_collection_ii_inventory_item_1_c.ic_inventory_collection_ii_inventory_item_1ii_inventory_item_idb 
                                    WHERE ii_inventory_item.id = ii_inventory_item_cstm.id_c AND ii_inventory_item.deleted = 0";
                        $queryII2       = $db->query($sqlII2);
                        $resultII2      = $db->fetchByAssoc($queryII2);
                        $allDataArr[$count]['Name']    = $resultII2['name'];
                        $timepointType  = $resultII2['timepoint_type'];
                        $collectionDate = $resultII2['collection_date'];
                        //get value of timepointtype field
                        if ($timepointType !== 'Defined') {
                            if ($resultII2['timepoint_type'] == 'AcuteBaseline') {
                                $allDataArr[$count]['timepoint_type'] = 'Acute/Baseline';
                            } else if ($resultII2['timepoint_type'] == 'AcuteTermination') {
                                $allDataArr[$count]['timepoint_type'] = 'Acute/Termination';
                            } else if ($resultII2['timepoint_type'] == 'ChronicBaseline') {
                                $allDataArr[$count]['timepoint_type'] = 'Chronic/Baseline';
                            } else if ($resultII2['timepoint_type'] == 'ChronicTreatment') {
                                $allDataArr[$count]['timepoint_type'] = 'Chronic/Treatment';
                            } else if ($resultII2['timepoint_type'] == 'Model CreationBaseline') {
                                $allDataArr[$count]['timepoint_type'] = 'Model Creation/Baseline';
                            } else {
                                $allDataArr[$count]['timepoint_type'] = $resultII2['timepoint_type'];
                            }
                        } else {
                            $allDataArr[$count]['timepoint_type'] = $resultII2['timepoint_name_c'];
                        }
                        //get value of collection date
                        if ($resultII2['collection_date_time_type'] == 'Date at Record Creation' && $collectionDate !== NULL) {
                            $formatedDate = explode("-", $collectionDate);
                            $newformatedDate = $formatedDate[1] . '/' . $formatedDate[2] . '/' . $formatedDate[0];
                            $allDataArr[$count]['collection_date'] = $newformatedDate;
                        } else {
                            if ($resultII2['collection_date_time_type'] == 'Date Time at Record Creation') {
                                $formatedDate = explode(" ", $resultII2['collection_date_time']);
                                $newformatedDate = $formatedDate[0];
                                $newFormatedDate1 = explode("-", $newformatedDate);
                                $newformatedDate2 = $newFormatedDate1[1] . '/' . $newFormatedDate1[2] . '/' . $newFormatedDate1[0];
                                $allDataArr[$count]['collection_date'] = $newformatedDate2;
                            }
                        }
                        $allDataArr[$count]['storage_condition']   = $resultII2['storage_condition']; //get the value of current storage condition field
                        $count++;
                    }
                }
            }
            $this->ss->assign('allDataArr', $allDataArr);
        }
    }
}
