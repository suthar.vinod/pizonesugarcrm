<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class saveAuthorChangeEmail{
	// static $already_ran = false;
	function saveAuthorChangeEmailRecord($bean, $event, $arguments){
		//if(self::$already_ran == true) return;
        //self::$already_ran = true;
		global $db;
		$site_url 		= $GLOBALS['sugar_config']['site_url'];
		$current_date 	= date("Y-m-d"); //current date
		// $GLOBALS['log']->fatal("===WPDAuthorEmailReminder==>".$bean->id."====".$bean->contact_id_c);
		
		// if($bean->id == $bean->fetched_row['id'] && ($bean->contact_id_c != $bean->fetched_row['contact_id_c'])){
		if(($bean->contact_id_c != $bean->fetched_row['contact_id_c'])){
		$WPD_ID 		= $bean->id;
		$WPD_name 		= $bean->name;
		$WPD_author_c	= $bean->contact_id_c;

		// $GLOBALS['log']->fatal("===WPD_ID==>".$WPD_ID);
		// $GLOBALS['log']->fatal("===WPD_name==>".$WPD_name);
		// $GLOBALS['log']->fatal("===WPD_author_contact_id_c==>".$WPD_author_c);
		// $GLOBALS['log']->fatal("===beanauthor_c==>".$bean->author_c);
		// $GLOBALS['log']->fatal("===beancontact_id_c==>".$bean->contact_id_c);
		// $GLOBALS['log']->fatal("===beanfetched_row===>".$bean->fetched_row['contact_id_c']);

			//Send Email/
			$template = new EmailTemplate();
			$emailObj = new Email();

			$wpdID  = $bean->contact_id_c;
			$Contact_bean = BeanFactory::getBean('Contacts', $wpdID);

			$AuthorEmailAddress = $Contact_bean->emailAddress->getPrimaryAddress($Contact_bean);
			
			// $GLOBALS['log']->fatal("=====Contactemail Address====>".$AuthorEmailAddress);
			
			$template->retrieve_by_string_fields(array('name' => 'Notification of Author Change.', 'type' => 'email'));
			
			$WPDLink = '<a target="_blank"href="'.$site_url.'/#M03_Work_Product_Deliverable/'.$WPD_ID.'">'.$WPD_name.'</a>';		 
			// $GLOBALS['log']->fatal("=====WPD Link====>".print_r($WPDLink,1));
			$template->body_html = str_replace('[insert_author_name]', $WPDLink, $template->body_html);
			// You have been assigned as the Author for [insert hyperlink to Work Product Deliverable record using WPD name].  Please review.

			// $GLOBALS['log']->fatal('Email Template:=>' . $template->body_html);

			$defaults	= $emailObj->getSystemDefaultEmail();
			$mail		= new SugarPHPMailer();
			$mail->setMailerForSystem();
			$mail->IsHTML(true);
			$mail->From		= $defaults['email'];
			$mail->FromName = $defaults['name'];
			$mail->Subject	= "Notification of Author Assignment";
			$mail->Body		= $template->body_html;
			$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';
				
			// $mail->AddAddress('fxs_mjohnson@apsemail.com');
			// $mail->AddAddress('vsuthar@apsemail.com');

			$domainToSearch = 'apsemail.com';
			$domain_name = substr(strrchr($AuthorEmailAddress, "@"), 1);
			if (trim(strtolower($domain_name) == $domainToSearch)) {
				$mail->AddAddress($AuthorEmailAddress);
			}

			// if ($AuthorEmailAddress!="") {
			// 	$mail->AddAddress($AuthorEmailAddress);
			// 	// $mail->AddAddress('lsaini@apsemail.com');
			// } 
				
			//If their exist some valid email addresses then send email
			if (!empty($wp_emailAdd_dev)) {
				if (!$mail->Send()) {
					$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
				} else {
					$GLOBALS['log']->debug('Email sent to the Author');
				}
				unset($mail);
			}
		}
	}
	
	 
}
