<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class send_email_lead_auditor
{
	function send_email_lead_auditorRecord($bean, $event, $arguments)
	{

		global $db;
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$workProductId	= "";
		$current_date = date("Y-m-d"); //current date

		if ($bean->draft_deliverable_sent_date_c != $bean->fetched_row['draft_deliverable_sent_date_c'] && $bean->fetched_row['draft_deliverable_sent_date_c'] == "") {

			$WPD_ID 		= $bean->id;
			$WPD_name 		= $bean->name;
			$workProductId	= $bean->m03_work_p0b66product_ida;
			$wp_bean 		= BeanFactory::getBean('M03_Work_Product', $workProductId);
			$LA_ID  		= $wp_bean->contacts_m03_work_product_2contacts_ida;
			$WPC  			= $wp_bean->work_product_compliance_c;

			/*#2461 : Update Criteria */
			$report_word = "Report";
			if ($WPC == 'GLP' && $LA_ID != '' && strpos($WPD_name, $report_word) !== false) {

				//Send Email/
				$template = new EmailTemplate();
				$emailObj = new Email();

				/*Get WP Lead Auditor EMail*/
				$Contact_bean = BeanFactory::getBean('Contacts', $LA_ID);
				$company[] = $Contact_bean->account_name;


				//$GLOBALS['log']->fatal("===company==>".print_r($company,1));

				$AuditorEmailAddress = $Contact_bean->emailAddress->getPrimaryAddress($Contact_bean);
				$template->retrieve_by_string_fields(array('name' => 'Draft Submitted for QA Review', 'type' => 'email'));

				$WPDLink = '<a target="_blank"href="' . $site_url . '/#M03_Work_Product_Deliverable/' . $WPD_ID . '">' . $WPD_name . '</a>';

				$template->body_html = str_replace('[WPD_Name]', $WPDLink, $template->body_html);
				// You have been assigned as the Author for [insert hyperlink to Work Product Deliverable record using WPD name].  Please review.

				//$GLOBALS['log']->fatal('Email Template:=>' . $template->body_html);

				$defaults	= $emailObj->getSystemDefaultEmail();
				$mail		= new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->IsHTML(true);
				$mail->From		= $defaults['email'];
				$mail->FromName = $defaults['name'];
				$mail->Subject	= "Draft Submitted for QA Review";
				$mail->Body		= $template->body_html;
				$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';

				//$mail->AddAddress('fxs_mjohnson@apsemail.com');
				//$mail->AddAddress('vsuthar@apsemail.com');
				//$company 			= array('american preclinical services','NAMSA');
				$domainToSearch = 'apsemail.com';
				$domain_name = substr(strrchr($AuditorEmailAddress, "@"), 1);
				if (trim(strtolower($domain_name) == $domainToSearch)) {
					$mail->AddAddress($AuditorEmailAddress);
				}

				//If their exist some valid email addresses then send email
				if (!empty($wp_emailAdd_dev)) {
					if (!$mail->Send()) {
						$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
					} else {
						$GLOBALS['log']->debug('email sent to Lead Auditor');
					}
					unset($mail);
				}
			}
		}
	}
}
