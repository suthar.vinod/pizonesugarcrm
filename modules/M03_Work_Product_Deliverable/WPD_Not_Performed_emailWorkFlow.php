<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class WPD_Not_Performed_emailWorkFlow{
	static $already_ran = false;
	function sendNotPerformedEmailWorkFlow($bean, $event, $arguments){
			if(self::$already_ran == true) return;
        	self::$already_ran = true;
		
			global $db;
			$site_url = $GLOBALS['sugar_config']['site_url'];
		 
			$current_date = date("Y-m-d"); //current date
		 
		 	/* Start AR Notification of Report Not to be Completed */
			if($bean->id == $bean->fetched_row['id']  && $bean->deliverable_status_c != $bean->fetched_row['deliverable_status_c'] && $bean->deliverable_status_c=="Not Performed")  {
						
						$wpd_id		= $bean->id; // Work Product Deliverable ID
						$wpdName	= $bean->name;
						
						//Send Email/
						///Upload Document Reminder Template 
						$template = new EmailTemplate();
						$emailObj = new Email();
						
						$template->retrieve_by_string_fields(array('name' => 'Report Not Performed', 'type' => 'email'));
						
						$wpdLink = '<a target="_blank"href="'.$site_url.'/#M03_Work_Product_Deliverable/'.$wpd_id.'">'.$wpdName.'</a>';
						
						$template->body_html = str_replace('[WPD_Name]', $wpdLink, $template->body_html);
						
						$defaults	= $emailObj->getSystemDefaultEmail();
						$mail		= new SugarPHPMailer();
						$mail->setMailerForSystem();
						$mail->IsHTML(true);
						$mail->From		= $defaults['email'];
						$mail->FromName = $defaults['name'];
						$mail->Subject	= $template->subject;
						$mail->Body		= $template->body_html;

						$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';

						$mail->AddAddress('fxs_mjohnson@apsemail.com');
						$mail->AddAddress('mmarion@apsemail.com');					
						$mail->AddAddress('skreger@apsemail.com');	
						$mail->AddAddress('jbranstad@apsemail.com');	
						$mail->AddAddress('pconforti@apsemail.com');	
						//If their exist some valid email addresses then send email
						if (!empty($wp_emailAdd_dev)) {
							if (!$mail->Send()) {
								$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
							} else {
								$GLOBALS['log']->debug('email sent');
							}
							unset($mail);
						}
			} 
				
			/* End AR Notification of Report Not to be Completed */

			$wpd_id		= $bean->id; // Work Product Deliverable ID
			$wpdName	= $bean->name;

			$workProductId	= $bean->m03_work_p0b66product_ida;
			$wp_bean = BeanFactory::getBean('M03_Work_Product', $workProductId);

			/* Start First AR Notification of Report Complete  */
		if($bean->id == $bean->fetched_row['id']  && $bean->deliverable_status_c != $bean->fetched_row['deliverable_status_c'] && $bean->deliverable_status_c=="Completed" && ($wp_bean->functional_area_c == "Analytical Services" || $wp_bean->functional_area_c == "Custom Biocompatibility" || $wp_bean->functional_area_c == "Standard Biocompatibility" || $wp_bean->functional_area_c == "Pharmacology" || $wp_bean->functional_area_c == "Toxicology"))
			{	
					if (!empty($bean->id)) {
						$NewTasksBean = BeanFactory::newBean('Tasks');
						$new_bean_id = create_guid();
						$NewTasksBean->id          	             		 = $new_bean_id;	
						$NewTasksBean->new_with_id 						 = true;			
						$NewTasksBean->assigned_user_id 	             = "14adb94a-dd1e-9054-8d47-576bf790edc0";
						$NewTasksBean->parent_id						 = $bean->id;
						$NewTasksBean->parent_type 						 = "M03_Work_Product_Deliverable"; 
						$NewTasksBean->email_reminder_time_c			 = "60"; 
						$NewTasksBean->priority			 				 = "Medium"; 
						$NewTasksBean->status			 				 = "Not Completed";
						$NewTasksBean->name			 				 	 = "Post Deliverable Completion Follow Up";
						$NewTasksBean->save();
						$NewTasksBean->load_relationship('m03_work_product_deliverable_tasks_1');
						$NewTasksBean->m03_work_product_deliverable_tasks_1->add($bean->id);
						/* $relationid1 = create_guid();
						$tasksql = 'INSERT INTO m03_work_product_deliverable_tasks_1_c (id,date_modified,deleted,m03_work_pbb72verable_ida,m03_work_product_deliverable_tasks_1tasks_idb)  
						values("'.$relationid1.'",now(),0,"'.$bean->id.'","'.$NewTasksBean->id.'")';
						$db->query($tasksql); */
					}

					//Send Email/
					///Upload Document Reminder Template 
					$template = new EmailTemplate();
					$emailObj = new Email();
					
					$template->retrieve_by_string_fields(array('name' => 'Report Completed', 'type' => 'email'));
					
					$wpdLink = '<a target="_blank"href="'.$site_url.'/#M03_Work_Product_Deliverable/'.$wpd_id.'">'.$wpdName.'</a>';
					
					$template->body_html = str_replace('[WPD_Name_Completed]', $wpdLink, $template->body_html);
					
					$defaults	= $emailObj->getSystemDefaultEmail();
					$mail		= new SugarPHPMailer();
					$mail->setMailerForSystem();
					$mail->IsHTML(true);
					$mail->From		= $defaults['email'];
					$mail->FromName = $defaults['name'];
					$mail->Subject	= $template->subject;
					$mail->Body		= $template->body_html;

					$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';

					$mail->AddAddress('fxs_mjohnson@apsemail.com');
					$mail->AddAddress('mmarion@apsemail.com');					
					$mail->AddAddress('pconforti@apsemail.com');
					$mail->AddAddress('ahoward@apsemail.com');
					//If their exist some valid email addresses then send email
					if (!empty($wp_emailAdd_dev)) {
						if (!$mail->Send()) {
							$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
						} else {
							$GLOBALS['log']->debug('email sent');
						}
						unset($mail);
					}  		
				
			} 
			/* End First AR Notification of Report Complete  */

			/* Start Second AR Notification of Report Complete  */
			if($bean->id == $bean->fetched_row['id'] && $bean->deliverable_status_c != $bean->fetched_row['deliverable_status_c']  && $bean->deliverable_status_c=="Completed" && ($wp_bean->functional_area_c == "Bioskills" || $wp_bean->functional_area_c == "Histology Services" || $wp_bean->functional_area_c == "ISR" || $wp_bean->functional_area_c == "Pathology Services" || $wp_bean->functional_area_c == "Regulatory"))
			{		

				if (!empty($bean->id)) {
					$NewTasksBean = BeanFactory::newBean('Tasks');
					$new_bean_id = create_guid();

					$NewTasksBean->id          	             		 = $new_bean_id;
					$NewTasksBean->new_with_id 						 = true;				
					$NewTasksBean->assigned_user_id 	             = "14adb94a-dd1e-9054-8d47-576bf790edc0";
					$NewTasksBean->parent_id						 = $bean->id;
					$NewTasksBean->parent_type 						 = "M03_Work_Product_Deliverable"; 
					$NewTasksBean->email_reminder_time_c			 = "60"; 
					$NewTasksBean->priority			 				 = "Medium"; 
					$NewTasksBean->status			 				 = "Not Completed";
					$NewTasksBean->name			 				 	 = "Post Deliverable Completion Follow Up";
					$NewTasksBean->save();
					$NewTasksBean->load_relationship('m03_work_product_deliverable_tasks_1');
					$NewTasksBean->m03_work_product_deliverable_tasks_1->add($bean->id);
					/* $relationid1 = create_guid();
					$tasksql = 'INSERT INTO m03_work_product_deliverable_tasks_1_c (id,date_modified,deleted,m03_work_pbb72verable_ida,m03_work_product_deliverable_tasks_1tasks_idb)  
					values("'.$relationid1.'",now(),0,"'.$bean->id.'","'.$NewTasksBean->id.'")';
					$db->query($tasksql); */
				}
					//Send Email/
					///Upload Document Reminder Template 
					$template = new EmailTemplate();
					$emailObj = new Email();
					
					$template->retrieve_by_string_fields(array('name' => 'Report Completed', 'type' => 'email'));
					
					$wpdLink = '<a target="_blank"href="'.$site_url.'/#M03_Work_Product_Deliverable/'.$wpd_id.'">'.$wpdName.'</a>';
					
					$template->body_html = str_replace('[WPD_Name_Completed]', $wpdLink, $template->body_html);
					
					$defaults	= $emailObj->getSystemDefaultEmail();
					$mail		= new SugarPHPMailer();
					$mail->setMailerForSystem();
					$mail->IsHTML(true);
					$mail->From		= $defaults['email'];
					$mail->FromName = $defaults['name'];
					$mail->Subject	= $template->subject;
					$mail->Body		= $template->body_html;

					$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';

					$mail->AddAddress('fxs_mjohnson@apsemail.com');
					$mail->AddAddress('mmarion@apsemail.com');					
					$mail->AddAddress('skreger@apsemail.com');	
					$mail->AddAddress('jbranstad@apsemail.com');	
					$mail->AddAddress('pconforti@apsemail.com');					
					
					//If their exist some valid email addresses then send email
					if (!empty($wp_emailAdd_dev)) {
						if (!$mail->Send()) {
							$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
						} else {
							$GLOBALS['log']->debug('email sent');
						}
						unset($mail);
					}
			}			
			 
	}
}
