<?php
// created: 2022-12-13 04:43:54
$viewdefs['Documents']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'document_name',
          'label' => 'LBL_LIST_DOCUMENT_NAME',
          'enabled' => true,
          'default' => true,
          'link' => true,
          'type' => 'name',
          'width' => '20',
        ),
        1 => 
        array (
          'name' => 'filename',
          'label' => 'LBL_LIST_FILENAME',
          'enabled' => true,
          'default' => true,
          'width' => '20',
        ),
        2 => 
        array (
          'name' => 'template_type',
          'label' => 'LBL_TEMPLATE_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'width' => '10',
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'team_name',
          'label' => 'LBL_TEAMS',
          'enabled' => true,
          'id' => 'TEAM_ID',
          'link' => true,
          'sortable' => false,
          'width' => '2',
          'default' => false,
        ),
        5 => 
        array (
          'name' => 'modified_by_name',
          'label' => 'LBL_MODIFIED',
          'enabled' => true,
          'readonly' => true,
          'id' => 'MODIFIED_USER_ID',
          'link' => true,
          'width' => '10',
          'default' => false,
        ),
        6 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
          'enabled' => true,
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'width' => '10',
          'default' => false,
        ),
        7 => 
        array (
          'name' => 'exp_date',
          'label' => 'LBL_DOC_EXP_DATE',
          'enabled' => true,
          'width' => '10',
          'default' => false,
        ),
        8 => 
        array (
          'name' => 'last_rev_create_date',
          'label' => 'LBL_LAST_REV_CREATE_DATE',
          'enabled' => true,
          'id' => 'DOCUMENT_REVISION_ID',
          'link' => true,
          'sortable' => false,
          'width' => '10',
          'default' => false,
        ),
        9 => 
        array (
          'name' => 'subcategory_id',
          'label' => 'LBL_SF_SUBCATEGORY',
          'enabled' => true,
          'width' => '15',
          'default' => false,
        ),
        10 => 
        array (
          'name' => 'category_id',
          'label' => 'LBL_LIST_CATEGORY',
          'enabled' => true,
          'default' => false,
          'width' => '10',
        ),
        11 => 
        array (
          'name' => 'doc_type',
          'label' => 'LBL_LIST_DOC_TYPE',
          'enabled' => true,
          'default' => false,
          'width' => '5',
        ),
        12 => 
        array (
          'name' => 'is_shared',
          'label' => 'LBL_IS_SHARED',
          'default' => false,
          'enabled' => true,
          'selected' => false,
        ),
      ),
    ),
  ),
);