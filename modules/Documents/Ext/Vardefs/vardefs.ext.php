<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_account_id_c.php

 // created: 2016-04-03 18:16:31

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:54
$dictionary['Document']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_test_c.php

 // created: 2016-10-25 03:26:58
$dictionary['Document']['fields']['test_c']['labelValue'] = 'test';
$dictionary['Document']['fields']['test_c']['dependency'] = '';


?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_template_type.php

 // created: 2016-10-25 03:26:58
$dictionary['Document']['fields']['template_type']['audited'] = false;
$dictionary['Document']['fields']['template_type']['massupdate'] = true;
$dictionary['Document']['fields']['template_type']['duplicate_merge'] = 'enabled';
$dictionary['Document']['fields']['template_type']['duplicate_merge_dom_value'] = '1';
$dictionary['Document']['fields']['template_type']['merge_filter'] = 'disabled';
$dictionary['Document']['fields']['template_type']['calculated'] = false;
$dictionary['Document']['fields']['template_type']['dependency'] = false;
$dictionary['Document']['fields']['template_type']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_description.php

 // created: 2016-10-25 03:26:58
$dictionary['Document']['fields']['description']['audited'] = false;
$dictionary['Document']['fields']['description']['massupdate'] = false;
$dictionary['Document']['fields']['description']['comments'] = 'Full text of the note';
$dictionary['Document']['fields']['description']['duplicate_merge'] = 'enabled';
$dictionary['Document']['fields']['description']['duplicate_merge_dom_value'] = '1';
$dictionary['Document']['fields']['description']['merge_filter'] = 'disabled';
$dictionary['Document']['fields']['description']['full_text_search']['enabled'] = true;
$dictionary['Document']['fields']['description']['full_text_search']['searchable'] = false;
$dictionary['Document']['fields']['description']['full_text_search']['boost'] = 0.60999999999999998667732370449812151491641998291015625;
$dictionary['Document']['fields']['description']['calculated'] = false;
$dictionary['Document']['fields']['description']['rows'] = '6';
$dictionary['Document']['fields']['description']['cols'] = '120';


?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/tm_tradeshow_management_documents_1_Documents.php

// created: 2018-01-18 22:47:15
$dictionary["Document"]["fields"]["tm_tradeshow_management_documents_1"] = array (
  'name' => 'tm_tradeshow_management_documents_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_documents_1',
  'source' => 'non-db',
  'module' => 'TM_Tradeshow_Management',
  'bean_name' => 'TM_Tradeshow_Management',
  'side' => 'right',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
  'link-type' => 'one',
);
$dictionary["Document"]["fields"]["tm_tradeshow_management_documents_1_name"] = array (
  'name' => 'tm_tradeshow_management_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'save' => true,
  'id_name' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
  'link' => 'tm_tradeshow_management_documents_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["tm_tradeshow_management_documents_1tm_tradeshow_management_ida"] = array (
  'name' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE_ID',
  'id_name' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
  'link' => 'tm_tradeshow_management_documents_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_controlled_document_number_c.php

 // created: 2018-01-31 16:48:51
$dictionary['Document']['fields']['controlled_document_number_c']['labelValue']='Controlled Document Number';
$dictionary['Document']['fields']['controlled_document_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Document']['fields']['controlled_document_number_c']['enforced']='';
$dictionary['Document']['fields']['controlled_document_number_c']['dependency']='';

 
?>
