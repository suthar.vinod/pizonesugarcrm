<?php
require_once ('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config;
global $db;
global $app_list_strings;
$compliance_dom         	= $app_list_strings['work_product_compliance_list'];
$deliverable_dom         	= $app_list_strings['deliverable_status_list'];
$offset_time = time();
$current_date = date("Y-m-d 00:00:00", $offset_time);
$ss = new Sugar_Smarty();
$ReportData = array();
        $temp = array();
		$srno = 1;
$query = "SELECT WPD.id AS WPDID, 
WPD.name AS WPDname,  
WPD.assigned_user_id AS Assign_user,
AU.first_name AS AUFname,
AU.last_name AS AULname,
WPDCSTM.internal_final_due_date_c AS Duedate,
WPDCSTM.deliverable_status_c AS WPD_Status,
WP.id AS WPID,
WPCSTM.work_product_compliance_c AS WPC,
WP.name AS WPname, 
SD.id AS SDID,
SD.first_name AS Fname,
SD.last_name AS Lname,
WPDCSTM.deliverable_c AS WPDdeliverable							          
FROM `m03_work_product_deliverable` AS WPD
	LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
		ON WPD.id= WPDCSTM.id_c
	RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
		ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
	LEFT JOIN m03_work_product AS WP
		ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
	LEFT JOIN m03_work_product_cstm AS WPCSTM
		ON WP.id=WPCSTM.id_c
	LEFT JOIN contacts AS SD
	ON WPCSTM.contact_id_c=SD.id
	LEFT JOIN users AS AU
	ON WPD.assigned_user_id=AU.id
	WHERE (WPDCSTM.deliverable_status_c = 'Waiting On Sponsor Ready to Audit' OR WPDCSTM.deliverable_status_c = 'Waiting on Sponsor') AND WPDCSTM.deliverable_c!='Protocol Development' AND WPD.deleted = 0 ORDER BY WPDCSTM.internal_final_due_date_c ASC";
$queryResult = $db->query($query);
while ($fetchResult = $db->fetchByAssoc($queryResult)) {
	$id	 					= $fetchResult['WPDID'];
	$WPDname	 			= $fetchResult['WPDname'];
	$WPC	 	    		= $compliance_dom[$fetchResult['WPC']];
	$WPDdeliverable	 		= $fetchResult['WPDdeliverable'];
	$WPD_Status	 			= $deliverable_dom[$fetchResult['WPD_Status']];
	$SDID	 				= $fetchResult['SDID'];
	$SDFname	 	    	= $fetchResult['Fname'];
	$SDLname	 	    	= $fetchResult['Lname'];
	$Assign_user	 		= $fetchResult['Assign_user'];
	$AUFname	 			= $fetchResult['AUFname'];
	$AULname	 			= $fetchResult['AULname'];
	$Duedate	 			= date("m/d/Y", strtotime($fetchResult['Duedate']));
        
	$wp_query = "SELECT id,parent_id,date_created from  m03_work_product_deliverable_audit 
	WHERE (after_value_string ='Waiting On Sponsor Ready to Audit' OR after_value_string ='Waiting on Sponsor') AND  parent_id = '" . $id . "'  group by parent_id";
								$queryResult1 = $db->query($wp_query);
								
								if ($queryResult1->num_rows > 0) {
									while ($fetchResult1 = $db->fetchByAssoc($queryResult1)) {
										$offset1 =  -18000;
										//$offset1 =  -21600; //daylight Saving
										$parent_id 	 = $fetchResult1['parent_id'];
										//$date_created1 = $fetchResult1['date_created'];
										$date_created = date("Y-m-d H:i:s", strtotime($fetchResult1['date_created'])+$offset1);
							
										$date1 = strtotime($date_created);
										$date2 = strtotime($current_date);
							
										$diff = abs($date1 - $date2);
										$days = floor(($diff) / (60 * 60 * 24));
										if ($days >= 30) {
											
											$SDfullname 	 =  $SDFname .  " " . $SDLname;
											$Assign_username = $AUFname .  " " . $AULname;

											$temp['wpid'] 	    		= $id;										
											$temp['WPDdeliverable'] 	= $WPDname;
											$temp['WPC'] 				= $WPC;
											$temp['WPD_Status'] 	    = $WPD_Status;
											$temp['SDID'] 	    		= $SDID;
											$temp['SDfullname'] 		= $SDfullname;
											$temp['Assign_userID'] 	    = $Assign_user;
											$temp['Assign_username'] 	= $Assign_username;
											$temp['Duedate'] 	    = $Duedate;																
											array_push($ReportData, $temp);	
										}
									}
								}
							
								
								
}
/* echo '<pre>';
print_r($ReportData); */

$ss->assign('date_created', $date_created);
$ss->assign('current_user', $current_user->id);
$ss->assign('ReportData', $ReportData);
$ss->assign('SITE_URL', $GLOBALS['sugar_config']['site_url']);
$html = $ss->fetch("custom/modules/Reports/tpls/CustomOuttoSponsorWPD.tpl");

echo $html;
