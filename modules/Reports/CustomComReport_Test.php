<?php
//AllDeviationsforStudy
require_once('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config, $app_list_strings, $db, $current_user;
$ss = new Sugar_Smarty();

 
//Submit form will check we have changed the filter or using by default filters.
if (isset($_REQUEST['submit_form']) && $_REQUEST['submit_form'] == 11) {

    $type_filter	= $_REQUEST["type_filter"];
    $wpc_1_filter	= $_REQUEST["wpc_1_filter"];
    $wpc_2_filter	= $_REQUEST["wpc_2_filter"];
    $wpc_3_filter	= $_REQUEST["wpc_3_filter"];
    $cwpa_1_filter	= $_REQUEST["cwpa_1_filter"];
    $cwpa_2_filter	= $_REQUEST["cwpa_2_filter"];
    $cwpa_3_filter	= $_REQUEST["cwpa_3_filter"];
    $cwpa_4_filter	= $_REQUEST["cwpa_4_filter"];

    $cwpa_1 = $_REQUEST["cwpa_1"];
    $cwpa_2 = $_REQUEST["cwpa_2"];
    $cwpa_3 = $_REQUEST["cwpa_3"];
    $cwpa_4 = $_REQUEST["cwpa_4"];


    if (!($type_filter == "empty" || $type_filter == "not_empty")) {
        $type_dropdown = implode(",", $_REQUEST["type_dropdown"]);
    }
    if (!($wpc_1_filter == "empty" || $wpc_1_filter == "not_empty")) {
        $work_product_code_id_1 = $_REQUEST["work_product_code_id_1"];
        $work_product_code_name_1 = $_REQUEST["work_product_code_name_1"];
    }
    if (!($wpc_2_filter == "empty" || $wpc_2_filter == "not_empty")) {
        $work_product_code_id_2 = $_REQUEST["work_product_code_id_2"];
        $work_product_code_name_2 = $_REQUEST["work_product_code_name_2"];
    }
    if (!($wpc_3_filter == "empty" || $wpc_3_filter == "not_empty")) {
        $work_product_code_id_3 = $_REQUEST["work_product_code_id_3"];
        $work_product_code_name_3 = $_REQUEST["work_product_code_name_3"];
    }
    if (!($cwpa_1_filter == "empty" || $cwpa_1_filter == "not_empty")) {
        $cwpa_1 = $_REQUEST["cwpa_1"];
    }
    if (!($cwpa_2_filter == "empty" || $cwpa_2_filter == "not_empty")) {
        $cwpa_2 = $_REQUEST["cwpa_2"];
    }
    if (!($cwpa_3_filter == "empty" || $cwpa_3_filter == "not_empty")) {
        $cwpa_3 = $_REQUEST["cwpa_3"];
    }
    if (!($cwpa_4_filter == "empty" || $cwpa_4_filter == "not_empty")) {
        $cwpa_4 = $_REQUEST["cwpa_4"];
    }

    $wp_data["type_filter"] = $type_filter;
    $wp_data["type_dropdown"] = implode(",", $_REQUEST["type_dropdown"]);

    $wp_data["wpc_1_filter"] = $wpc_1_filter;
    $wp_data["wpc_1_id"] = $work_product_code_id_1;
    $wp_data["wpc_1_name"] = $work_product_code_name_1;

    $wp_data["wpc_2_filter"] = $wpc_2_filter;
    $wp_data["wpc_2_id"] = $work_product_code_id_2;
    $wp_data["wpc_2_name"] = $work_product_code_name_2;

    $wp_data["wpc_3_filter"] = $wpc_3_filter;
    $wp_data["wpc_3_id"] = $work_product_code_id_3;
    $wp_data["wpc_3_name"] = $work_product_code_name_3;

    $wp_data["cwpa_1_filter"] = $cwpa_1_filter;
    $wp_data["cwpa_1_value"] = $cwpa_1;

    $wp_data["cwpa_2_filter"] = $cwpa_2_filter;
    $wp_data["cwpa_2_value"] = $cwpa_2;

    $wp_data["cwpa_3_filter"] = $cwpa_3_filter;
    $wp_data["cwpa_3_value"] = $cwpa_3;

    $wp_data["cwpa_4_filter"] = $cwpa_4_filter;
    $wp_data["cwpa_4_value"] = $cwpa_4;

   $ORDER_TYPE=$_REQUEST["order_type"];
    $ORDER_COLUMN=$_REQUEST["order_column"];

} else {

    $wp_data["type_filter"] = "one_of";
    $wp_data["type_dropdown"] = "Failed Sham,Passed Sham,Failed Pyrogen,Passed Pyrogen";

    $wp_data["wpc_1_filter"] = "is";
    $wp_data["wpc_1_id"] = "c81c2577-bd7c-16e3-1248-5785ac111a73";
    $wp_data["wpc_1_name"] = "ST01";

    $wp_data["wpc_2_filter"] = "is";
    $wp_data["wpc_2_id"] = "426d29a9-0810-54b0-1834-5788f6505070";
    $wp_data["wpc_2_name"] = "ST01a";

    $wp_data["wpc_3_filter"] = "is";
    $wp_data["wpc_3_id"] = "00535bde-96f2-11e7-a240-02feb3423f0d";
    $wp_data["wpc_3_name"] = "ST09";

    $wp_data["cwpa_1_filter"] = "equals";
    $wp_data["cwpa_1_value"] = "APS177-AH15";

    $wp_data["cwpa_2_filter"] = "contains";
    $wp_data["cwpa_2_value"] = "ST01";

    $wp_data["cwpa_3_filter"] = "contains";
    $wp_data["cwpa_3_value"] = "ST01a";

    $wp_data["cwpa_4_filter"] = "contains";
    $wp_data["cwpa_4_value"] = "ST09";
    
    $ORDER_TYPE="ASC";
    $ORDER_COLUMN="anml_animals_name";
}


if($_REQUEST["order_type"]!="" && $_REQUEST["order_type"]!=""){
    $ORDER_TYPE     = $_REQUEST["order_type"];
    $ORDER_COLUMN   = $_REQUEST["order_column"];
}else{   
    $ORDER_TYPE     = "ASC";
    $ORDER_COLUMN   = "anml_animals_name";     
}

$typeAppend = "";
switch ($type_filter) {
    case "is":
        $type_op= $_REQUEST["type_dropdown"];
        $type_op_string="'".implode("', '", $type_op)."'";
        $typeAppend = "(((l1_cstm.error_type_c =(".$type_op_string."))))";
        break;
    case "is_not":
        $type_op= $_REQUEST["type_dropdown"];
        $type_op_string="'".implode("', '", $type_op)."'";
        $typeAppend = "(((l1_cstm.error_type_c <> (".$type_op_string.") OR (l1_cstm.error_type_c IS NULL AND (".$type_op_string.") IS NOT NULL))))";
        break;
    case "one_of":
        $type_op= $_REQUEST["type_dropdown"];
        $type_op_string="'".implode("', '", $type_op)."'";
        $typeAppend = "(((l1_cstm.error_type_c IN (".$type_op_string."))))";
        break;
    case "not_one_of":
        $type_op= $_REQUEST["type_dropdown"];
        $type_op_string="'".implode("', '", $type_op)."'";
        $typeAppend = "(((l1_cstm.error_type_c NOT IN (".$type_op_string."))))";
        break;
    case "empty":
        $typeAppend = "((((COALESCE(LENGTH(IFNULL(l1_cstm.error_type_c, '')), 0) = 0 OR IFNULL(l1_cstm.error_type_c, '') = '^^'))))";
        break;
    case "not_empty":
        $typeAppend = "((((COALESCE(LENGTH(IFNULL(l1_cstm.error_type_c, '')), 0) > 0 OR IFNULL(l1_cstm.error_type_c, '') != '^^'))))";
        break;
    default:
        $typeAppend = "(((l1_cstm.error_type_c IN ('Failed Sham','Passed Sham','Failed Pyrogen','Passed Pyrogen'))))";
}
$wpc1Append = "";
switch ($wpc_1_filter) {
    case "is":
        $wpc1Append = "((l2_cstm.m03_work_product_code_id1_c = '".$work_product_code_id_1."'))";
        break;
    case "is_not":
        $wpc1Append = "((l2_cstm.m03_work_product_code_id1_c <> '".$work_product_code_id_1."'))";
        break;
    case "empty":
        $wpc1Append = "((COALESCE(LENGTH(l2_cstm.m03_work_product_code_id1_c), 0) = 0))";
        break;
    case "not_empty":
        $wpc1Append = "((COALESCE(LENGTH(l2_cstm.m03_work_product_code_id1_c), 0) <> 0))";
        break;
    default:
        $wpc1Append = "(( l2_cstm.m03_work_product_code_id1_c='c81c2577-bd7c-16e3-1248-5785ac111a73'))";
}
$wpc2Append = "";
switch ($wpc_2_filter) {
    case "is":
        $wpc2Append = "((l2_cstm.m03_work_product_code_id1_c = '".$work_product_code_id_2."'))";
        break;
    case "is_not":
        $wpc2Append = "((l2_cstm.m03_work_product_code_id1_c <> '".$work_product_code_id_2."'))";
        break;
    case "empty":
        $wpc2Append = "((COALESCE(LENGTH(l2_cstm.m03_work_product_code_id1_c), 0) = 0))";
        break;
    case "not_empty":
        $wpc2Append = "((COALESCE(LENGTH(l2_cstm.m03_work_product_code_id1_c), 0) <> 0))";
        break;
    default:
        $wpc2Append = "(( l2_cstm.m03_work_product_code_id1_c='426d29a9-0810-54b0-1834-5788f6505070'))";
}
$wpc3Append = "";
switch ($wpc_3_filter) {
    case "is":
        $wpc3Append = "((l2_cstm.m03_work_product_code_id1_c = '".$work_product_code_id_3."'))";
        break;
    case "is_not":
        $wpc3Append = "((l2_cstm.m03_work_product_code_id1_c <> '".$work_product_code_id_3."'))";
        break;
    case "empty":
        $wpc3Append = "((COALESCE(LENGTH(l2_cstm.m03_work_product_code_id1_c), 0) = 0))";
        break;
    case "not_empty":
        $wpc3Append = "((COALESCE(LENGTH(l2_cstm.m03_work_product_code_id1_c), 0) <> 0))";
        break;
    default:
        $wpc3Append = "((l2_cstm.m03_work_product_code_id1_c = '00535bde-96f2-11e7-a240-02feb3423f0d'))";
}
$cwpa1Append = "";
switch ($cwpa_1_filter) {
    case "equals":
        $cwpa1Append = "(anml_animals_cstm.assigned_to_wp_c = '".$cwpa_1."')";
        break;
    case "not_equals_str":
        $cwpa1Append = "(anml_animals_cstm.assigned_to_wp_c != '".$cwpa_1."')";
        break;
    case "contains":
        $cwpa1Append = "(anml_animals_cstm.assigned_to_wp_c LIKE '%".$cwpa_1."%')";
        break;
    case "does_not_contain":
        $cwpa1Append = "(anml_animals_cstm.assigned_to_wp_c NOT LIKE '%".$cwpa_1."%' OR (anml_animals_cstm.assigned_to_wp_c IS NULL))";
        break;
    case "empty":
        $cwpa1Append = "((COALESCE(LENGTH(anml_animals_cstm.assigned_to_wp_c), 0) = 0))";
        break;
    case "not_empty":
        $cwpa1Append = "((COALESCE(LENGTH(anml_animals_cstm.assigned_to_wp_c), 0) <> 0))";
        break;
    default:
        $cwpa1Append = "(anml_animals_cstm.assigned_to_wp_c = 'APS177-AH15')";
}
$cwpa2Append = "";
switch ($cwpa_2_filter) {
    case "equals":
        $cwpa2Append = "(anml_animals_cstm.assigned_to_wp_c = '".$cwpa_2."')";
        break;
    case "not_equals_str":
        $cwpa2Append = "(anml_animals_cstm.assigned_to_wp_c != '".$cwpa_2."')";
        break;
    case "contains":
        $cwpa2Append = "(anml_animals_cstm.assigned_to_wp_c LIKE '%".$cwpa_2."%')";
        break;
    case "does_not_contain":
        $cwpa2Append = "(anml_animals_cstm.assigned_to_wp_c NOT LIKE '%".$cwpa_2."%' OR (anml_animals_cstm.assigned_to_wp_c IS NULL))";
        break;
    case "empty":
        $cwpa2Append = "((COALESCE(LENGTH(anml_animals_cstm.assigned_to_wp_c), 0) = 0))";
        break;
    case "not_empty":
        $cwpa2Append = "((COALESCE(LENGTH(anml_animals_cstm.assigned_to_wp_c), 0) <> 0))";
        break;
    default:
        $cwpa2Append = "(anml_animals_cstm.assigned_to_wp_c LIKE '%ST01%')";
}
$cwpa3Append = "";
switch ($cwpa_3_filter) {
    case "equals":
        $cwpa3Append = "(anml_animals_cstm.assigned_to_wp_c = '".$cwpa_3."')";
        break;
    case "not_equals_str":
        $cwpa3Append = "(anml_animals_cstm.assigned_to_wp_c != '".$cwpa_3."')";
        break;
    case "contains":
        $cwpa3Append = "(anml_animals_cstm.assigned_to_wp_c LIKE '%".$cwpa_3."%')";
        break;
    case "does_not_contain":
        $cwpa3Append = "(anml_animals_cstm.assigned_to_wp_c NOT LIKE '%".$cwpa_3."%' OR (anml_animals_cstm.assigned_to_wp_c IS NULL))";
        break;
    case "empty":
        $cwpa3Append = "((COALESCE(LENGTH(anml_animals_cstm.assigned_to_wp_c), 0) = 0))";
        break;
    case "not_empty":
        $cwpa3Append = "((COALESCE(LENGTH(anml_animals_cstm.assigned_to_wp_c), 0) <> 0))";
        break;
    default:
        $cwpa3Append = "(anml_animals_cstm.assigned_to_wp_c LIKE '%ST01a%')";
}
$cwpa4Append = "";
switch ($cwpa_4_filter) {
    case "equals":
        $cwpa4Append = "(anml_animals_cstm.assigned_to_wp_c = '".$cwpa_4."')";
        break;
    case "not_equals_str":
        $cwpa4Append = "(anml_animals_cstm.assigned_to_wp_c != '".$cwpa_4."')";
        break;
    case "contains":
        $cwpa4Append = "(anml_animals_cstm.assigned_to_wp_c LIKE '%".$cwpa_4."%')";
        break;
    case "does_not_contain":
        $cwpa4Append = "(anml_animals_cstm.assigned_to_wp_c NOT LIKE '%".$cwpa_4."%' OR (anml_animals_cstm.assigned_to_wp_c IS NULL))";
        break;
    case "empty":
        $cwpa4Append = "((COALESCE(LENGTH(anml_animals_cstm.assigned_to_wp_c), 0) = 0))";
        break;
    case "not_empty":
        $cwpa4Append = "((COALESCE(LENGTH(anml_animals_cstm.assigned_to_wp_c), 0) <> 0))";
        break;
    default:
        $cwpa4Append = "(anml_animals_cstm.assigned_to_wp_c LIKE '%ST09%')";
}

/* Add New Condition for Ticket # 1630 */

echo "<br>===>".$tsQuery    = "SELECT TS.id,TS.name FROM anml_animals AS TS 
    LEFT JOIN anml_animals_cstm AS TSC ON TS.id=TSC.id_c
    LEFT JOIN anml_animals_wpe_work_product_enrollment_1_c AS TSWPE
        on TSC.id_c= TSWPE.anml_animals_wpe_work_product_enrollment_1anml_animals_ida and TSWPE.deleted=0
    LEFT JOIN m03_work_product_wpe_work_product_enrollment_1_c AS WPWPE
        on TSWPE.anml_anima9941ollment_idb= WPWPE.m03_work_p9bf5ollment_idb AND WPWPE.deleted=0 
            AND WPWPE.m03_work_p7d13product_ida='99ac34ca-e074-c580-0c73-5702c7e2f7c0'
    WHERE TSC.assigned_to_wp_c='APS177-AH15' AND TS.deleted=0 GROUP BY TS.id";
$resTS      = $db->query($tsQuery);
$tsArr      = array();
if($resTS->num_rows>0){
    while ($rowTS = $db->fetchByassoc($resTS)) {
        //echo "<br>==>".$rowTS['name']."===".$tsID       = $rowTS['id'];
        $tsID       = $rowTS['id'];
        $addTS_flag = 1;

        echo "<br><br>Inner Query===>".$tsCommQry = "SELECT TSCOMM.m06_error_anml_animals_1m06_error_ida AS CommID,
                COMMC.error_category_c,COMMC.error_type_c 
            FROM  m06_error_anml_animals_1_c AS TSCOMM 
            LEFT JOIN m06_error_cstm AS COMMC ON TSCOMM.m06_error_anml_animals_1m06_error_ida=COMMC.id_c
            WHERE m06_error_anml_animals_1anml_animals_idb = '".$tsID."'
            AND COMMC.error_category_c='Work Product Schedule Outcome' AND TSCOMM.deleted=0";
            // AND (COMMC.error_type_c!='Passed Sham' OR COMMC.error_type_c!='Failed Sham') 
        $resTSCom   = $db->query($tsCommQry);
        if($resTSCom->num_rows>0){
            //$ctr = 0;
            while ($rowTSCom = $db->fetchByassoc($resTSCom)) {
				 
                $commCategory   = $rowTSCom['error_category_c'];
                $commType       = $rowTSCom['error_type_c'];
                 echo "<br>comm===>".$commID         = $rowTSCom['CommID']; 
                if($commCategory == "Work Product Schedule Outcome" && ($commType=="Passed Sham" || $commType=="Failed Sham"))
                { 
					$addTS_flag = 1;//break;
                }elseif($commCategory == "Work Product Schedule Outcome" && $commType=="Performed Per Protocol"){

                    $checkWPQry		= "SELECT * FROM m06_error_m03_work_product_1_c AS COMMWP  WHERE COMMWP.m06_error_m03_work_product_1m06_error_ida = '".$commID."' 
                    AND COMMWP.deleted=0 "; 
                    $resWPCOM		= $db->query($checkWPQry);
                    if($resWPCOM->num_rows>0){
                        //$ctr = 0;
                        $addTS_flag = 0;
                        $commWPID   = array();
                        while ($rowWPCOM = $db->fetchByassoc($resWPCOM)) {
                            $commWPID[] = $rowWPCOM['m06_error_m03_work_product_1m03_work_product_idb'];    
                        }
                        if (in_array('99ac34ca-e074-c580-0c73-5702c7e2f7c0', $commWPID) && count($commWPID)==1) {
                           
                        }
                    }  
                }else{
                    $addTS_flag = 0;
                }
            }
        }else{
            //$addTS_flag = 1;
        }
            
        if($tsID!="" && $addTS_flag==1){
            $tsArr[] = $tsID;
        }//echo "<br>==>".$tsID."===".$addTS_flag;
    }
    echo "<pre>";
    print_r($tsArr);  
    if(count($tsArr)>0){
        $tsString = implode("','",$tsArr);
    }  
}
 echo "<br>==>".$tsString;
 $tsCondition = "";
if($tsString!=""){
    $tsCondition = " OR anml_animals.id IN ('".$tsString."')";
}



/*  */

$ReportData     = array();
$summaryData    = array();

// To get the count order by subtype c
echo "<br><br><br>==>".$sqlSubTypeCount = "SELECT IFNULL(anml_animals.name,'') anml_animals_name
,COUNT(anml_animals.id) anml_animals__allcount, COUNT(DISTINCT  anml_animals.id) anml_animals__count
FROM anml_animals
LEFT JOIN  m06_error_anml_animals_1_c l1_1 ON anml_animals.id=l1_1.m06_error_anml_animals_1anml_animals_idb AND l1_1.deleted=0

LEFT JOIN  m06_error l1 ON l1.id=l1_1.m06_error_anml_animals_1m06_error_ida AND l1.deleted=0
LEFT JOIN  m06_error_m03_work_product_1_c l2_1 ON l1.id=l2_1.m06_error_m03_work_product_1m06_error_ida AND l2_1.deleted=0

LEFT JOIN  m03_work_product l2 ON l2.id=l2_1.m06_error_m03_work_product_1m03_work_product_idb AND l2.deleted=0
LEFT JOIN anml_animals_cstm anml_animals_cstm ON anml_animals.id = anml_animals_cstm.id_c
LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
LEFT JOIN m03_work_product_cstm l2_cstm ON l2.id = l2_cstm.id_c
LEFT JOIN contacts contacts1 ON contacts1.id = l2_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
LEFT JOIN m03_work_product_code m03_work_product_code2 ON m03_work_product_code2.id = l2_cstm.m03_work_product_code_id1_c AND IFNULL(m03_work_product_code2.deleted,0)=0 

WHERE ((($typeAppend AND ((
    $wpc1Append OR 
    $wpc2Append OR 
    $wpc3Append
)) AND ((
    $cwpa1Append
    OR $cwpa2Append
    OR $cwpa3Append
    OR $cwpa4Append
)) ".$tsCondition.") 
AND ((((COALESCE(LENGTH(anml_animals_cstm.termination_date_c),0) = 0)))))) 
AND  anml_animals.deleted=0 
 AND IFNULL(contacts1.deleted,0)=0 
 GROUP BY anml_animals.name
 ORDER BY anml_animals_name ASC";

$resultSubtypeCount = $db->query($sqlSubTypeCount);
while ($rowSubtype = $db->fetchByassoc($resultSubtypeCount)) {
    array_push($summaryData, array(
        'anml_animals_name'			=> $rowSubtype['anml_animals_name'],
        'anml_animals__allcount'	=> $rowSubtype['anml_animals__allcount'],
        'anml_animals__count'		=> $rowSubtype['anml_animals__count'],
    ));
}

echo "<br><br><br>==>".$sql = "SELECT IFNULL(anml_animals.id,'') primaryid
,IFNULL(anml_animals.name,'') anml_animals_name
,anml_animals_cstm.usda_id_c anml_animals_cstm_usda_id_c,anml_animals_cstm.assigned_to_wp_c ANML_ANIMALS_CSTM_ASSIB5E836,IFNULL(l1.id,'') l1_id
,IFNULL(l1.name,'') l1_name
,IFNULL(l1_cstm.error_type_c,'') l1_cstm_error_type_c,l1_cstm.activities_c l1_cstm_activities_c,l1_cstm.actual_event_c l1_cstm_actual_event_c,IFNULL(l2.id,'') l2_id
,IFNULL(l2.name,'') l2_name
,l2_cstm.first_procedure_c l2_cstm_first_procedure_c,l2_cstm.contact_id_c l2_cstm_contact_id_c,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contacts1_name,l1.date_entered l1_date_entered
FROM anml_animals
LEFT JOIN  m06_error_anml_animals_1_c l1_1 ON anml_animals.id=l1_1.m06_error_anml_animals_1anml_animals_idb AND l1_1.deleted=0

LEFT JOIN  m06_error l1 ON l1.id=l1_1.m06_error_anml_animals_1m06_error_ida AND l1.deleted=0
LEFT JOIN  m06_error_m03_work_product_1_c l2_1 ON l1.id=l2_1.m06_error_m03_work_product_1m06_error_ida AND l2_1.deleted=0

LEFT JOIN  m03_work_product l2 ON l2.id=l2_1.m06_error_m03_work_product_1m03_work_product_idb AND l2.deleted=0
LEFT JOIN anml_animals_cstm anml_animals_cstm ON anml_animals.id = anml_animals_cstm.id_c
LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
LEFT JOIN m03_work_product_cstm l2_cstm ON l2.id = l2_cstm.id_c
LEFT JOIN contacts contacts1 ON contacts1.id = l2_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
LEFT JOIN m03_work_product_code m03_work_product_code2 ON m03_work_product_code2.id = l2_cstm.m03_work_product_code_id1_c AND IFNULL(m03_work_product_code2.deleted,0)=0 

 WHERE ( (($typeAppend AND ((
    $wpc1Append OR 
    $wpc2Append OR 
    $wpc3Append
)) AND ((
    $cwpa1Append
    OR $cwpa2Append
    OR $cwpa3Append
    OR $cwpa4Append
)) ".$tsCondition.") 
AND ((((COALESCE(LENGTH(anml_animals_cstm.termination_date_c),0) = 0)))))) 
AND  anml_animals.deleted=0 
 AND IFNULL(contacts1.deleted,0)=0 
order by anml_animals_name ASC, l1_date_entered desc";

$result = $db->query($sql);
$p=$result->num_rows;
$i = 0;
$offset1 =  -18000;
$offset1 =  -21600; //daylight Saving
$previousId = "";
while ($row = $db->fetchByassoc($result)) {

    if ($row['primaryid'] != $previousId) {
        $firstProcedure = "";
        if ($row['l2_cstm_first_procedure_c'] != "") {
            $firstProcedure = date("m/d/Y", strtotime($row['l2_cstm_first_procedure_c']));
        }

        $temp = array();
        $temp['l1_id']                  = $row['l1_id'];
        $temp['primaryid']              = $row['primaryid'];
        $temp['anml_animals_name']      = $row['anml_animals_name'];
        $temp['anml_animals_cstm_usda_id_c']    = $row['anml_animals_cstm_usda_id_c'];
        $temp['ANML_ANIMALS_CSTM_ASSIB5E836']   = $row['ANML_ANIMALS_CSTM_ASSIB5E836'];
        
        $temp['l1_cstm_error_type_c']   = $row['l1_cstm_error_type_c'];
        $temp['l1_name']                = $row['l1_name'];
        $temp['l1_cstm_activities_c']   = str_replace("^", "", $row['l1_cstm_activities_c']);
        $temp['l1_cstm_actual_event_c'] = $row['l1_cstm_actual_event_c'];
        $temp['l2_name']                = $row['l2_name'];
        $temp['l2_id']                  = $row['l2_id'];
        $temp['l2_cstm_first_procedure_c']  = $firstProcedure;
        $temp['l2_cstm_contact_id_c']       = $row['l2_cstm_contact_id_c'];
        $temp['contacts1_name']             = $row['contacts1_name'];
        if($row['l1_date_entered']!=""){
            $temp['l1_date_entered']  = date("m/d/Y H:i", strtotime($row['l1_date_entered']) + $offset1);
        }else{
            $temp['l1_date_entered'] = "";
        }
        
        $temp['summaryData'] = $summaryData[$i];
        array_push($ReportData, $temp);
        $i++;
        $previousId = $row['primaryid'];
    }
}


$ot="";
if($ORDER_TYPE=="ASC"){
    $ot=SORT_ASC;
}else{
    $ot=SORT_DESC;
}
array_multisort(array_column($ReportData, $ORDER_COLUMN), $ot, $ReportData);



$ss->assign('ReportData', $ReportData);
$ss->assign('SITE_URL', $GLOBALS['sugar_config']['site_url']);
$ss->assign('ORDER_TYPE', ($ORDER_TYPE=="ASC")?"DESC":"ASC");
$ss->assign('ORDER_COLUMN', $ORDER_COLUMN);
$ss->assign('GrandTotal', count($ReportData));
$ss->assign('WPDATA', $wp_data);
$ss->assign('TOTAL_RES',$p);

$html = $ss->fetch("custom/modules/Reports/tpls/CustomComReport.tpl");

echo $html;