<?php
require_once('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config, $app_list_strings, $db, $current_user;
$ss = new Sugar_Smarty();

if (isset($_REQUEST['date_created']) && $_REQUEST['date_created'] != ''  && $_REQUEST['submit_form'] == 1) {
        $date_created = $_REQUEST['date_created'];
        $date_created_start = $date_created .' 02:00:00';
        $date_created_end = $date_created .' 23:59:00';
       
}
/* if($date_created!=""){
        $date_created = date("Y-m-d",strtotime($date_created));
        $offset1 =  -21600; //daylight Saving
        $offset_time = strtotime($date_created) + $offset1;
        $date_created = date("Y-m-d H:i:s",$offset_time);
        //$date_created = explode(" ", $date_created);
        //$date_created = $date_created[0];                         
                   
} */

/** Get all Work Product Assignment records with Date Created = run time filter date */

/* $sql = "SELECT WPE.id AS wpeid,WPE.name AS wpename,WPE.date_entered,WPECSTM.id_c,WPECSTM.comments_c,WPEANML.id,WPEANML.anml_animals_wpe_work_product_enrollment_1anml_animals_ida,WPEANML.anml_anima9941ollment_idb,tbl_anml.id AS tsid,tbl_anml.name AS tsname FROM wpe_work_product_enrollment AS WPE
left join wpe_work_product_enrollment_cstm AS WPECSTM on WPECSTM.id_c = WPE.id
LEFT JOIN anml_animals_wpe_work_product_enrollment_1_c AS WPEANML ON WPEANML.anml_anima9941ollment_idb = WPE.id
LEFT JOIN anml_animals AS tbl_anml ON tbl_anml.id = WPEANML.anml_animals_wpe_work_product_enrollment_1anml_animals_ida
WHERE DATE(WPE.date_entered)='" . $date_created . "' and WPE.deleted=0"; */

$sql = "SELECT WPE.id AS wpeid,WPE.name AS wpename,WPE.date_entered,WPECSTM.id_c,WPECSTM.comments_c,WPEANML.id,WPEANML.anml_animals_wpe_work_product_enrollment_1anml_animals_ida,WPEANML.anml_anima9941ollment_idb,tbl_anml.id AS tsid,tbl_anml.name AS tsname FROM wpe_work_product_enrollment AS WPE
left join wpe_work_product_enrollment_cstm AS WPECSTM on WPECSTM.id_c = WPE.id
LEFT JOIN anml_animals_wpe_work_product_enrollment_1_c AS WPEANML ON WPEANML.anml_anima9941ollment_idb = WPE.id
LEFT JOIN anml_animals AS tbl_anml ON tbl_anml.id = WPEANML.anml_animals_wpe_work_product_enrollment_1anml_animals_ida
WHERE WPE.date_entered between '".$date_created_start."' and '".$date_created_end."' and WPE.deleted=0";

echo $sql;
echo '<br/><br/><br/><br/><br/>';
$ReportWPEData = array();
$wpedata = array();
$result = $db->query($sql);
while ($row = $db->fetchByassoc($result)) {
        $wpedata['wpeid'] = $row['wpeid'];
        $wpedata['wpename'] = $row['wpename'];
        $wpedata['comments_c'] = $row['comments_c'];
        $wpedata['date_entered'] = $row['date_entered'];
        $wpedata['tsid'] = $row['tsid'];
        $wpedata['tsname'] = $row['tsname'];
        $TSmBean = BeanFactory::retrieveBean('ANML_Animals', $row['tsid']);
        $usda_id_c = $TSmBean->usda_id_c;
        $abnormality_c = $TSmBean->abnormality_c;
        $wpedata['usda_id_c'] = $usda_id_c;
        $wpedata['abnormality_c'] = $abnormality_c;
        array_push($ReportWPEData, $wpedata);
}
/**Get all Test Systems where the Change Date for the Allocated Work Product field = run time filter date (so for the TS row, it would be looking at the audit log). */
/* $sql_TS = "SELECT id,parent_id,before_value_string,after_value_string from  anml_animals_audit WHERE DATE(date_created)='" . $date_created . "' AND field_name='m03_work_product_id_c' group by parent_id order by date_created DESC"; */
$sql_TS = "SELECT id,parent_id,before_value_string,after_value_string from  anml_animals_audit 
WHERE date_created between '".$date_created_start."' and '".$date_created_end."' AND field_name='m03_work_product_id_c' group by parent_id order by date_created DESC";
$tsdata = array();
$ReportTSData = array();
//$GLOBALS['log']->fatal('sql_TS line 40 ' . $sql_TS);
echo $sql_TS;
$result_Ts = $db->query($sql_TS);
while ($row_ts = $db->fetchByassoc($result_Ts)) {
        $tsdata['ts_id'] = $row_ts['parent_id'];
        $allocate_wp_id = $row_ts['after_value_string'];
        $testSystemBean = BeanFactory::retrieveBean('ANML_Animals', $row_ts['parent_id']);
        $tsname = $testSystemBean->name;
        $allocate_wp_name = $testSystemBean->allocated_work_product_c;
        $usda_id_c = $testSystemBean->usda_id_c;
        $abnormality_c = $testSystemBean->abnormality_c;
        $tsdata['date_created'] = $row_ts['date_created'];
        $tsdata['allocate_wp_id'] = $allocate_wp_id;
        $tsdata['allocate_wp_name'] = $allocate_wp_name;
        $tsdata['tsname'] = $tsname;
        $tsdata['usda_id_c'] = $usda_id_c;
        $tsdata['abnormality_c'] = $abnormality_c;
        array_push($ReportTSData, $tsdata);
}
$ss->assign('date_created', $date_created);
$ss->assign('current_user', $current_user->id);
$ss->assign('ReportWPEData', $ReportWPEData);
$ss->assign('ReportTSData', $ReportTSData);
$ss->assign('SITE_URL', $GLOBALS['sugar_config']['site_url']);
$html = $ss->fetch("custom/modules/Reports/tpls/CustomWPAAllocationtest.tpl");
echo $html;
