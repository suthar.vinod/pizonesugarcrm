<?php
//CustomReportUtils.php
function AllAdverseEventsforStudy($work_product_id)
{
	global $db, $app_list_strings;
	if ($work_product_id) {
		$ReportData = array();
		$ReportClassificationData = array();
		$RelatedCommData = array();
		$test_system_ids = array();
		$usda_ids = array();
		$work_product = array();
		// To get the count order by subtype c
		$sqlClassificationCount = "SELECT l1_cstm.subtype_c l1_cstm_subtype_c,l1_cstm.error_classification_c l1_cstm_error_classification_c,IFNULL(m03_work_product.name,'') m03_work_product_name
,count(*) count,l1.name,
ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id
FROM m03_work_product
LEFT JOIN  m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id=l1_1.m06_error_m03_work_product_1m03_work_product_idb AND l1_1.deleted=0

		LEFT JOIN  m06_error l1 ON l1.id=l1_1.m06_error_m03_work_product_1m06_error_ida AND l1.deleted=0
		LEFT JOIN  m06_error_anml_animals_1_c l2_1 ON l1.id=l2_1.m06_error_anml_animals_1m06_error_ida AND l2_1.deleted=0

		LEFT JOIN  anml_animals l2 ON l2.id=l2_1.m06_error_anml_animals_1anml_animals_idb AND l2.deleted=0
		LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
		LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
		LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb AND ReletedComm.deleted=0
		 WHERE (((IFNULL(l1_cstm.error_classification_c,'') IN ('Adverse Event',
					  'Adverse Event Study Article Related',
					  'Adverse Event Procedure Related',
					  'Adverse Event Test System Model Related',
					  'Adverse Event Etiology Unknown')
) AND (m03_work_product.id='$work_product_id'
))) 
AND  m03_work_product.deleted=0 
 GROUP BY l1.name,l1_cstm.error_classification_c,l1_cstm.subtype_c 
,m03_work_product.name
 ORDER BY l1_cstm_error_classification_c ASC,l1_cstm_subtype_c ASC,m03_work_product_name ASC";

		$resultClassificationCount = $db->query($sqlClassificationCount);
		$hash = array();
		$ReportClassificationDataCount = array();
		while ($rowClassification = $db->fetchByassoc($resultClassificationCount)) {
			$hash_key = str_replace('^', '', $rowClassification['l1_cstm_error_classification_c']);
			$hash_key = $app_list_strings['error_classification_c_list'][$hash_key];
			if (!array_key_exists($hash_key, $hash)) {
				$hash[$hash_key] = sizeof($ReportClassificationDataCount);
				$classificationValue = $app_list_strings['error_classification_c_list'][str_replace('^', '', $rowClassification['l1_cstm_error_classification_c'])];
				array_push($ReportClassificationDataCount, array(
					'm03_work_product_name' => $rowClassification['m03_work_product_name'],
					'l1_cstm_error_classification_c' => $classificationValue,
					'name' => $rowClassification['name'],
					'rel_comm_id' => $rowClassification['rel_comm_id'],
					'count' => 0,
				));
			}
			$ReportClassificationDataCount[$hash[$hash_key]]['count'] += 1;
		}

		/*Query to get Report Data*/
		$sql =
			"SELECT
	IFNULL(l1.id, '') l1_id,
	IFNULL(l1. NAME, '') l1_name,
	ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id,
	IFNULL(m03_work_product.id, '') m03_work_product_id,
	l2_cstm.animal_id_c l2_cstm_animal_id_c,
	l2_cstm.usda_id_c l2_cstm_usda_id_c,
	
	CONCAT (l2_cstm.animal_id_c,'-',l2_cstm.usda_id_c) AS TestUsda,
	l1_cstm.date_error_occurred_c L1_CSTM_DATE_ERROR_OCC6BD1CE,
	l1_cstm.actual_event_c l1_cstm_actual_event_c,
	l1_cstm.resolution_c l1_cstm_resolution_c,
	l1_cstm.impactful_dd_c impactful_dd_c,
	IFNULL(
		l1_cstm.corrective_action_1_c,
		''
	) L1_CSTM_CORRECTIVE_ACT674D79,
	l1_cstm.rationale_for_no_ca_c L1_CSTM_RATIONALE_FOR_F48A84,
	
	l1_cstm.subtype_c l1_cstm_subtype_c,
	l1_cstm.error_classification_c l1_cstm_error_classification_c,
	IFNULL(m03_work_product. NAME, '') m03_work_product_name
  FROM
  	m03_work_product
  LEFT JOIN m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id = l1_1.m06_error_m03_work_product_1m03_work_product_idb
  AND l1_1.deleted = 0
  LEFT JOIN m06_error l1 ON l1.id = l1_1.m06_error_m03_work_product_1m06_error_ida
  AND l1.deleted = 0
  LEFT JOIN m06_error_anml_animals_1_c l2_1 ON l1.id = l2_1.m06_error_anml_animals_1m06_error_ida
  AND l2_1.deleted = 0
  LEFT JOIN anml_animals l2 ON l2.id = l2_1.m06_error_anml_animals_1anml_animals_idb
  AND l2.deleted = 0
  LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
  LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
  LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb
  AND ReletedComm.deleted=0
  WHERE
  	(
  		(
  			(
  				IFNULL(
  					l1_cstm.error_classification_c,
  					''
  				) IN (
					  'Adverse Event',
					  'Adverse Event Study Article Related',
					  'Adverse Event Procedure Related',
					  'Adverse Event Test System Model Related',
					  'Adverse Event Etiology Unknown'
					  )
  			) 
  			AND (
  				m03_work_product.id = '$work_product_id'
  				
  			)
  		)
  	)
  AND m03_work_product.deleted = 0
  ORDER BY
  L1_CSTM_DATE_ERROR_OCC6BD1CE ASC,
  l1_cstm_error_classification_c ASC,
  l1_cstm_subtype_c ASC,
  m03_work_product_name ASC";

		$result = $db->query($sql);
		$work_product['work_product_id'] = $work_product_id;
		$commListArr = array();
		$returnData = array();
		while ($row = $db->fetchByassoc($result)) {
			$temp = array();
			$commListArr[]  = $row['l1_id'];
			$temp['communication_id'] = $row['l1_id'];
			$temp['communication_name'] = $row['l1_name'];
			$temp['work_product_id'] = $row['m03_work_product_id'];
			$temp['work_product_name'] = $row['m03_work_product_name'];
			$temp['test_system'] = $row['l2_cstm_animal_id_c'];
			$temp['usda_id'] = $row['l2_cstm_usda_id_c'];
			$temp['rel_comm_id'] = $row['rel_comm_id'];
			$temp['impactful_dd_c'] = $row['impactful_dd_c'];

			if ($row['L1_CSTM_DATE_ERROR_OCC6BD1CE'] != "") {
				$temp['date_occurred'] = date("m/d/Y", strtotime($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']));
			} else {
				$temp['date_occurred'] = "";
			}

			$temp['actual_assessment_date'] = $row['l1_cstm_reinspection_date_c'];
			$temp['l1_cstm_subtype_c'] =  str_replace('^', '', $row['l1_cstm_subtype_c']);
			$temp['l1_cstm_error_classification_c'] =  str_replace('^', '', $row['l1_cstm_error_classification_c']);
			$temp['actual_event'] = $row['l1_cstm_actual_event_c'];
			$temp['sd_assessment'] = $row['l1_cstm_resolution_c'];
			$temp['subtype'] = str_replace('^', '', $row['l1_cstm_subtype_c']);
			$temp['subtype'] = $app_list_strings['error_type_list'][$temp['subtype']];
			$temp['classification'] = str_replace('^', '', $row['l1_cstm_error_classification_c']);
			$temp['classification'] = $app_list_strings['error_classification_c_list'][$temp['classification']];
			$temp['corrective_action'] = $row['L1_CSTM_CORRECTIVE_ACT674D79'];
			$temp['corrective_action_details'] = $row['L1_CSTM_RATIONALE_FOR_F48A84'];
			$testSystemUsdaID = $temp['test_system'];

			if ($temp['usda_id'] != "")
				$testSystemUsdaID .= "-" . $temp['usda_id'];
			$test_system_ids[$temp['communication_name']][] = $testSystemUsdaID;

			$ReportData[$temp['communication_name']] = $temp;
			$ReportClassificationData[$temp['classification']][$temp['communication_name']] = $temp;
			if ($temp['rel_comm_id'] != "") {
				$RelatedCommData[$temp['rel_comm_id']][$temp['communication_name']] = $temp;
			}
			$work_product['work_product_name'] = $temp['work_product_name'];
		}
		foreach ($RelatedCommData as $keyrel => $valrel) {
			foreach ($valrel as $keyrel1 => $valrel1) {
				$RelatedCommData[$keyrel][$keyrel1]['test_system'] = implode(', ', $test_system_ids[$keyrel1]);
			}
		}
		foreach ($ReportClassificationData as $key => $val) {
			foreach ($val as $key1 => $val1) {
				$yes = "";
				$hiderow = "";
				/* To mark the related record and info to main array based on the all the related records */
				foreach ($RelatedCommData as $relkey => $relval) {
					foreach ($relval as $relkey1 => $relateddataval) {
						$ReportClassificationData[$key][$key1]['is_related'] = $yes;
						$ReportClassificationData[$key][$key1]['is_hide'] = $hiderow;
						if (($val1['classification'] == $relateddataval['classification']) && ($val1['communication_id'] == $relateddataval['rel_comm_id'])) {
							$yes = "yes";
							$ReportClassificationData[$key][$key1]['is_related'] = $yes;
						}
						/*hide the main record row which are having the related comm id value */
						if (in_array($val1['rel_comm_id'], $commListArr) && ($val1['classification'] == $relateddataval['classification']) && ($val1['rel_comm_id'] == $relateddataval['rel_comm_id'])) {
							$hiderow = 'yes';
							$ReportClassificationData[$key][$key1]['is_hide'] = $hiderow;
						}
					}
				}
				$ReportClassificationData[$key][$key1]['test_system'] = implode(', ', $test_system_ids[$key1]);
			}
		}
		$returnData['work_product'] = $work_product;
		//$returnData['ReportData'] = $ReportData;
		$returnData['ReportData'] = $ReportClassificationData;
		$returnData['Related_Comm_Data'] = $RelatedCommData;
		$returnData['ReportClassificationDataCount'] = $ReportClassificationDataCount;

		return $returnData;
	} else {
		return '';
	}
}
function AllDeviationsforStudy($work_product_id)
{
	global $db, $app_list_strings;
	if ($work_product_id) {
		$ReportData = array();
		$ReportSubtypeData = array();
		$RelatedCommData = array();
		$test_system_ids = array();
		$usda_ids = array();
		$work_product = array();
		// To get the count order by subtype c
		$sqlSubTypeCount = "SELECT l1_cstm.subtype_c l1_cstm_subtype_c,IFNULL(m03_work_product.name,'') m03_work_product_name
,count(*) count,l1.name,ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id
FROM m03_work_product
LEFT JOIN  m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id=l1_1.m06_error_m03_work_product_1m03_work_product_idb AND l1_1.deleted=0
LEFT JOIN  m06_error l1 ON l1.id=l1_1.m06_error_m03_work_product_1m06_error_ida AND l1.deleted=0
LEFT JOIN  m06_error_anml_animals_1_c l2_1 ON l1.id=l2_1.m06_error_anml_animals_1m06_error_ida AND l2_1.deleted=0
LEFT JOIN  anml_animals l2 ON l2.id=l2_1.m06_error_anml_animals_1anml_animals_idb AND l2.deleted=0
LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb AND ReletedComm.deleted=0
WHERE (((((IFNULL(l1_cstm.error_classification_c,'') IN ('Error','Impactful Deviation','Non Impactful Deviation')
) AND (m03_work_product.id = '$work_product_id'
))) AND (((IFNULL(l1_cstm.error_category_c,'') <> 'Internal Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Internal Feedback' IS NOT NULL)) AND (IFNULL(l1_cstm.error_type_c,'') <> 'Facility' OR (IFNULL(l1_cstm.error_type_c,'') IS NULL AND 'Facility' IS NOT NULL)) AND (IFNULL(l1_cstm.error_category_c,'') <> 'Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Feedback' IS NOT NULL)))))) 
AND  m03_work_product.deleted=0 
GROUP BY l1.name,l1_cstm.subtype_c
,m03_work_product.name
ORDER BY l1_cstm_subtype_c ASC,m03_work_product_name ASC";

		$resultSubtypeCount = $db->query($sqlSubTypeCount);
		$hash = array();
		$ReportSubtypeDataCount = array();
		while ($rowSubtype = $db->fetchByassoc($resultSubtypeCount)) {
			$hash_key = str_replace('^', '', $rowSubtype['l1_cstm_subtype_c']);
			$hashArr = explode(",", $hash_key);
			/*697 Bug : to fix the issue of multiple subtype record missing in report */
			$subtypeValueArr = array();
			$subTypeArr = explode(",", str_replace('^', '', $rowSubtype['l1_cstm_subtype_c']));
			foreach ($subTypeArr as $subtype1) {
				$subtypeValueArr[] = $app_list_strings['error_type_list'][$subtype1];
				$subtypeValue = implode(",", $subtypeValueArr);
			}
			foreach ($hashArr as $hashArrval) {
				$hash_key = $app_list_strings['error_type_list'][$hashArrval];
				if (!array_key_exists($subtypeValue, $hash)) {
					$hash[$subtypeValue] = sizeof($ReportSubtypeDataCount);
					//$subtypeValue = $app_list_strings['error_type_list'][str_replace('^','',$rowSubtype['l1_cstm_subtype_c'])];			
					array_push($ReportSubtypeDataCount, array(
						'm03_work_product_name' => $rowSubtype['m03_work_product_name'],
						'l1_cstm_subtype_c' => $subtypeValue,
						'name' => $rowSubtype['name'],
						'rel_comm_id' => $rowSubtype['rel_comm_id'],
						'count' => 0,
					));
				}
			}
			$ReportSubtypeDataCount[$hash[$subtypeValue]]['count'] += 1;
		}
		/* #697 : Remove the duplicates array from subtype count array */
		$unique = $ReportSubtypeDataCount;
		$duplicated = array();

		foreach ($unique as $k => $v) {
			if (($kt = array_search($v, $unique)) !== false and $k != $kt) {
				unset($unique[$kt]);
				$duplicated[] = $v;
			}
		}
		$ReportSubtypeDataCount = $unique;
		/*Query to get Report Data*/
		$sql =
			"SELECT
  IFNULL(l1.id, '') l1_id,
  IFNULL(l1. NAME, '') l1_name,
  ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id,
  IFNULL(m03_work_product.id, '') m03_work_product_id,
  l2_cstm.animal_id_c l2_cstm_animal_id_c,
  l2_cstm.usda_id_c l2_cstm_usda_id_c,
  CONCAT (l2_cstm.animal_id_c,'-',l2_cstm.usda_id_c) AS TestUsda,
  l1_cstm.date_error_occurred_c L1_CSTM_DATE_ERROR_OCC6BD1CE,
  l1_cstm.expected_event_c l1_cstm_expected_event_c,
  l1_cstm.actual_event_c l1_cstm_actual_event_c,
  l1_cstm.resolution_c l1_cstm_resolution_c,
  l1_cstm.impactful_dd_c impactful_dd_c,
  IFNULL(
	  l1_cstm.corrective_action_1_c,
	  ''
  ) L1_CSTM_CORRECTIVE_ACT674D79,
  l1_cstm.rationale_for_no_ca_c L1_CSTM_RATIONALE_FOR_F48A84,
  l1_cstm.subtype_c l1_cstm_subtype_c,
  IFNULL(m03_work_product. NAME, '') m03_work_product_name
FROM
	m03_work_product
LEFT JOIN m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id = l1_1.m06_error_m03_work_product_1m03_work_product_idb
AND l1_1.deleted = 0
LEFT JOIN m06_error l1 ON l1.id = l1_1.m06_error_m03_work_product_1m06_error_ida
AND l1.deleted = 0
LEFT JOIN m06_error_anml_animals_1_c l2_1 ON l1.id = l2_1.m06_error_anml_animals_1m06_error_ida
AND l2_1.deleted = 0
LEFT JOIN anml_animals l2 ON l2.id = l2_1.m06_error_anml_animals_1anml_animals_idb
AND l2.deleted = 0
LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb
AND ReletedComm.deleted=0
WHERE (((((IFNULL(l1_cstm.error_classification_c,'') IN ('Error','Impactful Deviation','Non Impactful Deviation')
) AND (m03_work_product.id = '$work_product_id'
))) AND (((IFNULL(l1_cstm.error_category_c,'') <> 'Internal Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Internal Feedback' IS NOT NULL)) AND (IFNULL(l1_cstm.error_type_c,'') <> 'Facility' OR (IFNULL(l1_cstm.error_type_c,'') IS NULL AND 'Facility' IS NOT NULL)) AND (IFNULL(l1_cstm.error_category_c,'') <> 'Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Feedback' IS NOT NULL)))))) 
AND  m03_work_product.deleted=0 
ORDER BY
L1_CSTM_DATE_ERROR_OCC6BD1CE ASC,
l2_cstm_animal_id_c ASC,
l1_cstm_subtype_c ASC,
m03_work_product_name ASC";
		$result = $db->query($sql);
		$work_product['work_product_id'] = $work_product_id;
		$returnData = array();
		$commListArr = array();
		while ($row = $db->fetchByassoc($result)) {
			$temp = array();
			$commListArr[]  = $row['l1_id'];
			$temp['communication_id'] = $row['l1_id'];
			$temp['communication_name'] = $row['l1_name'];
			$temp['work_product_id'] = $row['m03_work_product_id'];
			$temp['work_product_name'] = $row['m03_work_product_name'];
			$temp['test_system'] = $row['l2_cstm_animal_id_c'];
			$temp['usda_id'] = $row['l2_cstm_usda_id_c'];
			$temp['rel_comm_id'] = $row['rel_comm_id'];
			$temp['impactful_dd_c'] = $row['impactful_dd_c'];
			if ($row['L1_CSTM_DATE_ERROR_OCC6BD1CE'] != "") {
				$temp['date_occurred'] = date("m/d/Y", strtotime($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']));
			} else {
				$temp['date_occurred'] = "";
			}

			if ($row['l1_cstm_reinspection_date_c'] != "") {
				$temp['actual_assessment_date'] = date("m/d/Y", strtotime($row['l1_cstm_reinspection_date_c']));
			} else {
				$temp['actual_assessment_date'] = "";
			}
			$temp['expected_event'] = $row['l1_cstm_expected_event_c'];
			$temp['actual_event'] = $row['l1_cstm_actual_event_c'];
			$temp['sd_assessment'] = $row['l1_cstm_resolution_c'];
			$subtypeValueArr = array();
			$subTypeArr = explode(",", str_replace('^', '', $row['l1_cstm_subtype_c']));

			foreach ($subTypeArr as $subtype1) {
				$subtypeValueArr[] = $app_list_strings['error_type_list'][$subtype1];
			}
			$subtypeValue = implode(",", $subtypeValueArr);
			$temp['subtype'] = $subtypeValue; //$app_list_strings['error_type_list'][$temp['subtype']];   
			$temp['corrective_action'] = $row['L1_CSTM_CORRECTIVE_ACT674D79'];
			$temp['corrective_action_details'] = $row['L1_CSTM_RATIONALE_FOR_F48A84'];
			$testSystemUsdaID = $temp['test_system'];
			if ($temp['usda_id'] != "")
				$testSystemUsdaID .= "-" . $temp['usda_id'];
			$test_system_ids[$temp['communication_name']][] = $testSystemUsdaID;
			//$ReportData[$temp['communication_name']] = $temp;
			$ReportSubtypeData[$temp['subtype']][$temp['communication_name']] = $temp;
			if ($temp['rel_comm_id'] != "") {
				$RelatedCommData[$temp['rel_comm_id']][$temp['communication_name']] = $temp;
			}
			//$test_system_ids[$temp['communication_name']][] = $temp['test_system'];
			// $usda_ids[$temp['communication_name']][] = $temp['usda_id'];
			$work_product['work_product_name'] = $temp['work_product_name'];
		}
		foreach ($RelatedCommData as $keyrel => $valrel) {
			foreach ($valrel as $keyrel1 => $valrel1) {
				$RelatedCommData[$keyrel][$keyrel1]['test_system'] = implode(', ', $test_system_ids[$keyrel1]);
			}
		}
		foreach ($ReportSubtypeData as $key => $val) {
			foreach ($val as $key1 => $val1) {
				$yes = "";
				$hiderow = "";
				/* To mark the related record and info to main array based on the all the related records */
				foreach ($RelatedCommData as $relkey => $relval) {
					foreach ($relval as $relkey1 => $relateddataval) {
						/**fix bug 1357 : 11 june 2021 */
						$ReportSubtypeData[$key][$key1]['is_hide'] = $hiderow;
						$ReportSubtypeData[$key][$key1]['is_related'] = $yes;
						if (($val1['subtype'] == $relateddataval['subtype']) && ($val1['communication_id'] == $relateddataval['rel_comm_id'])) {
							$yes = "yes";
							$ReportSubtypeData[$key][$key1]['is_related'] = $yes;
						}
						/*hide the main record row which are having the related comm id value */
						if (in_array($val1['rel_comm_id'], $commListArr) && ($val1['subtype'] == $relateddataval['subtype']) && ($val1['rel_comm_id'] == $relateddataval['rel_comm_id'])) {
							$hiderow = 'yes';
							$ReportSubtypeData[$key][$key1]['is_hide'] = $hiderow;
						}
					}
				}
				$ReportSubtypeData[$key][$key1]['test_system'] = implode(', ', $test_system_ids[$key1]);
				$ReportSubtypeData[$key][$key1]['usda_id'] = implode(', ', $usda_ids[$key1]);
			}
		}
		$returnData['work_product'] = $work_product;
		//$returnData['ReportData'] = $ReportData;
		$returnData['ReportData'] = $ReportSubtypeData;
		$returnData['Related_Comm_Data'] = $RelatedCommData;
		$returnData['ReportSubtypeDataCount'] = $ReportSubtypeDataCount;
		return $returnData;
	} else {
		return '';
	}
}
function AllNotificationforStudy($work_product_id)
{
	global $db, $app_list_strings;
	if ($work_product_id) {
		$ReportData = array();
		$ReportSubtypeData = array();
		$RelatedCommData = array();
		$test_system_ids = array();
		$usda_ids = array();
		$work_product = array();
		// To get the count order by subtype c
		$sqlSubTypeCount = "SELECT l1_cstm.subtype_c l1_cstm_subtype_c,IFNULL(m03_work_product.name,'') m03_work_product_name
	,count(*) count,l1.name,ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id
	FROM m03_work_product
	LEFT JOIN  m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id=l1_1.m06_error_m03_work_product_1m03_work_product_idb AND l1_1.deleted=0
	
	LEFT JOIN  m06_error l1 ON l1.id=l1_1.m06_error_m03_work_product_1m06_error_ida AND l1.deleted=0
	LEFT JOIN  m06_error_anml_animals_1_c l2_1 ON l1.id=l2_1.m06_error_anml_animals_1m06_error_ida AND l2_1.deleted=0
	
	LEFT JOIN  anml_animals l2 ON l2.id=l2_1.m06_error_anml_animals_1anml_animals_idb AND l2.deleted=0
	LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
	LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb AND ReletedComm.deleted=0
	
	WHERE (((((IFNULL(l1_cstm.error_classification_c,'') IN ('Duplicate','Notification')
	) AND (m03_work_product.id = '$work_product_id'
	))) AND (((IFNULL(l1_cstm.error_category_c,'') <> 'Internal Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Internal Feedback' IS NOT NULL)) AND (IFNULL(l1_cstm.error_type_c,'') <> 'Facility' OR (IFNULL(l1_cstm.error_type_c,'') IS NULL AND 'Facility' IS NOT NULL)) AND (IFNULL(l1_cstm.error_category_c,'') <> 'Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Feedback' IS NOT NULL)))))) 
	AND  m03_work_product.deleted=0 
	GROUP BY l1.name,l1_cstm.subtype_c 
	,m03_work_product.name
	ORDER BY l1_cstm_subtype_c ASC,m03_work_product_name ASC";

		$resultSubtypeCount = $db->query($sqlSubTypeCount);
		$hash = array();
		$ReportSubtypeDataCount = array();
		while ($rowSubtype = $db->fetchByassoc($resultSubtypeCount)) {

			$hash_key = str_replace('^', '', $rowSubtype['l1_cstm_subtype_c']);
			$hashArr = explode(",", $hash_key);
			/*697 Bug : to fix the issue of multiple subtype record missing in report */
			$subtypeValueArr = array();
			$subTypeArr = explode(",", str_replace('^', '', $rowSubtype['l1_cstm_subtype_c']));

			foreach ($subTypeArr as $subtype1) {
				$subtypeValueArr[] = $app_list_strings['error_type_list'][$subtype1];
				$subtypeValue = implode(",", $subtypeValueArr);
			}

			foreach ($hashArr as $hashArrval) {
				$hash_key = $app_list_strings['error_type_list'][$hashArrval];
				if (!array_key_exists($subtypeValue, $hash)) {
					$hash[$subtypeValue] = sizeof($ReportSubtypeDataCount);
					//$subtypeValue = $app_list_strings['error_type_list'][str_replace('^','',$rowSubtype['l1_cstm_subtype_c'])];
					array_push($ReportSubtypeDataCount, array(
						'm03_work_product_name' => $rowSubtype['m03_work_product_name'],
						'l1_cstm_subtype_c' => $subtypeValue,
						'name' => $rowSubtype['name'],
						'rel_comm_id' => $rowSubtype['rel_comm_id'],
						'count' => 0,
					));
				}
			}
			$ReportSubtypeDataCount[$hash[$subtypeValue]]['count'] += 1;
		}
		/* #697 : Remove the duplicates array from subtype count array */
		$unique = $ReportSubtypeDataCount;
		$duplicated = array();

		foreach ($unique as $k => $v) {
			if (($kt = array_search($v, $unique)) !== false and $k != $kt) {
				unset($unique[$kt]);
				$duplicated[] = $v;
			}
		}
		$ReportSubtypeDataCount = $unique;
		/*Query to get Report Data*/

		$sql =
			"SELECT
  IFNULL(l1.id, '')l1_id,
  IFNULL(l1. NAME, '')l1_name,
  ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id,
  IFNULL(m03_work_product.id, '')m03_work_product_id,
  l2_cstm.animal_id_c l2_cstm_animal_id_c,
  l2_cstm.usda_id_c l2_cstm_usda_id_c,
  
  l1_cstm.date_error_occurred_c L1_CSTM_DATE_ERROR_OCC6BD1CE,
  l1_cstm.reinspection_date_c l1_cstm_reinspection_date_c,
  l1_cstm.actual_event_c l1_cstm_actual_event_c,
  l1_cstm.resolution_c l1_cstm_resolution_c,
  l1_cstm.subtype_c l1_cstm_subtype_c,
  IFNULL(m03_work_product. NAME, '')m03_work_product_name
FROM
	m03_work_product
LEFT JOIN m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id = l1_1.m06_error_m03_work_product_1m03_work_product_idb
AND l1_1.deleted = 0
LEFT JOIN m06_error l1 ON l1.id = l1_1.m06_error_m03_work_product_1m06_error_ida
AND l1.deleted = 0
LEFT JOIN m06_error_anml_animals_1_c l2_1 ON l1.id = l2_1.m06_error_anml_animals_1m06_error_ida
AND l2_1.deleted = 0
LEFT JOIN anml_animals l2 ON l2.id = l2_1.m06_error_anml_animals_1anml_animals_idb
AND l2.deleted = 0
LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb
 AND ReletedComm.deleted=0

WHERE (((((IFNULL(l1_cstm.error_classification_c,'') IN ('Duplicate','Notification')
) AND (m03_work_product.id = '$work_product_id'
))) AND (((IFNULL(l1_cstm.error_category_c,'') <> 'Internal Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Internal Feedback' IS NOT NULL)) AND (IFNULL(l1_cstm.error_type_c,'') <> 'Facility' OR (IFNULL(l1_cstm.error_type_c,'') IS NULL AND 'Facility' IS NOT NULL)) AND (IFNULL(l1_cstm.error_category_c,'') <> 'Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Feedback' IS NOT NULL)))))) 
AND  m03_work_product.deleted=0 
ORDER BY
L1_CSTM_DATE_ERROR_OCC6BD1CE ASC,
l2_cstm_animal_id_c ASC,
l1_cstm_subtype_c ASC,
m03_work_product_name ASC";
		$result = $db->query($sql);
		$work_product['work_product_id'] = $work_product_id;
		$returnData = array();
		$commListArr = array();
		while ($row = $db->fetchByassoc($result)) {
			$temp = array();
			$commListArr[]  = $row['l1_id'];
			$temp['communication_id'] = $row['l1_id'];
			$temp['communication_name'] = $row['l1_name'];
			$temp['work_product_id'] = $row['m03_work_product_id'];
			$temp['work_product_name'] = $row['m03_work_product_name'];
			$temp['test_system'] = $row['l2_cstm_animal_id_c'];
			$temp['usda_id'] = $row['l2_cstm_usda_id_c'];
			$temp['date_occurred'] = $row['L1_CSTM_DATE_ERROR_OCC6BD1CE'];
			$temp['actual_assessment_date'] = $row['l1_cstm_reinspection_date_c'];
			$temp['actual_event'] = str_replace('"', "'", $row['l1_cstm_actual_event_c']);
			$temp['sd_assessment'] = str_replace('"', "'", $row['l1_cstm_resolution_c']);
			$temp['rel_comm_id'] = $row['rel_comm_id'];
			$subtypeValueArr = array();
			$subTypeArr = explode(",", str_replace('^', '', $row['l1_cstm_subtype_c']));
			foreach ($subTypeArr as $subtype1) {
				$subtypeValueArr[] = $app_list_strings['error_type_list'][$subtype1];
			}
			$subtypeValue = implode(",", $subtypeValueArr);
			$temp['subtype'] = $subtypeValue; //str_replace('^','',$row['l1_cstm_subtype_c']);
			//$temp['subtype'] = $app_list_strings['error_type_list'][$temp['subtype']];
			$temp['l1_cstm_subtype_c'] = $subtypeValue; // str_replace('^','',$row['l1_cstm_subtype_c']);
			$ReportData[$temp['communication_name']] = $temp;
			$ReportSubtypeData[$temp['subtype']][$temp['communication_name']] = $temp;
			if ($temp['rel_comm_id'] != "") {
				$RelatedCommData[$temp['rel_comm_id']][$temp['communication_name']] = $temp;
			}
			$testSystemUsdaID = $temp['test_system'];
			if ($temp['usda_id'] != "")
				$testSystemUsdaID .= "-" . $temp['usda_id'];
			$test_system_ids[$temp['communication_name']][] = $testSystemUsdaID;
			//$test_system_ids[$temp['communication_name']][] = $temp['test_system'];
			$work_product['work_product_name'] = $temp['work_product_name'];
			// $usda_ids[$temp['communication_name']][] = $temp['usda_id'];

		}
		foreach ($RelatedCommData as $rel_key => $rel_val) {
			foreach ($rel_val as $rel_key1 => $rel_val1) {
				$RelatedCommData[$rel_key][$rel_key1]['test_system'] = implode(', ', $test_system_ids[$rel_key1]);
			}
		}
		foreach ($ReportSubtypeData as $key => $val) {
			foreach ($val as $key1 => $val1) {
				$yes = "";
				$hiderow = "";
				/* To mark the related record and info to main array based on the all the related records */
				foreach ($RelatedCommData as $relkey => $relval) {
					foreach ($relval as $relkey1 => $relateddataval) {
						/**fix bug 1357 : 11 june 2021 */
						$ReportSubtypeData[$key][$key1]['is_hide'] = $hiderow;
						$ReportSubtypeData[$key][$key1]['is_related'] = $yes;
						if (($val1['subtype'] == $relateddataval['subtype']) && ($val1['communication_id'] == $relateddataval['rel_comm_id'])) {
							$yes = "yes";
							$ReportSubtypeData[$key][$key1]['is_related'] = $yes;
						}
						/*hide the main record row which are having the related comm id value */
						if (in_array($val1['rel_comm_id'], $commListArr) && ($val1['subtype'] == $relateddataval['subtype']) && ($val1['rel_comm_id'] == $relateddataval['rel_comm_id'])) {
							$hiderow = 'yes';
							$ReportSubtypeData[$key][$key1]['is_hide'] = $hiderow;
						}
					}
				}
				$ReportSubtypeData[$key][$key1]['test_system'] = implode(', ', $test_system_ids[$key1]);
			}
		}
		$returnData['work_product'] = $work_product;
		//$returnData['ReportData'] = $ReportData;
		$returnData['ReportData'] = $ReportSubtypeData;
		$returnData['ReportSubtypeDataCount'] = $ReportSubtypeDataCount;
		$returnData['Related_Comm_Data'] = $RelatedCommData;
		return $returnData;
	} else {
		return '';
	}
}

/* #633 : 13 July 2021 */
function CustomTSWeight($filter_start_date, $filter_end_date)
{
	global $db;
	/*$sql = 'SELECT id,breed_c,name,date_last_weighed_c,date_of_birth_c,s_species_id_c FROM anml_animals
	left join anml_animals_cstm on anml_animals_cstm.id_c =anml_animals.id
    where s_species_id_c!="" and deleted=0 order by date_entered desc limit 150';*/
	$sql = 'SELECT id,breed_c,name,date_last_weighed_c,date_of_birth_c,s_species_id_c,sex_c FROM anml_animals
    left join anml_animals_cstm on anml_animals_cstm.id_c =anml_animals.id
    where s_species_id_c!="" and deleted=0 and UNIX_TIMESTAMP(date_entered) between UNIX_TIMESTAMP("' . $filter_start_date . '") and UNIX_TIMESTAMP("' . $filter_end_date . '")+86000  order by date_entered desc';

	$ReportData = array();
	$result = $db->query($sql);
	$returnData = array();
	while ($row = $db->fetchByassoc($result)) {
		$arrDLW = array();  // Array of Date Last Weight [Date Created] => Date Last Weight Value
		$arrW = array();   // Array of Weight            [Date Created] => Weight
		$id = $row['id']; // Test System ID

		$sqlDLW = 'SELECT * FROM anml_animals_audit where parent_id="' . $id . '" and field_name="date_last_weighed_c" order by date_created  asc';

		$resultDLW = $db->query($sqlDLW);

		while ($rowDLW = $db->fetchByassoc($resultDLW)) {
			$arrDLW[$rowDLW['date_created']] = $rowDLW['after_value_string'];
		}

		$sqlW = 'SELECT * FROM anml_animals_audit where parent_id="' . $id . '" and field_name="bodyweight_c" order by date_created  asc';
		$resultW = $db->query($sqlW);

		while ($rowW = $db->fetchByassoc($resultW)) {
			$arrW[$rowW['date_created']] = $rowW['after_value_string'];
		}

		// Creating Date Created field array. [0] => Date Created
		$keysDLW = array_keys($arrDLW);
		$keysW = array_keys($arrW);


		$temp = array();
		foreach (array_keys($keysW) as $kW) {
			//Current Date Created of weight
			$this_value = $keysW[$kW];
			//Next Date Created
			$nextval = $keysW[$kW + 1];

			foreach (array_keys($keysDLW) as $kDLW) {
				//Current Date Created of date last weight
				$this_value1 = $keysDLW[$kDLW];
				//Next Date Created of date last weight
				$nextval1 = $keysDLW[$kDLW + 1];

				if (strtotime($keysDLW[$kDLW]) >= strtotime($this_value) && (strtotime($nextval) > strtotime($keysDLW[$kDLW]) || $nextval == null)) {
					$speciesId = $row['s_species_id_c'];
					$temp['s_species_id_c'] = $speciesId;

					//Getting name of Species
					$sqlSpecies = 'SELECT name from s_species where id="' . $speciesId . '" and deleted=0';
					$resultSpecies = $db->query($sqlSpecies);
					$rowSpecies = $db->fetchByassoc($resultSpecies);

					$temp['species_name'] = $rowSpecies['name'];
					$temp['s_species_id_c'] = $speciesId;
					$temp['breed_c'] = $row['breed_c'];
					$temp['parent_id'] = $row['id'];
					$temp['name'] = $row['name'];
					$temp['sex_c'] = $row['sex_c'];
					$temp['date_last_weighed_c'] = date("m-d-Y", strtotime($arrDLW[$keysDLW[$kDLW]]));
					$temp['after_value_string'] = $arrW[$keysW[$kW]];

					//Getting Date Of Birth
					if ($row['date_of_birth_c'] != "" || $row['date_of_birth_c'] != null) {
						$sqlDOB = 'SELECT after_value_string FROM anml_animals_audit where parent_id="' . $id . '" and field_name="date_of_birth_c" and UNIX_TIMESTAMP(date_created) <= UNIX_TIMESTAMP("' . $this_value1 . '") order by date_created desc limit 1';
						$resultDOB = $db->query($sqlDOB);
						$rowDOB = $db->fetchByassoc($resultDOB);
						if ($rowDOB['after_value_string']) {
							$temp['date_of_birth_c'] = date("m-d-Y", strtotime($rowDOB['after_value_string']));
						} else
							$temp['date_of_birth_c'] = "";
					} else
						$temp['date_of_birth_c'] = "";
					array_push($ReportData, $temp);
				}
			}
		}
	}

	$returnData['ReportData'] = $ReportData;
	$returnData['filter_start_date'] = $filter_start_date;
	$returnData['filter_end_date'] = $filter_end_date;
	return $returnData;
}
