<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/Sugarpdf/Sugarpdf.php');

class ReportsSugarpdfallnotificationforstudyview extends ReportsSugarpdfReports
{

    function process()
    {
        $this->preDisplay();
        $this->display();
        $this->buildFileName();
    }

    /**
     * Custom header
     */

    public function Header()
    {
        global $db;
        $ormargins              = $this->getOriginalMargins();
        $headerfont             = $this->getHeaderFont();
        $headerdata             = $this->getHeaderData();
        $headerdata['logo']     = "sugarpdf_small_header_logo.png";
        $headerdata['title']    = "All Notification for a Study";
        $sqlForGetWPName        = 'SELECT name FROM m03_work_product WHERE id = "' . $_REQUEST['wpid'] . '"';  // query for get name of the Work Product
        $resultName             = $db->query($sqlForGetWPName);
        $rowName                = $db->fetchByassoc($resultName);
        $headerdata['string']   = 'Work Product: ' . $rowName['name'];

        //K_PATH_IMAGES
        $headerdata['logo_width'] = 50;
        if (($headerdata['logo']) and ($headerdata['logo'] != K_PATH_CUSTOM_IMAGES)) {
            $headerdata['logo'] = "sugarpdf_small_header_logo.png";
            $this->Image(K_PATH_CUSTOM_IMAGES . $headerdata['logo'], $this->GetX(), $this->getHeaderMargin(), $headerdata['logo_width']);
            $imgy = $this->getImageRBY();
        } else {
            $imgy = $this->GetY();
        }

        $cell_height = round(($this->getCellHeightRatio() * $headerfont[2]) / $this->getScaleFactor(), 2);
        // set starting margin for text data cell
        if ($this->getRTL()) {
            $header_x = $ormargins['right'] + ($headerdata['logo_width'] * 1.1);
        } else {
            $ormargins['left']    = 150;
            $header_x             = $ormargins['left'] + ($headerdata['logo_width'] * 1.1);
        }

        $this->SetTextColor(0, 0, 0);
        // header title
        $this->SetFont($headerfont[0], 'B', $headerfont[2] + 1);
        $this->SetX($header_x);
        $this->Cell(0, $cell_height, $headerdata['title'], 0, 1, '', 0, '', 0);
        // header string
        $this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);
        $this->SetX($header_x);
        $this->MultiCell(0, $cell_height, $headerdata['string'], 0, '', 0, 1, '', '', true, 0, false);

        $ormargins['left'] = 15;
        // print an ending header line
        $this->SetLineStyle(array('width' => 0.85 / $this->getScaleFactor(), 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->SetY((2.835 / $this->getScaleFactor()) + max($imgy, $this->GetY()));
        if ($this->getRTL()) {
            $this->SetX($ormargins['right']);
        } else {
            $this->SetX($ormargins['left']);
        }
        $this->Cell(0, 0, '', 'T', 0, 'C');
        $this->Ln();
    }

    function display()
    {
        // $GLOBALS['log']->fatal('workingffsdfs');
        global $db, $report_modules, $app_list_strings, $mod_strings, $locale, $current_user;
        $this->AddPage();
        $work_product_id            = $_REQUEST['wpid'];
        $wp_data['current_user_id'] = $current_user->id;
        $ReportData                 = array();
        $ReportData2                = array();
        $ReportSubtypeData          = array();
        $RelatedCommData            = array();
        $test_system_ids            = array();
        $usda_ids                   = array();

        $sqlSubTypeCount = "SELECT l1_cstm.subtype_c l1_cstm_subtype_c,IFNULL(m03_work_product.name,'') m03_work_product_name,
                            count(*) count,l1.name,
                            ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id
                            FROM m03_work_product
                            LEFT JOIN  m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id=l1_1.m06_error_m03_work_product_1m03_work_product_idb AND l1_1.deleted=0
                            LEFT JOIN  m06_error l1 ON l1.id=l1_1.m06_error_m03_work_product_1m06_error_ida AND l1.deleted=0
                            LEFT JOIN  m06_error_anml_animals_1_c l2_1 ON l1.id=l2_1.m06_error_anml_animals_1m06_error_ida AND l2_1.deleted=0
                            LEFT JOIN  anml_animals l2 ON l2.id=l2_1.m06_error_anml_animals_1anml_animals_idb AND l2.deleted=0
                            LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
                            LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
                            LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb AND ReletedComm.deleted=0
                            WHERE (((((IFNULL(l1_cstm.error_classification_c,'') IN ('Duplicate','Notification')
                            ) AND (m03_work_product.id = '" . $work_product_id . "'
                            ))) AND (((IFNULL(l1_cstm.error_category_c,'') <> 'Internal Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Internal Feedback' IS NOT NULL)) AND (IFNULL(l1_cstm.error_type_c,'') <> 'Facility' OR (IFNULL(l1_cstm.error_type_c,'') IS NULL AND 'Facility' IS NOT NULL)) AND (IFNULL(l1_cstm.error_category_c,'') <> 'Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Feedback' IS NOT NULL)))))) 
                            AND  m03_work_product.deleted=0 
                            GROUP BY l1.name,l1_cstm.subtype_c 
                            ,m03_work_product.name
                            ORDER BY l1_cstm_subtype_c ASC,m03_work_product_name ASC";
        $resultSubtypeCount     = $db->query($sqlSubTypeCount);
        $hash                   = array();
        $ReportSubtypeDataCount = array();

        while ($rowSubtype = $db->fetchByassoc($resultSubtypeCount)) {
            $hash_key        = str_replace('^', '', $rowSubtype['l1_cstm_subtype_c']);
            $hashArr         = explode(",", $hash_key);
            /*697 Bug : to fix the issue of multiple subtype record missing in report */
            $subtypeValueArr = array();
            $subTypeArr      = explode(",", $hash_key);

            foreach ($subTypeArr as $subtype1) {
                $subtypeValueArr[] = $app_list_strings['error_type_list'][$subtype1];
                $subtypeValue      = implode(",", $subtypeValueArr);
            }

            foreach ($hashArr as $hashArrval) {
                $hash_key = $app_list_strings['error_type_list'][$hashArrval];
                if (!array_key_exists($subtypeValue, $hash)) {
                    $hash[$subtypeValue] = sizeof($ReportSubtypeDataCount);
                    //$subtypeValue = $app_list_strings['error_type_list'][str_replace('^','',$rowSubtype['l1_cstm_subtype_c'])];
                    array_push($ReportSubtypeDataCount, array(
                        'm03_work_product_name' => $rowSubtype['m03_work_product_name'],
                        'l1_cstm_subtype_c'     => $subtypeValue,
                        'name'                  => $rowSubtype['name'],
                        'rel_comm_id'           => $rowSubtype['rel_comm_id'],
                        'count'                 => 0,
                    ));
                }
            }
            $ReportSubtypeDataCount[$hash[$subtypeValue]]['count'] += 1;
        }

        /* #697 : Remove the duplicates array from subtype count array */
        $unique = $ReportSubtypeDataCount;
        $duplicated = array();

        foreach ($unique as $k => $v) {
            if (($kt = array_search($v, $unique)) !== false and $k != $kt) {
                unset($unique[$kt]);
                $duplicated[] = $v;
            }
        }
        $ReportSubtypeDataCount = $unique;

        // $GLOBALS['log']->fatal("in subtypeData count===>". print_r($ReportSubtypeDataCount,1));
        /*Query to get Report Data*/

        $sql = "SELECT
                IFNULL(l1.id, '')l1_id,
                IFNULL(l1. NAME, '')l1_name,
                ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id,
                IFNULL(m03_work_product.id, '')m03_work_product_id,
                l2_cstm.animal_id_c l2_cstm_animal_id_c,
                l2_cstm.usda_id_c l2_cstm_usda_id_c,
                
                l1_cstm.date_error_occurred_c L1_CSTM_DATE_ERROR_OCC6BD1CE,
                l1_cstm.reinspection_date_c l1_cstm_reinspection_date_c,
                l1_cstm.actual_event_c l1_cstm_actual_event_c,
                l1_cstm.resolution_c l1_cstm_resolution_c,
                l1_cstm.subtype_c l1_cstm_subtype_c,
                IFNULL(m03_work_product. NAME, '')m03_work_product_name
            FROM
                m03_work_product
            LEFT JOIN m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id = l1_1.m06_error_m03_work_product_1m03_work_product_idb
            AND l1_1.deleted = 0
            LEFT JOIN m06_error l1 ON l1.id = l1_1.m06_error_m03_work_product_1m06_error_ida
            AND l1.deleted = 0
            LEFT JOIN m06_error_anml_animals_1_c l2_1 ON l1.id = l2_1.m06_error_anml_animals_1m06_error_ida
            AND l2_1.deleted = 0
            LEFT JOIN anml_animals l2 ON l2.id = l2_1.m06_error_anml_animals_1anml_animals_idb
            AND l2.deleted = 0
            LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
            LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
            LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb
            AND ReletedComm.deleted=0
            WHERE (((((IFNULL(l1_cstm.error_classification_c,'') IN ('Duplicate','Notification')
            ) AND (m03_work_product.id = '" . $work_product_id . "'
            ))) AND (((IFNULL(l1_cstm.error_category_c,'') <> 'Internal Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Internal Feedback' IS NOT NULL)) AND (IFNULL(l1_cstm.error_type_c,'') <> 'Facility' OR (IFNULL(l1_cstm.error_type_c,'') IS NULL AND 'Facility' IS NOT NULL)) AND (IFNULL(l1_cstm.error_category_c,'') <> 'Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Feedback' IS NOT NULL)))))) 
            AND  m03_work_product.deleted=0 
            ORDER BY
            L1_CSTM_DATE_ERROR_OCC6BD1CE ASC,
            l2_cstm_animal_id_c ASC,
                l1_cstm_subtype_c ASC,
                m03_work_product_name ASC";

        $result = $db->query($sql);
        while ($row = $db->fetchByassoc($result)) {
            $temp                       = array();
            $commListArr[]              = $row['l1_id'];
            $temp['communication_id']   = $row['l1_id'];
            $temp['communication_name'] = $row['l1_name'];
            $temp['work_product_id']    = $row['m03_work_product_id'];
            $temp['work_product_name']  = $row['m03_work_product_name'];
            $temp['test_system']        = $row['l2_cstm_animal_id_c'];
            $temp['usda_id']            = $row['l2_cstm_usda_id_c'];
            $temp['rel_comm_id']        = $row['rel_comm_id'];


            //For date in an desired format
            if ($row['L1_CSTM_DATE_ERROR_OCC6BD1CE'] != "") {
                $temp['date_occurred'] = date("m/d/Y", strtotime($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']));
            } else {
                $temp['date_occurred'] = "";
            }
            //For actual Assessment date
            if ($row['l1_cstm_reinspection_date_c'] != "") {
                $temp['actual_assessment_date'] = date("m/d/Y", strtotime($row['l1_cstm_reinspection_date_c']));
            } else {
                $temp['actual_assessment_date'] = "";
            }
            $temp['actual_event'] = $row['l1_cstm_actual_event_c'];
            $temp['sd_assessment'] = $row['l1_cstm_resolution_c'];


            $ReportData[$temp['communication_name']] = $temp;

            $subtypeValueArr = array();
            $subTypeArr = explode(",", str_replace('^', '', $row['l1_cstm_subtype_c']));

            foreach ($subTypeArr as $subtype1) {
                $subtypeValueArr[] = $app_list_strings['error_type_list'][$subtype1];
            }
            $subtypeValue = implode(",", $subtypeValueArr);
            //$temp['subtype'] = str_replace('^','',$row['l1_cstm_subtype_c']);
            $temp['subtype'] = $subtypeValue; //$app_list_strings['error_type_list'][$temp['subtype']];
            $ReportSubtypeData[$temp['subtype']][$temp['communication_name']] = $temp;
            if ($temp['rel_comm_id'] != "") {
                $RelatedCommData[$temp['rel_comm_id']][$temp['communication_name']] = $temp;
            }

            $testSystemUsdaID = $temp['test_system'];
            if ($temp['usda_id'] != "") $testSystemUsdaID .= "-" . $temp['usda_id'];

            $test_system_ids[$temp['communication_name']][] = $testSystemUsdaID;
            //$test_system_ids[$temp['communication_name']][] = $temp['test_system'];
            $usda_ids[$temp['communication_name']][] = $temp['usda_id'];
        }
        foreach ($RelatedCommData as $keyrel => $valrel) {
            foreach ($valrel as $keyrel1 => $valrel1) {
                $RelatedCommData[$keyrel][$keyrel1]['test_system'] = implode(', ', $test_system_ids[$keyrel1]);
            }
        }
        /* To mark the related record and info to main array based on the all the related records */
        foreach ($ReportSubtypeData as $key => $val) {
            foreach ($val as $key1 => $val1) {
                $yes = "";
                $hiderow = "";
                foreach ($RelatedCommData as $relkey => $relval) {
                    foreach ($relval as $relkey1 => $relateddataval) {
                        /**fix bug 1357 : 11 june 2021 */
                        $ReportSubtypeData[$key][$key1]['is_related'] = $yes;
                        $ReportSubtypeData[$key][$key1]['is_hide'] = $hiderow;
                        if (($val1['subtype'] == $relateddataval['subtype']) && ($val1['communication_id'] == $relateddataval['rel_comm_id'])) {
                            $yes = "yes";
                            $ReportSubtypeData[$key][$key1]['is_related'] = $yes;
                        }
                        /*hide the main record row which are having the related comm id value */
                        if (in_array($val1['rel_comm_id'], $commListArr) && ($val1['subtype'] == $relateddataval['subtype']) && ($val1['rel_comm_id'] == $relateddataval['rel_comm_id'])) {
                            $hiderow = 'yes';
                            $ReportSubtypeData[$key][$key1]['is_hide'] = $hiderow;
                        }
                    }
                }
                $ReportSubtypeData[$key][$key1]['test_system'] = implode(', ', $test_system_ids[$key1]);
            }
        }
       
        foreach($ReportSubtypeDataCount as $key2 => $subtype){
            $yes = "";
            $hiderow = "";
            $top_row    = array();
            $itemTop    = array();
    
            if ($key2 !== "") {
                $top_row = array('Subtype =' . $subtype['l1_cstm_subtype_c'] . ", Count=" . $subtype['count']);
            } else {
                $top_row = array("Subtype =None, Count=" . $subtype['count']);
            }
            $labelTop = $top_row[0];
            $labelTop2 = explode(" =", $labelTop);
            
            if ($labelTop2[0] == "Subtype") {
                $this->options['header']['fill']      = "#ffffff";
                $this->options['header']['textColor'] = "#000000";
                $this->options['header']['fontStyle'] = "";
            }
            $itemTop[0][$labelTop] = "";
            $this->writeCellTable($itemTop, $this->options);
           
           
            $rowHeader = array();
           foreach ($ReportSubtypeData as $key => $val) {
               $cnt = 0;
               $count12 = 0;
               foreach ($val as $key1 => $commData) {
                    if($subtype['l1_cstm_subtype_c'] == $commData['subtype']){
                        $test = explode(",", $ReportSubtypeData[$key][$key1]['test_system']);
                        if (count($test) > 1) {
                            $testName = $ReportSubtypeData[$key][$key1]['test_system'];
                        } else {
                            $testName = $commData['test_system'];
                        }
                        $communication_id        = $commData['communication_name'];
                        $test_system             = $testName;
                        $date_occurred           = $commData['date_occurred'];
                        $actual_assessment_date  = $commData['actual_assessment_date'];
                        $actual_event            = $commData['actual_event'];
                        $sd_assessment           = $commData['sd_assessment'];
                        $rowHeader[$cnt]         = array('cells' => array($communication_id, $test_system, $date_occurred, $actual_assessment_date, $actual_event, $sd_assessment));
                        $cnt++;
                    }
                }    
            }
            /* below code put the header color */
            $header_row = array('Communication ID', 'Test System ID - USDA ID', 'Date Occurred', 'Actual SD Assessment Date', 'Actual Event', 'SD Assessment');
            $item = array();
                for ($j = 0; $j < count($rowHeader); $j++) {
                    for ($i = 0; $i < count($header_row); $i++) {
                        $label = $header_row[$i];
                        $value = '';
                        if (isset($rowHeader[$j]['cells'][$i])) {
                            $value = $rowHeader[$j]['cells'][$i];
                            if ((empty($value) || $value == '-') && $label == 'Test System ID - USDA ID') {
                                $value = "Not&nbsp;Applicable";
                            }
                        }
                        $item[$j][$label] = $value;
                    }
                }
                foreach ($item[0] as $k => $v) {
                    if ($k == "Communication ID") {
                        $this->options['header']['fill'] = "#4B4B4B";
                        $this->options['header']['textColor'] = "#ffffff";
                    }
                }
            $this->options["align"] = "J";
            $this->options['header']['align'] = "L";
            $this->options['header']['fontStyle'] = "";
            $this->SetFont('', '', '6.5');
            $this->writeCellTable($item, $this->options);
        }
        $this->SetFont(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN);
        $this->Ln();
    }
    /**
     * Build filename
     */
    function buildFileName()
    {
        $this->fileName = 'example.pdf';
    }

    /**
     * This method draw an horizontal line with a specific style.
     */
    protected function drawLine()
    {
        $this->SetLineStyle(array('width' => 0.85 / $this->getScaleFactor(), 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(220, 220, 220)));
        $this->MultiCell(0, 0, '', 'T', 0, 'C');
    }
}
