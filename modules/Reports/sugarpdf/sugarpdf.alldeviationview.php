<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/Sugarpdf/Sugarpdf.php');

class ReportsSugarpdfalldeviationview extends ReportsSugarpdfReports
{
    function process()
    {
        $this->preDisplay();
        $this->display();
        $this->buildFileName();
    }

    /**
     * Custom header
     */

    public function Header()
    {
        global $db;
        $ormargins              = $this->getOriginalMargins();
        $headerfont             = $this->getHeaderFont();
        $headerdata             = $this->getHeaderData();
        $headerdata['logo']     = "sugarpdf_small_header_logo.png";
        $headerdata['title']    = "All Deviations for a Study";
        $sqlForGetWPName        = 'SELECT name FROM m03_work_product WHERE id = "' . $_REQUEST['wpid'] . '"';  // query for get name of the Work Product
        $resultName             = $db->query($sqlForGetWPName);
        $rowName                = $db->fetchByassoc($resultName);
        $headerdata['string']   = 'Work Product: ' . $rowName['name'];

        //K_PATH_IMAGES
        $headerdata['logo_width'] = 50;
        if (($headerdata['logo']) and ($headerdata['logo'] != K_PATH_CUSTOM_IMAGES)) {
            $headerdata['logo'] = "sugarpdf_small_header_logo.png";
            $this->Image(K_PATH_CUSTOM_IMAGES . $headerdata['logo'], $this->GetX(), $this->getHeaderMargin(), $headerdata['logo_width']);
            $imgy = $this->getImageRBY();
        } else {
            $imgy = $this->GetY();
        }

        $cell_height = round(($this->getCellHeightRatio() * $headerfont[2]) / $this->getScaleFactor(), 2);
        // set starting margin for text data cell
        if ($this->getRTL()) {
            $header_x = $ormargins['right'] + ($headerdata['logo_width'] * 1.1);
        } else {
            $ormargins['left']    = 150;
            $header_x             = $ormargins['left'] + ($headerdata['logo_width'] * 1.1);
        }

        $this->SetTextColor(0, 0, 0);
        // header title
        $this->SetFont($headerfont[0], 'B', $headerfont[2] + 1);
        $this->SetX($header_x);
        $this->Cell(0, $cell_height, $headerdata['title'], 0, 1, '', 0, '', 0);
        // header string
        $this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);
        $this->SetX($header_x);
        $this->MultiCell(0, $cell_height, $headerdata['string'], 0, '', 0, 1, '', '', true, 0, false);

        $ormargins['left'] = 15;
        // print an ending header line
        $this->SetLineStyle(array('width' => 0.85 / $this->getScaleFactor(), 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->SetY((2.835 / $this->getScaleFactor()) + max($imgy, $this->GetY()));
        if ($this->getRTL()) {
            $this->SetX($ormargins['right']);
        } else {
            $this->SetX($ormargins['left']);
        }
        $this->Cell(0, 0, '', 'T', 0, 'C');
        $this->Ln();
    }

    /**
     * Main content
     */
    function display()
    {
        global $db, $report_modules, $app_list_strings, $mod_strings, $locale;
        $this->AddPage();
        $work_product_id    = $_REQUEST['wpid'];
        // $GLOBALS['log']->fatal("WP+===>" . $work_product_id);
        $ReportData         = array();
        $ReportData2        = array();
        $ReportSubtypeData  = array();
        $RelatedCommData    = array();
        $test_system_ids    = array();
        $usda_ids           = array();
        // To get the count order by subtype c
        $sqlSubTypeCount = "SELECT l1_cstm.subtype_c l1_cstm_subtype_c,
        IFNULL(m03_work_product.name,'') m03_work_product_name,count(*) count,
        l1.name,
        ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id
        FROM m03_work_product
        LEFT JOIN  m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id=l1_1.m06_error_m03_work_product_1m03_work_product_idb AND l1_1.deleted=0
        LEFT JOIN  m06_error l1 ON l1.id=l1_1.m06_error_m03_work_product_1m06_error_ida AND l1.deleted=0
        LEFT JOIN  m06_error_anml_animals_1_c l2_1 ON l1.id=l2_1.m06_error_anml_animals_1m06_error_ida AND l2_1.deleted=0
        LEFT JOIN  anml_animals l2 ON l2.id=l2_1.m06_error_anml_animals_1anml_animals_idb AND l2.deleted=0
        LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
        LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
        LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb AND ReletedComm.deleted=0
    WHERE (((((IFNULL(l1_cstm.error_classification_c,'') IN ('Error','Impactful Deviation','Non Impactful Deviation')
    ) AND (m03_work_product.id = '" . $work_product_id . "'
    ))) AND (((IFNULL(l1_cstm.error_category_c,'') <> 'Internal Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Internal Feedback' IS NOT NULL)) AND (IFNULL(l1_cstm.error_type_c,'') <> 'Facility' OR (IFNULL(l1_cstm.error_type_c,'') IS NULL AND 'Facility' IS NOT NULL)) AND (IFNULL(l1_cstm.error_category_c,'') <> 'Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Feedback' IS NOT NULL)))))) 
    AND  m03_work_product.deleted=0 
    GROUP BY l1.name,l1_cstm.subtype_c
    ,m03_work_product.name
    ORDER BY l1_cstm_subtype_c ASC,m03_work_product_name ASC";

        $resultSubtypeCount     = $db->query($sqlSubTypeCount);
        $hash                   = array();
        $ReportSubtypeDataCount = array();
        // $GLOBALS['log']->fatal('==Data==' . print_r($db->fetchByassoc($resultSubtypeCount), 1));
        while ($rowSubtype = $db->fetchByassoc($resultSubtypeCount)) {
            $hash_key = str_replace('^', '', $rowSubtype['l1_cstm_subtype_c']);
            $hash_key = $app_list_strings['error_type_list'][$hash_key];
            if (!array_key_exists($hash_key, $hash)) {
                $hash[$hash_key] = sizeof($ReportSubtypeDataCount);
                $subtypeValue    = $app_list_strings['error_type_list'][str_replace('^', '', $rowSubtype['l1_cstm_subtype_c'])];
                array_push($ReportSubtypeDataCount, array(
                    'm03_work_product_name' => $rowSubtype['m03_work_product_name'],
                    'l1_cstm_subtype_c'     => $subtypeValue,
                    'name'                  => $rowSubtype['name'],
                    'rel_comm_id'           => $rowSubtype['rel_comm_id'],
                    'count'                 => 0,
                ));
            }
            $ReportSubtypeDataCount[$hash[$hash_key]]['count'] += 1;
        }
        /*Query to get Report Data*/
        $sql =
            "SELECT
        IFNULL(l1.id, '') l1_id,
        IFNULL(l1. NAME, '') l1_name,
        ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id,
        IFNULL(m03_work_product.id, '') m03_work_product_id,
        l2_cstm.animal_id_c l2_cstm_animal_id_c,
        l2_cstm.usda_id_c l2_cstm_usda_id_c,
        CONCAT (l2_cstm.animal_id_c,'-',l2_cstm.usda_id_c) AS TestUsda,
        l1_cstm.date_error_occurred_c L1_CSTM_DATE_ERROR_OCC6BD1CE,
        l1_cstm.expected_event_c l1_cstm_expected_event_c,
        l1_cstm.actual_event_c l1_cstm_actual_event_c,
        l1_cstm.resolution_c l1_cstm_resolution_c,
        IFNULL(
            l1_cstm.corrective_action_1_c,
            ''
        ) L1_CSTM_CORRECTIVE_ACT674D79,
        l1_cstm.rationale_for_no_ca_c L1_CSTM_RATIONALE_FOR_F48A84,
        l1_cstm.subtype_c l1_cstm_subtype_c,
        l1_cstm.impactful_dd_c impactful_dd_c,
        IFNULL(m03_work_product. NAME, '') m03_work_product_name
      FROM
          m03_work_product
      LEFT JOIN m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id = l1_1.m06_error_m03_work_product_1m03_work_product_idb
      AND l1_1.deleted = 0
      LEFT JOIN m06_error l1 ON l1.id = l1_1.m06_error_m03_work_product_1m06_error_ida
      AND l1.deleted = 0
      LEFT JOIN m06_error_anml_animals_1_c l2_1 ON l1.id = l2_1.m06_error_anml_animals_1m06_error_ida
      AND l2_1.deleted = 0
      LEFT JOIN anml_animals l2 ON l2.id = l2_1.m06_error_anml_animals_1anml_animals_idb
      AND l2.deleted = 0
      LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
      LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
      LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb
      AND ReletedComm.deleted=0
      WHERE (((((IFNULL(l1_cstm.error_classification_c,'') IN ('Error','Impactful Deviation','Non Impactful Deviation')
      ) AND (m03_work_product.id = '" . $work_product_id . "'
      ))) AND (((IFNULL(l1_cstm.error_category_c,'') <> 'Internal Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Internal Feedback' IS NOT NULL)) AND (IFNULL(l1_cstm.error_type_c,'') <> 'Facility' OR (IFNULL(l1_cstm.error_type_c,'') IS NULL AND 'Facility' IS NOT NULL)) AND (IFNULL(l1_cstm.error_category_c,'') <> 'Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Feedback' IS NOT NULL)))))) 
      AND  m03_work_product.deleted=0 
      ORDER BY
      L1_CSTM_DATE_ERROR_OCC6BD1CE ASC,
      l2_cstm_animal_id_c ASC,
        l1_cstm_subtype_c ASC,
        m03_work_product_name ASC";
        $result = $db->query($sql);
        while ($row = $db->fetchByassoc($result)) {
            $temp = array();
            $temp['communication_id']   = $row['l1_id'];
            $temp['communication_name'] = $row['l1_name'];
            $temp['work_product_id']    = $row['m03_work_product_id'];
            $temp['work_product_name']  = $row['m03_work_product_name'];
            $temp['test_system']        = $row['l2_cstm_animal_id_c'];
            $temp['usda_id']            = $row['l2_cstm_usda_id_c'];
            $temp['rel_comm_id']        = $row['rel_comm_id'];
            $temp['impactful_dd_c']     = $row['impactful_dd_c'];

            if ($row['L1_CSTM_DATE_ERROR_OCC6BD1CE'] != "") {
                $temp['date_occurred'] = date("m/d/Y", strtotime($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']));
            } else {
                $temp['date_occurred'] = "";
            }
            $temp['expected_event']            = $row['l1_cstm_expected_event_c'];
            $temp['actual_event']              = $row['l1_cstm_actual_event_c'];
            $temp['sd_assessment']             = $row['l1_cstm_resolution_c'];
            $temp['subtype']                   = str_replace('^', '', $row['l1_cstm_subtype_c']);
            $temp['subtype']                   = $app_list_strings['error_type_list'][$temp['subtype']];
            $temp['corrective_action']         = $row['L1_CSTM_CORRECTIVE_ACT674D79'];
            $temp['corrective_action_details'] = $row['L1_CSTM_RATIONALE_FOR_F48A84'];
            $testSystemUsdaID                  = $temp['test_system'];
            if ($temp['usda_id'] != "")
                $testSystemUsdaID .= "-" . $temp['usda_id'];
            $test_system_ids[$temp['communication_name']][] = $testSystemUsdaID;
            //$ReportData[$temp['communication_name']] = $temp;
            $ReportSubtypeData[$temp['subtype']][$temp['communication_name']] = $temp;
            if ($temp['rel_comm_id'] != "") {
                $RelatedCommData[$temp['rel_comm_id']][$temp['communication_name']] = $temp;
            }
            //$test_system_ids[$temp['communication_name']][] = $temp['test_system'];
            //$usda_ids[$temp['communication_name']][] = $temp['usda_id'];
        }
        foreach ($RelatedCommData as $keyrel => $valrel) {
            foreach ($valrel as $keyrel1 => $valrel1) {
                $RelatedCommData[$keyrel][$keyrel1]['test_system'] = implode(', ', $test_system_ids[$keyrel1]);
            }
        }
        foreach ($ReportSubtypeData as $key => $val) {
            foreach ($val as $key1 => $val1) {
                $yes = "";
                $hiderow = "";
                // To mark the related record and info to main array based on the all the related records /
                foreach ($RelatedCommData as $relkey => $relval) {
                    foreach ($relval as $relkey1 => $relateddataval) {
                        //if(($val1['subtype'] == $relateddataval['subtype']) && ($val1['communication_id'] == $relateddataval['rel_comm_id'] || $val1['rel_comm_id'] == $relateddataval['rel_comm_id']))
                        if (($val1['subtype'] == $relateddataval['subtype']) && ($val1['communication_id'] == $relateddataval['rel_comm_id'])) {
                            $yes = "yes";
                        }
                        /*hide the main record row which are having the related comm id value */
                        if ($val1['rel_comm_id'] == $relateddataval['rel_comm_id'] && $val1['subtype'] == $relateddataval['subtype']) {
                            $hiderow = 'yes';
                        }
                        $ReportSubtypeData[$key][$key1]['is_related'] = $yes;
                        $ReportSubtypeData[$key][$key1]['is_hide'] = $hiderow;
                    }
                }
                $ReportSubtypeData[$key][$key1]['test_system'] = implode(', ', $test_system_ids[$key1]);
                $ReportSubtypeData[$key][$key1]['usda_id'] = implode(', ', $usda_ids[$key1]);
            }
        }
        
        foreach($ReportSubtypeDataCount as $key2 => $subtype){
            $yes = "";
            $hiderow = "";
            $top_row    = array();
            $itemTop    = array();
    
            if ($key2 == "") {
                $top_row = array("Subtype =None, Count=" . $subtype['count']);
            } else {
                $top_row = array('Subtype =' . $subtype['l1_cstm_subtype_c'] . ", Count=" . $subtype['count']);
            }
            $labelTop = $top_row[0];
            $labelTop2 = explode(" =", $labelTop);
            
            if ($labelTop2[0] == "Subtype") {
                $this->options['header']['fill'] = "#ffffff";
                $this->options['header']['textColor'] = "#000000";
                $this->options['header']['fontStyle'] = "";
            }
            $itemTop[0][$labelTop] = "";
            $this->writeCellTable($itemTop, $this->options);

            $rowHeader = array();
           foreach ($ReportSubtypeData as $key => $val) {
               $cnt = 0;
               $count12 = 0;
               foreach ($val as $key1 => $commData) {
                    if($subtype['l1_cstm_subtype_c'] == $commData['subtype']){
                        $test = explode(",", $ReportSubtypeData[$key][$key1]['test_system']);
                        $communication_id               = $commData['communication_name'];
                        $test_system                    = $commData['test_system'];
                        $date_occurred                  = $commData['date_occurred'];
                        $expected_event                 = $commData['expected_event'];
                        $actual_event                   = $commData['actual_event'];
                        $sd_assessment                  = $commData['sd_assessment'];
                        $corrective_action              = $commData['corrective_action'];
                        $corrective_action_details      = $commData['corrective_action_details'];
                        $rowHeader[$cnt]                = array('cells' => array($communication_id, $test_system, $date_occurred, $expected_event, $actual_event, $sd_assessment, $corrective_action, $corrective_action_details));
                        $cnt++;
                    }
                }    
            }
            /* below code put the header color */
            $header_row = array('Communication ID', 'Test System ID - USDA ID', 'Date Occurred', 'Expected Event', 'Actual Event', 'Impact Assessment', 'Corrective Action Within the Study', 'Corrective Action Details');
                $item = array();
                for ($count = 0; $count < count($rowHeader); $count++) {
                    for ($i = 0; $i < sizeof($header_row); $i++) {
                        $label = $header_row[$i];
                        $value = '';
                        if (isset($rowHeader[$count]['cells'][$i])) {
                            $value = $rowHeader[$count]['cells'][$i];
                            if(empty($value) && $label == 'Test System ID - USDA ID'){
                                $value = "Not&nbsp;Applicable";
                            }
                            
                            // $GLOBALS['log']->fatal("Value===>". $value);
                        }
                        $item[$count][$label] = $value;
                    }
                }
                /* below code put the header color */
                foreach ($item[0] as $k => $v) {
                    if ($k == "Communication ID") {
                        $this->options['header']['fill'] = "#4B4B4B";
                        $this->options['header']['textColor'] = "#ffffff";
                    }
                    // $GLOBALS['log']->fatal("harshit===>". $k);
                }
                $this->options["align"] = "J";
                $this->options['header']['align'] = "L";
                $this->options['header']['fontStyle'] = "";
                $this->SetFont('', '', '6.5');
                $this->writeCellTable($item, $this->options);
        }
        $this->SetFont(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN);
        $this->Ln();
    }

    /**
     * Build filename
     */
    function buildFileName()
    {
        $this->fileName = 'example.pdf';
    }

    /**
     * This method draw an horizontal line with a specific style.
     */
    protected function drawLine()
    {
        $this->SetLineStyle(array('width' => 0.85 / $this->getScaleFactor(), 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(220, 220, 220)));
        $this->MultiCell(0, 0, '', 'T', 0, 'C');
    }
}
