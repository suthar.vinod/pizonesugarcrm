<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */


class ReportsSugarpdfListview extends ReportsSugarpdfReports
{
    function preDisplay(){
        global $app_list_strings, $locale, $timedate;
        $offset =  -18000;
        $offset_time = time() + $offset;
		$formattedDate = date("D, d M Y H:i:s",$offset_time);
        parent::preDisplay();
        //Set PDF document properties
   		if($this->bean->name == "untitled") {
            $this->SetHeaderData(PDF_SMALL_HEADER_LOGO, PDF_SMALL_HEADER_LOGO_WIDTH, $app_list_strings['moduleList'][$this->bean->module], $formattedDate);
			
        } else {
            $this->SetHeaderData(PDF_SMALL_HEADER_LOGO, PDF_SMALL_HEADER_LOGO_WIDTH, $this->bean->name,$formattedDate);
			
        }
        
    }
	function display(){
        global $report_modules, $app_list_strings;
        global $mod_strings, $locale;
        $this->bean->run_query();
        
        $this->AddPage();
        
        $item = array();
        $header_row = $this->bean->get_header_row('display_columns', false, false, true);
        $count = 0;
    
        while($row = $this->bean->get_next_row('result', 'display_columns', false, true)) {
            for($i= 0 ; $i < sizeof($header_row); $i++) {
                $label = $header_row[$i];
                $value = '';
                if (isset($row['cells'][$i])) {
                    $value = $row['cells'][$i];
                }
                $item[$count][$label] = $value;
            }
            $count++;
        }
        
        $this->writeCellTable($item, $this->options);
    }
    public function Header()
    {
        $ormargins         = $this->getOriginalMargins();
        $headerfont        = $this->getHeaderFont();
        $headerdata        = $this->getHeaderData();

        //8945 Specimen Collection Activities
        if($this->bean->name == "Vet Check Review"){
            $ormargins                  = $this->getOriginalMargins();
            $headerfont                 = $this->getHeaderFont();
            $headerdata                 = $this->getHeaderData();
            $headerdata["confidential"] = "Document ID: PFS-IL-GN-012, Rev 04";
            $headerdata["Report_info"]  = "For Internal Workflow Use Only; Not Considered Raw Data";
            $headerdata['logo_width'] = 50;
            if (($headerdata['logo']) and ($headerdata['logo'] != K_PATH_CUSTOM_IMAGES)) {
                $headerdata['logo'] = "sugarpdf_small_header_logo.png";
                $this->Image(K_PATH_CUSTOM_IMAGES . $headerdata['logo'], $this->GetX(), $this->getHeaderMargin(), $headerdata['logo_width']);
                $imgy = $this->getImageRBY();
            } else {
                $imgy = $this->GetY();
            }

            $cell_height = round(($this->getCellHeightRatio() * $headerfont[2]) / $this->getScaleFactor(), 2);
            // set starting margin for text data cell
            if ($this->getRTL()) {
                $header_x = $ormargins['right'] + ($headerdata['logo_width'] * 1.0);
            } else {
                $ormargins['left']    = 120;
                $header_x            = $ormargins['left'] + ($headerdata['logo_width'] * 0.7);
            }

            $this->SetTextColor(0, 0, 0);
            $this->SetX($header_x);
            $this->MultiCell(0, $cell_height, $headerdata['confidential'], 0, '', 0, 2, '', '', true, 0, false);

            // header title 
            $this->SetX($header_x);
            $this->Cell(0, $cell_height, $headerdata['title'], 0, 1, '', 0, '', 0);
            // header string
            $this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);

            $this->SetX($header_x);
            $this->MultiCell(0, $cell_height, $headerdata['string'], 0, '', 0, 1, '', '', true, 0, false);

            //Set the font size Bold for Report_Info
            $this->SetFont($headerfont[0], 'B', $headerfont[2] + 1);
            $this->SetX($header_x);
            $this->MultiCell(0, $cell_height, $headerdata['Report_info'], 0, '', 0, 1, '', '', true, 0, false);
            $ormargins['left'] = 15;
            // print an ending header line
            $this->SetLineStyle(array('width' => 0.85 / $this->getScaleFactor(), 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
            $this->SetY((2.835 / $this->getScaleFactor()) + max($imgy, $this->GetY()));
            if ($this->getRTL()) {
                $this->SetX($ormargins['right']);
            } else {
                $this->SetX($ormargins['left']);
            }
            $this->Cell(0, 0, '', 'T', 0, 'C');
            $this->Ln();
        }else {
            //K_PATH_IMAGES
            $headerdata['logo_width'] = 50;
            $headerdata["Report_info"]  = "";
            if (($headerdata['logo']) and ($headerdata['logo'] != K_BLANK_IMAGE)) {
                $headerdata['logo'] = "sugarpdf_small_header_logo.png";
                $this->Image(K_PATH_CUSTOM_IMAGES . $headerdata['logo'], $this->GetX(), $this->getHeaderMargin(), $headerdata['logo_width']);
                $imgy = $this->getImageRBY();
            } else {
                $imgy = $this->GetY();
            }
            $imgy = $this->GetY();

            $cell_height = round(($this->getCellHeightRatio() * $headerfont[2]) / $this->getScaleFactor(), 2);
            // set starting margin for text data cell
            if ($this->getRTL()) {
                $header_x = $ormargins['right'] + ($headerdata['logo_width'] * 1.1);
            } else {
                $ormargins['left']    = 150;
                $header_x            = $ormargins['left'] + ($headerdata['logo_width'] * 1.1);
            }

            $this->SetTextColor(0, 0, 0);
            // header title
            $this->SetFont($headerfont[0], 'B', $headerfont[2] + 1);
            $this->SetX($header_x);
            $this->Cell(0, $cell_height, $headerdata['title'], 0, 1, '', 0, '', 0);
            // header string
            $this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);
            $this->SetX($header_x);
            $this->MultiCell(0, $cell_height, $headerdata['string'], 0, '', 0, 1, '', '', true, 0, false);
            //below Report_Info used for the line is in proper place
            $this->MultiCell(0, $cell_height, $headerdata['Report_info'], 0, '', 0, 1, '', '', true, 0, false);
            $this->MultiCell(0, $cell_height, $headerdata['Report_info'], 0, '', 0, 1, '', '', true, 0, false);
            $this->MultiCell(0, $cell_height, $headerdata['Report_info'], 0, '', 0, 1, '', '', true, 0, false);
        }
        $ormargins['left'] = 15;
        // print an ending header line
        $this->SetLineStyle(array('width' => 0.85 / $this->getScaleFactor(), 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->SetY((2.835 / $this->getScaleFactor()) + max($imgy, $this->GetY()));
        if ($this->getRTL()) {
            $this->SetX($ormargins['right']);
        } else {
            $this->SetX($ormargins['left']);
        }
        $this->Cell(0, 0, '', 'T', 0, 'C');
    }
}