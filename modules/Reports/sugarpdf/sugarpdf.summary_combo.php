<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
class ReportsSugarpdfSummary_combo extends ReportsSugarpdfReports
{
    function preDisplay(){
        global $app_list_strings, $locale, $timedate;
        $offset =  -18000;
        $offset_time = time() + $offset;
		$formattedDate = date("D, d M Y H:i:s",$offset_time);
        parent::preDisplay();
        //Set PDF document properties
   		if($this->bean->name == "untitled") {
            $this->SetHeaderData(PDF_SMALL_HEADER_LOGO, PDF_SMALL_HEADER_LOGO_WIDTH, $app_list_strings['moduleList'][$this->bean->module], $formattedDate);
		} else {
            $this->SetHeaderData(PDF_SMALL_HEADER_LOGO, PDF_SMALL_HEADER_LOGO_WIDTH, $this->bean->name,$formattedDate);
		} 
    }
	
	function display()
    {
        global $app_list_strings, $locale;

        //add chart
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != false) {
            $this->bean->is_saved_report = true;
        }
        // fixing bug #47260: Print PDF Reports
        // if chart_type is 'none' we don't need do add any image to pdf
        if ($this->bean->chart_type != 'none') {
            $chartDisplay = new ChartDisplay();
            $xmlFile = $chartDisplay->get_cache_file_name($this->bean);

            $sugarChart = SugarChartFactory::getInstance();

            $shouldIncludeImage = true;
            // scheduleReportWithImage should not be set to true until chart gen service is in place
            if ($this->bean->isScheduledReport && empty(SugarConfig::getInstance()->get('schedule_report_with_chart'))) {
                $shouldIncludeImage = false;
            }
            if ($sugarChart->supports_image_export && $shouldIncludeImage) {
                $imageFile = $sugarChart->get_image_cache_file_name($xmlFile, ".".$sugarChart->image_export_type);
                // check image size is not '0'
                //if (file_exists($imageFile) && getimagesize($imageFile) > 0) {
                    if (SugarAutoloader::fileExists($imageFile) && verify_uploaded_image($imageFile)) {
                    $this->AddPage();
                    //list($width, $height) = getimagesize($imageFile);
                    $chartImageResource	= imagecreatefrompng($imageFile);
                    $width				= imagesx($chartImageResource);
                    $height				= imagesy($chartImageResource);
                    imagedestroy($chartImageResource);
                    $imageWidthAsUnits	= $this->pixelsToUnits($width);
                    $imageHeightAsUnits = $this->pixelsToUnits($height);

                    $dimensions		= $this->getPageDimensions();

                    $pageWidth		= $dimensions['wk'];
                    $pageHeight		= $dimensions['hk'];

                    $marginTop		= $dimensions['tm'];
                    $marginBottom	= $dimensions['bm'];
                    $marginLeft		= $dimensions['lm'];
                    $marginRight	= $dimensions['rm'];

                    $freeWidth		= 0.9 * ($pageWidth - $marginLeft - $marginRight);
                    $freeHeight		= 0.9 * ($pageHeight - $marginTop - $marginBottom);

                    $rate			= min($freeHeight / $imageHeightAsUnits, $freeWidth / $imageWidthAsUnits, 2);
                    $imageWidth		= floor($rate * $imageWidthAsUnits);
                    $imageHeight	= floor($rate * $imageHeightAsUnits);

                    $leftOffset		= $this->GetX() + ($freeWidth - $imageWidth) / 2;
                    $topOffset		= $this->GetY() + ($freeHeight - $imageHeight) / 2;

                    $this->Image($imageFile, $leftOffset, $topOffset, $imageWidth, $imageHeight, "", "", "N", false, 300, "", false, false, 0, true);

                    if ($sugarChart->print_html_legend_pdf) {
                        $legend = $sugarChart->buildHTMLLegend($xmlFile);
                        $this->writeHTML($legend, true, false, false, true, "");
                    }
                }
            }
        }

        $this->AddPage();

        //disable paging so we get all results in one pass
        $this->bean->enable_paging = false;
        $cols = count($this->bean->report_def['display_columns']);
        $this->bean->run_summary_combo_query();

        $header_row		= $this->bean->get_summary_header_row();
        $columns_row	= $this->bean->get_header_row();

        // build options for the writeHTMLTable from options for the writeCellTable
        $options = array('header' =>
            array(
                "tr" => array(
                    "bgcolor" => $this->options['header']['fill'],
                    "color"   => $this->options['header']['textColor']),
                "td"      => array()
            ),
            'evencolor' => $this->options['evencolor'],
            'oddcolor' => $this->options['oddcolor'],
        );

        while (($row = $this->bean->get_summary_next_row()) != 0) {
            // summary columns
            $item = array();
            $count = 0;

            for ($j=0; $j < sizeof($row['cells']); $j++) {
                if ($j > count($header_row) - 1) {
                    $label = $header_row[count($header_row) - 1];
                } else {
                    $label = $header_row[$j];
                }
                if (preg_match('/type.*=.*checkbox/Uis', $row['cells'][$j])) { // parse out checkboxes
                    if (preg_match('/checked/i', $row['cells'][$j])) {
                        $row['cells'][$j] = $app_list_strings['dom_switch_bool']['on'];
                    } else {
                        $row['cells'][$j] = $app_list_strings['dom_switch_bool']['off'];
                    }
                }
                $value = $row['cells'][$j];
                $item[$count][$label] = $value;
            }

            foreach ($item as $i) {
                $text = '';
                foreach ($i as $l => $j) {
                    if ($text) {
                        $text .= ', ';
                    }
                    $text .= $l . ' = ' . $j;
                }
				if($this->bean->name=="8945 Specimen Collection Activities" ||  $this->bean->name=="8960 Specimen Collection Activities" ){
					
				}else{
					$this->Write(1, $text);
					$this->Ln1();
				}
               
            }

            // display columns
            $item = array();
            $count = 0;

            for ($i=0; $i < $this->bean->current_summary_row_count; $i++) {
                if (($column_row = $this->bean->get_next_row('result', 'display_columns', false, true)) != 0) {
                    for ($j=0; $j < sizeof($columns_row); $j++) {
                        $label = $columns_row[$j];
                        $item[$count][$label] = $column_row['cells'][$j];
                    }
                    $count++;
                } else {
                    break;
                }
            }

            $this->writeHTMLTable($item, false, $options);
            $this->Ln1();
        }

        $this->bean->clear_results();

        if ($this->bean->has_summary_columns()) {
            $this->bean->run_total_query();
            $total_row = $this->bean->get_summary_total_row();
            $item = array();
            $count = 0;

            for ($j=0; $j < sizeof($header_row); $j++) {
                $label = $header_row[$j];
                $value = $total_row['cells'][$j];
                $item[$count][$label] = $value;
            }

            $this->writeHTMLTable($item, false, $options);
        }

    }



	/**
	 	 * This method is used to render the page header.
	 	 * It is automatically called by AddPage() and could be overwritten in your own inherited class.
		 * @access public
	*/
	
	public function Header() {
		$ormargins		= $this->getOriginalMargins();
		$headerfont		= $this->getHeaderFont();
		$headerdata		= $this->getHeaderData();
		
		//$GLOBALS['log']->fatal('==>ormargins=='.print_r($ormargins,1));
		//$GLOBALS['log']->fatal('==>headerfont=='.print_r($headerfont,1));
		//$GLOBALS['log']->fatal('==>headerdata=='.print_r($headerdata,1));
		
		
		if($this->bean->name=="8945 Specimen Collection Activities"){
			
			$headerdata['logo_width']	=	0;
				$imgy						=	$this->GetY();
			    $cell_height				=	round(($this->getCellHeightRatio() * $headerfont[2]) / $this->getScaleFactor(), 2);
			    // set starting margin for text data cell
                
                if ($this->getRTL()) {
                    $header_x = $ormargins['right'] + ($headerdata['logo_width'] * 1.1);
                } else {
                    $header_x = $ormargins['left'] + ($headerdata['logo_width'] * 1.1);
                }
			    $this->SetTextColor(0, 0, 0);
                // header title
                $this->SetFont($headerfont[0], '', $headerfont[2] + 1);
                $this->SetX($header_x);
                $this->Cell(0, $cell_height, "Document ID: PFS-IL-GN-016, Rev 02", 0, 1, '', 0, '', 0);
             
                //$this->SetFont($headerfont[0], '', $headerfont[2] + 1);
                $this->SetX(200);
                $this->Cell(1, $cell_height, "Effective Date: March 25, 2022", 0, 0, '', 0, '', 0);
             
                // header title
                $this->SetFont($headerfont[0], '', $headerfont[2] + 1);
                $this->SetX($header_x);
                $this->Cell(0, $cell_height, "Title: Large Animal Specimen Collection Worksheet", 0, 1, '', 0, '', 0);

                // header string
                $this->SetFont($headerfont[0], 'B', $headerfont[2]);
                $this->SetX($header_x);
                $this->MultiCell(0, $cell_height, "For Internal Workflow use Only; Not Subject to Audit", 0, '', 0, 1, '', '', true, 0, false);
            }else if($this->bean->name=="8960 Specimen Collection Activities"){

                $headerdata['logo_width']=0;
				$imgy = $this->GetY();
			    $cell_height = round(($this->getCellHeightRatio() * $headerfont[2]) / $this->getScaleFactor(), 2);
			    // set starting margin for text data cell
                
                if ($this->getRTL()) {
                    $header_x = $ormargins['right'] + ($headerdata['logo_width'] * 1.1);
                } else {
                    $header_x = $ormargins['left'] + ($headerdata['logo_width'] * 1.1);
                }
			    $this->SetTextColor(0, 0, 0);
                // header title
                $this->SetFont($headerfont[0], '', $headerfont[2] + 1);
                $this->SetX($header_x);
                $this->Cell(0, $cell_height, "Document ID: PFS-IL-GN-017 Rev 02", 0, 1, '', 0, '', 0);
             
                //$this->SetFont($headerfont[0], '', $headerfont[2] + 1);
                $this->SetX(200);
                $this->Cell(1, $cell_height, "Effective Date: March 25, 2022", 0, 0, '', 0, '', 0);
             
                // header title
                $this->SetFont($headerfont[0], '', $headerfont[2] + 1);
                $this->SetX($header_x);
                $this->Cell(0, $cell_height, "Title: Small Animal Specimen Collection Worksheet", 0, 1, '', 0, '', 0);

                // header string
                $this->SetFont($headerfont[0], 'B', $headerfont[2]);
                $this->SetX($header_x);
                $this->MultiCell(0, $cell_height, "For Internal Workflow use Only; Not Subject to Audit", 0, '', 0, 1, '', '', true, 0, false);
            }else{
				//K_PATH_IMAGES
                $headerdata['logo_width'] = 50;
                if (($headerdata['logo']) AND ($headerdata['logo'] != K_BLANK_IMAGE)) {
                    $this->Image(K_PATH_CUSTOM_IMAGES.$headerdata['logo'], $this->GetX(), $this->getHeaderMargin(), $headerdata['logo_width']);
                    $imgy = $this->getImageRBY();
                } else {
                    $imgy = $this->GetY();
                }

                $cell_height = round(($this->getCellHeightRatio() * $headerfont[2]) / $this->getScaleFactor(), 2);
                // set starting margin for text data cell
                if ($this->getRTL()) {
                    $header_x = $ormargins['right'] + ($headerdata['logo_width'] * 1.1);
                } else {
                    $ormargins['left']	= 150;
                    $header_x			= $ormargins['left'] + ($headerdata['logo_width'] * 1.1);
                }

                $this->SetTextColor(0, 0, 0);
                // header title
                $this->SetFont($headerfont[0], 'B', $headerfont[2] + 1);
                $this->SetX($header_x);
                $this->Cell(0, $cell_height, $headerdata['title'], 0, 1, '', 0, '', 0);
                // header string
                $this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);
                $this->SetX($header_x);
                $this->MultiCell(0, $cell_height, $headerdata['string'], 0, '', 0, 1, '', '', true, 0, false);

            }  
            $ormargins['left'] = 15;
            // print an ending header line
			$this->SetLineStyle(array('width' => 0.85 / $this->getScaleFactor(), 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
			$this->SetY((2.835 / $this->getScaleFactor()) + max($imgy, $this->GetY()));
			if ($this->getRTL()) {
				$this->SetX($ormargins['right']);
			} else {
				$this->SetX($ormargins['left']);
			}
			$this->Cell(0, 0, '', 'T', 0, 'C');
           
	}

    /**
		* This method is used to render the page footer.
		* It is automatically called by AddPage() and could be overwritten in your own inherited class.
		* @access public
	*/
	
	public function Footer() {
		$cur_y		= $this->GetY();
		$ormargins	= $this->getOriginalMargins();
		$this->SetTextColor(0, 0, 0);
		//set style for cell border
		$line_width	= 0.85 / $this->getScaleFactor();
		
		$this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
		//print document barcode
		
		$barcode = $this->getBarcode();
		
		if (!empty($barcode)) {
				$this->Ln($line_width);
				$barcode_width = round(($this->getPageWidth() - $ormargins['left'] - $ormargins['right'])/3);
				$this->write1DBarcode($barcode, 'C128B', $this->GetX(), $cur_y + $line_width, $barcode_width, (($this->getFooterMargin() / 3) - $line_width), 0.3, '', '');
		}
		
		if (empty($this->pagegroups)) {
			$pagenumtxt = $this->l['w_page'].' '.$this->getAliasNumPage().' / '.$this->getAliasNbPages();
		} else {
			$pagenumtxt = $this->l['w_page'].' '.$this->getPageNumGroupAlias().' / '.$this->getPageGroupAlias();
		}
		
		if($this->bean->name=="8945 Specimen Collection Activities" || $this->bean->name=="8960 Specimen Collection Activities" ){
			$this->SetX(15);
			$this->Cell(0, 0, "CONFIDENTIAL", 0, 0, '', 0, '', 0);
        }
		
		$this->SetY($cur_y);
		//Print page number
		if ($this->getRTL()) {
			$this->SetX($ormargins['right']);
			$this->Cell(0, 0, $pagenumtxt, 'T', 0, 'L');
		} else {
			$this->SetX($ormargins['left']);
			$this->Cell(0, 0, $pagenumtxt, 'T', 0, 'R');
		}
	}
}
