<?php
//AllDeviationsforStudy
require_once('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config, $app_list_strings, $db, $current_user;
$ss = new Sugar_Smarty();

if (isset($_REQUEST['work_product_group_id']) && $_REQUEST['work_product_group_id'] != '')
{
    $work_product_group_id = $_REQUEST['work_product_group_id'];
    $wpgArr[] = $work_product_group_id;
    $wp_data['id'] = $work_product_group_id;
    $wp_data['name'] = $_REQUEST['work_product_group_name'];
}

if (isset($_REQUEST['work_product_group_id_1']) && $_REQUEST['work_product_group_id_1'] != '')
{
    $work_product_group_id_1 = $_REQUEST['work_product_group_id_1'];
    $wpgArr[] = $work_product_group_id_1;
    $wp_data['id_1'] = $work_product_group_id_1;
    $wp_data['name_1'] = $_REQUEST['work_product_group_name_1'];
}
if (isset($_REQUEST['work_product_group_id_2']) && $_REQUEST['work_product_group_id_2'] != '')
{
    $work_product_group_id_2 = $_REQUEST['work_product_group_id_2'];
    $wpgArr[] = $work_product_group_id_2;
    $wp_data['id_2'] = $work_product_group_id_2;
    $wp_data['name_2'] = $_REQUEST['work_product_group_name_2'];
}
if (isset($_REQUEST['work_product_group_id_3']) && $_REQUEST['work_product_group_id_3'] != '')
{
    $work_product_group_id_3 = $_REQUEST['work_product_group_id_3'];
    $wpgArr[] = $work_product_group_id_3;
    $wp_data['id_3'] = $work_product_group_id_3;
    $wp_data['name_3'] = $_REQUEST['work_product_group_name_3'];
}
if (isset($_REQUEST['work_product_group_id_4']) && $_REQUEST['work_product_group_id_4'] != '')
{
    $work_product_group_id_4 = $_REQUEST['work_product_group_id_4'];
    $wpgArr[] = $work_product_group_id_4;
    $wp_data['id_4'] = $work_product_group_id_4;
    $wp_data['name_4'] = $_REQUEST['work_product_group_name_4'];
} 
$wpg_ids		= "'$work_product_group_id','$work_product_group_id_1'";
$tempWeekData	= array();
$WeekDataArray	= array();
$last6monthWeek	= intval(date("W", strtotime(date('Y-m-d', strtotime("-6 month")))));
$startdateweek	= intval(date("W", strtotime(date('Y-m-d'))));
$count			= 0;


for($i=$last6monthWeek;$i<=$startdateweek;$i++)
{
	$count =$count+1;
	$WeekDataArray[$count] = $i;	
}

$arLeadChartDat1	= array();
$rowHeadData[0][0]	= "Sr. No";
$i = 0;

for($cnt=1;$cnt<=count($wpgArr);$cnt++){

	$tsCondition = "";
	$tsQuery	 = "SELECT test_system_c FROM wpg_work_product_group_cstm where id_c='".$wpgArr[$i]."'";
	$resultQry	 = $db->query($tsQuery);
	while ($rowTS = $db->fetchByassoc($resultQry)) 
	{
		$ts	= str_replace("^","'",$rowTS['test_system_c']);
		if($ts!=""){
			$tsCondition = "AND  WPC.test_system_c IN (".$ts.")";
		}
		
	}	

	$sql = "SELECT (WEEK(`first_procedure_c`, 1)) AS weekCount,
					WPG.name,count(id_c) AS totalWP,
					WPG.total_capacity,
					WPC.first_procedure_c FROM m03_work_product_cstm AS WPC
			LEFT JOIN wpg_work_product_group_m03_work_product_code_1_c AS WP_WPG 
			ON WPC.m03_work_product_code_id1_c = WP_WPG.wpg_work_pa92cct_code_idb AND WP_WPG.deleted=0

			LEFT JOIN wpg_work_product_group AS WPG
			ON WPG.id = WP_WPG.wpg_work_p164at_group_ida
				
			WHERE  first_procedure_c >= curdate() - interval (dayofmonth(curdate()) - 1) day - INTERVAL 6 month      
					AND WP_WPG.wpg_work_p164at_group_ida ='".$wpgArr[$i]."' 
					AND WP_WPG.deleted=0
					".$tsCondition."
			group by weekCount,WPG.name ORDER BY weekCount ASC";

	$result			= $db->query($sql);	
	$tempWeekData	= array();
	$tempWeekData1	= array();
	$resultName 	= "";	
	while ($row = $db->fetchByassoc($result)) 
	{
		$weekCount				  	= $row['weekCount'];
		$resultName 	 			= $row['name'];
		$rowTotal 					= $row['totalWP'];
		$capacity 					= $row['total_capacity'];
		$Cu_rate  					= round($rowTotal / $capacity * 100,2);	
		$tempWeekData[$weekCount] 	= $row['totalWP']." (".$Cu_rate.") ";
		$tempWeekData1[$weekCount] 	= $Cu_rate;
	}
	
	for($cntr=0;$cntr<count($WeekDataArray);$cntr++){
		
		if($cntr==0)
		{
			$weekData[$cntr][0] 	= "Week Number";
			$weekData[$cntr][$cnt]	= $resultName;
		}else{
			$weekData[$cntr][0]		= $WeekDataArray[$cntr];
			if($tempWeekData[$WeekDataArray[$cntr]]!=""){
				$weekData[$cntr][$cnt] = $tempWeekData[$WeekDataArray[$cntr]];
			}else{
				$weekData[$cntr][$cnt] = 0;
			} 
		}
		$arLeadChartDat1[$WeekDataArray[$cntr]][$resultName] = $tempWeekData1[$WeekDataArray[$cntr]];
	}
	$i++;	
}

if(count($arLeadChartDat1)>0)
	$graphAvailable  = 1;
else
	$graphAvailable  = 0;	

	$obSugarChart = SugarChartFactory::getInstance();
	$obSugarChart->setData($arLeadChartDat1);
		
        
    $obSugarChart->setProperties('Custom Capacity Report', '', 'group by chart','off');
	$obSugarChart->is_currency = true;
	$obSugarChart->currency_symbol = '';
    $xmlFile = $obSugarChart->getXMLFileName('lead_status_graph');
    $obSugarChart->saveXMLFile($xmlFile, $obSugarChart->generateXML());
    $stReturnString=$obSugarChart->display('lead_status_graph', $xmlFile, "100%", '500');

    $obSugarChart = SugarChartFactory::getInstance();
    $rsResources = $obSugarChart->getChartResources();
    $ss->assign('GRAPH_RESOURCES',$rsResources);
    $ss->assign('LEADS_CHART_DATA',$stReturnString);
	$ss->assign('IS_CHART',$graphAvailable);
	
	

$ss->assign('current_user', $current_user->id);
$ss->assign('WPDATA', $wp_data);
$ss->assign('ReportData', $weekData);
$ss->assign('WPGNameData', $rowHeadData);
$ss->assign('SITE_URL', $GLOBALS['sugar_config']['site_url']);
$html = $ss->fetch("custom/modules/Reports/tpls/CustomCapacity.tpl");

echo $html;