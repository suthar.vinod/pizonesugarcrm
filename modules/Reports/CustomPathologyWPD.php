<?php
require_once ('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config;
global $db;
$ss = new Sugar_Smarty();
$ReportData = array();
        $temp = array();
		$srno = 1;
$query = "SELECT  
			WP.id AS id, 	
			WP.name AS name,
			WPCSTM.work_product_status_c AS WPP,
			WPDCSTM.internal_final_due_date_c AS Duedate, 
			WPD.name AS WPDname
			FROM `m03_work_product` AS WP
			RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
					ON  WP.id = WP_WPD.m03_work_p0b66product_ida AND WP_WPD.deleted=0
				LEFT JOIN m03_work_product_deliverable AS WPD
					ON WP_WPD.m03_work_pe584verable_idb=WPD.id AND WP.deleted=0
				RIGHT JOIN m03_work_product_cstm AS WPCSTM
					ON WP.id=WPCSTM.id_c
				LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
				ON WPD.id= WPDCSTM.id_c
			WHERE (WPDCSTM.deliverable_c ='Histopathology Notes' OR WPDCSTM.deliverable_c ='Histopathology Report2' OR WPDCSTM.deliverable_c ='Interim Histopathology Report2' OR WPDCSTM.deliverable_c = 'Interim Histopathology Report' OR WPDCSTM.deliverable_c = 'Histopathology Report')
			AND (WPCSTM.work_product_status_c = 'Testing' OR WPCSTM.work_product_status_c = 'In Development')AND WP.deleted = 0 GROUP BY WP.name ORDER BY WPDCSTM.internal_final_due_date_c ASC";

$queryResult = $db->query($query);
while ($fetchResult = $db->fetchByAssoc($queryResult)) {
	$id	 			= $fetchResult['id'];
	$WP_name	 	= $fetchResult['name'];
	$WPDname	 	= $fetchResult['WPDname'];
		$carts = array();
		$notin = "";
		$tissue = "";
        
	$wp_query = "SELECT WPD.id AS wid2, 	
						WP.name AS wpname, 
						WP.id AS wpid, 
						WPD.name AS WPDname,
						WPDCSTM.deliverable_c AS WPDdeliverable							          
						FROM `m03_work_product_deliverable` AS WPD
							LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
								ON WPD.id= WPDCSTM.id_c
							RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
								ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
							LEFT JOIN m03_work_product AS WP
								ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
							LEFT JOIN m03_work_product_cstm AS WPCSTM
								ON WP.id=WPCSTM.id_c
						WHERE  WP.name = '" . $WP_name . "'
						 AND WPD.deleted = 0";
								$queryResult1 = $db->query($wp_query);
								
								if ($queryResult1->num_rows > 0) {
									while ($fetchResult1 = $db->fetchByAssoc($queryResult1)) {
										$wpid	= $fetchResult1['wpid'];
										$wpname 	= $fetchResult1['wpname'];
										$WPDname 	= $fetchResult1['WPDname'];
										$WPDdeliverable 	= $fetchResult1['WPDdeliverable'];
										$carts[] = $WPDdeliverable;
									}
								}
							
								/* foreach ($carts as $cart) {
									if ($cart == "Paraffin Slide Completion" || $cart == "EXAKT Slide Completion" || $cart == "Plastic Microtome Slide Completion") {
										$notin = "yes";
									} else {
										$notin = "";
									}
									if ($cart == "Tissue Trimming") {
										$tissue = "yes";
									} else {
										$tissue = "";
									}									
								} */
								if (in_array("Paraffin Slide Completion", $carts)) {
									$notin = "Yes";
								} else if (in_array("EXAKT Slide Completion", $carts)) {
									$notin = "Yes";
								} else if (in_array("Plastic Microtome Slide Completion", $carts)) {
									$notin = "Yes";
								} else if (in_array("Slide Completion", $carts)) {
									$notin = "Yes";
								} else {
									$notin = "No";
								}
								if (in_array("Tissue Trimming", $carts)) {
									$tissue = "Yes";
								} else {
									$tissue = "No";
								}
								/* if (($notin == "" &&  $tissue != "") || ($notin == "" &&  $tissue == "")) {	
									 $temp['srno'] 		= $srno++;
									 $temp['wpid'] 	    = $wpid;
									 $temp['wpname'] 	= $wpname;						
									 array_push($ReportData, $temp);
								 } */
								 if (($tissue == "Yes" && $notin == "No") || ($tissue == "No" && $notin == "No"))
								{									
										$temp['srno'] 		= $srno++;
										$temp['wpid'] 	    = $wpid;
										$temp['wpname'] 	= $wpname;						
										array_push($ReportData, $temp);																	
								} 
								
}
/* echo '<pre>';
print_r($ReportData); */

$ss->assign('date_created', $date_created);
$ss->assign('current_user', $current_user->id);
$ss->assign('ReportData', $ReportData);
$ss->assign('SITE_URL', $GLOBALS['sugar_config']['site_url']);
$html = $ss->fetch("custom/modules/Reports/tpls/CustomPathologyWPD.tpl");

echo $html;
?>