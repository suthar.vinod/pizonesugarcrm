<?php
require_once ('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config;
global $db;
$ss = new Sugar_Smarty();

//Submit form will check we have changed the filter or using by default filters.
if (isset($_REQUEST['submit_form']) && $_REQUEST['submit_form'] == 1)
{
    $entrollment_status_filter = $_REQUEST["entrollment_status_filter"];
    $enrollment_activity_filter = $_REQUEST["enrollment_activity_filter"];
    $test_system_filter = $_REQUEST["test_system_filter"];
    $test_allocated_work_product_filter = $_REQUEST["test_allocated_work_product_filter"];
    $species_filter = $_REQUEST["species_filter"];
    $work_product_filter = $_REQUEST["work_product_filter"];

    if ($entrollment_status_filter == "is" || $entrollment_status_filter == "is_not")
    {
        $entrollment_status = $_REQUEST["entrollment_status"];
    }

    if ($enrollment_activity_filter == "is" || $enrollment_activity_filter == "is_not")
    {
        $enrollment_activity = $_REQUEST["enrollment_activity"];
    }

    if ($test_system_filter == "is" || $test_system_filter == "is_not")
    {
        $test_system_id = $_REQUEST["test_system_id"];
    }

    if ($test_allocated_work_product_filter == "is" || $test_allocated_work_product_filter == "is_not")
    {
        $test_allocated_work_product_id = $_REQUEST["test_allocated_work_product_id"];

    }
    if ($species_filter == "is" || $species_filter == "is_not")
    {
        $species_id = $_REQUEST["species_id"];
    }

    if ($work_product_filter == "is" || $work_product_filter == "is_not")
    {
        $work_product_id = $_REQUEST["work_product_id"];
    }
    $wp_data['entrollment_status_filter'] = $entrollment_status_filter;
    $wp_data['enrollment_activity_filter'] = $enrollment_activity_filter;
    $wp_data['test_system_filter'] = $test_system_filter;
    $wp_data['test_allocated_work_product_filter'] = $test_allocated_work_product_filter;
    $wp_data['species_filter'] = $species_filter;
    $wp_data['work_product_filter'] = $work_product_filter;
    $wp_data['entrollment_status'] = $entrollment_status;
    $wp_data['enrollment_activity'] = $enrollment_activity;
    $wp_data['test_system_id'] = $test_system_id;
    $wp_data['test_system_name'] = $_REQUEST['test_system_name'];
    $wp_data['test_allocated_work_product_id'] = $test_allocated_work_product_id;
    $wp_data['test_allocated_work_product_name'] = $_REQUEST['test_allocated_work_product_name'];
    $wp_data['species_id'] = $species_id;
    $wp_data['species_name'] = $_REQUEST['species_name'];
    $wp_data['work_product_id'] = $work_product_id;
    $wp_data['work_product_name'] = $_REQUEST['work_product_name'];

}
else
{

    $entrollment_status = "On Study";
    $species_id = "5dad1606-1131-11ea-80f4-02fb813964b8";
    $work_product_id = "4a529aae-5165-3149-6db7-5702c7b98a5e";
    $wp_data['entrollment_status_filter'] = "is";
    $wp_data['enrollment_activity_filter'] = "empty";
    $wp_data['test_system_filter'] = "not_empty";
    $wp_data['test_allocated_work_product_filter'] = "not_empty";
    $wp_data['species_filter'] = "is";
    $wp_data['work_product_filter'] = "is";
    $wp_data['entrollment_status'] = "On Study";
    $wp_data['enrollment_activity'] = "";
    $wp_data['test_system_id'] = "";
    $wp_data['test_system_name'] = "";
    $wp_data['test_allocated_work_product_id'] = "";
    $wp_data['test_allocated_work_product_name'] = "";
    $wp_data['species_id'] = "5dad1606-1131-11ea-80f4-02fb813964b8";
    $wp_data['species_name'] = "Lagomorph";
    $wp_data['work_product_id'] = "4a529aae-5165-3149-6db7-5702c7b98a5e";
    $wp_data['work_product_name'] = "APS001-AH01";

}
$esAppend = "";
switch ($entrollment_status_filter)
{
    case "is":
        $esAppend = "( Ifnull(wpe_work_product_enrollment_cstm.enrollment_status_c, '') = 
    '$entrollment_status') ";
    break;

    case "is_not":
        $esAppend = " ( wpe_work_product_enrollment_cstm.enrollment_status_c <> 
        'On Study' 
         OR ( wpe_work_product_enrollment_cstm.enrollment_status_c IS 
              NULL 
              AND '$entrollment_status' IS NOT NULL ) )  ";
    break;
    default:
        $esAppend = "( Ifnull(wpe_work_product_enrollment_cstm.enrollment_status_c, '') = 
    '$entrollment_status') ";

}

$eactivityAppend = "";
switch ($enrollment_activity_filter)
{
    case "is":
        $eactivityAppend = "(wpe_work_product_enrollment_cstm.activities_c = '^$enrollment_activity^'
        )";
    break;

    case "is_not":
        $eactivityAppend = " (wpe_work_product_enrollment_cstm.activities_c <> '^$enrollment_activity^' OR (wpe_work_product_enrollment_cstm.activities_c IS NULL)
        )
        ";
    break;
    case "empty":
        $eactivityAppend = " ((coalesce(LENGTH(IFNULL(wpe_work_product_enrollment_cstm.activities_c,'')),0) = 0 OR IFNULL(wpe_work_product_enrollment_cstm.activities_c,'') = '^^'))";
    break;
    case "not_empty":
        $eactivityAppend = " ((coalesce(LENGTH(IFNULL(wpe_work_product_enrollment_cstm.activities_c,'')),0) > 0 AND IFNULL(wpe_work_product_enrollment_cstm.activities_c,'') != '^^' )
        ) ";
    break;
    default:
        $eactivityAppend = " ((coalesce(LENGTH(IFNULL(wpe_work_product_enrollment_cstm.activities_c,'')),0) = 0 OR IFNULL(wpe_work_product_enrollment_cstm.activities_c,'') = '^^')) ";
}
$testSysNameAppend = "";
switch ($test_system_filter)
{
    case "is":
        $testSysNameAppend = "(l2.id='$test_system_id')";
    break;
    case "is_not":
        $testSysNameAppend = "( l2.id <> '$test_system_id' )";
    break;
    case "empty":
        $testSysNameAppend = "(( COALESCE(Length(l2.name), 0) = 0 ))";
    break;
    case "not_empty":
        $testSysNameAppend = "(( COALESCE(Length(l2.name), 0) <> 0 ))";
    break;
    default:
        $testSysNameAppend = "(( COALESCE(Length(l2.name), 0) <> 0 ))";
}
$testSysAllocatedWP = "";
switch ($test_allocated_work_product_filter)
{
    case "is":
        $testSysAllocatedWP = "(( l1_cstm.m03_work_product_id_c='$test_allocated_work_product_id'))";
    break;
    case "is_not":
        $testSysAllocatedWP = "(( l1_cstm.m03_work_product_id_c <> '$test_allocated_work_product_id'))";
    break;
    case "empty":
        $testSysAllocatedWP = "(( COALESCE(Length(l1_cstm.m03_work_product_id_c), 0) = 0 ))";
    break;
    case "not_empty":
        $testSysAllocatedWP = "(( COALESCE(Length(l1_cstm.m03_work_product_id_c), 0) <> 0 ))";
    break;
    default:
        $testSysAllocatedWP = "(( COALESCE(Length(l1_cstm.m03_work_product_id_c), 0) <> 0 ))";
}
$speciesAppend = "";
switch ($species_filter)
{
    case "is":
        $speciesAppend = "(( l1_cstm.s_species_id_c='$species_id'))";
    break;
    case "is_not":
        $speciesAppend = "(( l1_cstm.s_species_id_c <> '$species_id'))";
    break;
    case "empty":
        $speciesAppend = "(( COALESCE(Length(l1_cstm.s_species_id_c), 0) = 0 ))";
    break;
    case "not_empty":
        $speciesAppend = "(( COALESCE(Length(l1_cstm.s_species_id_c), 0) <> 0 ))";
    break;
    default:
        $speciesAppend = "(( l1_cstm.s_species_id_c='$species_id'))";
}
$wpAppend = "";
switch ($work_product_filter)
{
    case "is":
        $wpAppend = "(( l2.id='$work_product_id'))";
    break;
    case "is_not":
        $wpAppend = "((l2.id <> '$work_product_id'))";
    break;
    case "empty":
        $wpAppend = "(( COALESCE(Length(l2.id), 0) = 0 ))";
    break;
    case "not_empty":
        $wpAppend = "(( COALESCE(Length(l2.id), 0) <> 0 ))";
    break;
    default:
        $wpAppend = "(( l2.id='$work_product_id'))";
}

$sql = "SELECT IFNULL(wpe_work_product_enrollment.id,'') primaryid
,IFNULL(wpe_work_product_enrollment.name,'') WPE_WORK_PRODUCT_ENROL972FC8
,IFNULL(wpe_work_product_enrollment_cstm.enrollment_status_c,'') WPE_WORK_PRODUCT_ENROLB4DC47,
wpe_work_product_enrollment.date_entered WPE_WORK_PRODUCT_ENROLCC2959,
wpe_work_product_enrollment_cstm.activities_c WPE_WORK_PRODUCT_ENROLBAABD0,
IFNULL(wpe_work_product_enrollment.id,'') WPE_WORK_PRODUCT_ENROLC37BAE,l1_cstm.abnormality_c l1_cstm_abnormality_c,IFNULL(l1_cstm.sex_c,'') l1_cstm_sex_c,l1_cstm.bodyweight_c l1_cstm_bodyweight_c,l1_cstm.date_last_weighed_c l1_cstm_date_last_weighed_c,l1_cstm.m03_work_product_id_c L1_CSTM_M03_WORK_PRODU479074,m03_work_product1.name m03_work_product1_name,IFNULL(l1_cstm.breed_c,'') l1_cstm_breed_c
FROM wpe_work_product_enrollment
 INNER JOIN  anml_animals_wpe_work_product_enrollment_1_c l1_1 ON wpe_work_product_enrollment.id=l1_1.anml_anima9941ollment_idb AND l1_1.deleted=0
 INNER JOIN  anml_animals l1 ON l1.id=l1_1.anml_animals_wpe_work_product_enrollment_1anml_animals_ida AND l1.deleted=0
LEFT JOIN  m03_work_product_wpe_work_product_enrollment_1_c l2_1 ON wpe_work_product_enrollment.id=l2_1.m03_work_p9bf5ollment_idb AND l2_1.deleted=0
LEFT JOIN  m03_work_product l2 ON l2.id=l2_1.m03_work_p7d13product_ida AND l2.deleted=0
LEFT JOIN wpe_work_product_enrollment_cstm wpe_work_product_enrollment_cstm ON wpe_work_product_enrollment.id = wpe_work_product_enrollment_cstm.id_c
LEFT JOIN anml_animals_cstm l1_cstm ON l1.id = l1_cstm.id_c
LEFT JOIN m03_work_product m03_work_product1 ON m03_work_product1.id = l1_cstm.m03_work_product_id_c AND IFNULL(m03_work_product1.deleted,0)=0 
LEFT JOIN s_species s_species2 ON s_species2.id = l1_cstm.s_species_id_c AND IFNULL(s_species2.deleted,0)=0 
WHERE  (( (( COALESCE(Length(l2.NAME), 0) <> 0 )) 
    AND $esAppend 
    AND $eactivityAppend 
    AND $testSysNameAppend 
    AND $testSysAllocatedWP
    AND $speciesAppend
    AND (( l1_cstm.deceased_checkbox_c IS NOT NULL 
       AND l1_cstm.deceased_checkbox_c = '0' )) 
    AND $wpAppend ))
AND wpe_work_product_enrollment.deleted = 0 
AND Ifnull(m03_work_product1.deleted, 0) = 0 
group by WPE_WORK_PRODUCT_ENROL972FC8 order by WPE_WORK_PRODUCT_ENROLCC2959 desc";


$result = $db->query($sql);
$temp = array();

$offset1 =  -18000;
//$offset1 =  -21600; //daylight Saving


while ($row = $db->fetchByassoc($result))
{
    $dateLastWeight="";
    if($row['l1_cstm_date_last_weighed_c']!=""){
        $dateLastWeight=date("m/d/Y", strtotime($row['l1_cstm_date_last_weighed_c']));
    }

    $weight="0.0";
    if($row['l1_cstm_bodyweight_c']!=""){
        $weight=$row['l1_cstm_bodyweight_c'];
    }



    array_push($temp, array(
        'WPA_ID' => $row['primaryid'],
        'WPA_Name' => $row['WPE_WORK_PRODUCT_ENROL972FC8'],
        'status' => $row['WPE_WORK_PRODUCT_ENROLB4DC47'],
        'enroll_date' => date("m/d/Y H:i", strtotime($row['WPE_WORK_PRODUCT_ENROLCC2959'])+$offset1),
        'allocatedWorkProductId' => $row['L1_CSTM_M03_WORK_PRODU479074'],
        'allocatedWorkProductName' => $row['m03_work_product1_name'],
        'last_date_weighted' => $dateLastWeight,
        'weight' => $weight,
        'breed' => $row['l1_cstm_breed_c'],
        'sex' => $row['l1_cstm_sex_c'],
        'activities' => str_replace("^", "", $row['WPE_WORK_PRODUCT_ENROLBAABD0']) ,
        'abnormality' => $row['l1_cstm_abnormality_c'],
    ));

}

$ss->assign('REPORTDATA', $temp);
$ss->assign('WPDATA', $wp_data);
$ss->assign('SITE_URL', $GLOBALS['sugar_config']['site_url']);
// echo ">>".$GLOBALS['sugar_config']['site_url'];
$html = $ss->fetch("custom/modules/Reports/tpls/MostRecentWPAForTestSystem.tpl");
echo $html;

?>