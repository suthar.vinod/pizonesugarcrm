<?php
require_once ('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config, $app_list_strings, $db, $current_user;
$ss = new Sugar_Smarty();
$ORDER_TYPE = "ASC";
$ORDER_COLUMN = "wpename";

$category_filter = $_REQUEST['category_filter'];
$multicategoryfilter = $_REQUEST['multicatfilter'];
$category_status = $_REQUEST['category_status'];
 
if(!empty($multicategoryfilter) && ($category_filter == "one_of" || $category_filter == "not_one_of")){
    $multicategoryfilterArr = explode(",",$multicategoryfilter);
    $categoryfilterInList = '';
    foreach ($multicategoryfilterArr as $multicategoryfilterVal){
        //$categoryfilterInList[] = "'".$multicategoryfilterVal."'";
        $categoryfilterInList != "" && $categoryfilterInList .= ",";
        $categoryfilterInList .= "'".$multicategoryfilterVal."'";
    }
    $categorystatus = $categoryfilterInList;
    $multicategoryfilter = implode(',',$category_status);

}else{
    $categorystatus = $category_status;
    $category_status = array($category_status);
}


$category = "";

if ($_REQUEST['submitform'] == 1)
{
    //$category = "AND (l1.category IN ('".$_REQUEST["category_status"]."'))";
    switch ($category_filter)
    {
        case "is":
            $category = "AND (l1.category = '".$categorystatus."')";
        break;

        case "is_not":
            $category = "AND (l1.category <> '".$categorystatus."' OR ( l1.category IS NULL  AND  '".$categorystatus."' IS NOT NULL ))";
        break;
 
        case "one_of":
            $category = "AND (l1.category IN (".$categorystatus."))";
        break;

        case "not_one_of":
            $category = "AND (l1.category NOT IN (".$categorystatus.") OR  l1.category IS NULL)";
        break;

        case "empty":
            $category = "AND ((coalesce(LENGTH(IFNULL(l1.category,'')),0) = 0 OR IFNULL(l1.category,'') = '^^'))";
        break;
        case "not_empty":
            $category = "AND ((coalesce(LENGTH(IFNULL(l1.category,'')),0) > 0 AND IFNULL(l1.category,'') != '^^' )) ";
        break;

        default:
            $category = "AND (l1.category = '".$categorystatus."')";
    }
}

 
if(!isset($_REQUEST['category_status'])){
    $category = "AND (l1.category = 'Task')";
}

if ($_REQUEST["order_type"] != "" && $_REQUEST["order_type"] != "")
{
    $ORDER_TYPE = $_REQUEST["order_type"];
    $ORDER_COLUMN = $_REQUEST["order_column"];
}

// current_user//
$wp_data['current_user_id'] = $current_user->id;
$ReportData = array();
$summaryData = array();

// To get the count of APS WP iD
$sqlWPCount = "SELECT IFNULL(m03_work_product.name,'') m03_work_product_name
,count(*) count
FROM m03_work_product
 INNER JOIN  m03_work_product_taskd_task_design_1_c l1_1 ON m03_work_product.id=l1_1.m03_work_product_taskd_task_design_1m03_work_product_ida AND l1_1.deleted=0

 INNER JOIN  taskd_task_design l1 ON l1.id=l1_1.m03_work_product_taskd_task_design_1taskd_task_design_idb AND l1.deleted=0
 INNER JOIN  taskd_task_design_taskd_task_design_1_c l2_1 ON l1.id=l2_1.taskd_task_design_taskd_task_design_1taskd_task_design_idb AND l2_1.deleted=0

 INNER JOIN  taskd_task_design l2 ON l2.id=l2_1.taskd_task_design_taskd_task_design_1taskd_task_design_ida AND l2.deleted=0
LEFT JOIN taskd_task_design_cstm l1_cstm ON l1.id = l1_cstm.id_c
LEFT JOIN m03_work_product_cstm m03_work_product_cstm ON m03_work_product.id = m03_work_product_cstm.id_c

 WHERE ((((((coalesce(LENGTH(l1.actual_datetime), 0) = 0)) AND (l1.type_2 = 'Actual'
) AND (l1.category <> 'Unscheduled Task' OR ( l1.category IS NULL  AND  'Unscheduled Task' IS NOT NULL )) ".$category.")) AND (((m03_work_product_cstm.test_system_c IN ('Bovine','Canine','Caprine','Micro Yucatans','Ossabaw','Ovine','PorcineGeneral','Porcine Got','Porcine','Porcine Yuc','Porcine and Bovine','Porcine_Canine_Ovine','Yorkshire Yucatan')
))))) 
AND  m03_work_product.deleted=0 
 GROUP BY m03_work_product.name
 ORDER BY m03_work_product_name ASC";

$resultWPCount = $db->query($sqlWPCount);
while ($rowWP = $db->fetchByassoc($resultWPCount))
{
    array_push($summaryData, array(
        'work_product_name' => $rowWP['m03_work_product_name'],
        'work_product_name_count' => $rowWP['count'],
    ));
}

/*Query to get Report Data*/
$sql = "SELECT IFNULL(l1.id,'') l1_id
,IFNULL(l1.name,'') l1_name
,l1_cstm.order_2_c l1_cstm_order_2_c,IFNULL(l1.time_window,'') l1_time_window
,IFNULL(l2.id,'') l2_id
,IFNULL(l2.name,'') l2_name
,l1.planned_start_datetime_1st L1_PLANNED_START_DATET23171A,l1.planned_end_datetime_1st l1_planned_end_datetime_1st,l1.planned_start_datetime_2nd L1_PLANNED_START_DATETF25DF5,l1.planned_end_datetime_2nd l1_planned_end_datetime_2nd,IFNULL(m03_work_product.name,'') m03_work_product_name

FROM m03_work_product
 INNER JOIN  m03_work_product_taskd_task_design_1_c l1_1 ON m03_work_product.id=l1_1.m03_work_product_taskd_task_design_1m03_work_product_ida AND l1_1.deleted=0

 INNER JOIN  taskd_task_design l1 ON l1.id=l1_1.m03_work_product_taskd_task_design_1taskd_task_design_idb AND l1.deleted=0
 INNER JOIN  taskd_task_design_taskd_task_design_1_c l2_1 ON l1.id=l2_1.taskd_task_design_taskd_task_design_1taskd_task_design_idb AND l2_1.deleted=0

 INNER JOIN  taskd_task_design l2 ON l2.id=l2_1.taskd_task_design_taskd_task_design_1taskd_task_design_ida AND l2.deleted=0
LEFT JOIN taskd_task_design_cstm l1_cstm ON l1.id = l1_cstm.id_c
LEFT JOIN m03_work_product_cstm m03_work_product_cstm ON m03_work_product.id = m03_work_product_cstm.id_c

 WHERE ((((((coalesce(LENGTH(l1.actual_datetime), 0) = 0)) AND (l1.type_2 = 'Actual'
) AND (l1.category <> 'Unscheduled Task' OR ( l1.category IS NULL  AND  'Unscheduled Task' IS NOT NULL )) ".$category.")) AND (((m03_work_product_cstm.test_system_c IN ('Bovine','Canine','Caprine','Micro Yucatans','Ossabaw','Ovine','PorcineGeneral','Porcine Got','Porcine','Porcine Yuc','Porcine and Bovine','Porcine_Canine_Ovine','Yorkshire Yucatan')
))))) 
AND  m03_work_product.deleted=0 
 ORDER BY m03_work_product_name ASC,l1_cstm_order_2_c ASC";

$result = $db->query($sql);
$p = $result->num_rows;
$i = 0;
//$offset1 = - 18000;
//$offset1 = - 21600; //daylight Saving
while ($row = $db->fetchByassoc($result))
{

    $temp = array();
    $temp['l1_id']          = $row['l1_id'];    
    $temp['l1_name']        = $row['l1_name'];    
    $temp['time_window']    = $row['l1_time_window'];
    $temp['l2_id']          = $row['l2_id'];    
    $temp['l2_name']        = $row['l2_name'];
    if($row['l1_cstm_order_2_c']!=""){
        $temp['order']          = $row['l1_cstm_order_2_c'];
    }else{
        $temp['order']          = "0.0";
    }
    //$temp['planned_start_datetime_1st'] = date("m/d/Y H:i", strtotime($row['L1_PLANNED_START_DATET23171A']) + $offset1);
    //$temp['planned_end_datetime_1st'] = date("m/d/Y H:i", strtotime($row['l1_planned_end_datetime_1st']) + $offset1);
    //$temp['planned_start_datetime_2nd'] = date("m/d/Y H:i", strtotime($row['L1_PLANNED_START_DATETF25DF5']) + $offset1);
    //$temp['planned_end_datetime_2nd'] = date("m/d/Y H:i", strtotime($row['l1_planned_end_datetime_2nd'] ) + $offset1);
    if($row['L1_PLANNED_START_DATET23171A']!=""){
        $offset_value = set_offset_value($row['L1_PLANNED_START_DATET23171A']);
        $temp['planned_start_datetime_1st']  = date("m/d/Y H:i", strtotime($row['L1_PLANNED_START_DATET23171A']) + $offset_value);
    }else{
        $temp['planned_start_datetime_1st'] = "";
    }
    if($row['l1_planned_end_datetime_1st']!=""){
        $offset_value = set_offset_value($row['l1_planned_end_datetime_1st']);
        $temp['planned_end_datetime_1st']  = date("m/d/Y H:i", strtotime($row['l1_planned_end_datetime_1st']) + $offset_value);
    }else{
        $temp['planned_end_datetime_1st'] = "";
    }
    if($row['L1_PLANNED_START_DATETF25DF5']!=""){
        $offset_value = set_offset_value($row['L1_PLANNED_START_DATETF25DF5']);
        $temp['planned_start_datetime_2nd']  = date("m/d/Y H:i", strtotime($row['L1_PLANNED_START_DATETF25DF5']) + $offset_value);
    }else{
        $temp['planned_start_datetime_2nd'] = "";
    }
    if($row['l1_planned_end_datetime_2nd']!=""){
        $offset_value = set_offset_value($row['l1_planned_end_datetime_2nd']);
        $temp['planned_end_datetime_2nd']  = date("m/d/Y H:i", strtotime($row['l1_planned_end_datetime_2nd']) + $offset_value);
    }else{
        $temp['planned_end_datetime_2nd'] = "";
    }
    $temp['m03_work_product_name'] = $row['m03_work_product_name'];

    $temp['summaryData'] = $summaryData[$i];
    array_push($ReportData, $temp);
    $i++;

}

/* Second Report Deceased Animals to Remove from Schedule */

    $current_date_old = date("Y-m-d");
    $current_date =  date("Y-m-d"). ' 05:59:59';
    $current_date_new = date('Y-m-d H:i:s', strtotime($current_date . ' +1 day'));
if (isset($_REQUEST['last_date_created']) && $_REQUEST['last_date_created'] != '' && $_REQUEST['submit_form'] == 1)
{
$Comm_data['last_date_created'] = $_REQUEST['last_date_created'];
$last_date_created = $_REQUEST['last_date_created'];
//$last_date_created_end	    = $last_date_created .' 23:59:00';
$date_created_start = date('Y-m-d', strtotime($current_date_new . ' - ' . $last_date_created . ' days'));
$last_date_created_start = $date_created_start . ' 06:00:00';
$Comm_data['checked'] = 'checked';
}
else
{
$Comm_data['last_date_created'] = '3';
$last_date_created = '3';
//$last_date_created_end	    = $last_date_created .' 23:59:00';
$date_created_start = date('Y-m-d', strtotime($current_date_new . ' - ' . $last_date_created . ' days'));
$last_date_created_start = $date_created_start . ' 06:00:00';
$Comm_data['checked'] = '';
}

$Commsql = "SELECT IFNULL(m06_error.id,'') primaryid
,IFNULL(m06_error.name,'') m06_error_name
,IFNULL(l1.id,'') l1_id
,IFNULL(l1.name,'') l1_name
,IFNULL(l2.id,'') l2_id
,IFNULL(l2.name,'') l2_name
,l2_cstm.usda_id_c l2_cstm_usda_id_c
FROM m06_error
 INNER JOIN  m06_error_m03_work_product_1_c l1_1 ON m06_error.id=l1_1.m06_error_m03_work_product_1m06_error_ida AND l1_1.deleted=0

 INNER JOIN  m03_work_product l1 ON l1.id=l1_1.m06_error_m03_work_product_1m03_work_product_idb AND l1.deleted=0
 INNER JOIN  m06_error_anml_animals_1_c l2_1 ON m06_error.id=l2_1.m06_error_anml_animals_1m06_error_ida AND l2_1.deleted=0

 INNER JOIN  anml_animals l2 ON l2.id=l2_1.m06_error_anml_animals_1anml_animals_idb AND l2.deleted=0
LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
LEFT JOIN m06_error_cstm m06_error_cstm ON m06_error.id = m06_error_cstm.id_c
LEFT JOIN s_species s_species1 ON s_species1.id = l2_cstm.s_species_id_c AND IFNULL(s_species1.deleted,0)=0 

 WHERE (((((m06_error_cstm.error_type_c IN ('Deceased Animal','Procedure Death','Early Term Outcome','Found Deceased','Early Death')
) AND (m06_error.date_entered >= '" . $last_date_created_start . "' AND m06_error.date_entered <= '" . $current_date_new . "'
))) AND (((( l2_cstm.s_species_id_c='5dd8cd78-1131-11ea-b102-02fb813964b8')
) OR (( l2_cstm.s_species_id_c='5dc8b366-1131-11ea-a7ff-02fb813964b8')
) OR (( l2_cstm.s_species_id_c='5d7bb91c-1131-11ea-a7bf-02fb813964b8')
) OR (( l2_cstm.s_species_id_c='5d6df0f2-1131-11ea-be0a-02fb813964b8')
) OR (( l2_cstm.s_species_id_c='5d3b5598-1131-11ea-ad04-02fb813964b8')
))))) 
AND  m06_error.deleted=0 ";

$ReportCommData = array();
$Commdata = array();
$Commresult = $db->query($Commsql);
while ($Commrow = $db->fetchByassoc($Commresult))
{
    $Commdata['primaryid'] = $Commrow['primaryid'];
    $Commdata['m06_error_name'] = $Commrow['m06_error_name'];
    $Commdata['wpid'] = $Commrow['l1_id'];
    $Commdata['wp_name'] = $Commrow['l1_name'];
    $Commdata['tsid'] = $Commrow['l2_id'];
    $Commdata['tsname'] = $Commrow['l2_name'];
    $Commdata['usdaid'] = $Commrow['l2_cstm_usda_id_c'];
    $ReportCommData[] = $Commdata;
    //array_push($ReportCommData, $Commdata);
    
}

/* Third Report Task List: Schedule Matrix Review */

// To get the count of Tasks Due Date iD
$sqlTaskCount = "SELECT DATE_FORMAT(tasks.date_due - INTERVAL 360 MINUTE,'%Y-%m-%d') tasks_day_date_due,tasks.date_due + INTERVAL 0 MINUTE tasks_date_due,count(*) count
                FROM tasks
                LEFT JOIN  users l1 ON tasks.assigned_user_id=l1.id AND l1.deleted=0

                LEFT JOIN  m03_work_product l2 ON tasks.parent_id=l2.id AND l2.deleted=0
                AND tasks.parent_type = 'M03_Work_Product'
                LEFT JOIN tasks_cstm tasks_cstm ON tasks.id = tasks_cstm.id_c
                LEFT JOIN m03_work_product_cstm l2_cstm ON l2.id = l2_cstm.id_c

                WHERE (((tasks_cstm.task_type_c = 'Schedule Matrix Review'
                ) AND (tasks.status = 'Not Completed'
                ) AND (l2_cstm.test_system_c IN ('Bovine','Canine','Caprine','Micro Yucatans','Ossabaw','Ovine','PorcineGeneral','Porcine Got','Porcine','Porcine Yuc','Porcine and Bovine','Porcine_Canine_Ovine','Yorkshire Yucatan')
                ))) 
                AND  tasks.deleted=0 
                GROUP BY DATE_FORMAT(tasks.date_due - INTERVAL 360 MINUTE,'%Y-%m-%d')
                
                ORDER BY tasks_day_date_due ASC";

$resultTaskCount = $db->query($sqlTaskCount);
$TasksData = array();
while ($rowTask = $db->fetchByassoc($resultTaskCount))
{ 
    if($rowTask['tasks_date_due'] != ''){
        $offset_value = set_offset_value($rowTask['tasks_date_due']);
        array_push($TasksData, array(
            'tasks_due_date' => date("m/d/Y", strtotime($rowTask['tasks_date_due']) + $offset_value),            
            'tasks_count' => $rowTask['count'],
        ));
    }
}

$Tasksql = "SELECT IFNULL(tasks.id,'') primaryid
            ,IFNULL(tasks.name,'') tasks_name
            ,tasks.date_due tasks_date_due,IFNULL(l1.id,'') l1_id
            ,IFNULL(l1.first_name,'') l1_first_name
            ,IFNULL(l1.last_name,'') l1_last_name
            ,IFNULL(l1.title,'') l1_title
            ,DATE_FORMAT(tasks.date_due - INTERVAL 360 MINUTE,'%Y-%m-%d') tasks_day_date_due
            FROM tasks
            LEFT JOIN  users l1 ON tasks.assigned_user_id=l1.id AND l1.deleted=0

            LEFT JOIN  m03_work_product l2 ON tasks.parent_id=l2.id AND l2.deleted=0
            AND tasks.parent_type = 'M03_Work_Product'
            LEFT JOIN tasks_cstm tasks_cstm ON tasks.id = tasks_cstm.id_c
            LEFT JOIN m03_work_product_cstm l2_cstm ON l2.id = l2_cstm.id_c

            WHERE (((tasks_cstm.task_type_c = 'Schedule Matrix Review'
            ) AND (tasks.status = 'Not Completed'
            ) AND (l2_cstm.test_system_c IN ('Bovine','Canine','Caprine','Micro Yucatans','Ossabaw','Ovine','PorcineGeneral','Porcine Got','Porcine','Porcine Yuc','Porcine and Bovine','Porcine_Canine_Ovine','Yorkshire Yucatan')
            ))) 
            AND  tasks.deleted=0 
            ORDER BY tasks_day_date_due ASC";

$ReportTaskData = array();
$Taskdata = array();
$Taskresult = $db->query($Tasksql);
while ($Taskrow = $db->fetchByassoc($Taskresult))
{
    $Taskdata['primaryid'] = $Taskrow['primaryid'];
    $Taskdata['tasks_name'] = $Taskrow['tasks_name'];
    $Taskdata['asstoid'] = $Taskrow['l1_id'];
    $Taskdata['asstof_name'] = $Taskrow['l1_first_name'];
    $Taskdata['asstol_name'] = $Taskrow['l1_last_name'];
    $Taskdata['tasks_date1'] = $Taskrow['tasks_day_date_due'];
    //$Taskdata['tasks_day_date_due'] = date("m/d/Y H:i", strtotime($Taskrow['tasks_day_date_due']) + $offset1);
    if($Taskrow['tasks_date_due']!=""){
        $offset_value = set_offset_value($Taskrow['tasks_date_due']);
        $Taskdata['tasks_date_due_time1']  = date("m/d/Y", strtotime($Taskrow['tasks_date_due']) + $offset_value);
        $Taskdata['tasks_date_due_time']  = date("m/d/Y H:i", strtotime($Taskrow['tasks_date_due']) + $offset_value);
        
    }else{
        $Taskdata['tasks_day_date_due'] = "";
    }
    $ReportTaskData[] = $Taskdata;
    //array_push($ReportTaskData, $Taskdata);
    
}
//$ReportData = array_merge($ReportCommData, $ReportData);
$ot = "";
if ($ORDER_TYPE == "ASC")
{
    $ot = SORT_ASC;
}
else
{
    $ot = SORT_DESC;
}
array_multisort(array_column($ReportData, $ORDER_COLUMN) , $ot, $ReportData);
array_multisort(array_column($ReportCommData, $ORDER_COLUMN) , $ot, $ReportCommData);
array_multisort(array_column($ReportTaskData, $ORDER_COLUMN) , $ot, $ReportTaskData);

// echo '<pre>';
// print_r($TasksData);
// echo 'Related////////////////////////////';
// print_r($TaskData);
// $ss->assign('REPORTDATA',$ReportData);
$ss->assign('last_date_created', $last_date_created);
$ss->assign('REPORTDATA2', $summaryData);
$ss->assign('ReportData', $ReportData);
$ss->assign('ReportCommData', $ReportCommData);
$ss->assign('REPORTDATATASK', $TasksData);
$ss->assign('ReportTaskData', $ReportTaskData);
$ss->assign('SITE_URL', $GLOBALS['sugar_config']['site_url']);
$ss->assign('ORDER_TYPE', ($ORDER_TYPE == "ASC") ? "DESC" : "ASC");
$ss->assign('ORDER_COLUMN', $ORDER_COLUMN);
$ss->assign('COMMDATA', $Comm_data);
$ss->assign('category_status', $category_status); 
$ss->assign('category_filter', $category_filter); 
$ss->assign('multicatfilter', $multicategoryfilter);
$html = $ss->fetch("custom/modules/Reports/tpls/DailySchedulingNeeds.tpl");
echo $html;

function set_offset_value($offset_val)
{
        $month = date("M",strtotime($offset_val));
        $date  = date("d",strtotime($offset_val));
        if ($month == 'Jan' || $month == 'Feb' || $month == 'Dec'){
             $offset1 = - 21600;
        }else if (($month == 'Mar' && $date <= '13') || ($month == 'Nov' && $date >= '06')){
             $offset1 = - 21600;
        }else {
             $offset1 = - 18000;  
        } 
        return $offset1; 
}