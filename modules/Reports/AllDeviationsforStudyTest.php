<?php
//AllDeviationsforStudy
require_once('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config,$app_list_strings,$db,$current_user;
$ss = new Sugar_Smarty();
$subtype_list = $app_list_strings['error_type_list'];
//error_type_list
// $work_product_id = 'cd80da16-d55a-11e9-ba5e-06eddc549468';
if(isset($_REQUEST['work_product_id']) && $_REQUEST['work_product_id']!=''){
	$work_product_id = $_REQUEST['work_product_id'];
	$wp_data['id'] = $work_product_id;
	$wp_data['name'] = $_REQUEST['work_product_name'];
}else{
	$work_product_id = '086c1cee-0c04-11ec-b7c1-029395602a62';
	$wp_data['id'] = $work_product_id;
	$wp_data['name'] = 'DHA463-HE70a';
}
$wp_data['current_user_id'] = $current_user->id;
$ReportData = array();
$ReportData2 = array();
$ReportSubtypeData = array();
$RelatedCommData = array();
$test_system_ids = array();
$usda_ids = array();
// To get the count order by subtype c
$sqlSubTypeCount="SELECT l1_cstm.subtype_c l1_cstm_subtype_c,IFNULL(m03_work_product.name,'') m03_work_product_name
,count(*) count,l1.name,
ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id
FROM m03_work_product
LEFT JOIN  m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id=l1_1.m06_error_m03_work_product_1m03_work_product_idb AND l1_1.deleted=0

LEFT JOIN  m06_error l1 ON l1.id=l1_1.m06_error_m03_work_product_1m06_error_ida AND l1.deleted=0
LEFT JOIN  m06_error_anml_animals_1_c l2_1 ON l1.id=l2_1.m06_error_anml_animals_1m06_error_ida AND l2_1.deleted=0

LEFT JOIN  anml_animals l2 ON l2.id=l2_1.m06_error_anml_animals_1anml_animals_idb AND l2.deleted=0
LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb AND ReletedComm.deleted=0

WHERE (((((IFNULL(l1_cstm.error_classification_c,'') IN ('Error','Impactful Deviation','Non Impactful Deviation')
) AND (m03_work_product.id = '$work_product_id'
))) AND (((IFNULL(l1_cstm.error_category_c,'') <> 'Internal Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Internal Feedback' IS NOT NULL)) AND (IFNULL(l1_cstm.error_type_c,'') <> 'Facility' OR (IFNULL(l1_cstm.error_type_c,'') IS NULL AND 'Facility' IS NOT NULL)) AND (IFNULL(l1_cstm.error_category_c,'') <> 'Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Feedback' IS NOT NULL)))))) 
AND  m03_work_product.deleted=0 
GROUP BY l1.name,l1_cstm.subtype_c
,m03_work_product.name
 ORDER BY l1_cstm_subtype_c ASC,m03_work_product_name ASC";

$resultSubtypeCount = $db->query($sqlSubTypeCount);
$hash = array();
echo 'Query: =>'.$sqlSubTypeCount.'<br/>';
echo'<pre>';
$ReportSubtypeDataCount = array();
while ($rowSubtype = $db->fetchByassoc($resultSubtypeCount))
{

    $hash_key = str_replace('^', '', $rowSubtype['l1_cstm_subtype_c']);
    $hashArr = explode(",", $hash_key);
    /*697 Bug : to fix the issue of multiple subtype record missing in report */
    $subtypeValueArr = array();
	$subTypeArr = explode(",", str_replace('^', '', $rowSubtype['l1_cstm_subtype_c']));
	
    echo 'subTypeArr ==> ';print_r($subTypeArr);

    foreach ($subTypeArr as $subtype1)
    {
        $subtypeValueArr[] = $app_list_strings['error_type_list'][$subtype1];
        $subtypeValue = implode(",", $subtypeValueArr);
    }

    foreach ($hashArr as $hashArrval)
    {
        $hash_key = $app_list_strings['error_type_list'][$hashArrval];
        if (!array_key_exists($subtypeValue, $hash))
        {
            $hash[$subtypeValue] = sizeof($ReportSubtypeDataCount);
			//$subtypeValue = $app_list_strings['error_type_list'][str_replace('^','',$rowSubtype['l1_cstm_subtype_c'])];
			echo '<br/>subtypeValue ==> '.$subtypeValue;
            array_push($ReportSubtypeDataCount, array(
                'm03_work_product_name' => $rowSubtype['m03_work_product_name'],
                'l1_cstm_subtype_c' => $subtypeValue,
                'name' => $rowSubtype['name'],
                'rel_comm_id' => $rowSubtype['rel_comm_id'],
                'count' => 0,
            ));
        }
    }
    $ReportSubtypeDataCount[$hash[$subtypeValue]]['count'] += 1;
}
/* #697 : Remove the duplicates array from subtype count array */
$unique = $ReportSubtypeDataCount;
$duplicated = array();

foreach ($unique as $k => $v)
{
    if (($kt = array_search($v, $unique)) !== false and $k != $kt)
    {
        unset($unique[$kt]);
        $duplicated[] = $v;
    }
}
$ReportSubtypeDataCount = $unique;
echo '<br/><br/><br/>';
echo 'ReportSubtypeDataCount ==> '; print_r($ReportSubtypeDataCount);
/*Query to get Report Data*/ 
$sql =
  "SELECT
	IFNULL(l1.id, '') l1_id,
	IFNULL(l1. NAME, '') l1_name,
	ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id,
	IFNULL(m03_work_product.id, '') m03_work_product_id,
	l2_cstm.animal_id_c l2_cstm_animal_id_c,
	l2_cstm.usda_id_c l2_cstm_usda_id_c,
	CONCAT (l2_cstm.animal_id_c,'-',l2_cstm.usda_id_c) AS TestUsda,
	l1_cstm.date_error_occurred_c L1_CSTM_DATE_ERROR_OCC6BD1CE,
	l1_cstm.expected_event_c l1_cstm_expected_event_c,
	l1_cstm.actual_event_c l1_cstm_actual_event_c,
	l1_cstm.resolution_c l1_cstm_resolution_c,
	IFNULL(
		l1_cstm.corrective_action_1_c,
		''
	) L1_CSTM_CORRECTIVE_ACT674D79,
	l1_cstm.rationale_for_no_ca_c L1_CSTM_RATIONALE_FOR_F48A84,
	l1_cstm.subtype_c l1_cstm_subtype_c,
	l1_cstm.impactful_dd_c impactful_dd_c,
	IFNULL(m03_work_product. NAME, '') m03_work_product_name
  FROM
  	m03_work_product
  LEFT JOIN m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id = l1_1.m06_error_m03_work_product_1m03_work_product_idb
  AND l1_1.deleted = 0
  LEFT JOIN m06_error l1 ON l1.id = l1_1.m06_error_m03_work_product_1m06_error_ida
  AND l1.deleted = 0
  LEFT JOIN m06_error_anml_animals_1_c l2_1 ON l1.id = l2_1.m06_error_anml_animals_1m06_error_ida
  AND l2_1.deleted = 0
  LEFT JOIN anml_animals l2 ON l2.id = l2_1.m06_error_anml_animals_1anml_animals_idb
  AND l2.deleted = 0
  LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
  LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
  LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb
  AND ReletedComm.deleted=0
  WHERE (((((IFNULL(l1_cstm.error_classification_c,'') IN ('Error','Impactful Deviation','Non Impactful Deviation')
  ) AND (m03_work_product.id = '$work_product_id'
  ))) AND (((IFNULL(l1_cstm.error_category_c,'') <> 'Internal Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Internal Feedback' IS NOT NULL)) AND (IFNULL(l1_cstm.error_type_c,'') <> 'Facility' OR (IFNULL(l1_cstm.error_type_c,'') IS NULL AND 'Facility' IS NOT NULL)) AND (IFNULL(l1_cstm.error_category_c,'') <> 'Feedback' OR (IFNULL(l1_cstm.error_category_c,'') IS NULL AND 'Feedback' IS NOT NULL)))))) 
  AND  m03_work_product.deleted=0 
  ORDER BY
  L1_CSTM_DATE_ERROR_OCC6BD1CE ASC,
  l2_cstm_animal_id_c ASC,
	l1_cstm_subtype_c ASC,
	m03_work_product_name ASC";
$result = $db->query($sql);
$commListArr = array();
while($row = $db->fetchByassoc($result)){
	$temp = array();
	$commListArr[]  =$row['l1_id'];
	$temp['communication_id'] = $row['l1_id'];
	$temp['communication_name'] = $row['l1_name'];
	$temp['work_product_id'] = $row['m03_work_product_id'];
	$temp['work_product_name'] = $row['m03_work_product_name'];
	$temp['test_system'] = $row['l2_cstm_animal_id_c'];
	$temp['usda_id'] = $row['l2_cstm_usda_id_c'];
	$temp['rel_comm_id'] = $row['rel_comm_id'];
	$temp['impactful_dd_c'] = $row['impactful_dd_c'];

	if($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']!=""){
		$temp['date_occurred'] = date("m/d/Y", strtotime($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']));
	}else{
		$temp['date_occurred'] = "";
	}
	$temp['expected_event'] = $row['l1_cstm_expected_event_c'];
	$temp['actual_event'] = $row['l1_cstm_actual_event_c'];
	$temp['sd_assessment'] = $row['l1_cstm_resolution_c'];
	$subtypeValueArr = array();
    $subTypeArr = explode(",", str_replace('^', '', $row['l1_cstm_subtype_c']));

    foreach ($subTypeArr as $subtype1)
    {
        $subtypeValueArr[] = $app_list_strings['error_type_list'][$subtype1];
    }
    $subtypeValue = implode(",", $subtypeValueArr);
    //$temp['subtype'] = str_replace('^','',$row['l1_cstm_subtype_c']);
    $temp['subtype'] = $subtypeValue; //$app_list_strings['error_type_list'][$temp['subtype']];
    $ReportSubtypeData[$temp['subtype']][$temp['communication_name']] = $temp;
	$temp['corrective_action'] = $row['L1_CSTM_CORRECTIVE_ACT674D79'];
	$temp['corrective_action_details'] = $row['L1_CSTM_RATIONALE_FOR_F48A84'];
	$testSystemUsdaID = $temp['test_system'];
	if($temp['usda_id']!="")
		$testSystemUsdaID .= "-".$temp['usda_id'];	
	$test_system_ids[$temp['communication_name']][] = $testSystemUsdaID;
	//$ReportData[$temp['communication_name']] = $temp;
	$ReportSubtypeData[$temp['subtype']][$temp['communication_name']]= $temp;
	if($temp['rel_comm_id']!="")
	{
		$RelatedCommData[$temp['rel_comm_id']][$temp['communication_name']]= $temp;
	}
	//$test_system_ids[$temp['communication_name']][] = $temp['test_system'];
	//$usda_ids[$temp['communication_name']][] = $temp['usda_id'];
}	
foreach($RelatedCommData as $keyrel => $valrel){
	foreach($valrel as $keyrel1 => $valrel1){
		$RelatedCommData[$keyrel][$keyrel1]['test_system'] = implode(', ',$test_system_ids[$keyrel1]);
	}
}
foreach($ReportSubtypeData as $key => $val){
	foreach($val as $key1 => $val1){
		$yes = "";
		$hiderow = "";
		/* To mark the related record and info to main array based on the all the related records */
		foreach($RelatedCommData as $relkey => $relval){
			foreach($relval as $relkey1 => $relateddataval){
				/**fix bug 1357 : 11 june 2021 */
				$ReportSubtypeData[$key][$key1]['is_hide'] = $hiderow;
				$ReportSubtypeData[$key][$key1]['is_related'] = $yes;
				if(($val1['subtype'] == $relateddataval['subtype']) && ($val1['communication_id'] == $relateddataval['rel_comm_id']))
				{
					$yes = "yes";
                    $ReportSubtypeData[$key][$key1]['is_related'] = $yes;
				}
				/*hide the main record row which are having the related comm id value */
				if (in_array($val1['rel_comm_id'],$commListArr) && ($val1['subtype'] == $relateddataval['subtype']) && ($val1['rel_comm_id'] == $relateddataval['rel_comm_id'] ))
                {
					$hiderow = 'yes';
                    $ReportSubtypeData[$key][$key1]['is_hide'] = $hiderow;
				}
				
			}
		}
		$ReportSubtypeData[$key][$key1]['test_system'] = implode(', ',$test_system_ids[$key1]);
		$ReportSubtypeData[$key][$key1]['usda_id'] = implode(', ',$usda_ids[$key1]);
	}
}
echo '<pre>';
echo 'ReportData ==><br/>';
print_r($ReportData);
echo '<br/><br/><br/>';
echo 'ReportSubtypeData ==> '; print_r($ReportSubtypeData);
echo '<br/><br/><br/>';
echo 'ReportSubtypeDataCount ==> '; print_r($ReportSubtypeDataCount);
echo '<br/><br/><br/>';
die(); 
//$ss->assign('REPORTDATA',$ReportData);
/*$ss->assign('REPORTDATA2',$ReportSubtypeDataCount);
$ss->assign('ReportSubtypeData',$ReportSubtypeData);
$ss->assign('RelatedCommData',$RelatedCommData);
$ss->assign('WPDATA',$wp_data);
$html = $ss->fetch("custom/modules/Reports/tpls/AllDeviationsforStudy.tpl");
echo $html;*/
?>