{literal}
<style>
	.select2-container{
		width : 200px;
	}
	#report_table {
	  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	  table-layout: fixed;
	}
	#report_table th {
    font-weight: bold;
    text-align: left;
    padding: 4px 5px 4px 5px;
    border: 1px solid;
    background: #fff;
    color: #666;
    font-size: 13px;
   }
 	#report_table td {
	border-color: #ccc;
    background: white;
    padding: 4px 5px;
	}
	#report_table td.custom {
	border-color: #ccc;
    background: white;
    padding: 4px 5px;
    border: 1px solid;
	}

	#report_table tr:nth-child(even){background-color: #fff;}
	#report_table tr:last-child td {border-bottom:1px solid;}
	#report_table tr:hover {background-color: #fff;}
	#report_table td.cus_bg {background:#F7CAAC !important; color:#000;}
	.wpfilter {border: 0; background: #F7F8F9; padding-left: 6px;width:100px !important;}
	#report_table td.is_related {border-color: #ccc;padding: 4px 5px;background: #fff;
    border:0px;}
	#report_table td.is_related_first {padding: 4px 5px;background: #fff;border-left:1px solid #000;}
	#report_table td.is_related_last {padding: 4px 5px;background: #fff;border-right:1px solid #000;}
	#report_table td.is_related_parent {border-top:1px solid #000;border-color: #000;}
	.reportGroupViewTable {
    background: LightGrey;
    border-top: 1px solid #000;
    border-left: 1px solid #000;
}

</style>
<script>
$(document).ready(function(){
	/*$('#ApplyBtn').click(function(){
		work_product_id = $('#work_product_id').val();
		//#bwc/index.php?module=Reports&action=AllDeviationsforStudy
		if(work_product_id != ''){
			window.location.href = "#bwc/index.php?module=Reports&action=AllDeviationsforStudy&work_product_id="+work_product_id;
		}
		return true;
	});*/
	
		/* $( "#work_product_name" ).keypress(function() {
 	work_product_id = $('#work_product_id').val('');
});*/
$('#work_product_name:text').on('input', function(){ 
	work_product_id = $('#work_product_id').val(''); 
});
	$('#ApplyBtn').click(function(){
	work_product_id = $('#work_product_id').val();
	if(work_product_id != ''){
		window.location.href = "#bwc/index.php?module=Reports&action=AllDeviationsforStudy";
		return true;
	} 
	else if(work_product_id == '')
	{
		alert('"Work Products: APS Work Product ID" missing input value');
		return false;
	}
	
});
	$('#SelectBtn').click(function(){
		open_popup(
			"M03_Work_Product", 
			600, 
			400, 
			"", 
			true, 
			false, 
			{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"work_product_id","name":"work_product_name"}}, 
			"single", 
			true
		);
	});
});
function ExportToWord(report,w_product_id,user_id,wp_name){
	//window.open('index.php?module=Reports&action=ExportToWord&report_name='+report+'&wpid='+w_product_id+'');
	host= 'live';
	window.open(
  'http://deploytest_01.apsflightlog.com/ExportApp/ExportToWord?host='+host+'&user_id='+user_id+'&report='+report+'&work_product_id='+w_product_id+'&wp_name='+wp_name,
  '_blank' );
}
function ExportToPDF(siteUrl,wpID){
	window.open(siteUrl+'/index.php?module=Reports&action=sugarpdf&sugarpdf=alladverseeventsforstudyview&wpid='+wpID,'_blank');
}
</script>
{/literal}
<!--<table id="report_table" cellpadding="0" cellspacing="0" border="0" width="100%" >-->

<h2>All Adverse Events for a Study</h2>
<br>
<form id="EditView" name="EditView" method="post" action="#bwc/index.php?module=Reports&action=AllAdverseEventsforStudy">
<table>
	<tbody>
		<tr>
			<td>
				<input type="hidden" name="work_product_id" id="work_product_id" value="{$WPDATA.id}">
				Work Product Filter: <input type="text" name="work_product_name" id="work_product_name" class="sqsEnabled yui-ac-input wpfilter" autocomplete="off" value="{$WPDATA.name}" readonly>
			</td>
			<td>
				<input title="Select" type="button" value="Select Work Product" class="button_name" id="SelectBtn">
			</td>
		</tr>
		<tr>
			<td>
				<input title="Apply" type="submit" value="Apply" class="button_name" id="ApplyBtn">
			</td>
		</tr>
		<tr>
			<td>
				<input title="Export" type="button" value="Export To Word" class="button_name" id="ExportWord" onclick="ExportToWord('AllAdverseEventsforStudy','{$WPDATA.id}','{$WPDATA.current_user_id}','{$WPDATA.name}');">
			&nbsp;&nbsp;&nbsp;&nbsp;<input title="Export" type="button" value="Export To PDF" class="button_name" id="ExportPDF" onclick="ExportToPDF('{$SITE_URL}','{$WPDATA.id}')">
			</td>
		</tr>
	</tbody>
</table>
</form>
<br>

 {if is_array($REPORTDATA2) && count($REPORTDATA2) > 0}

{foreach from=$REPORTDATA2 key=index item=line_item2}
 <table id="combo_summary_div_0" width="100%" border="0" cellpadding="0" cellspacing="0" class="reportGroupViewTable">
	<tbody><tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportGroup1ByTable">
				<tbody><tr height="20">				
				  <th align="left" id="Id_0" name="Id_0" class="reportGroup1ByTableEvenListRowS1" valign="middle" nowrap="">
				  &nbsp;Classification = 
				  {if $line_item2.l1_cstm_error_classification_c == ""}
				  None
				  {else}
				  {$line_item2.l1_cstm_error_classification_c}{/if}
				  , Count = {$line_item2.count}</th>
				</tr>
			</tbody></table>
		</td>
	</tr>

	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportGroupNByTable">
				<tbody><tr height="20" class="reportGroupNByTableEvenListRowS1">
				  <td align="left" id="Id_0_0" name="Id_0_0" width="30%" nowrap="" class="reportGroupNByTableEvenListRowS1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APS Work Product ID = {$line_item2.m03_work_product_name}, Count = {$line_item2.count}</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>

<table id="report_table">
  <tr>
	<th style="width:10%">Communication ID</th>
	<th style="width:15%">Test System ID - USDA ID</th>
	<th style="width:10%">Date Occurred</th>
	<th style="width:20%">Actual Event</th>
	<th style="width:15%">Impact Assessment</th>
	<th style="width:15%">Corrective Action Within the Study</th>
	<th style="width:15%">Corrective Action Details</th>
  </tr>
  {if is_array($ReportClassificationData) && count($ReportClassificationData) > 0}
  {foreach from=$ReportClassificationData key=index1 item=line_item1}

   
  {foreach from=$line_item1 key=index2 item=line_item}
  {if $index1 == $line_item2.l1_cstm_error_classification_c }
	{if $line_item.is_related == 'yes'}
		<tr>
			
				<td class="is_related_first is_related_parent">{$line_item.communication_name}</td>
				<td class="is_related is_related_parent" style="word-wrap: break-word">{if $line_item.test_system == ""} Not Applicable{else}{$line_item.test_system}{/if}</td>
				<td class="is_related is_related_parent">{$line_item.date_occurred}</td>
				<td class="is_related is_related_parent">{$line_item.actual_event}</td>
				{if $line_item.impactful_dd_c == '' || $line_item.impactful_dd_c == 'Pending'}
				 <td class="is_related cus_bg is_related_parent">{$line_item.impactful_dd_c}<br/><br/>{$line_item.sd_assessment}</td>
				{else}
				<td class="is_related is_related_parent">{$line_item.impactful_dd_c}<br/><br/>{$line_item.sd_assessment}</td>
				{/if}
				<td class="is_related is_related_parent">{$line_item.corrective_action}</td>
				<td class="is_related_last is_related_parent">{$line_item.corrective_action_details}</td>
			
			
		</tr>
	{else}
		<tr {if $line_item.is_hide == 'yes'} style="display:none;" {/if}>
			
				<td class="custom">{$line_item.communication_name}</td>
				<td class="custom" style="word-wrap: break-word">{if $line_item.test_system == ""} Not Applicable{else}{$line_item.test_system}{/if}</td>
				<td class="custom">{$line_item.date_occurred}</td>
				<td class="custom">{$line_item.actual_event}</td>
				{if $line_item.impactful_dd_c == '' || $line_item.impactful_dd_c == 'Pending needs further assessment before finalizing'}
				 <td class="custom cus_bg">{$line_item.impactful_dd_c}<br/><br/>{$line_item.sd_assessment}</td>
			     {else}
				 <td class="custom">{$line_item.impactful_dd_c}<br/><br/>{$line_item.sd_assessment}</td>
				{/if}
				<td class="custom">{$line_item.corrective_action}</td>
				<td class="custom">{$line_item.corrective_action_details}</td>
			
		</tr>
	{/if}
		{foreach from=$RelatedCommData key=rel_index item=rel_line_item}
		 {foreach from=$rel_line_item key=index4 item=rel_line_item_rec}

		  {if $line_item.communication_id == $rel_line_item_rec.rel_comm_id && $line_item.classification == $rel_line_item_rec.classification  } 
			<tr>
				<td class="is_related_first">{$rel_line_item_rec.communication_name}</td>
					<td class="is_related" style="word-wrap: break-word">{if $rel_line_item_rec.test_system == ""} Not Applicable{else}{$rel_line_item_rec.test_system}{/if}</td>
					<td class="is_related">{$rel_line_item_rec.date_occurred}</td>
					<td class="is_related">{$rel_line_item_rec.actual_event}</td>
					{if $rel_line_item_rec.impactful_dd_c == '' || $rel_line_item_rec.impactful_dd_c == 'Pending needs further assessment before finalizing'}
					 <td class="is_related cus_bg">{$rel_line_item_rec.impactful_dd_c}<br/><br/>{$rel_line_item_rec.sd_assessment}</td>
					 {else}
					 <td class="is_related">{$rel_line_item_rec.impactful_dd_c}<br/><br/>{$rel_line_item_rec.sd_assessment}</td>
					 {/if}
					 <td class="is_related">{$rel_line_item_rec.corrective_action}</td>
					<td class="is_related_last">{$rel_line_item_rec.corrective_action_details}</td>
				
					
				
			</tr>
		{/if}
		 {/foreach}
		{/foreach}

	{/if}
	{/foreach}
  {/foreach}
   
  {else}
  <tr>
  <td colspan="7" style="text-align: center;">No Data Available</td>
  </tr>

{/if}
</table>


{/foreach}

 
 {else}
  <table id="report_table">
  <td   style="text-align: center;">No Data Available</td>
  </tr>
  </table>

{/if}