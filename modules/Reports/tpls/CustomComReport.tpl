{literal}
<style>
    .palered{
       background:#ffcccb !important;
    }
    .sortColumn:hover{
        cursor: pointer;
        text-decoration:underline;
    }
</style>
<script>
    function filterTypeChanged(code) {
        var fiterVal = $('#filterOptionDropdown' + code).val();
        if (code == 0) {
            if (!(fiterVal == "empty" || fiterVal == "not_empty")) {
                getDropdownOptions("error_category_list", "typeDropdown" + code);
                $("#typeDropdown" + code).show();
            } else {
                $("#typeDropdown" + code).hide();
            }
        }
        if (code ==1) {
            if (!(fiterVal == "empty" || fiterVal == "not_empty")) {
                $("#work_product_code_name_1").show();
                $('#SelectWorkProductCode1').show();
            } else {
                $("#work_product_code_name_1").hide();
                $('#SelectWorkProductCode1').hide();
            }
        }if (code ==2) {
            if (!(fiterVal == "empty" || fiterVal == "not_empty")) {
                $("#work_product_code_name_2").show();
                $('#SelectWorkProductCode2').show();
            } else {
                $("#work_product_code_name_2").hide();
                $('#SelectWorkProductCode2').hide();
            }
        }if (code ==3) {
            if (!(fiterVal == "empty" || fiterVal == "not_empty")) {
                $("#work_product_code_name_3").show();
                $('#SelectWorkProductCode3').show();
            } else {
                $("#work_product_code_name_3").hide();
                $('#SelectWorkProductCode3').hide();
            }
        }
        if (code == 4) {
            if (!(fiterVal == "empty" || fiterVal == "not_empty")) {
                $("#cwpa_1").show();
            } else {
                $("#cwpa_1").hide();
            }
        }
        if (code == 5) {
            if (!(fiterVal == "empty" || fiterVal == "not_empty")) {
                $("#cwpa_2").show();
            } else {
                $("#cwpa_2").hide();
            }
        }if (code == 6) {
            if (!(fiterVal == "empty" || fiterVal == "not_empty")) {
                $("#cwpa_3").show();
            } else {
                $("#cwpa_3").hide();
            }
        }if (code == 7) {
            if (!(fiterVal == "empty" || fiterVal == "not_empty")) {
                $("#cwpa_4").show();
            } else {
                $("#cwpa_4").hide();
            }
        }
    }

    function getDropdownOptions(list, id) {
        var typeOptions = SUGAR.language.languages.app_list_strings[list];
        var type_dd = "";
        for (var key in typeOptions) {
            // Here "key" gives the key of options and "typeOptions[key]" gives the value of it.
            type_dd += '<option value="' + key + '">' + typeOptions[key] + '</option>';
        }
        $('#' + id).html(type_dd);
        $('#' + id).show();
    }

    $(document).ready(function ($) {
       $("#btnReset").on("click",function(){
           
         $("#filterOptionDropdown0 option[value='one_of']").prop("selected", true);
         $("#typeDropdown0 option").prop("selected", false);
         $("#typeDropdown0 option[value='Failed Sham']").prop("selected", true);
         $("#typeDropdown0 option[value='Passed Sham']").prop("selected", true);
         $("#typeDropdown0 option[value='Failed Pyrogen']").prop("selected", true);
         $("#typeDropdown0 option[value='Passed Pyrogen']").prop("selected", true);

         $("#filterOptionDropdown1 option[value='is']").prop("selected", true);
         $("#work_product_code_id_1").val("c81c2577-bd7c-16e3-1248-5785ac111a73");
         $("#work_product_code_name_1").val("ST01");

         $("#filterOptionDropdown2 option[value='is']").prop("selected", true);
         $("#work_product_code_id_2").val("426d29a9-0810-54b0-1834-5788f6505070");
         $("#work_product_code_name_2").val("ST01a");

         $("#filterOptionDropdown3 option[value='is']").prop("selected", true);
         $("#work_product_code_id_3").val("00535bde-96f2-11e7-a240-02feb3423f0d");
         $("#work_product_code_name_3").val("ST09");


         $("#filterOptionDropdown4 option[value='equals']").prop("selected", true);
         $("#cwpa_1").val("APS177-AH15");
         $("#filterOptionDropdown5 option[value='contains']").prop("selected", true);
         $("#cwpa_2").val("ST01");
         $("#filterOptionDropdown6 option[value='contains']").prop("selected", true);
         $("#cwpa_3").val("ST01a");
         $("#filterOptionDropdown7 option[value='contains']").prop("selected", true);
         $("#cwpa_4").val("ST09");
         

$("#EditView").submit();
       });


       $(".sortColumn").click(function(){
          var currentClickedColumn=$(this).attr("sortcolumn");
          var previousSortedColumn=$("#order_column").val();
          $("#order_column").val(currentClickedColumn);
          if(currentClickedColumn!=previousSortedColumn){
                $("#order_type").val("ASC");
             
          }
          $("#EditView").submit();
       });

        getDropdownOptions("error_category_list", "typeDropdown0")

        $('#ApplyBtn').click(function () {
                window.location.href = "#bwc/index.php?module=Reports&action=CustomComReport";
                return true; 
        });

        
        $('.typecellfp').each(function() {
            var cellText = $(this).html();    
            if(cellText.trim()=="Failed Pyrogen"){
               $(this).addClass("palered");
            }
         });
      
        $('#SelectWorkProductCode1').click(function () {
            open_popup(
                "M03_Work_Product_Code",
                600,
                400,
                "",
                true,
                false,
                {
                    "call_back_function": "set_return",
                    "form_name": "EditView",
                    "field_to_name_array": {"id": "work_product_code_id_1", "name": "work_product_code_name_1"}
                },
                "single",
                true
            );
        });
        $('#SelectWorkProductCode2').click(function () {
            open_popup(
                "M03_Work_Product_Code",
                600,
                400,
                "",
                true,
                false,
                {
                    "call_back_function": "set_return",
                    "form_name": "EditView",
                    "field_to_name_array": {"id": "work_product_code_id_2", "name": "work_product_code_name_2"}
                },
                "single",
                true
            );
        });
        $('#SelectWorkProductCode3').click(function () {
            open_popup(
                "M03_Work_Product_Code",
                600,
                400,
                "",
                true,
                false,
                {
                    "call_back_function": "set_return",
                    "form_name": "EditView",
                    "field_to_name_array": {"id": "work_product_code_id_3", "name": "work_product_code_name_3"}
                },
                "single",
                true
            );
        });

        var type_filter = '{/literal}{$WPDATA.type_filter}{literal}';
        var type_dropdown = '{/literal}{$WPDATA.type_dropdown}{literal}';

        var wpc_1_filter = '{/literal}{$WPDATA.wpc_1_filter}{literal}';
        var wpc_1_id = '{/literal}{$WPDATA.wpc_1_id}{literal}';
        var wpc_1_name = '{/literal}{$WPDATA.wpc_1_name}{literal}';

        var wpc_2_filter = '{/literal}{$WPDATA.wpc_2_filter}{literal}';
        var wpc_2_id = '{/literal}{$WPDATA.wpc_2_id}{literal}';
        var wpc_2_name = '{/literal}{$WPDATA.wpc_2_name}{literal}';

        var wpc_3_filter = '{/literal}{$WPDATA.wpc_3_filter}{literal}';
        var wpc_3_id = '{/literal}{$WPDATA.wpc_3_id}{literal}';
        var wpc_3_name = '{/literal}{$WPDATA.wpc_3_name}{literal}';

        var cwpa_1_filter = '{/literal}{$WPDATA.cwpa_1_filter}{literal}';
        var cwpa_1_value = '{/literal}{$WPDATA.cwpa_1_value}{literal}';
        var cwpa_2_filter = '{/literal}{$WPDATA.cwpa_2_filter}{literal}';
        var cwpa_2_value = '{/literal}{$WPDATA.cwpa_2_value}{literal}';
        var cwpa_3_filter = '{/literal}{$WPDATA.cwpa_3_filter}{literal}';
        var cwpa_3_value = '{/literal}{$WPDATA.cwpa_3_value}{literal}';
        var cwpa_4_filter = '{/literal}{$WPDATA.cwpa_4_filter}{literal}';
        var cwpa_4_value = '{/literal}{$WPDATA.cwpa_4_value}{literal}';
        
   
        $("#filterOptionDropdown0").val('{/literal}{$WPDATA.type_filter}{literal}');
        if (!(type_filter == "empty" || type_filter == "not_empty")) {

            var values='{/literal}{$WPDATA.type_dropdown}{literal}';
            $.each(values.split(","), function(i,e){
               $("#typeDropdown0 option[value='" + e + "']").prop("selected", true);
            });
            $('#typeDropdown0').show();

        } else {
            $('#typeDropdown0').hide();
        }


        $("#filterOptionDropdown1").val('{/literal}{$WPDATA.wpc_1_filter}{literal}');
        if (!(wpc_1_filter == "empty" || wpc_1_filter == "not_empty")) {
            $('#work_product_code_id_1').val('{/literal}{$WPDATA.wpc_1_id}{literal}');
            $('#work_product_code_name_1').val('{/literal}{$WPDATA.wpc_1_name}{literal}');
            $('#SelectWorkProductCode1').show();
            $('#work_product_code_name_1').show();
        } else {
            $('#work_product_code_name_1').hide();
        }

        $("#filterOptionDropdown2").val('{/literal}{$WPDATA.wpc_2_filter}{literal}');
        if (!(wpc_2_filter == "empty" || wpc_2_filter == "not_empty")) {
            $('#work_product_code_id_2').val('{/literal}{$WPDATA.wpc_2_id}{literal}');
            $('#work_product_code_name_2').val('{/literal}{$WPDATA.wpc_2_name}{literal}');
            $('#SelectWorkProductCode2').show();
            $('#work_product_code_name_2').show();
        } else {
            $('#SelectWorkProductCode2').hide();
            $('#work_product_code_name_2').hide();
        } 


        $("#filterOptionDropdown3").val('{/literal}{$WPDATA.wpc_3_filter}{literal}');
        if (!(wpc_3_filter == "empty" || wpc_3_filter == "not_empty")) {
            $('#work_product_code_id_3').val('{/literal}{$WPDATA.wpc_3_id}{literal}');
            $('#work_product_code_name_3').val('{/literal}{$WPDATA.wpc_3_name}{literal}');
            $('#SelectWorkProductCode3').show();
            $('#work_product_code_name_3').show();
        } else {
            $('#SelectWorkProductCode3').hide();
            $('#work_product_code_name_3').hide();
        } 


        
        $("#filterOptionDropdown4").val('{/literal}{$WPDATA.cwpa_1_filter}{literal}');
        if (!(cwpa_1_filter == "empty" || cwpa_1_filter == "not_empty")) {
            $('#cwpa_1').val('{/literal}{$WPDATA.cwpa_1_value}{literal}');
            $('#cwpa_1').show();
        } else {
            $('#cwpa_1').hide();
        }

        $("#filterOptionDropdown5").val('{/literal}{$WPDATA.cwpa_2_filter}{literal}');
        if (!(cwpa_2_filter == "empty" || cwpa_2_filter == "not_empty")) {
            $('#cwpa_2').val('{/literal}{$WPDATA.cwpa_2_value}{literal}');
            $('#cwpa_2').show();
        } else {
            $('#cwpa_2').hide();
        }

        $("#filterOptionDropdown6").val('{/literal}{$WPDATA.cwpa_3_filter}{literal}');
        if (!(cwpa_3_filter == "empty" || cwpa_3_filter == "not_empty")) {
            $('#cwpa_3').val('{/literal}{$WPDATA.cwpa_3_value}{literal}');
            $('#cwpa_3').show();
        } else {
            $('#cwpa_3').hide();
        }

        $("#filterOptionDropdown7").val('{/literal}{$WPDATA.cwpa_4_filter}{literal}');
        if (!(cwpa_4_filter == "empty" || cwpa_4_filter == "not_empty")) {
            $('#cwpa_4').val('{/literal}{$WPDATA.cwpa_4_value}{literal}');
            $('#cwpa_4').show();
        } else {
            $('#cwpa_4').hide();
        }

    });

</script>


{/literal}

<br>
<h2>Pyrogen Animal Use</h2>
<br>

<form action="#bwc/index.php?module=Reports&action=CustomComReport" method="post" name="EditView" id="EditView">
	<input type="hidden" name="submit_form" id="submit_form" value="1">
	<input type="hidden" name="order_type" id="order_type" value="{$ORDER_TYPE}">
	<input type="hidden" name="order_column" id="order_column" value="{$ORDER_COLUMN}"> 
</form>
 
<br><br>
{if is_array($ReportData) && count($ReportData) > 0}
{foreach from=$ReportData key=index item=line_item}
<table id="combo_summary_div_0" width="100%" border="0" cellpadding="0" cellspacing="0" class="reportGroupViewTable">
   <tbody>
      <tr>
         <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportGroupNByTable">
               <tbody>
                  <tr height="20">
                     <th align="left" id="Id_0" name="Id_0" class="reportGroup1ByTableEvenListRowS1" valign="middle" nowrap="">
                         &nbsp;Test System = {$line_item.anml_animals_name}, Count = 1
                     </th>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportGroupByDataTableHeader">
               <tbody>
                  <tr>
                     <td>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportDataChildtablelistView">
                           <tbody>
                              <tr height="20">
                                 <th scope="col" align="center" sortcolumn="anml_animals_name" class="reportGroupByDataChildTablelistViewThS1 sortColumn" valign="middle" nowrap="">	
                                    Test System
                                 </th>
                                 <th scope="col" align="center" sortcolumn="anml_animals_cstm_usda_id_c" class="reportGroupByDataChildTablelistViewThS1 sortColumn" valign="middle" nowrap="">	
                                    USDA ID&nbsp;
                                 </th>
                                 <th scope="col" align="center" sortcolumn="ANML_ANIMALS_CSTM_ASSIB5E836" class="reportGroupByDataChildTablelistViewThS1 sortColumn" valign="middle" nowrap="">	
                                    Current Work Product Assignment&nbsp;</a>
                                 </th>
                                 <th scope="col" align="center" sortcolumn="l1_name" class="reportGroupByDataChildTablelistViewThS1 sortColumn" valign="middle" nowrap="">	
                                    Name&nbsp;</a>
                                 </th>
                                 <th scope="col" align="center" sortcolumn="l1_cstm_error_type_c" class="reportGroupByDataChildTablelistViewThS1 sortColumn" valign="middle" nowrap="">	
                                    Type&nbsp;</a>
                                 </th>
                                
                                 <th scope="col" align="center" sortcolumn="l2_name" class="reportGroupByDataChildTablelistViewThS1 sortColumn" valign="middle" nowrap="">	
                                    APS Work Product ID&nbsp;
                                 </th>
                                 <th scope="col" align="center" sortcolumn="l2_cstm_first_procedure_c" class="reportGroupByDataChildTablelistViewThS1 sortColumn" valign="middle" nowrap="">	
                                    First Procedure&nbsp;
                                 </th>
                                 
                                 <th scope="col" align="center" sortcolumn="l1_date_entered" class="reportGroupByDataChildTablelistViewThS1 sortColumn" valign="middle" nowrap="">	
                                    COM Date Created&nbsp;
                                 </th>
                              </tr>
                              <tr height="20" class="Array">
                                 <td width="9%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">
                                    <a target="_blank" href="{$SITE_URL}/#ANML_Animals/{$line_item.primaryid}">{$line_item.anml_animals_name}</a>
                                 </td>
                                 <td width="9%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">
                                    {$line_item.anml_animals_cstm_usda_id_c}
                                 </td>
                                 <td width="9%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">
                                    {$line_item.ANML_ANIMALS_CSTM_ASSIB5E836}
                                 </td>
                                 <td width="9%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">
                                    <a target="_blank" href="{$SITE_URL}/#M06_Error/{$line_item.l1_id}">{$line_item.l1_name}</a>
                                 </td>
                                 <td width="9%" valign="TOP" class="typecellfp oddListRowS1" bgcolor="#dc3434" scope="row">
                                    {$line_item.l1_cstm_error_type_c}
                                 </td>
                                
                                 <td width="9%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">
                                    <a target="_blank" href="{$SITE_URL}/#M03_Work_Product/{$line_item.l2_id}">
                                     {$line_item.l2_name}
                                     </a>
                                 </td>
                                 <td width="9%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">
                                    {$line_item.l2_cstm_first_procedure_c}
                                    
                                    
                                 </td>
                                 
                                 <td width="9%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">
                                    {$line_item.l1_date_entered}
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<br>
{/foreach}

 {else}
  <table id="report_table">
  <td   style="text-align: center;">No Data Available</td>
  </tr>
  </table>

{/if}
<h3>Grand Total</h3>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="list view">
   <tbody>
      <tr height="20">
         <td scope="col" align="left" valign="middle" nowrap="">	
            &nbsp;
         </td>
         <td scope="col" align="left" valign="middle" nowrap="">	
            Count
         </td>
      </tr>
      <tr height="20" class="Array" onmouseover="setPointer(this, '7', 'over', '', '', '');" onmouseout="setPointer(this, '7', 'out', '', '', '');" onmousedown="setPointer(this, '7', 'click', '', '', '');">
         <td width="%" align="left" valign="TOP" class="Array" bgcolor="" scope="row">
            &nbsp;
         </td>
         <td width="%" align="left" valign="TOP" class="Array" bgcolor="" scope="row">
            {$GrandTotal}
         </td>
      </tr>
   </tbody>
</table>
