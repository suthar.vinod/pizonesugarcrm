{literal}
<style>
    #report_table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        table-layout: fixed;
    }

    #report_table th {
        font-weight: bold;
        text-align: left;
        padding: 4px 5px 4px 5px;
        border: 1px solid;
        background: #fff;
        color: #666;
        font-size: 13px;
    }

    #report_table td {
        border-color: #ccc;
        background: white;
        padding: 4px 5px;
        border: 1px solid;
    }
    #report_table td a{
         text-decoration: none;
    }
    #report_table tr:nth-child(even) {
        background-color: #fff;
    }

    #report_table tr:hover {
        background-color: #fff;
    }
  

</style>

{/literal}

<br>
<h2>WPDs Out to Sponsor for 30+ Days</h2>
<br>

  <table id="report_table" width="100%">
            <tbody>
            <tr >
                <th  align="center" valign="middle">
                   Work Product Compliance&nbsp;
                </th>
                <th  align="center" valign="middle">
                   Deliverable Name&nbsp;
                </th>
                <th  align="center" valign="middle">
                    Deliverable Status&nbsp;
                </th>
                <th  align="center" valign="middle">
                   Study Director&nbsp;
                </th>
                <th  align="center" valign="middle">
                    Deliverable Assigned to &nbsp;
                </th>
                <th align="center" valign="middle" >
                    Internal Final Due Date&nbsp;
                </th>               
            </tr>
            {if is_array($ReportData) && count($ReportData) > 0}
            {foreach from=$ReportData key=index item=line_item}
            <tr >
                <td width="10%" >
                   {$line_item.WPC}
                </td>
                <td width="20%" >
                  <a target="_blank" href="{$SITE_URL}/#M03_Work_Product_Deliverable/{$line_item.wpid}"> {$line_item.WPDdeliverable}</a>
                </td>
                <td width="20%" >
                   {$line_item.WPD_Status}
                <td width="20%" >
                   <a target="_blank" href="{$SITE_URL}/#Contacts/{$line_item.SDID}"> {$line_item.SDfullname}</a>
                </td>
                <td width="20%" >
                   {$line_item.Assign_username}
                </td>
                <td width="10%" >
                     {$line_item.Duedate}
                </td>
                
            </tr>
            {/foreach}
            {else}
            <tr>
                <td colspan="8" style="text-align: center;">No Data Available</td>
            </tr>
            {/if}
            </tbody>
        </table>
