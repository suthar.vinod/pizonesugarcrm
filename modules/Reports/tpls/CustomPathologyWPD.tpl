{literal}
<style>
    #report_table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 10%;
        table-layout: fixed;
    }

    #report_table th {
        font-weight: bold;
        text-align: left;
        padding: 4px 5px 4px 5px;
        border: 1px solid;
        background: #fff;
        color: #666;
        font-size: 13px;
    }

    #report_table td {
        border-color: #ccc;
        background: white;
        padding: 4px 5px;
        border: 1px solid;
    }
    #report_table td a{
         text-decoration: none;
    }
    #report_table tr:nth-child(even) {
        background-color: #fff;
    }

    #report_table tr:hover {
        background-color: #fff;
    }
  

</style>

{/literal}

<br>
<h2>Custom Report for Pathology WPDs</h2>
<br>


        <table id="report_table" width="100%">
            <tbody>
            <tr>              
                <th  align="center" valign="middle">
                    APS Work Product IDs
                </th>               
            </tr>
            {if is_array($ReportData) && count($ReportData) > 0}
            {foreach from=$ReportData key=index item=line_item}
            <tr >            
                <td width="10%" >
                  
                   <a target="_blank" href="{$SITE_URL}/#M03_Work_Product/{$line_item.wpid}"> {$line_item.wpname}</a>
                   
                </td>                
            </tr>
            {/foreach}
            {else}
            <tr>
                <td colspan="8" style="text-align: center;">No Data Available</td>
            </tr>
            {/if}
            </tbody>
        </table>
