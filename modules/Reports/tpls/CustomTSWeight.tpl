{literal}
<style>
    #report_table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        table-layout: fixed;
    }

    #report_table th {
        font-weight: bold;
        text-align: left;
        padding: 4px 5px 4px 5px;
        border: 1px solid;
        background: #fff;
        color: #666;
        font-size: 13px;
    }

    #report_table td {
        border-color: #ccc;
        background: white;
        padding: 4px 5px;
        border: 1px solid;
    }

    #report_table tr:nth-child(even) {
        background-color: #fff;
    }

    #report_table tr:hover {
        background-color: #fff;
    }
  

</style>

<script>

$(document).ready(function(){
   $("#btnReset").click(function(){
      var year=new Date().getFullYear();      
      var currentDate=new Date().toISOString().slice(0, 10);
      $("#filter_start_date").val(currentDate.slice(0,-2)+"01");
      $("#filter_end_date").val(currentDate);

      $("#EditView").submit();
   })
});

function ExportToWord(report,type,user_id,filter_start_date,filter_end_date){
host= 'live';
window.open('http://deploytest_01.apsflightlog.com/ExportApp/ExportToWord?host='+host+'&user_id='+user_id+'&report='+report+'&type='+type+'&filter_start_date='+filter_start_date+'&filter_end_date='+filter_end_date,'_blank');
}
</script>
{/literal}

<br>
<h2>Custom Test System Weight</h2>
<br>
				<input title="Export" type="button" value="Export To Word" class="button_name" id="ExportWord" onclick="ExportToWord('CustomTSWeight','doc','{$current_user}','{$filter_start_date}','{$filter_end_date}');">
				<input title="Export" type="button" value="Export To Excel" class="button_name" id="ExportWord" onclick="ExportToWord('CustomTSWeight','xls','{$current_user}','{$filter_start_date}','{$filter_end_date}');">
<br>
<br>
<form action="#bwc/index.php?module=Reports&action=CustomTSWeight" method="post" name="EditView" id="EditView">
   <table width="100%" cellspacing="0" cellpadding="0">
      <tbody>
         <tr>
            <td valign="top" width="90%">
               <div id="filters_tab" style="display:''">
                  <div scope="row">
                     <h3>Run-time Filters:<span valign="bottom">&nbsp;<img border="0" onclick="return SUGAR.util.showHelpTips(this,'Specify values for <b>Run-time Filters</b> and click the <b>Apply Filters</b> button to re-run the report.' );" src="themes/RacerX/images/helpInline.png?v=kJf5Yxi324HatVAqLJY46g" alt="Information" class="inlineHelpTip"></span></h3>
                  </div>
                  <table id="filters_top" border="0" cellpadding="0" cellspacing="0">
                     <tbody id="filters">
                        <tr id="rowid1">
                           <td>
                           </td>
                           <td>
                              &nbsp;&nbsp;&nbsp;
                              Start Date (Date Created)  
                           </td>
                           <td>
                              
                           </td>
                           <td></td>
                           <td>
                              &nbsp;&nbsp;
                              <select disabled name="wpc_1_filter" id="filterOptionDropdown1" onchange="filterTypeChanged(1);">
                                 <option value="is" selected="">Is</option>
                                 <option value="is_not">Is Not</option>
                                 <option value="empty">Is Empty</option>
                                 <option value="not_empty">Is Not Empty</option>
                              </select>
                           </td>
                           <td>
                              <table>
                                 <tbody>
                                    <tr>
                                    
                                       <td>
                                            <input type="date" name="filter_start_date"
                                                   id="filter_start_date"
                                                   class="sqsEnabled yui-ac-input wpfilter" autocomplete="off"
                                                   value="{$filter_start_date}">
                                       </td>
                                      
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                           <td></td>
                        </tr>
                        <tr id="rowid2">
                           <td>
                           </td>
                           <td>
                              &nbsp;&nbsp;&nbsp;
                              
                              End Date (Date Created)    
                           </td>
                           <td>
                           </td>
                           <td>&nbsp;&nbsp;</td>
                           <td>
                              &nbsp;&nbsp;
                              <select disabled name="wpc_2_filter" id="filterOptionDropdown2" onchange="filterTypeChanged(2);">
                                 <option value="is" selected="">Is</option>
                                 <option value="is_not">Is Not</option>
                                 <option value="empty">Is Empty</option>
                                 <option value="not_empty">Is Not Empty</option>
                              </select>
                           </td>
                           <td>
                              <table>
                                 <tbody>
                                    <tr>
                                    
                                       <td>
                                       
                                            <input type="date" name="filter_end_date"
                                                   id="filter_end_date"
                                                   class="sqsEnabled yui-ac-input wpfilter" autocomplete="off"
                                                   value="{$filter_end_date}">
                                       </td>
                                       
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                           <td></td>
                        </tr>
                     </tbody>
                  </table>
                  <table>
                     <tbody>
                        <tr>
                           <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                           <td>
                              <input type="hidden" name="submit_form" id="submit_form" value="1">
                              <input type="submit" class="button" title="Apply" value="Apply">
                              <input type="button" class="button" title="Reset" id="btnReset" value="Reset">
                           </td>
                           <td>
                              
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </td>
         </tr>
      </tbody>
   </table>
</form>

        <table id="report_table" width="100%">
            <tbody>
            <tr >
                <th  align="center" valign="middle">
                    Species&nbsp;

                </th>
                <th  align="center" valign="middle">
                    Breed&nbsp;
                </th>
                <th  align="center" valign="middle">
                    Animal #&nbsp;
                </th>
                <th  align="center" valign="middle">
                    Date of Weight&nbsp;
                </th>
                <th  align="center" valign="middle">
                    Weight&nbsp;
                </th>
                <th align="center" valign="middle" >
                    DOB&nbsp;
                </th>
                <th align="center" valign="middle" >
                    Age (Months)&nbsp;
                </th>
                <th align="center" valign="middle" >
                    Sex&nbsp;
                </th>
            </tr>
            {if is_array($ReportData) && count($ReportData) > 0}
            {foreach from=$ReportData key=index item=line_item}
            <tr >
                <td width="10%" >
                  
                   <a target="_blank" href="{$SITE_URL}/#S_Species/{$line_item.s_species_id_c}"> {$line_item.species_name}</a>
                   
                </td>
                <td width="10%" >
                  {$line_item.breed_c} 
                </td>
                <td width="10%" >
                   <a target="_blank" href="{$SITE_URL}/#ANML_Animals/{$line_item.parent_id}"> {$line_item.name}</a>                </td>
                <td width="10%" >
                    {$line_item.date_last_weighed_c}
                </td>
                <td width="10%" >
                   {$line_item.after_value_string} 
                </td>
                <td width="10%" >
                    {$line_item.date_of_birth_c}
                </td>
                <td width="10%" >
                </td>
                <td width="10%" >
                    {$line_item.sex_c}
                </td>
            </tr>
            {/foreach}
            {else}
            <tr>
                <td colspan="8" style="text-align: center;">No Data Available</td>
            </tr>
            {/if}
            </tbody>
        </table>
