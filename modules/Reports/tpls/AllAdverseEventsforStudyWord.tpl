{literal}
<style>
	.select2-container{
		width : 200px;
	}
	#report_table {
	  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	  table-layout: fixed;
	}
	#report_table th {
    font-weight: bold;
    text-align: left;
    padding: 4px 5px 4px 5px;
    border: 1px solid;
    background: #fff;
    color: #666;
    font-size: 16px;
   }
 	#report_table td {
	border-color: #ccc;
    background: white;
    padding: 4px 5px;
    border: 1px solid;
	}
    #report_table tr:nth-child(even){background-color: #fff;}
	#report_table tr:hover {background-color: #fff;}

</style>

{/literal}
<img data-metadata="logo" src="http://10.150.6.31/sugar/custom/themes/default/images/company_logo.jpg" width="200" height="100" alt="APS">
<h2>All Adverse Events for a Study</h2>
<br>
<table id="report_table">
  <tr>
	<th style="width:10%">Communication</th>
	<th style="width:15%">Test System ID - USDA ID</th>
	<th style="width:10%">Date Occurred</th>
	<th style="width:20%">Actual Event</th>
	<th style="width:15%">Impact Assessment</th>
	<th style="width:15%">Corrective Action Within the Study</th>
	<th style="width:15%">Corrective Action Details</th>
  </tr>
  {if is_array($REPORTDATA) && count($REPORTDATA) > 0}
  {foreach from=$REPORTDATA key=index item=line_item}
	<tr>
		<td>{$line_item.communication_name}</td>
		<td style="word-wrap: break-word">{$line_item.test_system}</td>
		<!--<td style="word-wrap: break-word">{$line_item.usda_id}</td>-->
		<td>{$line_item.date_occurred}</td>
		<td>{$line_item.actual_event}</td>
		<td>{$line_item.sd_assessment}</td>
		<td>{$line_item.corrective_action}</td>
		<td>{$line_item.corrective_action_details}</td>
	</tr>
  {/foreach}
  {else}
  <tr>
  <td colspan="7" style="text-align: center;">No Data Available</td>
  </tr>

{/if}
</table>
