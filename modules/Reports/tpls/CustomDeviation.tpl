{$GRAPH_RESOURCES}
{$js_array}
{literal}
<style>
    #report_table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        table-layout: fixed;
    }

    #report_table th {
        font-weight: bold;
        text-align: left;
        padding: 4px 5px 4px 5px;
        border: 1px solid;
        background: #fff;
        color: #666;
        font-size: 13px;
    }

    #report_table td {
        border-color: #ccc;
        background: white;
        padding: 4px 5px;
        border: 1px solid;
    }

    #report_table tr:nth-child(even) {
        background-color: #fff;
    }

    #report_table tr:hover {
        background-color: #fff;
    }
  

</style>
<script>
$(document).ready(function(){
	$('#ApplyBtn').click(function(){
		date_occured_from = $('#date_occured_from').val();		
		date_occured_to   = $('#date_occured_to').val();		
		department        = $('#department').val();		
		if(department != '' && date_occured_to!="" && date_occured_from!="" ){
			window.location.href = "#bwc/index.php?module=Reports&action=CustomDeviation";
			return true;
		} 
		else if(date_occured_from == '')
		{
			alert('Please select From Date');
			return false;
		}
      else if(date_occured_to == '')
		{
			alert('Please select To Date');
			return false;
		}
      else if(department == '')
		{
			alert('Please select department');
			return false;
		}
		
	});
});

</script>  
<script>SUGAR.loadChart = true;</script>
{/literal}


<!--<table id="report_table" cellpadding="0" cellspacing="0" border="0" width="100%" >-->
<h2>Deviations per Animal by Department</h2>
<br>
<form action="#bwc/index.php?module=Reports&action=CustomDeviation" method="post" name="EditView" id="EditView">
   <table width="100%" cellspacing="0" cellpadding="0">
      <tbody>
         <tr>
            <td valign="top" width="90%">
               <div id="filters_tab" style="display:''">
                  <div scope="row">
                     <h3>Run-time Filters:<span valign="bottom">&nbsp;<img border="0" onclick="return SUGAR.util.showHelpTips(this,'Specify values for <b>Run-time Filters</b> and click the <b>Apply Filters</b> button to re-run the report.' );" src="themes/RacerX/images/helpInline.png?v=kJf5Yxi324HatVAqLJY46g" alt="Information" class="inlineHelpTip"></span></h3>
                  </div>
                  <table id="filters_top" border="0" cellpadding="0" cellspacing="0">
                     <tbody id="filters">					
                        <tr id="rowid1">
                            
                           <td>From Date Occured</td>
                           <td>&nbsp;&nbsp;</td>
                           <td>&nbsp;&nbsp;</td>                           
                           <td>
                              <table>
                                 <tbody>
                                    <tr>
                                       <td>
                          <input type="date" name="date_occured_from" id="date_occured_from"
                                       class="sqsEnabled yui-ac-input wpfilter" autocomplete="off" value="{$date_occured_from}">
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                           <td></td>
                        </tr>
						      <tr id="rowid2">
                            
                           <td>To Date Occured</td>
                           <td>&nbsp;&nbsp;</td>
                           <td>&nbsp;&nbsp;</td>                           
                           <td>
                              <table>
                                 <tbody>
                                    <tr>
                                       <td><input type="date" name="date_occured_to" id="date_occured_to"
                            class="sqsEnabled yui-ac-input wpfilter" autocomplete="off" value="{$date_occured_to}">
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                           <td></td>
                        </tr>
						      <tr id="rowid3">
                            
                           <td>Select Department</td>
                           <td>&nbsp;&nbsp;</td>
                           <td>&nbsp;&nbsp;</td>                           
                           <td>
                              <table>
                                 <tbody>
                                    <tr>
                                       <td><select name="department" id="department">{$deptOptions}</select> </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                           <td></td>
                        </tr>
                     </tbody>
                  </table>
                  <table>
                     <tbody>
                        <tr>
                           <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                           <td>
                              <input type="hidden" name="submit_form" id="submit_form" value="1">
                              <input type="submit" class="button" id="ApplyBtn" title="Apply" value="Apply">                              
                           </td>
                           <td>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </td>
         </tr>
      </tbody>
   </table>
</form>
<br>

{if $IS_CHART==1}
<div style="width:100%; max-height:550px; overflow: scroll;">
{$LEADS_CHART_DATA}
</div>
{/if}

<table id="report_table" width="100%">
	{foreach from=$ReportData key=index item=wpgItems}
    <tr>       
    	{foreach from=$wpgItems key=index1 item=line_item} <td width="10%">{$line_item}</td> {/foreach}
    </tr>
	{/foreach}
</table>
