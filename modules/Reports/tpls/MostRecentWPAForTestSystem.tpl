{literal}
<style>
    .select2-container {
        width: 200px;
    }

    #report_table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        table-layout: fixed;
    }

    #report_table th {
        font-weight: bold;
        text-align: left;
        padding: 4px 5px 4px 5px;
        border: 1px solid;
        background: #fff;
        color: #666;
        font-size: 13px;
    }

    #report_table td {
        border-color: #ccc;
        background: white;
        padding: 4px 5px;
        border: 1px solid;
    }

    #report_table tr:nth-child(even) {
        background-color: #fff;
    }

    #report_table tr:hover {
        background-color: #fff;
    }

    .wpfilter {
        border: 0;
        background: #F7F8F9;
        padding-left: 6px;
        width: 100px !important;
    }
</style>
<script>
    function filterTypeChanged(code) {
        var fiterVal = $('#filterOptionDropdown' + code).val();
        if (code == 1) {
            if (fiterVal == "is" || fiterVal == "is_not") {
                getDropdownOptions("wps_outcome_wpe_activities_list", "optionDropdown" + code);
            } else {

                $("#optionDropdown" + code).hide();
            }

        }

        if (code == 2) {
            if (fiterVal == "is" || fiterVal == "is_not") {
                $("#test_system_name").show();
                $('#SelectTestSystem').show();
            } else {
                $("#test_system_name").hide();
                $('#SelectTestSystem').hide();
            }

        }
        if (code == 3) {

            if (fiterVal == "is" || fiterVal == "is_not") {
                $("#test_allocated_work_product_name").show();
                $('#SelectAllocatedWorkProduct').show();
            } else {
                $("#test_allocated_work_product_name").hide();
                $('#SelectAllocatedWorkProduct').hide();
            }

        }

        if (code == 5) {

            if (fiterVal == "is" || fiterVal == "is_not") {
                $("#work_product_name").show();
                $('#SelectWorkProductBtn').show();
            } else {
                $("#work_product_name").hide();
                $('#SelectWorkProductBtn').hide();
            }

        }
        if (code == 4) {

            if (fiterVal == "is" || fiterVal == "is_not") {
                $("#species_name").show();
                $('#SelectSpecies').show();
            } else {
                $("#species_name").hide();
                $('#SelectSpecies').hide();
            }

        }

    }

    function getDropdownOptions(list, id) {

        var statusOptions = SUGAR.language.languages.app_list_strings[list];
        var status_dd = "";

        for (var key in statusOptions) {
            // Here "key" gives the key of options and "statusOptions[key]" gives the value of it.

            status_dd += '<option value="' + key + '">' + statusOptions[key] + '</option>';
        }
        $('#' + id).html(status_dd);
        $('#' + id).show();

    }

    $(document).ready(function () {

        getDropdownOptions("enrollment_status_list", "optionDropdown0")
         getDropdownOptions("wps_outcome_wpe_activities_list", "optionDropdown1")

        /* $( "#work_product_name" ).keypress(function() {
          work_product_id = $('#work_product_id').val('');
    });*/
        $('#work_product_name:text').on('input', function () {
            work_product_id = $('#work_product_id').val('');
        });
        $('#ApplyBtn').click(function () {
            work_product_id = $('#work_product_id').val();
            if (work_product_id != '') {
                window.location.href = "#bwc/index.php?module=Reports&action=WPAReport";
                return true;
            }
            // else if (work_product_id == '') {
             //   alert('"Work Products: APS Work Product ID" missing input value');
            //    return false;
           // }

        });

        $('#SelectWorkProductBtn').click(function () {
            open_popup(
                "M03_Work_Product",
                600,
                400,
                "",
                true,
                false,
                {
                    "call_back_function": "set_return",
                    "form_name": "EditView",
                    "field_to_name_array": {"id": "work_product_id", "name": "work_product_name"}
                },
                "single",
                true
            );
            /* $('#work_product_name').css('display', 'block');
             $('#work_product_name').attr('readonly', 'true');*/
        });
        $('#SelectTestSystem').click(function () {
            open_popup(
                "ANML_Animals",
                600,
                400,
                "",
                true,
                false,
                {
                    "call_back_function": "set_return",
                    "form_name": "EditView",
                    "field_to_name_array": {"id": "test_system_id", "name": "test_system_name"}
                },
                "single",
                true
            );
            /* $('#work_product_name').css('display', 'block');
             $('#work_product_name').attr('readonly', 'true');*/
        });
        $('#SelectAllocatedWorkProduct').click(function () {
            open_popup(
                "M03_Work_Product",
                600,
                400,
                "",
                true,
                false,
                {
                    "call_back_function": "set_return",
                    "form_name": "EditView",
                    "field_to_name_array": {
                        "id": "test_allocated_work_product_id",
                        "name": "test_allocated_work_product_name"
                    }
                },
                "single",
                true
            );
            /* $('#work_product_name').css('display', 'block');
             $('#work_product_name').attr('readonly', 'true');*/
        });
        $('#SelectSpecies').click(function () {
            open_popup(
                "S_Species",
                600,
                400,
                "",
                true,
                false,
                {
                    "call_back_function": "set_return",
                    "form_name": "EditView",
                    "field_to_name_array": {"id": "species_id", "name": "species_name"}
                },
                "single",
                true
            );
            /* $('#work_product_name').css('display', 'block');
             $('#work_product_name').attr('readonly', 'true');*/
        });

        var entrollment_status_filter = '{/literal}{$WPDATA.entrollment_status_filter}{literal}';
        var enrollment_activity_filter = '{/literal}{$WPDATA.enrollment_activity_filter}{literal}';
        var test_system_filter = '{/literal}{$WPDATA.test_system_filter}{literal}';
        var test_system_name = '{/literal}{$WPDATA.test_system_name}{literal}';
        var test_allocated_work_product_filter = '{/literal}{$WPDATA.test_allocated_work_product_filter}{literal}';
        var test_allocated_work_product_name = '{/literal}{$WPDATA.test_allocated_work_product_name}{literal}';
        var species_filter = '{/literal}{$WPDATA.species_filter}{literal}';
        var species_name = '{/literal}{$WPDATA.species_name}{literal}';
        var work_product_filter = '{/literal}{$WPDATA.work_product_filter}{literal}';
        var work_product_name = '{/literal}{$WPDATA.work_product_name}{literal}';


        $("#filterOptionDropdown0").val('{/literal}{$WPDATA.entrollment_status_filter}{literal}');
        if (entrollment_status_filter == "is" || entrollment_status_filter == "is_not") {
            $('#optionDropdown0').val('{/literal}{$WPDATA.entrollment_status}{literal}');
            $('#optionDropdown0').show();
        } else {
            $('#optionDropdown0').hide();
        }


        $("#filterOptionDropdown1").val('{/literal}{$WPDATA.enrollment_activity_filter}{literal}');
        if (enrollment_activity_filter == "is" || enrollment_activity_filter == "is_not") {
            console.log('{/literal}{$WPDATA.enrollment_activity}{literal}');
            $('#optionDropdown1').val('{/literal}{$WPDATA.enrollment_activity}{literal}');
            $('#optionDropdown1').show();
        } else {
            $('#optionDropdown1').hide();
        }

        $("#filterOptionDropdown2").val('{/literal}{$WPDATA.test_system_filter}{literal}');


        if (test_system_filter == "is" || test_system_filter == "is_not") {
            $('#SelectTestSystem').show();
            $('#test_system_name').show();
        } else {
           $('#test_system_name').val("");
            $('#SelectTestSystem').hide();
            $('#test_system_name').hide();
        }

        $("#filterOptionDropdown3").val('{/literal}{$WPDATA.test_allocated_work_product_filter}{literal}');
        if (test_allocated_work_product_filter == "is" || test_allocated_work_product_filter == "is_not") {
            $('#SelectAllocatedWorkProduct').show();
            $('#test_allocated_work_product_name').show();
        } else {
           $('#test_allocated_work_product_name').val("");
            $('#SelectAllocatedWorkProduct').hide();
            $('#test_allocated_work_product_name').hide();
        }

        $("#filterOptionDropdown4").val('{/literal}{$WPDATA.species_filter}{literal}');
        if (species_filter == "is" || species_filter == "is_not") {
            $('#SelectSpecies').show();
            $('#species_name').show();
        } else {
           $('#species_name').val("");
            $('#SelectSpecies').hide();
            $('#species_name').hide();
        }
        $("#filterOptionDropdown5").val('{/literal}{$WPDATA.work_product_filter}{literal}');

        if (work_product_filter == "is" || work_product_filter == "is_not") {
            $('#SelectWorkProductBtn').show();
            $('#work_product_name').show();
        } else {
           $('#work_product_name').val("");
            $('#SelectWorkProductBtn').hide();
            $('#work_product_name').hide();
        }
console.log('{/literal}{$WPDATA.test_system_filter}{literal}');
console.log('{/literal}{$WPDATA.test_allocated_work_product_filter}{literal}');


    });

</script>


{/literal}


<!--<table id="report_table" cellpadding="0" cellspacing="0" border="0" width="100%" >-->
<h2>WPA Report</h2>
<br>
<form id="EditView" name="EditView" method="post"
      action="#bwc/index.php?module=Reports&action=MostRecentWPAForTestSystem">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td valign="top" width="90%">
                <div id="filters_tab" style="display:''">
                    <div scope="row">
                        <h3>Run-time Filters:<span valign="bottom">&nbsp;<img border="0"
                                                                              onclick="return SUGAR.util.showHelpTips(this,'Specify values for <b>Run-time Filters</b> and click the <b>Apply Filters</b> button to re-run the report.' );"
                                                                              src="themes/RacerX/images/helpInline.png?v=_UExfi3RWhTVRmUJSTxJCg"
                                                                              alt="Information"
                                                                              class="inlineHelpTip"></span></h3>
                    </div>
                    <input type="hidden" name="filters_def" value="">
                    <table id="filters_top" border="0" cellpadding="0" cellspacing="0">
                        <tbody id="filters">
                        <tr id="rowid0">
                            <td>

                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;

                                Work Product Enrollment
                            </td>
                            <td>

                            </td>
                            <td>&nbsp;&gt;&nbsp;Status</td>
                            <td>
                                &nbsp;&gt;&nbsp;
                                <select name="entrollment_status_filter" id="filterOptionDropdown0"
                                        onchange="filterTypeChanged(0);">
                                    <option value="is" selected="">Is</option>
                                    <option value="is_not">Is Not</option>
                                </select>
                            </td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>

                                            <select name="entrollment_status" id="optionDropdown0">

                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td></td>
                        </tr>
                        <tr id="rowid1">
                            <td>

                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;

                                Work Product Enrollment
                            </td>
                            <td>

                            </td>
                            <td>&nbsp;&gt;&nbsp;Activities</td>
                            <td>
                                &nbsp;&gt;&nbsp;
                                <select name="enrollment_activity_filter" id="filterOptionDropdown1"
                                        onchange="filterTypeChanged(1);">
                                    <option value="is">Is</option>
                                    <option value="is_not">Is Not</option>
                                    <option value="empty" selected="">Is Empty</option>
                                    <option value="not_empty">Is Not Empty</option>
                                </select>
                            </td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <select name="enrollment_activity" style="display:none" id="optionDropdown1">

                                        </select>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td></td>
                        </tr>
                        <tr id="rowid2">
                            <td>

                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;

                                Work Product Enrollment &gt; Test Systems
                            </td>
                            <td>

                            </td>
                            <td>&nbsp;&gt;&nbsp;Test System</td>
                            <td>
                                &nbsp;&gt;&nbsp;
                                <select name="test_system_filter" id="filterOptionDropdown2"
                                        onchange="filterTypeChanged(2);">
                                    <option value="is">Is</option>
                                    <option value="is_not">Is Not</option>

                                    <option value="empty">Is Empty</option>
                                    <option value="not_empty" selected="">Is Not Empty</option>
                                </select>
                            </td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="test_system_id" id="test_system_id"
                                                   value="{$WPDATA.test_system_id}">
                                            <input type="text" name="test_system_name" id="test_system_name"
                                                   class="sqsEnabled yui-ac-input wpfilter" autocomplete="off"
                                                   style="display:none" value="{$WPDATA.test_system_name}" readonly>
                                        </td>
                                        <td>
                                            <input title="Select" type="button" value="Select" class="button_name"
                                                   style="display:none" id="SelectTestSystem">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td></td>
                        </tr>
                        <tr id="rowid3">
                            <td>

                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;

                                Work Product Enrollment &gt; Test Systems
                            </td>
                            <td>

                            </td>
                            <td>&nbsp;&gt;&nbsp;Allocated Work Product</td>
                            <td>
                                &nbsp;&gt;&nbsp;
                                <select name="test_allocated_work_product_filter" id="filterOptionDropdown3"
                                        onchange="filterTypeChanged(3);">
                                    <option value="is">Is</option>
                                    <option value="is_not">Is Not</option>
                                    <option value="empty">Is Empty</option>
                                    <option value="not_empty" selected="">Is Not Empty</option>
                                </select>
                            </td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="test_allocated_work_product_id"
                                                   id="test_allocated_work_product_id"
                                                   value="{$WPDATA.test_allocated_work_product_id}">
                                            <input type="text" name="test_allocated_work_product_name"
                                                   id="test_allocated_work_product_name"
                                                   class="sqsEnabled yui-ac-input wpfilter" autocomplete="off"
                                                   value="{$WPDATA.test_allocated_work_product_name}"
                                                   style="display:none" readonly>
                                        </td>
                                        <td>
                                            <input title="Select" type="button" value="Select" class="button_name"
                                                   style="display:none" id="SelectAllocatedWorkProduct">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td></td>
                        </tr>
                        <tr id="rowid4">
                            <td>

                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;

                                Work Product Enrollment &gt; Test Systems
                            </td>
                            <td>

                            </td>
                            <td>&nbsp;&gt;&nbsp;Species</td>
                            <td>
                                &nbsp;&gt;&nbsp;
                                <select name="species_filter" id="filterOptionDropdown4"
                                        onchange="filterTypeChanged(4);">
                                    <option value="is" selected="">Is</option>
                                    <option value="is_not">Is Not</option>
                                    <option value="empty">Is Empty</option>
                                    <option value="not_empty">Is Not Empty</option>
                                </select>
                            </td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="species_id" id="species_id"
                                                   value="{$WPDATA.species_id}">
                                            <input type="text" name="species_name" id="species_name"
                                                   class="sqsEnabled yui-ac-input wpfilter" autocomplete="off"
                                                   value="{$WPDATA.species_name}" readonly>
                                        </td>
                                        <td>
                                            <input title="Select" type="button" value="Select" class="button_name"
                                                   id="SelectSpecies">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td></td>
                        </tr>
                        <tr id="rowid5">
                            <td>

                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;

                                Work Product Enrollment &gt; Work Products
                            </td>
                            <td>

                            </td>
                            <td>&nbsp;&gt;&nbsp;ID</td>
                            <td>
                                &nbsp;&gt;&nbsp;
                                <select name="work_product_filter" id="filterOptionDropdown5"
                                        onchange="filterTypeChanged(5);">
                                    <option value="is" selected="">Is</option>
                                    <option value="is_not">Is Not</option>
                                    <option value="empty">Is Empty</option>
                                    <option value="not_empty">Is Not Empty</option>
                                </select>
                            </td>
                            <td>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="work_product_id" id="work_product_id"
                                                   value="{$WPDATA.work_product_id}">
                                            <input type="text" name="work_product_name" id="work_product_name"
                                                   class="sqsEnabled yui-ac-input wpfilter" autocomplete="off"
                                                   value="{$WPDATA.work_product_name}" readonly>
                                        </td>
                                        <td>
                                            <input title="Select" type="button" value="Select" class="button_name"
                                                   id="SelectWorkProductBtn">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                        <tr>
                            <td>
                                <input type="hidden" name="submit_form" id="submit_form" value="1">

                                <input title="Apply" type="submit" value="Apply" class="button_name" id="ApplyBtn">
                            </td>
                        </tr>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</form>
<br>


<div id="report_results">
    <div class="listViewBody nosearch">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="list view">
            <tbody>
            <tr height="20">
                <th scope="col" align="center" valign="middle" nowrap="">
                    WPA ID&nbsp;

                </th>
                <th scope="col" align="center" valign="middle" nowrap="">
                    Status&nbsp;
                </th>
                <th scope="col" align="center" valign="middle" nowrap="">
                    Date Enrollment Created&nbsp;
                </th>
                <th scope="col" align="center" valign="middle" nowrap="">
                    Allocated Work Product&nbsp;
                </th>
                <th scope="col" align="center" valign="middle" nowrap="">
                    Date Last Weighed&nbsp;
                </th>
                <th scope="num" align="center" valign="middle" nowrap="">
                    Weight&nbsp;
                </th>
                <th scope="col" align="center" valign="middle" nowrap="">
                    Breed&nbsp;
                </th>
                <th scope="col" align="center" valign="middle" nowrap="">
                    Sex&nbsp;
                </th>
                <th scope="col" align="center" valign="middle" nowrap="">
                    Activities&nbsp;
                </th>
                <th scope="col" align="center" valign="middle" nowrap="">
                    Abnormality&nbsp;
                </th>
            </tr>
            {if is_array($REPORTDATA) && count($REPORTDATA) > 0}
            {foreach from=$REPORTDATA key=index item=line_item}
            <tr height="20" class="oddListRowS1" onmouseover="setPointer(this, '', 'over', '', '', '');"
                onmouseout="setPointer(this, '', 'out', '', '', '');"
                onmousedown="setPointer(this, '', 'click', '', '', '');">
                <td width="10%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">
                   <a target="_blank" href="{$SITE_URL}/#WPE_Work_Product_Enrollment/{$line_item.WPA_ID}"> {$line_item.WPA_Name}</a>
                </td>
                <td width="10%" valign="TOP" class="oddListRowS1" bgcolor="">
                    {$line_item.status}
                </td>
                <td width="10%" valign="TOP" class="oddListRowS1" bgcolor="">
                    {$line_item.enroll_date}
                </td>
                <td width="10%" valign="TOP" class="oddListRowS1" bgcolor="">
                  <a target="_blank" href="{$SITE_URL}/#M03_Work_Product/{$line_item.allocatedWorkProductId}">  {$line_item.allocatedWorkProductName} </a>
                </td>
                <td width="10%" valign="TOP" class="oddListRowS1" bgcolor="">
                    {$line_item.last_date_weighted}
                </td>
                <td width="10%" valign="TOP" class="oddListRowS1 number_align" bgcolor="">
                    {$line_item.weight}
                </td>
                <td width="10%" valign="TOP" class="oddListRowS1" bgcolor="">
                    {$line_item.breed}
                </td>
                <td width="10%" valign="TOP" class="oddListRowS1" bgcolor="">
                    {$line_item.sex}
                </td>
                <td width="10%" valign="TOP" class="oddListRowS1" bgcolor="">
                    {$line_item.activities}
                </td>
                <td width="10%" valign="TOP" class="oddListRowS1" bgcolor="">
                    {$line_item.abnormality}
                </td>
            </tr>
            {/foreach}
            {else}
            <tr>
                <td colspan="8" style="text-align: center;">No Data Available</td>
            </tr>
            {/if}
            </tbody>
        </table>
        <p></p>
    </div>
    <p></p>
    <table width="100%" id="query_table" class="contentBox">
    </table>
    <script>var filterString = '((IFNULL(wpe_work_product_enrollment_cstm.enrollment_status_c,\&#039;\&#039;) = \&#039;On Study\&#039;) AND ((coalesce(LENGTH(wpe_work_product_enrollment_cstm.activities_c),0) = 0 OR Work Product Enrollment &gt; Activities = \&#039;^^\&#039;)) AND ((coalesce(LENGTH(l1.name), 0) = 0)) AND ((coalesce(LENGTH(l1_cstm.m03_work_product_id_c), 0) = 0)) AND ((coalesce(LENGTH(l1_cstm.s_species_id_c), 0) = 0)) AND (l2.id=\&#039;4a529aae-5165-3149-6db7-5702c7b98a5e\&#039;))';</script>
</div>




