{literal}
<style>
    .palered {
        background: #ffcccb !important;
    }

    .sortColumn:hover {
        cursor: pointer;
        text-decoration: underline;
    }

    .select2-container {
        width: 200px;
    }

    #report_table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        table-layout: fixed;
    }

    #report_table th {
        font-weight: bold;
        text-align: left;
        padding: 4px 5px 4px 5px;
        border: 1px solid;
        background: #fff;
        color: #666;
        font-size: 13px;
    }

    #report_table td {
        border-color: #ccc;
        background: white;
        padding: 4px 5px;
    }

    #report_table td.custom {
        border-color: #ccc;
        background: white;
        padding: 4px 5px;
        border: 1px solid;
    }

    #report_table tr:nth-child(even) {
        background-color: #fff;
    }

    #report_table tr:last-child td {
        border-bottom: 1px solid;
    }

    #report_table tr:hover {
        background-color: #fff;
    }

    .wpfilter {
        border: 0;
        background: #F7F8F9;
        padding-left: 6px;
        width: 100px !important;
    }

    #report_table td.is_related {
        border-color: #ccc;
        padding: 4px 5px;
        background: #fff;
        border: 0px;
    }

    #report_table td.is_related_first {
        padding: 4px 5px;
        background: #fff;
        border-left: 1px solid #000;
    }

    #report_table td.is_related_last {
        padding: 4px 5px;
        background: #fff;
        border-right: 1px solid #000;
    }

    #report_table td.custom {
        border-top: 1px solid #000;
        border-color: #000;
    }

    .reportGroupViewTable {
        background: LightGrey;
        border-top: 1px solid #000;
        border-left: 1px solid #000;
    }

    input {
        position: absolute;
        opacity: 0;
        z-index: -1;
    }

    .row {
        display: flex;
    }

    .row .col {
        flex: 1;
    }

    /* Accordion styles */
    .tabs {
        overflow: hidden;
        box-shadow: 0 4px 4px -2px rgba(0, 0, 0, 0.5);
    }

    .tab {
        width: 100%;
        color: white;
        overflow: hidden;
    }

    .tab-label {
        display: flex;
        justify-content: space-between;
        padding: 15px 20px;
        font-weight: bold;
        cursor: pointer;
        background: #eee;
        border: 1px solid #ccc;
    }

    .tab-label h2 {
        color: #000;
    }

    /* Icon */
    }

    .tab-label:hover {
        background: #ddd;
    }

    .tab-label::after {
        content: "\276F";
        width: 2em;
        height: 2em;
        text-align: center;
        transition: all 0.35s;
        color: #000;
    }

    .tab-content {
        max-height: 0;
        padding: 0 1em;
        color: #2c3e50;
        background: white;
        transition: all 0.35s;
    }

    .tab-close {
        display: flex;
        justify-content: flex-end;
        padding: 1em;
        font-size: 0.75em;
        background: #2c3e50;
        cursor: pointer;
    }

    .tab-close:hover {
        background: #1a252f;
    }

    input:checked+.tab-label::after {
        transform: rotate(90deg);
    }

    input:checked~.tab-content {
        max-height: 100%;
        padding: 1em;
    }

    input#last_date_created,
    .button {
        position: relative;
        opacity: 1;
        z-index: 1;
    }
</style>

<script>
	$(document).ready(function ($) {
    getDropdownOptions("task_procedure_list", "optionDropdown0")

		$(".sortColumn").click(function(){
			var currentColumn	=	$(this).attr("sortcolumn");
			var previousColumn	=	$("#order_column").val();
			$("#order_column").val(currentColumn);
			if(currentColumn!=previousColumn){
				$("#order_type").val("ASC");
			}
			$("#EditView").submit();
		});

    $('#category_filter').change(function() {
         
        if (this.value == "is" || this.value == "is_not") {
            getDropdownOptions("task_procedure_list", "optionDropdown0");
            $("#optionDropdown0").removeAttr("multiple");
            $("#optionDropdown0").attr("name","category_status");
        } else if(this.value == "one_of" || this.value == "not_one_of"){
          getDropdownOptions("task_procedure_list", "optionDropdown0");
          $("#optionDropdown0").attr("name","category_status[]");
          $("#optionDropdown0").attr("multiple","multiple");
        }else {
          $( "#optionDropdown0" ).val('');
          $( "#multicatfilter" ).val('');
          $("#optionDropdown0").hide();
        }
    });
 
    var categoryfilter = $( "#category_filter" ).val();
    var categoryStatus = $( "#optionDropdown0" ).val();
     
    if(categoryfilter == "empty" || categoryfilter == "not_empty"){
      $("#optionDropdown0").hide();
    }else if(categoryfilter == "one_of" || categoryfilter == "not_one_of"){
      $("#optionDropdown0").attr("name","category_status[]");
      $("#optionDropdown0").attr("multiple","multiple");
    }else if (categoryfilter == "is" || categoryfilter == "is_not") {
      $("#optionDropdown0").removeAttr("multiple");
      $("#optionDropdown0").attr("name","category_status");
    }

  });
 
  function displayVals() {

    var category_filter = $( "#category_filter" ).val();
    var multipleValues = $( "#optionDropdown0" ).val() || [];
    if(category_filter == "one_of" || category_filter == "not_one_of"){
      if(multipleValues != ""){
        $( "#multicatfilter" ).val(multipleValues.join( "," ));
      }
    }
  }
  function getDropdownOptions(list, id) {

        var statusOptions = SUGAR.language.languages.app_list_strings[list];
        var status_dd = "";

        for (var key in statusOptions) {
            
            status_dd += '<option value="' + key + '">' + statusOptions[key] + '</option>';
        }        

        $('#' + id).show();

    }  
</script>

{/literal}
<br>
<h2>Daily Scheduling Needs - LA </h2>
<br>
<div class="row">
    <div class="col">
        <div class="tabs">
            
            <div class="tab">
                <input type="checkbox" id="chck2" checked >
                <label class="tab-label" for="chck2"><h2>Deceased Animals to Remove from Schedule</h2></label>
                <div class="tab-content">
                    <form action="#bwc/index.php?module=Reports&action=DailySchedulingNeeds" method="post" name="EditView" id="EditView">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td valign="top" width="90%">
                                        <div id="filters_tab" style="display:''">
                                            <div scope="row">
                                                <h3>Run-time Filters:<span valign="bottom">&nbsp;<img border="0" onclick="return SUGAR.util.showHelpTips(this,'Specify values for <b>Run-time Filters</b> and click the <b>Apply Filters</b> button to re-run the report.' );" src="themes/RacerX/images/helpInline.png?v=kJf5Yxi324HatVAqLJY46g" alt="Information" class="inlineHelpTip"></span>                                                </h3>
                                            </div>
                                            <table id="filters_top" border="0" cellpadding="0" cellspacing="0">
                                                <tbody id="filters">
                                                    <tr id="rowid1">
                                                        <td>&nbsp;&nbsp;&nbsp;Communications</td>
                                                        <td>&nbsp;&gt;&nbsp;Date Created</td>
                                                        <td>&nbsp;&gt;&nbsp;<select name="qualify" onchange="filterTypeChanged(1);">
                                                            <option value="tp_last_n_days" selected="">Last # Days</option>
                                                          </select>
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <input type="text" name="last_date_created" id="last_date_created" size="30" autocomplete="off" value="{$COMMDATA.last_date_created}">
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="submit_form" id="submit_form" value="1">
                                                            <input type="hidden" name="order_type" id="order_type" value="{$ORDER_TYPE}">
                                                            <input type="hidden" name="order_column" id="order_column" value="{$ORDER_COLUMN}">
                                                            <input type="submit" class="button" id="ApplyBtn" title="Apply" value="Apply">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                    {if is_array($ReportCommData) && count($ReportCommData) > 0}
                    <table id="report_table" width="100%">
                        <tbody>
                            <tr>
                                <th align="center" valign="middle" sortcolumn="m06_error_name" class="reportSortData sortColumn">Name&nbsp;</th>
                                <th align="center" valign="middle" sortcolumn="wp_name" class="reportSortData sortColumn">APS Work Product ID&nbsp; </th>
                                <th align="center" valign="middle" sortcolumn="tsname" class="reportSortData sortColumn"> APS ID &nbsp; </th>
                                <th align="center" valign="middle" sortcolumn="usdaid" class="reportSortData sortColumn"> USDA ID&nbsp; </th>
                            </tr>
                            {foreach from=$ReportCommData key=index item=line_item}
                            <tr>
                                <td class="custom"> <a target="_blank" href="{$SITE_URL}/#M06_Error/{$line_item.primaryid}">{$line_item.m06_error_name}</a></td>
                                <td class="custom "> <a target="_blank" href="{$SITE_URL}/#M03_Work_Product/{$line_item.wpid}">{$line_item.wp_name} </a> </td>
                                <td class="custom "> <a target="_blank" href="{$SITE_URL}/#ANML_Animals/{$line_item.tsid}">{$line_item.tsname}</a></td>
                                <td class="custom ">  {$line_item.usdaid} </td>
                            </tr>
                            {/foreach}

                        </tbody>
                    </table>
                    {else}
                    <table id="report_table">
                        <tbody>
                            <tr>
                                <td style="text-align: center;">No Data Available</td>
                            </tr>
                        </tbody>
                    </table>
                    {/if}
                </div>
            </div>
            <div class="tab">
                <input type="checkbox" id="chck3" checked >
                <label class="tab-label" for="chck3"><h2>Task List: Schedule Matrix Review</h2></label>
                <div class="tab-content">
                    {if is_array($REPORTDATATASK) && count($REPORTDATATASK) > 0}

                    {foreach from=$REPORTDATATASK key=index item=line_item2}

                    <table id="combo_summary_div_0" width="100%" border="0" cellpadding="0" cellspacing="0"
                        class="reportGroupViewTable">
                        <tbody>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                        class="reportGroupNByTable">
                                        <tbody>
                                            <tr height="20">
                                                <th align="left" id="Id_0" name="Id_0" class="reportGroup1ByTableEvenListRowS1" valign="middle" nowrap=""> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Day: Send/Due Date = {$line_item2.tasks_due_date} </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                        class="reportGroupByDataTableHeader">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                                        class="reportDataChildtablelistView" id="report_table">
                                                        <tbody>
                                                            <tr height="20">
                                                                <th scope="col" align="center" sortcolumn="tasks_name" class="reportSortData sortColumn" valign="middle" nowrap=""> Schedule Matrix Review </th>
                                                                <th scope="col" align="center" sortcolumn="tasks_date_due_time" class="reportSortData sortColumn" valign="middle" nowrap="">Due Date &nbsp;</th>
                                                                <th scope="col" align="center" sortcolumn="asstof_name" class="reportSortData sortColumn" valign="middle" nowrap=""> Assigned To &nbsp;</a></th>
                                                            </tr>
                                                            {if is_array($ReportTaskData) && count($ReportTaskData) > 0}
                                                            {foreach from=$ReportTaskData key=index1 item=line_item}
                                                            {if $line_item2.tasks_due_date == $line_item.tasks_date_due_time1 }
                                                            <tr>
                                                                <td class="custom "><a target="_blank" href="{$SITE_URL}/#Tasks/{$line_item.primaryid}"> {$line_item.tasks_name}</a> </td>
                                                                <td class="custom ">{$line_item.tasks_date_due_time} </td>
                                                                <td class="custom "><a target="_blank" href="{$SITE_URL}/#bwc/index.php?action=DetailView&module=Users&record={$line_item.asstoid}">{$line_item.asstof_name} {$line_item.asstol_name}</a> </td>

                                                            </tr>
                                                            {/if}
                                                            {/foreach}
                                                            {else}
                                                            <tr>
                                                                <td colspan="6" style="text-align: center;">No Data Available</td>
                                                            </tr>
                                                            {/if}
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                   
                    {/foreach}
                    {else}
                    <table id="report_table">
                        <td style="text-align: center;">No Data Available</td>
                        </tr>
                    </table>

                    {/if}
                </div>
            </div>
            <div class="tab">
                <input type="checkbox" id="chck1" checked>
                <label class="tab-label" for="chck1"><h2>Scheduling Needed </h2></label>
                <div class="tab-content">
                    <form action="#bwc/index.php?module=Reports&action=DailySchedulingNeeds" method="post" name="EditView" id="EditView">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td valign="top" width="90%">
                                        <div id="filters_tab" style="display:''">
                                            <div scope="row">
                                                <h3>Run-time Filters:<span valign="bottom">&nbsp;<img border="0" onclick="return SUGAR.util.showHelpTips(this,'Specify values for <b>Run-time Filters</b> and click the <b>Apply Filters</b> button to re-run the report.' );" src="themes/RacerX/images/helpInline.png?v=kJf5Yxi324HatVAqLJY46g" alt="Information" class="inlineHelpTip"></span></h3>
                                            </div>
                                            <table id="filters_top" border="0" cellpadding="0" cellspacing="0">
                                                <tbody id="filters">
                                                    <tr id="rowid0">
                                                    <td>

                                                    </td>
                                                    <td>
                                                        &nbsp;&nbsp;&nbsp;

                                                        Task Design
                                                    </td>
                                                    <td>

                                                    </td>
                                                    <td>&nbsp;&gt;&nbsp;Category</td>
                                                    <td>
                                                        &nbsp;&gt;&nbsp;
                                                        <select id="category_filter" name="category_filter" title="select filter qualifier">
                                                            <option value="is" {if $category_filter == "is"} selected {/if}>Is</option>
                                                            <option value="is_not" {if $category_filter == "is_not"} selected {/if}>Is Not</option>
                                                            <option value="one_of" {if $category_filter == "one_of"} selected {/if}>Is One Of</option>
                                                            <option value="not_one_of" {if $category_filter == "not_one_of"} selected {/if}>Is Not One Of</option>
                                                            <option value="empty" {if $category_filter == "empty"} selected {/if}>Is Empty</option>
                                                            <option value="not_empty" {if $category_filter == "not_empty"} selected {/if}>Is Not Empty</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    
                                                                    <select name="category_status" id="optionDropdown0" onchange="displayVals();" {if $multicatfilter != ""} multiple="multiple" {/if}>
                                                                        <option value="Task" {if 'Task'|in_array:$category_status} selected {/if} >Task</option>
                                                                        <option value="Procedure" {if 'Procedure'|in_array:$category_status} selected {/if}>Procedure</option>
                                                                        <option value="Unscheduled Task" {if 'Unscheduled Task'|in_array:$category_status} selected {/if}>Unscheduled Task</option> 
                                                                    </select>
                                                                    <input type="hidden" name="multicatfilter" id="multicatfilter" value="{$multicatfilter}"> 
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        
                                                        <input type="hidden" name="submitform" id="submitform" value="1"> 
                                                        <input type="submit" class="button" id="ApplyBtn1" title="Apply" value="Apply">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                    {if is_array($REPORTDATA2) && count($REPORTDATA2) > 0}

                    {foreach from=$REPORTDATA2 key=index item=line_item2}

                    <table id="combo_summary_div_0" width="100%" border="0" cellpadding="0" cellspacing="0"
                        class="reportGroupViewTable">
                        <tbody>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportGroupNByTable">
                                        <tbody>
                                            <tr height="20">
                                                <th align="left" id="Id_0" name="Id_0" class="reportGroup1ByTableEvenListRowS1" valign="middle" nowrap=""> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APS Work Product ID ={$line_item2.work_product_name}</th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                        class="reportGroupByDataTableHeader">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                                        class="reportDataChildtablelistView" id="report_table">
                                                        <tbody>
                                                            <tr height="20">
                                                                <th scope="col" align="center" sortcolumn="l1_name" class="reportSortData sortColumn" valign="middle" nowrap=""> Name </th>
                                                                <th scope="col" align="center" sortcolumn="order" class="reportSortData sortColumn" valign="middle" nowrap=""> Order </th>
                                                                <th scope="col" align="center" sortcolumn="time_window" class="reportSortData sortColumn" valign="middle" nowrap=""> 	Time Window </th>
                                                                <th scope="col" align="center" sortcolumn="l2_name" class="reportSortData sortColumn" valign="middle" nowrap=""> Parent Procedure/Task </th>
                                                                <th scope="col" align="center" sortcolumn="planned_start_datetime_1st" class="reportSortData sortColumn" valign="middle" nowrap="">Planned Start Date/Time (1st Tier) &nbsp; </th>
                                                                <th scope="col" align="center" sortcolumn="planned_end_datetime_1st" class="reportSortData sortColumn" valign="middle" nowrap=""> Planned End Date/Time (1st Tier) &nbsp;</a></th>
                                                                <th scope="col" align="center" sortcolumn="planned_start_datetime_2nd" class="reportSortData sortColumn" valign="middle" nowrap=""> Planned Start Date/Time (2nd Tier) &nbsp;</a></th>
                                                                <th scope="col" align="center" sortcolumn="planned_end_datetime_2nd" class="reportSortData sortColumn" valign="middle" nowrap=""> Planned End Date/Time (2nd Tier) &nbsp;</a></th>
                                                            </tr>
                                                            {if is_array($ReportData) && count($ReportData) > 0}
                                                            {foreach from=$ReportData key=index1 item=line_item}
                                                            {if $line_item2.work_product_name == $line_item.m03_work_product_name }
                                                            <tr>
                                                                <td width="13%" class="custom "><a target="_blank" href="{$SITE_URL}/#TaskD_Task_Design/{$line_item.l1_id}">{$line_item.l1_name}</a> </td>
                                                                <td width="13%" class="custom number_align">{$line_item.order}</td>
                                                                <td width="13%" class="custom ">{$line_item.time_window}</td>
                                                                <td width="13%" class="custom "><a target="_blank" href="{$SITE_URL}/#TaskD_Task_Design/{$line_item.l2_id}">{$line_item.l2_name}</a> </td>
                                                                <td width="13%" class="custom ">{$line_item.planned_start_datetime_1st}</td>
                                                                <td width="13%" class="custom ">{$line_item.planned_end_datetime_1st}</td>
                                                                <td width="13%" class="custom ">{$line_item.planned_start_datetime_2nd}</td>
                                                                <td width="13%" class="custom ">{$line_item.planned_end_datetime_2nd}</td>
                                                            </tr>
                                                            {/if}
                                                            {/foreach}
                                                            {else}
                                                            <tr>
                                                                <td colspan="6" style="text-align: center;">No Data Available</td>
                                                            </tr>
                                                            {/if}
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>                    
                    {/foreach}
                    {else}
                    <table id="report_table">
                        <td style="text-align: center;">No Data Available</td>
                        </tr>
                    </table>

                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>