{literal}
<style>
    #report_table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        table-layout: fixed;
    }

    #report_table th {
        font-weight: bold;
        text-align: left;
        padding: 4px 5px 4px 5px;
        border: 1px solid;
        background: #fff;
        color: #666;
        font-size: 13px;
    }

    #report_table td {
        border-color: #ccc;
        background: white;
        padding: 4px 5px;
        border: 1px solid;
    }

    #report_table tr:nth-child(even) {
        background-color: #fff;
    }

    #report_table tr:hover {
        background-color: #fff;
    }
	.sortColumn:hover{
        cursor: pointer;
        text-decoration:underline;
    }
  

</style>

<script>
	$(document).ready(function ($) {
		$(".sortColumn").click(function(){
			var currentColumn	=	$(this).attr("sortcolumn");
			var previousColumn	=	$("#order_column").val();
			$("#order_column").val(currentColumn);
			if(currentColumn!=previousColumn){
				$("#order_type").val("ASC");
			}
			$("#EditView").submit();
		});
	});
	   
	   
</script>


{/literal}

<br>
<h2>Ops Support Assignments Review</h2>
<br>
<form action="#bwc/index.php?module=Reports&action=CustomWPAAllocation" method="post" name="EditView" id="EditView">
   <table width="100%" cellspacing="0" cellpadding="0">
      <tbody>
         <tr>
            <td valign="top" width="90%">
               <div id="filters_tab" style="display:''">
                  <div scope="row">
                     <h3>Run-time Filters:<span valign="bottom">&nbsp;<img border="0" onclick="return SUGAR.util.showHelpTips(this,'Specify values for <b>Run-time Filters</b> and click the <b>Apply Filters</b> button to re-run the report.' );" src="themes/RacerX/images/helpInline.png?v=kJf5Yxi324HatVAqLJY46g" alt="Information" class="inlineHelpTip"></span></h3>
                  </div>
                  <table id="filters_top" border="0" cellpadding="0" cellspacing="0">
                     <tbody id="filters">
                        <tr id="rowid1">
                           <td>
                           </td>
                           <td>Select Date Created</td>
                           <td>&nbsp;&nbsp;</td>
                           <td>&nbsp;&nbsp;</td>                           
                           <td>
                              <input type="date" name="date_created" id="date_created" class="sqsEnabled yui-ac-input wpfilter" autocomplete="off" value="{$date_created}">
                           </td>
                           
                        </tr>
                        
                     </tbody>
                  </table>
                  <table>
                     <tbody>
                        <tr>
                           <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                           <td>
								<input type="hidden" name="submit_form" id="submit_form" value="1">
								<input type="hidden" name="order_type" id="order_type" value="{$ORDER_TYPE}">
								<input type="hidden" name="order_column" id="order_column" value="{$ORDER_COLUMN}">
								<input type="submit" class="button" id="ApplyBtn" title="Apply" value="Apply">                              
                           </td>
                           <td>
                              
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </td>
         </tr>
      </tbody>
   </table>
</form>

        <table id="report_table" width="100%">
            <tbody>
            <tr >
                <th align="center" valign="middle" sortcolumn="wpename" class="reportSortData sortColumn">WPA ID&nbsp;</th>
                <th align="center" valign="middle" sortcolumn="allocate_wp_name" class="reportSortData sortColumn"> Allocated WP&nbsp; </th>
                <th align="center" valign="middle" sortcolumn="tsname" class="reportSortData sortColumn">  Test System ID #&nbsp; </th>
                <th align="center" valign="middle" sortcolumn="usda_id_c" class="reportSortData sortColumn"> USDA ID&nbsp; </th>               
                <th align="center" valign="middle" sortcolumn="abnormality_c" class="reportSortData sortColumn"> TS Abnormality&nbsp; </th>
                <th align="center" valign="middle" sortcolumn="tsbreed_c" class="reportSortData sortColumn"> TS Breed&nbsp; </th>
                <th align="center" valign="middle" sortcolumn="tssex_c" class="reportSortData sortColumn"> TS Sex&nbsp; </th>
                <th align="center" valign="middle" sortcolumn="tsdate_of_birth_c" class="reportSortData sortColumn"> TS Date of Birth&nbsp; </th>
                <th align="center" valign="middle" sortcolumn="tsdate_received_c" class="reportSortData sortColumn"> TS Date Received&nbsp; </th>
                <th align="center" valign="middle" sortcolumn="breedstrain" class="reportSortData sortColumn"> TSD Breed/Strain&nbsp; </th>
                <th align="center" valign="middle" sortcolumn="sex" class="reportSortData sortColumn"> TSD Sex&nbsp; </th>
                <th align="center" valign="middle" sortcolumn="min_age_type" class="reportSortData sortColumn"> TSD Min Age Type&nbsp; </th>
                <th align="center" valign="middle" sortcolumn="max_age_type" class="reportSortData sortColumn"> TSD Max Age Type&nbsp; </th>
                <th align="center" valign="middle" sortcolumn="prolonged_restraint_acc_c" class="reportSortData sortColumn"> TSD Prolonged Restraint Acclimation Required&nbsp; </th>
                           
                               
            </tr>

             {if is_array($ReportData) && count($ReportData) > 0}
            {foreach from=$ReportData key=index item=line_item}
            <tr >
                <td width="10%" > 
                 <a target="_blank" href="{$SITE_URL}/#WPE_Work_Product_Enrollment/{$line_item.wpeid}">{$line_item.wpename} </a>
                </td>
                <td width="10%" >
                   {$line_item.allocate_wp_name}  
                </td>
                <td width="10%" >
                 <a target="_blank" href="{$SITE_URL}/#ANML_Animals/{$line_item.tsid}">{$line_item.tsname}</a>
               </td>
                <td width="10%" >
                    {$line_item.usda_id_c}
                </td>
               
                <td width="10%" >
                    {$line_item.abnormality_c}
                </td>
                <td width="10%" >
                    {$line_item.tsbreed_c}
                </td>
                <td width="10%" >
                    {$line_item.tssex_c}
                </td>
                <td width="10%" >
                    {$line_item.tsdate_of_birth_c}
                </td>
                <td width="10%" >
                    {$line_item.tsdate_received_c}
                </td>
                 <td width="10%" >
                    {$line_item.breedstrain}
                </td>
                 <td width="10%" >
                    {$line_item.sex}
                </td>
                 <td width="10%" >
                    {$line_item.min_age_type}
                </td>
                 <td width="10%" >
                    {$line_item.max_age_type}
                </td>
                 <td width="10%" >
                    {$line_item.prolonged_restraint_acc_c}
                </td>
                
                
            </tr>
            {/foreach}
            
            {else}
            <tr>
                <td colspan="14" style="text-align: center;">No Data Available</td>
            </tr>
            {/if}
           
            </tbody>
        </table>
