{literal}
<style>
    #report_table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        table-layout: fixed;
    }

    #report_table th {
        font-weight: bold;
        text-align: left;
        padding: 4px 5px 4px 5px;
        border: 1px solid;
        background: #fff;
        color: #666;
        font-size: 13px;
    }

    #report_table td {
        border-color: #ccc;
        background: white;
        padding: 4px 5px;
        border: 1px solid;
    }

    #report_table tr:nth-child(even) {
        background-color: #fff;
    }

    #report_table tr:hover {
        background-color: #fff;
    }
  

</style>


{/literal}

<br>
<h2>Custom WPA/Allocation Report</h2>
<br>
<form action="#bwc/index.php?module=Reports&action=CustomWPAAllocation" method="post" name="EditView" id="EditView">
   <table width="100%" cellspacing="0" cellpadding="0">
      <tbody>
         <tr>
            <td valign="top" width="90%">
               <div id="filters_tab" style="display:''">
                  <div scope="row">
                     <h3>Run-time Filters:<span valign="bottom">&nbsp;<img border="0" onclick="return SUGAR.util.showHelpTips(this,'Specify values for <b>Run-time Filters</b> and click the <b>Apply Filters</b> button to re-run the report.' );" src="themes/RacerX/images/helpInline.png?v=kJf5Yxi324HatVAqLJY46g" alt="Information" class="inlineHelpTip"></span></h3>
                  </div>
                  <table id="filters_top" border="0" cellpadding="0" cellspacing="0">
                     <tbody id="filters">
                        <tr id="rowid1">
                           <td>
                           </td>
                           <td>Select Date Created</td>
                           <td>&nbsp;&nbsp;</td>
                           <td>&nbsp;&nbsp;</td>                           
                           <td>
                              <table>
                                 <tbody>
                                    <tr>
                                    
                                       <td>
                                            <input type="date" name="date_created"
                                                   id="date_created"
                                                   class="sqsEnabled yui-ac-input wpfilter" autocomplete="off"
                                                   value="{$date_created}">
                                       </td>
                                      
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                           <td></td>
                        </tr>
                        
                     </tbody>
                  </table>
                  <table>
                     <tbody>
                        <tr>
                           <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                           <td>
                              <input type="hidden" name="submit_form" id="submit_form" value="1">
                              <input type="submit" class="button" id="ApplyBtn" title="Apply" value="Apply">                              
                           </td>
                           <td>
                              
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </td>
         </tr>
      </tbody>
   </table>
</form>

        <table id="report_table" width="100%">
            <tbody>
            <tr >
                <th  align="center" valign="middle">
                    WPA ID&nbsp;
                </th>
                <th  align="center" valign="middle">
                    Allocated WP&nbsp;
                </th>
                <th  align="center" valign="middle">
                    Test System ID #&nbsp;
                </th>
                <th  align="center" valign="middle">
                    USDA ID&nbsp;
                </th>
                <th  align="center" valign="middle">
                    WPA Comments&nbsp;
                </th>
                <th align="center" valign="middle" >
                    TS Abnormality&nbsp;
                </th> 
                <th align="center" valign="middle" >
                    Date Created&nbsp;
                </th>               
            </tr>

             {if is_array($ReportWPEData) && count($ReportWPEData) > 0}
            {foreach from=$ReportWPEData key=index item=line_item}
            <tr >
                <td width="10%" > 
                 <a target="_blank" href="{$SITE_URL}/#WPE_Work_Product_Enrollment/{$line_item.wpeid}">
                   {$line_item.wpename} 
                  </a>
                </td>
                <td width="10%" >
                   &nbsp; 
                </td>
                <td width="10%" >
                 <a target="_blank" href="{$SITE_URL}/#ANML_Animals/{$line_item.tsid}">
                   {$line_item.tsname}
                   </a>
               </td>
                <td width="10%" >
                    {$line_item.usda_id_c}
                </td>
                <td width="10%" >
                   {$line_item.comments_c}
                </td>
                <td width="10%" >
                    {$line_item.abnormality_c}
                </td>
                <td width="10%" >
                    {$line_item.date_entered}
                </td>
                
            </tr>
            {/foreach}
            {foreach from=$ReportTSData key=index item=line_item}
            <tr >
                <td width="10%" > 
                    &nbsp;
                </td>
                <td width="10%" >
                 <a target="_blank" href="{$SITE_URL}/#M03_Work_Product/{$line_item.allocate_wp_id}">
                  {$line_item.allocate_wp_name}
                  </a> 
                </td>
                <td width="10%" >
                <a target="_blank" href="{$SITE_URL}/#ANML_Animals/{$line_item.ts_id}">
                   {$line_item.tsname}
                 </a>
               </td>
                <td width="10%" >
                    {$line_item.usda_id_c}
                </td>
                <td width="10%" >
                   &nbsp;
                </td>
                <td width="10%" >
                    {$line_item.abnormality_c}
                </td>
                 <td width="10%" >
                    {$line_item.date_created}
                </td>
                
            </tr>
            {/foreach}
            {else}
            <tr>
                <td colspan="6" style="text-align: center;">No Data Available</td>
            </tr>
            {/if}
           
            </tbody>
        </table>
