{$GRAPH_RESOURCES}
{$js_array}
{literal}
<style>
	.select2-container{
		width : 200px;
	}
	#report_table {
	  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	  table-layout: fixed;
	}
	#report_table th {
    font-weight: bold;
    text-align: left;
    padding: 4px 5px 4px 5px;
    border: 1px solid;
    background: #fff;
    color: #666;
    font-size: 13px;
   }
 	#report_table td {
	border-color: #ccc;
    background: white;
    padding: 4px 5px;
	border: 1px solid;
	}
	
	#report_table tr:nth-child(even){background-color: #fff;}
	#report_table tr:last-child td {border-bottom:1px solid;}
	#report_table tr:hover {background-color: #fff;}
	.wpfilter {border: 0; background: #F7F8F9; padding-left: 6px;width:100px !important;}


</style>
<script>
$(document).ready(function(){
$('#work_product_group_name:text').on('input', function(){ 
	work_product_group_id = $('#work_product_group_id').val(''); 
});
	$('#ApplyBtn').click(function(){
		work_product_group_id = $('#work_product_group_id').val();
		work_product_group_id_1 = $('#work_product_group_id_1').val();
		if(work_product_group_id != '' || work_product_group_id_1 != ''){
			window.location.href = "#bwc/index.php?module=Reports&action=CustomCapacity";
			return true;
		} 
		else if(work_product_group_id == '' || work_product_group_id_1 != '')
		{
			alert('"Work Product Group: Work Product Group ID" missing input value');
			return false;
		}
		
	});
	
	$('#SelectBtn').click(function(){
		open_popup(
			"WPG_Work_Product_Group", 
			600, 
			400, 
			"", 
			true, 
			false, 
			{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"work_product_group_id","name":"work_product_group_name"}}, 
			"single", 
			true
		);		
	});
	$('#SelectBtn_1').click(function(){
	open_popup(
		"WPG_Work_Product_Group", 
		600, 
		400, 
		"", 
		true, 
		false, 
		{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"work_product_group_id_1","name":"work_product_group_name_1"}}, 
		"single", 
		true
	);	
   });
   $('#SelectBtn_2').click(function(){
	open_popup(
		"WPG_Work_Product_Group", 
		600, 
		400, 
		"", 
		true, 
		false, 
		{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"work_product_group_id_2","name":"work_product_group_name_2"}}, 
		"single", 
		true
	);	
   });
   $('#SelectBtn_3').click(function(){
	open_popup(
		"WPG_Work_Product_Group", 
		600, 
		400, 
		"", 
		true, 
		false, 
		{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"work_product_group_id_3","name":"work_product_group_name_3"}}, 
		"single", 
		true
	);	
   });
   $('#SelectBtn_4').click(function(){
	open_popup(
		"WPG_Work_Product_Group", 
		600, 
		400, 
		"", 
		true, 
		false, 
		{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"work_product_group_id_4","name":"work_product_group_name_4"}}, 
		"single", 
		true
	);	
   });
});

</script>  
<script>SUGAR.loadChart = true;</script>
{/literal}


<!--<table id="report_table" cellpadding="0" cellspacing="0" border="0" width="100%" >-->
<h2>Custom Capacity Report</h2>
<br>
<form id="EditView" name="EditView" method="post" action="#bwc/index.php?module=Reports&action=CustomCapacity">
<div id="filters_tab" style="display:''">
                  <div scope="row">
                     <h3>Run-time Filters:<span valign="bottom">&nbsp;<img border="0" onclick="return SUGAR.util.showHelpTips(this,'Specify values for <b>Run-time Filters</b> and click the <b>Apply Filters</b> button to re-run the report.' );" src="themes/RacerX/images/helpInline.png?v=kJf5Yxi324HatVAqLJY46g" alt="Information" class="inlineHelpTip"></span></h3>
                  </div>
<table  id="filters_top" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td>
				<input type="hidden" name="work_product_group_id" id="work_product_group_id" value="{$WPDATA.id}">
				Work Product Group Filter: <input type="text" name="work_product_group_name" id="work_product_group_name" class="sqsEnabled yui-ac-input wpfilter" autocomplete="off" style="width: auto !important;" value="{$WPDATA.name}" readonly>
			</td>
			<td>
				<input title="Select" type="button" value="Select Work Product Group" class="button_name" id="SelectBtn">
			</td>
		</tr>
		<tr>
			<td>
				<input type="hidden" name="work_product_group_id_1" id="work_product_group_id_1" value="{$WPDATA.id_1}">
				Work Product Group Filter: <input type="text" name="work_product_group_name_1" id="work_product_group_name_1" class="sqsEnabled yui-ac-input wpfilter" autocomplete="off" style="width: auto !important;" value="{$WPDATA.name_1}" readonly>
			</td>
			<td>
				<input title="Select" type="button" value="Select Work Product Group" class="button_name" id="SelectBtn_1">
			</td>
		</tr>
		<tr>
			<td>
				<input type="hidden" name="work_product_group_id_2" id="work_product_group_id_2" value="{$WPDATA.id_2}">
				Work Product Group Filter: <input type="text" name="work_product_group_name_2" id="work_product_group_name_2" class="sqsEnabled yui-ac-input wpfilter" autocomplete="off" style="width: auto !important;" value="{$WPDATA.name_2}" readonly>
			</td>
			<td>
				<input title="Select" type="button" value="Select Work Product Group" class="button_name" id="SelectBtn_2">
			</td>
		</tr>
		<tr>
			<td>
				<input type="hidden" name="work_product_group_id_3" id="work_product_group_id_3" value="{$WPDATA.id_3}">
				Work Product Group Filter: <input type="text" name="work_product_group_name_3" id="work_product_group_name_3" class="sqsEnabled yui-ac-input wpfilter" autocomplete="off" style="width: auto !important;" value="{$WPDATA.name_3}" readonly>
			</td>
			<td>
				<input title="Select" type="button" value="Select Work Product Group" class="button_name" id="SelectBtn_3">
			</td>
		</tr>
		<tr>
			<td>
				<input type="hidden" name="work_product_group_id_4" id="work_product_group_id_4" value="{$WPDATA.id_4}">
				Work Product Group Filter: <input type="text" name="work_product_group_name_4" id="work_product_group_name_4" class="sqsEnabled yui-ac-input wpfilter" autocomplete="off" style="width: auto !important;" value="{$WPDATA.name_4}" readonly>
			</td>
			<td>
				<input title="Select" type="button" value="Select Work Product Group" class="button_name" id="SelectBtn_4">
			</td>
		</tr>
		<tr>
			<td>
				<input title="Apply" type="submit" value="Apply" class="button_name" id="ApplyBtn">
			</td>
		</tr>
		
	</tbody>
</table>
</div>
</form>
<br>

{if $IS_CHART==1}
<div style="width:100%; max-height:550px; overflow: scroll;">
{$LEADS_CHART_DATA}
</div>
{/if}

<table id="report_table" width="100%">
	{foreach from=$ReportData key=index item=wpgItems}
    <tr>       
    	{foreach from=$wpgItems key=index1 item=line_item}
        <td width="10%">{$line_item}</td>
        {/foreach}
    </tr>
	{/foreach}
</table>