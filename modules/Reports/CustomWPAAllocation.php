<?php
require_once('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config, $app_list_strings, $db, $current_user;
$ss = new Sugar_Smarty();

$ORDER_TYPE     = "ASC";
$ORDER_COLUMN   = "wpename";  


if (isset($_REQUEST['date_created']) && $_REQUEST['date_created'] != ''  && $_REQUEST['submit_form'] == 1) {
    $date_created		= $_REQUEST['date_created'];
    $date_created_start	= $date_created .' 02:00:00';
    $date_created_end	= $date_created .' 23:59:00';
		
	if($_REQUEST["order_type"]!="" && $_REQUEST["order_type"]!=""){
		$ORDER_TYPE     = $_REQUEST["order_type"];
		$ORDER_COLUMN   = $_REQUEST["order_column"];
	}
}

 


/** Get all Work Product Assignment records with Date Created = run time filter date */
 
	$sql = "SELECT 
				WPE.id AS wpeid,
				WPE.name AS wpename,
				WPE.date_entered,
				WPECSTM.id_c,
				WPECSTM.comments_c,
				WPEANML.id,
				WPEANML.anml_animals_wpe_work_product_enrollment_1anml_animals_ida,
				WPEANML.anml_anima9941ollment_idb,
				tbl_anml.id AS tsid,
				tbl_anml.name AS tsname,tbl_anmlcstm.breed_c AS tsbreed_c,tbl_anmlcstm.sex_c AS tssex_c,tbl_anmlcstm.date_of_birth_c AS tsdate_of_birth_c,tbl_anmlcstm.date_received_c AS tsdate_received_c,
				tbl_species.name AS TSspecies,
				WPEWP.id,
				WPEWP.m03_work_p7d13product_ida,
				WPEWP.m03_work_p9bf5ollment_idb,
				tbl_wp.id AS wpid,
				tbl_wp.name AS wpname
			FROM wpe_work_product_enrollment AS WPE
			LEFT JOIN wpe_work_product_enrollment_cstm AS WPECSTM on WPECSTM.id_c = WPE.id
			LEFT JOIN anml_animals_wpe_work_product_enrollment_1_c AS WPEANML ON WPEANML.anml_anima9941ollment_idb = WPE.id
			LEFT JOIN anml_animals AS tbl_anml ON tbl_anml.id = WPEANML.anml_animals_wpe_work_product_enrollment_1anml_animals_ida
			LEFT JOIN anml_animals_cstm AS tbl_anmlcstm ON tbl_anml.id = tbl_anmlcstm.id_c
			LEFT JOIN m03_work_product_wpe_work_product_enrollment_1_c AS WPEWP ON WPEWP.m03_work_p9bf5ollment_idb = WPE.id
			LEFT JOIN m03_work_product AS tbl_wp ON tbl_wp.id = WPEWP.m03_work_p7d13product_ida
			LEFT JOIN s_species tbl_species ON tbl_species.id = tbl_anmlcstm.s_species_id_c AND IFNULL(tbl_species.deleted,0)=0
			WHERE WPE.date_entered between '".$date_created_start."' and '".$date_created_end."' and WPE.deleted=0 and tbl_species.name NOT IN ('Chinchilla','Guinea Pig','Hamster','Lagomorph','Mouse','Rat')";

	$ReportWPEData	= array();
	$wpedata		= array();
	$result			= $db->query($sql);
	while ($row = $db->fetchByassoc($result)) {
		$wpedata['wpeid']					= $row['wpeid'];
		$wpedata['wpename']					= $row['wpename'];
		$wpedata['allocate_wp_id']			= "";
		$wpedata['allocate_wp_name'] 		= "";
		$wpedata['comments_c']				= $row['comments_c'];
		$wpedata['tsid']					= $row['tsid'];
		$wpedata['tsname']					= $row['tsname'];
		$wpedata['tsbreed_c']				= $row['tsbreed_c'];
		$wpedata['tssex_c']					= $row['tssex_c'];
		$wpedata['tsdate_of_birth_c']		= $row['tsdate_of_birth_c'];
		$wpedata['tsdate_received_c']		= $row['tsdate_received_c'];
		$wpedata['wpid']					= $row['wpid'];
		$wpid  								= $row['wpid'];
		$wpedata['wpname']					= $row['wpname'];
		/**#476 : Custom Report Changes : 28 nov 2022 : To fetch latst test system design records liked in wp of wpe */
		$sql_TSD = "SELECT tbl_tsd.id as tsdid
		,tbl_tsd.name as tsdname,tbl_tsd.breedstrain as breedstrain,tbl_tsd.sex as sex,tbl_tsd.min_age_type as min_age_type,tbl_tsd.max_age_type as max_age_type, tbl_tsdcstm.prolonged_restraint_acc_c as prolonged_restraint_acc_c, tbl_tsdcstm.acclimation_required_c as acclimation_required_c,tbl_species.name AS TSspecies
		FROM tsd1_test_system_design_1 AS tbl_tsd
		LEFT JOIN  m03_work_product_tsd1_test_system_design_1_1_c AS tbl_tsd_wp ON tbl_tsd.id=tbl_tsd_wp.m03_work_p4745esign_1_idb 
		LEFT JOIN  m03_work_product AS tbl_wp ON tbl_wp.id=tbl_tsd_wp.m03_work_product_tsd1_test_system_design_1_1m03_work_product_ida AND tbl_wp.deleted=0
		LEFT JOIN tsd1_test_system_design_1_cstm AS tbl_tsdcstm ON tbl_tsd.id = tbl_tsdcstm.id_c
		LEFT JOIN s_species tbl_species ON tbl_species.id = tbl_tsd.s_species_id_c AND IFNULL(tbl_species.deleted,0)=0
		 WHERE tbl_wp.id='".$wpid."' AND tbl_tsd_wp.deleted=0 and tbl_species.name NOT IN ('Chinchilla','Guinea Pig','Hamster','Lagomorph','Mouse','Rat') order by tbl_tsd_wp.date_modified desc LIMIT 1";

		//$GLOBALS['log']->fatal('sql query to fetch tsd from wp :' . $sql_TSD);
		$result_TSD		= $db->query($sql_TSD);
		$rowTSD 		= $db->fetchByAssoc($result_TSD);
		$wpedata['tsdid'] 							= $rowTSD['tsdid'];
		$wpedata['tsdname'] 						= $rowTSD['tsdname'];
		$wpedata['breedstrain'] 					= $rowTSD['breedstrain'];
		$wpedata['sex'] 							= $rowTSD['sex'];
		$wpedata['min_age_type'] 					= $rowTSD['min_age_type'];
		$wpedata['max_age_type'] 					= $rowTSD['max_age_type'];
		$wpedata['prolonged_restraint_acc_c'] 		= $rowTSD['prolonged_restraint_acc_c'];
		$wpedata['acclimation_required_c'] 			= $rowTSD['acclimation_required_c'];

		 /*********************************************************************************************************** */
				
		$TSmBean		= BeanFactory::retrieveBean('ANML_Animals', $row['tsid']);
		$usda_id_c		= $TSmBean->usda_id_c;
		$abnormality_c	= $TSmBean->abnormality_c;
		$wpedata['usda_id_c']		= $usda_id_c;
		$wpedata['abnormality_c']	= $abnormality_c;
		array_push($ReportWPEData, $wpedata);
	}
		
	/**Get all Test Systems where the Change Date for the Allocated Work Product field = run time filter date (so for the TS row, it would be looking at the audit log). */
	/* $sql_TS = "SELECT id,parent_id,before_value_string,after_value_string from  anml_animals_audit WHERE DATE(date_created)='" . $date_created . "' AND field_name='m03_work_product_id_c' group by parent_id order by date_created DESC"; */
	$sql_TS = "SELECT id,parent_id,before_value_string,after_value_string from  anml_animals_audit 
		WHERE date_created between '".$date_created_start."' and '".$date_created_end."' AND field_name='m03_work_product_id_c' group by parent_id order by date_created DESC";
	$tsdata			= array();
	$ReportTSData	= array();
	$result_Ts		= $db->query($sql_TS);
	while ($row_ts	= $db->fetchByassoc($result_Ts)) {
		$tsdata['ts_id']			= $row_ts['parent_id'];
		$allocate_wp_id				= $row_ts['after_value_string'];
		$testSystemBean				= BeanFactory::retrieveBean('ANML_Animals', $row_ts['parent_id']);
		$tsname						= $testSystemBean->name;
		$allocate_wp_name			= $testSystemBean->allocated_work_product_c;
		$usda_id_c					= $testSystemBean->usda_id_c;
		$abnormality_c				= $testSystemBean->abnormality_c;		
		$tsdata['wpeid']			= "";
		$tsdata['wpename']			= "";
		$tsdata['allocate_wp_id']	= $allocate_wp_id;
		$tsdata['allocate_wp_name'] = $allocate_wp_name;
		$tsdata['tsname']			= $tsname;
		$tsdata['usda_id_c']		= $usda_id_c;
		$tsdata['abnormality_c']	= $abnormality_c;	
		array_push($ReportTSData, $tsdata);
				
	}
		
	$ReportData = array_merge($ReportWPEData, $ReportTSData);
		
	$ot="";
	if($ORDER_TYPE=="ASC"){
		$ot=SORT_ASC;
	}else{
		$ot=SORT_DESC;
	}
	array_multisort(array_column($ReportData, $ORDER_COLUMN), $ot, $ReportData);


$ss->assign('date_created', $date_created);
$ss->assign('ORDER_TYPE', ($ORDER_TYPE=="ASC")?"DESC":"ASC");
$ss->assign('ORDER_COLUMN', $ORDER_COLUMN);
$ss->assign('current_user', $current_user->id);
$ss->assign('ReportData', $ReportData);
//$ss->assign('ReportTSData', $ReportTSData);
$ss->assign('SITE_URL', $GLOBALS['sugar_config']['site_url']);
$html = $ss->fetch("custom/modules/Reports/tpls/CustomWPAAllocation.tpl");
echo $html;
