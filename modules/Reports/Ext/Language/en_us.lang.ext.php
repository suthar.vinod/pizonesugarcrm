<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Reports/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Company';
$mod_strings['LBL_ACCOUNTS'] = 'Companies';
$mod_strings['LBL_ACCOUNT'] = 'Company';
$mod_strings['LBL_ACCOUNT_REPORTS'] = 'Company Reports';
$mod_strings['LBL_MY_TEAM_ACCOUNT_REPORTS'] = 'My Team&#039;s Company Reports';
$mod_strings['LBL_MY_ACCOUNT_REPORTS'] = 'My Company Reports';
$mod_strings['LBL_PUBLISHED_ACCOUNT_REPORTS'] = 'Published Company Reports';
$mod_strings['DEFAULT_REPORT_TITLE_3'] = 'Partner Company List';
$mod_strings['DEFAULT_REPORT_TITLE_4'] = 'Customer Company List';
$mod_strings['DEFAULT_REPORT_TITLE_16'] = 'Companies By Type By Industry';
$mod_strings['DEFAULT_REPORT_TITLE_43'] = 'Customer Company Owners';
$mod_strings['DEFAULT_REPORT_TITLE_44'] = 'My New Customer Companies';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Create Opportunity';
$mod_strings['LBL_OPPORTUNITIES'] = 'Opportunities';
$mod_strings['LBL_OPPORTUNITY'] = 'Opportunity';
$mod_strings['LBL_OPPORTUNITY_REPORTS'] = 'Opportunity Reports';
$mod_strings['LBL_MY_TEAM_OPPORTUNITY_REPORTS'] = 'My Team&#039;s Opportunity Reports';
$mod_strings['LBL_MY_OPPORTUNITY_REPORTS'] = 'My Opportunity Reports';
$mod_strings['LBL_PUBLISHED_OPPORTUNITY_REPORTS'] = 'Published Opportunity Reports';
$mod_strings['DEFAULT_REPORT_TITLE_6'] = 'Opportunities By Lead Source';
$mod_strings['DEFAULT_REPORT_TITLE_17'] = 'Opportunities Won By Lead Source';
$mod_strings['DEFAULT_REPORT_TITLE_45'] = 'Opportunities By Sales Stage';
$mod_strings['DEFAULT_REPORT_TITLE_46'] = 'Opportunities By Type';
$mod_strings['DEFAULT_REPORT_TITLE_50'] = 'Opportunities Won By Account';
$mod_strings['DEFAULT_REPORT_TITLE_51'] = 'Opportunities Won By User';
$mod_strings['DEFAULT_REPORT_TITLE_52'] = 'All Open Opportunities';
$mod_strings['DEFAULT_REPORT_TITLE_53'] = 'All Closed Opportunities';
$mod_strings['DEFAULT_REPORT_TITLE_56'] = 'Opportunities Product Breakdown For Past, Current, Next Timeperiod By Month';
$mod_strings['DEFAULT_REPORT_TITLE_57'] = 'Opportunities Sales Stage For Past, Current, Next Timeperiod By Month';
$mod_strings['DEFAULT_REPORT_TITLE_59'] = 'Opportunities By Sales Stage For Reportees in Current Period';
$mod_strings['LNK_NEW_MEETING'] = 'Schedule APS Activity';
$mod_strings['LBL_MEETING_REPORTS'] = 'APS Activity Reports';
$mod_strings['LBL_MY_TEAM_MEETING_REPORTS'] = 'My Team&#039;s APS Activity Reports';
$mod_strings['LBL_MY_MEETING_REPORTS'] = 'My APS Activity Reports';
$mod_strings['LBL_PUBLISHED_MEETING_REPORTS'] = 'Published APS Activity Reports';
$mod_strings['DEFAULT_REPORT_TITLE_15'] = 'APS Activities By Team By User';
$mod_strings['DEFAULT_REPORT_TITLE_48'] = 'Open APS Activities';

?>
