<?php 







if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$exemptModules[] = 'SugarChimp';
$exemptModules[] = 'SugarChimpSugarBackup';
$exemptModules[] = 'SugarChimpBackupQueue';
$exemptModules[] = 'SugarChimpMC';
$exemptModules[] = 'SugarChimpMCActivity';
$exemptModules[] = 'SugarChimpMCBackup';
$exemptModules[] = 'SugarChimpMCPeople';
$exemptModules[] = 'SugarChimpLock';
$exemptModules[] = 'SugarChimpOptoutTracker';
$exemptModules[] = 'SugarChimpMCCleanedEmail';
$exemptModules[] = 'SugarChimpBatches';
$exemptModules[] = 'SmartList';
$exemptModules[] = 'SmartListQueue';
