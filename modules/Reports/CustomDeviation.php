<?php

require_once('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config, $app_list_strings, $db, $current_user;
$ss = new Sugar_Smarty();

function getStartAndEndDate($week,$year){
	$dateTime = new DateTime();
	$dateTime->setISODate($year,$week);
	$result['start_date'] = $dateTime->format('Y-m-d');
	$dateTime->modify('+6 days');
	$result['end_date'] = $dateTime->format('Y-m-d');
	return $result;
} 

if (isset($_REQUEST['date_occured_from']) && $_REQUEST['date_occured_from'] != '')
{
   $date_occured_from = $_REQUEST['date_occured_from'];
}

if (isset($_REQUEST['date_occured_to']) && $_REQUEST['date_occured_to'] != '')
{
	$date_occured_to = $_REQUEST['date_occured_to'];
}
if (isset($_REQUEST['department']) && $_REQUEST['department'] != '')
{
    $department = $_REQUEST['department'];
}

	$department_list = $GLOBALS['app_list_strings']['department_list'];

	foreach($department_list as $dept => $val) {
		$selected	= ($department==$dept)?"selected":"";
		$deptOptions .=	"<option value='".$dept."' ".$selected.">".$val."</option>";
	}


if($date_occured_from!="" && $date_occured_from!="" && $department!=""){
	$qaDateObj	= new TimeDate();
	$doFrom		= $qaDateObj->to_display_date_time($date_occured_from, true, true, $current_user);
	$doFrom 	= str_replace('-', '/', $doFrom);
 
	$doTo		= $qaDateObj->to_display_date_time($date_occured_to, true, true, $current_user);
	$doTo 		= str_replace('-', '/', $doTo);
	
	$startFrom	= date("Y-m-d", strtotime($doFrom));
	$endTo		= date("Y-m-d", strtotime($doTo)); 
	
	$countt = 0; 

	$commSql = "SELECT 
			CONCAT(DATE_FORMAT(m06_error_cstm.date_error_occurred_c,'%x'),'-',DATE_FORMAT(m06_error_cstm.date_error_occurred_c,'%v')) as comm_week,
	l1_cstm.s_species_id_c AS species_id,
	s_species1.name species_name,COUNT(m06_error.id) comm_allcount, COUNT(DISTINCT  m06_error.id) comm_count
	FROM m06_error
	LEFT JOIN  m06_error_anml_animals_1_c l1_1 ON m06_error.id=l1_1.m06_error_anml_animals_1m06_error_ida AND l1_1.deleted=0
	
	LEFT JOIN  anml_animals l1 ON l1.id=l1_1.m06_error_anml_animals_1anml_animals_idb AND l1.deleted=0
	LEFT JOIN m06_error_cstm m06_error_cstm ON m06_error.id = m06_error_cstm.id_c
	LEFT JOIN anml_animals_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN s_species s_species1 ON s_species1.id = l1_cstm.s_species_id_c AND IFNULL(s_species1.deleted,0)=0 
	 WHERE (((m06_error_cstm.date_error_occurred_c >= '".$startFrom."' AND m06_error_cstm.date_error_occurred_c <= '".$endTo."'	) AND (m06_error_cstm.department_c = '^".$department."^'
	) AND ((coalesce(LENGTH(l1.id), 0) <> 0)) AND (m06_error_cstm.error_classification_c = 'Error'
	))) AND  m06_error.deleted=0 	 AND IFNULL(s_species1.deleted,0)=0 
	 GROUP BY CONCAT(DATE_FORMAT(m06_error_cstm.date_error_occurred_c,'%x'),'-',DATE_FORMAT(m06_error_cstm.date_error_occurred_c,'%v')) ,l1_cstm.s_species_id_c ,s_species1.name  ORDER BY comm_week ASC ,species_id ASC ";

	 $resultQry	 = $db->query($commSql);

	$reportData[0][0] 	= "Week Number";
	$reportData[0][1] 	= "Species";
	$reportData[0][2] 	= "Species Count";
	$reportData[0][3] 	= "Total Count";
	$reportData[0][4] 	= "Deviation";
	
	$chartReportData 	= array();
	 
	$cnt =1;
	while ($rowCom = $db->fetchByassoc($resultQry))
	{
		$speciesArr[]	= $rowCom['species_name'];
	}
	$speciesArr  = array_unique($speciesArr) ;

	$resultQry	 = $db->query($commSql); 
	while ($rowCom = $db->fetchByassoc($resultQry))
	{
		$commWeekData	= $rowCom['comm_week'];
		$speciesId		= $rowCom['species_id'];
		$speciesName	= $rowCom['species_name'];
		$commCount		= $rowCom['comm_count'];
		
		$reportData[$cnt][0] 	= $commWeekData;
		$reportData[$cnt][1] 	= $speciesName;
		$reportData[$cnt][2] 	= $commCount;
		
		$commDataArr 	= explode("-",$commWeekData);
		$commYear 		= $commDataArr[0];
		$commWeek 		= $commDataArr[1];
		$dates			= getStartAndEndDate($commWeek,$commYear);
 
		$startDate		= $dates['start_date'];
		$endDate		= $dates['end_date'];
		$totalSpecies	= 0; 
		$nextMondayDate	= date("Y-m-d", strtotime($startDate. ' + 7 days'));
		
		//$speciesSQL		= "SELECT * FROM sc_species_census WHERE name like '".$speciesName."%' and DATE(date_entered)>='".$startDate."' AND DATE(date_entered)<'".$endDate."' "; 
		$speciesSQL		= "SELECT * FROM sc_species_census WHERE name like '".$speciesName."%' and DATE(date_entered)='".$nextMondayDate."'"; 
		$resultSps		= $db->query($speciesSQL);
		if ($resultSps->num_rows > 0) {
			while ($rowSps = $db->fetchByassoc($resultSps)) 
			{
				$totalSpecies = $rowSps['total'];
			}
		}

		if($totalSpecies!=0)
 			$deviation = round(($commCount/$totalSpecies), 4) ;
 		else
			$deviation = "";
		
		$reportData[$cnt][3] 	= $totalSpecies;
		$reportData[$cnt][4] 	= $deviation;
		
		
		foreach($speciesArr as $sps) { 
			if($chartReportData[$commWeek][$sps]==0)
			{
				if($sps== $speciesName )	
					$chartReportData[$commWeek][$sps] 	=  $deviation;
				else 
					$chartReportData[$commWeek][$sps] 	=  0;
			}	
		}
		$cnt++;
	}
}

//echo "<pre>";
//print_r($chartReportData);
 
	if(count($chartReportData)>0)
		$graphAvailable  = 1;
	else
		$graphAvailable  = 0;	

	$obSugarChart = SugarChartFactory::getInstance();
	$obSugarChart->setData($chartReportData);
     
    $obSugarChart->setProperties('Custom Deviations Per Animal By Department Report', '', 'group by chart','off');
	$obSugarChart->is_currency = false;
	$obSugarChart->currency_symbol = '';
    $xmlFile = $obSugarChart->getXMLFileName('lead_status_graph');
    $obSugarChart->saveXMLFile($xmlFile, $obSugarChart->generateXML());
    $stReturnString=$obSugarChart->display('lead_status_graph', $xmlFile, "100%", '500');

    $obSugarChart = SugarChartFactory::getInstance();
    $rsResources = $obSugarChart->getChartResources();
    $ss->assign('GRAPH_RESOURCES',$rsResources);
    $ss->assign('LEADS_CHART_DATA',$stReturnString);
	$ss->assign('IS_CHART',$graphAvailable);
	 

$ss->assign('current_user',	$current_user->id); 
$ss->assign('ReportData', $reportData);
$ss->assign('date_occured_from', $date_occured_from);
$ss->assign('date_occured_to', $date_occured_to); 
$ss->assign('department', $department); 
$ss->assign('deptOptions', $deptOptions); 
$ss->assign('SITE_URL', $GLOBALS['sugar_config']['site_url']);
$html = $ss->fetch("custom/modules/Reports/tpls/CustomDeviation.tpl");
echo $html;