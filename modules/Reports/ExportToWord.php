<?php

require_once('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config;
global $db;
$ss = new Sugar_Smarty();
$report_name = $_GET['report_name'];
$wpid = $_GET['wpid'];
/*All Notification For a Study */
if($report_name=='AllNotificationforStudy')
{
	if(isset($_GET['wpid']) && $_GET['wpid']!=''){
	$work_product_id = $_GET['wpid'];
	$wp_data['id'] = $work_product_id;
	//$wp_data['name'] = 'KFS001-TX01';
	//$wp_data['name'] = $_REQUEST['work_product_name'];
	}
$sql =
"SELECT
IFNULL(l1.id, '')l1_id,
IFNULL(l1. NAME, '')l1_name,
IFNULL(m03_work_product.id, '')m03_work_product_id,
l2_cstm.animal_id_c l2_cstm_animal_id_c,
l2_cstm.usda_id_c l2_cstm_usda_id_c,
CONCAT (l2_cstm.animal_id_c,' - ',l2_cstm.usda_id_c) AS TestUsda,
l1_cstm.date_error_occurred_c L1_CSTM_DATE_ERROR_OCC6BD1CE,
l1_cstm.reinspection_date_c l1_cstm_reinspection_date_c,
l1_cstm.actual_event_c l1_cstm_actual_event_c,
l1_cstm.resolution_c l1_cstm_resolution_c,
l1_cstm.subtype_c l1_cstm_subtype_c,
IFNULL(m03_work_product. NAME, '')m03_work_product_name
FROM
m03_work_product
LEFT JOIN m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id = l1_1.m06_error_m03_work_product_1m03_work_product_idb
AND l1_1.deleted = 0
LEFT JOIN m06_error l1 ON l1.id = l1_1.m06_error_m03_work_product_1m06_error_ida
AND l1.deleted = 0
LEFT JOIN m06_error_anml_animals_1_c l2_1 ON l1.id = l2_1.m06_error_anml_animals_1m06_error_ida
AND l2_1.deleted = 0
LEFT JOIN anml_animals l2 ON l2.id = l2_1.m06_error_anml_animals_1anml_animals_idb
AND l2.deleted = 0
LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
WHERE
(
(
(
IFNULL(
l1_cstm.error_classification_c,
''
)IN('Notification')
)
AND(
m03_work_product.id = '$work_product_id'
)
)
)
AND m03_work_product.deleted = 0
ORDER BY
l1_cstm_subtype_c ASC,
m03_work_product_name ASC,
L1_CSTM_DATE_ERROR_OCC6BD1CE ASC";

$result = $db->query($sql);

$ReportData = array();
$test_system_ids = array();
$usda_ids = array();
while($row = $db->fetchByassoc($result)){
$temp = array();
$temp['communication_id'] = $row['l1_id'];
$temp['communication_name'] = $row['l1_name'];
$temp['work_product_id'] = $row['m03_work_product_id'];
$temp['work_product_name'] = $row['m03_work_product_name'];
$temp['test_system'] = $row['TestUsda'];
//$temp['usda_id'] = $row['l2_cstm_usda_id_c'];
if($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']!=""){
$temp['date_occurred'] = date("m/d/Y", strtotime($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']));
}else{
$temp['date_occurred'] = "";
}

if($row['l1_cstm_reinspection_date_c']!=""){
$temp['actual_assessment_date'] = date("m/d/Y", strtotime($row['l1_cstm_reinspection_date_c']));
}else{
$temp['actual_assessment_date'] = "";
}
$temp['date_occurred'] = $row['L1_CSTM_DATE_ERROR_OCC6BD1CE'];
$temp['actual_assessment_date'] = $row['l1_cstm_reinspection_date_c'];
$temp['actual_event'] = $row['l1_cstm_actual_event_c'];
$temp['sd_assessment'] = $row['l1_cstm_resolution_c'];
$temp['subtype'] = $row['l1_cstm_subtype_c'];
$ReportData[$temp['communication_name']] = $temp;
$test_system_ids[$temp['communication_name']][] = $temp['test_system'];
//$usda_ids[$temp['communication_name']][] = $temp['usda_id'];

}
foreach($ReportData as $key => $val){
$ReportData[$key]['test_system'] = implode(', ',$test_system_ids[$key]);
///$ReportData[$key]['usda_id'] = implode(', ',$usda_ids[$key]);
}
$ss->assign('REPORTDATA',$ReportData);
$ss->assign('WPDATA',$wp_data);


ob_clean();
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=AllNotificationforStudy.doc");

$html = '<html>
<meta http-equiv="Content-Type" content="text/html" charset="Windows-1252">
<body>
';
$html .= $ss->fetch("custom/modules/Reports/tpls/AllNotificationforStudyWord.tpl");

$html .='
</body>
</html>
';
echo $html;
}
/*All Deviation For Study */
elseif($report_name=='AllDeviationsforStudy')
{
	if(isset($_GET['wpid']) && $_GET['wpid']!=''){
	$work_product_id = $_GET['wpid'];
	$wp_data['id'] = $work_product_id;
	}

	$sql =
	"SELECT
	  IFNULL(l1.id, '') l1_id,
	  IFNULL(l1. NAME, '') l1_name,
	  IFNULL(m03_work_product.id, '') m03_work_product_id,
	  l2_cstm.animal_id_c l2_cstm_animal_id_c,
	  l2_cstm.usda_id_c l2_cstm_usda_id_c,
	  CONCAT (l2_cstm.animal_id_c,' - ',l2_cstm.usda_id_c) AS TestUsda,
	  l1_cstm.date_error_occurred_c L1_CSTM_DATE_ERROR_OCC6BD1CE,
	  l1_cstm.expected_event_c l1_cstm_expected_event_c,
	  l1_cstm.actual_event_c l1_cstm_actual_event_c,
	  l1_cstm.resolution_c l1_cstm_resolution_c,
	  IFNULL(
		  l1_cstm.corrective_action_1_c,
		  ''
	  ) L1_CSTM_CORRECTIVE_ACT674D79,
	  l1_cstm.rationale_for_no_ca_c L1_CSTM_RATIONALE_FOR_F48A84,
	  l1_cstm.subtype_c l1_cstm_subtype_c,
	  IFNULL(m03_work_product. NAME, '') m03_work_product_name
	FROM
		m03_work_product
	LEFT JOIN m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id = l1_1.m06_error_m03_work_product_1m03_work_product_idb
	AND l1_1.deleted = 0
	LEFT JOIN m06_error l1 ON l1.id = l1_1.m06_error_m03_work_product_1m06_error_ida
	AND l1.deleted = 0
	LEFT JOIN m06_error_anml_animals_1_c l2_1 ON l1.id = l2_1.m06_error_anml_animals_1m06_error_ida
	AND l2_1.deleted = 0
	LEFT JOIN anml_animals l2 ON l2.id = l2_1.m06_error_anml_animals_1anml_animals_idb
	AND l2.deleted = 0
	LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
	WHERE
		(
			(
				(
					IFNULL(
						l1_cstm.error_classification_c,
						''
					) IN (
						'Error',
						'Impactful Deviation',
						'Non Impactful Deviation'
					)
				)
				AND (
					m03_work_product.id = '$work_product_id'
				)
			)
		)
	AND m03_work_product.deleted = 0
	ORDER BY
	  l1_cstm_subtype_c ASC,
	  m03_work_product_name ASC,
	  L1_CSTM_DATE_ERROR_OCC6BD1CE ASC";
  
  $result = $db->query($sql);
  
  $ReportData = array();
  $test_system_ids = array();
  $usda_ids = array();
  while($row = $db->fetchByassoc($result)){
	  $temp = array();
	  $temp['communication_id'] = $row['l1_id'];
	  $temp['communication_name'] = $row['l1_name'];
	  $temp['work_product_id'] = $row['m03_work_product_id'];
	  $temp['work_product_name'] = $row['m03_work_product_name'];
	  $temp['test_system'] = $row['TestUsda'];
	  //$temp['usda_id'] = $row['l2_cstm_usda_id_c'];
	  
	  if($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']!=""){
		  $temp['date_occurred'] = date("m/d/Y", strtotime($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']));
	  }else{
		  $temp['date_occurred'] = "";
	  }
	  
	  $temp['expected_event'] = $row['l1_cstm_expected_event_c'];
	  $temp['actual_event'] = $row['l1_cstm_actual_event_c'];
	  $temp['sd_assessment'] = $row['l1_cstm_resolution_c'];
	  $temp['subtype'] = str_replace('^','',$row['l1_cstm_subtype_c']);
	  $temp['corrective_action'] = $row['L1_CSTM_CORRECTIVE_ACT674D79'];
	  $temp['corrective_action_details'] = $row['L1_CSTM_RATIONALE_FOR_F48A84'];
	  $ReportData[$temp['communication_name']] = $temp;
	  $test_system_ids[$temp['communication_name']][] = $temp['test_system'];
	  //$usda_ids[$temp['communication_name']][] = $temp['usda_id'];
  }	
  
  foreach($ReportData as $key => $val){
	  $ReportData[$key]['test_system'] = implode(', ',$test_system_ids[$key]);
	  $ReportData[$key]['usda_id'] = implode(', ',$usda_ids[$key]);
  }
  
  $ss->assign('REPORTDATA',$ReportData);
  $ss->assign('WPDATA',$wp_data);
  
  ob_clean();
  header("Content-type: application/vnd.ms-word");
  header("Content-Disposition: attachment;Filename=AllDeviationsforStudy.doc");
  
  $html = '<html>
  <meta http-equiv="Content-Type" content="text/html" charset="Windows-1252">
  <body>
  ';
  $html .= $ss->fetch("custom/modules/Reports/tpls/AllDeviationsforStudyWord.tpl");
  
  $html .='
  </body>
  </html>
  ';
  echo $html;

}

/*All Adverse Events for a Study */
elseif($report_name=='AllAdverseEventsforStudy')
{
	if(isset($_GET['wpid']) && $_GET['wpid']!=''){
	$work_product_id = $_GET['wpid'];
	$wp_data['id'] = $work_product_id;
	}

	$sql =
	"SELECT
	  IFNULL(l1.id, '') l1_id,
	  IFNULL(l1. NAME, '') l1_name,
	  IFNULL(m03_work_product.id, '') m03_work_product_id,
	  l2_cstm.animal_id_c l2_cstm_animal_id_c,
	  l2_cstm.usda_id_c l2_cstm_usda_id_c,
	  
	  CONCAT (l2_cstm.animal_id_c,' - ',l2_cstm.usda_id_c) AS TestUsda,
	  l1_cstm.date_error_occurred_c L1_CSTM_DATE_ERROR_OCC6BD1CE,
	  l1_cstm.actual_event_c l1_cstm_actual_event_c,
	  l1_cstm.resolution_c l1_cstm_resolution_c,
	  IFNULL(
		  l1_cstm.corrective_action_1_c,
		  ''
	  ) L1_CSTM_CORRECTIVE_ACT674D79,
	  l1_cstm.rationale_for_no_ca_c L1_CSTM_RATIONALE_FOR_F48A84,
	  
	  l1_cstm.subtype_c l1_cstm_subtype_c,
	  IFNULL(m03_work_product. NAME, '') m03_work_product_name
	FROM
		m03_work_product
	LEFT JOIN m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id = l1_1.m06_error_m03_work_product_1m03_work_product_idb
	AND l1_1.deleted = 0
	LEFT JOIN m06_error l1 ON l1.id = l1_1.m06_error_m03_work_product_1m06_error_ida
	AND l1.deleted = 0
	LEFT JOIN m06_error_anml_animals_1_c l2_1 ON l1.id = l2_1.m06_error_anml_animals_1m06_error_ida
	AND l2_1.deleted = 0
	LEFT JOIN anml_animals l2 ON l2.id = l2_1.m06_error_anml_animals_1anml_animals_idb
	AND l2.deleted = 0
	LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
	WHERE
		(
			(
				(
					IFNULL(
						l1_cstm.error_classification_c,
						''
					) IN ('Adverse Event')
				) 
				AND (
					m03_work_product.id = '$work_product_id'
					
				)
			)
		)
	AND m03_work_product.deleted = 0
	ORDER BY
	  l1_cstm_subtype_c ASC,
	  m03_work_product_name ASC,
	  L1_CSTM_DATE_ERROR_OCC6BD1CE ASC";
  //m03_work_product.id = '5858fbcc-24a9-11e9-9ca2-0220085a287c'
  $result = $db->query($sql);	
  $ReportData = array();
  $test_system_ids = array();
  $usda_ids = array();
  while($row = $db->fetchByassoc($result)){
	  $temp = array();
	  $temp['communication_id'] = $row['l1_id'];
	  $temp['communication_name'] = $row['l1_name'];
	  $temp['work_product_id'] = $row['m03_work_product_id'];
	  $temp['work_product_name'] = $row['m03_work_product_name'];
	  $temp['test_system'] = $row['TestUsda'];
	  $temp['usda_id'] = $row['l2_cstm_usda_id_c'];
	  
	  if($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']!=""){
		  $temp['date_occurred'] = date("m/d/Y", strtotime($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']));
	  }else{
		  $temp['date_occurred'] = "";
	  }
	  
	  $temp['actual_assessment_date'] = $row['l1_cstm_reinspection_date_c'];
	  $temp['actual_event'] = $row['l1_cstm_actual_event_c'];
	  $temp['sd_assessment'] = $row['l1_cstm_resolution_c'];
	  $temp['subtype'] = $row['l1_cstm_subtype_c'];
	  
	  $temp['corrective_action'] = $row['L1_CSTM_CORRECTIVE_ACT674D79'];
	  $temp['corrective_action_details'] = $row['L1_CSTM_RATIONALE_FOR_F48A84'];
	  
	  
	  $ReportData[$temp['communication_name']] = $temp;
	  $test_system_ids[$temp['communication_name']][] = $temp['test_system'];
	  //$usda_ids[$temp['communication_name']][] = $temp['usda_id'];
	  
	  
		  
  }
  
  foreach($ReportData as $key => $val){
	  $ReportData[$key]['test_system'] = implode(', ',$test_system_ids[$key]);
	  //$ReportData[$key]['usda_id'] = implode(', ',$usda_ids[$key]);
  }
  
  $ss->assign('REPORTDATA',$ReportData);
  $ss->assign('WPDATA',$wp_data);
  ob_clean();
  header("Content-type: application/vnd.ms-word");
  header("Content-Disposition: attachment;Filename=AllAdverseEventsforStudy.doc");
  
  $html = '<html>
  <meta http-equiv="Content-Type" content="text/html" charset="Windows-1252">
  <body>
  ';
  $html .= $ss->fetch("custom/modules/Reports/tpls/AllAdverseEventsforStudyWord.tpl");
  
  $html .='
  </body>
  </html>
  ';
  echo $html;


}


?>