<?php
require_once('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config,$app_list_strings,$db,$current_user;
$ss = new Sugar_Smarty();
//$work_product_id = $_REQUEST['work_product_id'];
// $work_product_id = 'cd80da16-d55a-11e9-ba5e-06eddc549468';
if(isset($_REQUEST['work_product_id']) && $_REQUEST['work_product_id']!=''){
	$work_product_id = $_REQUEST['work_product_id'];
	$wp_data['id'] = $work_product_id;
	$wp_data['name'] = $_REQUEST['work_product_name'];
}else{
	$work_product_id = '5858fbcc-24a9-11e9-9ca2-0220085a287c';
	$wp_data['id'] = $work_product_id;
	$wp_data['name'] = 'NTC012-IS21';
}
$wp_data['current_user_id'] = $current_user->id;

$ReportData = array();
$ReportData2 = array();
$ReportClassificationData = array();
$RelatedCommData = array();
$test_system_ids = array();
$usda_ids = array();


// To get the count order by subtype c
$sqlClassificationCount="SELECT l1_cstm.subtype_c l1_cstm_subtype_c,l1_cstm.error_classification_c l1_cstm_error_classification_c,IFNULL(m03_work_product.name,'') m03_work_product_name
,count(*) count,l1.name,
ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id
FROM m03_work_product
LEFT JOIN  m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id=l1_1.m06_error_m03_work_product_1m03_work_product_idb AND l1_1.deleted=0

LEFT JOIN  m06_error l1 ON l1.id=l1_1.m06_error_m03_work_product_1m06_error_ida AND l1.deleted=0
LEFT JOIN  m06_error_anml_animals_1_c l2_1 ON l1.id=l2_1.m06_error_anml_animals_1m06_error_ida AND l2_1.deleted=0

LEFT JOIN  anml_animals l2 ON l2.id=l2_1.m06_error_anml_animals_1anml_animals_idb AND l2.deleted=0
LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb AND ReletedComm.deleted=0

 WHERE (((IFNULL(l1_cstm.error_classification_c,'') IN ('Adverse Event',
					  'Adverse Event Study Article Related',
					  'Adverse Event Procedure Related',
					  'Adverse Event Test System Model Related',
					  'Adverse Event Etiology Unknown')
) AND (m03_work_product.id='$work_product_id'
))) 
AND  m03_work_product.deleted=0 
 GROUP BY l1.name,l1_cstm.error_classification_c,l1_cstm.subtype_c 
,m03_work_product.name
 ORDER BY l1_cstm_error_classification_c ASC,l1_cstm_subtype_c ASC,m03_work_product_name ASC";

$resultClassificationCount = $db->query($sqlClassificationCount);
$hash = array();
$ReportClassificationDataCount = array();
while($rowClassification = $db->fetchByassoc($resultClassificationCount)){
	$hash_key = str_replace('^','',$rowClassification['l1_cstm_error_classification_c']);
	$hash_key = $app_list_strings['error_classification_c_list'][$hash_key]; 
	if(!array_key_exists($hash_key, $hash)) {
		$hash[$hash_key] = sizeof($ReportClassificationDataCount);
		$classificationValue = $app_list_strings['error_classification_c_list'][str_replace('^','',$rowClassification['l1_cstm_error_classification_c'])];
        array_push($ReportClassificationDataCount, array(
            'm03_work_product_name' => $rowClassification['m03_work_product_name'],
			'l1_cstm_error_classification_c' => $classificationValue,
			'name' => $rowClassification['name'],
			'rel_comm_id' => $rowClassification['rel_comm_id'],
            'count' => 0,
        ));
    }
	$ReportClassificationDataCount[$hash[$hash_key]]['count'] += 1;
} 
/*Query to get Report Data*/ 
$sql =
  "SELECT
	IFNULL(l1.id, '') l1_id,
	IFNULL(l1. NAME, '') l1_name,
	ReletedComm.m06_error_m06_error_1m06_error_ida AS rel_comm_id,
	IFNULL(m03_work_product.id, '') m03_work_product_id,
	l2_cstm.animal_id_c l2_cstm_animal_id_c,
	l2_cstm.usda_id_c l2_cstm_usda_id_c,
	
	CONCAT (l2_cstm.animal_id_c,'-',l2_cstm.usda_id_c) AS TestUsda,
	l1_cstm.date_error_occurred_c L1_CSTM_DATE_ERROR_OCC6BD1CE,
	l1_cstm.actual_event_c l1_cstm_actual_event_c,
	l1_cstm.resolution_c l1_cstm_resolution_c,
	l1_cstm.impactful_dd_c impactful_dd_c,
	IFNULL(
		l1_cstm.corrective_action_1_c,
		''
	) L1_CSTM_CORRECTIVE_ACT674D79,
	l1_cstm.rationale_for_no_ca_c L1_CSTM_RATIONALE_FOR_F48A84,
	
	l1_cstm.subtype_c l1_cstm_subtype_c,
	l1_cstm.error_classification_c l1_cstm_error_classification_c,
	IFNULL(m03_work_product. NAME, '') m03_work_product_name
  FROM
  	m03_work_product
  LEFT JOIN m06_error_m03_work_product_1_c l1_1 ON m03_work_product.id = l1_1.m06_error_m03_work_product_1m03_work_product_idb
  AND l1_1.deleted = 0
  LEFT JOIN m06_error l1 ON l1.id = l1_1.m06_error_m03_work_product_1m06_error_ida
  AND l1.deleted = 0
  LEFT JOIN m06_error_anml_animals_1_c l2_1 ON l1.id = l2_1.m06_error_anml_animals_1m06_error_ida
  AND l2_1.deleted = 0
  LEFT JOIN anml_animals l2 ON l2.id = l2_1.m06_error_anml_animals_1anml_animals_idb
  AND l2.deleted = 0
  LEFT JOIN m06_error_cstm l1_cstm ON l1.id = l1_cstm.id_c
  LEFT JOIN anml_animals_cstm l2_cstm ON l2.id = l2_cstm.id_c
  LEFT JOIN m06_error_m06_error_1_c AS ReletedComm ON l1.id = ReletedComm.m06_error_m06_error_1m06_error_idb
  AND ReletedComm.deleted=0
  WHERE
  	(
  		(
  			(
  				IFNULL(
  					l1_cstm.error_classification_c,
  					''
  				) IN (
					  'Adverse Event',
					  'Adverse Event Study Article Related',
					  'Adverse Event Procedure Related',
					  'Adverse Event Test System Model Related',
					  'Adverse Event Etiology Unknown'
					  )
  			) 
  			AND (
  				m03_work_product.id = '$work_product_id'
  				
  			)
  		)
  	)
  AND m03_work_product.deleted = 0
  ORDER BY
  L1_CSTM_DATE_ERROR_OCC6BD1CE ASC,
  l1_cstm_error_classification_c ASC,
  l1_cstm_subtype_c ASC,
  m03_work_product_name ASC";
//m03_work_product.id = '5858fbcc-24a9-11e9-9ca2-0220085a287c'
$result = $db->query($sql);
$commListArr = array();
while($row = $db->fetchByassoc($result)){
	$temp = array();
	$commListArr[]  =$row['l1_id'];
	$temp['communication_id'] = $row['l1_id'];
	$temp['communication_name'] = $row['l1_name'];
	$temp['work_product_id'] = $row['m03_work_product_id'];
	$temp['work_product_name'] = $row['m03_work_product_name'];
	$temp['rel_comm_id'] = $row['rel_comm_id'];
	$temp['test_system'] = $row['l2_cstm_animal_id_c'];
	$temp['usda_id'] = $row['l2_cstm_usda_id_c'];
	$temp['impactful_dd_c'] = $row['impactful_dd_c'];
	if($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']!=""){
		$temp['date_occurred'] = date("m/d/Y", strtotime($row['L1_CSTM_DATE_ERROR_OCC6BD1CE']));
	}else{
		$temp['date_occurred'] = "";
	}
	$temp['actual_assessment_date'] = $row['l1_cstm_reinspection_date_c'];
	$temp['l1_cstm_subtype_c'] =  str_replace('^','',$row['l1_cstm_subtype_c']);
	$temp['l1_cstm_error_classification_c'] =  str_replace('^','',$row['l1_cstm_error_classification_c']);    
	$temp['actual_event'] = $row['l1_cstm_actual_event_c'];
	$temp['sd_assessment'] = $row['l1_cstm_resolution_c'];
	$temp['subtype'] = str_replace('^','',$row['l1_cstm_subtype_c']);
	$temp['subtype'] = $app_list_strings['error_type_list'][$temp['subtype']];

	$temp['classification'] = str_replace('^','',$row['l1_cstm_error_classification_c']);
	$temp['classification'] = $app_list_strings['error_classification_c_list'][$temp['classification']];

	$temp['corrective_action'] = $row['L1_CSTM_CORRECTIVE_ACT674D79'];
	$temp['corrective_action_details'] = $row['L1_CSTM_RATIONALE_FOR_F48A84'];
	$testSystemUsdaID = $temp['test_system'];
	if($temp['usda_id']!="")
		$testSystemUsdaID .= "-".$temp['usda_id'];	

	$test_system_ids[$temp['communication_name']][] = $testSystemUsdaID;
	$ReportClassificationData[$temp['classification']][$temp['communication_name']]= $temp;
	if($temp['rel_comm_id']!="")
	{
		$RelatedCommData[$temp['rel_comm_id']][$temp['communication_name']]= $temp;
	}
	
}
//echo '<pre>';print_r($ReportData2);die();
foreach($RelatedCommData as $keyrel => $valrel){
	foreach($valrel as $keyrel1 => $valrel1){
		$RelatedCommData[$keyrel][$keyrel1]['test_system'] = implode(', ',$test_system_ids[$keyrel1]);
	}
}
foreach($ReportClassificationData as $key => $val){
	foreach($val as $key1 => $val1){
		$yes = "";
		$hiderow = "";
		/* To mark the related record and info to main array based on the all the related records */
		foreach($RelatedCommData as $relkey => $relval){
			foreach($relval as $relkey1 => $relateddataval){
				$ReportClassificationData[$key][$key1]['is_related'] = $yes;
				$ReportClassificationData[$key][$key1]['is_hide'] = $hiderow;
				if(($val1['classification'] == $relateddataval['classification']) && ($val1['communication_id'] == $relateddataval['rel_comm_id']))
				{
					$yes = "yes";
					$ReportClassificationData[$key][$key1]['is_related'] = $yes;
				}
				/*hide the main record row which are having the related comm id value */
				if (in_array($val1['rel_comm_id'],$commListArr) && ($val1['classification'] == $relateddataval['classification']) && ($val1['rel_comm_id'] == $relateddataval['rel_comm_id'] ))
                {
					$hiderow ='yes';
					$ReportClassificationData[$key][$key1]['is_hide'] = $hiderow;
				}
				
			}
		}
		
		$ReportClassificationData[$key][$key1]['test_system'] = implode(', ',$test_system_ids[$key1]);
	}
}
//$ss->assign('REPORTDATA',$ReportData);
$ss->assign('REPORTDATA2',$ReportClassificationDataCount);
$ss->assign('ReportClassificationData',$ReportClassificationData);
$ss->assign('RelatedCommData',$RelatedCommData);
$ss->assign('WPDATA',$wp_data);
$html = $ss->fetch("custom/modules/Reports/tpls/AllAdverseEventsforStudy.tpl");
echo $html;
?>