<?php
//AllDeviationsforStudy
require_once('include/SugarSmarty/Sugar_Smarty.php');
global $sugar_config, $app_list_strings, $db, $current_user;
$ss = new Sugar_Smarty();

if (isset($_REQUEST['submit_form']) && $_REQUEST['submit_form'] == 1) {
     $filter_start_date = $_REQUEST["filter_start_date"];
     $filter_end_date = $_REQUEST["filter_end_date"];
} else {
     $filter_start_date = date("Y")."-".date("m")."-01";
     $filter_end_date = date("Y-m-d");
}

$sql = 'SELECT id,breed_c,name,date_last_weighed_c,date_of_birth_c,s_species_id_c,sex_c FROM anml_animals
left join anml_animals_cstm on anml_animals_cstm.id_c =anml_animals.id
where s_species_id_c!="" and deleted=0 and UNIX_TIMESTAMP(date_entered) between UNIX_TIMESTAMP("'.$filter_start_date.'") and UNIX_TIMESTAMP("'.$filter_end_date.'")+86000  order by date_entered desc';

$ReportData = array();
$summaryData = array();
$offset1 =  -18000;
//$offset1 =  -21600; //daylight Saving

$result = $db->query($sql);
while ($row = $db->fetchByassoc($result)) {
    $arrDLW = array();  // Array of Date Last Weight [Date Created] => Date Last Weight Value
    $arrW = array();   // Array of Weight            [Date Created] => Weight
    $id = $row['id']; // Test System ID

    $sqlDLW = 'SELECT * FROM anml_animals_audit where parent_id="' . $id . '" and field_name="date_last_weighed_c" order by date_created  asc';

    $resultDLW = $db->query($sqlDLW);

    while ($rowDLW = $db->fetchByassoc($resultDLW)) {
        $arrDLW[$rowDLW['date_created']] = $rowDLW['after_value_string'];
    }

    $sqlW = 'SELECT * FROM anml_animals_audit where parent_id="' . $id . '" and field_name="bodyweight_c" order by date_created  asc';
    $resultW = $db->query($sqlW);

    while ($rowW = $db->fetchByassoc($resultW)) {
        $arrW[$rowW['date_created']] = $rowW['after_value_string'];
    }

    // Creating Date Created field array. [0] => Date Created
    $keysDLW = array_keys($arrDLW);
    $keysW = array_keys($arrW);

    $temp = array();
    foreach (array_keys($keysW) as $kW) {
        //Current Date Created of weight
        $this_value = $keysW[$kW];
        //Next Date Created
        $nextval = $keysW[$kW + 1];

        foreach (array_keys($keysDLW) as $kDLW) {
            //Current Date Created of date last weight
            $this_value1 = $keysDLW[$kDLW];
            //Next Date Created of date last weight
            $nextval1 = $keysDLW[$kDLW + 1];

            if (strtotime($keysDLW[$kDLW]) >= strtotime($this_value) && (strtotime($nextval) > strtotime($keysDLW[$kDLW]) || $nextval == null)) {
                $speciesId = $row['s_species_id_c'];
                $temp['s_species_id_c'] = $speciesId;

                //Getting name of Species
                $sqlSpecies = 'SELECT name from s_species where id="' . $speciesId . '" and deleted=0';
                $resultSpecies = $db->query($sqlSpecies);
                $rowSpecies = $db->fetchByassoc($resultSpecies);

                $temp['species_name'] = $rowSpecies['name'];
                $temp['s_species_id_c'] = $speciesId;
                $temp['breed_c'] = $row['breed_c'];
                $temp['parent_id'] = $row['id'];
                $temp['name'] = $row['name'];
                $temp['sex_c'] = $row['sex_c'];
                $temp['date_last_weighed_c'] = date("m-d-Y", strtotime($arrDLW[$keysDLW[$kDLW]]));;
                $temp['after_value_string'] = $arrW[$keysW[$kW]];

                //Getting Date Of Birth
                if ($row['date_of_birth_c'] != "" || $row['date_of_birth_c'] != null) {
                    $sqlDOB = 'SELECT after_value_string FROM anml_animals_audit where parent_id="' . $id . '" and field_name="date_of_birth_c" and UNIX_TIMESTAMP(date_created) <= UNIX_TIMESTAMP("' . $this_value1 . '") order by date_created desc limit 1';
                    $resultDOB = $db->query($sqlDOB);
                    $rowDOB = $db->fetchByassoc($resultDOB);
                    if ($rowDOB['after_value_string']) {
                        $temp['date_of_birth_c'] = date("m-d-Y", strtotime($rowDOB['after_value_string']));
                    } else
                        $temp['date_of_birth_c'] = "";
                } else{ 
                    $temp['date_of_birth_c'] = "";
                }
                array_push($ReportData, $temp);
            }
        }
    }
}
$ss->assign('filter_start_date', $filter_start_date);
$ss->assign('filter_end_date', $filter_end_date);
$ss->assign('current_user', $current_user->id);
$ss->assign('ReportData', $ReportData);
$ss->assign('SITE_URL', $GLOBALS['sugar_config']['site_url']);
$html = $ss->fetch("custom/modules/Reports/tpls/CustomTSWeight.tpl");

echo $html;
