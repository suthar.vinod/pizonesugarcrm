<?php
$module_name = 'SW_Study_Workflow';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'm03_work_product_sw_study_workflow_1_name',
                'label' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_M03_WORK_PRODUCT_TITLE',
                'enabled' => true,
                'id' => 'M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1M03_WORK_PRODUCT_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              3 => 
              array (
                'name' => 'protocol_data_book_create_du_c',
                'label' => 'LBL_PROTOCOL_DATA_BOOK_CREATE_DU',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'verification_of_study_due_c',
                'label' => 'LBL_VERIFICATION_OF_STUDY_DUE',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'test_article_checkin_c',
                'label' => 'LBL_TEST_ARTICLE_CHECKIN',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'wp_deliverable_confirm_date_c',
                'label' => 'LBL_WP_DELIVERABLE_CONFIRM_DATE',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'non_procedure_room_date_c',
                'label' => 'LBL_NON_PROCEDURE_ROOM_DATE',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'default' => false,
                'name' => 'date_modified',
                'readonly' => true,
              ),
              9 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
