<?php
$popupMeta = array (
    'moduleMain' => 'SW_Study_Workflow',
    'varName' => 'SW_Study_Workflow',
    'orderBy' => 'sw_study_workflow.name',
    'whereClauses' => array (
  'name' => 'sw_study_workflow.name',
),
    'searchInputs' => array (
  0 => 'sw_study_workflow_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_M03_WORK_PRODUCT_TITLE',
    'id' => 'M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1M03_WORK_PRODUCT_IDA',
    'width' => 10,
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
  'PROTOCOL_DATA_BOOK_CREATE_DU_C' => 
  array (
    'type' => 'date',
    'label' => 'LBL_PROTOCOL_DATA_BOOK_CREATE_DU',
    'width' => 10,
    'default' => true,
    'name' => 'protocol_data_book_create_du_c',
  ),
  'VERIFICATION_OF_STUDY_DUE_C' => 
  array (
    'type' => 'date',
    'label' => 'LBL_VERIFICATION_OF_STUDY_DUE',
    'width' => 10,
    'default' => true,
    'name' => 'verification_of_study_due_c',
  ),
  'TEST_ARTICLE_CHECKIN_C' => 
  array (
    'type' => 'date',
    'label' => 'LBL_TEST_ARTICLE_CHECKIN',
    'width' => 10,
    'default' => true,
    'name' => 'test_article_checkin_c',
  ),
  'WP_DELIVERABLE_CONFIRM_DATE_C' => 
  array (
    'type' => 'date',
    'label' => 'LBL_WP_DELIVERABLE_CONFIRM_DATE',
    'width' => 10,
    'default' => true,
    'name' => 'wp_deliverable_confirm_date_c',
  ),
  'NON_PROCEDURE_ROOM_DATE_C' => 
  array (
    'type' => 'date',
    'label' => 'LBL_NON_PROCEDURE_ROOM_DATE',
    'width' => 10,
    'default' => true,
    'name' => 'non_procedure_room_date_c',
  ),
),
);
