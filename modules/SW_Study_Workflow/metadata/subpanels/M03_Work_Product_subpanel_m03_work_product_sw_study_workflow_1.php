<?php
// created: 2018-10-09 17:52:59
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'protocol_data_book_create_du_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_PROTOCOL_DATA_BOOK_CREATE_DU',
    'width' => 10,
    'default' => true,
  ),
  'verification_of_study_due_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_VERIFICATION_OF_STUDY_DUE',
    'width' => 10,
    'default' => true,
  ),
  'test_article_checkin_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_TEST_ARTICLE_CHECKIN',
    'width' => 10,
    'default' => true,
  ),
  'non_procedure_room_date_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_NON_PROCEDURE_ROOM_DATE',
    'width' => 10,
    'default' => true,
  ),
  'wp_deliverable_confirm_date_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_WP_DELIVERABLE_CONFIRM_DATE',
    'width' => 10,
    'default' => true,
  ),
);