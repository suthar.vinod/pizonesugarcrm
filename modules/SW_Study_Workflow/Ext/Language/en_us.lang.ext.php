<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Language/en_us.customm03_work_product_sw_study_workflow_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Language/en_us.customm03_work_product_sw_study_workflow_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_SW_STUDY_WORKFLOW_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_SW_STUDY_WORKFLOW_TITLE'] = 'Work Products';


?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_PROTOCOL_DATA_BOOK_CREATION'] = 'Protocol &amp; Data Book Creation';
$mod_strings['LBL_VERIFICATION_OF_STUDY_FORMS'] = 'Verification of Study Forms';
$mod_strings['LBL_WP_DELIVERABLE_CONFIRMATION'] = 'WP Deliverable Confirmation';
$mod_strings['LBL_TEST_CONTROL_ARTICLE_CHECKIN'] = 'Test &amp; Control Article Check-In';
$mod_strings['LBL_STUDY_COORDINATOR_NOTES'] = 'Study Coordinator Notes';
$mod_strings['LBL_PROTOCOL_DATA_BOOK_CREATE_DU'] = 'Protocol &amp; Data Book Creation Due Date';
$mod_strings['LBL_VERIFICATION_OF_STUDY_DUE'] = 'Verification of Study Forms Due Date';
$mod_strings['LBL_WP_DELIVERABLE_CONFIRM_DATE'] = 'WP Deliverable Confirmation Due Date';
$mod_strings['LBL_TEST_ARTICLE_CHECKIN'] = 'Test &amp; Control Article Check-In Due Date';
$mod_strings['LBL_RECORD_BODY'] = 'Study Initiation';
$mod_strings['LBL_NON_PROCEDURE_ROOM_TASK'] = 'Non-Procedure Room Task Confirmation';
$mod_strings['LBL_NON_PROCEDURE_ROOM_DATE'] = 'Non-Procedure Room Task Confirmation Due Date';
$mod_strings['LBL_ARCHIVING_SCANNING_PROT_DATA'] = 'Archiving or Scanning of Protocol/Study Data Binders';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Data/Task Management';
$mod_strings['LBL_SW_STUDY_WORKFLOW_FOCUS_DRAWER_DASHBOARD'] = 'Study Workflow Focus Drawer';
$mod_strings['LBL_SW_STUDY_WORKFLOW_RECORD_DASHBOARD'] = 'Study Workflow Record Dashboard';

?>
