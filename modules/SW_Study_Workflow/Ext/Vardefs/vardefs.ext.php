<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/m03_work_product_sw_study_workflow_1_SW_Study_Workflow.php

// created: 2018-10-09 16:44:23
$dictionary["SW_Study_Workflow"]["fields"]["m03_work_product_sw_study_workflow_1"] = array (
  'name' => 'm03_work_product_sw_study_workflow_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_sw_study_workflow_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_SW_STUDY_WORKFLOW_TITLE',
  'id_name' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["SW_Study_Workflow"]["fields"]["m03_work_product_sw_study_workflow_1_name"] = array (
  'name' => 'm03_work_product_sw_study_workflow_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
  'link' => 'm03_work_product_sw_study_workflow_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["SW_Study_Workflow"]["fields"]["m03_work_product_sw_study_workflow_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_SW_STUDY_WORKFLOW_TITLE_ID',
  'id_name' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
  'link' => 'm03_work_product_sw_study_workflow_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_verification_of_study_forms_c.php

 // created: 2018-10-09 16:49:08
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_forms_c']['labelValue']='Verification of Study Forms';
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_forms_c']['enforced']='';
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_forms_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_wp_deliverable_confirmation_c.php

 // created: 2018-10-09 16:50:03
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirmation_c']['labelValue']='WP Deliverable Confirmation';
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirmation_c']['enforced']='';
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirmation_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_test_control_article_checkin_c.php

 // created: 2018-10-09 16:50:58
$dictionary['SW_Study_Workflow']['fields']['test_control_article_checkin_c']['labelValue']='Test & Control Article Check-In';
$dictionary['SW_Study_Workflow']['fields']['test_control_article_checkin_c']['enforced']='';
$dictionary['SW_Study_Workflow']['fields']['test_control_article_checkin_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_verification_of_study_due_c.php

 // created: 2018-10-09 16:58:26
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_due_c']['duplicate_merge_dom_value']=0;
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_due_c']['labelValue']='Verification of Study Forms Due Date';
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_due_c']['calculated']='true';
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_due_c']['formula']='addDays(related($m03_work_product_sw_study_workflow_1,"first_procedure_c"),-2)';
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_due_c']['enforced']='true';
$dictionary['SW_Study_Workflow']['fields']['verification_of_study_due_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_test_article_checkin_c.php

 // created: 2018-10-09 17:04:34
$dictionary['SW_Study_Workflow']['fields']['test_article_checkin_c']['duplicate_merge_dom_value']=0;
$dictionary['SW_Study_Workflow']['fields']['test_article_checkin_c']['labelValue']='Test & Control Article Check-In Due Date';
$dictionary['SW_Study_Workflow']['fields']['test_article_checkin_c']['calculated']='1';
$dictionary['SW_Study_Workflow']['fields']['test_article_checkin_c']['formula']='addDays(related($m03_work_product_sw_study_workflow_1,"first_procedure_c"),-2)';
$dictionary['SW_Study_Workflow']['fields']['test_article_checkin_c']['enforced']='1';
$dictionary['SW_Study_Workflow']['fields']['test_article_checkin_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_non_procedure_room_task_c.php

 // created: 2018-10-09 17:11:02
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_task_c']['labelValue']='Non-Procedure Room Task Confirmation';
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_task_c']['enforced']='';
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_task_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_non_procedure_room_date_c.php

 // created: 2018-10-09 17:13:00
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_date_c']['duplicate_merge_dom_value']=0;
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_date_c']['labelValue']='Non-Procedure Room Task Confirmation Due Date';
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_date_c']['calculated']='true';
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_date_c']['formula']='related($m03_work_product_sw_study_workflow_1,"first_procedure_c")';
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_date_c']['enforced']='true';
$dictionary['SW_Study_Workflow']['fields']['non_procedure_room_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_archiving_scanning_prot_data_c.php

 // created: 2018-10-09 17:25:22
$dictionary['SW_Study_Workflow']['fields']['archiving_scanning_prot_data_c']['labelValue']='Archiving or Scanning of Protocol/Study Data Binders';
$dictionary['SW_Study_Workflow']['fields']['archiving_scanning_prot_data_c']['enforced']='';
$dictionary['SW_Study_Workflow']['fields']['archiving_scanning_prot_data_c']['dependency']='equal(related($m03_work_product_sw_study_workflow_1,"work_product_status_c"),"Archived")';

 
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_wp_deliverable_confirm_date_c.php

 // created: 2018-10-09 17:37:26
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirm_date_c']['duplicate_merge_dom_value']=0;
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirm_date_c']['labelValue']='WP Deliverable Confirmation Due Date';
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirm_date_c']['calculated']='1';
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirm_date_c']['formula']='addDays(related($m03_work_product_sw_study_workflow_1,"first_procedure_c"),-3)';
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirm_date_c']['enforced']='1';
$dictionary['SW_Study_Workflow']['fields']['wp_deliverable_confirm_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_name.php

 // created: 2019-04-23 12:08:12
$dictionary['SW_Study_Workflow']['fields']['name']['len']='255';
$dictionary['SW_Study_Workflow']['fields']['name']['audited']=true;
$dictionary['SW_Study_Workflow']['fields']['name']['massupdate']=false;
$dictionary['SW_Study_Workflow']['fields']['name']['unified_search']=false;
$dictionary['SW_Study_Workflow']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['SW_Study_Workflow']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_protocol_data_book_create_du_c.php

 // created: 2019-04-23 12:09:37
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_create_du_c']['duplicate_merge_dom_value']=0;
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_create_du_c']['labelValue']='Protocol & Data Book Creation Due Date';
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_create_du_c']['calculated']='1';
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_create_du_c']['formula']='addDays(related($m03_work_product_sw_study_workflow_1,"first_procedure_c"),-2)';
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_create_du_c']['enforced']='1';
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_create_du_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_protocol_data_book_creation_c.php

 // created: 2019-04-23 12:11:36
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_creation_c']['labelValue']='Protocol & Data Book Creation';
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_creation_c']['enforced']='';
$dictionary['SW_Study_Workflow']['fields']['protocol_data_book_creation_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/SW_Study_Workflow/Ext/Vardefs/sugarfield_study_coordinator_notes_c.php

 // created: 2019-04-23 12:12:52
$dictionary['SW_Study_Workflow']['fields']['study_coordinator_notes_c']['labelValue']='Study Coordinator Notes';
$dictionary['SW_Study_Workflow']['fields']['study_coordinator_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['SW_Study_Workflow']['fields']['study_coordinator_notes_c']['enforced']='';
$dictionary['SW_Study_Workflow']['fields']['study_coordinator_notes_c']['dependency']='';

 
?>
