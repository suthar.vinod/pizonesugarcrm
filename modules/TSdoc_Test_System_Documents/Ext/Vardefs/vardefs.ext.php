<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TSdoc_Test_System_Documents/Ext/Vardefs/anml_animals_tsdoc_test_system_documents_1_TSdoc_Test_System_Documents.php

// created: 2019-09-30 12:27:41
$dictionary["TSdoc_Test_System_Documents"]["fields"]["anml_animals_tsdoc_test_system_documents_1"] = array (
  'name' => 'anml_animals_tsdoc_test_system_documents_1',
  'type' => 'link',
  'relationship' => 'anml_animals_tsdoc_test_system_documents_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_TSDOC_TEST_SYSTEM_DOCUMENTS_TITLE',
  'id_name' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["TSdoc_Test_System_Documents"]["fields"]["anml_animals_tsdoc_test_system_documents_1_name"] = array (
  'name' => 'anml_animals_tsdoc_test_system_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
  'link' => 'anml_animals_tsdoc_test_system_documents_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["TSdoc_Test_System_Documents"]["fields"]["anml_animals_tsdoc_test_system_documents_1anml_animals_ida"] = array (
  'name' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_TSDOC_TEST_SYSTEM_DOCUMENTS_TITLE_ID',
  'id_name' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
  'link' => 'anml_animals_tsdoc_test_system_documents_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/TSdoc_Test_System_Documents/Ext/Vardefs/sugarfield_document_name.php

 // created: 2019-09-30 12:29:29
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['audited']=true;
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['massupdate']=false;
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['merge_filter']='disabled';
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['unified_search']=false;
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['TSdoc_Test_System_Documents']['fields']['document_name']['calculated']=false;

 
?>
