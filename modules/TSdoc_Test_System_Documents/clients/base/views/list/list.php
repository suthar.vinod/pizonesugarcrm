<?php
$module_name = 'TSdoc_Test_System_Documents';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'document_name',
                'label' => 'LBL_NAME',
                'link' => true,
                'default' => true,
                'enabled' => true,
              ),
              1 => 
              array (
                'name' => 'uploadfile',
                'label' => 'LBL_FILE_UPLOAD',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'category_id',
                'label' => 'LBL_LIST_CATEGORY',
                'default' => true,
                'enabled' => true,
              ),
              3 => 
              array (
                'name' => 'anml_animals_tsdoc_test_system_documents_1_name',
                'label' => 'LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_ANML_ANIMALS_TITLE',
                'enabled' => true,
                'id' => 'ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1ANML_ANIMALS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED_USER',
                'module' => 'Users',
                'id' => 'USERS_ID',
                'default' => false,
                'sortable' => false,
                'related_fields' => 
                array (
                  0 => 'modified_user_id',
                ),
                'enabled' => true,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
