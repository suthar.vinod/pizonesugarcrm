<?php
// created: 2019-02-25 15:23:00
$viewdefs['EFS_Equipment_Facility_Servi']['base']['view']['subpanel-for-equip_equipment-equip_equipment_efs_equipment_facility_servi_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'category_c',
          'label' => 'LBL_CATEGORY',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'service_interval_days_c',
          'label' => 'LBL_SERVICE_INTERVAL_DAYS',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'last_service_date_c',
          'label' => 'LBL_LAST_SERVICE_DATE',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'next_service_date_c',
          'label' => 'LBL_NEXT_SERVICE_DATE',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);