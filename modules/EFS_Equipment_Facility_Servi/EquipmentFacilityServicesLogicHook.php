<?php

class EquipmentFacilityServicesLogicHook {

    function generateName($bean, $event, $arguments) {
        global $db, $current_user;
        $isIdExist = $bean->fetched_row['id'];
        $lastServiceDate = $bean->last_service_date_c;
        $intervals = $bean->service_interval_days_c;

        //Service is inactive, hence next_serice_date should be null
        if ($bean->inactive_service_c == true) {
            $next_date = 'null';
        } else {
            $next_date = date('Y-m-d', strtotime($lastServiceDate . '+' . $intervals . 'days'));
        }

        //new record
        if (!$isIdExist) {

            $nameEFRecord = $this->getEquipmentAndFacilityName($bean->id);
            $nameEFRecord = $nameEFRecord[0]['name'];

            $sugarQueryForGetLatestName = new SugarQuery();
            $sugarQueryForGetLatestName->from(BeanFactory::newBean('EFS_Equipment_Facility_Servi'), array('alias' => 'efs'));
            $sugarQueryForGetLatestName->select(array('efs.name'));
            $sugarQueryForGetLatestName->joinTable('equip_equipment_efs_equipment_facility_servi_1_c', array(
                'joinType' => 'LEFT',
                'alias' => 'eefs',
                'linkingTable' => true,
            ))->on()->equalsField('efs.id', 'eefs.equip_equi3f6dy_servi_idb');
            $sugarQueryForGetLatestName->joinTable('equip_equipment', array(
                'joinType' => 'LEFT',
                'alias' => 'ee',
                'linkingTable' => true,
            ))->on()->equalsField('eefs.equip_equia9d9uipment_ida', 'ee.id');
            $sugarQueryForGetLatestName->where()->equals('ee.deleted', 0)
                    ->equals('efs.deleted', 0)
                    ->equals('eefs.deleted', 0)
                    ->equals('ee.name', $nameEFRecord)
                    ->notNull('efs.name')
                    ->notEquals('efs.name', '');
            $sugarQueryForGetLatestName->orderBy('efs.date_entered', 'DESC');
            //$sugarQueryForGetLatestName->limit(1);

            $resultForSeqNo = $sugarQueryForGetLatestName->execute();
            $count = count($resultForSeqNo);
            $nameEFS = $resultForSeqNo[0]['name'];
            $nameEFRecord = str_replace("'", "\'", $nameEFRecord);
            $name = $nameEFRecord . " EFS-";

            if (!empty($nameEFS)) {
                $nameDivisionArray = explode('EFS-', $nameEFS);
                $number = $nameDivisionArray[1];
                $number = $count + 1;
            } else {
                $number = 01;
            }
            $number = sprintf('%02d', $number);
            $newName = $name . $number;
        } else {
            $newName = $bean->name;
        }

        /* Update Next Service Date */
        //Update the next service date in Equipment and Facility Module with minimum next serive date 
        $queryForNextServiceDate = "SELECT min(efs_cstm.next_service_date_c) AS next_service_date_c FROM equip_equipment ee "
                . "INNER JOIN equip_equipment_efs_equipment_facility_servi_1_c eefs ON ee.id = eefs.equip_equia9d9uipment_ida "
                . "INNER JOIN efs_equipment_facility_servi efs ON eefs.equip_equi3f6dy_servi_idb = efs.id "
                . "INNER JOIN  efs_equipment_facility_servi_cstm efs_cstm ON efs.id = efs_cstm.id_c "
                . "WHERE ee.deleted = 0 AND eefs.deleted = 0 AND efs.deleted = 0 AND ee.id = '" . $efsId . "' AND efs.id != '" . $bean->id . "'";

        $result1 = $db->query($queryForNextServiceDate);

        $efNextServiceDateBefore = '';
        $efNextServiceDateBeforeQuery = "SELECT next_service_date_c FROM equip_equipment_cstm WHERE id_c='" . $efsId . "'";
        $efResult = $db->query($efNextServiceDateBeforeQuery);
        if ($rowEF = $db->fetchByAssoc($efResult)) {
            $efNextServiceDateBefore = $rowEF['next_service_date_c'];
        }



        $nextServiceDate = '';
        if ($row = $db->fetchByAssoc($result1)) {
            $service_date1 = $next_date;
            $service_date2 = $row['next_service_date_c'];
            $last_service_date = $row['last_service_date_c'];            
            //Current record date as well as all the records associated with Equipment and Facility have
            //next service date null.
            if (($service_date1 == null || $service_date1 == 'null' || empty($service_date1)) && ($service_date2 == null || $service_date2 == 'null' || empty($service_date2))) {
                $date = '';
                $nextServiceDate = $date;
                $query = "UPDATE equip_equipment_cstm SET next_service_date_c = " . $date . " WHERE id_c='" . $efsId . "'";
            } else {
                if ((!empty($service_date1) && $service_date1 != 'null') && (empty($service_date2) || $service_date2 == '' || $service_date2 == 'null')) {
                    $nextServiceDate = $service_date1;
                } else if ((!empty($service_date2) && $service_date2 != 'null') && empty($service_date1)) {
                    $nextServiceDate = $service_date2;
                } else if ($service_date1 < $service_date2 && (!empty($service_date1) && $service_date1 != null) && (!empty($service_date2) && $service_date2 != null)) {
                    $nextServiceDate = $service_date1;
                } else if ($service_date2 < $service_date1 && (!empty($service_date1) && $service_date1 != null) && (!empty($service_date2) && $service_date2 != null)) {
                    $nextServiceDate = $service_date2;
                }
            }
            $query = "UPDATE equip_equipment_cstm SET next_service_date_c = '" . $nextServiceDate . "' WHERE id_c='" . $efsId . "'";
            $db->query($query);

            $checkQuery = "SELECT id,after_value_string from equip_equipment_audit where field_name='next_service_date_c' AND parent_id='$efsId'  order by date_created desc limit 1";
            $checkQueryResult = $db->query($checkQuery);
            $checkRow = $db->fetchByAssoc($checkQueryResult);
            $nextServiceAudit = $checkRow['after_value_string'];
            $idAudit = $checkRow['id'];

            if ($efNextServiceDateBefore != $nextServiceDate && strtotime($nextServiceAudit) != strtotime($nextServiceDate)) {
                $event_id = create_guid();

                $auditsql = 'INSERT INTO equip_equipment_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $efsId . '","' . $event_id . '",now(),"' . $current_user->id . '",now(),"next_service_date_c","date","' . $efNextServiceDateBefore . '","' . $nextServiceDate . '")';
                $auditsqlResult = $db->query($auditsql);
                if ($auditsqlResult) {
                    //Inserting audit data in audit_events table
                    $source = '{"subject":{"_type":"logic-hook","class":"EquipmentFacilityServicesLogicHook","method":"generateName"},"attributes":[]}';
                    $sql = "INSERT INTO audit_events(id,type,parent_id,module_name,source,data,date_created) values('" . $event_id . "','update','" . $efsId . "','Equip_Equipment','" . $source . "',NULL,now())";
                    $db->query($sql);
                }
            }
        }

        ////////////////////////////////     


        if ($bean->inactive_service_c == true) {
            $queryForUpdateName = "UPDATE efs_equipment_facility_servi "
                    . "LEFT JOIN efs_equipment_facility_servi_cstm ON efs_equipment_facility_servi.id = efs_equipment_facility_servi_cstm.id_c "
                    . "SET efs_equipment_facility_servi.name = '" . $newName . "' , efs_equipment_facility_servi_cstm.next_service_date_c = " . $next_date . " "
                    . "WHERE efs_equipment_facility_servi.id = '" . $bean->id . "'";
        } else {
            $queryForUpdateName = "UPDATE efs_equipment_facility_servi "
                    . "LEFT JOIN efs_equipment_facility_servi_cstm ON efs_equipment_facility_servi.id = efs_equipment_facility_servi_cstm.id_c "
                    . "SET efs_equipment_facility_servi.name = '" . $newName . "' , efs_equipment_facility_servi_cstm.next_service_date_c = '" . $next_date . "' "
                    . "WHERE efs_equipment_facility_servi.id = '" . $bean->id . "'";
        }

        $result = $db->query($queryForUpdateName);        
        if (!$result) {            
        }
        if (!$isIdExist) {
            $event_id = create_guid();
            $auditsql = 'INSERT INTO efs_equipment_facility_servi_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values
            ("' . create_guid() . '","' . $bean->id . '","' . $event_id . '",now(),"' . $current_user->id . '",now(),"name","varchar","","' . $newName . '")';
            $auditsqlResult = $db->query($auditsql);
            if ($auditsqlResult) {
                //Inserting audit data in audit_events table
                $source = '{"subject":{"_type":"logic-hook","class":"EquipmentFacilityServicesLogicHook","method":"generateName"},"attributes":[]}';
                $sql = "INSERT INTO audit_events(id,type,parent_id,module_name,source,data,date_created) values('" . $event_id . "','update','" . $efsId . "','EFS_Equipment_Facility_Servi','" . $source . "',NULL,now())";
                $db->query($sql);
            }
        }
    }

    function getEquipmentAndFacilityName($id) {
        $sugarQueryForGetEFName = new SugarQuery();
        $sugarQueryForGetEFName->from(BeanFactory::newBean('Equip_Equipment'), array('alias' => 'ee'));
        $sugarQueryForGetEFName->select(array('ee.name', 'ee.id'));
        $sugarQueryForGetEFName->joinTable('equip_equipment_efs_equipment_facility_servi_1_c', array(
            'joinType' => 'LEFT',
            'alias' => 'eefs',
            'linkingTable' => true,
        ))->on()->equalsField('ee.id', 'eefs.equip_equia9d9uipment_ida');
        $sugarQueryForGetEFName->joinTable('efs_equipment_facility_servi', array(
            'joinType' => 'LEFT',
            'alias' => 'efs',
            'linkingTable' => true,
        ))->on()->equalsField('eefs.equip_equi3f6dy_servi_idb', 'efs.id');
        $sugarQueryForGetEFName->where()->equals('ee.deleted', 0)
                ->equals('efs.deleted', 0)
                ->equals('efs.id', $id);

        $result1 = $sugarQueryForGetEFName->execute();
        return $result1;
    }

    function checkBeforeSaveNextServiceDate($bean, $event, $arguments) {
        global $db;

        $serviceID = $bean->id;

        /* Get Last Service Date */
        $queryServiceDate = "SELECT max(EFRC.service_date_c) AS service_date_c  FROM efr_equipment_facility_recor AS EFR 
						INNER JOIN efr_equipment_facility_recor_cstm AS EFRC ON EFR.id = EFRC.id_c 
						INNER JOIN efs_equipment_facility_servi_efr_equipment_facility_recor_1_c AS EFSEFR ON EFRC.id_c = EFSEFR.efs_equipm4bb2y_recor_idb 
						INNER JOIN efs_equipment_facility_servi AS EFS ON EFSEFR.efs_equipmd3d1y_servi_ida = EFS.id 
						INNER JOIN efs_equipment_facility_servi_cstm  AS EFSC ON EFS.id = EFSC.id_c 
						WHERE EFS.deleted = 0 AND EFSEFR.deleted = 0 AND EFR.deleted = 0 AND EFS.id = '" . $serviceID . "' LIMIT 0,1";
        //$GLOBALS['log']->fatal(" EFS Query ==> ".$queryServiceDate); 
        $getServiceDate = $db->query($queryServiceDate);
        if ($rowServiceDate = $db->fetchByAssoc($getServiceDate)) {
            $latestServiceDate = $rowServiceDate['service_date_c'];
            //$GLOBALS['log']->fatal(" EFS latestServiceDate 166==>>> ".$latestServiceDate); 
            if ($latestServiceDate != "") {
                if ($bean->inactive_service_c == '1') {
                    $bean->next_service_date_c = '';
                } else {
                    $bean->last_service_date_c = $latestServiceDate;
                    $serviceInterval = $bean->service_interval_days_c;
                    $next_service_date_c = date("Y-m-d", strtotime($latestServiceDate . ' + ' . $serviceInterval . ' days'));
                    $bean->next_service_date_c = $next_service_date_c; //$GLOBALS['log']->fatal(" EFS last Serve date ==> ".$latestServiceDate); 
                }
            } else {
                if ($bean->last_service_date_c != "") {
                    $serviceInterval = $bean->service_interval_days_c;
                    $next_service_date_c = date("Y-m-d", strtotime($bean->last_service_date_c . ' + ' . $serviceInterval . ' days'));
                    $bean->next_service_date_c = $next_service_date_c;
                }
            }
        }



        if ($bean->savefromEF != 1) {
            $queryEFID = "SELECT  EEC.id_c,EEC.status_c  FROM efs_equipment_facility_servi AS EFS LEFT JOIN equip_equipment_efs_equipment_facility_servi_1_c  AS EEJOINEFS ON EFS.id = EEJOINEFS.equip_equi3f6dy_servi_idb LEFT JOIN equip_equipment_cstm AS EEC  ON EEJOINEFS.equip_equia9d9uipment_ida = EEC.id_c 
	WHERE EFS.id = '" . $serviceID . "' and EFS.deleted = 0 AND EEJOINEFS.deleted = 0 ORDER BY EFS.date_entered DESC  LIMIT 0,1";
            //$GLOBALS['log']->fatal(" EFS Query ==> ".$queryEFID);		
            $getEFID = $db->query($queryEFID);
            if ($rowEFID = $db->fetchByAssoc($getEFID)) {
                $latestEFID = $rowEFID['id_c'];
                $latestEFStatus = $rowEFID['status_c'];

                if ($latestEFStatus == "Retired") {
                    $bean->next_service_date_c = '';
                    $bean->is_status_retired_c = 1;
                    //$bean->save();
                }
            }
        }
    }

    function checkNextServiceDate($bean, $event, $arguments) {
        global $db;        
        if ($arguments['related_module'] == 'Equip_Equipment' && $arguments['link'] == 'equip_equipment_efs_equipment_facility_servi_1') {

            $relatedEFID = $arguments['related_id'];
            //$GLOBALS['log']->fatal(" EFS bean  ==> ".print_r($bean,1));
            $queryEFID = "SELECT  EEC.id_c,EEC.status_c FROM equip_equipment_cstm AS EEC  WHERE EEC.id_c = '" . $relatedEFID . "'";
            //$GLOBALS['log']->fatal(" EFS Query ==> ".$queryEFID);		
            $getEFID = $db->query($queryEFID);

            if ($rowEFID = $db->fetchByAssoc($getEFID)) {
                $latestEFID = $rowEFID['id_c'];
                $latestEFStatus = $rowEFID['status_c'];                
                if ($latestEFStatus == "Retired") {
                    $bean->next_service_date_c = '';
                    $bean->is_status_retired_c = 1;
                    if ($relatedEFID != "") {
                        $Equip_Equipment = BeanFactory::retrieveBean('Equip_Equipment', $relatedEFID);
                        $Equip_Equipment->save();
                    }
                }
            }
        }
    }

}

?>