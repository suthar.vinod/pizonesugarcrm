<?php
// created: 2019-02-25 15:23:00
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'category_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_CATEGORY',
    'width' => 10,
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'service_interval_days_c' => 
  array (
    'type' => 'int',
    'vname' => 'LBL_SERVICE_INTERVAL_DAYS',
    'width' => 10,
    'default' => true,
  ),
  'last_service_date_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_LAST_SERVICE_DATE',
    'width' => 10,
    'default' => true,
  ),
  'next_service_date_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_NEXT_SERVICE_DATE',
    'width' => 10,
    'default' => true,
  ),
);