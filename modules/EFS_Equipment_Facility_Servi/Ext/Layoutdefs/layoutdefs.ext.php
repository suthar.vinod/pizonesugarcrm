<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Layoutdefs/efs_equipment_facility_servi_efr_equipment_facility_recor_1_EFS_Equipment_Facility_Servi.php

 // created: 2019-02-25 15:18:39
$layout_defs["EFS_Equipment_Facility_Servi"]["subpanel_setup"]['efs_equipment_facility_servi_efr_equipment_facility_recor_1'] = array (
  'order' => 100,
  'module' => 'EFR_Equipment_Facility_Recor',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE',
  'get_subpanel_data' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Layoutdefs/_overrideEFS_Equipment_Facility_Servi_subpanel_efs_equipment_facility_servi_efr_equipment_facility_recor_1.php

//auto-generated file DO NOT EDIT
$layout_defs['EFS_Equipment_Facility_Servi']['subpanel_setup']['efs_equipment_facility_servi_efr_equipment_facility_recor_1']['override_subpanel_name'] = 'EFS_Equipment_Facility_Servi_subpanel_efs_equipment_facility_servi_efr_equipment_facility_recor_1';

?>
