<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Language/en_us.customequip_equipment_efs_equipment_facility_servi_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Language/en_us.customequip_equipment_efs_equipment_facility_servi_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment ';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE'] = 'Equipment ';

?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment ';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE'] = 'Equipment ';


?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CATEGORY'] = 'Category';
$mod_strings['LBL_DESCRIPTION'] = 'Service Description';
$mod_strings['LBL_SERVICE_INTERVAL_DAYS'] = 'Service Interval (Days)';
$mod_strings['LBL_LAST_SERVICE_DATE'] = 'Last Service Date';
$mod_strings['LBL_NEXT_SERVICE_DATE'] = 'Next Service Date';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment &amp; Facility';
$mod_strings['LNK_NEW_RECORD'] = 'Create Equipment &amp; Facility Service';
$mod_strings['LNK_LIST'] = 'View Equipment &amp; Facility Services';
$mod_strings['LBL_MODULE_NAME'] = 'Equipment &amp; Facility Services';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Equipment &amp; Facility Service';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Equipment &amp; Facility Service';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Equipment &amp; Facility Service vCard';
$mod_strings['LNK_IMPORT_EFS_EQUIPMENT_FACILITY_SERVI'] = 'Import Equipment &amp; Facility Services';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Equipment &amp; Facility Services List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Equipment &amp; Facility Service';
$mod_strings['LBL_EFS_EQUIPMENT_FACILITY_SERVI_SUBPANEL_TITLE'] = 'Equipment &amp; Facility Services';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Equipment &amp; Facility Services';
$mod_strings['LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment &amp; Facility Records';
$mod_strings['LBL_RESPONSIBLE_PERSONNEL_CONTACT_ID'] = 'Responsible Personnel (related Contact ID)';
$mod_strings['LBL_RESPONSIBLE_PERSONNEL'] = 'Responsible Personnel';
$mod_strings['LBL_IS_STATUS_RETIRED'] = 'is status retired';
$mod_strings['LBL_INACTIVE_SERVICE'] = 'Inactive Service';
$mod_strings['LBL_EFS_EQUIPMENT_FACILITY_SERVI_FOCUS_DRAWER_DASHBOARD'] = 'Equipment &amp; Facility Services Focus Drawer';
$mod_strings['LBL_EFS_EQUIPMENT_FACILITY_SERVI_RECORD_DASHBOARD'] = 'Equipment &amp; Facility Services Record Dashboard';

?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Language/en_us.customefs_equipment_facility_servi_efr_equipment_facility_recor_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment &amp; Facility Records';
$mod_strings['LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE'] = 'Equipment &amp; Facility Records';

?>
