<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Vardefs/equip_equipment_efs_equipment_facility_servi_1_EFS_Equipment_Facility_Servi.php

// created: 2019-02-25 15:17:34
$dictionary["EFS_Equipment_Facility_Servi"]["fields"]["equip_equipment_efs_equipment_facility_servi_1"] = array (
  'name' => 'equip_equipment_efs_equipment_facility_servi_1',
  'type' => 'link',
  'relationship' => 'equip_equipment_efs_equipment_facility_servi_1',
  'source' => 'non-db',
  'module' => 'Equip_Equipment',
  'bean_name' => 'Equip_Equipment',
  'side' => 'right',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE',
  'id_name' => 'equip_equia9d9uipment_ida',
  'link-type' => 'one',
);
$dictionary["EFS_Equipment_Facility_Servi"]["fields"]["equip_equipment_efs_equipment_facility_servi_1_name"] = array (
  'name' => 'equip_equipment_efs_equipment_facility_servi_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'save' => true,
  'id_name' => 'equip_equia9d9uipment_ida',
  'link' => 'equip_equipment_efs_equipment_facility_servi_1',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'name',
  'required' => 'true',  
);
$dictionary["EFS_Equipment_Facility_Servi"]["fields"]["equip_equia9d9uipment_ida"] = array (
  'name' => 'equip_equia9d9uipment_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE_ID',
  'id_name' => 'equip_equia9d9uipment_ida',
  'link' => 'equip_equipment_efs_equipment_facility_servi_1',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Vardefs/sugarfield_name.php

 // created: 2019-02-25 15:06:50
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['len']='255';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['audited']=true;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['massupdate']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['unified_search']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.0',
  'searchable' => true,
);
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['calculated']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['required']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['name']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Vardefs/sugarfield_last_service_date_c.php

 // created: 2019-02-28 12:56:43
$dictionary['EFS_Equipment_Facility_Servi']['fields']['last_service_date_c']['labelValue']='Last Service Date';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['last_service_date_c']['enforced']= '';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['last_service_date_c']['dependency']='';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['last_service_date_c']['required']=true;
 
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Vardefs/efs_equipment_facility_servi_efr_equipment_facility_recor_1_EFS_Equipment_Facility_Servi.php

// created: 2019-02-25 15:18:39
$dictionary["EFS_Equipment_Facility_Servi"]["fields"]["efs_equipment_facility_servi_efr_equipment_facility_recor_1"] = array (
  'name' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'type' => 'link',
  'relationship' => 'efs_equipment_facility_servi_efr_equipment_facility_recor_1',
  'source' => 'non-db',
  'module' => 'EFR_Equipment_Facility_Recor',
  'bean_name' => 'EFR_Equipment_Facility_Recor',
  'vname' => 'LBL_EFS_EQUIPMENT_FACILITY_SERVI_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE',
  'id_name' => 'efs_equipmd3d1y_servi_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Vardefs/sugarfield_category_c.php

 // created: 2019-02-25 15:11:38
$dictionary['EFS_Equipment_Facility_Servi']['fields']['category_c']['labelValue']='Category';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['category_c']['dependency']='';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['category_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Vardefs/sugarfield_description.php

 // created: 2019-02-25 15:12:14
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['audited']=true;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['massupdate']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['comments']='Full text of the note';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['duplicate_merge']='enabled';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['merge_filter']='disabled';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['unified_search']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['calculated']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['rows']='6';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Vardefs/sugarfield_service_interval_days_c.php

 // created: 2019-02-25 15:13:17
$dictionary['EFS_Equipment_Facility_Servi']['fields']['service_interval_days_c']['labelValue']='Service Interval (Days)';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['service_interval_days_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['EFS_Equipment_Facility_Servi']['fields']['service_interval_days_c']['enforced']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['service_interval_days_c']['dependency']=false;
$dictionary['EFS_Equipment_Facility_Servi']['fields']['service_interval_days_c']['required'] = true;

 
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Vardefs/sugarfield_contact_id_c.php

 // created: 2019-06-12 11:34:40

 
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Vardefs/sugarfield_responsible_personnel_c.php

 // created: 2019-06-12 11:44:43
$dictionary['EFS_Equipment_Facility_Servi']['fields']['responsible_personnel_c']['labelValue']='Responsible Personnel';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['responsible_personnel_c']['dependency']='equal(related($equip_equipment_efs_equipment_facility_servi_1,"department_c"),"Facilities")';

 
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Vardefs/sugarfield_is_status_retired_c.php

 // created: 2019-07-10 07:15:31
$dictionary['EFS_Equipment_Facility_Servi']['fields']['is_status_retired_c']['labelValue']='is status retired';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['is_status_retired_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['EFS_Equipment_Facility_Servi']['fields']['is_status_retired_c']['enforced']='';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['is_status_retired_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Vardefs/sugarfield_next_service_date_c.php

 // created: 2019-07-11 05:45:02
$dictionary['EFS_Equipment_Facility_Servi']['fields']['next_service_date_c']['labelValue']='Next Service Date';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['next_service_date_c']['enforced']='';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['next_service_date_c']['dependency']='and(not(equal($is_status_retired_c,"1")),equal($inactive_service_c,false))';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['next_service_date_c']['readonly']= true;

 
?>
<?php
// Merged from custom/Extension/modules/EFS_Equipment_Facility_Servi/Ext/Vardefs/sugarfield_inactive_service_c.php

 // created: 2019-12-10 15:13:24
$dictionary['EFS_Equipment_Facility_Servi']['fields']['inactive_service_c']['labelValue']='Inactive Service';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['inactive_service_c']['enforced']='';
$dictionary['EFS_Equipment_Facility_Servi']['fields']['inactive_service_c']['dependency']='';

 
?>
