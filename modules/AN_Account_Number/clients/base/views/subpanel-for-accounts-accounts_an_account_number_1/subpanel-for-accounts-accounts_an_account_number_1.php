<?php
// created: 2021-02-25 08:44:36
$viewdefs['AN_Account_Number']['base']['view']['subpanel-for-accounts-accounts_an_account_number_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'account_type',
          'label' => 'LBL_ACCOUNT_TYPE',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'ship_to_address',
          'label' => 'LBL_SHIP_TO_ADDRESS',
          'enabled' => true,
          'readonly' => false,
          'id' => 'CA_COMPANY_ADDRESS_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);