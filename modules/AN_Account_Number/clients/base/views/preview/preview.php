<?php
$module_name = 'AN_Account_Number';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
                'size' => 'large',
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'accounts_an_account_number_1_name',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'account_type',
                'label' => 'LBL_ACCOUNT_TYPE',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'ship_to_address',
                'studio' => 'visible',
                'label' => 'LBL_SHIP_TO_ADDRESS',
                'span' => 12,
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'account_number_address_c',
                'studio' => 'visible',
                'label' => 'LBL_ACCOUNT_NUMBER_ADDRESS',
                'span' => 12,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);
