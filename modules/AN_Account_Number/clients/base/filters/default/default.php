<?php
// created: 2021-12-14 09:05:45
$viewdefs['AN_Account_Number']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'account_number_address_c' => 
    array (
    ),
    'account_number_email_address_c' => 
    array (
    ),
    'account_number_phone_number_c' => 
    array (
    ),
    'account_type' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'accounts_an_account_number_1_name' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'ship_to_address' => 
    array (
    ),
    'tag' => 
    array (
    ),
  ),
);