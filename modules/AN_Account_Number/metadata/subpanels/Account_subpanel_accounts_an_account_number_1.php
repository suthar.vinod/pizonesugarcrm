<?php
// created: 2021-02-25 08:44:35
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'account_type' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ACCOUNT_TYPE',
    'width' => 10,
  ),
  'ship_to_address' => 
  array (
    'readonly' => false,
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_SHIP_TO_ADDRESS',
    'id' => 'CA_COMPANY_ADDRESS_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'CA_Company_Address',
    'target_record_key' => 'ca_company_address_id_c',
  ),
);