<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/AN_Account_Number/Ext/Vardefs/accounts_an_account_number_1_AN_Account_Number.php

// created: 2021-02-25 08:41:49
$dictionary["AN_Account_Number"]["fields"]["accounts_an_account_number_1"] = array (
  'name' => 'accounts_an_account_number_1',
  'type' => 'link',
  'relationship' => 'accounts_an_account_number_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_AN_ACCOUNT_NUMBER_TITLE',
  'id_name' => 'accounts_an_account_number_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["AN_Account_Number"]["fields"]["accounts_an_account_number_1_name"] = array (
  'name' => 'accounts_an_account_number_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_an_account_number_1accounts_ida',
  'link' => 'accounts_an_account_number_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["AN_Account_Number"]["fields"]["accounts_an_account_number_1accounts_ida"] = array (
  'name' => 'accounts_an_account_number_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_AN_ACCOUNT_NUMBER_TITLE_ID',
  'id_name' => 'accounts_an_account_number_1accounts_ida',
  'link' => 'accounts_an_account_number_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/AN_Account_Number/Ext/Vardefs/sugarfield_ca_company_address_id1_c.php

 // created: 2021-10-14 10:20:24

 
?>
<?php
// Merged from custom/Extension/modules/AN_Account_Number/Ext/Vardefs/sugarfield_account_number_address_c.php

 // created: 2021-10-14 10:20:24
$dictionary['AN_Account_Number']['fields']['account_number_address_c']['labelValue']='Account Number Address';
$dictionary['AN_Account_Number']['fields']['account_number_address_c']['dependency']='';
$dictionary['AN_Account_Number']['fields']['account_number_address_c']['required_formula']='';
$dictionary['AN_Account_Number']['fields']['account_number_address_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/AN_Account_Number/Ext/Vardefs/sugarfield_accounts_an_account_number_1accounts_ida.php

 // created: 2021-11-02 17:55:38
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['name']='accounts_an_account_number_1accounts_ida';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['type']='id';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['source']='non-db';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['vname']='LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_AN_ACCOUNT_NUMBER_TITLE_ID';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['id_name']='accounts_an_account_number_1accounts_ida';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['link']='accounts_an_account_number_1';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['table']='accounts';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['module']='Accounts';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['rname']='id';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['reportable']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['side']='right';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['massupdate']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['duplicate_merge']='disabled';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['hideacl']=true;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['audited']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1accounts_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/AN_Account_Number/Ext/Vardefs/sugarfield_accounts_an_account_number_1_name.php

 // created: 2021-11-02 17:55:41
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['required']=true;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['audited']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['massupdate']=true;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['hidemassupdate']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['duplicate_merge']='enabled';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['duplicate_merge_dom_value']='1';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['merge_filter']='disabled';
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['reportable']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['unified_search']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['calculated']=false;
$dictionary['AN_Account_Number']['fields']['accounts_an_account_number_1_name']['vname']='LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/AN_Account_Number/Ext/Vardefs/sugarfield_account_number_phone_number_c.php

 // created: 2021-12-14 08:58:08
$dictionary['AN_Account_Number']['fields']['account_number_phone_number_c']['labelValue']='Account Number Phone Number';
$dictionary['AN_Account_Number']['fields']['account_number_phone_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['AN_Account_Number']['fields']['account_number_phone_number_c']['enforced']='';
$dictionary['AN_Account_Number']['fields']['account_number_phone_number_c']['dependency']='';
$dictionary['AN_Account_Number']['fields']['account_number_phone_number_c']['required_formula']='';
$dictionary['AN_Account_Number']['fields']['account_number_phone_number_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/AN_Account_Number/Ext/Vardefs/sugarfield_account_number_email_address_c.php

 // created: 2021-12-14 09:03:29
$dictionary['AN_Account_Number']['fields']['account_number_email_address_c']['labelValue']='Account Number Email Address';
$dictionary['AN_Account_Number']['fields']['account_number_email_address_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['AN_Account_Number']['fields']['account_number_email_address_c']['enforced']='';
$dictionary['AN_Account_Number']['fields']['account_number_email_address_c']['dependency']='';
$dictionary['AN_Account_Number']['fields']['account_number_email_address_c']['required_formula']='';
$dictionary['AN_Account_Number']['fields']['account_number_email_address_c']['readonly_formula']='';

 
?>
