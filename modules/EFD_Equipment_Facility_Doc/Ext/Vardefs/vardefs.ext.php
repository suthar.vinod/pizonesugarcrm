<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/EFD_Equipment_Facility_Doc/Ext/Vardefs/sugarfield_document_name.php

 // created: 2019-02-19 17:40:35
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['audited']=true;
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['massupdate']=false;
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['merge_filter']='disabled';
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['unified_search']=false;
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.0',
  'searchable' => true,
);
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['calculated']=false;
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['readonly'] = true;
$dictionary['EFD_Equipment_Facility_Doc']['fields']['document_name']['required'] = false;
 
?>
<?php
// Merged from custom/Extension/modules/EFD_Equipment_Facility_Doc/Ext/Vardefs/equip_equipment_efd_equipment_facility_doc_1_EFD_Equipment_Facility_Doc.php

// created: 2019-02-19 17:46:13
$dictionary["EFD_Equipment_Facility_Doc"]["fields"]["equip_equipment_efd_equipment_facility_doc_1"] = array (
  'name' => 'equip_equipment_efd_equipment_facility_doc_1',
  'type' => 'link',
  'relationship' => 'equip_equipment_efd_equipment_facility_doc_1',
  'source' => 'non-db',
  'module' => 'Equip_Equipment',
  'bean_name' => 'Equip_Equipment',
  'side' => 'right',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EFD_EQUIPMENT_FACILITY_DOC_TITLE',
  'id_name' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
  'link-type' => 'one',
);
$dictionary["EFD_Equipment_Facility_Doc"]["fields"]["equip_equipment_efd_equipment_facility_doc_1_name"] = array (
  'name' => 'equip_equipment_efd_equipment_facility_doc_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'save' => true,
  'id_name' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
  'link' => 'equip_equipment_efd_equipment_facility_doc_1',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'name',
);
$dictionary["EFD_Equipment_Facility_Doc"]["fields"]["equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida"] = array (
  'name' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EFD_EQUIPMENT_FACILITY_DOC_TITLE_ID',
  'id_name' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
  'link' => 'equip_equipment_efd_equipment_facility_doc_1',
  'table' => 'equip_equipment',
  'module' => 'Equip_Equipment',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/EFD_Equipment_Facility_Doc/Ext/Vardefs/sugarfield_type_2_c.php

 // created: 2019-05-23 18:20:56
$dictionary['EFD_Equipment_Facility_Doc']['fields']['type_2_c']['labelValue']='Type';
$dictionary['EFD_Equipment_Facility_Doc']['fields']['type_2_c']['dependency']='';
$dictionary['EFD_Equipment_Facility_Doc']['fields']['type_2_c']['visibility_grid']='';

 
?>
