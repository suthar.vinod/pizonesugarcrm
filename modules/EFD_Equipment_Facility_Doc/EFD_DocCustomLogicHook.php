<?php

class EFD_DocCustomLogicHook {

    function generateFileName($bean, $event, $arguments) {
        
        global $db;

        $name = $this->getEquipFacilityName($bean->id);
        $type = $bean->type_2_c;
        $firstName = $name . ' ' . $type . ' ' . 'D-';
   
        $isIdExist = $bean->fetched_row['id'];

        if (!$isIdExist) {
            $number = $this->getLatestUploadedRecordNumber($bean->id, $type, $name);
            $finalName = $firstName . $number;       
        } else {
            $document_name = explode('-', $bean->document_name);
            $finalName = $firstName . $document_name[1];
        }
        
        if(!empty($bean->filename)) {
            $fileName = explode('.', $bean->filename);
            $fileName[0] = $finalName;
            $newFileName = $fileName[0] . '.' . $fileName[1];
        }
        else {
            $newFileName = '';
        }
        
            $bean->filename = $newFileName;
            $bean->document_name = $finalName;
            $queryForUpdateFileName = "UPDATE efd_equipment_facility_doc SET filename = '" . $newFileName . "', document_name = '" . $finalName . "' "
                    . "WHERE id = '" . $bean->id . "'";
            $db->query($queryForUpdateFileName);
    }

    function getEquipFacilityName($id) {
        $queryToGetEFS_Name = new SugarQuery();
        $queryToGetEFS_Name->from(BeanFactory::newBean('Equip_Equipment'), array('alias' => 'ee'));
        $queryToGetEFS_Name->select(array('ee.name'));
        $queryToGetEFS_Name->joinTable('equip_equipment_efd_equipment_facility_doc_1_c', array(
            'joinType' => 'LEFT',
            'alias' => 'efdoc',
            'linkingTable' => true,
        ))->on()->equalsField('ee.id', 'efdoc.equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida');
        $queryToGetEFS_Name->joinTable('efd_equipment_facility_doc', array(
            'joinType' => 'LEFT',
            'alias' => 'doc',
            'linkingTable' => true,
        ))->on()->equalsField('efdoc.equip_equiffa6ity_doc_idb', 'doc.id');
        $queryToGetEFS_Name->where()->equals('doc.deleted', 0)
                ->equals('efdoc.deleted', 0)
                ->equals('ee.deleted', 0)
                ->equals('doc.id', $id);
        $queryToGetEFS_Name->limit(1);

        $resultForName = $queryToGetEFS_Name->execute();
        $name = $resultForName[0]['name'];

        return $name;
        
    }

    function getLatestUploadedRecordNumber($id, $type, $name) {
        $queryToGetDocName = new SugarQuery();
        $queryToGetDocName->from(BeanFactory::newBean('EFD_Equipment_Facility_Doc'), array('alias' => 'doc'));
        $queryToGetDocName->select(array('doc.document_name'));
        $queryToGetDocName->where()->equals('doc.deleted', 0)
                ->contains('doc.document_name', $type)
                ->contains('doc.document_name', $name)
                ->notEquals('doc.id', $id);
        $queryToGetDocName->orderBy('doc.date_entered', 'DESC');
        $queryToGetDocName->limit(1);

        $resultForName = $queryToGetDocName->execute();
        $name = $resultForName[0]['document_name'];

        if (!empty($name)) {
            $nameDivisionArray = explode(' D-', $name);
            $number = $nameDivisionArray[1];
            $number = $number + 1;
        } else {
            $number = 01;
        }
        $number = sprintf('%02d', $number);
        return $number;
    }

}
