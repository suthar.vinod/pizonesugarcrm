<?php
// created: 2016-01-06 02:12:14
$viewdefs['Notes']['base']['menu']['quickcreate'] = array (
  'layout' => 'create',
  'label' => 'LNK_NEW_NOTE',
  'visible' => true,
  'order' => 10,
  'icon' => 'fa-plus',
  'related' => 
  array (
    0 => 
    array (
      'module' => 'Contacts',
      'link' => 'notes',
    ),
  ),
);