<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:55
$dictionary['Note']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_notes_Notes.php

// created: 2017-09-13 15:16:04
$dictionary["Note"]["fields"]["a1a_critical_phase_inspectio_activities_1_notes"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_notes',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_notes',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_NOTES_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/m01_sales_activities_1_notes_Notes.php

// created: 2018-12-10 23:02:35
$dictionary["Note"]["fields"]["m01_sales_activities_1_notes"] = array (
  'name' => 'm01_sales_activities_1_notes',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_notes',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_NOTES_FROM_M01_SALES_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/m03_work_product_activities_1_notes_Notes.php

// created: 2019-08-05 12:03:53
$dictionary["Note"]["fields"]["m03_work_product_activities_1_notes"] = array (
  'name' => 'm03_work_product_activities_1_notes',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_notes',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_NOTES_FROM_M03_WORK_PRODUCT_TITLE',
);

?>
