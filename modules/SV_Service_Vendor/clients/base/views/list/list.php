<?php
$module_name = 'SV_Service_Vendor';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'service_vendor',
                'label' => 'LBL_SERVICE_VENDOR',
                'enabled' => true,
                'id' => 'ACCOUNT_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'equip_equipment_sv_service_vendor_1_name',
                'label' => 'LBL_EQUIP_EQUIPMENT_SV_SERVICE_VENDOR_1_FROM_EQUIP_EQUIPMENT_TITLE',
                'enabled' => true,
                'id' => 'EQUIP_EQUIPMENT_SV_SERVICE_VENDOR_1EQUIP_EQUIPMENT_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
