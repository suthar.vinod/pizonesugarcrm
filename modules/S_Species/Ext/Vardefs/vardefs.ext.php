<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/S_Species/Ext/Vardefs/s_species_sc_species_census_1_S_Species.php

// created: 2021-04-29 06:58:32
$dictionary["S_Species"]["fields"]["s_species_sc_species_census_1"] = array (
  'name' => 's_species_sc_species_census_1',
  'type' => 'link',
  'relationship' => 's_species_sc_species_census_1',
  'source' => 'non-db',
  'module' => 'SC_Species_Census',
  'bean_name' => 'SC_Species_Census',
  'vname' => 'LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_S_SPECIES_TITLE',
  'id_name' => 's_species_sc_species_census_1s_species_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
