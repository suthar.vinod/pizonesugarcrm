<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-04-29 06:58:32
$viewdefs['S_Species']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_SC_SPECIES_CENSUS_TITLE',
  'context' => 
  array (
    'link' => 's_species_sc_species_census_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['S_Species']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 's_species_sc_species_census_1',
  'view' => 'subpanel-for-s_species-s_species_sc_species_census_1',
);
