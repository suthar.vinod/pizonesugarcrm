<?php
// created: 2021-01-12 16:31:08
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'type_2' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TYPE_2',
    'width' => 10,
  ),
  'subtype' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_SUBTYPE',
    'width' => 10,
  ),
  'roles' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ROLES',
    'width' => 10,
  ),
  'room' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_ROOM',
    'id' => 'RMS_ROOM_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'RMS_Room',
    'target_record_key' => 'rms_room_id_c',
  ),
  'species_2_c' => 
  array (
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_SPECIES_2',
    'width' => 10,
  ),
  'format' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_FORMAT',
    'width' => 10,
  ),
  'price' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_PRICE',
    'currency_format' => true,
    'width' => 10,
  ),
  'internal_barcode' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'readonly' => true,
    'vname' => 'LBL_INTERNAL_BARCODE',
    'width' => 10,
  ),
);