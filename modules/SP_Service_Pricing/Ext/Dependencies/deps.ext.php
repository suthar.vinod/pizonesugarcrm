<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SP_Service_Pricing/Ext/Dependencies/format_price_dep.php


$dependencies['SP_Service_Pricing']['set_price_required'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('format'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'price',
                'value' => 'isInList($format,createList("Each","Hourly","Task"))',
            ),
        ), 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'price',
                'value' => 'isInList($format,createList("Combination"))',
            ),
        ),     
    ),
); 

$dependencies['SP_Service_Pricing']['set_format_readonly'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('id'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'format',
                'value' => 'greaterThan(strlen($id),0)',
            ),
        ),     
    ),
);

?>
