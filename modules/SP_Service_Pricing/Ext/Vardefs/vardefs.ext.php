<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SP_Service_Pricing/Ext/Vardefs/sp_service_pricing_sp_service_pricing_1_SP_Service_Pricing.php

// created: 2021-01-12 11:07:14
$dictionary["SP_Service_Pricing"]["fields"]["sp_service_pricing_sp_service_pricing_1"] = array (
  'name' => 'sp_service_pricing_sp_service_pricing_1',
  'type' => 'link',
  'relationship' => 'sp_service_pricing_sp_service_pricing_1',
  'source' => 'non-db',
  'module' => 'SP_Service_Pricing',
  'bean_name' => 'SP_Service_Pricing',
  'vname' => 'LBL_SP_SERVICE_PRICING_SP_SERVICE_PRICING_1_FROM_SP_SERVICE_PRICING_L_TITLE',
  'id_name' => 'sp_service_pricing_sp_service_pricing_1sp_service_pricing_ida',
);
$dictionary["SP_Service_Pricing"]["fields"]["sp_service_pricing_sp_service_pricing_1"] = array (
  'name' => 'sp_service_pricing_sp_service_pricing_1',
  'type' => 'link',
  'relationship' => 'sp_service_pricing_sp_service_pricing_1',
  'source' => 'non-db',
  'module' => 'SP_Service_Pricing',
  'bean_name' => 'SP_Service_Pricing',
  'vname' => 'LBL_SP_SERVICE_PRICING_SP_SERVICE_PRICING_1_FROM_SP_SERVICE_PRICING_R_TITLE',
  'id_name' => 'sp_service_pricing_sp_service_pricing_1sp_service_pricing_ida',
);

?>
<?php
// Merged from custom/Extension/modules/SP_Service_Pricing/Ext/Vardefs/sugarfield_species_2_c.php

 // created: 2021-01-12 11:36:22
$dictionary['SP_Service_Pricing']['fields']['species_2_c']['labelValue']='Species';
$dictionary['SP_Service_Pricing']['fields']['species_2_c']['dependency']='';
$dictionary['SP_Service_Pricing']['fields']['species_2_c']['required_formula']='';
$dictionary['SP_Service_Pricing']['fields']['species_2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/SP_Service_Pricing/Ext/Vardefs/sugarfield_species.php

 // created: 2021-01-12 11:37:24

 
?>
<?php
// Merged from custom/Extension/modules/SP_Service_Pricing/Ext/Vardefs/sugarfield_type_2.php

 // created: 2021-01-12 11:41:22

 
?>
<?php
// Merged from custom/Extension/modules/SP_Service_Pricing/Ext/Vardefs/sugarfield_internal_barcode.php

 // created: 2020-09-19 10:23:48
 $dictionary['SP_Service_Pricing']['fields']['internal_barcode']['required']=false;
 $dictionary['SP_Service_Pricing']['fields']['internal_barcode']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/SP_Service_Pricing/Ext/Vardefs/sugarfield_price.php

 // created: 2021-09-04 04:30:27
$dictionary['SP_Service_Pricing']['fields']['price']['required']=true;
$dictionary['SP_Service_Pricing']['fields']['price']['name']='price';
$dictionary['SP_Service_Pricing']['fields']['price']['vname']='LBL_PRICE';
$dictionary['SP_Service_Pricing']['fields']['price']['type']='currency';
$dictionary['SP_Service_Pricing']['fields']['price']['massupdate']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['hidemassupdate']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['no_default']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['comments']='';
$dictionary['SP_Service_Pricing']['fields']['price']['help']='';
$dictionary['SP_Service_Pricing']['fields']['price']['importable']='true';
$dictionary['SP_Service_Pricing']['fields']['price']['duplicate_merge']='enabled';
$dictionary['SP_Service_Pricing']['fields']['price']['duplicate_merge_dom_value']='1';
$dictionary['SP_Service_Pricing']['fields']['price']['audited']=true;
$dictionary['SP_Service_Pricing']['fields']['price']['reportable']=true;
$dictionary['SP_Service_Pricing']['fields']['price']['unified_search']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['merge_filter']='disabled';
$dictionary['SP_Service_Pricing']['fields']['price']['pii']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['default']='';
$dictionary['SP_Service_Pricing']['fields']['price']['calculated']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['len']=26;
$dictionary['SP_Service_Pricing']['fields']['price']['size']='20';
$dictionary['SP_Service_Pricing']['fields']['price']['enable_range_search']=false;
$dictionary['SP_Service_Pricing']['fields']['price']['precision']=6;
$dictionary['SP_Service_Pricing']['fields']['price']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['SP_Service_Pricing']['fields']['price']['convertToBase']=true;
$dictionary['SP_Service_Pricing']['fields']['price']['showTransactionalAmount']=true;

 
?>
