<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SP_Service_Pricing/Ext/Layoutdefs/sp_service_pricing_sp_service_pricing_1_SP_Service_Pricing.php

 // created: 2021-01-12 11:07:14
$layout_defs["SP_Service_Pricing"]["subpanel_setup"]['sp_service_pricing_sp_service_pricing_1'] = array (
  'order' => 100,
  'module' => 'SP_Service_Pricing',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SP_SERVICE_PRICING_SP_SERVICE_PRICING_1_FROM_SP_SERVICE_PRICING_R_TITLE',
  'get_subpanel_data' => 'sp_service_pricing_sp_service_pricing_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/SP_Service_Pricing/Ext/Layoutdefs/_overrideSP_Service_Pricing_subpanel_sp_service_pricing_sp_service_pricing_1.php

//auto-generated file DO NOT EDIT
$layout_defs['SP_Service_Pricing']['subpanel_setup']['sp_service_pricing_sp_service_pricing_1']['override_subpanel_name'] = 'SP_Service_Pricing_subpanel_sp_service_pricing_sp_service_pricing_1';

?>
