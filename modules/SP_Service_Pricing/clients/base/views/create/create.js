({
    extendsFrom: 'CreateView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.on("render", this.function_auto_populate_internal_barcode, this);
    },

    function_auto_populate_internal_barcode: function () {
        var internal_barcode = this.model.get('internal_barcode');
        var self = this;       
                 $('[name="internal_barcode"]').attr('disabled',true);
                 $('div[data-name="internal_barcode"]').css('pointer-events', 'none');
    },

})


 