<?php
// created: 2021-01-12 16:31:08
$viewdefs['SP_Service_Pricing']['base']['view']['subpanel-for-sp_service_pricing-sp_service_pricing_sp_service_pricing_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'type_2',
          'label' => 'LBL_TYPE_2',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'subtype',
          'label' => 'LBL_SUBTYPE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'roles',
          'label' => 'LBL_ROLES',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'room',
          'label' => 'LBL_ROOM',
          'enabled' => true,
          'id' => 'RMS_ROOM_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'species_2_c',
          'label' => 'LBL_SPECIES_2',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'format',
          'label' => 'LBL_FORMAT',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'price',
          'label' => 'LBL_PRICE',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'internal_barcode',
          'label' => 'LBL_INTERNAL_BARCODE',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);