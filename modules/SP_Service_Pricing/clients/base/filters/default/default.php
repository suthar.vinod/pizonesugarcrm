<?php
// created: 2021-01-12 11:46:38
$viewdefs['SP_Service_Pricing']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'species_2_c' => 
    array (
    ),
    'internal_barcode' => 
    array (
    ),
    'team_name' => 
    array (
    ),
    'roles' => 
    array (
    ),
    'price' => 
    array (
    ),
    'room' => 
    array (
    ),
    'subtype' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'format' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'type_2' => 
    array (
    ),
    'description' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);