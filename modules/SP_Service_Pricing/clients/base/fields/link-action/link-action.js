({
    extendsFrom: 'LinkActionField',

    getDrawerOptions: function() {
        var parentModel = this.context.get('parentModel');
        var linkModule = this.context.get('module');
        var link = this.context.get('link');
        var filterOptions;

        if(parentModel.module === 'SP_Service_Pricing') {
            filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterSPPrice',
                'initial_filter_label': 'FilterFormatHourlyTask',
                'filter_populate': {
                    'format': 'Hourly',
                }
            });
            var self = this;
            setTimeout(function(){ self.lockFilter(); }, 1000); 
        }
        else {

            $("button.btn.btn-invisible.btn-dark").css("display", "block");
            $(".filter-definition-container").css('pointer-events', 'unset');
            $(".choice-filter-clickable").css('pointer-events', 'unset');
            
            filterOptions = new app.utils.FilterOptions().config(this.def);
            filterOptions.setInitialFilter(this.def.initial_filter || '$relate');
            filterOptions.populateRelate(parentModel);
        }

        return {
            layout: 'multi-selection-list-link',
            context: {
                module: linkModule,
                recParentModel: parentModel,
                recLink: link,
                recContext: this.context,
                recView: this.view,
                independentMassCollection: true,
                filterOptions: filterOptions.format()
            }
        };
    },

    lockFilter: function() {
         $("button.btn.btn-invisible.btn-dark").css("display", "none");
        $(".filter-definition-container").css('pointer-events', 'none');
        $(".choice-filter-clickable").css('pointer-events', 'none');
        $(".search-filter").css('pointer-events', 'none');
    } 

})