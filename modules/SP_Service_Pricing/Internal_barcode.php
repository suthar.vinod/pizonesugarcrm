<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class Internal_barcode_class{
	function UpdateInternal_barcodeHook($bean, $event, $arguments){
		global $db;
		if($bean->name !="" && ($bean->id != $bean->fetched_row['id'])  ){
			$sql = "SELECT * FROM `sp_service_pricing` WHERE  `deleted`!=1 ORDER BY `internal_barcode` desc limit 1";
			$result = $db->query($sql);	
			$row = $db->fetchByAssoc($result);	
 			$internal_barcode = $row['internal_barcode'];
			if($result->num_rows == 0)
			 {
					$number = sprintf("%07d", 1);
					$internal_barcode= 'APS PI '.$number;					 
					$bean->internal_barcode = $internal_barcode;
			 } else {

					// -- Old Code --
					// $internal_barcode_old = strrchr($internal_barcode,"APS PI");
					// $search = 'APS PI' ;
					// $internal_barcode2 = str_replace($search, '', $internal_barcode_old) ;

					$lastBarcodeNo=explode(" ",$internal_barcode);
					$lastBarcodeNo=end($lastBarcodeNo);
					$internal_barcode_new = $lastBarcodeNo+1;
					$internal_barcode_add = sprintf("%07d", $internal_barcode_new);									
					$internal_barcode_name = 'APS PI '.$internal_barcode_add;
					$bean->internal_barcode = $internal_barcode_name;
			}
		}
	}
}
?>