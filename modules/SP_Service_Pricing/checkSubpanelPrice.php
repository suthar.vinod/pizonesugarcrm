<?php

//AHTSHAM-TL - For Generate the System level Number
//Code for an auto-incrementing the �Name� field in the "Error" module?
//Naming convention will be �E18-xxxx�. This module is on the live site already.
//Since 2019 prefix has changed to "O" and in future from the year of 2020 it will "C"

class checkSubpanelPrice {
	function checkPrice1($bean, $event, $arguments) {
		global $db;
		if($bean->format=="Combination"){

			$spBbean = BeanFactory::retrieveBean("SP_Service_Pricing", $bean->id);
			$spBbean->save();
		}

	}   
	
	
	function checkPriceBeforeSave($bean, $event, $arguments) {
		global $db;
		if($bean->format=="Combination")
		{
			$queryAllPrice = "SELECT SUM(SPC.price) AS SPPrice FROM sp_service_pricing_sp_service_pricing_1_c AS SPJOIN
			LEFT JOIN sp_service_pricing AS SPC ON SPJOIN.sp_service_pricing_sp_service_pricing_1sp_service_pricing_idb = SPC.id
			WHERE SPJOIN.sp_service_pricing_sp_service_pricing_1sp_service_pricing_ida='".$bean->id."' AND SPC.format!='combination' AND SPC.deleted=0 AND SPJOIN.deleted=0";
			$resultAllPrice = $db->query($queryAllPrice);
			 
			if($resultAllPrice->num_rows>0){
				while($rowAllPrice = $db->fetchByAssoc($resultAllPrice)) {
					$allPrice = $rowAllPrice['SPPrice']; 
				} 
				$bean->price = $allPrice;
			} 
		} 
	} 


	function checkPriceAfterSave($bean, $event, $arguments) {
		global $db;
		if($bean->format!="Combination"){

			$queryAllPrice = "SELECT SPC.id AS SPID,SPC.format FROM sp_service_pricing AS SPC 
				LEFT JOIN sp_service_pricing_sp_service_pricing_1_c AS SPJOIN ON SPJOIN.sp_service_pricing_sp_service_pricing_1sp_service_pricing_ida = SPC.id
				WHERE SPJOIN.sp_service_pricing_sp_service_pricing_1sp_service_pricing_idb='".$bean->id."' AND SPC.deleted=0 AND SPJOIN.deleted=0";
			$resultAllPrice = $db->query($queryAllPrice);
			if($resultAllPrice->num_rows>0){
				while($rowAllPrice = $db->fetchByAssoc($resultAllPrice)) {
					$SPID = $rowAllPrice['SPID']; 
					$SPFormat = $rowAllPrice['format']; 
					//Retrieve bean
					$spBbean = BeanFactory::retrieveBean("SP_Service_Pricing", $SPID);
					$spBbean->save();

				} 				 
			} 
		}	
	} 
}
