<?php
// WARNING: The contents of this file are auto-generated.


// created: 2017-06-07 19:03:44
$viewdefs['A1A_Critical_Phase_Inspectio']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CPI_FINDINGS_TITLE',
  'context' => 
  array (
    'link' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  ),
);

// created: 2017-09-13 15:14:51
$viewdefs['A1A_Critical_Phase_Inspectio']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CALLS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'a1a_critical_phase_inspectio_activities_1_calls',
  ),
);

// created: 2017-09-13 15:16:58
$viewdefs['A1A_Critical_Phase_Inspectio']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_EMAILS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'a1a_critical_phase_inspectio_activities_1_emails',
  ),
);

// created: 2017-09-13 15:15:31
$viewdefs['A1A_Critical_Phase_Inspectio']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_MEETINGS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'a1a_critical_phase_inspectio_activities_1_meetings',
  ),
);

// created: 2017-09-13 15:16:04
$viewdefs['A1A_Critical_Phase_Inspectio']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_NOTES_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'a1a_critical_phase_inspectio_activities_1_notes',
  ),
);

// created: 2017-09-13 15:16:33
$viewdefs['A1A_Critical_Phase_Inspectio']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TASKS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'a1a_critical_phase_inspectio_activities_1_tasks',
  ),
);

// created: 2019-02-21 13:44:24
$viewdefs['A1A_Critical_Phase_Inspectio']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'context' => 
  array (
    'link' => 'a1a_critical_phase_inspectio_m06_error_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['A1A_Critical_Phase_Inspectio']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'a1a_critical_phase_inspectio_m06_error_1',
  'view' => 'subpanel-for-a1a_critical_phase_inspectio-a1a_critical_phase_inspectio_m06_error_1',
);
