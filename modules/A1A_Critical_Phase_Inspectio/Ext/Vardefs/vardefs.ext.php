<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/m03_work_product_a1a_critical_phase_inspectio_1_A1A_Critical_Phase_Inspectio.php

// created: 2017-06-07 19:01:24
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["m03_work_product_a1a_critical_phase_inspectio_1"] = array (
  'name' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'id_name' => 'm03_work_p33edproduct_ida',
  'link-type' => 'one',
);
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["m03_work_product_a1a_critical_phase_inspectio_1_name"] = array (
  'name' => 'm03_work_product_a1a_critical_phase_inspectio_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p33edproduct_ida',
  'link' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["m03_work_p33edproduct_ida"] = array (
  'name' => 'm03_work_p33edproduct_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE_ID',
  'id_name' => 'm03_work_p33edproduct_ida',
  'link' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_a1a_cpi_findings_1_A1A_Critical_Phase_Inspectio.php

// created: 2017-06-07 19:03:44
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_a1a_cpi_findings_1"] = array (
  'name' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'source' => 'non-db',
  'module' => 'A1A_CPI_Findings',
  'bean_name' => 'A1A_CPI_Findings',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'id_name' => 'a1a_criticbf09spectio_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_inspection_comments_c.php

 // created: 2017-06-08 18:34:10
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_comments_c']['labelValue']='Inspection Comments';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_comments_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_comments_c']['enforced']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_comments_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/nameFieldCustomization.php


$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['required'] = 'false';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['readonly'] = 'true';

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_calls_A1A_Critical_Phase_Inspectio.php

// created: 2017-09-13 15:14:51
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_activities_1_calls"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_calls',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_calls',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_CALLS_FROM_CALLS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_meetings_A1A_Critical_Phase_Inspectio.php

// created: 2017-09-13 15:15:31
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_activities_1_meetings"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_meetings',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_meetings',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_notes_A1A_Critical_Phase_Inspectio.php

// created: 2017-09-13 15:16:04
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_activities_1_notes"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_notes',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_notes',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_NOTES_FROM_NOTES_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_tasks_A1A_Critical_Phase_Inspectio.php

// created: 2017-09-13 15:16:33
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_activities_1_tasks"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_tasks',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_tasks',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_TASKS_FROM_TASKS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_emails_A1A_Critical_Phase_Inspectio.php

// created: 2017-09-13 15:16:58
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_activities_1_emails"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_emails',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_emails',
  'source' => 'non-db',
  'module' => 'Emails',
  'bean_name' => 'Email',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_EMAILS_FROM_EMAILS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/a1a_critical_phase_inspectio_m06_error_1_A1A_Critical_Phase_Inspectio.php

// created: 2019-02-21 13:44:24
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["a1a_critical_phase_inspectio_m06_error_1"] = array (
  'name' => 'a1a_critical_phase_inspectio_m06_error_1',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'id_name' => 'a1a_critic9e89spectio_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_name.php

 // created: 2019-11-25 15:04:35
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['len']='255';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['required']=false;
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['audited']=true;
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['massupdate']=false;
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['unified_search']=false;
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_inspection_comments_area_c.php

 // created: 2019-11-25 15:12:42
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_comments_area_c']['labelValue']='Inspection Comments Area';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_comments_area_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_comments_area_c']['enforced']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_comments_area_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_date_of_inspection.php

 // created: 2019-11-25 15:13:29
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['date_of_inspection']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_inspection_date_c.php

 // created: 2019-11-25 15:13:47
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_date_c']['labelValue']='Inspection Date(s)';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_date_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_date_c']['enforced']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_inspection_scope_c.php

 // created: 2019-11-25 15:15:26
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_scope_c']['labelValue']='Inspection Scope';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_scope_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_scope_c']['enforced']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_scope_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_test_systems_c.php

 // created: 2020-02-19 15:43:40
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['test_systems_c']['labelValue']='Test Systems';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['test_systems_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['test_systems_c']['enforced']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['test_systems_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_phase_of_inspection.php

 // created: 2020-05-20 16:19:41
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['phase_of_inspection']['audited']=true;
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['phase_of_inspection']['default']='';

 
?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_contributing_scientist_c.php

 // created: 2021-11-11 07:40:40
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['contributing_scientist_c']['labelValue']='Contributing Scientist';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['contributing_scientist_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['contributing_scientist_c']['enforced']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['contributing_scientist_c']['dependency']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['contributing_scientist_c']['required_formula']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['contributing_scientist_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/edoc_email_documents_a1a_critical_phase_inspectio_1_A1A_Critical_Phase_Inspectio.php

// created: 2022-02-01 04:24:04
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["edoc_email_documents_a1a_critical_phase_inspectio_1"] = array (
  'name' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'type' => 'link',
  'relationship' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'source' => 'non-db',
  'module' => 'EDoc_Email_Documents',
  'bean_name' => 'EDoc_Email_Documents',
  'side' => 'right',
  'vname' => 'LBL_EDOC_EMAIL_DOCUMENTS_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'id_name' => 'edoc_emailbf38cuments_ida',
  'link-type' => 'one',
);
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["edoc_email_documents_a1a_critical_phase_inspectio_1_name"] = array (
  'name' => 'edoc_email_documents_a1a_critical_phase_inspectio_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EDOC_EMAIL_DOCUMENTS_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'save' => true,
  'id_name' => 'edoc_emailbf38cuments_ida',
  'link' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'table' => 'edoc_email_documents',
  'module' => 'EDoc_Email_Documents',
  'rname' => 'document_name',
);
$dictionary["A1A_Critical_Phase_Inspectio"]["fields"]["edoc_emailbf38cuments_ida"] = array (
  'name' => 'edoc_emailbf38cuments_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_EDOC_EMAIL_DOCUMENTS_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE_ID',
  'id_name' => 'edoc_emailbf38cuments_ida',
  'link' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'table' => 'edoc_email_documents',
  'module' => 'EDoc_Email_Documents',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Vardefs/sugarfield_inspection_results_c.php

 // created: 2022-03-01 07:40:46
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_results_c']['labelValue']='Inspection Results';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_results_c']['dependency']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_results_c']['required_formula']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_results_c']['readonly_formula']='';
$dictionary['A1A_Critical_Phase_Inspectio']['fields']['inspection_results_c']['visibility_grid']='';

 
?>
