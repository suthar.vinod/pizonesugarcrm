<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Layoutdefs/a1a_critical_phase_inspectio_a1a_cpi_findings_1_A1A_Critical_Phase_Inspectio.php

 // created: 2017-06-07 19:03:44
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['a1a_critical_phase_inspectio_a1a_cpi_findings_1'] = array (
  'order' => 100,
  'module' => 'A1A_CPI_Findings',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CPI_FINDINGS_TITLE',
  'get_subpanel_data' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Layoutdefs/a1a_critical_phase_inspectio_activities_1_calls_A1A_Critical_Phase_Inspectio.php

 // created: 2017-09-13 15:14:51
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['activities'] = array (
  'order' => 10,
  'sort_order' => 'desc',
  'sort_by' => 'date_start',
  'title_key' => 'LBL_ACTIVITIES_SUBPANEL_TITLE',
  'type' => 'collection',
  'subpanel_name' => 'activities',
  'module' => 'Activities',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateTaskButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopScheduleMeetingButton',
    ),
    2 => 
    array (
      'widget_class' => 'SubPanelTopScheduleCallButton',
    ),
    3 => 
    array (
      'widget_class' => 'SubPanelTopComposeEmailButton',
    ),
  ),
  'collection_list' => 
  array (
    'meetings' => 
    array (
      'module' => 'Meetings',
      'subpanel_name' => 'ForActivities',
      'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_meetings',
    ),
    'tasks' => 
    array (
      'module' => 'Tasks',
      'subpanel_name' => 'ForActivities',
      'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_tasks',
    ),
    'calls' => 
    array (
      'module' => 'Calls',
      'subpanel_name' => 'ForActivities',
      'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_calls',
    ),
  ),
  'get_subpanel_data' => 'activities',
);
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['history'] = array (
  'order' => 20,
  'sort_order' => 'desc',
  'sort_by' => 'date_modified',
  'title_key' => 'LBL_HISTORY',
  'type' => 'collection',
  'subpanel_name' => 'history',
  'module' => 'History',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateNoteButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopArchiveEmailButton',
    ),
    2 => 
    array (
      'widget_class' => 'SubPanelTopSummaryButton',
    ),
  ),
  'collection_list' => 
  array (
    'meetings' => 
    array (
      'module' => 'Meetings',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_meetings',
    ),
    'tasks' => 
    array (
      'module' => 'Tasks',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_tasks',
    ),
    'calls' => 
    array (
      'module' => 'Calls',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_calls',
    ),
    'notes' => 
    array (
      'module' => 'Notes',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_notes',
    ),
    'emails' => 
    array (
      'module' => 'Emails',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_emails',
    ),
  ),
  'get_subpanel_data' => 'history',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Layoutdefs/a1a_critical_phase_inspectio_m06_error_1_A1A_Critical_Phase_Inspectio.php

 // created: 2019-02-21 13:44:24
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['a1a_critical_phase_inspectio_m06_error_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'a1a_critical_phase_inspectio_m06_error_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/Layoutdefs/_overrideA1A_Critical_Phase_Inspectio_subpanel_a1a_critical_phase_inspectio_m06_error_1.php

//auto-generated file DO NOT EDIT
$layout_defs['A1A_Critical_Phase_Inspectio']['subpanel_setup']['a1a_critical_phase_inspectio_m06_error_1']['override_subpanel_name'] = 'A1A_Critical_Phase_Inspectio_subpanel_a1a_critical_phase_inspectio_m06_error_1';

?>
