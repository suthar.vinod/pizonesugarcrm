<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/WirelessLayoutdefs/a1a_critical_phase_inspectio_a1a_cpi_findings_1_A1A_Critical_Phase_Inspectio.php

 // created: 2017-06-07 19:03:45
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['a1a_critical_phase_inspectio_a1a_cpi_findings_1'] = array (
  'order' => 100,
  'module' => 'A1A_CPI_Findings',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_A1A_CPI_FINDINGS_1_FROM_A1A_CPI_FINDINGS_TITLE',
  'get_subpanel_data' => 'a1a_critical_phase_inspectio_a1a_cpi_findings_1',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/WirelessLayoutdefs/a1a_critical_phase_inspectio_activities_1_calls_A1A_Critical_Phase_Inspectio.php

 // created: 2017-09-13 15:14:51
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['a1a_critical_phase_inspectio_activities_1_calls'] = array (
  'order' => 100,
  'module' => 'Calls',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_CALLS_FROM_CALLS_TITLE',
  'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_calls',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/WirelessLayoutdefs/a1a_critical_phase_inspectio_activities_1_meetings_A1A_Critical_Phase_Inspectio.php

 // created: 2017-09-13 15:15:31
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['a1a_critical_phase_inspectio_activities_1_meetings'] = array (
  'order' => 100,
  'module' => 'Meetings',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE',
  'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_meetings',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/WirelessLayoutdefs/a1a_critical_phase_inspectio_activities_1_notes_A1A_Critical_Phase_Inspectio.php

 // created: 2017-09-13 15:16:04
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['a1a_critical_phase_inspectio_activities_1_notes'] = array (
  'order' => 100,
  'module' => 'Notes',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_NOTES_FROM_NOTES_TITLE',
  'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_notes',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/WirelessLayoutdefs/a1a_critical_phase_inspectio_activities_1_tasks_A1A_Critical_Phase_Inspectio.php

 // created: 2017-09-13 15:16:33
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['a1a_critical_phase_inspectio_activities_1_tasks'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_TASKS_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_tasks',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/WirelessLayoutdefs/a1a_critical_phase_inspectio_activities_1_emails_A1A_Critical_Phase_Inspectio.php

 // created: 2017-09-13 15:16:58
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['a1a_critical_phase_inspectio_activities_1_emails'] = array (
  'order' => 100,
  'module' => 'Emails',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_EMAILS_FROM_EMAILS_TITLE',
  'get_subpanel_data' => 'a1a_critical_phase_inspectio_activities_1_emails',
);

?>
<?php
// Merged from custom/Extension/modules/A1A_Critical_Phase_Inspectio/Ext/WirelessLayoutdefs/a1a_critical_phase_inspectio_m06_error_1_A1A_Critical_Phase_Inspectio.php

 // created: 2019-02-21 13:44:24
$layout_defs["A1A_Critical_Phase_Inspectio"]["subpanel_setup"]['a1a_critical_phase_inspectio_m06_error_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'a1a_critical_phase_inspectio_m06_error_1',
);

?>
