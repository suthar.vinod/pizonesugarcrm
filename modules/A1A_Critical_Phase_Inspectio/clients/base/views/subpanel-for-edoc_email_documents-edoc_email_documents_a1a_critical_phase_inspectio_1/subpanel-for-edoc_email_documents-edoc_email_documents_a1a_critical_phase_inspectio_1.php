<?php
// created: 2022-02-01 04:27:21
$viewdefs['A1A_Critical_Phase_Inspectio']['base']['view']['subpanel-for-edoc_email_documents-edoc_email_documents_a1a_critical_phase_inspectio_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'm03_work_product_a1a_critical_phase_inspectio_1_name',
          'label' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_M03_WORK_PRODUCT_TITLE',
          'enabled' => true,
          'id' => 'M03_WORK_P33EDPRODUCT_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'phase_of_inspection',
          'label' => 'LBL_PHASE_OF_INSPECTION',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'inspection_date_c',
          'label' => 'LBL_INSPECTION_DATE',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'inspection_results_c',
          'label' => 'LBL_INSPECTION_RESULTS',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'test_systems_c',
          'label' => 'LBL_TEST_SYSTEMS',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'contributing_scientist_c',
          'label' => 'LBL_CONTRIBUTING_SCIENTIST',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);