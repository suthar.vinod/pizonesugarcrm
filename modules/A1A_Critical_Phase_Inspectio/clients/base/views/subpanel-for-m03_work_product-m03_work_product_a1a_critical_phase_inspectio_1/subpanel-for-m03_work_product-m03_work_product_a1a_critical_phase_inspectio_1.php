<?php
// created: 2021-11-09 08:42:07
$viewdefs['A1A_Critical_Phase_Inspectio']['base']['view']['subpanel-for-m03_work_product-m03_work_product_a1a_critical_phase_inspectio_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'phase_of_inspection',
          'label' => 'LBL_PHASE_OF_INSPECTION',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'inspection_results_c',
          'label' => 'LBL_INSPECTION_RESULTS',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'assigned_user_id',
          ),
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'inspection_date_c',
          'label' => 'LBL_INSPECTION_DATE',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        6 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);