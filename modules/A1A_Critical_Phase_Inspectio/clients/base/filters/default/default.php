<?php
// created: 2022-03-10 06:05:48
$viewdefs['A1A_Critical_Phase_Inspectio']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'contributing_scientist_c' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_of_inspection' => 
    array (
    ),
    'edoc_email_documents_a1a_critical_phase_inspectio_1_name' => 
    array (
    ),
    'inspection_date_c' => 
    array (
    ),
    'inspection_results_c' => 
    array (
    ),
    'inspection_scope_c' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'name' => 
    array (
    ),
    'phase_of_inspection' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'test_systems_c' => 
    array (
    ),
    'm03_work_product_a1a_critical_phase_inspectio_1_name' => 
    array (
    ),
  ),
);