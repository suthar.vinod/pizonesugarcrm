<?php
require_once 'include/Sugarpdf/sugarpdf/sugarpdf.pdfmanager.php';
require_once 'include/Sugarpdf/sugarpdf/sugarpdf.smarty.php';
//require_once('include/Sugar_Smarty.php');
//ob_start();
class A1A_Critical_Phase_InspectioSugarpdfPdfmanager extends SugarpdfPdfmanager
{
    public function preDisplay()
    {
        global $db, $current_user;
        //$this->ss = new Sugar_Smarty();
		//$GLOBALS['log']->fatal("17may po bean->name ".$this->bean->name);
        parent::preDisplay();
        $this->SetMargins(15, 10, 15);

        $previewMode = false;

        if (!empty($_REQUEST['pdf_preview']) && $_REQUEST['pdf_preview'] == 1) {
            $previewMode = true;
		}	
			
        if ($this->module == 'A1A_Critical_Phase_Inspectio' && $previewMode === false) {
             
            $beanid = $this->bean->id;
			$wp		= $this->bean->m03_work_p33edproduct_ida;
			
			$inspection_count=0;
			
			if($wp!=""){
				 $WPCountQuery	= "SELECT * FROM m03_work_product_a1a_critical_phase_inspectio_1_c where m03_work_p33edproduct_ida='".$wp."' and deleted=0 ORDER BY date_modified";
				$resultWP1		= $db->query($WPCountQuery);
				$inspection_count=0;$countRecord = 0;

				if ($resultWP1->num_rows > 0) {
					while ($rowWP = $db->fetchByAssoc($resultWP1)) {
						$cpiBean = $rowWP['m03_work_p934fspectio_idb'];
						$countRecord++;
						if( $cpiBean== $beanid){
							$inspection_count = $countRecord; 
						}
					}
				}
			}
			
			//M06_error bean
            $this->bean->load_relationship('a1a_critical_phase_inspectio_m06_error_1');
            $CommBeanId		= $this->bean->a1a_critical_phase_inspectio_m06_error_1->get();
			
            $commData	= array();
            $is_comm	= 0;
            foreach ($CommBeanId as $CommId)
            {
				$commBean    = BeanFactory::getBean('M06_Error', $CommId);
                array_push($commData, array(
					'name'				=> $commBean->name,
                    'expected_event_c'	=> $commBean->expected_event_c,
                    'actual_event_c'	=> $commBean->actual_event_c,
                    'recommendation_c'	=> $commBean->recommendation_c                 
                ));
               
            }
			if(!empty($commData))
            {
                $is_comm = 1;
            }
			
			$this->ss->assign('inspection_count', $inspection_count);
            $this->ss->assign('is_comm', $is_comm);
            $this->ss->assign('commData', $commData);
        }  
    }

    /**
     * PDF manager specific Footer function
     */
    public function Footer() {
        $cur_y = $this->GetY();
        $ormargins = $this->getOriginalMargins();
        $this->SetTextColor(0, 0, 0);
        //set style for cell border
        $line_width = 0.85 / $this->getScaleFactor();
        $this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        //print document barcode
        $barcode = $this->getBarcode();
        if (!empty($barcode)) {
            $this->Ln($line_width);
            $barcode_width = round(($this->getPageWidth() - $ormargins['left'] - $ormargins['right'])/3);
            $this->write1DBarcode($barcode, 'C128B', $this->GetX(), $cur_y + $line_width, $barcode_width, (($this->getFooterMargin() / 3) - $line_width), 0.3, '', '');
        }
        if (empty($this->pagegroups)) {
            $pagenumtxt = $this->l['w_page'].' '.$this->getAliasNumPage().' OF '.$this->getAliasNbPages();
            $pagenumtxt = 'PAGE'. $pagenumtxt;
        } else {
            $pagenumtxt = $this->l['w_page'].' '.$this->getPageNumGroupAlias().' OF '.$this->getPageGroupAlias();
            $pagenumtxt = 'PAGE'. $pagenumtxt;
        }
        $this->SetY($cur_y);

        if ($this->getRTL()) {
            $this->SetX($ormargins['right']);
            if ($this->footerText) {
                // footer text and page number
                $this->Cell(0, 0, $this->footerText, 'T', 0, 'R');
                $this->Cell(0, 0, $pagenumtxt, 0, 0, 'L');
            } else {
                // page number only
                $this->Cell(0, 0, $pagenumtxt, 'T', 0, 'L');
            }
        } else {
            $this->SetX($ormargins['left']);
            if ($this->footerText) {
                // footer text and page number
                $this->Cell(0, 0, $this->footerText, 'T', 0, 'L');
                $this->Cell(0, 0, $pagenumtxt, 0, 0, 'R');
            } else {
                // page number only
                $this->Cell(0, 0, $pagenumtxt, 'T', 0, 'R');
            }
        }
    }
}