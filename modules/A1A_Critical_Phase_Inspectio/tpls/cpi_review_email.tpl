<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
</head>
<body>
<table style="width: 600px;" border="1" cellspacing="0" cellpadding="4">
<tbody>
<tr>
	<th bgcolor="#b3d1ff" align="left" width="120pt">Name</th>
	<th bgcolor="#b3d1ff" align="left"  width="160pt">Inspection Results</th>
	<th bgcolor="#b3d1ff" align="left"  width="150pt">APS Work Product ID</th>
	<th bgcolor="#b3d1ff" align="left"  width="120pt">Study Director</th>
</tr>
{foreach from=$docArr key=index2 item=line_item}
 <tr>       
    <td width="120pt"><a href="https://apsdev.sugarondemand.com/#A1A_Critical_Phase_Inspectio/{$line_item.cpiID}">{$line_item.cpiName}</a></td>
	<td width="160pt">{$line_item.cpiResult}</td>
	<td width="150pt"><a href="https://apsdev.sugarondemand.com/#M03_Work_Product/{$line_item.wpID}">{$line_item.wpName}</a></td>
	<td width="120pt"><a href="https://apsdev.sugarondemand.com/#Contacts/{$line_item.sdID}">{$line_item.sdName}</a></td>
</tr>
	{/foreach}
</tbody>
</table>
 
<p><br/><br/><span style="font-size:10pt;font-family:Calibri, sans-serif;">CONFIDENTIALITY NOTICE: This e-mail transmission may contain confidential or legally privileged information that is intended only for the individual or entity named in the e-mail address. Any unauthorized distribution or copying of this transmittal or its attachments, if any, is prohibited. If you have received this e-mail transmission in error, please reply to <a class="email" href="mailto:itsupport@apsemail.com">itsupport@apsemail.com</a>, so that American Preclinical Services can arrange for proper delivery, then please delete the message from your inbox. Thank you.</span></p>
</body>
</html>