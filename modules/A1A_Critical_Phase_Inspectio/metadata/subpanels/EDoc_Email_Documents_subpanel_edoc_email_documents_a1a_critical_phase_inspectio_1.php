<?php
// created: 2022-02-01 04:27:19
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'm03_work_product_a1a_critical_phase_inspectio_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_M03_WORK_PRODUCT_TITLE',
    'id' => 'M03_WORK_P33EDPRODUCT_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M03_Work_Product',
    'target_record_key' => 'm03_work_p33edproduct_ida',
  ),
  'phase_of_inspection' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_PHASE_OF_INSPECTION',
    'width' => 10,
  ),
  'inspection_date_c' => 
  array (
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_INSPECTION_DATE',
    'width' => 10,
    'default' => true,
  ),
  'inspection_results_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_INSPECTION_RESULTS',
    'width' => 10,
  ),
  'test_systems_c' => 
  array (
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_TEST_SYSTEMS',
    'width' => 10,
    'default' => true,
  ),
  'contributing_scientist_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_CONTRIBUTING_SCIENTIST',
    'width' => 10,
    'default' => true,
  ),
);