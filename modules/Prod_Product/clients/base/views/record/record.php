<?php
$module_name = 'Prod_Product';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'Prod_Product',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'product_name',
                'label' => 'LBL_PRODUCT_NAME',
              ),
              1 => 
              array (
                'name' => 'manufacturer',
                'label' => 'LBL_MANUFACTURER',
              ),
              2 => 
              array (
                'name' => 'product_id_2',
                'label' => 'LBL_PRODUCT_ID_2',
              ),
              3 => 
              array (
                'name' => 'status_c',
                'label' => 'LBL_STATUS',
              ),
              4 => 
              array (
                'name' => 'external_barcode',
                'label' => 'LBL_EXTERNAL_BARCODE',
              ),
              5 => 
              array (
                'name' => 'vendor',
                'studio' => 'visible',
                'label' => 'LBL_VENDOR',
              ),
              6 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
              ),
              7 => 
              array (
                'name' => 'subtype',
                'label' => 'LBL_SUBTYPE',
              ),
              8 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              9 => 
              array (
                'name' => 'concentration',
                'label' => 'LBL_CONCENTRATION',
              ),
              10 => 
              array (
                'name' => 'concentration_unit',
                'studio' => 'visible',
                'label' => 'LBL_CONCENTRATION_UNIT',
              ),
              11 => 
              array (
                'name' => 'purchase_unit',
                'label' => 'LBL_PURCHASE_UNIT',
              ),
              12 => 
              array (
                'name' => 'cost_per_unit_2',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_COST_PER_UNIT_2',
              ),
              13 => 
              array (
                'name' => 'unit_quantity',
                'label' => 'LBL_UNIT_QUANTITY',
              ),
              14 => 
              array (
                'name' => 'cost_per_each',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_COST_PER_EACH',
              ),
              15 => 
              array (
                'name' => 'last_price_update_c',
                'label' => 'LBL_LAST_PRICE_UPDATE',
              ),
              16 => 
              array (
                'name' => 'cost_per_each_2',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_COST_PER_EACH_2',
              ),
              17 => 
              array (
                'name' => 'quality_requirement',
                'label' => 'LBL_QUALITY_REQUIREMENT',
              ),
              18 => 
              array (
                'name' => 'notes_multiselect_c',
                'label' => 'LBL_NOTES_MULTISELECT',
              ),
              19 => 
              array (
                'name' => 'allowed_storage',
                'label' => 'LBL_ALLOWED_STORAGE',
              ),
              20 => 
              array (
                'name' => 'department',
                'label' => 'LBL_DEPARTMENT',
              ),
              21 => 
              array (
                'name' => 'stock',
                'label' => 'LBL_STOCK',
              ),
              22 => 
              array (
                'name' => 'stock_quantity',
                'label' => 'LBL_STOCK_QUANTITY',
              ),
              23 => 
              array (
                'name' => 'mark_up_c',
                'label' => 'LBL_MARK_UP',
              ),
              24 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'client_cost_c',
                'label' => 'LBL_CLIENT_COST',
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
