({
    extendsFrom: 'RecordView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.onload_function, this);
        //this.model.on("change:purchase_unit", this.function_purchase_unit, this);
        this.model.once("sync",
            function () {
                this.model.on(
                    "change:purchase_unit",
                    this.function_purchase_unit,
                    this
                );
            },
            this
        );
    },
    onload_function: function () {
        $('span[data-name="name"]').css('pointer-events', 'none');

        var purchase_unit = this.model.get('purchase_unit');

        if (purchase_unit == 'Each') {
            $('div[data-name="unit_quantity"]').css('pointer-events', 'none');
        } else {
            $('div[data-name="unit_quantity"]').css('pointer-events', 'unset');
        }
    },
    function_purchase_unit: function () {
        var purchase_unit = this.model.get('purchase_unit');

        if (purchase_unit == 'Case' || purchase_unit == 'Box' || purchase_unit == 'Pallet' || purchase_unit == 'Roll' || purchase_unit == 'Sleeve' || purchase_unit == 'Bottle') {
            $('input[name="unit_quantity"]').prop('readonly', false);
            $('div[data-name="unit_quantity"]').css('pointer-events', 'unset');
        } else if (purchase_unit == 'Each') {
            this.model.set("unit_quantity", '1');
            $('input[name="unit_quantity"]').prop('readonly', true);
            $('div[data-name="unit_quantity"]').css('pointer-events', 'none');
        }else {
            $('input[name="unit_quantity"]').prop('readonly', false);
            $('div[data-name="unit_quantity"]').css('pointer-events', 'unset');
        }
    },
})


