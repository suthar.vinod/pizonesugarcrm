({
    extendsFrom: 'CreateView',
    initialize: function (options) {

        this._super('initialize', [options]);
        this.model.on("change:purchase_unit", this.function_purchase_unit, this);
    },
    function_purchase_unit: function () {
        var purchase_unit = this.model.get('purchase_unit');

        if (purchase_unit == 'Case' || purchase_unit == 'Box' || purchase_unit == 'Pallet' || purchase_unit == 'Roll' || purchase_unit == 'Sleeve'  || purchase_unit == 'Bottle') {
            this.model.set("unit_quantity", null);
            $('input[name="unit_quantity"]').prop('readonly', false);
        } else if (purchase_unit == 'Each') {
            this.model.set("unit_quantity", '1');
            $('input[name="unit_quantity"]').prop('readonly', true);
        } else {
            $('input[name="unit_quantity"]').prop('readonly', false);
        }
    },
})


 