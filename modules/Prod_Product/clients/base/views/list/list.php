<?php
$module_name = 'Prod_Product';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'vendor',
                'label' => 'LBL_VENDOR',
                'enabled' => true,
                'id' => 'ACCOUNT_ID1_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'product_id_2',
                'label' => 'LBL_PRODUCT_ID_2',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'manufacturer',
                'label' => 'LBL_MANUFACTURER',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'concentration',
                'label' => 'LBL_CONCENTRATION',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'concentration_unit',
                'label' => 'LBL_CONCENTRATION_UNIT',
                'enabled' => true,
                'id' => 'U_UNITS_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'department',
                'label' => 'LBL_DEPARTMENT',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'status_c',
                'label' => 'LBL_STATUS',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'quality_requirement',
                'label' => 'LBL_QUALITY_REQUIREMENT',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'allowed_storage',
                'label' => 'LBL_ALLOWED_STORAGE',
                'enabled' => true,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
