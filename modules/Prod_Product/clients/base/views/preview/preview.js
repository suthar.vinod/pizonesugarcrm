({
    extendsFrom: 'PreviewView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.on('render', _.bind(this.onload_volume, this));
    },
    
    onload_volume: function () {
        $('.preview-edit-btn').hide();
    },
})


