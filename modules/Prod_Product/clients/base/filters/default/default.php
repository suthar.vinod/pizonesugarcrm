<?php
// created: 2020-08-14 04:26:58
$viewdefs['Prod_Product']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'unit_quantity' => 
    array (
    ),
    'notes_multiselect_c' => 
    array (
    ),
    'status_c' => 
    array (
    ),
    'last_price_update_c' => 
    array (
    ),
    'stock' => 
    array (
    ),
    'allowed_storage' => 
    array (
    ),
    'purchase_unit' => 
    array (
    ),
    'concentration_unit' => 
    array (
    ),
    'concentration' => 
    array (
    ),
    'manufacturer' => 
    array (
    ),
    'quality_requirement' => 
    array (
    ),
    'subtype' => 
    array (
    ),
    'type_2' => 
    array (
    ),
    'cost_per_each_2' => 
    array (
    ),
    'cost_per_each' => 
    array (
    ),
    'cost_per_unit_2' => 
    array (
    ),
    'department' => 
    array (
    ),
    'product_name' => 
    array (
    ),
    'external_barcode' => 
    array (
    ),
    'vendor' => 
    array (
    ),
    'product_id_2' => 
    array (
    ),
    'stock_quantity' => 
    array (
    ),
  ),
);