<?php
$popupMeta = array (
    'moduleMain' => 'Prod_Product',
    'varName' => 'Prod_Product',
    'orderBy' => 'prod_product.name',
    'whereClauses' => array (
  'name' => 'prod_product.name',
),
    'searchInputs' => array (
  0 => 'prod_product_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'PRODUCT_ID_2' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_PRODUCT_ID_2',
    'width' => 10,
  ),
  'TYPE_2' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_TYPE_2',
    'width' => 10,
  ),
  'SUBTYPE' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SUBTYPE',
    'width' => 10,
  ),
),
);
