<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/sugarfield_last_price_update_c.php

 // created: 2020-08-20 08:04:31
$dictionary['Prod_Product']['fields']['last_price_update_c']['labelValue']='Last Price Update';
$dictionary['Prod_Product']['fields']['last_price_update_c']['enforced']='';
$dictionary['Prod_Product']['fields']['last_price_update_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/sugarfield_status_c.php

 // created: 2020-08-20 08:07:41
$dictionary['Prod_Product']['fields']['status_c']['labelValue']='Status';
$dictionary['Prod_Product']['fields']['status_c']['dependency']='';
$dictionary['Prod_Product']['fields']['status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/sugarfield_notes_multiselect_c.php

 // created: 2020-08-20 08:11:51
$dictionary['Prod_Product']['fields']['notes_multiselect_c']['labelValue']='Notes Multiselect';
$dictionary['Prod_Product']['fields']['notes_multiselect_c']['dependency']='';
$dictionary['Prod_Product']['fields']['notes_multiselect_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/sugarfield_client_cost_c.php

 // created: 2020-09-03 19:16:01
$dictionary['Prod_Product']['fields']['client_cost_c']['duplicate_merge_dom_value']=0;
$dictionary['Prod_Product']['fields']['client_cost_c']['labelValue']='Client Cost';
$dictionary['Prod_Product']['fields']['client_cost_c']['calculated']='1';
$dictionary['Prod_Product']['fields']['client_cost_c']['formula']='ifElse(not(equal($cost_per_each_2,"")),add($cost_per_each_2,multiply($cost_per_each_2,divide($mark_up_c,100))),add($cost_per_unit_2,multiply($cost_per_unit_2,divide($mark_up_c,100))))';
$dictionary['Prod_Product']['fields']['client_cost_c']['enforced']='1';
$dictionary['Prod_Product']['fields']['client_cost_c']['dependency']='';
$dictionary['Prod_Product']['fields']['client_cost_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/sugarfield_allowed_storage.php

 // created: 2020-09-08 09:42:24
$dictionary['Prod_Product']['fields']['allowed_storage']['hidemassupdate']=false;
$dictionary['Prod_Product']['fields']['allowed_storage']['options']='product_allowed_storage_list';

 
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/sugarfield_unit_quantity.php

 // created: 2020-09-08 11:33:20
$dictionary['Prod_Product']['fields']['unit_quantity']['hidemassupdate']=false;
$dictionary['Prod_Product']['fields']['unit_quantity']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/sugarfield_name.php

 // created: 2020-09-08 12:01:42
$dictionary['Prod_Product']['fields']['name']['unified_search']=false;
$dictionary['Prod_Product']['fields']['name']['calculated']=false;
$dictionary['Prod_Product']['fields']['name']['readonly']=true;
$dictionary['Prod_Product']['fields']['name']['hidemassupdate']=false;
$dictionary['Prod_Product']['fields']['name']['importable']='true';
$dictionary['Prod_Product']['fields']['name']['formula']='';
$dictionary['Prod_Product']['fields']['name']['enforced']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/prod_product_prodo_product_document_1_Prod_Product.php

// created: 2020-09-10 08:30:09
$dictionary["Prod_Product"]["fields"]["prod_product_prodo_product_document_1"] = array (
  'name' => 'prod_product_prodo_product_document_1',
  'type' => 'link',
  'relationship' => 'prod_product_prodo_product_document_1',
  'source' => 'non-db',
  'module' => 'ProDo_Product_Document',
  'bean_name' => 'ProDo_Product_Document',
  'vname' => 'LBL_PROD_PRODUCT_PRODO_PRODUCT_DOCUMENT_1_FROM_PROD_PRODUCT_TITLE',
  'id_name' => 'prod_product_prodo_product_document_1prod_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/sugarfield_cost_per_each.php

 // created: 2021-09-04 04:30:27
$dictionary['Prod_Product']['fields']['cost_per_each']['required']=true;
$dictionary['Prod_Product']['fields']['cost_per_each']['name']='cost_per_each';
$dictionary['Prod_Product']['fields']['cost_per_each']['vname']='LBL_COST_PER_EACH';
$dictionary['Prod_Product']['fields']['cost_per_each']['type']='currency';
$dictionary['Prod_Product']['fields']['cost_per_each']['massupdate']=false;
$dictionary['Prod_Product']['fields']['cost_per_each']['no_default']=false;
$dictionary['Prod_Product']['fields']['cost_per_each']['comments']='';
$dictionary['Prod_Product']['fields']['cost_per_each']['help']='';
$dictionary['Prod_Product']['fields']['cost_per_each']['importable']='false';
$dictionary['Prod_Product']['fields']['cost_per_each']['duplicate_merge']='disabled';
$dictionary['Prod_Product']['fields']['cost_per_each']['duplicate_merge_dom_value']=0;
$dictionary['Prod_Product']['fields']['cost_per_each']['audited']=true;
$dictionary['Prod_Product']['fields']['cost_per_each']['reportable']=true;
$dictionary['Prod_Product']['fields']['cost_per_each']['unified_search']=false;
$dictionary['Prod_Product']['fields']['cost_per_each']['merge_filter']='disabled';
$dictionary['Prod_Product']['fields']['cost_per_each']['pii']=false;
$dictionary['Prod_Product']['fields']['cost_per_each']['calculated']='1';
$dictionary['Prod_Product']['fields']['cost_per_each']['formula']='divide($cost_per_unit_2,$unit_quantity)';
$dictionary['Prod_Product']['fields']['cost_per_each']['enforced']=true;
$dictionary['Prod_Product']['fields']['cost_per_each']['len']=26;
$dictionary['Prod_Product']['fields']['cost_per_each']['size']='20';
$dictionary['Prod_Product']['fields']['cost_per_each']['enable_range_search']=false;
$dictionary['Prod_Product']['fields']['cost_per_each']['precision']=6;
$dictionary['Prod_Product']['fields']['cost_per_each']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Prod_Product']['fields']['cost_per_each']['dependency']='isInList($purchase_unit,createList("Box","Case","Sleeve","Pallet","Roll","Bottle"))';
$dictionary['Prod_Product']['fields']['cost_per_each']['hidemassupdate']=false;
$dictionary['Prod_Product']['fields']['cost_per_each']['convertToBase']=true;
$dictionary['Prod_Product']['fields']['cost_per_each']['showTransactionalAmount']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/sugarfield_cost_per_unit_2.php

 // created: 2021-09-04 04:30:27
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['required']=true;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['name']='cost_per_unit_2';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['vname']='LBL_COST_PER_UNIT_2';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['type']='currency';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['massupdate']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['no_default']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['comments']='';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['help']='';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['importable']='true';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['duplicate_merge']='enabled';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['duplicate_merge_dom_value']='1';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['audited']=true;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['reportable']=true;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['unified_search']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['merge_filter']='disabled';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['pii']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['default']='';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['calculated']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['len']=26;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['size']='20';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['enable_range_search']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['precision']=6;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['dependency']='isInList($purchase_unit,createList("Box","Case","Sleeve","Pallet","Roll","Bottle"))';
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['hidemassupdate']=false;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['convertToBase']=true;
$dictionary['Prod_Product']['fields']['cost_per_unit_2']['showTransactionalAmount']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/sugarfield_cost_per_each_2.php

 // created: 2021-09-04 04:30:27
$dictionary['Prod_Product']['fields']['cost_per_each_2']['required']=true;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['name']='cost_per_each_2';
$dictionary['Prod_Product']['fields']['cost_per_each_2']['vname']='LBL_COST_PER_EACH_2';
$dictionary['Prod_Product']['fields']['cost_per_each_2']['type']='currency';
$dictionary['Prod_Product']['fields']['cost_per_each_2']['massupdate']=false;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['no_default']=false;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['comments']='';
$dictionary['Prod_Product']['fields']['cost_per_each_2']['help']='';
$dictionary['Prod_Product']['fields']['cost_per_each_2']['importable']='true';
$dictionary['Prod_Product']['fields']['cost_per_each_2']['duplicate_merge']='enabled';
$dictionary['Prod_Product']['fields']['cost_per_each_2']['duplicate_merge_dom_value']='1';
$dictionary['Prod_Product']['fields']['cost_per_each_2']['audited']=true;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['reportable']=true;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['unified_search']=false;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['merge_filter']='disabled';
$dictionary['Prod_Product']['fields']['cost_per_each_2']['pii']=false;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['default']='';
$dictionary['Prod_Product']['fields']['cost_per_each_2']['calculated']=false;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['dependency']='equal($purchase_unit,"Each")';
$dictionary['Prod_Product']['fields']['cost_per_each_2']['len']=26;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['size']='20';
$dictionary['Prod_Product']['fields']['cost_per_each_2']['enable_range_search']=false;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['precision']=6;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['Prod_Product']['fields']['cost_per_each_2']['hidemassupdate']=false;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['convertToBase']=true;
$dictionary['Prod_Product']['fields']['cost_per_each_2']['showTransactionalAmount']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/prod_product_ori_order_request_item_1_Prod_Product.php

// created: 2021-10-19 09:41:58
$dictionary["Prod_Product"]["fields"]["prod_product_ori_order_request_item_1"] = array (
  'name' => 'prod_product_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'prod_product_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'vname' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_PROD_PRODUCT_TITLE',
  'id_name' => 'prod_product_ori_order_request_item_1prod_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/prod_product_poi_purchase_order_item_1_Prod_Product.php

// created: 2021-10-19 11:02:37
$dictionary["Prod_Product"]["fields"]["prod_product_poi_purchase_order_item_1"] = array (
  'name' => 'prod_product_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'prod_product_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'POI_Purchase_Order_Item',
  'bean_name' => 'POI_Purchase_Order_Item',
  'vname' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_PROD_PRODUCT_TITLE',
  'id_name' => 'prod_product_poi_purchase_order_item_1prod_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/sugarfield_mark_up_c.php

 // created: 2021-12-14 08:54:33
$dictionary['Prod_Product']['fields']['mark_up_c']['labelValue']='Mark-Up (%)';
$dictionary['Prod_Product']['fields']['mark_up_c']['enforced']='';
$dictionary['Prod_Product']['fields']['mark_up_c']['dependency']='';
$dictionary['Prod_Product']['fields']['mark_up_c']['required_formula']='';
$dictionary['Prod_Product']['fields']['mark_up_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Vardefs/sugarfield_subtype.php

 // created: 2022-03-23 12:31:20
$dictionary['Prod_Product']['fields']['subtype']['hidemassupdate']=false;
$dictionary['Prod_Product']['fields']['subtype']['visibility_grid']=array (
  'trigger' => 'type_2',
  'values' => 
  array (
    'Balloon Catheter' => 
    array (
      0 => '',
      1 => 'Angioplasty Balloon',
      2 => 'Cutting Balloon',
      3 => 'Drug Coated Balloon',
      4 => 'Other',
      5 => 'OTW over the wire',
      6 => 'POBA Balloon',
      7 => 'PTA Balloon',
    ),
    'Cannula' => 
    array (
      0 => '',
      1 => 'Aortic Root Cannula',
      2 => 'Arterial Cannula',
      3 => 'Other',
      4 => 'Pediatric Cannula',
      5 => 'Venous Cannula',
      6 => 'Vessel Cannula',
    ),
    'Catheter' => 
    array (
      0 => '',
      1 => 'Access Catheter',
      2 => 'Butterfly Catheter',
      3 => 'Central Venous Catheter',
      4 => 'Diagnostic Catheter',
      5 => 'EP Catheter',
      6 => 'Extension Catheter',
      7 => 'Foley Catheter',
      8 => 'Guide Catheter',
      9 => 'Imaging Catheter',
      10 => 'IV Catheter',
      11 => 'Mapping Catheter',
      12 => 'Micro Catheter',
      13 => 'Mila Catheter',
      14 => 'Other',
      15 => 'Pressure Catheter',
      16 => 'Support Catheter',
      17 => 'Transseptal Catheter',
    ),
    'Chemical' => 
    array (
      0 => '',
      1 => 'Analytes',
      2 => 'Control Matrix',
      3 => 'Corrosive Solution',
      4 => 'Flammable',
      5 => 'Flammable Solution',
      6 => 'Non Flammable',
      7 => 'Solvents Aqueous',
      8 => 'Solvents Organic',
      9 => 'Toxic Solution',
    ),
    'Cleaning Agent' => 
    array (
    ),
    'Contrast' => 
    array (
      0 => '',
      1 => 'Barium Contrast',
      2 => 'Iodinated Contrast',
      3 => 'Other',
    ),
    'Drug' => 
    array (
      0 => '',
      1 => 'Inhalant',
      2 => 'Injectable',
      3 => 'Intranasal',
      4 => 'IV Solution',
      5 => 'Oral',
      6 => 'Other',
      7 => 'Topical',
    ),
    'Graft' => 
    array (
      0 => '',
      1 => 'Coated',
      2 => 'ePTFE',
      3 => 'Knitted',
      4 => 'Other',
      5 => 'Patches',
      6 => 'Woven',
    ),
    'Inflation Device' => 
    array (
      0 => '',
      1 => 'High Pressure',
      2 => 'Other',
    ),
    'Reagent' => 
    array (
      0 => '',
      1 => 'Flammable',
      2 => 'Non Flammable',
    ),
    'Sheath' => 
    array (
      0 => '',
      1 => 'Introducer Sheath',
      2 => 'Other',
      3 => 'Peel Away',
      4 => 'Steerable Sheath',
    ),
    'Stent' => 
    array (
      0 => '',
      1 => 'Bare Metal Stent',
      2 => 'Drug Coated Stent',
      3 => 'Other',
    ),
    'Suture' => 
    array (
      0 => '',
      1 => 'Ethibond',
      2 => 'Ethilon',
      3 => 'MonoAdjustable Loop',
      4 => 'Monocryl',
      5 => 'Monoderm',
      6 => 'Other',
      7 => 'PDO',
      8 => 'PDS',
      9 => 'Pledget',
      10 => 'Prolene',
      11 => 'Silk',
      12 => 'Ti Cron',
      13 => 'Vicryl',
    ),
    'Wire' => 
    array (
      0 => '',
      1 => 'Exchange Wire',
      2 => 'Guide Wire',
      3 => 'Marking Wire',
      4 => 'Other',
      5 => 'Support Wire',
    ),
    '' => 
    array (
    ),
    'Equipment' => 
    array (
      0 => '',
      1 => 'GCMS Columns',
      2 => 'GCMS Parts',
      3 => 'ICPMS Parts',
      4 => 'LCMS Columns',
      5 => 'LCMS Parts',
      6 => 'Machine',
      7 => 'Other',
      8 => 'Replacement Part',
      9 => 'Software',
    ),
    'Solution' => 
    array (
    ),
    'Endotracheal TubeSupplies' => 
    array (
      0 => '',
      1 => 'Adaptor',
      2 => 'Breathing circuit',
      3 => 'Brush',
      4 => 'ET Tube',
      5 => 'Other',
      6 => 'Stylet',
    ),
    'Other' => 
    array (
      0 => '',
      1 => 'Contrast Spike',
      2 => 'Dressing',
      3 => 'Essentials Kit',
      4 => 'Invasive Blood Pressure Supplies',
      5 => 'Other',
      6 => 'Perc Needle',
      7 => 'Probe Cover',
      8 => 'Stopcock',
      9 => 'Torque Device',
      10 => 'Tuohy',
    ),
    'Tubing' => 
    array (
      0 => '',
      1 => 'Adaptor',
      2 => 'Connector',
      3 => 'Extension Tubing',
      4 => 'High Pressure Tubing',
      5 => 'Other',
      6 => 'Reducer',
      7 => 'WYE',
    ),
    'Autoclave supplies' => 
    array (
    ),
    'Bandaging' => 
    array (
      0 => '',
      1 => 'Dressing',
      2 => 'Sub layer wrap',
      3 => 'Other',
      4 => 'Tape',
      5 => 'Top layer wrap',
    ),
    'Blood draw supply' => 
    array (
      0 => '',
      1 => 'Cryovial',
      2 => 'Extension line',
      3 => 'Injection port',
      4 => 'Needle',
      5 => 'Needless port',
      6 => 'Other',
      7 => 'Syringe',
      8 => 'Vacutainer',
    ),
    'Cleaning Supplies' => 
    array (
      0 => '',
      1 => 'Bottle',
      2 => 'Brush',
      3 => 'Floor scrubber supplies',
      4 => 'Other',
      5 => 'SpongePad',
      6 => 'Trash bag',
    ),
    'Enrichment Toy' => 
    array (
    ),
    'ETO supplies' => 
    array (
    ),
    'Feeding supplies' => 
    array (
      0 => '',
      1 => 'Enrichment treats',
      2 => 'Feeding Tube',
      3 => 'Primary diet',
      4 => 'Other',
      5 => 'Supplement diet',
    ),
    'Office Supply' => 
    array (
      0 => '',
      1 => 'Binders and supplies',
      2 => 'Label',
      3 => 'Laminating Sheets',
      4 => 'Other',
      5 => 'Paper',
    ),
    'Shipping supplies' => 
    array (
      0 => '',
      1 => 'CD case',
      2 => 'Container',
      3 => 'Cold Pack',
      4 => 'Corrugated box',
      5 => 'Dry Ice',
      6 => 'Indicator label',
      7 => 'Insulated shipper',
      8 => 'Other',
      9 => 'Pallet',
      10 => 'Padding',
      11 => 'Tape',
      12 => 'Wrap',
    ),
    'Surgical Instrument' => 
    array (
    ),
    'Surgical site prep' => 
    array (
      0 => '',
      1 => 'Incision site cover',
      2 => 'Other',
      3 => 'Surgical scrub',
    ),
    'Bypass supply' => 
    array (
      0 => '',
      1 => 'Hemoconcentrator',
      2 => 'Other',
      3 => 'Oxygenator',
      4 => 'Pump pack',
      5 => 'Reservoir',
      6 => 'Tubing',
    ),
    'PPE supply' => 
    array (
      0 => '',
      1 => 'Boots',
      2 => 'Bouffanthair net',
      3 => 'Coveralloverall',
      4 => 'Ear protection',
      5 => 'Face shield',
      6 => 'Gloves non sterile',
      7 => 'Gloves sterile',
      8 => 'Mask',
      9 => 'Other',
      10 => 'Safety glasses',
      11 => 'Shoe covers',
      12 => 'Surgical gown non sterile',
      13 => 'Surgical gown sterile',
    ),
    'Control Matrix' => 
    array (
      0 => '',
      1 => 'Fluid',
      2 => 'Other',
      3 => 'Tissue',
    ),
    'Interventional Supplies' => 
    array (
      0 => 'Other',
    ),
    'Surgical Supplies' => 
    array (
      0 => '',
      1 => 'Aortic Punch',
      2 => 'Biopsy Punch',
      3 => 'Blade',
      4 => 'Cautery Supply',
      5 => 'CT supply',
      6 => 'Drainage supply',
      7 => 'Drape',
      8 => 'ECG supply',
      9 => 'Gauze',
      10 => 'Hand Scrub',
      11 => 'Other',
      12 => 'Patient Drape',
      13 => 'SharpsBiohazard',
      14 => 'Sponge',
      15 => 'Table drape',
    ),
    'Glassware' => 
    array (
      0 => '',
      1 => 'Beaker',
      2 => 'Beakers',
      3 => 'Bottle',
      4 => 'Consumable',
      5 => 'Flask',
      6 => 'Grade A Glassware',
      7 => 'Other',
    ),
    'Plastic Ware' => 
    array (
      0 => '',
      1 => 'Bottle',
      2 => 'Beaker',
      3 => 'Consumable',
      4 => 'Flask',
      5 => 'Other',
      6 => 'Pipette Tips',
      7 => 'Tubes',
      8 => 'Vial Closures',
    ),
    'Lab Supplies' => 
    array (
      0 => '',
      1 => 'Misc',
      2 => 'Other',
      3 => 'Storage container',
      4 => 'Storage Racks',
    ),
  ),
);
$dictionary['Prod_Product']['fields']['subtype']['default']='';

 
?>
