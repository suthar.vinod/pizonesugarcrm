<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Dependencies/name_value_dep.php

$dependencies['Prod_Product']['name_value_dep'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array(
        'product_name',
        'manufacturer',
        'vendor',
    ),
    'onload' => false,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'name',
                'value' => 'concat(
                    $product_name,
                    ifElse(equal($manufacturer,"Yes"), 1, ifElse(not(greaterThan(strlen($manufacturer),0)), " ", ", ")),
                    $manufacturer,					
                    ifElse(equal($vendor,"Yes"), 1, ifElse(not(greaterThan(strlen($vendor),0)), " ", ", ")),
                    $vendor,
                )',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'disabled',
            'params' => array(
                'target' => 'name',
                'value' => 'true',
            ),
        ),
    ),
);

?>
