<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-10-19 09:41:58
$viewdefs['Prod_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'prod_product_ori_order_request_item_1',
  ),
);

// created: 2021-10-19 11:02:37
$viewdefs['Prod_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'prod_product_poi_purchase_order_item_1',
  ),
);

// created: 2020-09-10 08:30:08
$viewdefs['Prod_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_PROD_PRODUCT_PRODO_PRODUCT_DOCUMENT_1_FROM_PRODO_PRODUCT_DOCUMENT_TITLE',
  'context' => 
  array (
    'link' => 'prod_product_prodo_product_document_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['Prod_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'prod_product_ori_order_request_item_1',
  'view' => 'subpanel-for-prod_product-prod_product_ori_order_request_item_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Prod_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'prod_product_poi_purchase_order_item_1',
  'view' => 'subpanel-for-prod_product-prod_product_poi_purchase_order_item_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Prod_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'prod_product_prodo_product_document_1',
  'view' => 'subpanel-for-prod_product-prod_product_prodo_product_document_1',
);
