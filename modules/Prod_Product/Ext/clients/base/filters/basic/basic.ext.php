<?php
// WARNING: The contents of this file are auto-generated.


$viewdefs['Prod_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterProd_ProductNSBTemplate',
    'name' => 'Filter Product',
    'filter_definition' => array(
        array(
            'department' => array(
                '$contains' => '',
            ),
        ),
		array(
            'type_2' => array(
                '$in' => '',
            ),
        ),
		array(
            'status_c' => array(
                '$not_in' => '',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);



$viewdefs['Prod_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterProd_ProductSBTemplate',
    'name' => 'Filter Product',
    'filter_definition' => array(
        array(
            'department' => array(
                '$contains' => '',
            ),
        ),
		array(
            'type_2' => array(
                '$in' => '',
            ),
        ),
		array(
            'subtype' => array(
                '$in' => '',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);

