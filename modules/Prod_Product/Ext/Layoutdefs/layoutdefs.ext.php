<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Layoutdefs/prod_product_prodo_product_document_1_Prod_Product.php

 // created: 2020-09-10 08:30:08
$layout_defs["Prod_Product"]["subpanel_setup"]['prod_product_prodo_product_document_1'] = array (
  'order' => 100,
  'module' => 'ProDo_Product_Document',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PROD_PRODUCT_PRODO_PRODUCT_DOCUMENT_1_FROM_PRODO_PRODUCT_DOCUMENT_TITLE',
  'get_subpanel_data' => 'prod_product_prodo_product_document_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Layoutdefs/prod_product_ori_order_request_item_1_Prod_Product.php

 // created: 2021-10-19 09:41:58
$layout_defs["Prod_Product"]["subpanel_setup"]['prod_product_ori_order_request_item_1'] = array (
  'order' => 100,
  'module' => 'ORI_Order_Request_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PROD_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'get_subpanel_data' => 'prod_product_ori_order_request_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Layoutdefs/prod_product_poi_purchase_order_item_1_Prod_Product.php

 // created: 2021-10-19 11:02:37
$layout_defs["Prod_Product"]["subpanel_setup"]['prod_product_poi_purchase_order_item_1'] = array (
  'order' => 100,
  'module' => 'POI_Purchase_Order_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PROD_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'get_subpanel_data' => 'prod_product_poi_purchase_order_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Layoutdefs/_overrideProd_Product_subpanel_prod_product_prodo_product_document_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Prod_Product']['subpanel_setup']['prod_product_prodo_product_document_1']['override_subpanel_name'] = 'Prod_Product_subpanel_prod_product_prodo_product_document_1';

?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Layoutdefs/_overrideProd_Product_subpanel_prod_product_ori_order_request_item_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Prod_Product']['subpanel_setup']['prod_product_ori_order_request_item_1']['override_subpanel_name'] = 'Prod_Product_subpanel_prod_product_ori_order_request_item_1';

?>
<?php
// Merged from custom/Extension/modules/Prod_Product/Ext/Layoutdefs/_overrideProd_Product_subpanel_prod_product_poi_purchase_order_item_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Prod_Product']['subpanel_setup']['prod_product_poi_purchase_order_item_1']['override_subpanel_name'] = 'Prod_Product_subpanel_prod_product_poi_purchase_order_item_1';

?>
