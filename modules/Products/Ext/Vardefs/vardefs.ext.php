<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Products/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:55
$dictionary['Product']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Products/Ext/Vardefs/sugarfield_turnaround_time_c.php

 // created: 2018-09-13 19:36:12
$dictionary['Product']['fields']['turnaround_time_c']['duplicate_merge_dom_value']=0;
$dictionary['Product']['fields']['turnaround_time_c']['labelValue']='Standard Turnaround Time';
$dictionary['Product']['fields']['turnaround_time_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Product']['fields']['turnaround_time_c']['calculated']='1';
$dictionary['Product']['fields']['turnaround_time_c']['formula']='related($product_templates_link,"turnaround_time_c")';
$dictionary['Product']['fields']['turnaround_time_c']['enforced']='1';
$dictionary['Product']['fields']['turnaround_time_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Products/Ext/Vardefs/sugarfield_timeline_type_c.php

 // created: 2018-09-13 19:50:29
$dictionary['Product']['fields']['timeline_type_c']['labelValue']='Timeline Type';
$dictionary['Product']['fields']['timeline_type_c']['dependency']='';
$dictionary['Product']['fields']['timeline_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Products/Ext/Vardefs/sugarfield_mft_part_num.php

 // created: 2018-09-13 20:01:54
$dictionary['Product']['fields']['mft_part_num']['audited']=false;
$dictionary['Product']['fields']['mft_part_num']['massupdate']=false;
$dictionary['Product']['fields']['mft_part_num']['comments']='Manufacturer part number';
$dictionary['Product']['fields']['mft_part_num']['duplicate_merge']='enabled';
$dictionary['Product']['fields']['mft_part_num']['duplicate_merge_dom_value']='1';
$dictionary['Product']['fields']['mft_part_num']['merge_filter']='disabled';
$dictionary['Product']['fields']['mft_part_num']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.15',
  'searchable' => true,
);
$dictionary['Product']['fields']['mft_part_num']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Products/Ext/Vardefs/sugarfield_test_turnaround_time_c.php

 // created: 2018-09-13 20:03:30
$dictionary['Product']['fields']['test_turnaround_time_c']['labelValue']='Test Turnaround Time';
$dictionary['Product']['fields']['test_turnaround_time_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Product']['fields']['test_turnaround_time_c']['enforced']='';
$dictionary['Product']['fields']['test_turnaround_time_c']['dependency']='';

 
?>
