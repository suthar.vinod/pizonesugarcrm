<?php
// created: 2016-01-06 02:48:10
$subpanel_layout['list_fields'] = array (
  'resource_name' => 
  array (
    'vname' => 'LBL_RESOURCE_NAME',
    'width' => '30%',
    'default' => true,
  ),
  'holiday_date' => 
  array (
    'vname' => 'LBL_HOLIDAY_DATE',
    'width' => '20%',
    'default' => true,
  ),
  'description' => 
  array (
    'vname' => 'LBL_DESCRIPTION',
    'width' => '48%',
    'sortable' => false,
    'default' => true,
  ),
  'remove_button' => 
  array (
    'widget_class' => 'SubPanelRemoveButtonProjects',
    'width' => '2%',
    'default' => true,
  ),
  'first_name' => 
  array (
    'usage' => 'query_only',
  ),
  'last_name' => 
  array (
    'usage' => 'query_only',
  ),
);