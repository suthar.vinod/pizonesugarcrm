<?php
$popupMeta = array (
    'moduleMain' => 'Ere_Error_Employees',
    'varName' => 'Ere_Error_Employees',
    'orderBy' => 'ere_error_employees.name',
    'whereClauses' => array (
  'name' => 'ere_error_employees.name',
  'initials_of_record_c' => 'ere_error_employees_cstm.initials_of_record_c',
),
    'searchInputs' => array (
  1 => 'name',
  2 => 'initials_of_record_c',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10',
  ),
  'initials_of_record_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_INITIALS_OF_RECORD',
    'width' => 10,
    'name' => 'initials_of_record_c',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'INITIALS_OF_RECORD_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_INITIALS_OF_RECORD',
    'width' => 10,
    'default' => true,
  ),
  'DEPARTMENT_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_DEPARTMENT',
    'width' => 10,
  ),
),
);
