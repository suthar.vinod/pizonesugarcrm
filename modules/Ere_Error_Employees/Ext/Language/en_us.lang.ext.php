<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Language/en_us.customm06_error_ere_error_employees_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Language/en_us.customm06_error_ere_error_employees_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_M06_ERROR_TITLE'] = 'Errors';
$mod_strings['LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE'] = 'Errors';

?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_M06_ERROR_TITLE'] = 'Errors';
$mod_strings['LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE'] = 'Errors';


?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Language/en_us.custom_department.php

$mod_strings['LBL_CUSTOM_DEPARTMENT'] = 'Department';

?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Employee';
$mod_strings['LBL_EMPLOYEE_NUMBER'] = 'Employee Number';
$mod_strings['LBL_INITIALS_OF_RECORD'] = 'Initials of Record';
$mod_strings['LBL_DEPARTMENT'] = 'Department';
$mod_strings['LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_M06_ERROR_TITLE'] = 'Deviations';
$mod_strings['LNK_NEW_RECORD'] = 'Create APS Employee';
$mod_strings['LNK_LIST'] = 'View APS Employees';
$mod_strings['LBL_MODULE_NAME'] = 'APS Employees';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'APS Employee';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New APS Employee';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import APS Employee vCard';
$mod_strings['LNK_IMPORT_ERE_ERROR_EMPLOYEES'] = 'Import APS Employees';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search APS Employee';
$mod_strings['LBL_ERE_ERROR_EMPLOYEES_SUBPANEL_TITLE'] = 'APS Employees';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My APS Employees';
$mod_strings['LBL_RESPONSIBLE_PERSONNEL'] = 'Responsible Personnel';
$mod_strings['LBL_ABC'] = 'abc';
$mod_strings['LBL_MANAGER'] = 'Manager';
$mod_strings['LBL_ERE_ERROR_EMPLOYEES_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Observation Employee';
$mod_strings['LBL_ERE_ERROR_EMPLOYEES_FOCUS_DRAWER_DASHBOARD'] = 'APS Employees Focus Drawer';
$mod_strings['LBL_ERE_ERROR_EMPLOYEES_RECORD_DASHBOARD'] = 'APS Employees Record Dashboard';

?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Language/en_us.customere_error_employees_de_deviation_employees_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ERE_ERROR_EMPLOYEES_DE_DEVIATION_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Deviation Employees';
$mod_strings['LBL_ERE_ERROR_EMPLOYEES_DE_DEVIATION_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE'] = 'Deviation Employees';

?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Language/en_us.customde_deviation_employees_ere_error_employees_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DE_DEVIATION_EMPLOYEES_ERE_ERROR_EMPLOYEES_1_FROM_DE_DEVIATION_EMPLOYEES_TITLE'] = 'Deviation Employees';
$mod_strings['LBL_DE_DEVIATION_EMPLOYEES_ERE_ERROR_EMPLOYEES_1_FROM_ERE_ERROR_EMPLOYEES_TITLE'] = 'Deviation Employees';

?>
