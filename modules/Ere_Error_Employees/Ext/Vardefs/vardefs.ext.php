<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Vardefs/sugarfield_employee_number_c.php

 // created: 2018-05-10 14:53:31
$dictionary['Ere_Error_Employees']['fields']['employee_number_c']['labelValue']='Employee Number';
$dictionary['Ere_Error_Employees']['fields']['employee_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Ere_Error_Employees']['fields']['employee_number_c']['enforced']='';
$dictionary['Ere_Error_Employees']['fields']['employee_number_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Vardefs/sugarfield_initials_of_record_c.php

 // created: 2018-05-11 13:40:40
$dictionary['Ere_Error_Employees']['fields']['initials_of_record_c']['labelValue']='Initials of Record';
$dictionary['Ere_Error_Employees']['fields']['initials_of_record_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Ere_Error_Employees']['fields']['initials_of_record_c']['enforced']='';
$dictionary['Ere_Error_Employees']['fields']['initials_of_record_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Vardefs/sugarfield_email.php

/**
 * Created by PhpStorm.
 * User: nabeel.alvi
 * Date: 8/16/18
 * Time: 7:39 PM
 */
$module = 'Ere_Error_Employees';
$dictionary[$module]['fields']['email'] = array(
    'name'        => 'email',
    'vname'        => 'LBL_EMAIL',
    'group'=>'email',
    'type'        => 'email',
    'function'    => array(
        'name'        => 'getEmailAddressWidget',
        'returns'    => 'html'
    ),
    'source'    => 'non-db',
    'studio' => array(
        'editField' => true,
        'visible' => true,
        'searchview' => true,
        'editview' => true),
);
$dictionary[$module]['fields']['email_addresses_primary'] = array(
    'name' => 'email_addresses_primary',
    'type' => 'link',
    'relationship' =>strtolower($module).'_email_addresses_primary',
    'source' => 'non-db',
    'vname' => 'LBL_EMAIL_ADDRESS_PRIMARY',
    'duplicate_merge' => 'disabled',
);
$dictionary[$module]['fields']['email_addresses'] = array (
    'name' => 'email_addresses',
    'type' => 'link',
    'relationship' =>strtolower($module).'_email_addresses',
    'source' => 'non-db',
    'vname' => 'LBL_EMAIL_ADDRESSES',
    'reportable'=>false,
    'unified_search' => true,
    'rel_fields' =>array('primary_address' => array('type'=>'bool')),
);
$dictionary[$module]['relationships'][strtolower($module).'_email_addresses'] = array(
    'lhs_module'=> $module,
    'lhs_table'=>strtolower($module),
    'lhs_key' => 'id',
    'rhs_module'=> 'EmailAddresses',
    'rhs_table'=> 'email_addresses',
    'rhs_key' => 'id',
    'relationship_type'=>'many-to-many',
    'join_table'=> 'email_addr_bean_rel',
    'join_key_lhs'=>'bean_id',
    'join_key_rhs'=>'email_address_id',
    'relationship_role_column'=>'bean_module',
    'relationship_role_column_value'=>$module
);
$dictionary[$module]['relationships'][strtolower($module).'_email_addresses_primary'] = array(
    'lhs_module'=> $module,
    'lhs_table' =>strtolower($module),
    'lhs_key' => 'id',
    'rhs_module' => 'EmailAddresses',
    'rhs_table' => 'email_addresses',
    'rhs_key' => 'id',
    'relationship_type'=>'many-to-many',
    'join_table'=> 'email_addr_bean_rel',
    'join_key_lhs'=>'bean_id',
    'join_key_rhs'=>'email_address_id',
    'relationship_role_column'=>'primary_address',
    'relationship_role_column_value'=>'1'
);
?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Vardefs/sugarfield_responsible_personnel_c.php

 // created: 2018-07-06 13:54:35
$dictionary['Ere_Error_Employees']['fields']['responsible_personnel_c']['labelValue']='Responsible Personnel';
$dictionary['Ere_Error_Employees']['fields']['responsible_personnel_c']['dependency']='';
$dictionary['Ere_Error_Employees']['fields']['responsible_personnel_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Vardefs/sugarfield_departments_c.php

 $dictionary['Ere_Error_Employees']['fields']['department'] = array(
    'name' => 'department',
    'label' => 'LBL_DEPARTMENT',
    'type' => 'enum',
    'help' => '',
    'comment' => '',
    'options' => 'departments_list', //maps to options - specify list name
    'default_value' => '', //key of entry in specified list
    'mass_update' => false, // true or false
    'required' => false, // true or false
    'reportable' => true, // true or false
    'audited' => false, // true or false
    'importable' => 'true', // 'true', 'false' or 'required'
    'duplicate_merge' => false, // true or false
);
 
?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Vardefs/sugarfield_ed_errors_department_id_c.php

 // created: 2018-08-10 10:58:48

 
?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Vardefs/sugarfield_manager_c.php


$dictionary['Ere_Error_Employees']['fields']['manager'] = array(
    'name'=>'manager',
    'vname'=>'LBL_MANAGER',
    'type' => 'enum',
    'massupdate' => false,
    'importable' => 'true',
    'duplicate_merge' => 'false',
    'required' => false,
    'reportable' => true,
    'no_default' => false,
    'options' => 'is_manager_list',
    'default' => 'Choose One',
);

?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Vardefs/m06_error_ere_error_employees_1_Ere_Error_Employees.php

// created: 2018-01-31 21:12:49
/*$dictionary["Ere_Error_Employees"]["fields"]["m06_error_ere_error_employees_1"] = array (
  'name' => 'm06_error_ere_error_employees_1',
  'type' => 'link',
  'relationship' => 'm06_error_ere_error_employees_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_ERE_ERROR_EMPLOYEES_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_ere_error_employees_1m06_error_ida',
);*/

?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Vardefs/sugarfield_department_c.php

 // created: 2018-11-15 21:24:47
$dictionary['Ere_Error_Employees']['fields']['department_c']['labelValue']='Department';
$dictionary['Ere_Error_Employees']['fields']['department_c']['dependency']='';
$dictionary['Ere_Error_Employees']['fields']['department_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Ere_Error_Employees/Ext/Vardefs/sugarfield_department.php

 // created: 2018-11-16 14:30:36
$dictionary['Ere_Error_Employees']['fields']['department']['massupdate']=false;
$dictionary['Ere_Error_Employees']['fields']['department']['duplicate_merge']='disabled';
$dictionary['Ere_Error_Employees']['fields']['department']['duplicate_merge_dom_value']='0';
$dictionary['Ere_Error_Employees']['fields']['department']['merge_filter']='disabled';
$dictionary['Ere_Error_Employees']['fields']['department']['unified_search']=false;
$dictionary['Ere_Error_Employees']['fields']['department']['calculated']=false;
$dictionary['Ere_Error_Employees']['fields']['department']['dependency']=false;
$dictionary['Ere_Error_Employees']['fields']['department']['studio']=false;

 
?>
