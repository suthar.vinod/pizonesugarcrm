<?php
$module_name = 'Erd_Error_Documents';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'doc_id_c',
                'label' => 'LBL_DOC_ID',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'department_owner_c',
                'label' => 'LBL_DEPARTMENT_OWNER',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'deviation_rate_basis_c',
                'label' => 'LBL_DEVIATION_RATE_BASIS',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'form_owner_c',
                'label' => 'LBL_FORM_OWNER',
                'enabled' => true,
                'readonly' => false,
                'id' => 'CONTACT_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'data_cells_per_cd_c',
                'label' => 'LBL_DATA_CELLS_PER_CD',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'average_cd_pages_per_basis_c',
                'label' => 'LBL_AVERAGE_CD_PAGES_PER_BASIS',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
