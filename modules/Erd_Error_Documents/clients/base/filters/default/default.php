<?php
// created: 2021-06-24 11:00:36
$viewdefs['Erd_Error_Documents']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'assigned_user_name' => 
    array (
    ),
    'average_cd_pages_per_basis_c' => 
    array (
    ),
    'data_cells_per_cd_c' => 
    array (
    ),
    'department_owner_c' => 
    array (
    ),
    'doc_id_c' => 
    array (
    ),
    'deviation_rate_basis_c' => 
    array (
    ),
    'form_owner_c' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'name' => 
    array (
    ),
    'tag' => 
    array (
    ),
  ),
);