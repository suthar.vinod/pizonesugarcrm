<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Language/en_us.customm06_error_erd_error_documents_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Language/en_us.customm06_error_erd_error_documents_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_M06_ERROR_TITLE'] = 'Errors';
$mod_strings['LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE'] = 'Errors';

?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_M06_ERROR_TITLE'] = 'Errors';
$mod_strings['LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE'] = 'Errors';


?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DOC_ID'] = 'Document ID';
$mod_strings['LBL_RECORD_BODY'] = 'Docs';
$mod_strings['LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE'] = 'Deviation';
$mod_strings['LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_M06_ERROR_TITLE'] = 'Communications';
$mod_strings['LNK_NEW_RECORD'] = 'Create Controlled Document';
$mod_strings['LNK_LIST'] = 'View Controlled Documents';
$mod_strings['LBL_MODULE_NAME'] = 'Controlled Documents';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Controlled Document';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Controlled Document';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Controlled Document vCard';
$mod_strings['LNK_IMPORT_ERD_ERROR_DOCUMENTS'] = 'Import Controlled Documents';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Controlled Document';
$mod_strings['LBL_ERD_ERROR_DOCUMENTS_SUBPANEL_TITLE'] = 'Controlled Documents';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Controlled Documents';
$mod_strings['LBL_ERD_ERROR_DOCUMENTS_FOCUS_DRAWER_DASHBOARD'] = 'Controlled Documents Focus Drawer';
$mod_strings['LBL_DEPARTMENT_OWNER'] = 'Department Owner';
$mod_strings['LBL_ERD_ERROR_DOCUMENTS_RECORD_DASHBOARD'] = 'Controlled Documents Record Dashboard';
$mod_strings['LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_CDU_CD_UTILIZATION_TITLE'] = 'CD Utilizations/Deviation Rates';
$mod_strings['LBL_DEVIATION_RATE_BASIS'] = 'Deviation Rate Basis';
$mod_strings['LBL_DATA_CELLS_PER_CD'] = 'Data Cells Per CD';
$mod_strings['LBL_AVERAGE_CD_PAGES_PER_BASIS'] = 'Average CD Pages Per Basis';
$mod_strings['LBL_FORM_OWNER_C_CONTACT_ID'] = 'Document Owner (related Contact ID)';
$mod_strings['LBL_FORM_OWNER'] = 'Document Owner';

?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Language/en_us.customm03_work_product_erd_error_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Language/en_us.customerd_error_documents_cdu_cd_utilization_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_CDU_CD_UTILIZATION_TITLE'] = 'Controlled Document Utilizations';
$mod_strings['LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_ERD_ERROR_DOCUMENTS_TITLE'] = 'Controlled Document Utilizations';

?>
