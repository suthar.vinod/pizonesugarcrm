<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Vardefs/m06_error_erd_error_documents_1_Erd_Error_Documents.php

// created: 2018-01-31 21:10:46
$dictionary["Erd_Error_Documents"]["fields"]["m06_error_erd_error_documents_1"] = array (
  'name' => 'm06_error_erd_error_documents_1',
  'type' => 'link',
  'relationship' => 'm06_error_erd_error_documents_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_erd_error_documents_1m06_error_ida',
);

?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Vardefs/sugarfield_name.php

 // created: 2019-05-23 17:03:13
$dictionary['Erd_Error_Documents']['fields']['name']['len']='255';
$dictionary['Erd_Error_Documents']['fields']['name']['audited']=true;
$dictionary['Erd_Error_Documents']['fields']['name']['massupdate']=false;
$dictionary['Erd_Error_Documents']['fields']['name']['unified_search']=false;
$dictionary['Erd_Error_Documents']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Erd_Error_Documents']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Vardefs/sugarfield_doc_id_c.php

 // created: 2019-05-23 17:03:53
$dictionary['Erd_Error_Documents']['fields']['doc_id_c']['labelValue']='Document ID';
$dictionary['Erd_Error_Documents']['fields']['doc_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Erd_Error_Documents']['fields']['doc_id_c']['enforced']='';
$dictionary['Erd_Error_Documents']['fields']['doc_id_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Vardefs/m03_work_product_erd_error_documents_1_Erd_Error_Documents.php

// created: 2019-11-05 12:53:29
$dictionary["Erd_Error_Documents"]["fields"]["m03_work_product_erd_error_documents_1"] = array (
  'name' => 'm03_work_product_erd_error_documents_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_erd_error_documents_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_erd_error_documents_1m03_work_product_ida',
);

?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Vardefs/sugarfield_department_owner_c.php

 // created: 2021-03-04 09:04:15
$dictionary['Erd_Error_Documents']['fields']['department_owner_c']['labelValue']='Department Owner';
$dictionary['Erd_Error_Documents']['fields']['department_owner_c']['dependency']='';
$dictionary['Erd_Error_Documents']['fields']['department_owner_c']['required_formula']='';
$dictionary['Erd_Error_Documents']['fields']['department_owner_c']['readonly_formula']='';
$dictionary['Erd_Error_Documents']['fields']['department_owner_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Vardefs/erd_error_documents_cdu_cd_utilization_1_Erd_Error_Documents.php

// created: 2021-06-29 08:02:02
$dictionary["Erd_Error_Documents"]["fields"]["erd_error_documents_cdu_cd_utilization_1"] = array (
  'name' => 'erd_error_documents_cdu_cd_utilization_1',
  'type' => 'link',
  'relationship' => 'erd_error_documents_cdu_cd_utilization_1',
  'source' => 'non-db',
  'module' => 'CDU_CD_Utilization',
  'bean_name' => 'CDU_CD_Utilization',
  'vname' => 'LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'id_name' => 'erd_error_documents_cdu_cd_utilization_1erd_error_documents_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Vardefs/sugarfield_deviation_rate_basis_c.php

 // created: 2021-06-29 08:47:05
$dictionary['Erd_Error_Documents']['fields']['deviation_rate_basis_c']['labelValue']='Deviation Rate Basis';
$dictionary['Erd_Error_Documents']['fields']['deviation_rate_basis_c']['dependency']='';
$dictionary['Erd_Error_Documents']['fields']['deviation_rate_basis_c']['required_formula']='';
$dictionary['Erd_Error_Documents']['fields']['deviation_rate_basis_c']['readonly_formula']='';
$dictionary['Erd_Error_Documents']['fields']['deviation_rate_basis_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Vardefs/sugarfield_data_cells_per_cd_c.php

 // created: 2021-06-29 08:50:28
$dictionary['Erd_Error_Documents']['fields']['data_cells_per_cd_c']['labelValue']='Data Cells Per CD';
$dictionary['Erd_Error_Documents']['fields']['data_cells_per_cd_c']['enforced']='false';
$dictionary['Erd_Error_Documents']['fields']['data_cells_per_cd_c']['dependency']='isInList($deviation_rate_basis_c,createList("Animal Days","Procedures","Studies"))';
$dictionary['Erd_Error_Documents']['fields']['data_cells_per_cd_c']['required_formula']='';
$dictionary['Erd_Error_Documents']['fields']['data_cells_per_cd_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Vardefs/sugarfield_average_cd_pages_per_basis_c.php

 // created: 2021-06-29 08:53:22
$dictionary['Erd_Error_Documents']['fields']['average_cd_pages_per_basis_c']['labelValue']='Average CD Pages Per Basis';
$dictionary['Erd_Error_Documents']['fields']['average_cd_pages_per_basis_c']['enforced']='';
$dictionary['Erd_Error_Documents']['fields']['average_cd_pages_per_basis_c']['dependency']='isInList($deviation_rate_basis_c,createList("Animal Days","Procedures","Studies"))';
$dictionary['Erd_Error_Documents']['fields']['average_cd_pages_per_basis_c']['required_formula']='';
$dictionary['Erd_Error_Documents']['fields']['average_cd_pages_per_basis_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Vardefs/sugarfield_contact_id_c.php

 // created: 2021-06-29 08:57:11

 
?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Vardefs/sugarfield_form_owner_c.php

 // created: 2021-06-29 08:57:11
$dictionary['Erd_Error_Documents']['fields']['form_owner_c']['labelValue']='Document Owner';
$dictionary['Erd_Error_Documents']['fields']['form_owner_c']['dependency']='isInList($deviation_rate_basis_c,createList("Animal Days","Procedures","Studies"))';
$dictionary['Erd_Error_Documents']['fields']['form_owner_c']['required_formula']='';
$dictionary['Erd_Error_Documents']['fields']['form_owner_c']['readonly_formula']='';

 
?>
