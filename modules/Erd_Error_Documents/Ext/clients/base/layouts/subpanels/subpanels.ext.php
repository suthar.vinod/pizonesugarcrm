<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-06-29 08:02:02
$viewdefs['Erd_Error_Documents']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_CDU_CD_UTILIZATION_TITLE',
  'context' => 
  array (
    'link' => 'erd_error_documents_cdu_cd_utilization_1',
  ),
);

// created: 2019-11-05 12:53:29
$viewdefs['Erd_Error_Documents']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_erd_error_documents_1',
  ),
);

// created: 2018-01-31 21:10:46
$viewdefs['Erd_Error_Documents']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_M06_ERROR_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_erd_error_documents_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['Erd_Error_Documents']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'erd_error_documents_cdu_cd_utilization_1',
  'view' => 'subpanel-for-erd_error_documents-erd_error_documents_cdu_cd_utilization_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Erd_Error_Documents']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_erd_error_documents_1',
  'view' => 'subpanel-for-erd_error_documents-m03_work_product_erd_error_documents_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Erd_Error_Documents']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm06_error_erd_error_documents_1',
  'view' => 'subpanel-for-erd_error_documents-m06_error_erd_error_documents_1',
);
