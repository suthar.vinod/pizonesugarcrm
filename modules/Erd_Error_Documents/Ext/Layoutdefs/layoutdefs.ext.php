<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Layoutdefs/m06_error_erd_error_documents_1_Erd_Error_Documents.php

 // created: 2018-01-31 21:10:46
$layout_defs["Erd_Error_Documents"]["subpanel_setup"]['m06_error_erd_error_documents_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M06_ERROR_ERD_ERROR_DOCUMENTS_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'm06_error_erd_error_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Layoutdefs/m03_work_product_erd_error_documents_1_Erd_Error_Documents.php

 // created: 2019-11-05 12:53:28
$layout_defs["Erd_Error_Documents"]["subpanel_setup"]['m03_work_product_erd_error_documents_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'm03_work_product_erd_error_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Layoutdefs/erd_error_documents_cdu_cd_utilization_1_Erd_Error_Documents.php

 // created: 2021-06-29 08:02:02
$layout_defs["Erd_Error_Documents"]["subpanel_setup"]['erd_error_documents_cdu_cd_utilization_1'] = array (
  'order' => 100,
  'module' => 'CDU_CD_Utilization',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ERD_ERROR_DOCUMENTS_CDU_CD_UTILIZATION_1_FROM_CDU_CD_UTILIZATION_TITLE',
  'get_subpanel_data' => 'erd_error_documents_cdu_cd_utilization_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Layoutdefs/_overrideErd_Error_Documents_subpanel_m06_error_erd_error_documents_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Erd_Error_Documents']['subpanel_setup']['m06_error_erd_error_documents_1']['override_subpanel_name'] = 'Erd_Error_Documents_subpanel_m06_error_erd_error_documents_1';

?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Layoutdefs/_overrideErd_Error_Documents_subpanel_erd_error_documents_cdu_cd_utilization_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Erd_Error_Documents']['subpanel_setup']['erd_error_documents_cdu_cd_utilization_1']['override_subpanel_name'] = 'Erd_Error_Documents_subpanel_erd_error_documents_cdu_cd_utilization_1';

?>
<?php
// Merged from custom/Extension/modules/Erd_Error_Documents/Ext/Layoutdefs/_overrideErd_Error_Documents_subpanel_m03_work_product_erd_error_documents_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Erd_Error_Documents']['subpanel_setup']['m03_work_product_erd_error_documents_1']['override_subpanel_name'] = 'Erd_Error_Documents_subpanel_m03_work_product_erd_error_documents_1';

?>
