<?php
// created: 2019-11-05 12:55:42
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'doc_id_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_DOC_ID',
    'width' => 10,
    'default' => true,
  ),
);