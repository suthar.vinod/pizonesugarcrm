<?php
// created: 2021-05-27 09:02:52
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'doc_id_c' => 
  array (
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_DOC_ID',
    'width' => 10,
    'default' => true,
  ),
);