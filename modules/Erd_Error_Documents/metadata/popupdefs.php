<?php
$popupMeta = array (
    'moduleMain' => 'Erd_Error_Documents',
    'varName' => 'Erd_Error_Documents',
    'orderBy' => 'erd_error_documents.name',
    'whereClauses' => array (
  'name' => 'erd_error_documents.name',
  'doc_id_c' => 'erd_error_documents_cstm.doc_id_c',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'doc_id_c',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'doc_id_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_DOC_ID',
    'width' => 10,
    'name' => 'doc_id_c',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'DOC_ID_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_DOC_ID',
    'width' => 10,
    'default' => true,
  ),
),
);
