<?php

class customLogicHook {

    function generateSystemId($bean, $event, $arguments) {
        global $db;
        $year = date("y"); ///get the year in 2 digit
        //Read Lock
        $sql_lock = "Lock TABLES m01_sales READ";
        $bean->db->query($sql_lock);

        // In our Sales Activities Module, we would like to updated the automatic naming function 
        // from SA16-XXXX to “APS16-XXXX�?.
        //$query = "Select name from m01_sales where name like '%APS16-%' ORDER BY m01_sales.name DESC LIMIT 0,1"; 
        $query = 'SELECT substring(name,-4) as name From m01_sales WHERE name LIKE "%APS' . $year . '%-%" ORDER BY name DESC LIMIT 0,1';
        $result = $db->query($query);
        // Unlock Table
        $sql_unlock = "UNLOCK TABLES";
        $bean->db->query($sql_unlock);

        if ($result->num_rows > 0) {
            $row = $db->fetchByAssoc($result);
            $system_id = $row['name'];
            if (!empty($system_id)) {
                #$number = (int) substr($system_id, -4);    
                $number = (int) $system_id;
                $number = $number + 1;
                $length = strlen($number);
                $allowed_length = 4;
                $left_length = $allowed_length - $length;
                $code = "APS";

                $code .= $year;
                $code .= '-';

                for ($i = 0; $i < $left_length; $i++) {
                    $code .= "0";
                }
                $code .= $number;
            } else {
                $code = "APS" . $year . "-0001";
            }
        } else {
            $code = "APS" . $year . "-0001";
        }

        if ($bean->name == "") {
            $bean->name = $code;
        }
    }

}
