<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_m02_sa_division_department__1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_M02_SA_DIVISION_DEPARTMENT__1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_M02_SA_DIVISION_DEPARTMENT__1_FROM_M02_SA_DIVISION_DEPARTMENT__TITLE'] = 'SA Divisions Departments ';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_m06_aps_communication_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_M06_APS_COMMUNICATION_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_M06_APS_COMMUNICATION_1_FROM_M06_APS_COMMUNICATION_TITLE'] = 'APS Communications';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_documents_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_DOCUMENTS_2_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_DOCUMENTS_2_FROM_DOCUMENTS_TITLE'] = 'Documents';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_m01_sales_activity_quote_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_M01_SALES_ACTIVITY_QUOTE_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_M01_SALES_ACTIVITY_QUOTE_1_FROM_M01_SALES_ACTIVITY_QUOTE_TITLE'] = 'Sales Activity Quotes';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_meetings_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_MEETINGS_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';
$mod_strings['LBL_M01_SALES_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Communications';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_m01_quote_document_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'Sales Activity Quotes';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_tasks_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_TASKS_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_TASKS_1_FROM_TASKS_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_DOCUMENTS_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Documents';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm03_work_product_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M01_SALES_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M01_SALES_1_FROM_M01_SALES_TITLE_ID'] = 'M03_Work_Product ID';
$mod_strings['LBL_M03_WORK_PRODUCT_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customaccounts_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M01_SALES_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE_ID'] = 'Companies ID';
$mod_strings['LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Companies';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customtasks_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TASKS_M01_SALES_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_TASKS_M01_SALES_1_FROM_M01_SALES_TITLE_ID'] = 'Tasks ID';
$mod_strings['LBL_TASKS_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_quote_document_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M01_SALES_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote Documents';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M01_SALES_1_FROM_M01_SALES_TITLE_ID'] = 'SA Quote Documents ID';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'SA Quote Documents';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customaccounts_m01_sales_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M01_SALES_2_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M01_SALES_2_FROM_M01_SALES_TITLE_ID'] = 'Companies ID';
$mod_strings['LBL_ACCOUNTS_M01_SALES_2_FROM_M01_SALES_TITLE'] = 'Companies';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_notes_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_NOTES_1_FROM_NOTES_TITLE'] = 'Activity Notes';
$mod_strings['LBL_M01_SALES_NOTES_1_FROM_M01_SALES_TITLE'] = 'Activity Notes';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_an01_activity_notes_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_AN01_ACTIVITY_NOTES_1_FROM_AN01_ACTIVITY_NOTES_TITLE'] = 'Activity Notes';
$mod_strings['LBL_M01_SALES_AN01_ACTIVITY_NOTES_1_FROM_M01_SALES_TITLE'] = 'Activity Notes';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_quotes_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_QUOTES_1_FROM_QUOTES_TITLE'] = 'Quotes';
$mod_strings['LBL_M01_SALES_QUOTES_1_FROM_M01_SALES_TITLE'] = 'Quotes';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customtm_tradeshow_management_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE'] = 'Tradeshow Management';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Tradeshow Management';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_activities_1_calls.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_ACTIVITIES_1_CALLS_FROM_CALLS_TITLE'] = 'Calls';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_activities_1_meetings.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE'] = 'Communications';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_activities_1_notes.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_ACTIVITIES_1_NOTES_FROM_NOTES_TITLE'] = 'Notes';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_activities_1_tasks.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_ACTIVITIES_1_TASKS_FROM_TASKS_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_activities_1_emails.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_ACTIVITIES_1_EMAILS_FROM_EMAILS_TITLE'] = 'Emails';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customcontacts_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTS_M01_SALES_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACTS_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Contacts';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customrr_regulatory_response_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_RR_REGULATORY_RESPONSE_TITLE'] = 'Regulatory Responses';
$mod_strings['LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Regulatory Responses';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_edoc_email_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE'] = 'Email Documents';
$mod_strings['LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_M01_SALES_TITLE'] = 'Email Documents';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_m01_sales_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_M01_SALES_1_FROM_M01_SALES_R_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_M01_SALES_1_FROM_M01_SALES_L_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_ori_order_request_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Order Request Items';
$mod_strings['LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_M01_SALES_TITLE'] = 'Order Request Items';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_poi_purchase_order_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE'] = 'Purchase Order Items';
$mod_strings['LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_M01_SALES_TITLE'] = 'Purchase Order Items';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_M01_SALES_TITLE'] = 'Inventory Items';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_ii_inventory_item_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_II_INVENTORY_ITEM_2_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_M01_SALES_II_INVENTORY_ITEM_2_FROM_M01_SALES_TITLE'] = 'Inventory Items';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customm01_sales_im_inventory_management_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE'] = 'Inventory Management';
$mod_strings['LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_M01_SALES_TITLE'] = 'Inventory Management';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Language/de_DE.customnsc_namsa_sub_companies_m01_sales_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE'] = 'NAMSA Subcontracting Companies';
$mod_strings['LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'NAMSA Subcontracting Companies';

?>
