<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/m01_sales_m01_quote_document_1_M01_Sales.php

 // created: 2016-01-25 22:19:12
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_m01_quote_document_1'] = array (
  'order' => 100,
  'module' => 'M01_Quote_Document',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE',
  'get_subpanel_data' => 'm01_sales_m01_quote_document_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/m01_sales_tasks_1_M01_Sales.php

 // created: 2016-01-25 23:00:46
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M01_SALES_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'm01_sales_tasks_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/m01_sales_m03_work_product_1_M01_Sales.php

 // created: 2016-01-29 21:10:09
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'm01_sales_m03_work_product_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/m01_sales_activities_1_calls_M01_Sales.php

 // created: 2018-12-10 23:01:47
$layout_defs["M01_Sales"]["subpanel_setup"]['activities'] = array (
  'order' => 10,
  'sort_order' => 'desc',
  'sort_by' => 'date_start',
  'title_key' => 'LBL_ACTIVITIES_SUBPANEL_TITLE',
  'type' => 'collection',
  'subpanel_name' => 'activities',
  'module' => 'Activities',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateTaskButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopScheduleMeetingButton',
    ),
    2 => 
    array (
      'widget_class' => 'SubPanelTopScheduleCallButton',
    ),
    3 => 
    array (
      'widget_class' => 'SubPanelTopComposeEmailButton',
    ),
  ),
  'collection_list' => 
  array (
    'meetings' => 
    array (
      'module' => 'Meetings',
      'subpanel_name' => 'ForActivities',
      'get_subpanel_data' => 'm01_sales_activities_1_meetings',
    ),
    'tasks' => 
    array (
      'module' => 'Tasks',
      'subpanel_name' => 'ForActivities',
      'get_subpanel_data' => 'm01_sales_activities_1_tasks',
    ),
    'calls' => 
    array (
      'module' => 'Calls',
      'subpanel_name' => 'ForActivities',
      'get_subpanel_data' => 'm01_sales_activities_1_calls',
    ),
  ),
  'get_subpanel_data' => 'activities',
);
$layout_defs["M01_Sales"]["subpanel_setup"]['history'] = array (
  'order' => 20,
  'sort_order' => 'desc',
  'sort_by' => 'date_modified',
  'title_key' => 'LBL_HISTORY',
  'type' => 'collection',
  'subpanel_name' => 'history',
  'module' => 'History',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateNoteButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopArchiveEmailButton',
    ),
    2 => 
    array (
      'widget_class' => 'SubPanelTopSummaryButton',
    ),
  ),
  'collection_list' => 
  array (
    'meetings' => 
    array (
      'module' => 'Meetings',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'm01_sales_activities_1_meetings',
    ),
    'tasks' => 
    array (
      'module' => 'Tasks',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'm01_sales_activities_1_tasks',
    ),
    'calls' => 
    array (
      'module' => 'Calls',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'm01_sales_activities_1_calls',
    ),
    'notes' => 
    array (
      'module' => 'Notes',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'm01_sales_activities_1_notes',
    ),
    'emails' => 
    array (
      'module' => 'Emails',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'm01_sales_activities_1_emails',
    ),
  ),
  'get_subpanel_data' => 'history',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/rr_regulatory_response_m01_sales_1_M01_Sales.php

 // created: 2019-07-09 11:54:56
$layout_defs["M01_Sales"]["subpanel_setup"]['rr_regulatory_response_m01_sales_1'] = array (
  'order' => 100,
  'module' => 'RR_Regulatory_Response',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'get_subpanel_data' => 'rr_regulatory_response_m01_sales_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/m01_sales_edoc_email_documents_1_M01_Sales.php

 // created: 2019-08-27 11:35:41
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_edoc_email_documents_1'] = array (
  'order' => 100,
  'module' => 'EDoc_Email_Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'm01_sales_edoc_email_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/m01_sales_m01_sales_1_M01_Sales.php

 // created: 2021-09-23 08:38:16
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_m01_sales_1'] = array (
  'order' => 100,
  'module' => 'M01_Sales',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M01_SALES_M01_SALES_1_FROM_M01_SALES_R_TITLE',
  'get_subpanel_data' => 'm01_sales_m01_sales_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/m01_sales_ori_order_request_item_1_M01_Sales.php

 // created: 2021-10-19 10:26:07
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_ori_order_request_item_1'] = array (
  'order' => 100,
  'module' => 'ORI_Order_Request_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'get_subpanel_data' => 'm01_sales_ori_order_request_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/m01_sales_poi_purchase_order_item_1_M01_Sales.php

 // created: 2021-10-19 10:58:37
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_poi_purchase_order_item_1'] = array (
  'order' => 100,
  'module' => 'POI_Purchase_Order_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'get_subpanel_data' => 'm01_sales_poi_purchase_order_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/m01_sales_ii_inventory_item_1_M01_Sales.php

 // created: 2021-11-09 09:56:04
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'm01_sales_ii_inventory_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/m01_sales_im_inventory_management_1_M01_Sales.php

 // created: 2021-11-09 10:22:13
$layout_defs["M01_Sales"]["subpanel_setup"]['m01_sales_im_inventory_management_1'] = array (
  'order' => 100,
  'module' => 'IM_Inventory_Management',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'get_subpanel_data' => 'm01_sales_im_inventory_management_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/_overrideM01_Sales_subpanel_m01_sales_m01_sales_activity_quote_1.php

//auto-generated file DO NOT EDIT
$layout_defs['M01_Sales']['subpanel_setup']['m01_sales_m01_sales_activity_quote_1']['override_subpanel_name'] = 'M01_Sales_subpanel_m01_sales_m01_sales_activity_quote_1';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/_overrideM01_Sales_subpanel_m01_sales_m02_sa_division_department__1.php

//auto-generated file DO NOT EDIT
$layout_defs['M01_Sales']['subpanel_setup']['m01_sales_m02_sa_division_department__1']['override_subpanel_name'] = 'M01_Sales_subpanel_m01_sales_m02_sa_division_department__1';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/_overrideM01_Sales_subpanel_m01_sales_documents_1.php

//auto-generated file DO NOT EDIT
$layout_defs['M01_Sales']['subpanel_setup']['m01_sales_documents_1']['override_subpanel_name'] = 'M01_Sales_subpanel_m01_sales_documents_1';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/_overrideM01_Sales_subpanel_m01_sales_tasks_1.php

//auto-generated file DO NOT EDIT
$layout_defs['M01_Sales']['subpanel_setup']['m01_sales_tasks_1']['override_subpanel_name'] = 'M01_Sales_subpanel_m01_sales_tasks_1';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/_overrideM01_Sales_subpanel_m01_sales_an01_activity_notes_1.php

//auto-generated file DO NOT EDIT
$layout_defs['M01_Sales']['subpanel_setup']['m01_sales_an01_activity_notes_1']['override_subpanel_name'] = 'M01_Sales_subpanel_m01_sales_an01_activity_notes_1';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/_overrideM01_Sales_subpanel_m01_sales_meetings_1.php

//auto-generated file DO NOT EDIT
$layout_defs['M01_Sales']['subpanel_setup']['m01_sales_meetings_1']['override_subpanel_name'] = 'M01_Sales_subpanel_m01_sales_meetings_1';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/_overrideM01_Sales_subpanel_m01_sales_quotes_1.php

//auto-generated file DO NOT EDIT
$layout_defs['M01_Sales']['subpanel_setup']['m01_sales_quotes_1']['override_subpanel_name'] = 'M01_Sales_subpanel_m01_sales_quotes_1';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/_overrideM01_Sales_subpanel_m01_sales_activities_1_emails.php

//auto-generated file DO NOT EDIT
$layout_defs['M01_Sales']['subpanel_setup']['m01_sales_activities_1_emails']['override_subpanel_name'] = 'M01_Sales_subpanel_m01_sales_activities_1_emails';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/_overrideM01_Sales_subpanel_m01_sales_m01_quote_document_1.php

//auto-generated file DO NOT EDIT
$layout_defs['M01_Sales']['subpanel_setup']['m01_sales_m01_quote_document_1']['override_subpanel_name'] = 'M01_Sales_subpanel_m01_sales_m01_quote_document_1';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/_overrideM01_Sales_subpanel_m01_sales_m03_work_product_1.php

//auto-generated file DO NOT EDIT
$layout_defs['M01_Sales']['subpanel_setup']['m01_sales_m03_work_product_1']['override_subpanel_name'] = 'M01_Sales_subpanel_m01_sales_m03_work_product_1';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/_overrideM01_Sales_subpanel_m01_sales_edoc_email_documents_1.php

//auto-generated file DO NOT EDIT
$layout_defs['M01_Sales']['subpanel_setup']['m01_sales_edoc_email_documents_1']['override_subpanel_name'] = 'M01_Sales_subpanel_m01_sales_edoc_email_documents_1';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Layoutdefs/_overrideM01_Sales_subpanel_m01_sales_m01_sales_1.php

//auto-generated file DO NOT EDIT
$layout_defs['M01_Sales']['subpanel_setup']['m01_sales_m01_sales_1']['override_subpanel_name'] = 'M01_Sales_subpanel_m01_sales_m01_sales_1';

?>
