<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_m01_quote_document_1_M01_Sales.php

// created: 2016-01-25 22:19:12
$dictionary["M01_Sales"]["fields"]["m01_sales_m01_quote_document_1"] = array (
  'name' => 'm01_sales_m01_quote_document_1',
  'type' => 'link',
  'relationship' => 'm01_sales_m01_quote_document_1',
  'source' => 'non-db',
  'module' => 'M01_Quote_Document',
  'bean_name' => 'M01_Quote_Document',
  'vname' => 'LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_m01_quote_document_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_tasks_1_M01_Sales.php

// created: 2016-01-25 23:00:46
$dictionary["M01_Sales"]["fields"]["m01_sales_tasks_1"] = array (
  'name' => 'm01_sales_tasks_1',
  'type' => 'link',
  'relationship' => 'm01_sales_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_M01_SALES_TASKS_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_tasks_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_m03_work_product_1_M01_Sales.php

// created: 2016-01-29 21:10:09
$dictionary["M01_Sales"]["fields"]["m01_sales_m03_work_product_1"] = array (
  'name' => 'm01_sales_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'm01_sales_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_m03_work_product_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/accounts_m01_sales_1_M01_Sales.php

// created: 2016-02-15 18:01:52
$dictionary["M01_Sales"]["fields"]["accounts_m01_sales_1"] = array (
  'name' => 'accounts_m01_sales_1',
  'type' => 'link',
  'relationship' => 'accounts_m01_sales_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE',
  'id_name' => 'accounts_m01_sales_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["M01_Sales"]["fields"]["accounts_m01_sales_1_name"] = array (
  'name' => 'accounts_m01_sales_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_m01_sales_1accounts_ida',
  'link' => 'accounts_m01_sales_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["M01_Sales"]["fields"]["accounts_m01_sales_1accounts_ida"] = array (
  'name' => 'accounts_m01_sales_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE_ID',
  'id_name' => 'accounts_m01_sales_1accounts_ida',
  'link' => 'accounts_m01_sales_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_currency_id.php

 // created: 2016-04-27 05:24:40

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:55
$dictionary['M01_Sales']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_final_followup_date_c.php

 // created: 2016-10-25 03:27:01
$dictionary['M01_Sales']['fields']['final_followup_date_c']['labelValue'] = 'Final Follow-Up Date';
$dictionary['M01_Sales']['fields']['final_followup_date_c']['enforced'] = '';
$dictionary['M01_Sales']['fields']['final_followup_date_c']['dependency'] = '';
$dictionary['M01_Sales']['fields']['final_followup_date_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_final_followup_notes_c.php

 // created: 2016-10-25 03:27:01
$dictionary['M01_Sales']['fields']['final_followup_notes_c']['labelValue'] = 'Final Follow-Up Notes';
$dictionary['M01_Sales']['fields']['final_followup_notes_c']['full_text_search']['enabled'] = true;
$dictionary['M01_Sales']['fields']['final_followup_notes_c']['full_text_search']['searchable'] = false;
$dictionary['M01_Sales']['fields']['final_followup_notes_c']['full_text_search']['boost'] = 1;
$dictionary['M01_Sales']['fields']['final_followup_notes_c']['enforced'] = '';
$dictionary['M01_Sales']['fields']['final_followup_notes_c']['dependency'] = '';


?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_description.php

 // created: 2016-10-25 03:27:01
$dictionary['M01_Sales']['fields']['description']['required'] = true;
$dictionary['M01_Sales']['fields']['description']['audited'] = true;
$dictionary['M01_Sales']['fields']['description']['massupdate'] = false;
$dictionary['M01_Sales']['fields']['description']['comments'] = 'Full text of the note';
$dictionary['M01_Sales']['fields']['description']['duplicate_merge'] = 'enabled';
$dictionary['M01_Sales']['fields']['description']['duplicate_merge_dom_value'] = '1';
$dictionary['M01_Sales']['fields']['description']['merge_filter'] = 'disabled';
$dictionary['M01_Sales']['fields']['description']['unified_search'] = false;
$dictionary['M01_Sales']['fields']['description']['full_text_search']['enabled'] = true;
$dictionary['M01_Sales']['fields']['description']['full_text_search']['searchable'] = false;
$dictionary['M01_Sales']['fields']['description']['full_text_search']['boost'] = 0.5;
$dictionary['M01_Sales']['fields']['description']['calculated'] = false;
$dictionary['M01_Sales']['fields']['description']['rows'] = '6';
$dictionary['M01_Sales']['fields']['description']['cols'] = '80';


?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_initial_followup_date_c.php

 // created: 2016-10-25 03:27:02
$dictionary['M01_Sales']['fields']['initial_followup_date_c']['labelValue'] = 'Initial Follow-Up Date';
$dictionary['M01_Sales']['fields']['initial_followup_date_c']['enforced'] = '';
$dictionary['M01_Sales']['fields']['initial_followup_date_c']['dependency'] = '';
$dictionary['M01_Sales']['fields']['initial_followup_date_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_initial_followup_notes_c.php

 // created: 2016-10-25 03:27:02
$dictionary['M01_Sales']['fields']['initial_followup_notes_c']['labelValue'] = 'Initial Follow-Up Notes';
$dictionary['M01_Sales']['fields']['initial_followup_notes_c']['full_text_search']['enabled'] = true;
$dictionary['M01_Sales']['fields']['initial_followup_notes_c']['full_text_search']['searchable'] = false;
$dictionary['M01_Sales']['fields']['initial_followup_notes_c']['full_text_search']['boost'] = 1;
$dictionary['M01_Sales']['fields']['initial_followup_notes_c']['enforced'] = '';
$dictionary['M01_Sales']['fields']['initial_followup_notes_c']['dependency'] = '';


?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_sales_activity_date_c.php

 // created: 2016-10-25 03:27:03
$dictionary['M01_Sales']['fields']['sales_activity_date_c']['labelValue'] = 'Sales Activity Date';
$dictionary['M01_Sales']['fields']['sales_activity_date_c']['enforced'] = '';
$dictionary['M01_Sales']['fields']['sales_activity_date_c']['dependency'] = '';
$dictionary['M01_Sales']['fields']['sales_activity_date_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_thirty_day_followup_date_c.php

 // created: 2016-10-25 03:27:04
$dictionary['M01_Sales']['fields']['thirty_day_followup_date_c']['labelValue'] = '30 Day Follow-Up Date';
$dictionary['M01_Sales']['fields']['thirty_day_followup_date_c']['enforced'] = '';
$dictionary['M01_Sales']['fields']['thirty_day_followup_date_c']['dependency'] = '';
$dictionary['M01_Sales']['fields']['thirty_day_followup_date_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_project_notes_c.php

 // created: 2017-02-24 19:21:46
$dictionary['M01_Sales']['fields']['project_notes_c']['labelValue']='Study Plan Notes';
$dictionary['M01_Sales']['fields']['project_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['project_notes_c']['enforced']='';
$dictionary['M01_Sales']['fields']['project_notes_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/tm_tradeshow_management_m01_sales_1_M01_Sales.php

// created: 2018-04-23 18:02:37
$dictionary["M01_Sales"]["fields"]["tm_tradeshow_management_m01_sales_1"] = array (
  'name' => 'tm_tradeshow_management_m01_sales_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_m01_sales_1',
  'source' => 'non-db',
  'module' => 'TM_Tradeshow_Management',
  'bean_name' => 'TM_Tradeshow_Management',
  'side' => 'right',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_M01_SALES_TITLE',
  'id_name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
  'link-type' => 'one',
);
$dictionary["M01_Sales"]["fields"]["tm_tradeshow_management_m01_sales_1_name"] = array (
  'name' => 'tm_tradeshow_management_m01_sales_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'save' => true,
  'id_name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
  'link' => 'tm_tradeshow_management_m01_sales_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'name',
);
$dictionary["M01_Sales"]["fields"]["tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida"] = array (
  'name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_M01_SALES_TITLE_ID',
  'id_name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
  'link' => 'tm_tradeshow_management_m01_sales_1',
  'table' => 'tm_tradeshow_management',
  'module' => 'TM_Tradeshow_Management',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_activities_1_calls_M01_Sales.php

// created: 2018-12-10 23:01:47
$dictionary["M01_Sales"]["fields"]["m01_sales_activities_1_calls"] = array (
  'name' => 'm01_sales_activities_1_calls',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_calls',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_CALLS_FROM_CALLS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_activities_1_meetings_M01_Sales.php

// created: 2018-12-10 23:02:13
$dictionary["M01_Sales"]["fields"]["m01_sales_activities_1_meetings"] = array (
  'name' => 'm01_sales_activities_1_meetings',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_meetings',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_activities_1_notes_M01_Sales.php

// created: 2018-12-10 23:02:35
$dictionary["M01_Sales"]["fields"]["m01_sales_activities_1_notes"] = array (
  'name' => 'm01_sales_activities_1_notes',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_notes',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_NOTES_FROM_NOTES_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_activities_1_tasks_M01_Sales.php

// created: 2018-12-10 23:02:55
$dictionary["M01_Sales"]["fields"]["m01_sales_activities_1_tasks"] = array (
  'name' => 'm01_sales_activities_1_tasks',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_tasks',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_TASKS_FROM_TASKS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_activities_1_emails_M01_Sales.php

// created: 2018-12-10 23:03:18
$dictionary["M01_Sales"]["fields"]["m01_sales_activities_1_emails"] = array (
  'name' => 'm01_sales_activities_1_emails',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_emails',
  'source' => 'non-db',
  'module' => 'Emails',
  'bean_name' => 'Email',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_EMAILS_FROM_EMAILS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/contacts_m01_sales_1_M01_Sales.php

// created: 2018-12-13 16:29:00
$dictionary["M01_Sales"]["fields"]["contacts_m01_sales_1"] = array (
  'name' => 'contacts_m01_sales_1',
  'type' => 'link',
  'relationship' => 'contacts_m01_sales_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_M01_SALES_1_FROM_M01_SALES_TITLE',
  'id_name' => 'contacts_m01_sales_1contacts_ida',
  'link-type' => 'one',
);
$dictionary["M01_Sales"]["fields"]["contacts_m01_sales_1_name"] = array (
  'name' => 'contacts_m01_sales_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_M01_SALES_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_m01_sales_1contacts_ida',
  'link' => 'contacts_m01_sales_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["M01_Sales"]["fields"]["contacts_m01_sales_1contacts_ida"] = array (
  'name' => 'contacts_m01_sales_1contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_M01_SALES_1_FROM_M01_SALES_TITLE_ID',
  'id_name' => 'contacts_m01_sales_1contacts_ida',
  'link' => 'contacts_m01_sales_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_quote_revision_date_c.php

 // created: 2019-04-01 12:23:04
$dictionary['M01_Sales']['fields']['quote_revision_date_c']['labelValue']='Quote Revision Date';
$dictionary['M01_Sales']['fields']['quote_revision_date_c']['enforced']='';
$dictionary['M01_Sales']['fields']['quote_revision_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_account_representative_c.php

 // created: 2019-04-02 11:55:35
$dictionary['M01_Sales']['fields']['account_representative_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Sales']['fields']['account_representative_c']['labelValue']='Account Representative';
$dictionary['M01_Sales']['fields']['account_representative_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['account_representative_c']['calculated']='1';
$dictionary['M01_Sales']['fields']['account_representative_c']['formula']='related($accounts_m01_sales_1,"account_representative_c")';
$dictionary['M01_Sales']['fields']['account_representative_c']['enforced']='1';
$dictionary['M01_Sales']['fields']['account_representative_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_company_status_c.php

 // created: 2019-04-02 11:57:02
$dictionary['M01_Sales']['fields']['company_status_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Sales']['fields']['company_status_c']['labelValue']='Company Status';
$dictionary['M01_Sales']['fields']['company_status_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['company_status_c']['calculated']='1';
$dictionary['M01_Sales']['fields']['company_status_c']['formula']='related($accounts_m01_sales_1,"company_status_c")';
$dictionary['M01_Sales']['fields']['company_status_c']['enforced']='1';
$dictionary['M01_Sales']['fields']['company_status_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_account_status_2_c.php

 // created: 2019-04-02 11:59:19
$dictionary['M01_Sales']['fields']['account_status_2_c']['labelValue']='SA Account Status';
$dictionary['M01_Sales']['fields']['account_status_2_c']['dependency']='';
$dictionary['M01_Sales']['fields']['account_status_2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_client_project_id_c.php

 // created: 2019-04-02 12:03:43
$dictionary['M01_Sales']['fields']['client_project_id_c']['labelValue']='Sponsor Study ID';
$dictionary['M01_Sales']['fields']['client_project_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['client_project_id_c']['enforced']='';
$dictionary['M01_Sales']['fields']['client_project_id_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_quote_date_c.php

 // created: 2019-04-02 12:36:24
$dictionary['M01_Sales']['fields']['quote_date_c']['labelValue']='Quote Date (Date Created)';
$dictionary['M01_Sales']['fields']['quote_date_c']['enforced']='';
$dictionary['M01_Sales']['fields']['quote_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_send_1st_invoice_date_c.php

 // created: 2019-04-10 12:16:50
$dictionary['M01_Sales']['fields']['send_1st_invoice_date_c']['labelValue']='Send 1st Invoice Date';
$dictionary['M01_Sales']['fields']['send_1st_invoice_date_c']['enforced']='';
$dictionary['M01_Sales']['fields']['send_1st_invoice_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_completed_1st_invoice_c.php

 // created: 2019-04-10 12:17:29
$dictionary['M01_Sales']['fields']['completed_1st_invoice_c']['labelValue']='Completed 1st Invoice';
$dictionary['M01_Sales']['fields']['completed_1st_invoice_c']['enforced']='';
$dictionary['M01_Sales']['fields']['completed_1st_invoice_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_send_2nd_invoice_date_c.php

 // created: 2019-04-10 12:19:33
$dictionary['M01_Sales']['fields']['send_2nd_invoice_date_c']['labelValue']='Send 2nd Invoice Date';
$dictionary['M01_Sales']['fields']['send_2nd_invoice_date_c']['enforced']='';
$dictionary['M01_Sales']['fields']['send_2nd_invoice_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_completed_2nd_invoice_c.php

 // created: 2019-04-10 12:20:21
$dictionary['M01_Sales']['fields']['completed_2nd_invoice_c']['labelValue']='Completed 2nd Invoice';
$dictionary['M01_Sales']['fields']['completed_2nd_invoice_c']['enforced']='';
$dictionary['M01_Sales']['fields']['completed_2nd_invoice_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_send_3rd_invoice_date_c.php

 // created: 2019-04-10 12:22:14
$dictionary['M01_Sales']['fields']['send_3rd_invoice_date_c']['labelValue']='Send 3rd Invoice Date';
$dictionary['M01_Sales']['fields']['send_3rd_invoice_date_c']['enforced']='';
$dictionary['M01_Sales']['fields']['send_3rd_invoice_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_completed_3rd_invoice_c.php

 // created: 2019-04-10 12:23:05
$dictionary['M01_Sales']['fields']['completed_3rd_invoice_c']['labelValue']='Completed 3rd Invoice';
$dictionary['M01_Sales']['fields']['completed_3rd_invoice_c']['enforced']='';
$dictionary['M01_Sales']['fields']['completed_3rd_invoice_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_send_4th_invoice_date_c.php

 // created: 2019-04-10 12:24:29
$dictionary['M01_Sales']['fields']['send_4th_invoice_date_c']['labelValue']='Send 4th Invoice Date';
$dictionary['M01_Sales']['fields']['send_4th_invoice_date_c']['enforced']='';
$dictionary['M01_Sales']['fields']['send_4th_invoice_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_completed_4th_invoice_c.php

 // created: 2019-04-10 12:25:50
$dictionary['M01_Sales']['fields']['completed_4th_invoice_c']['labelValue']='Completed 4th Invoice';
$dictionary['M01_Sales']['fields']['completed_4th_invoice_c']['enforced']='';
$dictionary['M01_Sales']['fields']['completed_4th_invoice_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_invoice_notes_c.php

 // created: 2019-04-10 12:26:56
$dictionary['M01_Sales']['fields']['invoice_notes_c']['labelValue']='Invoice Notes';
$dictionary['M01_Sales']['fields']['invoice_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['invoice_notes_c']['enforced']='';
$dictionary['M01_Sales']['fields']['invoice_notes_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_bc_study_article_received_c.php

 // created: 2019-04-10 12:27:51
$dictionary['M01_Sales']['fields']['bc_study_article_received_c']['labelValue']='BC Study Article Received';
$dictionary['M01_Sales']['fields']['bc_study_article_received_c']['enforced']='';
$dictionary['M01_Sales']['fields']['bc_study_article_received_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_spa_date_c.php

 // created: 2019-04-10 12:29:19
$dictionary['M01_Sales']['fields']['spa_date_c']['labelValue']='SPA Date';
$dictionary['M01_Sales']['fields']['spa_date_c']['enforced']='';
$dictionary['M01_Sales']['fields']['spa_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_sample_preparation_notes_c.php

 // created: 2019-04-10 12:30:55
$dictionary['M01_Sales']['fields']['sample_preparation_notes_c']['labelValue']='Sample Preparation Notes';
$dictionary['M01_Sales']['fields']['sample_preparation_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['sample_preparation_notes_c']['enforced']='';
$dictionary['M01_Sales']['fields']['sample_preparation_notes_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_approved_to_initiate_testing_c.php

 // created: 2019-04-18 12:46:09
$dictionary['M01_Sales']['fields']['approved_to_initiate_testing_c']['labelValue']='Approved to Initiate Testing';
$dictionary['M01_Sales']['fields']['approved_to_initiate_testing_c']['enforced']='';
$dictionary['M01_Sales']['fields']['approved_to_initiate_testing_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/rr_regulatory_response_m01_sales_1_M01_Sales.php

// created: 2019-07-09 11:54:56
$dictionary["M01_Sales"]["fields"]["rr_regulatory_response_m01_sales_1"] = array (
  'name' => 'rr_regulatory_response_m01_sales_1',
  'type' => 'link',
  'relationship' => 'rr_regulatory_response_m01_sales_1',
  'source' => 'non-db',
  'module' => 'RR_Regulatory_Response',
  'bean_name' => 'RR_Regulatory_Response',
  'vname' => 'LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'id_name' => 'rr_regulatory_response_m01_sales_1rr_regulatory_response_ida',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2019-08-12 11:33:07

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_po_received_c.php

 // created: 2019-08-12 11:37:20
$dictionary['M01_Sales']['fields']['po_received_c']['labelValue']='PO Received';
$dictionary['M01_Sales']['fields']['po_received_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_won_quote_date_c.php

 // created: 2019-08-12 11:40:46
$dictionary['M01_Sales']['fields']['won_quote_date_c']['labelValue']='Won Quote Date';
$dictionary['M01_Sales']['fields']['won_quote_date_c']['enforced']='';
$dictionary['M01_Sales']['fields']['won_quote_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_signed_quote_c.php

 // created: 2019-08-12 11:42:49
$dictionary['M01_Sales']['fields']['signed_quote_c']['labelValue']='Signed Quote';
$dictionary['M01_Sales']['fields']['signed_quote_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_edoc_email_documents_1_M01_Sales.php

// created: 2019-08-27 11:35:41
$dictionary["M01_Sales"]["fields"]["m01_sales_edoc_email_documents_1"] = array (
  'name' => 'm01_sales_edoc_email_documents_1',
  'type' => 'link',
  'relationship' => 'm01_sales_edoc_email_documents_1',
  'source' => 'non-db',
  'module' => 'EDoc_Email_Documents',
  'bean_name' => 'EDoc_Email_Documents',
  'vname' => 'LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_win_probability_c.php

 // created: 2019-08-28 11:50:56
$dictionary['M01_Sales']['fields']['win_probability_c']['labelValue']='Win Probability';
$dictionary['M01_Sales']['fields']['win_probability_c']['dependency']='isInList($sales_activity_quote_req_c,createList("draft for review","Out for Sponsor Review"))';
$dictionary['M01_Sales']['fields']['win_probability_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_anticipated_study_start_time_c.php

 // created: 2019-08-28 11:51:23
$dictionary['M01_Sales']['fields']['anticipated_study_start_time_c']['labelValue']='Anticipated Study Start Timeline';
$dictionary['M01_Sales']['fields']['anticipated_study_start_time_c']['dependency']='isInList($sales_activity_quote_req_c,createList("draft for review","Out for Sponsor Review"))';
$dictionary['M01_Sales']['fields']['anticipated_study_start_time_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_not_approved_to_initiate_tes_c.php

 // created: 2019-09-27 11:50:21
$dictionary['M01_Sales']['fields']['not_approved_to_initiate_tes_c']['labelValue']='Not Approved to Initiate Testing';
$dictionary['M01_Sales']['fields']['not_approved_to_initiate_tes_c']['enforced']='';
$dictionary['M01_Sales']['fields']['not_approved_to_initiate_tes_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_payment_terms_c.php

 // created: 2020-01-13 15:22:42
$dictionary['M01_Sales']['fields']['payment_terms_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Sales']['fields']['payment_terms_c']['labelValue']='Payment Terms';
$dictionary['M01_Sales']['fields']['payment_terms_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['payment_terms_c']['calculated']='1';
$dictionary['M01_Sales']['fields']['payment_terms_c']['formula']='related($accounts_m01_sales_1,"payment_terms_2_c")';
$dictionary['M01_Sales']['fields']['payment_terms_c']['enforced']='1';
$dictionary['M01_Sales']['fields']['payment_terms_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_reason_for_lost_quote_c.php

 // created: 2020-05-21 17:55:43
$dictionary['M01_Sales']['fields']['reason_for_lost_quote_c']['labelValue']='Reason for Lost Quote';
$dictionary['M01_Sales']['fields']['reason_for_lost_quote_c']['dependency']='';
$dictionary['M01_Sales']['fields']['reason_for_lost_quote_c']['visibility_grid']=array (
  'trigger' => 'sales_activity_quote_req_c',
  'values' => 
  array (
    'Lost' => 
    array (
      0 => '',
      1 => 'no response from client',
      2 => 'Chose another lab',
      3 => 'lab proximity to company',
      4 => 'lower quote from competitive lab',
      5 => 'scheduling issue',
      6 => 'experience',
      7 => 'lack of required equipment',
      8 => 'lack of timely response',
      9 => 'report timelines',
      10 => 'capacity',
      11 => 'WL',
    ),
    'Choose SA Status' => 
    array (
    ),
    'Lead' => 
    array (
    ),
    'Lead_Long Term Follow Up' => 
    array (
    ),
    'Study Plan Development' => 
    array (
    ),
    'Required' => 
    array (
    ),
    'Quote_Required_Scheduled' => 
    array (
    ),
    'draft for review' => 
    array (
    ),
    'Send to Sponsor' => 
    array (
    ),
    'Out for Sponsor Review' => 
    array (
    ),
    'Revision Required' => 
    array (
    ),
    'Won_waiting' => 
    array (
    ),
    'Won' => 
    array (
    ),
    'Won Quote Revision Required' => 
    array (
    ),
    'Won Quote Revision for Review' => 
    array (
    ),
    'Won Quote Revision Out for Review' => 
    array (
    ),
    'Invoicing_Initiated' => 
    array (
    ),
    'Invoicing_Complete' => 
    array (
    ),
    'Invoice_Additional_Testing_Performed' => 
    array (
    ),
    'Quote Expired Long Term FollowUp' => 
    array (
    ),
    'Not Performed' => 
    array (
    ),
    'Unknown' => 
    array (
    ),
    'Duplicate' => 
    array (
    ),
    '' => 
    array (
    ),
    'Internal APS Project' => 
    array (
    ),
    'MSA for Review' => 
    array (
    ),
    'NDA for Review' => 
    array (
    ),
    'Pricelist Submitted' => 
    array (
    ),
    'Send MSA to Sponsor' => 
    array (
    ),
    'Send NDA to Sponsor' => 
    array (
    ),
    'Send SOW to Sponsor' => 
    array (
    ),
    'SOW for Review' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_invoice_installment_2_c.php

 // created: 2020-05-21 17:54:15
$dictionary['M01_Sales']['fields']['invoice_installment_2_c']['labelValue']='Invoice Installment 2';
$dictionary['M01_Sales']['fields']['invoice_installment_2_c']['dependency']='';
$dictionary['M01_Sales']['fields']['invoice_installment_2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_invoice_installment_3_c.php

 // created: 2020-05-21 17:54:43
$dictionary['M01_Sales']['fields']['invoice_installment_3_c']['labelValue']='Invoice Installment 3';
$dictionary['M01_Sales']['fields']['invoice_installment_3_c']['dependency']='';
$dictionary['M01_Sales']['fields']['invoice_installment_3_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_invoice_installment_4_c.php

 // created: 2020-05-21 17:55:08
$dictionary['M01_Sales']['fields']['invoice_installment_4_c']['labelValue']='Invoice Installment 4';
$dictionary['M01_Sales']['fields']['invoice_installment_4_c']['dependency']='';
$dictionary['M01_Sales']['fields']['invoice_installment_4_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_functional_area_c.php

 // created: 2020-05-21 17:12:02
$dictionary['M01_Sales']['fields']['functional_area_c']['labelValue']='Functional Area';
$dictionary['M01_Sales']['fields']['functional_area_c']['dependency']='';
$dictionary['M01_Sales']['fields']['functional_area_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_invoice_installment_1_c.php

 // created: 2020-05-21 17:53:26
$dictionary['M01_Sales']['fields']['invoice_installment_1_c']['labelValue']='Invoice Installment 1';
$dictionary['M01_Sales']['fields']['invoice_installment_1_c']['dependency']='';
$dictionary['M01_Sales']['fields']['invoice_installment_1_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_quote_review_c.php

 // created: 2020-05-21 18:11:39
$dictionary['M01_Sales']['fields']['quote_review_c']['labelValue']='Quote Review Priority';
$dictionary['M01_Sales']['fields']['quote_review_c']['dependency']='isInList($sales_activity_quote_req_c,createList("draft for review","NDA for Review"))';
$dictionary['M01_Sales']['fields']['quote_review_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_study_compliance_c.php

 // created: 2020-05-21 17:57:36
$dictionary['M01_Sales']['fields']['study_compliance_c']['labelValue']='Compliance';
$dictionary['M01_Sales']['fields']['study_compliance_c']['dependency']='';
$dictionary['M01_Sales']['fields']['study_compliance_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_sales_activity_quote_req_c.php

 // created: 2020-05-21 17:56:15
$dictionary['M01_Sales']['fields']['sales_activity_quote_req_c']['labelValue']='Sales Activity Status';
$dictionary['M01_Sales']['fields']['sales_activity_quote_req_c']['dependency']='';
$dictionary['M01_Sales']['fields']['sales_activity_quote_req_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_represented_by_third_party_c.php

 // created: 2020-06-11 10:05:53
$dictionary['M01_Sales']['fields']['represented_by_third_party_c']['labelValue']='Represented by Third Party Company';
$dictionary['M01_Sales']['fields']['represented_by_third_party_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_spa_status_c.php

 // created: 2020-07-28 23:41:26
$dictionary['M01_Sales']['fields']['spa_status_c']['labelValue']='SPA Status';
$dictionary['M01_Sales']['fields']['spa_status_c']['dependency']='';
$dictionary['M01_Sales']['fields']['spa_status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_thirty_day_followup_notes_c.php

 // created: 2021-01-04 09:39:11
$dictionary['M01_Sales']['fields']['thirty_day_followup_notes_c']['labelValue']='Second Follow-Up Notes';
$dictionary['M01_Sales']['fields']['thirty_day_followup_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['thirty_day_followup_notes_c']['enforced']='';
$dictionary['M01_Sales']['fields']['thirty_day_followup_notes_c']['dependency']='';
$dictionary['M01_Sales']['fields']['thirty_day_followup_notes_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_quote_amount_c.php

 // created: 2021-01-26 06:41:27
$dictionary['M01_Sales']['fields']['quote_amount_c']['labelValue']='Quote Amount';
$dictionary['M01_Sales']['fields']['quote_amount_c']['enforced']='false';
$dictionary['M01_Sales']['fields']['quote_amount_c']['dependency']='isBefore($date_entered,date("1/1/2021"))';
$dictionary['M01_Sales']['fields']['quote_amount_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['M01_Sales']['fields']['quote_amount_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_m03_work_product_code_id_c.php

 // created: 2021-02-16 08:32:53

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_work_product_code_c.php

 // created: 2021-02-16 08:32:53
$dictionary['M01_Sales']['fields']['work_product_code_c']['labelValue']='Work Product Code';
$dictionary['M01_Sales']['fields']['work_product_code_c']['dependency']='equal($functional_area_c,"ISR")';
$dictionary['M01_Sales']['fields']['work_product_code_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_spa_reconciliation_duration_c.php

 // created: 2021-02-25 05:59:28
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['labelValue']='SPA Reconciliation Duration';
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['calculated']='true';
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['formula']='ifElse(or(equal($spa_date_c,""),equal($bc_study_article_received_c,"")),"",subtract(daysUntil($spa_date_c),daysUntil($bc_study_article_received_c)))';
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['enforced']='true';
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['dependency']='';
$dictionary['M01_Sales']['fields']['spa_reconciliation_duration_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_namsa_quote2_c.php

 // created: 2021-04-01 06:34:12
$dictionary['M01_Sales']['fields']['namsa_quote2_c']['labelValue']='NAMSA Quote';
$dictionary['M01_Sales']['fields']['namsa_quote2_c']['dependency']='';
$dictionary['M01_Sales']['fields']['namsa_quote2_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['namsa_quote2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_wpc_sc01_check_c.php

 // created: 2021-04-29 08:46:32
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['labelValue']='WPC SC01 check';
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['calculated']='true';
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['formula']='countConditional($m01_sales_m03_work_product_1,"wpc_sc01_c",true)';
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['enforced']='true';
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['dependency']='';
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['wpc_sc01_check_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_salesforce_opportunity_name_c.php

 // created: 2021-06-03 10:51:32
$dictionary['M01_Sales']['fields']['salesforce_opportunity_name_c']['labelValue']='Salesforce Opportunity Name';
$dictionary['M01_Sales']['fields']['salesforce_opportunity_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['salesforce_opportunity_name_c']['dependency']='';
$dictionary['M01_Sales']['fields']['salesforce_opportunity_name_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['salesforce_opportunity_name_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_spa_review_to_spa_finalizati_c.php

 // created: 2021-06-17 08:44:04
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['duplicate_merge_dom_value']=0;
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['labelValue']='SPA Review to SPA Finalization';
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['calculated']='true';
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['formula']='ifElse(or(equal($spa_date_c,""),equal($bc_study_article_received_c,"")),"",subtract(daysUntil($spa_date_c),daysUntil($bc_study_article_received_c)))';
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['enforced']='true';
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['dependency']='';
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['spa_review_to_spa_finalizati_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_m01_sales_1_M01_Sales.php

// created: 2021-09-23 08:38:16
$dictionary["M01_Sales"]["fields"]["m01_sales_m01_sales_1"] = array (
  'name' => 'm01_sales_m01_sales_1',
  'type' => 'link',
  'relationship' => 'm01_sales_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_M01_SALES_M01_SALES_1_FROM_M01_SALES_L_TITLE',
  'id_name' => 'm01_sales_m01_sales_1m01_sales_ida',
);
$dictionary["M01_Sales"]["fields"]["m01_sales_m01_sales_1"] = array (
  'name' => 'm01_sales_m01_sales_1',
  'type' => 'link',
  'relationship' => 'm01_sales_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_M01_SALES_M01_SALES_1_FROM_M01_SALES_R_TITLE',
  'id_name' => 'm01_sales_m01_sales_1m01_sales_ida',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_ori_order_request_item_1_M01_Sales.php

// created: 2021-10-19 10:26:08
$dictionary["M01_Sales"]["fields"]["m01_sales_ori_order_request_item_1"] = array (
  'name' => 'm01_sales_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'm01_sales_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'vname' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_ori_order_request_item_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_poi_purchase_order_item_1_M01_Sales.php

// created: 2021-10-19 10:58:37
$dictionary["M01_Sales"]["fields"]["m01_sales_poi_purchase_order_item_1"] = array (
  'name' => 'm01_sales_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'm01_sales_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'POI_Purchase_Order_Item',
  'bean_name' => 'POI_Purchase_Order_Item',
  'vname' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_poi_purchase_order_item_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_grant_submission_sa_2_c.php

 // created: 2021-10-26 09:04:21
$dictionary['M01_Sales']['fields']['grant_submission_sa_2_c']['labelValue']='Grant Submission SA';
$dictionary['M01_Sales']['fields']['grant_submission_sa_2_c']['dependency']='';
$dictionary['M01_Sales']['fields']['grant_submission_sa_2_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['grant_submission_sa_2_c']['readonly_formula']='';
$dictionary['M01_Sales']['fields']['grant_submission_sa_2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_ii_inventory_item_1_M01_Sales.php

// created: 2021-11-09 09:56:04
$dictionary["M01_Sales"]["fields"]["m01_sales_ii_inventory_item_1"] = array (
  'name' => 'm01_sales_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'm01_sales_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_ii_inventory_item_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/m01_sales_im_inventory_management_1_M01_Sales.php

// created: 2021-11-09 10:22:13
$dictionary["M01_Sales"]["fields"]["m01_sales_im_inventory_management_1"] = array (
  'name' => 'm01_sales_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'm01_sales_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'IM_Inventory_Management',
  'bean_name' => 'IM_Inventory_Management',
  'vname' => 'LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_M01_SALES_TITLE',
  'id_name' => 'm01_sales_im_inventory_management_1m01_sales_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_namsa_bde_referred_c.php

 // created: 2021-12-07 10:51:52
$dictionary['M01_Sales']['fields']['namsa_bde_referred_c']['labelValue']='NAMSA BDE Referred';
$dictionary['M01_Sales']['fields']['namsa_bde_referred_c']['dependency']='equal($namsa_quote2_c,"Yes")';
$dictionary['M01_Sales']['fields']['namsa_bde_referred_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['namsa_bde_referred_c']['readonly_formula']='';
$dictionary['M01_Sales']['fields']['namsa_bde_referred_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_action_needed_c.php

 // created: 2021-12-07 10:53:30
$dictionary['M01_Sales']['fields']['action_needed_c']['labelValue']='Action Needed';
$dictionary['M01_Sales']['fields']['action_needed_c']['dependency']='equal($namsa_bde_referred_c,"No")';
$dictionary['M01_Sales']['fields']['action_needed_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['action_needed_c']['readonly_formula']='';
$dictionary['M01_Sales']['fields']['action_needed_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_namsa_quote_id_c.php

 // created: 2021-12-07 10:54:35
$dictionary['M01_Sales']['fields']['namsa_quote_id_c']['labelValue']='NAMSA Quote ID (and SSF)';
$dictionary['M01_Sales']['fields']['namsa_quote_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['namsa_quote_id_c']['enforced']='';
$dictionary['M01_Sales']['fields']['namsa_quote_id_c']['dependency']='equal($namsa_bde_referred_c,"No")';
$dictionary['M01_Sales']['fields']['namsa_quote_id_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['namsa_quote_id_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_spa_needed_c.php

 // created: 2021-12-07 10:55:51
$dictionary['M01_Sales']['fields']['spa_needed_c']['labelValue']='SPA Needed?';
$dictionary['M01_Sales']['fields']['spa_needed_c']['dependency']='equal($namsa_bde_referred_c,"No")';
$dictionary['M01_Sales']['fields']['spa_needed_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['spa_needed_c']['readonly_formula']='';
$dictionary['M01_Sales']['fields']['spa_needed_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_study_article_to_mpls_date_c.php

 // created: 2021-12-07 10:57:53
$dictionary['M01_Sales']['fields']['study_article_to_mpls_date_c']['labelValue']='Study Article Shipped to MPLS Date';
$dictionary['M01_Sales']['fields']['study_article_to_mpls_date_c']['enforced']='';
$dictionary['M01_Sales']['fields']['study_article_to_mpls_date_c']['dependency']='';
$dictionary['M01_Sales']['fields']['study_article_to_mpls_date_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['study_article_to_mpls_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_study_article_status_c.php

 // created: 2021-12-07 12:28:20
$dictionary['M01_Sales']['fields']['study_article_status_c']['labelValue']='Study Article Status';
$dictionary['M01_Sales']['fields']['study_article_status_c']['dependency']='equal($namsa_bde_referred_c,"No")';
$dictionary['M01_Sales']['fields']['study_article_status_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['study_article_status_c']['readonly_formula']='';
$dictionary['M01_Sales']['fields']['study_article_status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_number_needed_text_c.php

 // created: 2021-12-07 12:31:02
$dictionary['M01_Sales']['fields']['number_needed_text_c']['labelValue']='Number of Study Article Needed';
$dictionary['M01_Sales']['fields']['number_needed_text_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['number_needed_text_c']['enforced']='';
$dictionary['M01_Sales']['fields']['number_needed_text_c']['dependency']='equal($namsa_bde_referred_c,"No")';
$dictionary['M01_Sales']['fields']['number_needed_text_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['number_needed_text_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/nsc_namsa_sub_companies_m01_sales_1_M01_Sales.php

// created: 2021-12-07 12:33:38
$dictionary["M01_Sales"]["fields"]["nsc_namsa_sub_companies_m01_sales_1"] = array (
  'name' => 'nsc_namsa_sub_companies_m01_sales_1',
  'type' => 'link',
  'relationship' => 'nsc_namsa_sub_companies_m01_sales_1',
  'source' => 'non-db',
  'module' => 'NSC_NAMSA_Sub_Companies',
  'bean_name' => 'NSC_NAMSA_Sub_Companies',
  'side' => 'right',
  'vname' => 'LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_M01_SALES_TITLE',
  'id_name' => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
  'link-type' => 'one',
);
$dictionary["M01_Sales"]["fields"]["nsc_namsa_sub_companies_m01_sales_1_name"] = array (
  'name' => 'nsc_namsa_sub_companies_m01_sales_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE',
  'save' => true,
  'id_name' => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
  'link' => 'nsc_namsa_sub_companies_m01_sales_1',
  'table' => 'nsc_namsa_sub_companies',
  'module' => 'NSC_NAMSA_Sub_Companies',
  'rname' => 'name',
);
$dictionary["M01_Sales"]["fields"]["nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida"] = array (
  'name' => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_M01_SALES_TITLE_ID',
  'id_name' => 'nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida',
  'link' => 'nsc_namsa_sub_companies_m01_sales_1',
  'table' => 'nsc_namsa_sub_companies',
  'module' => 'NSC_NAMSA_Sub_Companies',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida.php

 // created: 2021-12-07 14:43:28
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['name']='nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['type']='id';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['source']='non-db';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['vname']='LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_FROM_M01_SALES_TITLE_ID';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['id_name']='nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['link']='nsc_namsa_sub_companies_m01_sales_1';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['table']='nsc_namsa_sub_companies';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['module']='NSC_NAMSA_Sub_Companies';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['rname']='id';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['reportable']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['side']='right';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['massupdate']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['duplicate_merge']='disabled';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['hideacl']=true;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['audited']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1nsc_namsa_sub_companies_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_nsc_namsa_sub_companies_m01_sales_1_name.php

 // created: 2021-12-07 14:43:28
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['required']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['audited']=true;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['massupdate']=true;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['hidemassupdate']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['duplicate_merge']='enabled';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['duplicate_merge_dom_value']='1';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['merge_filter']='disabled';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['reportable']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['unified_search']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['calculated']=false;
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['dependency']='equal($namsa_bde_referred_c,"No")';
$dictionary['M01_Sales']['fields']['nsc_namsa_sub_companies_m01_sales_1_name']['vname']='LBL_NSC_NAMSA_SUB_COMPANIES_M01_SALES_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_bde_name_c.php

 // created: 2022-01-18 06:48:07
$dictionary['M01_Sales']['fields']['bde_name_c']['labelValue']='BDE Name';
$dictionary['M01_Sales']['fields']['bde_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['bde_name_c']['enforced']='';
$dictionary['M01_Sales']['fields']['bde_name_c']['dependency']='equal($namsa_bde_referred_c,"Yes")';
$dictionary['M01_Sales']['fields']['bde_name_c']['required_formula']='equal($namsa_bde_referred_c,"Yes")';
$dictionary['M01_Sales']['fields']['bde_name_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_study_article_storage_locati_c.php

 // created: 2022-01-20 08:06:11
$dictionary['M01_Sales']['fields']['study_article_storage_locati_c']['labelValue']='Study Article Storage Location';
$dictionary['M01_Sales']['fields']['study_article_storage_locati_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M01_Sales']['fields']['study_article_storage_locati_c']['enforced']='';
$dictionary['M01_Sales']['fields']['study_article_storage_locati_c']['dependency']='';
$dictionary['M01_Sales']['fields']['study_article_storage_locati_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['study_article_storage_locati_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_user_id_c.php

 // created: 2022-02-08 08:16:12

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_lab_specialist_c.php

 // created: 2022-02-08 08:16:12
$dictionary['M01_Sales']['fields']['lab_specialist_c']['labelValue']='Lab Specialist';
$dictionary['M01_Sales']['fields']['lab_specialist_c']['dependency']='';
$dictionary['M01_Sales']['fields']['lab_specialist_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['lab_specialist_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_aps_analytical_deliverables_c.php

 // created: 2022-02-22 07:19:46
$dictionary['M01_Sales']['fields']['aps_analytical_deliverables_c']['labelValue']='Analytical Deliverables';
$dictionary['M01_Sales']['fields']['aps_analytical_deliverables_c']['dependency']='isInList($functional_area_c,createList("ISR","Pharmacology","Toxicology"))';
$dictionary['M01_Sales']['fields']['aps_analytical_deliverables_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['aps_analytical_deliverables_c']['readonly_formula']='';
$dictionary['M01_Sales']['fields']['aps_analytical_deliverables_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_name.php

 // created: 2022-02-22 07:20:49
$dictionary['M01_Sales']['fields']['name']['readonly']=false;
$dictionary['M01_Sales']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['M01_Sales']['fields']['name']['len']='255';
$dictionary['M01_Sales']['fields']['name']['audited']=true;
$dictionary['M01_Sales']['fields']['name']['massupdate']=false;
$dictionary['M01_Sales']['fields']['name']['unified_search']=false;
$dictionary['M01_Sales']['fields']['name']['calculated']=false;
$dictionary['M01_Sales']['fields']['name']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_aps_histopathology_deliv_c.php

 // created: 2022-03-01 07:45:39
$dictionary['M01_Sales']['fields']['aps_histopathology_deliv_c']['labelValue']='Histopathology Deliverables';
$dictionary['M01_Sales']['fields']['aps_histopathology_deliv_c']['dependency']='isInList($functional_area_c,createList("ISR","Bioskills","Biocompatibility","Pharmacology","Toxicology"))';
$dictionary['M01_Sales']['fields']['aps_histopathology_deliv_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['aps_histopathology_deliv_c']['readonly_formula']='';
$dictionary['M01_Sales']['fields']['aps_histopathology_deliv_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales/Ext/Vardefs/sugarfield_bc_study_article_status_c.php

 // created: 2022-04-05 17:07:23
$dictionary['M01_Sales']['fields']['bc_study_article_status_c']['labelValue']='BC Study Article Status';
$dictionary['M01_Sales']['fields']['bc_study_article_status_c']['dependency']='';
$dictionary['M01_Sales']['fields']['bc_study_article_status_c']['required_formula']='';
$dictionary['M01_Sales']['fields']['bc_study_article_status_c']['readonly_formula']='';
$dictionary['M01_Sales']['fields']['bc_study_article_status_c']['visibility_grid']='';

 
?>
