<?php
// WARNING: The contents of this file are auto-generated.


// created: 2018-12-10 23:01:47
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CALLS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_activities_1_calls',
  ),
);

// created: 2018-12-10 23:03:18
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_EMAILS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_activities_1_emails',
  ),
);

// created: 2018-12-10 23:02:13
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_MEETINGS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_activities_1_meetings',
  ),
);

// created: 2018-12-10 23:02:35
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_NOTES_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_activities_1_notes',
  ),
);

// created: 2018-12-10 23:02:55
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TASKS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_activities_1_tasks',
  ),
);

// created: 2019-08-27 11:35:41
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_edoc_email_documents_1',
  ),
);

// created: 2021-11-09 09:56:04
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_ii_inventory_item_1',
  ),
);

// created: 2021-11-09 10:22:13
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M01_SALES_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_im_inventory_management_1',
  ),
);

// created: 2016-01-25 22:19:12
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M01_SALES_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_m01_quote_document_1',
  ),
);

// created: 2021-09-23 08:38:16
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M01_SALES_M01_SALES_1_FROM_M01_SALES_R_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_m01_sales_1',
  ),
);

// created: 2016-01-29 21:10:09
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_m03_work_product_1',
  ),
);

// created: 2021-10-19 10:26:07
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M01_SALES_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_ori_order_request_item_1',
  ),
);

// created: 2021-10-19 10:58:37
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M01_SALES_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_poi_purchase_order_item_1',
  ),
);

// created: 2016-01-25 23:00:46
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M01_SALES_TASKS_1_FROM_TASKS_TITLE',
  'context' => 
  array (
    'link' => 'm01_sales_tasks_1',
  ),
);

// created: 2019-07-09 11:54:56
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_RR_REGULATORY_RESPONSE_M01_SALES_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'context' => 
  array (
    'link' => 'rr_regulatory_response_m01_sales_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm01_sales_activities_1_emails',
  'view' => 'subpanel-for-m01_sales-m01_sales_activities_1_emails',
);


//auto-generated file DO NOT EDIT
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm01_sales_edoc_email_documents_1',
  'view' => 'subpanel-for-m01_sales-m01_sales_edoc_email_documents_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm01_sales_m01_quote_document_1',
  'view' => 'subpanel-for-m01_sales-m01_sales_m01_quote_document_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm01_sales_m01_sales_1',
  'view' => 'subpanel-for-m01_sales-m01_sales_m01_sales_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm01_sales_m03_work_product_1',
  'view' => 'subpanel-for-m01_sales-m01_sales_m03_work_product_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M01_Sales']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm01_sales_tasks_1',
  'view' => 'subpanel-for-m01_sales-m01_sales_tasks_1',
);
