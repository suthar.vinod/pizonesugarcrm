<?php
// created: 2021-09-23 08:46:50
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'contacts_m01_sales_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CONTACTS_M01_SALES_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_M01_SALES_1CONTACTS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Contacts',
    'target_record_key' => 'contacts_m01_sales_1contacts_ida',
  ),
  'sales_activity_quote_req_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_SALES_ACTIVITY_QUOTE_REQ',
    'width' => 10,
  ),
);