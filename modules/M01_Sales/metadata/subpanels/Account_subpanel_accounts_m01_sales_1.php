<?php
// created: 2019-01-07 21:06:33
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'functional_area_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_FUNCTIONAL_AREA',
    'width' => 10,
  ),
  'contacts_m01_sales_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CONTACTS_M01_SALES_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_M01_SALES_1CONTACTS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Contacts',
    'target_record_key' => 'contacts_m01_sales_1contacts_ida',
  ),
  'sales_activity_quote_req_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_SALES_ACTIVITY_QUOTE_REQ',
    'width' => 10,
  ),
  'won_quote_date_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_WON_QUOTE_DATE',
    'width' => 10,
    'default' => true,
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
);