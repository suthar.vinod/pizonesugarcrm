<?php
$module_name = 'M01_Sales';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'edit' => 
      array (
        'templateMeta' => 
        array (
          'maxColumns' => '1',
          'widths' => 
          array (
            0 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
            1 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
          ),
          'useTabs' => false,
        ),
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_PANEL_DEFAULT',
            'columns' => '1',
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 'name',
              1 => 'assigned_user_name',
              2 => 
              array (
                'name' => 'accounts_m01_sales_1_name',
                'label' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_ACCOUNTS_TITLE',
              ),
              3 => 
              array (
                'name' => 'functional_area_c',
                'label' => 'LBL_FUNCTIONAL_AREA',
              ),
              4 => 
              array (
                'name' => 'sales_activity_quote_req_c',
                'label' => 'LBL_SALES_ACTIVITY_QUOTE_REQ',
              ),
              5 => 
              array (
                'name' => 'description',
                'comment' => 'Full text of the note',
                'label' => 'LBL_DESCRIPTION',
              ),
              6 => 'team_name',
            ),
          ),
        ),
      ),
    ),
  ),
);
