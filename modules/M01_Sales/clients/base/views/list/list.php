<?php
$module_name = 'M01_Sales';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'readonly' => true,
                'required' => false,
                'default' => true,
                'enabled' => true,
                'link' => true,
                'width' => 'small',
              ),
              1 => 
              array (
                'name' => 'sales_activity_quote_req_c',
                'label' => 'LBL_SALES_ACTIVITY_QUOTE_REQ',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'accounts_m01_sales_1_name',
                'label' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_ACCOUNTS_TITLE',
                'enabled' => true,
                'id' => 'ACCOUNTS_M01_SALES_1ACCOUNTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'contacts_m01_sales_1_name',
                'label' => 'LBL_CONTACTS_M01_SALES_1_FROM_CONTACTS_TITLE',
                'enabled' => true,
                'id' => 'CONTACTS_M01_SALES_1CONTACTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'company_status_c',
                'label' => 'LBL_COMPANY_STATUS',
                'enabled' => true,
                'default' => true,
                'width' => 'small',
              ),
              5 => 
              array (
                'name' => 'study_compliance_c',
                'label' => 'LBL_STUDY_COMPLIANCE',
                'enabled' => true,
                'default' => true,
                'width' => 'small',
              ),
              6 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
                'width' => 'small',
              ),
              7 => 
              array (
                'name' => 'spa_status_c',
                'label' => 'LBL_SPA_STATUS',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
