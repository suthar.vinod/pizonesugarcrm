({
    extendsFrom: 'CreateView',
    initialize: function (options) {

        this._super('initialize', [options]);        
        this.model.on("change:namsa_bde_referred_c", this.function_study_article_status, this);        
    },

    /*1769 Custom Default Value*/
    function_study_article_status: function () {
        var namsa_bde_referred = this.model.get('namsa_bde_referred_c');
        console.log('namsa_bde_referred ',namsa_bde_referred);
        if (namsa_bde_referred == 'No') {
            this.model.set("study_article_status_c", 'To Ship From NW');			
        }
		
    },
})