<?php
$viewdefs['M01_Sales'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 
              array (
                'name' => 'name',
                'readonly' => false,
                'required' => false,
              ),
            ),
          ),
          1 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'accounts_m01_sales_1_name',
                'label' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_ACCOUNTS_TITLE',
              ),
              1 => 
              array (
                'name' => 'contacts_m01_sales_1_name',
              ),
              2 => 
              array (
                'name' => 'assigned_user_name',
              ),
              3 => 
              array (
                'name' => 'account_representative_c',
                'label' => 'LBL_ACCOUNT_REPRESENTATIVE',
              ),
              4 => 
              array (
                'name' => 'company_status_c',
                'label' => 'LBL_COMPANY_STATUS',
              ),
              5 => 
              array (
                'name' => 'payment_terms_c',
                'label' => 'LBL_PAYMENT_TERMS',
              ),
              6 => 
              array (
                'name' => 'account_status_2_c',
                'label' => 'LBL_ACCOUNT_STATUS_2',
              ),
              7 => 
              array (
                'name' => 'represented_by_third_party_c',
                'label' => 'LBL_REPRESENTED_BY_THIRD_PARTY',
              ),
              8 => 
              array (
                'name' => 'created_by_name',
                'readonly' => true,
                'label' => 'LBL_CREATED',
              ),
              9 => 
              array (
                'name' => 'sales_activity_date_c',
                'label' => 'LBL_SALES_ACTIVITY_DATE',
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'study_compliance_c',
                'label' => 'LBL_STUDY_COMPLIANCE',
              ),
              2 => 
              array (
                'name' => 'functional_area_c',
                'label' => 'LBL_FUNCTIONAL_AREA',
              ),
              3 => 
              array (
                'name' => 'client_project_id_c',
                'label' => 'LBL_CLIENT_PROJECT_ID',
              ),
              4 => 
              array (
              ),
              5 => 
              array (
                'name' => 'tag',
                'span' => 12,
              ),
              6 => 
              array (
                'name' => 'project_notes_c',
                'studio' => 'visible',
                'label' => 'LBL_PROJECT_NOTES',
              ),
              7 => 
              array (
                'name' => 'team_name',
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'sales_activity_quote_req_c',
                'label' => 'LBL_SALES_ACTIVITY_QUOTE_REQ',
              ),
              1 => 
              array (
              ),
              2 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'quote_amount_c',
                'label' => 'LBL_QUOTE_AMOUNT',
              ),
              3 => 
              array (
                'name' => 'quote_review_c',
                'label' => 'LBL_QUOTE_REVIEW',
              ),
              4 => 
              array (
                'name' => 'signed_quote_c',
                'label' => 'LBL_SIGNED_QUOTE',
              ),
              5 => 
              array (
                'name' => 'po_received_c',
                'label' => 'LBL_PO_RECEIVED',
              ),
              6 => 
              array (
                'name' => 'grant_submission_sa_2_c',
                'label' => 'LBL_GRANT_SUBMISSION_SA_2',
              ),
              7 => 
              array (
                'name' => 'reason_for_lost_quote_c',
                'label' => 'LBL_REASON_FOR_LOST_QUOTE',
              ),
              8 => 
              array (
                'name' => 'win_probability_c',
                'label' => 'LBL_WIN_PROBABILITY',
              ),
              9 => 
              array (
                'name' => 'anticipated_study_start_time_c',
                'label' => 'LBL_ANTICIPATED_STUDY_START_TIMELINE',
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL10',
            'label' => 'LBL_RECORDVIEW_PANEL10',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'initial_followup_date_c',
                'label' => 'LBL_INITIAL_FOLLOWUP_DATE',
              ),
              1 => 
              array (
              ),
              2 => 
              array (
                'name' => 'initial_followup_notes_c',
                'studio' => 'visible',
                'label' => 'LBL_INITIAL_FOLLOWUP_NOTES',
                'span' => 12,
              ),
              3 => 
              array (
                'name' => 'thirty_day_followup_date_c',
                'label' => 'LBL_THIRTY_DAY_FOLLOWUP_DATE',
              ),
              4 => 
              array (
              ),
              5 => 
              array (
                'name' => 'thirty_day_followup_notes_c',
                'studio' => 'visible',
                'label' => 'LBL_THIRTY_DAY_FOLLOWUP_NOTES',
                'span' => 12,
              ),
              6 => 
              array (
                'name' => 'final_followup_date_c',
                'label' => 'LBL_FINAL_FOLLOWUP_DATE',
              ),
              7 => 
              array (
              ),
              8 => 
              array (
                'name' => 'final_followup_notes_c',
                'studio' => 'visible',
                'label' => 'LBL_FINAL_FOLLOWUP_NOTES',
                'span' => 12,
              ),
            ),
          ),
          5 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL4',
            'label' => 'LBL_RECORDVIEW_PANEL4',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'invoice_installment_1_c',
                'label' => 'LBL_INVOICE_INSTALLMENT_1',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'send_1st_invoice_date_c',
                'label' => 'LBL_SEND_1ST_INVOICE_DATE',
              ),
              2 => 
              array (
                'name' => 'completed_1st_invoice_c',
                'label' => 'LBL_COMPLETED_1ST_INVOICE',
              ),
              3 => 
              array (
                'name' => 'invoice_installment_2_c',
                'label' => 'LBL_INVOICE_INSTALLMENT_2',
                'span' => 12,
              ),
              4 => 
              array (
                'name' => 'send_2nd_invoice_date_c',
                'label' => 'LBL_SEND_2ND_INVOICE_DATE',
              ),
              5 => 
              array (
                'name' => 'completed_2nd_invoice_c',
                'label' => 'LBL_COMPLETED_2ND_INVOICE',
              ),
              6 => 
              array (
                'name' => 'invoice_installment_3_c',
                'label' => 'LBL_INVOICE_INSTALLMENT_3',
                'span' => 12,
              ),
              7 => 
              array (
                'name' => 'send_3rd_invoice_date_c',
                'label' => 'LBL_SEND_3RD_INVOICE_DATE',
              ),
              8 => 
              array (
                'name' => 'completed_3rd_invoice_c',
                'label' => 'LBL_COMPLETED_3RD_INVOICE',
              ),
              9 => 
              array (
                'name' => 'invoice_installment_4_c',
                'label' => 'LBL_INVOICE_INSTALLMENT_4',
                'span' => 12,
              ),
              10 => 
              array (
                'name' => 'send_4th_invoice_date_c',
                'label' => 'LBL_SEND_4TH_INVOICE_DATE',
              ),
              11 => 
              array (
                'name' => 'completed_4th_invoice_c',
                'label' => 'LBL_COMPLETED_4TH_INVOICE',
              ),
              12 => 
              array (
                'name' => 'invoice_notes_c',
                'label' => 'LBL_INVOICE_NOTES',
                'span' => 12,
              ),
            ),
          ),
          6 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL7',
            'label' => 'LBL_RECORDVIEW_PANEL7',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'bc_study_article_received_c',
                'label' => 'LBL_BC_STUDY_ARTICLE_RECEIVED',
              ),
              1 => 
              array (
                'name' => 'bc_study_article_status_c',
                'label' => 'LBL_BC_STUDY_ARTICLE_STATUS',
              ),
              2 => 
              array (
                'name' => 'spa_date_c',
                'label' => 'LBL_SPA_DATE',
              ),
              3 => 
              array (
                'name' => 'spa_status_c',
                'label' => 'LBL_SPA_STATUS',
              ),
              4 => 
              array (
                'name' => 'sample_preparation_notes_c',
                'label' => 'LBL_SAMPLE_PREPARATION_NOTES',
                'span' => 12,
              ),
              5 => 
              array (
                'name' => 'approved_to_initiate_testing_c',
                'label' => 'LBL_APPROVED_TO_INITIATE_TESTING',
              ),
              6 => 
              array (
                'name' => 'not_approved_to_initiate_tes_c',
                'label' => 'LBL_NOT_APPROVED_TO_INITIATE_TES',
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);
