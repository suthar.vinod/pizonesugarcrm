<?php
// created: 2019-01-07 21:06:34
$viewdefs['M01_Sales']['base']['view']['subpanel-for-accounts-accounts_m01_sales_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'functional_area_c',
          'label' => 'LBL_FUNCTIONAL_AREA',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'contacts_m01_sales_1_name',
          'label' => 'LBL_CONTACTS_M01_SALES_1_FROM_CONTACTS_TITLE',
          'enabled' => true,
          'id' => 'CONTACTS_M01_SALES_1CONTACTS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sales_activity_quote_req_c',
          'label' => 'LBL_SALES_ACTIVITY_QUOTE_REQ',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'won_quote_date_c',
          'label' => 'LBL_WON_QUOTE_DATE',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);