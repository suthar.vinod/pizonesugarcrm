<?php
// created: 2022-02-08 08:24:18
$viewdefs['M01_Sales']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'action_needed_c' => 
    array (
    ),
    'aps_analytical_deliverables_c' => 
    array (
    ),
    'aps_histopathology_deliv_c' => 
    array (
    ),
    'name' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'bc_study_article_received_c' => 
    array (
    ),
    'bde_name_c' => 
    array (
    ),
    'accounts_m01_sales_1_name' => 
    array (
    ),
    'contacts_m01_sales_1_name' => 
    array (
    ),
    'functional_area_c' => 
    array (
    ),
    'grant_submission_sa_2_c' => 
    array (
    ),
    'lab_specialist_c' => 
    array (
    ),
    'namsa_bde_referred_c' => 
    array (
    ),
    'namsa_quote2_c' => 
    array (
    ),
    'namsa_quote_id_c' => 
    array (
    ),
    'quote_date_c' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'sales_activity_date_c' => 
    array (
    ),
    'description' => 
    array (
    ),
    'sales_activity_quote_req_c' => 
    array (
    ),
    'spa_date_c' => 
    array (
    ),
    'spa_needed_c' => 
    array (
    ),
    'spa_review_to_spa_finalizati_c' => 
    array (
    ),
    'spa_status_c' => 
    array (
    ),
    'client_project_id_c' => 
    array (
    ),
    'study_article_to_mpls_date_c' => 
    array (
    ),
    'study_article_storage_locati_c' => 
    array (
    ),
    'work_product_code_c' => 
    array (
    ),
  ),
);