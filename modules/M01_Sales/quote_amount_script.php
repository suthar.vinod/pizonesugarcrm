<?php

global $db;

$entered_date1 = '2021-01-03';
$entered_date2 = date("Y-m-d 23:59:59"); //current date
echo "<br>===>" . $query = "SELECT m01_sales.id,m01_sales.name, m01_sales_cstm.quote_amount_c,m01_sales.date_entered,m01_sales.date_modified FROM `m01_sales` 
                                INNER JOIN m01_sales_cstm ON m01_sales.id = m01_sales_cstm.id_c 
                                WHERE deleted = 0 AND m01_sales_cstm.quote_amount_c = '0.000000' AND m01_sales.date_modified > '" . $entered_date1 . "' AND m01_sales.date_modified < '" . $entered_date2 . "' 
                                ORDER BY m01_sales.date_modified DESC";
$queryResult = $db->query($query);

$srno = 1;

$tBody = "<table width='1250px' cellspacing='6' cellpadding='5' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th bgcolor='#b3d1ff' align='left'> ID </th>
			<th bgcolor='#b3d1ff' align='left'> Name </th>
			<th bgcolor='#b3d1ff' align='left'>Quote Amount New</th>
			<th bgcolor='#b3d1ff' align='left'>Quote Amount Old</th>
                        <th bgcolor='#b3d1ff' align='left'>Date Created</th>
                        <th bgcolor='#b3d1ff' align='left'>Date Modified</th>
                        </tr>";


while ($fetchResult = $db->fetchByAssoc($queryResult)) {
    $id = $fetchResult['id'];
    $name = $fetchResult['name'];
    $quote_amount = $fetchResult['quote_amount_c'];
    $dateCreated = $fetchResult['date_entered'];
    $dateModified = $fetchResult['date_modified'];

    $queryAudit = "SELECT * FROM `m01_sales_audit` WHERE field_name = 'quote_amount_c' AND parent_id = '$id' ORDER BY date_created DESC limit 1";
    $queryAuditResult = $db->query($queryAudit);
    $fetchAuditResult = $db->fetchByAssoc($queryAuditResult);
    $fetchAuditResult['before_value_string'];
    if ($queryAuditResult->num_rows > 0) {
        if ($srno % 2 == 0)
            $stylr = "style='background-color: #e6e6e6;'";
        else
            $stylr = "style='background-color: #f3f3f3;'";

        $tBody .= "<tr><td " . $stylr . ">" . $srno++ . "</td>
                    <td " . $stylr . ">" . $id . "</td> 
                    <td " . $stylr . ">" . $name . "</td>
                    <td " . $stylr . ">" . $quote_amount . "</td>
                    <td " . $stylr . ">" . $fetchAuditResult['before_value_string'] . "</td>
                        <td " . $stylr . ">" . $dateCreated . "</td>
                            <td " . $stylr . ">" . $dateModified . "</td>
                    </tr>";
    }
}

echo "<br><br>" . $tBody .="</table>";
?>