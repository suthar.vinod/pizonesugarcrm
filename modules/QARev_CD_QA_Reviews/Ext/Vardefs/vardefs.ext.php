<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/QARev_CD_QA_Reviews/Ext/Vardefs/qarev_cd_qa_reviews_meetings_1_QARev_CD_QA_Reviews.php

// created: 2021-02-11 07:49:03
$dictionary["QARev_CD_QA_Reviews"]["fields"]["qarev_cd_qa_reviews_meetings_1"] = array (
  'name' => 'qarev_cd_qa_reviews_meetings_1',
  'type' => 'link',
  'relationship' => 'qarev_cd_qa_reviews_meetings_1',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_QAREV_CD_QA_REVIEWS_TITLE',
  'id_name' => 'qarev_cd_qa_reviews_meetings_1qarev_cd_qa_reviews_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/QARev_CD_QA_Reviews/Ext/Vardefs/qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1_QARev_CD_QA_Reviews.php

// created: 2021-02-11 07:50:25
$dictionary["QARev_CD_QA_Reviews"]["fields"]["qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1"] = array (
  'name' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1',
  'type' => 'link',
  'relationship' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1',
  'source' => 'non-db',
  'module' => 'QADoc_CD_QA_Rev_Docs',
  'bean_name' => 'QADoc_CD_QA_Rev_Docs',
  'vname' => 'LBL_QAREV_CD_QA_REVIEWS_QADOC_CD_QA_REV_DOCS_1_FROM_QAREV_CD_QA_REVIEWS_TITLE',
  'id_name' => 'qarev_cd_q5440reviews_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
