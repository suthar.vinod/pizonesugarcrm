<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-02-11 07:49:03
$viewdefs['QARev_CD_QA_Reviews']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_MEETINGS_TITLE',
  'context' => 
  array (
    'link' => 'qarev_cd_qa_reviews_meetings_1',
  ),
);

// created: 2021-02-11 07:50:26
$viewdefs['QARev_CD_QA_Reviews']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_QAREV_CD_QA_REVIEWS_QADOC_CD_QA_REV_DOCS_1_FROM_QADOC_CD_QA_REV_DOCS_TITLE',
  'context' => 
  array (
    'link' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1',
  ),
);