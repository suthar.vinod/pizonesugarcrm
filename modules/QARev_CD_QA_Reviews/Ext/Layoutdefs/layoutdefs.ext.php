<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/QARev_CD_QA_Reviews/Ext/Layoutdefs/qarev_cd_qa_reviews_meetings_1_QARev_CD_QA_Reviews.php

 // created: 2021-02-11 07:49:03
$layout_defs["QARev_CD_QA_Reviews"]["subpanel_setup"]['qarev_cd_qa_reviews_meetings_1'] = array (
  'order' => 100,
  'module' => 'Meetings',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_MEETINGS_TITLE',
  'get_subpanel_data' => 'qarev_cd_qa_reviews_meetings_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/QARev_CD_QA_Reviews/Ext/Layoutdefs/qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1_QARev_CD_QA_Reviews.php

 // created: 2021-02-11 07:50:25
$layout_defs["QARev_CD_QA_Reviews"]["subpanel_setup"]['qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1'] = array (
  'order' => 100,
  'module' => 'QADoc_CD_QA_Rev_Docs',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_QAREV_CD_QA_REVIEWS_QADOC_CD_QA_REV_DOCS_1_FROM_QADOC_CD_QA_REV_DOCS_TITLE',
  'get_subpanel_data' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/QARev_CD_QA_Reviews/Ext/Layoutdefs/_overrideQARev_CD_QA_Reviews_subpanel_qarev_cd_qa_reviews_meetings_1.php

//auto-generated file DO NOT EDIT
$layout_defs['QARev_CD_QA_Reviews']['subpanel_setup']['qarev_cd_qa_reviews_meetings_1']['override_subpanel_name'] = 'QARev_CD_QA_Reviews_subpanel_qarev_cd_qa_reviews_meetings_1';

?>
<?php
// Merged from custom/Extension/modules/QARev_CD_QA_Reviews/Ext/Layoutdefs/_overrideQARev_CD_QA_Reviews_subpanel_qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1.php

//auto-generated file DO NOT EDIT
$layout_defs['QARev_CD_QA_Reviews']['subpanel_setup']['qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1']['override_subpanel_name'] = 'QARev_CD_QA_Reviews_subpanel_qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1';

?>
