<?php

class animalRoomUpdateClass {
    /**
     * 
      If a room transfer record is unlinked or deleted the Test System Room field should update to the next relevant linked Room Transfer record
     *
     */
    public function updateRoom($bean, $event, $arguments) {
        if ($arguments['isUpdate'] == 1 && isset($arguments['dataChanges']) && (isset($arguments['dataChanges']['transfer_date_c']) || isset($arguments['dataChanges']['rms_room_id_c'])) && !empty($bean->anml_animals_rt_room_transfer_1anml_animals_ida)) {
            $conn = $GLOBALS['db']->getConnection();
            $sql = "SELECT t.id, tc.rms_room_id_c, tc.transfer_date_c 
            FROM rt_room_transfer t 
            INNER JOIN rt_room_transfer_cstm tc ON t.id = tc.id_c AND t.deleted = 0 
            INNER JOIN anml_animals_rt_room_transfer_1_c tr ON t.id = tr.anml_animals_rt_room_transfer_1rt_room_transfer_idb AND tr.deleted = 0 AND tr.anml_animals_rt_room_transfer_1anml_animals_ida = '{$bean->anml_animals_rt_room_transfer_1anml_animals_ida}' ORDER BY tc.transfer_date_c DESC, date_entered DESC LIMIT 1";
            $rslt = $conn->executeQuery($sql);
            $row = $rslt->fetch();
            $rms_room_id_c = $row['rms_room_id_c'];

            $conn->executeQuery("UPDATE anml_animals_cstm SET rms_room_id_c = '{$rms_room_id_c}' WHERE id_c = '{$bean->anml_animals_rt_room_transfer_1anml_animals_ida}'");
        }
    }
}
