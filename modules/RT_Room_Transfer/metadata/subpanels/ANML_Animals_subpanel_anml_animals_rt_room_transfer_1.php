<?php
// created: 2019-06-19 11:46:30
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'reason_for_transfer_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_REASON_FOR_TRANSFER',
    'width' => 10,
  ),
  'transfer_date_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_TRANSFER_DATE',
    'width' => 10,
    'default' => true,
  ),
  'room_c' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_ROOM',
    'id' => 'RMS_ROOM_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'RMS_Room',
    'target_record_key' => 'rms_room_id_c',
  ),
);