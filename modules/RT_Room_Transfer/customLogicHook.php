<?php

class customLogicHook {

    function generateAutomatedName($bean, $event, $arguments) {
        global $db;
        $isIdExist = $bean->fetched_row['id'];

        if (!$isIdExist) {

            $queryForGetAnimalName = "SELECT anml_animals.name FROM anml_animals "
                    . "LEFT JOIN anml_animals_rt_room_transfer_1_c ON anml_animals.id = anml_animals_rt_room_transfer_1_c.anml_animals_rt_room_transfer_1anml_animals_ida "
                    . "LEFT JOIN rt_room_transfer ON anml_animals_rt_room_transfer_1_c.anml_animals_rt_room_transfer_1rt_room_transfer_idb = rt_room_transfer.id "
                    . "WHERE rt_room_transfer.deleted = 0 and rt_room_transfer.id = '" . $bean->id . "'";

            $animalName = $db->query($queryForGetAnimalName);
            $result1 = $db->fetchByAssoc($animalName);
            $nameAnimal = $result1['name'];

            $name = $nameAnimal . " RT";
            
            $queryForGetLatestName = "SELECT rt_room_transfer.name FROM rt_room_transfer "
                    . "LEFT JOIN anml_animals_rt_room_transfer_1_c ON rt_room_transfer.id = anml_animals_rt_room_transfer_1_c.anml_animals_rt_room_transfer_1rt_room_transfer_idb "
                    . "LEFT JOIN anml_animals ON anml_animals_rt_room_transfer_1_c.anml_animals_rt_room_transfer_1anml_animals_ida = anml_animals.id "
                    . "WHERE anml_animals.deleted = 0 AND anml_animals.name = '".$nameAnimal."' AND rt_room_transfer.deleted = 0  AND rt_room_transfer.name is not null AND rt_room_transfer.name <> '' "
                    . "ORDER BY rt_room_transfer.date_entered DESC "
                    . "LIMIT 0,1";
           
            $_seqNo = $db->query($queryForGetLatestName);
            $resultForSeqNo = $db->fetchByAssoc($_seqNo);
            $nameRT = $resultForSeqNo['name'];
            
            if (!empty($nameRT)) {
                $nameDivisionArray = explode('-', $nameRT);
                $number = $nameDivisionArray[1];
                $number = $number + 1;
            } else {
                $number = 01;
            }
            $number = sprintf('%02d', $number);
            $newName = $name . '-' . $number;
        } else {

            $queryForGetAnimalName = "SELECT anml_animals.name FROM anml_animals "
                    . "LEFT JOIN anml_animals_rt_room_transfer_1_c ON anml_animals.id = anml_animals_rt_room_transfer_1_c.anml_animals_rt_room_transfer_1anml_animals_ida "
                    . "LEFT JOIN rt_room_transfer ON anml_animals_rt_room_transfer_1_c.anml_animals_rt_room_transfer_1rt_room_transfer_idb = rt_room_transfer.id "
                    . "WHERE rt_room_transfer.deleted = 0 and rt_room_transfer.id = '" . $bean->id . "'";

            $animalName = $db->query($queryForGetAnimalName);
            $result1 = $db->fetchByAssoc($animalName);
            $nameAnimal = $result1['name'];

            $nameDivisionArray = explode(' ', $bean->name);
            $nameDivisionArray[0] = $nameAnimal;
            $newName = $nameDivisionArray[0] . ' ' . $nameDivisionArray[1];
            $bean->name = $newName;
        }

        $queryForUpdateName = "UPDATE rt_room_transfer SET name = '" . $newName . "' "
                . "WHERE id = '" . $bean->id . "'";
        $result = $db->query($queryForUpdateName);
        if (!$result) {
            $GLOBALS['log']->debug("Name Updation Failed!");
        }
    }

}
