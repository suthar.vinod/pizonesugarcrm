<?php
// created: 2019-06-19 11:46:31
$viewdefs['RT_Room_Transfer']['base']['view']['subpanel-for-anml_animals-anml_animals_rt_room_transfer_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'reason_for_transfer_c',
          'label' => 'LBL_REASON_FOR_TRANSFER',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'transfer_date_c',
          'label' => 'LBL_TRANSFER_DATE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'room_c',
          'label' => 'LBL_ROOM',
          'enabled' => true,
          'id' => 'RMS_ROOM_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);