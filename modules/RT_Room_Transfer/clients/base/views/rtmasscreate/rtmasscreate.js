
({
    extendsFrom: 'CreateView',

    /**
     * Initialize the view and prepare the model with default button metadata
     * for the current layout.
     */
    initialize: function (options) {
        var self = this;

        self._super("initialize", [options]);

        self.model.set('viewtitle', app.lang.get('LBL_RT_MASS_CREATION', 'RT_Room_Transfer'));

        self.context.on('button:rt_mass_update_button:click', this.rtMassCreate, this);
    },
    rtMassCreate: function () {
        var self = this;

        self.toggleButtons(false);
        var allFields = self.getFields(self.module, self.model);
        var fieldsToValidate = {};
        for (var fieldKey in allFields) {
            if (app.acl.hasAccessToModel('edit', self.model, fieldKey)) {
                _.extend(fieldsToValidate, _.pick(allFields, fieldKey));
            }
        }
        self.model.doValidate(fieldsToValidate, _.bind(self.processRTMassCreate, self));
    },
    processRTMassCreate: function (isValid) {
        var self = this;

        if (isValid) {
            //  Preparing Data
            var animalIdsToSend = [];
            var models = self.context.parent.get('mass_collection').models;
            _.each(models, function (selectedModel) {
                animalIdsToSend.push(selectedModel.get('id'));
            });

            if (animalIdsToSend.length > 0) {
                //  Get this model's values
                var valuesToSave = {};
                var fieldsToIterate = self.meta.panels[1].fields;
                _.each(fieldsToIterate, function (field) {
                    valuesToSave[field.name] = (_.isUndefined(self.model.get(field.name))) ? '' : self.model.get(field.name);
                });
                 valuesToSave['rms_room_id_c'] = (_.isUndefined(self.model.get('rms_room_id_c'))) ? '' : self.model.get('rms_room_id_c');
                var dataToSend = {ids: animalIdsToSend, values: valuesToSave};
                self.saveRTs(dataToSend);
            }
        }
    },
    saveRTs: function (dataToSend) {
        var self = this;
        //  Building URL
        var url = app.api.buildURL('rt_mass_create/');
        //  Calling to save with Parameters/data.
        app.alert.show("rtMassCreation", {
            level: 'process',
            title: "Saving",
            autoClose: false,
        });
        app.api.call('create', url, {data: dataToSend}, {
            success: function (serverData) {
                self.toggleButtons(true);
                if (serverData.status)
                {
                    var messageLevel = 'success';
                } else {
                    var messageLevel = 'error';
                }
                app.alert.dismiss('rtMassCreation');
                app.alert.show("rtMassCreation", {
                    level: messageLevel,
                    messages: serverData.Message,
                    autoClose: false,
//                    autoCloseDelay: 5000
                });
                app.drawer.close();
            },
            error: function (serverData) {
                self.toggleButtons(true);
                app.alert.dismiss('rtMassCreation');
                console.log("In Error rtMassCreation ", serverData);
                app.alert.show("rtMassCreation", {
                    level: 'error',
                    messages: 'Saving failed',
                    autoClose: false,
                });
            },
        });
    }
})

