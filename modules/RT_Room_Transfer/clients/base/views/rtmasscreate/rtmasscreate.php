<?php

$viewdefs['RT_Room_Transfer']['base']['view']['rtmasscreate'] = array(
    'buttons' => array(
        array(
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'events' => array(
                'click' => 'button:cancel_button:click',
            ),
        ),
        array(
            'type' => 'rowaction',
            'event' => 'button:rt_mass_update_button:click',
            'name' => 'save_button',
            'label' => 'LBL_CREATE_RTS_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
    'panels' => array(
        array(
            'name' => 'panel_header',
            'header' => true,
            'fields' => array(
                array(
                    'name' => 'viewtitle',
                    'type' => 'varchar',
                    'dismiss_label' => true,
                    'readonly' => true,
                ),
            )
        ),
        array(
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'fields' => array(
                'transfer_date_c',
                'reason_for_transfer_c',
                'room_c',
            ),
        ),
    ),
);

