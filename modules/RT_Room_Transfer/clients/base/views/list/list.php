<?php
$module_name = 'RT_Room_Transfer';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'anml_animals_rt_room_transfer_1_name',
                'label' => 'LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_ANML_ANIMALS_TITLE',
                'enabled' => true,
                'id' => 'ANML_ANIMALS_RT_ROOM_TRANSFER_1ANML_ANIMALS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'transfer_date_c',
                'label' => 'LBL_TRANSFER_DATE',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'reason_for_transfer_c',
                'label' => 'LBL_REASON_FOR_TRANSFER',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'room_c',
                'label' => 'LBL_ROOM',
                'enabled' => true,
                'readonly' => false,
                'id' => 'RMS_ROOM_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
