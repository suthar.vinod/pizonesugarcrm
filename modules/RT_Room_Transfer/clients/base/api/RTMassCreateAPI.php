<?php

class RTMassCreateAPI extends SugarApi {

    public function registerApiRest() {
        return array(
            'rtMassCreate' => array(
                'reqType' => 'POST',
                'path' => array('rt_mass_create'),
                'pathVars' => array(''),
                'method' => 'rtMassCreate',
                'shortHelp' => 'Cretes RTs from the Animals sent in this request',
                'longHelp' => 'Cretes RTs from the Animals sent in this request',
            ),
        );
    }

    public function rtMassCreate($api, $args) {
        $response = array();
        $response['status'] = FALSE;
        $response['Message'] = "Saving failed";

        try {
            //  Save it to DB
            $this->_rtMassCreateToDB($args);

            $response['status'] = TRUE;
            $response['Message'] = "Saved successfully";
        } catch (Exception $exc) {
            $GLOBALS['log']->debug("RTMassCreateAPI : rtMassCreate() -> " . $exc->getMessage());
            $response['status'] = FALSE;
            $response['Message'] = "Saving failed";
        }

        return $response;
    }

    private function _rtMassCreateToDB($args) {
        try {
            $ids = $args['data']['ids'];
            $valuesToSet = $args['data']['values'];
            foreach ($ids as $id) {
                $parentBean = BeanFactory::getBean("ANML_Animals", $id, array('disable_row_level_security' => true));

                $childBean = BeanFactory::newBean("RT_Room_Transfer", null, array('disable_row_level_security' => true));
                foreach ($valuesToSet as $fieldName => $value) {
                    $childBean->$fieldName = $value;
                }

                $childBean->anml_animals_rt_room_transfer_1anml_animals_ida = $id;
    
                $childBean->save();
            }
        } catch (Exception $exc) {
            $GLOBALS['log']->debug("WPEMassCreateAPI : _wpeMassCreateToDB() -> " . $exc->getMessage());
        }
    }

}

