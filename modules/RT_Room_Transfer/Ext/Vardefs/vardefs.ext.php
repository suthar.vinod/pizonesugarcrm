<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RT_Room_Transfer/Ext/Vardefs/anml_animals_rt_room_transfer_1_RT_Room_Transfer.php

// created: 2019-02-20 13:19:54
$dictionary["RT_Room_Transfer"]["fields"]["anml_animals_rt_room_transfer_1"] = array (
  'name' => 'anml_animals_rt_room_transfer_1',
  'type' => 'link',
  'relationship' => 'anml_animals_rt_room_transfer_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_RT_ROOM_TRANSFER_TITLE',
  'id_name' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["RT_Room_Transfer"]["fields"]["anml_animals_rt_room_transfer_1_name"] = array (
  'name' => 'anml_animals_rt_room_transfer_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
  'link' => 'anml_animals_rt_room_transfer_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
  'required' => 'true',  
);
$dictionary["RT_Room_Transfer"]["fields"]["anml_animals_rt_room_transfer_1anml_animals_ida"] = array (
  'name' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_RT_ROOM_TRANSFER_TITLE_ID',
  'id_name' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
  'link' => 'anml_animals_rt_room_transfer_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/RT_Room_Transfer/Ext/Vardefs/sugarfield_transfer_date_c.php

 // created: 2019-02-20 13:13:00
$dictionary['RT_Room_Transfer']['fields']['transfer_date_c']['options']='date_range_search_dom';
$dictionary['RT_Room_Transfer']['fields']['transfer_date_c']['labelValue']='Transfer Date';
$dictionary['RT_Room_Transfer']['fields']['transfer_date_c']['enforced']='';
$dictionary['RT_Room_Transfer']['fields']['transfer_date_c']['dependency']='';
$dictionary['RT_Room_Transfer']['fields']['transfer_date_c']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/RT_Room_Transfer/Ext/Vardefs/sugarfield_name.php

 // created: 2019-06-19 15:12:58
$dictionary['RT_Room_Transfer']['fields']['name']['len']='255';
$dictionary['RT_Room_Transfer']['fields']['name']['audited']=true;
$dictionary['RT_Room_Transfer']['fields']['name']['massupdate']=false;
$dictionary['RT_Room_Transfer']['fields']['name']['unified_search']=false;
$dictionary['RT_Room_Transfer']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['RT_Room_Transfer']['fields']['name']['calculated']=false;
$dictionary['RT_Room_Transfer']['fields']['name']['required']=false;
$dictionary['RT_Room_Transfer']['fields']['name']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/RT_Room_Transfer/Ext/Vardefs/sugarfield_reason_for_transfer_c.php

 // created: 2019-06-19 15:13:50
$dictionary['RT_Room_Transfer']['fields']['reason_for_transfer_c']['labelValue']='Reason for Transfer';
$dictionary['RT_Room_Transfer']['fields']['reason_for_transfer_c']['dependency']='';
$dictionary['RT_Room_Transfer']['fields']['reason_for_transfer_c']['visibility_grid']='';

 
?>
