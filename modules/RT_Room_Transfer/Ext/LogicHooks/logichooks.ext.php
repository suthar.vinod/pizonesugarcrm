<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RT_Room_Transfer/Ext/LogicHooks/logic_hooks.php


$hook_array['before_save'][] = Array('1','workflow','include/workflow/WorkFlowHandler.php','WorkFlowHandler','WorkFlowHandler',);
$hook_array['after_save'][] = Array('1','Generate automated name of Room Transfers','custom/modules/RT_Room_Transfer/customLogicHook.php','customLogicHook','generateAutomatedName',);


?>
<?php
// Merged from custom/Extension/modules/RT_Room_Transfer/Ext/LogicHooks/animalRoomUpdate.php

$hook_version = 1;

if (!isset($hook_array)) {
	$hook_array = array();
}
if (!isset($hook_array['after_save'])) {
	$hook_array['after_save'] = array();
}
$hook_array['after_save'][] = array(
   count($hook_array['after_save']),
   'Link Room to parent Animal from Related Room Transfer record with latest Transfer Date',
   'custom/modules/RT_Room_Transfer/LogicHooksImplementations/animalRoomUpdate.php',
   'animalRoomUpdateClass',
   'updateRoom'
);

?>
