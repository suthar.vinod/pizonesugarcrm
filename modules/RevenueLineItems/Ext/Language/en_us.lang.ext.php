<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Company Name:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Company Name';
$mod_strings['LBL_ACCOUNT_ID'] = 'Company ID';
$mod_strings['LBL_LIST_OPPORTUNITY_NAME'] = 'Opportunity Name';
$mod_strings['LBL_OPPORTUNITY_ID'] = 'Opportunity ID';
$mod_strings['LBL_OPPORTUNITY'] = 'Opportunity';
$mod_strings['WARNING_MERGE_RLIS_WITH_DIFFERENT_OPPORTUNITIES'] = 'One or more of the records you&#039;ve selected can not be merged together as they belong to different Opportunities';

?>
