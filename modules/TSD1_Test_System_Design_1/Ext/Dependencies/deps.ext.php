<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Dependencies/field_required_dependency.php


$dependencies['TSD1_Test_System_Design_1']['test_system_require_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('date_entered','id'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'tasks_start_prior_to_day_7_c',
                'value' => 'or(not(greaterThan(strlen(toString($date_entered)),0)),isAfter($date_entered,"2021-08-19"))',
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Dependencies/visible_fields.php

$dependencies['TSD1_Test_System_Design_1']['set_blanket_readonly'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('species','breedstrain','sex','animal_source_type','animal_source','min_age_type','min_age_at_initial_procedure','min_age_units_c',
    'max_age_type','max_age_at_initial_procedure','max_age_units_c','min_wt_type','min_wt_at_initial_procedure','max_wt_at_initial_procedure','contraindicated_act_med_2_c',
    'diet_mods_c','acclimation_required_c','nonglp_pre_screen_c','m03_work_product_tsd1_test_system_design_1_1_name','number_1','description'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'breedstrain',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),  
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'sex',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'animal_source_type',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'animal_source',
                'value' => 'and(not(equal($species,"Human Cadaver")),equal($animal_source_type,"Specific"))',
            ),
        ),        
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'min_age_type',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ), 
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'min_age_at_initial_procedure',
                'value' => 'and(not(equal($species,"Human Cadaver")),equal($min_age_type,"Actual"))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'min_age_units_c',
                'value' => 'and(not(equal($species,"Human Cadaver")),equal($min_age_type,"Actual"))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'max_age_type',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ), 
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'max_age_at_initial_procedure',
                'value' => 'and(not(equal($species,"Human Cadaver")),equal($max_age_type,"Actual"))',
            ),
        ), 
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'max_age_units_c',
                'value' => 'and(not(equal($species,"Human Cadaver")),equal($max_age_type,"Actual"))',
            ),
        ), 
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'min_wt_type',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),      
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'max_wt_type',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'contraindicated_act_med_2_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ), 
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'diet_mods_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ), 
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'acclimation_required_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ), 
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'nonglp_pre_screen_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ), 
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_tsd1_test_system_design_1_1_name',
                'value'=>'true'
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'filename',
                'value' => 'equal($species,"Human Cadaver")',
            ),
        ),        
    ),
); 

//for ticket 1816 (Conditionally Hide new TSD fields)
/*
    By: Harshit Shreshthi
    Set visibility and required of the fields if species is equal to Human Cadaver
 */
$dependencies['TSD1_Test_System_Design_1']['hide_newest_fields_TSD'] = array(
    'hooks' => array("edit","view"), 
    'trigger' => 'true',
    'triggerFields' => array('species','prolonged_restraint_acc_c','tasks_start_prior_to_day_7_c','specialize_housing_required_c','confirm_acclimation_restrain_c','communicate_custom_needs_c','contraindicated_act_med_2_c','diet_mods_c','nonglp_pre_screen_c','vendor_surgical_model_requir_c','acclimation_required_c','hazardous_agents_c','date_entered'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'prolonged_restraint_acc_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),  
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'vendor_surgical_model_requir_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'tasks_start_prior_to_day_7_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'specialize_housing_required_c',
                'value' => 'and(not(equal($species,"Human Cadaver")))',
            ),
        ),        
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'confirm_acclimation_restrain_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'hazardous_agents_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ), 
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'communicate_custom_needs_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),  
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'contraindicated_act_med_2_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),         
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'diet_mods_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),         
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'prolonged_restraint_acc_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),         
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'nonglp_pre_screen_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),         
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'vendor_surgical_model_requir_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),         
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'specialize_housing_required_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),         
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'acclimation_required_c',
                'value' => 'not(equal($species,"Human Cadaver"))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'hazardous_agents_c',
                'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2022-01-06"))),not(equal($species,"Human Cadaver")))',
            ),
        ),         
    ),
);

?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Dependencies/species_field_dep.php

$dependencies['TSD1_Test_System_Design_1']['set_options_vis11111'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('species'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'breedstrain',
                'keys' => 'ifElse(
                    equal($species,"Ovine"),
                    createList("","Cross Breed","Dorset","Dorset X","Hampshire","Hampshire X","Polypay","Polypay X","Suffolk","Suffolk X","Other"),
                    ifElse(
                    equal($species,"Canine"),
                    createList("","Beagle","Mongrel","Other"),
                    ifElse(
                    equal($species,"Porcine"),
                    createList("","Gottingen", "Hanford","Landrace Cross","LDLR","Micro Yucatan","Ossabaw","Sinclair","Yucatan","Yorkshire X","Other"),
                    ifElse(
                    equal($species,"Bovine"),
                    createList("","Holstein","Other"),
                    ifElse(
                    equal($species,"Caprine"),
                    createList("","Cross Breed","Other"),
                    ifElse(
                    equal($species,"Mouse"),
                    createList("","BALBc","C57","CD1","ND4","Other"),
                    ifElse(
                    equal($species,"Lagomorph"),
                    createList("","New Zealand White","Other"),
                    ifElse(
                    equal($species,"Rat"),
                    createList("","Sprague Dawley","Wistar","Lewis","Other"),
                    ifElse(
                    equal($species,"Hamster"),
                    createList("","Golden Syrian","Other"),
                    ifElse(
                    equal($species,"Guinea Pig"),
                    createList("","Hartley","Other"),
                    createList("")))))))))))',
            'labels' => 'ifElse(
                equal($species,"Ovine"),
                createList("","Cross Breed - NonSpecific","Dorset","Dorset X","Hampshire","Hampshire X","Polypay","Polypay X","Suffolk","Suffolk X","Other"),
                ifElse(
                equal($species,"Canine"),
                createList("","Beagle","Mongrel","Other"),
                ifElse(
                equal($species,"Porcine"),
                createList("","Gottingen","Hanford","Landrace Cross","LDLR Yucatan","Micro-Yucatan","Ossabaw","Sinclair","Yucatan","Yorkshire X","Other"),
                ifElse(
                equal($species,"Bovine"),
                createList("","Holstein","Other"),
                ifElse(
                equal($species,"Caprine"),
                createList("","Cross Breed - NonSpecific","Other"),
                ifElse(
                equal($species,"Mouse"),
                createList("","BALB/c","C57","CD1","ND4","Other"),
                ifElse(
                equal($species,"Lagomorph"),
                createList("","New Zealand White","Other"),
                ifElse(
                equal($species,"Rat"),
                createList("","Sprague Dawley","Wistar","Lewis","Other"),
                ifElse(
                equal($species,"Hamster"),
                createList("","Golden Syrian","Other"),
                ifElse(
                equal($species,"Guinea Pig"),
                createList("","Hartley","Other"),
                createList("")))))))))))'
            ),
        ),
    ),
);
?>
