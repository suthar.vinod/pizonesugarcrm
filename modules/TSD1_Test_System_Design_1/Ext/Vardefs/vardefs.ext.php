<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/m03_work_product_tsd1_test_system_design_1_1_TSD1_Test_System_Design_1.php

// created: 2021-02-11 08:58:28
$dictionary["TSD1_Test_System_Design_1"]["fields"]["m03_work_product_tsd1_test_system_design_1_1"] = array (
  'name' => 'm03_work_product_tsd1_test_system_design_1_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_tsd1_test_system_design_1_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_TSD1_TEST_SYSTEM_DESIGN_1_TITLE',
  'id_name' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["TSD1_Test_System_Design_1"]["fields"]["m03_work_product_tsd1_test_system_design_1_1_name"] = array (
  'name' => 'm03_work_product_tsd1_test_system_design_1_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
  'link' => 'm03_work_product_tsd1_test_system_design_1_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["TSD1_Test_System_Design_1"]["fields"]["m03_work_product_tsd1_test_system_design_1_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_TSD1_TEST_SYSTEM_DESIGN_1_TITLE_ID',
  'id_name' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
  'link' => 'm03_work_product_tsd1_test_system_design_1_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_name.php

 // created: 2021-02-11 09:15:26
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['importable']='false';
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['duplicate_merge']='disabled';
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['merge_filter']='disabled';
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['unified_search']=false;
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['calculated']='true';
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['formula']='concat(related($m03_work_product_tsd1_test_system_design_1_1,"name")," TS",getDropdownValue("number_list",$number_1))';
$dictionary['TSD1_Test_System_Design_1']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_max_age_type.php

 // created: 2021-02-11 13:19:08
$dictionary['TSD1_Test_System_Design_1']['fields']['max_age_type']['default']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_u_units_id_c.php

 // created: 2021-02-18 05:45:51

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_min_age_units_c.php

 // created: 2021-02-18 05:45:51
$dictionary['TSD1_Test_System_Design_1']['fields']['min_age_units_c']['labelValue']='Min. Age Units';
$dictionary['TSD1_Test_System_Design_1']['fields']['min_age_units_c']['dependency']='equal($min_age_type,"Actual")';
$dictionary['TSD1_Test_System_Design_1']['fields']['min_age_units_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_u_units_id1_c.php

 // created: 2021-02-18 05:47:27

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_max_age_units_c.php

 // created: 2021-02-18 05:47:27
$dictionary['TSD1_Test_System_Design_1']['fields']['max_age_units_c']['labelValue']='Max. Age Units';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_age_units_c']['dependency']='equal($max_age_type,"Actual")';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_age_units_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_min_age_at_initial_procedure.php

 // created: 2021-02-18 08:54:59

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_max_age_at_initial_procedure.php

 // created: 2021-02-18 08:55:35

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_description.php

 // created: 2021-02-18 08:56:07
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['audited']=true;
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['massupdate']=false;
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['hidemassupdate']=false;
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['comments']='Full text of the note';
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['duplicate_merge']='enabled';
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['merge_filter']='disabled';
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['unified_search']=false;
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['calculated']=false;
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['rows']='6';
$dictionary['TSD1_Test_System_Design_1']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_file_mime_type.php

 // created: 2020-10-16 16:09:35
$GLOBALS['dictionary']['TSD1_Test_System_Design_1']['fields']['file_mime_type'] = array(

     'name' => 'file_mime_type',
     'vname' => 'LBL_FILE_MIME_TYPE',
     'type' => 'varchar',
     'len' => '100',
     'comment' => 'Attachment MIME type',
     'importable' => false,
);

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_file_url.php

 // created: 2020-10-16 16:09:35
$GLOBALS['dictionary']['TSD1_Test_System_Design_1']['fields']['file_url'] = array (
     'name' => 'file_url',
     'vname' => 'LBL_FILE_URL',
     'type' => 'varchar',
     'source' => 'non-db',
     'reportable' => false,
     'comment' => 'Path to file (can be URL)',
     'importable' => false,

);

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_filename.php

 // created: 2020-10-16 16:09:35
$GLOBALS['dictionary']['TSD1_Test_System_Design_1']['fields']['filename'] = array ( 
	'name' => 'filename',
    'vname' => 'LBL_FILENAME',
    'type' => 'file',
    'dbType' => 'varchar',
    'len' => '255',
    'comments' => 'File name associated with the note (attachment)',
    'reportable' => true,
    'comment' => 'File name associated with the note (attachment)',
    'importable' => false,
    'dependency' => 'equal($species,"Human Cadaver")',
	'audited' => false,
	'studio' => true,
	'massupdate' => false,
	'unified_search' => false,
	'calculated' => false,
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '1',
	'merge_filter' => 'disabled',
);
 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_tasks_start_prior_to_day_7_c.php

 // created: 2021-10-14 10:34:02
$dictionary['TSD1_Test_System_Design_1']['fields']['tasks_start_prior_to_day_7_c']['labelValue']='Tasks start on or before day -7';
$dictionary['TSD1_Test_System_Design_1']['fields']['tasks_start_prior_to_day_7_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['tasks_start_prior_to_day_7_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['tasks_start_prior_to_day_7_c']['readonly_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['tasks_start_prior_to_day_7_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_min_wt_type.php

 // created: 2021-11-30 12:31:15

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_min_wt_at_initial_procedure.php

 // created: 2021-11-30 12:32:10
$dictionary['TSD1_Test_System_Design_1']['fields']['min_wt_at_initial_procedure']['dependency']='isInList($min_wt_type,createList("Actual","Actual or having suitable anatomy"))';
$dictionary['TSD1_Test_System_Design_1']['fields']['min_wt_at_initial_procedure']['required_formula']='isInList($min_wt_type,createList("Actual","Actual or having suitable anatomy"))';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_max_wt_at_initial_procedure.php

 // created: 2021-11-30 12:33:29
$dictionary['TSD1_Test_System_Design_1']['fields']['max_wt_at_initial_procedure']['dependency']='isInList($max_wt_type,createList("Actual","Actual or having suitable anatomy"))';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_degree_of_immobility_c.php

 // created: 2021-12-07 09:23:36
$dictionary['TSD1_Test_System_Design_1']['fields']['degree_of_immobility_c']['labelValue']='Degree of Immobility';
$dictionary['TSD1_Test_System_Design_1']['fields']['degree_of_immobility_c']['dependency']='equal($prolonged_restraint_acc_c,"Yes")';
$dictionary['TSD1_Test_System_Design_1']['fields']['degree_of_immobility_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['degree_of_immobility_c']['readonly_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['degree_of_immobility_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_max_duration_integer_c.php

 // created: 2021-12-07 09:45:12
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_integer_c']['labelValue']='Max. Duration Integer';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_integer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_integer_c']['enforced']='false';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_integer_c']['dependency']='or(isInList($type_of_acclimation_c,createList("Manual restraint","Leash","Sling","Imaging Cart")),equal($purpose_c,"Restrict animals movement only while working with the animal"))';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_integer_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_integer_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_u_units_id2_c.php

 // created: 2021-12-07 09:46:30

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_max_duration_unit_c.php

 // created: 2021-12-07 09:46:30
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_unit_c']['labelValue']='Max. Duration Unit';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_unit_c']['dependency']='or(isInList($type_of_acclimation_c,createList("Manual restraint","Leash","Sling","Imaging Cart")),equal($purpose_c,"Restrict animals movement only while working with the animal"))';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_unit_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['max_duration_unit_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_other_type_of_acclimation_c.php

 // created: 2021-12-07 09:47:46
$dictionary['TSD1_Test_System_Design_1']['fields']['other_type_of_acclimation_c']['labelValue']='Other Type of Acclimation';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_type_of_acclimation_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['other_type_of_acclimation_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_type_of_acclimation_c']['dependency']='equal($type_of_acclimation_c,"Other")';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_type_of_acclimation_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_type_of_acclimation_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_details_of_specific_position_c.php

 // created: 2021-12-07 09:49:02
$dictionary['TSD1_Test_System_Design_1']['fields']['details_of_specific_position_c']['labelValue']='Details of Specific Positioning';
$dictionary['TSD1_Test_System_Design_1']['fields']['details_of_specific_position_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['details_of_specific_position_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['details_of_specific_position_c']['dependency']='equal($purpose_c,"Complete awake imaging where animal is required to be in a specific position for imaging")';
$dictionary['TSD1_Test_System_Design_1']['fields']['details_of_specific_position_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['details_of_specific_position_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_requirements_c.php

 // created: 2021-12-07 09:56:34
$dictionary['TSD1_Test_System_Design_1']['fields']['requirements_c']['labelValue']='Requirements';
$dictionary['TSD1_Test_System_Design_1']['fields']['requirements_c']['dependency']='equal($specialize_housing_required_c,"Yes")';
$dictionary['TSD1_Test_System_Design_1']['fields']['requirements_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['requirements_c']['readonly_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['requirements_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_other_housing_requirements_c.php

 // created: 2021-12-07 09:59:36
$dictionary['TSD1_Test_System_Design_1']['fields']['other_housing_requirements_c']['labelValue']='Other Housing Requirements';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_housing_requirements_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['other_housing_requirements_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_housing_requirements_c']['dependency']='equal($requirements_c,"Other")';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_housing_requirements_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_confirm_acclimation_restrain_c.php

 // created: 2021-12-07 10:00:43
$dictionary['TSD1_Test_System_Design_1']['fields']['confirm_acclimation_restrain_c']['labelValue']='Confirm with vivarium management that the type of acclimation or restraint items are in house and available to be used';
$dictionary['TSD1_Test_System_Design_1']['fields']['confirm_acclimation_restrain_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['confirm_acclimation_restrain_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['confirm_acclimation_restrain_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_communicate_custom_needs_c.php

 // created: 2021-12-07 10:02:41
$dictionary['TSD1_Test_System_Design_1']['fields']['communicate_custom_needs_c']['labelValue']='Communicate any custom needs (type of jacket, harness’, etc.) to vivarium management and purchasing';
$dictionary['TSD1_Test_System_Design_1']['fields']['communicate_custom_needs_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['communicate_custom_needs_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['communicate_custom_needs_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_type_of_acclimation_c.php

 // created: 2021-12-14 09:10:50
$dictionary['TSD1_Test_System_Design_1']['fields']['type_of_acclimation_c']['labelValue']='Type of Acclimation';
$dictionary['TSD1_Test_System_Design_1']['fields']['type_of_acclimation_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['type_of_acclimation_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['type_of_acclimation_c']['readonly_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['type_of_acclimation_c']['visibility_grid']=array (
  'trigger' => 'degree_of_immobility_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Minimal' => 
    array (
      0 => '',
      1 => 'JacketHarness',
      2 => 'E collar',
      3 => 'C collar',
      4 => 'Manual restraint',
      5 => 'Leash',
      6 => 'Other',
    ),
    'Partial' => 
    array (
      0 => '',
      1 => 'Crossties',
      2 => 'Sling',
      3 => 'Imaging Cart',
      4 => 'Other',
    ),
    'Complete' => 
    array (
      0 => '',
      1 => 'Pyrogen testingRabbit restrainer',
      2 => 'Other',
    ),
    'Minimal Partial' => 
    array (
      0 => '',
      1 => 'Crossties Jacket',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_purpose_c.php

 // created: 2021-12-14 09:20:27
$dictionary['TSD1_Test_System_Design_1']['fields']['purpose_c']['labelValue']='Purpose';
$dictionary['TSD1_Test_System_Design_1']['fields']['purpose_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['purpose_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['purpose_c']['readonly_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['purpose_c']['visibility_grid']=array (
  'trigger' => 'type_of_acclimation_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'JacketHarness' => 
    array (
      0 => '',
      1 => 'Protect item or device from animal no additional weight added',
      2 => 'Hold device or item additional weight added',
    ),
    'E collar' => 
    array (
    ),
    'C collar' => 
    array (
    ),
    'Manual restraint' => 
    array (
    ),
    'Leash' => 
    array (
    ),
    'Other' => 
    array (
    ),
    'Crossties' => 
    array (
      0 => '',
      1 => 'Connected to external items',
      2 => 'Restrict animals movement only while working with the animal',
    ),
    'Sling' => 
    array (
      0 => '',
      1 => 'Perform procedure on animal while in sling',
      2 => 'Gain access to limbs or specific area',
    ),
    'Imaging Cart' => 
    array (
      0 => '',
      1 => 'Complete awake imaging where the animal is required to remain calm and still',
      2 => 'Complete awake imaging where animal is required to be in a specific position for imaging',
    ),
    'Pyrogen testingRabbit restrainer' => 
    array (
    ),
    'Crossties Jacket' => 
    array (
      0 => '',
      1 => 'Protect item or device from animal no additional weight added',
      2 => 'Hold device or item additional weight added',
      3 => 'Connected to external items',
      4 => 'Restrict animals movement only while working with the animal',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_prolonged_restraint_acc_c.php

 // created: 2021-12-28 09:09:17
$dictionary['TSD1_Test_System_Design_1']['fields']['prolonged_restraint_acc_c']['labelValue']='Prolonged Restraint Acclimation Required';
$dictionary['TSD1_Test_System_Design_1']['fields']['prolonged_restraint_acc_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['prolonged_restraint_acc_c']['readonly_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['prolonged_restraint_acc_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_vendor_surgical_model_requir_c.php

 // created: 2021-12-28 09:10:08
$dictionary['TSD1_Test_System_Design_1']['fields']['vendor_surgical_model_requir_c']['labelValue']='Vendor Surgical Model Required';
$dictionary['TSD1_Test_System_Design_1']['fields']['vendor_surgical_model_requir_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['vendor_surgical_model_requir_c']['readonly_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['vendor_surgical_model_requir_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_specialize_housing_required_c.php

 // created: 2021-12-28 09:11:12
$dictionary['TSD1_Test_System_Design_1']['fields']['specialize_housing_required_c']['labelValue']='Specialized Housing Required';
$dictionary['TSD1_Test_System_Design_1']['fields']['specialize_housing_required_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['specialize_housing_required_c']['readonly_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['specialize_housing_required_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_acclimation_required_c.php

 // created: 2021-12-28 09:20:45
$dictionary['TSD1_Test_System_Design_1']['fields']['acclimation_required_c']['labelValue']='Prolonged Restraint Acclimation Required (Obsolete)';
$dictionary['TSD1_Test_System_Design_1']['fields']['acclimation_required_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['acclimation_required_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['acclimation_required_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['acclimation_required_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_contraindicated_act_med_2_c.php

 // created: 2021-12-28 09:23:20
$dictionary['TSD1_Test_System_Design_1']['fields']['contraindicated_act_med_2_c']['labelValue']='Ordering Contraindicated Activities or Meds';
$dictionary['TSD1_Test_System_Design_1']['fields']['contraindicated_act_med_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['contraindicated_act_med_2_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['contraindicated_act_med_2_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['contraindicated_act_med_2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_diet_mods_c.php

 // created: 2021-12-28 09:24:08
$dictionary['TSD1_Test_System_Design_1']['fields']['diet_mods_c']['labelValue']='Diet Mods or Unapproved Medications';
$dictionary['TSD1_Test_System_Design_1']['fields']['diet_mods_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['diet_mods_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['diet_mods_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['diet_mods_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_nonglp_pre_screen_c.php

 // created: 2021-12-28 09:25:11
$dictionary['TSD1_Test_System_Design_1']['fields']['nonglp_pre_screen_c']['labelValue']='nonGLP Pre-Screening Required';
$dictionary['TSD1_Test_System_Design_1']['fields']['nonglp_pre_screen_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['nonglp_pre_screen_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['nonglp_pre_screen_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['nonglp_pre_screen_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_other_breedstrain_c.php

 // created: 2021-12-30 09:21:08
$dictionary['TSD1_Test_System_Design_1']['fields']['other_breedstrain_c']['labelValue']='Other Breed/Strain';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_breedstrain_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['other_breedstrain_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_breedstrain_c']['dependency']='equal($breedstrain,"Other")';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_breedstrain_c']['required_formula']='equal($breedstrain,"Other")';
$dictionary['TSD1_Test_System_Design_1']['fields']['other_breedstrain_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_hazardous_agents_c.php

 // created: 2022-01-06 09:42:17
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agents_c']['labelValue']='Hazardous Agents (BSL2, chemo, etc.)';
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agents_c']['dependency']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agents_c']['required_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agents_c']['readonly_formula']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agents_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_hazardous_agent_details_c.php

 // created: 2022-01-06 09:44:27
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agent_details_c']['labelValue']='Hazardous Agent Details';
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agent_details_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agent_details_c']['enforced']='';
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agent_details_c']['dependency']='equal($hazardous_agents_c,"Yes")';
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agent_details_c']['required_formula']='equal($hazardous_agents_c,"Yes")';
$dictionary['TSD1_Test_System_Design_1']['fields']['hazardous_agent_details_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/TSD1_Test_System_Design_1/Ext/Vardefs/sugarfield_number_1.php

 // created: 2022-03-22 07:13:35

 
?>
