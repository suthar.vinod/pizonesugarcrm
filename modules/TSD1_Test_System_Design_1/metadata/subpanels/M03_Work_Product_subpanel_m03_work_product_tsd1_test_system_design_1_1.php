<?php
// created: 2021-02-18 09:00:11
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'species' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_SPECIES',
    'id' => 'S_SPECIES_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'S_Species',
    'target_record_key' => 's_species_id_c',
  ),
  'breedstrain' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_BREEDSTRAIN',
    'width' => 10,
  ),
  'sex' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_SEX',
    'width' => 10,
  ),
  'animal_source_type' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ANIMAL_SOURCE_TYPE',
    'width' => 10,
  ),
  'animal_source' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_ANIMAL_SOURCE',
    'id' => 'ACCOUNT_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Accounts',
    'target_record_key' => 'account_id_c',
  ),
  'min_age_type' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_MIN_AGE_TYPE',
    'width' => 10,
  ),
  'min_age_at_initial_procedure' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_MIN_AGE_AT_INITIAL_PROCEDURE',
    'width' => 10,
  ),
  'min_age_units_c' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_MIN_AGE_UNITS',
    'id' => 'U_UNITS_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'U_Units',
    'target_record_key' => 'u_units_id_c',
  ),
  'max_age_type' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_MAX_AGE_TYPE',
    'width' => 10,
  ),
  'max_age_at_initial_procedure' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_MAX_AGE_AT_INITIAL_PROCEDURE',
    'width' => 10,
  ),
  'max_age_units_c' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_MAX_AGE_UNITS',
    'id' => 'U_UNITS_ID1_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'U_Units',
    'target_record_key' => 'u_units_id1_c',
  ),
  'min_wt_type' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_MIN_WT_TYPE',
    'width' => 10,
  ),
  'min_wt_at_initial_procedure' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_MIN_WT_AT_INITIAL_PROCEDURE',
    'width' => 10,
  ),
  'max_wt_type' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_MAX_WT_TYPE',
    'width' => 10,
  ),
  'max_wt_at_initial_procedure' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_MAX_WT_AT_INITIAL_PROCEDURE',
    'width' => 10,
  ),
);