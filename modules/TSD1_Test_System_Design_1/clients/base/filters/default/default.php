<?php
// created: 2022-01-06 09:47:53
$viewdefs['TSD1_Test_System_Design_1']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'animal_source' => 
    array (
    ),
    'animal_source_type' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'breedstrain' => 
    array (
    ),
    'communicate_custom_needs_c' => 
    array (
    ),
    'confirm_acclimation_restrain_c' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'degree_of_immobility_c' => 
    array (
    ),
    'details_of_specific_position_c' => 
    array (
    ),
    'diet_mods_c' => 
    array (
    ),
    'hazardous_agents_c' => 
    array (
    ),
    'hazardous_agent_details_c' => 
    array (
    ),
    'max_age_at_initial_procedure' => 
    array (
    ),
    'min_age_at_initial_procedure' => 
    array (
    ),
    'max_age_type' => 
    array (
    ),
    'min_age_type' => 
    array (
    ),
    'max_age_units_c' => 
    array (
    ),
    'min_age_units_c' => 
    array (
    ),
    'max_duration_integer_c' => 
    array (
    ),
    'max_duration_unit_c' => 
    array (
    ),
    'max_wt_type' => 
    array (
    ),
    'min_wt_type' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'name' => 
    array (
    ),
    'nonglp_pre_screen_c' => 
    array (
    ),
    'description' => 
    array (
    ),
    'number_1' => 
    array (
    ),
    'contraindicated_act_med_2_c' => 
    array (
    ),
    'other_breedstrain_c' => 
    array (
    ),
    'other_housing_requirements_c' => 
    array (
    ),
    'other_type_of_acclimation_c' => 
    array (
    ),
    'prolonged_restraint_acc_c' => 
    array (
    ),
    'purpose_c' => 
    array (
    ),
    'requirements_c' => 
    array (
    ),
    'sex' => 
    array (
    ),
    'specialize_housing_required_c' => 
    array (
    ),
    'species' => 
    array (
    ),
    'max_wt_at_initial_procedure' => 
    array (
    ),
    'min_wt_at_initial_procedure' => 
    array (
    ),
    'tasks_start_prior_to_day_7_c' => 
    array (
    ),
    'type_of_acclimation_c' => 
    array (
    ),
    'vendor_surgical_model_requir_c' => 
    array (
    ),
    'm03_work_product_tsd1_test_system_design_1_1_name' => 
    array (
    ),
  ),
);