({
 
    extendsFrom: 'RecordView',
    initialize: function (options) {
       this._super('initialize', [options]);
       this.model.once("sync",function() {this.model.on("change:species",this.showHideFilds,this);},this);
    },
    
    showHideFilds: function() {		
	    var species = this.model.get('species');		
       if( species === 'Human Cadaver' ) {
            this.model.set('breedstrain', "");
            this.model.set('sex', "");
            this.model.set('animal_source_type', "");
            this.model.set('animal_source', "");
            this.model.set('min_age_type', "");
            this.model.set('min_age_at_initial_procedure', "");
            this.model.set('min_age_units_c', "");
            this.model.set('max_age_type', "");
            this.model.set('max_age_at_initial_procedure', "");
            this.model.set('max_age_units_c', "");
            this.model.set('min_wt_type', "");
			this.model.set('max_wt_type', "");
            this.model.set('min_wt_at_initial_procedure', "");
            this.model.set('max_wt_at_initial_procedure', "");
            this.model.set('contraindicated_act_med_2_c', "");
            this.model.set('diet_mods_c', "");
            this.model.set('acclimation_required_c', "");
            this.model.set('nonglp_pre_screen_c', "");
            this.model.set('filename', "");
       } 
    }
})