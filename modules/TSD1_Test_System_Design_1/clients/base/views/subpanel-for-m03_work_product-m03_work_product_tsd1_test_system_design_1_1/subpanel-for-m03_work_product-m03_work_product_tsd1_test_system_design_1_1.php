<?php
// created: 2021-02-18 09:00:12
$viewdefs['TSD1_Test_System_Design_1']['base']['view']['subpanel-for-m03_work_product-m03_work_product_tsd1_test_system_design_1_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'species',
          'label' => 'LBL_SPECIES',
          'enabled' => true,
          'id' => 'S_SPECIES_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'breedstrain',
          'label' => 'LBL_BREEDSTRAIN',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sex',
          'label' => 'LBL_SEX',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'animal_source_type',
          'label' => 'LBL_ANIMAL_SOURCE_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'animal_source',
          'label' => 'LBL_ANIMAL_SOURCE',
          'enabled' => true,
          'id' => 'ACCOUNT_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'min_age_type',
          'label' => 'LBL_MIN_AGE_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'min_age_at_initial_procedure',
          'label' => 'LBL_MIN_AGE_AT_INITIAL_PROCEDURE',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'min_age_units_c',
          'label' => 'LBL_MIN_AGE_UNITS',
          'enabled' => true,
          'id' => 'U_UNITS_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'max_age_type',
          'label' => 'LBL_MAX_AGE_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'max_age_at_initial_procedure',
          'label' => 'LBL_MAX_AGE_AT_INITIAL_PROCEDURE',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'max_age_units_c',
          'label' => 'LBL_MAX_AGE_UNITS',
          'enabled' => true,
          'id' => 'U_UNITS_ID1_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'min_wt_type',
          'label' => 'LBL_MIN_WT_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        13 => 
        array (
          'name' => 'min_wt_at_initial_procedure',
          'label' => 'LBL_MIN_WT_AT_INITIAL_PROCEDURE',
          'enabled' => true,
          'default' => true,
        ),
        14 => 
        array (
          'name' => 'max_wt_type',
          'label' => 'LBL_MAX_WT_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        15 => 
        array (
          'name' => 'max_wt_at_initial_procedure',
          'label' => 'LBL_MAX_WT_AT_INITIAL_PROCEDURE',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);