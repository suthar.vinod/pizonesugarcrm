<?php
$module_name = 'TSD1_Test_System_Design_1';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'species',
                'label' => 'LBL_SPECIES',
                'enabled' => true,
                'id' => 'S_SPECIES_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'breedstrain',
                'label' => 'LBL_BREEDSTRAIN',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'sex',
                'label' => 'LBL_SEX',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'animal_source_type',
                'label' => 'LBL_ANIMAL_SOURCE_TYPE',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'animal_source',
                'label' => 'LBL_ANIMAL_SOURCE',
                'enabled' => true,
                'id' => 'ACCOUNT_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'min_age_type',
                'label' => 'LBL_MIN_AGE_TYPE',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'min_age_at_initial_procedure',
                'label' => 'LBL_MIN_AGE_AT_INITIAL_PROCEDURE',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'min_age_units_c',
                'label' => 'LBL_MIN_AGE_UNITS',
                'enabled' => true,
                'id' => 'U_UNITS_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'max_age_type',
                'label' => 'LBL_MAX_AGE_TYPE',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'max_age_at_initial_procedure',
                'label' => 'LBL_MAX_AGE_AT_INITIAL_PROCEDURE',
                'enabled' => true,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'max_age_units_c',
                'label' => 'LBL_MAX_AGE_UNITS',
                'enabled' => true,
                'id' => 'U_UNITS_ID1_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'min_wt_type',
                'label' => 'LBL_MIN_WT_TYPE',
                'enabled' => true,
                'default' => true,
              ),
              13 => 
              array (
                'name' => 'min_wt_at_initial_procedure',
                'label' => 'LBL_MIN_WT_AT_INITIAL_PROCEDURE',
                'enabled' => true,
                'default' => true,
              ),
              14 => 
              array (
                'name' => 'max_wt_type',
                'label' => 'LBL_MAX_WT_TYPE',
                'enabled' => true,
                'default' => true,
              ),
              15 => 
              array (
                'name' => 'max_wt_at_initial_procedure',
                'label' => 'LBL_MAX_WT_AT_INITIAL_PROCEDURE',
                'enabled' => true,
                'default' => true,
              ),
              16 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              17 => 
              array (
                'name' => 'communicate_custom_needs_c',
                'label' => 'LBL_COMMUNICATE_CUSTOM_NEEDS',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              18 => 
              array (
                'name' => 'confirm_acclimation_restrain_c',
                'label' => 'LBL_CONFIRM_ACCLIMATION_RESTRAIN',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              19 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => false,
              ),
              20 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => false,
              ),
              21 => 
              array (
                'name' => 'degree_of_immobility_c',
                'label' => 'LBL_DEGREE_OF_IMMOBILITY',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              22 => 
              array (
                'name' => 'details_of_specific_position_c',
                'label' => 'LBL_DETAILS_OF_SPECIFIC_POSITION',
                'enabled' => true,
                'readonly' => false,
                'sortable' => false,
                'default' => false,
              ),
              23 => 
              array (
                'name' => 'diet_mods_c',
                'label' => 'LBL_DIET_MODS',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              24 => 
              array (
                'name' => 'hazardous_agents_c',
                'label' => 'LBL_HAZARDOUS_AGENTS',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              25 => 
              array (
                'name' => 'hazardous_agent_details_c',
                'label' => 'LBL_HAZARDOUS_AGENT_DETAILS',
                'enabled' => true,
                'readonly' => false,
                'sortable' => false,
                'default' => false,
              ),
              26 => 
              array (
                'name' => 'max_duration_integer_c',
                'label' => 'LBL_MAX_DURATION_INTEGER',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              27 => 
              array (
                'name' => 'max_duration_unit_c',
                'label' => 'LBL_MAX_DURATION_UNIT',
                'enabled' => true,
                'readonly' => false,
                'id' => 'U_UNITS_ID2_C',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              28 => 
              array (
                'name' => 'nonglp_pre_screen_c',
                'label' => 'LBL_NONGLP_PRE_SCREEN',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              29 => 
              array (
                'name' => 'contraindicated_act_med_2_c',
                'label' => 'LBL_CONTRAINDICATED_ACT_MED_2',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              30 => 
              array (
                'name' => 'other_breedstrain_c',
                'label' => 'LBL_OTHER_BREEDSTRAIN',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              31 => 
              array (
                'name' => 'other_housing_requirements_c',
                'label' => 'LBL_OTHER_HOUSING_REQUIREMENTS',
                'enabled' => true,
                'readonly' => false,
                'sortable' => false,
                'default' => false,
              ),
              32 => 
              array (
                'name' => 'other_type_of_acclimation_c',
                'label' => 'LBL_OTHER_TYPE_OF_ACCLIMATION',
                'enabled' => true,
                'readonly' => false,
                'sortable' => false,
                'default' => false,
              ),
              33 => 
              array (
                'name' => 'prolonged_restraint_acc_c',
                'label' => 'LBL_PROLONGED_RESTRAINT_ACC',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              34 => 
              array (
                'name' => 'purpose_c',
                'label' => 'LBL_PURPOSE',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              35 => 
              array (
                'name' => 'requirements_c',
                'label' => 'LBL_REQUIREMENTS',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              36 => 
              array (
                'name' => 'specialize_housing_required_c',
                'label' => 'LBL_SPECIALIZE_HOUSING_REQUIRED',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              37 => 
              array (
                'name' => 'tasks_start_prior_to_day_7_c',
                'label' => 'LBL_TASKS_START_PRIOR_TO_DAY_7',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              38 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
              39 => 
              array (
                'name' => 'type_of_acclimation_c',
                'label' => 'LBL_TYPE_OF_ACCLIMATION',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              40 => 
              array (
                'name' => 'vendor_surgical_model_requir_c',
                'label' => 'LBL_VENDOR_SURGICAL_MODEL_REQUIR',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
