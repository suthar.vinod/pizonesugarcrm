<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M01_Sales_Activity_Quote/Ext/Vardefs/sugarfield_currency_id.php

 // created: 2016-01-25 02:09:16

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales_Activity_Quote/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2016-01-25 02:09:17

 
?>
<?php
// Merged from custom/Extension/modules/M01_Sales_Activity_Quote/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:56
$dictionary['M01_Sales_Activity_Quote']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/M01_Sales_Activity_Quote/Ext/Vardefs/sugarfield_quote_amount_c.php

 // created: 2016-10-25 03:27:04
$dictionary['M01_Sales_Activity_Quote']['fields']['quote_amount_c']['labelValue'] = 'Quote Amount';
$dictionary['M01_Sales_Activity_Quote']['fields']['quote_amount_c']['enforced'] = '';
$dictionary['M01_Sales_Activity_Quote']['fields']['quote_amount_c']['related_fields'][0] = 'currency_id';
$dictionary['M01_Sales_Activity_Quote']['fields']['quote_amount_c']['related_fields'][1] = 'base_rate';


?>
<?php
// Merged from custom/Extension/modules/M01_Sales_Activity_Quote/Ext/Vardefs/sugarfield_name.php

 // created: 2016-10-25 03:27:04
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['len'] = '255';
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['audited'] = true;
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['massupdate'] = false;
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['unified_search'] = false;
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['full_text_search']['boost'] = 1.5500000000000000444089209850062616169452667236328125;
$dictionary['M01_Sales_Activity_Quote']['fields']['name']['calculated'] = false;


?>
<?php
// Merged from custom/Extension/modules/M01_Sales_Activity_Quote/Ext/Vardefs/sugarfield_quote_date_c.php

 // created: 2016-10-25 03:27:04
$dictionary['M01_Sales_Activity_Quote']['fields']['quote_date_c']['labelValue'] = 'Quote Date';
$dictionary['M01_Sales_Activity_Quote']['fields']['quote_date_c']['enforced'] = '';
$dictionary['M01_Sales_Activity_Quote']['fields']['quote_date_c']['dependency'] = '';
$dictionary['M01_Sales_Activity_Quote']['fields']['quote_date_c']['full_text_search']['boost'] = 1;


?>
