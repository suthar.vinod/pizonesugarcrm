<?php
// created: 2016-01-25 14:15:05
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'quote_date_c' => 
  array (
    'type' => 'date',
    'default' => true,
    'vname' => 'LBL_QUOTE_DATE',
    'width' => '10%',
  ),
  'quote_amount_c' => 
  array (
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_QUOTE_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
  ),
);