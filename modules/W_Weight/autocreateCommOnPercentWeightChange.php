<?php
if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

class autocreateCommOnPercentWeightChange
{
	static $already_ran = false;

	function autocreateCommOnWeightChange($bean, $event, $arguments)
	{
		if (self::$already_ran == true)
			return;   //So that hook will only trigger once
		self::$already_ran = true;
		global $db, $current_user;		
		/*Query 1: To fetch species details from TS*/
		$create_comm     = 0;
		$isupdate 		 = $arguments['isUpdate'];
		$animalId        = $bean->anml_animals_w_weight_1anml_animals_ida;
		$TSBean = BeanFactory::retrieveBean("ANML_Animals", $animalId);
		$allocate_wp_id  = $TSBean->m03_work_product_id_c;
		
		$perc_wt_chng_c  = $bean->perc_wt_chng_c;
		$perc_wt_chng_c  = round($perc_wt_chng_c, 2);

		if ($allocate_wp_id != '' && $allocate_wp_id != null){
			$WP_ID =  $allocate_wp_id;
		} else {
			$WP_ID = $this->getStudyId($animalId);
		}

		if ($isupdate == false) {

			$strQuery = "SELECT tbl_animal.name,tbl_ani_cstm.s_species_id_c,tbl_spec.name AS species_name         
			FROM anml_animals AS tbl_animal 
			LEFT JOIN anml_animals_cstm AS tbl_ani_cstm ON tbl_ani_cstm.id_c = tbl_animal.id 
			LEFT JOIN s_species AS tbl_spec ON tbl_spec.id = tbl_ani_cstm.s_species_id_c    
			WHERE tbl_animal.id='" . $animalId . "' AND tbl_animal.deleted=0";

			$results      = $db->query($strQuery);
			$Row          = $db->fetchByAssoc($results);
			$species_name = $Row['species_name'];
			/** Create a comm record for condition: Species on parent Test System is one of Bovine, 
			 * Canine, Caprine, Ovine AND % Weight Change is <-10 */
			if (($species_name == 'Bovine' || $species_name == 'Canine' || $species_name == 'Caprine' || $species_name == 'Ovine') && ($perc_wt_chng_c <'-10.00')) {
				$create_comm = 1;
			} else {
				$create_comm = 0;
			}

			$date_2_c            = $bean->date_2_c;
			$Date_Occure 		 = strtotime($date_2_c);
			$date                = date('Y-m-d', $Date_Occure);
			$actual_event   	 = $perc_wt_chng_c;
			/* Date Time on new record is greater than 14 days from the closest Date Time 
		on any of the previous records for that TS  */
			/**#1474 update date time field with date_2_c : 13 Aug 2021 */
			$newdate_time_in 	 = date('Y-m-d', strtotime($date_2_c . ' - 14 days'));
			$date_before_2Week 	 = $newdate_time_in;
			/** Create a comm record for condition: Species on parent Test System is Porcine AND
			 * Date Time on new record is greater than 14 days from the closest Date Time on any of the previous records for that TS
			 * AND % Weight Change on the new record is </= 0	**/
			if (($species_name == 'Porcine')  && ($perc_wt_chng_c <= '0.00')) {
				$Query = "SELECT tbl_we.id,tbl_we.name,tbl_we_cstm.id_c,tbl_we_cstm.date_2_c,tbl_we_cstm.perc_wt_chng_c               
			 FROM anml_animals_w_weight_1_c AS tbl_animal_we  
			 LEFT JOIN w_weight AS tbl_we ON tbl_animal_we.anml_animals_w_weight_1w_weight_idb = tbl_we.id    
			 LEFT JOIN w_weight_cstm AS tbl_we_cstm ON tbl_we.id = tbl_we_cstm.id_c    
			 WHERE tbl_animal_we.anml_animals_w_weight_1anml_animals_ida='" . $animalId . "' AND tbl_animal_we.deleted=0 AND tbl_we_cstm.id_c !='".$bean->id."' AND tbl_we.deleted=0  ORDER BY tbl_we_cstm.date_2_c DESC LIMIT 1";
			 
			 $res 			  = $db->query($Query);
			 $row 			  = $db->fetchByAssoc($res);
			 $last_date_2_c   = $row['date_2_c'];
			 if ($res->num_rows > 0 && $last_date_2_c < $date_before_2Week && $last_date_2_c != '') 
			 {
				 $create_comm = 1;
			 }								
			 else {
				 $create_comm = 0;
			 }				
			 
			}			
			if ($create_comm == 1) {
				//$WP_ID = $this->getStudyId($animalId);
				$NewCommBean = BeanFactory::newBean('M06_Error');
				$new_bean_id = create_guid();

				$NewCommBean->id          	             = $new_bean_id;				
				$NewCommBean->new_with_id 	             = true;
				$NewCommBean->fetched_row 	             = null;

				$NewCommBean->name = $this->generateSystemId();

				$NewCommBean->submitter_c                = $current_user->name;
				$NewCommBean->error_category_c           = "Vet Check";
				$NewCommBean->error_type_c               = "Initial Issue";
				$NewCommBean->date_error_occurred_c      = $date;
				$NewCommBean->actual_event_c             = 'Animal Weight change of ' . $actual_event . '%';

				if (!empty($animalId)) {
					$NewCommBean->test_system_involved_c = 'Yes';
				} else {
					$NewCommBean->test_system_involved_c = 'No';
				}				
				$user_Email =  $current_user->email1;
				$contact_query = "SELECT EAB.bean_id FROM `email_addresses` AS EA LEFT JOIN `email_addr_bean_rel` AS EAB ON EA.id =EAB.email_address_id WHERE EA.email_address='" . $user_Email . "' AND EAB.bean_module='Contacts' AND EAB.deleted=0";
				$contact_exec = $db->query($contact_query);
				if ($contact_exec->num_rows > 0) {
					$con_result = $db->fetchByAssoc($contact_exec);
					$employee_id = $con_result['bean_id']; // On Study Count					
				}
				//$NewCommBean->employee_id = $current_user->id;
				$NewCommBean->employee_id = $employee_id;
				$NewCommBean->wordpress_flag = 1;
				$animalId_1 = array($animalId);
				//$type = 'Entry';
				// $NewCommBean->rp_entry             = '[\"' . $employee_id . '\"]';
				// $NewCommBean->rp_entry_email_dept  = '[\"'. $WPD_UserName.'--'.$employee_id.'\"]';

				$NewCommBean->related_data  = array(
					"Erd_Error_Documents" => "",					
					"ANML_Animals" => $animalId_1,
					"Equip_Equipment" => "",
					"RMS_Room" => "",
					"A1A_Critical_Phase_Inspectio" => "",
					"Teams" => "",
					"M06_Error" => ''
				);
				$NewCommBean->work_products = '[\"' . $WP_ID . '\"]';
				$NewCommBean->save();
				//That code will be uncomment when we deploy ticket #220. (one to many relationship) 
				//$this->createDeviationEmployee($employee_id, $NewCommBean->id, $type, $date);	
				if (!empty($animalId)) {
					$relationid = create_guid();
					$tssql = 'INSERT INTO m06_error_anml_animals_1_c (id,date_modified,deleted,m06_error_anml_animals_1m06_error_ida,m06_error_anml_animals_1anml_animals_idb) 
							values("'.$relationid.'",now(),0,"'.$NewCommBean->id.'","'.$animalId.'")';
					$db->query($tssql);
				}
	
				if (!empty($WP_ID)) {
					//$NewCommBean->m06_error_m03_work_product_1->add($WPID);
					$relationid1 = create_guid();
					$wpsql = 'INSERT INTO m06_error_m03_work_product_1_c (id,date_modified,deleted,m06_error_m03_work_product_1m06_error_ida,m06_error_m03_work_product_1m03_work_product_idb) 
							values("'.$relationid1.'",now(),0,"'.$NewCommBean->id.'","'.$WP_ID.'")';	
					$db->query($wpsql);
				}
				$CD_ID = '45ed0230-06c8-11e8-bae1-06d4fd13a43b';
				if (!empty($CD_ID)) {
					$relationid1 = create_guid();
					$cdsql = 'INSERT INTO m06_error_erd_error_documents_1_c (id,date_modified,deleted,m06_error_erd_error_documents_1m06_error_ida,m06_error_erd_error_documents_1erd_error_documents_idb) 
						values("'.$relationid1.'",now(),0,"'.$NewCommBean->id.'","'.$CD_ID.'")';	
					$db->query($cdsql);
				}
	
				if (!empty($bean->id)) {
					//$NewCommBean->m06_error_m03_work_product_1->add($WPID);
					// $relationid1 = create_guid();
					// $cdsql = 'INSERT INTO w_weight_m06_error_1_c (id,date_modified,deleted,w_weight_m06_error_1w_weight_ida,w_weight_m06_error_1m06_error_idb)  
					// 	values("'.$relationid1.'",now(),0,"'.$bean->id.'","'.$NewCommBean->id.'")';
					// $db->query($cdsql);
					$NewCommBean->load_relationship('w_weight_m06_error_2');
			    	$NewCommBean->w_weight_m06_error_2->add($bean->id);
				}
				
			}
		}
	}
	//That code will be uncomment when we deploy ticket #220. (one to many relationship) 
	/*function createDeviationEmployee($employee_Id, $commId, $type, $date_occurred){
		global $db,$current_user;
		$new_bean = BeanFactory::newBean('DE_Deviation_Employees');
		$new_bean->id = create_guid();
		$new_bean->new_with_id = true;
		$new_bean->fetched_row = null;
		$new_bean->name		= $this->nameCreation($date_occurred, $employee_Id);
		$new_bean->ere_error_ee6cployees_ida	= $employee_Id;
		$new_bean->deviation_type_c 			= $type;
		$new_bean->contacts_de_deviation_employees_1contacts_ida = $employee_Id;
		$new_bean->save();
		$new_bean->load_relationship('m06_error_de_deviation_employees_2');
		$new_bean->m06_error_de_deviation_employees_2->add($commId);		
	}*/

	/*function nameCreation($date_occurred, $employee_Id) {

        global $db;
        $date_occurred	= strtotime($date_occurred);  
        $curr_year		= date("y", $date_occurred); //get year of "Date Error Occured" 2 digit
         
		$query = "SELECT max(de_deviation_employees.name) AS name FROM `de_deviation_employees`
				INNER JOIN contacts_de_deviation_employees_1_c
					ON de_deviation_employees.id = contacts_de_deviation_employees_1_c.contacts_de_deviation_employees_1de_deviation_employees_idb
					AND contacts_de_deviation_employees_1_c.deleted = 0
				WHERE contacts_de_deviation_employees_1_c.contacts_de_deviation_employees_1contacts_ida = '{$employee_Id}'
					AND de_deviation_employees.deleted = 0 AND de_deviation_employees.name LIKE '%{$curr_year}%'
				ORDER BY contacts_de_deviation_employees_1_c.date_modified DESC;";

		$result = $db->query($query);
 
		if ($result->num_rows > 0) {
			$row = $db->fetchByAssoc($result);

			if (!empty($row['name'])) {
				$system_id = $row['name'];
				$system_id = explode('-', $system_id);

				$prev_year = str_split($system_id[0]);

				$system_id = str_split($system_id[1]);
				$system_id = $system_id[0] . $system_id[1] . $system_id[2] . $system_id[3];

				if ($prev_year[0] == 'D') {
					$prev_year = $prev_year[1] . $prev_year[2];
				} else {
					$prev_year = $prev_year[2] . $prev_year[3];
				}
			} else {
				$prev_year = 0;
				$system_id = 1;
			}


			if (!empty($system_id)) {
				if ($prev_year == $curr_year) {
					// we are in same year
					$code = $this->generate_code($system_id, $curr_year);
				} else {
					$code = 'CE' . $curr_year . '-0001';
				}
			}
		} else {
			$code = 'CE' . $curr_year . '-0001';
		}

        if ($code != "") {
            return $code;
        }
    }

	function generate_code($system_id, $curr_year) {
        #$number = (int) substr($system_id, -4);
        $number = (int) $system_id;
        $number = $number + 1;
        $length = strlen($number);
        if ($length == 1) {
            $number = '000' . $number;
        } else if ($length == 2) {
            $number = '00' . $number;
        } else if ($length == 3) {
            $number = '0' . $number;
        }
        $code = 'CE' . $curr_year . '-' . $number;

        return $code;
    }*/

	function getStudyId($animalId)
	{
		global $db;
		$wpe_sql  = "SELECT WP.id AS WPID
										FROM anml_animals_wpe_work_product_enrollment_1_c AS TSWPA
										LEFT JOIN m03_work_product_wpe_work_product_enrollment_1_c AS WPWPA 
										ON TSWPA.anml_anima9941ollment_idb = WPWPA.m03_work_p9bf5ollment_idb
										LEFT JOIN m03_work_product AS WP ON  WPWPA.m03_work_p7d13product_ida = WP.id
										WHERE TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida = '" . $animalId . "'
										AND TSWPA.deleted = 0 ORDER BY TSWPA.date_modified DESC LIMIT 1";
		$wpe_exec = $db->query($wpe_sql);
		if ($wpe_exec->num_rows > 0) {
			$resultWP = $db->fetchByAssoc($wpe_exec);
			$WP_ID    = $resultWP['WPID'];
			return $WP_ID;
		}
	}

	function generateSystemId()
	{
		global $db;
		$year        = date("y");
		$capitalYear = date("Y");

		if ($capitalYear == 70) {
			$year        = date("y");
			$capitalYear = date("Y");
		}

		$query  = "SELECT * FROM custom_modules_sequence WHERE module_name = 'Communications' AND sequence_year_short_name = '" . $year . "' ORDER BY id DESC LIMIT 0,1";
		$result = $db->query($query);

		if ($rows = $db->fetchByAssoc($result)) {
			$id             = $rows['id'];
			$running_number = $rows['running_number'] + 1;
			$query_update   = "UPDATE custom_modules_sequence SET running_number = '" . $running_number . "' WHERE id = '" . $id . "'";
			$db->query($query_update);
		} else {
			$nameCheck         = "C" . $year . "%";
			$queryGetLastName  = "SELECT MAX(CAST(SUBSTRING(NAME, 5, LENGTH(NAME)-4) AS UNSIGNED)) AS `name_error` FROM `m06_error` WHERE NAME LIKE '" . $nameCheck . "' and deleted = 0 ";
			$resultGetLastName = $db->query($queryGetLastName);
			if ($rowGetLastName = $db->fetchByAssoc($resultGetLastName)) {
				$lastNumber     = $rowGetLastName['name_error'];
				$running_number = $lastNumber + 1;
			} else {
				$running_number = 1;
			}
			$queryInsert = "INSERT INTO `custom_modules_sequence` (`module_name`, `module_short_name`, `sequence_year`, `sequence_year_short_name`, `start_number`, `lpad_string`, `sequence_number_length`, `running_number`, `active_status`) VALUES ('Communications', 'C', '" . $capitalYear . "', '" . $year . "', '1', '0', '3', '" . $running_number . "', 1)";
			$db->query($queryInsert);
		}

		if ($year < '20') {
			$number_len = 4;
		} else {
			$number_len = 5;
		}
		$running_number = str_pad(strval($running_number), $number_len, "0", STR_PAD_LEFT);

		$code = 'C' . $year . '-' . $running_number;

		return $code;
	}
}
