<?php

class WeightHook
{
    /**
     * @param $bean
     * @param $event
     * @param $arguments
     * Set WeightUnit Method
     */
    static $already_ran = false;  // To make sure LogicHooks will execute just onces
    
    function setWeightUnit($bean, $event, $arguments)
    {
        global $db;
        //Get related Test System record
        $relatedTestSystem = $bean->anml_animals_w_weight_1anml_animals_ida;
        //Get Test System Bean
        $TSbean = BeanFactory::getBean('ANML_Animals', $relatedTestSystem);
        $query = "Select u_units_id_c from s_species where id ='" . $TSbean->s_species_id_c."';";
        //Get Species Weight Unit
        $result = $db->query($query);
        if ($result && mysqli_num_rows($result) > 0){
            //fetch and set recent weight  into bodyweight field.
            $row = $db->fetchByAssoc($result);
            //populate Weight's weight  unit from Species Weight unit
            $bean->u_units_id_c = $row['u_units_id_c'];
			
        }
    }
    /**#1385 :01 july 2021 : Gsingh */
    /**
     * @param $bean
     * @param $event
     * @param $arguments
     * Set WeightUnit Method
     */
    function setPercentageWeightChange($bean, $event, $arguments)
    {
       // if (self::$already_ran == true)
       // return;   //So that hook will only trigger once
       // self::$already_ran = true;
        global $db;
        
        if($bean->load_relationship('anml_animals_w_weight_1'))
        {
           $test_system_id=$bean->anml_animals_w_weight_1anml_animals_ida;
            $date_2_c=$bean->date_2_c;           
            //Query to Fetch Ids of All Weight Modules Linked with Selected Test Systems
            //$strQuery="SELECT anml_animals_w_weight_1w_weight_idb AS wID FROM `anml_animals_w_weight_1_c` WHERE anml_animals_w_weight_1anml_animals_ida='". $test_system_id."' AND anml_animals_w_weight_1w_weight_idb <> '".$bean->id."' AND deleted=0";
            $strQuery="SELECT tbl_animal_we.*,tbl_animal_we.anml_animals_w_weight_1w_weight_idb AS wID,
            tbl_we.id,tbl_we.name,tbl_we.weight,tbl_we.date_entered AS date_created,tbl_we.date_modified,tbl_we.deleted,tbl_we_cstm.perc_wt_chng_c,tbl_we_cstm.date_2_c            
            FROM anml_animals_w_weight_1_c AS tbl_animal_we  
            LEFT JOIN w_weight AS tbl_we ON tbl_animal_we.anml_animals_w_weight_1w_weight_idb = tbl_we.id    
            LEFT JOIN w_weight_cstm AS tbl_we_cstm ON tbl_we.id = tbl_we_cstm.id_c    
            WHERE tbl_animal_we.anml_animals_w_weight_1anml_animals_ida='". $test_system_id."' AND tbl_animal_we.anml_animals_w_weight_1w_weight_idb <> '" .$bean->id ."' AND tbl_animal_we.deleted=0 AND tbl_we.deleted=0 AND tbl_we_cstm.date_2_c < '".$date_2_c."' ORDER BY tbl_we_cstm.date_2_c DESC";
            $results = $db->query($strQuery);
            $weightIDs= array();
            while ($arRow = $db->fetchByAssoc($results))
            {
                array_push($weightIDs,$arRow['wID']);
            }
            
            if(!empty($weightIDs))
            {
                $weightIdString="";
                foreach ($weightIDs as $wId) {
                    $weightIdString=$weightIdString."'".$wId."',";
                }               

                $weightIdString = rtrim($weightIdString, ",");
                
                //Query to get Max Weight Value from all weight Records related to Test system
                $strQuery="SELECT MAX(weight) FROM `w_weight` WHERE id IN ($weightIdString)";
                $maxWeight = $db->getOne($strQuery);               
                if($maxWeight){
                    $percWeightChng=(($bean->weight-$maxWeight)/$maxWeight)*100;
                    $bean->perc_wt_chng_c=$percWeightChng;                   
                }

            }
            else
            {
                $bean->perc_wt_chng_c="0.00";                
            }

        }

    }

}
