<?php
include_once('include/TimeDate.php');
class WeightBWHook
{
    static $already_ran = false;

    function pullBodyWeight($bean, $event, $arguments)
    {
        global $db;
        if (self::$already_ran == true) return;
        self::$already_ran = true;
        $timeDate = new TimeDate();
        $check_recent = false;
        $testSystem = $bean->anml_animals_w_weight_1anml_animals_ida;
        $TSbean = BeanFactory::retrieveBean('ANML_Animals', $testSystem);
        if ($TSbean->load_relationship('anml_animals_w_weight_1')) {
            //Fetch related Weight Record
            $weightBeans = $TSbean->anml_animals_w_weight_1->get();
            $query = "SELECT tbl_we.id,tbl_we.weight,tbl_we_cstm.date_2_c 
            FROM w_weight AS tbl_we
            LEFT JOIN w_weight_cstm AS tbl_we_cstm ON tbl_we.id = tbl_we_cstm.id_c
             WHERE tbl_we.id IN (";
            //Get All related weight records
            foreach ($weightBeans as $i => $weight) {
                $query .= ($i != count($weightBeans) - 1) ? "'" . $weight . "'," : "'" . $weight . "'";
                $check_recent = true;
            }
            if ($check_recent == 'true') {
                $query .= ",'" . $bean->id . "'";
            } else {
                $query .= "'" . $bean->id . "'";
            }
            $query .= ") AND deleted = 0 ORDER BY tbl_we_cstm.date_2_c desc LIMIT 1;";
            $result = $db->query($query);
            if ($result && mysqli_num_rows($result) > 0) {
                // fetch and set recent weight,datetime  into Test System's field
                $row = $db->fetchByAssoc($result);
                $date = $row['date_2_c'];
                //$db_date = $timeDate->to_db_date($date,false);
                /*#1474 changes : 16 aug 2021 */
                $db_date = $date;
                $TSbean->bodyweight_c = $row['weight'];
                if($TSbean->com_usda_id == ""){
                    $TSbean->date_last_weighed_c = $db_date;
                    $TSbean->save();
                }
                
            }
        }
    }
}
