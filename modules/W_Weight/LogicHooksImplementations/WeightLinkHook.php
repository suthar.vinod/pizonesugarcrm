<?php
include_once('include/TimeDate.php');
class WeightLinkHook
{
    function linkWeightRecord($bean, $event, $arguments)
    {
        global $db;
        $check_recent = false;
        $timeDate = new TimeDate();
        //Execute script on linking existing record case    
        if (empty($bean->fetched_row['id'])) {
            return;
        }
        $TSbean = BeanFactory::getBean('ANML_Animals', $arguments['related_id']);
        //Load relationship
        if ($TSbean->load_relationship('anml_animals_w_weight_1')) {
            $weightBeans = $TSbean->anml_animals_w_weight_1->get();
            $query = "SELECT tbl_we.id,tbl_we.weight,tbl_we_cstm.date_2_c 
            FROM w_weight AS tbl_we
            LEFT JOIN w_weight_cstm AS tbl_we_cstm ON tbl_we.id = tbl_we_cstm.id_c
             WHERE tbl_we.id IN (";
            //Get All related weight records
            foreach ($weightBeans as $i => $weight) {
                $query .= ($i != count($weightBeans) - 1) ? "'" . $weight . "'," : "'" . $weight . "'";
                $check_recent = true;
            }
            if ($check_recent == true) {
                $query .= ",'" . $bean->id . "'";
            } else {
                $query .= "'" . $bean->id . "'";
            }
            $query .= ") AND deleted = 0 ORDER BY tbl_we_cstm.date_2_c desc LIMIT 1;";
            $result = $db->query($query, 1);
            if ($result && mysqli_num_rows($result) > 0) {
                // fetch and set recent weight  into bodyweight field.
                $row = $db->fetchByAssoc($result);
                //$date = date('m/d/Y', strtotime($row['date_2_c']));
                $date = $row['date_2_c'];
                //$db_date = $timeDate->to_db_date($date,false);
                 /*#1474 changes : 16 aug 2021 */
                $db_date = $date;
                $TSbean->bodyweight_c = $row['weight'];
                if($TSbean->com_usda_id == ""){
                    $TSbean->date_last_weighed_c = $db_date;
                    $TSbean->save();
                }
                
            }
        }
    }
}
