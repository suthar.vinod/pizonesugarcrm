<?php
// created: 2021-08-19 07:17:29
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'type_2' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TYPE_2',
    'width' => 10,
  ),
  'weight' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_WEIGHT',
    'width' => 10,
  ),
  'weight_unit' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'readonly' => true,
    'vname' => 'LBL_WEIGHT_UNIT',
    'id' => 'U_UNITS_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'U_Units',
    'target_record_key' => 'u_units_id_c',
  ),
  'perc_wt_chng_c' => 
  array (
    'readonly' => '1',
    'readonly_formula' => '',
    'type' => 'decimal',
    'vname' => 'LBL_PERC_WT_CHNG',
    'width' => 10,
    'default' => true,
  ),
);