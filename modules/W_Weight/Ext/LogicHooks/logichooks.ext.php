<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/LogicHooks/WeightHook.php


$hook_array['before_save'][] = array(
   '1',
   'Pull Species Unit to populate Weight Unit ',
   'custom/modules/W_Weight/WeightHook.php',
   'WeightHook',
   'setWeightUnit'
);


?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/LogicHooks/WeightChangeHook.php


$hook_array['before_save'][] = array(
   '2',
   'Set Percentage Weight Change ',
   'custom/modules/W_Weight/WeightHook.php',
   'WeightHook',
   'setPercentageWeightChange'
);

?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/LogicHooks/AutoCommCreationHook.php

$hook_array['before_save'][] = array(
   '3',
   'COM Auto-creation for % Weight Change',
   'custom/modules/W_Weight/autocreateCommOnPercentWeightChange.php',
   'autocreateCommOnPercentWeightChange',
   'autocreateCommOnWeightChange'
);

?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/LogicHooks/WeightBWHook.php


$hook_array['after_save'][] = array(
   '0',
   'Set recent weight record value to Body weight',
   'custom/modules/W_Weight/LogicHooksImplementations/WeightBWHook.php',
   'WeightBWHook',
   'pullBodyWeight'
);
$hook_array['after_relationship_add'][] = array(
   '100',
   'link record hook',
   'custom/modules/W_Weight/LogicHooksImplementations/WeightLinkHook.php',
   'WeightLinkHook',
   'linkWeightRecord'
);
/**Bug Fix : #2644 : Null TS record creation (14 July 2022)
 * We have commented the below file to avoid blank ts creation on deletion of 
 * weight records  */
/* $hook_array['after_relationship_delete'][] = array(
   '99',
   'link record hook',
   'custom/modules/W_Weight/LogicHooksImplementations/WeightLinkHook.php',
   'WeightLinkHook',
   'linkWeightRecord'
); */
/************************ EOC Bug Fix #2644 **************************************** */
$hook_array['before_save'][] = array(
   '101',
   'Auto-create COM for Weight Protocol Range',
   'custom/modules/W_Weight/LogicHooksImplementations/autocreateCommOnChangeWMPR.php',
   'autocreateCommOnChangeWMPR',
   'autocreateComm'
);

?>
