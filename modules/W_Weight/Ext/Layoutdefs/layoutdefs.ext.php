<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Layoutdefs/w_weight_m06_error_2_W_Weight.php

 // created: 2021-09-14 09:20:50
$layout_defs["W_Weight"]["subpanel_setup"]['w_weight_m06_error_2'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_W_WEIGHT_M06_ERROR_2_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'w_weight_m06_error_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Layoutdefs/_overrideW_Weight_subpanel_w_weight_m06_error_2.php

//auto-generated file DO NOT EDIT
$layout_defs['W_Weight']['subpanel_setup']['w_weight_m06_error_2']['override_subpanel_name'] = 'W_Weight_subpanel_w_weight_m06_error_2';

?>
