<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/w_weight_m06_error_1_W_Weight.php

// created: 2020-01-07 13:27:00
$dictionary["W_Weight"]["fields"]["w_weight_m06_error_1"] = array (
  'name' => 'w_weight_m06_error_1',
  'type' => 'link',
  'relationship' => 'w_weight_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'w_weight_m06_error_1m06_error_idb',
);
$dictionary["W_Weight"]["fields"]["w_weight_m06_error_1_name"] = array (
  'name' => 'w_weight_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'save' => true,
  'id_name' => 'w_weight_m06_error_1m06_error_idb',
  'link' => 'w_weight_m06_error_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'name',
);
$dictionary["W_Weight"]["fields"]["w_weight_m06_error_1m06_error_idb"] = array (
  'name' => 'w_weight_m06_error_1m06_error_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_1_FROM_M06_ERROR_TITLE_ID',
  'id_name' => 'w_weight_m06_error_1m06_error_idb',
  'link' => 'w_weight_m06_error_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/sugarfield_test_system_name_c.php

 // created: 2020-01-07 13:30:06
$dictionary['W_Weight']['fields']['test_system_name_c']['duplicate_merge_dom_value']=0;
$dictionary['W_Weight']['fields']['test_system_name_c']['labelValue']='Test System Name';
$dictionary['W_Weight']['fields']['test_system_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['W_Weight']['fields']['test_system_name_c']['calculated']='true';
$dictionary['W_Weight']['fields']['test_system_name_c']['formula']='related($anml_animals_w_weight_1,"name")';
$dictionary['W_Weight']['fields']['test_system_name_c']['enforced']='true';
$dictionary['W_Weight']['fields']['test_system_name_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/sugarfield_current_wp_assignment_c.php

 // created: 2020-01-07 13:31:08
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['duplicate_merge_dom_value']=0;
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['labelValue']='Current Work Product Assignment';
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['calculated']='true';
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['formula']='related($anml_animals_w_weight_1,"assigned_to_wp_c")';
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['enforced']='true';
$dictionary['W_Weight']['fields']['current_wp_assignment_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/sugarfield_wt_meets_prosop_requirement.php

$dictionary['W_Weight']['fields']['weight_meets_pro_req']['readonly'] = true;

?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/sugarfield_study_id.php

$dictionary['W_Weight']['fields']['study_id']['readonly'] = true;

?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/sugarfield_weight_meets_pro_req.php

 // created: 2020-01-08 13:55:03
$dictionary['W_Weight']['fields']['weight_meets_pro_req']['required']=false;
$dictionary['W_Weight']['fields']['weight_meets_pro_req']['massupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/sugarfield_weight_unit.php

 // created: 2020-01-09 12:48:03
$dictionary['W_Weight']['fields']['weight_unit']['readonly']=true;
$dictionary['W_Weight']['fields']['weight_unit']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/anml_animals_w_weight_1_W_Weight.php

// created: 2020-01-07 13:24:35
$dictionary["W_Weight"]["fields"]["anml_animals_w_weight_1"] = array (
  'name' => 'anml_animals_w_weight_1',
  'type' => 'link',
  'relationship' => 'anml_animals_w_weight_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_W_WEIGHT_TITLE',
  'id_name' => 'anml_animals_w_weight_1anml_animals_ida',
  'link-type' => 'one',
  'required' => true,
);
$dictionary["W_Weight"]["fields"]["anml_animals_w_weight_1_name"] = array (
  'name' => 'anml_animals_w_weight_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_w_weight_1anml_animals_ida',
  'link' => 'anml_animals_w_weight_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
  'required' => true,
);
$dictionary["W_Weight"]["fields"]["anml_animals_w_weight_1anml_animals_ida"] = array (
  'name' => 'anml_animals_w_weight_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_W_WEIGHT_TITLE_ID',
  'id_name' => 'anml_animals_w_weight_1anml_animals_ida',
  'link' => 'anml_animals_w_weight_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
  'required' => true,
);
$dictionary["W_Weight"]["fields"]["anml_animals_w_weight_1_name"] ["required"]= true;
$dictionary["W_Weight"]["fields"]["anml_animals_w_weight_1anml_animals_ida"] ["required"]= true;
$dictionary["W_Weight"]["fields"]["anml_animals_w_weight_1"] ["required"]= true;
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/sugarfield_perc_wt_chng_c.php

 // created: 2021-07-15 08:16:38
$dictionary['W_Weight']['fields']['perc_wt_chng_c']['labelValue']='% Weight Change';
$dictionary['W_Weight']['fields']['perc_wt_chng_c']['enforced']='';
$dictionary['W_Weight']['fields']['perc_wt_chng_c']['dependency']='';
$dictionary['W_Weight']['fields']['perc_wt_chng_c']['required_formula']='';
$dictionary['W_Weight']['fields']['perc_wt_chng_c']['readonly']='1';
$dictionary['W_Weight']['fields']['perc_wt_chng_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/sugarfield_feed_quantity.php

 // created: 2021-07-22 09:20:58
$dictionary['W_Weight']['fields']['feed_quantity']['required']=false;
$dictionary['W_Weight']['fields']['feed_quantity']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/sugarfield_housing_size.php

 // created: 2021-07-22 09:21:43
$dictionary['W_Weight']['fields']['housing_size']['required']=false;
$dictionary['W_Weight']['fields']['housing_size']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/sugarfield_date_2_c.php

 // created: 2021-08-19 07:04:30
$dictionary['W_Weight']['fields']['date_2_c']['labelValue']='Date';
$dictionary['W_Weight']['fields']['date_2_c']['enforced']='';
$dictionary['W_Weight']['fields']['date_2_c']['dependency']='';
$dictionary['W_Weight']['fields']['date_2_c']['required_formula']='';
$dictionary['W_Weight']['fields']['date_2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/sugarfield_name.php

 // created: 2021-08-12 12:58:32
$dictionary['W_Weight']['fields']['name']['importable']='false';
$dictionary['W_Weight']['fields']['name']['duplicate_merge']='disabled';
$dictionary['W_Weight']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['W_Weight']['fields']['name']['merge_filter']='disabled';
$dictionary['W_Weight']['fields']['name']['unified_search']=false;
$dictionary['W_Weight']['fields']['name']['calculated']='1';
$dictionary['W_Weight']['fields']['name']['formula']='concat($test_system_name_c," WT ",toString($date_time),toString($date_2_c))';
$dictionary['W_Weight']['fields']['name']['enforced']=true;
$dictionary['W_Weight']['fields']['name']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/W_Weight/Ext/Vardefs/w_weight_m06_error_2_W_Weight.php

// created: 2021-09-14 09:20:50
$dictionary["W_Weight"]["fields"]["w_weight_m06_error_2"] = array (
  'name' => 'w_weight_m06_error_2',
  'type' => 'link',
  'relationship' => 'w_weight_m06_error_2',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_W_WEIGHT_M06_ERROR_2_FROM_W_WEIGHT_TITLE',
  'id_name' => 'w_weight_m06_error_2w_weight_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
