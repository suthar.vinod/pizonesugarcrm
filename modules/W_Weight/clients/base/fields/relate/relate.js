({
    extendsFrom: 'RelateField',
    initialize: function (options) {
        this._super('initialize', [options]);

    },
    /*To search text box filter*/
    search: _.debounce(function (query) {
        var term = query.term || '',
            self = this,
            searchModule = this.getSearchModule(),
            params = {},
            limit = self.def.limit || 5,
            relatedModuleField = this.getRelatedModuleField();

        if (query.context) {
            params.offset = this.searchCollection.next_offset;
        }

        //console.log(params);
        params.filter = this.buildFilterDefinition(term);

        this.searchCollection.fetch({
            //Don't show alerts for this request
            showAlerts: false,
            update: true,
            remove: _.isUndefined(params.offset),
            fields: this.getSearchFields(),
            context: self,
            params: params,
            //limit: limit,
            success: function (data) {
                var fetch = { results: [], more: data.next_offset > 0, context: data };
                if (fetch.more) {
                    var fieldEl = self.$(self.fieldTag),
                        //For teamset widget, we should specify which index element to be filled in
                        plugin = (fieldEl.length > 1) ? $(fieldEl.get(self.currentIndex)).data("select2") : fieldEl.data("select2"),
                        height = plugin.searchmore.children("li:first").children(":first").outerHeight(),
                        //0.2 makes scroll not to touch the bottom line which avoid fetching next record set
                        maxHeight = height * (limit - .2);
                    plugin.results.css("max-height", maxHeight);
                }
                _.each(data.models, function (model, index) {
                    if (params.offset && index < params.offset) {
                        return;
                    }
                    fetch.results.push({
                        id: model.id,
                        text: model.get(relatedModuleField) + ''
                    });
                });
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback(fetch);
                }
            },
            error: function () {
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback({ results: [] });
                }
                app.logger.error("Unable to fetch the bean collection.");
            }
        });

    }, app.config.requiredElapsed || 500),

    _getCustomFilterEE: function (searchTerm) {

        return [{
            'name': {
                '$contains': 'Scale',
            },
            'serial_number_c': {
                '$starts': searchTerm,
            },

        }];
    },
    /**
     * @override
     */
    buildFilterDefinition: function (searchTerm) {
        var parentFilter = this._super('buildFilterDefinition', [searchTerm]);

        if (this.getSearchModule() == 'Equip_Equipment') {
            //console.log('aaa,', parentFilter.concat(this._getCustomFilterEE(searchTerm)));
            return this._getCustomFilterEE(searchTerm);
        } else {
            return parentFilter;
        }
    },
    /**EOC Disable search filter code */
})