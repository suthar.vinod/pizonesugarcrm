({
    extendsFrom: 'CreateView',
    initialize: function (options) {

        this._super('initialize', [options]);
        this.model.on("change:anml_animals_w_weight_1_name", this.get_study_id, this);
    },

    /*1387 Get WP id from linked latest WPA*/
    get_study_id: function () {
        var anml_animals_w_weight_1anml_animals_ida = this.model.get('anml_animals_w_weight_1anml_animals_ida');
        var module_Id = this.model.get('id');

        if (anml_animals_w_weight_1anml_animals_ida != '') {
            console.log('1');
            var url = app.api.buildURL("getStudyId");
            var method = "create";
            var data = {
                module: 'W_Weight',
                moduleId: module_Id,
                animalId: anml_animals_w_weight_1anml_animals_ida,
            };

            var callback = {
                success: _.bind(function successCB(res) {
                    //console.log('data', res);
                    this.model.set('study_id', res);
                }, this)
            };
            app.api.call(method, url, data, callback);

        } else {
            this.model.set('study_id', '');
        }

    },
})