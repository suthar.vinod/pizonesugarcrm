<?php
$module_name = 'W_Weight';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'anml_animals_w_weight_1_name',
                'label' => 'LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_ANML_ANIMALS_TITLE',
                'enabled' => true,
                'id' => 'ANML_ANIMALS_W_WEIGHT_1ANML_ANIMALS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'date_2_c',
                'label' => 'LBL_DATE_2',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'weight',
                'label' => 'LBL_WEIGHT',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'weight_unit',
                'label' => 'LBL_WEIGHT_UNIT',
                'enabled' => true,
                'readonly' => true,
                'id' => 'U_UNITS_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'perc_wt_chng_c',
                'label' => 'LBL_PERC_WT_CHNG',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'protocol_min_weight',
                'label' => 'LBL_PROTOCOL_MIN_WEIGHT',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'protocol_max_weight',
                'label' => 'LBL_PROTOCOL_MAX_WEIGHT',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'weight_meets_pro_req',
                'label' => 'LBL_WEIGHT_MEETS_PRO_REQ',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
