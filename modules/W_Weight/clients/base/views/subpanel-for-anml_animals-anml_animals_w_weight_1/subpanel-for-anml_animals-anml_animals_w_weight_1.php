<?php
// created: 2021-08-19 07:17:30
$viewdefs['W_Weight']['base']['view']['subpanel-for-anml_animals-anml_animals_w_weight_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'type_2',
          'label' => 'LBL_TYPE_2',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'weight',
          'label' => 'LBL_WEIGHT',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'weight_unit',
          'label' => 'LBL_WEIGHT_UNIT',
          'enabled' => true,
          'readonly' => true,
          'id' => 'U_UNITS_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'perc_wt_chng_c',
          'label' => 'LBL_PERC_WT_CHNG',
          'enabled' => true,
          'readonly' => '1',
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);