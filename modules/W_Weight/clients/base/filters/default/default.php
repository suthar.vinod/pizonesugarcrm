<?php
// created: 2021-09-22 05:20:08
$viewdefs['W_Weight']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'date_entered' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'current_wp_assignment_c' => 
    array (
    ),
    'date_2_c' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'name' => 
    array (
    ),
    'perc_wt_chng_c' => 
    array (
    ),
    'protocol_max_weight' => 
    array (
    ),
    'protocol_min_weight' => 
    array (
    ),
    'scale_used' => 
    array (
    ),
    'anml_animals_w_weight_1_name' => 
    array (
    ),
    'type_2' => 
    array (
    ),
    'weight' => 
    array (
    ),
    'weight_meets_pro_req' => 
    array (
    ),
    'weight_unit' => 
    array (
    ),
  ),
);