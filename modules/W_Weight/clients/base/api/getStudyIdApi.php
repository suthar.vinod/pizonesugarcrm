<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class getStudyIdApi extends SugarApi {

    public function registerApiRest() {
        return array(
            'getStudyIdData' => array(
                'reqType' => 'POST',
                'noLoginRequired' => false,
                'path' => array('getStudyId'),
                'pathVars' => array('getStudyId'),
                'method' => 'getStudyId',
                'shortHelp' => '',
                'longHelp' => '',
            ),
        );
    }

    
    function getStudyId($api, $args) {
		global $db,$current_user;
		 
		$module = $args['module'];
		$moduleId = $args['moduleId'];
		$animalId = $args['animalId'];
		
		$wpe_sql = "SELECT WP.name AS WPName
						FROM anml_animals_wpe_work_product_enrollment_1_c AS TSWPA
						LEFT JOIN m03_work_product_wpe_work_product_enrollment_1_c AS WPWPA 
						ON TSWPA.anml_anima9941ollment_idb = WPWPA.m03_work_p9bf5ollment_idb
						LEFT JOIN m03_work_product AS WP ON  WPWPA.m03_work_p7d13product_ida = WP.id
						WHERE TSWPA.anml_animals_wpe_work_product_enrollment_1anml_animals_ida = '".$animalId."'
						AND TSWPA.deleted = 0 ORDER BY TSWPA.date_modified DESC LIMIT 1";
			$wpe_exec = $db->query($wpe_sql);
			if ($wpe_exec->num_rows > 0) 
			{ 
				$resultWP = $db->fetchByAssoc($wpe_exec);
				$study_id = $resultWP['WPName'];
				return $study_id;
			}
		 
    }
	
}

