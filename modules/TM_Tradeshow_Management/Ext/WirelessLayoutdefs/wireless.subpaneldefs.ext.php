<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/WirelessLayoutdefs/tm_tradeshow_management_documents_1_TM_Tradeshow_Management.php

 // created: 2018-01-18 22:47:15
$layout_defs["TM_Tradeshow_Management"]["subpanel_setup"]['tm_tradeshow_management_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'tm_tradeshow_management_documents_1',
);

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/WirelessLayoutdefs/tm_tradeshow_management_ta_tradeshow_activities_1_TM_Tradeshow_Management.php

 // created: 2018-04-23 17:10:34
$layout_defs["TM_Tradeshow_Management"]["subpanel_setup"]['tm_tradeshow_management_ta_tradeshow_activities_1'] = array (
  'order' => 100,
  'module' => 'TA_Tradeshow_Activities',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'get_subpanel_data' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
);

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/WirelessLayoutdefs/tm_tradeshow_management_m01_sales_1_TM_Tradeshow_Management.php

 // created: 2018-04-23 18:02:37
$layout_defs["TM_Tradeshow_Management"]["subpanel_setup"]['tm_tradeshow_management_m01_sales_1'] = array (
  'order' => 100,
  'module' => 'M01_Sales',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_M01_SALES_TITLE',
  'get_subpanel_data' => 'tm_tradeshow_management_m01_sales_1',
);

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/WirelessLayoutdefs/tm_tradeshow_management_td_tradeshow_documents_1_TM_Tradeshow_Management.php

 // created: 2019-02-13 23:29:07
$layout_defs["TM_Tradeshow_Management"]["subpanel_setup"]['tm_tradeshow_management_td_tradeshow_documents_1'] = array (
  'order' => 100,
  'module' => 'TD_Tradeshow_Documents',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_TM_TRADESHOW_MANAGEMENT_TD_TRADESHOW_DOCUMENTS_1_FROM_TD_TRADESHOW_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'tm_tradeshow_management_td_tradeshow_documents_1',
);

?>
