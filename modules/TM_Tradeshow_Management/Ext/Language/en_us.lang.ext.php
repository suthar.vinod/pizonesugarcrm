<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_RECORD'] = 'Create Tradeshow_Management';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Tradeshow_Management';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Tradeshow_Management';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Tradeshow_Management vCard';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Tradeshow_Management List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Tradeshow_Management';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Tradeshow_Management';
$mod_strings['LBL_LOCATION'] = 'Location';
$mod_strings['LBL_LOCATION_STREET'] = 'Location Street';
$mod_strings['LBL_LOCATION_CITY'] = 'Location City';
$mod_strings['LBL_LOCATION_STATE'] = 'Location State';
$mod_strings['LBL_LOCATION_POSTALCODE'] = 'Location PostalCode';
$mod_strings['LBL_LOCATION_COUNTRY'] = 'Location Country';
$mod_strings['LBL_START_DATE'] = 'Start Date';
$mod_strings['LBL_END_DATE'] = 'End Date';
$mod_strings['LBL_CONFERENCE_WEBSITE'] = 'Conference Website';
$mod_strings['LBL_SALES_FOCUS'] = 'Sales Focus';
$mod_strings['LBL_REGISTRATION_COMPLETED'] = 'Conference Registration Completed';
$mod_strings['LBL_PRIMARY_ACTIVITY'] = 'Primary Activity';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_ESTIMATED_COST'] = 'Estimated Cost';
$mod_strings['LBL_BD_ATTENDEES'] = 'BD Attendees';
$mod_strings['LBL_HOTEL_RESERVATION_COMPLETED'] = 'Hotel Reservation Completed';
$mod_strings['LBL_HOTEL_NAME'] = 'Hotel Name';
$mod_strings['LBL_FLIGHT_RESERVATIONS_COMPLETE'] = 'Flight Reservations Complete';
$mod_strings['LBL_ASSIGNED_TO'] = 'Tradeshow Lead';
$mod_strings['LBL_ASSIGNED_TO_NAME'] = 'Tradeshow Lead';
$mod_strings['LBL_TRADESHOW_CONTACT'] = 'Tradeshow Contact';
$mod_strings['LBL_CONTACT_EMAIL_ADDESS'] = 'Contact Email Addess';
$mod_strings['LBL_BOOTH_NUMBER'] = 'Booth Number';
$mod_strings['LBL_BADGE_REGISTRATION_COMPLETE'] = 'Badge Registration Complete';
$mod_strings['LBL_TYPE_OF_BANNER'] = 'Type of Display';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Tradeshow Logistics';
$mod_strings['LBL_RECORD_BODY'] = 'Tradeshow Description';
$mod_strings['LBL_BOOTH_FURNISHINGS_ORDERED'] = 'Booth Furnishings Ordered';
$mod_strings['LNK_LIST'] = 'View Tradeshow Management';
$mod_strings['LBL_MODULE_NAME'] = 'Tradeshow Management';
$mod_strings['LNK_IMPORT_TM_TRADESHOW_MANAGEMENT'] = 'Import Tradeshow Management';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_SUBPANEL_TITLE'] = 'Tradeshow Management';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE'] = 'Sales';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_BADGE_ALLOTMENT'] = 'Badge Allotment';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_FOCUS_DRAWER_DASHBOARD'] = 'Tradeshow Management Focus Drawer';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_RECORD_DASHBOARD'] = 'Tradeshow Management Record Dashboard';

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Language/en_us.customtm_tradeshow_management_m01_quote_document_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'Tradeshow Documents';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_M01_QUOTE_DOCUMENT_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE'] = 'Tradeshow Documents';

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Language/en_us.customtm_tradeshow_management_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Tradeshow Documents';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE'] = 'Tradeshow Documents';

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Language/en_us.customcontacts_tm_tradeshow_management_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTS_TM_TRADESHOW_MANAGEMENT_1_FROM_CONTACTS_TITLE'] = 'Tradeshow Activity';
$mod_strings['LBL_CONTACTS_TM_TRADESHOW_MANAGEMENT_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE'] = 'Tradeshow Activity';

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Language/en_us.customtm_tradeshow_management_ta_tradeshow_activities_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE'] = 'Tradeshow Activities';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE'] = 'Tradeshow Activities';

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Language/en_us.customtm_tradeshow_management_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE'] = 'Sales Activities';

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Language/en_us.customtm_tradeshow_management_td_tradeshow_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_TD_TRADESHOW_DOCUMENTS_1_FROM_TD_TRADESHOW_DOCUMENTS_TITLE'] = 'Tradeshow Documents';
$mod_strings['LBL_TM_TRADESHOW_MANAGEMENT_TD_TRADESHOW_DOCUMENTS_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE'] = 'Tradeshow Documents';

?>
