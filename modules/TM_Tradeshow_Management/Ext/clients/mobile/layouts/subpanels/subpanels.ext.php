<?php
// WARNING: The contents of this file are auto-generated.


// created: 2018-01-18 22:47:15
$viewdefs['TM_Tradeshow_Management']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'context' => 
  array (
    'link' => 'tm_tradeshow_management_documents_1',
  ),
);

// created: 2018-04-23 18:02:37
$viewdefs['TM_Tradeshow_Management']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_M01_SALES_TITLE',
  'context' => 
  array (
    'link' => 'tm_tradeshow_management_m01_sales_1',
  ),
);

// created: 2018-04-23 17:10:34
$viewdefs['TM_Tradeshow_Management']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'context' => 
  array (
    'link' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  ),
);

// created: 2019-02-13 23:29:07
$viewdefs['TM_Tradeshow_Management']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TM_TRADESHOW_MANAGEMENT_TD_TRADESHOW_DOCUMENTS_1_FROM_TD_TRADESHOW_DOCUMENTS_TITLE',
  'context' => 
  array (
    'link' => 'tm_tradeshow_management_td_tradeshow_documents_1',
  ),
);