<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_currency_id.php

 // created: 2018-01-18 16:58:57

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2018-01-18 16:58:58

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/tm_tradeshow_management_documents_1_TM_Tradeshow_Management.php

// created: 2018-01-18 22:47:15
$dictionary["TM_Tradeshow_Management"]["fields"]["tm_tradeshow_management_documents_1"] = array (
  'name' => 'tm_tradeshow_management_documents_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_DOCUMENTS_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'id_name' => 'tm_tradeshow_management_documents_1tm_tradeshow_management_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/tm_tradeshow_management_ta_tradeshow_activities_1_TM_Tradeshow_Management.php

// created: 2018-04-23 17:10:34
$dictionary["TM_Tradeshow_Management"]["fields"]["tm_tradeshow_management_ta_tradeshow_activities_1"] = array (
  'name' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_ta_tradeshow_activities_1',
  'source' => 'non-db',
  'module' => 'TA_Tradeshow_Activities',
  'bean_name' => 'TA_Tradeshow_Activities',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TA_TRADESHOW_ACTIVITIES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'id_name' => 'tm_tradesh0f8eagement_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/tm_tradeshow_management_m01_sales_1_TM_Tradeshow_Management.php

// created: 2018-04-23 18:02:37
$dictionary["TM_Tradeshow_Management"]["fields"]["tm_tradeshow_management_m01_sales_1"] = array (
  'name' => 'tm_tradeshow_management_m01_sales_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_M01_SALES_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'id_name' => 'tm_tradeshow_management_m01_sales_1tm_tradeshow_management_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/tm_tradeshow_management_td_tradeshow_documents_1_TM_Tradeshow_Management.php

// created: 2019-02-13 23:29:07
$dictionary["TM_Tradeshow_Management"]["fields"]["tm_tradeshow_management_td_tradeshow_documents_1"] = array (
  'name' => 'tm_tradeshow_management_td_tradeshow_documents_1',
  'type' => 'link',
  'relationship' => 'tm_tradeshow_management_td_tradeshow_documents_1',
  'source' => 'non-db',
  'module' => 'TD_Tradeshow_Documents',
  'bean_name' => 'TD_Tradeshow_Documents',
  'vname' => 'LBL_TM_TRADESHOW_MANAGEMENT_TD_TRADESHOW_DOCUMENTS_1_FROM_TM_TRADESHOW_MANAGEMENT_TITLE',
  'id_name' => 'tm_tradeshb931agement_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_name.php

 // created: 2019-04-23 12:21:22
$dictionary['TM_Tradeshow_Management']['fields']['name']['len']='255';
$dictionary['TM_Tradeshow_Management']['fields']['name']['audited']=true;
$dictionary['TM_Tradeshow_Management']['fields']['name']['massupdate']=false;
$dictionary['TM_Tradeshow_Management']['fields']['name']['unified_search']=false;
$dictionary['TM_Tradeshow_Management']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['TM_Tradeshow_Management']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_location_city_c.php

 // created: 2019-04-23 12:22:13
$dictionary['TM_Tradeshow_Management']['fields']['location_city_c']['labelValue']='Location City';
$dictionary['TM_Tradeshow_Management']['fields']['location_city_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['location_city_c']['group']='location_c';
$dictionary['TM_Tradeshow_Management']['fields']['location_city_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['location_city_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_location_state_c.php

 // created: 2019-04-23 12:22:45
$dictionary['TM_Tradeshow_Management']['fields']['location_state_c']['labelValue']='Location State';
$dictionary['TM_Tradeshow_Management']['fields']['location_state_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['location_state_c']['group']='location_c';
$dictionary['TM_Tradeshow_Management']['fields']['location_state_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['location_state_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_location_country_c.php

 // created: 2019-04-23 12:23:14
$dictionary['TM_Tradeshow_Management']['fields']['location_country_c']['labelValue']='Location Country';
$dictionary['TM_Tradeshow_Management']['fields']['location_country_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['location_country_c']['group']='location_c';
$dictionary['TM_Tradeshow_Management']['fields']['location_country_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['location_country_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_start_date_c.php

 // created: 2019-04-23 12:23:54
$dictionary['TM_Tradeshow_Management']['fields']['start_date_c']['labelValue']='Start Date';
$dictionary['TM_Tradeshow_Management']['fields']['start_date_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['start_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_end_date_c.php

 // created: 2019-04-23 12:24:39
$dictionary['TM_Tradeshow_Management']['fields']['end_date_c']['labelValue']='End Date';
$dictionary['TM_Tradeshow_Management']['fields']['end_date_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['end_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_conference_website_c.php

 // created: 2019-04-23 12:25:44
$dictionary['TM_Tradeshow_Management']['fields']['conference_website_c']['labelValue']='Conference Website';
$dictionary['TM_Tradeshow_Management']['fields']['conference_website_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['conference_website_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_sales_focus_c.php

 // created: 2019-04-23 12:26:26
$dictionary['TM_Tradeshow_Management']['fields']['sales_focus_c']['labelValue']='Sales Focus';
$dictionary['TM_Tradeshow_Management']['fields']['sales_focus_c']['dependency']='';
$dictionary['TM_Tradeshow_Management']['fields']['sales_focus_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_primary_activity_c.php

 // created: 2019-04-23 12:27:12
$dictionary['TM_Tradeshow_Management']['fields']['primary_activity_c']['labelValue']='Primary Activity';
$dictionary['TM_Tradeshow_Management']['fields']['primary_activity_c']['dependency']='';
$dictionary['TM_Tradeshow_Management']['fields']['primary_activity_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_booth_number_c.php

 // created: 2019-04-23 12:27:57
$dictionary['TM_Tradeshow_Management']['fields']['booth_number_c']['labelValue']='Booth Number';
$dictionary['TM_Tradeshow_Management']['fields']['booth_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['booth_number_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['booth_number_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_estimated_cost_c.php

 // created: 2019-04-23 12:28:40
$dictionary['TM_Tradeshow_Management']['fields']['estimated_cost_c']['labelValue']='Estimated Cost';
$dictionary['TM_Tradeshow_Management']['fields']['estimated_cost_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['estimated_cost_c']['dependency']='';
$dictionary['TM_Tradeshow_Management']['fields']['estimated_cost_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_tradeshow_contact_c.php

 // created: 2019-04-23 12:29:25
$dictionary['TM_Tradeshow_Management']['fields']['tradeshow_contact_c']['labelValue']='Tradeshow Contact';
$dictionary['TM_Tradeshow_Management']['fields']['tradeshow_contact_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['tradeshow_contact_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['tradeshow_contact_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_contact_email_addess_c.php

 // created: 2019-04-23 12:30:09
$dictionary['TM_Tradeshow_Management']['fields']['contact_email_addess_c']['labelValue']='Contact Email Addess';
$dictionary['TM_Tradeshow_Management']['fields']['contact_email_addess_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['contact_email_addess_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['contact_email_addess_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_type_of_banner_c.php

 // created: 2019-04-23 12:30:42
$dictionary['TM_Tradeshow_Management']['fields']['type_of_banner_c']['labelValue']='Type of Display';
$dictionary['TM_Tradeshow_Management']['fields']['type_of_banner_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['type_of_banner_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['type_of_banner_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_bd_attendees_c.php

 // created: 2019-04-23 12:32:51
$dictionary['TM_Tradeshow_Management']['fields']['bd_attendees_c']['labelValue']='BD Attendees';
$dictionary['TM_Tradeshow_Management']['fields']['bd_attendees_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['bd_attendees_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['bd_attendees_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_hotel_name_c.php

 // created: 2019-04-23 12:34:56
$dictionary['TM_Tradeshow_Management']['fields']['hotel_name_c']['labelValue']='Hotel Name';
$dictionary['TM_Tradeshow_Management']['fields']['hotel_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['hotel_name_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['hotel_name_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_badge_allotment_c.php

 // created: 2020-02-12 12:26:28
$dictionary['TM_Tradeshow_Management']['fields']['badge_allotment_c']['labelValue']='Badge Allotment';
$dictionary['TM_Tradeshow_Management']['fields']['badge_allotment_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['TM_Tradeshow_Management']['fields']['badge_allotment_c']['enforced']='';
$dictionary['TM_Tradeshow_Management']['fields']['badge_allotment_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_booth_furnishings_ordered_c.php

 // created: 2020-05-21 17:09:46
$dictionary['TM_Tradeshow_Management']['fields']['booth_furnishings_ordered_c']['labelValue']='Booth Furnishings Ordered';
$dictionary['TM_Tradeshow_Management']['fields']['booth_furnishings_ordered_c']['dependency']='';
$dictionary['TM_Tradeshow_Management']['fields']['booth_furnishings_ordered_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_badge_registration_complete_c.php

 // created: 2020-06-11 10:10:22
$dictionary['TM_Tradeshow_Management']['fields']['badge_registration_complete_c']['labelValue']='Badge Registration Complete';
$dictionary['TM_Tradeshow_Management']['fields']['badge_registration_complete_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_hotel_reservation_completed_c.php

 // created: 2020-06-11 10:11:13
$dictionary['TM_Tradeshow_Management']['fields']['hotel_reservation_completed_c']['labelValue']='Hotel Reservation Completed';
$dictionary['TM_Tradeshow_Management']['fields']['hotel_reservation_completed_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_registration_completed_c.php

 // created: 2020-06-11 10:11:59
$dictionary['TM_Tradeshow_Management']['fields']['registration_completed_c']['labelValue']='Conference Registration Completed';
$dictionary['TM_Tradeshow_Management']['fields']['registration_completed_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_flight_reservations_complete_c.php

 // created: 2020-06-11 10:12:51
$dictionary['TM_Tradeshow_Management']['fields']['flight_reservations_complete_c']['labelValue']='Flight Reservations Complete';
$dictionary['TM_Tradeshow_Management']['fields']['flight_reservations_complete_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/TM_Tradeshow_Management/Ext/Vardefs/sugarfield_description.php

 // created: 2021-01-21 06:56:36
$dictionary['TM_Tradeshow_Management']['fields']['description']['audited']=true;
$dictionary['TM_Tradeshow_Management']['fields']['description']['massupdate']=false;
$dictionary['TM_Tradeshow_Management']['fields']['description']['hidemassupdate']=false;
$dictionary['TM_Tradeshow_Management']['fields']['description']['comments']='Full text of the note';
$dictionary['TM_Tradeshow_Management']['fields']['description']['duplicate_merge']='enabled';
$dictionary['TM_Tradeshow_Management']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['TM_Tradeshow_Management']['fields']['description']['merge_filter']='disabled';
$dictionary['TM_Tradeshow_Management']['fields']['description']['unified_search']=false;
$dictionary['TM_Tradeshow_Management']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['TM_Tradeshow_Management']['fields']['description']['calculated']=false;
$dictionary['TM_Tradeshow_Management']['fields']['description']['rows']='6';
$dictionary['TM_Tradeshow_Management']['fields']['description']['cols']='80';

 
?>
