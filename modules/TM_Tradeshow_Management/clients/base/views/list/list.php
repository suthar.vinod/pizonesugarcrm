<?php
$module_name = 'TM_Tradeshow_Management';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sales_focus_c',
                'label' => 'LBL_SALES_FOCUS',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'registration_completed_c',
                'label' => 'LBL_REGISTRATION_COMPLETED',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              4 => 
              array (
                'name' => 'location_city_c',
                'label' => 'LBL_LOCATION_CITY',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'location_state_c',
                'label' => 'LBL_LOCATION_STATE',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'location_country_c',
                'label' => 'LBL_LOCATION_COUNTRY',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'start_date_c',
                'label' => 'LBL_START_DATE',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'end_date_c',
                'label' => 'LBL_END_DATE',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'type_of_banner_c',
                'label' => 'LBL_TYPE_OF_BANNER',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'primary_activity_c',
                'label' => 'LBL_PRIMARY_ACTIVITY',
                'enabled' => true,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'bd_attendees_c',
                'label' => 'LBL_BD_ATTENDEES',
                'enabled' => true,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => false,
              ),
              13 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => false,
              ),
              14 => 
              array (
                'name' => 'hotel_reservation_completed_c',
                'label' => 'LBL_HOTEL_RESERVATION_COMPLETED',
                'enabled' => true,
                'default' => false,
              ),
              15 => 
              array (
                'name' => 'flight_reservations_complete_c',
                'label' => 'LBL_FLIGHT_RESERVATIONS_COMPLETE',
                'enabled' => true,
                'default' => false,
              ),
              16 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
