<?php
$module_name = 'TM_Tradeshow_Management';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'TM_Tradeshow_Management',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'location_city_c',
                'label' => 'LBL_LOCATION_CITY',
              ),
              1 => 
              array (
                'name' => 'location_state_c',
                'label' => 'LBL_LOCATION_STATE',
              ),
              2 => 
              array (
                'name' => 'location_country_c',
                'label' => 'LBL_LOCATION_COUNTRY',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'name' => 'start_date_c',
                'label' => 'LBL_START_DATE',
              ),
              5 => 
              array (
                'name' => 'end_date_c',
                'label' => 'LBL_END_DATE',
              ),
              6 => 
              array (
                'name' => 'conference_website_c',
                'label' => 'LBL_CONFERENCE_WEBSITE',
              ),
              7 => 
              array (
                'name' => 'sales_focus_c',
                'label' => 'LBL_SALES_FOCUS',
              ),
              8 => 
              array (
                'name' => 'primary_activity_c',
                'label' => 'LBL_PRIMARY_ACTIVITY',
              ),
              9 => 
              array (
                'name' => 'booth_number_c',
                'label' => 'LBL_BOOTH_NUMBER',
              ),
              10 => 'assigned_user_name',
              11 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'estimated_cost_c',
                'label' => 'LBL_ESTIMATED_COST',
              ),
              12 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'tradeshow_contact_c',
                'label' => 'LBL_TRADESHOW_CONTACT',
              ),
              1 => 
              array (
                'name' => 'contact_email_addess_c',
                'label' => 'LBL_CONTACT_EMAIL_ADDESS',
              ),
              2 => 
              array (
                'name' => 'type_of_banner_c',
                'label' => 'LBL_TYPE_OF_BANNER',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'name' => 'registration_completed_c',
                'label' => 'LBL_REGISTRATION_COMPLETED',
              ),
              5 => 
              array (
                'name' => 'badge_registration_complete_c',
                'label' => 'LBL_BADGE_REGISTRATION_COMPLETE',
              ),
              6 => 
              array (
                'name' => 'bd_attendees_c',
                'label' => 'LBL_BD_ATTENDEES',
              ),
              7 => 
              array (
              ),
              8 => 
              array (
                'name' => 'hotel_reservation_completed_c',
                'label' => 'LBL_HOTEL_RESERVATION_COMPLETED',
              ),
              9 => 
              array (
                'name' => 'hotel_name_c',
                'label' => 'LBL_HOTEL_NAME',
              ),
              10 => 
              array (
                'name' => 'flight_reservations_complete_c',
                'label' => 'LBL_FLIGHT_RESERVATIONS_COMPLETE',
              ),
              11 => 
              array (
                'name' => 'booth_furnishings_ordered_c',
                'label' => 'LBL_BOOTH_FURNISHINGS_ORDERED',
              ),
              12 => 
              array (
                'name' => 'badge_allotment_c',
                'label' => 'LBL_BADGE_ALLOTMENT',
              ),
              13 => 
              array (
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
