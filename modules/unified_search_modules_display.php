<?php
// created: 2022-12-11 04:10:14
$unified_search_modules_display = array (
  'Accounts' => 
  array (
    'visible' => true,
  ),
  'Calls' => 
  array (
    'visible' => true,
  ),
  'Contacts' => 
  array (
    'visible' => true,
  ),
  'Documents' => 
  array (
    'visible' => true,
  ),
  'M01_Sales' => 
  array (
    'visible' => true,
  ),
  'M03_Work_Product' => 
  array (
    'visible' => true,
  ),
  'Manufacturers' => 
  array (
    'visible' => true,
  ),
  'M01_Quote_Document' => 
  array (
    'visible' => true,
  ),
  'Meetings' => 
  array (
    'visible' => true,
  ),
  'Notes' => 
  array (
    'visible' => true,
  ),
  'Opportunities' => 
  array (
    'visible' => true,
  ),
  'ProductCategories' => 
  array (
    'visible' => true,
  ),
  'Products' => 
  array (
    'visible' => true,
  ),
  'Bugs' => 
  array (
    'visible' => false,
  ),
  'Campaigns' => 
  array (
    'visible' => false,
  ),
  'Cases' => 
  array (
    'visible' => false,
  ),
  'Contracts' => 
  array (
    'visible' => false,
  ),
  'KBDocuments' => 
  array (
    'visible' => false,
  ),
  'Leads' => 
  array (
    'visible' => false,
  ),
  'Project' => 
  array (
    'visible' => false,
  ),
  'ProjectTask' => 
  array (
    'visible' => false,
  ),
  'ProspectLists' => 
  array (
    'visible' => false,
  ),
  'Prospects' => 
  array (
    'visible' => false,
  ),
  'Quotes' => 
  array (
    'visible' => false,
  ),
  'Tasks' => 
  array (
    'visible' => false,
  ),
);