<?php
// created: 2021-04-20 09:11:54
$viewdefs['M03_Work_Product_Code']['base']['view']['subpanel-for-wpg_work_product_group-wpg_work_product_group_m03_work_product_code_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'wp_code_description_c',
          'label' => 'LBL_WP_CODE_DESCRIPTION',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'functional_area_c',
          'label' => 'LBL_FUNCTIONAL_AREA',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'total_animal_number_c',
          'label' => 'LBL_TOTAL_ANIMAL_NUMBER',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);