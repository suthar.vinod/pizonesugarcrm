<?php
// created: 2021-12-07 12:09:01
$viewdefs['M03_Work_Product_Code']['base']['view']['subpanel-for-tc_namsa_test_codes-tc_namsa_test_codes_m03_work_product_code_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'wp_code_description_c',
          'label' => 'LBL_WP_CODE_DESCRIPTION',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);