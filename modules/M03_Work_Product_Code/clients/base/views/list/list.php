<?php
$module_name = 'M03_Work_Product_Code';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
                'width' => 'xsmall',
              ),
              1 => 
              array (
                'name' => 'functional_area_c',
                'label' => 'LBL_FUNCTIONAL_AREA',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'wp_code_description_c',
                'label' => 'LBL_WP_CODE_DESCRIPTION',
                'enabled' => true,
                'default' => true,
                'width' => 'large',
              ),
              3 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'applicable_standard_protocol_c',
                'label' => 'LBL_APPLICABLE_STANDARD_PROTOCOL',
                'enabled' => true,
                'id' => 'ERD_ERROR_DOCUMENTS_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'wpg_work_product_group_m03_work_product_code_1_name',
                'label' => 'LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_WPG_WORK_PRODUCT_GROUP_TITLE',
                'enabled' => true,
                'id' => 'WPG_WORK_P164AT_GROUP_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'total_animal_number_c',
                'label' => 'LBL_TOTAL_ANIMAL_NUMBER',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'wpc_type_c',
                'label' => 'LBL_WPC_TYPE',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
