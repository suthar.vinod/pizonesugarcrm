<?php
$viewdefs['M03_Work_Product_Code'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'recorddashlet' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'wp_code_description_c',
                'label' => 'LBL_WP_CODE_DESCRIPTION',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'functional_area_c',
                'label' => 'LBL_FUNCTIONAL_AREA',
              ),
              2 => 
              array (
                'name' => 'applicable_standard_protocol_c',
                'studio' => 'visible',
                'label' => 'LBL_APPLICABLE_STANDARD_PROTOCOL',
              ),
              3 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              4 => 
              array (
                'name' => 'total_animal_number_c',
                'label' => 'LBL_TOTAL_ANIMAL_NUMBER',
              ),
              5 => 
              array (
              ),
              6 => 
              array (
                'name' => 'wpg_work_product_group_m03_work_product_code_1_name',
              ),
              7 => 
              array (
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
