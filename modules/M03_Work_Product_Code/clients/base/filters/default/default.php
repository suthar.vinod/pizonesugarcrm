<?php
// created: 2022-05-24 07:08:43
$viewdefs['M03_Work_Product_Code']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'applicable_standard_protocol_c' => 
    array (
    ),
    'application_cpi_avg_hours_c' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'evaluation_cpi_avg_hours_c' => 
    array (
    ),
    'final_report_cpi_avg_hours_c' => 
    array (
    ),
    'functional_area_c' => 
    array (
    ),
    'sample_preparation_cpi_avg_h_c' => 
    array (
    ),
    'test_price_c' => 
    array (
    ),
    'total_animal_number_c' => 
    array (
    ),
    'name' => 
    array (
    ),
    'wpg_work_product_group_m03_work_product_code_1_name' => 
    array (
    ),
    'wp_code_description_c' => 
    array (
    ),
    'wpc_type_c' => 
    array (
    ),
  ),
);