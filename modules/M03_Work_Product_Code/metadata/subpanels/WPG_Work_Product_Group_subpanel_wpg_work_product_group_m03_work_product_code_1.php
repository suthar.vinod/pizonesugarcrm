<?php
// created: 2021-04-20 09:11:54
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'wp_code_description_c' => 
  array (
    'readonly' => false,
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_WP_CODE_DESCRIPTION',
    'width' => 10,
  ),
  'functional_area_c' => 
  array (
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_FUNCTIONAL_AREA',
    'width' => 10,
  ),
  'total_animal_number_c' => 
  array (
    'readonly' => false,
    'type' => 'int',
    'vname' => 'LBL_TOTAL_ANIMAL_NUMBER',
    'width' => 10,
    'default' => true,
  ),
);