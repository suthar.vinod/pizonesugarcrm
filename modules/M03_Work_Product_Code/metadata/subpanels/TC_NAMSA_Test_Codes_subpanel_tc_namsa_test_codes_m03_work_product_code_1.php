<?php
// created: 2021-12-07 12:09:01
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'wp_code_description_c' => 
  array (
    'readonly' => false,
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_WP_CODE_DESCRIPTION',
    'width' => 10,
  ),
);