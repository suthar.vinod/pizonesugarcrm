<?php
$popupMeta = array (
    'moduleMain' => 'M03_Work_Product_Code',
    'varName' => 'M03_Work_Product_Code',
    'orderBy' => 'm03_work_product_code.name',
    'whereClauses' => array (
  'name' => 'm03_work_product_code.name',
  'wp_code_description_c' => 'm03_work_product_code_cstm.wp_code_description_c',
  'functional_area_c' => 'm03_work_product_code_cstm.functional_area_c',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'wp_code_description_c',
  7 => 'functional_area_c',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10',
  ),
  'functional_area_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_FUNCTIONAL_AREA',
    'width' => 10,
    'name' => 'functional_area_c',
  ),
  'wp_code_description_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_WP_CODE_DESCRIPTION',
    'width' => '10',
    'name' => 'wp_code_description_c',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'FUNCTIONAL_AREA_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_FUNCTIONAL_AREA',
    'width' => 10,
  ),
  'WP_CODE_DESCRIPTION_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_WP_CODE_DESCRIPTION',
    'width' => 10,
    'name' => 'wp_code_description_c',
  ),
),
);
