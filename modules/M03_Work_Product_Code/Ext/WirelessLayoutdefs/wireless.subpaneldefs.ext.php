<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/WirelessLayoutdefs/m03_work_product_m03_work_product_code_1_M03_Work_Product_Code.php

 // created: 2021-01-21 14:15:48
$layout_defs["M03_Work_Product_Code"]["subpanel_setup"]['m03_work_product_m03_work_product_code_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'm03_work_product_m03_work_product_code_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/WirelessLayoutdefs/m03_work_product_code_taskd_task_design_1_M03_Work_Product_Code.php

 // created: 2021-12-02 09:36:03
$layout_defs["M03_Work_Product_Code"]["subpanel_setup"]['m03_work_product_code_taskd_task_design_1'] = array (
  'order' => 100,
  'module' => 'TaskD_Task_Design',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'get_subpanel_data' => 'm03_work_product_code_taskd_task_design_1',
);

?>
