<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:56
$dictionary['M03_Work_Product_Code']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_name.php

 // created: 2016-10-25 03:27:08
$dictionary['M03_Work_Product_Code']['fields']['name']['len'] = '255';
$dictionary['M03_Work_Product_Code']['fields']['name']['audited'] = true;
$dictionary['M03_Work_Product_Code']['fields']['name']['massupdate'] = false;
$dictionary['M03_Work_Product_Code']['fields']['name']['unified_search'] = false;
$dictionary['M03_Work_Product_Code']['fields']['name']['full_text_search']['enabled'] = true;
$dictionary['M03_Work_Product_Code']['fields']['name']['full_text_search']['searchable'] = true;
$dictionary['M03_Work_Product_Code']['fields']['name']['full_text_search']['boost'] = 1.5500000000000000444089209850062616169452667236328125;
$dictionary['M03_Work_Product_Code']['fields']['name']['calculated'] = false;


?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_wp_code_description_c.php

 // created: 2016-10-25 03:27:08
$dictionary['M03_Work_Product_Code']['fields']['wp_code_description_c']['labelValue'] = 'WP Code Description';
$dictionary['M03_Work_Product_Code']['fields']['wp_code_description_c']['enforced'] = '';
$dictionary['M03_Work_Product_Code']['fields']['wp_code_description_c']['full_text_search']['boost'] = 1;


?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_description.php

 // created: 2019-08-13 12:11:32
$dictionary['M03_Work_Product_Code']['fields']['description']['audited']=true;
$dictionary['M03_Work_Product_Code']['fields']['description']['massupdate']=false;
$dictionary['M03_Work_Product_Code']['fields']['description']['comments']='Full text of the note';
$dictionary['M03_Work_Product_Code']['fields']['description']['duplicate_merge']='enabled';
$dictionary['M03_Work_Product_Code']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['M03_Work_Product_Code']['fields']['description']['merge_filter']='disabled';
$dictionary['M03_Work_Product_Code']['fields']['description']['unified_search']=false;
$dictionary['M03_Work_Product_Code']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['M03_Work_Product_Code']['fields']['description']['calculated']=false;
$dictionary['M03_Work_Product_Code']['fields']['description']['rows']='6';
$dictionary['M03_Work_Product_Code']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_erd_error_documents_id_c.php

 // created: 2019-08-26 11:48:11

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_applicable_standard_protocol_c.php

 // created: 2019-08-26 11:48:11
$dictionary['M03_Work_Product_Code']['fields']['applicable_standard_protocol_c']['labelValue']='Applicable Standard Protocol';
$dictionary['M03_Work_Product_Code']['fields']['applicable_standard_protocol_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_functional_area_c.php

 // created: 2020-03-03 12:59:40
$dictionary['M03_Work_Product_Code']['fields']['functional_area_c']['labelValue']='Functional Area';
$dictionary['M03_Work_Product_Code']['fields']['functional_area_c']['dependency']='';
$dictionary['M03_Work_Product_Code']['fields']['functional_area_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_total_animal_number_c.php

 // created: 2021-01-12 10:30:38
$dictionary['M03_Work_Product_Code']['fields']['total_animal_number_c']['labelValue']='Total Animal Number';
$dictionary['M03_Work_Product_Code']['fields']['total_animal_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product_Code']['fields']['total_animal_number_c']['enforced']='';
$dictionary['M03_Work_Product_Code']['fields']['total_animal_number_c']['dependency']='';
$dictionary['M03_Work_Product_Code']['fields']['total_animal_number_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/m03_work_product_m03_work_product_code_1_M03_Work_Product_Code.php

// created: 2021-01-21 14:15:48
$dictionary["M03_Work_Product_Code"]["fields"]["m03_work_product_m03_work_product_code_1"] = array (
  'name' => 'm03_work_product_m03_work_product_code_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_m03_work_product_code_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_m03_work_product_code_1m03_work_product_ida',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/wpg_work_product_group_m03_work_product_code_1_M03_Work_Product_Code.php

// created: 2021-04-20 09:04:33
$dictionary["M03_Work_Product_Code"]["fields"]["wpg_work_product_group_m03_work_product_code_1"] = array (
  'name' => 'wpg_work_product_group_m03_work_product_code_1',
  'type' => 'link',
  'relationship' => 'wpg_work_product_group_m03_work_product_code_1',
  'source' => 'non-db',
  'module' => 'WPG_Work_Product_Group',
  'bean_name' => 'WPG_Work_Product_Group',
  'side' => 'right',
  'vname' => 'LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'id_name' => 'wpg_work_p164at_group_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product_Code"]["fields"]["wpg_work_product_group_m03_work_product_code_1_name"] = array (
  'name' => 'wpg_work_product_group_m03_work_product_code_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_WPG_WORK_PRODUCT_GROUP_TITLE',
  'save' => true,
  'id_name' => 'wpg_work_p164at_group_ida',
  'link' => 'wpg_work_product_group_m03_work_product_code_1',
  'table' => 'wpg_work_product_group',
  'module' => 'WPG_Work_Product_Group',
  'rname' => 'name',
);
$dictionary["M03_Work_Product_Code"]["fields"]["wpg_work_p164at_group_ida"] = array (
  'name' => 'wpg_work_p164at_group_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE_ID',
  'id_name' => 'wpg_work_p164at_group_ida',
  'link' => 'wpg_work_product_group_m03_work_product_code_1',
  'table' => 'wpg_work_product_group',
  'module' => 'WPG_Work_Product_Group',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_sample_preparation_cpi_avg_h_c.php

 // created: 2021-09-21 10:56:04
$dictionary['M03_Work_Product_Code']['fields']['sample_preparation_cpi_avg_h_c']['labelValue']='Sample Preparation CPI Avg. (hours)';
$dictionary['M03_Work_Product_Code']['fields']['sample_preparation_cpi_avg_h_c']['enforced']='';
$dictionary['M03_Work_Product_Code']['fields']['sample_preparation_cpi_avg_h_c']['dependency']='';
$dictionary['M03_Work_Product_Code']['fields']['sample_preparation_cpi_avg_h_c']['required_formula']='';
$dictionary['M03_Work_Product_Code']['fields']['sample_preparation_cpi_avg_h_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_application_cpi_avg_hours_c.php

 // created: 2021-09-21 10:57:16
$dictionary['M03_Work_Product_Code']['fields']['application_cpi_avg_hours_c']['labelValue']='Application CPI Avg. (hours)';
$dictionary['M03_Work_Product_Code']['fields']['application_cpi_avg_hours_c']['enforced']='';
$dictionary['M03_Work_Product_Code']['fields']['application_cpi_avg_hours_c']['dependency']='';
$dictionary['M03_Work_Product_Code']['fields']['application_cpi_avg_hours_c']['required_formula']='';
$dictionary['M03_Work_Product_Code']['fields']['application_cpi_avg_hours_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_evaluation_cpi_avg_hours_c.php

 // created: 2021-09-21 10:58:43
$dictionary['M03_Work_Product_Code']['fields']['evaluation_cpi_avg_hours_c']['labelValue']='Evaluation CPI Avg. (hours)';
$dictionary['M03_Work_Product_Code']['fields']['evaluation_cpi_avg_hours_c']['enforced']='';
$dictionary['M03_Work_Product_Code']['fields']['evaluation_cpi_avg_hours_c']['dependency']='';
$dictionary['M03_Work_Product_Code']['fields']['evaluation_cpi_avg_hours_c']['required_formula']='';
$dictionary['M03_Work_Product_Code']['fields']['evaluation_cpi_avg_hours_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_final_report_cpi_avg_hours_c.php

 // created: 2021-09-21 11:00:05
$dictionary['M03_Work_Product_Code']['fields']['final_report_cpi_avg_hours_c']['labelValue']='Final Report CPI Avg. (hours)';
$dictionary['M03_Work_Product_Code']['fields']['final_report_cpi_avg_hours_c']['enforced']='';
$dictionary['M03_Work_Product_Code']['fields']['final_report_cpi_avg_hours_c']['dependency']='';
$dictionary['M03_Work_Product_Code']['fields']['final_report_cpi_avg_hours_c']['required_formula']='';
$dictionary['M03_Work_Product_Code']['fields']['final_report_cpi_avg_hours_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/m03_work_product_code_taskd_task_design_1_M03_Work_Product_Code.php

// created: 2021-12-02 09:36:03
$dictionary["M03_Work_Product_Code"]["fields"]["m03_work_product_code_taskd_task_design_1"] = array (
  'name' => 'm03_work_product_code_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_code_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'vname' => 'LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'id_name' => 'm03_work_p7fc0ct_code_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/tc_namsa_test_codes_m03_work_product_code_1_M03_Work_Product_Code.php

// created: 2021-12-07 12:00:27
$dictionary["M03_Work_Product_Code"]["fields"]["tc_namsa_test_codes_m03_work_product_code_1"] = array (
  'name' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'type' => 'link',
  'relationship' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'source' => 'non-db',
  'module' => 'TC_NAMSA_Test_Codes',
  'bean_name' => 'TC_NAMSA_Test_Codes',
  'side' => 'right',
  'vname' => 'LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'id_name' => 'tc_namsa_t49d3t_codes_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product_Code"]["fields"]["tc_namsa_test_codes_m03_work_product_code_1_name"] = array (
  'name' => 'tc_namsa_test_codes_m03_work_product_code_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_TC_NAMSA_TEST_CODES_TITLE',
  'save' => true,
  'id_name' => 'tc_namsa_t49d3t_codes_ida',
  'link' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'table' => 'tc_namsa_test_codes',
  'module' => 'TC_NAMSA_Test_Codes',
  'rname' => 'name',
);
$dictionary["M03_Work_Product_Code"]["fields"]["tc_namsa_t49d3t_codes_ida"] = array (
  'name' => 'tc_namsa_t49d3t_codes_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE_ID',
  'id_name' => 'tc_namsa_t49d3t_codes_ida',
  'link' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'table' => 'tc_namsa_test_codes',
  'module' => 'TC_NAMSA_Test_Codes',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_test_price_c.php

 // created: 2022-04-05 07:02:17
$dictionary['M03_Work_Product_Code']['fields']['test_price_c']['labelValue']='Test Price';
$dictionary['M03_Work_Product_Code']['fields']['test_price_c']['enforced']='';
$dictionary['M03_Work_Product_Code']['fields']['test_price_c']['dependency']='';
$dictionary['M03_Work_Product_Code']['fields']['test_price_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['M03_Work_Product_Code']['fields']['test_price_c']['required_formula']='';
$dictionary['M03_Work_Product_Code']['fields']['test_price_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_currency_id.php

 // created: 2022-04-05 07:02:19

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2022-04-05 07:02:19

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Vardefs/sugarfield_wpc_type_c.php

 // created: 2022-05-24 06:59:13
$dictionary['M03_Work_Product_Code']['fields']['wpc_type_c']['labelValue']='WPC Type';
$dictionary['M03_Work_Product_Code']['fields']['wpc_type_c']['dependency']='';
$dictionary['M03_Work_Product_Code']['fields']['wpc_type_c']['required_formula']='';
$dictionary['M03_Work_Product_Code']['fields']['wpc_type_c']['readonly_formula']='';
$dictionary['M03_Work_Product_Code']['fields']['wpc_type_c']['visibility_grid']='';

 
?>
