<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Language/en_us.customm03_work_product_m03_work_product_code_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Language/en_us.customm03_work_product_m03_work_product_code_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE'] = 'Work Products';


?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Language/en_us.customwpg_work_product_group_m03_work_product_code_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_WPG_WORK_PRODUCT_GROUP_TITLE'] = 'Work Product Groups';
$mod_strings['LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE'] = 'Work Product Groups';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Language/en_us.customm03_work_product_code_taskd_task_design_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE'] = 'Task Designs';
$mod_strings['LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_CODE_TITLE'] = 'Task Designs';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Language/en_us.customtc_namsa_test_codes_m03_work_product_code_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_TC_NAMSA_TEST_CODES_TITLE'] = 'NAMSA Test Codes';
$mod_strings['LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE'] = 'NAMSA Test Codes';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product_Code/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Work Product Code ID';
$mod_strings['LBL_WP_CODE_DESCRIPTION'] = 'WP Code Description';
$mod_strings['LBL_STANDARD_PROTOCOL_TITLE'] = 'Standard Protocol Title';
$mod_strings['LBL_FUNCTIONAL_AREA'] = 'Functional Area';
$mod_strings['LBL_RECORD_BODY'] = 'WPC';
$mod_strings['LBL_DESCRIPTION'] = 'Notes';
$mod_strings['LBL_APPLICABLE_STANDARD_PROTOCOL_ERD_ERROR_DOCUMENTS_ID'] = 'Applicable Standard Protocol (related Error Documents ID)';
$mod_strings['LBL_APPLICABLE_STANDARD_PROTOCOL'] = 'Applicable Standard Protocol';
$mod_strings['LBL_TOTAL_ANIMAL_NUMBER'] = 'Total Animal Number';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Blanket Work Product(s)';
$mod_strings['LBL_M03_WORK_PRODUCT_CODE_FOCUS_DRAWER_DASHBOARD'] = 'Work Product Codes Focus Drawer';
$mod_strings['LBL_WPG_WORK_PRODUCT_GROUP_M03_WORK_PRODUCT_CODE_1_FROM_WPG_WORK_PRODUCT_GROUP_TITLE'] = 'Work Product Group';
$mod_strings['LBL_SAMPLE_PREPARATION_CPI_AVG'] = 'Sample Preparation CPI Avg.';
$mod_strings['LBL_APPLICATION_CPI_AVG'] = 'Application CPI Avg.';
$mod_strings['LBL_EVALUATION_CPI_AVG'] = 'Evaluation CPI Avg.';
$mod_strings['LBL_FINAL_REPORT_CPI_AVG'] = 'Final Report CPI Avg.';
$mod_strings['LBL_SAMPLE_PREPARATION_CPI_AVG_H'] = 'Sample Preparation CPI Avg. (hours)';
$mod_strings['LBL_APPLICATION_CPI_AVG_HOURS'] = 'Application CPI Avg. (hours)';
$mod_strings['LBL_EVALUATION_CPI_AVG_HOURS'] = 'Evaluation CPI Avg. (hours)';
$mod_strings['LBL_FINAL_REPORT_CPI_AVG_HOURS'] = 'Final Report CPI Avg. (hours)';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_TEST_PRICE'] = 'Test Price';
$mod_strings['LBL_WPC_TYPE'] = 'WPC Type';

?>
