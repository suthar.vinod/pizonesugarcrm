<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-12-02 09:36:03
$viewdefs['M03_Work_Product_Code']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_CODE_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_code_taskd_task_design_1',
  ),
);

// created: 2021-01-21 14:15:48
$viewdefs['M03_Work_Product_Code']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_m03_work_product_code_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product_Code']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_code_taskd_task_design_1',
  'view' => 'subpanel-for-m03_work_product_code-m03_work_product_code_taskd_task_design_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product_Code']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_m03_work_product_code_1',
  'view' => 'subpanel-for-m03_work_product_code-m03_work_product_m03_work_product_code_1',
);
