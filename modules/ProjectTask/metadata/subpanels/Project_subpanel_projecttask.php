<?php
// created: 2016-01-06 23:09:35
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'type' => 'name',
    'vname' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
  ),
  'percent_complete' => 
  array (
    'vname' => 'LBL_LIST_PERCENT_COMPLETE',
    'width' => '20%',
    'default' => true,
  ),
  'status' => 
  array (
    'vname' => 'LBL_LIST_STATUS',
    'width' => '20%',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'vname' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Users',
    'width' => '20%',
    'default' => true,
  ),
  'date_finish' => 
  array (
    'vname' => 'LBL_LIST_DATE_DUE',
    'width' => '20%',
    'default' => true,
  ),
);