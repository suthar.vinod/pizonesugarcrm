<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Administration/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_OPPORTUNITIES_DESC'] = 'Configure admin setting for the Opportunities module. The Opportunities Settings include viewing Opportunities by just the Opportunity, or by Opportunities plus the Revenue Line Items attached to it.';
$mod_strings['LBL_MANAGE_OPPORTUNITIES_TITLE'] = 'Opportunities';
$mod_strings['LBL_MANAGE_OPPORTUNITIES_DESC'] = 'Configure Opportunities Module';

?>
<?php
// Merged from custom/Extension/modules/Administration/Ext/Language/en_us.IntelliDocs.php

// CRM Accelerator Admin strings
$mod_strings['LBL_INTELLIDOCS_TITLE'] = 'Flexidocs for SugarCRM';
$mod_strings['LBL_INTELLIDOCS_DESC'] = 'Allows you to manually update local documents from your Flexidocs account.';
$mod_strings['LBL_INTELLIDOCS_CONFIG'] = 'Sync Flexidocs';
$mod_strings['LBL_INTELLIDOCS_CONFIG_DESC'] = 'Manually update documents';
$mod_strings['LBL_INTELLIDOCS_PLATFORM'] = 'Open Flexidocs Platform';
$mod_strings['LBL_INTELLIDOCS_PLATFORM_DESC'] = 'Go to the Flexidocs Platform and mananage your documents and electronic signing';
$mod_strings['LBL_FLEXIDOCS_TEMPLATES'] = 'Manage FlexiDocs Templates';
$mod_strings['LBL_FLEXIDOCS_TEMPLATES_DESC'] = 'Allows you to controls the visibility and manage other options of your flexidocs templates';

?>
