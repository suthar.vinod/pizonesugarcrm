<?php

class updateORIEstimatedArrivalDate
{

    function updateEstimatedArrivalDate($bean, $event, $arguments)
    {

        global $db; 
        $beanId = $bean->id;
        $estimated_arrival_date = $bean->estimated_arrival_date;
        if (isset($arguments['dataChanges']['status_c']) || isset($arguments['dataChanges']['order_date']) || isset($arguments['dataChanges']['estimated_arrival_date']) && $beanId != "") {
            $sqlpo = 'SELECT * FROM po_purchase_order_ori_order_request_item_1_c WHERE po_purchase_order_ori_order_request_item_1po_purchase_order_ida = "' . $beanId . '" AND deleted = "0" ';

            $resultpo = $db->query($sqlpo);
            if ($resultpo->num_rows > 0) {
                $i = 0;
                while ($row = $db->fetchByAssoc($resultpo)) {
                    $oriId = $row['po_purchasdbbcst_item_idb'];

                    $ORI_Order_Request_Item = BeanFactory::retrieveBean('ORI_Order_Request_Item', $oriId);
                    $ORI_Order_Request_Item->estimated_arrival_date = $estimated_arrival_date;
                    $ORI_Order_Request_Item->load_relationship('ori_order_request_item_ri_received_items_1');
                    $relatedRIIds = $ORI_Order_Request_Item->ori_order_request_item_ri_received_items_1->get();
                    $total = 0;
                    for ($a = 0; $a < count($relatedRIIds); $a++) {
                        $RI_Received_Items = BeanFactory::retrieveBean('RI_Received_Items', $relatedRIIds[$a]);
                        $quantity_received = $RI_Received_Items->quantity_received;
                        $total = $total + $quantity_received;
                    }
                    $POStatus = $bean->status_c;
                    $POname = $bean->name;

                    $unit_quantity_requested = $ORI_Order_Request_Item->unit_quantity_requested;
                    $uniqueArr =  array();
                    $uniqueArr["Ordered"] = "Ordered";
                    $uniqueArr["Backordered"] = "Backordered";

                    if ($ORI_Order_Request_Item->request_date != "" && $POname == "" && count($relatedRIIds) <= 0) {
                        $ORI_Order_Request_Item->status = "Requested";
                        $ORI_Order_Request_Item->save();
                    } else if ($ORI_Order_Request_Item->request_date != "" && $POname != "" && $POStatus == "Pending" && count($relatedRIIds) <= 0) {
                        $ORI_Order_Request_Item->status = "Requested";
                        $ORI_Order_Request_Item->save();
                    } else if ($ORI_Order_Request_Item->request_date != "" && $POname != "" && $POStatus == "Submitted" && count($relatedRIIds) <= 0) {
                        if ($ORI_Order_Request_Item->status == "Ordered" || $ORI_Order_Request_Item->status == "Backordered") {
                            $ORI_Order_Request_Item->status = $uniqueArr[$ORI_Order_Request_Item->status];
                        } else {
                            $ORI_Order_Request_Item->status = "Ordered";
                        }
                        $ORI_Order_Request_Item->save();
                    } else  if (count($relatedRIIds) > 0 && $unit_quantity_requested <= $total) {
                        $ORI_Order_Request_Item->status = "Inventory";
                        $ORI_Order_Request_Item->save();
                    } else if (count($relatedRIIds) > 0 && $unit_quantity_requested != $total) {
                        $ORI_Order_Request_Item->status = "Partially Received";
                        $ORI_Order_Request_Item->save();
                    }


                    // bug 2302
                    $or_id = $ORI_Order_Request_Item->or_order_request_ori_order_request_item_1or_order_request_ida;
                    if (!empty($or_id)) {
                        $OR_Order_Request = BeanFactory::retrieveBean('OR_Order_Request', $or_id);
                        $OR_Order_Request->load_relationship('or_order_request_ori_order_request_item_1');
                        $ORIids = $OR_Order_Request->or_order_request_ori_order_request_item_1->get();
                        $po_id = "";
                        $POArray = array();
                        for ($a = 0; $a < count($ORIids); $a++) {
                            $ORI_Bean = BeanFactory::retrieveBean('ORI_Order_Request_Item', $ORIids[$a]);
                            $po_id = $ORI_Bean->po_purchase_order_ori_order_request_item_1po_purchase_order_ida;

                            if ($po_id) {
                                array_push($POArray, $po_id);
                            } else {
                                array_push($POArray, "");
                            }
                        }

                        $POArray1 = array_unique($POArray);
                        if (!empty($POArray1)) {
                            if (in_array("", $POArray1)) {
                                $POArrayString = implode(" ", $POArray1);
                                if (trim($POArrayString) == "") {
                                    $OR_Order_Request->status_c = "Open";
                                } else {
                                    $OR_Order_Request->status_c = "Partially Submitted";
                                }
                            } else {
                                $OR_Order_Request->status_c = "Fully Submitted";
                            }
                            $OR_Order_Request->save();
                        } else {
                            if ($OR_Order_Request->status_c == "Open" || $OR_Order_Request->status_c == "Complete") {
                                $OR_Order_Request->status_c = $OR_Order_Request->status_c;
                                $OR_Order_Request->save();
                            }
                        }
                    }
                    $i++;
                }
            }
        }
    }
}
