({
    extendsFrom: 'RelateField',
    initialize: function (options) {
        this._super('initialize', [options]);

    },
    openSelectDrawer: function () {
        if (this.getSearchModule() == "CA_Company_Address") {
            this.selectLocation();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        }
        else if (this.getSearchModule() == "Contacts") {
            this.selectLocation_team();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else if (this.getSearchModule() == "AN_Account_Number" && this.model.get("account_id_c") != "" && this.model.get("account_id_c") != undefined) {
            this.selectAccount();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        }
        else {
            this._super('openSelectDrawer');
            $("button.btn.btn-invisible.btn-dark").css("display", "block");
            $(".filter-definition-container").css('pointer-events', 'unset');
            $(".choice-filter-clickable").css('pointer-events', 'unset');

        }
    },
    selectAccount: function () {
        var vendorId = this.model.get("account_id_c");
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterAN_Account_NumberTemplate',
                'initial_filter_label': 'Account(Vendor)',
                'filter_populate': {
                    'accounts_an_account_number_1_name': [vendorId],
                },
                'filter_relate': {
                    'accounts_an_account_number_1_name': [vendorId],
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Rooms relate field.
        filterOptions = (this.getSearchModule() == "AN_Account_Number") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    selectLocation: function () {
        company_name = '821a05ea-12bc-942f-3111-569407bab1db';
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterCompaniesAddress',
                'initial_filter_label': 'Companies',
                'filter_populate': {
                    'accounts_ca_company_address_1_name': [company_name],
                },
                'filter_relate': {
                    'accounts_ca_company_address_1_name': [company_name],
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "CA_Company_Address") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    selectLocation_team: function () {
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterPurchaseOrder',
                'initial_filter_label': 'Teams',
                'filter_populate': {
                    'name': 'Purchasing',
                },
                'filter_relate': {
                    'name': 'Purchasing',
                },
            })
            .populateRelate(this.model)
            .format();
        filterOptions = (this.getSearchModule() == "Contacts") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    search: _.debounce(function (query) {
        var term = query.term || '',
            self = this,
            searchModule = this.getSearchModule(),
            params = {},
            limit = self.def.limit || 5,
            relatedModuleField = this.getRelatedModuleField();

        if (query.context) {
            params.offset = this.searchCollection.next_offset;
        }

        //console.log(params);
        params.filter = this.buildFilterDefinition(term);

        this.searchCollection.fetch({
            //Don't show alerts for this request
            showAlerts: false,
            update: true,
            remove: _.isUndefined(params.offset),
            fields: this.getSearchFields(),
            context: self,
            params: params,
            //limit: limit,
            success: function (data) {
                var fetch = { results: [], more: data.next_offset > 0, context: data };
                if (fetch.more) {
                    var fieldEl = self.$(self.fieldTag),
                        //For teamset widget, we should specify which index element to be filled in
                        plugin = (fieldEl.length > 1) ? $(fieldEl.get(self.currentIndex)).data("select2") : fieldEl.data("select2"),
                        height = plugin.searchmore.children("li:first").children(":first").outerHeight(),
                        //0.2 makes scroll not to touch the bottom line which avoid fetching next record set
                        maxHeight = height * (limit - .2);
                    plugin.results.css("max-height", maxHeight);
                }
                _.each(data.models, function (model, index) {
                    if (params.offset && index < params.offset) {
                        return;
                    }
                    fetch.results.push({
                        id: model.id,
                        text: model.get(relatedModuleField) + ''
                    });
                });
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback(fetch);
                }
            },
            error: function () {
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback({ results: [] });
                }
                app.logger.error("Unable to fetch the bean collection.");
            }
        });

    }, app.config.requiredElapsed || 500),
    _getCustomFilter1: function () {
        return [{
            '$or': [
                { 'accounts_ca_company_address_1_name': "American Preclinical Services" }
            ]
        }];
    },
    _getCustomFilter_Contact: function () {
        return [{
            '$or': [
                {
                    'team_id': "f540fc6a-ca9c-11ea-a8bb-02fb813964b8"
                }
            ]
        }];
    },
    _getCustomFilter_Account: function () {
        var vendorId = this.model.get("vendor");
        console.log(vendorId);
        return [{
            '$or': [
                {
                    'accounts_an_account_number_1_name': vendorId
                }
            ]
        }];
    },
    /**
     * @override
     */
    buildFilterDefinition: function (searchTerm) {
        var parentFilter = this._super('buildFilterDefinition', [searchTerm]);
        if (this.getSearchModule() == 'CA_Company_Address') {
            return parentFilter.concat(this._getCustomFilter1());
        } else if (this.getSearchModule() == 'Contacts') {
            return parentFilter.concat(this._getCustomFilter_Contact());
        } else if (this.getSearchModule() == 'AN_Account_Number' && this.model.get("vendor") != "" && this.model.get("vendor") != undefined) {
            return parentFilter.concat(this._getCustomFilter_Account());
        } else {
            return parentFilter;
        }
    },
});