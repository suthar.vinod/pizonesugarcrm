({
    extendsFrom: 'RecordView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.readOnlyFieldsOpp, this);
        this.model.on("change:vendor", this.function_vendor, this);		
		
    },
    readOnlyFieldsOpp: function () {

        /*var vendor = this.model.get('vendor');
        if (vendor != "") {
            $('div[data-name="account_number"]').css('pointer-events', 'none');
        } else {
            $('div[data-name="account_number"').css('pointer-events', 'unset');
        }*/

    },
    function_vendor: function () {
        var account_id_c = this.model.get('account_id_c');
        var vendor = this.model.get('vendor');
        if (vendor != '') {
            var self = this;
            App.api.call("get", "rest/v10/Accounts/" + account_id_c + "?fields=account_number_c", null, {
                success: function (dropdowndata) {
                    if (dropdowndata.account_number_c != "") {
                        self.model.set('account_number', dropdowndata.account_number_c);
                    } else {
                        self.model.set('account_number', '');
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }else{
			this.model.set('account_number', ''); 
		}
    },
	 
})


