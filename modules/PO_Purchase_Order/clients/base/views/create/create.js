({
  extendsFrom: 'CreateView',
  initialize: function (options) {
    this._super('initialize', [options]);
    this.model.on("change:vendor", this.function_vendor, this);
    this.on('render', _.bind(this.onload_function, this));
    function onChangeSearch(subtype) {
      var count = 0;
      var checked = $(subtype).prop('checked');
      $('.checkchk').prop('checked', checked);
      if ($(".checkAllst:checked")) {

        count = $(".checkchk:checked").length;
        if (count > 0) {
          $('.showalert').html('');
          var str = 'You have selected ' + count + ' records in the result set.<button type="button" class="btn btn-link btn-inline removeselectedchk" data-action="clear" onclick="removeselectedchk();">Clear selections.</button>';
          $('.showalert').show();
          $('.showalert').html(str);
          $('.showalertcls').show();
        } else {
          $('.showalert').hide();
          $('.showalert').html('');
          $('.showalertcls').hide();
        }

      } else {
        $('.showalert').hide();
        $('.showalert').html('');
        $('.showalertcls').hide();
      }
    }

    $(document).on("change", ".checkchk", function () {

      if ($('.checkchk:checked').length == $('.checkchk').length) {
        $('.checkAllst').prop('checked', true);
        $('.showalert').show();
      } else {
        $('.checkAllst').prop('checked', false);
        $('.showalert').hide();
      }
    });

    function removeselectedchk() {
      $('.showalert').html('');
      $('.showalertcls').hide();
      $('input[name="checkst"]').prop('checked', false);
      $('input[name="checkAllst"]').prop('checked', false);
    }

    /* Prakash Jangir 01-Mar-2021 Ticket#915 Sorting --> Start <--*/
    // var offsetORIPO = 0;
    var orderType = "desc";
    var lastSortedColumnName = "name";
    var hideShowMoreButton = 0;
    var appendQuery = "";


    $(document).on('click', 'div.main-pane.ori_selection th', function (event) {

      $('div.alert-wrapper').hide();
      if (typeof moduleName !== 'undefined' && moduleName == "PO_Purchase_Order") {
        currentClickedColumnName = $(this).attr('data-fieldname');
        if (currentClickedColumnName == undefined)
          return;

        offsetORIPO = 0;
        if (lastSortedColumnName == currentClickedColumnName) {
          if (orderType == "asc")
            orderType = "desc";
          else
            orderType = "asc";
        } else {
          orderType = "desc";
        }
        lastSortedColumnName = currentClickedColumnName;
        appendQuery = "&order_by=" + currentClickedColumnName + "%3A" + orderType;
        $('.checkall').trigger('click');
        $('.checkall input[name="check"]').prop('checked', false);
        $('input[name="check"]').trigger('click');
        $('input[name="check"]').prop('checked', false);
        var self = this;
        console.log("rest/v10/ORI_Order_Request_Item/" + vendorName + "/" + vendorId + "/" + offsetORIPO + "/" + lastSortedColumnName + "/" + orderType + "/get_ori_data");
        setTimeout(function () {
          app.api.call("read", "rest/v10/ORI_Order_Request_Item/" + vendorName + "/" + vendorId + "/" + offsetORIPO + "/" + lastSortedColumnName + "/" + orderType + "/get_ori_data", null, {
            success: function (oris) {
              offsetORIPO += 20;
              if (oris.records.length == 0) {
                hideShowMoreButton = 1;
                // $('button[data-action="show-more-ori-po"]').hide();
                // return;
              }
              var oriModels = [];
              var oriModelsReset = [];

              if (oris.records && oris.records.length > 0) {
                oriModels = oris.records;
              }
              oriViewPO.collection.reset();
              oriViewPO.collection.add(oriModelsReset);
              oriViewPO.collection.add(oriModels);
              oriViewPO.render();
              $('div.main-pane.ori_selection div.flex-list-view-content').css("overflow", "visible");
              if (oris.records.length > 19) {
                $('button[data-action="show-more-ori-po"]').show();
              } else {
                $('button[data-action="show-more-ori-po"]').hide();
              }

            }.bind(self),
            error: function (error) {
              app.alert.show("ori_error", {
                level: "error",
                messages: "Error retrieving Order Request Item",
                autoClose: true
              });
            }
          });

        }, 200);

      }
    });

    $(document).on('click', 'button[data-action="show-more-ori-po"]', function () {
      $('button[data-action="show-more-ori-po"]').hide();
      $('div.loading-ori').show();

      app.api.call("read", "rest/v10/ORI_Order_Request_Item/" + vendorName + "/" + vendorId + "/" + offsetORIPO + "/" + lastSortedColumnName + "/" + orderType + "/get_ori_data", null, {
        success: function (oris) {
          $('div.loading-ori').hide();
          offsetORIPO += 20;
          if (oris.records.length == 0) {
            hideShowMoreButton = 1;
            $('button[data-action="show-more-ori-po"]').hide();
            return;
          }
          var oriModelsPO = [];

          if (oris.records && oris.records.length > 0) {
            oriModelsPO = oris.records;
          }
          oriViewPO.collection.add(oriModelsPO);
          oriViewPO.render();
          $('div.main-pane.ori_selection div.flex-list-view-content').css("overflow", "visible");
          $('button[data-action="show-more-ori-po"]').show();

        }.bind(this),
        error: function (error) {
          app.alert.show("ori_error", {
            level: "error",
            messages: "Error retrieving Order Request Item",
            autoClose: true
          });
        }
      });
    });


    /* --> End <-- */

    $(document).on("keyup", ".ori_selection input.search-name", function () {

      var name_search = $('.ori_selection input.search-name').val();
      var oriName = $('.ori_selection input.search-name').attr('name');
      var oriVendor = $('.ori_selection input.search-name').attr('vendor');
      var vendorId = $('.ori_selection input.search-name').attr('vendorId');

      $(".ori_selection").css('width', "100%");
      $(".ori_selection .table").html('');


      var APIURL = "";
      if (oriVendor != "" && oriVendor != null) {
        APIURL = "rest/v10/ORI_Order_Request_Item/" + oriVendor + "/" + vendorId + "/" + name_search + "/get_ori_data", null;
        app.api.call("read", APIURL, null, {
          success: function successFunc(or_selection) {

            $(".ori_selection .table").html('');
            var data = "";
            data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
            $.each(or_selection.records, function (key, item) {

              if (item.name != '') {
                data += '<tr name="ORI_Order_Request_Item' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span sclass="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk" id ="' + item.id + '"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td></tr>';
              }

            });
            data += '</tbody>';
            $('.ori_selection .table').append(data);

          }.bind(this),
          error: function (error) {
          }
        });
      }

    });

    $(document).on("click", ".fa.fa-times.add-on", function () {
      $('.showalert').html('');
      $('.ori_selection input.search-name').val('');
      var oriVendor = $('.ori_selection input.search-name').attr('vendor');
      var vendorId = $('.ori_selection input.search-name').attr('vendorId');

      $(".ori_selection").css('width', "100%");
      $(".ori_selection .table").html('');
      var APIURL = "";

      if (oriVendor != "" && oriVendor != null) {
        APIURL = "rest/v10/ORI_Order_Request_Item/" + oriVendor + "/" + vendorId + "/get_ori_data", null;

        app.api.call("read", APIURL, null, {
          success: function successFunc(oriItem) {
            $(".ori_selection .table").html('');
            var data = "";
            data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
            $.each(oriItem.records, function (key, item) {

              if (item.name != '') {
                data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span sclass="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk" id = "' + item.id + '"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td></tr>';
              }

            });
            data += '</tbody>';
            $('.ori_selection .table').append(data);

          }.bind(this),
          error: function (error) {
          }
        });
      }

    });
  },

  onload_function: function () {
    $('div.search-filter').css('display', 'none');
    $(".ori_selection").css('width', "100%");
    this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();

  },


  function_vendor: function () {
    moduleName = "PO_Purchase_Order";
    offsetORIPO = 0;
    orderType = "desc";
    lastSortedColumnName = "";
    hideShowMoreButton = 0;
    appendQuery = "";
    $(".ori_selection").css('width', "100%");
    var account_id_c = this.model.get('account_id_c');
    var vendor = this.model.get('vendor');
    if (vendor != '') {
      var self = this;
      App.api.call("get", "rest/v10/Accounts/" + account_id_c + "?fields=account_number_c", null, {
        success: function (dropdowndata) {
          if (dropdowndata.account_number_c != "") {
            self.model.set('account_number', dropdowndata.account_number_c);
          } else {
            self.model.set('account_number', '');
          }
        },
        error: function (error) {
        }
      });

      vendorName = this.model.get('vendor');
      vendorId = this.model.get('account_id_c');
      $('button[data-action="show-more-ori-po"]').remove();

      // app.api.call("read", "rest/v10/ORI_Order_Request_Item?fields=name&max_num=100000&order_by=name:asc&filter[0][status][$equals][]=Requested&filter[1][vendor_name][$equals][]=" + this.model.get('vendor'), null, {
      /* this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();
      this.layout.layout.$el.find("[data-component=sidebar] .ori_selection").show(); */
      this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").show();

      app.api.call("read", "rest/v10/ORI_Order_Request_Item/" + vendorName + "/" + vendorId + "/" + offsetORIPO + "/name/desc" + "/get_ori_data", null, {

        success: function successFunc(ori) {
          offsetORIPO += 20;
          $(".ori_selection").css('width', "100%");

          oriLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ori-selection");
          oriViewPO = oriLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ori-selection");
          var oriModels = [];
          var selectedORIViewModels = oriViewPO.massCollection;
          if (ori.records && ori.records.length > 0) {
            oriModels = ori.records;
          }
          oriViewPO.collection.reset();
          oriViewPO.collection.add(oriModels);
          oriViewPO.render();

          // $('.ori_selection .table').find(".sorting").removeClass('orderByname');
          // $('.ori_selection .table').find(".sorting").removeClass('orderBydepartment_new_c');
          // $('.ori_selection .table').find(".sorting").removeClass('orderByprod_product_ori_order_request_item_1_name');
          // $('.ori_selection .table').find(".sorting").removeClass('orderByproduct_name');
          // $('.ori_selection .table').find(".sorting").removeClass('orderByproduct_id_2');
          $('.ori_selection').find(".search-name").attr('name', 'orisearch');
          $('.ori_selection').find(".search-name").attr('vendor', this.model.get('vendor'));
          $('.ori_selection').find(".search-name").attr('vendorId', this.model.get('account_id_c'));
          $('div.main-pane.ori_selection div.flex-list-view-content').css("overflow", "visible");


          this.layout.layout.$el.find("input[name=check]").prop('chekced', false);
          this.$('input[name="check"]').prop('checked', false);
          $('.ori_selection').find('input[name="check"]').prop('checked', false);

          var ori_ids = [];
          ori_ids = _.map(selectedORIViewModels.models, function (model) {
            return model.set("id", "");
          });
          setTimeout(function () {
            if ($('button[data-action="show-more-ori-po"]').length == 0) {
              if (ori.records.length > 19) {
                $("div.main-pane.ori_selection div.main-content").append(`
                  <div>
                      <button data-action="show-more-ori-po" class="btn btn-link btn-invisible more padded">More Order Request Item...</button>
                  </div>
                  <div class="block-footer loading-ori hide">
                    <div class="loading">
                        Loading...
                    </div>
                  </div>`);
              }
            }
          }, 500);
        }.bind(this),
        error: function (error) {
          app.alert.show("ORi_error", {
            level: "error",
            messages: "Error retrieving ORI for this PO",
            autoClose: true
          });
        }
      });
    } else {
      this.model.set('account_number', '');
      this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();

      /* var oriLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ori-selection");
      var oriViewPO = oriLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ori-selection");
      oriViewPO.collection.reset();
      oriViewPO.render(); */
    }
  },
  saveAndClose: function () {

    var oriLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ori-selection");
    var oriViewPO = oriLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ori-selection");
    var selectedORI = oriViewPO.massCollection;
    if (oriViewPO.collection.length > 0) {
      if (selectedORI.length > 0) {
        this.initiateSave(_.bind(function initSave() {
          if (this.closestComponent("drawer")) {
            this._linkORIItem().then(function thenFunc(result) {
              if (result === "success") {
                app.alert.dismiss("link_ori");
              } else {
                app.alert.show("no_ori_error", {
                  level: "error",
                  messages: "Failed linking Order Request Item..."
                });
              }
              app.drawer.close(this.context, this.model);
            }.bind(this));
          }
        }, this));
      } else {
        var selectedIDAfterFilter = [];
        $(".checkchk").each(function () {
          if ($(this).prop('checked') == true) {
            selectedIDAfterFilter.push(this.id);
          }
        });
        if (selectedIDAfterFilter.length > 0) {
          this.initiateSave(_.bind(function initSave() {
            app.api.call("create", "rest/v10/PO_Purchase_Order/" + this.model.get("id") + "/link", {
              link_name: "po_purchase_order_ori_order_request_item_1",
              ids: selectedIDAfterFilter
            }, {
              success: function () {
                app.alert.show("link_ori", {
                  level: "info",
                  messages: "Linking Order Request Item..."

                });
                app.router.redirect('#PO_Purchase_Order');
              },
              error: function (error) {
                app.alert.show("no_ori_error", {
                  level: "error",
                  messages: "Failed linking Order Request Item..."
                });
              }
            });
          }, this));

        } else {
          this.initiateSave(_.bind(function initSave() {
            app.router.redirect('#PO_Purchase_Order');
          }, this));
        }

      }
    } else {
      this.initiateSave(_.bind(function initSave() {
        app.router.redirect('#PO_Purchase_Order');
      }, this));
    }


  },

  _linkORIItem: function () {
    return new Promise(function promiseFunc(resolve, reject) {

      var poId = this.model.get("id");
      var oriLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ori-selection");
      var oriViewPO = oriLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ori-selection");
      var selectedORI = oriViewPO.massCollection;

      if (selectedORI.length > 0) {
        app.alert.show("link_ori", {
          level: "info",
          messages: "Linking Order Request Item..."

        });

        var ORI_ids = [];
        ORI_ids = _.map(selectedORI.models, function (model) {
          return model.get("id");
        });
        app.api.call("create", "rest/v10/PO_Purchase_Order/" + poId + "/link", {
          link_name: "po_purchase_order_ori_order_request_item_1",
          ids: ORI_ids
        }, {
          success: function () {
            app.router.redirect('#PO_Purchase_Order');
            resolve("success");
          },
          error: function (error) {
            resolve(error);
          }
        });
      } else {
        resolve("success");
      }

    }.bind(this));
  },
})