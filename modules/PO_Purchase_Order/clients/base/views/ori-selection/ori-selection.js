({
    extendsFrom: "MultiSelectionListView",
    allowedFields: ["name", "department_new_c","prod_product_ori_order_request_item_1_name","product_name","product_id_2","or_request_by_c"],
    initialize: function () {
        this._super("initialize", arguments);
        this.collection.reset();
    },
    render: function () {
        this._super("render", arguments);
        this.layout.$el.parent().addClass("span12 ori_selection");
    },
    addActions: function () {
        this._super("addActions");
        this.rightColumns = [];
        this.meta.panels[0].fields = _.filter(this.meta.panels[0].fields, function filterFields(field) {
            return this.allowedFields.indexOf(field.name) !== -1;
        }.bind(this));
    }
});