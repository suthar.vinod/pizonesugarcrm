<?php
$module_name = 'PO_Purchase_Order';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'recorddashlet' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'status_c',
                'label' => 'LBL_STATUS',
              ),
              1 => 
              array (
                'name' => 'order_date',
                'label' => 'LBL_ORDER_DATE',
              ),
              2 => 
              array (
                'name' => 'vendor',
                'studio' => 'visible',
                'label' => 'LBL_VENDOR',
              ),
              3 => 
              array (
                'name' => 'account_number_2_c',
                'studio' => 'visible',
                'label' => 'LBL_ACCOUNT_NUMBER_2',
              ),
              4 => 
              array (
                'name' => 'shipping_cost',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_SHIPPING_COST',
              ),
              5 => 
              array (
              ),
              6 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'readonly' => false,
                'name' => 'tax_c',
                'label' => 'LBL_TAX',
              ),
              7 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'readonly' => false,
                'name' => 'hazard_fee_c',
                'label' => 'LBL_HAZARD_FEE',
              ),
              8 => 
              array (
                'name' => 'approved_by',
                'studio' => 'visible',
                'label' => 'LBL_APPROVED_BY',
              ),
              9 => 
              array (
                'name' => 'total_cost',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_TOTAL_COST',
              ),
              10 => 
              array (
                'name' => 'equipment',
                'label' => 'LBL_EQUIPMENT',
              ),
              11 => 
              array (
                'name' => 'ship_to',
                'studio' => 'visible',
                'label' => 'LBL_SHIP_TO',
              ),
              12 => 
              array (
                'name' => 'confirmation_number',
                'label' => 'LBL_CONFIRMATION_NUMBER',
              ),
              13 => 
              array (
                'name' => 'estimated_arrival_date',
                'label' => 'LBL_ESTIMATED_ARRIVAL_DATE',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
