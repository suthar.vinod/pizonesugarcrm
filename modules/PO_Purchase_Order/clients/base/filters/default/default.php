<?php
// created: 2022-04-14 06:58:34
$viewdefs['PO_Purchase_Order']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'account_number_2_c' => 
    array (
    ),
    'approved_by' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'confirmation_number' => 
    array (
    ),
    'equipment' => 
    array (
    ),
    'estimated_arrival_date' => 
    array (
    ),
    'hazard_fee_c' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'name' => 
    array (
    ),
    'description' => 
    array (
    ),
    'order_date' => 
    array (
    ),
    'po_comments_c' => 
    array (
    ),
    'ship_to' => 
    array (
    ),
    'shipping_cost' => 
    array (
    ),
    'status_c' => 
    array (
    ),
    'tax_c' => 
    array (
    ),
    'total_cost' => 
    array (
    ),
    'vendor' => 
    array (
    ),
  ),
);