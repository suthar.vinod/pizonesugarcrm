<?php

class customLogicPONameHook {

    function generateSystemId($bean, $event, $arguments) {
        
        global $db;
		
		/*if($bean->load_relationship('po_purchase_order_ori_order_request_item_1'))
		{
		   $relatedBeans = $bean->po_purchase_order_ori_order_request_item_1->get();
			if(count($relatedBeans) > 0) {
			   foreach($relatedBeans as $relatedID) {
					if($relatedID != '') {
						$query = "update ori_order_request_item set estimated_arrival_date = '" .$bean->estimated_arrival_date."' where id = '$relatedID' and deleted = 0 ";
						$db->query($query);
					}
			   }
			}
		}*/

        if ($arguments['isUpdate'] == false) {
            $start = '1';
            $number_len = 4;
            $year = gmdate('y');
            $sql = "SELECT name, substring(name,-4) as last_number  FROM po_purchase_order WHERE deleted = 0 AND (name !='' OR name is not NULL) ORDER BY name DESC LIMIT 1";
            $result = $db->query($sql);
            $row = $db->fetchByAssoc($result);
            
            $LastYear = substr($row['name'], 2, 2);
            
            if (empty($row['last_number'])) {
                $next_number = intval($start);
            } else {
                $last_number = $row['last_number'];
                $next_number = intval($last_number) + 1;
            }
            $next_number = str_pad(strval($next_number), $number_len, "0", STR_PAD_LEFT);
            if ($LastYear < $year) {
                $next_number = '0001';
            }
            $bean->name = 'PO' . $year . '-' . $next_number;
        }
    }

}
