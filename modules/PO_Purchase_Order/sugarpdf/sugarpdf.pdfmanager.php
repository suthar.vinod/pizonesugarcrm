<?php

require_once 'include/Sugarpdf/sugarpdf/sugarpdf.pdfmanager.php';
require_once 'include/Sugarpdf/sugarpdf/sugarpdf.smarty.php';
//require_once('include/Sugar_Smarty.php');

class PO_Purchase_OrderSugarpdfPdfmanager extends SugarpdfPdfmanager
{
    public function preDisplay()
    {
        global $db, $current_user;
        global $timedate;
        //$this->ss = new Sugar_Smarty();
		//$GLOBALS['log']->fatal("15may po bean->name ".$this->bean->name);
        parent::preDisplay();

        $previewMode = false;

        if (!empty($_REQUEST['pdf_preview']) && $_REQUEST['pdf_preview'] == 1) {

            $previewMode = true;

        }

        if ($previewMode === false) {

            // Select links to add
			
			if (!empty($this->bean->name)) {
                $fields['poName'] = $this->bean->name;
            }
			
			if (!empty($this->bean->vendor)){
                $fields['poVendor'] = $this->bean->vendor;
				
			}

			if (!empty($this->bean->po_comments_c)){
                $fields['po_comments_c'] = $this->bean->po_comments_c;
				
			}
			
			$accountAddressName = "";
			if (!empty($this->bean->an_account_number_id_c)) {
				$accountNo  = $this->bean->an_account_number_id_c;
				$AN_bean	 = BeanFactory::getBean('AN_Account_Number', $accountNo);
				$CA_id	= $AN_bean->ca_company_address_id1_c;
					
				if (!empty($CA_id)) {
					$CA_bean	 = BeanFactory::getBean('CA_Company_Address', $CA_id);
					$accountAddressName	= $CA_bean->name; 
					$fields['poAddressName'] = $accountAddressName;
				}
			}
			
			if (!empty($this->bean->ship_to)) {
                $fields['poShipTo'] = $this->bean->ship_to;
            }
			
			if (!empty($this->bean->confirmation_number)) {
                $fields['poConfirmationNumber'] = $this->bean->confirmation_number;
            }
			
			if($this->bean->order_date!=""){
				$orderDate				= $this->bean->order_date;
				$orderDate_arr			= explode("-",$orderDate);
				$fields['poOrderDate']  = $orderDate_arr[1]."/".$orderDate_arr[2]."/".$orderDate_arr[0];
			}
					
			 
			if (!empty($this->bean->account_number_2_c)) {
                $fields['poAccount'] = $this->bean->account_number_2_c;
                 /**#1806 : PO Pdf update : gsingh 21-12-2021 */
                $account_number_phone_number_c	= $AN_bean->account_number_phone_number_c;
                $account_number_email_address_c	= $AN_bean->account_number_email_address_c;
                $fields['poAccountPhone'] = $account_number_phone_number_c;
                $fields['poAccountEmail'] = $account_number_email_address_c;
                 /**#1806 : PO Pdf update : gsingh 21-12-2021 */

            }

            $departmentList = $GLOBALS['app_list_strings']['department_list'];
			       
            /////////////////////////////////////
            $oriQuery = "SELECT ORI.id,
                        ORI_PROD.prod_product_ori_order_request_item_1prod_product_ida AS productID, 
                        ORI.product_id_2,
                        ORI.description,
                        ORI.cost_per_unit,
                        ORIC.department_new_c,
                        ORIC.purchase_unit_2_c,
                        ORI.related_to,                        
						ORI.unit_quantity_requested AS productQuantity 
            FROM po_purchase_order_ori_order_request_item_1_c  AS PO_ORI
            LEFT JOIN ori_order_request_item AS ORI  ON ORI.id = PO_ORI.po_purchasdbbcst_item_idb
            LEFT JOIN ori_order_request_item_cstm AS ORIC ON ORIC.id_c = ORI.id
            LEFT JOIN prod_product_ori_order_request_item_1_c AS ORI_PROD
            ON ORI_PROD.prod_product_ori_order_request_item_1ori_order_request_item_idb = ORI.id AND ORI_PROD.deleted=0
            WHERE PO_ORI.po_purchase_order_ori_order_request_item_1po_purchase_order_ida ='".$this->bean->id."' 
            and PO_ORI.deleted=0
            AND (ORI.product_selection='In System Vendor Non Specific' OR ORI.product_selection='In System Vendor Specific')";
			//GROUP BY ORI_PROD.prod_product_ori_order_request_item_1prod_product_ida
            $resultORI = $db->query($oriQuery);
            $cnt        = 0;
            $totalPrice = 0;
            $productData = array();
            $totalDesCharCount = 0;
            if ($resultORI->num_rows > 0) {
				$allRecord = 1;

				while ($rowORI = $db->fetchByAssoc($resultORI)) {

                    $item_number    = "";
                    $item_desc      = "";
                    if($rowORI['product_id_2']!="")
                    {
                        $item_number =  $rowORI['product_id_2'];
                        $item_desc   =  $rowORI['description'];
                        $quantity    = 1;
                    }
                    if($rowORI['productID']!="")
                    {
                        $productID      = $rowORI['productID'];
                        $quantity       = $rowORI['productQuantity'];
                        $productBean    = BeanFactory::getBean('Prod_Product', $productID); 
                        
                        $item_number =  $productBean->product_id_2;
                        //$item_desc   =  $productBean->description;
                        $item_desc   =  $productBean->name;
                    }
                    
                    $item_department   = $rowORI['department_new_c'];
                    $item_unit         = $rowORI['purchase_unit_2_c'];
                    $unitCost          = $rowORI['cost_per_unit'];
                    $itemTotal         = $quantity*$unitCost;
                    $totalPrice        += $itemTotal;
                    /**#1806 : PO Pdf update : gsingh 21-12-2021 */
                    $related_to         = $rowORI['related_to'];                   
                    if($related_to =='Work Product')
                    {
                        $OriBean    = BeanFactory::getBean('ORI_Order_Request_Item', $rowORI['id']);
                        $wp_id      =  $OriBean->m03_work_product_ori_order_request_item_1m03_work_product_ida;
                        $WPBean     = BeanFactory::getBean('M03_Work_Product', $wp_id);
                        $study_sale_data    = $WPBean->name;
                       
                    }
                    else  if($related_to =='Sales')
                    {
                        $OriBean    = BeanFactory::getBean('ORI_Order_Request_Item', $rowORI['id']);                      
                        $sale_id    =  $OriBean->m01_sales_ori_order_request_item_1m01_sales_ida;
                        $SalesBean     = BeanFactory::getBean('M01_Sales', $sale_id);
                        $study_sale_data    = $SalesBean->name;
                    }
                    else
                    {
                        $study_sale_data ="";
                    }
                     /**#1806 : EOC : PO Pdf update : gsingh 21-12-2021 */

                    $productData[$cnt]['item_number']       = $item_number;
                    $productData[$cnt]['item_description']  = $item_desc;
                    $productData[$cnt]['item_department']   = $departmentList[$item_department];
                    $productData[$cnt]['item_unit']         = $item_unit;
                    $productData[$cnt]['item_quantity']     = $quantity;
                     /**#1806 : PO Pdf update : gsingh 21-12-2021 */
                    $productData[$cnt]['related_to']        = $related_to;                  
                    $productData[$cnt]['study_sale_data']   = $study_sale_data;
                     /**#1806 : EOC : PO Pdf update : gsingh 21-12-2021 */
                    $productData[$cnt]['item_price']        = number_format($unitCost,2);
                    $productData[$cnt]['item_total']        = number_format($itemTotal,2);

                    $cnt++;
                }
            }

            /* Get ORI With Product Not in System */
            $oriQuery2 = "SELECT ORI.id,
                    ORI.product_id_2 AS productID, 
                    ORI.product_name AS productName,
                    ORI.related_to,
                    SUM(ORI.cost_per_unit),
                    ORI.description,
                    ORIC.department_new_c,
                    ORIC.purchase_unit_2_c ,ORI.unit_quantity_requested AS productQuantity
            FROM po_purchase_order_ori_order_request_item_1_c  AS PO_ORI
            LEFT JOIN ori_order_request_item AS ORI  ON ORI.id = PO_ORI.po_purchasdbbcst_item_idb
            LEFT JOIN ori_order_request_item_cstm AS ORIC ON ORIC.id_c = ORI.id
            WHERE PO_ORI.po_purchase_order_ori_order_request_item_1po_purchase_order_ida ='".$this->bean->id."' 
            and PO_ORI.deleted=0
            AND (ORI.product_selection='Not In System Vendor Non Specific' OR ORI.product_selection='Not In System Vendor Specific') 
            GROUP BY ORI.product_id_2";//
 
            $resultORI2 = $db->query($oriQuery2);
            if ($resultORI2->num_rows > 0) {
				$allRecord = 1;

				while ($rowORI2 = $db->fetchByAssoc($resultORI2)) {

                    $item_number    = "";
                    $item_desc      = "";
                    if($rowORI2['productID']!="")
                    {
                        $item_number =  $rowORI2['productID'];
						
						$productBean    = BeanFactory::getBean('Prod_Product', $item_number); 
                        //$item_desc   	=  $productBean->name;
						$item_desc   =  $rowORI2['productName'];
                        $quantity    =  $rowORI2['productQuantity'];
                    }
                    
                    $item_department   = $rowORI2['department_new_c'];
                    $item_unit         = $rowORI2['purchase_unit_2_c'];
                    $unitCost          = 0;//$rowORI['cost_per_unit'];
                    $itemTotal         = 0;//$quantity*$unitCost;
                    $totalPrice        += $itemTotal;
                     /**#1806 : PO Pdf update : gsingh 21-12-2021 */
                    $related_to         = $rowORI['related_to'];                   
                    if($related_to =='Work Product')
                    {
                        $OriBean    = BeanFactory::getBean('ORI_Order_Request_Item', $rowORI['id']);
                        $wp_id      =  $OriBean->m03_work_product_ori_order_request_item_1m03_work_product_ida;
                        $WPBean     = BeanFactory::getBean('M03_Work_Product', $wp_id);
                        $study_sale_data    = $WPBean->name;
                       
                    }
                    else  if($related_to =='Sales')
                    {
                        $OriBean    = BeanFactory::getBean('ORI_Order_Request_Item', $rowORI['id']);                      
                        $sale_id    =  $OriBean->m01_sales_ori_order_request_item_1m01_sales_ida;
                        $SalesBean     = BeanFactory::getBean('M01_Sales', $sale_id);
                        $study_sale_data    = $SalesBean->name;
                    }
                    else
                    {
                        $study_sale_data ="";
                    }
                     /**#1806 : EOC : PO Pdf update : gsingh 21-12-2021 */
                    $productData[$cnt]['item_number']       = $item_number;
                    $productData[$cnt]['item_description']  = $item_desc;
                    $productData[$cnt]['item_department']   = $departmentList[$item_department];
                    $productData[$cnt]['item_unit']         = $item_unit;
                    $productData[$cnt]['item_quantity']     = $quantity;
                     /**#1806 : PO Pdf update : gsingh 21-12-2021 */
                    $productData[$cnt]['related_to']        = $related_to;                  
                    $productData[$cnt]['study_sale_data']   = $study_sale_data;
                     /**#1806 : EOC : PO Pdf update : gsingh 21-12-2021 */
                    $productData[$cnt]['item_price']        = number_format($unitCost,2);
                    $productData[$cnt]['item_total']        = number_format($itemTotal,2);

                    $cnt++;
                }
            } 
            
            /* Purchase Order Item Detail */
            $poiQuery = "SELECT POI.id,
                        POI_PROD.prod_product_poi_purchase_order_item_1prod_product_ida AS productID,
                        POI.related_to,
                        POI.department,POI.cost_per_unit ,POI.unit_quantity_ordered AS productQuantity
            FROM po_purchase_order_poi_purchase_order_item_1_c AS PO_POI
            LEFT JOIN poi_purchase_order_item AS POI 
                ON POI.id = PO_POI.po_purchas4ee6er_item_idb 
            LEFT JOIN prod_product_poi_purchase_order_item_1_c AS POI_PROD 
                ON POI_PROD.prod_produf05ber_item_idb  = POI.id AND POI_PROD.deleted=0
            WHERE PO_POI.po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida='".$this->bean->id."'  AND PO_POI.deleted=0" ;
			//count(POI_PROD.prod_product_poi_purchase_order_item_1prod_product_ida) productQuantity1,GROUP BY POI_PROD.prod_product_poi_purchase_order_item_1prod_product_ida

            $resultPOI = $db->query($poiQuery);
             
            if ($resultPOI->num_rows > 0) {
                while ($rowPOI = $db->fetchByAssoc($resultPOI)) {

                    $item_number    = "";
                    $item_desc      = ""; 
                    if($rowPOI['productID']!="")
                    {
                        $productID      = $rowPOI['productID'];
                        $quantity       = $rowPOI['productQuantity'];
                        $productBean    = BeanFactory::getBean('Prod_Product', $productID); 
                        
                        $item_number    =  $productBean->product_id_2;
                        $item_unit      =  $productBean->purchase_unit;
                        //$item_desc      =  $productBean->description;
						$item_desc   	=  $productBean->name;
                    } 
                    $item_department   = $rowPOI['department']; 
                    $unitCost          = $rowPOI['cost_per_unit'];
                    $itemTotal         = $quantity*$unitCost;
                    $totalPrice        += $itemTotal;
                     /**#1806 : PO Pdf update : gsingh 21-12-2021 */
                    $related_to        = $rowPOI['related_to'];
                    if($related_to =='Work Product')
                    {
                        $POIBean    = BeanFactory::getBean('POI_Purchase_Order_Item', $rowPOI['id']);
                        $wp_id      = $POIBean->m03_work_product_poi_purchase_order_item_1m03_work_product_ida;
                        $WPBean     = BeanFactory::getBean('M03_Work_Product', $wp_id);
                        $study_sale_data_poi    = $WPBean->name;
                       
                    }
                    else  if($related_to =='Sales')
                    {
                        $POIBean            = BeanFactory::getBean('POI_Purchase_Order_Item', $rowPOI['id']);                      
                        $sale_id            =  $POIBean->m01_sales_poi_purchase_order_item_1m01_sales_ida;
                        $SalesBean          = BeanFactory::getBean('M01_Sales', $sale_id);
                        $study_sale_data_poi    = $SalesBean->name;
                    }
                    else
                    {
                        $study_sale_data_poi ="";
                    }  
                     /**#1806 : EOC : PO Pdf update : gsingh 21-12-2021 */
                    $productData[$cnt]['item_number']       = $item_number;
                    $productData[$cnt]['item_description']  = $item_desc;
                    $productData[$cnt]['item_department']   = $departmentList[$item_department];
                    $productData[$cnt]['item_unit']         = $item_unit;
                    $productData[$cnt]['item_quantity']     = $quantity;
                     /**#1806 : PO Pdf update : gsingh 21-12-2021 */
                    $productData[$cnt]['related_to']        = $related_to;                  
                    $productData[$cnt]['study_sale_data']   = $study_sale_data_poi;
                     /**#1806 : EOC : PO Pdf update : gsingh 21-12-2021 */
                    $productData[$cnt]['item_price']        = number_format($unitCost,2);
                    $productData[$cnt]['item_total']        = number_format($itemTotal,2);
                    $totalDesCharCount += strlen($productData[$cnt]['item_description']);

                    $cnt++;
                }
            } 
            if($totalDesCharCount< 583)
                $customFontSize = '1em';//16px
            if($totalDesCharCount>300 && $totalDesCharCount<380)
                $customFontSize = '0.98em';  //14.4px  
            if($totalDesCharCount>380 && $totalDesCharCount<450)
                $customFontSize = '1em'; //12.8px
            if($totalDesCharCount>450 && $totalDesCharCount<525)
                $customFontSize = '0.90em';   //11.2px 
            if($totalDesCharCount>583)
                $customFontSize = '0.88em'; //9.6px
            
            $poTax          =  $this->bean->tax_c;
            $poHazardFee    =  $this->bean->hazard_fee_c;
            $poShippingCost =  $this->bean->shipping_cost;
            $poNetCost      =  $totalPrice+$poTax+$poHazardFee+$poShippingCost;
            $this->ss->assign('customFontSize', $customFontSize);
            $this->ss->assign('ProductData', $productData); 
            $this->ss->assign('totalPrice', number_format($totalPrice,2)); 
            $this->ss->assign('poTax', number_format($poTax,2) );
            $this->ss->assign('poHazardFee',number_format($poHazardFee,2) );
            $this->ss->assign('poShippingCost', number_format($poShippingCost,2) );
            $this->ss->assign('poNetCost', number_format($poNetCost,2));
            $this->ss->assign('fields', $fields);
        } 
    }
}