<?php

if (!defined('sugarEntry') || !sugarEntry)
  die('Not A Valid Entry Point');

class setPOstatus
{
  function setPOstatusAfterRelAdd($bean, $event, $arguments)
  {
    global $db, $current_user;
    $module = $arguments['module'];
    $poid = $bean->id;
    $relatedModule = $arguments['related_module'];
    $currentPOBean = BeanFactory::retrieveBean('PO_Purchase_Order', $poid);
    $currentPOBean->load_relationship('po_purchase_order_poi_purchase_order_item_1');
    $currentPOBean->load_relationship('po_purchase_order_ori_order_request_item_1');
    /** Load poi and ori bean to get all the linked record of poi and ori */
    $relatedPOI = $currentPOBean->po_purchase_order_poi_purchase_order_item_1->get();
    $relatedORI = $currentPOBean->po_purchase_order_ori_order_request_item_1->get();
    /**linkedrecstatus array : To store the status valye of all the linked poi and ori records  */
    $linkedrecstatus = array();
    if ($module === 'PO_Purchase_Order' && $relatedModule === 'POI_Purchase_Order_Item' || $module === 'PO_Purchase_Order' && $relatedModule === 'ORI_Order_Request_Item') {
      /*find all the lined poi record and get the status value */
      foreach ($relatedPOI as $POIid) {
        $clonedBeanPOI = BeanFactory::getBean('POI_Purchase_Order_Item', $POIid);
        $poi_status = $clonedBeanPOI->status;
        if ($poi_status != 'Inventory') {
          $linkedrecstatus[] = $poi_status;
        }
      }
      /*find all the lined ori record and get the status value */
      foreach ($relatedORI as $ORIid) {
        $clonedBeanORI = BeanFactory::getBean('ORI_Order_Request_Item', $ORIid);
        $ori_status = $clonedBeanORI->status;
        if ($ori_status != 'Inventory') {
          $linkedrecstatus[] = $ori_status;
        }
      }
       /**25 nov 2021 : #1778 : Status field on Purchase Orders to automatically update to read only 
       * Complete if all of the ORIs and POIs linked to the PO have Status = Fully Received. */
      if ((count($linkedrecstatus) == 0) && (count($relatedPOI) != 0 || count($relatedORI) != 0)) {
        /**To store the old value of status field , we are using status_old_c field */
        $bean->status_old_c = $bean->status_c;
        $bean->status_c = "Complete";
        $bean->save();
      } else {
          /**In case status valye changed from full recevied to other then it will pull the old value to status field */
        if ($bean->status_old_c != '') {
          $bean->status_c = $bean->status_old_c;
          $bean->save();
        }
      }
    }
  }
}
?>