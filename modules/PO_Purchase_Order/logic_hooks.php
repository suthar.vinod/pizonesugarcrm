<?php
$hook_version = 1;
$hook_array = Array();
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(
    '1',
    'Purchase Order Name Creation',
    'custom/modules/PO_Purchase_Order/customLogicPONameHook.php',
    'customLogicPONameHook',
    'generateSystemId',
);
$hook_array['before_save'][] = array(
    '2',
    'Auto-update PO Status to Complete',
    'custom/modules/PO_Purchase_Order/setPOstatusOnSave.php',
    'setPOstatusOnSave',
    'setPOstatusAfterSave',
);

$hook_array['after_save'][] = Array(
    '3',
    'Update Purchase Order Item Order Date',
    'custom/modules/POI_Purchase_Order_Item/updatePOIOrderDate.php',
    'updatePOIOrderDate',
    'updateOrderDate',
);

$hook_array['after_save'][] = Array(
    '4',
    'Update Order Request Item Estimated Arrival Date',
    'custom/modules/PO_Purchase_Order/updateORIEstimatedArrivalDate.php',
    'updateORIEstimatedArrivalDate',
    'updateEstimatedArrivalDate',
);
$hook_array['after_relationship_add'][] = array(
    '5',
    'Auto-update PO Status to Complete',
    'custom/modules/PO_Purchase_Order/setPOstatus.php',
    'setPOstatus',
    'setPOstatusAfterRelAdd',
);
$hook_array['after_relationship_delete'][] = array(
    '6',
    'Auto-update PO Status to Complete',
    'custom/modules/PO_Purchase_Order/setPOstatus.php',
    'setPOstatus',
    'setPOstatusAfterRelAdd',
);
?>