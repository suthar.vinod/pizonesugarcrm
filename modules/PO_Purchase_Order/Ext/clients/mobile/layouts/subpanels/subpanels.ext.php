<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-10-19 09:25:42
$viewdefs['PO_Purchase_Order']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'po_purchase_order_ori_order_request_item_1',
  ),
);

// created: 2021-10-19 09:29:00
$viewdefs['PO_Purchase_Order']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'po_purchase_order_poi_purchase_order_item_1',
  ),
);