<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-02-25 12:36:42
$viewdefs['PO_Purchase_Order']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'po_purchase_order_ori_order_request_item_1',
  ),
);

// created: 2020-07-31 12:33:12
$viewdefs['PO_Purchase_Order']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'po_purchase_order_poi_purchase_order_item_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['PO_Purchase_Order']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'po_purchase_order_ori_order_request_item_1',
  'view' => 'subpanel-for-po_purchase_order-po_purchase_order_ori_order_request_item_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['PO_Purchase_Order']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'po_purchase_order_poi_purchase_order_item_1',
  'view' => 'subpanel-for-po_purchase_order-po_purchase_order_poi_purchase_order_item_1',
);
