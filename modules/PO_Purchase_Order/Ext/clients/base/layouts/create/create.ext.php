<?php
// WARNING: The contents of this file are auto-generated.



$module = 'PO_Purchase_Order';
$components = $viewdefs[$module]['base']['layout']['create']['components'] ?? [];
$components = \Sugarcrm\Sugarcrm\custom\src\wsystems\InventoryManagementCustomizations\IMUtils::injectAfterSidebarMainpane(
                $components, [
            'layout' => [
                'type' => 'base',
                'name' => 'dashboard-pane',
                'css_class' => 'dashboard-pane',
                'components' => [
                    [
                        'layout' => 'ori-selection',
                    ],
                    
                ],
            ],
                ], 'preview-pane'
);
$viewdefs[$module]['base']['layout']['create']['components'] = $components;
