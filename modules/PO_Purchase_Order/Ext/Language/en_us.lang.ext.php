<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Language/en_us.custompo_purchase_order_ori_order_request_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Order Request Items';
$mod_strings['LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE'] = 'Order Request Items';

?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Language/en_us.custompo_purchase_order_poi_purchase_order_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE'] = 'Purchase Order Items';
$mod_strings['LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE'] = 'Purchase Order Items';

?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NUMBER_2_C_AN_ACCOUNT_NUMBER_ID'] = 'Account Number (related Account Number ID)';
$mod_strings['LBL_ACCOUNT_NUMBER_2'] = 'Account Number';
$mod_strings['LBL_STATUS'] = 'Status';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_TAX'] = 'Tax';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_HAZARD_FEE'] = 'Hazard Fee';
$mod_strings['LBL_ACCOUNT_NUMBER'] = 'Account # (Obsolete)';
$mod_strings['LBL_DESCRIPTION'] = 'Notes';
$mod_strings['LBL_PO_COMMENTS'] = 'PO Comments';

?>
