<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/WirelessLayoutdefs/po_purchase_order_ori_order_request_item_1_PO_Purchase_Order.php

 // created: 2021-10-19 09:25:42
$layout_defs["PO_Purchase_Order"]["subpanel_setup"]['po_purchase_order_ori_order_request_item_1'] = array (
  'order' => 100,
  'module' => 'ORI_Order_Request_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'get_subpanel_data' => 'po_purchase_order_ori_order_request_item_1',
);

?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/WirelessLayoutdefs/po_purchase_order_poi_purchase_order_item_1_PO_Purchase_Order.php

 // created: 2021-10-19 09:29:00
$layout_defs["PO_Purchase_Order"]["subpanel_setup"]['po_purchase_order_poi_purchase_order_item_1'] = array (
  'order' => 100,
  'module' => 'POI_Purchase_Order_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'get_subpanel_data' => 'po_purchase_order_poi_purchase_order_item_1',
);

?>
