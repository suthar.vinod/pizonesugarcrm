<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Dependencies/account_number_dep.php

$dependencies['PO_Purchase_Order']['account_number_empty01'] = array(
    'hooks' => array("all"),
    'triggerFields' => array('vendor'),
    'onload' => false,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => "an_account_number_id_c",
                'value' => ''
            )
        ),
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => "account_number_2_c",
                'value' => ''
            )
        ),
        
    ),
);



?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Dependencies/po_status_depedency.php
 
/*#1778 :  read only Complete if all of the ORIs and POIs linked to the PO have Status = Fully Received.  */
$dependencies['PO_Purchase_Order']['read_only_status'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('status_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'status_c',
                'value' => 'equal($status_c,"Complete")',
            ),
        ),
    ),
);

?>
