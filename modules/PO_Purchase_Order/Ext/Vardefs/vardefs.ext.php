<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/po_purchase_order_ori_order_request_item_1_PO_Purchase_Order.php

// created: 2021-02-25 12:36:42
$dictionary["PO_Purchase_Order"]["fields"]["po_purchase_order_ori_order_request_item_1"] = array (
  'name' => 'po_purchase_order_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'po_purchase_order_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'vname' => 'LBL_PO_PURCHASE_ORDER_ORI_ORDER_REQUEST_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE',
  'id_name' => 'po_purchase_order_ori_order_request_item_1po_purchase_order_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/po_purchase_order_poi_purchase_order_item_1_PO_Purchase_Order.php

// created: 2020-07-31 12:33:12
$dictionary["PO_Purchase_Order"]["fields"]["po_purchase_order_poi_purchase_order_item_1"] = array (
  'name' => 'po_purchase_order_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'po_purchase_order_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'POI_Purchase_Order_Item',
  'bean_name' => 'POI_Purchase_Order_Item',
  'vname' => 'LBL_PO_PURCHASE_ORDER_POI_PURCHASE_ORDER_ITEM_1_FROM_PO_PURCHASE_ORDER_TITLE',
  'id_name' => 'po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/sugarfield_an_account_number_id_c.php

 // created: 2021-02-18 14:39:25

 
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/sugarfield_account_number_2_c.php

 // created: 2021-02-18 14:39:25
$dictionary['PO_Purchase_Order']['fields']['account_number_2_c']['labelValue']='Account Number';
$dictionary['PO_Purchase_Order']['fields']['account_number_2_c']['dependency']='';
$dictionary['PO_Purchase_Order']['fields']['account_number_2_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/sugarfield_tax_c.php

 // created: 2021-04-22 13:50:39
$dictionary['PO_Purchase_Order']['fields']['tax_c']['labelValue']='Tax';
$dictionary['PO_Purchase_Order']['fields']['tax_c']['enforced']='';
$dictionary['PO_Purchase_Order']['fields']['tax_c']['dependency']='';
$dictionary['PO_Purchase_Order']['fields']['tax_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['PO_Purchase_Order']['fields']['tax_c']['required_formula']='';
$dictionary['PO_Purchase_Order']['fields']['tax_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/sugarfield_hazard_fee_c.php

 // created: 2021-04-22 13:51:28
$dictionary['PO_Purchase_Order']['fields']['hazard_fee_c']['labelValue']='Hazard Fee';
$dictionary['PO_Purchase_Order']['fields']['hazard_fee_c']['enforced']='';
$dictionary['PO_Purchase_Order']['fields']['hazard_fee_c']['dependency']='';
$dictionary['PO_Purchase_Order']['fields']['hazard_fee_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['PO_Purchase_Order']['fields']['hazard_fee_c']['required_formula']='';
$dictionary['PO_Purchase_Order']['fields']['hazard_fee_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/sugarfield_account_number.php

 // created: 2021-02-18 14:38:15
$dictionary['PO_Purchase_Order']['fields']['account_number']['required']=false;
$dictionary['PO_Purchase_Order']['fields']['account_number']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/sugarfield_total_cost.php

 // created: 2021-08-11 11:13:23
$dictionary['PO_Purchase_Order']['fields']['total_cost']['required']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['name']='total_cost';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['vname']='LBL_TOTAL_COST';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['type']='currency';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['massupdate']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['no_default']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['comments']='';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['help']='';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['importable']='false';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['duplicate_merge']='disabled';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['duplicate_merge_dom_value']=0;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['audited']=true;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['reportable']=true;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['unified_search']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['merge_filter']='disabled';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['pii']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['calculated']='1';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['len']=26;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['size']='20';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['enable_range_search']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['precision']=6;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['PO_Purchase_Order']['fields']['total_cost']['hidemassupdate']=false;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['convertToBase']=true;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['showTransactionalAmount']=true;
$dictionary['PO_Purchase_Order']['fields']['total_cost']['formula']='add($hazard_fee_c,$tax_c,$shipping_cost,rollupSum($po_purchase_order_poi_purchase_order_item_1,"purchaserequesttotal_c"),rollupSum($po_purchase_order_ori_order_request_item_1,"orderrequesttotal_c"))';
$dictionary['PO_Purchase_Order']['fields']['total_cost']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/sugarfield_shipping_cost.php

 // created: 2021-07-30 04:58:50
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['required']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['name']='shipping_cost';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['vname']='LBL_SHIPPING_COST';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['type']='currency';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['massupdate']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['no_default']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['comments']='';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['help']='';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['importable']='true';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['duplicate_merge']='enabled';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['duplicate_merge_dom_value']='1';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['audited']=true;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['reportable']=true;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['unified_search']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['merge_filter']='disabled';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['pii']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['default']='';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['calculated']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['len']=26;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['size']='20';
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['enable_range_search']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['precision']=6;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['hidemassupdate']=false;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['convertToBase']=true;
$dictionary['PO_Purchase_Order']['fields']['shipping_cost']['showTransactionalAmount']=true;

 
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/sugarfieldVardef.php

 // created: 2020-08-14 05:42:02
$dictionary['PO_Purchase_Order']['fields']['account_number']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/sugarfield_name.php

 // created: 2020-07-30 08:46:19
$dictionary['PO_Purchase_Order']['fields']['name']['required']=false;
$dictionary['PO_Purchase_Order']['fields']['name']['unified_search']=false;
$dictionary['PO_Purchase_Order']['fields']['name']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/sugarfield_description.php

 // created: 2022-01-11 06:18:58
$dictionary['PO_Purchase_Order']['fields']['description']['audited']=true;
$dictionary['PO_Purchase_Order']['fields']['description']['massupdate']=false;
$dictionary['PO_Purchase_Order']['fields']['description']['hidemassupdate']=false;
$dictionary['PO_Purchase_Order']['fields']['description']['comments']='Full text of the note';
$dictionary['PO_Purchase_Order']['fields']['description']['duplicate_merge']='enabled';
$dictionary['PO_Purchase_Order']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['PO_Purchase_Order']['fields']['description']['merge_filter']='disabled';
$dictionary['PO_Purchase_Order']['fields']['description']['unified_search']=false;
$dictionary['PO_Purchase_Order']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['PO_Purchase_Order']['fields']['description']['calculated']=false;
$dictionary['PO_Purchase_Order']['fields']['description']['rows']='6';
$dictionary['PO_Purchase_Order']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/sugarfield_status_c.php

 // created: 2022-02-22 07:08:21
$dictionary['PO_Purchase_Order']['fields']['status_c']['labelValue']='Status';
$dictionary['PO_Purchase_Order']['fields']['status_c']['dependency']='';
$dictionary['PO_Purchase_Order']['fields']['status_c']['required_formula']='';
$dictionary['PO_Purchase_Order']['fields']['status_c']['readonly_formula']='';
$dictionary['PO_Purchase_Order']['fields']['status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/PO_Purchase_Order/Ext/Vardefs/sugarfield_po_comments_c.php

 // created: 2022-04-14 06:54:19
$dictionary['PO_Purchase_Order']['fields']['po_comments_c']['labelValue']='PO Comments';
$dictionary['PO_Purchase_Order']['fields']['po_comments_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['PO_Purchase_Order']['fields']['po_comments_c']['enforced']='';
$dictionary['PO_Purchase_Order']['fields']['po_comments_c']['dependency']='';
$dictionary['PO_Purchase_Order']['fields']['po_comments_c']['required_formula']='';
$dictionary['PO_Purchase_Order']['fields']['po_comments_c']['readonly_formula']='';

 
?>
