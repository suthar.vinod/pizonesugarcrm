<?php 
 

class custom_calculation {
    function update_value($bean, $event, $arguments) {
        global $db;
        $current_year	= date("Y");
		$current_date2	= date("m/d/Y");
		$previous_week = strtotime("-1 week");
		$lastWeek_date	= date("Y-m-d",$previous_week); //lastWeek_date
		$GLOBALS['log']->fatal('lastWeek_date ===>:'.$lastWeek_date);
        $beanId    		= $bean->id;
		
		if($bean->id !=$bean->fetched_row['id']){
			$speciesID 		= $bean->s_species_sc_species_census_1_name;
			$speciesIDa 	= $bean->s_species_sc_species_census_1s_species_ida;
			
			$tsTotalCount		= 0;
			$tsCountAllocated	= 0;
			$tsCountOnStudy		= 0; 
			$tsCountStock		= 0;
		 
			$sql1  = "SELECT count(TS.id) AS tsCountAllocated FROM anml_animals AS TS 
			   LEFT JOIN anml_animals_cstm AS TSC ON TS.id=TSC.id_c
			   WHERE TSC.s_species_id_c='".$speciesIDa."'
			   AND TS.enrollment_status = 'On Study'
			   AND (TSC.termination_date_c IS NULL OR  TSC.termination_date_c='')
			   AND 
			   
			   ((TSC.assigned_to_wp_c='APS001-AH01'
			   AND (TSC.m03_work_product_id_c IS NOT NULL  OR TSC.m03_work_product_id_c!='') )
			   
			   OR (TSC.assigned_to_wp_c!='APS001-AH01'
				   AND (TSC.confirmed_on_study_c IS NULL  OR TSC.confirmed_on_study_c='0'))
			  )
			   AND TS.deleted = 0
			  AND TS.name NOT IN ('Caprine','Canine','Bovine','Rabit','Ovine','Mouse','Hamster','Porcine','Rat','Guinea Pig','GuineaPg','Lagomorph','Rabbit') ";
		
			$sql1_exec = $db->query($sql1);
			if ($sql1_exec->num_rows > 0) 
			{
				$result = $db->fetchByAssoc($sql1_exec);
				$tsCountAllocated = $result['tsCountAllocated'];
			}



		   $sql2  = "SELECT count(TS.id) AS tsCountOnStudy FROM anml_animals AS TS 
			   LEFT JOIN anml_animals_cstm AS TSC ON TS.id=TSC.id_c
			   WHERE TSC.s_species_id_c='".$speciesIDa."'
			   AND TSC.assigned_to_wp_c!='APS001-AH01'
			   AND TS.enrollment_status = 'On Study'
			   AND (TSC.termination_date_c IS NULL OR  TSC.termination_date_c='')
			   AND TSC.confirmed_on_study_c =1
			   AND TS.deleted = 0
			   AND TS.name NOT IN ('Caprine','Canine','Bovine','Rabit','Ovine','Mouse','Hamster','Porcine','Rat','Guinea Pig','GuineaPg','Lagomorph','Rabbit') ";
			$sql2_exec = $db->query($sql2);
			if ($sql2_exec->num_rows > 0) 
			{
				$result = $db->fetchByAssoc($sql2_exec);
				$tsCountOnStudy = $result['tsCountOnStudy'];
				
			}

			$sql3  = "SELECT count(TS.id) AS tsCountStock FROM anml_animals AS TS 
				LEFT JOIN anml_animals_cstm AS TSC ON TS.id=TSC.id_c
				WHERE TSC.s_species_id_c='".$speciesIDa."'
				AND TSC.assigned_to_wp_c='APS001-AH01'
				AND (TSC.m03_work_product_id_c IS NULL  OR TSC.m03_work_product_id_c='')
				AND TS.enrollment_status = 'On Study'
				AND (TSC.termination_date_c IS NULL OR  TSC.termination_date_c='')
				 AND TS.deleted = 0
				AND TS.name NOT IN ('Caprine','Canine','Bovine','Rabit','Ovine','Mouse','Hamster','Porcine','Rat','Guinea Pig','GuineaPg','Lagomorph','Rabbit') ";
		
			$sql3_exec = $db->query($sql3);
			 if ($sql3_exec->num_rows > 0) 
			 {
				 $result = $db->fetchByAssoc($sql3_exec);
				 $tsCountStock = $result['tsCountStock'];
				 
			 }
		 
			 $tsTotalCount = $tsCountAllocated+$tsCountOnStudy+$tsCountStock;
			 
			 
			$ytdQuantitySql4  = "SELECT count(TS.id) AS tsCountYTD FROM anml_animals AS TS 
			   LEFT JOIN anml_animals_cstm AS TSC ON TS.id=TSC.id_c
			   WHERE TSC.s_species_id_c='".$speciesIDa."' AND TS.deleted = 0 AND YEAR(TSC.date_received_c) = '".$current_year."' "; 
			$sql4_exec = $db->query($ytdQuantitySql4);
			//$GLOBALS['log']->fatal('species ytdQuantitySql4  == '.$ytdQuantitySql4);
			if ($sql4_exec->num_rows > 0) 
			{
				 $result 		= $db->fetchByAssoc($sql4_exec);
				 $tsCountYTD	= $result['tsCountYTD'];
			}
		
			$weekQuantitySql5  = "SELECT count(TS.id) AS tsCountWeek FROM anml_animals AS TS 
			   LEFT JOIN anml_animals_cstm AS TSC ON TS.id=TSC.id_c
			   WHERE TSC.s_species_id_c='".$speciesIDa."' AND TS.deleted = 0 
					AND YEAR(TSC.date_received_c) = '".$current_year."'
					AND TSC.date_received_c  >= '".$lastWeek_date."'";
			//$GLOBALS['log']->fatal('species weekQuantitySql5  == '.$weekQuantitySql5);				
			$sql5_exec = $db->query($weekQuantitySql5);
			if ($sql5_exec->num_rows > 0) 
			{
				 $result5 		= $db->fetchByAssoc($sql5_exec);
				 $tsCountWeek	= $result5['tsCountWeek'];
			}
			
			$totalQuantitySql6  = "SELECT count(TS.id) AS tsCountLiveTotal FROM anml_animals AS TS 
			   LEFT JOIN anml_animals_cstm AS TSC ON TS.id=TSC.id_c
			   WHERE TSC.s_species_id_c='".$speciesIDa."' AND TS.deleted = 0 
					AND (TSC.deceased_checkbox_c IS NOT NULL AND TSC.deceased_checkbox_c='0')
					AND TS.name NOT IN ('Caprine','Canine','Bovine','Rabit','Ovine','Mouse','Hamster','Porcine','Rat','Guinea Pig','GuineaPg','Lagomorph','Rabbit') "; 
			$GLOBALS['log']->fatal('species totalQuantitySql6  == '.$current_date2."===".$totalQuantitySql6);
			$sql6_exec = $db->query($totalQuantitySql6);
			if ($sql6_exec->num_rows > 0) 
			{
				 $result6 			= $db->fetchByAssoc($sql6_exec);
				 $tsCountLiveTotal	= $result6['tsCountLiveTotal'];
			}
		 
		 
		 
			$bean->on_study 		= $tsCountOnStudy;
			$bean->allocated 		= $tsCountAllocated; 
			$bean->stock 			= $tsCountStock; 
			$bean->total 			= $tsTotalCount; 
			$bean->quantity_ytd_c 	= $tsCountYTD; 
			$bean->quantity_received_per_week_c 	= $tsCountWeek; 
			$bean->total_quantity_c 	= $tsCountLiveTotal; 
		}
        
	}
}