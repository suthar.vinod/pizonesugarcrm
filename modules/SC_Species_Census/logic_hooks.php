<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['before_save'] = Array(); 
//$hook_array['before_save'][] = Array('1','workflow','include/workflow/WorkFlowHandler.php','WorkFlowHandler','WorkFlowHandler',);
$hook_array['before_save'][] = array(
   '15',
   'Update Calculated values ',
   'custom/modules/SC_Species_Census/custom_calculation.php',
   'custom_calculation',
   'update_value',
);

$hook_array['process_record'][] = array(
      11,
      'update user in Audit log Record',
      'custom/modules/SC_Species_Census/updateUserAuditLog.php',
      'updateUserAuditLog',
      'updateUserAudit'
   ); 

?>