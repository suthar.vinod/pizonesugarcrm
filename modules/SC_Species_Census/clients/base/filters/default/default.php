<?php
// created: 2021-05-13 09:19:10
$viewdefs['SC_Species_Census']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'allocated' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'date_2' => 
    array (
    ),
    'la_percent_assigned_2_c' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'name' => 
    array (
    ),
    'on_study' => 
    array (
    ),
    'quantity_ytd_c' => 
    array (
    ),
    'quantity_received_per_week_c' => 
    array (
    ),
    'sa_percent_assigned_2_c' => 
    array (
    ),
    's_species_sc_species_census_1_name' => 
    array (
    ),
    'stock' => 
    array (
    ),
    'total' => 
    array (
    ),
    'total_quantity_c' => 
    array (
    ),
  ),
);