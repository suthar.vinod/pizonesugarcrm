<?php
$module_name = 'SC_Species_Census';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'date_2',
                'label' => 'LBL_DATE_2',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'allocated',
                'label' => 'LBL_ALLOCATED',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'on_study',
                'label' => 'LBL_ON_STUDY',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'stock',
                'label' => 'LBL_STOCK',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'total',
                'label' => 'LBL_TOTAL',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 's_species_sc_species_census_1_name',
                'label' => 'LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_S_SPECIES_TITLE',
                'enabled' => true,
                'id' => 'S_SPECIES_SC_SPECIES_CENSUS_1S_SPECIES_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'quantity_ytd_c',
                'label' => 'LBL_QUANTITY_YTD',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'quantity_received_per_week_c',
                'label' => 'LBL_QUANTITY_RECEIVED_PER_WEEK',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'total_quantity_c',
                'label' => 'LBL_TOTAL_QUANTITY',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'la_percent_assigned_2_c',
                'label' => 'LBL_LA_PERCENT_ASSIGNED_2',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'sa_percent_assigned_2_c',
                'label' => 'LBL_SA_PERCENT_ASSIGNED_2',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
