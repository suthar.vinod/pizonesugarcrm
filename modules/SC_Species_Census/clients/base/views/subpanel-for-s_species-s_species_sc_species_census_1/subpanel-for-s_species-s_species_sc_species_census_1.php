<?php
// created: 2021-05-13 09:23:05
$viewdefs['SC_Species_Census']['base']['view']['subpanel-for-s_species-s_species_sc_species_census_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'allocated',
          'label' => 'LBL_ALLOCATED',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'on_study',
          'label' => 'LBL_ON_STUDY',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'stock',
          'label' => 'LBL_STOCK',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'total',
          'label' => 'LBL_TOTAL',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'quantity_ytd_c',
          'label' => 'LBL_QUANTITY_YTD',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'quantity_received_per_week_c',
          'label' => 'LBL_QUANTITY_RECEIVED_PER_WEEK',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'total_quantity_c',
          'label' => 'LBL_TOTAL_QUANTITY',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'la_percent_assigned_2_c',
          'label' => 'LBL_LA_PERCENT_ASSIGNED_2',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'sa_percent_assigned_2_c',
          'label' => 'LBL_SA_PERCENT_ASSIGNED_2',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);