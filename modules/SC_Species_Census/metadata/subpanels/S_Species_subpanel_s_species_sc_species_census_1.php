<?php
// created: 2021-05-13 09:23:05
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'allocated' => 
  array (
    'readonly' => false,
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_ALLOCATED',
    'width' => 10,
  ),
  'on_study' => 
  array (
    'readonly' => false,
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_ON_STUDY',
    'width' => 10,
  ),
  'stock' => 
  array (
    'readonly' => false,
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_STOCK',
    'width' => 10,
  ),
  'total' => 
  array (
    'readonly' => false,
    'type' => 'int',
    'vname' => 'LBL_TOTAL',
    'width' => 10,
    'default' => true,
  ),
  'quantity_ytd_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'int',
    'vname' => 'LBL_QUANTITY_YTD',
    'width' => 10,
    'default' => true,
  ),
  'quantity_received_per_week_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'int',
    'vname' => 'LBL_QUANTITY_RECEIVED_PER_WEEK',
    'width' => 10,
    'default' => true,
  ),
  'total_quantity_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'int',
    'vname' => 'LBL_TOTAL_QUANTITY',
    'width' => 10,
    'default' => true,
  ),
  'la_percent_assigned_2_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'decimal',
    'vname' => 'LBL_LA_PERCENT_ASSIGNED_2',
    'width' => 10,
    'default' => true,
  ),
  'sa_percent_assigned_2_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'decimal',
    'vname' => 'LBL_SA_PERCENT_ASSIGNED_2',
    'width' => 10,
    'default' => true,
  ),
);