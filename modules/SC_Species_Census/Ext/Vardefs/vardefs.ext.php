<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SC_Species_Census/Ext/Vardefs/s_species_sc_species_census_1_SC_Species_Census.php

// created: 2021-04-29 06:58:32
$dictionary["SC_Species_Census"]["fields"]["s_species_sc_species_census_1"] = array (
  'name' => 's_species_sc_species_census_1',
  'type' => 'link',
  'relationship' => 's_species_sc_species_census_1',
  'source' => 'non-db',
  'module' => 'S_Species',
  'bean_name' => 'S_Species',
  'side' => 'right',
  'vname' => 'LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_SC_SPECIES_CENSUS_TITLE',
  'id_name' => 's_species_sc_species_census_1s_species_ida',
  'link-type' => 'one',
);
$dictionary["SC_Species_Census"]["fields"]["s_species_sc_species_census_1_name"] = array (
  'name' => 's_species_sc_species_census_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_S_SPECIES_TITLE',
  'save' => true,
  'id_name' => 's_species_sc_species_census_1s_species_ida',
  'link' => 's_species_sc_species_census_1',
  'table' => 's_species',
  'module' => 'S_Species',
  'rname' => 'name',
);
$dictionary["SC_Species_Census"]["fields"]["s_species_sc_species_census_1s_species_ida"] = array (
  'name' => 's_species_sc_species_census_1s_species_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_SC_SPECIES_CENSUS_TITLE_ID',
  'id_name' => 's_species_sc_species_census_1s_species_ida',
  'link' => 's_species_sc_species_census_1',
  'table' => 's_species',
  'module' => 'S_Species',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/SC_Species_Census/Ext/Vardefs/sugarfield_name.php

 // created: 2021-04-29 07:12:22
$dictionary['SC_Species_Census']['fields']['name']['importable']='false';
$dictionary['SC_Species_Census']['fields']['name']['duplicate_merge']='disabled';
$dictionary['SC_Species_Census']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['SC_Species_Census']['fields']['name']['merge_filter']='disabled';
$dictionary['SC_Species_Census']['fields']['name']['unified_search']=false;
$dictionary['SC_Species_Census']['fields']['name']['calculated']='true';
$dictionary['SC_Species_Census']['fields']['name']['formula']='concat(related($s_species_sc_species_census_1,"name")," Census ",toString($date_2))';
$dictionary['SC_Species_Census']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/SC_Species_Census/Ext/Vardefs/sugarfield_quantity_ytd_c.php

 // created: 2021-05-13 08:04:28
$dictionary['SC_Species_Census']['fields']['quantity_ytd_c']['labelValue']='Quantity YTD';
$dictionary['SC_Species_Census']['fields']['quantity_ytd_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['SC_Species_Census']['fields']['quantity_ytd_c']['enforced']='';
$dictionary['SC_Species_Census']['fields']['quantity_ytd_c']['dependency']='';
$dictionary['SC_Species_Census']['fields']['quantity_ytd_c']['required_formula']='';
$dictionary['SC_Species_Census']['fields']['quantity_ytd_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/SC_Species_Census/Ext/Vardefs/sugarfield_quantity_received_per_week_c.php

 // created: 2021-05-13 08:05:58
$dictionary['SC_Species_Census']['fields']['quantity_received_per_week_c']['labelValue']='Quantity Received Per Week';
$dictionary['SC_Species_Census']['fields']['quantity_received_per_week_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['SC_Species_Census']['fields']['quantity_received_per_week_c']['enforced']='';
$dictionary['SC_Species_Census']['fields']['quantity_received_per_week_c']['dependency']='';
$dictionary['SC_Species_Census']['fields']['quantity_received_per_week_c']['required_formula']='';
$dictionary['SC_Species_Census']['fields']['quantity_received_per_week_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/SC_Species_Census/Ext/Vardefs/sugarfield_total_quantity_c.php

 // created: 2021-05-13 08:07:37
$dictionary['SC_Species_Census']['fields']['total_quantity_c']['labelValue']='Total Quantity';
$dictionary['SC_Species_Census']['fields']['total_quantity_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['SC_Species_Census']['fields']['total_quantity_c']['enforced']='';
$dictionary['SC_Species_Census']['fields']['total_quantity_c']['dependency']='';
$dictionary['SC_Species_Census']['fields']['total_quantity_c']['required_formula']='';
$dictionary['SC_Species_Census']['fields']['total_quantity_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/SC_Species_Census/Ext/Vardefs/sugarfield_la_percent_assigned_2_c.php

 // created: 2021-05-13 08:09:00
$dictionary['SC_Species_Census']['fields']['la_percent_assigned_2_c']['labelValue']='LA % Assigned';
$dictionary['SC_Species_Census']['fields']['la_percent_assigned_2_c']['enforced']='';
$dictionary['SC_Species_Census']['fields']['la_percent_assigned_2_c']['dependency']='';
$dictionary['SC_Species_Census']['fields']['la_percent_assigned_2_c']['required_formula']='';
$dictionary['SC_Species_Census']['fields']['la_percent_assigned_2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/SC_Species_Census/Ext/Vardefs/sugarfield_sa_percent_assigned_2_c.php

 // created: 2021-05-13 08:10:45
$dictionary['SC_Species_Census']['fields']['sa_percent_assigned_2_c']['labelValue']='SA % Assigned';
$dictionary['SC_Species_Census']['fields']['sa_percent_assigned_2_c']['enforced']='';
$dictionary['SC_Species_Census']['fields']['sa_percent_assigned_2_c']['dependency']='';
$dictionary['SC_Species_Census']['fields']['sa_percent_assigned_2_c']['required_formula']='';
$dictionary['SC_Species_Census']['fields']['sa_percent_assigned_2_c']['readonly_formula']='';

 
?>
