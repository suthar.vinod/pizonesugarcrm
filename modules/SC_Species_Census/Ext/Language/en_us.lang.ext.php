<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SC_Species_Census/Ext/Language/en_us.customs_species_sc_species_census_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/SC_Species_Census/Ext/Language/en_us.customs_species_sc_species_census_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_S_SPECIES_TITLE'] = 'Species';
$mod_strings['LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_SC_SPECIES_CENSUS_TITLE'] = 'Species';

?>
<?php
// Merged from custom/Extension/modules/SC_Species_Census/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_S_SPECIES_TITLE'] = 'Species';
$mod_strings['LBL_S_SPECIES_SC_SPECIES_CENSUS_1_FROM_SC_SPECIES_CENSUS_TITLE'] = 'Species';


?>
<?php
// Merged from custom/Extension/modules/SC_Species_Census/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_QUANTITY_YTD'] = 'Quantity YTD';
$mod_strings['LBL_QUANTITY_RECEIVED_PER_WEEK'] = 'Quantity Received Per Week';
$mod_strings['LBL_TOTAL_QUANTITY'] = 'Total Quantity';
$mod_strings['LBL_LA_PERCENT_ASSIGNED_2'] = 'LA % Assigned';
$mod_strings['LBL_SA_PERCENT_ASSIGNED_2'] = 'SA % Assigned';
$mod_strings['LBL_SC_SPECIES_CENSUS_RECORD_DASHBOARD'] = 'Species Census Record Dashboard';

?>
