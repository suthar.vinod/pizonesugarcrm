<?php
 
	global $db;
	$current_date	= "2022-03-14"; //2022-01-17
	$current_date2	= "03/14/2022"; //date("m/d/Y");
	$current_date3	= "03-14-2022"; //date("m-d-Y");
	$current_date4	= "2022-03-14"; //date("Y-m-d");
	$current_year	= date("Y"); 
	
	$laAssignName[0] =  'Bovine Census '.$current_date2;
	$laAssignName[1] =  'Canine Census '.$current_date2;
	$laAssignName[2] =  'Caprine Census '.$current_date2;
	$laAssignName[3] =  'Ovine Census '.$current_date2;
	$laAssignName[4] =  'Porcine Census '.$current_date2;
	$laAssignName[5] =  'Bovine Census '.$current_date3;
	$laAssignName[6] =  'Canine Census '.$current_date3;
	$laAssignName[7] =  'Caprine Census '.$current_date3;
	$laAssignName[8] =  'Ovine Census '.$current_date3;
	$laAssignName[9] =  'Porcine Census '.$current_date3;
	$laAssignName[10] = 'Bovine Census '.$current_date4;
	$laAssignName[11] = 'Canine Census '.$current_date4;
	$laAssignName[12] = 'Caprine Census '.$current_date4;
	$laAssignName[13] = 'Ovine Census '.$current_date4;
	$laAssignName[14] = 'Porcine Census '.$current_date4;
	
	$laAssigned 		= implode("','",$laAssignName);	
	$laCalculation 		= 0;
	echo "<br><br>===>".$queryCustomLaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species laCalculation Query == '.$queryCustomLaAssigned);
	$queryLAResult = $db->query($queryCustomLaAssigned);
	if ($queryLAResult->num_rows > 0) 
    {
		 $resultLA 			= $db->fetchByAssoc($queryLAResult);
		 $scName	= $resultLA['name'];
		 $totalAllocated	= $resultLA['totalAllocated'];
		 $totalOnStudy		= $resultLA['totalOnStudy'];
		 $totalValue		= $resultLA['totalValue'];
		 
		 $laCalculation = (($totalAllocated+$totalOnStudy)/$totalValue)*100;
		 echo "<br>===>".'species laCalculation  == '.$scName.'==='.$laCalculation.'==='.$totalAllocated.'===='.$totalOnStudy.'===='.$totalStock ;
	}

	echo "<br><br>===>".$queryCustomLaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$queryLAIDResult = $db->query($queryCustomLaAssignedID);
	if ($queryLAIDResult->num_rows > 0) 
    {
		 while($resultLAID 			= $db->fetchByAssoc($queryLAIDResult)){
			$beanID		= $resultLAID['id'];
			$SC_Bean	= BeanFactory::getBean('SC_Species_Census', $beanID);
			$SC_Bean->la_percent_assigned_2_c	= $laCalculation;	
			$SC_Bean->save();
		}		  
	}

	
		
	/**/
	$saAssignName[0] =  'Lagomorph Census '.$current_date2;
	$saAssignName[1] =  'Rat Census '.$current_date2;
	$saAssignName[2] =  'Murine Census '.$current_date2;
	$saAssignName[3] =  'Hamster Census '.$current_date2;
	$saAssignName[4] =  'Guinea Pig Census '.$current_date2;
	$saAssignName[5] =  'GuineaPg Census '.$current_date2;
	$saAssignName[6] =  'Lagomorph Census '.$current_date3;
	$saAssignName[7] =  'Rat Census '.$current_date3;
	$saAssignName[8] =  'Murine Census '.$current_date3;
	$saAssignName[9] =  'Hamster Census '.$current_date3;
	$saAssignName[10] =  'Guinea Pig Census '.$current_date3;
	$saAssignName[11] =  'GuineaPg Census '.$current_date3;
	$saAssignName[12] =  'Lagomorph Census '.$current_date4;
	$saAssignName[13] =  'Rat Census '.$current_date4;
	$saAssignName[14] =  'Murine Census '.$current_date4;
	$saAssignName[15] =  'Hamster Census '.$current_date4;
	$saAssignName[16] =  'Guinea Pig Census '.$current_date4;
	$saAssignName[17] =  'GuineaPg Census '.$current_date4;
	
	$saCalculation = 0;
	$saAssigned = implode("','",$saAssignName);
	echo "<br><br>===>".$queryCustomSaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species saCalculation Query == '.$queryCustomSaAssigned);
	$querySAResult = $db->query($queryCustomSaAssigned);
	if ($querySAResult->num_rows > 0) 
    {
		 $resultSA 			= $db->fetchByAssoc($querySAResult);
		  $scName	= $resultSA['name'];
		  $totalSA_Allocated	= $resultSA['totalAllocated'];
		 $totalSA_OnStudy	= $resultSA['totalOnStudy'];
		 $totalSA_Value		= $resultSA['totalValue'];
		 $saCalculation = (($totalSA_Allocated+$totalSA_OnStudy)/$totalSA_Value)*100;
		 echo "<br>===>".'species saCalculation == '.$scName.'==='.$saCalculation.'==='.$totalSA_Allocated.'===='.$totalSA_OnStudy.'===='.$totalSA_Stock ; 	 
	}
	
	echo "<br><br>===>".$querySaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$querySAIDResult = $db->query($querySaAssignedID);
	if ($querySAIDResult->num_rows > 0) 
    {
		 while($resultSAID 			= $db->fetchByAssoc($querySAIDResult)){
			 
			$beanSAID		= $resultSAID['id'];
			$SC_SABean	= BeanFactory::getBean('SC_Species_Census', $beanSAID);
			$SC_SABean->sa_percent_assigned_2_c	= $saCalculation;
			//$GLOBALS['log']->fatal('===Newspecies '.$SC_SABean->name.' saCalculation  ==> '.$saCalculation);	
			$SC_SABean->save();
		}		  
	}
	echo "<br>==========================================================================================";
	echo "<br>==========================================================================================";
	 
	$current_date	= "2022-03-07"; //2022-01-17
	$current_date2	= "03/07/2022"; //date("m/d/Y");
	$current_date3	= "03-07-2022"; //date("m-d-Y");
	$current_date4	= "2022-03-07"; //date("Y-m-d");
	$current_year	= date("Y"); 
	
	$laAssignName[0] =  'Bovine Census '.$current_date2;
	$laAssignName[1] =  'Canine Census '.$current_date2;
	$laAssignName[2] =  'Caprine Census '.$current_date2;
	$laAssignName[3] =  'Ovine Census '.$current_date2;
	$laAssignName[4] =  'Porcine Census '.$current_date2;
	$laAssignName[5] =  'Bovine Census '.$current_date3;
	$laAssignName[6] =  'Canine Census '.$current_date3;
	$laAssignName[7] =  'Caprine Census '.$current_date3;
	$laAssignName[8] =  'Ovine Census '.$current_date3;
	$laAssignName[9] =  'Porcine Census '.$current_date3;
	$laAssignName[10] = 'Bovine Census '.$current_date4;
	$laAssignName[11] = 'Canine Census '.$current_date4;
	$laAssignName[12] = 'Caprine Census '.$current_date4;
	$laAssignName[13] = 'Ovine Census '.$current_date4;
	$laAssignName[14] = 'Porcine Census '.$current_date4;
	
	$laAssigned 		= implode("','",$laAssignName);	
	$laCalculation 		= 0;
	echo "<br><br>===>".$queryCustomLaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species laCalculation Query == '.$queryCustomLaAssigned);
	$queryLAResult = $db->query($queryCustomLaAssigned);
	if ($queryLAResult->num_rows > 0) 
    {
		 $resultLA 			= $db->fetchByAssoc($queryLAResult);
		 $scName	= $resultLA['name'];
		 $totalAllocated	= $resultLA['totalAllocated'];
		 $totalOnStudy		= $resultLA['totalOnStudy'];
		 $totalValue		= $resultLA['totalValue'];
		 
		 $laCalculation = (($totalAllocated+$totalOnStudy)/$totalValue)*100;
		 echo "<br>===>".'species laCalculation  == '.$scName.'==='.$laCalculation.'==='.$totalAllocated.'===='.$totalOnStudy.'===='.$totalStock ;
	}

	echo "<br><br>===>".$queryCustomLaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$queryLAIDResult = $db->query($queryCustomLaAssignedID);
	if ($queryLAIDResult->num_rows > 0) 
    {
		 while($resultLAID 			= $db->fetchByAssoc($queryLAIDResult)){
			$beanID		= $resultLAID['id'];
			$SC_Bean	= BeanFactory::getBean('SC_Species_Census', $beanID);
			$SC_Bean->la_percent_assigned_2_c	= $laCalculation;	
			$SC_Bean->save();
		}		  
	}

	
		
	/**/
	$saAssignName[0] =  'Lagomorph Census '.$current_date2;
	$saAssignName[1] =  'Rat Census '.$current_date2;
	$saAssignName[2] =  'Murine Census '.$current_date2;
	$saAssignName[3] =  'Hamster Census '.$current_date2;
	$saAssignName[4] =  'Guinea Pig Census '.$current_date2;
	$saAssignName[5] =  'GuineaPg Census '.$current_date2;
	$saAssignName[6] =  'Lagomorph Census '.$current_date3;
	$saAssignName[7] =  'Rat Census '.$current_date3;
	$saAssignName[8] =  'Murine Census '.$current_date3;
	$saAssignName[9] =  'Hamster Census '.$current_date3;
	$saAssignName[10] =  'Guinea Pig Census '.$current_date3;
	$saAssignName[11] =  'GuineaPg Census '.$current_date3;
	$saAssignName[12] =  'Lagomorph Census '.$current_date4;
	$saAssignName[13] =  'Rat Census '.$current_date4;
	$saAssignName[14] =  'Murine Census '.$current_date4;
	$saAssignName[15] =  'Hamster Census '.$current_date4;
	$saAssignName[16] =  'Guinea Pig Census '.$current_date4;
	$saAssignName[17] =  'GuineaPg Census '.$current_date4;
	
	$saCalculation = 0;
	$saAssigned = implode("','",$saAssignName);
	echo "<br><br>===>".$queryCustomSaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species saCalculation Query == '.$queryCustomSaAssigned);
	$querySAResult = $db->query($queryCustomSaAssigned);
	if ($querySAResult->num_rows > 0) 
    {
		 $resultSA 			= $db->fetchByAssoc($querySAResult);
		  $scName	= $resultSA['name'];
		  $totalSA_Allocated	= $resultSA['totalAllocated'];
		 $totalSA_OnStudy	= $resultSA['totalOnStudy'];
		 $totalSA_Value		= $resultSA['totalValue'];
		 $saCalculation = (($totalSA_Allocated+$totalSA_OnStudy)/$totalSA_Value)*100;
		 echo "<br>===>".'species saCalculation == '.$scName.'==='.$saCalculation.'==='.$totalSA_Allocated.'===='.$totalSA_OnStudy.'===='.$totalSA_Stock ; 	 
	}
	
	echo "<br><br>===>".$querySaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$querySAIDResult = $db->query($querySaAssignedID);
	if ($querySAIDResult->num_rows > 0) 
    {
		 while($resultSAID 			= $db->fetchByAssoc($querySAIDResult)){
			 
			$beanSAID		= $resultSAID['id'];
			$SC_SABean	= BeanFactory::getBean('SC_Species_Census', $beanSAID);
			$SC_SABean->sa_percent_assigned_2_c	= $saCalculation;
			//$GLOBALS['log']->fatal('===Newspecies '.$SC_SABean->name.' saCalculation  ==> '.$saCalculation);	
			$SC_SABean->save();
		}		  
	}
	
	echo "<br>==========================================================================================";
	echo "<br>==========================================================================================";
	
	
	$current_date	= "2022-02-28"; //2022-01-17
	$current_date2	= "02/28/2022"; //date("m/d/Y");
	$current_date3	= "02-28-2022"; //date("m-d-Y");
	$current_date4	= "2022-02-28"; //date("Y-m-d");
	$current_year	= date("Y"); 
	
	$laAssignName[0] =  'Bovine Census '.$current_date2;
	$laAssignName[1] =  'Canine Census '.$current_date2;
	$laAssignName[2] =  'Caprine Census '.$current_date2;
	$laAssignName[3] =  'Ovine Census '.$current_date2;
	$laAssignName[4] =  'Porcine Census '.$current_date2;
	$laAssignName[5] =  'Bovine Census '.$current_date3;
	$laAssignName[6] =  'Canine Census '.$current_date3;
	$laAssignName[7] =  'Caprine Census '.$current_date3;
	$laAssignName[8] =  'Ovine Census '.$current_date3;
	$laAssignName[9] =  'Porcine Census '.$current_date3;
	$laAssignName[10] = 'Bovine Census '.$current_date4;
	$laAssignName[11] = 'Canine Census '.$current_date4;
	$laAssignName[12] = 'Caprine Census '.$current_date4;
	$laAssignName[13] = 'Ovine Census '.$current_date4;
	$laAssignName[14] = 'Porcine Census '.$current_date4;
	
	$laAssigned 		= implode("','",$laAssignName);	
	$laCalculation 		= 0;
	echo "<br><br>===>".$queryCustomLaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species laCalculation Query == '.$queryCustomLaAssigned);
	$queryLAResult = $db->query($queryCustomLaAssigned);
	if ($queryLAResult->num_rows > 0) 
    {
		 $resultLA 			= $db->fetchByAssoc($queryLAResult);
		 $scName	= $resultLA['name'];
		 $totalAllocated	= $resultLA['totalAllocated'];
		 $totalOnStudy		= $resultLA['totalOnStudy'];
		 $totalValue		= $resultLA['totalValue'];
		 
		 $laCalculation = (($totalAllocated+$totalOnStudy)/$totalValue)*100;
		 echo "<br>===>".'species laCalculation  == '.$scName.'==='.$laCalculation.'==='.$totalAllocated.'===='.$totalOnStudy.'===='.$totalStock ;
	}

	echo "<br><br>===>".$queryCustomLaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$queryLAIDResult = $db->query($queryCustomLaAssignedID);
	if ($queryLAIDResult->num_rows > 0) 
    {
		 while($resultLAID 			= $db->fetchByAssoc($queryLAIDResult)){
			$beanID		= $resultLAID['id'];
			$SC_Bean	= BeanFactory::getBean('SC_Species_Census', $beanID);
			$SC_Bean->la_percent_assigned_2_c	= $laCalculation;	
			$SC_Bean->save();
		}		  
	}

	
		
	/**/
	$saAssignName[0] =  'Lagomorph Census '.$current_date2;
	$saAssignName[1] =  'Rat Census '.$current_date2;
	$saAssignName[2] =  'Murine Census '.$current_date2;
	$saAssignName[3] =  'Hamster Census '.$current_date2;
	$saAssignName[4] =  'Guinea Pig Census '.$current_date2;
	$saAssignName[5] =  'GuineaPg Census '.$current_date2;
	$saAssignName[6] =  'Lagomorph Census '.$current_date3;
	$saAssignName[7] =  'Rat Census '.$current_date3;
	$saAssignName[8] =  'Murine Census '.$current_date3;
	$saAssignName[9] =  'Hamster Census '.$current_date3;
	$saAssignName[10] =  'Guinea Pig Census '.$current_date3;
	$saAssignName[11] =  'GuineaPg Census '.$current_date3;
	$saAssignName[12] =  'Lagomorph Census '.$current_date4;
	$saAssignName[13] =  'Rat Census '.$current_date4;
	$saAssignName[14] =  'Murine Census '.$current_date4;
	$saAssignName[15] =  'Hamster Census '.$current_date4;
	$saAssignName[16] =  'Guinea Pig Census '.$current_date4;
	$saAssignName[17] =  'GuineaPg Census '.$current_date4;
	
	$saCalculation = 0;
	$saAssigned = implode("','",$saAssignName);
	echo "<br><br>===>".$queryCustomSaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species saCalculation Query == '.$queryCustomSaAssigned);
	$querySAResult = $db->query($queryCustomSaAssigned);
	if ($querySAResult->num_rows > 0) 
    {
		 $resultSA 			= $db->fetchByAssoc($querySAResult);
		  $scName	= $resultSA['name'];
		  $totalSA_Allocated	= $resultSA['totalAllocated'];
		 $totalSA_OnStudy	= $resultSA['totalOnStudy'];
		 $totalSA_Value		= $resultSA['totalValue'];
		 $saCalculation = (($totalSA_Allocated+$totalSA_OnStudy)/$totalSA_Value)*100;
		 echo "<br>===>".'species saCalculation == '.$scName.'==='.$saCalculation.'==='.$totalSA_Allocated.'===='.$totalSA_OnStudy.'===='.$totalSA_Stock ; 	 
	}
	
	echo "<br><br>===>".$querySaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$querySAIDResult = $db->query($querySaAssignedID);
	if ($querySAIDResult->num_rows > 0) 
    {
		 while($resultSAID 			= $db->fetchByAssoc($querySAIDResult)){
			 
			$beanSAID		= $resultSAID['id'];
			$SC_SABean	= BeanFactory::getBean('SC_Species_Census', $beanSAID);
			$SC_SABean->sa_percent_assigned_2_c	= $saCalculation;
			//$GLOBALS['log']->fatal('===Newspecies '.$SC_SABean->name.' saCalculation  ==> '.$saCalculation);	
			$SC_SABean->save();
		}		  
	}
	
	echo "<br>==========================================================================================";
	echo "<br>==========================================================================================";
	
	
	$current_date	= "2022-02-21"; //2022-01-17
	$current_date2	= "02/21/2022"; //date("m/d/Y");
	$current_date3	= "02-21-2022"; //date("m-d-Y");
	$current_date4	= "2022-02-21"; //date("Y-m-d");
	$current_year	= date("Y"); 
	
	$laAssignName[0] =  'Bovine Census '.$current_date2;
	$laAssignName[1] =  'Canine Census '.$current_date2;
	$laAssignName[2] =  'Caprine Census '.$current_date2;
	$laAssignName[3] =  'Ovine Census '.$current_date2;
	$laAssignName[4] =  'Porcine Census '.$current_date2;
	$laAssignName[5] =  'Bovine Census '.$current_date3;
	$laAssignName[6] =  'Canine Census '.$current_date3;
	$laAssignName[7] =  'Caprine Census '.$current_date3;
	$laAssignName[8] =  'Ovine Census '.$current_date3;
	$laAssignName[9] =  'Porcine Census '.$current_date3;
	$laAssignName[10] = 'Bovine Census '.$current_date4;
	$laAssignName[11] = 'Canine Census '.$current_date4;
	$laAssignName[12] = 'Caprine Census '.$current_date4;
	$laAssignName[13] = 'Ovine Census '.$current_date4;
	$laAssignName[14] = 'Porcine Census '.$current_date4;
	
	$laAssigned 		= implode("','",$laAssignName);	
	$laCalculation 		= 0;
	echo "<br><br>===>".$queryCustomLaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species laCalculation Query == '.$queryCustomLaAssigned);
	$queryLAResult = $db->query($queryCustomLaAssigned);
	if ($queryLAResult->num_rows > 0) 
    {
		 $resultLA 			= $db->fetchByAssoc($queryLAResult);
		 $scName	= $resultLA['name'];
		 $totalAllocated	= $resultLA['totalAllocated'];
		 $totalOnStudy		= $resultLA['totalOnStudy'];
		 $totalValue		= $resultLA['totalValue'];
		 
		 $laCalculation = (($totalAllocated+$totalOnStudy)/$totalValue)*100;
		 echo "<br>===>".'species laCalculation  == '.$scName.'==='.$laCalculation.'==='.$totalAllocated.'===='.$totalOnStudy.'===='.$totalStock ;
	}

	echo "<br><br>===>".$queryCustomLaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$queryLAIDResult = $db->query($queryCustomLaAssignedID);
	if ($queryLAIDResult->num_rows > 0) 
    {
		 while($resultLAID 			= $db->fetchByAssoc($queryLAIDResult)){
			$beanID		= $resultLAID['id'];
			$SC_Bean	= BeanFactory::getBean('SC_Species_Census', $beanID);
			$SC_Bean->la_percent_assigned_2_c	= $laCalculation;	
			$SC_Bean->save();
		}		  
	}

	
		
	/**/
	$saAssignName[0] =  'Lagomorph Census '.$current_date2;
	$saAssignName[1] =  'Rat Census '.$current_date2;
	$saAssignName[2] =  'Murine Census '.$current_date2;
	$saAssignName[3] =  'Hamster Census '.$current_date2;
	$saAssignName[4] =  'Guinea Pig Census '.$current_date2;
	$saAssignName[5] =  'GuineaPg Census '.$current_date2;
	$saAssignName[6] =  'Lagomorph Census '.$current_date3;
	$saAssignName[7] =  'Rat Census '.$current_date3;
	$saAssignName[8] =  'Murine Census '.$current_date3;
	$saAssignName[9] =  'Hamster Census '.$current_date3;
	$saAssignName[10] =  'Guinea Pig Census '.$current_date3;
	$saAssignName[11] =  'GuineaPg Census '.$current_date3;
	$saAssignName[12] =  'Lagomorph Census '.$current_date4;
	$saAssignName[13] =  'Rat Census '.$current_date4;
	$saAssignName[14] =  'Murine Census '.$current_date4;
	$saAssignName[15] =  'Hamster Census '.$current_date4;
	$saAssignName[16] =  'Guinea Pig Census '.$current_date4;
	$saAssignName[17] =  'GuineaPg Census '.$current_date4;
	
	$saCalculation = 0;
	$saAssigned = implode("','",$saAssignName);
	echo "<br><br>===>".$queryCustomSaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species saCalculation Query == '.$queryCustomSaAssigned);
	$querySAResult = $db->query($queryCustomSaAssigned);
	if ($querySAResult->num_rows > 0) 
    {
		 $resultSA 			= $db->fetchByAssoc($querySAResult);
		  $scName	= $resultSA['name'];
		  $totalSA_Allocated	= $resultSA['totalAllocated'];
		 $totalSA_OnStudy	= $resultSA['totalOnStudy'];
		 $totalSA_Value		= $resultSA['totalValue'];
		 $saCalculation = (($totalSA_Allocated+$totalSA_OnStudy)/$totalSA_Value)*100;
		 echo "<br>===>".'species saCalculation == '.$scName.'==='.$saCalculation.'==='.$totalSA_Allocated.'===='.$totalSA_OnStudy.'===='.$totalSA_Stock ; 	 
	}
	
	echo "<br><br>===>".$querySaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$querySAIDResult = $db->query($querySaAssignedID);
	if ($querySAIDResult->num_rows > 0) 
    {
		 while($resultSAID 			= $db->fetchByAssoc($querySAIDResult)){
			 
			$beanSAID		= $resultSAID['id'];
			$SC_SABean	= BeanFactory::getBean('SC_Species_Census', $beanSAID);
			$SC_SABean->sa_percent_assigned_2_c	= $saCalculation;
			//$GLOBALS['log']->fatal('===Newspecies '.$SC_SABean->name.' saCalculation  ==> '.$saCalculation);	
			$SC_SABean->save();
		}		  
	}
	
	echo "<br>==========================================================================================";
	echo "<br>==========================================================================================";
	
	
	$current_date	= "2022-02-14"; //2022-01-17
	$current_date2	= "02/14/2022"; //date("m/d/Y");
	$current_date3	= "02-14-2022"; //date("m-d-Y");
	$current_date4	= "2022-02-14"; //date("Y-m-d");
	$current_year	= date("Y"); 
	
	$laAssignName[0] =  'Bovine Census '.$current_date2;
	$laAssignName[1] =  'Canine Census '.$current_date2;
	$laAssignName[2] =  'Caprine Census '.$current_date2;
	$laAssignName[3] =  'Ovine Census '.$current_date2;
	$laAssignName[4] =  'Porcine Census '.$current_date2;
	$laAssignName[5] =  'Bovine Census '.$current_date3;
	$laAssignName[6] =  'Canine Census '.$current_date3;
	$laAssignName[7] =  'Caprine Census '.$current_date3;
	$laAssignName[8] =  'Ovine Census '.$current_date3;
	$laAssignName[9] =  'Porcine Census '.$current_date3;
	$laAssignName[10] = 'Bovine Census '.$current_date4;
	$laAssignName[11] = 'Canine Census '.$current_date4;
	$laAssignName[12] = 'Caprine Census '.$current_date4;
	$laAssignName[13] = 'Ovine Census '.$current_date4;
	$laAssignName[14] = 'Porcine Census '.$current_date4;
	
	$laAssigned 		= implode("','",$laAssignName);	
	$laCalculation 		= 0;
	echo "<br><br>===>".$queryCustomLaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species laCalculation Query == '.$queryCustomLaAssigned);
	$queryLAResult = $db->query($queryCustomLaAssigned);
	if ($queryLAResult->num_rows > 0) 
    {
		 $resultLA 			= $db->fetchByAssoc($queryLAResult);
		 $scName	= $resultLA['name'];
		 $totalAllocated	= $resultLA['totalAllocated'];
		 $totalOnStudy		= $resultLA['totalOnStudy'];
		 $totalValue		= $resultLA['totalValue'];
		 
		 $laCalculation = (($totalAllocated+$totalOnStudy)/$totalValue)*100;
		 echo "<br>===>".'species laCalculation  == '.$scName.'==='.$laCalculation.'==='.$totalAllocated.'===='.$totalOnStudy.'===='.$totalStock ;
	}

	echo "<br><br>===>".$queryCustomLaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$queryLAIDResult = $db->query($queryCustomLaAssignedID);
	if ($queryLAIDResult->num_rows > 0) 
    {
		 while($resultLAID 			= $db->fetchByAssoc($queryLAIDResult)){
			$beanID		= $resultLAID['id'];
			$SC_Bean	= BeanFactory::getBean('SC_Species_Census', $beanID);
			$SC_Bean->la_percent_assigned_2_c	= $laCalculation;	
			$SC_Bean->save();
		}		  
	}

	
		
	/**/
	$saAssignName[0] =  'Lagomorph Census '.$current_date2;
	$saAssignName[1] =  'Rat Census '.$current_date2;
	$saAssignName[2] =  'Murine Census '.$current_date2;
	$saAssignName[3] =  'Hamster Census '.$current_date2;
	$saAssignName[4] =  'Guinea Pig Census '.$current_date2;
	$saAssignName[5] =  'GuineaPg Census '.$current_date2;
	$saAssignName[6] =  'Lagomorph Census '.$current_date3;
	$saAssignName[7] =  'Rat Census '.$current_date3;
	$saAssignName[8] =  'Murine Census '.$current_date3;
	$saAssignName[9] =  'Hamster Census '.$current_date3;
	$saAssignName[10] =  'Guinea Pig Census '.$current_date3;
	$saAssignName[11] =  'GuineaPg Census '.$current_date3;
	$saAssignName[12] =  'Lagomorph Census '.$current_date4;
	$saAssignName[13] =  'Rat Census '.$current_date4;
	$saAssignName[14] =  'Murine Census '.$current_date4;
	$saAssignName[15] =  'Hamster Census '.$current_date4;
	$saAssignName[16] =  'Guinea Pig Census '.$current_date4;
	$saAssignName[17] =  'GuineaPg Census '.$current_date4;
	
	$saCalculation = 0;
	$saAssigned = implode("','",$saAssignName);
	echo "<br><br>===>".$queryCustomSaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species saCalculation Query == '.$queryCustomSaAssigned);
	$querySAResult = $db->query($queryCustomSaAssigned);
	if ($querySAResult->num_rows > 0) 
    {
		 $resultSA 			= $db->fetchByAssoc($querySAResult);
		  $scName	= $resultSA['name'];
		  $totalSA_Allocated	= $resultSA['totalAllocated'];
		 $totalSA_OnStudy	= $resultSA['totalOnStudy'];
		 $totalSA_Value		= $resultSA['totalValue'];
		 $saCalculation = (($totalSA_Allocated+$totalSA_OnStudy)/$totalSA_Value)*100;
		 echo "<br>===>".'species saCalculation == '.$scName.'==='.$saCalculation.'==='.$totalSA_Allocated.'===='.$totalSA_OnStudy.'===='.$totalSA_Stock ; 	 
	}
	
	echo "<br><br>===>".$querySaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$querySAIDResult = $db->query($querySaAssignedID);
	if ($querySAIDResult->num_rows > 0) 
    {
		 while($resultSAID 			= $db->fetchByAssoc($querySAIDResult)){
			 
			$beanSAID		= $resultSAID['id'];
			$SC_SABean	= BeanFactory::getBean('SC_Species_Census', $beanSAID);
			$SC_SABean->sa_percent_assigned_2_c	= $saCalculation;
			//$GLOBALS['log']->fatal('===Newspecies '.$SC_SABean->name.' saCalculation  ==> '.$saCalculation);	
			$SC_SABean->save();
		}		  
	}
	
	
	echo "<br>==========================================================================================";
	echo "<br>==========================================================================================";
	
	
	$current_date	= "2022-02-07"; //2022-01-17
	$current_date2	= "02/07/2022"; //date("m/d/Y");
	$current_date3	= "02-07-2022"; //date("m-d-Y");
	$current_date4	= "2022-02-07"; //date("Y-m-d");
	$current_year	= date("Y"); 
	
	$laAssignName[0] =  'Bovine Census '.$current_date2;
	$laAssignName[1] =  'Canine Census '.$current_date2;
	$laAssignName[2] =  'Caprine Census '.$current_date2;
	$laAssignName[3] =  'Ovine Census '.$current_date2;
	$laAssignName[4] =  'Porcine Census '.$current_date2;
	$laAssignName[5] =  'Bovine Census '.$current_date3;
	$laAssignName[6] =  'Canine Census '.$current_date3;
	$laAssignName[7] =  'Caprine Census '.$current_date3;
	$laAssignName[8] =  'Ovine Census '.$current_date3;
	$laAssignName[9] =  'Porcine Census '.$current_date3;
	$laAssignName[10] = 'Bovine Census '.$current_date4;
	$laAssignName[11] = 'Canine Census '.$current_date4;
	$laAssignName[12] = 'Caprine Census '.$current_date4;
	$laAssignName[13] = 'Ovine Census '.$current_date4;
	$laAssignName[14] = 'Porcine Census '.$current_date4;
	
	$laAssigned 		= implode("','",$laAssignName);	
	$laCalculation 		= 0;
	echo "<br><br>===>".$queryCustomLaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species laCalculation Query == '.$queryCustomLaAssigned);
	$queryLAResult = $db->query($queryCustomLaAssigned);
	if ($queryLAResult->num_rows > 0) 
    {
		 $resultLA 			= $db->fetchByAssoc($queryLAResult);
		 $scName	= $resultLA['name'];
		 $totalAllocated	= $resultLA['totalAllocated'];
		 $totalOnStudy		= $resultLA['totalOnStudy'];
		 $totalValue		= $resultLA['totalValue'];
		 
		 $laCalculation = (($totalAllocated+$totalOnStudy)/$totalValue)*100;
		 echo "<br>===>".'species laCalculation  == '.$scName.'==='.$laCalculation.'==='.$totalAllocated.'===='.$totalOnStudy.'===='.$totalStock ;
	}

	echo "<br><br>===>".$queryCustomLaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$queryLAIDResult = $db->query($queryCustomLaAssignedID);
	if ($queryLAIDResult->num_rows > 0) 
    {
		 while($resultLAID 			= $db->fetchByAssoc($queryLAIDResult)){
			$beanID		= $resultLAID['id'];
			$SC_Bean	= BeanFactory::getBean('SC_Species_Census', $beanID);
			$SC_Bean->la_percent_assigned_2_c	= $laCalculation;	
			$SC_Bean->save();
		}		  
	}

	
		
	/**/
	$saAssignName[0] =  'Lagomorph Census '.$current_date2;
	$saAssignName[1] =  'Rat Census '.$current_date2;
	$saAssignName[2] =  'Murine Census '.$current_date2;
	$saAssignName[3] =  'Hamster Census '.$current_date2;
	$saAssignName[4] =  'Guinea Pig Census '.$current_date2;
	$saAssignName[5] =  'GuineaPg Census '.$current_date2;
	$saAssignName[6] =  'Lagomorph Census '.$current_date3;
	$saAssignName[7] =  'Rat Census '.$current_date3;
	$saAssignName[8] =  'Murine Census '.$current_date3;
	$saAssignName[9] =  'Hamster Census '.$current_date3;
	$saAssignName[10] =  'Guinea Pig Census '.$current_date3;
	$saAssignName[11] =  'GuineaPg Census '.$current_date3;
	$saAssignName[12] =  'Lagomorph Census '.$current_date4;
	$saAssignName[13] =  'Rat Census '.$current_date4;
	$saAssignName[14] =  'Murine Census '.$current_date4;
	$saAssignName[15] =  'Hamster Census '.$current_date4;
	$saAssignName[16] =  'Guinea Pig Census '.$current_date4;
	$saAssignName[17] =  'GuineaPg Census '.$current_date4;
	
	$saCalculation = 0;
	$saAssigned = implode("','",$saAssignName);
	echo "<br><br>===>".$queryCustomSaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species saCalculation Query == '.$queryCustomSaAssigned);
	$querySAResult = $db->query($queryCustomSaAssigned);
	if ($querySAResult->num_rows > 0) 
    {
		 $resultSA 			= $db->fetchByAssoc($querySAResult);
		  $scName	= $resultSA['name'];
		  $totalSA_Allocated	= $resultSA['totalAllocated'];
		 $totalSA_OnStudy	= $resultSA['totalOnStudy'];
		 $totalSA_Value		= $resultSA['totalValue'];
		 $saCalculation = (($totalSA_Allocated+$totalSA_OnStudy)/$totalSA_Value)*100;
		 echo "<br>===>".'species saCalculation == '.$scName.'==='.$saCalculation.'==='.$totalSA_Allocated.'===='.$totalSA_OnStudy.'===='.$totalSA_Stock ; 	 
	}
	
	echo "<br><br>===>".$querySaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$querySAIDResult = $db->query($querySaAssignedID);
	if ($querySAIDResult->num_rows > 0) 
    {
		 while($resultSAID 			= $db->fetchByAssoc($querySAIDResult)){
			 
			$beanSAID		= $resultSAID['id'];
			$SC_SABean	= BeanFactory::getBean('SC_Species_Census', $beanSAID);
			$SC_SABean->sa_percent_assigned_2_c	= $saCalculation;
			//$GLOBALS['log']->fatal('===Newspecies '.$SC_SABean->name.' saCalculation  ==> '.$saCalculation);	
			$SC_SABean->save();
		}		  
	}
	
	echo "<br>==========================================================================================";
	echo "<br>==========================================================================================";
	
	
	$current_date	= "2022-01-31"; //2022-01-17
	$current_date2	= "01/31/2022"; //date("m/d/Y");
	$current_date3	= "01-31-2022"; //date("m-d-Y");
	$current_date4	= "2022-01-31"; //date("Y-m-d");
	$current_year	= date("Y"); 
	
	$laAssignName[0] =  'Bovine Census '.$current_date2;
	$laAssignName[1] =  'Canine Census '.$current_date2;
	$laAssignName[2] =  'Caprine Census '.$current_date2;
	$laAssignName[3] =  'Ovine Census '.$current_date2;
	$laAssignName[4] =  'Porcine Census '.$current_date2;
	$laAssignName[5] =  'Bovine Census '.$current_date3;
	$laAssignName[6] =  'Canine Census '.$current_date3;
	$laAssignName[7] =  'Caprine Census '.$current_date3;
	$laAssignName[8] =  'Ovine Census '.$current_date3;
	$laAssignName[9] =  'Porcine Census '.$current_date3;
	$laAssignName[10] = 'Bovine Census '.$current_date4;
	$laAssignName[11] = 'Canine Census '.$current_date4;
	$laAssignName[12] = 'Caprine Census '.$current_date4;
	$laAssignName[13] = 'Ovine Census '.$current_date4;
	$laAssignName[14] = 'Porcine Census '.$current_date4;
	
	$laAssigned 		= implode("','",$laAssignName);	
	$laCalculation 		= 0;
	echo "<br><br>===>".$queryCustomLaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species laCalculation Query == '.$queryCustomLaAssigned);
	$queryLAResult = $db->query($queryCustomLaAssigned);
	if ($queryLAResult->num_rows > 0) 
    {
		 $resultLA 			= $db->fetchByAssoc($queryLAResult);
		 $scName	= $resultLA['name'];
		 $totalAllocated	= $resultLA['totalAllocated'];
		 $totalOnStudy		= $resultLA['totalOnStudy'];
		 $totalValue		= $resultLA['totalValue'];
		 
		 $laCalculation = (($totalAllocated+$totalOnStudy)/$totalValue)*100;
		 echo "<br>===>".'species laCalculation  == '.$scName.'==='.$laCalculation.'==='.$totalAllocated.'===='.$totalOnStudy.'===='.$totalStock ;
	}

	echo "<br><br>===>".$queryCustomLaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$queryLAIDResult = $db->query($queryCustomLaAssignedID);
	if ($queryLAIDResult->num_rows > 0) 
    {
		 while($resultLAID 			= $db->fetchByAssoc($queryLAIDResult)){
			$beanID		= $resultLAID['id'];
			$SC_Bean	= BeanFactory::getBean('SC_Species_Census', $beanID);
			$SC_Bean->la_percent_assigned_2_c	= $laCalculation;	
			$SC_Bean->save();
		}		  
	}

	
		
	/**/
	$saAssignName[0] =  'Lagomorph Census '.$current_date2;
	$saAssignName[1] =  'Rat Census '.$current_date2;
	$saAssignName[2] =  'Murine Census '.$current_date2;
	$saAssignName[3] =  'Hamster Census '.$current_date2;
	$saAssignName[4] =  'Guinea Pig Census '.$current_date2;
	$saAssignName[5] =  'GuineaPg Census '.$current_date2;
	$saAssignName[6] =  'Lagomorph Census '.$current_date3;
	$saAssignName[7] =  'Rat Census '.$current_date3;
	$saAssignName[8] =  'Murine Census '.$current_date3;
	$saAssignName[9] =  'Hamster Census '.$current_date3;
	$saAssignName[10] =  'Guinea Pig Census '.$current_date3;
	$saAssignName[11] =  'GuineaPg Census '.$current_date3;
	$saAssignName[12] =  'Lagomorph Census '.$current_date4;
	$saAssignName[13] =  'Rat Census '.$current_date4;
	$saAssignName[14] =  'Murine Census '.$current_date4;
	$saAssignName[15] =  'Hamster Census '.$current_date4;
	$saAssignName[16] =  'Guinea Pig Census '.$current_date4;
	$saAssignName[17] =  'GuineaPg Census '.$current_date4;
	
	$saCalculation = 0;
	$saAssigned = implode("','",$saAssignName);
	echo "<br><br>===>".$queryCustomSaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species saCalculation Query == '.$queryCustomSaAssigned);
	$querySAResult = $db->query($queryCustomSaAssigned);
	if ($querySAResult->num_rows > 0) 
    {
		 $resultSA 			= $db->fetchByAssoc($querySAResult);
		  $scName	= $resultSA['name'];
		  $totalSA_Allocated	= $resultSA['totalAllocated'];
		 $totalSA_OnStudy	= $resultSA['totalOnStudy'];
		 $totalSA_Value		= $resultSA['totalValue'];
		 $saCalculation = (($totalSA_Allocated+$totalSA_OnStudy)/$totalSA_Value)*100;
		 echo "<br>===>".'species saCalculation == '.$scName.'==='.$saCalculation.'==='.$totalSA_Allocated.'===='.$totalSA_OnStudy.'===='.$totalSA_Stock ; 	 
	}
	
	echo "<br><br>===>".$querySaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$querySAIDResult = $db->query($querySaAssignedID);
	if ($querySAIDResult->num_rows > 0) 
    {
		 while($resultSAID 			= $db->fetchByAssoc($querySAIDResult)){
			 
			$beanSAID		= $resultSAID['id'];
			$SC_SABean	= BeanFactory::getBean('SC_Species_Census', $beanSAID);
			$SC_SABean->sa_percent_assigned_2_c	= $saCalculation;
			//$GLOBALS['log']->fatal('===Newspecies '.$SC_SABean->name.' saCalculation  ==> '.$saCalculation);	
			$SC_SABean->save();
		}		  
	}
	
	echo "<br>==========================================================================================";
	echo "<br>==========================================================================================";
	
	
	$current_date	= "2022-01-24"; //2022-01-17
	$current_date2	= "01/24/2022"; //date("m/d/Y");
	$current_date3	= "01-24-2022"; //date("m-d-Y");
	$current_date4	= "2022-01-24"; //date("Y-m-d");
	$current_year	= date("Y"); 
	
	$laAssignName[0] =  'Bovine Census '.$current_date2;
	$laAssignName[1] =  'Canine Census '.$current_date2;
	$laAssignName[2] =  'Caprine Census '.$current_date2;
	$laAssignName[3] =  'Ovine Census '.$current_date2;
	$laAssignName[4] =  'Porcine Census '.$current_date2;
	$laAssignName[5] =  'Bovine Census '.$current_date3;
	$laAssignName[6] =  'Canine Census '.$current_date3;
	$laAssignName[7] =  'Caprine Census '.$current_date3;
	$laAssignName[8] =  'Ovine Census '.$current_date3;
	$laAssignName[9] =  'Porcine Census '.$current_date3;
	$laAssignName[10] = 'Bovine Census '.$current_date4;
	$laAssignName[11] = 'Canine Census '.$current_date4;
	$laAssignName[12] = 'Caprine Census '.$current_date4;
	$laAssignName[13] = 'Ovine Census '.$current_date4;
	$laAssignName[14] = 'Porcine Census '.$current_date4;
	
	$laAssigned 		= implode("','",$laAssignName);	
	$laCalculation 		= 0;
	echo "<br><br>===>".$queryCustomLaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species laCalculation Query == '.$queryCustomLaAssigned);
	$queryLAResult = $db->query($queryCustomLaAssigned);
	if ($queryLAResult->num_rows > 0) 
    {
		 $resultLA 			= $db->fetchByAssoc($queryLAResult);
		 $scName	= $resultLA['name'];
		 $totalAllocated	= $resultLA['totalAllocated'];
		 $totalOnStudy		= $resultLA['totalOnStudy'];
		 $totalValue		= $resultLA['totalValue'];
		 
		 $laCalculation = (($totalAllocated+$totalOnStudy)/$totalValue)*100;
		 echo "<br>===>".'species laCalculation  == '.$scName.'==='.$laCalculation.'==='.$totalAllocated.'===='.$totalOnStudy.'===='.$totalStock ;
	}

	echo "<br><br>===>".$queryCustomLaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$queryLAIDResult = $db->query($queryCustomLaAssignedID);
	if ($queryLAIDResult->num_rows > 0) 
    {
		 while($resultLAID 			= $db->fetchByAssoc($queryLAIDResult)){
			$beanID		= $resultLAID['id'];
			$SC_Bean	= BeanFactory::getBean('SC_Species_Census', $beanID);
			$SC_Bean->la_percent_assigned_2_c	= $laCalculation;	
			$SC_Bean->save();
		}		  
	}

	
		
	/**/
	$saAssignName[0] =  'Lagomorph Census '.$current_date2;
	$saAssignName[1] =  'Rat Census '.$current_date2;
	$saAssignName[2] =  'Murine Census '.$current_date2;
	$saAssignName[3] =  'Hamster Census '.$current_date2;
	$saAssignName[4] =  'Guinea Pig Census '.$current_date2;
	$saAssignName[5] =  'GuineaPg Census '.$current_date2;
	$saAssignName[6] =  'Lagomorph Census '.$current_date3;
	$saAssignName[7] =  'Rat Census '.$current_date3;
	$saAssignName[8] =  'Murine Census '.$current_date3;
	$saAssignName[9] =  'Hamster Census '.$current_date3;
	$saAssignName[10] =  'Guinea Pig Census '.$current_date3;
	$saAssignName[11] =  'GuineaPg Census '.$current_date3;
	$saAssignName[12] =  'Lagomorph Census '.$current_date4;
	$saAssignName[13] =  'Rat Census '.$current_date4;
	$saAssignName[14] =  'Murine Census '.$current_date4;
	$saAssignName[15] =  'Hamster Census '.$current_date4;
	$saAssignName[16] =  'Guinea Pig Census '.$current_date4;
	$saAssignName[17] =  'GuineaPg Census '.$current_date4;
	
	$saCalculation = 0;
	$saAssigned = implode("','",$saAssignName);
	echo "<br><br>===>".$queryCustomSaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species saCalculation Query == '.$queryCustomSaAssigned);
	$querySAResult = $db->query($queryCustomSaAssigned);
	if ($querySAResult->num_rows > 0) 
    {
		 $resultSA 			= $db->fetchByAssoc($querySAResult);
		  $scName	= $resultSA['name'];
		  $totalSA_Allocated	= $resultSA['totalAllocated'];
		 $totalSA_OnStudy	= $resultSA['totalOnStudy'];
		 $totalSA_Value		= $resultSA['totalValue'];
		 $saCalculation = (($totalSA_Allocated+$totalSA_OnStudy)/$totalSA_Value)*100;
		 echo "<br>===>".'species saCalculation == '.$scName.'==='.$saCalculation.'==='.$totalSA_Allocated.'===='.$totalSA_OnStudy.'===='.$totalSA_Stock ; 	 
	}
	
	echo "<br><br>===>".$querySaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$querySAIDResult = $db->query($querySaAssignedID);
	if ($querySAIDResult->num_rows > 0) 
    {
		 while($resultSAID 			= $db->fetchByAssoc($querySAIDResult)){
			 
			$beanSAID		= $resultSAID['id'];
			$SC_SABean	= BeanFactory::getBean('SC_Species_Census', $beanSAID);
			$SC_SABean->sa_percent_assigned_2_c	= $saCalculation;
			//$GLOBALS['log']->fatal('===Newspecies '.$SC_SABean->name.' saCalculation  ==> '.$saCalculation);	
			$SC_SABean->save();
		}		  
	}
	
	
	echo "<br>==========================================================================================";
	echo "<br>==========================================================================================";
	
	
	$current_date	= "2022-01-17"; //2022-01-17
	$current_date2	= "01/17/2022"; //date("m/d/Y");
	$current_date3	= "01-17-2022"; //date("m-d-Y");
	$current_date4	= "2022-01-17"; //date("Y-m-d");
	$current_year	= date("Y"); 
	
	$laAssignName[0] =  'Bovine Census '.$current_date2;
	$laAssignName[1] =  'Canine Census '.$current_date2;
	$laAssignName[2] =  'Caprine Census '.$current_date2;
	$laAssignName[3] =  'Ovine Census '.$current_date2;
	$laAssignName[4] =  'Porcine Census '.$current_date2;
	$laAssignName[5] =  'Bovine Census '.$current_date3;
	$laAssignName[6] =  'Canine Census '.$current_date3;
	$laAssignName[7] =  'Caprine Census '.$current_date3;
	$laAssignName[8] =  'Ovine Census '.$current_date3;
	$laAssignName[9] =  'Porcine Census '.$current_date3;
	$laAssignName[10] = 'Bovine Census '.$current_date4;
	$laAssignName[11] = 'Canine Census '.$current_date4;
	$laAssignName[12] = 'Caprine Census '.$current_date4;
	$laAssignName[13] = 'Ovine Census '.$current_date4;
	$laAssignName[14] = 'Porcine Census '.$current_date4;
	
	$laAssigned 		= implode("','",$laAssignName);	
	$laCalculation 		= 0;
	echo "<br><br>===>".$queryCustomLaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species laCalculation Query == '.$queryCustomLaAssigned);
	$queryLAResult = $db->query($queryCustomLaAssigned);
	if ($queryLAResult->num_rows > 0) 
    {
		 $resultLA 			= $db->fetchByAssoc($queryLAResult);
		 $scName	= $resultLA['name'];
		 $totalAllocated	= $resultLA['totalAllocated'];
		 $totalOnStudy		= $resultLA['totalOnStudy'];
		 $totalValue		= $resultLA['totalValue'];
		 
		 $laCalculation = (($totalAllocated+$totalOnStudy)/$totalValue)*100;
		 echo "<br>===>".'species laCalculation  == '.$scName.'==='.$laCalculation.'==='.$totalAllocated.'===='.$totalOnStudy.'===='.$totalStock ;
	}

	echo "<br><br>===>".$queryCustomLaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$queryLAIDResult = $db->query($queryCustomLaAssignedID);
	if ($queryLAIDResult->num_rows > 0) 
    {
		 while($resultLAID 			= $db->fetchByAssoc($queryLAIDResult)){
			$beanID		= $resultLAID['id'];
			$SC_Bean	= BeanFactory::getBean('SC_Species_Census', $beanID);
			$SC_Bean->la_percent_assigned_2_c	= $laCalculation;	
			$SC_Bean->save();
		}		  
	}

	
		
	/**/
	$saAssignName[0] =  'Lagomorph Census '.$current_date2;
	$saAssignName[1] =  'Rat Census '.$current_date2;
	$saAssignName[2] =  'Murine Census '.$current_date2;
	$saAssignName[3] =  'Hamster Census '.$current_date2;
	$saAssignName[4] =  'Guinea Pig Census '.$current_date2;
	$saAssignName[5] =  'GuineaPg Census '.$current_date2;
	$saAssignName[6] =  'Lagomorph Census '.$current_date3;
	$saAssignName[7] =  'Rat Census '.$current_date3;
	$saAssignName[8] =  'Murine Census '.$current_date3;
	$saAssignName[9] =  'Hamster Census '.$current_date3;
	$saAssignName[10] =  'Guinea Pig Census '.$current_date3;
	$saAssignName[11] =  'GuineaPg Census '.$current_date3;
	$saAssignName[12] =  'Lagomorph Census '.$current_date4;
	$saAssignName[13] =  'Rat Census '.$current_date4;
	$saAssignName[14] =  'Murine Census '.$current_date4;
	$saAssignName[15] =  'Hamster Census '.$current_date4;
	$saAssignName[16] =  'Guinea Pig Census '.$current_date4;
	$saAssignName[17] =  'GuineaPg Census '.$current_date4;
	
	$saCalculation = 0;
	$saAssigned = implode("','",$saAssignName);
	echo "<br><br>===>".$queryCustomSaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species saCalculation Query == '.$queryCustomSaAssigned);
	$querySAResult = $db->query($queryCustomSaAssigned);
	if ($querySAResult->num_rows > 0) 
    {
		 $resultSA 			= $db->fetchByAssoc($querySAResult);
		  $scName	= $resultSA['name'];
		  $totalSA_Allocated	= $resultSA['totalAllocated'];
		 $totalSA_OnStudy	= $resultSA['totalOnStudy'];
		 $totalSA_Value		= $resultSA['totalValue'];
		 $saCalculation = (($totalSA_Allocated+$totalSA_OnStudy)/$totalSA_Value)*100;
		 echo "<br>===>".'species saCalculation == '.$scName.'==='.$saCalculation.'==='.$totalSA_Allocated.'===='.$totalSA_OnStudy.'===='.$totalSA_Stock ; 	 
	}
	
	echo "<br><br>===>".$querySaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$querySAIDResult = $db->query($querySaAssignedID);
	if ($querySAIDResult->num_rows > 0) 
    {
		 while($resultSAID 			= $db->fetchByAssoc($querySAIDResult)){
			 
			$beanSAID		= $resultSAID['id'];
			$SC_SABean	= BeanFactory::getBean('SC_Species_Census', $beanSAID);
			$SC_SABean->sa_percent_assigned_2_c	= $saCalculation;
			//$GLOBALS['log']->fatal('===Newspecies '.$SC_SABean->name.' saCalculation  ==> '.$saCalculation);	
			$SC_SABean->save();
		}		  
	}
	