<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/AN01_Activity_Notes/Ext/Vardefs/meetings_an01_activity_notes_1_AN01_Activity_Notes.php

// created: 2017-05-11 22:18:50
$dictionary["AN01_Activity_Notes"]["fields"]["meetings_an01_activity_notes_1"] = array (
  'name' => 'meetings_an01_activity_notes_1',
  'type' => 'link',
  'relationship' => 'meetings_an01_activity_notes_1',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'side' => 'right',
  'vname' => 'LBL_MEETINGS_AN01_ACTIVITY_NOTES_1_FROM_AN01_ACTIVITY_NOTES_TITLE',
  'id_name' => 'meetings_an01_activity_notes_1meetings_ida',
  'link-type' => 'one',
);
$dictionary["AN01_Activity_Notes"]["fields"]["meetings_an01_activity_notes_1_name"] = array (
  'name' => 'meetings_an01_activity_notes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_MEETINGS_AN01_ACTIVITY_NOTES_1_FROM_MEETINGS_TITLE',
  'save' => true,
  'id_name' => 'meetings_an01_activity_notes_1meetings_ida',
  'link' => 'meetings_an01_activity_notes_1',
  'table' => 'meetings',
  'module' => 'Meetings',
  'rname' => 'name',
);
$dictionary["AN01_Activity_Notes"]["fields"]["meetings_an01_activity_notes_1meetings_ida"] = array (
  'name' => 'meetings_an01_activity_notes_1meetings_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_MEETINGS_AN01_ACTIVITY_NOTES_1_FROM_AN01_ACTIVITY_NOTES_TITLE_ID',
  'id_name' => 'meetings_an01_activity_notes_1meetings_ida',
  'link' => 'meetings_an01_activity_notes_1',
  'table' => 'meetings',
  'module' => 'Meetings',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/AN01_Activity_Notes/Ext/Vardefs/nameFieldCustomization.php


$dictionary['AN01_Activity_Notes']['fields']['document_name']['required'] = 'false';
$dictionary['AN01_Activity_Notes']['fields']['document_name']['readonly'] = 'true';

?>
