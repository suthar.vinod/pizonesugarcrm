<?php

class HookBeforeSaveForNameGenerationAN {

    /**
     * 
      1.Coding Project #1 � Activity Notes Module
      a. All new records will have the following auto-incremented name � AN17-00001.
      Each new record will increase the naming by 1 (i.e. 00002, 00003, etc.).
     * 
     */
    public function generateName($bean, $event, $arguments) {
        $tableName = 'an01_activity_notes';
        $prefix = 'AN';
        $fieldName = 'document_name';
        $code = $this->_getCurrentId($tableName, $prefix, $fieldName);

        if (empty($bean->fetched_row[$fieldName]) || $bean->fetched_row[$fieldName] != $bean->$fieldName) {
            $bean->$fieldName = $code;
        }
    }

    private function _getCurrentId($tableName, $prefix, $fieldName) {
        global $db;
        //   get the year in 2 digits
        $year = date("y");
        //  Lock Table
        $sql_lock = "Lock TABLES {$tableName} READ";
        $db->query($sql_lock);

        //  In our Sales Activities Module, we would like to updated the automatic naming function from CP{YY}-XXXX.
        $query = "SELECT substring({$fieldName},-5) AS {$fieldName} From {$tableName} "
                . "WHERE {$fieldName} LIKE '%{$prefix}" . $year . "%-%' "
                . "ORDER BY $fieldName DESC LIMIT 0,1";
        $result = $db->query($query);

        //  Unlock Table
        $sql_unlock = "UNLOCK TABLES";
        $db->query($sql_unlock);

        $row = $db->fetchByAssoc($result);

        $system_id = $row[$fieldName];
        if (!empty($system_id)) {

            $number = (int) $system_id;
            $number = $number + 1;

            $length = strlen($number);
            $allowed_length = 5;

            $left_length = $allowed_length - $length;
            $code = $prefix;

            $code .= $year;
            $code .= '-';

            for ($i = 0; $i < $left_length; $i++) {
                $code .= "0";
            }
            $code .= $number;
        } else {
            $code = $prefix . $year . "-00001";
        }

        return $code;
    }

}
