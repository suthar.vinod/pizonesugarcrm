<?php
// created: 2016-01-11 20:22:33
$viewdefs['Opportunities']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'sales_stage' => 
    array (
    ),
    'account_name' => 
    array (
    ),
    'amount' => 
    array (
    ),
    'best_case' => 
    array (
    ),
    'worst_case' => 
    array (
    ),
    'next_step' => 
    array (
    ),
    'probability' => 
    array (
    ),
    'lead_source' => 
    array (
    ),
    'opportunity_type' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'date_closed' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);