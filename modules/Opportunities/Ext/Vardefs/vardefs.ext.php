<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:55
$dictionary['Opportunity']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/dupe_check.ext.php

$dictionary['Opportunity']['fields']['renewal']['studio'] = false;
$dictionary['Opportunity']['fields']['renewal_parent_name']['studio'] = false;
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_forecasted_likely.php

 // created: 2022-09-05 03:29:37
$dictionary['Opportunity']['fields']['forecasted_likely']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['forecasted_likely']['comments']='Rollup of included RLIs on the Opportunity';
$dictionary['Opportunity']['fields']['forecasted_likely']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['forecasted_likely']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['forecasted_likely']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['forecasted_likely']['formula']='ifElse(equal(indexOf($commit_stage, forecastIncludedCommitStages()), -1), 0, $amount)';
$dictionary['Opportunity']['fields']['forecasted_likely']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_probability.php

 // created: 2022-09-05 03:29:38
$dictionary['Opportunity']['fields']['probability']['massupdate']=false;
$dictionary['Opportunity']['fields']['probability']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['probability']['comments']='The probability of closure';
$dictionary['Opportunity']['fields']['probability']['importable']='required';
$dictionary['Opportunity']['fields']['probability']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['probability']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['probability']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['probability']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['probability']['min']=false;
$dictionary['Opportunity']['fields']['probability']['max']=false;
$dictionary['Opportunity']['fields']['probability']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_service_start_date.php

 // created: 2022-09-05 03:29:38
$dictionary['Opportunity']['fields']['service_start_date']['audited']=false;
$dictionary['Opportunity']['fields']['service_start_date']['massupdate']=true;
$dictionary['Opportunity']['fields']['service_start_date']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['service_start_date']['comments']='Service start date field.';
$dictionary['Opportunity']['fields']['service_start_date']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['service_start_date']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['service_start_date']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['service_start_date']['calculated']=false;
$dictionary['Opportunity']['fields']['service_start_date']['related_fields']=array (
);
$dictionary['Opportunity']['fields']['service_start_date']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['service_start_date']['studio']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_commit_stage.php

 // created: 2022-09-05 03:29:38
$dictionary['Opportunity']['fields']['commit_stage']['audited']=false;
$dictionary['Opportunity']['fields']['commit_stage']['massupdate']=true;
$dictionary['Opportunity']['fields']['commit_stage']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['commit_stage']['options']='';
$dictionary['Opportunity']['fields']['commit_stage']['comments']='Forecast commit ranges: Include, Likely, Omit etc.';
$dictionary['Opportunity']['fields']['commit_stage']['importable']=true;
$dictionary['Opportunity']['fields']['commit_stage']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['commit_stage']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['commit_stage']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['commit_stage']['enforced']=false;
$dictionary['Opportunity']['fields']['commit_stage']['dependency']=false;
$dictionary['Opportunity']['fields']['commit_stage']['related_fields']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_lost.php

 // created: 2022-09-05 03:29:38
$dictionary['Opportunity']['fields']['lost']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['lost']['options']='numeric_range_search_dom';
$dictionary['Opportunity']['fields']['lost']['comments']='Rollup of lost RLIs on the Opportunity';
$dictionary['Opportunity']['fields']['lost']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['lost']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['lost']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['lost']['formula']='ifElse(equal(indexOf($sales_stage, forecastOnlySalesStages(false, true, false)), -1), 0, $amount)';
$dictionary['Opportunity']['fields']['lost']['studio']=false;
$dictionary['Opportunity']['fields']['lost']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
