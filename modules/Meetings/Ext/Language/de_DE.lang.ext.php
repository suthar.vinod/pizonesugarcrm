<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/de_DE.customm01_sales_meetings_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_MEETINGS_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';
$mod_strings['LBL_M01_SALES_MEETINGS_1_FROM_MEETINGS_TITLE_ID'] = 'Sales Activities ID';
$mod_strings['LBL_M01_SALES_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Sales Activities';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/de_DE.customm03_work_product_meetings_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE_ID'] = 'Work Products ID';
$mod_strings['LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/de_DE.custommeetings_notes_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MEETINGS_NOTES_1_FROM_NOTES_TITLE'] = 'Notes (2)';
$mod_strings['LBL_MEETINGS_NOTES_1_FROM_MEETINGS_TITLE'] = 'Notes (2)';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/de_DE.custommeetings_an01_activity_notes_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MEETINGS_AN01_ACTIVITY_NOTES_1_FROM_AN01_ACTIVITY_NOTES_TITLE'] = 'Activity Notes';
$mod_strings['LBL_MEETINGS_AN01_ACTIVITY_NOTES_1_FROM_MEETINGS_TITLE'] = 'Activity Notes';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/de_DE.customm03_work_product_deliverable_meetings_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_MEETINGS_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Work Product Deliverables';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/de_DE.customa1a_critical_phase_inspectio_activities_1_meetings.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_MEETINGS_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE'] = 'Critical Phase Inspections';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/de_DE.customm01_sales_activities_1_meetings.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_ACTIVITIES_1_MEETINGS_FROM_M01_SALES_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/de_DE.customm03_work_product_activities_1_meetings.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_MEETINGS_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/de_DE.customqarev_cd_qa_reviews_meetings_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_QAREV_CD_QA_REVIEWS_TITLE'] = 'Controlled Document QA Reviews';
$mod_strings['LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Controlled Document QA Reviews';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/de_DE.custommeetings_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_MEETINGS_TITLE'] = 'Work Products';

?>
