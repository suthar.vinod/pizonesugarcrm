<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/en_us.customm01_sales_meetings_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_MEETINGS_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';
$mod_strings['LBL_M01_SALES_MEETINGS_1_FROM_MEETINGS_TITLE_ID'] = 'Sales Activities ID';
$mod_strings['LBL_M01_SALES_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Sales Activities';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/en_us.customm03_work_product_meetings_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE_ID'] = 'Work Products ID';
$mod_strings['LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/en_us.custommeetings_notes_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MEETINGS_NOTES_1_FROM_NOTES_TITLE'] = 'Notes (2)';
$mod_strings['LBL_MEETINGS_NOTES_1_FROM_MEETINGS_TITLE'] = 'Notes (2)';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/en_us.custommeetings_an01_activity_notes_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MEETINGS_AN01_ACTIVITY_NOTES_1_FROM_AN01_ACTIVITY_NOTES_TITLE'] = 'Activity Notes';
$mod_strings['LBL_MEETINGS_AN01_ACTIVITY_NOTES_1_FROM_MEETINGS_TITLE'] = 'Activity Notes';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/en_us.customm03_work_product_deliverable_meetings_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_MEETINGS_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Work Product Deliverables';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/en_us.customa1a_critical_phase_inspectio_activities_1_meetings.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_MEETINGS_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE'] = 'Critical Phase Inspections';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/en_us.customm01_sales_activities_1_meetings.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_ACTIVITIES_1_MEETINGS_FROM_M01_SALES_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/en_us.customm03_work_product_activities_1_meetings.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_MEETINGS_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/en_us.customqarev_cd_qa_reviews_meetings_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_QAREV_CD_QA_REVIEWS_TITLE'] = 'Controlled Document QA Reviews';
$mod_strings['LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Controlled Document QA Reviews';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/en_us.custommeetings_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_MEETINGS_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_MEETING'] = 'Schedule APS Activity';
$mod_strings['LBL_MODULE_NAME'] = 'APS Activities';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'APS Activity';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Create APS Activity';
$mod_strings['LNK_MEETING_LIST'] = 'View APS Activities';
$mod_strings['LNK_IMPORT_MEETINGS'] = 'Import APS Activities';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'APS Activity List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'APS Activity Search';
$mod_strings['LBL_PROJECTS_MEETINGS_FROM_PROJECT_TITLE'] = 'Project';
$mod_strings['LBL_PROJECTS_MEETINGS_FROM_MEETINGS_TITLE_ID'] = 'Project ID';
$mod_strings['LBL_PROJECTS_MEETINGS_FROM_MEETINGS_TITLE'] = 'Project';
$mod_strings['LBL_M01_SALES_MEETINGS_1_FROM_M01_SALES_TITLE'] = 'Sales Activity';
$mod_strings['LBL_MEETING_TYPE'] = 'Meeting Type';
$mod_strings['ERR_DELETE_RECORD'] = 'A record number must be specified to delete the APS Activity.';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'APS Activities';
$mod_strings['LBL_MEETING'] = 'APS Activity:';
$mod_strings['LBL_MODULE_TITLE'] = 'APS Activities: Home';
$mod_strings['LBL_SEQUENCE'] = 'APS Activity update sequence';
$mod_strings['LBL_TYPE'] = 'APS Activity Type';
$mod_strings['LBL_PASSWORD'] = 'APS Activity Password';
$mod_strings['LBL_URL'] = 'Start/Join APS Activity';
$mod_strings['NTC_REMOVE_INVITEE'] = 'Are you sure you want to remove this invitee from the APS Activity?';
$mod_strings['LBL_LIST_JOIN_MEETING'] = 'Join APS Activity';
$mod_strings['LBL_JOIN_EXT_MEETING'] = 'Join APS Activity';
$mod_strings['LBL_HOST_EXT_MEETING'] = 'Start APS Activity';
$mod_strings['LBL_EXTNOT_MAIN'] = 'You are not able to join this APS Activity because you are not an Invitee.';
$mod_strings['LBL_EXTNOT_RECORD_LINK'] = 'View APS Activity';
$mod_strings['LBL_EXTNOSTART_HEADER'] = 'Error: Cannot Start APS Activity';
$mod_strings['LBL_EXTNOSTART_MAIN'] = 'You cannot start this APS Activity because you are not an Administrator or the owner of the APS Activity.';
$mod_strings['LBL_RECURRING_LIMIT_ERROR'] = 'This recurring APS Activity cannot be scheduled because it exceeds the maximum allowed recurrence of $limit.';
$mod_strings['LBL_SYNCED_RECURRING_MSG'] = 'This APS Activity originated in another system and was synced to Sugar. To make changes, go to the original APS Activity within the other system. Changes made in the other system can be synced to this record.';
$mod_strings['LBL_CREATE_MODULE'] = 'Schedule APS Activity';
$mod_strings['LBL_RELATED_RECORD_DEFAULT_NAME'] = 'APS Activity with {{{this}}}';
$mod_strings['LBL_REMINDER_TITLE'] = 'APS Activity:';
$mod_strings['LBL_ACTIVITY_PERSONNEL'] = 'Activity Personnel';
$mod_strings['LBL_BUSINESS_DEVELOP_ACTIVITY'] = 'Business Development Activity';
$mod_strings['LBL_SCIENTIFIC_ACTIVITY'] = 'Scientific Activity';
$mod_strings['LBL_QUALITY_ASSURANCE_ACTIVITY'] = 'Quality Assurance Audit Activity';
$mod_strings['LBL_PATHOLOGY_ACTIVITY'] = 'Pathology Activity';
$mod_strings['LBL_ANALYTICAL_ACTIVITY'] = 'Analytical Activity';
$mod_strings['LBL_REGULATORY_ACTIVITY'] = 'Regulatory Activity';
$mod_strings['LBL_ACTUAL_HOURS_WORKED'] = 'Actual Hours Worked';
$mod_strings['LBL_REPORT_AUDIT_PAGE_TOTAL'] = 'Report Audit Page Total';
$mod_strings['LBL_MEETINGS_NOTES_FROM_NOTES_TITLE'] = 'Communication Notes';
$mod_strings['LBL_MEETINGS_NOTES_FROM_MEETINGS_TITLE'] = 'Communication Notes';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_MEETINGS_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverable';
$mod_strings['LBL_ANALYTICAL_EQUIPMENT_LIST'] = 'Analytical Equipment List';
$mod_strings['LBL_LIST_MY_MEETINGS'] = 'My Communications (Sugar) as Owner';
$mod_strings['LBL_MY_SCHEDULED_MEETINGS'] = 'Scheduled Communications (Sugar) as Owner';
$mod_strings['LBL_GUEST_MEETINGS'] = 'My Communications (Sugar) as Guest';
$mod_strings['LBL_GUEST_SCHEDULED_MEETINGS'] = 'Scheduled Communications (Sugar) as Guest';
$mod_strings['LBL_MEETINGS_FOCUS_DRAWER_DASHBOARD'] = 'Communications (Sugar) Focus Drawer';
$mod_strings['LBL_INTEGRATION_WORK_STREAM'] = 'Integration Work Stream';
$mod_strings['LBL_MEETINGS_RECORD_DASHBOARD'] = 'Communications (Sugar) Record Dashboard';

?>
