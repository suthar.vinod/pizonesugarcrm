<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:55
$dictionary['Meeting']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/m03_work_product_meetings_1_Meetings.php

// created: 2017-05-02 19:55:40
$dictionary["Meeting"]["fields"]["m03_work_product_meetings_1"] = array (
  'name' => 'm03_work_product_meetings_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_meetings_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE',
  'id_name' => 'm03_work_product_meetings_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["Meeting"]["fields"]["m03_work_product_meetings_1_name"] = array (
  'name' => 'm03_work_product_meetings_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_meetings_1m03_work_product_ida',
  'link' => 'm03_work_product_meetings_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["Meeting"]["fields"]["m03_work_product_meetings_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_meetings_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE_ID',
  'id_name' => 'm03_work_product_meetings_1m03_work_product_ida',
  'link' => 'm03_work_product_meetings_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/meetings_an01_activity_notes_1_Meetings.php

// created: 2017-05-11 22:18:50
$dictionary["Meeting"]["fields"]["meetings_an01_activity_notes_1"] = array (
  'name' => 'meetings_an01_activity_notes_1',
  'type' => 'link',
  'relationship' => 'meetings_an01_activity_notes_1',
  'source' => 'non-db',
  'module' => 'AN01_Activity_Notes',
  'bean_name' => 'AN01_Activity_Notes',
  'vname' => 'LBL_MEETINGS_AN01_ACTIVITY_NOTES_1_FROM_MEETINGS_TITLE',
  'id_name' => 'meetings_an01_activity_notes_1meetings_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/nameFieldCustomization.php


$dictionary['Meeting']['fields']['name']['required'] = 'false';
$dictionary['Meeting']['fields']['name']['readonly'] = 'true';
$dictionary['Meeting']['fields']['name']['len'] = '75';

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_email_reminder_time.php

 // created: 2017-06-16 19:20:11
$dictionary['Meeting']['fields']['email_reminder_time']['default']='-1';
$dictionary['Meeting']['fields']['email_reminder_time']['audited']=false;
$dictionary['Meeting']['fields']['email_reminder_time']['comments']='Specifies when a email reminder alert should be issued; -1 means no alert; otherwise the number of seconds prior to the start';
$dictionary['Meeting']['fields']['email_reminder_time']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['email_reminder_time']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['email_reminder_time']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['email_reminder_time']['calculated']=false;
$dictionary['Meeting']['fields']['email_reminder_time']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_meetings_Meetings.php

// created: 2017-09-13 15:15:31
$dictionary["Meeting"]["fields"]["a1a_critical_phase_inspectio_activities_1_meetings"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_meetings',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_meetings',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_MEETINGS_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/m01_sales_activities_1_meetings_Meetings.php

// created: 2018-12-10 23:02:13
$dictionary["Meeting"]["fields"]["m01_sales_activities_1_meetings"] = array (
  'name' => 'm01_sales_activities_1_meetings',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_meetings',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_MEETINGS_FROM_M01_SALES_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/m03_work_product_activities_1_meetings_Meetings.php

// created: 2019-08-05 12:03:27
$dictionary["Meeting"]["fields"]["m03_work_product_activities_1_meetings"] = array (
  'name' => 'm03_work_product_activities_1_meetings',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_meetings',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_MEETINGS_FROM_M03_WORK_PRODUCT_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_actual_hours_worked_c.php

 // created: 2021-01-12 10:35:32
$dictionary['Meeting']['fields']['actual_hours_worked_c']['labelValue']='Actual Hours Worked';
$dictionary['Meeting']['fields']['actual_hours_worked_c']['enforced']='';
$dictionary['Meeting']['fields']['actual_hours_worked_c']['dependency']='';
$dictionary['Meeting']['fields']['actual_hours_worked_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_analytical_activity_c.php

 // created: 2021-01-12 10:36:36
$dictionary['Meeting']['fields']['analytical_activity_c']['labelValue']='Analytical Activity';
$dictionary['Meeting']['fields']['analytical_activity_c']['dependency']='';
$dictionary['Meeting']['fields']['analytical_activity_c']['required_formula']='';
$dictionary['Meeting']['fields']['analytical_activity_c']['visibility_grid']=array (
  'trigger' => 'activity_personnel_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Business Development' => 
    array (
    ),
    'Scientific' => 
    array (
    ),
    'Quality Assurance' => 
    array (
    ),
    'Pathology' => 
    array (
    ),
    'Analytical' => 
    array (
      0 => '',
      1 => 'Analytical Report Writing',
      2 => 'Data Analysis',
      3 => 'Data Delivery To Toxicologist',
      4 => 'Exhaustive exaggerated extraction start',
      5 => 'Instrument Maintenance',
      6 => 'Method Development',
      7 => 'Protocol Development',
      8 => 'Sample Analysis',
      9 => 'Sample Prep',
      10 => 'Simulated Use Extraction Start',
      11 => 'Out of Office',
    ),
    'Regulatory' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_analytical_equipment_list_c.php

 // created: 2021-01-12 10:37:30
$dictionary['Meeting']['fields']['analytical_equipment_list_c']['labelValue']='Analytical Equipment List';
$dictionary['Meeting']['fields']['analytical_equipment_list_c']['dependency']='';
$dictionary['Meeting']['fields']['analytical_equipment_list_c']['required_formula']='';
$dictionary['Meeting']['fields']['analytical_equipment_list_c']['visibility_grid']=array (
  'trigger' => 'analytical_activity_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Analytical Report Writing' => 
    array (
    ),
    'Data Analysis' => 
    array (
    ),
    'Instrument Maintenance' => 
    array (
      0 => '',
      1 => 'ISQ LT',
      2 => 'TSQ Quantiva 1',
      3 => 'TSQ Quantiva 2',
      4 => 'QE Focus',
      5 => 'Nicolet iS10',
      6 => 'ASE350',
      7 => 'Agilent 7500 CS',
      8 => 'Precellys Evolution',
      9 => 'Cryolys',
    ),
    'Method Development' => 
    array (
    ),
    'Protocol Development' => 
    array (
    ),
    'Sample Analysis' => 
    array (
    ),
    'Sample Prep' => 
    array (
    ),
    'Out of Office' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_business_develop_activity_c.php

 // created: 2021-01-12 10:38:08
$dictionary['Meeting']['fields']['business_develop_activity_c']['labelValue']='Business Development Activity';
$dictionary['Meeting']['fields']['business_develop_activity_c']['dependency']='';
$dictionary['Meeting']['fields']['business_develop_activity_c']['required_formula']='';
$dictionary['Meeting']['fields']['business_develop_activity_c']['visibility_grid']=array (
  'trigger' => 'activity_personnel_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Business Development' => 
    array (
      0 => '',
      1 => 'Conference Call',
      2 => 'Client_Visit_Onsite',
      3 => 'Client_Visit_Offsite',
      4 => 'Tradeshow',
      5 => 'Procedure Coverage',
      6 => 'Other',
    ),
    'Scientific' => 
    array (
    ),
    'Quality Assurance' => 
    array (
    ),
    'Pathology' => 
    array (
    ),
    'Analytical' => 
    array (
    ),
    'Regulatory' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_meeting_type_c.php

 // created: 2021-01-12 10:38:53
$dictionary['Meeting']['fields']['meeting_type_c']['labelValue']='Meeting Type';
$dictionary['Meeting']['fields']['meeting_type_c']['dependency']='';
$dictionary['Meeting']['fields']['meeting_type_c']['required_formula']='';
$dictionary['Meeting']['fields']['meeting_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_pathology_activity_c.php

 // created: 2021-01-12 10:39:26
$dictionary['Meeting']['fields']['pathology_activity_c']['labelValue']='Pathology Activity';
$dictionary['Meeting']['fields']['pathology_activity_c']['dependency']='';
$dictionary['Meeting']['fields']['pathology_activity_c']['required_formula']='';
$dictionary['Meeting']['fields']['pathology_activity_c']['visibility_grid']=array (
  'trigger' => 'activity_personnel_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Business Development' => 
    array (
    ),
    'Scientific' => 
    array (
    ),
    'Quality Assurance' => 
    array (
    ),
    'Pathology' => 
    array (
      0 => '',
      1 => 'Necropsy',
      2 => 'Faxitron',
      3 => 'Tissue Receipt',
      4 => 'Tissue Trimming',
      5 => 'Tissue Shipping',
      6 => 'Paraffin Embedding',
      7 => 'Plastic Embedding',
      8 => 'Slide Completion',
      9 => 'Slide Shipping',
      10 => 'SEM Imaging',
      11 => 'Histomorphometry',
      12 => 'Slide Review',
      13 => 'Gross Necropsy Report Writing',
      14 => 'Pathology Report Writing',
      15 => 'Out of Office',
    ),
    'Analytical' => 
    array (
    ),
    'Regulatory' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_regulatory_activity_c.php

 // created: 2021-01-12 10:39:54
$dictionary['Meeting']['fields']['regulatory_activity_c']['labelValue']='Regulatory Activity';
$dictionary['Meeting']['fields']['regulatory_activity_c']['dependency']='';
$dictionary['Meeting']['fields']['regulatory_activity_c']['required_formula']='';
$dictionary['Meeting']['fields']['regulatory_activity_c']['visibility_grid']=array (
  'trigger' => 'activity_personnel_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Business Development' => 
    array (
    ),
    'Scientific' => 
    array (
    ),
    'Quality Assurance' => 
    array (
    ),
    'Pathology' => 
    array (
    ),
    'Analytical' => 
    array (
    ),
    'Regulatory' => 
    array (
      0 => '',
      1 => 'Regulatory Consulting',
      2 => 'FDA Response Writing',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_report_audit_page_total_c.php

 // created: 2021-01-12 10:40:30
$dictionary['Meeting']['fields']['report_audit_page_total_c']['labelValue']='Report Audit Page Total';
$dictionary['Meeting']['fields']['report_audit_page_total_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['report_audit_page_total_c']['enforced']='';
$dictionary['Meeting']['fields']['report_audit_page_total_c']['dependency']='equal($quality_assurance_activity_c,"Report Audit")';
$dictionary['Meeting']['fields']['report_audit_page_total_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_status.php

 // created: 2021-01-12 10:41:44
$dictionary['Meeting']['fields']['status']['audited']=true;
$dictionary['Meeting']['fields']['status']['massupdate']=true;
$dictionary['Meeting']['fields']['status']['comments']='Meeting status (ex: Planned, Held, Not held)';
$dictionary['Meeting']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['status']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['status']['calculated']=false;
$dictionary['Meeting']['fields']['status']['dependency']=false;
$dictionary['Meeting']['fields']['status']['default']='Planned';
$dictionary['Meeting']['fields']['status']['hidemassupdate']=false;
$dictionary['Meeting']['fields']['status']['full_text_search']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_name.php

 // created: 2021-01-12 10:42:16
$dictionary['Meeting']['fields']['name']['required']=false;
$dictionary['Meeting']['fields']['name']['audited']=true;
$dictionary['Meeting']['fields']['name']['massupdate']=false;
$dictionary['Meeting']['fields']['name']['hidemassupdate']=false;
$dictionary['Meeting']['fields']['name']['comments']='Meeting name';
$dictionary['Meeting']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['name']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.43',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_scientific_activity_c.php

 // created: 2021-01-26 17:00:44
$dictionary['Meeting']['fields']['scientific_activity_c']['labelValue']='Scientific Activity';
$dictionary['Meeting']['fields']['scientific_activity_c']['dependency']='';
$dictionary['Meeting']['fields']['scientific_activity_c']['required_formula']='';
$dictionary['Meeting']['fields']['scientific_activity_c']['visibility_grid']=array (
  'trigger' => 'activity_personnel_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Business Development' => 
    array (
    ),
    'Scientific' => 
    array (
      0 => '',
      1 => 'Protocol Development',
      2 => 'Reporting',
      3 => 'Operations Oversight',
    ),
    'Quality Assurance' => 
    array (
    ),
    'Pathology' => 
    array (
    ),
    'Analytical' => 
    array (
    ),
    'Regulatory' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/qarev_cd_qa_reviews_meetings_1_Meetings.php

// created: 2021-02-11 07:49:03
$dictionary["Meeting"]["fields"]["qarev_cd_qa_reviews_meetings_1"] = array (
  'name' => 'qarev_cd_qa_reviews_meetings_1',
  'type' => 'link',
  'relationship' => 'qarev_cd_qa_reviews_meetings_1',
  'source' => 'non-db',
  'module' => 'QARev_CD_QA_Reviews',
  'bean_name' => 'QARev_CD_QA_Reviews',
  'side' => 'right',
  'vname' => 'LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_MEETINGS_TITLE',
  'id_name' => 'qarev_cd_qa_reviews_meetings_1qarev_cd_qa_reviews_ida',
  'link-type' => 'one',
);
$dictionary["Meeting"]["fields"]["qarev_cd_qa_reviews_meetings_1_name"] = array (
  'name' => 'qarev_cd_qa_reviews_meetings_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_QAREV_CD_QA_REVIEWS_TITLE',
  'save' => true,
  'id_name' => 'qarev_cd_qa_reviews_meetings_1qarev_cd_qa_reviews_ida',
  'link' => 'qarev_cd_qa_reviews_meetings_1',
  'table' => 'qarev_cd_qa_reviews',
  'module' => 'QARev_CD_QA_Reviews',
  'rname' => 'name',
);
$dictionary["Meeting"]["fields"]["qarev_cd_qa_reviews_meetings_1qarev_cd_qa_reviews_ida"] = array (
  'name' => 'qarev_cd_qa_reviews_meetings_1qarev_cd_qa_reviews_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_QAREV_CD_QA_REVIEWS_MEETINGS_1_FROM_MEETINGS_TITLE_ID',
  'id_name' => 'qarev_cd_qa_reviews_meetings_1qarev_cd_qa_reviews_ida',
  'link' => 'qarev_cd_qa_reviews_meetings_1',
  'table' => 'qarev_cd_qa_reviews',
  'module' => 'QARev_CD_QA_Reviews',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/meetings_m03_work_product_1_Meetings.php

// created: 2021-04-01 09:05:41
$dictionary["Meeting"]["fields"]["meetings_m03_work_product_1"] = array (
  'name' => 'meetings_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'meetings_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'meetings_m03_work_product_1m03_work_product_idb',
);

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_activity_personnel_c.php

 // created: 2021-03-23 16:19:00
$dictionary['Meeting']['fields']['activity_personnel_c']['labelValue']='Activity Personnel';
$dictionary['Meeting']['fields']['activity_personnel_c']['dependency']='equal($integration_work_stream_c,"")';
$dictionary['Meeting']['fields']['activity_personnel_c']['required_formula']='';
$dictionary['Meeting']['fields']['activity_personnel_c']['readonly_formula']='';
$dictionary['Meeting']['fields']['activity_personnel_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_integration_work_stream_c.php

 // created: 2021-04-01 09:27:45
$dictionary['Meeting']['fields']['integration_work_stream_c']['labelValue']='Integration Work Stream';
$dictionary['Meeting']['fields']['integration_work_stream_c']['dependency']='';
$dictionary['Meeting']['fields']['integration_work_stream_c']['required_formula']='';
$dictionary['Meeting']['fields']['integration_work_stream_c']['readonly_formula']='';
$dictionary['Meeting']['fields']['integration_work_stream_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_quality_assurance_activity_c.php

 // created: 2022-02-01 07:34:15
$dictionary['Meeting']['fields']['quality_assurance_activity_c']['labelValue']='Quality Assurance Audit Activity';
$dictionary['Meeting']['fields']['quality_assurance_activity_c']['dependency']='';
$dictionary['Meeting']['fields']['quality_assurance_activity_c']['required_formula']='';
$dictionary['Meeting']['fields']['quality_assurance_activity_c']['readonly_formula']='';
$dictionary['Meeting']['fields']['quality_assurance_activity_c']['visibility_grid']=array (
  'trigger' => 'activity_personnel_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Business Development' => 
    array (
    ),
    'Scientific' => 
    array (
    ),
    'Quality Assurance' => 
    array (
      0 => '',
      1 => 'Application',
      2 => 'Analytical',
      3 => 'Animal Health Report',
      4 => 'Blood Collection',
      5 => 'Body Weights Observations',
      6 => 'Cell Processing',
      7 => 'Colony Counting',
      8 => 'Contributing Scientist Report',
      9 => 'Controlled Document Review',
      10 => 'Data Review',
      11 => 'Dose',
      12 => 'Evaluation',
      13 => 'Faxitron',
      14 => 'Final Report',
      15 => 'Followup Procedure',
      16 => 'Histology',
      17 => 'Implant',
      18 => 'In Life',
      19 => 'In_Life Report',
      20 => 'Interim M_M report',
      21 => 'Interim Pathology Report',
      22 => 'Interim Report',
      23 => 'Model Creation',
      24 => 'Morphometry',
      25 => 'Necropsy',
      26 => 'Pathology Report',
      27 => 'Protocol Amendment',
      28 => 'Protocol Review',
      29 => 'Final Report Amendment',
      30 => 'Sample Preparation',
      31 => 'Sample Receipt',
      32 => 'SEM Report',
      33 => 'Staining',
      34 => 'Terminal Procedure',
      35 => 'Tissue Trimming',
      36 => 'Training CPI',
      37 => 'Training Second Review',
      38 => 'Treatment',
      39 => 'Other_Non Study Specific',
      40 => 'Out of Office',
    ),
    'Pathology' => 
    array (
    ),
    'Analytical' => 
    array (
    ),
    'Regulatory' => 
    array (
    ),
  ),
);

 
?>
