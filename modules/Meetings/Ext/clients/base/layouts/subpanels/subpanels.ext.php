<?php
// WARNING: The contents of this file are auto-generated.


// created: 2017-05-11 22:18:50
$viewdefs['Meetings']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_MEETINGS_AN01_ACTIVITY_NOTES_1_FROM_AN01_ACTIVITY_NOTES_TITLE',
  'context' => 
  array (
    'link' => 'meetings_an01_activity_notes_1',
  ),
);

// created: 2021-04-01 09:05:41
$viewdefs['Meetings']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'meetings_m03_work_product_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['Meetings']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'meetings_an01_activity_notes_1',
  'view' => 'subpanel-for-meetings-meetings_an01_activity_notes_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Meetings']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'meetings_m03_work_product_1',
  'view' => 'subpanel-for-meetings-meetings_m03_work_product_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Meetings']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'opportunity',
  'view' => 'subpanel-for-meetings-opportunity',
);
