<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/WirelessLayoutdefs/meetings_an01_activity_notes_1_Meetings.php

 // created: 2017-05-11 22:18:50
$layout_defs["Meetings"]["subpanel_setup"]['meetings_an01_activity_notes_1'] = array (
  'order' => 100,
  'module' => 'AN01_Activity_Notes',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_MEETINGS_AN01_ACTIVITY_NOTES_1_FROM_AN01_ACTIVITY_NOTES_TITLE',
  'get_subpanel_data' => 'meetings_an01_activity_notes_1',
);

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/WirelessLayoutdefs/meetings_m03_work_product_1_Meetings.php

 // created: 2021-04-01 09:05:41
$layout_defs["Meetings"]["subpanel_setup"]['meetings_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'meetings_m03_work_product_1',
);

?>
