<?php

class HookBeforeSaveForNameGenerationMeetings
{

    /**
     * 
      3.  Coding Project #3 � Communications Module.
      a.  This one is a little trickier and I think we will need to discuss.
      Essentially you will take either the Sales Activity Number or Work Product ID (whichever has a value),
      followed by the Activity Personnel & Activity. An example would be APS17-0727 BD Conference Call.
     *
     */
    public function generateName($bean, $event, $arguments)
    {
        //  Get "Sales Activity" name
        global $db, $app_list_strings;

        $relatedModuleName = 'M01_Sales';
        $nameField = 'm01_sales_meetings_1_name';
        $idField = 'm01_sales_meetings_1m01_sales_ida';
        $salesActivityName = $this->_getRecordName($bean, $relatedModuleName, $nameField, $idField);

        //  Get "Work Product" name
        $relatedModuleName1 = 'M03_Work_Product';
        $nameField1 = 'm03_work_product_meetings_1_name';
        $idField1 = 'm03_work_product_meetings_1m03_work_product_ida';
        $workProductName = $this->_getRecordName($bean, $relatedModuleName1, $nameField1, $idField1);

        //  Get "Activity Personal" value
        $activityPersonalValue = $app_list_strings['activity_personnel_list'][$bean->activity_personnel_c];
        $work_stream = $app_list_strings['integration_work_stream_list'][$bean->integration_work_stream_c];

        //Get Work Stream
        $sql = "SELECT * FROM meetings WHERE name LIKE '%" . $work_stream . "%' And deleted=0 ORDER BY `date_entered` desc limit 1";
        $result = $db->query($sql);
        $row = $db->fetchByAssoc($result);
        $meet_name = $row['name'];

        if ($bean->fetched_row['id'] == "") {

            if ($result->num_rows == 0) {
                $number = sprintf("%02d", 1);
                if (!empty($workProductName)) {
                    $meet_name = $workProductName . ' - ' . $work_stream . '-' . $number;
                } else {
                    $meet_name = $work_stream . '-' . $number;
                }
            } else {
                $lastBarcodeNo = explode("-", $meet_name);
                $lastBarcodeNo = end($lastBarcodeNo);
                $meet_name_new = $lastBarcodeNo + 1;
                $meet_name_add = sprintf("%02d", $meet_name_new);
                if (!empty($workProductName)) {
                    $meet_name = $workProductName . ' - ' . $work_stream . '-' . $meet_name_add;
                } else {
                    $meet_name = $work_stream . '-' . $meet_name_add;
                }
            }
        }

        if ($bean->id == $bean->fetched_row['id'] && ($bean->integration_work_stream_c != $bean->fetched_row['integration_work_stream_c'])) {
            // //Get Work Stream
            // $sql = "SELECT * FROM meetings WHERE name LIKE '%" . $work_stream . "%' And deleted=0 ORDER BY `date_entered` desc limit 1";
            // $result = $db->query($sql);
            // $row = $db->fetchByAssoc($result);
            // $meet_name = $row['name'];
            if ($result->num_rows == 0) {
                $number = sprintf("%02d", 1);
                if (!empty($workProductName)) {
                    $meet_name = $workProductName . ' - ' . $work_stream . '-' . $number;
                } else {
                    $meet_name = $work_stream . '-' . $number;
                }
            } else {
                $lastBarcodeNo = explode("-", $meet_name);
                $lastBarcodeNo = end($lastBarcodeNo);
                $meet_name_new = $lastBarcodeNo + 1;
                $meet_name_add = sprintf("%02d", $meet_name_new);
                if (!empty($workProductName)) {
                    $meet_name = $workProductName . ' - ' . $work_stream . '-' . $meet_name_add;
                } else {
                    $meet_name = $work_stream . '-' . $meet_name_add;
                }
            }
        } else {
            $lastBarcodeNo = explode("-", $meet_name);
            $lastBarcodeNo = end($lastBarcodeNo);
            $meet_name_new = $lastBarcodeNo;
            $meet_name_add = sprintf("%02d", $meet_name_new);
            if (!empty($workProductName)) {
                $meet_name = $workProductName . ' - ' . $work_stream . '-' . $meet_name_add;
            } else {
                $meet_name = $work_stream . '-' . $meet_name_add;
            }
        }
        //  Get "Dependent Field" value
        $dependentFieldName = $this->_getDependentFieldValue($bean);

        $name1 = '';
        $name2 = '';

        if (!empty($salesActivityName)) {
            $name1 = $salesActivityName . ' ' . $activityPersonalValue;
            
        }

        $date_entered = date("Y/m/d", strtotime($bean->date_entered));

        if (!empty($work_stream) && ($date_entered > '2021/03/24')) {
            $name2 = $meet_name;
        } else if (!empty($workProductName) && ($date_entered > '2021/03/24')) {
            $name2 = $workProductName . ' - ' . $activityPersonalValue;
            
        } else {
            $name2 = $workProductName . ' ' . $activityPersonalValue;
           
        }


        if (!empty($dependentFieldName)) {
            $name1 = (!empty($name1)) ? $name1 . ' ' . $dependentFieldName : $salesActivityName;
            $name2 = (!empty($name2)) ? $name2 . ' ' . $dependentFieldName : $workProductName;
        }

        //  Already set ?
        if (!empty($name1)) {
            if ($bean->name == $name1) {
                return;
            }
        }

        //  Already set ?
        if (!empty($name2)) {
            if ($bean->name == $name2) {
                return;
            }
        }

        //  Set this.
        if (!empty($name1)) {
            $bean->name = $name1;
            return;
        }

        //  OR set this.
        if (!empty($name2)) {
            $bean->name = $name2;
            return;
        }

        //  Otherwise Set this.
        $bean->name = $activityPersonalValue;
        if (!empty($dependentFieldName)) {
            $bean->name = $activityPersonalValue . ' ' . $dependentFieldName;
        }
    }

    public function _getRecordName(&$bean, $relatedModuleName, $nameField, $idField)
    {
        $name = '';
        if (empty($bean->$nameField)) {
            if (!empty($bean->$idField)) {
                $b = BeanFactory::getBean($relatedModuleName, $bean->$idField, array('disable_row_level_security' => true));
                $name = $b->name;
            }
        } else {
            if (!empty($bean->$idField)) {
                //$name = $bean->$nameField;
                $b = BeanFactory::getBean($relatedModuleName, $bean->$idField, array('disable_row_level_security' => true));
                $name = $b->name;
            }
        }

        return $name;
    }

    private function _getDependentFieldValue(&$bean)
    {
        global $db, $app_list_strings;
        //  Get "Activity Personal" value
        $work_stream = $app_list_strings['integration_work_stream_list'][$bean->integration_work_stream_c];

        $mapping = array(
            'Business Development' => 'business_develop_activity_c',
            'Scientific' => 'scientific_activity_c',
            'Quality Assurance' => 'quality_assurance_activity_c',
            'Pathology' => 'pathology_activity_c',
            'Analytical' => 'analytical_activity_c',
            'Regulatory' => 'regulatory_activity_c',
        );

        $mapping2 = array(
            'Business Development' => 'business_develop_activity_list',
            'Scientific' => 'scientific_activity_list',
            'Quality Assurance' => 'phase_of_inspection_list',
            'Pathology' => 'pathology_activity_list',
            'Analytical' => 'analytical_activity_list',
            'Regulatory' => 'regulatory_activity_list',
        );

        if (array_key_exists($bean->activity_personnel_c, $mapping)) {
            $fieldName = $mapping[$bean->activity_personnel_c];

            return $app_list_strings[$mapping2[$bean->activity_personnel_c]][$bean->$fieldName];
            // $bean->$fieldName;
        } else {
            return '';
        }
    }
}
