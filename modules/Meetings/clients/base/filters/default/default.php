<?php
// created: 2021-03-23 16:17:53
$viewdefs['Meetings']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'activity_personnel_c' => 
    array (
    ),
    'type' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'date_end' => 
    array (
    ),
    'integration_work_stream_c' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'date_start' => 
    array (
    ),
    'status' => 
    array (
    ),
    'name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'team_name' => 
    array (
    ),
  ),
);