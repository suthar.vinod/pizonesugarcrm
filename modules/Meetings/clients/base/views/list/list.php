<?php
// created: 2022-12-13 04:43:54
$viewdefs['Meetings']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_LIST_SUBJECT',
          'link' => true,
          'default' => true,
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'repeat_type',
          ),
        ),
        1 => 
        array (
          'name' => 'm03_work_product_meetings_1_name',
          'label' => 'LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_M03_WORK_PRODUCT_TITLE',
          'enabled' => true,
          'id' => 'M03_WORK_PRODUCT_MEETINGS_1M03_WORK_PRODUCT_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_LIST_ASSIGNED_USER',
          'id' => 'ASSIGNED_USER_ID',
          'default' => true,
          'enabled' => true,
        ),
        3 => 
        array (
          'name' => 'activity_personnel_c',
          'label' => 'LBL_ACTIVITY_PERSONNEL',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'date_start',
          'label' => 'LBL_LIST_DATE',
          'type' => 'datetimecombo-colorcoded',
          'css_class' => 'overflow-visible',
          'completed_status_value' => 'Held',
          'link' => false,
          'default' => true,
          'enabled' => true,
          'readonly' => true,
          'related_fields' => 
          array (
            0 => 'status',
          ),
        ),
      ),
    ),
  ),
);