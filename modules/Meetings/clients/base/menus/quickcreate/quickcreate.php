<?php
// created: 2016-01-06 02:12:14
$viewdefs['Meetings']['base']['menu']['quickcreate'] = array (
  'layout' => 'create',
  'label' => 'LNK_NEW_MEETING',
  'visible' => true,
  'order' => 8,
  'icon' => 'fa-calendar',
);