<?php
// created: 2022-02-22 07:54:56
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'timepoint_type' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TIMEPOINT_TYPE',
    'width' => 10,
  ),
  'timepoint_name_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_TIMEPOINT_NAME',
    'width' => 10,
    'default' => true,
  ),
  'collection_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_COLLECTION_DATE',
    'width' => 10,
    'default' => true,
  ),
  'collection_date_time' => 
  array (
    'type' => 'datetimecombo',
    'vname' => 'LBL_COLLECTION_DATE_TIME',
    'width' => 10,
    'default' => true,
  ),
  'storage_condition' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STORAGE_CONDITION',
    'width' => 10,
  ),
);