<?php
// created: 2021-11-17 05:42:49
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STATUS',
    'width' => 10,
  ),
  'category' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_CATEGORY',
    'width' => 10,
  ),
  'type_2' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TYPE_2',
    'width' => 10,
  ),
  'location_building' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_LOCATION_BUILDING',
    'width' => 10,
  ),
  'location_room' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_LOCATION_ROOM',
    'id' => 'RMS_ROOM_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'RMS_Room',
    'target_record_key' => 'rms_room_id_c',
  ),
  'allowed_storage_conditions' => 
  array (
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_ALLOWED_STORAGE_CONDITIONS',
    'width' => 10,
  ),
  'storage_condition' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STORAGE_CONDITION',
    'width' => 10,
  ),
  'allowed_storage_medium' => 
  array (
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_ALLOWED_STORAGE_MEDIUM',
    'width' => 10,
  ),
  'storage_medium' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STORAGE_MEDIUM',
    'width' => 10,
  ),
  'internal_barcode' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'readonly' => true,
    'vname' => 'LBL_INTERNAL_BARCODE',
    'width' => 10,
  ),
  'ic_inventory_collection_ii_inventory_item_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
    'id' => 'IC_INVENTO128FLECTION_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'IC_Inventory_Collection',
    'target_record_key' => 'ic_invento128flection_ida',
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'readonly' => true,
    'vname' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'created_by',
  ),
);