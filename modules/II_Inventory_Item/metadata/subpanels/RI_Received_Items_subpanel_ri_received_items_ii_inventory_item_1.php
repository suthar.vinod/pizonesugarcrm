<?php
// created: 2021-09-01 11:51:56
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'category' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_CATEGORY',
    'width' => 10,
  ),
  'type_2' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TYPE_2',
    'width' => 10,
  ),
  'quantity_2_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'int',
    'vname' => 'LBL_QUANTITY_2',
    'width' => 10,
    'default' => true,
  ),
  'lot_number_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_LOT_NUMBER',
    'width' => 10,
  ),
  'expiration_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_EXPIRATION_DATE',
    'width' => 10,
    'default' => true,
  ),
  'product_name' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_PRODUCT_NAME',
    'width' => 10,
  ),
  'manufacturer_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_MANUFACTURER',
    'width' => 10,
  ),
  'product_id_2_c' => 
  array (
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_PRODUCT_ID_2',
    'width' => 10,
    'default' => true,
  ),
);