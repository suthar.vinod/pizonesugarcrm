<?php
$popupMeta = array (
    'moduleMain' => 'II_Inventory_Item',
    'varName' => 'II_Inventory_Item',
    'orderBy' => 'ii_inventory_item.name',
    'whereClauses' => array (
  'name' => 'ii_inventory_item.name',
  'internal_barcode' => 'ii_inventory_item.internal_barcode',
  'assigned_user_id' => 'ii_inventory_item.assigned_user_id',
  'favorites_only' => 'ii_inventory_item.favorites_only',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'internal_barcode',
  5 => 'assigned_user_id',
  6 => 'favorites_only',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10',
  ),
  'internal_barcode' => 
  array (
    'type' => 'varchar',
    'readonly' => 'true',
    'label' => 'LBL_INTERNAL_BARCODE',
    'width' => '10',
    'name' => 'internal_barcode',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10',
  ),
  'favorites_only' => 
  array (
    'name' => 'favorites_only',
    'label' => 'LBL_FAVORITES_FILTER',
    'type' => 'bool',
    'width' => '10',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'INTERNAL_BARCODE' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'readonly' => true,
    'label' => 'LBL_INTERNAL_BARCODE',
    'width' => 10,
  ),
  'COLLECTION_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_COLLECTION_DATE',
    'width' => 10,
    'default' => true,
  ),
),
);
