<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class II_emailReminder{
	static $already_ran = false;
	function sendEmailReminder($bean, $event, $arguments){
		//if(self::$already_ran == true) return;
        //self::$already_ran = true;
		
		global $db;
		$site_url 		= $GLOBALS['sugar_config']['site_url'];
		$current_date 	= date("Y-m-d"); //current date
		
		if($bean->id && $bean->reminder_mail_sent_c!=1 && $bean->analyze_by_date_c!="" ){
			$ii_ID 		= $bean->id;
			$ii_name	= $bean->name;
			$analyze_by_date = $bean->analyze_by_date_c;

			$date1 = strtotime($analyze_by_date);
			$date2 = strtotime($current_date);

			$diff = abs($date1-$date2);
			$days = floor(($diff)/ (60*60*24)); 

			if((($date1>$date2 && $days<14) || $date1<$date2 || $days==0)){
				
				$template = new EmailTemplate();
				$emailObj = new Email();
				$wpID  = $bean->m03_work_product_ii_inventory_item_1m03_work_product_ida;
				$wp = BeanFactory::getBean('M03_Work_Product', $wpID);

				$sd_id = $wp->contact_id_c;
			

				$sdEmailAddress = "";
				if ($sd_id!="") {
                    $studyDirector = BeanFactory::getBean('Contacts', $sd_id);
                    $sdEmailAddress = $studyDirector->emailAddress->getPrimaryAddress($studyDirector);
					// $GLOBALS['log']->fatal("=====wpSDID email====>".$sdEmailAddress);
                } 
				$template->retrieve_by_string_fields(array('name' => 'Reminder Inventory Item Pending Analyze by Date', 'type' => 'email'));
					
				$IILink = '<a target="_blank"href="'.$site_url.'/#II_Inventory_Item/'.$ii_ID.'">'.$ii_name.'</a>';
					 
				$template->body_html = str_replace('[insert_inventory_item_name]', $IILink, $template->body_html);
					
				$defaults	= $emailObj->getSystemDefaultEmail();
				$mail		= new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->IsHTML(true);
				$mail->From		= $defaults['email'];
				$mail->FromName = $defaults['name'];
				$mail->Subject	= "Reminder - Pending Analyze by Date for ".$ii_name;
				$mail->Body		= $template->body_html;
				$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';
					
				$mail->AddAddress('fxs_mjohnson@apsemail.com');				
				// $mail->AddAddress('vsuthar@apsemail.com');

				if ($sdEmailAddress!="") {
					$mail->AddAddress($sdEmailAddress);
                } 
					
				//If their exist some valid email addresses then send email
				if (!empty($wp_emailAdd_dev)) {
					if (!$mail->Send()) {
						//$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
					} else {
						//$GLOBALS['log']->debug('Analyse by date email sent');
						$bean->reminder_mail_sent_c =1;
					}
					unset($mail);
				}
			} 
		}
	}
	
	 
}
?>