({
    extendsFrom: 'RelateField',
    initialize: function (options) {
        this._super('initialize', [options]);

    },
    openSelectDrawer: function () {

        if (this.getSearchModule() == "RI_Received_Items") {
            this.selectRI_Received_Items();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else if (this.getSearchModule() == "ANML_Animals" && (this.model.get('ts_hidden_string_c') != undefined && this.model.get('ts_hidden_string_c') != "")) {
            console.log('aaaaa',this.model.get('ts_hidden_string_c'));
            this.selectLocation();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else if (this.getSearchModule() == "RMS_Room" && this.model.get('location_building') != "") {
            this.selectRooms();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else if (this.getSearchModule() == "Equip_Equipment" && this.model.get('location_building') != "") {
            this.selectEEs();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else {
            this._super('openSelectDrawer');
            $("button.btn.btn-invisible.btn-dark").css("display", "block");
            $(".filter-definition-container").css('pointer-events', 'unset');
            $(".choice-filter-clickable").css('pointer-events', 'unset');

        }

    },

    selectRI_Received_Items: function () {
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterQuantityRI_Received_Items',
                'initial_filter_label': 'Quantity Received Greaterthen Linked Inventory Item',
                'filter_populate': {
                    'linked_ii_sum_flag_c': 1,
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Rooms relate field.
        filterOptions = (this.getSearchModule() == "RI_Received_Items") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    selectRooms: function () {
        var location_building = this.model.get('location_building');
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterRMS_RoomTemplate',
                'initial_filter_label': 'Location(Rooms)',
                'filter_populate': {
                    'building_c': location_building,
                },
                'filter_relate': {
                    'building_c': location_building,
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Rooms relate field.
        filterOptions = (this.getSearchModule() == "RMS_Room") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    selectEEs: function () {
        var location_building = this.model.get('location_building');
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterEquipmentTemplateLocation',
                'initial_filter_label': 'Location(Equipment)',
                'filter_populate': {
                    'building_c': location_building,
                },
                'filter_relate': {
                    'building_c': location_building,
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "Equip_Equipment") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    selectLocation: function () {
        var ts_name = this.model.get('ts_hidden_string_c');
        var tsname = ts_name.split(",");

        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'FilterTestSystem',
                'initial_filter_label': 'Test System',
                'filter_populate': {
                    'name': tsname,
                },
                'filter_relate': {
                    'name': tsname,
                },
            })
            .populateRelate(this.model)
            .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Test system relate field.
        filterOptions = (this.getSearchModule() == "ANML_Animals") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
    /*To disable search text box filter of Test System */
    search: _.debounce(function (query) {
        var term = query.term || '',
            self = this,
            searchModule = this.getSearchModule(),
            params = {},
            limit = self.def.limit || 5,
            relatedModuleField = this.getRelatedModuleField();

        if (query.context) {
            params.offset = this.searchCollection.next_offset;
        }

        params.filter = this.buildFilterDefinition(term);

        this.searchCollection.fetch({
            //Don't show alerts for this request
            showAlerts: false,
            update: true,
            remove: _.isUndefined(params.offset),
            fields: this.getSearchFields(),
            context: self,
            params: params,
            //limit: limit,
            success: function (data) {
                var fetch = { results: [], more: data.next_offset > 0, context: data };
                if (fetch.more) {
                    var fieldEl = self.$(self.fieldTag),
                        //For teamset widget, we should specify which index element to be filled in
                        plugin = (fieldEl.length > 1) ? $(fieldEl.get(self.currentIndex)).data("select2") : fieldEl.data("select2"),
                        height = plugin.searchmore.children("li:first").children(":first").outerHeight(),
                        //0.2 makes scroll not to touch the bottom line which avoid fetching next record set
                        maxHeight = height * (limit - .2);
                    plugin.results.css("max-height", maxHeight);
                }
                _.each(data.models, function (model, index) {
                    if (params.offset && index < params.offset) {
                        return;
                    }
                    fetch.results.push({
                        id: model.id,
                        text: model.get(relatedModuleField) + ''
                    });
                });
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback(fetch);
                }
            },
            error: function () {
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback({ results: [] });
                }
                app.logger.error("Unable to fetch the bean collection.");
            }
        });

    }, app.config.requiredElapsed || 500),

    find: function (key, array) {
        var results = [];
        for (var i = 0; i < array.length; i++) {
            if ((array[i].toLowerCase()).indexOf(key.toLowerCase()) == 0) {
                results.push(array[i]);
            }
        }
        return results;
    },
    _getCustomFilter1: function (testsystem) {
        //var m03_work_product_name = this.model.get('m03_work_product_ii_inventory_item_1_name');
        var ts_name = this.model.get('ts_hidden_string_c');
        var AllTsName = ts_name.split(",");
        var testsystem = this.find(testsystem, AllTsName);

        if (testsystem != "") {
            return [{
                'name': {
                    '$in': testsystem,
                },

            }];
        } else {
            return [{
                '$or': [
                    { 'name': '' }
                ]
            }];
        }
    },
    _getCustomFilterRoom: function () {
        var location_building = this.model.get('location_building');
        return [{
            '$or': [
                { 'building_c': location_building }
            ]
        }];
    },
    _getCustomFilterEE: function () {
        var location_building = this.model.get('location_building');
        return [{
            '$or': [
                { 'building_c': location_building }
            ]
        }];
    },
    _getCustomFilterRI_Received_Items: function () {
        return [{
            'linked_ii_sum_flag_c': {
                '$equals': 1,
            },

        }];
    },
    /**
     * @override
     */
    buildFilterDefinition: function (searchTerm) {
        var parentFilter = this._super('buildFilterDefinition', [searchTerm]);
        //console.log('bbbbb',this.model.get('ts_hidden_string_c'));
        if (this.getSearchModule() == 'ANML_Animals' && (this.model.get('ts_hidden_string_c') != undefined && this.model.get('ts_hidden_string_c') != "")) {
            //console.log('ccc');
            return parentFilter.concat(this._getCustomFilter1(searchTerm));
        } else if (this.getSearchModule() == 'RI_Received_Items') {
            return parentFilter.concat(this._getCustomFilterRI_Received_Items());
        } else if (this.getSearchModule() == 'RMS_Room' && this.model.get('location_building') != "") {
            return parentFilter.concat(this._getCustomFilterRoom());
        } else if (this.getSearchModule() == 'Equip_Equipment' && this.model.get('location_building') != "") {
            return parentFilter.concat(this._getCustomFilterEE());
        } else {
            return parentFilter;
        }
    },
    /**EOC Disable search filter code */
})