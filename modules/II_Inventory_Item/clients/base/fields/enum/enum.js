({
    extendsFrom: 'EnumField',
    initialize: function (opts) {
        this._super('initialize', [opts]);
        this._initEvents();
        this.model.on('data:sync:complete', this.onload_function, this);
    },
    _initEvents: function () {
        this._changeIndustry();
    },
    onload_function: function () {
        this.changeIndustryHandler();
    },
    _changeIndustry: function () {
        this.model.on("change:related_to_c", this.changeIndustryHandler, this);
    },
    changeIndustryHandler: function () {

        if (this.model.get('related_to_c') == "Purchase Order Item" || this.model.get('related_to_c') == "Order Request Item") {
            var self = this;
            if (self.name === 'type_2') {
                var newOptions = {
                    '': '',
                    'Autoclave supplies': 'Autoclave supplies',
                    'Balloon Catheter': 'Balloon Catheter',
                    'Bandaging': 'Bandaging',
                    'Blood draw supply': 'Blood draw supply',
                    'Bypass supply': 'Bypass supply',
                    'Cannula': 'Cannula',
                    'Catheter': 'Catheter',
                    'Chemical': 'Chemical',
                    'Cleaning Agent': 'Cleaning Agent',
                    'Cleaning Supplies': 'Cleaning Supplies',
                    'Contrast': 'Contrast',
                    'Control Matrix': 'Control Matrix',
                    'Drug': 'Drug',
                    'Endotracheal TubeSupplies': 'Endotracheal Tube/Supplies',
                    'Enrichment Toy': 'Enrichment Toy',
                    'Equipment': 'Equipment',
                    'ETO supplies': 'ETO supplies',
                    'Feeding supplies': 'Feeding supplies',
                    'Glassware': 'Glassware',
                    'Graft': 'Graft',
                    'Inflation Device': 'Inflation Device',
                    'Interventional Supplies': 'Interventional Supplies',
                    'Office Supply': 'Office Supply',
                    'Other': 'Other',
                    'Plastic Ware': 'Plastic Ware',
                    'PPE supply': 'PPE supply',
                    'Reagent': 'Reagent',
                    'Sheath': 'Sheath',
                    'Shipping supplies': 'Shipping supplies',
                    'Solution': 'Solution',
                    'Stent': 'Stent',
                    'Surgical Instrument': 'Surgical Instrument',
                    'Surgical site prep': 'Surgical site prep',
                    'Surgical Supplies': 'Surgical Supplies',
                    'Suture': 'Suture',
                    'Tubing': 'Tubing',
                    'Wire': 'Wire',
                };
                self.items = newOptions;
            }
        }

        if (this.model.get('location_type') == "") {
            var self = this;
            if (self.name === 'location_type') {
                var newOptions = {
                    '': '',
                    'Known': 'Known',
                    'Unknown': 'Unknown',
                };
                self.items = newOptions;
            }
        }

        /*if (this.model.get('location_type') == "Equipment" || this.model.get('location_type') == "Room" || this.model.get('location_type') == "RoomCabinet" || this.model.get('location_type') == "RoomShelf" || this.model.get('location_type') == "Cabinet" || this.model.get('location_type') == "Shelf") {
         var self = this;
         if (self.name === 'location_type') {
         var newOptions = {
         '': '',
         'Cabinet': 'Cabinet',
         'Equipment': 'Equipment',
         'RoomCabinet': 'Room/Cabinet',
         'RoomShelf': 'Room/Shelf',
         'Room': 'Room',
         'Shelf': 'Shelf',
         };
         self.items = newOptions;
         }
         }*/
    }
})