<?php
// created: 2022-02-22 08:07:36
$viewdefs['II_Inventory_Item']['base']['filter']['default'] = array (
  'quicksearch_field' => 
  array (
    0 => 
    array (
      0 => 'name',
      1 => 'internal_barcode',
    ),
  ),
  'quicksearch_priority' => 1,
  'fields' => 
  array (
    'actual_discard_date' => 
    array (
    ),
    'allowed_storage_conditions' => 
    array (
    ),
    'allowed_storage_medium' => 
    array (
    ),
    'analyze_by_date_c' => 
    array (
    ),
    'category' => 
    array (
    ),
    'collection_date' => 
    array (
    ),
    'collection_date_time' => 
    array (
    ),
    'collection_date_time_type' => 
    array (
    ),
    'concentration_c' => 
    array (
    ),
    'concentration_unit_c' => 
    array (
    ),
    'storage_condition' => 
    array (
    ),
    'storage_medium' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'days_to_expiration' => 
    array (
    ),
    'expiration_date' => 
    array (
    ),
    'expiration_date_time' => 
    array (
    ),
    'expiration_test_article' => 
    array (
    ),
    'external_barcode' => 
    array (
    ),
    'internal_barcode' => 
    array (
    ),
    'ic_inventory_collection_ii_inventory_item_1_name' => 
    array (
    ),
    'location_bin_c' => 
    array (
    ),
    'location_building' => 
    array (
    ),
    'location_cabinet' => 
    array (
    ),
    'location_cubby_c' => 
    array (
    ),
    'location_equipment' => 
    array (
    ),
    'location_rack_c' => 
    array (
    ),
    'location_room' => 
    array (
    ),
    'location_shelf' => 
    array (
    ),
    'location_type' => 
    array (
    ),
    'lot_number_c' => 
    array (
    ),
    'manufacturer_c' => 
    array (
    ),
    'name' => 
    array (
    ),
    'description' => 
    array (
    ),
    'number_of_aliquots_c' => 
    array (
    ),
    'other_test_type_c' => 
    array (
    ),
    'other_type_c' => 
    array (
    ),
    'owner' => 
    array (
    ),
    'stability_considerations_c' => 
    array (
    ),
    'pk_samples_c' => 
    array (
    ),
    'planned_discard_date' => 
    array (
    ),
    'product_id_2_c' => 
    array (
    ),
    'product_name' => 
    array (
    ),
    'poi_purchase_order_item_ii_inventory_item_1_name' => 
    array (
    ),
    'quantity_2_c' => 
    array (
    ),
    'received_date' => 
    array (
    ),
    'ri_received_items_ii_inventory_item_1_name' => 
    array (
    ),
    'related_to_c' => 
    array (
    ),
    'retention_interval_days' => 
    array (
    ),
    'retention_start_date' => 
    array (
    ),
    'm01_sales_ii_inventory_item_1_name' => 
    array (
    ),
    'ship_to_address' => 
    array (
    ),
    'ship_to_company' => 
    array (
    ),
    'ship_to_contact' => 
    array (
    ),
    'specimen_type_1_c' => 
    array (
    ),
    'specimen_type_2_c' => 
    array (
    ),
    'specimen_type_3_c' => 
    array (
    ),
    'specimen_type_4_c' => 
    array (
    ),
    'specimen_type_5_c' => 
    array (
    ),
    'status' => 
    array (
    ),
    'sterilization' => 
    array (
    ),
    'sterilization_exp_date_c' => 
    array (
    ),
    'subtype' => 
    array (
    ),
    'anml_animals_ii_inventory_item_1_name' => 
    array (
    ),
    'test_type_c' => 
    array (
    ),
    'test_type_1_c' => 
    array (
    ),
    'test_type_2_c' => 
    array (
    ),
    'test_type_3_c' => 
    array (
    ),
    'test_type_4_c' => 
    array (
    ),
    'test_type_5_c' => 
    array (
    ),
    'timepoint_name_c' => 
    array (
    ),
    'timepoint_integer' => 
    array (
    ),
    'timepoint_type' => 
    array (
    ),
    'timepoint_unit' => 
    array (
    ),
    'type_2' => 
    array (
    ),
    'm03_work_product_ii_inventory_item_1_name' => 
    array (
    ),
    'm03_work_product_deliverable_ii_inventory_item_1_name' => 
    array (
    ),
  ),
);