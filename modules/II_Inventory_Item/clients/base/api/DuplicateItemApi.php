<?php

//use \Sugarcrm\Sugarcrm\custom\wsystems\InventoryItemCreation\Utils\InventoryItemCreateDuplicatesUtils;
//use \Sugarcrm\Sugarcrm\custom\wsystems\InventoryItemCreation\Utils\InventoryItemCreateDuplicatesTsUtils;
//use \Sugarcrm\Sugarcrm\custom\wsystems\InventoryItemCreation\Utils\InventoryItemCreateDuplicatesPoiUtils;
//use \Sugarcrm\Sugarcrm\custom\wsystems\InventoryItemCreation\Utils\InventoryItemCreateDuplicatesOriUtils;

if (!defined('sugarEntry') || !sugarEntry) {
  die('Not A Valid Entry Point');
}

class DuplicateItemApi extends SugarApi
{

  public function registerApiRest()
  {
    return array(
      'MyGetEndpoint' => array(
        'reqType' => 'GET',
        'noLoginRequired' => false,
        'path' => array('II_Inventory_Item', '?', '?', 'create_duplicates'),
        'pathVars' => array('module', 'record', 'subtype'),
        'method' => 'create_duplicates',
        'shortHelp' => '',
        'longHelp' => '',
      ),

      'create_ts_duplicates' => array(
        'reqType' => 'POST',
        'noLoginRequired' => false,
        'path' => array('create_ts_duplicates'),
        'pathVars' => array('create_ts_duplicates'),
        'method' => 'create_ts_duplicates',
        'shortHelp' => '',
        'longHelp' => '',
      ),
      'MyGetEndpointPoi' => array(
        'reqType' => 'POST',
        'noLoginRequired' => false,
        'path' => array('create_poi_duplicates'),
        'pathVars' => array(),
        'method' => 'create_poi_duplicates',
        'shortHelp' => '',
        'longHelp' => '',
      ),
      'MyGetEndpointOri' => array(
        'reqType' => 'POST',
        'noLoginRequired' => false,
        'path' => array('create_ori_duplicates'),
        'pathVars' => array(),
        'method' => 'create_ori_duplicates',
        'shortHelp' => '',
        'longHelp' => '',
      ),
      /**#1572 : 16 Sep 2021 
      'MyGetEndpointNum' => array(
        'reqType' => 'GET',
        'noLoginRequired' => false,
        'path' => array('II_Inventory_Item', '?', '?', 'create_aliquots_duplicates'),
        'pathVars' => array('module', 'record', 'aliquots'),
        'method' => 'create_aliquots_duplicates',
        'shortHelp' => '',
        'longHelp' => '',
      ),*/
      'MyGetEndpointNum' => array(
        'reqType' => 'POST',
        'noLoginRequired' => false,
        'path' => array('create_aliquots_duplicates'),
        'pathVars' => array(),
        'method' => 'create_aliquots_duplicates',
        'shortHelp' => '',
        'longHelp' => '',
      ),

      'MyGetEndpoint1aTs' => array(
        'reqType' => 'POST',
        'noLoginRequired' => false,
        'path' => array('create_aliquots_tsmulti_duplicates'),
        'pathVars' => array(),
        'method' => 'create_aliquots_tsmulti_duplicates',
        'shortHelp' => '',
        'longHelp' => '',
      ),

      'MyGetEndpointSP' => array(
        'reqType' => 'POST',
        'noLoginRequired' => false,
        'path' => array('specimen_type_duplicates'),
        'pathVars' => array(),
        'method' => 'specimen_type_duplicates',
        'shortHelp' => '',
        'longHelp' => '',
      ),

      'MyGetEndpointSP12' => array(
        'reqType' => 'POST',
        'noLoginRequired' => false,
        'path' => array('specimen_type_ts_duplicates'),
        'pathVars' => array(),
        'method' => 'specimen_type_ts_duplicates',
        'shortHelp' => '',
        'longHelp' => '',
      ),
    );
  }

  public function specimen_type_duplicates($api, $args)
  {
    global $db, $app_list_strings;
    $module = $args['module'];
    $recordId = $args['recordId'];
    $specimen_type = $args['specimen_type'];
    $specimen_type0 = $specimen_type[0];
    $test_type = $args['test_type'];

    $sqlPK = 'UPDATE ii_inventory_item_cstm SET pk_samples_c = "NA", specimen_type_1_c="", specimen_type_2_c="", specimen_type_3_c="", specimen_type_4_c="", specimen_type_5_c="", test_type_1_c="", test_type_2_c="", test_type_3_c="", test_type_4_c="", test_type_5_c="" WHERE id_c = "' . $recordId . '"';
    $db->query($sqlPK);

    $clonedBean = BeanFactory::retrieveBean($module, $recordId);
    /**14 Sep 2022 : bug fix 468 : PMT re-naming samples incorrectly : Special character was causing issue in name replace 
     * whenever there are more than one record with speciment type , so we have mapped that by their list          */
    $ii_test_type_list = $app_list_strings['ii_test_type_list'];
    $specimen_type_list = $app_list_strings['specimen_type_list'];

    if (count($specimen_type) > 0) {

      $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
      $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
      $clonedBean->load_relationship("m03_work_product_ii_inventory_item_2");
      $clonedBean->load_relationship("anml_animals_ii_inventory_item_1");

      //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");
      //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();

      $product_ids = $clonedBean->m03_work_product_ii_inventory_item_1->get();
      $sale_id = $clonedBean->m01_sales_ii_inventory_item_1->get();
      $work_product_id = $clonedBean->m03_work_product_ii_inventory_item_2->get();
      $test_system_id = $clonedBean->anml_animals_ii_inventory_item_1->get();
      for ($k = 1; $k < count($specimen_type); $k++) {
        if ($specimen_type[$k] != "") {
          $IIBean = BeanFactory::newBean('II_Inventory_Item');

          $bid = create_guid();
          $IIBean->id = $bid;
          $IIBean->new_with_id = true;
          $clonedBeanName = substr($clonedBean->name, 0, -3);
          if (!empty($test_system_id)) {
            $arrPullTSId1 = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
            $arrPullUSDAId1 = array("Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig");
            $testSystemBean1 = BeanFactory::retrieveBean("ANML_Animals", $test_system_id[0]);
            $species1 = $testSystemBean1->species_2_c;
            $ts_name = $testSystemBean1->name;


            $nameAppend1 = "";
            $clonedBeanArr1 = explode(" ", $clonedBeanName);
            if (in_array($species1, $arrPullTSId1)) {
              $nameAppend1 = $ts_name;
              $clonedBeanArr1[1] = $nameAppend1;
            } else if (in_array($species1, $arrPullUSDAId1)) {
              if ($testSystemBean1->usda_id_c != "") {
                $nameAppend1 = $testSystemBean1->usda_id_c;
              } else {
                $nameAppend1 = $ts_name;
              }
              $clonedBeanArr1[1] = $nameAppend1;
            } else {
              $nameAppend1 = $ts_name;
              $clonedBeanArr1[1] = $nameAppend1;
            }
            $clonedBeanName_New1 = implode(" ", $clonedBeanArr1);
            if ($specimen_type[$k] == "Other") {
              $bean_newName = str_replace($specimen_type0, $clonedBean->other_type_c, $clonedBeanName_New1);
              $IIBean->other_type_c = $clonedBean->other_type_c;
              /**09 March 2022 : #2218 */
              if ($test_type[$k] == "Other") {
                $bean_newName = str_replace($test_type[0], $clonedBean->other_test_type_c, $bean_newName);
              }
            } else {
              if ($clonedBean->other_type_c != "") {
                $bean_newName = str_replace($clonedBean->other_type_c, $specimen_type[$k], $clonedBeanName_New1);
              } else {
                //$bean_newName = str_replace($specimen_type0, $specimen_type[$k], $clonedBeanName_New1);
                /**14 Sep 2022 : bug fix 468 : PMT re-naming samples incorrectly : Special character was causing issue in name replace 
                 * whenever there are more than one record with speciment type , so we have mapped that by their list
                 */
                $bean_newName = str_replace($specimen_type_list[$specimen_type0], $specimen_type_list[$specimen_type[$k]], $clonedBeanName_New1);
              }
              /**09 March 2022 : #2218 */
              if ($clonedBean->other_test_type_c != "") {
                $bean_newName = str_replace($clonedBean->other_test_type_c, $test_type[$k], $bean_newName);
              } else {
                //$bean_newName = str_replace($test_type[0], $test_type[$k], $bean_newName);
                /**14 Sep 2022 : bug fix 468 : PMT re-naming samples incorrectly : Special character was causing issue in name replace 
                 * whenever there are more than one record with speciment type , so we have mapped that by their list
                 */

                $bean_newName = str_replace($ii_test_type_list[$test_type[0]], $ii_test_type_list[$test_type[$k]], $bean_newName);
              }
              /**Eoc 09 March 2022 : #2218 */
            }
          } else {
            // $GLOBALS['log']->fatal("in else condtion clonedBeanName line 191====>" . $clonedBeanName);
            if ($specimen_type[$k] == "Other") {
              $bean_newName = str_replace($specimen_type0, $clonedBean->other_type_c, $clonedBeanName);
              //$GLOBALS['log']->fatal("in else equal other condtion bean_newName line 194 ====>" . $bean_newName);
              $IIBean->other_type_c = $clonedBean->other_type_c;

              /**09 March 2022 : #2218 */
              if ($test_type[$k] == "Other") {
                $bean_newName = str_replace($test_type[0], $clonedBean->other_test_type_c, $bean_newName);
                //$GLOBALS['log']->fatal("in else equal other test type other condtion bean_newName line 200 ====>" . $bean_newName);       
              }
            } else {
              if ($clonedBean->other_type_c != "") {
                // $GLOBALS['log']->fatal("in other type c not blank condtion clonedBeanName line 205 ====>" . $clonedBeanName);
                $bean_newName = str_replace($clonedBean->other_type_c, $specimen_type[$k], $clonedBeanName);
                //$GLOBALS['log']->fatal("in other type c not blank condtion bean_newName line 207 ====>" . $bean_newName);
              } else {
                // $GLOBALS['log']->fatal("in other type c not blank condtion clonedBeanName line 209 ====>" . $clonedBeanName);
                $bean_newName = str_replace($specimen_type0, $specimen_type[$k], $clonedBeanName);
                // $GLOBALS['log']->fatal("in other type c not blank condtion bean_newName line 210 ====>" . $bean_newName);
              }

              /**09 March 2022 : #2218 */
              if ($clonedBean->other_test_type_c != "") {
                // $GLOBALS['log']->fatal("in other_test_type_c not blank condtion bean_newName line 216 ====>" . $bean_newName);
                $bean_newName = str_replace($clonedBean->other_test_type_c, $test_type[$k], $bean_newName);
                //$GLOBALS['log']->fatal("in other_test_type_c not blank condtion bean_newName line 218 ====>" . $bean_newName);
              } else {
                //$GLOBALS['log']->fatal("in other_test_type_c not blank else condtion bean_newName line 220 ====>" . $bean_newName);
                $bean_newName = str_replace($test_type[0], $test_type[$k], $bean_newName);
                //$GLOBALS['log']->fatal("in other_test_type_c not blank else condtion bean_newName line 222 ====>" . $bean_newName);
              }
              /**Eoc 09 March 2022 : #2218 */
            }
          }

          if ($test_type[0] == "Other" && $k == 0) {
            $IIBean->other_test_type_c = $clonedBean->other_test_type_c;
          } else {
            $IIBean->other_test_type_c = '';
          }

          $IIBean->name = $bean_newName;
          $IIBean->description = $clonedBean->description;

          $IIBean->type_2 = $specimen_type[$k];
          $IIBean->test_type_c = $test_type[$k];

          $IIBean->related_to_c = $clonedBean->related_to_c;
          $IIBean->category = $clonedBean->category;

          $IIBean->owner = $clonedBean->owner;
          $IIBean->lot_number_c = $clonedBean->lot_number_c;
          $IIBean->manufacturer_c = $clonedBean->manufacturer_c;
          $IIBean->external_barcode = $clonedBean->external_barcode;
          $IIBean->concentration_c = $clonedBean->concentration_c;
          $IIBean->u_units_id_c = $clonedBean->u_units_id_c;
          $IIBean->concentration_unit_c = $clonedBean->concentration_unit_c;
          $IIBean->sterilization = $clonedBean->sterilization;
          $IIBean->timepoint_type = $clonedBean->timepoint_type;
          $IIBean->timepoint_integer = $clonedBean->timepoint_integer;
          $IIBean->u_units_id1_c = $clonedBean->u_units_id1_c;
          $IIBean->timepoint_unit = $clonedBean->timepoint_unit;
          $IIBean->processing_method = $clonedBean->processing_method;
          $IIBean->collection_date_time = $clonedBean->collection_date_time;
          $IIBean->expiration_test_article = $clonedBean->expiration_test_article;
          $IIBean->expiration_date = $clonedBean->expiration_date;
          $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
          $IIBean->location_building = $clonedBean->location_building;
          $IIBean->rms_room_id_c = $clonedBean->rms_room_id_c;
          $IIBean->location_room = $clonedBean->location_room;
          $IIBean->equip_equipment_id_c = $clonedBean->equip_equipment_id_c;
          $IIBean->location_equipment = $clonedBean->location_equipment;
          $IIBean->location_shelf = $clonedBean->location_shelf;
          $IIBean->location_cabinet = $clonedBean->location_cabinet;
          $IIBean->contact_id_c = $clonedBean->contact_id_c;
          $IIBean->ship_to_contact = $clonedBean->ship_to_contact;
          $IIBean->account_id_c = $clonedBean->account_id_c;
          $IIBean->ship_to_company = $clonedBean->ship_to_company;
          $IIBean->ca_company_address_id_c = $clonedBean->ca_company_address_id_c;
          $IIBean->ship_to_address = $clonedBean->ship_to_address;
          $IIBean->retention_start_date = $clonedBean->retention_start_date;
          $IIBean->retention_interval_days = $clonedBean->retention_interval_days;
          $IIBean->planned_discard_date = $clonedBean->planned_discard_date;
          $IIBean->actual_discard_date = $clonedBean->actual_discard_date;
          $IIBean->quantity_c = $clonedBean->quantity_c;
          $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
          //$IIBean->type_2 = $clonedBean->type_2;
          $IIBean->product_name = $clonedBean->product_name;
          $IIBean->allowed_storage_conditions = $clonedBean->allowed_storage_conditions;
          $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
          $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
          $IIBean->storage_medium = $clonedBean->storage_medium;
          $IIBean->collection_date = $clonedBean->collection_date;
          $IIBean->received_date = $clonedBean->received_date;
          $IIBean->storage_condition = $clonedBean->storage_condition;
          $IIBean->location_type = $clonedBean->location_type;
          $IIBean->status = $clonedBean->status;
          $IIBean->inactive_c = $clonedBean->inactive_c;
          $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
          $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
          $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
          $IIBean->product_id_2_c = $clonedBean->product_id_2_c;
          $IIBean->pk_samples_c = 'NA';
          $IIBean->number_of_aliquots_c = $clonedBean->number_of_aliquots_c;
          $IIBean->timepoint_name_c = $clonedBean->timepoint_name_c;
          $IIBean->timepoint_end_integer_c = $clonedBean->timepoint_end_integer_c;
          $IIBean->location_rack_c = $clonedBean->location_rack_c;
          $IIBean->location_cubby_c = $clonedBean->location_cubby_c;
          $IIBean->location_bin_c = $clonedBean->location_bin_c;
          $IIBean->location_shelf = $clonedBean->location_shelf;
          $IIBean->location_cabinet = $clonedBean->location_cabinet;
          $IIBean->quantity_2_c = $clonedBean->quantity_2_c;

          $IIBean->fetched_row = null;
          $IIBean->unique_id_c = $this->generateRandomString(6);

          $IIBean->save();

          if (!empty($product_ids)) {
            $IIBean->m03_work_product_ii_inventory_item_1->add($product_ids[0]);
          }
          if (!empty($sale_id)) {
            $IIBean->m01_sales_ii_inventory_item_1->add($sale_id[0]);
          }
          if (!empty($work_product_id)) {
            foreach ($work_product_id as $wp_id) {
              $cusid = create_guid();
              $insert_query = "INSERT INTO m03_work_product_ii_inventory_item_2_c (id,m03_work_product_ii_inventory_item_2m03_work_product_ida,m03_work_product_ii_inventory_item_2ii_inventory_item_idb) VALUES ('" . $cusid . "','" . $wp_id . "','" . $IIBean->id . "')";
              $db->query($insert_query);
            }
          }
          if (!empty($test_system_id)) {
            $IIBean->anml_animals_ii_inventory_item_1->add($test_system_id[0]);
          }

          /*if (!empty($invoice_id)) {
            $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[$k]);
          }*/
        }
      }
    }
    return true;
  }

  public function specimen_type_ts_duplicates($api, $args)
  {
    global $db, $app_list_strings;
    $module = $args['module'];
    $recordId = $args['recordId'];
    $specimen_type = $args['specimen_type'];
    $specimen_type0 = $specimen_type[0];
    $test_type = $args['test_type'];
    $tsId = $args['tsId'];
    $tsName = $args['tsName'];
    $strTsId = explode(',', $tsId);
    $strTsName = explode(',', $tsName);

    $sqlPK = 'UPDATE ii_inventory_item_cstm SET pk_samples_c = "NA", specimen_type_1_c="", specimen_type_2_c="", specimen_type_3_c="", specimen_type_4_c="", specimen_type_5_c="", test_type_1_c="", test_type_2_c="", test_type_3_c="", test_type_4_c="", test_type_5_c="" WHERE id_c = "' . $recordId . '"';
    $db->query($sqlPK);

    $clonedBean = BeanFactory::retrieveBean($module, $recordId);

    $II_type_dom = $app_list_strings['inventory_item_type_list'];
    $II_Subtype_dom = $app_list_strings['inventory_item_subtype_list'];
    $II_product_dom = $app_list_strings['pro_subtype_list'];
    $II_specimen_dom = $app_list_strings['tissues_list'];

    /**14 Sep 2022 : bug fix 468 : PMT re-naming samples incorrectly : Special character was causing issue in name replace 
     * whenever there are more than one record with speciment type , so we have mapped that by their list
     */
    $ii_test_type_list = $app_list_strings['ii_test_type_list'];
    $specimen_type_list = $app_list_strings['specimen_type_list'];


    if (count($specimen_type) > 0) {

      $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
      $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
      $clonedBean->load_relationship("m03_work_product_ii_inventory_item_2");
      $clonedBean->load_relationship("anml_animals_ii_inventory_item_1");

      //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");
      //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();

      $product_ids = $clonedBean->m03_work_product_ii_inventory_item_1->get();
      $sale_id = $clonedBean->m01_sales_ii_inventory_item_1->get();
      $work_product_id = $clonedBean->m03_work_product_ii_inventory_item_2->get();
      $test_system_id1 = $clonedBean->anml_animals_ii_inventory_item_1->get();

      $clonedBeanName = substr($clonedBean->name, 0, -3);

      $arrPullTSId = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
      $arrPullUSDAId = array("Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig");

      $testSystemBean = BeanFactory::retrieveBean("ANML_Animals", $strTsId[0]);
      $species = $testSystemBean->species_2_c;

      $nameAppend = "";
      $clonedBeanArr = explode(" ", $clonedBeanName);
      if (in_array($species, $arrPullTSId)) {
        $nameAppend = $strTsName[0];
        $clonedBeanArr[1] = $nameAppend;
      } else if (in_array($species, $arrPullUSDAId)) {
        if ($testSystemBean->usda_id_c != "") {
          $nameAppend = $testSystemBean->usda_id_c;
        } else {
          $nameAppend = $strTsName[0];
        }
        $clonedBeanArr[1] = $nameAppend;
      } else {
        $nameAppend = $strTsName[0];
        $clonedBeanArr[1] = $nameAppend;
      }
      $clonedBeanName_New1 = implode(" ", $clonedBeanArr);
      //$GLOBALS['log']->fatal("in line 401 clonedBeanName_New1 ====>" . $clonedBeanName_New1);  

      for ($k = 1; $k < count($specimen_type); $k++) {
        $test_system_id = $strTsId[0];
        if ($specimen_type[$k] != "") {
          $IIBean = BeanFactory::newBean('II_Inventory_Item');

          $bid = create_guid();
          $IIBean->id = $bid;
          $IIBean->new_with_id = true;

          if ($specimen_type[$k] == "Other") {
            $bean_newName = str_replace($specimen_type0, $clonedBean->other_type_c, $clonedBeanName_New1);
            $IIBean->other_type_c = $clonedBean->other_type_c;
            /**09 March 2022 : #2218 */
            if ($test_type[$k] == "Other") {
              $bean_newName = str_replace($test_type[0], $clonedBean->other_test_type_c, $bean_newName);
            }
          } else {
            if ($clonedBean->other_type_c != "") {
              $bean_newName = str_replace($clonedBean->other_type_c, $specimen_type[$k], $clonedBeanName_New1);
            } else {
              // $bean_newName = str_replace($specimen_type0, $specimen_type[$k], $clonedBeanName_New1);
              /**14 Sep 2022 : bug fix 468 : PMT re-naming samples incorrectly : Special character was causing issue in name replace 
               * whenever there are more than one record with speciment type , so we have mapped that by their list
               */
              $bean_newName = str_replace($specimen_type_list[$specimen_type0], $specimen_type_list[$specimen_type[$k]], $clonedBeanName_New1);
            }
            /**09 March 2022 : #2218 */
            if ($clonedBean->other_test_type_c != "") {
              $bean_newName = str_replace($clonedBean->other_test_type_c, $test_type[$k], $clonedBeanName_New1);
              //$GLOBALS['log']->fatal("in line 428 bean_newName ====>" . $bean_newName); 
            } else {
              //$bean_newName = str_replace($test_type[0], $test_type[$k], $bean_newName);
              //$GLOBALS['log']->fatal("in line 430 bean_newName ====>" . $bean_newName);
              // $GLOBALS['log']->fatal("in line 431 test type 0 ====>" . $test_type[0]);
              // $GLOBALS['log']->fatal("in line 432 test type k ====>" . $test_type[$k]);
              /**14 Sep 2022 : bug fix 468 : PMT re-naming samples incorrectly : Special character was causing issue in name replace 
               * whenever there are more than one record with speciment type , so we have mapped that by their list
               */

              $bean_newName = str_replace($ii_test_type_list[$test_type[0]], $ii_test_type_list[$test_type[$k]], $bean_newName);
              // $GLOBALS['log']->fatal("in line 434 bean_newName ====>" . $bean_newName); 
            }
            /**Eoc 09 March 2022 : #2218 */
          }

          if ($test_type[0] == "Other" && $k == 0) {
            $IIBean->other_test_type_c = $clonedBean->other_test_type_c;
          } else {
            $IIBean->other_test_type_c = '';
          }
          //$GLOBALS['log']->fatal("in line 441 bean_newName ====>" . $bean_newName); 

          $IIBean->name = $bean_newName;
          $IIBean->description = $clonedBean->description;

          $IIBean->type_2 = $specimen_type[$k];
          $IIBean->test_type_c = $test_type[$k];

          $IIBean->related_to_c = $clonedBean->related_to_c;
          $IIBean->category = $clonedBean->category;

          $IIBean->owner = $clonedBean->owner;
          $IIBean->lot_number_c = $clonedBean->lot_number_c;
          $IIBean->manufacturer_c = $clonedBean->manufacturer_c;
          $IIBean->external_barcode = $clonedBean->external_barcode;
          $IIBean->concentration_c = $clonedBean->concentration_c;
          $IIBean->u_units_id_c = $clonedBean->u_units_id_c;
          $IIBean->concentration_unit_c = $clonedBean->concentration_unit_c;
          $IIBean->sterilization = $clonedBean->sterilization;
          $IIBean->timepoint_type = $clonedBean->timepoint_type;
          $IIBean->timepoint_integer = $clonedBean->timepoint_integer;
          $IIBean->u_units_id1_c = $clonedBean->u_units_id1_c;
          $IIBean->timepoint_unit = $clonedBean->timepoint_unit;
          $IIBean->processing_method = $clonedBean->processing_method;
          $IIBean->collection_date_time = $clonedBean->collection_date_time;
          $IIBean->expiration_test_article = $clonedBean->expiration_test_article;
          $IIBean->expiration_date = $clonedBean->expiration_date;
          $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
          $IIBean->location_building = $clonedBean->location_building;
          $IIBean->rms_room_id_c = $clonedBean->rms_room_id_c;
          $IIBean->location_room = $clonedBean->location_room;
          $IIBean->equip_equipment_id_c = $clonedBean->equip_equipment_id_c;
          $IIBean->location_equipment = $clonedBean->location_equipment;
          $IIBean->location_shelf = $clonedBean->location_shelf;
          $IIBean->location_cabinet = $clonedBean->location_cabinet;
          $IIBean->contact_id_c = $clonedBean->contact_id_c;
          $IIBean->ship_to_contact = $clonedBean->ship_to_contact;
          $IIBean->account_id_c = $clonedBean->account_id_c;
          $IIBean->ship_to_company = $clonedBean->ship_to_company;
          $IIBean->ca_company_address_id_c = $clonedBean->ca_company_address_id_c;
          $IIBean->ship_to_address = $clonedBean->ship_to_address;
          $IIBean->retention_start_date = $clonedBean->retention_start_date;
          $IIBean->retention_interval_days = $clonedBean->retention_interval_days;
          $IIBean->planned_discard_date = $clonedBean->planned_discard_date;
          $IIBean->actual_discard_date = $clonedBean->actual_discard_date;
          $IIBean->quantity_c = $clonedBean->quantity_c;
          $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
          //$IIBean->type_2 = $clonedBean->type_2;
          $IIBean->product_name = $clonedBean->product_name;
          $IIBean->allowed_storage_conditions = $clonedBean->allowed_storage_conditions;
          $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
          $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
          $IIBean->storage_medium = $clonedBean->storage_medium;
          $IIBean->collection_date = $clonedBean->collection_date;
          $IIBean->received_date = $clonedBean->received_date;
          $IIBean->storage_condition = $clonedBean->storage_condition;
          $IIBean->location_type = $clonedBean->location_type;
          $IIBean->status = $clonedBean->status;
          $IIBean->inactive_c = $clonedBean->inactive_c;
          $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
          $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
          //$IIBean->test_type_c = $clonedBean->test_type_c;
          $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
          $IIBean->product_id_2_c = $clonedBean->product_id_2_c;
          $IIBean->pk_samples_c = 'NA';
          $IIBean->number_of_aliquots_c = $clonedBean->number_of_aliquots_c;
          $IIBean->timepoint_name_c = $clonedBean->timepoint_name_c;
          $IIBean->timepoint_end_integer_c = $clonedBean->timepoint_end_integer_c;
          $IIBean->location_rack_c = $clonedBean->location_rack_c;
          $IIBean->location_cubby_c = $clonedBean->location_cubby_c;
          $IIBean->location_bin_c = $clonedBean->location_bin_c;
          $IIBean->location_shelf = $clonedBean->location_shelf;
          $IIBean->location_cabinet = $clonedBean->location_cabinet;
          $IIBean->quantity_2_c = $clonedBean->quantity_2_c;

          $IIBean->fetched_row = null;
          $IIBean->unique_id_c = $this->generateRandomString(6);

          $IIBean->save();

          if (!empty($product_ids)) {
            $IIBean->m03_work_product_ii_inventory_item_1->add($product_ids[0]);
          }
          if (!empty($sale_id)) {
            $IIBean->m01_sales_ii_inventory_item_1->add($sale_id[0]);
          }
          if (!empty($work_product_id)) {
            foreach ($work_product_id as $wp_id) {
              $cusid = create_guid();
              $insert_query = "INSERT INTO m03_work_product_ii_inventory_item_2_c (id,m03_work_product_ii_inventory_item_2m03_work_product_ida,m03_work_product_ii_inventory_item_2ii_inventory_item_idb) VALUES ('" . $cusid . "','" . $wp_id . "','" . $IIBean->id . "')";
              $db->query($insert_query);
            }
          }
          if (!empty($test_system_id)) {
            $IIBean->anml_animals_ii_inventory_item_1->add($test_system_id);
          }

          /*if (!empty($invoice_id)) {
              $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[$k]);
            }*/
        }
      }
    }


    for ($i = 1; $i < count($strTsName); $i++) {
      if (count($specimen_type) > 0) {

        $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
        $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
        $clonedBean->load_relationship("m03_work_product_ii_inventory_item_2");
        $clonedBean->load_relationship("anml_animals_ii_inventory_item_1");

        //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");
        //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();

        $product_ids = $clonedBean->m03_work_product_ii_inventory_item_1->get();
        $sale_id = $clonedBean->m01_sales_ii_inventory_item_1->get();
        $work_product_id = $clonedBean->m03_work_product_ii_inventory_item_2->get();
        $test_system_id1 = $clonedBean->anml_animals_ii_inventory_item_1->get();

        $clonedBeanName = substr($clonedBean->name, 0, -3);

        $arrPullTSId = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
        $arrPullUSDAId = array("Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig");

        $testSystemBean = BeanFactory::retrieveBean("ANML_Animals", $strTsId[$i]);
        $species = $testSystemBean->species_2_c;

        $nameAppend = "";
        $clonedBeanArr = explode(" ", $clonedBeanName);
        if (in_array($species, $arrPullTSId)) {
          $nameAppend = $strTsName[$i];
          $clonedBeanArr[1] = $nameAppend;
        } else if (in_array($species, $arrPullUSDAId)) {
          if ($testSystemBean->usda_id_c != "") {
            $nameAppend = $testSystemBean->usda_id_c;
          } else {
            $nameAppend = $strTsName[$i];
          }
          $clonedBeanArr[1] = $nameAppend;
        } else {
          $nameAppend = $strTsName[$i];
          $clonedBeanArr[1] = $nameAppend;
        }
        $clonedBeanName_New1 = implode(" ", $clonedBeanArr);

        for ($k = 0; $k < count($specimen_type); $k++) {
          $test_system_id = $strTsId[$i];
          if ($specimen_type[$k] != "") {
            $IIBean = BeanFactory::newBean('II_Inventory_Item');

            $bid = create_guid();
            $IIBean->id = $bid;
            $IIBean->new_with_id = true;
            if ($specimen_type[$k] == "Other") {
              // $GLOBALS['log']->fatal("in line 594 ====>" . $specimen_type[$k]);         
              $bean_newName = str_replace($specimen_type0, $clonedBean->other_type_c, $clonedBeanName_New1);
              $IIBean->other_type_c = $clonedBean->other_type_c;
              /**09 March 2022 : #2218 */
              if ($test_type[$k] == "Other") {
                //$GLOBALS['log']->fatal("in line 599 test_type ====>" . $test_type[$k]);  
                $bean_newName = str_replace($test_type[0], $clonedBean->other_test_type_c, $bean_newName);
                // $GLOBALS['log']->fatal("in line 601 bean_newName ====>" . $bean_newName);              
              }
            } else {
              //$GLOBALS['log']->fatal("in line 601 specimen_type ====>" .$specimen_type[$k]);     
              if ($clonedBean->other_type_c != "") {
                //$GLOBALS['log']->fatal("in line 606 other_type_c ====>" . $clonedBean->other_type_c); 
                $bean_newName = str_replace($clonedBean->other_type_c, $specimen_type[$k], $clonedBeanName_New1);
              } else {
                //$GLOBALS['log']->fatal("in line 609 other_type_c ====>" . $clonedBean->other_type_c); 
                $bean_newName = str_replace($specimen_type0, $specimen_type[$k], $clonedBeanName_New1);
                //$GLOBALS['log']->fatal("in line 61111 bean_newName ====>" . $bean_newName);
              }
              $GLOBALS['log']->fatal("in line 6133333 est_type ====>" . $test_type[$k]);
              /**09 March 2022 : #2218 */
              if ($clonedBean->other_test_type_c != "" && $test_type[$k] != 'Other') {
                // $GLOBALS['log']->fatal("in line 608 clonedBean->other_test_type_c ====>" . $clonedBean->other_test_type_c);
                // $GLOBALS['log']->fatal("in line 615 bean_newName ====>" . $bean_newName);
                $bean_newName = str_replace($clonedBean->other_test_type_c, $test_type[$k], $bean_newName);
                // $GLOBALS['log']->fatal("in line 610 bean_newName ====>" . $bean_newName);
              } else {
                //$GLOBALS['log']->fatal("in line 612 bean_newName ====>" . $bean_newName);
                $bean_newName = str_replace($test_type[0], $test_type[$k], $bean_newName);
                //$GLOBALS['log']->fatal("in line 614 bean_newName ====>" . $bean_newName);

              }
              /**Eoc 09 March 2022 : #2218 */
            }

            if ($test_type[0] == "Other" && $k == 0) {
              $IIBean->other_test_type_c = $clonedBean->other_test_type_c;
            } else {
              $IIBean->other_test_type_c = '';
            }

            $IIBean->name = $bean_newName;
            $IIBean->description = $clonedBean->description;

            $IIBean->type_2 = $specimen_type[$k];
            $IIBean->test_type_c = $test_type[$k];

            $IIBean->related_to_c = $clonedBean->related_to_c;
            $IIBean->category = $clonedBean->category;
            $IIBean->owner = $clonedBean->owner;
            $IIBean->lot_number_c = $clonedBean->lot_number_c;
            $IIBean->manufacturer_c = $clonedBean->manufacturer_c;
            $IIBean->external_barcode = $clonedBean->external_barcode;
            $IIBean->concentration_c = $clonedBean->concentration_c;
            $IIBean->u_units_id_c = $clonedBean->u_units_id_c;
            $IIBean->concentration_unit_c = $clonedBean->concentration_unit_c;
            $IIBean->sterilization = $clonedBean->sterilization;
            $IIBean->timepoint_type = $clonedBean->timepoint_type;
            $IIBean->timepoint_integer = $clonedBean->timepoint_integer;
            $IIBean->u_units_id1_c = $clonedBean->u_units_id1_c;
            $IIBean->timepoint_unit = $clonedBean->timepoint_unit;
            $IIBean->processing_method = $clonedBean->processing_method;
            $IIBean->collection_date_time = $clonedBean->collection_date_time;
            $IIBean->expiration_test_article = $clonedBean->expiration_test_article;
            $IIBean->expiration_date = $clonedBean->expiration_date;
            $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
            $IIBean->location_building = $clonedBean->location_building;
            $IIBean->rms_room_id_c = $clonedBean->rms_room_id_c;
            $IIBean->location_room = $clonedBean->location_room;
            $IIBean->equip_equipment_id_c = $clonedBean->equip_equipment_id_c;
            $IIBean->location_equipment = $clonedBean->location_equipment;
            $IIBean->location_shelf = $clonedBean->location_shelf;
            $IIBean->location_cabinet = $clonedBean->location_cabinet;
            $IIBean->contact_id_c = $clonedBean->contact_id_c;
            $IIBean->ship_to_contact = $clonedBean->ship_to_contact;
            $IIBean->account_id_c = $clonedBean->account_id_c;
            $IIBean->ship_to_company = $clonedBean->ship_to_company;
            $IIBean->ca_company_address_id_c = $clonedBean->ca_company_address_id_c;
            $IIBean->ship_to_address = $clonedBean->ship_to_address;
            $IIBean->retention_start_date = $clonedBean->retention_start_date;
            $IIBean->retention_interval_days = $clonedBean->retention_interval_days;
            $IIBean->planned_discard_date = $clonedBean->planned_discard_date;
            $IIBean->actual_discard_date = $clonedBean->actual_discard_date;
            $IIBean->quantity_c = $clonedBean->quantity_c;
            $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
            //$IIBean->type_2 = $clonedBean->type_2;
            $IIBean->product_name = $clonedBean->product_name;
            $IIBean->allowed_storage_conditions = $clonedBean->allowed_storage_conditions;
            $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
            $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
            $IIBean->storage_medium = $clonedBean->storage_medium;
            $IIBean->collection_date = $clonedBean->collection_date;
            $IIBean->received_date = $clonedBean->received_date;
            $IIBean->storage_condition = $clonedBean->storage_condition;
            $IIBean->location_type = $clonedBean->location_type;
            $IIBean->status = $clonedBean->status;
            $IIBean->inactive_c = $clonedBean->inactive_c;
            $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
            $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
            //$IIBean->test_type_c = $clonedBean->test_type_c;
            $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
            $IIBean->product_id_2_c = $clonedBean->product_id_2_c;
            $IIBean->pk_samples_c = 'NA';
            $IIBean->number_of_aliquots_c = $clonedBean->number_of_aliquots_c;
            $IIBean->timepoint_name_c = $clonedBean->timepoint_name_c;
            $IIBean->timepoint_end_integer_c = $clonedBean->timepoint_end_integer_c;
            $IIBean->location_rack_c = $clonedBean->location_rack_c;
            $IIBean->location_cubby_c = $clonedBean->location_cubby_c;
            $IIBean->location_bin_c = $clonedBean->location_bin_c;
            $IIBean->location_shelf = $clonedBean->location_shelf;
            $IIBean->location_cabinet = $clonedBean->location_cabinet;
            $IIBean->quantity_2_c = $clonedBean->quantity_2_c;

            $IIBean->fetched_row = null;
            $IIBean->unique_id_c = $this->generateRandomString(6);

            $IIBean->save();

            if (!empty($product_ids)) {
              $IIBean->m03_work_product_ii_inventory_item_1->add($product_ids[0]);
            }
            if (!empty($sale_id)) {
              $IIBean->m01_sales_ii_inventory_item_1->add($sale_id[0]);
            }
            if (!empty($work_product_id)) {
              foreach ($work_product_id as $wp_id) {
                $cusid = create_guid();
                $insert_query = "INSERT INTO m03_work_product_ii_inventory_item_2_c (id,m03_work_product_ii_inventory_item_2m03_work_product_ida,m03_work_product_ii_inventory_item_2ii_inventory_item_idb) VALUES ('" . $cusid . "','" . $wp_id . "','" . $IIBean->id . "')";
                $db->query($insert_query);
              }
            }
            if (!empty($test_system_id)) {
              $IIBean->anml_animals_ii_inventory_item_1->add($test_system_id);
            }

            /*if (!empty($invoice_id)) {
              $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[$k]);
            }*/
          }
        }
      }
    }
    return true;
  }

  public function create_ts_duplicates($api, $args)
  {
    global $app_list_strings;

    $module = $args['module'];
    $recordId = $args['record'];
    $tsId = $args['tsId'];
    $tsName = $args['tsName'];
    if ($args['subtype'] != "") {
      $subtypeStr = $args['subtype'];
    } else {
      $subtypeStr = 0;
    }

    $clonedBean = BeanFactory::retrieveBean($module, $recordId);

    $strTsId = explode(',', $tsId);
    $strTsName = explode(',', $tsName);
    $subtype = explode(',', $subtypeStr);

    $clonedBeanName = substr($clonedBean->name, 0, -3);

    $II_type_dom = $app_list_strings['inventory_item_type_list'];
    $II_specimen_dom = $app_list_strings['tissues_list'];

    if (count($subtype) > 0 && $subtypeStr != "0") {

      for ($k = 1; $k < count($subtype); $k++) {

        $arrPullTSId1 = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
        $arrPullUSDAId1 = array("Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig");

        $testSystemBean1 = BeanFactory::retrieveBean("ANML_Animals", $strTsId[0]);
        $species1 = $testSystemBean1->species_2_c;

        $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
        $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
        $clonedBean->load_relationship("m03_work_product_ii_inventory_item_2");
        $clonedBean->load_relationship("anml_animals_ii_inventory_item_1");
        //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");

        $product_ids = $clonedBean->m03_work_product_ii_inventory_item_1->get();
        $sale_id = $clonedBean->m01_sales_ii_inventory_item_1->get();
        $work_product_id = $clonedBean->m03_work_product_ii_inventory_item_2->get();
        //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();

        $test_system_id1 = $strTsId[0];

        $nameAppend1 = "";
        $clonedBeanArr1 = explode(" ", $clonedBeanName);

        if (in_array($species1, $arrPullTSId1)) {
          $nameAppend1 = $strTsName[0];
          $clonedBeanArr1[1] = $nameAppend1;
        } else if (in_array($species1, $arrPullUSDAId1)) {
          if ($testSystemBean1->usda_id_c != "") {
            $nameAppend1 = $testSystemBean1->usda_id_c;
          } else {
            $nameAppend1 = $strTsName[0];
          }
          $clonedBeanArr1[1] = $nameAppend1;
        } else {
          $nameAppend1 = $strTsName[0];
          $clonedBeanArr1[1] = $nameAppend1;
        }

        $clonedBeanName_New1 = implode(" ", $clonedBeanArr1);

        $clonedBean_newName = str_replace($II_specimen_dom[$subtype[0]], $II_specimen_dom[$subtype[$k]], $clonedBeanName_New1);

        $IIBean = BeanFactory::newBean('II_Inventory_Item');

        $bid = create_guid();
        $IIBean->id = $bid;
        $IIBean->new_with_id = true;

        $IIBean->name = $clonedBean_newName;
        $IIBean->fetched_row = null;

        $IIBean->description = $clonedBean->description;
        $IIBean->related_to_c = $clonedBean->related_to_c;
        $IIBean->category = $clonedBean->category;
        $IIBean->owner = $clonedBean->owner;
        $IIBean->lot_number_c = $clonedBean->lot_number_c;
        $IIBean->manufacturer_c = $clonedBean->manufacturer_c;
        $IIBean->external_barcode = $clonedBean->external_barcode;
        //$IIBean->internal_barcode = $clonedBean->internal_barcode;
        $IIBean->concentration_c = $clonedBean->concentration_c;
        $IIBean->u_units_id_c = $clonedBean->u_units_id_c;
        $IIBean->concentration_unit_c = $clonedBean->concentration_unit_c;
        $IIBean->sterilization = $clonedBean->sterilization;
        $IIBean->timepoint_type = $clonedBean->timepoint_type;
        $IIBean->timepoint_integer = $clonedBean->timepoint_integer;
        $IIBean->u_units_id1_c = $clonedBean->u_units_id1_c;
        $IIBean->timepoint_unit = $clonedBean->timepoint_unit;
        $IIBean->processing_method = $clonedBean->processing_method;
        $IIBean->collection_date_time = $clonedBean->collection_date_time;
        $IIBean->expiration_test_article = $clonedBean->expiration_test_article;
        $IIBean->expiration_date = $clonedBean->expiration_date;
        $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
        $IIBean->location_building = $clonedBean->location_building;
        $IIBean->rms_room_id_c = $clonedBean->rms_room_id_c;
        $IIBean->location_room = $clonedBean->location_room;
        $IIBean->equip_equipment_id_c = $clonedBean->equip_equipment_id_c;
        $IIBean->location_equipment = $clonedBean->location_equipment;
        $IIBean->location_shelf = $clonedBean->location_shelf;
        $IIBean->location_cabinet = $clonedBean->location_cabinet;
        $IIBean->contact_id_c = $clonedBean->contact_id_c;
        $IIBean->ship_to_contact = $clonedBean->ship_to_contact;
        $IIBean->account_id_c = $clonedBean->account_id_c;
        $IIBean->ship_to_company = $clonedBean->ship_to_company;
        $IIBean->ca_company_address_id_c = $clonedBean->ca_company_address_id_c;
        $IIBean->ship_to_address = $clonedBean->ship_to_address;
        $IIBean->retention_start_date = $clonedBean->retention_start_date;
        $IIBean->retention_interval_days = $clonedBean->retention_interval_days;
        $IIBean->planned_discard_date = $clonedBean->planned_discard_date;
        $IIBean->actual_discard_date = $clonedBean->actual_discard_date;
        $IIBean->quantity_c = $clonedBean->quantity_c;
        $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
        $IIBean->type_2 = $clonedBean->type_2;
        $IIBean->product_name = $clonedBean->product_name;
        $IIBean->allowed_storage_conditions = $clonedBean->allowed_storage_conditions;
        $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
        $IIBean->subtype = $subtype[$k];
        $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
        $IIBean->storage_medium = $clonedBean->storage_medium;
        $IIBean->collection_date = $clonedBean->collection_date;
        $IIBean->received_date = $clonedBean->received_date;
        $IIBean->storage_condition = $clonedBean->storage_condition;
        $IIBean->location_type = $clonedBean->location_type;
        $IIBean->status = $clonedBean->status;
        $IIBean->inactive_c = $clonedBean->inactive_c;
        $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
        $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
        $IIBean->test_type_c = $clonedBean->test_type_c;
        $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
        $IIBean->product_id_2_c = $clonedBean->product_id_2_c;
        $IIBean->pk_samples_c = $clonedBean->pk_samples_c;
        $IIBean->number_of_aliquots_c = $clonedBean->number_of_aliquots_c;
        $IIBean->other_test_type_c = $clonedBean->other_test_type_c;
        $IIBean->timepoint_name_c = $clonedBean->timepoint_name_c;
        $IIBean->timepoint_end_integer_c = $clonedBean->timepoint_end_integer_c;
        $IIBean->location_rack_c = $clonedBean->location_rack_c;
        $IIBean->location_cubby_c = $clonedBean->location_cubby_c;
        $IIBean->location_bin_c = $clonedBean->location_bin_c;
        $IIBean->location_shelf = $clonedBean->location_shelf;
        $IIBean->location_cabinet = $clonedBean->location_cabinet;
        $IIBean->quantity_2_c = $clonedBean->quantity_2_c;

        $IIBean->unique_id_c = $this->generateRandomString(6);
        $IIBean->save();

        if (!empty($product_ids)) {
          $IIBean->m03_work_product_ii_inventory_item_1->add($product_ids[0]);
        }
        if (!empty($sale_id)) {
          $IIBean->m01_sales_ii_inventory_item_1->add($sale_id[0]);
        }
        if (!empty($work_product_id)) {
          $IIBean->m03_work_product_ii_inventory_item_2->add($work_product_id[0]);
        }
        if (!empty($test_system_id1)) {
          $IIBean->anml_animals_ii_inventory_item_1->add($test_system_id1);
        }
        /*if (!empty($invoice_id)) {
          $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[0]);
        }*/
      }
    }

    for ($i = 1; $i < count($strTsName); $i++) {

      $arrPullTSId = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
      $arrPullUSDAId = array("Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig");

      $testSystemBean = BeanFactory::retrieveBean("ANML_Animals", $strTsId[$i]);
      $species = $testSystemBean->species_2_c;

      $nameAppend = "";
      $clonedBeanArr = explode(" ", $clonedBeanName);
      if (in_array($species, $arrPullTSId)) {
        $nameAppend = $strTsName[$i];
        $clonedBeanArr[1] = $nameAppend;
      } else if (in_array($species, $arrPullUSDAId)) {
        if ($testSystemBean->usda_id_c != "") {
          $nameAppend = $testSystemBean->usda_id_c;
        } else {
          $nameAppend = $strTsName[$i];
        }
        $clonedBeanArr[1] = $nameAppend;
      } else {
        $nameAppend = $strTsName[$i];
        $clonedBeanArr[1] = $nameAppend;
      }

      $clonedBeanName_New = implode(" ", $clonedBeanArr);

      if (count($subtype) > 0 && $subtypeStr != "0") {
        $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
        $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
        $clonedBean->load_relationship("m03_work_product_ii_inventory_item_2");
        $clonedBean->load_relationship("anml_animals_ii_inventory_item_1");
        //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");

        for ($j = 0; $j < count($subtype); $j++) {
          $clonedBean_newName = str_replace($II_specimen_dom[$subtype[0]], $II_specimen_dom[$subtype[$j]], $clonedBeanName_New);

          $product_ids = $clonedBean->m03_work_product_ii_inventory_item_1->get();
          $sale_id = $clonedBean->m01_sales_ii_inventory_item_1->get();
          $work_product_id = $clonedBean->m03_work_product_ii_inventory_item_2->get();
          //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();
          $test_system_id = $strTsId[$i];

          $IIBean = BeanFactory::newBean('II_Inventory_Item');

          $bid = create_guid();
          $IIBean->id = $bid;
          $IIBean->new_with_id = true;

          $IIBean->name = $clonedBean_newName;
          $IIBean->fetched_row = null;

          $IIBean->description = $clonedBean->description;
          $IIBean->related_to_c = $clonedBean->related_to_c;
          $IIBean->category = $clonedBean->category;
          $IIBean->owner = $clonedBean->owner;
          $IIBean->lot_number_c = $clonedBean->lot_number_c;
          $IIBean->manufacturer_c = $clonedBean->manufacturer_c;
          $IIBean->external_barcode = $clonedBean->external_barcode;
          //$IIBean->internal_barcode = $clonedBean->internal_barcode;
          $IIBean->concentration_c = $clonedBean->concentration_c;
          $IIBean->u_units_id_c = $clonedBean->u_units_id_c;
          $IIBean->concentration_unit_c = $clonedBean->concentration_unit_c;
          $IIBean->sterilization = $clonedBean->sterilization;
          $IIBean->timepoint_type = $clonedBean->timepoint_type;
          $IIBean->timepoint_integer = $clonedBean->timepoint_integer;
          $IIBean->u_units_id1_c = $clonedBean->u_units_id1_c;
          $IIBean->timepoint_unit = $clonedBean->timepoint_unit;
          $IIBean->processing_method = $clonedBean->processing_method;
          $IIBean->collection_date_time = $clonedBean->collection_date_time;
          $IIBean->expiration_test_article = $clonedBean->expiration_test_article;
          $IIBean->expiration_date = $clonedBean->expiration_date;
          $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
          $IIBean->location_building = $clonedBean->location_building;
          $IIBean->rms_room_id_c = $clonedBean->rms_room_id_c;
          $IIBean->location_room = $clonedBean->location_room;
          $IIBean->equip_equipment_id_c = $clonedBean->equip_equipment_id_c;
          $IIBean->location_equipment = $clonedBean->location_equipment;
          $IIBean->location_shelf = $clonedBean->location_shelf;
          $IIBean->location_cabinet = $clonedBean->location_cabinet;
          $IIBean->contact_id_c = $clonedBean->contact_id_c;
          $IIBean->ship_to_contact = $clonedBean->ship_to_contact;
          $IIBean->account_id_c = $clonedBean->account_id_c;
          $IIBean->ship_to_company = $clonedBean->ship_to_company;
          $IIBean->ca_company_address_id_c = $clonedBean->ca_company_address_id_c;
          $IIBean->ship_to_address = $clonedBean->ship_to_address;
          $IIBean->retention_start_date = $clonedBean->retention_start_date;
          $IIBean->retention_interval_days = $clonedBean->retention_interval_days;
          $IIBean->planned_discard_date = $clonedBean->planned_discard_date;
          $IIBean->actual_discard_date = $clonedBean->actual_discard_date;
          $IIBean->quantity_c = $clonedBean->quantity_c;
          $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
          $IIBean->type_2 = $clonedBean->type_2;
          $IIBean->product_name = $clonedBean->product_name;
          $IIBean->allowed_storage_conditions = $clonedBean->allowed_storage_conditions;
          $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
          $IIBean->subtype = $subtype[$j];
          $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
          $IIBean->storage_medium = $clonedBean->storage_medium;
          $IIBean->collection_date = $clonedBean->collection_date;
          $IIBean->received_date = $clonedBean->received_date;
          $IIBean->storage_condition = $clonedBean->storage_condition;
          $IIBean->location_type = $clonedBean->location_type;
          $IIBean->status = $clonedBean->status;
          $IIBean->inactive_c = $clonedBean->inactive_c;
          $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
          $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
          $IIBean->test_type_c = $clonedBean->test_type_c;
          $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
          $IIBean->product_id_2_c = $clonedBean->product_id_2_c;
          $IIBean->pk_samples_c = $clonedBean->pk_samples_c;
          $IIBean->number_of_aliquots_c = $clonedBean->number_of_aliquots_c;
          $IIBean->other_test_type_c = $clonedBean->other_test_type_c;
          $IIBean->timepoint_name_c = $clonedBean->timepoint_name_c;
          $IIBean->timepoint_end_integer_c = $clonedBean->timepoint_end_integer_c;
          $IIBean->location_rack_c = $clonedBean->location_rack_c;
          $IIBean->location_cubby_c = $clonedBean->location_cubby_c;
          $IIBean->location_bin_c = $clonedBean->location_bin_c;
          $IIBean->location_shelf = $clonedBean->location_shelf;
          $IIBean->location_cabinet = $clonedBean->location_cabinet;
          $IIBean->quantity_2_c = $clonedBean->quantity_2_c;

          $IIBean->unique_id_c = $this->generateRandomString(6);
          $IIBean->save();
          if (!empty($product_ids)) {
            $IIBean->m03_work_product_ii_inventory_item_1->add($product_ids[0]);
          }
          if (!empty($sale_id)) {
            $IIBean->m01_sales_ii_inventory_item_1->add($sale_id[0]);
          }
          if (!empty($work_product_id)) {
            $IIBean->m03_work_product_ii_inventory_item_2->add($work_product_id[0]);
          }
          if (!empty($test_system_id)) {
            $IIBean->anml_animals_ii_inventory_item_1->add($test_system_id);
          }
          /*if (!empty($invoice_id)) {
            $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[0]);
          }*/
        }
      } else {
        $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
        $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
        $clonedBean->load_relationship("m03_work_product_ii_inventory_item_2");
        $clonedBean->load_relationship("anml_animals_ii_inventory_item_1");
        //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");


        $product_ids = $clonedBean->m03_work_product_ii_inventory_item_1->get();
        $sale_id = $clonedBean->m01_sales_ii_inventory_item_1->get();
        $work_product_id = $clonedBean->m03_work_product_ii_inventory_item_2->get();
        //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();

        $test_system_id = $strTsId[$i];


        $IIBean = BeanFactory::newBean('II_Inventory_Item');

        $bid = create_guid();
        $IIBean->id = $bid;
        $IIBean->new_with_id = true;

        $IIBean->name = $clonedBeanName_New;
        $IIBean->fetched_row = null;

        $IIBean->description = $clonedBean->description;
        $IIBean->related_to_c = $clonedBean->related_to_c;
        $IIBean->category = $clonedBean->category;
        $IIBean->owner = $clonedBean->owner;
        $IIBean->lot_number_c = $clonedBean->lot_number_c;
        $IIBean->manufacturer_c = $clonedBean->manufacturer_c;
        $IIBean->external_barcode = $clonedBean->external_barcode;
        //$IIBean->internal_barcode = $clonedBean->internal_barcode;
        $IIBean->concentration_c = $clonedBean->concentration_c;
        $IIBean->u_units_id_c = $clonedBean->u_units_id_c;
        $IIBean->concentration_unit_c = $clonedBean->concentration_unit_c;
        $IIBean->sterilization = $clonedBean->sterilization;
        $IIBean->timepoint_type = $clonedBean->timepoint_type;
        $IIBean->timepoint_integer = $clonedBean->timepoint_integer;
        $IIBean->u_units_id1_c = $clonedBean->u_units_id1_c;
        $IIBean->timepoint_unit = $clonedBean->timepoint_unit;
        $IIBean->processing_method = $clonedBean->processing_method;
        $IIBean->collection_date_time = $clonedBean->collection_date_time;
        $IIBean->expiration_test_article = $clonedBean->expiration_test_article;
        $IIBean->expiration_date = $clonedBean->expiration_date;
        $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
        $IIBean->location_building = $clonedBean->location_building;
        $IIBean->rms_room_id_c = $clonedBean->rms_room_id_c;
        $IIBean->location_room = $clonedBean->location_room;
        $IIBean->equip_equipment_id_c = $clonedBean->equip_equipment_id_c;
        $IIBean->location_equipment = $clonedBean->location_equipment;
        $IIBean->location_shelf = $clonedBean->location_shelf;
        $IIBean->location_cabinet = $clonedBean->location_cabinet;
        $IIBean->contact_id_c = $clonedBean->contact_id_c;
        $IIBean->ship_to_contact = $clonedBean->ship_to_contact;
        $IIBean->account_id_c = $clonedBean->account_id_c;
        $IIBean->ship_to_company = $clonedBean->ship_to_company;
        $IIBean->ca_company_address_id_c = $clonedBean->ca_company_address_id_c;
        $IIBean->ship_to_address = $clonedBean->ship_to_address;
        $IIBean->retention_start_date = $clonedBean->retention_start_date;
        $IIBean->retention_interval_days = $clonedBean->retention_interval_days;
        $IIBean->planned_discard_date = $clonedBean->planned_discard_date;
        $IIBean->actual_discard_date = $clonedBean->actual_discard_date;
        $IIBean->quantity_c = $clonedBean->quantity_c;
        $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
        $IIBean->type_2 = $clonedBean->type_2;
        $IIBean->product_name = $clonedBean->product_name;
        $IIBean->allowed_storage_conditions = $clonedBean->allowed_storage_conditions;
        $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
        $IIBean->subtype = $clonedBean->subtype;
        $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
        $IIBean->storage_medium = $clonedBean->storage_medium;
        $IIBean->collection_date = $clonedBean->collection_date;
        $IIBean->received_date = $clonedBean->received_date;
        $IIBean->storage_condition = $clonedBean->storage_condition;
        $IIBean->location_type = $clonedBean->location_type;
        $IIBean->status = $clonedBean->status;
        $IIBean->inactive_c = $clonedBean->inactive_c;
        $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
        $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
        $IIBean->test_type_c = $clonedBean->test_type_c;
        $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
        $IIBean->product_id_2_c = $clonedBean->product_id_2_c;
        $IIBean->pk_samples_c = $clonedBean->pk_samples_c;
        $IIBean->number_of_aliquots_c = $clonedBean->number_of_aliquots_c;
        $IIBean->other_test_type_c = $clonedBean->other_test_type_c;
        $IIBean->timepoint_name_c = $clonedBean->timepoint_name_c;
        $IIBean->timepoint_end_integer_c = $clonedBean->timepoint_end_integer_c;
        $IIBean->location_rack_c = $clonedBean->location_rack_c;
        $IIBean->location_cubby_c = $clonedBean->location_cubby_c;
        $IIBean->location_bin_c = $clonedBean->location_bin_c;
        $IIBean->location_shelf = $clonedBean->location_shelf;
        $IIBean->location_cabinet = $clonedBean->location_cabinet;
        $IIBean->quantity_2_c = $clonedBean->quantity_2_c;

        $IIBean->unique_id_c = $this->generateRandomString(6);
        $IIBean->save();
        if (!empty($product_ids)) {
          $IIBean->m03_work_product_ii_inventory_item_1->add($product_ids[0]);
        }
        if (!empty($sale_id)) {
          $IIBean->m01_sales_ii_inventory_item_1->add($sale_id[0]);
        }
        if (!empty($work_product_id)) {
          $IIBean->m03_work_product_ii_inventory_item_2->add($work_product_id[0]);
        }
        if (!empty($test_system_id)) {
          $IIBean->anml_animals_ii_inventory_item_1->add($test_system_id);
        }
        /*if (!empty($invoice_id)) {
          $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[0]);
        }*/
      }
    }
    //$dup = new InventoryItemCreateDuplicatesTsUtils($module, $recordId, $tsId, $tsName, $subtype);
    //$dup->duplicate();

    return true;
  }

  public function create_poi_duplicates($api, $args)
  {

    global $db, $app_list_strings;
    $II_product_category_list_dom = $app_list_strings['product_category_list'];
    $II_pro_subtype_list_dom = $app_list_strings['pro_subtype_list'];
    $module = $args['module'];
    $recordId = $args['record'];

    $poiId = $args['poiId'];
    $poiName = $args['poiName'];

    $poiType2 = $args['poiType2'];
    $poiSubtype = $args['poiSubtype'];
    $poisalesId = $args['poisalesId'];
    $poiwpId = $args['poiwpId'];
    $productIdsArray = $args['POIProductIdData'];


    $strpoiName = explode(',', $poiName);
    $strpoiType2 = explode(',', $poiType2);
    $strpoiSubtype = explode(',', $poiSubtype);
    $strpoisalesId = explode(',', $poisalesId);
    $strpoiwpId = explode(',', $poiwpId);
    $strpoiId = explode(',', $poiId);
    $productIds = explode(',', $productIdsArray);

    $clonedBean = BeanFactory::retrieveBean($module, $recordId);

    $clonedBeanName = substr($clonedBean->name, 0, -3);

    $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
    $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
    $clonedBean->load_relationship("poi_purchase_order_item_ii_inventory_item_1");

    //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");
    //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();

    $clonedBean->poi_purchase_order_item_ii_inventory_item_1->add($strpoiId[0]);

    if (count($strpoiName) > 1) {
      for ($i = 1; $i < count($strpoiName); $i++) {

        $prductBean = BeanFactory::retrieveBean("Prod_Product", $productIds[$i]);

        $Bean_product_name = $prductBean->product_name;
        $Bean_product_id_2_c = $prductBean->product_id_2;
        $Bean_manufacturer_c = $prductBean->manufacturer;
        $Bean_external_barcode = $prductBean->external_barcode;
        $Bean_concentration_c = $prductBean->concentration;
        $Bean_allowed_storage_conditions = $prductBean->allowed_storage;
        $Bean_u_units_id_c = $prductBean->u_units_id_c;
        $Bean_concentration_unit_c = $prductBean->concentration_unit;

        $WpName = "";
        $SaleName = "";
        if ($strpoisalesId[$i] != '' && $strpoisalesId[$i] == '0') {
        } else {
          $sqlSales = 'SELECT name FROM m01_sales WHERE id = "' . $strpoisalesId[$i] . '"';
          $resultSales = $db->query($sqlSales);
          if ($resultSales->num_rows > 0) {
            $rowSales = $db->fetchByAssoc($resultSales);
            $SaleName = $rowSales['name'];
          }
        }

        if ($strpoiwpId[$i] != '' && $strpoiwpId[$i] == '0') {
        } else {
          $sqlWp = 'SELECT name FROM m03_work_product WHERE id = "' . $strpoiwpId[$i] . '"';
          $resultWp = $db->query($sqlWp);
          if ($resultWp->num_rows > 0) {
            $rowWp = $db->fetchByAssoc($resultWp);
            $WpName = $rowWp['name'];
          }
        }

        //$ProductName = " " . $clonedBean->product_name;
        $ProductName = " " . $Bean_product_name;

        $tp = $strpoiType2[$i];
        if ($strpoiType2[$i] != '' && $strpoiType2[$i] == '0') {
        } else {
          $Type_2 = $II_product_category_list_dom[$tp];
          $TypeName = " " . $Type_2;
        }

        $Subtype = $strpoiSubtype[$i];

        if ($Subtype != '' && $Subtype == '0') {
          $SubtypeName = "";
          $Subtype_Name = "";
        } else {
          $SubType_2 = $II_pro_subtype_list_dom[$Subtype];
          $SubtypeName = " " . $SubType_2;
          $Subtype_Name = $Subtype;
        }

        $BeanName = $SaleName . $WpName . $ProductName . $TypeName . $SubtypeName;

        $IIBean = BeanFactory::newBean('II_Inventory_Item');

        $bid = create_guid();
        $IIBean->id = $bid;
        $IIBean->new_with_id = true;
        $IIBean->fetched_row = null;

        $IIBean->name = $BeanName;
        $IIBean->description = $clonedBean->description;
        $IIBean->related_to_c = $clonedBean->related_to_c;
        $IIBean->category = $clonedBean->category;
        $IIBean->owner = $clonedBean->owner;
        $IIBean->lot_number_c = $clonedBean->lot_number_c;
        $IIBean->manufacturer_c = $Bean_manufacturer_c;
        $IIBean->external_barcode = $Bean_external_barcode;
        //$IIBean->internal_barcode = $clonedBean->internal_barcode;
        $IIBean->concentration_c = $Bean_concentration_c;
        $IIBean->u_units_id_c = $Bean_u_units_id_c;
        $IIBean->concentration_unit_c = $Bean_concentration_unit_c;
        $IIBean->sterilization = $clonedBean->sterilization;
        $IIBean->timepoint_type = $clonedBean->timepoint_type;
        $IIBean->timepoint_integer = $clonedBean->timepoint_integer;
        $IIBean->u_units_id1_c = $clonedBean->u_units_id1_c;
        $IIBean->timepoint_unit = $clonedBean->timepoint_unit;
        $IIBean->processing_method = $clonedBean->processing_method;
        $IIBean->collection_date_time = $clonedBean->collection_date_time;
        $IIBean->expiration_test_article = $clonedBean->expiration_test_article;
        $IIBean->expiration_date = $clonedBean->expiration_date;
        $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
        $IIBean->location_building = $clonedBean->location_building;
        $IIBean->rms_room_id_c = $clonedBean->rms_room_id_c;
        $IIBean->location_room = $clonedBean->location_room;
        $IIBean->equip_equipment_id_c = $clonedBean->equip_equipment_id_c;
        $IIBean->location_equipment = $clonedBean->location_equipment;
        $IIBean->location_shelf = $clonedBean->location_shelf;
        $IIBean->location_cabinet = $clonedBean->location_cabinet;
        $IIBean->contact_id_c = $clonedBean->contact_id_c;
        $IIBean->ship_to_contact = $clonedBean->ship_to_contact;
        $IIBean->account_id_c = $clonedBean->account_id_c;
        $IIBean->ship_to_company = $clonedBean->ship_to_company;
        $IIBean->ca_company_address_id_c = $clonedBean->ca_company_address_id_c;
        $IIBean->ship_to_address = $clonedBean->ship_to_address;
        $IIBean->retention_start_date = $clonedBean->retention_start_date;
        $IIBean->retention_interval_days = $clonedBean->retention_interval_days;
        $IIBean->planned_discard_date = $clonedBean->planned_discard_date;
        $IIBean->actual_discard_date = $clonedBean->actual_discard_date;
        $IIBean->quantity_c = $clonedBean->quantity_c;
        $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
        $IIBean->type_2 = $strpoiType2[$i];
        $IIBean->product_name = $Bean_product_name;
        $IIBean->allowed_storage_conditions = $Bean_allowed_storage_conditions;
        $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
        $IIBean->subtype = $Subtype_Name;
        $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
        $IIBean->storage_medium = $clonedBean->storage_medium;
        $IIBean->collection_date = $clonedBean->collection_date;
        $IIBean->received_date = $clonedBean->received_date;
        $IIBean->storage_condition = $clonedBean->storage_condition;
        $IIBean->location_type = $clonedBean->location_type;
        $IIBean->status = $clonedBean->status;
        $IIBean->inactive_c = $clonedBean->inactive_c;
        $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
        $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
        $IIBean->test_type_c = $clonedBean->test_type_c;
        $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
        $IIBean->product_id_2_c = $Bean_product_id_2_c;
        $IIBean->pk_samples_c = $clonedBean->pk_samples_c;
        $IIBean->number_of_aliquots_c = $clonedBean->number_of_aliquots_c;
        $IIBean->other_test_type_c = $clonedBean->other_test_type_c;
        $IIBean->timepoint_name_c = $clonedBean->timepoint_name_c;
        $IIBean->timepoint_end_integer_c = $clonedBean->timepoint_end_integer_c;
        $IIBean->location_rack_c = $clonedBean->location_rack_c;
        $IIBean->location_cubby_c = $clonedBean->location_cubby_c;
        $IIBean->location_bin_c = $clonedBean->location_bin_c;
        $IIBean->location_shelf = $clonedBean->location_shelf;
        $IIBean->location_cabinet = $clonedBean->location_cabinet;
        $IIBean->quantity_2_c = $clonedBean->quantity_2_c;

        $IIBean->unique_id_c = $this->generateRandomString(6);
        $IIBean->save();

        if (!empty($strpoiId)) {
          $IIBean->poi_purchase_order_item_ii_inventory_item_1->add($strpoiId[$i]);
        }

        if ($strpoisalesId[$i] != '' && $strpoisalesId[$i] == '0') {
        } else {
          $IIBean->m01_sales_ii_inventory_item_1->add($strpoisalesId[$i]);
        }

        if ($strpoiwpId[$i] != '' && $strpoiwpId[$i] == '0') {
        } else {
          $IIBean->m03_work_product_ii_inventory_item_1->add($strpoiwpId[$i]);
        }
        /*if (!empty($invoice_id)) {
          $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[$i]);
        }*/
      }
    } else {
      if (is_numeric($clonedBean->quantity_c) && intval($clonedBean->quantity_c) > 0) {
        $quantity = intval($clonedBean->quantity_c);
        for ($i = 0; $i < $quantity - 1; $i++) {

          $IIBean = BeanFactory::newBean('II_Inventory_Item');

          $bid = create_guid();
          $IIBean->id = $bid;
          $IIBean->new_with_id = true;
          $IIBean->fetched_row = null;

          $IIBean->name = substr($clonedBean->name, 0, -3);
          $IIBean->description = $clonedBean->description;
          $IIBean->related_to_c = $clonedBean->related_to_c;
          $IIBean->category = $clonedBean->category;
          $IIBean->owner = $clonedBean->owner;
          $IIBean->lot_number_c = $clonedBean->lot_number_c;
          $IIBean->manufacturer_c = $clonedBean->manufacturer_c;
          $IIBean->external_barcode = $clonedBean->external_barcode;
          //$IIBean->internal_barcode = $clonedBean->internal_barcode;
          $IIBean->concentration_c = $clonedBean->concentration_c;
          $IIBean->u_units_id_c = $clonedBean->u_units_id_c;
          $IIBean->concentration_unit_c = $clonedBean->concentration_unit_c;
          $IIBean->sterilization = $clonedBean->sterilization;
          $IIBean->timepoint_type = $clonedBean->timepoint_type;
          $IIBean->timepoint_integer = $clonedBean->timepoint_integer;
          $IIBean->u_units_id1_c = $clonedBean->u_units_id1_c;
          $IIBean->timepoint_unit = $clonedBean->timepoint_unit;
          $IIBean->processing_method = $clonedBean->processing_method;
          $IIBean->collection_date_time = $clonedBean->collection_date_time;
          $IIBean->expiration_test_article = $clonedBean->expiration_test_article;
          $IIBean->expiration_date = $clonedBean->expiration_date;
          $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
          $IIBean->location_building = $clonedBean->location_building;
          $IIBean->rms_room_id_c = $clonedBean->rms_room_id_c;
          $IIBean->location_room = $clonedBean->location_room;
          $IIBean->equip_equipment_id_c = $clonedBean->equip_equipment_id_c;
          $IIBean->location_equipment = $clonedBean->location_equipment;
          $IIBean->location_shelf = $clonedBean->location_shelf;
          $IIBean->location_cabinet = $clonedBean->location_cabinet;
          $IIBean->contact_id_c = $clonedBean->contact_id_c;
          $IIBean->ship_to_contact = $clonedBean->ship_to_contact;
          $IIBean->account_id_c = $clonedBean->account_id_c;
          $IIBean->ship_to_company = $clonedBean->ship_to_company;
          $IIBean->ca_company_address_id_c = $clonedBean->ca_company_address_id_c;
          $IIBean->ship_to_address = $clonedBean->ship_to_address;
          $IIBean->retention_start_date = $clonedBean->retention_start_date;
          $IIBean->retention_interval_days = $clonedBean->retention_interval_days;
          $IIBean->planned_discard_date = $clonedBean->planned_discard_date;
          $IIBean->actual_discard_date = $clonedBean->actual_discard_date;
          $IIBean->quantity_c = $clonedBean->quantity_c;
          $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
          $IIBean->type_2 = $clonedBean->type_2;
          $IIBean->product_name = $clonedBean->product_name;
          $IIBean->allowed_storage_conditions = $clonedBean->allowed_storage_conditions;
          $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
          $IIBean->subtype = $clonedBean->subtype;
          $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
          $IIBean->storage_medium = $clonedBean->storage_medium;
          $IIBean->collection_date = $clonedBean->collection_date;
          $IIBean->received_date = $clonedBean->received_date;
          $IIBean->storage_condition = $clonedBean->storage_condition;
          $IIBean->location_type = $clonedBean->location_type;
          $IIBean->status = $clonedBean->status;
          $IIBean->inactive_c = $clonedBean->inactive_c;
          $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
          $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
          $IIBean->test_type_c = $clonedBean->test_type_c;
          $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
          $IIBean->product_id_2_c = $clonedBean->product_id_2_c;
          $IIBean->pk_samples_c = $clonedBean->pk_samples_c;
          $IIBean->number_of_aliquots_c = $clonedBean->number_of_aliquots_c;
          $IIBean->other_test_type_c = $clonedBean->other_test_type_c;
          $IIBean->timepoint_name_c = $clonedBean->timepoint_name_c;
          $IIBean->timepoint_end_integer_c = $clonedBean->timepoint_end_integer_c;
          $IIBean->location_rack_c = $clonedBean->location_rack_c;
          $IIBean->location_cubby_c = $clonedBean->location_cubby_c;
          $IIBean->location_bin_c = $clonedBean->location_bin_c;
          $IIBean->location_shelf = $clonedBean->location_shelf;
          $IIBean->location_cabinet = $clonedBean->location_cabinet;
          $IIBean->quantity_2_c = $clonedBean->quantity_2_c;

          $IIBean->unique_id_c = $this->generateRandomString(6);
          $IIBean->save();

          if (!empty($strpoiId)) {
            $IIBean->poi_purchase_order_item_ii_inventory_item_1->add($strpoiId[0]);
          }
          if (!empty($strpoisalesId)) {
            $IIBean->m01_sales_ii_inventory_item_1->add($strpoisalesId[0]);
          }
          if (!empty($strpoiwpId)) {
            $IIBean->m03_work_product_ii_inventory_item_1->add($strpoiwpId[0]);
          }
          /*if (!empty($invoice_id)) {
            $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[0]);
          }*/
        }
      }
    }

    //$dup = new InventoryItemCreateDuplicatesPoiUtils($module, $recordId, $poiName, $poiType2, $poiSubtype, $poisalesId, $poiwpId, $poiId);
    //$dup->duplicate();

    return true;
  }

  public function create_ori_duplicates($api, $args)
  {

    global $db, $app_list_strings;

    $II_product_category_list_dom = $app_list_strings['product_category_list'];
    $II_pro_subtype_list_dom = $app_list_strings['pro_subtype_list'];

    $module = $args['module'];
    $recordId = $args['record'];
    $oriId = $args['oriId'];
    $oriName = $args['oriName'];

    $oriType2 = $args['oriType2'];
    $oriSubtype = $args['oriSubtype'];
    $orisalesId = $args['orisalesId'];
    $oriwpId = $args['oriwpId'];
    $productIdsArray = $args['ORI_ProductIdData'];

    $stroriName = explode(',', $oriName);
    $stroriType2 = explode(',', $oriType2);
    $stroriSubtype = explode(',', $oriSubtype);
    $strorisalesId = explode(',', $orisalesId);
    $stroriwpId = explode(',', $oriwpId);
    $stroriId = explode(',', $oriId);
    $productIds = explode(',', $productIdsArray);

    $clonedBean = BeanFactory::retrieveBean($module, $recordId);
    $clonedBeanName = substr($clonedBean->name, 0, -3);

    $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
    $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
    $clonedBean->load_relationship("ori_order_request_item_ii_inventory_item_1");

    //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");
    //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();

    if (!empty($stroriId)) {
      $clonedBean->ori_order_request_item_ii_inventory_item_1->add($stroriId[0]);
    }

    if (count($stroriName) > 1) {
      for ($i = 1; $i < count($stroriName); $i++) {
        $prductBean = BeanFactory::retrieveBean("Prod_Product", $productIds[$i]);
        $Bean_product_name = $prductBean->product_name;
        $Bean_product_id_2_c = $prductBean->product_id_2;
        $Bean_manufacturer_c = $prductBean->manufacturer;
        $Bean_external_barcode = $prductBean->external_barcode;
        $Bean_concentration_c = $prductBean->concentration;
        $Bean_allowed_storage_conditions = $prductBean->allowed_storage;
        $Bean_u_units_id_c = $prductBean->u_units_id_c;
        $Bean_concentration_unit_c = $prductBean->concentration_unit;

        $SaleName = "";
        $WpName = "";
        if ($strorisalesId[$i] != '' && $strorisalesId[$i] == '0') {
        } else {
          $sqlSales = 'SELECT name FROM m01_sales WHERE id = "' . $strorisalesId[$i] . '"';
          $resultSales = $db->query($sqlSales);
          if ($resultSales->num_rows > 0) {
            $rowSales = $db->fetchByAssoc($resultSales);
            $SaleName = $rowSales['name'];
          }
        }

        if ($stroriwpId[$i] != '' && $stroriwpId[$i] == '0') {
        } else {
          $sqlWp = 'SELECT name FROM m03_work_product WHERE id = "' . $stroriwpId[$i] . '"';
          $resultWp = $db->query($sqlWp);
          if ($resultWp->num_rows > 0) {
            $rowWp = $db->fetchByAssoc($resultWp);
            $WpName = $rowWp['name'];
          }
        }

        //$ProductName = " " . $clonedBean->product_name;
        $ProductName = " " . $Bean_product_name;


        $tp = $stroriType2[$i];
        if ($stroriType2[$i] != '' && $stroriType2[$i] == '0') {
        } else {
          $Type_2 = $II_product_category_list_dom[$tp];
          $TypeName = " " . $Type_2;
        }

        $Subtype = $stroriSubtype[$i];
        if ($Subtype != '' && $Subtype == '0') {
          $SubtypeName = "";
          $Subtype_Name = "";
        } else {
          $SubType_2 = $II_pro_subtype_list_dom[$Subtype];
          $SubtypeName = " " . $SubType_2;
          $Subtype_Name = $Subtype;
        }

        $BeanName = $SaleName . $WpName . $ProductName . $TypeName . $SubtypeName;

        $IIBean = BeanFactory::newBean('II_Inventory_Item');

        $bid = create_guid();
        $IIBean->id = $bid;
        $IIBean->new_with_id = true;
        $IIBean->fetched_row = null;

        $IIBean->name = $BeanName;
        $IIBean->description = $clonedBean->description;
        $IIBean->related_to_c = $clonedBean->related_to_c;
        $IIBean->category = $clonedBean->category;
        $IIBean->owner = $clonedBean->owner;
        $IIBean->lot_number_c = $clonedBean->lot_number_c;
        $IIBean->manufacturer_c = $Bean_manufacturer_c;
        $IIBean->external_barcode = $Bean_external_barcode;
        //$IIBean->internal_barcode = $clonedBean->internal_barcode;
        $IIBean->concentration_c = $Bean_concentration_c;
        $IIBean->u_units_id_c = $Bean_u_units_id_c;
        $IIBean->concentration_unit_c = $Bean_concentration_unit_c;
        $IIBean->sterilization = $clonedBean->sterilization;
        $IIBean->timepoint_type = $clonedBean->timepoint_type;
        $IIBean->timepoint_integer = $clonedBean->timepoint_integer;
        $IIBean->u_units_id1_c = $clonedBean->u_units_id1_c;
        $IIBean->timepoint_unit = $clonedBean->timepoint_unit;
        $IIBean->processing_method = $clonedBean->processing_method;
        $IIBean->collection_date_time = $clonedBean->collection_date_time;
        $IIBean->expiration_test_article = $clonedBean->expiration_test_article;
        $IIBean->expiration_date = $clonedBean->expiration_date;
        $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
        $IIBean->location_building = $clonedBean->location_building;
        $IIBean->rms_room_id_c = $clonedBean->rms_room_id_c;
        $IIBean->location_room = $clonedBean->location_room;
        $IIBean->equip_equipment_id_c = $clonedBean->equip_equipment_id_c;
        $IIBean->location_equipment = $clonedBean->location_equipment;
        $IIBean->location_shelf = $clonedBean->location_shelf;
        $IIBean->location_cabinet = $clonedBean->location_cabinet;
        $IIBean->contact_id_c = $clonedBean->contact_id_c;
        $IIBean->ship_to_contact = $clonedBean->ship_to_contact;
        $IIBean->account_id_c = $clonedBean->account_id_c;
        $IIBean->ship_to_company = $clonedBean->ship_to_company;
        $IIBean->ca_company_address_id_c = $clonedBean->ca_company_address_id_c;
        $IIBean->ship_to_address = $clonedBean->ship_to_address;
        $IIBean->retention_start_date = $clonedBean->retention_start_date;
        $IIBean->retention_interval_days = $clonedBean->retention_interval_days;
        $IIBean->planned_discard_date = $clonedBean->planned_discard_date;
        $IIBean->actual_discard_date = $clonedBean->actual_discard_date;
        $IIBean->quantity_c = $clonedBean->quantity_c;
        $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
        $IIBean->type_2 = $stroriType2[$i];
        $IIBean->product_name = $Bean_product_name;
        $IIBean->allowed_storage_conditions = $Bean_allowed_storage_conditions;
        $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
        $IIBean->subtype = $Subtype_Name;
        $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
        $IIBean->storage_medium = $clonedBean->storage_medium;
        $IIBean->collection_date = $clonedBean->collection_date;
        $IIBean->received_date = $clonedBean->received_date;
        $IIBean->storage_condition = $clonedBean->storage_condition;
        $IIBean->location_type = $clonedBean->location_type;
        $IIBean->status = $clonedBean->status;
        $IIBean->inactive_c = $clonedBean->inactive_c;
        $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
        $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
        $IIBean->test_type_c = $clonedBean->test_type_c;
        $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
        $IIBean->product_id_2_c = $Bean_product_id_2_c;

        $IIBean->unique_id_c = $this->generateRandomString(6);
        $IIBean->save();

        if (!empty($stroriId)) {
          $IIBean->ori_order_request_item_ii_inventory_item_1->add($stroriId[$i]);
        }

        if ($strorisalesId[$i] != '' && $strorisalesId[$i] == '0') {
        } else {
          $IIBean->m01_sales_ii_inventory_item_1->add($strorisalesId[$i]);
        }

        if ($stroriwpId[$i] != '' && $stroriwpId[$i] == '0') {
        } else {
          $IIBean->m03_work_product_ii_inventory_item_1->add($stroriwpId[$i]);
        }
        /*if (!empty($invoice_id)) {
          $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[0]);
        }*/
      }
    } else {
      if (is_numeric($clonedBean->quantity_c) && intval($clonedBean->quantity_c) > 0) {
        $quantity = intval($clonedBean->quantity_c);
        for ($i = 0; $i < $quantity - 1; $i++) {

          $IIBean = BeanFactory::newBean('II_Inventory_Item');

          $bid = create_guid();
          $IIBean->id = $bid;
          $IIBean->new_with_id = true;
          $IIBean->fetched_row = null;

          $IIBean->name = substr($clonedBean->name, 0, -3);
          $IIBean->description = $clonedBean->description;
          $IIBean->related_to_c = $clonedBean->related_to_c;
          $IIBean->category = $clonedBean->category;
          $IIBean->owner = $clonedBean->owner;
          $IIBean->lot_number_c = $clonedBean->lot_number_c;
          $IIBean->manufacturer_c = $clonedBean->manufacturer_c;
          $IIBean->external_barcode = $clonedBean->external_barcode;
          //$IIBean->internal_barcode = $clonedBean->internal_barcode;
          $IIBean->concentration_c = $clonedBean->concentration_c;
          $IIBean->u_units_id_c = $clonedBean->u_units_id_c;
          $IIBean->concentration_unit_c = $clonedBean->concentration_unit_c;
          $IIBean->sterilization = $clonedBean->sterilization;
          $IIBean->timepoint_type = $clonedBean->timepoint_type;
          $IIBean->timepoint_integer = $clonedBean->timepoint_integer;
          $IIBean->u_units_id1_c = $clonedBean->u_units_id1_c;
          $IIBean->timepoint_unit = $clonedBean->timepoint_unit;
          $IIBean->processing_method = $clonedBean->processing_method;
          $IIBean->collection_date_time = $clonedBean->collection_date_time;
          $IIBean->expiration_test_article = $clonedBean->expiration_test_article;
          $IIBean->expiration_date = $clonedBean->expiration_date;
          $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
          $IIBean->location_building = $clonedBean->location_building;
          $IIBean->rms_room_id_c = $clonedBean->rms_room_id_c;
          $IIBean->location_room = $clonedBean->location_room;
          $IIBean->equip_equipment_id_c = $clonedBean->equip_equipment_id_c;
          $IIBean->location_equipment = $clonedBean->location_equipment;
          $IIBean->location_shelf = $clonedBean->location_shelf;
          $IIBean->location_cabinet = $clonedBean->location_cabinet;
          $IIBean->contact_id_c = $clonedBean->contact_id_c;
          $IIBean->ship_to_contact = $clonedBean->ship_to_contact;
          $IIBean->account_id_c = $clonedBean->account_id_c;
          $IIBean->ship_to_company = $clonedBean->ship_to_company;
          $IIBean->ca_company_address_id_c = $clonedBean->ca_company_address_id_c;
          $IIBean->ship_to_address = $clonedBean->ship_to_address;
          $IIBean->retention_start_date = $clonedBean->retention_start_date;
          $IIBean->retention_interval_days = $clonedBean->retention_interval_days;
          $IIBean->planned_discard_date = $clonedBean->planned_discard_date;
          $IIBean->actual_discard_date = $clonedBean->actual_discard_date;
          $IIBean->quantity_c = $clonedBean->quantity_c;
          $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
          $IIBean->type_2 = $clonedBean->type_2;
          $IIBean->product_name = $clonedBean->product_name;
          $IIBean->allowed_storage_conditions = $clonedBean->allowed_storage_conditions;
          $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
          $IIBean->subtype = $clonedBean->subtype;
          $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
          $IIBean->storage_medium = $clonedBean->storage_medium;
          $IIBean->collection_date = $clonedBean->collection_date;
          $IIBean->received_date = $clonedBean->received_date;
          $IIBean->storage_condition = $clonedBean->storage_condition;
          $IIBean->location_type = $clonedBean->location_type;
          $IIBean->status = $clonedBean->status;
          $IIBean->inactive_c = $clonedBean->inactive_c;
          $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
          $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
          $IIBean->test_type_c = $clonedBean->test_type_c;
          $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
          $IIBean->product_id_2_c = $clonedBean->product_id_2_c;
          $IIBean->unique_id_c = $this->generateRandomString(6);
          $IIBean->save();

          if (!empty($stroriId)) {
            $IIBean->ori_order_request_item_ii_inventory_item_1->add($stroriId[0]);
          }
          if (!empty($strorisalesId)) {
            $IIBean->m01_sales_ii_inventory_item_1->add($strorisalesId[0]);
          }
          if (!empty($stroriwpId)) {
            $IIBean->m03_work_product_ii_inventory_item_1->add($stroriwpId[0]);
          }
          /*if (!empty($invoice_id)) {
            $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[0]);
          }*/
        }
      }
    }


    //$dup = new InventoryItemCreateDuplicatesOriUtils($module, $recordId, $oriName, $oriType2, $oriSubtype, $orisalesId, $oriwpId, $oriId);
    //$dup->duplicate();

    return true;
  }

  public function create_duplicates($api, $args)
  {
    global $db, $app_list_strings;
    $module = $args['module'];
    $recordId = $args['record'];
    if ($args['subtype'] != "") {
      $subtypeStr = $args['subtype'];
    } else {
      $subtypeStr = 0;
    }

    $II_type_dom = $app_list_strings['inventory_item_type_list'];
    $II_Subtype_dom = $app_list_strings['inventory_item_subtype_list'];
    $II_product_dom = $app_list_strings['pro_subtype_list'];
    $II_specimen_dom = $app_list_strings['tissues_list'];

    $clonedBean = BeanFactory::retrieveBean($module, $recordId);


    if (is_numeric($clonedBean->quantity_c) && intval($clonedBean->quantity_c) <= 10) {
      //$dup = new InventoryItemCreateDuplicatesUtils($module, $recordId, $subtype);
      //$dup->duplicate();

      /* ====================================================== */

      $subtype = explode(',', $subtypeStr);

      if (count($subtype) > 0 && $subtypeStr != "0") {

        $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
        $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
        $clonedBean->load_relationship("m03_work_product_ii_inventory_item_2");
        $clonedBean->load_relationship("anml_animals_ii_inventory_item_1");

        //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");
        //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();

        $product_ids = $clonedBean->m03_work_product_ii_inventory_item_1->get();
        $sale_id = $clonedBean->m01_sales_ii_inventory_item_1->get();
        $work_product_id = $clonedBean->m03_work_product_ii_inventory_item_2->get();
        $test_system_id = $clonedBean->anml_animals_ii_inventory_item_1->get();

        $beanName = substr($clonedBean->name, 0, -3);

        $subtypeR = $clonedBean->subtype;
        $subtype_specimen_c = $clonedBean->subtype;
        $subtype_product_c = $clonedBean->subtype;

        for ($k = 1; $k < count($subtype); $k++) {

          $IIBean = BeanFactory::newBean('II_Inventory_Item');

          $bid = create_guid();
          $IIBean->id = $bid;
          $IIBean->new_with_id = true;

          if (isset($subtypeR) && $subtypeR != "" && $clonedBean->type_2 == "Equipment Facility Record") {
            $bean_newName = str_replace($II_Subtype_dom[$subtype[0]], $II_Subtype_dom[$subtype[$k]], $beanName);
            $IIBean->subtype = $subtype[$k];
          }
          if (isset($subtype_specimen_c) && $subtype_specimen_c != "" && ($clonedBean->type_2 == "Block" || $clonedBean->type_2 == "Culture" || $clonedBean->type_2 == "Slide" || $clonedBean->type_2 == "Tissue")) {
            $bean_newName = str_replace($II_specimen_dom[$subtype[0]], $II_specimen_dom[$subtype[$k]], $beanName);
            $IIBean->subtype = $subtype[$k];
          }
          if (isset($subtype_product_c) && $subtype_product_c != "" && $clonedBean->category == "Product") {
            $bean_newName = str_replace($II_product_dom[$subtype[0]], $II_product_dom[$subtype[$k]], $beanName);
            $IIBean->subtype = $subtype[$k];
          }
          $IIBean->name = $bean_newName;
          $IIBean->description = $clonedBean->description;
          $IIBean->related_to_c = $clonedBean->related_to_c;
          $IIBean->category = $clonedBean->category;
          $IIBean->owner = $clonedBean->owner;
          $IIBean->lot_number_c = $clonedBean->lot_number_c;
          $IIBean->manufacturer_c = $clonedBean->manufacturer_c;
          $IIBean->external_barcode = $clonedBean->external_barcode;
          //$IIBean->internal_barcode = $clonedBean->internal_barcode;
          $IIBean->concentration_c = $clonedBean->concentration_c;
          $IIBean->u_units_id_c = $clonedBean->u_units_id_c;
          $IIBean->concentration_unit_c = $clonedBean->concentration_unit_c;
          $IIBean->sterilization = $clonedBean->sterilization;
          $IIBean->timepoint_type = $clonedBean->timepoint_type;
          $IIBean->timepoint_integer = $clonedBean->timepoint_integer;
          $IIBean->u_units_id1_c = $clonedBean->u_units_id1_c;
          $IIBean->timepoint_unit = $clonedBean->timepoint_unit;
          $IIBean->processing_method = $clonedBean->processing_method;
          $IIBean->collection_date_time = $clonedBean->collection_date_time;
          $IIBean->expiration_test_article = $clonedBean->expiration_test_article;
          $IIBean->expiration_date = $clonedBean->expiration_date;
          $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
          $IIBean->location_building = $clonedBean->location_building;
          $IIBean->rms_room_id_c = $clonedBean->rms_room_id_c;
          $IIBean->location_room = $clonedBean->location_room;
          $IIBean->equip_equipment_id_c = $clonedBean->equip_equipment_id_c;
          $IIBean->location_equipment = $clonedBean->location_equipment;
          $IIBean->location_shelf = $clonedBean->location_shelf;
          $IIBean->location_cabinet = $clonedBean->location_cabinet;
          $IIBean->contact_id_c = $clonedBean->contact_id_c;
          $IIBean->ship_to_contact = $clonedBean->ship_to_contact;
          $IIBean->account_id_c = $clonedBean->account_id_c;
          $IIBean->ship_to_company = $clonedBean->ship_to_company;
          $IIBean->ca_company_address_id_c = $clonedBean->ca_company_address_id_c;
          $IIBean->ship_to_address = $clonedBean->ship_to_address;
          $IIBean->retention_start_date = $clonedBean->retention_start_date;
          $IIBean->retention_interval_days = $clonedBean->retention_interval_days;
          $IIBean->planned_discard_date = $clonedBean->planned_discard_date;
          $IIBean->actual_discard_date = $clonedBean->actual_discard_date;
          $IIBean->quantity_c = $clonedBean->quantity_c;
          $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
          $IIBean->type_2 = $clonedBean->type_2;
          $IIBean->product_name = $clonedBean->product_name;
          $IIBean->allowed_storage_conditions = $clonedBean->allowed_storage_conditions;
          $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
          //$IIBean->subtype = $clonedBean->subtype;
          $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
          $IIBean->storage_medium = $clonedBean->storage_medium;
          $IIBean->collection_date = $clonedBean->collection_date;
          $IIBean->received_date = $clonedBean->received_date;
          $IIBean->storage_condition = $clonedBean->storage_condition;
          $IIBean->location_type = $clonedBean->location_type;
          $IIBean->status = $clonedBean->status;
          $IIBean->inactive_c = $clonedBean->inactive_c;
          $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
          $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
          $IIBean->test_type_c = $clonedBean->test_type_c;
          $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
          $IIBean->product_id_2_c = $clonedBean->product_id_2_c;
          $IIBean->pk_samples_c = $clonedBean->pk_samples_c;
          $IIBean->number_of_aliquots_c = $clonedBean->number_of_aliquots_c;
          $IIBean->other_test_type_c = $clonedBean->other_test_type_c;
          $IIBean->timepoint_name_c = $clonedBean->timepoint_name_c;
          $IIBean->timepoint_end_integer_c = $clonedBean->timepoint_end_integer_c;
          $IIBean->location_rack_c = $clonedBean->location_rack_c;
          $IIBean->location_cubby_c = $clonedBean->location_cubby_c;
          $IIBean->location_bin_c = $clonedBean->location_bin_c;
          $IIBean->location_shelf = $clonedBean->location_shelf;
          $IIBean->location_cabinet = $clonedBean->location_cabinet;
          $IIBean->quantity_2_c = $clonedBean->quantity_2_c;
          $IIBean->fetched_row = null;
          $IIBean->unique_id_c = $this->generateRandomString(6);

          $IIBean->save();

          if (!empty($product_ids)) {
            $IIBean->m03_work_product_ii_inventory_item_1->add($product_ids[0]);
          }
          if (!empty($sale_id)) {
            $IIBean->m01_sales_ii_inventory_item_1->add($sale_id[0]);
          }
          if (!empty($work_product_id)) {
            foreach ($work_product_id as $wp_id) {
              $cusid = create_guid();
              $insert_query = "INSERT INTO m03_work_product_ii_inventory_item_2_c (id,m03_work_product_ii_inventory_item_2m03_work_product_ida,m03_work_product_ii_inventory_item_2ii_inventory_item_idb) VALUES ('" . $cusid . "','" . $wp_id . "','" . $IIBean->id . "')";
              $db->query($insert_query);
            }
          }
          if (!empty($test_system_id)) {
            $IIBean->anml_animals_ii_inventory_item_1->add($test_system_id[0]);
          }

          /*if (!empty($invoice_id)) {
            $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[$k]);
          }*/
        }
        /*if (!empty($invoice_id)) {
          $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id);
        }*/
      } else {
        if (is_numeric($clonedBean->quantity_c) && intval($clonedBean->quantity_c) > 0) {
          $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
          $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
          $clonedBean->load_relationship("m03_work_product_ii_inventory_item_2");
          $clonedBean->load_relationship("anml_animals_ii_inventory_item_1");
          $quantity = intval($clonedBean->quantity_c);

          //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");
          //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();

          $product_ids = $clonedBean->m03_work_product_ii_inventory_item_1->get();
          $sale_id = $clonedBean->m01_sales_ii_inventory_item_1->get();
          $work_product_id = $clonedBean->m03_work_product_ii_inventory_item_2->get();
          $test_system_id = $clonedBean->anml_animals_ii_inventory_item_1->get();

          for ($i = 0; $i < $quantity - 1; $i++) {
            $IIBean = BeanFactory::newBean('II_Inventory_Item');
            $bid = create_guid();
            $IIBean->id = $bid;
            $IIBean->new_with_id = true;
            $IIBean->fetched_row = null;

            $IIBean->name = substr($clonedBean->name, 0, -3);
            $IIBean->description = $clonedBean->description;
            $IIBean->related_to_c = $clonedBean->related_to_c;
            $IIBean->category = $clonedBean->category;
            $IIBean->owner = $clonedBean->owner;
            $IIBean->lot_number_c = $clonedBean->lot_number_c;
            $IIBean->manufacturer_c = $clonedBean->manufacturer_c;
            $IIBean->external_barcode = $clonedBean->external_barcode;
            //$IIBean->internal_barcode = $clonedBean->internal_barcode;
            $IIBean->concentration_c = $clonedBean->concentration_c;
            $IIBean->u_units_id_c = $clonedBean->u_units_id_c;
            $IIBean->concentration_unit_c = $clonedBean->concentration_unit_c;
            $IIBean->sterilization = $clonedBean->sterilization;
            $IIBean->timepoint_type = $clonedBean->timepoint_type;
            $IIBean->timepoint_integer = $clonedBean->timepoint_integer;
            $IIBean->u_units_id1_c = $clonedBean->u_units_id1_c;
            $IIBean->timepoint_unit = $clonedBean->timepoint_unit;
            $IIBean->processing_method = $clonedBean->processing_method;
            $IIBean->collection_date_time = $clonedBean->collection_date_time;
            $IIBean->expiration_test_article = $clonedBean->expiration_test_article;
            $IIBean->expiration_date = $clonedBean->expiration_date;
            $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
            $IIBean->location_building = $clonedBean->location_building;
            $IIBean->rms_room_id_c = $clonedBean->rms_room_id_c;
            $IIBean->location_room = $clonedBean->location_room;
            $IIBean->equip_equipment_id_c = $clonedBean->equip_equipment_id_c;
            $IIBean->location_equipment = $clonedBean->location_equipment;
            $IIBean->location_shelf = $clonedBean->location_shelf;
            $IIBean->location_cabinet = $clonedBean->location_cabinet;
            $IIBean->contact_id_c = $clonedBean->contact_id_c;
            $IIBean->ship_to_contact = $clonedBean->ship_to_contact;
            $IIBean->account_id_c = $clonedBean->account_id_c;
            $IIBean->ship_to_company = $clonedBean->ship_to_company;
            $IIBean->ca_company_address_id_c = $clonedBean->ca_company_address_id_c;
            $IIBean->ship_to_address = $clonedBean->ship_to_address;
            $IIBean->retention_start_date = $clonedBean->retention_start_date;
            $IIBean->retention_interval_days = $clonedBean->retention_interval_days;
            $IIBean->planned_discard_date = $clonedBean->planned_discard_date;
            $IIBean->actual_discard_date = $clonedBean->actual_discard_date;
            $IIBean->quantity_c = $clonedBean->quantity_c;
            $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
            $IIBean->type_2 = $clonedBean->type_2;
            $IIBean->product_name = $clonedBean->product_name;
            $IIBean->allowed_storage_conditions = $clonedBean->allowed_storage_conditions;
            $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
            $IIBean->subtype = $clonedBean->subtype;
            $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
            $IIBean->storage_medium = $clonedBean->storage_medium;
            $IIBean->collection_date = $clonedBean->collection_date;
            $IIBean->received_date = $clonedBean->received_date;
            $IIBean->storage_condition = $clonedBean->storage_condition;
            $IIBean->location_type = $clonedBean->location_type;
            $IIBean->status = $clonedBean->status;
            $IIBean->inactive_c = $clonedBean->inactive_c;
            $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
            $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
            $IIBean->test_type_c = $clonedBean->test_type_c;
            $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
            $IIBean->product_id_2_c = $clonedBean->product_id_2_c;
            $IIBean->pk_samples_c = $clonedBean->pk_samples_c;
            $IIBean->number_of_aliquots_c = $clonedBean->number_of_aliquots_c;
            $IIBean->other_test_type_c = $clonedBean->other_test_type_c;
            $IIBean->timepoint_name_c = $clonedBean->timepoint_name_c;
            $IIBean->timepoint_end_integer_c = $clonedBean->timepoint_end_integer_c;
            $IIBean->location_rack_c = $clonedBean->location_rack_c;
            $IIBean->location_cubby_c = $clonedBean->location_cubby_c;
            $IIBean->location_bin_c = $clonedBean->location_bin_c;
            $IIBean->location_shelf = $clonedBean->location_shelf;
            $IIBean->location_cabinet = $clonedBean->location_cabinet;
            $IIBean->quantity_2_c = $clonedBean->quantity_2_c;

            $IIBean->unique_id_c = $this->generateRandomString(6);

            $IIBean->save();


            if ($product_ids) {
              $IIBean->m03_work_product_ii_inventory_item_1->add($product_ids[0]);
            }
            if ($sale_id) {
              $IIBean->m01_sales_ii_inventory_item_1->add($sale_id[0]);
            }

            if ($work_product_id) {
              foreach ($work_product_id as $wp_id) {
                $cusid = create_guid();
                $insert_query = "INSERT INTO m03_work_product_ii_inventory_item_2_c (id,m03_work_product_ii_inventory_item_2m03_work_product_ida,m03_work_product_ii_inventory_item_2ii_inventory_item_idb) VALUES ('" . $cusid . "','" . $wp_id . "','" . $IIBean->id . "')";
                $db->query($insert_query);
              }
            }

            if ($test_system_id) {
              $IIBean->anml_animals_ii_inventory_item_1->add($test_system_id[0]);
            }

            /*if ($invoice_id) {
              $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[0]);
            }*/

            $sql = 'UPDATE ii_inventory_item_audit SET before_value_string = "" WHERE parent_id = "' . $bid . '"';
            $db->query($sql);
          }
        }
      }

      /* ====================================================== */
    } else if (is_numeric($clonedBean->quantity_c) && intval($clonedBean->quantity_c) > 10) {
      $this->startDuplicatesJob($module, $recordId);
    }

    return true;
  }

  /**
   * Used to start the scheduled job
   */
  protected function startDuplicatesJob($module, $recordId)
  {
    $jq = new SugarJobQueue();
    $job = new SchedulersJob();
    $job->data = serialize(array("module" => $module, "recordId" => $recordId));
    $job->name = "Inventory Items Duplicate Job";
    $job->target = "class::Sugarcrm\Sugarcrm\custom\wsystems\InventoryItemCreation\Jobs\InventoryItemCreateDuplicates";
    $jq->submitJob($job);
  }

  private function generateRandomString($length = 10)
  {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return "AP-" . $randomString;
  }
  /*#1556 : Auto-create duplicate IIs for Aliquots : 16 Sep 2021 */
  public function create_aliquots_duplicates($api, $args)
  {
    global $db, $app_list_strings;
    $module     = $args['module'];
    $recordId   = $args['record'];
    $aliquots   = $args['aliquots'];
    if ($args['subtype'] != "") {
      $subtypeStr = $args['subtype'];
    } else {
      $subtypeStr = 0;
    }

    $II_type_dom = $app_list_strings['inventory_item_type_list'];
    $II_Subtype_dom = $app_list_strings['inventory_item_subtype_list'];
    $II_product_dom = $app_list_strings['pro_subtype_list'];
    $II_specimen_dom = $app_list_strings['tissues_list'];

    $clonedBean = BeanFactory::retrieveBean($module, $recordId);
    $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
    $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
    $clonedBean->load_relationship("m03_work_product_ii_inventory_item_2");
    $clonedBean->load_relationship("anml_animals_ii_inventory_item_1");

    //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");
    //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();

    $product_ids = $clonedBean->m03_work_product_ii_inventory_item_1->get();
    $sale_id = $clonedBean->m01_sales_ii_inventory_item_1->get();
    $work_product_id = $clonedBean->m03_work_product_ii_inventory_item_2->get();
    $test_system_id = $clonedBean->anml_animals_ii_inventory_item_1->get();

    $beanName = substr($clonedBean->name, 0, -3);

    if ($aliquots > 0) {

      $subtype = explode(',', $subtypeStr);
      if (count($subtype) > 0 && $subtypeStr != "0") {

        $subtypeR = $clonedBean->subtype;
        $subtype_specimen_c = $clonedBean->subtype;
        $subtype_product_c = $clonedBean->subtype;

        for ($l = 0; $l < $aliquots; $l++) {

          $pos = strpos($beanName, $II_specimen_dom[$subtype[0]]);

          if ($l == 0 && $pos !== false) {
            $countStart = 1;
          } else {
            $countStart = 0;
          }

          for ($k = $countStart; $k < count($subtype); $k++) {

            if (isset($subtypeR) && $subtypeR != "" && $clonedBean->type_2 == "Equipment Facility Record") {
              $bean_newName = str_replace($II_Subtype_dom[$subtype[0]], $II_Subtype_dom[$subtype[$k]], $beanName);
              $newSubTypeName = $subtype[$k];
            }
            if (isset($subtype_specimen_c) && $subtype_specimen_c != "" && ($clonedBean->type_2 == "Block" || $clonedBean->type_2 == "Culture" || $clonedBean->type_2 == "Slide" || $clonedBean->type_2 == "Tissue")) {
              $bean_newName = str_replace($II_specimen_dom[$subtype[0]], $II_specimen_dom[$subtype[$k]], $beanName);
              $newSubTypeName = $subtype[$k];
            }
            if (isset($subtype_product_c) && $subtype_product_c != "" && $clonedBean->category == "Product") {
              $bean_newName = str_replace($II_product_dom[$subtype[0]], $II_product_dom[$subtype[$k]], $beanName);
              $newSubTypeName = $subtype[$k];
            }


            /*Call function to create a new Inventory item record*/
            $this->getNewII_bean($clonedBean, $bean_newName, $newSubTypeName, $product_ids[0], $sale_id[0], $work_product_id, $test_system_id[0]);

            /*if (!empty($invoice_id)) {
              $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[$k]);
            }*/
          }
        }
      } else {
        for ($k = 0; $k < $aliquots - 1; $k++) {

          $arrPullTSId1     = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
          $arrPullUSDAId1   = array("Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig");

          $testSystemBean1 = BeanFactory::retrieveBean("ANML_Animals", $test_system_id[0]);
          $species1       = $testSystemBean1->species_2_c;
          $ts_name        = $testSystemBean1->name;
          $clonedBeanName = substr($clonedBean->name, 0, -3);

          $nameAppend1 = "";
          $clonedBeanArr1 = explode(" ", $clonedBeanName);

          if (in_array($species1, $arrPullTSId1)) {
            $nameAppend1 = $ts_name;
            $clonedBeanArr1[1] = $nameAppend1;
          } else if (in_array($species1, $arrPullUSDAId1)) {
            if ($testSystemBean1->usda_id_c != "") {
              $nameAppend1 = $testSystemBean1->usda_id_c;
            } else {
              $nameAppend1 = $ts_name;
            }
            $clonedBeanArr1[1] = $nameAppend1;
          } else {
            $nameAppend1 = $ts_name;
            $clonedBeanArr1[1] = $nameAppend1;
          }

          $clonedBeanName_New1 = implode(" ", $clonedBeanArr1);
          if (!empty($test_system_id)) {
            /*Bug#2062 : Inventory Item Naming Sequence 25 Jan 2022*/
            if ($clonedBean->type_2 == "Other") {
              $clonedBean_newName = str_replace($clonedBean->type_2, $clonedBean->other_type_c, $clonedBeanName_New1);
            } else {
              $clonedBean_newName = str_replace($clonedBean->type_2, $clonedBean->type_2, $clonedBeanName_New1);
            }
          } else {
            if ($clonedBean->type_2 == "Other") {
              $clonedBean_newName = str_replace($clonedBean->type_2, $clonedBean->other_type_c, $clonedBeanName);
            } else {
              $clonedBean_newName = str_replace($clonedBean->type_2, $clonedBean->type_2, $clonedBeanName);
            }
          }
          $newSubTypeName = "";



          /*Call function to create a new Inventory item record*/
          $this->getNewII_bean($clonedBean, $clonedBean_newName, $newSubTypeName, $product_ids[0], $sale_id[0], $work_product_id, $test_system_id[0]);
          /*if ($invoice_id) {
            $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[0]);
          }*/

          //$sql = 'UPDATE ii_inventory_item_audit SET before_value_string = "" WHERE parent_id = "' . $bid . '"';
          //$db->query($sql);
        }
      }
      if ($clonedBean->type_2 == 'EDTA Plasma' || $clonedBean->type_2 == 'Na Heparin Plasma' || $clonedBean->type_2 == 'NaCit Plasma' || $clonedBean->type_2 == 'Serum') {

        $arrPullTSId1     = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
        $arrPullUSDAId1   = array("Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig");

        $testSystemBean1 = BeanFactory::retrieveBean("ANML_Animals", $test_system_id[0]);
        $species1       = $testSystemBean1->species_2_c;
        $ts_name        = $testSystemBean1->name;
        $clonedBeanName_WB = substr($clonedBean->name, 0, -3);

        $nameAppend1 = "";
        $clonedBeanArr1 = explode(" ", $clonedBeanName_WB);

        if (in_array($species1, $arrPullTSId1)) {
          $nameAppend1 = $ts_name;
          $clonedBeanArr1[1] = $nameAppend1;
        } else if (in_array($species1, $arrPullUSDAId1)) {
          if ($testSystemBean1->usda_id_c != "") {
            $nameAppend1 = $testSystemBean1->usda_id_c;
          } else {
            $nameAppend1 = $ts_name;
          }
          $clonedBeanArr1[1] = $nameAppend1;
        } else {
          $nameAppend1 = $ts_name;
          $clonedBeanArr1[1] = $nameAppend1;
        }

        $clonedBeanNameWB = implode(" ", $clonedBeanArr1);
        $clonedBeanNewName = str_replace($clonedBean->type_2, 'Whole Blood', $clonedBeanNameWB);
        $clonedBean->type_2 = 'Whole Blood';
        $this->getNewII_bean($clonedBean, $clonedBeanNewName, "", $product_ids[0], $sale_id[0], $work_product_id, $test_system_id[0]);
      }
      /* ====================================================== */
    }

    return true;
  }
  /** 1556 : for related to : multiple test system */
  public function create_aliquots_tsmulti_duplicates($api, $args)
  {
    global $app_list_strings;

    $module    = $args['module'];
    $recordId  = $args['record'];
    $tsId    = $args['tsId'];
    $tsName    = $args['tsName'];
    $aliquots  = $args['aliquots'];
    if ($args['subtype'] != "") {
      $subtypeStr = $args['subtype'];
    } else {
      $subtypeStr = 0;
    }

    $clonedBean = BeanFactory::retrieveBean($module, $recordId);

    $strTsId    = explode(',', $tsId);
    $strTsName    = explode(',', $tsName);
    $subtype    = explode(',', $subtypeStr);

    $clonedBeanName = substr($clonedBean->name, 0, -3);
    $clonedBean->load_relationship("m03_work_product_ii_inventory_item_1");
    $clonedBean->load_relationship("m01_sales_ii_inventory_item_1");
    $clonedBean->load_relationship("m03_work_product_ii_inventory_item_2");
    $clonedBean->load_relationship("anml_animals_ii_inventory_item_1");
    //$clonedBean->load_relationship("invi_invoice_item_ii_inventory_item_1");

    $product_ids    = $clonedBean->m03_work_product_ii_inventory_item_1->get();
    $sale_id      = $clonedBean->m01_sales_ii_inventory_item_1->get();
    $work_product_id   = $clonedBean->m03_work_product_ii_inventory_item_2->get();
    //$invoice_id = $clonedBean->invi_invoice_item_ii_inventory_item_1->get();

    $II_type_dom    = $app_list_strings['inventory_item_type_list'];
    $II_specimen_dom  = $app_list_strings['tissues_list'];

    /*Create duplicate records on aliquots basis */
    for ($l = 0; $l < $aliquots; $l++) {

      if (count($subtype) > 0 && $subtypeStr != "0") {

        $pos = strpos($clonedBeanName, $II_specimen_dom[$subtype[0]]);

        if ($l == 0 && $pos !== false) {
          $countStart = 1;
        } else {
          $countStart = 0;
        }

        for ($k = $countStart; $k < count($subtype); $k++) {

          $arrPullTSId1  = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
          $arrPullUSDAId1 = array("Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig");

          $testSystemBean1  = BeanFactory::retrieveBean("ANML_Animals", $strTsId[0]);
          $species1      = $testSystemBean1->species_2_c;

          $test_system_id1  = $strTsId[0];

          $nameAppend1    = "";
          $clonedBeanArr1    = explode(" ", $clonedBeanName);

          if (in_array($species1, $arrPullTSId1)) {
            $nameAppend1 = $strTsName[0];
            $clonedBeanArr1[1] = $nameAppend1;
          } else if (in_array($species1, $arrPullUSDAId1)) {
            if ($testSystemBean1->usda_id_c != "") {
              $nameAppend1 = $testSystemBean1->usda_id_c;
            } else {
              $nameAppend1 = $strTsName[0];
            }
            $clonedBeanArr1[1] = $nameAppend1;
          } else {
            $nameAppend1 = $strTsName[0];
            $clonedBeanArr1[1] = $nameAppend1;
          }

          $clonedBeanName_New1  = implode(" ", $clonedBeanArr1);
          $clonedBean_newName    = str_replace($II_specimen_dom[$subtype[0]], $II_specimen_dom[$subtype[$k]], $clonedBeanName_New1);

          /*Call function to create a new Inventory item record*/
          $this->getNewII_bean($clonedBean, $clonedBean_newName, $subtype[$k], $product_ids[0], $sale_id[0], $work_product_id[0], $test_system_id1);

          /*if (!empty($invoice_id)) {
					$IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[0]);
				}*/
        }
      }

      if ($subtypeStr == "0") {
        if ($l == 0)
          $tsCountStart = 1;
        else
          $tsCountStart = 0;
      } else {
        $tsCountStart = 1;
      }

      for ($i = $tsCountStart; $i < count($strTsName); $i++) {

        $arrPullTSId  = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
        $arrPullUSDAId  = array("Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig");

        $testSystemBean  = BeanFactory::retrieveBean("ANML_Animals", $strTsId[$i]);
        $species    = $testSystemBean->species_2_c;

        $nameAppend    = "";
        $clonedBeanArr  = explode(" ", $clonedBeanName);
        if (in_array($species, $arrPullTSId)) {
          $nameAppend      = $strTsName[$i];
          $clonedBeanArr[1]    = $nameAppend;
        } else if (in_array($species, $arrPullUSDAId)) {
          if ($testSystemBean->usda_id_c != "") {
            $nameAppend = $testSystemBean->usda_id_c;
          } else {
            $nameAppend = $strTsName[$i];
          }
          $clonedBeanArr[1] = $nameAppend;
        } else {
          $nameAppend = $strTsName[$i];
          $clonedBeanArr[1] = $nameAppend;
        }

        $clonedBeanName_New = implode(" ", $clonedBeanArr);

        if (count($subtype) > 0 && $subtypeStr != "0") {

          for ($j = 0; $j < count($subtype); $j++) {

            $clonedBean_newName = str_replace($II_specimen_dom[$subtype[0]], $II_specimen_dom[$subtype[$j]], $clonedBeanName_New);


            $test_system_id   = $strTsId[$i];

            /*Call function to create a new Inventory item record*/
            $this->getNewII_bean($clonedBean, $clonedBean_newName, $subtype[$j], $product_ids[0], $sale_id[0], $work_product_id[0], $test_system_id);

            /*if (!empty($invoice_id)) {
              $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[0]);
            }*/
          }
        } else {
          $subtypeName = "";

          $test_system_id  = $strTsId[$i];

          $pos = strpos($clonedBeanName, $nameAppend);

          if ($l == 0 && $pos !== false) {
          } else {

            /*Call function to create a new Inventory item record*/
            $this->getNewII_bean($clonedBean, $clonedBeanName_New, $subtypeName, $product_ids[0], $sale_id[0], $work_product_id[0], $test_system_id);
          }


          /*if (!empty($invoice_id)) {
            $IIBean->invi_invoice_item_ii_inventory_item_1->add($invoice_id[0]);
          }*/
        }
      }
      //$dup = new InventoryItemCreateDuplicatesTsUtils($module, $recordId, $tsId, $tsName, $subtype);
      //$dup->duplicate();

    }
    for ($ii = 0; $ii < count($strTsName); $ii++) {
      if ($clonedBean->type_2 == 'EDTA Plasma' || $clonedBean->type_2 == 'Na Heparin Plasma' || $clonedBean->type_2 == 'NaCit Plasma' || $clonedBean->type_2 == 'Serum') {

        $arrPullTSId1     = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
        $arrPullUSDAId1   = array("Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig");

        $testSystemBean1 = BeanFactory::retrieveBean("ANML_Animals", $strTsId[$ii]);
        $species1       = $testSystemBean1->species_2_c;
        $ts_name        = $testSystemBean1->name;
        $clonedBeanName_WB = substr($clonedBean->name, 0, -3);

        $nameAppend1 = "";
        $clonedBeanArr1 = explode(" ", $clonedBeanName_WB);

        if (in_array($species1, $arrPullTSId1)) {
          $nameAppend1 = $ts_name;
          $clonedBeanArr1[1] = $nameAppend1;
        } else if (in_array($species1, $arrPullUSDAId1)) {
          if ($testSystemBean1->usda_id_c != "") {
            $nameAppend1 = $testSystemBean1->usda_id_c;
          } else {
            $nameAppend1 = $ts_name;
          }
          $clonedBeanArr1[1] = $nameAppend1;
        } else {
          $nameAppend1 = $ts_name;
          $clonedBeanArr1[1] = $nameAppend1;
        }

        $clonedBeanNameWB = implode(" ", $clonedBeanArr1);
        $clonedBeanNewName = str_replace($clonedBean->type_2, 'Whole Blood', $clonedBeanNameWB);
        $oldType = $clonedBean->type_2;
        $clonedBean->type_2 = 'Whole Blood';
        $this->getNewII_bean($clonedBean, $clonedBeanNewName, '', $product_ids[0], $sale_id[0], $work_product_id[0], $strTsId[$ii]);
        $clonedBean->type_2 = $oldType;
      }
    }
    return true;
  }

  /*Create II bean records and its relationships*/

  public function getNewII_bean($clonedBean, $clonedBean_newName, $newSubtype, $product_ids, $sale_id, $work_product_id, $test_system_id1)
  {
    global $db;
    $IIBean  = BeanFactory::newBean('II_Inventory_Item');
    $bid  = create_guid();
    $IIBean->id        = $bid;
    $IIBean->new_with_id  = true;
    $IIBean->name      = $clonedBean_newName;
    $IIBean->fetched_row  = null;
    $IIBean->description   = $clonedBean->description;
    $IIBean->related_to_c   = $clonedBean->related_to_c;
    $IIBean->category     = $clonedBean->category;
    $IIBean->owner       = $clonedBean->owner;
    $IIBean->lot_number_c   = $clonedBean->lot_number_c;
    $IIBean->manufacturer_c = $clonedBean->manufacturer_c;
    $IIBean->external_barcode   = $clonedBean->external_barcode;
    //$IIBean->internal_barcode = $clonedBean->internal_barcode;
    $IIBean->concentration_c   = $clonedBean->concentration_c;
    $IIBean->u_units_id_c     = $clonedBean->u_units_id_c;
    $IIBean->concentration_unit_c = $clonedBean->concentration_unit_c;
    $IIBean->sterilization   = $clonedBean->sterilization;
    $IIBean->timepoint_type = $clonedBean->timepoint_type;
    $IIBean->timepoint_integer   = $clonedBean->timepoint_integer;
    $IIBean->u_units_id1_c     = $clonedBean->u_units_id1_c;
    $IIBean->timepoint_unit   = $clonedBean->timepoint_unit;
    $IIBean->processing_method   = $clonedBean->processing_method;
    $IIBean->collection_date_time     = $clonedBean->collection_date_time;
    $IIBean->expiration_test_article   = $clonedBean->expiration_test_article;
    $IIBean->expiration_date   = $clonedBean->expiration_date;
    $IIBean->days_to_expiration = $clonedBean->days_to_expiration;
    $IIBean->location_building  = $clonedBean->location_building;
    $IIBean->rms_room_id_c    = $clonedBean->rms_room_id_c;
    $IIBean->location_room    = $clonedBean->location_room;
    $IIBean->equip_equipment_id_c  = $clonedBean->equip_equipment_id_c;
    $IIBean->location_equipment    = $clonedBean->location_equipment;
    $IIBean->location_shelf     = $clonedBean->location_shelf;
    $IIBean->location_cabinet     = $clonedBean->location_cabinet;
    $IIBean->contact_id_c       = $clonedBean->contact_id_c;
    $IIBean->ship_to_contact     = $clonedBean->ship_to_contact;
    $IIBean->account_id_c     = $clonedBean->account_id_c;
    $IIBean->ship_to_company   = $clonedBean->ship_to_company;
    $IIBean->ca_company_address_id_c  = $clonedBean->ca_company_address_id_c;
    $IIBean->ship_to_address    = $clonedBean->ship_to_address;
    $IIBean->retention_start_date  = $clonedBean->retention_start_date;
    $IIBean->retention_interval_days  = $clonedBean->retention_interval_days;
    $IIBean->planned_discard_date    = $clonedBean->planned_discard_date;
    $IIBean->actual_discard_date    = $clonedBean->actual_discard_date;
    $IIBean->quantity_c = $clonedBean->quantity_c;
    $IIBean->expiration_date_time = $clonedBean->expiration_date_time;
    $IIBean->type_2 = $clonedBean->type_2;
    $IIBean->product_name = $clonedBean->product_name;
    $IIBean->allowed_storage_conditions = $clonedBean->allowed_storage_conditions;
    $IIBean->allowed_storage_medium = $clonedBean->allowed_storage_medium;
    $IIBean->subtype = $newSubtype; //$subtype[$k];
    $IIBean->collection_date_time_type = $clonedBean->collection_date_time_type;
    $IIBean->storage_medium = $clonedBean->storage_medium;
    $IIBean->collection_date = $clonedBean->collection_date;
    $IIBean->received_date = $clonedBean->received_date;
    $IIBean->storage_condition = $clonedBean->storage_condition;
    $IIBean->location_type = $clonedBean->location_type;
    $IIBean->status = $clonedBean->status;
    $IIBean->inactive_c = $clonedBean->inactive_c;
    $IIBean->stability_considerations_c = $clonedBean->stability_considerations_c;
    $IIBean->analyze_by_date_c = $clonedBean->analyze_by_date_c;
    $IIBean->test_type_c = $clonedBean->test_type_c;
    $IIBean->sterilization_exp_date_c = $clonedBean->sterilization_exp_date_c;
    $IIBean->product_id_2_c = $clonedBean->product_id_2_c;
    $IIBean->pk_samples_c = $clonedBean->pk_samples_c;
    $IIBean->number_of_aliquots_c = $clonedBean->number_of_aliquots_c;
    $IIBean->other_test_type_c = $clonedBean->other_test_type_c;
    $IIBean->timepoint_name_c = $clonedBean->timepoint_name_c;
    $IIBean->timepoint_end_integer_c = $clonedBean->timepoint_end_integer_c;
    $IIBean->location_rack_c = $clonedBean->location_rack_c;
    $IIBean->location_cubby_c = $clonedBean->location_cubby_c;
    $IIBean->location_bin_c = $clonedBean->location_bin_c;
    $IIBean->location_shelf = $clonedBean->location_shelf;
    $IIBean->location_cabinet = $clonedBean->location_cabinet;
    $IIBean->quantity_2_c = $clonedBean->quantity_2_c;
    $IIBean->unique_id_c = $this->generateRandomString(6);

    $IIBean->save(); //Save Inventory Bean



    if (!empty($product_ids)) {
      $IIBean->m03_work_product_ii_inventory_item_1->add($product_ids);
    }

    if (!empty($sale_id)) {
      $IIBean->m01_sales_ii_inventory_item_1->add($sale_id);
    }

    if (is_array($work_product_id) && count($work_product_id) > 0) {
      if (!empty($work_product_id)) {
        foreach ($work_product_id as $wp_id) {
          $cusid = create_guid();
          $insert_query = "INSERT INTO m03_work_product_ii_inventory_item_2_c (id,m03_work_product_ii_inventory_item_2m03_work_product_ida,m03_work_product_ii_inventory_item_2ii_inventory_item_idb) VALUES ('" . $cusid . "','" . $wp_id . "','" . $IIBean->id . "')";
          $db->query($insert_query);
        }
      }
    } else {
      if (!empty($work_product_id)) {
        $IIBean->m03_work_product_ii_inventory_item_2->add($work_product_id);
      }
    }

    if (!empty($test_system_id1)) {
      $IIBean->anml_animals_ii_inventory_item_1->add($test_system_id1);
    }
  }
  /* End function*/

  /*****************************************#1556 : Auto-create duplicate IIs for Aliquots : 16 Sep 2021 EOC ************************ */
}
