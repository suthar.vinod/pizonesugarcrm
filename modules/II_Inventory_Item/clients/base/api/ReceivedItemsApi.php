<?php
if (!defined('sugarEntry') || !sugarEntry) {
  die('Not A Valid Entry Point');
}

class ReceivedItemsApi extends SugarApi
{

  public function registerApiRest()
  {
    return array(
    
      'MyGetEndpointPoi' => array(
        'reqType' => 'POST',
        'noLoginRequired' => false,
        'path' => array('pull_poi_ori_from_ri'),
        'pathVars' => array(),
        'method' => 'pull_poi_ori_from_ri',
        'shortHelp' => '',
        'longHelp' => '',
      ),
    );
  }

 

  public function pull_poi_ori_from_ri($api, $args)
  {

    global $db;    
    $module = $args['module'];
    $recordId = $args['recordId'];

    $RIBean = BeanFactory::retrieveBean($module, $recordId);
    $name = $RIBean->name;
    $type_2 = $RIBean->type_2;
    $poi_id = $RIBean->poi_purcha665cer_item_ida;
    $poi_name = $RIBean->poi_purchase_order_item_ri_received_items_1_name;
    $ori_id = $RIBean->ori_order_6b32st_item_ida;
    $ori_name = $RIBean->ori_order_request_item_ri_received_items_1_name;
    if($poi_id!="")
  {
        $PoiBean = BeanFactory::retrieveBean("POI_Purchase_Order_Item", $poi_id);
        $related_to = $PoiBean->related_to;
        $owner = $PoiBean->owner;
        $type = $PoiBean->type_2;
        $subtype = $PoiBean->subtype;
        if($related_to == "Sales")
        {
          $sale_id = $PoiBean->m01_sales_poi_purchase_order_item_1m01_sales_ida;
          $sale_name = $PoiBean->m01_sales_poi_purchase_order_item_1_name;
        }
        else
        {
          $sale_id = "";
          $sale_name = "";
        }
        if($related_to == "Work Product")
        {
          $wp_id = $PoiBean->m03_work_product_poi_purchase_order_item_1m03_work_product_ida;
          $wp_name = $PoiBean->m03_work_product_poi_purchase_order_item_1_name;
        }
        else
        {
          $wp_id = "";
          $wp_name = "";
        }
        /*#390 : Pull product info from product selected of poi */
        $pro_id = $PoiBean->prod_product_poi_purchase_order_item_1prod_product_ida;
        $pro_name = $PoiBean->prod_product_poi_purchase_order_item_1_name;

        $ProdBean = BeanFactory::retrieveBean("Prod_Product", $pro_id);
        $product_name = $ProdBean->product_name;
        $product_id = $ProdBean->product_id_2;
        $manufacturer = $ProdBean->manufacturer;
        $concentration = $ProdBean->concentration;
        $concentration_unit = $ProdBean->concentration_unit;
        $u_units_id_c = $ProdBean->u_units_id_c;
        $allowed_storage = $ProdBean->allowed_storage;
        $external_barcode = $ProdBean->external_barcode;
  }
    else if($ori_id!="")
  {
        $OriBean = BeanFactory::retrieveBean("ORI_Order_Request_Item", $ori_id);
        $related_to = $OriBean->related_to;
        $owner = $OriBean->owner;
        $type = $OriBean->type_2;
        $subtype = $OriBean->subtype;
        $ori_product_id = $OriBean->product_id_2;
        $ori_product_name = $OriBean->product_name;
        $ori_manufacturer = $OriBean->manufacturer;
        $ori_concentration_unit = $OriBean->concentration_unit;
        $ori_concentration = $OriBean->concentration;
        $ori_u_units_id_c = $OriBean->u_units_id_c;

        if($related_to == "Sales")
        {
          $sale_id = $OriBean->m01_sales_ori_order_request_item_1m01_sales_ida;
          $sale_name = $OriBean->m01_sales_ori_order_request_item_1_name;
        }
        else
        {
          $sale_id = "";
          $sale_name = "";
        }
        if($related_to == "Work Product")
        {
          $wp_id = $OriBean->m03_work_product_ori_order_request_item_1m03_work_product_ida;
          $wp_name = $OriBean->m03_work_product_ori_order_request_item_1_name;
        }
        else
        {
          $wp_id = "";
          $wp_name = "";
        }
        /*#390 : Pull product info from product selected of ori */
        $pro_id = $OriBean->prod_product_ori_order_request_item_1prod_product_ida;
        $pro_name = $OriBean->prod_product_ori_order_request_item_1_name;
        
        /* Get product information from product based on product selected in poi/ori */
      $ProdBean = BeanFactory::retrieveBean("Prod_Product", $pro_id);
      if($ori_product_name == "")
      {
        $product_name = $ProdBean->product_name;
      }
      else
      {
        $product_name = $ori_product_name;
      }

      if($ori_product_id == "")
      {
        $product_id = $ProdBean->product_id_2;
      }
      else
      {
        $product_id = $ori_product_id;
      }

      if($ori_manufacturer == "")
      {
        $manufacturer = $ProdBean->manufacturer;
      }
      else
      {
        $manufacturer = $ori_manufacturer;
      }

      if($ori_concentration == "")
      {
        $concentration = $ProdBean->concentration;
      }
      else
      {
        $concentration = $ori_concentration;
      }

      if($ori_concentration_unit == "")
      {
        $concentration_unit = $ProdBean->concentration_unit;
      }
      else
      {
        $concentration_unit = $ori_concentration_unit;
      }
      if($ori_u_units_id_c == "")
      {
        $u_units_id_c = $ProdBean->u_units_id_c;
      }
      else
      {
        $u_units_id_c = $ori_u_units_id_c;
      }
      
      $allowed_storage = $ProdBean->allowed_storage;
      $external_barcode = $ProdBean->external_barcode;

  }
    

    return array(
        'name' => $name,
        'type_2' => $type_2,
        'poi_id' => $poi_id,
        'poi_name' => $poi_name,
        'ori_id' => $ori_id,
        'ori_name' => $ori_name,
        'sale_id' => $sale_id,
        'sale_name' => $sale_name,
        'wp_id' => $wp_id,
        'wp_name' => $wp_name,
        'owner' => $owner,
        'product_name' => $product_name,
        'product_id' => $product_id,
        'manufacturer' => $manufacturer,
        'concentration' => $concentration,
        'concentration_unit' => $concentration_unit,
        'u_units_id_c' => $u_units_id_c,
        'allowed_storage' => $allowed_storage,
        'external_barcode' => $external_barcode,
        'type' => $type,
        'subtype' => $subtype,
    );
  } 

  
 
}
