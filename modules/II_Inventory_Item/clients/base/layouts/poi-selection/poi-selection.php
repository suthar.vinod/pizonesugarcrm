<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$viewdefs['II_Inventory_Item']['base']['layout']['poi-selection'] = array(
    'type'       => 'selection-list',
    'components' => array(
        array(
            'layout' => array(
                'type'       => 'base',
                'name'       => 'main-pane',
                'css_class'  => 'main-pane span12',
                'components' => array(
                    array(
                        'layout' => array(
                            'type'             => 'filterpanel',
                            'availableToggles' => array(),
                            'filter_options'   => array(
                                'stickiness' => false,
                            ),
                            'components'       => array(
                                array(
                                    'view'       => 'customPoi-selection',
                                    'context'    => array(
                                        'module' => 'POI_Purchase_Order_Item',
                                    ),
                                    'loadModule' => 'II_Inventory_Item',
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);
