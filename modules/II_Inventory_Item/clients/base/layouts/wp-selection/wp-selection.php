<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$viewdefs['II_Inventory_Item']['base']['layout']['wp-selection'] = array(
    'type'       => 'selection-list',
    'components' => array(
        array(
            'layout' => array(
                'type'       => 'base',
                'name'       => 'main-pane',
                'css_class'  => 'main-pane span12',
                'components' => array(
                    // array(
                    //     'view' => 'selection-list-context',
                    //     'context' => array(
                    //         'module' => 'M03_Work_Product'
                    //     ),
                    //     'loadModule' => 'II_Inventory_Item'
                    // ),
                    array(
                        'layout' => array(
                            'type'             => 'filterpanel',
                            'availableToggles' => array(),
                            'filter_options'   => array(
                                'stickiness' => false,
                            ),
                            'components'       => array(
                                // array(
                                //     'layout' => 'filter',
                                //     'loadModule' => 'Filters',
                                // ),
                                // array(
                                //     'view' => 'filter-rows',
                                // ),
                                // array(
                                //     'view' => 'filter-actions',
                                // ),
                                array(
                                    'view'       => 'customWp-selection',
                                    'context'    => array(
                                        'module' => 'M03_Work_Product',
                                    ),
                                    'loadModule' => 'II_Inventory_Item',
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);
