<?php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$viewdefs['II_Inventory_Item']['base']['layout']['subtype-selection'] = array(
    'components' => array(
        array(
            'layout' => array(
                'type' => 'base',
                'name' => 'main-pane',
                'css_class' => 'main-pane span12 subtype_selection',
                'components' => array(
                    array(
                        'layout' => array(
                            'type' => 'filterpanel',
                            'availableToggles' => array(),
                            'filter_options' => array(
                                'stickiness' => false,
                            ),
                            'components' => array(
                                array(
                                'view' => 'customSt-selection',
                                'context'    => array(
                                'module' => 'II_Inventory_Item',
                                ),
                                //'loadModule' => 'II_Inventory_Item',
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);