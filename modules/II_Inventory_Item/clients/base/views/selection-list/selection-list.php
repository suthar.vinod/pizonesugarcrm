<?php
$module_name = 'II_Inventory_Item';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'internal_barcode',
                'label' => 'LBL_INTERNAL_BARCODE',
                'enabled' => true,
                'readonly' => 'true',
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'collection_date',
                'label' => 'LBL_COLLECTION_DATE',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'timepoint_type',
                'label' => 'LBL_TIMEPOINT_TYPE',
                'enabled' => true,
                'default' => false,
              ),
              4 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
              5 => 
              array (
                'name' => 'storage_condition',
                'label' => 'LBL_STORAGE_CONDITION',
                'enabled' => true,
                'default' => false,
              ),
              6 => 
              array (
                'name' => 'location_room',
                'label' => 'LBL_LOCATION_ROOM',
                'enabled' => true,
                'id' => 'RMS_ROOM_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              7 => 
              array (
                'name' => 'anml_animals_ii_inventory_item_1_name',
                'label' => 'LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_ANML_ANIMALS_TITLE',
                'enabled' => true,
                'id' => 'ANML_ANIMALS_II_INVENTORY_ITEM_1ANML_ANIMALS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              8 => 
              array (
                'name' => 'allowed_storage_conditions',
                'label' => 'LBL_ALLOWED_STORAGE_CONDITIONS',
                'enabled' => true,
                'default' => false,
              ),
              9 => 
              array (
                'name' => 'allowed_storage_medium',
                'label' => 'LBL_ALLOWED_STORAGE_MEDIUM',
                'enabled' => true,
                'default' => false,
              ),
              10 => 
              array (
                'name' => 'location_equipment',
                'label' => 'LBL_LOCATION_EQUIPMENT',
                'enabled' => true,
                'id' => 'EQUIP_EQUIPMENT_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              11 => 
              array (
                'name' => 'timepoint_name_c',
                'label' => 'LBL_TIMEPOINT_NAME',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              12 => 
              array (
                'name' => 'collection_date_time',
                'label' => 'LBL_COLLECTION_DATE_TIME',
                'enabled' => true,
                'default' => false,
              ),
              13 => 
              array (
                'name' => 'location_building',
                'label' => 'LBL_LOCATION_BUILDING',
                'enabled' => true,
                'default' => false,
              ),
              14 => 
              array (
                'name' => 'other_test_type_c',
                'label' => 'LBL_OTHER_TEST_TYPE',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              15 => 
              array (
                'name' => 'location_shelf',
                'label' => 'LBL_LOCATION_SHELF',
                'enabled' => true,
                'default' => false,
              ),
              16 => 
              array (
                'name' => 'location_cabinet',
                'label' => 'LBL_LOCATION_CABINET',
                'enabled' => true,
                'default' => false,
              ),
              17 => 
              array (
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'default' => false,
                'name' => 'date_modified',
                'readonly' => true,
              ),
              18 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              19 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
                'enabled' => true,
                'default' => false,
              ),
              20 => 
              array (
                'name' => 'test_type_c',
                'label' => 'LBL_TEST_TYPE',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              21 => 
              array (
                'name' => 'other_type_c',
                'label' => 'LBL_OTHER_TYPE',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              22 => 
              array (
                'name' => 'collection_date_time_type',
                'label' => 'LBL_COLLECTION_DATE_TIME_TYPE',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
