({
    extendsFrom: 'RecordlistView',
    initialize: function (options) {
        this._super('initialize', [options]);

        // Bind "Set red Next Review Date" functionality with data syncronization
        this.listenTo(this.collection, 'data:sync:complete', this.renderDateOccurred);

    },
    renderDateOccurred: function () {

        _.each(this.rowFields, function (field) {
           
            var obj = _.findWhere(field, {name: 'name'});
            var objtype = _.findWhere(field, {name: 'type_2'});
            var objsubtype = _.findWhere(field, {name: 'subtype'});
            var objCategory = _.findWhere(field, {name: 'category'});
            var self = this;
            if(objCategory != undefined){
                var name = obj.model.get('name');
                var category = objCategory.model.get('category');

                App.api.call("get", "rest/v10/II_Inventory_Item?fields=id,type_2,subtype&filter[0][name][$equals]=" + name, null, {
                    success: function (Data) {
                        
                        if (category == 'Product') {

                            objtype.$el.parents('td').html('<span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + Data.records[0].type_2 + '</span>">' + Data.records[0].type_2 + '</div>');

                        } else if ((category == 'Record' || category == 'Specimen') && Data.records[0] != undefined) {
                            if (typeof(objsubtype) != "undefined")
                            objsubtype.$el.parents('td').html('<span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + Data.records[0].subtype + '</span>">' + Data.records[0].subtype + '</div>');
                        }
                    }
                });
            }
        });
    },
})

