<?php
// created: 2021-11-17 05:42:50
$viewdefs['II_Inventory_Item']['base']['view']['subpanel-for-im_inventory_management-ii_inventory_item_im_inventory_management_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'category',
          'label' => 'LBL_CATEGORY',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'type_2',
          'label' => 'LBL_TYPE_2',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'location_building',
          'label' => 'LBL_LOCATION_BUILDING',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'location_room',
          'label' => 'LBL_LOCATION_ROOM',
          'enabled' => true,
          'id' => 'RMS_ROOM_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'allowed_storage_conditions',
          'label' => 'LBL_ALLOWED_STORAGE_CONDITIONS',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'storage_condition',
          'label' => 'LBL_STORAGE_CONDITION',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'allowed_storage_medium',
          'label' => 'LBL_ALLOWED_STORAGE_MEDIUM',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'storage_medium',
          'label' => 'LBL_STORAGE_MEDIUM',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'internal_barcode',
          'label' => 'LBL_INTERNAL_BARCODE',
          'enabled' => true,
          'readonly' => 'true',
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'ic_inventory_collection_ii_inventory_item_1_name',
          'label' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
          'enabled' => true,
          'id' => 'IC_INVENTO128FLECTION_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'created_by_name',
          'label' => 'LBL_CREATED',
          'enabled' => true,
          'readonly' => true,
          'id' => 'CREATED_BY',
          'link' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);