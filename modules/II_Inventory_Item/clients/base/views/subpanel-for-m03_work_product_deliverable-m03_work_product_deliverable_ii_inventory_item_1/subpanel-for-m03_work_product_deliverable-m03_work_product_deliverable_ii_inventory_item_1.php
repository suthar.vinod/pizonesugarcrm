<?php
// created: 2022-02-22 07:54:58
$viewdefs['II_Inventory_Item']['base']['view']['subpanel-for-m03_work_product_deliverable-m03_work_product_deliverable_ii_inventory_item_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'timepoint_type',
          'label' => 'LBL_TIMEPOINT_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'timepoint_name_c',
          'label' => 'LBL_TIMEPOINT_NAME',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'collection_date',
          'label' => 'LBL_COLLECTION_DATE',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'collection_date_time',
          'label' => 'LBL_COLLECTION_DATE_TIME',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'storage_condition',
          'label' => 'LBL_STORAGE_CONDITION',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);