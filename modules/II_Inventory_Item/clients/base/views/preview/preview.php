<?php
$module_name = 'II_Inventory_Item';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'related_to_c',
                'label' => 'LBL_RELATED_TO',
              ),
              1 => 
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
              ),
              2 => 
              array (
                'name' => 'm01_sales_ii_inventory_item_1_name',
              ),
              3 => 
              array (
                'name' => 'm03_work_product_ii_inventory_item_1_name',
              ),
              4 => 
              array (
                'name' => 'anml_animals_ii_inventory_item_1_name',
              ),
              5 => 
              array (
                'name' => 'category',
                'label' => 'LBL_CATEGORY',
              ),
              6 => 
              array (
                'name' => 'poi_purchase_order_item_ii_inventory_item_1_name',
              ),
              7 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
              ),
              8 => 
              array (
                'name' => 'subtype',
                'label' => 'LBL_SUBTYPE',
              ),
              9 => 
              array (
                'name' => 'owner',
                'label' => 'LBL_OWNER',
              ),
              10 => 
              array (
                'name' => 'internal_barcode',
                'label' => 'LBL_INTERNAL_BARCODE',
              ),
              11 => 
              array (
                'name' => 'external_barcode',
                'label' => 'LBL_EXTERNAL_BARCODE',
              ),
              12 => 
              array (
                'name' => 'ic_inventory_collection_ii_inventory_item_1_name',
              ),
            ),
          ),
          2 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'columns' => 2,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'quantity_c',
                'label' => 'Quantity',
              ),
              1 => 
              array (
                'span' => 12,
              ),
              2 => 
              array (
                'name' => 'product_name',
                'label' => 'LBL_PRODUCT_NAME',
              ),
              3 => 
              array (
                'name' => 'received_date',
                'label' => 'LBL_RECEIVED_DATE',
              ),
              4 => 
              array (
                'name' => 'lot_number_c',
                'label' => 'LBL_LOT_NUMBER',
              ),
              5 => 
              array (
                'name' => 'manufacturer_c',
                'label' => 'LBL_MANUFACTURER',
              ),
              6 => 
              array (
                'name' => 'concentration_c',
                'label' => 'LBL_CONCENTRATION',
              ),
              7 => 
              array (
                'name' => 'concentration_unit_c',
                'studio' => 'visible',
                'label' => 'LBL_CONCENTRATION_UNIT',
              ),
              8 => 
              array (
                'name' => 'sterilization',
                'label' => 'LBL_STERILIZATION',
              ),
              9 => 
              array (
                'span' => 12,
              ),
              10 => 
              array (
                'name' => 'timepoint_type',
                'label' => 'LBL_TIMEPOINT_TYPE',
              ),
              11 => 
              array (
                'span' => 12,
              ),
              12 => 
              array (
                'name' => 'timepoint_integer',
                'label' => 'LBL_TIMEPOINT_INTEGER',
              ),
              13 => 
              array (
                'name' => 'timepoint_unit',
                'studio' => 'visible',
                'label' => 'LBL_TIMEPOINT_UNIT',
              ),
              14 => 
              array (
                'name' => 'processing_method',
                'label' => 'LBL_PROCESSING_METHOD',
              ),
              15 => 
              array (
                'span' => 12,
              ),
              16 => 
              array (
                'name' => 'collection_date_time_type',
                'label' => 'LBL_COLLECTION_DATE_TIME_TYPE',
              ),
              17 => 
              array (
                'name' => 'collection_date_time',
                'label' => 'LBL_COLLECTION_DATE_TIME',
              ),
              18 => 
              array (
                'name' => 'collection_date',
                'label' => 'LBL_COLLECTION_DATE',
              ),
              19 => 
              array (
                'span' => 12,
              ),
              20 => 
              array (
                'name' => 'expiration_test_article',
                'label' => 'LBL_EXPIRATION_TEST_ARTICLE',
              ),
              21 => 
              array (
                'span' => 12,
              ),
              22 => 
              array (
                'name' => 'expiration_date',
                'label' => 'LBL_EXPIRATION_DATE',
              ),
              23 => 
              array (
                'span' => 12,
              ),
              24 => 
              array (
                'name' => 'expiration_date_time',
                'label' => 'LBL_EXPIRATION_DATE_TIME',
              ),
              25 => 
              array (
                'span' => 12,
              ),
              26 => 
              array (
                'name' => 'days_to_expiration',
                'label' => 'LBL_DAYS_TO_EXPIRATION',
              ),
              27 => 
              array (
                'span' => 12,
              ),
              28 => 
              array (
                'name' => 'allowed_storage_conditions',
                'label' => 'LBL_ALLOWED_STORAGE_CONDITIONS',
              ),
              29 => 
              array (
                'name' => 'allowed_storage_medium',
                'label' => 'LBL_ALLOWED_STORAGE_MEDIUM',
              ),
              30 => 
              array (
                'name' => 'storage_condition',
                'label' => 'LBL_STORAGE_CONDITION',
              ),
              31 => 
              array (
                'name' => 'storage_medium',
                'label' => 'LBL_STORAGE_MEDIUM',
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'location_type',
                'label' => 'LBL_LOCATION_TYPE',
              ),
              1 => 
              array (
                'span' => 12,
              ),
              2 => 
              array (
                'name' => 'location_building',
                'label' => 'LBL_LOCATION_BUILDING',
              ),
              3 => 
              array (
                'name' => 'location_room',
                'studio' => 'visible',
                'label' => 'LBL_LOCATION_ROOM',
              ),
              4 => 
              array (
                'name' => 'location_equipment',
                'studio' => 'visible',
                'label' => 'LBL_LOCATION_EQUIPMENT',
              ),
              5 => 
              array (
                'span' => 12,
              ),
              6 => 
              array (
                'name' => 'location_shelf',
                'label' => 'LBL_LOCATION_SHELF',
              ),
              7 => 
              array (
                'span' => 12,
              ),
              8 => 
              array (
                'name' => 'location_cabinet',
                'label' => 'LBL_LOCATION_CABINET',
              ),
              9 => 
              array (
                'span' => 12,
              ),
              10 => 
              array (
                'name' => 'ship_to_contact',
                'studio' => 'visible',
                'label' => 'LBL_SHIP_TO_CONTACT',
              ),
              11 => 
              array (
                'name' => 'ship_to_company',
                'studio' => 'visible',
                'label' => 'LBL_SHIP_TO_COMPANY',
              ),
              12 => 
              array (
                'name' => 'ship_to_address',
                'studio' => 'visible',
                'label' => 'LBL_SHIP_TO_ADDRESS',
              ),
              13 => 
              array (
                'span' => 12,
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'retention_start_date',
                'label' => 'LBL_RETENTION_START_DATE',
              ),
              1 => 
              array (
                'name' => 'retention_interval_days',
                'label' => 'LBL_RETENTION_INTERVAL_DAYS',
              ),
              2 => 
              array (
                'name' => 'planned_discard_date',
                'label' => 'LBL_PLANNED_DISCARD_DATE',
              ),
              3 => 
              array (
                'span' => 12,
              ),
              4 => 
              array (
                'name' => 'actual_discard_date',
                'label' => 'LBL_ACTUAL_DISCARD_DATE',
              ),
              5 => 
              array (
                'span' => 12,
              ),
            ),
          ),
          5 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL4',
            'label' => 'LBL_RECORDVIEW_PANEL4',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'description',
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);
