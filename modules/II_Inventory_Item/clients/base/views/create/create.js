({
    extendsFrom: 'CreateView',
    initialize: function (options) {

        this._super('initialize', [options]);
        this.model.on("change:related_to_c", this.function_test_system_action, this);
        //this.model.on("change:product_name", this.function_get_product_barcode, this);
        this.model.on('change:expiration_date', this.function_expiration_date_cal, this);
        this.model.on('change:expiration_date_time', this.function_expiration_date_time_cal, this);
        this.model.on('change:category', this.function_category_change, this);
        this.on('render', _.bind(this.onload_function, this));

        this.model.on('change:type_2', this.function_type_2_change, this);
        this.model.on('change:location_building', this.function_location_building, this);
        this.model.on('change:location_equipment', this.function_location_equipment, this);
        this.model.on("change:status", this.emplty_location_tab_fields, this);

        $(document).on("keyup", ".ts_selection input.search-name", function () {

            var name_search = $('.ts_selection input.search-name').val();
            console.log('name_search',name_search);
            var workProduct = $('.ts_selection input.search-name').attr('wpId');
      
            $(".ts_selection").css('width', "100%");
            $(".ts_selection .table").html('');
            $('.iiloadmore').html('');
            $('.loading-ii').remove();
            //$("div.main-pane.ts_selection div.main-content").html('');
            //if(name_search != ""){
                //console.log('111')
                app.api.call("create", "rest/v10/get_wpa_test_system_search", {
                    workProduct: workProduct,
                    searchText: name_search,
                    record_count: 0,
                }, {
                    success: function (tsRecords) {                        
                        var tsModels = [];
                        if (tsRecords.records && tsRecords.records.length > 0) {
                            tsModels = tsRecords.records;
                            setTimeout(function () {
                                if ($('button[data-action="show-more-ii-sidecar"]').length == 0) {
                                    if (tsRecords.records.length > 19) {
                                        $("div.main-pane.ts_selection div.main-content").append('<div class="iiloadmore"><button record_count="20" wpId="' + workProduct + '" data-action="show-more-ii-sidecar" class="btn btn-link btn-invisible more padded get-ii-sidecar-count">More Inventory Item...</button></div><div class="block-footer loading-ii hide"><div class="loading">Loading...</div></div>');
                                    }
                                }
                            }, 500);
                        } 
                        
                        tsView.collection.reset();
                        tsView.collection.add(tsModels);
                        tsView.render();

                        $(".ts_selection .dataTable td:first-child span").css("margin-left", "2px");
                        $(".ts_selection .dataTable td:nth-child(2) span").css("line-height", "21px");
                        $(".ts_selection .btn.checkall").css("border", "none");
                        $(".ts_selection table th").removeClass("orderByname");
                        $(".ts_selection table th").removeClass("orderByusda_id_c");
                        $(".ts_selection table th.sorting").css("cursor", "unset");
                        $('.ts_selection input[name="check"]').prop('checked', false);
                    }.bind(this),
                    error: function (error) {
                        app.alert.show("ts_wp_error", {
                            level: "error",
                            messages: "Error retrieving Test System of Work Products",
                            autoClose: true
                        });
                    }
                });
                $('.ts_selection input[name="check"]').prop('checked', false);
            //}      
        });

        $(document).on("click", ".sicon-close.add-on", function () {
            var workProduct = $('.ts_selection input.search-name').attr('wpId');
            app.api.call("create", "rest/v10/get_wpa_test_system", {
                workProduct: workProduct,
                record_count: 0,
            }, {
                success: function (tsRecords) {  
                    console.log('tsRecords',tsRecords);                  
                    var tsModels = [];
                    if (tsRecords.records && tsRecords.records.length > 0) {
                        tsModels = tsRecords.records;
                        setTimeout(function () {
                            if ($('button[data-action="show-more-ii-sidecar"]').length == 0) {
                                if (tsRecords.records.length > 19) {
                                    $("div.main-pane.ts_selection div.main-content").append('<div class="iiloadmore"><button record_count="20" wpId="' + workProduct + '" data-action="show-more-ii-sidecar" class="btn btn-link btn-invisible more padded get-ii-sidecar-count">More Inventory Item...</button></div><div class="block-footer loading-ii hide"><div class="loading">Loading...</div></div>');
                                }
                            }
                        }, 500);
                    } 
                    
                    tsView.collection.reset();
                    tsView.collection.add(tsModels);
                    tsView.render();

                    $(".ts_selection .dataTable td:first-child span").css("margin-left", "2px");
                    $(".ts_selection .dataTable td:nth-child(2) span").css("line-height", "21px");
                    $(".ts_selection .btn.checkall").css("border", "none");
                    $(".ts_selection table th").removeClass("orderByname");
                    $(".ts_selection table th").removeClass("orderByusda_id_c");
                    $(".ts_selection table th.sorting").css("cursor", "unset");
                    $('.ts_selection input[name="check"]').prop('checked', false);
                }.bind(this),
                error: function (error) {
                    app.alert.show("ts_wp_error", {
                        level: "error",
                        messages: "Error retrieving Test System of Work Products",
                        autoClose: true
                    });
                }
            });
            $('.ts_selection input[name="check"]').prop('checked', false);
          });
    },
    /**#1572 : hide ts on multiple test system for create view */
    function_test_system_action: function () {
        var relatedto = this.model.get('related_to_c');
        if (relatedto == 'Multiple Test Systems') {
            $('div[data-name="anml_animals_ii_inventory_item_1_name"]').hide();
        }
        else if (relatedto == 'Single Test System') {
            $('div[data-name="anml_animals_ii_inventory_item_1_name"]').show();
        }
    },

    /* #1194 Inventory Items - New Location fields*/
    emplty_location_tab_fields: function () {
        var self = this;
        var status = this.model.get('status');

        if (status == "Discarded" || status == "Obsolete" || status == "Exhausted") {

            this.model.set('location_type', null);
            this.model.set('location_building', null);
            this.model.set('rms_room_id_c', null);
            this.model.set('location_room', null);
            this.model.set('equip_equipment_id_c', null);
            this.model.set('location_equipment', null);
            this.model.set('location_bin_c', null);
            this.model.set('location_cubby_c', null);
            this.model.set('location_rack_c', null);

            this.model.set('location_shelf', null);
            this.model.set('location_cabinet', null);

            this.model.set('location_values_c', null);
            this.model.set('location_equi_serial_no_hide_c', "");
            this.model.set('test_ii_c', null);

            this.model.set("ca_company_address_id_c", null);
            this.model.set("ship_to_address", null);

            this.model.set("account_id_c", null);
            this.model.set("ship_to_company", null);

            this.model.set("contact_id_c", null);
            this.model.set("ship_to_contact", null);
        }

    },
    function_location_building: function () {
        if (this.model.get('location_type') == "Known") {
            this.model.set('location_room', null);
            this.model.set('rms_room_id_c', null);
            this.model.set('location_equipment', null);
            this.model.set('equip_equipment_id_c', null);
            if (this.model.get('location_building') == "") {
                this.model.set('location_shelf', null);
                this.model.set('location_cabinet', null);
            }
        }
    },
    function_type_2_change: function () {
        var type2 = this.model.get("type_2");
        if (type2 == "Equipment Facility Record" || type2 == "Block" || type2 == "Culture" || type2 == "Slide" || type2 == "Tissue") {
            $('div[data-name="subtype"]').hide();
            this.model.fields.subtype.readonly = true;
        } else {
            $('div[data-name="subtype"]').show();
            this.model.fields.subtype.readonly = false;
        }

        if (type2 == "Other") {
            this.model.set('test_type_c', "Other");
        } else {
            this.model.set('test_type_c', '');
        }

    },
    onload_function: function () {
        var category = this.model.get('category');
        var relatedto = this.model.get('related_to_c');
        if (category == 'Product' && (relatedto == 'Purchase Order Item' || relatedto == 'Order Request Item')) {
            $('div[data-name="type_2"]').css('pointer-events', 'none');
        } else {
            $('div[data-name="type_2"]').css('pointer-events', 'unset');
        }


        var uniqueIpDropDown_list = app.lang.getAppListStrings('im_location_type_list');
        Object.keys(uniqueIpDropDown_list).forEach(function (key) {
            delete uniqueIpDropDown_list[key];
        });

        var known_unknown_list = app.lang.getAppListStrings('known_unknown_list');

        this.model.fields['location_type'].options = known_unknown_list;

        this._render();
        //this.render();  

    },
    function_category_change: function () {
        var category = this.model.get('category');

        if (category != 'Product') {
            this.model.set("product_name", null);
            this.model.set("prod_product_id_c", null);
        }

    },
    function_expiration_date_cal: function () {
        var expiration_date = this.model.get('expiration_date');
        if (expiration_date != '') {
            var self = this;
            $.ajax({
                type: 'GET',
                dataType: "JSON",
                url: "index.php?entryPoint=getInventoryItem",
                data: { expiration_date: expiration_date, post_action: "expiration_date_data" },
                success: function (data) {
                    if (data.days_to_expiration != "") {
                        self.model.set('days_to_expiration', data.days_to_expiration);
                        $('div[data-name="days_to_expiration"]').css('pointer-events', 'none');

                    } else {
                        self.model.set('days_to_expiration', "0");
                        $('div[data-name="days_to_expiration"]').css('pointer-events', 'unset');
                    }
                }
            });
        }
    },
    function_expiration_date_time_cal: function () {
        var expiration_date_time = this.model.get('expiration_date_time');
        if (expiration_date_time != '') {
            var self = this;
            $.ajax({
                type: 'GET',
                dataType: "JSON",
                url: "index.php?entryPoint=getInventoryItem",
                data: { expiration_date_time: expiration_date_time, post_action: "expiration_date_time_data" },
                success: function (data) {
                    if (data.days_to_expiration != "") {
                        self.model.set('days_to_expiration', data.days_to_expiration);
                        $('div[data-name="days_to_expiration"]').css('pointer-events', 'none');

                    } else {
                        self.model.set('days_to_expiration', "0");
                        $('div[data-name="days_to_expiration"]').css('pointer-events', 'unset');
                    }
                }
            });
        }
    },
    function_get_product_barcode: function () {
        var category = this.model.get('category');
        var product_name_c = this.model.get('product_name');
        var related_to = this.model.get('related_to_c');
        if (category == 'Product' && product_name_c != "") {

            var self = this;
            $.ajax({
                type: 'GET',
                dataType: "JSON",
                url: "index.php?entryPoint=getInventoryItem",
                data: { product_name: product_name_c, post_action: "product_data" },
                success: function (data) {
                    if (data.external_barcode != "" && data.external_barcode != null) {
                        self.model.set('external_barcode', data.external_barcode);

                        self.model.set('unique_id_type', 'External');
                        $('span[data-fieldname="unique_id_type"] span.select2-chosen').text("External & Internal");

                        var uniqueIpDropDown_list = app.lang.getAppListStrings('unique_id_type_list');
                        Object.keys(uniqueIpDropDown_list).forEach(function (key) {
                            delete uniqueIpDropDown_list[key];
                        });
                        uniqueIpDropDown_list[''] = "";
                        uniqueIpDropDown_list['External'] = "External & Internal";
                        self.model.fields['unique_id_type'].options = uniqueIpDropDown_list;
                        self.render();
                        $('div[data-name="external_barcode"]').css('pointer-events', 'none');


                    } else {
                        self.model.set('external_barcode', "");

                        self.model.set('unique_id_type', "");
                        $('span[data-fieldname="unique_id_type"] span.select2-chosen').text("Required");

                        var uniqueIpDropDown_list = app.lang.getAppListStrings('unique_id_type_list');
                        Object.keys(uniqueIpDropDown_list).forEach(function (key) {
                            delete uniqueIpDropDown_list[key];
                        });
                        uniqueIpDropDown_list[''] = "";
                        uniqueIpDropDown_list['Internal'] = "Internal Only";
                        uniqueIpDropDown_list['External'] = "External & Internal";
                        self.model.fields['unique_id_type'].options = uniqueIpDropDown_list;
                        self.render();
                        $('div[data-name="external_barcode"]').css('pointer-events', 'unset');
                    }
                }
            });
        }
    },
    function_location_equipment: function () {
        var self = this;
        var equip_equipment_id = this.model.get('equip_equipment_id_c');
        if (equip_equipment_id) {
            App.api.call("get", "rest/v10/Equip_Equipment/" + equip_equipment_id + "?fields=serial_number_c", null, {
                success: function (data) {
                    self.model.set("location_equi_serial_no_hide_c", data.serial_number_c);
                }
            });
        }

    },
})