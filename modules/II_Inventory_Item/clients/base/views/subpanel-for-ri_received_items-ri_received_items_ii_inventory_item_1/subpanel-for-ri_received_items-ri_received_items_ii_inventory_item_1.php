<?php
// created: 2021-09-01 11:51:56
$viewdefs['II_Inventory_Item']['base']['view']['subpanel-for-ri_received_items-ri_received_items_ii_inventory_item_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'category',
          'label' => 'LBL_CATEGORY',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'type_2',
          'label' => 'LBL_TYPE_2',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'quantity_2_c',
          'label' => 'LBL_QUANTITY_2',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'lot_number_c',
          'label' => 'LBL_LOT_NUMBER',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'expiration_date',
          'label' => 'LBL_EXPIRATION_DATE',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'product_name',
          'label' => 'LBL_PRODUCT_NAME',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'manufacturer_c',
          'label' => 'LBL_MANUFACTURER',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'product_id_2_c',
          'label' => 'LBL_PRODUCT_ID_2',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);