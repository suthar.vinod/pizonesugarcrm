({
    extendsFrom: 'MultiSelectionListView',

    allowedFields: ["name", "work_product_status_c"],

    initialize: function (options) {
        this._super("initialize", arguments);
        this.collection.reset();
    },

    render: function () {
        this._super("render", arguments);
        this.layout.$el.parent().addClass("span12 wp_selection");
        this.layout.$el.parent().css("width","100%");
    },

    addActions: function () {
        this._super('addActions');
        this.rightColumns = [];

        this.meta.panels[0].fields = _.filter(this.meta.panels[0].fields, function (field) {
            return this.allowedFields.indexOf(field.name) !== -1
        }.bind(this));
    }
})