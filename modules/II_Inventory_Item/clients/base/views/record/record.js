({
    extendsFrom: 'RecordView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.onload_function, this);
        this.model.on('change:expiration_date', this.function_expiration_date_cal, this);
        //this.model.on("change:product_name", this.function_get_product_barcode, this);
        this.model.on('change:expiration_date_time', this.function_expiration_date_time_cal, this);
        this.model.on('change:expiration_date_time', this.function_expiration_date_time_cal, this);
        this.context.on('button:cancel_button:click', this.cancel_button, this);
        this.context.on('button:edit_button:click', this.edit_button, this);
        this.model.on('change:location_type', this.function_location_type, this);
        //this.model.on('change:location_building', this.function_location_building, this);
        this.model.on("change:location_equipment", this.function_location_equipment, this);
        this.model.on("change:status", this.emplty_location_tab_fields, this);
        this.model.once("sync",
            function () {
                this.model.on(
                    "change:location_building",
                    this.function_location_building,
                    this
                );
            },
            this
        );
    },

    /* #1194 Inventory Items - New Location fields*/
    emplty_location_tab_fields: function () {
        var self = this;
        var status = this.model.get('status');

        if (status == "Discarded" || status == "Obsolete" || status == "Exhausted") {
            this.model.set('location_type', "");
            this.model.set('location_building', "");
            this.model.set('rms_room_id_c', "");
            this.model.set('location_room', "");
            this.model.set('equip_equipment_id_c', "");
            this.model.set('location_equipment', "");
            this.model.set('location_bin_c', "");
            this.model.set('location_cubby_c', "");
            this.model.set('location_rack_c', "");

            this.model.set('location_shelf', "");
            this.model.set('location_cabinet', "");

            this.model.set('location_values_c', "");
            this.model.set('location_equi_serial_no_hide_c', "");
            this.model.set('test_ii_c', "");

            this.model.set("ca_company_address_id_c", "");
            this.model.set("ship_to_address", "");

            this.model.set("account_id_c", "");
            this.model.set("ship_to_company", "");

            this.model.set("contact_id_c", "");
            this.model.set("ship_to_contact", "");


        }

    },

    /* #1117 Inventory Items - New Location fields*/
    function_location_equipment: function () {
        var self = this;
        var equip_equipment_id = this.model.get('equip_equipment_id_c');
        if (equip_equipment_id) {
            App.api.call("get", "rest/v10/Equip_Equipment/" + equip_equipment_id + "?fields=serial_number_c", null, {
                success: function (data) {
                    self.model.set("location_equi_serial_no_hide_c", data.serial_number_c);
                }
            });
        }

    },

    function_location_building: function () {
        if (this.model.get('location_type') == "Known") {
            this.model.set('location_room', "");
            this.model.set('rms_room_id_c', "");
            this.model.set('location_equipment', "");
            this.model.set('equip_equipment_id_c', "");
            if (this.model.get('location_building') == "") {
                this.model.set('location_shelf', "");
                this.model.set('location_cabinet', "");
            }
        }
    },
    edit_button: function () {
        var self = this;
        var related_to_c = self.model.get('related_to_c');
        setTimeout(function () {
            if (self.model.get('location_type') != "Known" && self.model.get('location_type') != "Unknown") {
                self.$el.find('.record-edit-link-wrapper[data-name=location_type]').addClass('hide');
                self.$el.find('input[name="location_type"]').prop('disabled', true);
            }

            if (self.model.get('location_type') != "Known") {
                $('span[data-fieldname="location_room"]').css('pointer-events', 'none');
                $('span[data-fieldname="location_equipment"]').css('pointer-events', 'none');

                $('span[data-fieldname="location_building"] abbr.select2-search-choice-close').remove();
                $('span[data-fieldname="location_building"] .select2-arrow').remove();

                $('span[data-fieldname="location_room"] abbr.select2-search-choice-close').remove();
                $('span[data-fieldname="location_room"] .select2-arrow').remove();

                $('span[data-fieldname="location_equipment"] abbr.select2-search-choice-close').remove();
                $('span[data-fieldname="location_equipment"] .select2-arrow').remove();

                $('span[data-fieldname="location_shelf"] abbr.select2-search-choice-close').remove();
                $('span[data-fieldname="location_shelf"] .select2-arrow').remove();

                $('span[data-fieldname="location_cabinet"] abbr.select2-search-choice-close').remove();
                $('span[data-fieldname="location_cabinet"] .select2-arrow').remove();

                $('span[data-fieldname="location_building"] .edit').addClass('disabled');
                $('span[data-fieldname="location_building"] .edit div:first-child').addClass('select2-container-disabled');

                $('span[data-fieldname="location_room"] .edit').addClass('disabled');
                $('span[data-fieldname="location_room"] .edit div:first-child').addClass('select2-container-disabled');
                $('span[data-fieldname="location_room"] .edit div:first-child').css('pointer-events', 'none');

                $('span[data-fieldname="location_equipment"] .edit').addClass('disabled');
                $('span[data-fieldname="location_equipment"] .edit div:first-child').addClass('select2-container-disabled');
                $('span[data-fieldname="location_equipment"] .edit div:first-child').css('pointer-events', 'none');

                $('span[data-fieldname="location_shelf"] .edit').addClass('disabled');
                $('span[data-fieldname="location_shelf"] .edit div:first-child').addClass('select2-container-disabled');

                $('span[data-fieldname="location_cabinet"] .edit').addClass('disabled');
                $('span[data-fieldname="location_cabinet"] .edit div:first-child').addClass('select2-container-disabled');
            }
            /* 26 Feb 2021 : #390 */
            if (related_to_c == 'Purchase Order Item' || related_to_c == 'Order Request Item') {

                $('input[name="owner"]').prop('disabled', true);
                $('input[name="product_name"]').prop('disabled', true);
                $('input[name="type_2"]').prop('disabled', true);
                $('input[name="manufacturer_c"]').prop('disabled', true);
                $('input[name="concentration_c"]').prop('disabled', true);
                $('input[name="concentration_unit_c"]').prop('disabled', true);
                $('input[name="product_id_2_c"]').prop('disabled', true);
                $('input[name="external_barcode"]').prop('disabled', true);
                $('input[name="allowed_storage_conditions"]').prop('disabled', true);
                $('input[name="subtype"]').prop('disabled', true);
                $('input[name="category"]').prop('disabled', true);
            } else {
                $('input[name="owner"]').prop('disabled', false);
                $('input[name="product_name"]').prop('disabled', false);
                $('input[name="manufacturer_c"]').prop('disabled', false);
                $('input[name="concentration_c"]').prop('disabled', false);
                $('input[name="concentration_unit_c"]').prop('disabled', false);
                $('input[name="product_id_2_c"]').prop('disabled', false);
                $('input[name="external_barcode"]').prop('disabled', false);
                $('input[name="allowed_storage_conditions"]').prop('disabled', false);
            }
            /* ////////  26 Feb 2021 : #390 //////// */


            if (self.model.get('status') == "Exhausted") {
                $('input[name="owner"]').prop('disabled', true);
                $('input[name="product_name"]').prop('disabled', true);
                $('input[name="manufacturer_c"]').prop('disabled', true);
                $('input[name="concentration_c"]').prop('disabled', true);
                $('input[name="concentration_unit_c"]').prop('disabled', true);
                $('input[name="product_id_2_c"]').prop('disabled', true);
                $('input[name="external_barcode"]').prop('disabled', true);
                $('input[name="allowed_storage_conditions"]').prop('disabled', true);

            } else {
                $('input[name="owner"]').prop('disabled', false);
                $('input[name="product_name"]').prop('disabled', false);
                $('input[name="manufacturer_c"]').prop('disabled', false);
                $('input[name="concentration_c"]').prop('disabled', false);
                $('input[name="concentration_unit_c"]').prop('disabled', false);
                $('input[name="product_id_2_c"]').prop('disabled', false);
                $('input[name="external_barcode"]').prop('disabled', false);
                $('input[name="allowed_storage_conditions"]').prop('disabled', false);
            }

        }, 300);
    },
    cancel_button: function () {
          /**15 nov 2021 : 1117 prod bug fix */
          var location_type = this.model.get('location_type');
          var location_equipment = this.model.get('location_equipment'); 
          var location_bin_c = this.model.get('location_bin_c'); 
          var location_cubby_c = this.model.get('location_cubby_c'); 
          var location_rack_c = this.model.get('location_rack_c'); 
            $('span[data-fieldname="location_room"]').css('pointer-events', 'all');
            $('span[data-fieldname="location_equipment"]').css('pointer-events', 'all');
            var typeArr = [];

            var ID = this.model.get('id');
            var self = this;
            App.api.call("get", "rest/v10/II_Inventory_Item/" + ID + "?fields=type_2", null, {
                success: function (typeData) {
                    var ProductTypeArr = App.lang.getAppListStrings("product_category_list");
                    var InventoryTypeArr = App.lang.getAppListStrings("inventory_item_type_list");
                    typeArr = $.extend(ProductTypeArr, InventoryTypeArr);
                    var typeNM = typeArr[typeData.type_2];
                    self.model.set('type_2', typeData.type_2);

                }
            });
             /**15 nov 2021 : 1117 prod bug fix */
        if((location_equipment=='Freezer -80C 51014222') && (location_bin_c!='' || location_cubby_c!='' ||location_rack_c!='' ))
        {
            
            $('div[data-name="location_bin_c"]').removeClass('vis_action_hidden');
            $('span[data-fieldname="location_bin_c"] span').removeClass('vis_action_hidden');
            $('div[data-name="location_bin_c"]').css('pointer-events', 'none');
            $('span[data-fieldname="location_bin_c"]').css('pointer-events', 'none');
            $('div[data-name="location_bin_c"]').parents('.row-fluid').css('display', 'block');
            
            $('div[data-name="location_cubby_c"]').removeClass('vis_action_hidden');
            $('span[data-fieldname="location_cubby_c"] span').removeClass('vis_action_hidden');
            $('div[data-name="location_cubby_c"]').css('pointer-events', 'none');
            $('span[data-fieldname="location_cubby_c"]').css('pointer-events', 'none');
            $('div[data-name="location_cubby_c"]').parents('.row-fluid').css('display', 'block');

            $('div[data-name="location_rack_c"]').removeClass('vis_action_hidden');
            $('span[data-fieldname="location_rack_c"] span').removeClass('vis_action_hidden');
            $('div[data-name="location_rack_c"]').css('pointer-events', 'none');
            $('span[data-fieldname="location_rack_c"]').css('pointer-events', 'none');
            $('div[data-name="location_rack_c"]').parents('.row-fluid').css('display', 'block');
        }
    },
    function_location_type: function () {

        if (this.model.get('location_type') == "Known") {
            $('div[data-name="location_building"]').removeAttr("style");
            $('span[data-fieldname="location_building"]').removeAttr("style");
            $('div[data-name="location_room"]').removeAttr("style");
            $('span[data-fieldname="location_room"]').removeAttr("style");
            $('div[data-name="location_equipment"]').removeAttr("style");
            $('span[data-fieldname="location_equipment"]').removeAttr("style");
            $('div[data-name="location_shelf"]').removeAttr("style");
            $('span[data-fieldname="location_shelf"]').removeAttr("style");
            $('div[data-name="location_cabinet"]').removeAttr("style");
            $('span[data-fieldname="location_cabinet"]').removeAttr("style");
        }
    },
    onload_function: function () {

        var quantity_c = this.model.get('quantity_c');
        var external_barcode = this.model.get('external_barcode');

        $('span[data-name="name"]').css('pointer-events', 'none');
        $('div[data-name="type_2"]').css('pointer-events', 'none');
        $('span[data-fieldname="type_2"]').css('pointer-events', 'none');
        $('div[data-name="category"]').css('pointer-events', 'none');
        $('span[data-fieldname="category"]').css('pointer-events', 'none');
        $('div[data-name="subtype"]').css('pointer-events', 'none');
        $('span[data-fieldname="subtype"]').css('pointer-events', 'none');
        $('div[data-name="product_name"]').css('pointer-events', 'none');
        $('span[data-fieldname="product_name"]').css('pointer-events', 'none');
        /*01 Feb 2021 : #390 */
        $('div[data-name="owner"]').css('pointer-events', 'none');
        $('span[data-fieldname="owner"]').css('pointer-events', 'none');

        if (this.model.get('test_ii_c') != "" || this.model.get('location_type') != "Known") {
            $('div[data-name="location_building"]').css('pointer-events', 'none');
            $('span[data-fieldname="location_building"]').css('pointer-events', 'none');
            $('div[data-name="location_room"]').css('pointer-events', 'none');
            $('span[data-fieldname="location_room"]').css('pointer-events', 'all');
            $('span[data-fieldname="location_room"] div').css('pointer-events', 'none');
            $('div[data-name="location_equipment"]').css('pointer-events', 'none');
            $('span[data-fieldname="location_equipment"]').css('pointer-events', 'all');
            $('span[data-fieldname="location_equipment"] div').css('pointer-events', 'none');
            $('div[data-name="location_shelf"]').css('pointer-events', 'none');
            $('span[data-fieldname="location_shelf"]').css('pointer-events', 'none');
            $('div[data-name="location_cabinet"]').css('pointer-events', 'none');
            $('span[data-fieldname="location_cabinet"]').css('pointer-events', 'none');
        } else {
            $('div[data-name="location_building"]').css('pointer-events', 'unset');
            $('span[data-fieldname="location_building"]').css('pointer-events', 'unset');
            $('div[data-name="location_room"]').css('pointer-events', 'unset');
            $('span[data-fieldname="location_room"]').css('pointer-events', 'unset');
            $('div[data-name="location_equipment"]').css('pointer-events', 'unset');
            $('span[data-fieldname="location_equipment"]').css('pointer-events', 'unset');
            $('div[data-name="location_shelf"]').css('pointer-events', 'unset');
            $('span[data-fieldname="location_shelf"]').css('pointer-events', 'unset');
            $('div[data-name="location_cabinet"]').css('pointer-events', 'unset');
            $('span[data-fieldname="location_cabinet"]').css('pointer-events', 'unset');
        }

        if (quantity_c != "") {
            this.$('div[data-name=quantity_c]').hide();
            this.$('span[data-fieldname=quantity_c]').hide();
        }

        if (external_barcode != "") {
            $('div[data-name="external_barcode"]').css('pointer-events', 'none');
        }

        $('div[data-name="ori_order_request_item_ii_inventory_item_1_name"]').hide();
        $('div[data-name="poi_purchase_order_item_ii_inventory_item_1_name"]').hide();

        var related_to = this.model.get('related_to_c');
        if (related_to == 'Purchase Order Item') {
            $('div[data-name="poi_purchase_order_item_ii_inventory_item_1_name"]').css('pointer-events', 'none');
            $('span[data-fieldname="poi_purchase_order_item_ii_inventory_item_1_name"]').css('pointer-events', 'all');
            $('div[data-name="poi_purchase_order_item_ii_inventory_item_1_name"]').show();

            /**10 nov 2021 : 390 bug fix */
            $('div[data-name="owner"]').css('pointer-events', 'none');
            $('div[data-name="product_name"]').css('pointer-events', 'none');
            $('div[data-name="type_2"]').css('pointer-events', 'none');
            $('div[data-name="manufacturer_c"]').css('pointer-events', 'none');
            $('div[data-name="concentration_c"]').css('pointer-events', 'none');
            $('div[data-name="concentration_unit_c"]').css('pointer-events', 'none');
            $('div[data-name="product_id_2_c"]').css('pointer-events', 'none');
            $('div[data-name="external_barcode"]').css('pointer-events', 'none');
            $('div[data-name="allowed_storage_conditions"]').css('pointer-events', 'none');
            $('div[data-name="subtype"]').css('pointer-events', 'none');
            $('div[data-name="category"]').css('pointer-events', 'none');
           
        }
        if (related_to == 'Order Request Item') {
            $('div[data-name="ori_order_request_item_ii_inventory_item_1_name"]').css('pointer-events', 'none');
            $('span[data-fieldname="ori_order_request_item_ii_inventory_item_1_name"]').css('pointer-events', 'all');
            $('div[data-name="ori_order_request_item_ii_inventory_item_1_name"]').show();
             /**10 nov 2021 : 390 bug fix */
            $('div[data-name="owner"]').css('pointer-events', 'none');
            $('div[data-name="product_name"]').css('pointer-events', 'none');
            $('div[data-name="type_2"]').css('pointer-events', 'none');
            $('div[data-name="manufacturer_c"]').css('pointer-events', 'none');
            $('div[data-name="concentration_c"]').css('pointer-events', 'none');
            $('div[data-name="concentration_unit_c"]').css('pointer-events', 'none');
            $('div[data-name="product_id_2_c"]').css('pointer-events', 'none');
            $('div[data-name="external_barcode"]').css('pointer-events', 'none');
            $('div[data-name="allowed_storage_conditions"]').css('pointer-events', 'none');
            $('div[data-name="subtype"]').css('pointer-events', 'none');
            $('div[data-name="category"]').css('pointer-events', 'none');
        }

        if (related_to == 'Multiple Test Systems') {
            $('div[data-name="anml_animals_ii_inventory_item_1_name"]').removeClass('vis_action_hidden');
            $('span[data-fieldname="anml_animals_ii_inventory_item_1_name"] .detail').removeClass('vis_action_hidden');
        }

        if ((related_to == 'Purchase Order Item' || related_to == 'Order Request Item') && this.model.get('m01_sales_ii_inventory_item_1_name') != "") {
            $('div[data-name=m01_sales_ii_inventory_item_1_name]').removeClass('vis_action_hidden ');
            $('span[data-fieldname=m01_sales_ii_inventory_item_1_name] span').removeClass('vis_action_hidden ');
            $('div[data-name="m01_sales_ii_inventory_item_1_name"]').parents('.panel_body').css('display', 'block');
        }
        if ((related_to == 'Purchase Order Item' || related_to == 'Order Request Item') && this.model.get('m03_work_product_ii_inventory_item_1_name') != "") {
            $('div[data-name=m03_work_product_ii_inventory_item_1_name]').removeClass('vis_action_hidden ');
            $('span[data-fieldname=m03_work_product_ii_inventory_item_1_name] span').removeClass('vis_action_hidden ');
            $('div[data-name="m03_work_product_ii_inventory_item_1_name"]').parents('.panel_body').css('display', 'block');
        }

        var subtypeArr = [];
        var subtypeTissueArr = App.lang.getAppListStrings("tissues_list");
        var subtypeSpecimanArr = App.lang.getAppListStrings("inventory_item_subtype_list");
        var subtypeProductArr = App.lang.getAppListStrings("pro_subtype_list");

        var subtypeTS = $.extend(subtypeTissueArr, subtypeSpecimanArr);
        subtypeArr = $.extend(subtypeTS, subtypeProductArr);


        var subtypeNM = subtypeArr[this.model.get('subtype')];

        if (subtypeNM == 'undefined') {
            subtypeNM = '';
        }

        if (subtypeNM != 'undefined') {
            this.model.set('subtype', this.model.get('subtype'))
            $('span[data-fieldname="subtype"] span.detail').html('');
            $('span[data-fieldname="subtype"] span.detail').html('<div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + subtypeNM + '">' + subtypeNM + '</div>');
        }
        var typeArr = [];


        var ID = this.model.get('id');
        var self = this;
        App.api.call("get", "rest/v10/II_Inventory_Item/" + ID + "?fields=type_2,rms_room_id_c,location_room,equip_equipment_id_c,location_equipment", null, {
            success: function (typeData) {
                var ProductTypeArr = App.lang.getAppListStrings("product_category_list");
                var InventoryTypeArr = App.lang.getAppListStrings("inventory_item_type_list");
                typeArr = $.extend(ProductTypeArr, InventoryTypeArr);
                var typeNM = typeArr[typeData.type_2];
                //self.model.set('type_2', typeData.type_2);
                $('span[data-fieldname="type_2"] span.detail').html('');
                $('span[data-fieldname="type_2"] span.detail').html('<div class="ellipsis_inline" data-placement="bottom" title="' + typeNM + '">' + typeNM + '</div>');
                /*#27 Sep 2021 : to fix the issue of warning alert box on edit record by without changing the value #1572 */
                /*
                self.model.set('rms_room_id_c', typeData.rms_room_id_c);
                self.model.set('location_room', typeData.location_room);
                self.model.set('equip_equipment_id_c', typeData.equip_equipment_id_c);
                self.model.set('location_equipment', typeData.location_equipment);*/
            }
        });

        //readonly all location fields 

        app.api.call("get", "rest/v10/II_Inventory_Item/" + this.model.get('id') + "/link/ii_inventory_item_im_inventory_management_1?fields=name,location_type,location_building,location_room,location_shelf,location_cabinet,location_equipment,end_condition,ending_storage_media,ship_to_contact,ship_to_company,ship_to_address&max_num=1&order_by=name:desc", null, {
            success: function (RelatedData) {

                if (RelatedData.records.length > 0) {
                    if (self.model.get('location_type') != "Known") {
                        $('span[data-fieldname="location_building"] abbr.select2-search-choice-close').remove();
                        $('span[data-fieldname="location_building"] .select2-arrow').remove();

                        $('span[data-fieldname="location_room"] abbr.select2-search-choice-close').remove();
                        $('span[data-fieldname="location_room"] .select2-arrow').remove();

                        $('span[data-fieldname="location_equipment"] abbr.select2-search-choice-close').remove();
                        $('span[data-fieldname="location_equipment"] .select2-arrow').remove();

                        $('span[data-fieldname="location_shelf"] abbr.select2-search-choice-close').remove();
                        $('span[data-fieldname="location_shelf"] .select2-arrow').remove();

                        $('span[data-fieldname="location_cabinet"] abbr.select2-search-choice-close').remove();
                        $('span[data-fieldname="location_cabinet"] .select2-arrow').remove();

                        $('span[data-fieldname="location_building"] .edit').addClass('disabled');
                        $('span[data-fieldname="location_building"] .edit div:first-child').addClass('select2-container-disabled');

                        $('span[data-fieldname="location_room"] .edit').addClass('disabled');
                        $('span[data-fieldname="location_room"] .edit div:first-child').addClass('select2-container-disabled');
                        $('span[data-fieldname="location_room"] .edit div:first-child').css('pointer-events', 'none');

                        $('span[data-fieldname="location_equipment"] .edit').addClass('disabled');
                        $('span[data-fieldname="location_equipment"] .edit div:first-child').addClass('select2-container-disabled');
                        $('span[data-fieldname="location_equipment"] .edit div:first-child').css('pointer-events', 'none');

                        $('span[data-fieldname="location_shelf"] .edit').addClass('disabled');
                        $('span[data-fieldname="location_shelf"] .edit div:first-child').addClass('select2-container-disabled');

                        $('span[data-fieldname="location_cabinet"] .edit').addClass('disabled');
                        $('span[data-fieldname="location_cabinet"] .edit div:first-child').addClass('select2-container-disabled');
                    }
                }

            }
        });

        if (this.model.get('location_type') != "Known" && this.model.get('location_type') != "Unknown") {
            //$('span.record-edit-link-wrapper').addClass('hide');
            this.$el.find('.record-edit-link-wrapper[data-name=location_type]').addClass('hide');
        }
    },
    function_get_product_barcode: function () {
        var category = this.model.get('category');
        var product_name_c = this.model.get('product_name');
        var related_to = this.model.get('related_to_c');

        if (category == 'Product' && product_name_c != "") {
            var unique_id_typeVal = this.model.get('unique_id_type');
            var uniqueArr = new Object();
            uniqueArr["Internal"] = "Internal Only";
            uniqueArr["External"] = "External & Internal";

            var self = this;
            $.ajax({
                type: 'GET',
                dataType: "JSON",
                url: "index.php?entryPoint=getInventoryItem",
                data: { product_name: product_name_c, post_action: "product_data" },
                success: function (data) {
                    if (data.external_barcode != "" && data.external_barcode != null) {
                        self.model.set('external_barcode', data.external_barcode);

                        self.model.set('unique_id_type', 'External');
                        $('span[data-fieldname="unique_id_type"] span.select2-chosen').text("External & Internal");

                        var uniqueIpDropDown_list = app.lang.getAppListStrings('unique_id_type_list');
                        Object.keys(uniqueIpDropDown_list).forEach(function (key) {
                            delete uniqueIpDropDown_list[key];
                        });
                        uniqueIpDropDown_list[''] = "";
                        uniqueIpDropDown_list['External'] = "External & Internal";
                        self.model.fields['unique_id_type'].options = uniqueIpDropDown_list;
                        self.render();

                        self.model.set('unique_id_type', unique_id_typeVal);
                        $('span[data-fieldname="unique_id_type"] span.select2-chosen').text(uniqueArr[unique_id_typeVal]);
                        $('div[data-name="external_barcode"]').css('pointer-events', 'none');


                    } else {
                        self.model.set('external_barcode', "");
                        self.model.set('unique_id_type', "");
                        $('span[data-fieldname="unique_id_type"] span.select2-chosen').text("Required");

                        var uniqueIpDropDown_list = app.lang.getAppListStrings('unique_id_type_list');
                        Object.keys(uniqueIpDropDown_list).forEach(function (key) {
                            delete uniqueIpDropDown_list[key];
                        });
                        uniqueIpDropDown_list[''] = "";
                        uniqueIpDropDown_list['Internal'] = "Internal Only";
                        uniqueIpDropDown_list['External'] = "External & Internal";
                        self.model.fields['unique_id_type'].options = uniqueIpDropDown_list;
                        self.render();

                        self.model.set('unique_id_type', unique_id_typeVal);
                        $('span[data-fieldname="unique_id_type"] span.select2-chosen').text(uniqueArr[unique_id_typeVal]);
                        $('div[data-name="external_barcode"]').css('pointer-events', 'unset');
                    }
                }
            });
        }
    },
    function_expiration_date_cal: function () {
        var expiration_date = this.model.get('expiration_date');
        if (expiration_date != '') {
            var self = this;
            $.ajax({
                type: 'GET',
                dataType: "JSON",
                url: "index.php?entryPoint=getInventoryItem",
                data: { expiration_date: expiration_date, post_action: "expiration_date_data" },
                success: function (data) {
                    if (data.days_to_expiration != "") {
                        self.model.set('days_to_expiration', data.days_to_expiration);
                        $('div[data-name="days_to_expiration"]').css('pointer-events', 'none');

                    } else {
                        self.model.set('days_to_expiration', "0");
                        $('div[data-name="days_to_expiration"]').css('pointer-events', 'unset');
                    }
                }
            });
        }
    },
    function_expiration_date_time_cal: function () {
        var expiration_date_time = this.model.get('expiration_date_time');
        if (expiration_date_time != '') {
            var self = this;
            $.ajax({
                type: 'GET',
                dataType: "JSON",
                url: "index.php?entryPoint=getInventoryItem",
                data: { expiration_date_time: expiration_date_time, post_action: "expiration_date_time_data" },
                success: function (data) {
                    if (data.days_to_expiration != "") {
                        self.model.set('days_to_expiration', data.days_to_expiration);
                        $('div[data-name="days_to_expiration"]').css('pointer-events', 'none');

                    } else {
                        self.model.set('days_to_expiration', "0");
                        $('div[data-name="days_to_expiration"]').css('pointer-events', 'unset');
                    }
                }
            });
        }
    },
})