<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/SetORIStatus.php


$hook_array['after_relationship_delete'][] = Array(
    
    5,
    
    'Update the ORI Status after II delete.',
    
    'custom/src/wsystems/ORI/LogicHooks/SetORIStatus.php',
    
    'SetStatusForORI',
    
    'setStatusMethod'
);

$hook_array['after_relationship_add'][] = Array(
    
    5,
    
    'aUpdate the ORI Status after II add.',
    
    'custom/src/wsystems/ORI/LogicHooks/SetORIStatus.php',
    
    'SetStatusForORI',
    
    'setStatusMethod'
);
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/CalculateReceivedDate.php


$hook_array['after_save'][] = array(
    1,
    'After Save Change Received Date',
    'custom/modules/POI_Purchase_Order_Item/CalculateRelateDate.php',
    'CalculateRelateDate',
    'calculateRelateDateAfterSave',
);


?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/CalculateShipping.php


$hook_array['after_relationship_add'][] = array(
    //Processing index. For sorting the array.
    25,

    //Label. A string value to identify the hook.
    'After relating a Inventory Management record calculate shipping data',

    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateShipping.php',

    //The class the method is in.
    'CalculateShipping',

    //The method to call.
    'calculate',
);

$hook_array['after_relationship_delete'][] = array(
    //Processing index. For sorting the array.
    26,

    //Label. A string value to identify the hook.
    'After relating a Inventory Management record calculate shipping data',

    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateShipping.php',

    //The class the method is in.
    'CalculateShipping',

    //The method to call.
    'calculate',
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/PopulateInventorySubpanelsFromWorkProduct.php


$hook_array['after_relationship_delete'][] = array(
    1,
    'Delete record on unlink action from m03_work_product_ii_inventory_item_2 relationship',
    null,
    'Sugarcrm\\Sugarcrm\\custom\\wsystems\\PopulateInventorySubpanelsFromWorkProduct\\LogicHooks\\IIDeleteRelationshipHook',
    'deleteIISubpanelRelationship',
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/CalculateLocation.php


$hook_array['after_relationship_add'][] = array(
    //Processing index. For sorting the array.
    201,

    //Label. A string value to identify the hook.
    'After relating a Inventory Management record resave the bean so the status field is calculated',

    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateLocation.php',

    //The class the method is in.
    'CalculateLocation',

    //The method to call.
    'calculate',
);

$hook_array['after_relationship_delete'][] = array(
    //Processing index. For sorting the array.
    202,

    //Label. A string value to identify the hook.
    'After relating a Inventory Management record resave the bean so the status field is calculated',

    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateLocation.php',

    //The class the method is in.
    'CalculateLocation',

    //The method to call.
    'calculate',
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/CalculateStatus.php


$hook_array['after_relationship_add'][] = array(
    //Processing index. For sorting the array.
    13,

    //Label. A string value to identify the hook.
    'After relating a Inventory Management record resave the bean so the status field is calculated',

    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateStatus.php',

    //The class the method is in.
    'CalculateStatus',

    //The method to call.
    'calculate',
);

$hook_array['after_relationship_delete'][] = array(
    //Processing index. For sorting the array.
    14,

    //Label. A string value to identify the hook.
    'After relating a Inventory Management record resave the bean so the status field is calculated',

    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateStatus.php',

    //The class the method is in.
    'CalculateStatus',

    //The method to call.
    'calculate',
);
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/II_emailReminder_Hook.php


$hook_array['before_save'][] = Array(
    200, 
    'Send email notification if Current date is with in 14 date from First Procedure date', 
    'custom/modules/II_Inventory_Item/II_emailReminder.php', 
    'II_emailReminder', 
    'sendEmailReminder'
 );


?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/SetName.php


$hook_array['before_save'][] = Array(
    //Processing index. For sorting the array.
    2,
    //Label. A string value to identify the hook.
    'append a sequence of numbers at the name of the Item.',
    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/SetNameSequence.php',
    //The class the method is in.
    'SetNameSequence',
    //The method to call.
    'setName'
);

$hook_array['before_save'][] = array(
    //Processing index. For sorting the array.
    10,
    //Label. A string value to identify the hook.
    'Before Save Change Status',
    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateStatus.php',
    //The class the method is in.
    'CalculateStatus',
    //The method to call.
    'calculateBeforeSave',
);

$hook_array['before_save'][] = array(
    107,
    'Internal Barcode Creation',
    'custom/modules/II_Inventory_Item/customLogicInternalBarcodeHook.php',
    'customLogicInternalBarcodeHook',
    'generateInternalBarcodeId',
);

$hook_array['before_save'][] = array(
    //Processing index. For sorting the array.
    20,
    //Label. A string value to identify the hook.
    'Before Save Change Status',
    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateLocation.php',
    //The class the method is in.
    'CalculateLocation',
    //The method to call.
    'calculateBeforeSave',
);

$hook_array['before_save'][] = array(
    //Processing index. For sorting the array.
    21,
    //Label. A string value to identify the hook.
    'Before Save Change Status',
    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateShipping.php',
    //The class the method is in.
    'CalculateShipping',
    //The method to call.
    'calculateBeforeSave',
);

$hook_array['after_save'][] = array(
    //Processing index. For sorting the array.
    22,
    //Label. A string value to identify the hook.
    'After Save Change Location',
    //The PHP file where your class is located.
    'custom/src/wsystems/InventoryItemCreation/LogicHooks/CalculateLocation.php',
    //The class the method is in.
    'CalculateLocation',
    //The method to call.
    'calculateAfterSave',
);

 $hook_array['after_save'][] = Array(
    51,
    'After Save Change RI Filter',
    'custom/modules/RI_Received_Items/ReceivedItemFilterInII.php',
    'ReceivedItemFilterInIIHook',
    'ReceivedItemFilterInIIMethodIIAfterSave',
 );


?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/LogicHooks/IIFilterInactiveItemsHook.php

//$hook_version = 1;
//$hook_array   = array();

$hook_array['before_filter']   = array();
$hook_array['before_filter'][] = array(
    17,
    'Do not show inactive Items on the multi selection list',
    'custom/src/wsystems/InventoryManagementLinkMultipleItems/LogicHooks/IIFilterInactiveItemsHook.php',
    'IIFilterInactiveItemsHook',
    'filterItems',
);

?>
