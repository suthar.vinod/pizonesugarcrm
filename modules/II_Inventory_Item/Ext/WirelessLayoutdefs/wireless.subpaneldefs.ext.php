<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/WirelessLayoutdefs/ii_inventory_item_im_inventory_management_1_II_Inventory_Item.php

 // created: 2021-11-09 09:39:34
$layout_defs["II_Inventory_Item"]["subpanel_setup"]['ii_inventory_item_im_inventory_management_1'] = array (
  'order' => 100,
  'module' => 'IM_Inventory_Management',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'get_subpanel_data' => 'ii_inventory_item_im_inventory_management_1',
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/WirelessLayoutdefs/m03_work_product_ii_inventory_item_2_II_Inventory_Item.php

 // created: 2021-11-09 10:07:50
$layout_defs["II_Inventory_Item"]["subpanel_setup"]['m03_work_product_ii_inventory_item_2'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'm03_work_product_ii_inventory_item_2',
);

?>
