<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/ri_received_items_ii_inventory_item_1_II_Inventory_Item.php

// created: 2021-11-09 09:52:04
$dictionary["II_Inventory_Item"]["fields"]["ri_received_items_ii_inventory_item_1"] = array (
  'name' => 'ri_received_items_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'ri_received_items_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'RI_Received_Items',
  'bean_name' => 'RI_Received_Items',
  'side' => 'right',
  'vname' => 'LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["ri_received_items_ii_inventory_item_1_name"] = array (
  'name' => 'ri_received_items_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'save' => true,
  'id_name' => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
  'link' => 'ri_received_items_ii_inventory_item_1',
  'table' => 'ri_received_items',
  'module' => 'RI_Received_Items',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["ri_received_items_ii_inventory_item_1ri_received_items_ida"] = array (
  'name' => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
  'link' => 'ri_received_items_ii_inventory_item_1',
  'table' => 'ri_received_items',
  'module' => 'RI_Received_Items',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/ori_order_request_item_ii_inventory_item_1_II_Inventory_Item.php

// created: 2021-02-25 08:49:48
$dictionary["II_Inventory_Item"]["fields"]["ori_order_request_item_ii_inventory_item_1"] = array (
  'name' => 'ori_order_request_item_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'ori_order_request_item_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'side' => 'right',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'ori_order_2123st_item_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["ori_order_request_item_ii_inventory_item_1_name"] = array (
  'name' => 'ori_order_request_item_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'save' => true,
  'id_name' => 'ori_order_2123st_item_ida',
  'link' => 'ori_order_request_item_ii_inventory_item_1',
  'table' => 'ori_order_request_item',
  'module' => 'ORI_Order_Request_Item',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["ori_order_2123st_item_ida"] = array (
  'name' => 'ori_order_2123st_item_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'ori_order_2123st_item_ida',
  'link' => 'ori_order_request_item_ii_inventory_item_1',
  'table' => 'ori_order_request_item',
  'module' => 'ORI_Order_Request_Item',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/m01_sales_ii_inventory_item_1_II_Inventory_Item.php

// created: 2020-07-31 13:07:04
$dictionary["II_Inventory_Item"]["fields"]["m01_sales_ii_inventory_item_1"] = array (
  'name' => 'm01_sales_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'm01_sales_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'm01_sales_ii_inventory_item_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["m01_sales_ii_inventory_item_1_name"] = array (
  'name' => 'm01_sales_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_ii_inventory_item_1m01_sales_ida',
  'link' => 'm01_sales_ii_inventory_item_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["m01_sales_ii_inventory_item_1m01_sales_ida"] = array (
  'name' => 'm01_sales_ii_inventory_item_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'm01_sales_ii_inventory_item_1m01_sales_ida',
  'link' => 'm01_sales_ii_inventory_item_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/anml_animals_ii_inventory_item_1_II_Inventory_Item.php

// created: 2020-07-31 13:14:09
$dictionary["II_Inventory_Item"]["fields"]["anml_animals_ii_inventory_item_1"] = array (
  'name' => 'anml_animals_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'anml_animals_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'anml_animals_ii_inventory_item_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["anml_animals_ii_inventory_item_1_name"] = array (
  'name' => 'anml_animals_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'id_name' => 'anml_animals_ii_inventory_item_1anml_animals_ida',
  'link' => 'anml_animals_ii_inventory_item_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["anml_animals_ii_inventory_item_1anml_animals_ida"] = array (
  'name' => 'anml_animals_ii_inventory_item_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'anml_animals_ii_inventory_item_1anml_animals_ida',
  'link' => 'anml_animals_ii_inventory_item_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/m03_work_product_ii_inventory_item_1_II_Inventory_Item.php

// created: 2020-07-31 13:09:00
$dictionary["II_Inventory_Item"]["fields"]["m03_work_product_ii_inventory_item_1"] = array (
  'name' => 'm03_work_product_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'm03_work_product_ii_inventory_item_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["m03_work_product_ii_inventory_item_1_name"] = array (
  'name' => 'm03_work_product_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_ii_inventory_item_1m03_work_product_ida',
  'link' => 'm03_work_product_ii_inventory_item_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["m03_work_product_ii_inventory_item_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_ii_inventory_item_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'm03_work_product_ii_inventory_item_1m03_work_product_ida',
  'link' => 'm03_work_product_ii_inventory_item_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_location_values_c.php

 // created: 2020-10-06 09:39:53
$dictionary['II_Inventory_Item']['fields']['location_values_c']['labelValue']='Location Values';
$dictionary['II_Inventory_Item']['fields']['location_values_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['location_values_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['location_values_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_sterilization_exp_date_c.php

 // created: 2021-02-18 16:11:44
$dictionary['II_Inventory_Item']['fields']['sterilization_exp_date_c']['labelValue']='Sterilization Expiration Date';
$dictionary['II_Inventory_Item']['fields']['sterilization_exp_date_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['sterilization_exp_date_c']['dependency']='equal($type_2,"Control Article")';
$dictionary['II_Inventory_Item']['fields']['sterilization_exp_date_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_product_id_2_c.php

 // created: 2021-02-05 09:44:22
$dictionary['II_Inventory_Item']['fields']['product_id_2_c']['labelValue']='Product ID';
$dictionary['II_Inventory_Item']['fields']['product_id_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['product_id_2_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['product_id_2_c']['dependency']='isInList($category,createList("Product","Solution"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_timepoint_end_integer_c.php

 // created: 2021-04-28 12:34:02
$dictionary['II_Inventory_Item']['fields']['timepoint_end_integer_c']['labelValue']='Timepoint End Integer';
$dictionary['II_Inventory_Item']['fields']['timepoint_end_integer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['timepoint_end_integer_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['timepoint_end_integer_c']['dependency']='isInList($timepoint_type,createList("Defined","Ad Hoc"))';
$dictionary['II_Inventory_Item']['fields']['timepoint_end_integer_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['timepoint_end_integer_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_specimen_type_1_c.php

 // created: 2021-09-24 11:35:50
$dictionary['II_Inventory_Item']['fields']['specimen_type_1_c']['labelValue']='Specimen Type 1';
$dictionary['II_Inventory_Item']['fields']['specimen_type_1_c']['dependency']='equal($pk_samples_c,"No")';
$dictionary['II_Inventory_Item']['fields']['specimen_type_1_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['specimen_type_1_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['specimen_type_1_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_location_bin_c.php

 // created: 2021-05-31 11:55:50
$dictionary['II_Inventory_Item']['fields']['location_bin_c']['labelValue']='Location (Bin #)';
$dictionary['II_Inventory_Item']['fields']['location_bin_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['location_bin_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['location_bin_c']['dependency']='and(
and(
not(equal($location_type,"")),
not(equal($location_type,"Unknown"))),
equal($location_equi_serial_no_hide_c,"51014222")
)';
$dictionary['II_Inventory_Item']['fields']['location_bin_c']['required_formula']='and(
and(
not(equal($location_type,"")),
not(equal($location_type,"Unknown"))),
equal($location_equi_serial_no_hide_c,"51014222")
)';
$dictionary['II_Inventory_Item']['fields']['location_bin_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_reminder_mail_sent_c.php

 // created: 2021-05-07 00:18:04
$dictionary['II_Inventory_Item']['fields']['reminder_mail_sent_c']['labelValue']='Reminder Mail Sent';
$dictionary['II_Inventory_Item']['fields']['reminder_mail_sent_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['reminder_mail_sent_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['reminder_mail_sent_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_ts_hidden_string_c.php

 // created: 2021-06-03 12:06:06
$dictionary['II_Inventory_Item']['fields']['ts_hidden_string_c']['labelValue']='TS Hidden String';
$dictionary['II_Inventory_Item']['fields']['ts_hidden_string_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['ts_hidden_string_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['ts_hidden_string_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['ts_hidden_string_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['ts_hidden_string_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_location_cubby_c.php

 // created: 2021-07-08 19:23:33
$dictionary['II_Inventory_Item']['fields']['location_cubby_c']['labelValue']='Location (Slot #)';
$dictionary['II_Inventory_Item']['fields']['location_cubby_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['location_cubby_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['location_cubby_c']['dependency']='and(
and(
not(equal($location_type,"")),
not(equal($location_type,"Unknown"))),
equal($location_equi_serial_no_hide_c,"51014222")
)';
$dictionary['II_Inventory_Item']['fields']['location_cubby_c']['required_formula']='and(
and(
not(equal($location_type,"")),
not(equal($location_type,"Unknown"))),
equal($location_equi_serial_no_hide_c,"51014222")
)';
$dictionary['II_Inventory_Item']['fields']['location_cubby_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_location_rack_c.php

 // created: 2021-07-09 12:43:51
$dictionary['II_Inventory_Item']['fields']['location_rack_c']['labelValue']='Location (Freezer Rack #)';
$dictionary['II_Inventory_Item']['fields']['location_rack_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['location_rack_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['location_rack_c']['dependency']='and(
and(
not(equal($location_type,"")),
not(equal($location_type,"Unknown"))),
equal($location_equi_serial_no_hide_c,"51014222")
)';
$dictionary['II_Inventory_Item']['fields']['location_rack_c']['required_formula']='and(
and(
not(equal($location_type,"")),
not(equal($location_type,"Unknown"))),
equal($location_equi_serial_no_hide_c,"51014222")
)';
$dictionary['II_Inventory_Item']['fields']['location_rack_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_location_shelf.php

 // created: 2021-07-09 12:42:42
$dictionary['II_Inventory_Item']['fields']['location_shelf']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['location_shelf']['dependency']='or(equal($location_type,"Known"),equal($location_type,"Cabinet"),equal($location_type,"Equipment"),equal($location_type,"Room"),equal($location_type,"RoomCabinet"),equal($location_type,"RoomShelf"),equal($location_type,"Shelf"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_lot_number_c.php

 // created: 2021-04-28 05:44:21
$dictionary['II_Inventory_Item']['fields']['lot_number_c']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['lot_number_c']['required_formula']='isInList($category,createList("Product","Study Article"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_allowed_storage_medium.php

 // created: 2021-05-05 05:14:55
$dictionary['II_Inventory_Item']['fields']['allowed_storage_medium']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['allowed_storage_medium']['required_formula']='equal($type_2,"Tissue")';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_received_date.php

 // created: 2021-04-28 05:50:18
$dictionary['II_Inventory_Item']['fields']['received_date']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['received_date']['required_formula']='equal($category,"Product")';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_collection_date_time_type.php

 // created: 2021-05-05 05:07:58
$dictionary['II_Inventory_Item']['fields']['collection_date_time_type']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['collection_date_time_type']['required_formula']='equal($category,"Specimen")';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_location_cabinet.php

 // created: 2020-09-17 06:36:36
$dictionary['II_Inventory_Item']['fields']['location_cabinet']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['location_cabinet']['dependency']='or(equal($location_type,"Known"),equal($location_type,"Cabinet"),equal($location_type,"Equipment"),equal($location_type,"Room"),equal($location_type,"RoomCabinet"),equal($location_type,"RoomShelf"),equal($location_type,"Shelf"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_processing_method.php

 // created: 2021-02-16 16:39:57
$dictionary['II_Inventory_Item']['fields']['processing_method']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['processing_method']['visibility_grid']=array (
  'trigger' => 'type_2',
  'values' => 
  array (
    'Accessory Article' => 
    array (
    ),
    'Block' => 
    array (
    ),
    'Control Article' => 
    array (
    ),
    'Culture' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Data Book' => 
    array (
    ),
    'Data Sheet' => 
    array (
    ),
    'Equipment Facility Record' => 
    array (
    ),
    'Extract' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Fecal' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Hard Drive' => 
    array (
    ),
    'Plasma' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Protocol Book' => 
    array (
    ),
    'Serum' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Slide' => 
    array (
    ),
    'Test Article' => 
    array (
    ),
    'Tissue' => 
    array (
    ),
    'Urine' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Whole Blood' => 
    array (
      0 => '',
      1 => '1',
    ),
    '' => 
    array (
    ),
    'Accessory Product' => 
    array (
    ),
    'Chemical' => 
    array (
    ),
    'Cleaning Agent' => 
    array (
    ),
    'Drug' => 
    array (
    ),
    'EDTA Plasma' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Hard drive' => 
    array (
    ),
    'Na Heparin Plasma' => 
    array (
      0 => '',
      1 => '1',
    ),
    'NaCit Plasma' => 
    array (
      0 => '',
      1 => '1',
    ),
    'Reagent' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_ri_received_items_ii_inventory_item_1_name.php

 // created: 2021-08-04 20:33:04
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['required']=true;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['audited']=true;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['massupdate']=true;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['duplicate_merge']='enabled';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['duplicate_merge_dom_value']='1';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['merge_filter']='disabled';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['reportable']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['unified_search']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['calculated']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['dependency']='isInList($related_to_c,createList("Purchase Order Item","Order Request Item"))';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['required_formula']='isInList($related_to_c,createList("Purchase Order Item","Order Request Item"))';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1_name']['vname']='LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_concentration_unit_c.php

 // created: 2021-02-05 09:42:03
$dictionary['II_Inventory_Item']['fields']['concentration_unit_c']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['concentration_unit_c']['dependency']='isInList($category,createList("Product","Solution"))';
$dictionary['II_Inventory_Item']['fields']['concentration_unit_c']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_quantity_c.php

 // created: 2021-08-13 12:16:19
$dictionary['II_Inventory_Item']['fields']['quantity_c']['default']='1';
$dictionary['II_Inventory_Item']['fields']['quantity_c']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['quantity_c']['enable_range_search']=false;
$dictionary['II_Inventory_Item']['fields']['quantity_c']['min']=1;
$dictionary['II_Inventory_Item']['fields']['quantity_c']['max']=false;
$dictionary['II_Inventory_Item']['fields']['quantity_c']['disable_num_format']='';
$dictionary['II_Inventory_Item']['fields']['quantity_c']['required_formula']='isInList($category,createList("Study Article"))';
$dictionary['II_Inventory_Item']['fields']['quantity_c']['dependency']='isInList($category,createList("Study Article"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_location_building.php

 // created: 2020-09-17 06:34:34
$dictionary['II_Inventory_Item']['fields']['location_building']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['location_building']['dependency']='or(equal($location_type,"Known"),equal($location_type,"Cabinet"),equal($location_type,"Equipment"),equal($location_type,"Room"),equal($location_type,"RoomCabinet"),equal($location_type,"RoomShelf"),equal($location_type,"Shelf"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_retention_interval_days.php

 // created: 2021-02-24 14:00:53
$dictionary['II_Inventory_Item']['fields']['retention_interval_days']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['retention_interval_days']['dependency']='isInList($status,createList("Onsite Archived","Onsite Archived Expired","Offsite Archived","Offsite Archived Expired"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_allowed_storage_conditions.php

 // created: 2021-02-25 10:39:15
$dictionary['II_Inventory_Item']['fields']['allowed_storage_conditions']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['allowed_storage_conditions']['required']=true;
$dictionary['II_Inventory_Item']['fields']['allowed_storage_conditions']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_anml_animals_ii_inventory_item_1_name.php
 
$dictionary['II_Inventory_Item']['fields']['anml_animals_ii_inventory_item_1_name']['required']='true';
$dictionary['II_Inventory_Item']['fields']['days_to_expiration']['readonly']='true';
$dictionary['II_Inventory_Item']['fields']['internal_barcode']['readonly']='true';
$dictionary['II_Inventory_Item']['fields']['name']['readonly']='true';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_product_name.php

 // created: 2021-04-28 05:26:51
$dictionary['II_Inventory_Item']['fields']['product_name']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['product_name']['required_formula']='isInList($category,createList("Product","Study Article"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfieldVardef.php

 // created: 2020-08-14 05:42:02
//$dictionary['II_Inventory_Item']['fields']['subtype']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_subtype.php

 // created: 2020-08-29 07:47:49
$dictionary['II_Inventory_Item']['fields']['subtype']['required']=false;
$dictionary['II_Inventory_Item']['fields']['subtype']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['subtype']['massupdate']=false;
$dictionary['II_Inventory_Item']['fields']['subtype']['options']='pro_subtype_list';
$dictionary['II_Inventory_Item']['fields']['subtype']['dependency']='or(equal($category,"Product"),equal($type_2,"Block"),equal($type_2,"Culture"),equal($type_2,"Slide"),equal($type_2,"Tissue"),equal($type_2,"Equipment Facility Record"))';
$dictionary['II_Inventory_Item']['fields']['subtype']['default']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_ri_received_items_ii_inventory_item_1ri_received_items_ida.php

 // created: 2021-08-04 20:33:03
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['name']='ri_received_items_ii_inventory_item_1ri_received_items_ida';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['type']='id';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['source']='non-db';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['vname']='LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['id_name']='ri_received_items_ii_inventory_item_1ri_received_items_ida';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['link']='ri_received_items_ii_inventory_item_1';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['table']='ri_received_items';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['module']='RI_Received_Items';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['rname']='id';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['reportable']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['side']='right';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['massupdate']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['duplicate_merge']='disabled';
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['hideacl']=true;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['audited']=false;
$dictionary['II_Inventory_Item']['fields']['ri_received_items_ii_inventory_item_1ri_received_items_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_description.php

 // created: 2021-05-19 14:11:19
$dictionary['II_Inventory_Item']['fields']['description']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['description']['importable']='false';
$dictionary['II_Inventory_Item']['fields']['description']['duplicate_merge']='disabled';
$dictionary['II_Inventory_Item']['fields']['description']['duplicate_merge_dom_value']='0';
$dictionary['II_Inventory_Item']['fields']['description']['unified_search']=false;
$dictionary['II_Inventory_Item']['fields']['description']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_name.php

 // created: 2021-04-28 08:16:38
$dictionary['II_Inventory_Item']['fields']['name']['required']=false;
$dictionary['II_Inventory_Item']['fields']['name']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['name']['unified_search']=false;
$dictionary['II_Inventory_Item']['fields']['name']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/ii_inventory_item_im_inventory_management_1_II_Inventory_Item.php

// created: 2020-07-31 13:40:17
$dictionary["II_Inventory_Item"]["fields"]["ii_inventory_item_im_inventory_management_1"] = array (
  'name' => 'ii_inventory_item_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'ii_inventory_item_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'IM_Inventory_Management',
  'bean_name' => 'IM_Inventory_Management',
  'vname' => 'LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'id_name' => 'ii_invento5fdeagement_idb',
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/ic_inventory_collection_ii_inventory_item_1_II_Inventory_Item.php

// created: 2020-07-31 13:29:35
$dictionary["II_Inventory_Item"]["fields"]["ic_inventory_collection_ii_inventory_item_1"] = array (
  'name' => 'ic_inventory_collection_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'ic_inventory_collection_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'IC_Inventory_Collection',
  'bean_name' => 'IC_Inventory_Collection',
  'side' => 'right',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'ic_invento128flection_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["ic_inventory_collection_ii_inventory_item_1_name"] = array (
  'name' => 'ic_inventory_collection_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'save' => true,
  'id_name' => 'ic_invento128flection_ida',
  'link' => 'ic_inventory_collection_ii_inventory_item_1',
  'table' => 'ic_inventory_collection',
  'module' => 'IC_Inventory_Collection',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["ic_invento128flection_ida"] = array (
  'name' => 'ic_invento128flection_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'ic_invento128flection_ida',
  'link' => 'ic_inventory_collection_ii_inventory_item_1',
  'table' => 'ic_inventory_collection',
  'module' => 'IC_Inventory_Collection',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/poi_purchase_order_item_ii_inventory_item_1_II_Inventory_Item.php

// created: 2020-07-31 13:15:46
$dictionary["II_Inventory_Item"]["fields"]["poi_purchase_order_item_ii_inventory_item_1"] = array (
  'name' => 'poi_purchase_order_item_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'poi_purchase_order_item_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'POI_Purchase_Order_Item',
  'bean_name' => 'POI_Purchase_Order_Item',
  'side' => 'right',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'poi_purcha8945er_item_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["poi_purchase_order_item_ii_inventory_item_1_name"] = array (
  'name' => 'poi_purchase_order_item_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'save' => true,
  'id_name' => 'poi_purcha8945er_item_ida',
  'link' => 'poi_purchase_order_item_ii_inventory_item_1',
  'table' => 'poi_purchase_order_item',
  'module' => 'POI_Purchase_Order_Item',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["poi_purcha8945er_item_ida"] = array (
  'name' => 'poi_purcha8945er_item_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'poi_purcha8945er_item_ida',
  'link' => 'poi_purchase_order_item_ii_inventory_item_1',
  'table' => 'poi_purchase_order_item',
  'module' => 'POI_Purchase_Order_Item',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/m03_work_product_ii_inventory_item_2_II_Inventory_Item.php

// created: 2020-07-31 13:11:35
$dictionary["II_Inventory_Item"]["fields"]["m03_work_product_ii_inventory_item_2"] = array (
  'name' => 'm03_work_product_ii_inventory_item_2',
  'type' => 'link',
  'relationship' => 'm03_work_product_ii_inventory_item_2',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_ii_inventory_item_2m03_work_product_ida',
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_test_ii_c.php

 // created: 2020-09-23 08:19:54
$dictionary['II_Inventory_Item']['fields']['test_ii_c']['labelValue']='Test II';
$dictionary['II_Inventory_Item']['fields']['test_ii_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['test_ii_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['test_ii_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_timepoint_name_c.php

 // created: 2021-04-08 12:25:44
$dictionary['II_Inventory_Item']['fields']['timepoint_name_c']['labelValue']='Timepoint Name';
$dictionary['II_Inventory_Item']['fields']['timepoint_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['timepoint_name_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['timepoint_name_c']['dependency']='isInList($timepoint_type,createList("Defined","Ad Hoc"))';
$dictionary['II_Inventory_Item']['fields']['timepoint_name_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_analyze_by_date_c.php

 // created: 2021-04-15 10:30:47
$dictionary['II_Inventory_Item']['fields']['analyze_by_date_c']['labelValue']='Analyze by Date';
$dictionary['II_Inventory_Item']['fields']['analyze_by_date_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['analyze_by_date_c']['dependency']='equal($stability_considerations_c,"Yes")';
$dictionary['II_Inventory_Item']['fields']['analyze_by_date_c']['required_formula']='equal($stability_considerations_c,"Yes")';
$dictionary['II_Inventory_Item']['fields']['analyze_by_date_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_location_equi_serial_no_hide_c.php

 // created: 2021-04-12 09:34:57
$dictionary['II_Inventory_Item']['fields']['location_equi_serial_no_hide_c']['labelValue']='Location Equi Sr No Hidden';
$dictionary['II_Inventory_Item']['fields']['location_equi_serial_no_hide_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['location_equi_serial_no_hide_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['location_equi_serial_no_hide_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['location_equi_serial_no_hide_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['location_equi_serial_no_hide_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_storage_medium.php

 // created: 2021-05-05 05:16:18
$dictionary['II_Inventory_Item']['fields']['storage_medium']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['storage_medium']['required_formula']='equal($type_2,"Tissue")';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_timepoint_integer.php

 // created: 2021-04-08 12:26:47
$dictionary['II_Inventory_Item']['fields']['timepoint_integer']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_actual_discard_date.php

 // created: 2021-04-28 05:56:13
$dictionary['II_Inventory_Item']['fields']['actual_discard_date']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['actual_discard_date']['required_formula']='equal($status,"Discarded")';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_internal_barcode.php

 // created: 2021-09-22 12:20:42
$dictionary['II_Inventory_Item']['fields']['internal_barcode']['required']=false;
$dictionary['II_Inventory_Item']['fields']['internal_barcode']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['internal_barcode']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_manufacturer_c.php

 // created: 2021-04-28 05:45:49
$dictionary['II_Inventory_Item']['fields']['manufacturer_c']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['manufacturer_c']['required']=true;
$dictionary['II_Inventory_Item']['fields']['manufacturer_c']['required_formula']='isInList($category,createList("Product","Study Article"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_inactive.php

$dictionary['II_Inventory_Item']['fields']['inactive_c'] = array(
    'name'            => 'inactive_c',
    'label'           => 'Inactive',
    'type'            => 'bool',
    'module'          => 'II_Inventory_Item',
    'default_value'   => true,
    'help'            => '',
    'comment'         => '',
    'audited'         => false,
    'mass_update'     => false,
    'duplicate_merge' => false,
    'reportable'      => true,
    'importable'      => 'true',
);

//$dictionary['II_Inventory_Item']['fields']['name']['fields'] = array(0 => 'name', 1 => 'inactive_c');

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_location_equipment.php

 // created: 2020-09-17 06:35:42
$dictionary['II_Inventory_Item']['fields']['location_equipment']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['location_equipment']['dependency']='or(equal($location_type,"Known"),equal($location_type,"Cabinet"),equal($location_type,"Equipment"),equal($location_type,"Room"),equal($location_type,"RoomCabinet"),equal($location_type,"RoomShelf"),equal($location_type,"Shelf"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_expiration_date.php

 // created: 2021-04-28 05:48:57
$dictionary['II_Inventory_Item']['fields']['expiration_date']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['expiration_date']['required_formula']='or(equal($category,"Product"),isInList($type_2,createList("Accessory Article","Control Article")),and(equal($type_2,"Test Article"),equal($expiration_test_article,"Known")))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_location_room.php

 // created: 2020-09-17 06:35:12
$dictionary['II_Inventory_Item']['fields']['location_room']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['location_room']['dependency']='or(equal($location_type,"Known"),equal($location_type,"Cabinet"),equal($location_type,"Equipment"),equal($location_type,"Room"),equal($location_type,"RoomCabinet"),equal($location_type,"RoomShelf"),equal($location_type,"Shelf"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_external_barcode.php

 // created: 2021-02-05 09:40:07
$dictionary['II_Inventory_Item']['fields']['external_barcode']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['external_barcode']['dependency']='isInList($category,createList("Product","Solution"))';
$dictionary['II_Inventory_Item']['fields']['external_barcode']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_concentration_c.php

 // created: 2021-02-05 09:41:41
$dictionary['II_Inventory_Item']['fields']['concentration_c']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['concentration_c']['dependency']='isInList($category,createList("Product","Solution"))';
$dictionary['II_Inventory_Item']['fields']['concentration_c']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_planned_discard_date.php

 // created: 2021-04-28 05:55:05
$dictionary['II_Inventory_Item']['fields']['planned_discard_date']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['planned_discard_date']['dependency']='isInList($status,createList("Onsite Archived","Onsite Archived Expired","Offsite Archived","Offsite Archived Expired"))';
$dictionary['II_Inventory_Item']['fields']['planned_discard_date']['required_formula']='isInList($status,createList("Onsite Archived","Onsite Archived Expired","Offsite Archived","Offsite Archived Expired"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_retention_start_date.php

 // created: 2021-04-28 05:49:43
$dictionary['II_Inventory_Item']['fields']['retention_start_date']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['retention_start_date']['dependency']='isInList($status,createList("Onsite Archived","Onsite Archived Expired","Offsite Archived","Offsite Archived Expired"))';
$dictionary['II_Inventory_Item']['fields']['retention_start_date']['required_formula']='isInList($status,createList("Onsite Archived","Onsite Archived Expired","Offsite Archived","Offsite Archived Expired"))';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_collection_date_time.php

 // created: 2021-04-28 05:47:58
$dictionary['II_Inventory_Item']['fields']['collection_date_time']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['collection_date_time']['required_formula']='equal($collection_date_time_type,"Date Time at Record Creation")';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_category.php

 // created: 2021-11-10 00:27:06
$dictionary['II_Inventory_Item']['fields']['category']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['category']['visibility_grid']=array (
  'trigger' => 'related_to_c',
  'values' => 
  array (
    'Internal Use' => 
    array (
      0 => 'Record',
    ),
    'Multiple Test Systems' => 
    array (
      0 => 'Specimen',
    ),
    'Sales' => 
    array (
      0 => '',
      1 => 'Record',
      2 => 'Specimen',
      3 => 'Study Article',
    ),
    'Single Test System' => 
    array (
      0 => '',
      1 => 'Record',
      2 => 'Specimen',
    ),
    'Work Product' => 
    array (
      0 => '',
      1 => 'Record',
      2 => 'Specimen',
      3 => 'Study Article',
    ),
    '' => 
    array (
    ),
    'Order Request Item' => 
    array (
      0 => 'Product',
    ),
    'Purchase Order Item' => 
    array (
      0 => 'Product',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_owner.php

 // created: 2021-11-10 00:32:19
$dictionary['II_Inventory_Item']['fields']['owner']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['owner']['dependency']='isInList($category,createList("Product","Solution","Study Article"))';
$dictionary['II_Inventory_Item']['fields']['owner']['default']='';
$dictionary['II_Inventory_Item']['fields']['owner']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_timepoint_type.php

 // created: 2021-11-10 00:37:30
$dictionary['II_Inventory_Item']['fields']['timepoint_type']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_storage_condition.php

 // created: 2021-11-10 00:49:01
$dictionary['II_Inventory_Item']['fields']['storage_condition']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_location_type.php

 // created: 2021-11-10 00:51:12
$dictionary['II_Inventory_Item']['fields']['location_type']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['location_type']['options']='im_location_type_list';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_processed_per_protocol_c.php

 // created: 2021-11-10 02:06:28
$dictionary['II_Inventory_Item']['fields']['processed_per_protocol_c']['labelValue']='Processed per Protocol';
$dictionary['II_Inventory_Item']['fields']['processed_per_protocol_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['processed_per_protocol_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['processed_per_protocol_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['processed_per_protocol_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_quantity_2_c.php

 // created: 2021-11-10 02:10:09
$dictionary['II_Inventory_Item']['fields']['quantity_2_c']['labelValue']='Quantity';
$dictionary['II_Inventory_Item']['fields']['quantity_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['quantity_2_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['quantity_2_c']['dependency']='isInList($related_to_c,createList("Purchase Order Item","Order Request Item"))';
$dictionary['II_Inventory_Item']['fields']['quantity_2_c']['required_formula']='isInList($related_to_c,createList("Purchase Order Item","Order Request Item"))';
$dictionary['II_Inventory_Item']['fields']['quantity_2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_status.php

 // created: 2021-11-10 02:15:14
$dictionary['II_Inventory_Item']['fields']['status']['default']='Planned Inventory';
$dictionary['II_Inventory_Item']['fields']['status']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_inactive_c.php

 // created: 2021-11-12 07:41:07
$dictionary['II_Inventory_Item']['fields']['inactive_c']['labelValue']='Inactive';
$dictionary['II_Inventory_Item']['fields']['inactive_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['inactive_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['inactive_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_stability_considerations_c.php

 // created: 2021-11-16 09:46:44
$dictionary['II_Inventory_Item']['fields']['stability_considerations_c']['labelValue']='PK Stability Considerations';
$dictionary['II_Inventory_Item']['fields']['stability_considerations_c']['dependency']='or(
isInList($type_2,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_1_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_2_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_3_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_4_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_5_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")))';
$dictionary['II_Inventory_Item']['fields']['stability_considerations_c']['required_formula']='or(
isInList($type_2,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_1_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_2_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_3_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_4_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")),
isInList($specimen_type_5_c,createList("EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Serum","Urine","Whole Blood","Tissue")))';
$dictionary['II_Inventory_Item']['fields']['stability_considerations_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['stability_considerations_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_type_2.php

 // created: 2022-01-18 07:05:59
$dictionary['II_Inventory_Item']['fields']['type_2']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['type_2']['visibility_grid']=array (
  'trigger' => 'category',
  'values' => 
  array (
    'Product' => 
    array (
      0 => '',
      1 => 'Chemical',
      2 => 'Cleaning Agent',
      3 => 'Drug',
      4 => 'Reagent',
    ),
    'Record' => 
    array (
      0 => '',
      1 => 'Data Book',
      2 => 'Data Sheet',
      3 => 'Equipment Facility Record',
      4 => 'Hard Drive',
      5 => 'Protocol Book',
    ),
    'Solution' => 
    array (
    ),
    'Specimen' => 
    array (
      0 => '',
      1 => 'Balloons',
      2 => 'Block',
      3 => 'Culture',
      4 => 'EDTA Plasma',
      5 => 'Extract',
      6 => 'Fecal',
      7 => 'Na Heparin Plasma',
      8 => 'NaCit Plasma',
      9 => 'Other',
      10 => 'Serum',
      11 => 'Slide',
      12 => 'Tissue',
      13 => 'Urine',
      14 => 'Whole Blood',
    ),
    'Study Article' => 
    array (
      0 => '',
      1 => 'Accessory Article',
      2 => 'Control Article',
      3 => 'Test Article',
    ),
    '' => 
    array (
    ),
  ),
);
$dictionary['II_Inventory_Item']['fields']['type_2']['default']='';
$dictionary['II_Inventory_Item']['fields']['type_2']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_test_type_c.php

 // created: 2022-01-18 07:10:55
$dictionary['II_Inventory_Item']['fields']['test_type_c']['labelValue']='Test Type';
$dictionary['II_Inventory_Item']['fields']['test_type_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['test_type_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['test_type_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['test_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_other_type_c.php

 // created: 2022-02-01 06:54:34
$dictionary['II_Inventory_Item']['fields']['other_type_c']['labelValue']='Other Specimen Type';
$dictionary['II_Inventory_Item']['fields']['other_type_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['other_type_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['other_type_c']['dependency']='or(and(equal($type_2,"Other"),equal($category,"Specimen")),equal($specimen_type_1_c,"Other"))';
$dictionary['II_Inventory_Item']['fields']['other_type_c']['required_formula']='or(and(equal($type_2,"Other"),equal($category,"Specimen")),equal($specimen_type_1_c,"Other"))';
$dictionary['II_Inventory_Item']['fields']['other_type_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_other_test_type_c.php

 // created: 2022-02-01 06:56:22
$dictionary['II_Inventory_Item']['fields']['other_test_type_c']['labelValue']='Other Test Type';
$dictionary['II_Inventory_Item']['fields']['other_test_type_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['other_test_type_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['other_test_type_c']['dependency']='or(equal($test_type_c,"Other"),equal($test_type_1_c,"Other"))';
$dictionary['II_Inventory_Item']['fields']['other_test_type_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['other_test_type_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_specimen_type_2_c.php

 // created: 2022-02-01 07:02:49
$dictionary['II_Inventory_Item']['fields']['specimen_type_2_c']['labelValue']='Specimen Type 2';
$dictionary['II_Inventory_Item']['fields']['specimen_type_2_c']['dependency']='equal($pk_samples_c,"No")';
$dictionary['II_Inventory_Item']['fields']['specimen_type_2_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['specimen_type_2_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['specimen_type_2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_specimen_type_3_c.php

 // created: 2022-02-01 07:04:30
$dictionary['II_Inventory_Item']['fields']['specimen_type_3_c']['labelValue']='Specimen Type 3';
$dictionary['II_Inventory_Item']['fields']['specimen_type_3_c']['dependency']='equal($pk_samples_c,"No")';
$dictionary['II_Inventory_Item']['fields']['specimen_type_3_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['specimen_type_3_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['specimen_type_3_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_specimen_type_4_c.php

 // created: 2022-02-01 07:05:42
$dictionary['II_Inventory_Item']['fields']['specimen_type_4_c']['labelValue']='Specimen Type 4';
$dictionary['II_Inventory_Item']['fields']['specimen_type_4_c']['dependency']='equal($pk_samples_c,"No")';
$dictionary['II_Inventory_Item']['fields']['specimen_type_4_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['specimen_type_4_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['specimen_type_4_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_specimen_type_5_c.php

 // created: 2022-02-01 07:06:49
$dictionary['II_Inventory_Item']['fields']['specimen_type_5_c']['labelValue']='Specimen Type 5';
$dictionary['II_Inventory_Item']['fields']['specimen_type_5_c']['dependency']='equal($pk_samples_c,"No")';
$dictionary['II_Inventory_Item']['fields']['specimen_type_5_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['specimen_type_5_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['specimen_type_5_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_pk_samples_c.php

 // created: 2022-02-01 07:08:34
$dictionary['II_Inventory_Item']['fields']['pk_samples_c']['labelValue']='Multiple Aliquots Needed?';
$dictionary['II_Inventory_Item']['fields']['pk_samples_c']['dependency']='equal($category,"Specimen")';
$dictionary['II_Inventory_Item']['fields']['pk_samples_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['pk_samples_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['pk_samples_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/m03_work_product_deliverable_ii_inventory_item_1_II_Inventory_Item.php

// created: 2022-02-22 07:44:29
$dictionary["II_Inventory_Item"]["fields"]["m03_work_product_deliverable_ii_inventory_item_1"] = array (
  'name' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Deliverable',
  'bean_name' => 'M03_Work_Product_Deliverable',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'm03_work_p7e14verable_ida',
  'link-type' => 'one',
);
$dictionary["II_Inventory_Item"]["fields"]["m03_work_product_deliverable_ii_inventory_item_1_name"] = array (
  'name' => 'm03_work_product_deliverable_ii_inventory_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'save' => true,
  'id_name' => 'm03_work_p7e14verable_ida',
  'link' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'table' => 'm03_work_product_deliverable',
  'module' => 'M03_Work_Product_Deliverable',
  'rname' => 'name',
);
$dictionary["II_Inventory_Item"]["fields"]["m03_work_p7e14verable_ida"] = array (
  'name' => 'm03_work_p7e14verable_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE_ID',
  'id_name' => 'm03_work_p7e14verable_ida',
  'link' => 'm03_work_product_deliverable_ii_inventory_item_1',
  'table' => 'm03_work_product_deliverable',
  'module' => 'M03_Work_Product_Deliverable',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_number_of_aliquots_c.php

 // created: 2022-02-24 08:30:16
$dictionary['II_Inventory_Item']['fields']['number_of_aliquots_c']['labelValue']='Number of Aliquots';
$dictionary['II_Inventory_Item']['fields']['number_of_aliquots_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['II_Inventory_Item']['fields']['number_of_aliquots_c']['enforced']='';
$dictionary['II_Inventory_Item']['fields']['number_of_aliquots_c']['dependency']='equal($pk_samples_c,"Yes")';
$dictionary['II_Inventory_Item']['fields']['number_of_aliquots_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['number_of_aliquots_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_collection_date.php

 // created: 2022-03-31 11:45:53
$dictionary['II_Inventory_Item']['fields']['collection_date']['hidemassupdate']=false;
$dictionary['II_Inventory_Item']['fields']['collection_date']['required_formula']='equal($collection_date_time_type,"Date at Record Creation")';
$dictionary['II_Inventory_Item']['fields']['collection_date']['massupdate']=true;

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_test_type_1_c.php

 // created: 2022-04-12 09:06:19
$dictionary['II_Inventory_Item']['fields']['test_type_1_c']['labelValue']='Test Type 1';
$dictionary['II_Inventory_Item']['fields']['test_type_1_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['test_type_1_c']['required_formula']='';
$dictionary['II_Inventory_Item']['fields']['test_type_1_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['test_type_1_c']['visibility_grid']=array (
  'trigger' => 'specimen_type_1_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Culture' => 
    array (
      0 => '',
      1 => 'Aerobic and Anaerobic Culture ID Bacteria and Fungi',
      2 => 'Aerobic Culture and ID Bacteria only',
      3 => 'Aerobic Culture and ID Fungi only',
      4 => 'Blood Culture',
    ),
    'Fecal' => 
    array (
      0 => '',
      1 => 'Cryptosporidium ELISA',
      2 => 'Fecal Ova and Parasite',
      3 => 'Giardia ELISA',
    ),
    'NaCit Plasma' => 
    array (
      0 => '',
      1 => 'Anti Factor Xa',
      2 => 'Coagulation',
      3 => 'D Dimer',
      4 => 'Fibrinogen',
      5 => 'PT aPTT',
      6 => 'PT aPTT Fibrinogen',
    ),
    'Na Heparin Plasma' => 
    array (
      0 => '',
      1 => 'Free Hemoglobin',
    ),
    'Other' => 
    array (
      0 => '',
      1 => 'Other',
    ),
    'Serum' => 
    array (
      0 => '',
      1 => 'Amylase andor Lipase',
      2 => 'Haptoglobin',
      3 => 'Ionized Calcium',
      4 => 'PK in Serum',
      5 => 'Troponin 1',
    ),
    'Urine' => 
    array (
      0 => '',
      1 => 'Urinalysis',
      2 => 'Urine ProteinCreatinine Ratio',
    ),
    'Whole Blood' => 
    array (
      0 => '',
      1 => 'CBC',
      2 => 'CBC plus Heartworm',
      3 => 'CBC with Reticulocytes',
      4 => 'Chemistry',
      5 => 'Custom Chemistry',
      6 => 'Other',
      7 => 'PK in Plasma',
      8 => 'PK in Serum',
      9 => 'PK in Whole Blood',
    ),
    'EDTA Plasma' => 
    array (
      0 => '',
      1 => 'PK in Plasma',
    ),
    'Balloons' => 
    array (
      0 => '',
      1 => 'Other',
      2 => 'PK in Balloons',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_test_type_2_c.php

 // created: 2022-04-12 09:10:38
$dictionary['II_Inventory_Item']['fields']['test_type_2_c']['labelValue']='Test Type 2';
$dictionary['II_Inventory_Item']['fields']['test_type_2_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['test_type_2_c']['required_formula']='not(equal($specimen_type_2_c,""))';
$dictionary['II_Inventory_Item']['fields']['test_type_2_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['test_type_2_c']['visibility_grid']=array (
  'trigger' => 'specimen_type_2_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Culture' => 
    array (
      0 => '',
      1 => 'Aerobic and Anaerobic Culture ID Bacteria and Fungi',
      2 => 'Aerobic Culture and ID Bacteria only',
      3 => 'Aerobic Culture and ID Fungi only',
      4 => 'Blood Culture',
    ),
    'EDTA Plasma' => 
    array (
      0 => '',
      1 => 'PK in Plasma',
    ),
    'Fecal' => 
    array (
      0 => '',
      1 => 'Cryptosporidium ELISA',
      2 => 'Fecal Ova and Parasite',
      3 => 'Giardia ELISA',
    ),
    'NaCit Plasma' => 
    array (
      0 => '',
      1 => 'Anti Factor Xa',
      2 => 'Coagulation',
      3 => 'D Dimer',
      4 => 'Fibrinogen',
      5 => 'PT aPTT',
      6 => 'PT aPTT Fibrinogen',
    ),
    'Na Heparin Plasma' => 
    array (
      0 => '',
      1 => 'Free Hemoglobin',
    ),
    'Other' => 
    array (
      0 => '',
      1 => 'Other',
    ),
    'Serum' => 
    array (
      0 => '',
      1 => 'Amylase andor Lipase',
      2 => 'Haptoglobin',
      3 => 'Ionized Calcium',
      4 => 'PK in Serum',
      5 => 'Troponin 1',
    ),
    'Urine' => 
    array (
      0 => '',
      1 => 'Urinalysis',
      2 => 'Urine ProteinCreatinine Ratio',
    ),
    'Whole Blood' => 
    array (
      0 => '',
      1 => 'CBC',
      2 => 'CBC plus Heartworm',
      3 => 'CBC with Reticulocytes',
      4 => 'Chemistry',
      5 => 'Custom Chemistry',
      6 => 'PK in Plasma',
      7 => 'PK in Serum',
      8 => 'PK in Whole Blood',
    ),
    'Balloons' => 
    array (
      0 => '',
      1 => 'PK in Balloons',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_test_type_3_c.php

 // created: 2022-04-12 09:15:29
$dictionary['II_Inventory_Item']['fields']['test_type_3_c']['labelValue']='Test Type 3';
$dictionary['II_Inventory_Item']['fields']['test_type_3_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['test_type_3_c']['required_formula']='not(equal($specimen_type_3_c,""))';
$dictionary['II_Inventory_Item']['fields']['test_type_3_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['test_type_3_c']['visibility_grid']=array (
  'trigger' => 'specimen_type_3_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Culture' => 
    array (
      0 => '',
      1 => 'Aerobic and Anaerobic Culture ID Bacteria and Fungi',
      2 => 'Aerobic Culture and ID Bacteria only',
      3 => 'Aerobic Culture and ID Fungi only',
      4 => 'Blood Culture',
    ),
    'EDTA Plasma' => 
    array (
      0 => '',
      1 => 'PK in Plasma',
    ),
    'Fecal' => 
    array (
      0 => '',
      1 => 'Cryptosporidium ELISA',
      2 => 'Fecal Ova and Parasite',
      3 => 'Giardia ELISA',
    ),
    'NaCit Plasma' => 
    array (
      0 => '',
      1 => 'Anti Factor Xa',
      2 => 'Coagulation',
      3 => 'D Dimer',
      4 => 'Fibrinogen',
      5 => 'PT aPTT',
      6 => 'PT aPTT Fibrinogen',
    ),
    'Na Heparin Plasma' => 
    array (
      0 => '',
      1 => 'Free Hemoglobin',
    ),
    'Other' => 
    array (
      0 => '',
      1 => 'Other',
    ),
    'Serum' => 
    array (
      0 => '',
      1 => 'Amylase andor Lipase',
      2 => 'Haptoglobin',
      3 => 'Ionized Calcium',
      4 => 'PK in Serum',
      5 => 'Troponin 1',
    ),
    'Urine' => 
    array (
      0 => '',
      1 => 'Urinalysis',
      2 => 'Urine ProteinCreatinine Ratio',
    ),
    'Whole Blood' => 
    array (
      0 => '',
      1 => 'CBC',
      2 => 'CBC plus Heartworm',
      3 => 'CBC with Reticulocytes',
      4 => 'Chemistry',
      5 => 'Custom Chemistry',
      6 => 'PK in Plasma',
      7 => 'PK in Serum',
      8 => 'PK in Whole Blood',
    ),
    'Balloons' => 
    array (
      0 => '',
      1 => 'PK in Balloons',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_test_type_4_c.php

 // created: 2022-04-12 09:21:19
$dictionary['II_Inventory_Item']['fields']['test_type_4_c']['labelValue']='Test Type 4';
$dictionary['II_Inventory_Item']['fields']['test_type_4_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['test_type_4_c']['required_formula']='not(equal($specimen_type_4_c,""))';
$dictionary['II_Inventory_Item']['fields']['test_type_4_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['test_type_4_c']['visibility_grid']=array (
  'trigger' => 'specimen_type_4_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Culture' => 
    array (
      0 => '',
      1 => 'Aerobic and Anaerobic Culture ID Bacteria and Fungi',
      2 => 'Aerobic Culture and ID Bacteria only',
      3 => 'Aerobic Culture and ID Fungi only',
      4 => 'Blood Culture',
    ),
    'EDTA Plasma' => 
    array (
      0 => '',
      1 => 'PK in Plasma',
    ),
    'Fecal' => 
    array (
      0 => '',
      1 => 'Cryptosporidium ELISA',
      2 => 'Fecal Ova and Parasite',
      3 => 'Giardia ELISA',
    ),
    'NaCit Plasma' => 
    array (
      0 => '',
      1 => 'Anti Factor Xa',
      2 => 'Coagulation',
      3 => 'D Dimer',
      4 => 'Fibrinogen',
      5 => 'PT aPTT',
      6 => 'PT aPTT Fibrinogen',
    ),
    'Na Heparin Plasma' => 
    array (
      0 => '',
      1 => 'Free Hemoglobin',
    ),
    'Other' => 
    array (
      0 => '',
      1 => 'Other',
    ),
    'Serum' => 
    array (
      0 => '',
      1 => 'Amylase andor Lipase',
      2 => 'Haptoglobin',
      3 => 'Ionized Calcium',
      4 => 'PK in Serum',
      5 => 'Troponin 1',
    ),
    'Urine' => 
    array (
      0 => '',
      1 => 'Urinalysis',
      2 => 'Urine ProteinCreatinine Ratio',
    ),
    'Whole Blood' => 
    array (
      0 => '',
      1 => 'CBC',
      2 => 'CBC plus Heartworm',
      3 => 'CBC with Reticulocytes',
      4 => 'Chemistry',
      5 => 'Custom Chemistry',
      6 => 'PK in Plasma',
      7 => 'PK in Serum',
      8 => 'PK in Whole Blood',
    ),
    'Balloons' => 
    array (
      0 => '',
      1 => 'PK in Balloons',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Vardefs/sugarfield_test_type_5_c.php

 // created: 2022-04-12 09:22:55
$dictionary['II_Inventory_Item']['fields']['test_type_5_c']['labelValue']='Test Type 5';
$dictionary['II_Inventory_Item']['fields']['test_type_5_c']['dependency']='';
$dictionary['II_Inventory_Item']['fields']['test_type_5_c']['required_formula']='not(equal($specimen_type_5_c,""))';
$dictionary['II_Inventory_Item']['fields']['test_type_5_c']['readonly_formula']='';
$dictionary['II_Inventory_Item']['fields']['test_type_5_c']['visibility_grid']=array (
  'trigger' => 'specimen_type_5_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Culture' => 
    array (
      0 => '',
      1 => 'Aerobic and Anaerobic Culture ID Bacteria and Fungi',
      2 => 'Aerobic Culture and ID Bacteria only',
      3 => 'Aerobic Culture and ID Fungi only',
      4 => 'Blood Culture',
    ),
    'EDTA Plasma' => 
    array (
      0 => '',
      1 => 'PK in Plasma',
    ),
    'Fecal' => 
    array (
      0 => '',
      1 => 'Cryptosporidium ELISA',
      2 => 'Fecal Ova and Parasite',
      3 => 'Giardia ELISA',
    ),
    'NaCit Plasma' => 
    array (
      0 => '',
      1 => 'Anti Factor Xa',
      2 => 'Coagulation',
      3 => 'D Dimer',
      4 => 'Fibrinogen',
      5 => 'PT aPTT',
      6 => 'PT aPTT Fibrinogen',
    ),
    'Na Heparin Plasma' => 
    array (
      0 => '',
      1 => 'Free Hemoglobin',
    ),
    'Other' => 
    array (
      0 => '',
      1 => 'Other',
    ),
    'Serum' => 
    array (
      0 => '',
      1 => 'Amylase andor Lipase',
      2 => 'Haptoglobin',
      3 => 'Ionized Calcium',
      4 => 'PK in Serum',
      5 => 'Troponin 1',
    ),
    'Urine' => 
    array (
      0 => '',
      1 => 'Urinalysis',
      2 => 'Urine ProteinCreatinine Ratio',
    ),
    'Whole Blood' => 
    array (
      0 => '',
      1 => 'CBC',
      2 => 'CBC plus Heartworm',
      3 => 'CBC with Reticulocytes',
      4 => 'Chemistry',
      5 => 'Custom Chemistry',
      6 => 'PK in Plasma',
      7 => 'PK in Serum',
      8 => 'PK in Whole Blood',
    ),
    'Balloons' => 
    array (
      0 => '',
      1 => 'PK in Balloons',
    ),
  ),
);

 
?>
