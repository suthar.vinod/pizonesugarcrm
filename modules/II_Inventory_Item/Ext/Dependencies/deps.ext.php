<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/wp_required_dep.php


$dependencies['II_Inventory_Item']['wp_required_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('m03_work_product_ii_inventory_item_1_name', 'related_to_c'),
    'onload'        => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions'       => array(
        array(
            'name'   => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'm03_work_product_ii_inventory_item_1_name',
                'label'  => 'sales_required_label',
                'value'  => 'or(equal($related_to_c, "Work Product"),equal($related_to_c, "Single Test System"),equal($related_to_c, "Multiple Test Systems"))',
            ),
        ),
    ),
);

$dependencies['II_Inventory_Item']['ii_required_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array(
        'id',
        'm01_sales_ii_inventory_item_1_name',
        'm03_work_product_ii_inventory_item_1_name',
        'anml_animals_ii_inventory_item_1_name',
        'category',
        'type_2',
        'unique_id_type',
        'unique_id_c',
        'subtype',
    ),
    'onload'        => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions'       => array(
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'quantity_c',
                'label'  => 'quantity_c_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'type_2',
                'label'  => 'type_2_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'm03_work_product_ii_inventory_item_1_name',
                'label'  => 'm03_work_product_ii_inventory_item_1_name_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'm01_sales_ii_inventory_item_1_name',
                'label'  => 'm01_sales_ii_inventory_item_1_name_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'anml_animals_ii_inventory_item_1_name',
                'label'  => 'anml_animals_ii_inventory_item_1_name_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'category',
                'label'  => 'category_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),

        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'unique_id_type',
                'label'  => 'unique_id_type_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'unique_id_c',
                'label'  => 'unique_id_c_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'subtype',
                'label'  => 'subtype_label',
                'value'  => 'not(equal(strlen($id),0))',
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/shipping_dep.php


$dependencies['II_Inventory_Item']['shipping_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('ship_to_contact', 'ship_to_address', 'ship_to_company'),
    'onload'        => true,
    'actions'       => array(

        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'ship_to_contact',
                'label'  => 'ship_to_contact_label',
                'value'  => 'greaterThan(strlen($id), 0)',
            ),
        ),
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'ship_to_address',
                'label'  => 'ship_to_address_label',
                'value'  => 'greaterThan(strlen($id), 0)',
            ),
        ),
        array(
            'name'   => 'SetRequired',
            'params' => array(
                'target' => 'ship_to_company',
                'label'  => 'ship_to_company_label',
                'value'  => 'greaterThan(strlen($id), 0)',
            ),
        ),
		
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ship_to_contact',
                'value'  => 'not(equal(strlen($ship_to_contact), 0))',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ship_to_address',
                'value'  => 'not(equal(strlen($ship_to_address), 0))',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ship_to_company',
                'value'  => 'not(equal(strlen($ship_to_company), 0))',
            ),
        ),

        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ship_to_contact',
                'value'  => 'true',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ship_to_address',
                'value'  => 'true',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ship_to_company',
                'value'  => 'true',
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/sales_required_dep.php


$dependencies['II_Inventory_Item']['sales_required_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('m01_sales_ii_inventory_item_1_name', 'related_to_c'),
    'onload'        => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions'       => array(
        array(
            'name'   => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'm01_sales_ii_inventory_item_1_name',
                'label'  => 'sales_required_label',
                'value'  => 'equal($related_to_c, "Sales")',
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/location_type_vis_dep.php

/* $dependencies['II_Inventory_Item']['location_type_vis_01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('status'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_type',
                'value' => 'ifElse(or(equal($status,"Discarded"),equal($status,"Obsolete")),false,true)',                
            ),
        ),
    ),
); */
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/work_product_visibility_dep.php


$dependencies['II_Inventory_Item']['work_product_visibility_dep'] = array(
    'hooks'         => array("edit"),
    'triggerFields' => array('related_to_c'),
    'onload'        => true,

    'actions' => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_ii_inventory_item_1_name',
                'value'  => 'or(equal($related_to_c, "Work Product"),equal($related_to_c, "Single Test System"),equal($related_to_c, "Multiple Test Systems"))',
            ),
        ),
    ),
);
$dependencies['II_Inventory_Item']['test_system_visibility_dep'] = array(
    'hooks'         => array("edit"),
    'triggerFields' => array('related_to_c'),
    'onload'        => true,

    'actions' => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'anml_animals_ii_inventory_item_1_name',                
                'value'  => 'or(equal($related_to_c, "Single Test System"),equal($related_to_c, "Multiple Test Systems"))',
            ),
        ),
    ),
);
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/all_field_readonly.php
 
$dependencies['II_Inventory_Item']['ii_inventory_item_allFieldReadonly_depsss'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('status'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'actual_discard_date',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'allowed_storage_conditions',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'allowed_storage_medium',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ), 
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'analyze_by_date_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'category',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'collection_date',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'collection_date_time_type',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'collection_date_time',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'concentration_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'concentration_unit_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'storage_condition',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ), 
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'storage_medium',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ), 
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'days_to_expiration',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ), 
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'expiration_test_article',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'expiration_date',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'expiration_date_time',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'external_barcode',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'inactive_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),    
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ic_inventory_collection_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        /*array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'invi_invoice_item_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),*/
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_bin_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_building',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_rack_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_room',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_cubby_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_equi_serial_no_hide_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_type',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'location_values_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'manufacturer_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'description',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'number_of_aliquots_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ori_order_request_item_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'other_test_type_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'other_type_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'owner',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'pk_samples_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'planned_discard_date',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'processed_per_protocol_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'processing_method',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'product_id_2_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'product_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'poi_purchase_order_item_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'quantity_2_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'quantity_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'received_date',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ri_received_items_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'related_to_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'reminder_mail_sent_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'retention_interval_days',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'retention_start_date',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'm01_sales_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ship_to_address',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ship_to_company',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ship_to_contact',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'specimen_type_1_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'specimen_type_2_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'specimen_type_3_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'specimen_type_4_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'specimen_type_5_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'stability_considerations_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'sterilization',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'sterilization_exp_date_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'subtype',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'anml_animals_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'test_type_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'test_type_1_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'test_type_2_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'test_type_3_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'test_type_4_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'test_type_5_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'timepoint_end_integer_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'timepoint_name_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'timepoint_integer',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'timepoint_type',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'timepoint_unit',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ts_hidden_string_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'type_2',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'lot_number_c',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'm03_work_product_ii_inventory_item_1_name',
                'value'  => 'equal($status, "Exhausted")',
            ),
        ),
        
     ),
);
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/inventory_collections_dep.php

$dependencies['II_Inventory_Item']['ii_inventory_collection_visibility_dep'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('ic_inventory_collection_ii_inventory_item_1_name'),
    'onload'        => true,
    'actions'       => array(     
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ic_inventory_collection_ii_inventory_item_1_name',
                'value'  => 'not(equal($ic_inventory_collection_ii_inventory_item_1_name, ""))',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ic_inventory_collection_ii_inventory_item_1_name',
                'value'  => 'not(equal($ic_inventory_collection_ii_inventory_item_1_name, ""))',
            ),
        ),
     ),
);
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/related_to_readonly_dep.php


$dependencies['II_Inventory_Item']['related_to_readonly_dep'] = array(
    'hooks'         => array("edit"),
    'trigger'       => 'true',
    'triggerFields' => array('related_to_c'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'related_to_c',
                'value'  => 'greaterThan(strlen($id), 0)',
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/name_value_dep.php

/*
$dependencies['II_Inventory_Item']['name_value_dep'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array(
        'related_to_c',
        'm03_work_product_ii_inventory_item_2',
        'm01_sales_ii_inventory_item_1',
        'anml_animals_ii_inventory_item_1_name',
        'product_name_c',
        'solution_name_c',
        'type_2',
        'subtype',
        'subtype_specimen_c',
        'subtype_product_c',
    ),
    'onload' => false,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'name',
                'value' => 'concat(
                    ifElse(
                        or(equal($related_to_c, "Work Product"),equal($related_to_c, "Single Test System"),equal($related_to_c, "Multiple Test Systems")),
                        related($m03_work_product_ii_inventory_item_2,"name"),
                        ifElse(equal($related_to_c, "Sales"), related($m01_sales_ii_inventory_item_1,"name"), "")
                    ),
                    " ",
                    $anml_animals_ii_inventory_item_1_name,
                    " ",
                    $product_name_c,
                    " ",
                    $solution_name_c,
                    " ",
                    $type_2,
                    " ",
                    $subtype,
                    $subtype_product_c,
                    $subtype_specimen_c,
                )',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'disabled',
            'params' => array(
                'target' => 'name',
                'value' => 'true',
            ),
        ),
    ),
);*/

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/status_dep.php


$dependencies['II_Inventory_Item']['status_dep'] = array(
    'hooks'         => array("all"),
    'trigger'       => 'true',
    'triggerFields' => array('status'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'status',
                'value'  => 'true',
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/sales_visibility_dep.php


$dependencies['II_Inventory_Item']['sales_visibility_dep'] = array(
    'hooks'         => array("edit"),
    'triggerFields' => array('related_to_c'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'm01_sales_ii_inventory_item_1_name',
                'value'  => 'equal($related_to_c, "Sales")',
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/poi_ori_visibility_dep.php


$dependencies['II_Inventory_Item']['poi_ori_visibility_dep'] = array(
    'hooks'         => array("all"),
    'triggerFields' => array('related_to_c'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'poi_purchase_order_item_ii_inventory_item_1_name',
                'value'  => 'equal($related_to_c, "Purchase Order Item")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'poi_purchase_order_item_ii_inventory_item_1_name',
                'value'  => 'equal($related_to_c, "Purchase Order Item")',
            ),
        ),
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'ori_order_request_item_ii_inventory_item_1_name',
                'value'  => 'equal($related_to_c, "Order Request Item")',
            ),
        ),
        array(
            'name'   => 'ReadOnly',
            'params' => array(
                'target' => 'ori_order_request_item_ii_inventory_item_1_name',
                'value'  => 'equal($related_to_c, "Order Request Item")',
            ),
        ),
    ),
);
/*To set type and subtype list from ori/poi into II type and subtype */
$dependencies['II_Inventory_Item']['setoptions_type'] = array(
    'hooks' => array("all"),
    'trigger' => 'or(equal($related_to_c, "Purchase Order Item"),equal($related_to_c, "Order Request Item"))',
    'triggerFields' => array('related_to_c','type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'type_2',
                'keys' => 'getDropdownKeySet("product_category_list")',
                'labels' => 'getDropdownValueSet("product_category_list")'
            ),
        ),
    ),
);


?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/location_dep.php


$dependencies['II_Inventory_Item']['setoptions_location_type'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('test_ii_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'location_type',
                'keys' => 'ifElse(greaterThan(strlen($test_ii_c),0), getDropdownKeySet("im_location_type_list"), getDropdownKeySet("known_unknown_list"))',
                'labels' => 'ifElse(greaterThan(strlen($test_ii_c),0), getDropdownValueSet("im_location_type_list"), getDropdownValueSet("known_unknown_list"))'
            ),
        ),
    ),
);

$dependencies['II_Inventory_Item']['location_dep'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('location_type', 'location_building', 'location_room', 'location_shelf', 'location_cabinet', 'location_equipment', 'storage_medium', 'storage_condition', 'test_ii_c','status'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'location_room',
                'value' => 'not(greaterThan(strlen($test_ii_c), 0))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'location_type',
                'value' => 'not(greaterThan(strlen($ship_to_contact), 0))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'location_building',
                'value' => 'not(greaterThan(strlen($ship_to_contact), 0))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'location_room',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),equal($location_type,"Known"))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_type',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),not(greaterThan(strlen($ship_to_address), 0)),not(greaterThan(strlen($ship_to_company), 0)), not(equal($status, "Discarded")),not(equal($status, "Obsolete")),not(equal($status, "Exhausted")))',                
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_building',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),isInList($location_type, createList("Known", "Cabinet", "Equipment", "Room", "RoomCabinet", "RoomShelf", "Shelf")))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_room',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),isInList($location_type, createList("Known", "Cabinet", "Equipment", "Room", "RoomCabinet", "RoomShelf", "Shelf")))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_equipment',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),isInList($location_type, createList("Known", "Cabinet", "Equipment", "Room", "RoomCabinet", "RoomShelf", "Shelf")))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_shelf',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),isInList($location_type, createList("Known", "Cabinet", "Equipment", "Room", "RoomCabinet", "RoomShelf", "Shelf")))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'location_cabinet',
                'value' => 'and(not(greaterThan(strlen($ship_to_contact), 0)),isInList($location_type, createList("Known", "Cabinet", "Equipment", "Room", "RoomCabinet", "RoomShelf", "Shelf")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_type',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        //, not(equal(strlen($location_type), "Known"),equal(strlen($location_type), "Unknown"))  
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_building',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_room',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_shelf',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_equipment',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_cabinet',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        /*array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'storage_condition',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),greaterThan(strlen(related($ii_inventory_item_im_inventory_management_1,"end_condition")),0),greaterThan(strlen($storage_condition),0))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'storage_medium',
                'value' => 'or(greaterThan(strlen($test_ii_c),0),greaterThan(strlen(related($ii_inventory_item_im_inventory_management_1,"ending_storage_media")),0))',
            ),
        ),*/

        /* Prakash 1117 */
       /* array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_bin_c',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_cubby_c',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_rack_c',
                'value' => 'and(greaterThan(strlen($test_ii_c),0),not(equal($location_type, "Known")),not(equal($location_type, "Unknown")))',
            ),
        ),*/
    ),
);
/*#391 : 03 March 2021 */
$dependencies['II_Inventory_Item']['Shipping_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Cabinet", "Equipment", "Room", "RoomCabinet", "RoomShelf", "Shelf"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'contact_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'ca_company_address_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'account_id_c',
                'value'  => '',
            ),
        ),    
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'ship_to_contact',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'ship_to_company',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'ship_to_address',
                'value'  => '',
            ),
        ),
       
     ),
);
/*#391 : 08 March 2021 */
$dependencies['II_Inventory_Item']['cabinet_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Cabinet"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'rms_room_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_room',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['II_Inventory_Item']['equipement_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Equipment"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'rms_room_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_room',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['II_Inventory_Item']['shelf_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Shelf"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'rms_room_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_room',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['II_Inventory_Item']['room_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("Room"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['II_Inventory_Item']['roomshelf_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("RoomShelf"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_cabinet',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
       
     ),
);
$dependencies['II_Inventory_Item']['roomscabinet_fields_dep'] = array(
    'hooks'         => array("edit"),
    'trigger' => 'isInList($location_type, createList("RoomCabinet"))',
    'triggerFields' => array('location_type'),
    'onload'        => true,
    'actions'       => array(
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_shelf',
                'value'  => '',
            ),
        ),
       
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'location_equipment',
                'value'  => '',
            ),
        ),
        array(
            'name'   => 'SetValue',
            'params' => array(
                'target' => 'equip_equipment_id_c',
                'value'  => '',
            ),
        ),
       
     ),
);
/**12 nov 2021 : prod bug fix 391 */
/*#391 : 03 March 2021 */
$dependencies['II_Inventory_Item']['storage_dep'] = array(
    'hooks'         => array("all"),
    'trigger' => 'and(greaterThan(strlen($test_ii_c),0),greaterThan(strlen(related($ii_inventory_item_im_inventory_management_1,"end_condition")),0),greaterThan(strlen($storage_condition),0))',
    'triggerFields' => array('storage_condition','test_ii_c','ii_inventory_item_im_inventory_management_1','end_condition'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'storage_condition',
                'value' => 'true',
            ),
        ),           
       
     ),
);

$dependencies['II_Inventory_Item']['storage_medium_dep'] = array(
    'hooks'         => array("all"),
    'trigger' => 'or(greaterThan(strlen($test_ii_c),0),greaterThan(strlen(related($ii_inventory_item_im_inventory_management_1,"ending_storage_media")),0))',
    'triggerFields' => array('storage_medium','test_ii_c','ii_inventory_item_im_inventory_management_1','ending_storage_media'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'storage_medium',
                'value' => 'true',
            ),
        ),           
       
     ),
);

/**////////// */
/*12 nov 2021 : 1117 bug fix prod */
$dependencies['II_Inventory_Item']['location_bin_cubb_rack_dep'] = array(
    'hooks'         => array("all"),
    'trigger' => 'and(greaterThan(strlen($test_ii_c),0),not(isInList($location_type,createList("Known","Unknown"))))',
    'triggerFields' => array('test_ii_c','location_type'),
    'onload'        => true,
    'actions'       => array( 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_bin_c',
                'value' => 'true',
            ),
        ), 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_cubby_c',
                'value' => 'true',
            ),
        ), 
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'location_rack_c',
                'value' => 'true',
            ),
        ),           
       
     ),
);
/**/////// */

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Dependencies/test_type_dependency.php

$dependencies['II_Inventory_Item']['set_multi_vis_drop'] = array(
    'hooks' => array("all"),
    'trigger' => 'ifElse(equal($category,"Specimen"),true,false)',
    'triggerFields' => array('type_2'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'test_type_c',
                'keys' => 'ifElse(
                    and(equal($type_2,"Whole Blood"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","CBC","CBC with Reticulocytes","Chemistry","Custom Chemistry","PK in Whole Blood","PK in Serum","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Whole Blood"),equal($pk_samples_c,"NA")),
                    createList("","PK in Whole Blood","Other"),
                    ifElse(
                    and(equal($type_2,"Balloons"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Other","PK in Balloons"),
                    ifElse(
                    and(equal($type_2,"Balloons"),equal($pk_samples_c,"NA")),
                    createList("","Other","PK in Balloons"),
                    ifElse(
                    and(equal($type_2,"Serum"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Amylase andor Lipase","Haptoglobin","Troponin1","Ionized Calcium","PK in Serum"),
                    ifElse(
                    and(equal($type_2,"Serum"),equal($pk_samples_c,"NA")),
                    createList("","PK in Serum","Other"),
                    ifElse(
                    and(equal($type_2,"EDTA Plasma"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"EDTA Plasma"),equal($pk_samples_c,"NA")),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Na Heparin Plasma"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Free Hemoglobin"),
                    ifElse(
                    and(equal($type_2,"Na Heparin Plasma"),equal($pk_samples_c,"NA")),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"NaCit Plasma"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Coagulation","Anti Factor Xa","D Dimer","Fibrinogen","PT aPTT","PT aPTT Fibrinogen"),
                    ifElse(
                    and(equal($type_2,"NaCit Plasma"),equal($pk_samples_c,"NA")),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Urine"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Urinalysis","Urine ProteinCreatinine Ratio"),
                    ifElse(
                    and(equal($type_2,"Urine"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Fecal"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Fecal Ova and Parasite","Cryptosporidium ELISA","Giardia ELISA"),
                    ifElse(
                    and(equal($type_2,"Fecal"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Other"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Other"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Culture"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Aerobic and Anaerobic Culture ID Bacteria and Fungi","Aerobic Culture and ID Bacteria only","Aerobic Culture and ID Fungi only","Blood Culture"),
                    ifElse(
                    and(equal($type_2,"Culture"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Whole Blood"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Whole Blood","Other"),
                    ifElse(
                    and(equal($type_2,"Balloons"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Balloons","Other"),
                    ifElse(
                    and(equal($type_2,"Serum"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Serum","Other"),
                    ifElse(
                    and(equal($type_2,"EDTA Plasma"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Na Heparin Plasma"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"NaCit Plasma"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Urine"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Fecal"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Other"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Culture"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Block"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Extract"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Slide"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Tissue"),equal($pk_samples_c,"Yes")),
                    createList("","PK in Tissue","Other"),
                    ifElse(
                    and(equal($type_2,"Extract"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Slide"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Tissue"),equal($pk_samples_c,"NA")),
                    createList("","PK in Tissue","Other"),
                    ifElse(
                    and(equal($type_2,"Block"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    createList("")))))))))))))))))))))))))))))))))))))))',
                'labels' => 'ifElse(
                    and(equal($type_2,"Whole Blood"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","CBC","CBC with Reticulocytes","Chemistry","Custom Chemistry","Pharmacokinetic (PK) in Whole Blood","Pharmacokinetic (PK) in Serum","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Whole Blood"),equal($pk_samples_c,"NA")),
                    createList("","Pharmacokinetic (PK) in Whole Blood","Other"),
                    ifElse(
                    and(equal($type_2,"Balloons"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Other","Pharmacokinetic (PK) in Balloons"),
                    ifElse(
                    and(equal($type_2,"Balloons"),equal($pk_samples_c,"NA")),
                    createList("","Other","Pharmacokinetic (PK) in Balloons"),
                    ifElse(
                    and(equal($type_2,"Serum"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Amylase and/or Lipase","Haptoglobin","Troponin1","Ionized Calcium","Pharmacokinetic (PK) in Serum"),
                    ifElse(
                    and(equal($type_2,"Serum"),equal($pk_samples_c,"NA")),
                    createList("","Pharmacokinetic (PK) in Serum","Other"),
                    ifElse(
                    and(equal($type_2,"EDTA Plasma"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"EDTA Plasma"),equal($pk_samples_c,"NA")),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Na Heparin Plasma"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Free Hemoglobin"),
                    ifElse(
                    and(equal($type_2,"Na Heparin Plasma"),equal($pk_samples_c,"NA")),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"NaCit Plasma"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Coagulation","Anti-Factor Xa","D-Dimer","Fibrinogen","PT, aPTT","PT, aPTT, Fibrinogen"),
                    ifElse(
                    and(equal($type_2,"NaCit Plasma"),equal($pk_samples_c,"NA")),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Urine"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Urinalysis","Urine Protein/Creatinine Ratio"),
                    ifElse(
                    and(equal($type_2,"Urine"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Fecal"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Fecal Ova and Parasite","Cryptosporidium ELISA","Giardia ELISA"),
                    ifElse(
                    and(equal($type_2,"Fecal"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Other"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Other"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Culture"),equal($pk_samples_c,"NA"),not(equal($id,""))),
                    createList("","Aerobic and Anaerobic Culture, ID-Bacteria and Fungi","Aerobic Culture and ID-Bacteria only","Aerobic Culture and ID-Fungi only","Blood Culture"),
                    ifElse(
                    and(equal($type_2,"Culture"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Whole Blood"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Whole Blood","Other"),
                    ifElse(
                    and(equal($type_2,"Balloons"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Balloons","Other"),
                    ifElse(
                    and(equal($type_2,"Serum"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Serum","Other"),
                    ifElse(
                    and(equal($type_2,"EDTA Plasma"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Na Heparin Plasma"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"NaCit Plasma"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Plasma","Other"),
                    ifElse(
                    and(equal($type_2,"Urine"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Fecal"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Other"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Culture"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Block"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Extract"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Slide"),equal($pk_samples_c,"Yes")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Tissue"),equal($pk_samples_c,"Yes")),
                    createList("","Pharmacokinetic (PK) in Tissue","Other"),
                    ifElse(
                    and(equal($type_2,"Extract"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Slide"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    ifElse(
                    and(equal($type_2,"Tissue"),equal($pk_samples_c,"NA")),
                    createList("","Pharmacokinetic (PK) in Tissue","Other"),
                    ifElse(
                    and(equal($type_2,"Block"),equal($pk_samples_c,"NA")),
                    createList("","Other"),
                    createList("")))))))))))))))))))))))))))))))))))))))'
            ),
        ),
    ),
);

$dependencies['II_Inventory_Item']['set_vis_drop01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'test_type_c',
                'value'  => 'and(equal($category,"Specimen"),isInList($type_2,createList("Balloons","Whole Blood","Serum","EDTA Plasma","Na Heparin Plasma","NaCit Plasma","Urine","Fecal","Culture","Other","Tissue","Slide","Block","Extract")))',
            ),
        ),
    ),
);
$dependencies['II_Inventory_Item']['set_vis_type2_123'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('pk_samples_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name'   => 'SetVisibility',
            'params' => array(
                'target' => 'type_2',
                'value'  => 'and(equal($category,"Specimen"),or(equal($pk_samples_c,"Yes"),equal($pk_samples_c,"NA")))',
            ),
        ),
    ),
);

?>
