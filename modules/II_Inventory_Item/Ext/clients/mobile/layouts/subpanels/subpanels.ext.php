<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-11-09 09:39:34
$viewdefs['II_Inventory_Item']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'context' => 
  array (
    'link' => 'ii_inventory_item_im_inventory_management_1',
  ),
);

// created: 2021-11-09 10:07:50
$viewdefs['II_Inventory_Item']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_ii_inventory_item_2',
  ),
);