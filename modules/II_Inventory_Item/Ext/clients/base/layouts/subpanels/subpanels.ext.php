<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-07-31 13:40:17
$viewdefs['II_Inventory_Item']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'context' => 
  array (
    'link' => 'ii_inventory_item_im_inventory_management_1',
  ),
);

// created: 2020-07-31 13:11:35
$viewdefs['II_Inventory_Item']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_ii_inventory_item_2',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['II_Inventory_Item']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'ii_inventory_item_im_inventory_management_1',
  'view' => 'subpanel-for-ii_inventory_item-ii_inventory_item_im_inventory_management_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['II_Inventory_Item']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_ii_inventory_item_2',
  'view' => 'subpanel-for-ii_inventory_item-m03_work_product_ii_inventory_item_2',
);
