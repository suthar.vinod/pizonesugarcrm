<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/en_us.customii_inventory_item_im_inventory_management_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE'] = 'Inventory Management';
$mod_strings['LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Management';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/en_us.customic_inventory_collection_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_IC_INVENTORY_COLLECTION_TITLE'] = 'Inventory Collections';
$mod_strings['LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Collections';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/en_us.customori_order_request_item_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Order Request Items';
$mod_strings['LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Order Request Items';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/en_us.custompoi_purchase_order_item_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE'] = 'Purchase Order Items';
$mod_strings['LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Purchase Order Items';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/en_us.customri_received_items_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_RI_RECEIVED_ITEMS_TITLE'] = 'Received Items';
$mod_strings['LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Received Items';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/en_us.customm01_sales_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/en_us.customm01_sales_ii_inventory_item_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_II_INVENTORY_ITEM_2_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_II_INVENTORY_ITEM_2_FROM_II_INVENTORY_ITEM_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/en_us.customanml_animals_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_ANML_ANIMALS_TITLE'] = 'Test Systems';
$mod_strings['LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Test Systems';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/en_us.customm03_work_product_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/en_us.customm03_work_product_ii_inventory_item_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_II_INVENTORY_ITEM_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_LOCATION_VALUES_C'] = 'Location Values';
$mod_strings['INACTIVE'] = 'Inactive';
$mod_strings['LBL_TEST_TYPE'] = 'Test Type';
$mod_strings['LBL_STERILIZATION_EXP_DATE'] = 'Sterilization Expiration Date';
$mod_strings['LBL_PRODUCT_ID_2'] = 'Product ID';
$mod_strings['LBL_TEST_II'] = 'Test II';
$mod_strings['LBL_TIMEPOINT_END_INTEGER'] = 'Timepoint End Integer';
$mod_strings['LBL_TIMEPOINT_NAME'] = 'Timepoint Name';
$mod_strings['LBL_ANALYZE_BY_DATE'] = 'Analyze by Date';
$mod_strings['LBL_STABILITY_CONSIDERATIONS'] = 'PK Stability Considerations';
$mod_strings['LBL_SPECIMEN_TYPE_1'] = 'Specimen Type 1';
$mod_strings['LBL_PK_SAMPLES'] = 'Multiple Aliquots Needed?';
$mod_strings['LBL_SPECIMEN_TYPE_2'] = 'Specimen Type 2';
$mod_strings['LBL_SPECIMEN_TYPE_3'] = 'Specimen Type 3';
$mod_strings['LBL_SPECIMEN_TYPE_4'] = 'Specimen Type 4';
$mod_strings['LBL_SPECIMEN_TYPE_5'] = 'Specimen Type 5';
$mod_strings['LBL_TEST_TYPE_1'] = 'Test Type 1';
$mod_strings['LBL_TEST_TYPE_2'] = 'Test Type 2';
$mod_strings['LBL_TEST_TYPE_3'] = 'Test Type 3';
$mod_strings['LBL_TEST_TYPE_4'] = 'Test Type 4';
$mod_strings['LBL_TEST_TYPE_5'] = 'Test Type 5';
$mod_strings['LBL_LOCATION_EQUI_SERIAL_NO_HIDE'] = 'Location Equi Sr No Hidden';
$mod_strings['LBL_LOCATION_BIN'] = 'Location (Bin #)';
$mod_strings['LBL_PROCESSED_PER_PROTOCOL'] = 'Processed per Protocol';
$mod_strings['LBL_OTHER_TYPE'] = 'Other Specimen Type';
$mod_strings['LBL_REMINDER_MAIL_SENT_C'] = 'Reminder Mail Sent';
$mod_strings['LBL_TS_HIDDEN_STRING'] = 'TS Hidden String';
$mod_strings['LBL_LOCATION_CUBBY'] = 'Location (Slot #)';
$mod_strings['LBL_LOCATION_RACK'] = 'Location (Freezer Rack #)';
$mod_strings['LBL_QUANTITY_2'] = 'Quantity';
$mod_strings['LBL_OTHER_TEST_TYPE'] = 'Other Test Type';
$mod_strings['LBL_NUMBER_OF_ALIQUOTS'] = 'Number of Aliquots';
$mod_strings['LBL_LOCATION_SHELF'] = 'Location (Room Shelf)';
$mod_strings['LBL_LOT_NUMBER'] = 'Unique ID';
$mod_strings['LBL_STORAGE_CONDITION'] = 'Current Storage Condition';
$mod_strings['LBL_STORAGE_MEDIUM'] = 'Current Storage Medium';
$mod_strings['LBL_TIMEPOINT_INTEGER'] = 'Timepoint Start Integer';
$mod_strings['LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_NAME_FIELD_TITLE'] = 'Received Items';
$mod_strings['LBL_TIMEPOINT_END_INTEGER_C'] = 'Timepoint End Integer';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Location';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/en_us.customm03_work_product_deliverable_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Work Product Deliverables';

?>
