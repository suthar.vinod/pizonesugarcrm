<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/es_ES.customii_inventory_item_im_inventory_management_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE'] = 'Inventory Management';
$mod_strings['LBL_II_INVENTORY_ITEM_IM_INVENTORY_MANAGEMENT_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Management';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/es_ES.customic_inventory_collection_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_IC_INVENTORY_COLLECTION_TITLE'] = 'Inventory Collections';
$mod_strings['LBL_IC_INVENTORY_COLLECTION_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Collections';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/es_ES.customori_order_request_item_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Order Request Items';
$mod_strings['LBL_ORI_ORDER_REQUEST_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Order Request Items';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/es_ES.custompoi_purchase_order_item_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE'] = 'Purchase Order Items';
$mod_strings['LBL_POI_PURCHASE_ORDER_ITEM_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Purchase Order Items';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/es_ES.customri_received_items_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_RI_RECEIVED_ITEMS_TITLE'] = 'Received Items';
$mod_strings['LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Received Items';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/es_ES.customm01_sales_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/es_ES.customm01_sales_ii_inventory_item_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_II_INVENTORY_ITEM_2_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_II_INVENTORY_ITEM_2_FROM_II_INVENTORY_ITEM_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/es_ES.customanml_animals_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_ANML_ANIMALS_TITLE'] = 'Test Systems';
$mod_strings['LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Test Systems';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/es_ES.customm03_work_product_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/es_ES.customm03_work_product_ii_inventory_item_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_II_INVENTORY_ITEM_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/II_Inventory_Item/Ext/Language/es_ES.customm03_work_product_deliverable_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Work Product Deliverables';

?>
