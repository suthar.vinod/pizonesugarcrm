<?php
// created: 2022-12-13 04:43:54
$viewdefs['Tasks']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'link' => true,
          'label' => 'LBL_LIST_SUBJECT',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'm01_sales_tasks_1_name',
          'label' => 'LBL_M01_SALES_TASKS_1_FROM_M01_SALES_TITLE',
          'enabled' => true,
          'id' => 'M01_SALES_TASKS_1M01_SALES_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
          'width' => 'small',
        ),
        2 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_LIST_ASSIGNED_TO_NAME',
          'id' => 'ASSIGNED_USER_ID',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'status',
          'label' => 'LBL_LIST_STATUS',
          'link' => false,
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'date_due',
          'label' => 'LBL_LIST_DUE_DATE',
          'type' => 'datetimecombo-colorcoded',
          'css_class' => 'overflow-visible',
          'completed_status_value' => 'Completed',
          'link' => false,
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'sa_wp_id_c',
          'label' => 'LBL_SA_WP_ID',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'parent_name',
          'label' => 'LBL_LIST_RELATED_TO',
          'dynamic_module' => 'PARENT_TYPE',
          'id' => 'PARENT_ID',
          'link' => true,
          'enabled' => true,
          'default' => true,
          'sortable' => false,
          'ACLTag' => 'PARENT',
          'related_fields' => 
          array (
            0 => 'parent_id',
            1 => 'parent_type',
          ),
        ),
        7 => 
        array (
          'name' => 'contact_name',
          'label' => 'LBL_LIST_CONTACT',
          'link' => true,
          'id' => 'CONTACT_ID',
          'module' => 'Contacts',
          'enabled' => true,
          'default' => true,
          'ACLTag' => 'CONTACT',
          'related_fields' => 
          array (
            0 => 'contact_id',
          ),
        ),
        8 => 
        array (
          'name' => 'team_name',
          'label' => 'LBL_LIST_TEAM',
          'enabled' => true,
          'default' => false,
        ),
        9 => 
        array (
          'name' => 'date_start',
          'label' => 'LBL_LIST_START_DATE',
          'css_class' => 'overflow-visible',
          'link' => false,
          'enabled' => true,
          'default' => false,
        ),
        10 => 
        array (
          'name' => 'm03_work_product_tasks_1_name',
          'label' => 'LBL_M03_WORK_PRODUCT_TASKS_1_FROM_M03_WORK_PRODUCT_TITLE',
          'enabled' => true,
          'id' => 'M03_WORK_PRODUCT_TASKS_1M03_WORK_PRODUCT_IDA',
          'link' => true,
          'sortable' => false,
          'default' => false,
        ),
        11 => 
        array (
          'name' => 'task_type_c',
          'label' => 'LBL_TASK_TYPE',
          'enabled' => true,
          'default' => false,
        ),
        12 => 
        array (
          'name' => 'date_modified',
          'enabled' => true,
          'default' => false,
        ),
        13 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'default' => false,
          'readonly' => true,
        ),
        14 => 
        array (
          'name' => 'no_necropsy_needed_c',
          'label' => 'LBL_NO_NECROPSY_NEEDED',
          'enabled' => true,
          'default' => false,
        ),
      ),
    ),
  ),
);