<?php
// created: 2016-01-06 02:12:14
$viewdefs['Tasks']['base']['menu']['quickcreate'] = array (
  'layout' => 'create',
  'label' => 'LNK_NEW_TASK',
  'visible' => true,
  'order' => 9,
  'icon' => 'fa-plus',
);