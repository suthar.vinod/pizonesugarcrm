<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Layoutdefs/tasks_m03_work_product_1_Tasks.php

 // created: 2016-02-25 20:23:10
$layout_defs["Tasks"]["subpanel_setup"]['tasks_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TASKS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'tasks_m03_work_product_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Layoutdefs/tasks_m03_work_product_deliverable_1_Tasks.php

 // created: 2016-02-29 15:01:15
$layout_defs["Tasks"]["subpanel_setup"]['tasks_m03_work_product_deliverable_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product_Deliverable',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'get_subpanel_data' => 'tasks_m03_work_product_deliverable_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Layoutdefs/_overrideTask_subpanel_tasks_m01_sales_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Tasks']['subpanel_setup']['tasks_m01_sales_1']['override_subpanel_name'] = 'Task_subpanel_tasks_m01_sales_1';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Layoutdefs/_overrideTask_subpanel_tasks_m03_work_product_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Tasks']['subpanel_setup']['tasks_m03_work_product_1']['override_subpanel_name'] = 'Task_subpanel_tasks_m03_work_product_1';

?>
