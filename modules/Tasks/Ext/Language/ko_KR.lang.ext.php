<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.customm01_sales_tasks_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M01_SALES_TASKS_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_TASKS_1_FROM_TASKS_TITLE_ID'] = 'Sales ID';
$mod_strings['LBL_M01_SALES_TASKS_1_FROM_TASKS_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.customtasks_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TASKS_M01_SALES_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_TASKS_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.customtasks_m01_quote_document_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TASKS_M01_QUOTE_DOCUMENT_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_TASKS_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote Documents';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.customm03_work_product_tasks_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_TASKS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_TASKS_1_FROM_TASKS_TITLE_ID'] = 'Work Products ID';
$mod_strings['LBL_M03_WORK_PRODUCT_TASKS_1_FROM_TASKS_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.customtasks_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.customtasks_m03_work_product_deliverable_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.customm03_work_product_deliverable_tasks_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE_ID'] = 'Work Product Deliverables ID';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE'] = 'Work Product Deliverables';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.customa1a_critical_phase_inspectio_activities_1_tasks.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_TASKS_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE'] = 'Critical Phase Inspections';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.customm01_sales_activities_1_tasks.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_ACTIVITIES_1_TASKS_FROM_M01_SALES_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.customm06_error_tasks_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_TASKS_1_FROM_M06_ERROR_TITLE'] = 'Observations';
$mod_strings['LBL_M06_ERROR_TASKS_1_FROM_TASKS_TITLE'] = 'Observations';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.customm03_work_product_activities_1_tasks.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_TASKS_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.customta_tradeshow_activities_tasks_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE'] = 'Tradeshow Activities';
$mod_strings['LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TASKS_TITLE'] = 'Tradeshow Activities';

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Language/ko_KR.custombid_batch_id_tasks_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_BID_BATCH_ID_TASKS_1_FROM_BID_BATCH_ID_TITLE'] = 'Batch IDs';
$mod_strings['LBL_BID_BATCH_ID_TASKS_1_FROM_TASKS_TITLE'] = 'Batch IDs';

?>
