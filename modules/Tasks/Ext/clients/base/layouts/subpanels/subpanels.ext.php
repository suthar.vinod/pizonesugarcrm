<?php
// WARNING: The contents of this file are auto-generated.


// created: 2016-02-25 20:23:10
$viewdefs['Tasks']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TASKS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'tasks_m03_work_product_1',
  ),
);

// created: 2016-02-29 15:01:15
$viewdefs['Tasks']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'context' => 
  array (
    'link' => 'tasks_m03_work_product_deliverable_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['Tasks']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'opportunities',
  'view' => 'subpanel-for-tasks-opportunities',
);


//auto-generated file DO NOT EDIT
$viewdefs['Tasks']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'tasks_m03_work_product_1',
  'view' => 'subpanel-for-tasks-tasks_m03_work_product_1',
);
