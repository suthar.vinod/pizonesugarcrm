<?php
// WARNING: The contents of this file are auto-generated.


// created: 2016-02-25 20:23:10
$viewdefs['Tasks']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TASKS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'tasks_m03_work_product_1',
  ),
);

// created: 2016-02-29 15:01:16
$viewdefs['Tasks']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'context' => 
  array (
    'link' => 'tasks_m03_work_product_deliverable_1',
  ),
);