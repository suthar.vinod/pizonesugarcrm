<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/m01_sales_tasks_1_Tasks.php

// created: 2016-01-25 23:00:46
$dictionary["Task"]["fields"]["m01_sales_tasks_1"] = array (
  'name' => 'm01_sales_tasks_1',
  'type' => 'link',
  'relationship' => 'm01_sales_tasks_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_TASKS_1_FROM_TASKS_TITLE',
  'id_name' => 'm01_sales_tasks_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["Task"]["fields"]["m01_sales_tasks_1_name"] = array (
  'name' => 'm01_sales_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_TASKS_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_tasks_1m01_sales_ida',
  'link' => 'm01_sales_tasks_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["m01_sales_tasks_1m01_sales_ida"] = array (
  'name' => 'm01_sales_tasks_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_TASKS_1_FROM_TASKS_TITLE_ID',
  'id_name' => 'm01_sales_tasks_1m01_sales_ida',
  'link' => 'm01_sales_tasks_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/m03_work_product_tasks_1_Tasks.php

// created: 2016-02-25 20:01:03
$dictionary["Task"]["fields"]["m03_work_product_tasks_1"] = array (
  'name' => 'm03_work_product_tasks_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_tasks_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_TASKS_1_FROM_TASKS_TITLE',
  'id_name' => 'm03_work_product_tasks_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["Task"]["fields"]["m03_work_product_tasks_1_name"] = array (
  'name' => 'm03_work_product_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_TASKS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_tasks_1m03_work_product_ida',
  'link' => 'm03_work_product_tasks_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["m03_work_product_tasks_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_tasks_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_TASKS_1_FROM_TASKS_TITLE_ID',
  'id_name' => 'm03_work_product_tasks_1m03_work_product_ida',
  'link' => 'm03_work_product_tasks_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/tasks_m03_work_product_1_Tasks.php

// created: 2016-02-25 20:23:10
$dictionary["Task"]["fields"]["tasks_m03_work_product_1"] = array (
  'name' => 'tasks_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'tasks_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_1_FROM_TASKS_TITLE',
  'id_name' => 'tasks_m03_work_product_1tasks_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/tasks_m03_work_product_deliverable_1_Tasks.php

// created: 2016-02-29 15:01:15
$dictionary["Task"]["fields"]["tasks_m03_work_product_deliverable_1"] = array (
  'name' => 'tasks_m03_work_product_deliverable_1',
  'type' => 'link',
  'relationship' => 'tasks_m03_work_product_deliverable_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Deliverable',
  'bean_name' => 'M03_Work_Product_Deliverable',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_TASKS_TITLE',
  'id_name' => 'tasks_m03_work_product_deliverable_1tasks_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/m03_work_product_deliverable_tasks_1_Tasks.php

// created: 2016-02-29 19:53:08
$dictionary["Task"]["fields"]["m03_work_product_deliverable_tasks_1"] = array (
  'name' => 'm03_work_product_deliverable_tasks_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_deliverable_tasks_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Deliverable',
  'bean_name' => 'M03_Work_Product_Deliverable',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE',
  'id_name' => 'm03_work_pbb72verable_ida',
  'link-type' => 'one',
);
$dictionary["Task"]["fields"]["m03_work_product_deliverable_tasks_1_name"] = array (
  'name' => 'm03_work_product_deliverable_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'save' => true,
  'id_name' => 'm03_work_pbb72verable_ida',
  'link' => 'm03_work_product_deliverable_tasks_1',
  'table' => 'm03_work_product_deliverable',
  'module' => 'M03_Work_Product_Deliverable',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["m03_work_pbb72verable_ida"] = array (
  'name' => 'm03_work_pbb72verable_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE_ID',
  'id_name' => 'm03_work_pbb72verable_ida',
  'link' => 'm03_work_product_deliverable_tasks_1',
  'table' => 'm03_work_product_deliverable',
  'module' => 'M03_Work_Product_Deliverable',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:57
$dictionary['Task']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/a1a_critical_phase_inspectio_activities_1_tasks_Tasks.php

// created: 2017-09-13 15:16:33
$dictionary["Task"]["fields"]["a1a_critical_phase_inspectio_activities_1_tasks"] = array (
  'name' => 'a1a_critical_phase_inspectio_activities_1_tasks',
  'type' => 'link',
  'relationship' => 'a1a_critical_phase_inspectio_activities_1_tasks',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'vname' => 'LBL_A1A_CRITICAL_PHASE_INSPECTIO_ACTIVITIES_1_TASKS_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/m01_sales_activities_1_tasks_Tasks.php

// created: 2018-12-10 23:02:55
$dictionary["Task"]["fields"]["m01_sales_activities_1_tasks"] = array (
  'name' => 'm01_sales_activities_1_tasks',
  'type' => 'link',
  'relationship' => 'm01_sales_activities_1_tasks',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_M01_SALES_ACTIVITIES_1_TASKS_FROM_M01_SALES_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/m06_error_tasks_1_Tasks.php

// created: 2019-05-19 21:27:08
$dictionary["Task"]["fields"]["m06_error_tasks_1"] = array (
  'name' => 'm06_error_tasks_1',
  'type' => 'link',
  'relationship' => 'm06_error_tasks_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'side' => 'right',
  'vname' => 'LBL_M06_ERROR_TASKS_1_FROM_TASKS_TITLE',
  'id_name' => 'm06_error_tasks_1m06_error_ida',
  'link-type' => 'one',
);
$dictionary["Task"]["fields"]["m06_error_tasks_1_name"] = array (
  'name' => 'm06_error_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M06_ERROR_TASKS_1_FROM_M06_ERROR_TITLE',
  'save' => true,
  'id_name' => 'm06_error_tasks_1m06_error_ida',
  'link' => 'm06_error_tasks_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["m06_error_tasks_1m06_error_ida"] = array (
  'name' => 'm06_error_tasks_1m06_error_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M06_ERROR_TASKS_1_FROM_TASKS_TITLE_ID',
  'id_name' => 'm06_error_tasks_1m06_error_ida',
  'link' => 'm06_error_tasks_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/m03_work_product_activities_1_tasks_Tasks.php

// created: 2019-08-05 12:04:19
$dictionary["Task"]["fields"]["m03_work_product_activities_1_tasks"] = array (
  'name' => 'm03_work_product_activities_1_tasks',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_tasks',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_TASKS_FROM_M03_WORK_PRODUCT_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/ta_tradeshow_activities_tasks_1_Tasks.php

// created: 2019-10-31 11:25:05
$dictionary["Task"]["fields"]["ta_tradeshow_activities_tasks_1"] = array (
  'name' => 'ta_tradeshow_activities_tasks_1',
  'type' => 'link',
  'relationship' => 'ta_tradeshow_activities_tasks_1',
  'source' => 'non-db',
  'module' => 'TA_Tradeshow_Activities',
  'bean_name' => 'TA_Tradeshow_Activities',
  'side' => 'right',
  'vname' => 'LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TASKS_TITLE',
  'id_name' => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
  'link-type' => 'one',
);
$dictionary["Task"]["fields"]["ta_tradeshow_activities_tasks_1_name"] = array (
  'name' => 'ta_tradeshow_activities_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'save' => true,
  'id_name' => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
  'link' => 'ta_tradeshow_activities_tasks_1',
  'table' => 'ta_tradeshow_activities',
  'module' => 'TA_Tradeshow_Activities',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida"] = array (
  'name' => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TASKS_TITLE_ID',
  'id_name' => 'ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida',
  'link' => 'ta_tradeshow_activities_tasks_1',
  'table' => 'ta_tradeshow_activities',
  'module' => 'TA_Tradeshow_Activities',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_name.php

 // created: 2020-02-27 13:26:26
$dictionary['Task']['fields']['name']['audited']=true;
$dictionary['Task']['fields']['name']['massupdate']=false;
$dictionary['Task']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['name']['merge_filter']='disabled';
$dictionary['Task']['fields']['name']['unified_search']=false;
$dictionary['Task']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.45',
  'searchable' => true,
);
$dictionary['Task']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_status.php

 // created: 2020-02-27 13:41:07
$dictionary['Task']['fields']['status']['audited']=true;
$dictionary['Task']['fields']['status']['massupdate']=true;
$dictionary['Task']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['status']['merge_filter']='disabled';
$dictionary['Task']['fields']['status']['unified_search']=false;
$dictionary['Task']['fields']['status']['calculated']=false;
$dictionary['Task']['fields']['status']['dependency']=false;
$dictionary['Task']['fields']['status']['default']='Not Completed';
$dictionary['Task']['fields']['status']['full_text_search']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_date_due.php

 // created: 2020-02-27 13:43:14
$dictionary['Task']['fields']['date_due']['audited']=true;
$dictionary['Task']['fields']['date_due']['massupdate']=true;
$dictionary['Task']['fields']['date_due']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['date_due']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['date_due']['merge_filter']='disabled';
$dictionary['Task']['fields']['date_due']['unified_search']=false;
$dictionary['Task']['fields']['date_due']['full_text_search']=array (
);
$dictionary['Task']['fields']['date_due']['calculated']=false;
$dictionary['Task']['fields']['date_due']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_priority.php

 // created: 2020-02-27 13:45:17
$dictionary['Task']['fields']['priority']['default']='High';
$dictionary['Task']['fields']['priority']['audited']=true;
$dictionary['Task']['fields']['priority']['massupdate']=true;
$dictionary['Task']['fields']['priority']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['priority']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['priority']['merge_filter']='disabled';
$dictionary['Task']['fields']['priority']['unified_search']=false;
$dictionary['Task']['fields']['priority']['calculated']=false;
$dictionary['Task']['fields']['priority']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_task_type_c.php

 // created: 2020-05-21 17:58:25
$dictionary['Task']['fields']['task_type_c']['labelValue']='Task Type';
$dictionary['Task']['fields']['task_type_c']['dependency']='';
$dictionary['Task']['fields']['task_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_no_necropsy_needed_c.php

 // created: 2020-07-23 09:20:35
$dictionary['Task']['fields']['no_necropsy_needed_c']['labelValue']='No Necropsy Needed';
$dictionary['Task']['fields']['no_necropsy_needed_c']['enforced']='';
$dictionary['Task']['fields']['no_necropsy_needed_c']['dependency']='equal($task_type_c,"Send Necropsy Findings")';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_email_reminder_time_c.php

 // created: 2021-01-14 09:47:22
$dictionary['Task']['fields']['email_reminder_time_c']['labelValue']='Email Reminder Time';
$dictionary['Task']['fields']['email_reminder_time_c']['dependency']='';
$dictionary['Task']['fields']['email_reminder_time_c']['required_formula']='';
$dictionary['Task']['fields']['email_reminder_time_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_review_date_c.php

 // created: 2021-01-14 09:48:04
$dictionary['Task']['fields']['review_date_c']['labelValue']='Review Date';
$dictionary['Task']['fields']['review_date_c']['enforced']='';
$dictionary['Task']['fields']['review_date_c']['dependency']='';
$dictionary['Task']['fields']['review_date_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_parent_name.php

 // created: 2022-02-03 10:00:02
$dictionary['Task']['fields']['parent_name']['audited']=false;
$dictionary['Task']['fields']['parent_name']['massupdate']=false;
$dictionary['Task']['fields']['parent_name']['hidemassupdate']=false;
$dictionary['Task']['fields']['parent_name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['parent_name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['parent_name']['merge_filter']='disabled';
$dictionary['Task']['fields']['parent_name']['unified_search']=false;
$dictionary['Task']['fields']['parent_name']['calculated']=false;
$dictionary['Task']['fields']['parent_name']['dependency']='not(equal($task_type_c,"Schedule Matrix Review"))';
$dictionary['Task']['fields']['parent_name']['related_fields']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_parent_type.php

 // created: 2022-02-03 10:00:02
$dictionary['Task']['fields']['parent_type']['audited']=false;
$dictionary['Task']['fields']['parent_type']['massupdate']=true;
$dictionary['Task']['fields']['parent_type']['hidemassupdate']=false;
$dictionary['Task']['fields']['parent_type']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['parent_type']['duplicate_merge_dom_value']=1;
$dictionary['Task']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['Task']['fields']['parent_type']['unified_search']=false;
$dictionary['Task']['fields']['parent_type']['calculated']=false;
$dictionary['Task']['fields']['parent_type']['len']=255;
$dictionary['Task']['fields']['parent_type']['options']='';
$dictionary['Task']['fields']['parent_type']['vname']='LBL_PARENT_TYPE';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_parent_id.php

 // created: 2022-02-03 10:00:02
$dictionary['Task']['fields']['parent_id']['audited']=false;
$dictionary['Task']['fields']['parent_id']['massupdate']=false;
$dictionary['Task']['fields']['parent_id']['hidemassupdate']=false;
$dictionary['Task']['fields']['parent_id']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['parent_id']['duplicate_merge_dom_value']=1;
$dictionary['Task']['fields']['parent_id']['merge_filter']='disabled';
$dictionary['Task']['fields']['parent_id']['unified_search']=false;
$dictionary['Task']['fields']['parent_id']['calculated']=false;
$dictionary['Task']['fields']['parent_id']['len']=255;
$dictionary['Task']['fields']['parent_id']['vname']='LBL_PARENT_TYPE';
$dictionary['Task']['fields']['parent_id']['reportable']=true;
$dictionary['Task']['fields']['parent_id']['group']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_m06_error_tasks_1m06_error_ida.php

 // created: 2022-02-03 10:01:24
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['name']='m06_error_tasks_1m06_error_ida';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['type']='id';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['source']='non-db';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['vname']='LBL_M06_ERROR_TASKS_1_FROM_TASKS_TITLE_ID';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['id_name']='m06_error_tasks_1m06_error_ida';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['link']='m06_error_tasks_1';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['table']='m06_error';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['module']='M06_Error';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['rname']='id';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['reportable']=false;
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['side']='right';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['massupdate']=false;
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['duplicate_merge']='disabled';
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['hideacl']=true;
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['audited']=false;
$dictionary['Task']['fields']['m06_error_tasks_1m06_error_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_m06_error_tasks_1_name.php

 // created: 2022-02-03 10:01:25
$dictionary['Task']['fields']['m06_error_tasks_1_name']['audited']=false;
$dictionary['Task']['fields']['m06_error_tasks_1_name']['massupdate']=true;
$dictionary['Task']['fields']['m06_error_tasks_1_name']['hidemassupdate']=false;
$dictionary['Task']['fields']['m06_error_tasks_1_name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['m06_error_tasks_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['m06_error_tasks_1_name']['merge_filter']='disabled';
$dictionary['Task']['fields']['m06_error_tasks_1_name']['reportable']=false;
$dictionary['Task']['fields']['m06_error_tasks_1_name']['unified_search']=false;
$dictionary['Task']['fields']['m06_error_tasks_1_name']['calculated']=false;
$dictionary['Task']['fields']['m06_error_tasks_1_name']['dependency']='not(equal($task_type_c,"Schedule Matrix Review"))';
$dictionary['Task']['fields']['m06_error_tasks_1_name']['vname']='LBL_M06_ERROR_TASKS_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sa_wp_id_c.php

 // created: 2022-02-03 10:02:00
$dictionary['Task']['fields']['sa_wp_id_c']['duplicate_merge_dom_value']=0;
$dictionary['Task']['fields']['sa_wp_id_c']['labelValue']='SA Quote ID';
$dictionary['Task']['fields']['sa_wp_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Task']['fields']['sa_wp_id_c']['calculated']='1';
$dictionary['Task']['fields']['sa_wp_id_c']['formula']='related($m01_sales_tasks_1,"work_product_id_c")';
$dictionary['Task']['fields']['sa_wp_id_c']['enforced']='1';
$dictionary['Task']['fields']['sa_wp_id_c']['dependency']='not(equal($task_type_c,"Schedule Matrix Review"))';
$dictionary['Task']['fields']['sa_wp_id_c']['required_formula']='';
$dictionary['Task']['fields']['sa_wp_id_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida.php

 // created: 2022-02-03 10:03:21
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['name']='ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['type']='id';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['source']='non-db';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['vname']='LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_FROM_TASKS_TITLE_ID';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['id_name']='ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['link']='ta_tradeshow_activities_tasks_1';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['table']='ta_tradeshow_activities';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['module']='TA_Tradeshow_Activities';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['rname']='id';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['reportable']=false;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['side']='right';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['massupdate']=false;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['duplicate_merge']='disabled';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['hideacl']=true;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['audited']=false;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1ta_tradeshow_activities_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_ta_tradeshow_activities_tasks_1_name.php

 // created: 2022-02-03 10:03:23
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1_name']['audited']=false;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1_name']['massupdate']=true;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1_name']['hidemassupdate']=false;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1_name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1_name']['merge_filter']='disabled';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1_name']['reportable']=false;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1_name']['unified_search']=false;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1_name']['calculated']=false;
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1_name']['dependency']='not(equal($task_type_c,"Schedule Matrix Review"))';
$dictionary['Task']['fields']['ta_tradeshow_activities_tasks_1_name']['vname']='LBL_TA_TRADESHOW_ACTIVITIES_TASKS_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_m03_work_pbb72verable_ida.php

 // created: 2022-02-03 10:04:20
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['name']='m03_work_pbb72verable_ida';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['type']='id';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['source']='non-db';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['vname']='LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_FROM_TASKS_TITLE_ID';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['id_name']='m03_work_pbb72verable_ida';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['link']='m03_work_product_deliverable_tasks_1';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['table']='m03_work_product_deliverable';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['module']='M03_Work_Product_Deliverable';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['rname']='id';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['reportable']=false;
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['side']='right';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['massupdate']=false;
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['duplicate_merge']='disabled';
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['hideacl']=true;
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['audited']=false;
$dictionary['Task']['fields']['m03_work_pbb72verable_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_m03_work_product_deliverable_tasks_1_name.php

 // created: 2022-02-03 10:04:20
$dictionary['Task']['fields']['m03_work_product_deliverable_tasks_1_name']['audited']=false;
$dictionary['Task']['fields']['m03_work_product_deliverable_tasks_1_name']['massupdate']=true;
$dictionary['Task']['fields']['m03_work_product_deliverable_tasks_1_name']['hidemassupdate']=false;
$dictionary['Task']['fields']['m03_work_product_deliverable_tasks_1_name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['m03_work_product_deliverable_tasks_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['m03_work_product_deliverable_tasks_1_name']['merge_filter']='disabled';
$dictionary['Task']['fields']['m03_work_product_deliverable_tasks_1_name']['reportable']=false;
$dictionary['Task']['fields']['m03_work_product_deliverable_tasks_1_name']['unified_search']=false;
$dictionary['Task']['fields']['m03_work_product_deliverable_tasks_1_name']['calculated']=false;
$dictionary['Task']['fields']['m03_work_product_deliverable_tasks_1_name']['dependency']='not(equal($task_type_c,"Schedule Matrix Review"))';
$dictionary['Task']['fields']['m03_work_product_deliverable_tasks_1_name']['vname']='LBL_M03_WORK_PRODUCT_DELIVERABLE_TASKS_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_contact_id.php

 // created: 2022-02-03 10:05:11
$dictionary['Task']['fields']['contact_id']['name']='contact_id';
$dictionary['Task']['fields']['contact_id']['type']='id';
$dictionary['Task']['fields']['contact_id']['group']='contact_name';
$dictionary['Task']['fields']['contact_id']['reportable']=false;
$dictionary['Task']['fields']['contact_id']['vname']='LBL_CONTACT_ID';
$dictionary['Task']['fields']['contact_id']['audited']=false;
$dictionary['Task']['fields']['contact_id']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_contact_name.php

 // created: 2022-02-03 10:05:11
$dictionary['Task']['fields']['contact_name']['len']=255;
$dictionary['Task']['fields']['contact_name']['audited']=false;
$dictionary['Task']['fields']['contact_name']['massupdate']=true;
$dictionary['Task']['fields']['contact_name']['hidemassupdate']=false;
$dictionary['Task']['fields']['contact_name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['contact_name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['contact_name']['merge_filter']='disabled';
$dictionary['Task']['fields']['contact_name']['unified_search']=false;
$dictionary['Task']['fields']['contact_name']['calculated']=false;
$dictionary['Task']['fields']['contact_name']['dependency']='not(equal($task_type_c,"Schedule Matrix Review"))';
$dictionary['Task']['fields']['contact_name']['related_fields']=array (
  0 => 'contact_id',
);

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_m01_sales_tasks_1m01_sales_ida.php

 // created: 2022-02-03 10:05:55
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['name']='m01_sales_tasks_1m01_sales_ida';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['type']='id';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['source']='non-db';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['vname']='LBL_M01_SALES_TASKS_1_FROM_TASKS_TITLE_ID';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['id_name']='m01_sales_tasks_1m01_sales_ida';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['link']='m01_sales_tasks_1';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['table']='m01_sales';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['module']='M01_Sales';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['rname']='id';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['reportable']=false;
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['side']='right';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['massupdate']=false;
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['duplicate_merge']='disabled';
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['hideacl']=true;
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['audited']=false;
$dictionary['Task']['fields']['m01_sales_tasks_1m01_sales_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_m01_sales_tasks_1_name.php

 // created: 2022-02-03 10:05:56
$dictionary['Task']['fields']['m01_sales_tasks_1_name']['audited']=false;
$dictionary['Task']['fields']['m01_sales_tasks_1_name']['massupdate']=true;
$dictionary['Task']['fields']['m01_sales_tasks_1_name']['hidemassupdate']=false;
$dictionary['Task']['fields']['m01_sales_tasks_1_name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['m01_sales_tasks_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['m01_sales_tasks_1_name']['merge_filter']='disabled';
$dictionary['Task']['fields']['m01_sales_tasks_1_name']['reportable']=false;
$dictionary['Task']['fields']['m01_sales_tasks_1_name']['unified_search']=false;
$dictionary['Task']['fields']['m01_sales_tasks_1_name']['calculated']=false;
$dictionary['Task']['fields']['m01_sales_tasks_1_name']['dependency']='not(equal($task_type_c,"Schedule Matrix Review"))';
$dictionary['Task']['fields']['m01_sales_tasks_1_name']['vname']='LBL_M01_SALES_TASKS_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/bid_batch_id_tasks_1_Tasks.php

// created: 2022-09-22 05:31:46
$dictionary["Task"]["fields"]["bid_batch_id_tasks_1"] = array (
  'name' => 'bid_batch_id_tasks_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_tasks_1',
  'source' => 'non-db',
  'module' => 'BID_Batch_ID',
  'bean_name' => 'BID_Batch_ID',
  'side' => 'right',
  'vname' => 'LBL_BID_BATCH_ID_TASKS_1_FROM_TASKS_TITLE',
  'id_name' => 'bid_batch_id_tasks_1bid_batch_id_ida',
  'link-type' => 'one',
);
$dictionary["Task"]["fields"]["bid_batch_id_tasks_1_name"] = array (
  'name' => 'bid_batch_id_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_TASKS_1_FROM_BID_BATCH_ID_TITLE',
  'save' => true,
  'id_name' => 'bid_batch_id_tasks_1bid_batch_id_ida',
  'link' => 'bid_batch_id_tasks_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["bid_batch_id_tasks_1bid_batch_id_ida"] = array (
  'name' => 'bid_batch_id_tasks_1bid_batch_id_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_TASKS_1_FROM_TASKS_TITLE_ID',
  'id_name' => 'bid_batch_id_tasks_1bid_batch_id_ida',
  'link' => 'bid_batch_id_tasks_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
