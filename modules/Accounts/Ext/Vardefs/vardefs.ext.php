<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_m01_sales_1_Accounts.php

// created: 2016-02-15 18:01:52
$dictionary["Account"]["fields"]["accounts_m01_sales_1"] = array (
  'name' => 'accounts_m01_sales_1',
  'type' => 'link',
  'relationship' => 'accounts_m01_sales_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'vname' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_m01_sales_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:54
$dictionary['Account']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_ac01_acquired_companies_1_Accounts.php

// created: 2018-02-14 15:35:52
$dictionary["Account"]["fields"]["accounts_ac01_acquired_companies_1"] = array (
  'name' => 'accounts_ac01_acquired_companies_1',
  'type' => 'link',
  'relationship' => 'accounts_ac01_acquired_companies_1',
  'source' => 'non-db',
  'module' => 'AC01_Acquired_Companies',
  'bean_name' => 'AC01_Acquired_Companies',
  'vname' => 'LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_ac01_acquired_companies_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_ta_tradeshow_activities_1_Accounts.php

// created: 2018-04-23 17:12:12
$dictionary["Account"]["fields"]["accounts_ta_tradeshow_activities_1"] = array (
  'name' => 'accounts_ta_tradeshow_activities_1',
  'type' => 'link',
  'relationship' => 'accounts_ta_tradeshow_activities_1',
  'source' => 'non-db',
  'module' => 'TA_Tradeshow_Activities',
  'bean_name' => 'TA_Tradeshow_Activities',
  'vname' => 'LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_ta_tradeshow_activities_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_m03_work_product_1_Accounts.php

// created: 2018-12-26 15:29:08
$dictionary["Account"]["fields"]["accounts_m03_work_product_1"] = array (
  'name' => 'accounts_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'accounts_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_m03_work_product_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_cd_company_documents_1_Accounts.php

// created: 2019-02-06 20:01:35
$dictionary["Account"]["fields"]["accounts_cd_company_documents_1"] = array (
  'name' => 'accounts_cd_company_documents_1',
  'type' => 'link',
  'relationship' => 'accounts_cd_company_documents_1',
  'source' => 'non-db',
  'module' => 'CD_Company_Documents',
  'bean_name' => 'CD_Company_Documents',
  'vname' => 'LBL_ACCOUNTS_CD_COMPANY_DOCUMENTS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_cd_company_documents_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_vendor_level_c.php

 // created: 2019-05-18 13:45:58
$dictionary['Account']['fields']['vendor_level_c']['labelValue']='Vendor Level';
$dictionary['Account']['fields']['vendor_level_c']['dependency']='equal($vendor_list_c,true)';
$dictionary['Account']['fields']['vendor_level_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_category_c.php

 // created: 2019-02-26 20:59:03
$dictionary['Account']['fields']['category_c']['labelValue']='Category';
$dictionary['Account']['fields']['category_c']['dependency']='';
$dictionary['Account']['fields']['category_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_company_notes_c.php

 // created: 2019-02-20 18:24:55
$dictionary['Account']['fields']['company_notes_c']['labelValue']='Notes';
$dictionary['Account']['fields']['company_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['company_notes_c']['enforced']='';
$dictionary['Account']['fields']['company_notes_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_referred_by_c.php

 // created: 2019-02-20 16:58:40
$dictionary['Account']['fields']['referred_by_c']['labelValue']='Referred By';
$dictionary['Account']['fields']['referred_by_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['referred_by_c']['enforced']='';
$dictionary['Account']['fields']['referred_by_c']['dependency']='equal($connection_source_c,"Referral")';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_tradeshow_name_c.php

 // created: 2019-02-20 16:59:23
$dictionary['Account']['fields']['tradeshow_name_c']['labelValue']='Trade Show Name';
$dictionary['Account']['fields']['tradeshow_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['tradeshow_name_c']['enforced']='false';
$dictionary['Account']['fields']['tradeshow_name_c']['dependency']='equal($connection_source_c,"Trade Show")';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_website.php

 // created: 2019-02-20 15:23:03
$dictionary['Account']['fields']['website']['len']='255';
$dictionary['Account']['fields']['website']['audited']=true;
$dictionary['Account']['fields']['website']['massupdate']=false;
$dictionary['Account']['fields']['website']['comments']='URL of website for the company';
$dictionary['Account']['fields']['website']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['website']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['website']['merge_filter']='disabled';
$dictionary['Account']['fields']['website']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['website']['calculated']=false;
$dictionary['Account']['fields']['website']['gen']='';
$dictionary['Account']['fields']['website']['link_target']='_blank';
$dictionary['Account']['fields']['website']['importable']='required';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_iso_certified_c.php

 // created: 2019-05-21 13:07:25
$dictionary['Account']['fields']['iso_certified_c']['labelValue']='ISO Certified';
$dictionary['Account']['fields']['iso_certified_c']['formula']='equal($vendor_list_c,true)';
$dictionary['Account']['fields']['iso_certified_c']['enforced']='false';
$dictionary['Account']['fields']['iso_certified_c']['dependency']='equal($vendor_list_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_approval_interval_days_c.php

 // created: 2019-05-21 13:13:41
$dictionary['Account']['fields']['approval_interval_days_c']['labelValue']='Approval Interval (days)';
$dictionary['Account']['fields']['approval_interval_days_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['approval_interval_days_c']['enforced']='';
$dictionary['Account']['fields']['approval_interval_days_c']['dependency']='equal($vendor_list_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_client_code_c.php

 // created: 2019-05-23 15:52:19
$dictionary['Account']['fields']['client_code_c']['labelValue']='Company Code';
$dictionary['Account']['fields']['client_code_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['client_code_c']['enforced']='';
$dictionary['Account']['fields']['client_code_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_street.php

 // created: 2019-06-26 19:38:04
$dictionary['Account']['fields']['billing_address_street']['audited']=false;
$dictionary['Account']['fields']['billing_address_street']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_street']['comments']='The street address used for billing address';
$dictionary['Account']['fields']['billing_address_street']['importable']='false';
$dictionary['Account']['fields']['billing_address_street']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['billing_address_street']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['billing_address_street']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.35',
  'searchable' => true,
);
$dictionary['Account']['fields']['billing_address_street']['calculated']=false;
$dictionary['Account']['fields']['billing_address_street']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor"))';
$dictionary['Account']['fields']['billing_address_street']['rows']='4';
$dictionary['Account']['fields']['billing_address_street']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_city.php

 // created: 2019-06-26 19:38:39
$dictionary['Account']['fields']['billing_address_city']['audited']=false;
$dictionary['Account']['fields']['billing_address_city']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_city']['comments']='The city used for billing address';
$dictionary['Account']['fields']['billing_address_city']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_city']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_city']['calculated']=false;
$dictionary['Account']['fields']['billing_address_city']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor"))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_postalcode.php

 // created: 2019-06-26 19:39:31
$dictionary['Account']['fields']['billing_address_postalcode']['audited']=false;
$dictionary['Account']['fields']['billing_address_postalcode']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_postalcode']['comments']='The postal code used for billing address';
$dictionary['Account']['fields']['billing_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_postalcode']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_postalcode']['calculated']=false;
$dictionary['Account']['fields']['billing_address_postalcode']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor"))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_shipping_address_street.php

 // created: 2019-06-26 19:41:11
$dictionary['Account']['fields']['shipping_address_street']['audited']=false;
$dictionary['Account']['fields']['shipping_address_street']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_street']['comments']='The street address used for for shipping purposes';
$dictionary['Account']['fields']['shipping_address_street']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['shipping_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['shipping_address_street']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.34',
  'searchable' => true,
);
$dictionary['Account']['fields']['shipping_address_street']['calculated']=false;
$dictionary['Account']['fields']['shipping_address_street']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor"))';
$dictionary['Account']['fields']['shipping_address_street']['rows']='4';
$dictionary['Account']['fields']['shipping_address_street']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_shipping_address_city.php

 // created: 2019-06-26 19:41:33
$dictionary['Account']['fields']['shipping_address_city']['len']='100';
$dictionary['Account']['fields']['shipping_address_city']['audited']=false;
$dictionary['Account']['fields']['shipping_address_city']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_city']['comments']='The city used for the shipping address';
$dictionary['Account']['fields']['shipping_address_city']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['shipping_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['shipping_address_city']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_city']['calculated']=false;
$dictionary['Account']['fields']['shipping_address_city']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor"))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_shipping_address_state.php

 // created: 2019-06-26 19:42:16
$dictionary['Account']['fields']['shipping_address_state']['len']='100';
$dictionary['Account']['fields']['shipping_address_state']['audited']=false;
$dictionary['Account']['fields']['shipping_address_state']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_state']['comments']='The state used for the shipping address';
$dictionary['Account']['fields']['shipping_address_state']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['shipping_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['shipping_address_state']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_state']['calculated']=false;
$dictionary['Account']['fields']['shipping_address_state']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor"))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_shipping_address_postalcode.php

 // created: 2019-06-26 19:42:39
$dictionary['Account']['fields']['shipping_address_postalcode']['len']='20';
$dictionary['Account']['fields']['shipping_address_postalcode']['audited']=false;
$dictionary['Account']['fields']['shipping_address_postalcode']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_postalcode']['comments']='The zip code used for the shipping address';
$dictionary['Account']['fields']['shipping_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['shipping_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['shipping_address_postalcode']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_postalcode']['calculated']=false;
$dictionary['Account']['fields']['shipping_address_postalcode']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor"))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_shipping_address_country.php

 // created: 2019-06-26 19:43:11
$dictionary['Account']['fields']['shipping_address_country']['len']='255';
$dictionary['Account']['fields']['shipping_address_country']['audited']=false;
$dictionary['Account']['fields']['shipping_address_country']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_country']['comments']='The country used for the shipping address';
$dictionary['Account']['fields']['shipping_address_country']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['shipping_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['shipping_address_country']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_country']['calculated']=false;
$dictionary['Account']['fields']['shipping_address_country']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor"))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_ca_company_address_1_Accounts.php

// created: 2019-07-03 12:14:01
$dictionary["Account"]["fields"]["accounts_ca_company_address_1"] = array (
  'name' => 'accounts_ca_company_address_1',
  'type' => 'link',
  'relationship' => 'accounts_ca_company_address_1',
  'source' => 'non-db',
  'module' => 'CA_Company_Address',
  'bean_name' => 'CA_Company_Address',
  'vname' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_ca_company_address_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_current_vendor_status_c.php

 // created: 2019-07-09 12:29:47
$dictionary['Account']['fields']['current_vendor_status_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['current_vendor_status_c']['labelValue']='Vendor Status';
$dictionary['Account']['fields']['current_vendor_status_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['current_vendor_status_c']['calculated']='true';
$dictionary['Account']['fields']['current_vendor_status_c']['formula']='related($accounts_cd_company_documents_1,"vendor_status_c")';
$dictionary['Account']['fields']['current_vendor_status_c']['enforced']='true';
$dictionary['Account']['fields']['current_vendor_status_c']['dependency']='equal($vendor_list_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_method_of_qualification_c.php

 // created: 2019-07-09 12:31:13
$dictionary['Account']['fields']['method_of_qualification_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['method_of_qualification_c']['labelValue']='Method of Qualification';
$dictionary['Account']['fields']['method_of_qualification_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['method_of_qualification_c']['calculated']='true';
$dictionary['Account']['fields']['method_of_qualification_c']['formula']='related($accounts_cd_company_documents_1,"method_of_qualification_c")';
$dictionary['Account']['fields']['method_of_qualification_c']['enforced']='true';
$dictionary['Account']['fields']['method_of_qualification_c']['dependency']='equal($vendor_list_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_cn_company_name_1_Accounts.php

// created: 2019-09-26 14:13:30
$dictionary["Account"]["fields"]["accounts_cn_company_name_1"] = array (
  'name' => 'accounts_cn_company_name_1',
  'type' => 'link',
  'relationship' => 'accounts_cn_company_name_1',
  'source' => 'non-db',
  'module' => 'CN_Company_Name',
  'bean_name' => 'CN_Company_Name',
  'vname' => 'LBL_ACCOUNTS_CN_COMPANY_NAME_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_cn_company_name_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/approval_date_c.php

 
$dictionary['Account']['fields']['approval_date_c']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_approval_date_c.php

 // created: 2019-12-09 05:32:31
$dictionary['Account']['fields']['approval_date_c']['labelValue']='Approval Date';
$dictionary['Account']['fields']['approval_date_c']['enforced']='';
$dictionary['Account']['fields']['approval_date_c']['dependency']='equal($vendor_list_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_next_approval_due_date_c.php

 // created: 2019-12-09 05:35:35
$dictionary['Account']['fields']['next_approval_due_date_c']['labelValue']='Next Approval Due Date';
$dictionary['Account']['fields']['next_approval_due_date_c']['formula']='addDays($approval_date_c,$approval_interval_days_c)';
$dictionary['Account']['fields']['next_approval_due_date_c']['enforced']='false';
$dictionary['Account']['fields']['next_approval_due_date_c']['dependency']='and(equal($vendor_list_c,true),greaterThan(strlen(toString($next_approval_due_date_c)),0))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_payment_terms_2_c.php

 // created: 2020-07-31 15:10:12
$dictionary['Account']['fields']['payment_terms_2_c']['labelValue']='Payment Terms';
$dictionary['Account']['fields']['payment_terms_2_c']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor","Sponsor and Manufacturer","SponsorVendor Manufacturer"))';
$dictionary['Account']['fields']['payment_terms_2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_country.php

 // created: 2020-07-31 15:07:47
$dictionary['Account']['fields']['billing_address_country']['len']='255';
$dictionary['Account']['fields']['billing_address_country']['audited']=true;
$dictionary['Account']['fields']['billing_address_country']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_country']['comments']='The country used for the billing address';
$dictionary['Account']['fields']['billing_address_country']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_country']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_country']['calculated']=false;
$dictionary['Account']['fields']['billing_address_country']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor","Sponsor and Manufacturer","SponsorVendor Manufacturer"))';
$dictionary['Account']['fields']['billing_address_country']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_state.php

 // created: 2020-07-31 15:08:35
$dictionary['Account']['fields']['billing_address_state']['audited']=true;
$dictionary['Account']['fields']['billing_address_state']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_state']['comments']='The state used for billing address';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_state']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_state']['calculated']=false;
$dictionary['Account']['fields']['billing_address_state']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor","Sponsor and Manufacturer","SponsorVendor Manufacturer"))';
$dictionary['Account']['fields']['billing_address_state']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_biocomp_pricelist_c.php

 // created: 2020-07-31 15:12:28
$dictionary['Account']['fields']['biocomp_pricelist_c']['labelValue']='Biocomp Pricelist';
$dictionary['Account']['fields']['biocomp_pricelist_c']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor","Sponsor and Manufacturer","SponsorVendor Manufacturer"))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_company_status_c.php

 // created: 2020-07-31 15:09:30
$dictionary['Account']['fields']['company_status_c']['labelValue']='Account Status';
$dictionary['Account']['fields']['company_status_c']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor","Sponsor and Manufacturer","SponsorVendor Manufacturer"))';
$dictionary['Account']['fields']['company_status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_vendor_list_c.php

 // created: 2020-07-31 14:42:41
$dictionary['Account']['fields']['vendor_list_c']['labelValue']='Approved Vendor List';
$dictionary['Account']['fields']['vendor_list_c']['enforced']='';
$dictionary['Account']['fields']['vendor_list_c']['dependency']='isInList($category_c,createList("Sponsor and Vendor","Vendor","Manufacturer","Sponsor and Manufacturer","Vendor and Manufacturer","SponsorVendor Manufacturer"))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_connection_source_c.php

 // created: 2020-07-31 15:11:54
$dictionary['Account']['fields']['connection_source_c']['labelValue']='Connection Source';
$dictionary['Account']['fields']['connection_source_c']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor","Sponsor and Manufacturer","SponsorVendor Manufacturer"))';
$dictionary['Account']['fields']['connection_source_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_vendor_type_c.php

 // created: 2020-07-31 14:43:10
$dictionary['Account']['fields']['vendor_type_c']['labelValue']='Vendor Type';
$dictionary['Account']['fields']['vendor_type_c']['dependency']='isInList($category_c,createList("Sponsor and Vendor","Vendor","Manufacturer","Sponsor and Manufacturer","Vendor and Manufacturer","SponsorVendor Manufacturer"))';
$dictionary['Account']['fields']['vendor_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_view_all_company_projects_c.php

 // created: 2020-07-31 15:11:22
$dictionary['Account']['fields']['view_all_company_projects_c']['labelValue']='View All Company Projects in Portal?';
$dictionary['Account']['fields']['view_all_company_projects_c']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor","Sponsor and Manufacturer","SponsorVendor Manufacturer"))';
$dictionary['Account']['fields']['view_all_company_projects_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_account_number_c.php

 // created: 2020-07-31 14:44:58
$dictionary['Account']['fields']['account_number_c']['labelValue']='Account #';
$dictionary['Account']['fields']['account_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['account_number_c']['enforced']='';
$dictionary['Account']['fields']['account_number_c']['dependency']='isInList($category_c,createList("Sponsor and Vendor","Vendor","Manufacturer","Sponsor and Manufacturer","Vendor and Manufacturer","SponsorVendor Manufacturer"))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_account_representative_c.php

 // created: 2020-07-31 15:13:04
$dictionary['Account']['fields']['account_representative_c']['labelValue']='Account Representative';
$dictionary['Account']['fields']['account_representative_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['account_representative_c']['enforced']='';
$dictionary['Account']['fields']['account_representative_c']['dependency']='isInList($category_c,createList("Sponsor","Sponsor and Vendor","Sponsor and Manufacturer","SponsorVendor Manufacturer"))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_equip_equipment_1_Accounts.php

// created: 2020-11-03 06:37:20
$dictionary["Account"]["fields"]["accounts_equip_equipment_1"] = array (
  'name' => 'accounts_equip_equipment_1',
  'type' => 'link',
  'relationship' => 'accounts_equip_equipment_1',
  'source' => 'non-db',
  'module' => 'Equip_Equipment',
  'bean_name' => 'Equip_Equipment',
  'vname' => 'LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'id_name' => 'accounts_equip_equipment_1equip_equipment_idb',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_an_account_number_1_Accounts.php

// created: 2021-02-25 08:41:49
$dictionary["Account"]["fields"]["accounts_an_account_number_1"] = array (
  'name' => 'accounts_an_account_number_1',
  'type' => 'link',
  'relationship' => 'accounts_an_account_number_1',
  'source' => 'non-db',
  'module' => 'AN_Account_Number',
  'bean_name' => 'AN_Account_Number',
  'vname' => 'LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_an_account_number_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_nsc_namsa_sub_companies_1_Accounts.php

// created: 2021-12-07 12:21:22
$dictionary["Account"]["fields"]["accounts_nsc_namsa_sub_companies_1"] = array (
  'name' => 'accounts_nsc_namsa_sub_companies_1',
  'type' => 'link',
  'relationship' => 'accounts_nsc_namsa_sub_companies_1',
  'source' => 'non-db',
  'module' => 'NSC_NAMSA_Sub_Companies',
  'bean_name' => 'NSC_NAMSA_Sub_Companies',
  'vname' => 'LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_nsc_namsa_sub_companies_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
