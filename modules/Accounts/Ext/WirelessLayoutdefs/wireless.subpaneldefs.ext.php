<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_m01_sales_1_Accounts.php

 // created: 2016-02-15 18:01:52
$layout_defs["Accounts"]["subpanel_setup"]['accounts_m01_sales_1'] = array (
  'order' => 100,
  'module' => 'M01_Sales',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE',
  'get_subpanel_data' => 'accounts_m01_sales_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_ac01_acquired_companies_1_Accounts.php

 // created: 2018-02-14 15:35:52
$layout_defs["Accounts"]["subpanel_setup"]['accounts_ac01_acquired_companies_1'] = array (
  'order' => 100,
  'module' => 'AC01_Acquired_Companies',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_AC01_ACQUIRED_COMPANIES_TITLE',
  'get_subpanel_data' => 'accounts_ac01_acquired_companies_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_ta_tradeshow_activities_1_Accounts.php

 // created: 2018-04-23 17:12:13
$layout_defs["Accounts"]["subpanel_setup"]['accounts_ta_tradeshow_activities_1'] = array (
  'order' => 100,
  'module' => 'TA_Tradeshow_Activities',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'get_subpanel_data' => 'accounts_ta_tradeshow_activities_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_m03_work_product_1_Accounts.php

 // created: 2018-12-26 15:29:08
$layout_defs["Accounts"]["subpanel_setup"]['accounts_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'accounts_m03_work_product_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_cd_company_documents_1_Accounts.php

 // created: 2019-02-06 20:01:35
$layout_defs["Accounts"]["subpanel_setup"]['accounts_cd_company_documents_1'] = array (
  'order' => 100,
  'module' => 'CD_Company_Documents',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_CD_COMPANY_DOCUMENTS_1_FROM_CD_COMPANY_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'accounts_cd_company_documents_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_ca_company_address_1_Accounts.php

 // created: 2019-07-03 12:14:01
$layout_defs["Accounts"]["subpanel_setup"]['accounts_ca_company_address_1'] = array (
  'order' => 100,
  'module' => 'CA_Company_Address',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_CA_COMPANY_ADDRESS_TITLE',
  'get_subpanel_data' => 'accounts_ca_company_address_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_cn_company_name_1_Accounts.php

 // created: 2019-09-26 14:13:30
$layout_defs["Accounts"]["subpanel_setup"]['accounts_cn_company_name_1'] = array (
  'order' => 100,
  'module' => 'CN_Company_Name',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_CN_COMPANY_NAME_1_FROM_CN_COMPANY_NAME_TITLE',
  'get_subpanel_data' => 'accounts_cn_company_name_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_equip_equipment_1_Accounts.php

 // created: 2020-11-03 06:37:20
$layout_defs["Accounts"]["subpanel_setup"]['accounts_equip_equipment_1'] = array (
  'order' => 100,
  'module' => 'Equip_Equipment',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'get_subpanel_data' => 'accounts_equip_equipment_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_an_account_number_1_Accounts.php

 // created: 2021-02-25 08:41:49
$layout_defs["Accounts"]["subpanel_setup"]['accounts_an_account_number_1'] = array (
  'order' => 100,
  'module' => 'AN_Account_Number',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_AN_ACCOUNT_NUMBER_TITLE',
  'get_subpanel_data' => 'accounts_an_account_number_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_nsc_namsa_sub_companies_1_Accounts.php

 // created: 2021-12-07 12:21:22
$layout_defs["Accounts"]["subpanel_setup"]['accounts_nsc_namsa_sub_companies_1'] = array (
  'order' => 100,
  'module' => 'NSC_NAMSA_Sub_Companies',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE',
  'get_subpanel_data' => 'accounts_nsc_namsa_sub_companies_1',
);

?>
