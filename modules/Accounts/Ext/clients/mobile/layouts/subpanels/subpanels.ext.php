<?php
// WARNING: The contents of this file are auto-generated.


// created: 2018-02-14 15:35:52
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_AC01_ACQUIRED_COMPANIES_TITLE',
  'context' => 
  array (
    'link' => 'accounts_ac01_acquired_companies_1',
  ),
);

// created: 2021-02-25 08:41:49
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_AN_ACCOUNT_NUMBER_TITLE',
  'context' => 
  array (
    'link' => 'accounts_an_account_number_1',
  ),
);

// created: 2019-07-03 12:14:01
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_CA_COMPANY_ADDRESS_TITLE',
  'context' => 
  array (
    'link' => 'accounts_ca_company_address_1',
  ),
);

// created: 2019-02-06 20:01:35
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_CD_COMPANY_DOCUMENTS_1_FROM_CD_COMPANY_DOCUMENTS_TITLE',
  'context' => 
  array (
    'link' => 'accounts_cd_company_documents_1',
  ),
);

// created: 2019-09-26 14:13:30
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_CN_COMPANY_NAME_1_FROM_CN_COMPANY_NAME_TITLE',
  'context' => 
  array (
    'link' => 'accounts_cn_company_name_1',
  ),
);

// created: 2020-11-03 06:37:20
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'context' => 
  array (
    'link' => 'accounts_equip_equipment_1',
  ),
);

// created: 2016-02-15 18:01:52
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE',
  'context' => 
  array (
    'link' => 'accounts_m01_sales_1',
  ),
);

// created: 2018-12-26 15:29:08
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'context' => 
  array (
    'link' => 'accounts_m03_work_product_1',
  ),
);

// created: 2021-12-07 12:21:22
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE',
  'context' => 
  array (
    'link' => 'accounts_nsc_namsa_sub_companies_1',
  ),
);

// created: 2018-04-23 17:12:13
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE',
  'context' => 
  array (
    'link' => 'accounts_ta_tradeshow_activities_1',
  ),
);