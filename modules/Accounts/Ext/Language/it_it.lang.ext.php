<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M01_SALES_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_m01_quote_document_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M01_QUOTE_DOCUMENT_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote Documents';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_m01_sales_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M01_SALES_2_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M01_SALES_2_FROM_M01_SALES_TITLE'] = 'Sales Activities';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_m01_sales_activity_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M01_SALES_ACTIVITY_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M01_SALES_ACTIVITY_1_FROM_M01_SALES_ACTIVITY_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_DOCUMENTS_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Documents';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_ac01_acquired_companies_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_AC01_ACQUIRED_COMPANIES_TITLE'] = 'Acquired Companies';
$mod_strings['LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_ACCOUNTS_TITLE'] = 'Acquired Companies';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_ta_tradeshow_activities_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE'] = 'Tradeshow Activities';
$mod_strings['LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_ACCOUNTS_TITLE'] = 'Tradeshow Activities';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE'] = 'Work Products';
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_cd_company_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_CD_COMPANY_DOCUMENTS_1_FROM_CD_COMPANY_DOCUMENTS_TITLE'] = 'Company Documents';
$mod_strings['LBL_ACCOUNTS_CD_COMPANY_DOCUMENTS_1_FROM_ACCOUNTS_TITLE'] = 'Company Documents';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customequip_equipment_accounts_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EQUIP_EQUIPMENT_ACCOUNTS_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment ';
$mod_strings['LBL_EQUIP_EQUIPMENT_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Equipment ';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_ca_company_address_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_CA_COMPANY_ADDRESS_TITLE'] = 'Company Addresses';
$mod_strings['LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_ACCOUNTS_TITLE'] = 'Company Addresses';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_cn_company_name_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_CN_COMPANY_NAME_1_FROM_CN_COMPANY_NAME_TITLE'] = 'Company Names';
$mod_strings['LBL_ACCOUNTS_CN_COMPANY_NAME_1_FROM_ACCOUNTS_TITLE'] = 'Company Names';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_equip_equipment_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment &amp; Facilities';
$mod_strings['LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_ACCOUNTS_TITLE'] = 'Equipment &amp; Facilities';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_an_account_number_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_AN_ACCOUNT_NUMBER_TITLE'] = 'Account Numbers';
$mod_strings['LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_ACCOUNTS_TITLE'] = 'Account Numbers';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_nsc_namsa_sub_companies_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_2_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE'] = 'NAMSA Subcontracting Companies';
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_2_FROM_ACCOUNTS_TITLE'] = 'NAMSA Subcontracting Companies';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_nsc_namsa_sub_companies_3.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_3_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE'] = 'NAMSA Subcontracting Companies';
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_3_FROM_ACCOUNTS_TITLE'] = 'NAMSA Subcontracting Companies';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_nsc_namsa_sub_companies_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE'] = 'NAMSA Subcontracting Companies';
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_ACCOUNTS_TITLE'] = 'NAMSA Subcontracting Companies';

?>
