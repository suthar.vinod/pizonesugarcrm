<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us..php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_PROJECTS_SUBPANEL_TITLE'] = 'Projects';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Company';
$mod_strings['LNK_CREATE'] = 'Create Company';
$mod_strings['LBL_MODULE_NAME'] = 'Companies';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Company';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Company';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Companies';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Company Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Companies';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Company record you are about to create might be a duplicate of an Company record that already exists. Company records containing similar names are listed below.Click Save to continue creating this new Company, or click Cancel to return to the module without creating the Company.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Company';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Company List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Company Search';
$mod_strings['LBL_MEMBER_OF'] = 'Parent Company';
$mod_strings['LBL_MEMBER_ORG_SUBPANEL_TITLE'] = 'Member Organizations';
$mod_strings['LBL_OPPORTUNITIES_SUBPANEL_TITLE'] = 'Opportunities';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Companies';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Company from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Company.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Company Name:';
$mod_strings['LBL_ACCOUNT'] = 'Company:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Companies';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Companies';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Company';
$mod_strings['LBL_MODULE_TITLE'] = 'Companies: Home';
$mod_strings['LBL_MODULE_ID'] = 'Companies';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Company ID';
$mod_strings['MSG_DUPLICATE'] = 'The Company record you are about to create might be a duplicate of an Company record that already exists. Company records containing similar names are listed below.Click Create Company to continue creating this new Company, or select an existing Company listed below.';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Company Type';
$mod_strings['LBL_SPECIALTY'] = 'Company Type';
$mod_strings['LBL_COMPANY_TYPE'] = 'Company Category';
$mod_strings['LBL_COMPANY_STATUS'] = 'Sponsor Status';
$mod_strings['LBL_ALTERED_BID_TERMS'] = 'Altered Bid Terms';
$mod_strings['LBL_CLIENT_CODE'] = 'Company Code';
$mod_strings['LBL_PHONE_OFFICE'] = 'Main Phone';
$mod_strings['LBL_FAX'] = 'Main Fax';
$mod_strings['LBL_PAYMENT_TERMS'] = 'Payment Terms';
$mod_strings['LBL_WEBSITE'] = 'Website:';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'APS Activities';
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_ACCOUNTS_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote &amp; Study Documents';
$mod_strings['LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';
$mod_strings['LBL_SIGNED_CDA'] = 'Signed CDA';
$mod_strings['LBL_COMPANY_NOTES'] = 'Notes';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'State';
$mod_strings['LBL_CDA_DATE'] = 'CDA Signature Date';
$mod_strings['LBL_CDA_EXPIRATION_DATE'] = 'CDA Expiration Date';
$mod_strings['LBL_SIGNED_MSA'] = 'Signed MSA';
$mod_strings['LBL_SIGNED_QUALITY_AGREEMENT'] = 'Signed Quality Agreement';
$mod_strings['LBL_MSA_EXPIRATION_DATE'] = 'MSA Expiration Date';
$mod_strings['LBL_QUALITY_AGREEMENT_EXPIRATION'] = 'Quality Agreement Expiration Date';
$mod_strings['LBL_BIOCOMP_PRICELIST'] = 'Biocomp Pricelist';
$mod_strings['LBL_CONNECTION_SOURCE'] = 'Connection Source';
$mod_strings['LBL_TRADESHOW_NAME'] = 'Trade Show Name';
$mod_strings['LBL_REFERRED_BY'] = 'Referred By';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Sponsor Info';
$mod_strings['LBL_RECORD_BODY'] = 'General Info';
$mod_strings['LBL_SCHEDULED_ONSITE_VISIT'] = 'Scheduled Onsite Visit';
$mod_strings['LBL_SPONSOR_ATTENDEES'] = 'Sponsor Attendees';
$mod_strings['LBL_DESCRIPTION_OF_VISIT'] = 'Description of Visit';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Vendor Info';
$mod_strings['LBL_VIEW_ALL_COMPANY_PROJECTS'] = 'View All Company Projects in Portal?';
$mod_strings['LBL_ACCOUNT_REPRESENTATIVE'] = 'Account Representative';
$mod_strings['LBL_CATEGORY'] = 'Category';
$mod_strings['LBL_PAYMENT_TERMS_2'] = 'Payment Terms';
$mod_strings['LBL_VENDOR_TYPE'] = 'Vendor Type';
$mod_strings['LBL_ISO_CERTIFIED'] = 'ISO Certified';
$mod_strings['LBL_VENDOR_LEVEL'] = 'Vendor Level';
$mod_strings['LBL_METHOD_OF_QUALIFICATION'] = 'Method of Qualification';
$mod_strings['LBL_CURRENT_VENDOR_STATUS'] = 'Vendor Status';
$mod_strings['LBL_APPROVAL_DATE'] = 'Approval Date';
$mod_strings['LBL_APPROVAL_INTERVAL_DAYS'] = 'Approval Interval (days)';
$mod_strings['LBL_NEXT_APPROVAL_DUE_DATE'] = 'Next Approval Due Date';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Notes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M01_SALES_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_m01_quote_document_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M01_QUOTE_DOCUMENT_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote Documents';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_m01_sales_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M01_SALES_2_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M01_SALES_2_FROM_M01_SALES_TITLE'] = 'Sales Activities';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_m01_sales_activity_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M01_SALES_ACTIVITY_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M01_SALES_ACTIVITY_1_FROM_M01_SALES_ACTIVITY_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_DOCUMENTS_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Documents';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_ac01_acquired_companies_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_AC01_ACQUIRED_COMPANIES_TITLE'] = 'Acquired Companies';
$mod_strings['LBL_ACCOUNTS_AC01_ACQUIRED_COMPANIES_1_FROM_ACCOUNTS_TITLE'] = 'Acquired Companies';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_ta_tradeshow_activities_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_TA_TRADESHOW_ACTIVITIES_TITLE'] = 'Tradeshow Activities';
$mod_strings['LBL_ACCOUNTS_TA_TRADESHOW_ACTIVITIES_1_FROM_ACCOUNTS_TITLE'] = 'Tradeshow Activities';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE'] = 'Work Products';
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_cd_company_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_CD_COMPANY_DOCUMENTS_1_FROM_CD_COMPANY_DOCUMENTS_TITLE'] = 'Company Documents';
$mod_strings['LBL_ACCOUNTS_CD_COMPANY_DOCUMENTS_1_FROM_ACCOUNTS_TITLE'] = 'Company Documents';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customequip_equipment_accounts_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EQUIP_EQUIPMENT_ACCOUNTS_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment ';
$mod_strings['LBL_EQUIP_EQUIPMENT_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Equipment ';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_ca_company_address_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_CA_COMPANY_ADDRESS_TITLE'] = 'Company Addresses';
$mod_strings['LBL_ACCOUNTS_CA_COMPANY_ADDRESS_1_FROM_ACCOUNTS_TITLE'] = 'Company Addresses';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_cn_company_name_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_CN_COMPANY_NAME_1_FROM_CN_COMPANY_NAME_TITLE'] = 'Company Names';
$mod_strings['LBL_ACCOUNTS_CN_COMPANY_NAME_1_FROM_ACCOUNTS_TITLE'] = 'Company Names';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_equip_equipment_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment &amp; Facilities';
$mod_strings['LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_ACCOUNTS_TITLE'] = 'Equipment &amp; Facilities';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_an_account_number_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_AN_ACCOUNT_NUMBER_TITLE'] = 'Account Numbers';
$mod_strings['LBL_ACCOUNTS_AN_ACCOUNT_NUMBER_1_FROM_ACCOUNTS_TITLE'] = 'Account Numbers';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_nsc_namsa_sub_companies_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_2_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE'] = 'NAMSA Subcontracting Companies';
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_2_FROM_ACCOUNTS_TITLE'] = 'NAMSA Subcontracting Companies';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_nsc_namsa_sub_companies_3.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_3_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE'] = 'NAMSA Subcontracting Companies';
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_3_FROM_ACCOUNTS_TITLE'] = 'NAMSA Subcontracting Companies';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_PROJECTS_SUBPANEL_TITLE'] = 'Projects';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Company';
$mod_strings['LNK_CREATE'] = 'Create Company';
$mod_strings['LBL_MODULE_NAME'] = 'Companies';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Company';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Company';
$mod_strings['LNK_ACCOUNT_LIST'] = 'View Companies';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'View Company Reports';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Import Companies';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'The Company record you are about to create might be a duplicate of an Company record that already exists. Company records containing similar names are listed below.Click Save to continue creating this new Company, or click Cancel to return to the module without creating the Company.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Save Company';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Company List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Company Search';
$mod_strings['LBL_MEMBER_OF'] = 'Parent Company';
$mod_strings['LBL_MEMBER_ORG_SUBPANEL_TITLE'] = 'Member Organizations';
$mod_strings['LBL_OPPORTUNITIES_SUBPANEL_TITLE'] = 'Opportunities';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Companies';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Are you sure you want to remove this Company from the project?';
$mod_strings['ERR_DELETE_RECORD'] = 'You must specify a record number in order to delete the Company.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Company Name:';
$mod_strings['LBL_ACCOUNT'] = 'Company:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Companies';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Companies';
$mod_strings['LBL_DUPLICATE'] = 'Possible Duplicate Company';
$mod_strings['LBL_MODULE_TITLE'] = 'Companies: Home';
$mod_strings['LBL_MODULE_ID'] = 'Companies';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'Parent Company ID';
$mod_strings['MSG_DUPLICATE'] = 'The Company record you are about to create might be a duplicate of an Company record that already exists. Company records containing similar names are listed below.Click Create Company to continue creating this new Company, or select an existing Company listed below.';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Company Type';
$mod_strings['LBL_SPECIALTY'] = 'Company Type';
$mod_strings['LBL_COMPANY_TYPE'] = 'Company Category';
$mod_strings['LBL_COMPANY_STATUS'] = 'Account Status';
$mod_strings['LBL_ALTERED_BID_TERMS'] = 'Altered Bid Terms';
$mod_strings['LBL_CLIENT_CODE'] = 'Client Code';
$mod_strings['LBL_PHONE_OFFICE'] = 'Main Phone';
$mod_strings['LBL_FAX'] = 'Main Fax';
$mod_strings['LBL_PAYMENT_TERMS'] = 'Payment Terms (obsolete)';
$mod_strings['LBL_WEBSITE'] = 'Website:';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'APS Activities';
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_ACCOUNTS_M01_QUOTE_DOCUMENT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote &amp; Study Documents';
$mod_strings['LBL_ACCOUNTS_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales Activities';
$mod_strings['LBL_SIGNED_CDA'] = 'Signed CDA';
$mod_strings['LBL_COMPANY_NOTES'] = 'Company Notes';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Headquarters State';
$mod_strings['LBL_CDA_DATE'] = 'CDA Signature Date';
$mod_strings['LBL_CDA_EXPIRATION_DATE'] = 'CDA Expiration Date';
$mod_strings['LBL_SIGNED_MSA'] = 'Signed MSA';
$mod_strings['LBL_SIGNED_QUALITY_AGREEMENT'] = 'Signed Quality Agreement';
$mod_strings['LBL_MSA_EXPIRATION_DATE'] = 'MSA Expiration Date';
$mod_strings['LBL_QUALITY_AGREEMENT_EXPIRATION'] = 'Quality Agreement Expiration Date';
$mod_strings['LBL_BIOCOMP_PRICELIST'] = 'Biocomp Pricelist';
$mod_strings['LBL_CONNECTION_SOURCE'] = 'Connection Source';
$mod_strings['LBL_TRADESHOW_NAME'] = 'Trade Show Name';
$mod_strings['LBL_REFERRED_BY'] = 'Referred By';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Sponsor Info';
$mod_strings['LBL_RECORD_BODY'] = 'General Info';
$mod_strings['LBL_SCHEDULED_ONSITE_VISIT'] = 'Scheduled Onsite Visit';
$mod_strings['LBL_SPONSOR_ATTENDEES'] = 'Sponsor Attendees';
$mod_strings['LBL_DESCRIPTION_OF_VISIT'] = 'Description of Visit';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Vendor Info';
$mod_strings['LBL_VIEW_ALL_COMPANY_PROJECTS'] = 'View All Company Projects in Portal?';
$mod_strings['LBL_ACCOUNT_REPRESENTATIVE'] = 'Account Representative';
$mod_strings['LBL_VENDOR_LIST'] = 'Approved Vendor List';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'New Panel 4';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'Headquarters Country';
$mod_strings['LBL_ACCOUNTS_LIST_DASHBOARD'] = 'Companies List Dashboard';
$mod_strings['LBL_ACCOUNTS_RECORD_DASHBOARD'] = 'Companies Record Dashboard';
$mod_strings['LBL_ACCOUNTS_MULTI_LINE_DASHBOARD'] = 'Company Details';
$mod_strings['LBL_RENEWALS_CONSOLE_ACCOUNT_NAME_INDUSTRY'] = 'Company Name/Industry';
$mod_strings['LBL_FILTER_ACCOUNTS_REPORTS'] = 'Companies&#039; reports';
$mod_strings['LBL_ACCOUNT_NUMBER'] = 'Account #';
$mod_strings['LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment &amp; Facilities';
$mod_strings['LBL_ACCOUNTS_FOCUS_DRAWER_DASHBOARD'] = 'Companies Focus Drawer';
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE'] = 'NSC_NAMSA_Sub_Companies';
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_ACCOUNTS_TITLE'] = 'NSC_NAMSA_Sub_Companies';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_nsc_namsa_sub_companies_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_NSC_NAMSA_SUB_COMPANIES_TITLE'] = 'NAMSA Subcontracting Companies';
$mod_strings['LBL_ACCOUNTS_NSC_NAMSA_SUB_COMPANIES_1_FROM_ACCOUNTS_TITLE'] = 'NAMSA Subcontracting Companies';

?>
