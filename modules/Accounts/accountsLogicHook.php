<?php

class accountsLogicHook {

    function generateClicentId($bean, $event, $arguments) {
        global $db;

        $randomString = $this->genrate_code();

        //Get the most recently related/created Company Document record
        $queryForGetIdOfCompanyDocument = "SELECT cd_company_documents.id, cd_company_documents_cstm.method_of_qualification_c, cd_company_documents_cstm.vendor_status_c, cd_company_documents_cstm.date_of_qualification_c FROM accounts_cstm LEFT JOIN accounts_cd_company_documents_1_c "
                . "ON accounts_cstm.id_c = accounts_cd_company_documents_1_c.accounts_cd_company_documents_1accounts_ida "
                . "LEFT JOIN cd_company_documents ON accounts_cd_company_documents_1_c.accounts_cd_company_documents_1cd_company_documents_idb = cd_company_documents.id "
                . "LEFT JOIN cd_company_documents_cstm ON cd_company_documents.id = cd_company_documents_cstm.id_c "
                . "WHERE accounts_cstm.id_c = '" . $bean->id . "' AND cd_company_documents.deleted = 0 AND accounts_cd_company_documents_1_c.deleted = 0 AND cd_company_documents.category_id = 'Qualification' "
                . "ORDER BY cd_company_documents.date_entered DESC LIMIT 0,1";
        $companyDocument = $db->query($queryForGetIdOfCompanyDocument);
        $result = $db->fetchByAssoc($companyDocument);
        $companyDocumentID = $result['id'];
        $qualification = $result['method_of_qualification_c'];
        $status = $result['vendor_status_c'];
        $qualification_date = $result['date_of_qualification_c'];
        
        //If no related company document exist
        if (empty($companyDocumentID)) {
            
            //Set the default values depending on category
            if ($bean->category_c == "Vendor") {
                $bean->method_of_qualification_c = "TBD";
                $bean->current_vendor_status_c = "Pending Approval";
                $bean->approval_date_c = null;
                $bean->next_approval_due_date_c = null;
            } else {
                $bean->method_of_qualification_c = "";
                $bean->current_vendor_status_c = "";
            }
        } else { //It is in edit or it is calling after relationship add or delete
            $bean->method_of_qualification_c = $qualification;
            $bean->current_vendor_status_c = $status;
            $bean->approval_date_c = $qualification_date;
          
            //If status is neither Disapproved nor Retired then calculate appropriate Next Approval Due date
            if($status != "Disapproved" && $status != "Retired" ){
                $bean->next_approval_due_date_c = date('Y-m-d', strtotime($bean->approval_date_c . '+' . $bean->approval_interval_days_c . 'days'));
                
            } else { //Other made it null as well as hide, Hidden functionality is implemented by Sugar Formula
                $bean->next_approval_due_date_c = null;
            }
            
        }

        // IF THIS IS NEW RECORD 
        if ($bean->fetched_row['id'] == "") {
            $bean->client_code_c = $randomString;
        } else {
            if ($bean->client_code_c == "") {
                $bean->client_code_c = ""; //$randomString; 
            } else if ($bean->client_code_c != "") {

                if ($this->check_duplicate($bean->client_code_c, $bean->id)) {
                    $bean->client_code_c = $bean->fetched_row['client_code_c'];
                } else {
                    $bean->client_code_c = strtoupper($bean->client_code_c);
                }
            }
        }
    }

    /*
      +------------------------
      | Fucntion to Generate UNIQUE THREE Letter Code for company.
      +------------------------
     */

    function genrate_code() {

        $characters = 'ABCDEFIJKLMNOPGHMNOPQIJKLMNOPRSTUVWIJKLMNOPQIJKLMNOPRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        if ($this->check_duplicate($randomString)) {
            return $this->genrate_code();
        } else {
            return $randomString;
        }
    }
    

    /*
      +------------------------
      | Fucntion to Check Duplicate THREE Letter Code for company.
      +------------------------
     */

    function check_duplicate($code, $record_id = "") {
        global $db;
        $whereClause = "";
        if ($record_id != "") {
            $whereClause = " AND accounts_cstm.id_c != '" . $record_id . "' AND accounts.deleted = '0'";
        }
        $query = "SELECT accounts_cstm.id_c FROM accounts_cstm INNER JOIN accounts ON accounts_cstm.id_c = accounts.id WHERE accounts_cstm.client_code_c = '$code' " . $whereClause;

        $result = $db->query($query);

        $row = $db->fetchByAssoc($result);
        if ($row['id_c']) {
            return 1;
        } else {
            return 0;
        }
    }

}