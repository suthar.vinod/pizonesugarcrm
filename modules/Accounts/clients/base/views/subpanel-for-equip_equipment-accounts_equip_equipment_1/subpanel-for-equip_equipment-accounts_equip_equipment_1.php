<?php
// created: 2020-11-03 06:41:30
$viewdefs['Accounts']['base']['view']['subpanel-for-equip_equipment-accounts_equip_equipment_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'default' => true,
          'label' => 'LBL_LIST_ACCOUNT_NAME',
          'enabled' => true,
          'name' => 'name',
          'link' => true,
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);