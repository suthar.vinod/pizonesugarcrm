<?php
$viewdefs['Accounts'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'size' => 'large',
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 
              array (
                'name' => 'name',
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'client_code_c',
                'label' => 'LBL_CLIENT_CODE',
              ),
              1 => 
              array (
                'name' => 'phone_office',
              ),
              2 => 
              array (
                'name' => 'category_c',
                'label' => 'LBL_CATEGORY',
              ),
              3 => 
              array (
                'name' => 'website',
              ),
              4 => 
              array (
                'name' => 'billing_address_country',
                'comment' => 'The country used for the billing address',
                'label' => 'LBL_BILLING_ADDRESS_COUNTRY',
              ),
              5 => 
              array (
                'name' => 'billing_address_state',
                'comment' => 'The state used for billing address',
                'label' => 'LBL_BILLING_ADDRESS_STATE',
              ),
              6 => 
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
                'span' => 12,
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'company_status_c',
                'label' => 'LBL_COMPANY_STATUS',
              ),
              1 => 
              array (
                'name' => 'payment_terms_2_c',
                'label' => 'LBL_PAYMENT_TERMS_2',
              ),
              2 => 
              array (
                'name' => 'view_all_company_projects_c',
                'label' => 'LBL_VIEW_ALL_COMPANY_PROJECTS',
              ),
              3 => 
              array (
                'name' => 'connection_source_c',
                'label' => 'LBL_CONNECTION_SOURCE',
              ),
              4 => 
              array (
                'name' => 'referred_by_c',
                'label' => 'LBL_REFERRED_BY',
              ),
              5 => 
              array (
                'name' => 'tradeshow_name_c',
                'label' => 'LBL_TRADESHOW_NAME',
              ),
              6 => 
              array (
                'name' => 'biocomp_pricelist_c',
                'label' => 'LBL_BIOCOMP_PRICELIST',
              ),
              7 => 
              array (
                'name' => 'account_representative_c',
                'label' => 'LBL_ACCOUNT_REPRESENTATIVE',
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'vendor_type_c',
                'label' => 'LBL_VENDOR_TYPE',
              ),
              1 => 
              array (
                'name' => 'vendor_list_c',
                'label' => 'LBL_VENDOR_LIST',
              ),
              2 => 
              array (
                'name' => 'account_number_c',
                'label' => 'LBL_ACCOUNT_NUMBER',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'name' => 'iso_certified_c',
                'label' => 'LBL_ISO_CERTIFIED',
                'span' => 12,
              ),
              5 => 
              array (
                'name' => 'vendor_level_c',
                'label' => 'LBL_VENDOR_LEVEL',
              ),
              6 => 
              array (
                'name' => 'method_of_qualification_c',
                'label' => 'LBL_METHOD_OF_QUALIFICATION',
              ),
              7 => 
              array (
                'name' => 'current_vendor_status_c',
                'label' => 'LBL_CURRENT_VENDOR_STATUS',
              ),
              8 => 
              array (
                'name' => 'approval_date_c',
                'label' => 'LBL_APPROVAL_DATE',
              ),
              9 => 
              array (
                'name' => 'approval_interval_days_c',
                'label' => 'LBL_APPROVAL_INTERVAL_DAYS',
              ),
              10 => 
              array (
                'name' => 'next_approval_due_date_c',
                'label' => 'LBL_NEXT_APPROVAL_DUE_DATE',
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'company_notes_c',
                'studio' => 'visible',
                'label' => 'LBL_COMPANY_NOTES',
                'span' => 12,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);
