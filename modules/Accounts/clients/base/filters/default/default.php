<?php
// created: 2020-01-08 16:43:53
$viewdefs['Accounts']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'biocomp_pricelist_c' => 
    array (
    ),
    'client_code_c' => 
    array (
    ),
    'account_type' => 
    array (
    ),
    'industry' => 
    array (
    ),
    'annual_revenue' => 
    array (
    ),
    'rating' => 
    array (
    ),
    'phone_office' => 
    array (
    ),
    'category_c' => 
    array (
    ),
    'ownership' => 
    array (
    ),
    'employees' => 
    array (
    ),
    'sic_code' => 
    array (
    ),
    'view_all_company_projects_c' => 
    array (
    ),
    'ticker_symbol' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'assigned_user_name' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'billing_address_country' => 
    array (
    ),
    'billing_address_state' => 
    array (
    ),
  ),
);