<?php
// created: 2020-11-03 06:41:29
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_LIST_ACCOUNT_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'category_c' => 
  array (
    'name' => 'category_c',
    'usage' => 'query_only',
  ),
);