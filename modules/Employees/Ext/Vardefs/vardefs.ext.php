<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Employees/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:54
$dictionary['Employee']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Employees/Ext/Vardefs/sugarfield_email.php

 // created: 2018-09-24 15:24:08
$dictionary['Employee']['fields']['email']['len']='255';
$dictionary['Employee']['fields']['email']['required']=false;
$dictionary['Employee']['fields']['email']['audited']=false;
$dictionary['Employee']['fields']['email']['massupdate']=true;
$dictionary['Employee']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Employee']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['Employee']['fields']['email']['merge_filter']='disabled';
$dictionary['Employee']['fields']['email']['unified_search']=false;
$dictionary['Employee']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.67',
  'searchable' => true,
);
$dictionary['Employee']['fields']['email']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Employees/Ext/Vardefs/sugarfield_employee_status.php

 // created: 2018-09-25 12:50:30
$dictionary['Employee']['fields']['employee_status']['default']='Active';
$dictionary['Employee']['fields']['employee_status']['audited']=false;
$dictionary['Employee']['fields']['employee_status']['massupdate']=true;
$dictionary['Employee']['fields']['employee_status']['duplicate_merge']='enabled';
$dictionary['Employee']['fields']['employee_status']['duplicate_merge_dom_value']='1';
$dictionary['Employee']['fields']['employee_status']['merge_filter']='disabled';
$dictionary['Employee']['fields']['employee_status']['unified_search']=false;
$dictionary['Employee']['fields']['employee_status']['calculated']=false;
$dictionary['Employee']['fields']['employee_status']['dependency']=false;

 
?>
