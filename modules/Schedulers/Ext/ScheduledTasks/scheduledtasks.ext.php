<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/overdue_deliverables.php


//Add job in job string
array_push($job_strings, 'overdue_deliverables');

/* * This function will found out all the Work Product Deliverables whose
 * Internal Final Due Date has passed and will send the emails to "Assigned To", "Report to"
 * and Study Directors or related Work Products.
 */

function overdue_deliverables() {

    $deliverable_status = array('Completed', 'Sponsor Retracted', 'None', 'Not Performed'); //Array of those statuses at which email will not trigger
    $current_date = date("Y-m-d"); //current date
    $wpd_emailAdd = array(); // contains the email addresses of assigned to, Report to and Study Director
    //Query to get the "Internal Final Due Date" and "Deliverable Status" of Work Product Deliverables
    $query = new SugarQuery();
    $query->from(BeanFactory::newBean('M03_Work_Product_Deliverable'), array('alias' => 'wpd', 'team_security' => false));
    $query->joinTable('m03_work_product_deliverable_cstm', array(
        'joinType' => 'INNER',
        'alias' => 'wpd_cstm',
        'linkingTable' => true,
    ))->on()->equalsField('wpd.id', 'wpd_cstm.id_c');
    $query->select('wpd.id', 'wpd.name', 'wpd_cstm.internal_final_due_date_c', 'wpd_cstm.deliverable_status_c');
    $query->where()
            ->notIn('wpd_cstm.deliverable_status_c', $deliverable_status)
            ->lt('wpd_cstm.internal_final_due_date_c', $current_date)
            ->notNull('wpd_cstm.internal_final_due_date_c')
            ->isNotEmpty('wpd_cstm.internal_final_due_date_c');

    $queryCompile = $query->compile();
    $queryResult = $query->execute();

    //Get site url so that appropriate Work Product Delieverable can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];

    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();

    //We will send individual Emails for individual Work Product Deliverables
    foreach ($queryResult as $key => $values) {
        //delete the previous email addresses from the array
        unset($wpd_emailAdd);
        $template = getTemplate($values, $template, $site_url);
        $wpd_emailAdd = getEmailAddOfAssignedToAndReportTo($values['id']);
        $wpd_emailAdd[] = getTheStudyDirectorOfRelatedWP($values['id']);

        //verify all the email addresses should belongs to valid domain
        foreach ($wpd_emailAdd as $key => $emails) {
            $status = verifyContactForNotification($emails);

            //email address belongs to invalid domain
            if ($status == false) {
                unset($wpd_emailAdd[$key]);
            }
        }
        $wpd_emailAdd = array_values($wpd_emailAdd); //re-index the array elements
        //remove the repeated email addresses
        $wpd_emailAdd = array_unique($wpd_emailAdd);
       //   if ($values['id'] == 'eb144ebe-7c95-11e9-b5b7-06e41dba421a') {
        sendMail($values['id'], $template, $wpd_emailAdd);
       //   }
    }


    return true;
}

/**
 * Get the overdue email template and set the appropriate values
 * @param type $values
 * @param type $template
 * @param type $site_url
 * @return type
 */
function getTemplate($values, $template, $site_url) {
    $template->retrieve_by_string_fields(array('name' => 'Overdue Deliverable', 'type' => 'email'));


    $wpd_name = '<a target="_blank"href="' . $site_url . '/#M03_Work_Product_Deliverable/' . $values['id'] . '">' . $values['name'] . '</a>';

    $template->body_html = str_replace('[insert Work Product Deliverable name]', $wpd_name, $template->body_html);
    return $template;
}

/**
 * This function will send the emails to the "Assigned To", "Report to" and the study director
 * of Work Product
 * @param type $template
 * @param type $wpd_emailAdd
 */
function sendMail($id, $template, $wpd_emailAdd) {

    $emailObj = new Email();
    $defaults = $emailObj->getSystemDefaultEmail();
    $mail = new SugarPHPMailer();
    $mail->setMailerForSystem();
    $mail->IsHTML(true);
    $mail->From = $defaults['email'];
    $mail->FromName = $defaults['name'];

    $mail->Subject = $template->subject;


    $mail->Body = $template->body_html;


    foreach ($wpd_emailAdd as $wmail) {
        $mail->AddAddress($wmail);
    }

    //If their exist some valid email addresses then send email
    if (!empty($wpd_emailAdd)) {
	//$GLOBALS['log']->fatal("valid email addresses are ". print_r($wpd_emailAdd, 1));
        if (!$mail->Send()) {
            $GLOBALS['log']->debug("Fail to Send Email, Please check settings");
        } else {
            $GLOBALS['log']->debug('email sent for '. $id);
        }
    }
}

/**
 * Get The "Assigned to" and "Report To" users id's
 * @global type $db
 * @param type $id
 * @return type
 */
function getEmailAddOfAssignedToAndReportTo($id) {
    global $db;

    $ids = array(); //Array to maintain ids of Assigned user and Report to user
    $email_adds = array(); //Array to maintain the Email Addresses of Assigned user and Report to user
    //Get the user id
    $queryToGetUserId = "SELECT users.id, users.reports_to_id FROM `users` INNER JOIN
                  `m03_work_product_deliverable` ON m03_work_product_deliverable.assigned_user_id = users.id
                   WHERE users.deleted = 0 AND m03_work_product_deliverable.id = '" . $id . "' LIMIT 0,1";

    $result = $db->query($queryToGetUserId);
    if ($user = $db->fetchByAssoc($result)) {
        $ids[] = $user['id'];
        $ids[] = $user['reports_to_id'];
        $email_adds = getRecordEmail($ids);
    }
    //  $GLOBALS['log']->fatal("email adds are " . print_r($email_adds, 1));
    return $email_adds;
}

/**
 * Retrieves email addresses of "Assigned to" and "Report to" users based on user ids
 * @param string $record
 * @return string email_address|null
 */
function getRecordEmail($ids) {
    //  $GLOBALS['log']->fatal("ids are " . print_r($ids, 1));
    $user_ids = implode("', '", $ids);
    //   $GLOBALS['log']->fatal("user ids are " . print_r($user_ids, 1));
    $email_addresses = array();

    //Get only those email addresses which are not empty
    $query = "
        SELECT
            email_addresses.email_address
        FROM
            `email_addresses`
        INNER JOIN
            `email_addr_bean_rel` ON email_addr_bean_rel.email_address_id = email_addresses.id
        WHERE email_addr_bean_rel.deleted = 0 AND email_addresses.deleted='0' AND email_addresses.invalid_email='0' AND email_addresses.opt_out='0'
        AND email_addr_bean_rel.bean_id IN ('{$user_ids}') AND email_addresses.email_address IS NOT NULL AND email_addresses.email_address != '' ";

    $res = $GLOBALS['db']->query($query);
    if ($res->num_rows >= 1) { //If any email address is associated with user
        while ($user = $GLOBALS['db']->fetchByAssoc($res)) {
            $email_addresses[] = $user['email_address'];
        }
        return $email_addresses;
    }

    return '';
}

/**
 * This function will return the Study Director Email Address of the related Work Product
 * @param type $wpd_id
 */
function getTheStudyDirectorOfRelatedWP($wpd_id) {
    global $db;

    //Query to Get the Work Product and Study Director ids related to the Work Product Delieverables

    $query = new SugarQuery();
    $query->from(BeanFactory::newBean('M03_Work_Product_Deliverable'), array('alias' => 'wpd', 'team_security' => false));
    $query->joinTable('m03_work_product_m03_work_product_deliverable_1_c', array(
        'joinType' => 'INNER',
        'alias' => 'wp_wpd',
        'linkingTable' => true,
    ))->on()->equalsField('wpd.id', 'wp_wpd.m03_work_pe584verable_idb');
    $query->joinTable('m03_work_product', array(
        'joinType' => 'INNER',
        'alias' => 'wp',
        'linkingTable' => true,
    ))->on()->equalsField('wp_wpd.m03_work_p0b66product_ida', 'wp.id');
    $query->joinTable('m03_work_product_cstm', array(
        'joinType' => 'INNER',
        'alias' => 'wp_cstm',
        'linkingTable' => true,
    ))->on()->equalsField('wp.id', 'wp_cstm.id_c');
    $query->select('wp.id', 'wp_cstm.contact_id_c');
    $query->where()
            ->equals('wpd.deleted', 0)
            ->equals('wp_wpd.deleted', 0)
            ->equals('wp.deleted', 0)
            ->equals('wpd.id', $wpd_id);
    $query->limit(1);

    $queryResult = $query->execute();
    $wp_id = $queryResult[0]['id'];
    $study_dir_id = $queryResult[0]['contact_id_c'];

    //Get the primary email address of the selected contact id
    $stdy_bean = BeanFactory::getBean('Contacts', $study_dir_id);
    
    $primaryEmailAddress = $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
    if ($primaryEmailAddress != false) {
        return $primaryEmailAddress;
    }
}

/**
 * Verify user email address domain is 'apsemail.com' otherwise return false
 * @param string $account_name
 * @param string $email
 * @return bool true|false
 */
function verifyContactForNotification($email) {
    $domainToSearch = 'apsemail.com';
    $domain_name = substr(strrchr($email, "@"), 1);
    if (trim(strtolower($domain_name) == $domainToSearch)) {
        return true;
    }
    return false;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/communication_requiring_assessment.php


//Add job in job string
$job_strings[] = 'communication_requiring_assessment';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function communication_requiring_assessment() {
    global $db;
    $current_date = date("Y-m-d"); //current date
    $wp_emailAdd = array(); // contains the email addresses of assigned to, Report to and Study Director
    //Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];

    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();

    $queryCustom = "SELECT 
                    commu.id AS communicationID, 
                    commu.name AS CommunicationName,  
                    commu_cstm.target_reinspection_date_c, 
					commu_cstm.error_category_c,  					
                    commu_cstm.reinspection_date_c, 
                    commu_wp.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
                    wp.name  AS workProductName 
            FROM `m06_error` AS commu
                    INNER JOIN m06_error_cstm AS commu_cstm
                    ON  commu.id = commu_cstm.id_c

                    RIGHT JOIN m06_error_m03_work_product_1_c AS commu_wp
                    ON  commu.id = commu_wp.m06_error_m03_work_product_1m06_error_ida

                    INNER JOIN m03_work_product AS wp
                    ON  commu_wp.m06_error_m03_work_product_1m03_work_product_idb=wp.id 
		WHERE 
			 commu_cstm.error_category_c != 'Internal Feedback' AND commu_cstm.error_category_c != 'External Feedback' 
			AND
            commu_cstm.target_reinspection_date_c >= '2020-01-30'
            AND
            commu_cstm.target_reinspection_date_c <'" . $current_date . "' 
            AND 
            commu_cstm.reinspection_date_c IS NULL 
            AND commu_wp.m06_error_m03_work_product_1m03_work_product_idb IS NOT NULL

            ORDER BY commu_cstm.target_reinspection_date_c DESC";

    $queryCommResult = $db->query($queryCustom);
    //
     while ($fetchCommResult = $db->fetchByAssoc($queryCommResult)) {

        $workProductID = $fetchCommResult['workProductID'];
        $communicationID = $fetchCommResult['communicationID'];
        $communicationName = $fetchCommResult['CommunicationName'];
        $workProductName = $fetchCommResult['workProductName'];

        if ($workProductID != "" && $communicationID != "") {
            //delete the previous email addresses from the array
            unset($wp_emailAdd);
            unset($wp_emailAddTest); //to Be deleted
            //$wp_emailAdd	= getEmailAddOfAssignedToAndReportTo($fetchResult['workProductID']);

            /* Get Email id of Assigned User and report to User */
            $wp_emailAdd = array();
            $wp_emailAddTest = array(); //to Be deleted
             

            /* Get Study Director */
            
            $query = new SugarQuery();
            $query->from(BeanFactory::newBean('M03_Work_Product'), array('alias' => 'wp', 'team_security' => false));

            $query->joinTable('m03_work_product_cstm', array(
                'joinType' => 'INNER',
                'alias' => 'wp_cstm',
                'linkingTable' => true,
            ))->on()->equalsField('wp.id', 'wp_cstm.id_c');
            $query->select('wp.id', 'wp_cstm.contact_id_c');
            $query->where()
                    ->equals('wp.deleted', 0)
                    ->equals('wp.id', $workProductID);
            $query->limit(1);

            $queryResult = $query->execute();
            $wp_id = $queryResult[0]['id'];
            $study_dir_id = $queryResult[0]['contact_id_c'];

            if ($study_dir_id != "" && $wp_id != "") {

                //Get the primary email address of the selected contact id
                $stdy_bean = BeanFactory::getBean('Contacts', $study_dir_id);

                $primaryEmailAddress = $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
				//$primaryEmailAddress = $stdy_bean->email1;
                if ($primaryEmailAddress != false) {
                    $wp_emailAdd[] = $primaryEmailAddress;
                }
				
				//get Study Director's Manager
				if($stdy_bean->contact_id_c){ 
					$director_id =$stdy_bean->contact_id_c;
					$director_bean = BeanFactory::getBean('Contacts', $director_id);
					if($director_bean->email1){
						$director_email = $director_bean->email1;
						 $wp_emailAdd[] = $director_email;
					}
				}
            }
			
			$wp_emailAdd = array_values($wp_emailAdd); //re-index the array elements
            //remove the repeated email addresses
            $wp_emailAdd = array_unique($wp_emailAdd);

            $template->retrieve_by_string_fields(array('name' => 'Communication Requiring Assessment', 'type' => 'email'));

            $communication_name = '<a target="_blank"href="' . $site_url . '/#M06_Error/' . $communicationID . '">' . $communicationName . '</a>';
            $workProduct_name = '<a target="_blank"href="' . $site_url . '/#M03_Work_Product/' . $workProductID . '">' . $workProductName . '</a>';

            $template->body_html = str_replace('[Communication ID]', $communication_name, $template->body_html);
            $template->body_html = str_replace('[Work Product]', $workProduct_name, $template->body_html);
            //$template->body_html .= implode(",", $wp_emailAdd); // for testing to be deleted
            //$template		= getTemplate($fetchResult, $template, $site_url);
             
			
			$wp_emailAdd[] = 'mconforti@apsemail.com';
            //$wp_emailAdd[] = 'mjohnson@apsemail.com';

            $defaults = $emailObj->getSystemDefaultEmail();
			$mail = new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From = $defaults['email'];
            $mail->FromName = $defaults['name'];
            $mail->Subject = "Test Run - ".$template->subject;
            $mail->Body = $template->body_html;
			
			// for testing to be deleted
			//$wp_emailAddTest[] = 'cjagadeeswaraiah@apsemail.com'; ////to Be deleted
			//$wp_emailAddTest[] = 'vsuthar@apsemail.com'; ////to Be deleted
			// for testing to be deleted
			//$wp_emailAddTest[] = 'vsuthar@apsemail.com'; ////to Be deleted
			
            foreach ($wp_emailAdd as $wmail) { // wp_emailAddTest to be replace with  wp_emailAdd 
                $mail->AddAddress($wmail);
            }
            
			// for testing to be deleted
            //$mail->AddBCC('lsaini@apsemail.com');
			$mail->AddBCC('cjagadeeswaraiah@apsemail.com');
			$mail->AddBCC('vsuthar@apsemail.com');
            //If their exist some valid email addresses then send email
            if (!empty($wp_emailAdd)) {
                //$GLOBALS['log']->fatal("valid email addresses are ". print_r($wpd_emailAdd, 1));
                if (!$mail->Send()) {
                    $GLOBALS['log']->debug("Fail to Send Email, Please check settings");
                } else {
                    $GLOBALS['log']->debug('email sent for ' . $communicationID);
                }
				unset($mail);
            }
        }
    }

    /// For Reassement Communication Workflow
    $queryCustomReassement = "SELECT 
					commu.id AS communicationID, 
					commu.name AS CommunicationName,  
					 commu_cstm.error_category_c,  
					commu_cstm.target_sd_reassessment_date_c,  
					commu_cstm.actual_sd_reassessment_date_c, 
					commu_wp.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
					wp.name  AS workProductName 
				FROM `m06_error` AS commu
					INNER JOIN m06_error_cstm AS commu_cstm
					ON  commu.id = commu_cstm.id_c
					
					RIGHT JOIN m06_error_m03_work_product_1_c AS commu_wp
					ON  commu.id = commu_wp.m06_error_m03_work_product_1m06_error_ida
					
					INNER JOIN m03_work_product AS wp
					ON  commu_wp.m06_error_m03_work_product_1m03_work_product_idb=wp.id 
				WHERE 
				commu_cstm.error_category_c != 'Internal Feedback' AND commu_cstm.error_category_c != 'External Feedback'
				AND
				commu_cstm.reassessment_required_c=1 AND  
				commu_cstm.target_sd_reassessment_date_c<'" . $current_date . "' 
				AND 
				commu_cstm.actual_sd_reassessment_date_c IS NULL 
				AND commu_wp.m06_error_m03_work_product_1m03_work_product_idb IS NOT NULL
				AND YEAR(commu_cstm.target_sd_reassessment_date_c) = YEAR(CURDATE()) 
				ORDER BY commu_cstm.target_sd_reassessment_date_c DESC";
 
    $queryResultReassement = $db->query($queryCustomReassement);

    while ($fetchResultReassement = $db->fetchByAssoc($queryResultReassement)) {
        $re_workProductID = $fetchResultReassement['workProductID'];
        $re_communicationID = $fetchResultReassement['communicationID'];
        $re_communicationName = $fetchResultReassement['CommunicationName'];
        $re_workProductName = $fetchResultReassement['workProductName'];

        if ($re_workProductID != "" && $re_communicationID != "") {
            //delete the previous email addresses from the array
            unset($wp_emailAdd);
            unset($wp_emailAddTest);
            //$wp_emailAdd	= getEmailAddOfAssignedToAndReportTo($fetchResultReassement['workProductID']);
            //$wp_emailAdd[]	= getTheStudyDirectorOfRelatedWP($fetchResultReassement['workProductID']);
            //===============================
            //$wp_emailAdd	= getEmailAddOfAssignedToAndReportTo($fetchResult['workProductID']);

            /* Get Email id of Assigned User and report to User */
            $ids = array(); //Array to maintain ids of Assigned user and Report to user
            $email_adds = array(); //Array to maintain the Email Addresses of Assigned user and Report to user
            $wp_emailAdd = array();
            $wp_emailAddTest = array(); //to Be deleted
             

            /* Get Study Director */
            //$wp_emailAdd[]	= getTheStudyDirectorOfRelatedWP($fetchResult['workProductID']);

            $query = new SugarQuery();
            $query->from(BeanFactory::newBean('M03_Work_Product'), array('alias' => 'wp', 'team_security' => false));

            $query->joinTable('m03_work_product_cstm', array(
                'joinType' => 'INNER',
                'alias' => 'wp_cstm',
                'linkingTable' => true,
            ))->on()->equalsField('wp.id', 'wp_cstm.id_c');
            $query->select('wp.id', 'wp_cstm.contact_id_c');
            $query->where()
                    ->equals('wp.deleted', 0)
                    ->equals('wp.id', $re_workProductID);
            $query->limit(1);

            $queryResult = $query->execute();
            $wp_id = $queryResult[0]['id'];
            $study_dir_id = $queryResult[0]['contact_id_c'];

            if ($study_dir_id != "" && $wp_id != "") {

                //Get the primary email address of the selected contact id
                $stdy_bean = BeanFactory::getBean('Contacts', $study_dir_id);

                $primaryEmailAddress = $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
                if ($primaryEmailAddress != false) {
                     $wp_emailAdd[] = $primaryEmailAddress;
                }
				
				//get Study Director's Manager
				if($stdy_bean->contact_id_c){ 
					$director_id =$stdy_bean->contact_id_c;
					$director_bean = BeanFactory::getBean('Contacts', $director_id);
					if($director_bean->email1){
						$director_email = $director_bean->email1;
						 $wp_emailAdd[] = $director_email;
					}
				}
            }
			
			 //$template		= getTemplate($fetchResultReassement, $template, $site_url);
            $wp_emailAdd = array_values($wp_emailAdd); //re-index the array elements
            //remove the repeated email addresses
            $wp_emailAdd = array_unique($wp_emailAdd);


            $template->retrieve_by_string_fields(array('name' => 'Communication Requiring Reassessment', 'type' => 'email'));

            $communication_name = '<a target="_blank"href="' . $site_url . '/#M06_Error/' . $re_communicationID . '">' . $re_communicationName . '</a>';
            $workProduct_name = '<a target="_blank"href="' . $site_url . '/#M03_Work_Product/' . $re_workProductID . '">' . $re_workProductName . '</a>';

            $template->body_html = str_replace('[Communication ID]', $communication_name, $template->body_html);
            $template->body_html = str_replace('[Work Product]', $workProduct_name, $template->body_html);
            //$template->body_html .= implode(",", $wp_emailAdd); // For testing purpose only , to be deleted
             
			$wp_emailAdd[] = 'mconforti@apsemail.com';
			
            $defaults = $emailObj->getSystemDefaultEmail();
			$mail = new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From = $defaults['email'];
            $mail->FromName = $defaults['name'];
			$mail->Subject = "Test Run - ".$template->subject;
            $mail->Body = $template->body_html;
			
			//$wp_emailAddTest[] = 'cjagadeeswaraiah@apsemail.com'; ////to Be deleted
			//$wp_emailAddTest[] = 'vsuthar@apsemail.com';
			
            foreach ($wp_emailAdd as $wmail) {
                $mail->AddAddress($wmail);
            }
             
            $mail->AddBCC('cjagadeeswaraiah@apsemail.com');
			$mail->AddBCC('vsuthar@apsemail.com');
			//$mail->AddBCC('lsaini@apsemail.com');
            //If their exist some valid email addresses then send email
            if (!empty($wp_emailAdd)) {
                //$GLOBALS['log']->fatal("valid email addresses are ". print_r($wpd_emailAdd, 1));
                if (!$mail->Send()) {
                    $GLOBALS['log']->debug("Fail to Send Email, Please check settings");
                } else {
                    $GLOBALS['log']->debug('email sent for ' . $re_communicationID);
                }
				unset($mail);
            }
        }
    }
   return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/deceased_animal_communication.php

//Add job in job string
$job_strings[] = 'deceased_animal_communication';
/* * This function will send email to pathologist to inform about the Deceased Animals. */

function deceased_animal_communication() {
    global $db;
    
	$offset_time = time(); 
	$currentDate = date("Y-m-d 00:00:00",$offset_time);
	$lastmonthDate = date("Y-m-d 00:00:00", strtotime('-30 days')); 
     
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];
	 
    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();

    $emailAdd = array();  //user to whom email is being sent
	$queryCustom = "SELECT 
						l1_cstm.s_species_id_c l1_cstm_s_species_id_c,
						s_species1.name s_species1_name,
						COUNT(COMM.id) m06_error__allcount, 
						COUNT(DISTINCT  COMM.id) m06_error__count
					FROM m06_error AS COMM
					LEFT JOIN  m06_error_anml_animals_1_c AS l1_1  
						ON COMM.id=l1_1.m06_error_anml_animals_1m06_error_ida AND l1_1.deleted=0

					LEFT JOIN  anml_animals l1 
						ON l1.id=l1_1.m06_error_anml_animals_1anml_animals_idb AND l1.deleted=0
					LEFT JOIN m06_error_cstm m06_error_cstm 
						ON COMM.id = m06_error_cstm.id_c
					LEFT JOIN anml_animals_cstm l1_cstm
						ON l1.id = l1_cstm.id_c
					LEFT JOIN s_species s_species1 
						ON s_species1.id = l1_cstm.s_species_id_c AND IFNULL(s_species1.deleted,0)=0 

					 WHERE (((
					 
					 (m06_error_cstm.error_category_c = 'Deceased Animal'  
						AND m06_error_cstm.related_to_c = 'non Study'
						AND (m06_error_cstm.date_time_discovered_c >= '".$lastmonthDate."' AND m06_error_cstm.date_time_discovered_c <= '".$currentDate."')
						)) 
						AND (((( l1_cstm.s_species_id_c='5de72184-1131-11ea-abb2-02fb813964b8')
					) OR (( l1_cstm.s_species_id_c='5dbadd5e-1131-11ea-b633-02fb813964b8')
					) OR (( l1_cstm.s_species_id_c='5dad1606-1131-11ea-80f4-02fb813964b8')
					) OR (( l1_cstm.s_species_id_c='5d9c2666-1131-11ea-9421-02fb813964b8')
					) OR (( l1_cstm.s_species_id_c='5d89b5d0-1131-11ea-afac-02fb813964b8')
					))))) 
					AND  COMM.deleted=0 
					 AND IFNULL(s_species1.deleted,0)=0 
					 GROUP BY l1_cstm.s_species_id_c 
					,s_species1.name  ORDER BY l1_cstm_s_species_id_c ASC";

    $queryCommResult = $db->query($queryCustom);
    $speciesArr = array();
    while ($fetchCommResult = $db->fetchByAssoc($queryCommResult)) {

        $speciesCount = $fetchCommResult['m06_error__allcount'];
        $speciesName = $fetchCommResult['s_species1_name'];
        $l1_cstm_s_species_id_c = $fetchCommResult['l1_cstm_s_species_id_c'];
		
		if($speciesCount>4){
			$speciesArr[] = $speciesName."s - ".$speciesCount;
			
		}
         
	}

	if(count($speciesArr)>0){ 
		$emailBody = "<table width='600px' cellspacing='0' cellpadding='4' border='0' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'><tr><td>>4 Test Systems of the following species have been reported as deceased in the last 30 days: </td></tr>";
		foreach ($speciesArr as $key => $value) { 
			$emailBody .= "<tr><td>".$value."</td></tr>";		
		}		
		 
		$emailBody .= "</table><br></br>";
		
		$emailBody .= '<address style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-style: normal; font-weight: 400; letter-spacing: normal; text-indent: 0px; text-transform: none; word-spacing: 0px;"><span>CONFIDENTIALITY NOTICE: This e-mail transmission may contain confidential or legally privileged information that is intended only for the individual or entity named in the e-mail address. Any unauthorized distribution or copying of this transmittal or its attachments, if any, is prohibited. If you have received this e-mail transmission in error, please reply to&nbsp;</span><a class="email" href="mailto:mjohnson@apsemail.com">mjohnson@apsemail.com</a><span>, so that American Preclinical Services can arrange for proper delivery, then please delete the message from your inbox. Thank you.</span></address>';
	 
		//$emailAdd[] = 'mjohnson@apsemail.com';
		$emailAdd[] = 'clinicalVets@apsemail.com';
		$emailAdd[] = 'pathologists@apsemail.com';
		
		
		 
		$defaults = $emailObj->getSystemDefaultEmail();
			$mail = new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From = $defaults['email'];
            $mail->FromName = $defaults['name'];
            $mail->Subject = "Deceased Small Animal Trend";
            $mail->Body = $emailBody;
					 
			foreach ($emailAdd as $wmail) {
				$mail->AddAddress($wmail);
			}
			 
			 
			//$mail->AddCC('vsuthar@apsemail.com');
			$mail->AddBCC('cjagadeeswaraiah@apsemail.com');
			//If their exist some valid email addresses then send email
            if (!empty($emailAdd)) {
                if (!$mail->Send()) {
                    $GLOBALS['log']->fatal("Deceased Small Animal Trend email Failed");
                } else {
                    $GLOBALS['log']->fatal('Deceased Small Animal Trend email sent');
                }
				unset($mail);
            }
    }
	
	
	return true;
	}		 
  
	 

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/equip_service_workflow_reminder.php

//Add job in job string

use function PHPSTORM_META\type;

$job_strings[] = 'equip_service_workflow_reminder';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function equip_service_workflow_reminder()
{
  global $db;
  $current_date = date("Y-m-d"); //current date

  $site_url = $GLOBALS['sugar_config']['site_url'];
  $domainToSearch = 'apsemail.com';

  $efIdArray = array();
  $eArr = array();
  $queryCustom = "SELECT id FROM efr_equipment_facility_recor where DATE_ADD(date(date_entered), INTERVAL 30 DAY) = '$current_date' and deleted=0";


  $efrQuery = $db->query($queryCustom);

  while ($fetchResult = $db->fetchByAssoc($efrQuery)) {
    $efrId = $fetchResult['id'];

    $sql = "SELECT equip_equi01e2uipment_ida FROM equip_equipment_efr_equipment_facility_recor_2_c where equip_equiffd2y_recor_idb='$efrId' and deleted=0";

    $sqlResult = $db->query($sql);
    $fetchResult1 = $db->fetchByAssoc($sqlResult);
    $efId = $fetchResult1['equip_equi01e2uipment_ida'];
    if ($efId != "") {
      $efIdArray[] = $efId;
    }
  }
  $efIdArray = array_unique($efIdArray);

  $p = 0;
  foreach ($efIdArray as $eId) {
    $p = 0;
    $efBean = BeanFactory::retrieveBean('Equip_Equipment', $eId, array('disable_row_level_security' => true));
    $efBean->load_relationship('equip_equipment_efr_equipment_facility_recor_2');
    $relateEfrIds = $efBean->equip_equipment_efr_equipment_facility_recor_2->get();
    if ($efBean->status_c != 'Retired') {
      foreach ($relateEfrIds as $efrrId) {
        $efBean = BeanFactory::retrieveBean('EFR_Equipment_Facility_Recor', $efrrId, array('disable_row_level_security' => true));
        if ($p == 1)
          continue;
        if (strpos($efBean->type_2_c, 'Initial In Service') && (!strpos($efBean->type_2_c, 'Initial In Service 2'))) {
          $p = 1;
        } else {
          if (strpos($efBean->type_2_c, 'Initial In Service 2'))
            $eArr[$efrrId] = $eId;
        }
      }
    }
    if ($p > 0) {
      foreach ($eArr as $value) {
        $key = array_search($eId, $eArr);
        unset($eArr[$key]);
      }
    }
  }

  foreach ($eArr as $erfval => $efVal) {
    $efrBean = BeanFactory::retrieveBean('EFR_Equipment_Facility_Recor', $erfval, array('disable_row_level_security' => true));
    $efrName = $efrBean->name;
    $efBean = BeanFactory::retrieveBean('Equip_Equipment', $efVal, array('disable_row_level_security' => true));
    $department_name = $efBean->department_c;
    $efName = $efBean->name;

    //delete the previous email addresses from the array
    unset($ef_emailAdd);
    $ef_emailAdd = array();

    $userEmailsSql = 'SELECT id_c FROM contacts_cstm where equipment_owner_for_dept_c LIKE "%^' . $department_name . '^%"';
    $userEmailsSqlResult = $db->query($userEmailsSql);

    while ($fetchResultEmail = $db->fetchByAssoc($userEmailsSqlResult)) {
      $contactID = $fetchResultEmail['id_c'];
      $Contact_bean = BeanFactory::getBean('Contacts', $contactID);

      $primaryEmailAddress = $Contact_bean->emailAddress->getPrimaryAddress($Contact_bean);
      if ($primaryEmailAddress != false) {
        $ef_emailAdd[] = $primaryEmailAddress;
      }

      //get Manager
      if ($Contact_bean->contact_id_c) {
        $director_id = $Contact_bean->contact_id_c;
        $director_bean = BeanFactory::getBean('Contacts', $director_id);
        if ($director_bean->email1) {
          $director_email = $director_bean->email1;
          $ef_emailAdd[] = $director_email;
        }
      }
    }

    $ef_emailAdd = array_values($ef_emailAdd); //re-index the array elements    
    $ef_emailAdd = array_unique($ef_emailAdd); //remove the repeated email addresses

    //Send Email/
    ///Upload Document Reminder Template 
    $template = new EmailTemplate();
    $emailObj = new Email();

    $template->retrieve_by_string_fields(array('name' => 'Initial In Service - Completed In Service workflow reminder for EF', 'type' => 'email'));

    $efrLink = '<a target="_blank"href="' . $site_url . '/#EFR_Equipment_Facility_Recor/' . $erfval . '">' . $efrName . '</a>';
    $efLink = '<a target="_blank"href="' . $site_url . '/#Equip_Equipment/' . $efVal . '">' . $efName . '</a>';

    $template->body_html = str_replace('[EFR_LINK]', $efrLink, $template->body_html);
    $template->body_html = str_replace('[EF_LINK]', $efLink, $template->body_html);

    $defaults  = $emailObj->getSystemDefaultEmail();
    $mail    = new SugarPHPMailer();
    $mail->setMailerForSystem();
    $mail->IsHTML(true);
    $mail->From    = $defaults['email'];
    $mail->FromName = $defaults['name'];
    $mail->Subject  = "Equipment & Facility In Service Reminder";
    $mail->Body    = $template->body_html;

    $mail->AddAddress('emarkuson@apsemail.com');

    foreach ($ef_emailAdd as $wmail) {
      $mail->AddAddress($wmail);
    }

    //If their exist some valid email addresses then send email
    if (!$mail->Send()) {
      $GLOBALS['log']->fatal("Fail to Send Email, Please check settings 175");
    } else {
      $GLOBALS['log']->fatal('email sent 175 ');
    }
    unset($mail);
  }
  return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/send_analyse_by_date_notification_mail.php

//Add job in job string
$job_strings[] = 'send_analyse_by_date_notification_mail';
/* * This function will send_analyse_by_date_notification_mail from Inventory Item Module. */

function send_analyse_by_date_notification_mail()
{
	global $db;
	
	//Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();
	
	
	$current_date = date("Y-m-d"); //current date
	$current_date2 = date("m/d/Y");
	//Get site url so that appropriate Work Product can be shown in the Email Template
	$site_url = $GLOBALS['sugar_config']['site_url'];
	 

	$queryCustom = "SELECT II.*,IIC.id_c,IIC.analyze_by_date_c,
	IIWP.m03_work_product_ii_inventory_item_1m03_work_product_ida as WP,
	WPC.contact_id_c AS ContactEmailID,emailID.email_address_id,emailAddr.email_address AS emailaddress
	FROM `ii_inventory_item` AS II	
		LEFT JOIN `ii_inventory_item_cstm` AS IIC  ON II.id=IIC.id_c
		LEFT JOIN `m03_work_product_ii_inventory_item_1_c` AS IIWP ON IIWP.m03_work_product_ii_inventory_item_1ii_inventory_item_idb = II.id and IIWP.deleted=0
		LEFT JOIN m03_work_product_cstm AS WPC ON IIWP.m03_work_product_ii_inventory_item_1m03_work_product_ida=WPC.id_c    
		LEFT JOIN `email_addr_bean_rel` AS emailID  ON emailID.bean_id=WPC.contact_id_c
		LEFT JOIN `email_addresses` AS emailAddr  ON emailID.email_address_id=emailAddr.id 
	WHERE (IIC.analyze_by_date_c IS NOT NULL OR IIC.analyze_by_date_c!='') 
						AND IIC.analyze_by_date_c >'now()' AND II.deleted=0 
						AND (IIC.reminder_mail_sent_c IS NULL OR IIC.reminder_mail_sent_c='0')";

	$queryIIResult = $db->query($queryCustom);
	
	 
	while ($fetchIIResult = $db->fetchByAssoc($queryIIResult)) {

		$II_id				= $fetchIIResult['id'];
		$ii_name			= $fetchIIResult['name'];
		$analyzeByDate 		= $fetchIIResult['analyze_by_date_c'];
		$emailAddr 			= $fetchIIResult['emailaddress'];

		// $GLOBALS['log']->fatal('iiName ===>:'.$ii_name."====".$II_id);
		// $GLOBALS['log']->fatal('emailAddr ===>:'.$emailAddr."====".$II_id);
		 
		$date1 = strtotime($analyzeByDate);
		$date2 = strtotime($current_date);
		$diff = abs($date1-$date2);
		$days = floor(($diff)/ (60*60*24)); 

		if((($date1>$date2 && $days<14) || $date1<$date2 || $days==0)){
			//Send Email/
			
			$template->retrieve_by_string_fields(array('name' => 'Reminder Inventory Item Pending Analyze by Date', 'type' => 'email'));
			
			$IILink = '<a target="_blank"href="'.$site_url.'/#II_Inventory_Item/'.$II_id.'">'.$ii_name.'</a>';
					 
			
			$template->body_html = str_replace('[insert_inventory_item_name]', $IILink, $template->body_html);
					
				$defaults	= $emailObj->getSystemDefaultEmail();
				$mail		= new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->IsHTML(true);
				$mail->From		= $defaults['email'];
				$mail->FromName = $defaults['name'];
				$mail->Subject	= "Reminder - Pending Analyze by Date for ".$ii_name;
				$mail->Body		= $template->body_html;
				$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';
					
				$mail->AddAddress('fxs_mjohnson@apsemail.com');
				$mail->AddAddress('sschaefers@apsemail.com');
				// $mail->AddAddress('vsuthar@apsemail.com');
				$mail->AddAddress($emailAddr);
					
				//If their exist some valid email addresses then send email
				if (!empty($wp_emailAdd_dev)) {
					if (!$mail->Send()) {
						$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
					} else {
						$GLOBALS['log']->debug('email sent');
						
						$updateQuery = "UPDATE `ii_inventory_item_cstm` SET `reminder_mail_sent_c`='1' WHERE id_c='".$II_id."'";
						$db->query($updateQuery);
					}
					unset($mail);
				}
		} 
		
	}
	return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/primary_animal_calculation_for_aps001.php

//Add job in job string
$job_strings[] = 'primary_animal_calculation_for_aps001';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function primary_animal_calculation_for_aps001()
{
	global $db;
	$current_date = date("Y-m-d"); //current date
	$entered_date1 = '2020-11-15';


	$WPID[0] = '4a529aae-5165-3149-6db7-5702c7b98a5e';
	$WPID[1] = '811ae58e-9281-2755-25d5-5702c7252696';


	for ($cnt = 0; $cnt < count($WPID); $cnt++) {

		$workProductId 			= $WPID[$cnt];

		$WP_Bean = BeanFactory::retrieveBean('M03_Work_Product', $workProductId);

		$onStudyWithoutTSCount		= 0;
		$onStudyWithTSCount			= 0;
		$totalOnStudyCount			= 0;

		$WPTS 						= array();
		// Query to get On_Study/On_Study Transferred Count


		/*Query to get total number of onStudy/onStudyTransferred Enrollment status Without Test System*/
		$studyWithoutTS_sql = "SELECT  WP_WPE.`m03_work_p9bf5ollment_idb` 
					FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE 
					LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p9bf5ollment_idb`=WPE_CUST.id_c 
					LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p9bf5ollment_idb`=TS.anml_anima9941ollment_idb 
					WHERE WP_WPE.`m03_work_p7d13product_ida`='" . $workProductID . "' AND 
					(WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') AND WP_WPE.deleted=0 AND (TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NULL )";
		//TS.deleted=1 OR TS.deleted IS NULL OR

		$studyWithoutTS_exec = $db->query($studyWithoutTS_sql);

		if ($studyWithoutTS_exec->num_rows > 0) {
			$onStudyWithoutTSCount = $studyWithoutTS_exec->num_rows; //total rows without TS

			while ($fetchWOTS = $db->fetchByAssoc($studyWithoutTS_exec)) {
				$WPTS[]	= $fetchWOTS['m03_work_p9bf5ollment_idb'];
			}
		}

		/*Query to get total number of onStudy/onStudyTransferred Enrollment status With Test System*/
		$studyWithTS_sql = "SELECT  WP_WPE.`m03_work_p9bf5ollment_idb` 
					FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE 
					LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p9bf5ollment_idb`=WPE_CUST.id_c 
					LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p9bf5ollment_idb`=TS.anml_anima9941ollment_idb 
					WHERE WP_WPE.`m03_work_p7d13product_ida`='" . $workProductId . "' 
					AND (WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') AND TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NOT NULL  AND WP_WPE.deleted=0 AND TS.deleted=0 GROUP BY TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida";

		$studyWithTS_exec = $db->query($studyWithTS_sql);

		if ($studyWithTS_exec->num_rows > 0) {
			$onStudyWithTSCount = $studyWithTS_exec->num_rows; //total rows with TS
			while ($fetchWTS = $db->fetchByAssoc($studyWithTS_exec)) {
				$WPTS[]	= $fetchWTS['m03_work_p9bf5ollment_idb'];
			}
		}

		/*=========================================================*/
		/*=========================================================*/

		/*Query to get total number of onStudy/onStudyTransferred Enrollment status Without Test System*/
		$blanket_sql = "SELECT  WP_WPE.`m03_work_p90c4ollment_idb` 
								FROM `m03_work_product_wpe_work_product_enrollment_2_c` AS WP_WPE 
								LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p90c4ollment_idb`=WPE_CUST.id_c 
								LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p90c4ollment_idb`=TS.anml_anima9941ollment_idb 
							WHERE WP_WPE.`m03_work_p9f23product_ida`='" . $workProductId . "' AND 
								(WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') 
								AND WP_WPE.deleted=0 AND (TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NULL )";
		//TS.deleted=1 OR TS.deleted IS NULL OR

		$blanket_exec = $db->query($blanket_sql);

		if ($blanket_exec->num_rows > 0) {
			while ($fetchB1 = $db->fetchByAssoc($blanket_exec)) {
				$WPTS[]	= $fetchB1['m03_work_p90c4ollment_idb'];
			}
		}

		/*Query to get total number of onStudy/onStudyTransferred Enrollment status With Test System*/
		$blanket2_sql = "SELECT  WP_WPE.`m03_work_p90c4ollment_idb` 
									FROM `m03_work_product_wpe_work_product_enrollment_2_c` AS WP_WPE 
										LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p90c4ollment_idb`=WPE_CUST.id_c 
										LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p90c4ollment_idb`=TS.anml_anima9941ollment_idb 
									WHERE WP_WPE.`m03_work_p9f23product_ida`='" . $workProductId . "' 
										AND (WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred')
										AND TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NOT NULL  AND WP_WPE.deleted=0 AND TS.deleted=0 
									GROUP BY TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida";

		$blanket2_exec = $db->query($blanket2_sql);

		if ($blanket2_exec->num_rows > 0) {
			while ($fetchB2 = $db->fetchByAssoc($blanket2_exec)) {
				$WPTS[]	= $fetchB2['m03_work_p90c4ollment_idb'];
			}
		}

		/*=========================================================*/
		/*=========================================================*/
		$totalOnStudyCount  = count(array_unique($WPTS));

		// Get Total value from Bean
		$totalPrimary	= $WP_Bean->total_animals_used_primary_c;

		$totalPrimaryBefore	= $WP_Bean->primary_animals_countdown_c;

		//Calculate countdown Value by subtracting onStudy/Backup value from Total value 
		$Primary_Animals_Countdown	= $totalPrimary - $totalOnStudyCount;


		if ($totalPrimaryBefore != $Primary_Animals_Countdown) {
			$updateQuery = "UPDATE `m03_work_product_cstm` SET `primary_animals_countdown_c`='" . $Primary_Animals_Countdown . "' WHERE `id_c`='" . $workProductId . "' ";
			$db->query($updateQuery);

			$event_id = create_guid();
			$auditsql = 'INSERT INTO m03_work_product_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $workProductId . '","' . $event_id . '",now(),"943a2d38-7bd3-11e9-99fb-06e41dba421a",now(),"total_animals_used_primary_c","int","' . $totalPrimary . '","' . $Primary_Animals_Countdown . '")';
			$auditsqlResult = $db->query($auditsql);

			if ($auditsqlResult) {
				//Inserting audit data in audit_events table
				$source = '{"subject":{"_type":"user","id":"943a2d38-7bd3-11e9-99fb-06e41dba421a","_module":"Users","client":{"_type":"rest-api"}},"attributes":{"platform":"base"}}';
				$sql = "INSERT INTO audit_events(id,type,parent_id,module_name,source,data,date_created) values('" . $event_id . "','update','" . $workProductId . "','M03_Work_Product','" . $source . "',NULL,now())";
				$db->query($sql);
			}
		}
	}


	return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/add_relation_for_missed_communication_record.php

//Add job in job string
$job_strings[] = 'add_relation_for_missed_communication_record';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function add_relation_for_missed_communication_record()
{
	global $db;
	$emailObj = new Email();
	$related_dataArr		= array();
	$related_ErrorDataArr	= array();
	$mailContent 			= "";
	$current_date	= date("Y-m-d"); //current date
	$current_time	= date("Y-m-d H:i:s");
	$startTime 		= strtotime("-10 minutes", strtotime($current_time));
	$start_time		= date("Y-m-d H:i:s", $startTime);

	$sql = "SELECT id FROM `m06_error` AS COMM LEFT JOIN m06_error_cstm AS COMM_CSTM
	ON COMM.id = COMM_CSTM.id_c WHERE deleted=0 AND `date_entered`>='" . $start_time . "' AND `date_entered`<'" . $current_time . "' AND COMM_CSTM.related_text_c IS NOT NULL AND commapp_mismatch_c IS NULL";
	$resultComm = $db->query($sql);
	if ($resultComm->num_rows > 0) {
		while ($rowAllRecord = $db->fetchByAssoc($resultComm)) {
			$commID = $rowAllRecord['id'];

			$commBean   	= BeanFactory::getBean('M06_Error', $commID);
			$commName 		= $commBean->name;
			$commrelated 	= $commBean->related_text_c;
			$workProduct	= $commBean->work_products;

			$related_dataArr		= array();

			$res = str_ireplace(array('\"', '"', '}', '{', '[', ']', '\\', '""'), '', $commrelated);

			$dataArr = explode(",", $res);
			foreach ($dataArr as $resid) {
				$resid = preg_replace('/[^a-zA-Z0-9_ -:]/s', '', $resid);
				$residd = explode(":", $resid);
				$related_dataArr[str_replace(array('quot', '&'), '', $residd[0])] =  str_replace(array('quot', '&'), '', $residd[1]);
			}

			if ($workProduct != "")
			{
				$wp = str_ireplace(array('\"', '[', ']', '\\', 'quot', '&;','"'), '', $workProduct);
				$related_dataArr['M03_Work_Product'] = str_replace(array('quot','&','"'),'',$wp);
			}
			foreach ($related_dataArr as $key => $valueID) {
				if ($valueID != "") {
					if ($key == 'ANML_Animals') {
						$TSBean  = BeanFactory::getBean('ANML_Animals', $valueID);

						if ($TSBean->name != "") {
							$checkQuery = "SELECT * FROM m06_error_anml_animals_1_c where deleted=0 AND m06_error_anml_animals_1m06_error_ida='" . $commID . "' and  m06_error_anml_animals_1anml_animals_idb='" . $valueID . "'";
							$resultTestSystem = $db->query($checkQuery);
							if ($resultTestSystem->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_anml_animals_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
							//echo "<br>==>Animal Not available";
						}
					}

					if ($key == 'M03_Work_Product') {

						$WPBean = BeanFactory::getBean('M03_Work_Product', $valueID);
						if ($WPBean->name != "") {
							$checkQuery = "SELECT * FROM `m06_error_m03_work_product_1_c` where deleted=0 AND m06_error_m03_work_product_1m06_error_ida='" . $commID . "' and  m06_error_m03_work_product_1m03_work_product_idb='" . $valueID . "'";
							$resultTestSystem = $db->query($checkQuery);
							if ($resultTestSystem->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_m03_work_product_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
						}
					}


					if ($key == 'Erd_Error_Documents') {
						$ERDBean = BeanFactory::getBean('Erd_Error_Documents', $valueID);
						if ($ERDBean->name != "") {
							$checkQuery = "SELECT * FROM `m06_error_erd_error_documents_1_c` where deleted=0 AND  m06_error_erd_error_documents_1m06_error_ida='" . $commID . "' and  m06_error_erd_error_documents_1erd_error_documents_idb='" . $valueID . "'";
							$resultEDoc = $db->query($checkQuery);
							if ($resultEDoc->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_erd_error_documents_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
						}
					}

					if ($key == 'Equip_Equipment') {
						$EFBean = BeanFactory::getBean('Equip_Equipment', $valueID);
						if ($EFBean->name != "") {
							$checkQuery = "SELECT * FROM `m06_error_equip_equipment_1_c` where deleted=0 AND  m06_error_equip_equipment_1m06_error_ida='" . $commID . "' and  m06_error_equip_equipment_1equip_equipment_idb='" . $valueID . "'";
							$resultEquip = $db->query($checkQuery);
							if ($resultEquip->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_equip_equipment_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
						}
					}

					if ($key == 'RMS_Room') {
						$RMSBean = BeanFactory::getBean('RMS_Room', $valueID);
						if ($RMSBean->name != "") {
							$checkQuery = "SELECT * FROM `m06_error_rms_room_1_c` where deleted=0 AND  m06_error_rms_room_1m06_error_ida='" . $commID . "' and  m06_error_rms_room_1rms_room_idb='" . $valueID . "'";
							$resultRoom = $db->query($checkQuery);
							if ($resultRoom->num_rows == 0) {
								// create Relationship
								$addRelationship['m06_error_rms_room_1'] =  $valueID;
							}
						} else {
							$related_ErrorDataArr[$key] = $valueID;
						}
					}
				}
			}

			$recordContent = "";

		if (count($addRelationship) > 0) {
			$mailContent .= "<br><br>For Communication ".$commName.", Missing records for relationships<br>";
			$recordContent .= "For Communication ".$commName.", Missing records for relationships \n";
			foreach ($addRelationship as $moduleRel => $recordID) {
				$mailContent .= $moduleRel . "==" . $recordID . "</br>";
				$recordContent .= $moduleRel . "==" . $recordID." \n";
				//$commBean->load_relationship($moduleRel);
				//$commBean->$moduleRel->add($recordID);
				//$commBean->save();
			}
		}

		if (count($related_ErrorDataArr) > 0) {
			$mailContent .= "<br><br>For Communication ".$commName.",Following records didnot find in System :<br>";
			$recordContent .= "For Communication ".$commName.",Following records didnot find in System :  \n";
			foreach ($related_ErrorDataArr as $moduleRel => $recordID) {
				$mailContent .= $moduleRel . "==" . $recordID . "</br>";
				$recordContent .= $moduleRel . "==" . $recordID." \n";
			}
		}

		if($recordContent!=""){
			$updateSql = "UPDATE `m06_error_cstm` SET `commapp_mismatch_c` = 'Cron Status : ".$recordContent."' WHERE `id_c` = '" . $commID . "' ";
			$db->query($updateSql);
		}else{
			$updateSql = "UPDATE `m06_error_cstm` SET `commapp_mismatch_c` = 'Relationship not required' WHERE `id_c` = '" . $commID . "' ";
			$db->query($updateSql);
		}
		}
	}

	if ($mailContent != "") {		
		$defaults = $emailObj->getSystemDefaultEmail();
		$mail = new SugarPHPMailer();
		$mail->setMailerForSystem();
		$mail->IsHTML(true);
		$mail->From = $defaults['email'];
		$mail->FromName = $defaults['name'];
		$mail->Subject = "Missed Relationship from Comm App";
		$mail->Body = $mailContent;

		$mail->AddCC('vsuthar@apsemail.com');
		$emailAdd[] = 'vsuthar@apsemail.com';
		//If their exist some valid email addresses then send email
		if (!empty($emailAdd)) {
			if (!$mail->Send()) {
				$GLOBALS['log']->fatal("Fail to sent mail for Missed Relationship from Comm App");
			} else {
				$GLOBALS['log']->fatal('Successfully mail sent for Missed Relationship from Comm App');
			}
			unset($mail);
		}
	}

	return true;
}
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/get_days_to_expiration_date.php

//Add job in job string
$job_strings[] = 'get_days_to_expiration_date';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function get_days_to_expiration_date()
{
	global $db;
	$current_date	= date("Y-m-d"); //current date
	
	$ii_sql = "SELECT id,expiration_date,expiration_date_time,days_to_expiration 
			FROM `ii_inventory_item` 
			WHERE deleted=0 AND expiration_date >='".$current_date."' OR expiration_date_time >='".$current_date."'";
	$resultII = $db->query($ii_sql);
	if ($resultII->num_rows > 0) {
		while ($rowII = $db->fetchByAssoc($resultII)) {
			$id 				= $rowII['id'];
			$expirationDate		= $rowII['expiration_date'];
			$expirationDateTime = $rowII['expiration_date_time'];

			$checkDate = ($expirationDate!="")?$expirationDate:$expirationDateTime;

			if($checkDate!=""){				
				$expDatetime	= date('Y-m-d 0:0:0',strtotime($checkDate)); 
				$currentTime	= date('Y-m-d 0:0:0', time()); 
				$date1			= strtotime($expDatetime);  
				$date2			= strtotime($currentTime); 
				// Formulate the Difference between two dates 
				$diff = abs($date1 - $date2); /*Get Time Difference*/
				$days = floor(($diff)/ (60*60*24));	
				
				if($date1 > $date2){
					$no_of_days = $days;
				}else{
					$no_of_days = '0';
				}
				$iiBean   	= BeanFactory::getBean('II_Inventory_Item', $id);
				$iiBean->days_to_expiration    	 = $no_of_days;
				$iiBean->save();
				$iiUpdate = "UPDATE `ii_inventory_item_audit` SET created_by='943a2d38-7bd3-11e9-99fb-06e41dba421a' WHERE `field_name`='days_to_expiration' AND `parent_id`='".$id."' AND `date_created` >='".$current_date."'";
				$db->query($iiUpdate); //943a2d38-7bd3-11e9-99fb-06e41dba421a
			}
		}
	}
	return true;
}
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/wpd_upload_document_reminder.php

//Add job in job string
$job_strings[] = 'wpd_upload_document_reminder';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function wpd_upload_document_reminder() {
    global $db;
    $current_date = date("Y-m-d"); //current date
	 
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];
	$domainToSearch = 'apsemail.com';
 

    $queryCustom = "SELECT  WPD.id AS wpd_id,
							WPD.name AS wpd_name,
							WPD.date_entered,
							WPDCSTM.deliverable_c, 
							WPDCSTM.due_date_c, 
							WP_WPD.m03_work_p0b66product_ida AS workProductID,
							WP.name AS workProductName,
							WP.assigned_user_id AS Assigneduser,
							WPCSTM.contact_id_c AS StudyDirector,
							WPCSTM.first_procedure_c  
					FROM `m03_work_product_deliverable` AS WPD
						LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
							ON WPD.id= WPDCSTM.id_c
						RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
							ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
						LEFT JOIN m03_work_product AS WP
							ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
						LEFT JOIN m03_work_product_cstm AS WPCSTM
							ON WP.id=WPCSTM.id_c
							WHERE (((((WPDCSTM.deliverable_c = 'SpecimenSample Disposition'
									))) AND (((WPDCSTM.type_2_c IN ('Paraffin Blocks','Slide')
									) OR WPD.filename IS NULL OR WPDCSTM.attn_to_name_c IS NULL OR WPDCSTM.address_street_c IS NULL OR WPDCSTM.address_city_c IS NULL OR WPDCSTM.address_state_c IS NULL OR WPDCSTM.address_postalcode_c IS NULL OR WPDCSTM.address_country_c IS NULL OR WPDCSTM.phone_number_c IS NULL OR WPDCSTM.address_confirmed_c IS NULL OR WPDCSTM.receiving_party_expecting_c IS NULL OR WPDCSTM.faxitron_required_c IS NULL
									)) AND (((WPDCSTM.type_2_c IN ('Paraffin Blocks','Slide')
									) OR WPDCSTM.attn_to_name_c IS NULL OR WPDCSTM.address_street_c IS NULL OR WPDCSTM.address_city_c IS NULL OR WPDCSTM.address_state_c IS NULL OR WPDCSTM.address_postalcode_c IS NULL OR WPDCSTM.address_country_c IS NULL OR WPDCSTM.phone_number_c IS NULL OR WPDCSTM.address_confirmed_c IS NULL OR WPDCSTM.receiving_party_expecting_c IS NULL OR WPDCSTM.faxitron_required_c IS NULL
									))))
						AND WPDCSTM.due_date_c>'" . $current_date . "'
						AND WPD.date_entered>'2021-10-21'
						AND WPD.deleted = 0 
					ORDER BY WPD.date_entered ASC";

    $queryWPDResult = $db->query($queryCustom);
	
	
	 while($fetchResult = $db->fetchByAssoc($queryWPDResult)) {

        $wpd_id			= $fetchResult['wpd_id'];
		$wpdName		= $fetchResult['wpd_name'];
		$workProductID	= $fetchResult['workProductID'];
		$workProductName	= $fetchResult['workProductName'];
		$assignedUser_id	= $fetchResult['Assigneduser']; 
        $SD_ID				= $fetchResult['StudyDirector'];
		$dueDate		= $fetchResult['due_date_c']; 
		
		
        if (($SD_ID != "" || $assignedUser_id!="" ) && $workProductID != "" && $wpd_id != "" && $dueDate!="" ) {
			
			$UserName 			= "";
			$UserAddress	 	= "";
			$studyDirectorName	= "";
			$SDEmailAddress 	= "";
			
			/*Get Assigned User*/
			if($assignedUser_id!=""){
				$user_bean		= BeanFactory::getBean('Employees', $assignedUser_id);
				$UserName		= $user_bean->name;
				$UserAddress	= $user_bean->emailAddress->getPrimaryAddress($user_bean);
			}	
			
			/*Get Study Director*/
			if($SD_ID!=""){
				$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
			}	
			
			
			$date1= strtotime($dueDate);
			$date2= strtotime($current_date);

			$diff = abs($date1-$date2);
			$days = floor(($diff)/ (60*60*24)); 
			$wp_emailAdd_dev = array();
				
			if(($date1>$date2 && $days==14)){
					//Send Email/
					///Upload Document Reminder Template 
					$template = new EmailTemplate();
					$emailObj = new Email();
					
					$template->retrieve_by_string_fields(array('name' => 'Upload Document Reminder Template', 'type' => 'email'));
					
					$wpdLink = '<a target="_blank"href="'.$site_url.'/#M03_Work_Product_Deliverable/'.$wpd_id.'">'.$wpdName.'</a>';
					 
					$WP_MGT = ($UserName!="")?$UserName:$studyDirectorName;
					$template->body_html = str_replace('[WP_MGT]', $WP_MGT, $template->body_html);
					$template->body_html = str_replace('[WPD_ID]', $wpdLink, $template->body_html);
					
					$defaults	= $emailObj->getSystemDefaultEmail();
					$mail		= new SugarPHPMailer();
					$mail->setMailerForSystem();
					$mail->IsHTML(true);
					$mail->From		= $defaults['email'];
					$mail->FromName = $defaults['name'];
					$mail->Subject	= "Reminder - Complete fields on ".$wpdName;
					$mail->Body		= $template->body_html;
							 
					 
					$wp_emailAdd[] = 'operationssupport@apsemail.com';
					
					if(!empty($UserAddress))
						$mail->AddAddress($UserAddress);
					if(!empty($SDEmailAddress))
						$mail->AddAddress($SDEmailAddress);
					$mail->AddAddress('operationssupport@apsemail.com');
					//$mail->AddAddress('vsuthar@apsemail.com');
					
					 
					//If their exist some valid email addresses then send email
					if (!empty($wp_emailAdd)) {
						if (!$mail->Send()) {
							$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
						} else {
							$GLOBALS['log']->debug('email sent');
						}
						unset($mail);
					}
				} 	
		}
	}
	
	
	 return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/send_email_for_cpi_report.php

//Add job in job string
$job_strings[] = 'send_email_for_cpi_report';

function send_email_for_cpi_report()
{
	global $db;
	$current_date	= date("Y-m-d"); //current date 
	$fileDate		= date("jMY", strtotime("-1 day", strtotime($current_date)));
	$yesterdaydate	= date("Y-m-d", strtotime("-1 days", strtotime($current_date)));
	$current_date  	= $current_date .' 05:59:59';
	$yesterdaydate 	= $yesterdaydate .' 06:00:00';
	 
	/**/
	$reportSQL = "SELECT IFNULL(a1a_critical_phase_inspectio.id,'') primaryid
	,IFNULL(a1a_critical_phase_inspectio.name,'') A1A_CRITICAL_PHASE_INSF6098C
	,IFNULL(a1a_critical_phase_inspectio_cstm.inspection_results_c,'') A1A_CRITICAL_PHASE_INSBF63F7,IFNULL(l1.id,'') l1_id
	,IFNULL(l1.name,'') l1_name
	,l1_cstm.contact_id_c l1_cstm_contact_id_c,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contacts1_name
	FROM a1a_critical_phase_inspectio
	 INNER JOIN  m03_work_product_a1a_critical_phase_inspectio_1_c l1_1 ON a1a_critical_phase_inspectio.id=l1_1.m03_work_p934fspectio_idb AND l1_1.deleted=0
	 INNER JOIN  m03_work_product l1 ON l1.id=l1_1.m03_work_p33edproduct_ida AND l1.deleted=0
	LEFT JOIN m03_work_product_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN contacts contacts1 ON contacts1.id = l1_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
	LEFT JOIN a1a_critical_phase_inspectio_cstm a1a_critical_phase_inspectio_cstm ON a1a_critical_phase_inspectio.id = a1a_critical_phase_inspectio_cstm.id_c
	WHERE (((a1a_critical_phase_inspectio.date_entered >= '" . $yesterdaydate . "' AND a1a_critical_phase_inspectio.date_entered <= '" . $current_date . "'
	)))  	AND  a1a_critical_phase_inspectio.deleted=0 
	 AND IFNULL(contacts1.deleted,0)=0 
	 ORDER BY l1_cstm_contact_id_c ASC";

	$reportResult	= $db->query($reportSQL);

	if ($reportResult->num_rows > 0) {
		$template		= new EmailTemplate();
		$emailObj		= new Email();
		$wpSD_docEmailArr 	= array();
		$wpSD_Email 	= array();
		$cpiArray	 	= array();
		$$docArr	 	= array();
		$site_url		= $GLOBALS['sugar_config']['site_url'];
		$template->retrieve_by_string_fields(array('name' => 'Critical Phase Inspections for Review', 'type' => 'email'));

		//Email Body Header
		$emailBody = "<table width='100%' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 13px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>Name</th><th bgcolor='#b3d1ff' align='left'>Inspection Results</th><th bgcolor='#b3d1ff' align='left'>APS Work Product ID</th><th bgcolor='#b3d1ff' align='left'>Study Director</th></tr>";
		 
		
		 
		$cntr =0;
		while ($fetchReportResult = $db->fetchByAssoc($reportResult)) {
			$CPIId					= $fetchReportResult['primaryid'];
			$cpiArray[$cntr]		= $CPIId;
			$CPIname				= $fetchReportResult['A1A_CRITICAL_PHASE_INSF6098C'];
			$inspection_results		= $fetchReportResult['A1A_CRITICAL_PHASE_INSBF63F7'];
			$wpID					= $fetchReportResult['l1_id'];
			$wpName					= $fetchReportResult['l1_name'];
			$SDname					= $fetchReportResult['contacts1_name'];
			$SDID					= $fetchReportResult['l1_cstm_contact_id_c'];
			
			$docArr[$cntr]['cpiName']		=  $CPIname;
			$docArr[$cntr]['cpiID']			=  $CPIId;
			$docArr[$cntr]['cpiResult']  	=  $inspection_results;
			$docArr[$cntr]['wpID']  		=  $wpID;
			$docArr[$cntr]['wpName']  		=  $wpName;
			$docArr[$cntr]['sdName']  		=  $SDname;
			$docArr[$cntr]['sdID']  		=  $SDID;

			$sdBean			= BeanFactory::getBean('Contacts', $SDID);
			$account_name	= $sdBean->account_name;
			
			$primaryEmailAddress = $sdBean->emailAddress->getPrimaryAddress($sdBean);
			if ($primaryEmailAddress != false) {
				if (checkEmailForNotification($primaryEmailAddress)) {
					$wpSD_Email[] = $primaryEmailAddress;
					$wpSD_docEmailArr[] = $sdBean->first_name." ".$sdBean->last_name . "(" . $primaryEmailAddress . ")";
				}
			}

			$CPILink	= '<a target="_blank" href="'. $site_url . '/#A1A_Critical_Phase_Inspectio/' . $CPIId . '" >' . $CPIname . '</a>';
			$wpLink		= '<a target="_blank" href="' . $site_url . '/#M03_Work_Product/' . $wpID . '" >' . $wpName . '</a>';
			$sdLink		= '<a target="_blank" href="' . $site_url . '/#Contacts/' . $SDID . '" >' . $SDname . '</a>';

			$emailBody .= "<tr><td>" . $CPILink . "</td><td>" . $inspection_results . "</td><td>" . $wpLink . "</td><td>" . $sdLink . "</td></tr>";
			$cntr++;
		}

		$wpSD_Email			= array_values($wpSD_Email); //re-index the array elements
		$wpSD_Email			= array_unique($wpSD_Email);
		
		$emailBody		.= "</table>";
		 

		$template->body_html = str_replace('[Report_Table]', $emailBody, $template->body_html);

		$defaults		= $emailObj->getSystemDefaultEmail();
		$mail			= new SugarPHPMailer();
		$mail->setMailerForSystem();
		$mail->IsHTML(true);
		$mail->From		= $defaults['email'];
		$mail->FromName = $defaults['name'];
		$mail->Subject	= $template->subject;
		$mail->Body		= $template->body_html;

		foreach ($wpSD_Email as $wmail) {
			$mail->AddAddress($wmail); 
		}
		
		

		$mail->AddAddress('esteinmetz@apsemail.com');
		$mail->AddAddress('edrake@apsemail.com');
		$mail->AddAddress('jpomonis@apsemail.com');
		$mail->AddAddress('emarkuson@apsemail.com');
		//$mail->AddAddress('fxs_mjohnson@apsemail.com');
		$mail->AddBCC('vsuthar@apsemail.com');
		//If their exist some valid email addresses then send email

		if (!$mail->Send()) {
			$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
		} else {
			$GLOBALS['log']->debug('email sent');
		}
		unset($mail);


		$eDocNewName = "Critical Phase Inspections for Review " . $fileDate . ".pdf";
		/*Save Document after sending email if Work product compliance is GLP*/ {
			$emailDocId  = "";
			$edoc_sql = "SELECT `id`,`type_2`,`document_name` FROM `edoc_email_documents` AS EDOC 
									 WHERE EDOC.`document_name` LIKE '" . $eDocNewName . "' AND deleted=0 ORDER BY `EDOC`.`date_entered` ASC";
			$edoc_exec = $db->query($edoc_sql);
			while ($resultEdoc = $db->fetchByAssoc($edoc_exec)) {
				$type_2		= $resultEdoc['type_2'];
				$emailDocId  = $resultEdoc['id'];
			}

			if ($emailDocId == "") {
				//Create bean
				$emailDocBean = BeanFactory::newBean('EDoc_Email_Documents');
				$emailDocBean->document_name	= $eDocNewName;
				$emailDocBean->department		= 'Quality Assurance Unit';
				$emailDocBean->type_2			= "Other";
				$emailDocBean->assigned_user_id	= $current_user->id;

				$emailDocBean->filename	= $eDocNewName;
				$emailDocBean->file_ext	= 'pdf';
				$emailDocBean->file_mime_type	= 'application/pdf';

				$emailDocBean->save();
				$emailDocId = $emailDocBean->id;

				if (count($cpiArray) > 0) {
					foreach ($cpiArray as $cpi) {
						$gid = create_guid(); //strtolower($GUID);
						$sql_msb = "INSERT INTO `edoc_email_documents_a1a_critical_phase_inspectio_1_c` (id,edoc_emailbf38cuments_ida, edoc_emailc542spectio_idb) VALUES ('" . $gid . "','" . $emailDocId . "', '" . $cpi . "')";
						$db->query($sql_msb);
					}
				}
			}

			$ss				= new Sugar_Smarty();
			$ss->security	= true;
			$ss->security_settings['PHP_TAGS'] = false;
			if (defined('SUGAR_SHADOW_PATH')) {
				$ss->secure_dir[] = SUGAR_SHADOW_PATH;
			}
			$ss->assign('MOD', $GLOBALS['mod_strings']);
			$ss->assign('APP', $GLOBALS['app_strings']);
			$ss->assign('docArr', $docArr);
			$html2 = $ss->fetch('custom/modules/A1A_Critical_Phase_Inspectio/tpls/cpi_review_email.tpl');
			
			//top part
			
			$top_html = "";  
			
			$offset		=  -18000;
			//$offset	=  -21600; //daylight Saving
			$offset_time	= time() + $offset +1;
			$formattedDate	= date("D m/d/Y g:i a",$offset_time);

			
			
			
			$wpSD_docEmailArr[] =  "Erik Steinmetz(esteinmetz@apsemail.com)";
			$wpSD_docEmailArr[] =  "Emily Drake(edrake@apsemail.com)";
			$wpSD_docEmailArr[] =  "Jim Pomonis(jpomonis@apsemail.com)";
			$wpSD_docEmailArr[] =  "Emily Markuson(emarkuson@apsemail.com)";
			//$wpSD_docEmailArr[] =  "Madeline Johnson(fxs_mjohnson@apsemail.com)";
			
			$wpSD_docEmailArr	= array_values($wpSD_docEmailArr); //re-index the array elements
			$wpSD_docEmailArr	= array_unique($wpSD_docEmailArr);
			//$wpSD_Email[] =  "Vinod Suthar(vsuthar@apsemail.com)";
			
			$top_html = '<span style="font-size:8pt;">'.$formattedDate.'</span>';
			$top_html .= '<br/><span style="font-size:8pt;">From: PMT (do_not_reply@apsemail.com)</span>';
			$top_html .= '<br/><span style="font-size:8pt;">Subject: '.$template->subject.'</span>';
			$top_html .= '<br/><span style="font-size:8pt;">To: '.implode(",", $wpSD_docEmailArr).'</span>';
							
			$borderHtml = '<p><span style="font-size:10pt;">----------------------------------------------------------------------------------------------------------------------------------------------------------------</span></p><p><span style="font-size:12pt;">Hello</span></p><p><span style="font-size:12pt;">Please review yesterday\'s Critical Phase Inspection records below.</span></p>';
			$body_html_new = $top_html.$borderHtml.$html2;
			////////////////////


			$fileName		= $eDocNewName;
			$fileDocID		= $emailDocId;
			$fileInfo		= create_PDF($body_html_new, $fileName);
			$objFile		= new UploadFile('uploadfile');
			// Set the filename
			$objFile->file_ext	= 'pdf';
			$objFile->set_for_soap($fileName, (sugar_file_get_contents($fileInfo['file_path']))); 
			$objFile->create_stored_filename();
			$objFile->final_move($fileDocID);
		}
		/*ENd Email document creation*/
	}

	return true;
}

function create_PDF($html, $name)
{
	require_once('vendor/tcpdf/tcpdf.php');
	$tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$tcpdf->SetCreator(PDF_CREATOR);
	$name = str_replace(' ', '_', $name);
	$date = date('Y-m-d');
	$file_info['file_path'] = "upload/" . $name;
	$file_info['file_name'] = $name;

	$tcpdf->AddPage();
	$tcpdf->writeHTML($html, true, false, true, false, '');
	$tcpdf->Output($file_info['file_path'], "F");
	return $file_info;
}

function checkEmailForNotification($email) {
    $domainToSearch = 'apsemail.com';
    $domain_name = substr(strrchr($email, "@"), 1);
    if (trim(strtolower($domain_name) == $domainToSearch)) {
        return true;
    }
    return false;
}



?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/create_sc_species_census_record.php

//Add job in job string
$job_strings[] = 'create_sc_species_census_record';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function create_sc_species_census_record()
{
	global $db;
	$current_date	= date("Y-m-d"); //current date
	$current_date2	= date("m/d/Y");
	$current_date3	= date("m-d-Y");
	$current_date4	= date("Y-m-d");
	$current_year	= date("Y");
	
	$previous_week = strtotime("-1 week");
	$lastWeek_date	= date("Y-m-d",$previous_week); //lastWeek_date
	//Get site url so that appropriate Work Product can be shown in the Email Template
	$site_url = $GLOBALS['sugar_config']['site_url'];
	 

	$queryCustom = "SELECT spec.id AS speciesID, spec.name AS speciesName FROM `s_species` AS spec WHERE deleted=0";

	$querySpecResult = $db->query($queryCustom);

	while ($fetchSpecResult = $db->fetchByAssoc($querySpecResult)) {

		$speciesName		= $fetchSpecResult['speciesName'];
		$speciesIDa		    = $fetchSpecResult['speciesID'];
		 
		
		$module					= 'SC_Species_Census';
		$SC_bean				= BeanFactory::newBean($module);
		$SC_bean->date_2		= $current_date;
		$SC_bean->name			= $speciesName . ' Census ' . $current_date2;
		
		$SC_bean->s_species_sc_species_census_1_name    	 = $speciesName;
        $SC_bean->s_species_sc_species_census_1s_species_ida = $speciesIDa;
		$SC_bean->created_by    	  = '943a2d38-7bd3-11e9-99fb-06e41dba421a';
		$SC_bean->assigned_user_id    = '943a2d38-7bd3-11e9-99fb-06e41dba421a';
		$SC_bean->modified_user_id    = '943a2d38-7bd3-11e9-99fb-06e41dba421a';
		$SC_bean->save();
		


		
		$SC_bean->load_relationship("s_species_sc_species_census_1");
		$SC_bean->s_species_sc_species_census_1->add($speciesIDa);
	}
	
	$laAssignName[0] =  'Bovine Census '.$current_date2;
	$laAssignName[1] =  'Canine Census '.$current_date2;
	$laAssignName[2] =  'Caprine Census '.$current_date2;
	$laAssignName[3] =  'Ovine Census '.$current_date2;
	$laAssignName[4] =  'Porcine Census '.$current_date2;
	$laAssignName[5] =  'Bovine Census '.$current_date3;
	$laAssignName[6] =  'Canine Census '.$current_date3;
	$laAssignName[7] =  'Caprine Census '.$current_date3;
	$laAssignName[8] =  'Ovine Census '.$current_date3;
	$laAssignName[9] =  'Porcine Census '.$current_date3;
	$laAssignName[10] = 'Bovine Census '.$current_date4;
	$laAssignName[11] = 'Canine Census '.$current_date4;
	$laAssignName[12] = 'Caprine Census '.$current_date4;
	$laAssignName[13] = 'Ovine Census '.$current_date4;
	$laAssignName[14] = 'Porcine Census '.$current_date4;
	
	$laAssigned 		= implode("','",$laAssignName);	
	$laCalculation 		= 0;
	$queryCustomLaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species laCalculation Query == '.$queryCustomLaAssigned);
	$queryLAResult = $db->query($queryCustomLaAssigned);
	if ($queryLAResult->num_rows > 0) 
    {
		 $resultLA 			= $db->fetchByAssoc($queryLAResult);
		 $totalAllocated	= $resultLA['totalAllocated'];
		 $totalOnStudy		= $resultLA['totalOnStudy'];
		 $totalValue		= $resultLA['totalValue'];
		 
		 $laCalculation = (($totalAllocated+$totalOnStudy)/$totalValue)*100;
		 //$GLOBALS['log']->fatal('species laCalculation  == '.$laCalculation.'==='.$totalAllocated.'===='.$totalOnStudy.'===='.$totalStock);
	}
	
	$queryCustomLaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$laAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$queryLAIDResult = $db->query($queryCustomLaAssignedID);
	if ($queryLAIDResult->num_rows > 0) 
    {
		 while($resultLAID 			= $db->fetchByAssoc($queryLAIDResult)){
			$GLOBALS['log']->fatal('i am in the loop  == '.$resultLAID['id']); 
			$beanID		= $resultLAID['id'];
			$SC_Bean	= BeanFactory::getBean('SC_Species_Census', $beanID);
			$SC_Bean->la_percent_assigned_2_c	= $laCalculation;
			//$GLOBALS['log']->fatal('updatespecies '.$SC_Bean->name.' laCalculation  ==> '.$laCalculation);	
			$SC_Bean->save();
		}		  
	}
	
	/**/
	$saAssignName[0] =  'Lagomorph Census '.$current_date2;
	$saAssignName[1] =  'Rat Census '.$current_date2;
	$saAssignName[2] =  'Murine Census '.$current_date2;
	$saAssignName[3] =  'Hamster Census '.$current_date2;
	$saAssignName[4] =  'Guinea Pig Census '.$current_date2;
	$saAssignName[5] =  'GuineaPg Census '.$current_date2;
	$saAssignName[6] =  'Lagomorph Census '.$current_date3;
	$saAssignName[7] =  'Rat Census '.$current_date3;
	$saAssignName[8] =  'Murine Census '.$current_date3;
	$saAssignName[9] =  'Hamster Census '.$current_date3;
	$saAssignName[10] =  'Guinea Pig Census '.$current_date3;
	$saAssignName[11] =  'GuineaPg Census '.$current_date3;
	$saAssignName[12] =  'Lagomorph Census '.$current_date4;
	$saAssignName[13] =  'Rat Census '.$current_date4;
	$saAssignName[14] =  'Murine Census '.$current_date4;
	$saAssignName[15] =  'Hamster Census '.$current_date4;
	$saAssignName[16] =  'Guinea Pig Census '.$current_date4;
	$saAssignName[17] =  'GuineaPg Census '.$current_date4;
	
	$saCalculation = 0;
	$saAssigned = implode("','",$saAssignName);
	$queryCustomSaAssigned = "SELECT  SC.id,SC.name,SUM(SC.allocated) AS totalAllocated,SUM(SC.on_study) AS totalOnStudy,SUM(SC.stock) AS totalStock,SUM(SC.total) AS totalValue FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0";
	//$GLOBALS['log']->fatal('species saCalculation Query == '.$queryCustomSaAssigned);
	$querySAResult = $db->query($queryCustomSaAssigned);
	if ($querySAResult->num_rows > 0) 
    {
		 $resultSA 			= $db->fetchByAssoc($querySAResult);
		 $totalSA_Allocated	= $resultSA['totalAllocated'];
		 $totalSA_OnStudy	= $resultSA['totalOnStudy'];
		 $totalSA_Value		= $resultSA['totalValue'];
		 $saCalculation = (($totalSA_Allocated+$totalSA_OnStudy)/$totalSA_Value)*100;
		 //$GLOBALS['log']->fatal('species saCalculation == '.$saCalculation.'==='.$totalSA_Allocated.'===='.$totalSA_OnStudy.'===='.$totalSA_Stock);	
	}
	
	$querySaAssignedID = "SELECT  SC.id FROM `sc_species_census` AS SC LEFT JOIN `sc_species_census_cstm` AS SCC ON SC.id=SCC.id_c WHERE SC.name IN ('".$saAssigned."') AND SC.deleted=0 GROUP BY SC.name";
	$querySAIDResult = $db->query($querySaAssignedID);
	if ($querySAIDResult->num_rows > 0) 
    {
		 while($resultSAID 			= $db->fetchByAssoc($querySAIDResult)){
			 
			$beanSAID		= $resultSAID['id'];
			$SC_SABean	= BeanFactory::getBean('SC_Species_Census', $beanSAID);
			$SC_SABean->sa_percent_assigned_2_c	= $saCalculation;
			//$GLOBALS['log']->fatal('===Newspecies '.$SC_SABean->name.' saCalculation  ==> '.$saCalculation);	
			$SC_SABean->save();
		}		  
	}
	
	return true;
}
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/overdue_to_qa_email_workflow.php

//Add job in job string
$job_strings[] = 'overdue_to_qa_email_workflow';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function overdue_to_qa_email_workflow() {
	global $db;
    $current_date = date("Y-m-d"); //current date
	 
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url		= $GLOBALS['sugar_config']['site_url'];
	$domainToSearch = 'apsemail.com';
	$emailBody 		= "";

    $queryCustom = "SELECT  WPD.id AS wpd_id,
							WPD.name AS wpd_name,
							WPD.date_entered,
							WPDCSTM.deliverable_c, 
							WPDCSTM.due_date_c, 
							WPDCSTM.final_due_date_c, 
							WP_WPD.m03_work_p0b66product_ida AS workProductID,
							WP.name AS workProductName,
							WP.assigned_user_id AS Assigneduser,
							WPCSTM.contact_id_c AS StudyDirector,
							WPCSTM.first_procedure_c,
							WPCSTM.last_procedure_c,
						CONCAT(SDContact.first_name,' ', SDContact.last_name) AS StudyDirectorName							
					FROM `m03_work_product_deliverable` AS WPD
						LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
							ON WPD.id= WPDCSTM.id_c
						RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
							ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
						LEFT JOIN m03_work_product AS WP
							ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
						LEFT JOIN m03_work_product_cstm AS WPCSTM
							ON WP.id=WPCSTM.id_c
						LEFT JOIN contacts AS SDContact
						ON SDContact.id=WPCSTM.contact_id_c
					WHERE  WPCSTM.functional_area_c =  'Standard Biocompatibility' 
					AND WPCSTM.work_product_compliance_c   =  'GLP'
						AND WPDCSTM.deliverable_c   =  'Final Report'
						AND WPDCSTM.due_date_c		<= '".$current_date."'
						AND WPDCSTM.draft_deliverable_sent_date_c IS NULL
						AND WPDCSTM.deliverable_status_c !='Completed'
						AND WPDCSTM.deliverable_status_c !='Not Performed'
						AND WPDCSTM.deliverable_status_c !='None'
						AND WPDCSTM.deliverable_status_c !='Completed Account on Hold'
						AND WPDCSTM.deliverable_status_c !='Sponsor Retracted' 
						AND WPD.date_entered>'2021-09-19'
						AND WPD.deleted = 0 
					ORDER BY StudyDirectorName ASC";

    $queryWPDResult = $db->query($queryCustom);
	
	
	 while($fetchResult = $db->fetchByAssoc($queryWPDResult)) {

        $wpd_id				= $fetchResult['wpd_id'];
		$wpdName			= $fetchResult['wpd_name'];
		$workProductID		= $fetchResult['workProductID'];
		$workProductName	= $fetchResult['workProductName'];
		$assignedUser_id	= $fetchResult['Assigneduser']; 
        $SD_ID				= $fetchResult['StudyDirector'];
        $SD_Name				= $fetchResult['StudyDirectorName'];
		
		$dueDate			= "";
		if($fetchResult['due_date_c']!="")
			$dueDate			= date("m-d-Y", strtotime($fetchResult['due_date_c']));
		
		$lastProcedureDate			= "";
		if($fetchResult['last_procedure_c']!="")
			$lastProcedureDate			= date("m-d-Y", strtotime($fetchResult['last_procedure_c']));
		
		$finalDueDate			= "";
		if($fetchResult['final_due_date_c']!="")
			$finalDueDate			= date("m-d-Y", strtotime($fetchResult['final_due_date_c']));
		
		
		
        if ($workProductID != "" && $wpd_id != "" && $dueDate!="" ) {
			
			$UserName 			= "";
			$UserAddress	 	= "";
			$studyDirectorName	= "";
			$SDEmailAddress 	= "";
			
			/*Get Study Director*/
			if($SD_ID!=""){
				$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
				
				$domain_name = substr(strrchr($SDEmailAddress, "@"), 1);
				if (trim(strtolower($domain_name) != $domainToSearch)) {
					$SDEmailAddress 	= "";
					//$studyDirectorName	= "";
				}
			}
			
			$workProduct_name   = '<a target="_blank" href="'.$site_url.'/#M03_Work_Product_Deliverable/'.$wpd_id.'" >'.$wpdName. '</a>';
			$emailBody .= "<tr><td>".$studyDirectorName."</td><td>".$workProduct_name."</td><td>".$lastProcedureDate."</td><td>".$dueDate."</td><td>".$finalDueDate."</td></tr>";
				
			$wp_emailAdd[] = $SDEmailAddress;	
		}	
	 }

	if($emailBody!="" ){ //&& count($wp_emailAdd)>0
		$cntr = 1;
		
		$emailBody = "<table width='800px' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'><tr>
			<th bgcolor='#b3d1ff' align='left'>Study Director</th>
			<th bgcolor='#b3d1ff' align='left'>Work Product Deliverable</th>
			<th bgcolor='#b3d1ff' align='left'>Last Procedure Date</th>
			<th bgcolor='#b3d1ff' align='left'>Draft Due Date</th>
			<th bgcolor='#b3d1ff' align='left'>External Final Due Date</th>
			</tr>".$emailBody."</table>";	
			
			
			//$wp_emailAdd[] = 'tfossum@apsemail.com';
			$wp_emailAdd[] = 'hackerson@apsemail.com';
			$wp_emailAdd[] = 'cleet@apsemail.com';	 
			$wp_emailAdd[] = 'emarkuson@apsemail.com';	 
			$wp_emailAdd[] = 'anelson@apsemail.com';	 
			$wp_emailAdd[] = 'cvaleri@apsemail.com';	 
			$wp_emailAdd[] = 'ablakstvedt@apsemail.com';	 
			 
			//remove the repeated email addresses
			$wp_emailAdd 	= array_unique($wp_emailAdd);
			$emailString 	= implode(",",$wp_emailAdd);	

			//Send Email/
			///Upload Document Reminder Template 
			$template = new EmailTemplate();
			$emailObj = new Email();			

			$template->retrieve_by_string_fields(array('name' => 'Notification of Final Report Overdue for QA Review', 'type' => 'email'));

			$template->body_html = str_replace('[Consolidate_Report]', $emailBody, $template->body_html);
			//$template->body_html .= $emailString;
			
			$defaults	= $emailObj->getSystemDefaultEmail();
			$mail		= new SugarPHPMailer();
			$mail->setMailerForSystem();
			$mail->IsHTML(true);
			$mail->From		= $defaults['email'];
			$mail->FromName = $defaults['name'];
			$mail->Subject	= $template->subject;
			$mail->Body		= $template->body_html;
			
			foreach ($wp_emailAdd as $wmail){ 
				$domain_name = substr(strrchr($wmail, "@"), 1);
				if (trim(strtolower($domain_name) == $domainToSearch)){
					$mail->AddAddress($wmail); 
				}
			}	
			 
			$mail->AddBCC('vsuthar@apsemail.com');		

			//If their exist some valid email addresses then send email
			if (!empty($wp_emailAdd)) {
				if (!$mail->Send()) {
					$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
				} else {
					$GLOBALS['log']->debug('email sent');
				}
				unset($mail);
			}		 
	}	
	return true;
}
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/communication_requiring_assessment_consolidate.php

//Add job in job string
$job_strings[] = 'communication_requiring_assessment_consolidate';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function communication_requiring_assessment_consolidate() {
    global $db;
    $current_date = date("Y-m-d"); //current date
	
    $wp_emailAdd = array(); // contains the email addresses of Study Director and its Manager
	$sd_array = array(); //Contains id For Study Directors
    
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];
	$domainToSearch = 'apsemail.com';
    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();

	$queryCustom = "SELECT 
					commu.id AS communicationID, 
					commu.name AS CommunicationName,  
					commu_cstm.target_reinspection_date_c, 
					commu_cstm.error_category_c,  					
					commu_cstm.reinspection_date_c, 
					commu_wp.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
					wp.name  AS workProductName,
					wp_cstm.contact_id_c,
					wp.assigned_user_id,
					wpCode.name AS workProductCode,
					wp_cstm.work_product_compliance_c
				FROM `m06_error` AS commu
					INNER JOIN m06_error_cstm AS commu_cstm
					ON  commu.id = commu_cstm.id_c

					RIGHT JOIN m06_error_m03_work_product_1_c AS commu_wp
					ON  commu.id = commu_wp.m06_error_m03_work_product_1m06_error_ida
					
					INNER JOIN m03_work_product AS wp
					ON  commu_wp.m06_error_m03_work_product_1m03_work_product_idb=wp.id 
					
					INNER JOIN m03_work_product_cstm AS wp_cstm
					ON  wp.id=wp_cstm.id_c 
					
					LEFT JOIN m03_work_product_code wpCode ON wpCode.id = wp_cstm.m03_work_product_code_id1_c
				WHERE  
					commu_cstm.error_category_c != 'Internal Feedback' 
				AND commu_cstm.error_category_c != 'Feedback'
				AND commu_cstm.error_category_c != 'Equipment Maintenance Request'
				AND commu_cstm.error_category_c != 'External Audit'
				AND commu_cstm.error_category_c != 'Maintenance Request'
				AND commu_cstm.error_category_c != 'Process Audit'
				AND commu_cstm.error_category_c != 'Weekly Sweep'
				AND commu_cstm.error_category_c != 'Study Specific Charge' 
				AND NOT((commu_cstm.error_category_c = 'Work Product Schedule Outcome' AND  ( commu_cstm.error_type_c = 'Standard Biocomp TS Selection' OR commu_cstm.error_type_c = 'Passed Pyrogen' OR commu_cstm.error_type_c = 'Not used shared BU' OR commu_cstm.error_type_c = 'Performed Per Protocol' OR commu_cstm.error_type_c = 'Not Used' OR commu_cstm.error_type_c = 'Sedation for Animal Health')))
				AND YEAR(commu_cstm.target_reinspection_date_c) >= 2020 
				AND commu_cstm.target_reinspection_date_c <'" . $current_date . "'
				AND commu_wp.m06_error_m03_work_product_1m03_work_product_idb IS NOT NULL
				AND commu_wp.deleted = 0
				AND commu.deleted = 0			
				AND ((wpCode.name NOT LIKE 'AH%' AND  wpCode.name NOT LIKE 'TR%') OR wpCode.name IS NULL)
				AND(			
				(  (wp_cstm.work_product_compliance_c != 'NonGLP Discovery')
				AND (commu_cstm.resolution_c IS NULL 
						OR commu_cstm.reinspection_date_c IS NULL 
						OR commu_cstm.error_classification_c IS NULL  
						OR commu_cstm.error_classification_c ='Choose One'
					)
				)
				OR
				(
				(wp_cstm.work_product_compliance_c = 'NonGLP Discovery')
				AND  commu_cstm.reinspection_date_c IS NULL 
				)  
				)			
				ORDER BY commu_cstm.target_reinspection_date_c ASC";

			$queryCommResult = $db->query($queryCustom);


			while ($fetchCommResult = $db->fetchByAssoc($queryCommResult)) {

				$communicationID		= $fetchCommResult['communicationID'];
				$workProductID			= $fetchCommResult['workProductID'];
				$workProductName		= $fetchCommResult['workProductName'];
				$communicationName		= $fetchCommResult['CommunicationName'];
				$targetAssessmentDate	= $fetchCommResult['target_reinspection_date_c'];
				$workProductCompliance	= $fetchCommResult['work_product_compliance_c'];		
				//$SD_ID				= $fetchCommResult['contact_id_c'];
				$SD_ID					= $fetchCommResult['assigned_user_id'];

				if ($SD_ID != "" && $workProductID != "" && $communicationID != "" && $communicationName!=""  && $targetAssessmentDate!="" ) {

					$SD_UserAddress		= ""; /**Work Product's Assigned to User's Email Address */
					$SD_UserName		= "";
					$SD_manager_name	= "";
					$SD_manager_email	= ""; 


					/*Get Assigned to user detail*/
					$user_bean1	 		= BeanFactory::getBean('Employees', $SD_ID);
					$SD_Status			= $user_bean1->employee_status;
					if($SD_Status!="Terminated"){
						$SD_UserName		= $user_bean1->name;				
						$SD_UserAddress		= $user_bean1->emailAddress->getPrimaryAddress($user_bean1);
						$domain_name = substr(strrchr($SD_UserAddress, "@"), 1);
						if (trim(strtolower($domain_name) != $domainToSearch)) {
							$SD_UserAddress = "";
							$SD_UserName	= "";
						}
					}

					
					//get Assigned to User's Manager Detail
					if($user_bean1->reports_to_id){ 
						$reportsTo_id		= $user_bean1->reports_to_id;
						$director_bean 		= BeanFactory::getBean('Employees', $reportsTo_id);
						$SD_manager_name	= $director_bean->name;
						$SD_manager_email	= $director_bean->emailAddress->getPrimaryAddress($director_bean);

						$domain_name = substr(strrchr($SD_manager_email, "@"), 1);
						if (trim(strtolower($domain_name) != $domainToSearch)) {
							$SD_manager_email = "";
							$SD_manager_name  = "";
						} 
					}

					$sd_array[$SD_ID][]	 = array( 
							"communicationID" 	=> $communicationID,  
							"communicationName" => $communicationName,
							"workProductID" 	=> $workProductID, 
							"workProductName" 	=> $workProductName,
							"workProductCompliance" => $workProductCompliance,	
							"assessmentDate" 	=> date("m-d-Y", strtotime($targetAssessmentDate)),
							"mgtName" 			=> $SD_UserName,
							"mgtMail" 			=> $SD_UserAddress,
							"sd_mgtMail" 		=> $SD_manager_email,
							"sd_mgtName" 		=> $SD_manager_name,
					); 					
				}
			}


		if(count($sd_array)>0){ 
			$cntr = 1;
			foreach ($sd_array as $key => $value) { 
				$communication_name	 =""; 	

				unset($wp_emailAdd); 
				/* Get Email id of Assigned User and report to User */
				$wp_emailAdd = array();

				//Email Body Header
				$emailBody = "<table width='500px' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
				<tr><th bgcolor='#b3d1ff' align='left'>Communication ID </th><th bgcolor='#b3d1ff' align='left'>Work Product ID</th><th bgcolor='#b3d1ff' align='left'>Target Assessment Date</th></tr>";	

				for($cnt=0;$cnt<count($sd_array[$key]);$cnt++){
					$communication_name = '<a target="_blank"href="'.$site_url.'/#M06_Error/'.$sd_array[$key][$cnt]['communicationID'].'">'.$sd_array[$key][$cnt]['communicationName'] . '</a>';
					$workProduct_name   = "";
					if($sd_array[$key][$cnt]['workProductID']!="" && $sd_array[$key][$cnt]['workProductName']!=""){
						$workProduct_name   = '<a target="_blank" href="'.$site_url.'/#M03_Work_Product/'.$sd_array[$key][$cnt]['workProductID'].'" >'.$sd_array[$key][$cnt]['workProductName'] . '</a>';
					}

					$sd_mgtName  	  = $sd_array[$key][$cnt]['sd_mgtName'];
					$sd_mgtMail  	  = $sd_array[$key][$cnt]['sd_mgtMail'];
					$mgtName  		  = $sd_array[$key][$cnt]['mgtName'];
					$mgtMail  		  = $sd_array[$key][$cnt]['mgtMail'];
					$assessment_date  = $sd_array[$key][$cnt]['assessmentDate'];

					$emailBody .= "<tr><td>".$communication_name."</td><td>".$workProduct_name."</td><td>".$assessment_date."</td></tr>";
				}	
				$emailBody .="</table>";

				//$wp_emailAdd[] = 'mconforti@apsemail.com';
				$wp_emailAdd[] = $mgtMail;	
				$wp_emailAdd[] = $sd_mgtMail;
				$wp_emailAdd   = array_values($wp_emailAdd); //re-index the array elements
				//remove the repeated email addresses
				$wp_emailAdd = array_unique($wp_emailAdd);

				$emailString = implode(",",$wp_emailAdd);

				if($mgtName=="")
					$mgtName = $sd_mgtName;

				if($mgtName=="")
					$mgtName = "Michael Confirti";

				$template->retrieve_by_string_fields(array('name' => 'Communication Requiring Assessment Consolidate', 'type' => 'email'));

				$template->body_html = str_replace('[SD Name]', $mgtName, $template->body_html);
				$template->body_html = str_replace('[Consolidate_Report]', $emailBody, $template->body_html);
				//$template->body_html .= $emailString; 

				$defaults	= $emailObj->getSystemDefaultEmail();
				$mail		= new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->IsHTML(true);
				$mail->From		= $defaults['email'];
				$mail->FromName = $defaults['name'];
				$mail->Subject	= $template->subject;
				$mail->Body		= $template->body_html;
					
				foreach ($wp_emailAdd as $wmail) { 
					$domain_name = substr(strrchr($wmail, "@"), 1);
					if (trim(strtolower($domain_name) == $domainToSearch)) {
						$mail->AddAddress($wmail); 
					}
				} 

				$mail->AddBCC('vsuthar@apsemail.com');
				$mail->AddBCC('cjagadeeswaraiah@apsemail.com');
				//If their exist some valid email addresses then send email
				if (!empty($wp_emailAdd)) { 
					if (!$mail->Send()) {
						$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
					} else {
						$GLOBALS['log']->debug('email sent for ' . $study_dir_id);
					}
					unset($mail);
				}
			}
		}
    
     	 
  
	unset($sd_array);
	$sd_array = array();
	//*Functionality to send Emails for Communication Reassessments*//
	$queryCustomReassement = "SELECT 
					commu.id AS communicationID, 
					commu.name AS CommunicationName,  
					 commu_cstm.error_category_c,  
					commu_cstm.target_sd_reassessment_date_c,  
					commu_cstm.actual_sd_reassessment_date_c, 
					commu_wp.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
					wp.name  AS workProductName,
					wp_cstm.contact_id_c,
					wp.assigned_user_id,
					wp_cstm.work_product_compliance_c		
				FROM `m06_error` AS commu
					INNER JOIN m06_error_cstm AS commu_cstm
					ON  commu.id = commu_cstm.id_c
					
					RIGHT JOIN m06_error_m03_work_product_1_c AS commu_wp
					ON  commu.id = commu_wp.m06_error_m03_work_product_1m06_error_ida
					
					INNER JOIN m03_work_product AS wp
					ON  commu_wp.m06_error_m03_work_product_1m03_work_product_idb=wp.id 
					
					INNER JOIN m03_work_product_cstm AS wp_cstm
                    ON  wp.id=wp_cstm.id_c 
				WHERE 
				commu_cstm.sd_reassessment_required_c='Yes' 
				AND commu_cstm.target_sd_reassessment_date_c<'" . $current_date . "'  
				AND commu_wp.m06_error_m03_work_product_1m03_work_product_idb IS NOT NULL
				AND YEAR(commu_cstm.target_sd_reassessment_date_c) >= 2020 
				AND commu_wp.deleted = 0
				AND commu.deleted = 0
				AND commu_cstm.actual_sd_reassessment_date_c IS NULL 
			
				ORDER BY commu_cstm.target_sd_reassessment_date_c ASC";
				
				
	 

    $queryCommReassessResult = $db->query($queryCustomReassement);
    
    while ($fetchCommReassessResult = $db->fetchByAssoc($queryCommReassessResult)) {

        $workProductID			= $fetchCommReassessResult['workProductID'];
        $communicationID		= $fetchCommReassessResult['communicationID'];
        $communicationName		= $fetchCommReassessResult['CommunicationName'];
        $workProductName		= $fetchCommReassessResult['workProductName'];
		$workProductCompliance	= $fetchCommReassessResult['work_product_compliance_c'];
		$assessmentDate			= $fetchCommReassessResult['target_sd_reassessment_date_c'];
		$SD_ID					= $fetchCommResult['assigned_user_id'];
		//$SD_ID = $fetchCommReassessResult['contact_id_c'];
		
        if ($workProductID != "" && $communicationID != "" && $SD_ID != "" ) {
			$sd_array[$SD_ID][]	 = array( 
						"workProductID" => $workProductID, 
						"workProductName" => $workProductName,	
						"communicationID" => $communicationID, 
						"workProductCompliance" => $workProductCompliance,							
						"communicationName" => $communicationName,
						"assessmentDate" => date("m-d-Y", strtotime($assessmentDate)),		
					); 
					
		}
	}

	if(count($sd_array)>0){ 
		foreach ($sd_array as $key => $value) { 
			$study_dir_id		= $key;
			$communication_name	= "";
			$workProduct_name	= "";	
			
			unset($wp_emailAdd);
			unset($wp_emailAddTest); //to Be deleted
			/* Get Email id of Assigned User and report to User */
			$wp_emailAdd = array();
			$wp_emailAddTest = array(); //to Be deleted
				
				
			//Email Body Header
			$emailBody = "<table width='500px' cellspacing='0' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>Communication ID </th><th bgcolor='#b3d1ff' align='left'>Work Product ID</th><th bgcolor='#b3d1ff' align='left'>Target Reassessment Date</th></tr>";	
			for($cnt=0;$cnt<count($sd_array[$key]);$cnt++){
				$communication_name = '<a target="_blank"href="' . $site_url . '/#M06_Error/' . $sd_array[$key][$cnt]['communicationID'] . '">' . $sd_array[$key][$cnt]['communicationName'] . '</a>'; 
				
				$redStyle = "";
				if($sd_array[$key][$cnt]['workProductCompliance']=="GLP")
					$redStyle = "style='color:red'";
				$workProduct_name = '<a target="_blank" href="' . $site_url . '/#M03_Work_Product/' . $sd_array[$key][$cnt]['workProductID'] . '" '.$redStyle.'>' . $sd_array[$key][$cnt]['workProductName'] . '</a>';			
				
				$assessment_date  = $sd_array[$key][$cnt]['assessmentDate'];
				$emailBody .= "<tr><td>".$communication_name."</td><td>".$workProduct_name."</td><td>".$assessment_date."</td></tr>";
			}
			$emailBody .="</table>";
			if ($study_dir_id != "") {

				/*Get Assigned to user detail*/
				$SD_UserName		= "";	
				$user_bean1	 		= BeanFactory::getBean('Employees', $study_dir_id);
				$SD_Status			= $user_bean1->employee_status;
				if($SD_Status!="Terminated"){
					$SD_UserName		= $user_bean1->name;				
					$SD_UserAddress		= $user_bean1->emailAddress->getPrimaryAddress($user_bean1);

					$domain_name = substr(strrchr($SD_UserAddress, "@"), 1);
					if (trim(strtolower($domain_name) == $domainToSearch)) {
						$wp_emailAdd[] = $SD_UserAddress;
					}
				}

				//get Assigned to User's Manager
				if($user_bean1->reports_to_id){ 
					$reportsTo_id		= $user_bean1->reports_to_id;
					$director_bean 		= BeanFactory::getBean('Employees', $reportsTo_id);
					$SD_manager_name	= $director_bean->name;
					$SD_manager_email	= $director_bean->emailAddress->getPrimaryAddress($director_bean);

					$domain_name = substr(strrchr($SD_manager_email, "@"), 1);
					if (trim(strtolower($domain_name) == $domainToSearch)) {
						$wp_emailAdd[] = $SD_manager_email;
					} 
				}

				if($SD_UserName=="")
					$sdName = $SD_manager_name;
			
				if($sdName=="")
					$sdName = "Michael Confirti";
					
				
			}
			//$wp_emailAdd[] = 'mconforti@apsemail.com';	
			$wp_emailAdd = array_values($wp_emailAdd); //re-index the array elements
			//remove the repeated email addresses
			$wp_emailAdd = array_unique($wp_emailAdd);
				
			$template->retrieve_by_string_fields(array('name' => 'Communication Requiring Reassessment Consolidate', 'type' => 'email'));
			
			$template->body_html = str_replace('[SD Name]', $sdName, $template->body_html);
			$template->body_html = str_replace('[Consolidate_Report]', $emailBody, $template->body_html);
			//$template->body_html .= implode(",",$wp_emailAdd);    
			
			$defaults = $emailObj->getSystemDefaultEmail();
				$mail = new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->IsHTML(true);
				$mail->From = $defaults['email'];
				$mail->FromName = $defaults['name'];
				$mail->Subject = $template->subject;
				$mail->Body = $template->body_html;
				

				//$wp_emailAddTest[] = 'vsuthar@apsemail.com'; ////to Be deleted
				foreach ($wp_emailAdd as $wmail) { 
				$domain_name = substr(strrchr($wmail, "@"), 1);
					if (trim(strtolower($domain_name) == $domainToSearch)) {
						$mail->AddAddress($wmail); 
					}
				}
				
				$mail->AddBCC('vsuthar@apsemail.com');
				$mail->AddBCC('cjagadeeswaraiah@apsemail.com');
				//If their exist some valid email addresses then send email
				if (!empty($wp_emailAdd)) {
					//$GLOBALS['log']->fatal("valid email addresses are ". print_r($wpd_emailAdd, 1));
					if (!$mail->Send()) {
						$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
					} else {
						$GLOBALS['log']->debug('email sent for ' . $study_dir_id);
					}
					unset($mail);
				}
		
				
			}
		}	
	return true;
   } 

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/td_single_date_recalculate.php

//Add job in job string
$job_strings[] = 'td_single_date_recalculate';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function td_single_date_recalculate()
{
	global $db;
	$site_url = $GLOBALS['sugar_config']['site_url'];
	$query = "SELECT IFNULL(taskd_task_design.id,'') primaryid
,IFNULL(taskd_task_design.name,'') taskd_task_design_name
,IFNULL(taskd_task_design.type_2,'') taskd_task_design_type_2,taskd_task_design.actual_datetime - INTERVAL 300 MINUTE actual_datetime,taskd_task_design.planned_start_datetime_1st - INTERVAL 300 MINUTE planned_start_datetime_1st,taskd_task_design.planned_start_datetime_2nd - INTERVAL 300 MINUTE planned_start_datetime_2nd,taskd_task_design.scheduled_start_datetime - INTERVAL 300 MINUTE scheduled_start_datetime,IFNULL(taskd_task_design.relative,'') taskd_task_design_relative,taskd_task_design_cstm.cron_updated_c,taskd_task_design_cstm.single_date_2_c
FROM taskd_task_design
LEFT JOIN taskd_task_design_cstm taskd_task_design_cstm ON taskd_task_design.id = taskd_task_design_cstm.id_c
 WHERE (((((taskd_task_design.actual_datetime - INTERVAL 300 MINUTE > '2022-05-04 01:00:00'
) OR (taskd_task_design.planned_start_datetime_1st - INTERVAL 300 MINUTE > '2022-05-04 01:00:00'
) OR (taskd_task_design.planned_start_datetime_2nd - INTERVAL 300 MINUTE > '2022-05-04 01:00:00'
) OR (taskd_task_design.scheduled_start_datetime - INTERVAL 300 MINUTE > '2022-05-04 01:00:00'
))) AND (((taskd_task_design.type_2 NOT IN ('Plan','Plan SP') OR  taskd_task_design.type_2 IS NULL 
) AND (taskd_task_design_cstm.cron_updated_c != 'yes' OR (taskd_task_design_cstm.cron_updated_c IS NULL)
)))))  
AND  taskd_task_design.deleted=0 limit 500";
	$queryResult = $db->query($query);
	while ($fetchResult = $db->fetchByAssoc($queryResult)) {
		$tdid    									= $fetchResult['primaryid'];
		$tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $tdid);
		$taskd_task_design_name	 					= $fetchResult['taskd_task_design_name'];
		$type	 									= $fetchResult['taskd_task_design_type_2'];
		$actual_datetime	 						= $fetchResult['actual_datetime'];
		$planned_start_datetime_1st	 				= $fetchResult['planned_start_datetime_1st'];
		$planned_start_datetime_2nd	 				= $fetchResult['planned_start_datetime_2nd'];
		$scheduled_start_datetime	 				= $fetchResult['scheduled_start_datetime'];
		$single_date_2_c	 						= $fetchResult['single_date_2_c'];
		$taskd_task_design_relative	 				= $fetchResult['taskd_task_design_relative'];
		$cron_updated_c	 							= $fetchResult['cron_updated_c'];
		//echo "td bean id => ".$tdBean->id.'<br/>';
		/**Calculation of single date 2 c field for td records */
		if ($taskd_task_design_relative == "NA") {
			$single_date = strtotime($actual_datetime);
			if (!empty($actual_datetime)) {
				$single_date_2_c = date("Y-m-d", $single_date);
			} else if (!empty($scheduled_start_datetime)) {
				$single_date = strtotime($scheduled_start_datetime);
				$single_date_2_c = date("Y-m-d", $single_date);
			}
		} elseif ($taskd_task_design_relative == "1st Tier") {
			if (!empty($actual_datetime)) {
				$single_date = strtotime($actual_datetime);
				$single_date_2_c = date("Y-m-d", $single_date);
			} else if (!empty($planned_start_datetime_1st)) {
				$single_date = strtotime($planned_start_datetime_1st);
				$single_date_2_c = date("Y-m-d", $single_date);
			} else if (!empty($scheduled_start_datetime)) {
				$single_date = strtotime($scheduled_start_datetime);
				$single_date_2_c = date("Y-m-d", $single_date);
			}
		} elseif ($taskd_task_design_relative == "2nd Tier") {
			if (!empty($actual_datetime)) {
				$single_date = strtotime($actual_datetime);
				$single_date_2_c = date("Y-m-d", $single_date);
			} else if (!empty($planned_start_datetime_2nd)) {
				$single_date = strtotime($planned_start_datetime_2nd);
				$single_date_2_c = date("Y-m-d", $single_date);
			} else if (!empty($scheduled_start_datetime)) {
				$single_date = strtotime($scheduled_start_datetime);
				$single_date_2_c = date("Y-m-d", $single_date);
			}
		}
		$tdBean->single_date_2_c = $single_date_2_c;
		$tdBean->cron_updated_c = "yes";
		$tdBean->save();
		/*** end of single date 2 c field calculation */
	}

 return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/overdue_acknowledgement_daily_reminder.php

//Add job in job string
$job_strings[] = 'overdue_acknowledgement_daily_reminder';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function overdue_acknowledgement_daily_reminder() {
    global $db;
    $current_date = date("Y-m-d"); //current date
	
	$entered_date1 = '2020-12-15 00:00:00';
	
    $wp_emailAdd = array(); // contains the email addresses of Study Director and its Manager
	$sd_array = array(); //Contains id For Study Directors
    
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];
	$domainToSearch = 'apsemail.com';
    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();

    $queryCustom = "SELECT 
                    commu.id AS communicationID, 
                    commu.name AS CommunicationName,  
                    commu_cstm.target_sd_ack_date_c, 
					commu_cstm.error_category_c,  					
                    commu_cstm.reinspection_date_c, 
                    commu_wp.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
                    wp.name  AS workProductName,
					wp_cstm.contact_id_c,
					wpCode.name AS workProductCode,
					wp_cstm.work_product_compliance_c
            FROM `m06_error` AS commu
                    INNER JOIN m06_error_cstm AS commu_cstm
                    ON  commu.id = commu_cstm.id_c

                    RIGHT JOIN m06_error_m03_work_product_1_c AS commu_wp
                    ON  commu.id = commu_wp.m06_error_m03_work_product_1m06_error_ida
					
					INNER JOIN m03_work_product AS wp
                    ON  commu_wp.m06_error_m03_work_product_1m03_work_product_idb=wp.id 
					
					 INNER JOIN m03_work_product_cstm AS wp_cstm
                    ON  wp.id=wp_cstm.id_c 
					
					LEFT JOIN m03_work_product_code wpCode ON wpCode.id = wp_cstm.m03_work_product_code_id1_c
		WHERE  
			commu_cstm.error_category_c != 'Internal Feedback' 
			AND commu_cstm.error_category_c != 'Feedback'
			AND commu_cstm.error_category_c != 'Equipment Maintenance Request'
			AND commu_cstm.error_category_c != 'External Audit'
			AND commu_cstm.error_category_c != 'Maintenance Request'
			AND commu_cstm.error_category_c != 'Process Audit'
			AND commu_cstm.error_category_c != 'Weekly Sweep'  
			AND commu_cstm.error_category_c != 'Study Specific Charge'
			AND NOT((commu_cstm.error_category_c = 'Work Product Schedule Outcome' AND  ( commu_cstm.error_type_c = 'Passed Pyrogen' OR commu_cstm.error_type_c = 'Not used shared BU' OR commu_cstm.error_type_c = 'Standard Biocomp TS Selection' OR commu_cstm.error_type_c = 'Performed Per Protocol' OR commu_cstm.error_type_c = 'Not Used' OR commu_cstm.error_type_c = 'Sedation for Animal Health')))
			AND YEAR(commu_cstm.target_sd_ack_date_c) >= 2020  
			AND commu_cstm.target_sd_ack_date_c >'" . $entered_date1 . "'
			AND commu_cstm.target_sd_ack_date_c <'" . $current_date . "'
			AND commu_wp.m06_error_m03_work_product_1m03_work_product_idb IS NOT NULL
			AND commu_wp.deleted = 0
			AND commu.deleted = 0
			AND  ((wpCode.name NOT LIKE 'AH%' AND  wpCode.name NOT LIKE 'TR%') OR wpCode.name IS NULL)
			AND commu_cstm.actual_sd_ack_date_c IS NULL 
			
		ORDER BY commu_cstm.target_sd_ack_date_c ASC";

    $queryCommResult = $db->query($queryCustom);

	 while($fetchCommResult = $db->fetchByAssoc($queryCommResult)) {

        $communicationID	= $fetchCommResult['communicationID'];
		$workProductID		= $fetchCommResult['workProductID'];
		$workProductName	= $fetchCommResult['workProductName'];
        $communicationName	= $fetchCommResult['CommunicationName'];
        $targetAckDate		= $fetchCommResult['target_sd_ack_date_c']; 
		$SD_ID				= $fetchCommResult['contact_id_c'];		
		
        if ($SD_ID != "" && $communicationID != "" && $communicationName!=""  && $targetAckDate!="" ) {
			
			$mgt_bean		= BeanFactory::getBean('Contacts', $SD_ID);
			$mgtName		= $mgt_bean->first_name." ".$mgt_bean->last_name;
			$mgtEmailAddress = $mgt_bean->emailAddress->getPrimaryAddress($mgt_bean);
			
			//get Study Director's Manager
			if($mgt_bean->contact_id_c){ 
				$director_id =$mgt_bean->contact_id_c;
				$director_bean = BeanFactory::getBean('Contacts', $director_id);
				$SD_manager_name		= $director_bean->first_name." ".$director_bean->last_name;
				if($director_bean->email1){
					$director_email = $director_bean->email1;
					$SD_manager_email = $director_email;
                }
			}
			
			$sd_array[$SD_ID][]	 = array( 
						"communicationID" => $communicationID,  
						"communicationName" => $communicationName,
						"workProductID" => $workProductID, 
						"workProductName" => $workProductName,
						"ackDate" => date("m-d-Y", strtotime($targetAckDate)),
						"SDName" => $mgtName,
						"SDMail" => $mgtEmailAddress,
						"sd_mgtMail" => $SD_manager_email,
						"sd_mgtName" => $SD_manager_name,
						
					); 							
		}
	}
	
	
	if(count($sd_array)>0){ 
		$cntr = 1;
		foreach ($sd_array as $key => $value) { 
			$communication_name	 =""; 	
		
			unset($wp_emailAdd); 
            /* Get Email id of Assigned User and report to User */
            $wp_emailAdd = array();
			
			//Email Body Header
			$emailBody = "<table width='500px' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Communication ID </th><th bgcolor='#b3d1ff' align='left'>Work Product ID</th><th bgcolor='#b3d1ff' align='left'>Target Aknowledgement Date</th></tr>";	
			
			for($cnt=0;$cnt<count($sd_array[$key]);$cnt++){
				$communication_name = '<a target="_blank"href="'.$site_url.'/#M06_Error/'.$sd_array[$key][$cnt]['communicationID'].'">'.$sd_array[$key][$cnt]['communicationName'] . '</a>';
				$workProduct_name   = "";
				if($sd_array[$key][$cnt]['workProductID']!="" && $sd_array[$key][$cnt]['workProductName']!=""){
					$workProduct_name   = '<a target="_blank" href="'.$site_url.'/#M03_Work_Product/'.$sd_array[$key][$cnt]['workProductID'].'" >'.$sd_array[$key][$cnt]['workProductName'] . '</a>';
				}
			
				$mgtName  = $sd_array[$key][$cnt]['SDName'];
				$mgtMail  = $sd_array[$key][$cnt]['SDMail'];
				$sdMgtMail  = $sd_array[$key][$cnt]['sd_mgtMail'];
				$aknowledgement_date  = $sd_array[$key][$cnt]['ackDate'];
				
				$emailBody .= "<tr><td>".$communication_name."</td><td>".$workProduct_name."</td><td>".$aknowledgement_date."</td></tr>";
			}	
			$emailBody .="</table>";
			
			//$wp_emailAdd[] = 'mconforti@apsemail.com';
			$wp_emailAdd[] = $mgtMail;
			$wp_emailAdd[] = $sdMgtMail;			
			$wp_emailAdd = array_values($wp_emailAdd); //re-index the array elements
			//remove the repeated email addresses
			$wp_emailAdd = array_unique($wp_emailAdd);
			
			//$emailString = implode(",",$wp_emailAdd);
				
			$template->retrieve_by_string_fields(array('name' => 'Overdue Acknowledgement Daily Reminder Consolidate', 'type' => 'email'));
			 
			$template->body_html = str_replace('[MGT Name]', $mgtName, $template->body_html);
			$template->body_html = str_replace('[Consolidate_Report]', $emailBody, $template->body_html);
			  
			$defaults	= $emailObj->getSystemDefaultEmail();
			$mail		= new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From		= $defaults['email'];
            $mail->FromName = $defaults['name'];
            $mail->Subject	= $template->subject;
            $mail->Body		= $template->body_html;
					 
			foreach ($wp_emailAdd as $wmail) { 
				$domain_name = substr(strrchr($wmail, "@"), 1);
				if (trim(strtolower($domain_name) == $domainToSearch)) {
					$mail->AddAddress($wmail); 
				}
			} 			
			$mail->AddBCC('vsuthar@apsemail.com');
			//$mail->AddBCC('cjagadeeswaraiah@apsemail.com');
			//If their exist some valid email addresses then send email
            if (!empty($wp_emailAdd)) {
                //$GLOBALS['log']->fatal("valid email addresses are ". print_r($wpd_emailAdd, 1));
                if (!$mail->Send()) {
                    $GLOBALS['log']->debug("Fail to Send Email, Please check settings");
                } else {
                    $GLOBALS['log']->debug('email sent for ' . $wp_emailAdd);
                }
				unset($mail);
            }
		}
	}	
	return true;
} 

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/gd_status_change_daily_reminder_batch_id_second.php

//Add job in job string
$job_strings['gd_status_change_daily_reminder_batch_id_second'] = 'gd_status_change_daily_reminder_batch_id_second';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function gd_status_change_daily_reminder_batch_id_second()
{
	global $db;
	$current_date = date("Y-m-d"); //current date	
	$yesterdaydate	= date("Y-m-d", strtotime("-1 days", strtotime($current_date)));
	$current_date	= $current_date . ' 05:59:59';
	$yesterdaydate	= $yesterdaydate . ' 06:00:00';
	$GLOBALS['log']->fatal("yesterdaydate ". $yesterdaydate);	
	$GLOBALS['log']->fatal("current_date ". $current_date);
	/**/
	$reportSQL = "SELECT IFNULL(gd_group_design.id,'') primaryid
	,IFNULL(gd_group_design.name,'') gd_name
	,IFNULL(gd_group_design.id,'') gd_group_design_id
	,l2_cstm.contact_id_c contact_id,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contacts_name,
	gd_audit.after_value_string AS after_value,
	gd_audit.date_created AS Date_c

	FROM gd_group_design
	INNER JOIN  bid_batch_id_gd_group_design_1_c l1_1 ON gd_group_design.id=l1_1.bid_batch_id_gd_group_design_1gd_group_design_idb AND l1_1.deleted=0

	INNER JOIN  bid_batch_id l1 ON l1.id=l1_1.bid_batch_id_gd_group_design_1bid_batch_id_ida AND l1.deleted=0
	INNER JOIN  bid_batch_id_m03_work_product_1_c l2_1 ON l1.id=l2_1.bid_batch_id_m03_work_product_1bid_batch_id_ida AND l2_1.deleted=0

	INNER JOIN  m03_work_product l2 ON l2.id=l2_1.bid_batch_id_m03_work_product_1m03_work_product_idb AND l2.deleted=0
	LEFT JOIN m03_work_product_cstm l2_cstm ON l2.id = l2_cstm.id_c
	LEFT JOIN contacts contacts1 ON contacts1.id = l2_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
		LEFT JOIN gd_group_design_audit gd_audit ON gd_group_design.id = gd_audit.parent_id
	WHERE (((l2_cstm.test_system_c IN ('Chinchilla','Guinea Pig','Hamster','Lagomorph','Mouse','Rat')
	)))
	AND gd_audit.after_value_string ='complete'
	AND (gd_audit.date_created - INTERVAL 360 MINUTE >= '" . $yesterdaydate . "' AND gd_audit.date_created - INTERVAL 360 MINUTE <= '" . $current_date . "')
	AND  gd_group_design.deleted=0 
	AND IFNULL(contacts1.deleted,0)=0 group by gd_audit.parent_id ORDER BY gd_name ASC";

	$reportResult	= $db->query($reportSQL);

	if ($reportResult->num_rows > 0) {
		$site_url		= $GLOBALS['sugar_config']['site_url'];
		//Prepare the Email Template to send to appropriate members
		$template		= new EmailTemplate();
		$emailObj		= new Email();
		$template->retrieve_by_string_fields(array('name' => 'Completed Group Design for Review Second', 'type' => 'email'));
		$wp_emailAdd = array();
		//Email Body Header
		$emailBody = "<table width='50%' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 13px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>Group Designs</th></tr>";
		while ($fetchReportResult = $db->fetchByAssoc($reportResult)) {
			$GDID					= $fetchReportResult['primaryid'];
			$GD_Name				= $fetchReportResult['gd_name'];
			$SD_ID				 	= $fetchReportResult['contact_id'];
			
			/*Get Study Director*/
			if ($SD_ID != "") {
				$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
				$wp_emailAdd[] 		= $SDEmailAddress;
			}
			$wp_emailAdd = array_unique($wp_emailAdd);
			$Group_Design_Name		= '<a target="_blank" href="' . $site_url . '/#GD_Group_Design/' . $GDID . '" >' . $GD_Name . '</a>';

			$emailBody .= "<tr><td>" . $Group_Design_Name . "</td></tr>";
		}

		$emailBody .= "</table>";
		$template->body_html = str_replace('[Completed_GD_Report_Second]', $emailBody, $template->body_html);
		//$GLOBALS['log']->fatal("valid email addresses are ". print_r($template->body_html, 1));			
		$defaults		= $emailObj->getSystemDefaultEmail();
		$mail			= new SugarPHPMailer();
		$mail->setMailerForSystem();
		$mail->IsHTML(true);
		$mail->From		= $defaults['email'];
		$mail->FromName = $defaults['name'];
		$mail->Subject	= $template->subject;
		$mail->Body		= $template->body_html;

		$mail->AddAddress('eclark@apsemail.com');
		$mail->AddAddress('dzehowski@apsemail.com');		
		$mail->AddAddress('edrake@apsemail.com');

		foreach ($wp_emailAdd as $wmail) {
			$mail->AddAddress($wmail);
		}

		if (!$mail->Send()) {
			$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
		} else {
			$GLOBALS['log']->debug('email sent');
		}
		unset($mail);
	}

	return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/gd_status_change_daily_reminder_second.php

//Add job in job string
$job_strings['gd_status_change_daily_reminder_second'] = 'gd_status_change_daily_reminder_second';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function gd_status_change_daily_reminder_second()
{
	global $db;
	$current_date = date("Y-m-d"); //current date
	$yesterdaydate	= date("Y-m-d", strtotime("-1 days", strtotime($current_date)));
	$current_date	= $current_date . ' 05:59:59';
	$yesterdaydate	= $yesterdaydate . ' 06:00:00';	
	$GLOBALS['log']->fatal("yesterdaydate ". $yesterdaydate);	
	$GLOBALS['log']->fatal("current_date ". $current_date);	
	/**/
	$reportSQL = "SELECT IFNULL(gd_group_design.id,'') primaryid
	,IFNULL(gd_group_design.name,'') gd_name
	,IFNULL(gd_group_design_cstm.status_c,'') GD_GROUP_DESIGN_CSTM_SA89250,IFNULL(gd_group_design.id,'') gd_group_design_id
	,l1_cstm.contact_id_c contact_id,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contacts_name,
	gd_audit.after_value_string AS after_value,
	gd_audit.date_created AS Date_c
	FROM gd_group_design
	 INNER JOIN  m03_work_product_gd_group_design_1_c l1_1 ON gd_group_design.id=l1_1.m03_work_product_gd_group_design_1gd_group_design_idb AND l1_1.deleted=0
	
	 INNER JOIN  m03_work_product l1 ON l1.id=l1_1.m03_work_product_gd_group_design_1m03_work_product_ida AND l1.deleted=0
	LEFT JOIN gd_group_design_cstm gd_group_design_cstm ON gd_group_design.id = gd_group_design_cstm.id_c
	LEFT JOIN m03_work_product_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN contacts contacts1 ON contacts1.id = l1_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
	LEFT JOIN gd_group_design_audit gd_audit ON gd_group_design.id = gd_audit.parent_id
	 WHERE (((l1_cstm.test_system_c IN ('Chinchilla','Guinea Pig','Hamster','Lagomorph','Mouse','Rat')))) 
	AND gd_audit.after_value_string ='complete'
	AND (gd_audit.date_created - INTERVAL 360 MINUTE >= '" . $yesterdaydate . "' AND gd_audit.date_created - INTERVAL 360 MINUTE <= '" . $current_date . "')
	AND  gd_group_design.deleted=0 
	AND IFNULL(contacts1.deleted,0)=0 group by gd_audit.parent_id ORDER BY gd_name ASC";

	$reportResult	= $db->query($reportSQL);
	$GLOBALS['log']->fatal("reportSQL ". $reportSQL);	
	if ($reportResult->num_rows > 0) {
		$site_url		= $GLOBALS['sugar_config']['site_url'];
		//Prepare the Email Template to send to appropriate members
		$template		= new EmailTemplate();
		$emailObj		= new Email();
		$template->retrieve_by_string_fields(array('name' => 'Completed Group Design for Review Second', 'type' => 'email'));
		$wp_emailAdd = array();
		//Email Body Header
		$emailBody = "<table width='50%' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 13px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>Group Designs</th></tr>";
		while ($fetchReportResult = $db->fetchByAssoc($reportResult)) {
			$GDID					= $fetchReportResult['primaryid'];
			$GD_Name				= $fetchReportResult['gd_name'];
			$SD_ID				 	= $fetchReportResult['contact_id'];
			
			/*Get Study Director*/
			if ($SD_ID != "") {
				$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
				$wp_emailAdd[] 		= $SDEmailAddress;
			}
			$wp_emailAdd = array_unique($wp_emailAdd);
			$Group_Design_Name		= '<a target="_blank" href="' . $site_url . '/#GD_Group_Design/' . $GDID . '" >' . $GD_Name . '</a>';

			$emailBody .= "<tr><td>" . $Group_Design_Name . "</td></tr>";
		}

		$emailBody .= "</table>";
		$template->body_html = str_replace('[Completed_GD_Report_Second]', $emailBody, $template->body_html);
		//$GLOBALS['log']->fatal("valid email addresses are ". print_r($template->body_html, 1));			
		$defaults		= $emailObj->getSystemDefaultEmail();
		$mail			= new SugarPHPMailer();
		$mail->setMailerForSystem();
		$mail->IsHTML(true);
		$mail->From		= $defaults['email'];
		$mail->FromName = $defaults['name'];
		$mail->Subject	= $template->subject;
		$mail->Body		= $template->body_html;

		//$mail->AddAddress('JMcKenzie@apsemail.com');
		//$mail->AddAddress('dzehowski@apsemail.com');	
		$mail->AddAddress('jgeist@apsemail.com');
		$mail->AddAddress('treiten@apsemail.com');	
		//$mail->AddAddress('fxs_mjohnson@apsemail.com');		

		foreach ($wp_emailAdd as $wmail) {
			$mail->AddAddress($wmail);
		}
		
		//If their exist some valid email addresses then send email

		if (!$mail->Send()) {
			$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
		} else {
			$GLOBALS['log']->debug('email sent');
		}
		unset($mail);
	}

	return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/send_email_for_biocomp_report.php

//Add job in job string
$job_strings['send_email_for_biocomp_report'] = 'send_email_for_biocomp_report';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function send_email_for_biocomp_report()
{
	global $db;
	$current_date = date("Y-m-d"); //current date	
	/**/
	$reportSQL = "SELECT 
	IFNULL(m03_work_product_deliverable.id,'') primaryid,m03_work_product_deliverable_cstm.draft_deliverable_sent_date_c,
	IFNULL(l1.id,'') wp_id,IFNULL(l1.name,'') wp_name,m03_work_product_deliverable_cstm.internal_final_due_date_c wpFinalDueDate,
	l1_cstm.contact_id_c l1_cstm_contact_id_c,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contacts1_name,
	l1_cstm.contact_id7_c AS qaReviewerID,LTRIM(RTRIM(CONCAT(IFNULL(contacts3.first_name,''),' ',IFNULL(contacts3.last_name,'')))) qaReviewerName,
	IFNULL(l1_cstm.functional_area_c,'') wp_functional_area_c,l1_cstm.last_procedure_c
	FROM m03_work_product_deliverable
	INNER JOIN  m03_work_product_m03_work_product_deliverable_1_c l1_1 ON m03_work_product_deliverable.id=l1_1.m03_work_pe584verable_idb AND l1_1.deleted=0

	INNER JOIN  m03_work_product l1 ON l1.id=l1_1.m03_work_p0b66product_ida AND l1.deleted=0
	LEFT JOIN m03_work_product_deliverable_cstm m03_work_product_deliverable_cstm ON m03_work_product_deliverable.id = m03_work_product_deliverable_cstm.id_c
	LEFT JOIN m03_work_product_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN contacts contacts1 ON contacts1.id = l1_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
	LEFT JOIN contacts contacts3 ON contacts3.id = l1_cstm.contact_id7_c AND IFNULL(contacts3.deleted,0)=0 

	WHERE (((l1_cstm.functional_area_c = 'Standard Biocompatibility') AND (l1_cstm.work_product_compliance_c = 'GLP')
	AND (m03_work_product_deliverable_cstm.deliverable_status_c <> 'Completed' OR ( m03_work_product_deliverable_cstm.deliverable_status_c IS NULL  AND  'Completed' IS NOT NULL )) AND ((coalesce(LENGTH(m03_work_product_deliverable_cstm.draft_deliverable_sent_date_c), 0) <> 0)) AND (m03_work_product_deliverable_cstm.deliverable_c = 'Final Report'
	))) 
	AND  m03_work_product_deliverable.deleted=0 
	AND IFNULL(contacts1.deleted,0)=0 GROUP BY wp_id
	ORDER BY CASE WHEN (IFNULL(l1_cstm.functional_area_c,'')='' OR IFNULL(l1_cstm.functional_area_c,'') IS NULL) THEN 0
	WHEN IFNULL(l1_cstm.functional_area_c,'')='Analytical Services' THEN 1
	WHEN IFNULL(l1_cstm.functional_area_c,'')='Bioskills' THEN 2
	WHEN IFNULL(l1_cstm.functional_area_c,'')='Biocompatibility' THEN 3
	WHEN IFNULL(l1_cstm.functional_area_c,'')='Standard Biocompatibility' THEN 4
	WHEN IFNULL(l1_cstm.functional_area_c,'')='Histology Services' THEN 5
	WHEN IFNULL(l1_cstm.functional_area_c,'')='ISR' THEN 6
	WHEN IFNULL(l1_cstm.functional_area_c,'')='Pathology Services' THEN 7
	WHEN IFNULL(l1_cstm.functional_area_c,'')='Pharmacology' THEN 8
	WHEN IFNULL(l1_cstm.functional_area_c,'')='Regulatory' THEN 9
	WHEN IFNULL(l1_cstm.functional_area_c,'')='Toxicology' THEN 10 ELSE 11 END ASC
	,wpFinalDueDate ASC";

	$reportResult	= $db->query($reportSQL);

	if ($reportResult->num_rows > 0) {
		$site_url		= $GLOBALS['sugar_config']['site_url'];
		//Prepare the Email Template to send to appropriate members
		$template		= new EmailTemplate();
		$emailObj		= new Email();
		$template->retrieve_by_string_fields(array('name' => 'Biocomp Report Queue', 'type' => 'email'));
		$srno = 1;
		//Email Body Header
		$emailBody = "<table width='100%' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 13px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>#</th><th bgcolor='#b3d1ff' align='left'>Date Received</th><th bgcolor='#b3d1ff' align='left'>Study ID</th><th bgcolor='#b3d1ff' align='left'>Last Procedure Date</th><th bgcolor='#b3d1ff' align='left'>Internal Due Date</th><th bgcolor='#b3d1ff' align='left'>Study Director</th><th bgcolor='#b3d1ff' align='left'>QA Trainee Reviewer</th></tr>";

		while ($fetchReportResult = $db->fetchByAssoc($reportResult)) {
			$wpID				= $fetchReportResult['wp_id'];
			$checkLeadAudit		= "SELECT * FROM `contacts_m03_work_product_2_c` WHERE `contacts_m03_work_product_2m03_work_product_idb`='" . $wpID . "'";
			$leadAuditResult	= $db->query($checkLeadAudit);
			if ($leadAuditResult->num_rows == 0) {
				$wpdID				= $fetchReportResult['primaryid'];
				$dateDraftSent		= $fetchReportResult['draft_deliverable_sent_date_c'];				
				$wpName				= $fetchReportResult['wp_name'];
				$wpFinalDueDate		= $fetchReportResult['wpFinalDueDate'];
				$SDID				= $fetchReportResult['l1_cstm_contact_id_c'];
				$SDName				= $fetchReportResult['contacts1_name'];
				$qaReviewerId		= $fetchReportResult['qaReviewerID'];
				$qaReviewerName		= $fetchReportResult['qaReviewerName'];
				$lastProcedureDate  = $fetchReportResult['last_procedure_c'];


				if ($dateDraftSent != "") {
					$dateQA_arr		= explode("-", $dateDraftSent);
					$dateDraftSent  = $dateQA_arr[1] . "/" . $dateQA_arr[2] . "/" . $dateQA_arr[0];
				}

				if ($wpFinalDueDate != "") {
					$dateDue_arr = explode("-", $wpFinalDueDate);
					$wpFinalDueDate  = $dateDue_arr[1] . "/" . $dateDue_arr[2] . "/" . $dateDue_arr[0];
				}
				if ($lastProcedureDate != "") {
					$lastProcedureDate_arr = explode("-", $lastProcedureDate);
					$lastProcedureDate  = $lastProcedureDate_arr[1] . "/" . $lastProcedureDate_arr[2] . "/" . $lastProcedureDate_arr[0];
				}

					/*$qaDateObj			= new TimeDate();
					$formattedCD		= $qaDateObj->to_display_date_time($dateQA_Received, true, true, $current_user);
					$formattedCD 		= str_replace('-', '/', $formattedCD);
					$dateQA_Received	= date("m/d/Y",strtotime($formattedCD));
								
					$formattedFinalDate		= $qaDateObj->to_display_date_time($wpFinalDueDate, true, true, $current_user);
					$formattedFinalDate 	= str_replace('-', '/', $formattedFinalDate);
					$wpFinalDueDate			= date("m/d/Y",strtotime($formattedFinalDate));*/


				$wpLink		= '<a target="_blank" href="' . $site_url . '/#M03_Work_Product/' . $wpID . '" >' . $wpName . '</a>';
				$sdLink		= '<a target="_blank" href="' . $site_url . '/#Contacts/' . $SDID . '" >' . $SDName . '</a>';
				$qaLink		= '<a target="_blank" href="' . $site_url . '/#Contacts/' . $qaReviewerId . '" >' . $qaReviewerName . '</a>';

				$emailBody .= "<tr><td>".$srno++."</td><td>" . $dateDraftSent . "</td><td>" . $wpLink . "</td><td>" . $lastProcedureDate . "</td><td>" . $wpFinalDueDate . "</td><td>" . $sdLink . "</td><td>" . $qaLink . "</td></tr>";
			}
		}

		$emailBody .= "</table>";
		$template->body_html = str_replace('[Consolidate_Report]', $emailBody, $template->body_html);

		$defaults		= $emailObj->getSystemDefaultEmail();
		$mail			= new SugarPHPMailer();
		$mail->setMailerForSystem();
		$mail->IsHTML(true);
		$mail->From		= $defaults['email'];
		$mail->FromName = $defaults['name'];
		$mail->Subject	= $template->subject;
		$mail->Body		= $template->body_html;

		//$mail->AddAddress('anelson@apsemail.com');
		$mail->AddAddress('qa.auditors@apsemail.com');

		//$mail->AddAddress('fxs_mjohnson@apsemail.com');
		$mail->AddBCC('vsuthar@apsemail.com'); 
		//If their exist some valid email addresses then send email

		if (!$mail->Send()) {
			$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
		} else {
			$GLOBALS['log']->debug('email sent');
		}
		unset($mail);
	}

	return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/daily_scheduling_needs_sa.php

//Add job in job string
$job_strings['daily_scheduling_needs_sa'] = 'daily_scheduling_needs_sa';
//  This function will found out all the pending workproduct Assessment in Communication. */

function daily_scheduling_needs_sa()
{
	global $db;
	$emailObj = new Email();
	//Prepare the Email Template to send to Appropriate Members
	$template		= new EmailTemplate();
	$site_url		= $GLOBALS['sugar_config']['site_url'];
	
	$hyperlink = '<a target="_blank" href="'.$site_url.'#bwc/index.php?module=Reports&action=DailySchedulingNeedsSA&bwcRedirect=1">Daily Scheduling Needs - SA</a>';

	$template->retrieve_by_string_fields(array('name' => 'Daily Scheduling Needs - SA', 'type' => 'email'));
	// $template->body_html = str_replace('[REPORT_Name]', 'Daily Scheduling Needs - LA', $template->body_html);
	$template->body_html = str_replace('[Daily_Scheduling_Needs_Report_Data_SA]', $hyperlink, $template->body_html);
	//$template->body_html .= $emailString; 

	$defaults	= $emailObj->getSystemDefaultEmail();
	$mail		= new SugarPHPMailer();
	$mail->setMailerForSystem();
	$mail->IsHTML(true);
	$mail->From		= $defaults['email'];
	$mail->FromName = $defaults['name'];
	$mail->Subject	= $template->subject;
	$mail->Body		= $template->body_html;
		
	$mail->AddAddress('jgeist@apsemail.com'); 	//Jennifer Geist
	$mail->AddAddress('treiten@apsemail.com'); //Teri Reiten
	$mail->addbcc('vsuthar@apsemail.com'); //vinod suthar
	// $mail->addbcc('spa_testing@apsemail.com'); 
	//If their exist some valid email addresses then send email
	if (!$mail->Send()) {
		$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
	} else {
		$GLOBALS['log']->debug('email sent successfully');
	}
	unset($mail);
	return true;
}
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/send_daily_review_to_in_life_managers.php

//Add job in job string
$job_strings['send_daily_review_to_in_life_managers'] = 'send_daily_review_to_in_life_managers';
//  This function will found out all the pending workproduct Assessment in Communication. */

function send_daily_review_to_in_life_managers()
{
	global $db;
	$emailObj = new Email();
	//Prepare the Email Template to send to Appropriate Members
	$template		= new EmailTemplate();
	$site_url		= $GLOBALS['sugar_config']['site_url'];
	
	$hyperlink = '<a target="_blank" href="'.$site_url.'/#bwc/index.php?module=Reports&action=DailySchedulingNeeds&bwcRedirect=1">Daily Scheduling Needs – LA</a>';

	$template->retrieve_by_string_fields(array('name' => 'Notification of daily scheduling needs - LA', 'type' => 'email'));
	// $template->body_html = str_replace('[REPORT_Name]', 'Daily Scheduling Needs - LA', $template->body_html);
	$template->body_html = str_replace('[Daily_Scheduling_Needs_Report_Data_LA]', $hyperlink, $template->body_html);
	//$template->body_html .= $emailString; 

	$defaults	= $emailObj->getSystemDefaultEmail();
	$mail		= new SugarPHPMailer();
	$mail->setMailerForSystem();
	$mail->IsHTML(true);
	$mail->From		= $defaults['email'];
	$mail->FromName = $defaults['name'];
	$mail->Subject	= $template->subject;
	$mail->Body		= $template->body_html;
		
	$mail->AddAddress('adargis@apsemail.com');  //Amber Dargis
	$mail->AddAddress('nuitermarkt@apsemail.com'); //Natalie Uitermarkt
	$mail->addbcc('vsuthar@apsemail.com'); //vinod suthar
	// $mail->addbcc('spa_testing@apsemail.com'); 
	//If their exist some valid email addresses then send email
	if (!$mail->Send()) {
		$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
	} else {
		$GLOBALS['log']->debug('email sent successfully');
	}
	unset($mail);
	return true;
}
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/management_comm_review_daily_reminder.php

//Add job in job string
$job_strings[] = 'management_comm_review_daily_reminder';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function management_comm_review_daily_reminder() {
    global $db;
    $current_date = date("Y-m-d"); //current date	
	$entered_date1 = '2020-12-15 00:00:00';	
	$previous_day	= date('Y-m-d', strtotime('-1 day', strtotime($current_date)));
	$next_day		= date('Y-m-d', strtotime('+1 day', strtotime($current_date)));
	
	
    $wp_emailAdd	= array(); // contains the email addresses of Study Director and its Manager
	$sd_array		= array(); //Contains id For Study Directors
    
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url		= $GLOBALS['sugar_config']['site_url'];
	$domainToSearch = 'apsemail.com';
	//$domainToSearch = array('apsemail.com', 'namsa.com');
    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();

    $queryCustom = "SELECT 
                    COMM.id AS communicationID, 
                    COMM.name AS CommunicationName, 
					CONTACTEMPLOYEE.contacts_de_deviation_employees_1contacts_ida AS CONTACTID,
					EMPLOYEE.id,
					EMPLOYEECSTM.deviation_type_c,
					CONTACT.first_name,
					CONTACT.last_name,
					CONTACTCSTM.contact_id_c AS MGTID,
					CONTACTCSTM.contact_id1_c AS oneUpMGTID,
					CONTACTCSTM.management_c AS MGTCheck,
                    COMM_CSTM.target_management_assessment_c, 
					COMM_CSTM.actual_management_assessment_c,   
					COMM_CSTM.error_category_c,
					COMM_WP.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
                    WP.name  AS workProductName
            FROM `m06_error` AS COMM
                    INNER JOIN m06_error_cstm AS COMM_CSTM
                    ON  COMM.id = COMM_CSTM.id_c
					
					LEFT JOIN m06_error_m03_work_product_1_c AS COMM_WP
                    ON  COMM.id = COMM_WP.m06_error_m03_work_product_1m06_error_ida AND COMM_WP.deleted=0
					
					LEFT JOIN m03_work_product AS WP
                    ON  COMM_WP.m06_error_m03_work_product_1m03_work_product_idb=WP.id AND WP.deleted=0 

                    RIGHT JOIN m06_error_de_deviation_employees_2_c AS COMMDEV
                    ON  COMM.id = COMMDEV.m06_error_de_deviation_employees_2m06_error_ida AND COMMDEV.deleted=0 
					
					INNER JOIN de_deviation_employees AS EMPLOYEE
                    ON  COMMDEV.m06_error_de_deviation_employees_2de_deviation_employees_idb=EMPLOYEE.id  AND EMPLOYEE.deleted=0 
					
					INNER JOIN de_deviation_employees_cstm AS EMPLOYEECSTM
                    ON  EMPLOYEE.id=EMPLOYEECSTM.id_c 
					
					INNER JOIN contacts_de_deviation_employees_1_c AS CONTACTEMPLOYEE
                    ON  EMPLOYEE.id =CONTACTEMPLOYEE.contacts_de_deviation_employees_1de_deviation_employees_idb AND CONTACTEMPLOYEE.deleted=0
					
					LEFT JOIN contacts AS CONTACT
					 	ON CONTACT.id=CONTACTEMPLOYEE.contacts_de_deviation_employees_1contacts_ida  AND CONTACT.deleted=0
					LEFT JOIN contacts_cstm AS CONTACTCSTM
					 	ON CONTACT.id=CONTACTCSTM.id_c
					 
		WHERE  
			COMM.deleted = 0
			AND YEAR(COMM_CSTM.target_management_assessment_c) >= 2020 
			AND COMM_CSTM.target_management_assessment_c >'" . $entered_date1 . "'
			AND COMM_CSTM.target_management_assessment_c <='" . $next_day . "'
			AND 
			(
			(
				(COMM_CSTM.error_category_c = 'Critical Phase Inspection' 
					OR	COMM_CSTM.error_category_c = 'Daily QC'
					OR	COMM_CSTM.error_category_c = 'Data Book QC'
					OR	COMM_CSTM.error_category_c = 'Real time study conduct'
					OR	COMM_CSTM.error_category_c = 'Retrospective Data QC'
					OR	COMM_CSTM.error_category_c = 'Process Audit'
					OR	COMM_CSTM.error_category_c = 'Test Failure'
					OR	COMM_CSTM.error_category_c = 'External Audit'
				)  AND( COMM_CSTM.subtype_c IS NULL )
			) OR (
				COMM_CSTM.error_category_c = 'Training' 
				AND COMM_CSTM.target_management_assessment_c < '" .$current_date. "'
				AND COMM.date_entered >'" . $entered_date1 . "'   
				AND ( COMM_CSTM.actual_management_assessment_c IS NULL  OR COMM_CSTM.management_assessment_train_c IS NULL )
			) OR (
				COMM_CSTM.error_category_c = 'Feedback'
				AND COMM_CSTM.target_management_assessment_c < '" .$current_date. "'
				AND COMM.date_entered >'" . $entered_date1 . "'   
				AND ( COMM_CSTM.actual_management_assessment_c IS NULL  OR COMM_CSTM.mgt_assessment_c IS NULL )
			)
			)	
		ORDER BY COMM_CSTM.target_management_assessment_c ASC";
		//GROUP BY COMM.id,MGTID

    $queryCommResult = $db->query($queryCustom);
	
	 while($fetchCommResult = $db->fetchByAssoc($queryCommResult)) {

        $communicationID	= $fetchCommResult['communicationID'];
        $communicationName	= $fetchCommResult['CommunicationName'];
		$workProductID		= $fetchCommResult['workProductID'];
		$workProductName	= $fetchCommResult['workProductName'];
        $targetAssessmentDate	= $fetchCommResult['target_management_assessment_c'];
		$EMP_ID				= $fetchCommResult['CONTACTID'];
		$EMP_TYPE			= $fetchCommResult['deviation_type_c'];
		$SD_ID				= $fetchCommResult['MGTID'];
		$Oneup_ID			= $fetchCommResult['oneUpMGTID'];
		$MGTCheck			= $fetchCommResult['MGTCheck'];
		
		
        if (($SD_ID != "" || $Oneup_ID != "") && ($communicationID != "" && $communicationName!=""  && $targetAssessmentDate!="") ) {
			
			$emp_bean		= BeanFactory::getBean('Contacts', $EMP_ID); /*Get Employee Bean */
			$empName		= $emp_bean->first_name." ".$emp_bean->last_name;
			$primaryEmailAddress = $emp_bean->emailAddress->getPrimaryAddress($emp_bean);

			$mgt_bean		= BeanFactory::getBean('Contacts', $SD_ID); /*Get Manager Bean */
			$mgtName		= $mgt_bean->first_name." ".$mgt_bean->last_name;
			$mgtEmailAddress = $mgt_bean->emailAddress->getPrimaryAddress($mgt_bean);

			$oneUpManagerBean 	 = BeanFactory::getBean('Contacts', $Oneup_ID); /*Get OneUp Manager Bean */
			$oneUpmanager_name	 = $oneUpManagerBean->first_name." ".$oneUpManagerBean->last_name;
			$oneUpmanagerAddress = $oneUpManagerBean->emailAddress->getPrimaryAddress($oneUpManagerBean);
			$helloName = "";	
			
			if($SD_ID!=""){
				$arrayID = $SD_ID;
				$helloName = $mgtName;
			}else{
				$arrayID = $Oneup_ID;
				$helloName = $oneUpmanager_name;
			}
			if($MGTCheck){
				$arrayID = $EMP_ID;
				$helloName = $empName;
			}

			$sd_array[$arrayID][]	 = array( 
						"communicationID"	=> $communicationID,  
						"communicationName" => $communicationName,
						"workProductID"		=> $workProductID, 
						"workProductName"	=> $workProductName,
						"assessmentDate"	=> date("m-d-Y", strtotime($targetAssessmentDate)),
						"empName"			=> $empName,
						"empType" 			=> $EMP_TYPE,
						"empMail" 			=> $primaryEmailAddress,
						"mgtName" 			=> $helloName,
						"mgtMail" 			=> $mgtEmailAddress,
						"OneupMngName" 		=> $oneUpmanager_name,
						"OneupMngMail" 		=> $oneUpmanagerAddress,
						"mgtCheck" 			=> $MGTCheck,
					);					
		}
	}
	
	
	if(count($sd_array)>0){ 
		$cntr = 1;
		foreach ($sd_array as $key => $value) { 
			$communication_name	 =""; 	
		
			unset($wp_emailAdd); 
            /* Get Email id of Assigned User and report to User */
			$wp_emailAdd = array();
			$arr = array();
			
			//Email Body Header
			$emailBody = "<table width='500px' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Communication ID </th><th bgcolor='#b3d1ff' align='left'>Work Product ID</th><th bgcolor='#b3d1ff' align='left'>Target Subtype Date</th></tr>";	
			
			for($cnt=0;$cnt<count($sd_array[$key]);$cnt++){
				$communication_name = '<a target="_blank"href="'.$site_url.'/#M06_Error/'.$sd_array[$key][$cnt]['communicationID'].'">'.$sd_array[$key][$cnt]['communicationName'] . '</a>';
				$workProduct_name   = "";
				if($sd_array[$key][$cnt]['workProductID']!="" && $sd_array[$key][$cnt]['workProductName']!=""){
					$workProduct_name   = '<a target="_blank" href="'.$site_url.'/#M03_Work_Product/'.$sd_array[$key][$cnt]['workProductID'].'" >'.$sd_array[$key][$cnt]['workProductName'] . '</a>';
				}
				$mgtCheck		= $sd_array[$key][$cnt]['mgtCheck'];
				$empMail		= $sd_array[$key][$cnt]['empMail'];

				$mgtName 	 	= $sd_array[$key][$cnt]['mgtName'];
				$mgtMail  		= $sd_array[$key][$cnt]['mgtMail'];
				$OneupMngName  	= $sd_array[$key][$cnt]['OneupMngName'];
				$OneupMngMail  	= $sd_array[$key][$cnt]['OneupMngMail'];
				$assessment_date  = $sd_array[$key][$cnt]['assessmentDate'];

				if($mgtCheck == 1) {
					$GLOBALS['log']->debug("mgtCheck =1");
					$wp_emailAdd[] = $empMail;	
				}				
				$wp_emailAdd[] = $mgtMail;
				$wp_emailAdd[] = $OneupMngMail;	

				//$emailBody .= "<tr><td>".$communication_name."</td><td>".$workProduct_name."</td><td>".$assessment_date."</td></tr>";
				$arr[$sd_array[$key][$cnt]['communicationName']] = "<tr><td>".$communication_name."</td><td>".$workProduct_name."</td><td>".$assessment_date."</td></tr>";
			}
			$arr =  array_unique($arr);
			foreach($arr as $key=>$value){
				$emailBody .= $value; 
			}
			$emailBody .="</table>";
			
			//$wp_emailAdd[] = 'mconforti@apsemail.com';
			$wp_emailAdd[] = 'emarkuson@apsemail.com';
				
			$wp_emailAdd = array_values($wp_emailAdd); //re-index the array elements
			//remove the repeated email addresses
			$wp_emailAdd = array_unique($wp_emailAdd);
			
			$emailString = implode(",",$wp_emailAdd);
				
			$template->retrieve_by_string_fields(array('name' => 'Management Communication Review Daily Reminder Consolidate', 'type' => 'email'));
			 
			$template->body_html = str_replace('[MGT Name]', $mgtName, $template->body_html);
			$template->body_html = str_replace('[Consolidate_Report]', $emailBody, $template->body_html);
			  
			$defaults	= $emailObj->getSystemDefaultEmail();
			$mail		= new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From		= $defaults['email'];
            $mail->FromName = $defaults['name'];
            $mail->Subject	= $template->subject;
            $mail->Body		= $template->body_html;
					 
			foreach ($wp_emailAdd as $wmail) { 
				$domain_name = substr(strrchr($wmail, "@"), 1);
				if (trim(strtolower($domain_name) == $domainToSearch)) {
					$mail->AddAddress($wmail); 
				}
            }  						
			$mail->AddBCC('vsuthar@apsemail.com');
			//$mail->AddBCC('fxs_mjohnson@apsemail.com');
			//If their exist some valid email addresses then send email
            if (!empty($wp_emailAdd)) {
                //$GLOBALS['log']->fatal("valid email addresses are ". print_r($wpd_emailAdd, 1));
                if (!$mail->Send()) {
                    $GLOBALS['log']->debug("Fail to Send Email, Please check settings");
                } else {
                    $GLOBALS['log']->debug('email sent');
                }
				unset($mail);
            }
		}
	}	
	return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/protocol_due_for_annual_review_email_workflow.php

//Add job in job string
$job_strings[] = 'protocol_due_for_annual_review_email_workflow';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function protocol_due_for_annual_review_email_workflow()
{
	global $db;
	$current_date = date("Y-m-d"); //current date
	//$current_date = date("2021-10-27"); //current date

	$next_2_year_date 	=  date("Y-m-d", strtotime("+2 years", strtotime($current_date)));
	$next_1_year_date 	=  date("Y-m-d", strtotime("+1 years", strtotime($current_date)));
	$next_90_date  	  	=  date("Y-m-d", strtotime("+90 days", strtotime($current_date)));
	$next_60_date  	  	=  date("Y-m-d", strtotime("+60 days", strtotime($current_date)));
	$next_30_date 		=  date("Y-m-d", strtotime("+30 days", strtotime($current_date)));
	$next_15_date 		=  date("Y-m-d", strtotime("+15 days", strtotime($current_date)));
	$next_7_date  		=  date("Y-m-d", strtotime("+7 days",  strtotime($current_date)));

	//Get site url so that appropriate Work Product can be shown in the Email Template
	$site_url		= $GLOBALS['sugar_config']['site_url'];
	$domainToSearch = 'apsemail.com';
	$emailBody 		= "";

	$queryCustom = "SELECT  WP.id AS workProductID,
						WP.name AS workProductName, 
						WP.assigned_user_id AS Assigneduser,
						WPCSTM.contact_id_c AS StudyDirector,
						WPCSTM.iacuc_first_annual_review_c,
						WPCSTM.iacuc_second_annual_review_c,
						WPCSTM.iacuc_triennial_review_c,
						WPCSTM.first_annual_review_c,
						WPCSTM.second_annual_review_c,
						WPCSTM.triennial_review_c,
					CONCAT(SDContact.first_name,' ', SDContact.last_name) AS StudyDirectorName							
					FROM `m03_work_product` AS WP
					LEFT JOIN m03_work_product_cstm AS WPCSTM
						ON WP.id = WPCSTM.id_c
					LEFT JOIN contacts AS SDContact
					ON SDContact.id=WPCSTM.contact_id_c
					WHERE  WPCSTM.iacuc_protocol_status_c =  'Active' 
					AND WPCSTM.triennial_review_c = 0
					AND (   WPCSTM.iacuc_triennial_review_c = '" . $next_2_year_date . "'
					OR WPCSTM.iacuc_triennial_review_c = '" . $next_1_year_date . "'
					OR WPCSTM.iacuc_triennial_review_c = '" . $next_90_date . "'
					OR WPCSTM.iacuc_triennial_review_c = '" . $next_60_date . "'						 	
					OR WPCSTM.iacuc_triennial_review_c = '" . $next_30_date . "'
					OR WPCSTM.iacuc_triennial_review_c = '" . $next_15_date . "'						 
					OR (WPCSTM.iacuc_triennial_review_c <= '" . $next_7_date . "' AND  WPCSTM.iacuc_triennial_review_c >= '" . $current_date . "')
					)
					AND WP.deleted = 0 
					ORDER BY StudyDirectorName ASC";
	//$GLOBALS['log']->fatal("Review Protocol Query ==".$queryCustom);
	$queryWPDResult = $db->query($queryCustom);


	while ($fetchResult = $db->fetchByAssoc($queryWPDResult)) {

		$workProductID		= $fetchResult['workProductID'];
		$workProductName	= $fetchResult['workProductName'];
		$assignedUser_id	= $fetchResult['Assigneduser'];
		$SD_ID				= $fetchResult['StudyDirector'];
		$SD_Name			= $fetchResult['StudyDirectorName'];
		$firstReviewDate			= $fetchResult['iacuc_first_annual_review_c'];
		$secondReviewDate			= $fetchResult['iacuc_second_annual_review_c'];
		$thirdReviewDate			= $fetchResult['iacuc_triennial_review_c'];
		$firstReviewDone			= $fetchResult['first_annual_review_c'];
		$secondReviewDone			= $fetchResult['second_annual_review_c'];
		$thirdReviewDone			= $fetchResult['triennial_review_c'];
		$dueDays					= "";

		if ($workProductID != "") {

			$wpName  = '<a target="_blank" href="' . $site_url . '/#M03_Work_Product/' . $workProductID . '" >' . $workProductName . '</a>';
			$thirdReviewDateformat = date("m-d-Y", strtotime($thirdReviewDate));
			if ($thirdReviewDate == $next_2_year_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC 1 Year Reminder";
				$dueDays = "This protocol has been active with IACUC for 1 year, if work is no longer ongoing please close out with IACUC.";
			}

			if ($thirdReviewDate == $next_1_year_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC 2 Year Reminder";
				$dueDays = "This protocol has been active with IACUC for 2 years, if work is no longer ongoing please close out with IACUC.";
			}

			if ($thirdReviewDate == $next_90_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC Review Reminder";
				$dueDays = "This protocol is due for Triennial Review, please complete the IACUC Protocol Renewal/Closure Form, and protocol re-write if work is ongoing, prior to " . $thirdReviewDateformat . ".";
			}

			if ($thirdReviewDate == $next_60_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC Review Reminder";
				$dueDays = "This protocol is due for Triennial Review, please complete the IACUC Protocol Renewal/Closure Form, and protocol re-write if work is ongoing, prior to " . $thirdReviewDateformat . ".";
			}

			if ($thirdReviewDate == $next_30_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC Review Reminder";
				$dueDays = "This protocol is due for Triennial Review, please complete the IACUC Protocol Renewal/Closure Form, and protocol re-write if work is ongoing, prior to " . $thirdReviewDateformat . ".";
			}

			if ($thirdReviewDate == $next_15_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC Review Reminder";
				$dueDays = "This protocol is due for Triennial Review, please complete the IACUC Protocol Renewal/Closure Form, and protocol re-write if work is ongoing, prior to " . $thirdReviewDateformat . ".";
			}

			if ($thirdReviewDate >= $current_date && $thirdReviewDate <= $next_7_date && $thirdReviewDone == 0) {
				$email_subject = "IACUC Review Reminder";
				$dueDays = "This protocol is due for Triennial Review, please complete the IACUC Protocol Renewal/Closure Form, and protocol re-write if work is ongoing, prior to " . $thirdReviewDateformat . ".";
			}



			/*Get Study Director*/
			if ($SD_ID != "") {
				$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);

				$domain_name = substr(strrchr($SDEmailAddress, "@"), 1);
				if (trim(strtolower($domain_name) != $domainToSearch)) {
					$SDEmailAddress 	= "";
					$studyDirectorName	= "";
				}
			}

			if ($dueDays != "") {
				///Upload Document Reminder Template 
				$template = new EmailTemplate();
				$emailObj = new Email();

				$template->retrieve_by_string_fields(array('name' => 'Notification of Protocol Due for Annual Review', 'type' => 'email'));

				$template->body_html = str_replace('[DUE_WP]', $wpName, $template->body_html);
				$template->body_html = str_replace('[DUE_DAYS]', $dueDays, $template->body_html);
				//$template->body_html .= $emailString;

				$defaults	= $emailObj->getSystemDefaultEmail();
				$mail		= new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->IsHTML(true);
				$mail->From		= $defaults['email'];
				$mail->FromName = $defaults['name'];
				$mail->Subject	= $email_subject;
				$mail->Body		= $template->body_html;

				//$mail->AddAddress('ehollen@apsemail.com');
				$mail->AddAddress('cleet@apsemail.com');
				/* #1939 add mail to flow and Study Director of the Work Product :  5 Jan 2022  */
				$mail->AddAddress('calcox@apsemail.com');
				$mail->AddAddress('lzugschwert@apsemail.com');
				$mail->AddAddress('lwalz@apsemail.com');
				$mail->AddAddress('IACUCSubmissions@apsemail.com');
				$mail->AddAddress($SDEmailAddress);
				$wp_emailAdd[] = 'ehollen@apsemail.com';


				foreach ($wp_emailAdd as $wmail) {
					$domain_name = substr(strrchr($wmail, "@"), 1);
					if (trim(strtolower($domain_name) == $domainToSearch)) {
						$mail->AddAddress($wmail);
					}
				}
				$mail->AddBCC('vsuthar@apsemail.com'); //to be deleted 

				//If their exist some valid email addresses then send email
				if (!empty($wp_emailAdd)) {
					if (!$mail->Send()) {
						$GLOBALS['log']->fatal("Fail to Send Email, Please check settings");
					} else {
						$GLOBALS['log']->fatal('email sent for ');
					}
					unset($mail);
				}
			}
		}
	}
	return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/gd_status_change_daily_reminder.php

//Add job in job string
$job_strings['gd_status_change_daily_reminder'] = 'gd_status_change_daily_reminder';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function gd_status_change_daily_reminder()
{
	global $db;
	$current_date = date("Y-m-d"); //current date	
	$yesterdaydate	= date("Y-m-d", strtotime("-1 days", strtotime($current_date)));
	$current_date	= $current_date . ' 05:59:59';
	$yesterdaydate	= $yesterdaydate . ' 06:00:00';
	$GLOBALS['log']->fatal("yesterdaydate ". $yesterdaydate);	
	$GLOBALS['log']->fatal("current_date ". $current_date);	

	/**/
	$reportSQL = "SELECT IFNULL(gd_group_design.id,'') primaryid
	,IFNULL(gd_group_design.name,'') gd_name
	,IFNULL(gd_group_design_cstm.status_c,'') GD_GROUP_DESIGN_CSTM_SA89250,IFNULL(gd_group_design.id,'') gd_group_design_id
	,l1_cstm.contact_id_c contact_id,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contacts_name,
	gd_audit.after_value_string AS after_value,
	gd_audit.date_created AS Date_c
	FROM gd_group_design
	 INNER JOIN  m03_work_product_gd_group_design_1_c l1_1 ON gd_group_design.id=l1_1.m03_work_product_gd_group_design_1gd_group_design_idb AND l1_1.deleted=0
	
	 INNER JOIN  m03_work_product l1 ON l1.id=l1_1.m03_work_product_gd_group_design_1m03_work_product_ida AND l1.deleted=0
	LEFT JOIN gd_group_design_cstm gd_group_design_cstm ON gd_group_design.id = gd_group_design_cstm.id_c
	LEFT JOIN m03_work_product_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN contacts contacts1 ON contacts1.id = l1_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
	LEFT JOIN gd_group_design_audit gd_audit ON gd_group_design.id = gd_audit.parent_id
	 WHERE (((l1_cstm.test_system_c IN ('Bovine','Canine','Caprine','Micro Yucatans','Ossabaw','Ovine','PorcineGeneral','Porcine Got','Porcine','Porcine Yuc','Porcine and Bovine','Porcine_Canine_Ovine','Yorkshire Yucatan')
	))) 
	AND gd_audit.after_value_string ='complete'
	AND (gd_audit.date_created - INTERVAL 360 MINUTE >= '" . $yesterdaydate . "' AND gd_audit.date_created - INTERVAL 360 MINUTE <= '" . $current_date . "')
	AND  gd_group_design.deleted=0 
	 AND IFNULL(contacts1.deleted,0)=0 group by gd_audit.parent_id ORDER BY gd_name ASC";
	 $GLOBALS['log']->fatal("reportSQL ". $reportSQL);	
	$reportResult	= $db->query($reportSQL);

	if ($reportResult->num_rows > 0) {
		$site_url		= $GLOBALS['sugar_config']['site_url'];
		//Prepare the Email Template to send to appropriate members
		$template		= new EmailTemplate();
		$emailObj		= new Email();
		$template->retrieve_by_string_fields(array('name' => 'Completed Group Design for Review', 'type' => 'email'));
		$wp_emailAdd = array();
		//Email Body Header
		$emailBody = "<table width='50%' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 13px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>Group Designs</th></tr>";
		while ($fetchReportResult = $db->fetchByAssoc($reportResult)) {
			$GDID					= $fetchReportResult['primaryid'];
			$GD_Name				= $fetchReportResult['gd_name'];
			$SD_ID				 	= $fetchReportResult['contact_id'];

			/*Get Study Director*/
			if ($SD_ID != "") {
				$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
				$wp_emailAdd[] 		= $SDEmailAddress;
			}
			$wp_emailAdd = array_values($wp_emailAdd); //re-index the array elements
			$wp_emailAdd = array_unique($wp_emailAdd);
			$Group_Design_Name		= '<a target="_blank" href="' . $site_url . '/#GD_Group_Design/' . $GDID . '" >' . $GD_Name . '</a>';

			$emailBody .= "<tr><td>" . $Group_Design_Name . "</td></tr>";
		}

		$emailBody .= "</table>";
		$template->body_html = str_replace('[Completed_GD_Report_First]', $emailBody, $template->body_html);
		//$GLOBALS['log']->fatal("valid email addresses are ". print_r($template->body_html, 1));			
		$defaults		= $emailObj->getSystemDefaultEmail();
		$mail			= new SugarPHPMailer();
		$mail->setMailerForSystem();
		$mail->IsHTML(true);
		$mail->From		= $defaults['email'];
		$mail->FromName = $defaults['name'];
		$mail->Subject	= $template->subject;
		$mail->Body		= $template->body_html;		

		$mail->AddAddress('adargis@apsemail.com');
		$mail->AddAddress('nuitermarkt@apsemail.com');
		//$mail->AddAddress('arice@apsemail.com');
		//$mail->AddAddress('fxs_mjohnson@apsemail.com');

		foreach ($wp_emailAdd as $wmail) { 
			$mail->AddAddress($wmail); 
		}

		//If their exist some valid email addresses then send email
		if (!$mail->Send()) {
			$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
		} else {
			$GLOBALS['log']->debug('email sent');
		}
		unset($mail);
	}

	return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/gd_status_change_daily_reminder_batch_id.php

//Add job in job string
$job_strings['gd_status_change_daily_reminder_batch_id'] = 'gd_status_change_daily_reminder_batch_id';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function gd_status_change_daily_reminder_batch_id()
{
	global $db;
	$current_date = date("Y-m-d"); //current date	
	$yesterdaydate	= date("Y-m-d", strtotime("-1 days", strtotime($current_date)));
	$current_date	= $current_date . ' 05:59:59';
	$yesterdaydate	= $yesterdaydate . ' 06:00:00';
	$GLOBALS['log']->fatal("yesterdaydate ". $yesterdaydate);	
	$GLOBALS['log']->fatal("current_date ". $current_date);	


	/**/
	$reportSQL = "SELECT IFNULL(gd_group_design.id,'') primaryid
	,IFNULL(gd_group_design.name,'') gd_name
	,IFNULL(gd_group_design.id,'') gd_group_design_id
	,l2_cstm.contact_id_c contact_id,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contacts_name,
	gd_audit.after_value_string AS after_value,
	gd_audit.date_created AS Date_c
	FROM gd_group_design
	INNER JOIN  bid_batch_id_gd_group_design_1_c l1_1 ON gd_group_design.id=l1_1.bid_batch_id_gd_group_design_1gd_group_design_idb AND l1_1.deleted=0

	INNER JOIN  bid_batch_id l1 ON l1.id=l1_1.bid_batch_id_gd_group_design_1bid_batch_id_ida AND l1.deleted=0
	INNER JOIN  bid_batch_id_m03_work_product_1_c l2_1 ON l1.id=l2_1.bid_batch_id_m03_work_product_1bid_batch_id_ida AND l2_1.deleted=0

	INNER JOIN  m03_work_product l2 ON l2.id=l2_1.bid_batch_id_m03_work_product_1m03_work_product_idb AND l2.deleted=0
	LEFT JOIN m03_work_product_cstm l2_cstm ON l2.id = l2_cstm.id_c
	LEFT JOIN contacts contacts1 ON contacts1.id = l2_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
		LEFT JOIN gd_group_design_audit gd_audit ON gd_group_design.id = gd_audit.parent_id
	WHERE (((l2_cstm.test_system_c IN ('Bovine','Canine','Caprine','Micro Yucatans','Ossabaw','Ovine','PorcineGeneral','Porcine Got','Porcine','Porcine Yuc','Porcine and Bovine','Porcine_Canine_Ovine','Yorkshire Yucatan')
	)))
	AND gd_audit.after_value_string ='complete'
	AND (gd_audit.date_created - INTERVAL 360 MINUTE >= '" . $yesterdaydate . "' AND gd_audit.date_created - INTERVAL 360 MINUTE <= '" . $current_date . "')
	AND  gd_group_design.deleted=0 
	AND IFNULL(contacts1.deleted,0)=0 group by gd_audit.parent_id ORDER BY gd_name ASC";

	$reportResult	= $db->query($reportSQL);

	if ($reportResult->num_rows > 0) {
		$site_url		= $GLOBALS['sugar_config']['site_url'];
		//Prepare the Email Template to send to appropriate members
		$template		= new EmailTemplate();
		$emailObj		= new Email();
		$template->retrieve_by_string_fields(array('name' => 'Completed Group Design for Review', 'type' => 'email'));
		$wp_emailAdd = array();
		//Email Body Header
		$emailBody = "<table width='50%' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 13px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>Group Designs</th></tr>";
		while ($fetchReportResult = $db->fetchByAssoc($reportResult)) {
			$GDID					= $fetchReportResult['primaryid'];
			$GD_Name				= $fetchReportResult['gd_name'];
			$SD_ID				 	= $fetchReportResult['contact_id'];

			/*Get Study Director*/
			if ($SD_ID != "") {
				$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
				$wp_emailAdd[] 		= $SDEmailAddress;
			}
			$wp_emailAdd = array_values($wp_emailAdd); //re-index the array elements
			$wp_emailAdd = array_unique($wp_emailAdd);
			$Group_Design_Name		= '<a target="_blank" href="' . $site_url . '/#GD_Group_Design/' . $GDID . '" >' . $GD_Name . '</a>';

			$emailBody .= "<tr><td>" . $Group_Design_Name . "</td></tr>";
		}

		$emailBody .= "</table>";
		$template->body_html = str_replace('[Completed_GD_Report_First]', $emailBody, $template->body_html);
		//$GLOBALS['log']->fatal("valid email addresses are ". print_r($template->body_html, 1));			
		$defaults		= $emailObj->getSystemDefaultEmail();
		$mail			= new SugarPHPMailer();
		$mail->setMailerForSystem();
		$mail->IsHTML(true);
		$mail->From		= $defaults['email'];
		$mail->FromName = $defaults['name'];
		$mail->Subject	= $template->subject;
		$mail->Body		= $template->body_html;

		$mail->AddAddress('adargis@apsemail.com');
		$mail->AddAddress('nuitermarkt@apsemail.com');
		//$mail->AddAddress('arice@apsemail.com');

		foreach ($wp_emailAdd as $wmail) {
			$mail->AddAddress($wmail);
		}

		if (!$mail->Send()) {
			$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
		} else {
			$GLOBALS['log']->debug('email sent');
		}
		unset($mail);
	}

	return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/upcoming_tissue_trimming_daily_reminder.php

//Add job in job string
$job_strings['upcoming_tissue_trimming_daily_reminder'] = 'upcoming_tissue_trimming_daily_reminder';
/* * This function will found out all the pending workproduct Assessment in Communication. */

function upcoming_tissue_trimming_daily_reminder()
{
	global $db;
	$current_date = date("Y-m-d"); //current date	
	global $app_list_strings;
	$compliance_dom         	= $app_list_strings['work_product_compliance_list'];	
  
	/**/
	$reportSQL = "SELECT IFNULL(m03_work_product_deliverable.id,'') primaryid,IFNULL(m03_work_product_deliverable.name,'') Deliverable_Name,IFNULL(m03_work_product_deliverable_cstm.timeline_type_c,'') Timeline,IFNULL(l1.id,'') sd_id
	,IFNULL(l1.name,'') sd_name
	,l1_cstm.contact_id_c contact_id,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contact_name,IFNULL(l1_cstm.work_product_compliance_c,'') Compliance,IFNULL(l2.id,'') lead_audi_id
	,IFNULL(l2.first_name,'') l2_first_name
	,IFNULL(l2.last_name,'') l2_last_name
	,IFNULL(l2.salutation,'') l2_salutation,IFNULL(l2.title,'') title,m03_work_product_deliverable_cstm.due_date_c due_date,m03_work_product_deliverable_cstm.work_product_deliverable_not_c notes
	FROM m03_work_product_deliverable
	LEFT JOIN  m03_work_product_m03_work_product_deliverable_1_c l1_1 ON m03_work_product_deliverable.id=l1_1.m03_work_pe584verable_idb AND l1_1.deleted=0	
	LEFT JOIN  m03_work_product l1 ON l1.id=l1_1.m03_work_p0b66product_ida AND l1.deleted=0
	LEFT JOIN  contacts_m03_work_product_2_c l2_1 ON l1.id=l2_1.contacts_m03_work_product_2m03_work_product_idb AND l2_1.deleted=0
	LEFT JOIN  contacts l2 ON l2.id=l2_1.contacts_m03_work_product_2contacts_ida AND l2.deleted=0
	LEFT JOIN m03_work_product_deliverable_cstm m03_work_product_deliverable_cstm ON m03_work_product_deliverable.id = m03_work_product_deliverable_cstm.id_c
	LEFT JOIN m03_work_product_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN contacts contacts1 ON contacts1.id = l1_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
	LEFT JOIN contacts contacts2 ON contacts2.id = l1_cstm.contact_id4_c AND IFNULL(contacts2.deleted,0)=0 
	
	WHERE (((m03_work_product_deliverable_cstm.deliverable_status_c NOT IN ('Completed','Sponsor Retracted','Not Performed') OR  m03_work_product_deliverable_cstm.deliverable_status_c IS NULL 
	) AND ((coalesce(LENGTH(m03_work_product_deliverable_cstm.due_date_c), 0) <> 0)) AND (m03_work_product_deliverable_cstm.deliverable_c = 'Tissue Trimming'
	))) 
	AND  m03_work_product_deliverable.deleted=0 
	 AND IFNULL(contacts1.deleted,0)=0 
	 AND IFNULL(contacts2.deleted,0)=0 
	 ORDER BY CASE WHEN (IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='' OR IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'') IS NULL) THEN 0
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Abnormal Extract' THEN 1
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Data Table' THEN 2
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical ExhaustiveExaggerated Extraction' THEN 3
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Report' THEN 4
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Headspace Analysis' THEN 5
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Interim Validation Report' THEN 6
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Method' THEN 7
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Method Development' THEN 8
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Nickel Ion Release Extraction' THEN 9
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Pre testing' THEN 10
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Protocol Dvelopment' THEN 11
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Specimen Analysis' THEN 12
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Sample Prep' THEN 13
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Analytical Simulated Use Extraction' THEN 14
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Stability Analysis' THEN 15
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Bioanalytical Validation Report' THEN 16
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Animal Health Report' THEN 17
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Animal Selection Populate TSD' THEN 18
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Animal Selection Procedure Use' THEN 19
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='BC Abnormal Study Article' THEN 20
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Biological Evaluation Plan' THEN 21
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Chemical and Physical Characterization of Particulates' THEN 22
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Contributing Scientist Report Amendment' THEN 23
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Critical Phase Audit' THEN 24
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Decalcification' THEN 25
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Early DeathTermination Investigation Report' THEN 26
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Faxitron' THEN 27
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Final Report' THEN 28
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Final Report Amendment' THEN 29
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Frozen Slide Completion' THEN 30
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='GLP Discontinuation Report' THEN 31
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Gross Morphometry' THEN 32
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Gross Pathology Report' THEN 33
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histology Controls' THEN 34
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histomorphometry' THEN 35
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histopathology Methods Report' THEN 36
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histopathology Report2' THEN 37
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='IACUC Submission' THEN 38
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='In Life Report' THEN 39
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Interim Animal Health Report' THEN 40
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Interim Gross Pathology Report' THEN 41
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Interim Histopathology Report2' THEN 42
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Interim In Life Report' THEN 43
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Interim Histopathology Report' THEN 44
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Interim Report' THEN 45
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Paraffin Slide Completion' THEN 46
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Pathology Unknown' THEN 47
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Pathology Misc' THEN 48
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histopathology Notes' THEN 49
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Pathology Protocol Review' THEN 50
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histopathology Report' THEN 51
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Pharmacokinetic Report' THEN 52
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Pharmacology Data Summary' THEN 53
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='EXAKT Slide Completion' THEN 54
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Plastic Microtome Slide Completion' THEN 55
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Protocol Development' THEN 56
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='QA Data Review' THEN 57
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='QC Data Review' THEN 58
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Recuts' THEN 59
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Regulatory Response' THEN 60
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Risk Assessment Report' THEN 61
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Risk Assessment Report Review' THEN 62
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='SEM' THEN 63
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='SEND Report' THEN 64
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Slide Scanning' THEN 65
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='SpecimenSample Disposition' THEN 66
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Sponsor Contracted Contributing Scientist Report' THEN 67
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Statistical Review' THEN 68
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Statistical Summary' THEN 69
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Statistics Report' THEN 70
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Summary Certificate of ISO 10993' THEN 71
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Summary Certificate of USP Class VI Testing' THEN 72
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Tissue Receipt' THEN 73
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Tissue Trimming' THEN 74
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='TWT Protocol Review' THEN 75
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='TWT Report Review' THEN 76
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Waiting on external test site' THEN 77
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Animal Selection' THEN 78
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Histopathology and Histomorphometry Report' THEN 79
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Tissue Shipping' THEN 80
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='SEM Prep' THEN 81
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='SEM Report' THEN 82
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Shipping Request' THEN 83
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Slide Completion' THEN 84
	WHEN IFNULL(m03_work_product_deliverable_cstm.deliverable_c,'')='Slide Shipping' THEN 85 ELSE 86 END ASC
	,due_date ASC";

	$reportResult	= $db->query($reportSQL);
	
	if($reportResult->num_rows>0){
		$site_url		= $GLOBALS['sugar_config']['site_url'];
		//Prepare the Email Template to send to appropriate members
		$template		= new EmailTemplate();
		$emailObj		= new Email();		
		$template->retrieve_by_string_fields(array('name' => 'Upcoming Tissue Trimming WPDs', 'type' => 'email'));
		
		//Email Body Header
		$emailBody = "<table width='100%' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 13px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>Deliverable Name</th><th bgcolor='#b3d1ff' align='left'>Timeline</th><th bgcolor='#b3d1ff' align='left'>Study ID </th><th bgcolor='#b3d1ff' align='left'>Study Director</th><th bgcolor='#b3d1ff' align='left'>Compliance</th><th bgcolor='#b3d1ff' align='left'>Lead Auditor (GLP Only)</th><th bgcolor='#b3d1ff' align='left'>Draft Due Date</th><th bgcolor='#b3d1ff' align='left'>Notes</th></tr>";
					
		while($fetchReportResult = $db->fetchByAssoc($reportResult)) {
			$wpdID					= $fetchReportResult['primaryid'];
			$Deliverable_Name		= $fetchReportResult['Deliverable_Name'];
			$Timeline				= $fetchReportResult['Timeline'];
			$sd_id					= $fetchReportResult['sd_id'];
			$sd_name				= $fetchReportResult['sd_name'];
			$contact_id				= $fetchReportResult['contact_id'];
			$contact_name			= $fetchReportResult['contact_name'];
			$Compliance  			= $compliance_dom[$fetchReportResult['Compliance']]; 
			$lead_audi_id  			= $fetchReportResult['lead_audi_id'];
			$lead_audi_f_name		= $fetchReportResult['l2_first_name'];
			$lead_audi_l_name		= $fetchReportResult['l2_last_name'];
			$due_date  				= date("m/d/Y", strtotime($fetchReportResult['due_date']));
			$notes					= $fetchReportResult['notes'];

				
			$Deliverable_Name		= '<a target="_blank" href="'.$site_url.'/#M03_Work_Product_Deliverable/'.$wpdID.'" >'.$Deliverable_Name . '</a>';			
			$wpLink		= '<a target="_blank" href="'.$site_url.'/#M03_Work_Product/'.$sd_id.'" >'.$sd_name . '</a>';
			$sdLink		= '<a target="_blank" href="'.$site_url.'/#Contacts/'.$contact_id.'" >'.$contact_name . '</a>';
			$lead_audiLink		= '<a target="_blank" href="'.$site_url.'/#Contacts/'.$lead_audi_id.'" >'.$lead_audi_f_name . ' '.$lead_audi_l_name . '</a>';
						
			$emailBody .= "<tr><td>".$Deliverable_Name."</td><td>".$Timeline."</td><td>".$wpLink."</td><td>".$sdLink."</td><td>".$Compliance."</td><td>".$lead_audiLink."</td><td>".$due_date."</td><td>".$notes."</td></tr>";
		}
	
		$emailBody .="</table>";
		$template->body_html = str_replace('[Tissue_Trimming_Report]', $emailBody, $template->body_html);
		//$GLOBALS['log']->fatal("valid email addresses are ". print_r($template->body_html, 1));			
		$defaults		= $emailObj->getSystemDefaultEmail();
		$mail			= new SugarPHPMailer();
		$mail->setMailerForSystem();
		$mail->IsHTML(true);
		$mail->From		= $defaults['email'];
		$mail->FromName = $defaults['name'];
		$mail->Subject	= $template->subject;
		$mail->Body		= $template->body_html;
				
		$mail->AddAddress('anelson@apsemail.com');
		$mail->AddAddress('QA.Auditors@apsemail.com');
		$mail->AddAddress('necropsy@apsemail.com');  
		$mail->AddAddress('jblanch@apsemail.com'); 
		$mail->AddAddress('rhanson@apsemail.com');
		$mail->AddAddress('malhunaidi@apsemail.com');
		//$mail->AddAddress('fxs_mjohnson@apsemail.com'); 
		//If their exist some valid email addresses then send email
					
		if (!$mail->Send()) {
			$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
		} else {
			$GLOBALS['log']->debug('email sent' );
		}
		unset($mail);	
	}
			 
	return true;
}
?>
