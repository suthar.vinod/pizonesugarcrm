<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.overdue_deliverables.php


$mod_strings['LBL_OVERDUE_DELIVERABLES'] = 'Overdue Deliverables';


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.communication_requiring_assessment.php

$mod_strings['LBL_COMMUNICATION_REQUIRING_ASSESSMENT'] = 'Communication Requiring Assessment';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.deceased_animal_communication.php

$mod_strings['LBL_DECEASED_ANIMAL_COMMUNICATION'] = 'Deceased Animal Communication';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.communication_requiring_assessment_consolidate.php

$mod_strings['LBL_COMMUNICATION_REQUIRING_ASSESSMENT_CONSOLIDATE'] = 'Communication Requiring Assessment Consolidate';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.wpd_upload_document_reminder.php

$mod_strings['LBL_WPD_UPLOAD_DOCUMENT_REMINDER'] = 'WPD Upload Document Reminder';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.management_comm_review_daily_reminder.php

$mod_strings['LBL_MANAGEMENT_COMM_REVIEW_DAILY_REMINDER'] = 'Management Comm Review Daily Reminder';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.overdue_acknowledgement_daily_reminder.php

$mod_strings['LBL_OVERDUE_ACKNOWLEDGEMENT_DAILY_REMINDER'] = 'Overdue Acknowledgement Daily Reminder';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.primary_animal_calculation_for_aps001.php

$mod_strings['LBL_PRIMARY_ANIMAL_CALCULATION_FOR_APS001'] = 'Primary Animal Calculation For APS001';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.create_sc_species_census_record.php

$mod_strings['LBL_CREATE_SC_SPECIES_CENSUS_RECORD'] = 'Create SC Species Census Record';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.add_relation_for_missed_communication_record.php

$mod_strings['LBL_ADD_RELATION_FOR_MISSED_COMMUNICATION_RECORD'] = 'Add Relation For Missed Communication Record';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.equip_service_workflow_reminder.php

$mod_strings['LBL_EQUIP_SERVICE_WORKFLOW_REMINDER'] = 'Equip Service Workflow Reminder';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.overdue_to_qa_email_workflow.php

$mod_strings['LBL_OVERDUE_TO_QA_EMAIL_WORKFLOW'] = 'Overdue To QA Email Workflow';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.protocol_due_for_annual_review_email_workflow.php

$mod_strings['LBL_PROTOCOL_DUE_FOR_ANNUAL_REVIEW_EMAIL_WORKFLOW'] = 'Protocol Due For Annual Review Email Workflow';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.send_analyse_by_date_notification_mail.php

$mod_strings['LBL_SEND_ANALYSE_BY_DATE_NOTIFICATION_MAIL'] = 'Send analyse by date notification mail';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.get_days_to_expiration_date.php

$mod_strings['LBL_GET_DAYS_TO_EXPIRATION_DATE'] = 'Get Days to expiration Date';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.send_email_for_biocomp_report.php

$mod_strings['LBL_SEND_EMAIL_FOR_BIOCOMP_REPORT'] = 'Send Email For Biocomp Report';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.upcoming_tissue_trimming_daily_reminder.php


$mod_strings['LBL_UPCOMING_TISSUE_TRIMMING_DAILY_REMINDER'] = 'Upcoming Tissue Trimming WPDs';


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.send_email_for_cpi_report.php

$mod_strings['LBL_SEND_EMAIL_FOR_CPI_REPORT'] = 'Send Email For CPI Report';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.gd_status_change_daily_reminder.php


$mod_strings['LBL_GD_STATUS_CHANGE_DAILY_REMINDER'] = 'GD Status Change email summary';


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.gd_status_change_daily_reminder_second.php


$mod_strings['LBL_GD_STATUS_CHANGE_DAILY_REMINDER_SECOND'] = 'GD Status Change email summary second';


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.td_single_date_recalculate.php


$mod_strings['LBL_TD_SINGLE_DATE_RECALCULATE'] = 'TD Single Date Recalculate';


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.gd_status_change_daily_reminder_batch_id.php


$mod_strings['LBL_GD_STATUS_CHANGE_DAILY_REMINDER_BATCH_ID'] = 'GD Status Change email summary for Batch ID';


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.gd_status_change_daily_reminder_batch_id_second.php


$mod_strings['LBL_GD_STATUS_CHANGE_DAILY_REMINDER_BATCH_ID_SECOND'] = 'GD Status Change email summary for batch ID second';


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.daily_scheduling_needs_sa.php

$mod_strings['LBL_DAILY_SCHEDULING_NEEDS_SA'] = 'Daily Scheduling Needs - SA';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/en_us.send_daily_review_to_in_life_managers.php

$mod_strings['LBL_SEND_DAILY_REVIEW_TO_IN_LIFE_MANAGERS'] = 'Send Daily Review to in Life Managers';
?>
