<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RRD_Regulatory_Response_Doc/Ext/Vardefs/sugarfield_document_name.php

 // created: 2019-07-09 12:09:13
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['audited']=true;
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['massupdate']=false;
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['merge_filter']='disabled';
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['unified_search']=false;
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['RRD_Regulatory_Response_Doc']['fields']['document_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RRD_Regulatory_Response_Doc/Ext/Vardefs/rrd_regulatory_response_doc_rr_regulatory_response_1_RRD_Regulatory_Response_Doc.php

// created: 2019-07-09 12:11:34
$dictionary["RRD_Regulatory_Response_Doc"]["fields"]["rrd_regulatory_response_doc_rr_regulatory_response_1"] = array (
  'name' => 'rrd_regulatory_response_doc_rr_regulatory_response_1',
  'type' => 'link',
  'relationship' => 'rrd_regulatory_response_doc_rr_regulatory_response_1',
  'source' => 'non-db',
  'module' => 'RR_Regulatory_Response',
  'bean_name' => 'RR_Regulatory_Response',
  'vname' => 'LBL_RRD_REGULATORY_RESPONSE_DOC_RR_REGULATORY_RESPONSE_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'id_name' => 'rrd_regulac86besponse_idb',
);

?>
