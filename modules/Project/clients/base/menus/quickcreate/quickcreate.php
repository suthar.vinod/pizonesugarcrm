<?php
// created: 2016-01-06 02:12:14
$viewdefs['Project']['base']['menu']['quickcreate'] = array (
  'layout' => 'create',
  'label' => 'LNK_NEW_PROJECT',
  'visible' => true,
  'icon' => 'fa-plus',
  'order' => 5,
);