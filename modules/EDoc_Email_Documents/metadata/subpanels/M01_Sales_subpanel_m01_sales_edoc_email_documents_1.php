<?php
// created: 2019-08-27 11:39:16
$subpanel_layout['list_fields'] = array (
  'document_name' => 
  array (
    'name' => 'document_name',
    'vname' => 'LBL_LIST_DOCUMENT_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'uploadfile' => 
  array (
    'type' => 'file',
    'vname' => 'LBL_FILE_UPLOAD',
    'width' => 10,
    'default' => true,
  ),
  'type_2' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TYPE_2',
    'width' => 10,
  ),
  'department' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_DEPARTMENT',
    'width' => 10,
  ),
);