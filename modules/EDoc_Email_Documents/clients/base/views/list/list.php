<?php
$module_name = 'EDoc_Email_Documents';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'document_name',
                'label' => 'LBL_NAME',
                'link' => true,
                'default' => true,
                'enabled' => true,
              ),
              1 => 
              array (
                'name' => 'uploadfile',
                'label' => 'LBL_FILE_UPLOAD',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'department',
                'label' => 'LBL_DEPARTMENT',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'm01_sales_edoc_email_documents_1_name',
                'label' => 'LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_M01_SALES_TITLE',
                'enabled' => true,
                'id' => 'M01_SALES_EDOC_EMAIL_DOCUMENTS_1M01_SALES_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'm03_work_product_edoc_email_documents_1_name',
                'label' => 'LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE',
                'enabled' => true,
                'id' => 'M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1M03_WORK_PRODUCT_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED_USER',
                'module' => 'Users',
                'id' => 'USERS_ID',
                'default' => false,
                'sortable' => false,
                'related_fields' => 
                array (
                  0 => 'modified_user_id',
                ),
                'enabled' => true,
              ),
              7 => 
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'readonly' => true,
                'default' => false,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
