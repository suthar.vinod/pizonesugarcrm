<?php
// created: 2020-02-05 14:28:41
$viewdefs['EDoc_Email_Documents']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'document_name' => 
    array (
    ),
    'category_id' => 
    array (
    ),
    'subcategory_id' => 
    array (
    ),
    'active_date' => 
    array (
    ),
    'exp_date' => 
    array (
    ),
    'tag' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'm03_work_product_edoc_email_documents_1_name' => 
    array (
    ),
    'm01_sales_edoc_email_documents_1_name' => 
    array (
    ),
  ),
);