<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/EDoc_Email_Documents/Ext/Layoutdefs/edoc_email_documents_a1a_critical_phase_inspectio_1_EDoc_Email_Documents.php

 // created: 2022-02-01 04:24:04
$layout_defs["EDoc_Email_Documents"]["subpanel_setup"]['edoc_email_documents_a1a_critical_phase_inspectio_1'] = array (
  'order' => 100,
  'module' => 'A1A_Critical_Phase_Inspectio',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_EDOC_EMAIL_DOCUMENTS_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'get_subpanel_data' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/EDoc_Email_Documents/Ext/Layoutdefs/_overrideEDoc_Email_Documents_subpanel_edoc_email_documents_a1a_critical_phase_inspectio_1.php

//auto-generated file DO NOT EDIT
$layout_defs['EDoc_Email_Documents']['subpanel_setup']['edoc_email_documents_a1a_critical_phase_inspectio_1']['override_subpanel_name'] = 'EDoc_Email_Documents_subpanel_edoc_email_documents_a1a_critical_phase_inspectio_1';

?>
