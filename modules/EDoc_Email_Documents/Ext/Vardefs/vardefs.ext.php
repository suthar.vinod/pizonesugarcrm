<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/EDoc_Email_Documents/Ext/Vardefs/m01_sales_edoc_email_documents_1_EDoc_Email_Documents.php

// created: 2019-08-27 11:35:41
$dictionary["EDoc_Email_Documents"]["fields"]["m01_sales_edoc_email_documents_1"] = array (
  'name' => 'm01_sales_edoc_email_documents_1',
  'type' => 'link',
  'relationship' => 'm01_sales_edoc_email_documents_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'id_name' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["EDoc_Email_Documents"]["fields"]["m01_sales_edoc_email_documents_1_name"] = array (
  'name' => 'm01_sales_edoc_email_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
  'link' => 'm01_sales_edoc_email_documents_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["EDoc_Email_Documents"]["fields"]["m01_sales_edoc_email_documents_1m01_sales_ida"] = array (
  'name' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE_ID',
  'id_name' => 'm01_sales_edoc_email_documents_1m01_sales_ida',
  'link' => 'm01_sales_edoc_email_documents_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/EDoc_Email_Documents/Ext/Vardefs/m03_work_product_edoc_email_documents_1_EDoc_Email_Documents.php

// created: 2019-08-27 11:37:04
$dictionary["EDoc_Email_Documents"]["fields"]["m03_work_product_edoc_email_documents_1"] = array (
  'name' => 'm03_work_product_edoc_email_documents_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_edoc_email_documents_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'side' => 'right',
  'vname' => 'LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'id_name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
  'link-type' => 'one',
);
$dictionary["EDoc_Email_Documents"]["fields"]["m03_work_product_edoc_email_documents_1_name"] = array (
  'name' => 'm03_work_product_edoc_email_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
  'link' => 'm03_work_product_edoc_email_documents_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'name',
);
$dictionary["EDoc_Email_Documents"]["fields"]["m03_work_product_edoc_email_documents_1m03_work_product_ida"] = array (
  'name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE_ID',
  'id_name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
  'link' => 'm03_work_product_edoc_email_documents_1',
  'table' => 'm03_work_product',
  'module' => 'M03_Work_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/EDoc_Email_Documents/Ext/Vardefs/edoc_email_documents_a1a_critical_phase_inspectio_1_EDoc_Email_Documents.php

// created: 2022-02-01 04:24:04
$dictionary["EDoc_Email_Documents"]["fields"]["edoc_email_documents_a1a_critical_phase_inspectio_1"] = array (
  'name' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'type' => 'link',
  'relationship' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'vname' => 'LBL_EDOC_EMAIL_DOCUMENTS_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'id_name' => 'edoc_emailbf38cuments_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
