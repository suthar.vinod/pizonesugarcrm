<?php
// WARNING: The contents of this file are auto-generated.


// created: 2022-02-01 04:24:04
$viewdefs['EDoc_Email_Documents']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_EDOC_EMAIL_DOCUMENTS_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'context' => 
  array (
    'link' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['EDoc_Email_Documents']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'edoc_email_documents_a1a_critical_phase_inspectio_1',
  'view' => 'subpanel-for-edoc_email_documents-edoc_email_documents_a1a_critical_phase_inspectio_1',
);
