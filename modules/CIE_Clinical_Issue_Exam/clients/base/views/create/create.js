({
    extendsFrom: 'CreateView',

    initialize: function (options) {

        this._super('initialize', [options]);

        //when Musculoskeletal (MS) Data field will change
        this.model.on('change:musculoskeletal_data', this.changeDropdownValue, this);

        //when Musculoskeletal Palpation field will change
        this.model.on('change:musculoskeletal_palpation', this.checkPalpationValue, this);

        // Integument(Skin)data field will change, Lession Wound field will visible
        this.model.on('change:integument_data', this.changeIntegumentData, this);

        //Set Lession Wound Location when "Lession Wound" will change
        this.model.on('change:lesionswound', this.changeLesionsWound, this);

        this.on('render', this.hideLWLocationField, this);
    },

    /**
     * function to set visibility hidden to gait dropdown field
     */
    hideLWLocationField: function () {

        _.each(this.fields, function (field) {
            if (_.isEqual(field.def.name, 'lesionswound_location')) {
                this.$('[data-name=lesionswound_location]').css('visibility', 'hidden');
            }
        }, this);
    },

    showLWLocationField: function () {

        _.each(this.fields, function (field) {
            if (_.isEqual(field.def.name, 'lesionswound_location')) {
                this.$('[data-name=lesionswound_location]').css('visibility', 'visible');
            }
        }, this);

    },

    checkPalpationValue: function () {
        var self = this;
        var palpation = self.model.get('musculoskeletal_palpation');
        if (palpation === 'Normal' || palpation === 'Not Examined') {
            self.showAllFields();
            self.model.set("swelling_location", "Not Applicable");
            self.model.set("swelling_size", "Not Applicable");
            self.model.set("heat_location", "Not Applicable");
            self.model.set("pain_location", "Not Applicable");
        } else if (palpation === 'Heat') {
            self.showAllFields();
            self.model.set("swelling_location", "Not Applicable");
            self.model.set("swelling_size", "Not Applicable");
            self.model.set("heat_location", null);
            self.model.set("pain_location", "Not Applicable");

        } else if (palpation === 'Pain') {
            self.showAllFields();
            self.model.set("swelling_location", "Not Applicable");
            self.model.set("swelling_size", "Not Applicable");
            self.model.set("heat_location", "Not Applicable");
            self.model.set("pain_location", null);

        } else if (palpation === 'Heat and Pain') {
            self.showAllFields();
            self.model.set("swelling_location", "Not Applicable");
            self.model.set("swelling_size", "Not Applicable");
            self.model.set("heat_location", null);
            self.model.set("pain_location", null);

        } else if (palpation === 'Swelling') {
            self.showAllFields();
            self.model.set("swelling_location", null);
            self.model.set("swelling_size", null);
            self.model.set("heat_location", "Not Applicable");
            self.model.set("pain_location", "Not Applicable");

        } else if (palpation === 'Swelling and Heat') {
            self.showAllFields();
            self.model.set("swelling_location", null);
            self.model.set("swelling_size", null);
            self.model.set("heat_location", null);
            self.model.set("pain_location", "Not Applicable");

        } else if (palpation === 'Swelling and Pain') {
            self.showAllFields();
            self.model.set("swelling_location", null);
            self.model.set("swelling_size", null);
            self.model.set("heat_location", "Not Applicable");
            self.model.set("pain_location", null);

        } else if (palpation === 'Swelling Heat Pain') {
            self.showAllFields();
            self.model.set("swelling_location", null);
            self.model.set("swelling_size", null);
            self.model.set("heat_location", null);
            self.model.set("pain_location", null);

        } else if (palpation === '') {
            self.hideAllFields();
        } else {
            self.showAllFields();
            self.model.set("swelling_location", null);
            self.model.set("swelling_size", null);
            self.model.set("heat_location", null);
            self.model.set("pain_location", null);
        }
    },

    changeDropdownValue: function () {
        var self = this;
        if (self.model.get('musculoskeletal_data') === 'All Normal' || self.model.get('musculoskeletal_data') === 'All Not Examined') {
            self.showAllFields();
            self.model.set("swelling_location", "Not Applicable");
            self.model.set("swelling_size", "Not Applicable");
            self.model.set("heat_location", "Not Applicable");
            self.model.set("pain_location", "Not Applicable");
        }

        //If Musculoskeletal Data = Null, then Swelling Location is hidden
        if (self.model.get('musculoskeletal_data') == '') {
            self.hideAllFields();
        }

        if (self.model.get('musculoskeletal_data') !== 'All Normal' && self.model.get('musculoskeletal_data') !== 'All Not Examined') {
            self.model.set("swelling_location", null);
            self.model.set("swelling_size", null);
            self.model.set("heat_location", null);
            self.model.set("pain_location", null);

        }

    },

    changeIntegumentData: function () {
        this.showLWLocationField();
        var self = this;
        //Visible and "Not Applicable" Lession Wound Location field if Lession Wound is not "No"
        //and it will happend when integument_data will "All Normal". "Lession Wound" value is not displaying
        //properly hence visibility is applying on integument_data field
        if (self.model.get('integument_data') === 'All Normal') {
            self.$('span[data-fieldname="lesionswound_location"]').show();
            self.$('.record-label[data-name="lesionswound_location"]').show();
            self.model.set("lesionswound_location", "Not Applicable");

        }
        //Hide Lession Wound Location field if Lession Wound is null
        // and it will null when integument_data will null
        if (self.model.get('integument_data') == '') {

            self.$('span[data-fieldname="lesionswound_location"]').hide();
            self.$('.record-label[data-name="lesionswound_location"]').hide();

        }
        //Visible and null Lession Wound Location field if Lession Wound is not "No"
        if (self.model.get('integument_data') !== 'All Normal' && self.model.get('integument_data') !== '') {

            if (self.model.get('lesionswound') === 'No') {

                self.$('span[data-fieldname="lesionswound_location"]').show();
                self.$('.record-label[data-name="lesionswound_location"]').show();
                self.model.set("lesionswound_location", "Not Applicable");

            } else {
                self.$('span[data-fieldname="lesionswound_location"]').hide();
                self.$('.record-label[data-name="lesionswound_location"]').hide();
                self.model.set("lesionswound_location", null);
            }

        }
    },

    changeLesionsWound: function () {
        this.showLWLocationField();
        var self = this;

        //Visible and "Not Applicable" Lession Wound Location field if Lession Wound is not "No"
        //and it will happend when integument_data will "All Normal". "Lession Wound" value is not displaying
        //properly hence visibility is applying on integument_data field
        if (self.model.get('lesionswound') === 'No') {

            self.$('span[data-fieldname="lesionswound_location"]').show();
            self.$('.record-label[data-name="lesionswound_location"]').show();
            self.model.set("lesionswound_location", "Not Applicable");

        }
        //Hide Lession Wound Location field if Lession Wound is null
        // and it will null when integument_data will null
        if (self.model.get('lesionswound') == '' || self.model.get('lesionswound') === "Not Examined") {

            self.$('span[data-fieldname="lesionswound_location"]').hide();
            self.$('.record-label[data-name="lesionswound_location"]').hide();

        }
        //Visible and null Lession Wound Location field if Lession Wound is not "No"
        if (self.model.get('lesionswound') !== 'No' && self.model.get('lesionswound') !== '' && self.model.get('lesionswound') !== "Not Examined") {

            self.$('span[data-fieldname="lesionswound_location"]').show();
            self.$('.record-label[data-name="lesionswound_location"]').show();
            self.model.set("lesionswound_location", null);

        }
    },
    /**
     * This function will set the visibility of Swelling Location, Swelling Size, Heat Location and
     * Pain Location fields
     */
    showAllFields: function () {
        var self = this;
        self.$('span[data-fieldname="swelling_location"]').show();
        self.$('.record-label[data-name="swelling_location"]').show();
        self.$('span[data-fieldname="swelling_size"]').show();
        self.$('.record-label[data-name="swelling_size"]').show();
        self.$('span[data-fieldname="heat_location"]').show();
        self.$('.record-label[data-name="heat_location"]').show();
        self.$('span[data-fieldname="pain_location"]').show();
        self.$('.record-label[data-name="pain_location"]').show();
    },

    /**
     * This function will unset the visibility of Swelling Location, Swelling Size, Heat Location and
     * Pain Location fields
     */
    hideAllFields: function () {
        var self = this;
        self.$('span[data-fieldname="swelling_location"]').hide();
        self.$('.record-label[data-name="swelling_location"]').hide();
        self.$('span[data-fieldname="swelling_size"]').hide();
        self.$('.record-label[data-name="swelling_size"]').hide();
        self.$('span[data-fieldname="heat_location"]').hide();
        self.$('.record-label[data-name="heat_location"]').hide();
        self.$('span[data-fieldname="pain_location"]').hide();
        self.$('.record-label[data-name="pain_location"]').hide();
    },
    _render: function () {
        var self = this;
        self._super('_render');
        //If Musculoskeletal Data = Null, then Swelling Location | Swelling Size | Heat Location
        // | Pain Location are hidden
        if (self.model.get('musculoskeletal_data') == '') {
            self.hideAllFields();
        } else {
            self.showAllFields();
        }

    },
    dispose: function () {
        this._super('_dispose');
    }
})


