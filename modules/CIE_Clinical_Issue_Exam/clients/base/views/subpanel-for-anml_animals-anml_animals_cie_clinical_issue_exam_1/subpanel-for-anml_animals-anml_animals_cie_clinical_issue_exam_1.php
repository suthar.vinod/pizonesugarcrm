<?php
// created: 2020-02-07 13:50:34
$viewdefs['CIE_Clinical_Issue_Exam']['base']['view']['subpanel-for-anml_animals-anml_animals_cie_clinical_issue_exam_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'category',
          'label' => 'LBL_CATEGORY',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'type_2',
          'label' => 'LBL_TYPE_2',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_name',
          'label' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_L_TITLE',
          'enabled' => true,
          'id' => 'CIE_CLINIC2BF6UE_EXAM_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'cie_clinical_issue_exam_m06_error_1_name',
          'label' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_M06_ERROR_TITLE',
          'enabled' => true,
          'id' => 'CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1M06_ERROR_IDB',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'temperature',
          'label' => 'LBL_TEMPERATURE',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'pulse',
          'label' => 'LBL_PULSE',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'respiration_rate',
          'label' => 'LBL_RESPIRATION_RATE',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'resolution_date',
          'label' => 'LBL_RESOLUTION_DATE',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);