({
    extendsFrom: 'RecordView',

    initialize: function (options) {

        this._super('initialize', [options]);

        //when Musculoskeletal (MS) Data field will change
        this.model.on('change:musculoskeletal_data', this.changeDropdownValue, this);

        //when Musculoskeletal Palpation field will change
        this.model.on('change:musculoskeletal_palpation', this.checkPalpationValue, this);
    },

    checkPalpationValue: function() {
        var self = this;
        var palpation = self.model.get('musculoskeletal_palpation');
        if (palpation === 'Normal' || palpation === 'Not Examined') {
            self.showAllFields();
            self.model.set("swelling_location", "Not Applicable");
            self.model.set("swelling_size", "Not Applicable");
            self.model.set("heat_location", "Not Applicable");
            self.model.set("pain_location", "Not Applicable");
        } else if(palpation === 'Heat') {
            self.showAllFields();
            self.model.set("swelling_location", "Not Applicable");
            self.model.set("swelling_size", "Not Applicable");
            self.model.set("heat_location", null);
            self.model.set("pain_location", "Not Applicable");

        } else if(palpation === 'Pain') {
            self.showAllFields();
            self.model.set("swelling_location", "Not Applicable");
            self.model.set("swelling_size", "Not Applicable");
            self.model.set("heat_location", "Not Applicable");
            self.model.set("pain_location", null);

        } else if(palpation === 'Heat and Pain') {
            self.showAllFields();
            self.model.set("swelling_location", "Not Applicable");
            self.model.set("swelling_size", "Not Applicable");
            self.model.set("heat_location", null);
            self.model.set("pain_location", null);

        } else if(palpation === 'Swelling') {
            self.showAllFields();
            self.model.set("swelling_location", null);
            self.model.set("swelling_size", null);
            self.model.set("heat_location", "Not Applicable");
            self.model.set("pain_location", "Not Applicable");

        } else if(palpation === 'Swelling and Heat') {
            self.showAllFields();
            self.model.set("swelling_location", null);
            self.model.set("swelling_size", null);
            self.model.set("heat_location", null);
            self.model.set("pain_location", "Not Applicable");

        } else if(palpation === 'Swelling and Pain') {
            self.showAllFields();
            self.model.set("swelling_location", null);
            self.model.set("swelling_size", null);
            self.model.set("heat_location", "Not Applicable");
            self.model.set("pain_location", null);

        } else if(palpation === 'Swelling Heat Pain') {
            self.showAllFields();
            self.model.set("swelling_location", null);
            self.model.set("swelling_size", null);
            self.model.set("heat_location", null);
            self.model.set("pain_location", null);

        } else if(palpation === ''){
            self.hideAllFields();
        } else {
            self.showAllFields();
            self.model.set("swelling_location", null);
            self.model.set("swelling_size", null);
            self.model.set("heat_location", null);
            self.model.set("pain_location", null);
        }
    },

    changeDropdownValue: function () {
        var self = this;
        if (self.model.get('musculoskeletal_data') === 'All Normal' || self.model.get('musculoskeletal_data') === 'All Not Examined') {
            self.showAllFields();
            self.model.set("swelling_location", "Not Applicable");
            self.model.set("swelling_size", "Not Applicable");
            self.model.set("heat_location", "Not Applicable");
            self.model.set("pain_location", "Not Applicable");
        }

        //If Musculoskeletal Data = Null, then Swelling Location is hidden
        if(self.model.get('musculoskeletal_data') == ''){
            self.hideAllFields();
        }
        if (self.model.get('lesionswound') === 'No') {
            self.model.set("lesionswound_location", "Not Applicable");

        }
        if (self.model.get('musculoskeletal_data') !== 'All Normal' && self.model.get('musculoskeletal_data') !== 'All Not Examined') {
            self.model.set("swelling_location", null);
            self.model.set("swelling_size", null);
            self.model.set("heat_location", null);
            self.model.set("pain_location", null);

        }
        if (self.model.get('lesionswound') !== 'No') {
            self.model.set("lesionswound_location", null);

        }
    },

    /**
     * This function will set the visibility of Swelling Location, Swelling Size, Heat Location and
     * Pain Location fields
     */
    showAllFields: function() {
        var self = this;
        self.$('span[data-fieldname="swelling_location"]').show();
        self.$('.record-label[data-name="swelling_location"]').show();
        self.$('span[data-fieldname="swelling_size"]').show();
        self.$('.record-label[data-name="swelling_size"]').show();
        self.$('span[data-fieldname="heat_location"]').show();
        self.$('.record-label[data-name="heat_location"]').show();
        self.$('span[data-fieldname="pain_location"]').show();
        self.$('.record-label[data-name="pain_location"]').show();
    },

    /**
     * This function will unset the visibility of Swelling Location, Swelling Size, Heat Location and
     * Pain Location fields
     */
    hideAllFields: function() {
        var self = this;
        self.$('span[data-fieldname="swelling_location"]').hide();
        self.$('.record-label[data-name="swelling_location"]').hide();
        self.$('span[data-fieldname="swelling_size"]').hide();
        self.$('.record-label[data-name="swelling_size"]').hide();
        self.$('span[data-fieldname="heat_location"]').hide();
        self.$('.record-label[data-name="heat_location"]').hide();
        self.$('span[data-fieldname="pain_location"]').hide();
        self.$('.record-label[data-name="pain_location"]').hide();
    },
    _render: function () {
        var self = this;
        self._super('_render');
        //If Musculoskeletal Data = Null, then Swelling Location | Swelling Size | Heat Location
        // | Pain Location are hidden
        if(self.model.get('musculoskeletal_data') == '') {
            self.hideAllFields();
        } else {
            self.showAllFields();
        }

    },
    dispose: function () {
        this._super('_dispose');
    }
})


