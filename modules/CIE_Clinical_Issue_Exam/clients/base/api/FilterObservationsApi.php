<?php
/**
* @class FilterObservationsApi
* @extends SugarApi
*/
class FilterObservationsApi extends SugarApi
{
    /**
    * registerApiRest default method to register custom endpoints
    * @return registered api array object
    */
    public function registerApiRest()
    {
        return array(
            'FilterObservations' => array(
                'reqType' => 'GET',
                'path' => array('CIE_Clinical_Issue_Exam','?', 'FilterObservations'),
                'pathVars' => array('module','id',''),
                'method' => 'FilterObservationsMethod',
                'shortHelp' => 'Filter Observations on Basis of Test Systems in Subpanel',
            ),
        );
    }
    /**
    * FilterObservationsMethod runs sql query to get all observations(M06_Error) related to selected Test Systems(Animals)
    * @param $api contains api basic info
    * @param $args contains arguments passed when this api called
    * @return $obIds list of ids of Observations
    */
    public function FilterObservationsMethod($api, $args)
    {
        $this->requireArgs($args,array('id'));
        $strQuery="SELECT m06_error_anml_animals_1m06_error_ida AS obId FROM `m06_error_anml_animals_1_c` WHERE m06_error_anml_animals_1anml_animals_idb=". $GLOBALS['db']->quoted($args['id'])." AND 	deleted=0";
        $results = $GLOBALS['db']->query($strQuery);
        $obIds= array();
        while ($arRow = $GLOBALS['db']->fetchByAssoc($results))
        {
            array_push($obIds,$arRow['obId']);
        }
        return $obIds;
    }

}
