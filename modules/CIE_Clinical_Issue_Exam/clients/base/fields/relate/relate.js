/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * Relate field provides a link to a module that is set in the definition of
 * this field metadata.
 *
 * This field requires at least the follow definitions to be exist in the
 * field:
 *
 * ```
 * array(
 *     'name' => 'account_name',
 *     'rname' => 'name',
 *     'id_name' => 'account_id',
 *     'module' => 'Accounts',
 *     'link' => true,
 *     //...
 * ),
 * ```
 *
 * The field also support a `populate_list` to update other fields in the
 * current model from other fields of the selected model.
 *
 * ```
 * array(
 *     //...
 *     'populate_list' => array(
 *         'populate_list' => array(
 *         'billing_address_street' => 'primary_address_street',
 *         'billing_address_city' => 'primary_address_city',
 *         'billing_address_state' => 'primary_address_state',
 *         'billing_address_postalcode' => 'primary_address_postalcode',
 *         'billing_address_country' => 'primary_address_country',
 *         'phone_office' => 'phone_work',
 *         //...
 *
 *     ),
 * )
 * ```
 *
 * This field allows you to configure the minimum chars that trigger a search
 * when using the typeahead feature.
 *
 * ```
 * array(
 *     //...
 *     'minChars' => 3,
 * )
 * ```
 *
 * TODO: we have a mix of properties here with camelCase and underscore.
 * Needs to be addressed.
 *
 * @class View.Fields.Base.RelateField
 * @alias SUGAR.App.view.fields.BaseRelateField
 * @extends View.Fields.Base.BaseField
 */
({
    extendsFrom: 'BaseRelateField',
    initialize: function (options) {
        this.plugins = _.union(this.plugins, ['LinkedModel']);
        this._super('initialize', [options]);

    },
    /**
     * Opens the selection drawer.
     */
    openSelectDrawer: function () {
        var id = this.model.get('anml_animals_cie_clinical_issue_exam_1anml_animals_ida');
        var url = app.api.buildURL('/CIE_Clinical_Issue_Exam/' + id + '/FilterObservations');
        if (id) {
            if (this.getSearchModule() == "M06_Error") {
                app.api.call('GET', url, {}, {
                    success: _.bind(function (data) {
                        this.openFilteredSelectDrawer(data);
                    }, this),
                    error: function (error) {
                        console.log("API Failed");
                    },
                });
            } else if (this.getSearchModule() == "CIE_Clinical_Issue_Exam") {
                this.openFilteredCIESelectDrawer(id);
            } else if (this.getSearchModule() == "CO_Clinical_Observation") {
                this.openFilteredCOSelectDrawer(id);
            }
        } else {
            var filterOptions = this.getFilterOptions();
            app.drawer.open({
                layout: 'selection-list',
                context: {
                    module: this.getSearchModule(),
                    fields: this.getSearchFields(),
                    filterOptions: filterOptions,
                }
            }, _.bind(this.setValue, this));
        }
    },

    /**
     * Opens Observations(M06_Error) Selections List and Apply initial filter
     * on Selections list on basis of ids passed in data

     * @param {array} lists of ids
     */
    openFilteredSelectDrawer: function (data) {
        var cli_issues = [""];
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'filterObservations',
                'initial_filter_label': 'LBL_FILTER_OBSERVATIONS_TEMPLATE',
                'filter_populate': {
                    'cie_clinical_issue_exam_m06_error_1cie_clinical_issue_exam_ida': cli_issues,
                    'id': data,
                },
                'editable': false,
                'is_template': false,
            })
            .format();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
        $(".filter-definition-container").hide();

    },
    /**
     * Opens CIE_Clinical_Issue_Exam Selections List and Apply initial filter
     * on Selections list on basis of id passed in data

     * @param{string} id of related Test Systems
     */
    openFilteredCIESelectDrawer: function (id) {
        var test_sys = [id];
        var types = ["Initial"];
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'filterCIEs',
                'initial_filter_label': 'LBL_FILTER_CIE_TEMPLATE',
                'filter_populate': {
                    'anml_animals_cie_clinical_issue_exam_1anml_animals_ida': test_sys,
                    'type_2': types,
                }
            })
            .format();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
        $(".filter-definition-container").hide();
    },
    openFilteredCOSelectDrawer: function (id) {

        var test_sys = [id];
        var cie = [];
        var vetCheck = ["Yes"];
        var filterOptions = new app.utils.FilterOptions()
            .config({
                'initial_filter': 'filterCObss',
                'initial_filter_label': 'LBL_FILTER_CO_TEMPLATE',
                'filter_populate': {
                    'anml_animals_co_clinical_observation_1anml_animals_ida': test_sys,
                    'cie_clinicb9a6ue_exam_ida': cie,
                    'vet_check_submitted': vetCheck,
                }
            })
            .format();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
        $(".filter-definition-container").hide();
    }
})
