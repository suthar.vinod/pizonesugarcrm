<?php
// created: 2019-12-31 11:30:59
$viewdefs['CIE_Clinical_Issue_Exam']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'resolution_date' => 
    array (
    ),
    'anml_animals_cie_clinical_issue_exam_1_name' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'type_2' => 
    array (
    ),
  ),
);
