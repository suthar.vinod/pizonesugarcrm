<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class relatedClinicalIssueSubpanel {
    /*
      /sets resolution_date of child record according to the parent record
     */

    static $called = false;

    function relatedClinicalIssueSubpanelMethod($bean, $event, $arguments) {
        if (self::$called == true) {
            return;
        }
        if ($arguments['relationship'] == 'cie_clinical_issue_exam_cie_clinical_issue_exam_1') {
            $parentBean = BeanFactory::retrieveBean('CIE_Clinical_Issue_Exam', $arguments['id']);
            $rid = $arguments['related_id'];
            if ($bean->resolution_date) {
                $update_linked_query = 'UPDATE cie_clinical_issue_exam SET resolution_date = ' . "'$bean->resolution_date'" . ' WHERE id =' . "'$rid'";
            } else {
                $update_linked_query = 'UPDATE cie_clinical_issue_exam SET resolution_date = null WHERE id =' . "'$rid'";
            }
            $GLOBALS['db']->query($update_linked_query);
        }
        self::$called = true;
    }

}

