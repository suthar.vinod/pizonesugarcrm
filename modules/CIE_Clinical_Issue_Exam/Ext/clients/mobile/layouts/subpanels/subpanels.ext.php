<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-01-07 13:46:57
$viewdefs['CIE_Clinical_Issue_Exam']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_R_TITLE',
  'context' => 
  array (
    'link' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1',
  ),
);

// created: 2020-01-07 13:50:42
$viewdefs['CIE_Clinical_Issue_Exam']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_2_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'context' => 
  array (
    'link' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  ),
);