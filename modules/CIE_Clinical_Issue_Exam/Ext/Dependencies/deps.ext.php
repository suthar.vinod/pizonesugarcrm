<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/obs_rel_field.php

$dependencies['CIE_Clinical_Issue_Exam']['com_rel_dep'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('category','type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'cie_clinical_issue_exam_m06_error_1_name',
                'value' => 'and(equal($category, "Vet Check Clin Ob Related"),equal($type_2,"Initial"))', //Formula
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/cli_obs_rel_field.php

$dependencies['CIE_Clinical_Issue_Exam']['co_rel_dep'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('category','type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'cie_clinical_issue_exam_co_clinical_observation_1_name',
                'value' => 'and(equal($category, "Vet Check Clin Ob Related"),equal($type_2,"Initial"))', //Formula
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_examiner_type_dep.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_examiner_type_tech'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(and(equal(related($cie_clinical_issue_exam_cie_clinical_issue_exam_1_right,"follow_upresolution_allowed_by"),"Technician"),equal($category,"Vet Check Clin Ob Related")),or(equal($type_2,"Follow Up"),equal($type_2,"Resolution")))',
   'triggerFields' => array('follow_upresolution_allowed_by','category','cie_clinical_issue_exam_cie_clinical_issue_exam_1_right','type_2'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("tech_list")',
         'labels' => 'getDropdownValueSet("tech_list")'
       ),
     ),
   ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_examiner_type_vet'] = array(
   'hooks' => array("edit","save"),
    'trigger' => 'and(and(equal(related($cie_clinical_issue_exam_cie_clinical_issue_exam_1_right,"follow_upresolution_allowed_by"),"Veterinarian"),equal($category,"Vet Check Clin Ob Related")),or(equal($type_2,"Follow Up"),equal($type_2,"Resolution")))',
   'triggerFields' => array('follow_upresolution_allowed_by','category','cie_clinical_issue_exam_cie_clinical_issue_exam_1_right','type_2'),
    'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("vet_list")',
        'labels' => 'getDropdownValueSet("vet_list")'
       ),
     ),
   ),

);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_examiner_type_both'] = array(
   'hooks' => array("edit","save"),
    'trigger' => 'and(and(equal(related($cie_clinical_issue_exam_cie_clinical_issue_exam_1_right,"follow_upresolution_allowed_by"),"Technician or Veterinarian"),equal($category,"Vet Check Clin Ob Related")),or(equal($type_2,"Follow Up"),equal($type_2,"Resolution")))',
   'triggerFields' => array('follow_upresolution_allowed_by','category','cie_clinical_issue_exam_cie_clinical_issue_exam_1_right','type_2'),
 'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("examiner_type_list")',
         'labels' => 'getDropdownValueSet("examiner_type_list")'
       ),
     ),
   ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_not_clin'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(equal($category,"Vet Check Not Clin Ob Related"),or(equal($type_2,"Follow Up"),equal($type_2,"Resolution")))',
   'triggerFields' => array('category','type_2'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("examiner_type_list")',
         'labels' => 'getDropdownValueSet("examiner_type_list")'
       ),
     ),
   ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_type_physical_exam'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'equal($category,"Physical Exam")',
   'triggerFields' => array('category'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("examiner_type_list")',
         'labels' => 'getDropdownValueSet("examiner_type_list")'
       ),
     ),
   ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_related_clin_issue'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'equal($cie_clinical_issue_exam_cie_clinical_issue_exam_1_right,"")',
   'triggerFields' => array('cie_clinical_issue_exam_cie_clinical_issue_exam_1_right'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("examiner_type_list")',
         'labels' => 'getDropdownValueSet("examiner_type_list")'
       ),
     ),
   ),
);



?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_follow_up_type.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_follow_up_type'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(or(equal($category,"Vet Check Clin Ob Related"),equal($category,"Vet Check Not Clin Ob Related")),equal($type_2,"Initial"))',
   'triggerFields' => array('type_2','category'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("vet_list")',
        'labels' => 'getDropdownValueSet("vet_list")'
       ),
     ),
   ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_follow_up_type_not'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(isInList($category,createList("Vet Check Clin Ob Related", "Vet Check Not Clin Ob Related")),not(equal($type_2,"Initial")))',
   'triggerFields' => array('category','type_2'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("examiner_type_list")',
        'labels' => 'getDropdownValueSet("examiner_type_list")'
       ),
     ),
   ),
 );



?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_related_cli_issue_examiner_type.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_related_examiner_type_tech'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(equal(related($cie_clinical_issue_exam_cie_clinical_issue_exam_1_right,"examiner_type"),"Technician"),and(or(equal($category,"Physical Exam"),equal($category,"Vet Check Not Clin Ob Related")),not(equal($type_2,"Initial"))))',
   'triggerFields' => array('cie_clinical_issue_exam_cie_clinical_issue_exam_1_right','examiner_type','category'.'type_2'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("tech_list")',
        'labels' => 'getDropdownValueSet("tech_list")'
       ),
     ),
   ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_related_examiner_type_vet'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(equal(related($cie_clinical_issue_exam_cie_clinical_issue_exam_1_right,"examiner_type"),"Veterinarian"),and(or(equal($category,"Physical Exam"),equal($category,"Vet Check Not Clin Ob Related")),not(equal($type_2,"Initial"))))',
   'triggerFields' => array('cie_clinical_issue_exam_cie_clinical_issue_exam_1_right','examiner_type','category','type_2'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'examiner_type',
         'keys' => 'getDropdownKeySet("vet_list")',
        'labels' => 'getDropdownValueSet("vet_list")'
       ),
     ),
   ),
);


?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/cie_rel_field.php

$dependencies['CIE_Clinical_Issue_Exam']['cie_rel_dep'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_name',
                'value' => 'or(equal($type_2,"Follow Up"),equal($type_2,"Resolution"))', //Formula
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/set_options_for_oral_cavity_field.php


/**
 * When Gastrointestinal Data = All Normal AND Test System species = Canine|Lagamorph
 * then set Normal option in both Oral Cavity and Abdominal Palpation fields
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_oral_cavity_field'] = array(
    'hooks' => array("all"),
    'trigger' => 'and(isInList($gastrointestinal_data,createList("All Normal")),or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
    'triggerFields' => array('gastrointestinal_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
    //'trigger' => 'equal($test_c, true)',
    //'triggerFields' => array('test_c'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'oral_cavity',
                'keys' => 'getDropdownKeySet("oral_cavity_list_normal")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_normal")'
               //'keys' => "createList('Normal')",
              //'labels' => "createList('Normal')"
            ),
        ),
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_palpation',
                'keys' => 'getDropdownKeySet("oral_cavity_list_normal")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_normal")'
                 //'keys' => "createList('Normal')",
               //'labels' => "createList('Normal')"
            ),
        ),
    ),
//    'notActions' => array(
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'oral_cavity',
//                'keys' => 'getDropdownKeySet("oral_cavity_list_ass")',
//                'labels' => 'getDropdownValueSet("oral_cavity_list_ass")'
//            ),
//        ),
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'abdominal_palpation',
//                'keys' => 'getDropdownKeySet("oral_cavity_list")',
//                'labels' => 'getDropdownValueSet("oral_cavity_list")'
//            ),
//        ),
//    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_Examiner'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($gastrointestinal_data,createList("All Not Examined"))',
    'triggerFields' => array('gastrointestinal_data'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'oral_cavity',
                'keys' => 'getDropdownKeySet("oral_cavity_list_not_exam")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_not_exam")'
                // 'keys' => "createList('Not Examined')",
               //'labels' => "createList('Not Examined')"
            ),
        ),
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_palpation',
                'keys' => 'getDropdownKeySet("oral_cavity_list_not_exam")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_not_exam")'
                // 'keys' => "createList('Not Examined')",
                //'labels' => "createList('Not Examined')"
            ),
        ),
    ),
//    'notActions' => array(
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'oral_cavity',
//                'keys' => 'getDropdownKeySet("oral_cavity_list_ass")',
//                'labels' => 'getDropdownValueSet("oral_cavity_list_ass")'
//            ),
//        ),
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'abdominal_palpation',
//                'keys' => 'getDropdownKeySet("oral_cavity_list")',
//                'labels' => 'getDropdownValueSet("oral_cavity_list")'
//            ),
//        ),
//    ),
);

/**
 * If Gastrointestinal Data = Complete Section AND Test System has species Canine OR Lagamorph,
 * then Oral Cavity = Normal, See Additional Subjective Observations
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_complete_section'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($gastrointestinal_data,createList("Complete Section")),or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
    'triggerFields' => array('gastrointestinal_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
    //'trigger' => 'equal($gt_complete_section_c, true)',
    //'triggerFields' => array('gt_complete_section_c'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'oral_cavity',
                'keys' => 'getDropdownKeySet("normal_see_gas_list")',
                'labels' => 'getDropdownValueSet("normal_see_gas_list")'
                // 'keys' => "createList('Normal', 'See Additional Subjective Observations')",
                //'labels' =>"createList('Normal', 'See Additional Subjective Observations')",
            ),
        ),
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_palpation',
                'keys' => 'getDropdownKeySet("normal_see_gas_list")',
                'labels' => 'getDropdownValueSet("normal_see_gas_list")'
              //'keys' => "createList('Normal', 'See Additional Subjective Observations')",
              //'labels' =>"createList('Normal', 'See Additional Subjective Observations')",
            ),
        ),
    ),
//    'notActions' => array(
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'oral_cavity',
//                'keys' => 'getDropdownKeySet("oral_cavity_list_ass")',
//                'labels' => 'getDropdownValueSet("oral_cavity_list_ass")'
//            ),
//        ),
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'abdominal_palpation',
//                'keys' => 'getDropdownKeySet("oral_cavity_list")',
//                'labels' => 'getDropdownValueSet("oral_cavity_list")'
//            ),
//        ),
//    ),
);

/**
 * If all 3 above mentioned dependencies criteria didn't meat then default dropdown list should be display
 */
//$dependencies['CIE_Clinical_Issue_Exam']['unset_options_for_all_dependencies'] = array(
//   'hooks' => array("edit", "save"),
//   // 'trigger' => 'and(not(and(isInList($gastrointestinal_data,createList("All Normal")),or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_c"),"Lagamorph")))),'
//   // . ' not(isInList($gastrointestinal_data,createList("All Not Examined")),'
//   // . 'not(and(isInList($gastrointestinal_data,createList("Complete Section")),or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_c"),"Lagamorph")))) ))',
//    'trigger' => 'and(greaterThan(strlen($gastrointestinal_data),0),and(not(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine")),not(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph"))))',
//   // 'trigger' => 'and(not(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine")),not(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
//    'triggerFields' => array('gastrointestinal_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
//    //'trigger' => 'equal($test_c, true)',
//    //'triggerFields' => array('test_c'),
//    'onload' => 'true',
//    //Actions is a list of actions to fire when the trigger is true
//    'actions' => array(
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'oral_cavity',
//                'keys' => 'getDropdownKeySet("normal_ne_ginotes_subnotes_list")',
//                'labels' => 'getDropdownValueSet("normal_ne_ginotes_subnotes_list")'
//               //'keys' => "createList('Normal')",
//              //'labels' => "createList('Normal')"
//            ),
//        ),
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'abdominal_palpation',
//                'keys' => 'getDropdownKeySet("normal_ne_ginotes_subnotes_list")',
//                'labels' => 'getDropdownValueSet("normal_ne_ginotes_subnotes_list")'
//                 //'keys' => "createList('Normal')",
//               //'labels' => "createList('Normal')"
//            ),
//        ),
//    ),
//);


/**
 * Set visibility for "Oral Cavity" and "Abdominal Palpation"
 * If $gastrointestinal_data is not empty and Species is either Canine or Lagamorph
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_visibity_for_oc_and_ap'] = array(
    'hooks' => array("edit", "save"),
    // 'trigger' => 'not(equal($gastrointestinal_data,""))',
    'triggerFields' => array('gastrointestinal_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetVisibility', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'oral_cavity',
                'value' => 'and(not(equal($gastrointestinal_data,"")), or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
            ),
        ),
        array(
            'name' => 'SetVisibility', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_palpation',
                'value' => 'and(not(equal($gastrointestinal_data,"")), or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
            ),
        ),
    ),
);



?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/set_options_for_abdominal_auscultation.php


$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_abdo_aus_for_ne'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($gastrointestinal_data,createList("All Not Examined"))',
    'triggerFields' => array('gastrointestinal_data'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_auscultation',
                'keys' => 'getDropdownKeySet("oral_cavity_list_not_exam")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_not_exam")'
            ),
        ),
    ),

);

$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_abdo_aus_for_normal'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($gastrointestinal_data,createList("All Normal"))',
    'triggerFields' => array('gastrointestinal_data'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_auscultation',
               'keys' => 'getDropdownKeySet("oral_cavity_list_normal")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_normal")'
            ),
        ),
    ),

);

$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_abdo_aus_for_com_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'or(and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($appetite,createList("Mild Inappetent", "Moderate Inappetent", "Anorexic"))), '
    . 'and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($feces,createList("Absent", "Diarrhea", "Scant", "Soft"))),'
    . ' and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($oral_cavity,createList("See Gastrointestinal Notes"))),'
    . 'and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($abdominal_palpation,createList("See Gastrointestinal Notes"))))',
    'triggerFields' => array('gastrointestinal_data', 'appetite', 'feces', 'oral_cavity', 'abdominal_palpation'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_auscultation',
                'keys' => 'getDropdownKeySet("normal_see_gas_list")',
                'labels' => 'getDropdownValueSet("normal_see_gas_list")'
            ),
        ),
    ),

);

$dependencies['CIE_Clinical_Issue_Exam']['unset_options_for_abdo_aus'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(not(isInList($gastrointestinal_data,createList("All Not Examined"))),not(isInList($gastrointestinal_data,createList("All Normal"))),not(or(and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($appetite,createList("Mild Inappetent", "Moderate Inappetent", "Anorexic"))), '
    . 'and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($feces,createList("Absent", "Diarrhea", "Scant", "Soft"))),'
    . ' and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($oral_cavity,createList("See Gastrointestinal Notes"))),'
    . 'and(isInList($gastrointestinal_data,createList("Complete Section")), isInList($abdominal_palpation,createList("See Gastrointestinal Notes"))))))',
    'triggerFields' => array('gastrointestinal_data', 'appetite', 'feces', 'oral_cavity', 'abdominal_palpation'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'abdominal_auscultation',
                'keys' => 'getDropdownKeySet("normal_ne_ginotes_subnotes_list")',
                'labels' => 'getDropdownValueSet("normal_ne_ginotes_subnotes_list")'
            ),
        ),
    ),

);


?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/set_options_for_popliteal_LN.php


/**
 * If Lymphatic Data Data = All Normal AND Test System is related to Canine OR Lagamorph,
 * then Popliteal LN (Canine/Lagamorph) = Normal
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_popliteal_ln_for_normal'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($lymphatic_data,createList("All Normal")),or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
    'triggerFields' => array('lymphatic_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'popliteal_ln_caninelagamorph',
                'keys' => 'getDropdownKeySet("oral_cavity_list_normal")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_normal")'
            ),
        ),
    ),

);

/**
 * If Lymphatic Data Data = All Not Examined, then Popliteal LN (Canine/Lagamorph) = Not Examined
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_popliteal_ln_for_not_exam'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'isInList($lymphatic_data,createList("All Not Examined"))',
    'triggerFields' => array('lymphatic_data'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'popliteal_ln_caninelagamorph',
                'keys' => 'getDropdownKeySet("oral_cavity_list_not_exam")',
                'labels' => 'getDropdownValueSet("oral_cavity_list_not_exam")'
            ),
        ),
    ),

);

/**
 * If Lymphatic Data = Complete Section AND Test System is related to Canine OR Lagamorph,
 * then Popliteal  LN (Canine/Lagamorph) = Normal, See Lymphatic Notes
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_options_for_popliteal_ln_for_complete_section'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(isInList($lymphatic_data,createList("Complete Section")),or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
    'triggerFields' => array('lymphatic_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetOptions', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'popliteal_ln_caninelagamorph',
                'keys' => 'getDropdownKeySet("normal_sln_list")',
                'labels' => 'getDropdownValueSet("normal_sln_list")'
            ),
        ),
    ),
);

/**
 * If all 3 above mentioned dependencies criteria didn't meet then default dropdown list should be display
 */
//$dependencies['CIE_Clinical_Issue_Exam']['unset_options_for_all_popliteal_dependencies'] = array(
//   'hooks' => array("edit", "save"),
//   // 'trigger' => 'not(greaterThan(strlen($lymphatic_data),0))',
//     'trigger' => 'or(greaterThan(strlen($lymphatic_data),0),and(not(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine")),not(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph"))))',
//    'triggerFields' => array('lymphatic_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
//    'onload' => 'true',
//    //Actions is a list of actions to fire when the trigger is true
//    'actions' => array(
//        array(
//            'name' => 'SetOptions', //Action type
//            //The parameters passed in depend on the action type
//            'params' => array(
//                'target' => 'popliteal_ln_caninelagamorph',
//                'keys' => 'getDropdownKeySet("normal_ne_sln_list")',
//                'labels' => 'getDropdownValueSet("normal_ne_sln_list")'
//            ),
//        ),
//    ),
//);

/**
 * Set visibility for "Popliteal LN" field
 * If lymphatic_data is not empty and Species is either Canine or Lagamorph
 */
$dependencies['CIE_Clinical_Issue_Exam']['set_visibity_for_pop_ln'] = array(
    'hooks' => array("edit", "save"),
    // 'trigger' => 'not(equal($gastrointestinal_data,""))',
    'triggerFields' => array('lymphatic_data', 'anml_animals_cie_clinical_issue_exam_1', 'species_2_c'),
    'onload' => 'true',
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetVisibility', //Action type
            //The parameters passed in depend on the action type
            'params' => array(
                'target' => 'popliteal_ln_caninelagamorph',
                'value' => 'and(not(equal($lymphatic_data,"")), or(equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Canine"),equal(related($anml_animals_cie_clinical_issue_exam_1,"species_2_c"),"Lagamorph")))',
            ),
        ),
    ),
);




?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/set_required_for_textfields.php



$dependencies['CIE_Clinical_Issue_Exam']['set_required_for_textfields'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('musculoskeletal_data', 'musculoskeletal_palpation'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'swelling_location',
                'value' => 'or(not(equal($musculoskeletal_data, "")), not(equal($musculoskeletal_palpation, "")))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'swelling_size',
                'value' => 'or(not(equal($musculoskeletal_data, "")), not(equal($musculoskeletal_palpation, "")))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'heat_location',
                'value' => 'or(not(equal($musculoskeletal_data, "")), not(equal($musculoskeletal_palpation, "")))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'pain_location',
                'value' => 'or(not(equal($musculoskeletal_data, "")), not(equal($musculoskeletal_palpation, "")))',
            ),
        ),
    ),
);


?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_ophthal_dep.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_ophthal_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(or(equal($section_notes_data, "All None"),or(or(or(or(equal($eye_positionsymmetry,"See Ophthalmic Notes"),equal($conjunctiva,"See Ophthalmic Notes")),equal($cornea,"See Ophthalmic Notes")),equal($pupil,"See Ophthalmic Notes")),equal($ocular_discharge,"See Ophthalmic Notes"))),not(equal($ophthalmic_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'eye_positionsymmetry', 'conjunctiva','cornea','pupil','ocular_discharge','ophthalmic_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_ophthal_notes_none'] = array(
  'trigger' => 'and(and(equal($section_notes_data, "All None"),and(and(and(and(equal($eye_positionsymmetry,"Normal"),equal($conjunctiva,"Normal")),equal($cornea,"Normal")),equal($pupil,"Normal")),equal($ocular_discharge,"No"))),equal($format,"Organ Systems"))',
  'triggerFields' => array('section_notes_data', 'eye_positionsymmetry', 'conjunctiva','cornea','pupil','ocular_discharge','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_ophthal_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "Complete Individual Sections"),and(and(and(and(equal($eye_positionsymmetry,"Normal"),equal($conjunctiva,"Normal")),equal($cornea,"Normal")),equal($pupil,"Normal")),equal($ocular_discharge,"No"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'eye_positionsymmetry', 'conjunctiva','cornea','pupil','ocular_discharge','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_gastro_dep.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(isInList($section_notes_data,createList("All None", "Complete Individual Sections")),or(or(equal($oral_cavity,"See Gastrointestinal Notes"),equal($abdominal_auscultation,"See Gastrointestinal Notes")),equal($abdominal_palpation,"See Gastrointestinal Notes"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'oral_cavity', 'abdominal_palpation', 'abdominal_auscultation','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"All None"),and(and(equal($oral_cavity,"Normal"),equal($abdominal_auscultation,"Normal")),equal($abdominal_palpation,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'oral_cavity', 'abdominal_palpation', 'abdominal_auscultation','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_notes_species_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(and(equal($section_notes_data,"All None"),equal($abdominal_auscultation,"Normal")),and(not(equal($abdominal_palpation,"See Gastrointestinal Notes")),not(equal($oral_cavity,"See Gastrointestinal Notes")))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'oral_cavity', 'abdominal_palpation', 'abdominal_auscultation','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"Complete Individual Sections"),and(and(equal($oral_cavity,"Normal"),equal($abdominal_auscultation,"Normal")),equal($abdominal_palpation,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'oral_cavity', 'abdominal_palpation', 'abdominal_auscultation','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_species_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(and(equal($section_notes_data,"Complete Individual Sections"),equal($abdominal_auscultation,"Normal")),and(not(equal($abdominal_palpation,"See Gastrointestinal Notes")),not(equal($oral_cavity,"See Gastrointestinal Notes")))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'oral_cavity', 'abdominal_palpation', 'abdominal_auscultation','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_integument_dep.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_integ_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(and(isInList($section_notes_data,createList("All None", "Complete Individual Sections")),or(or(or(equal($xerosis,"See Integument Notes"),equal($pruritus,"See Integument Notes")),equal($alopecia,"See Integument Notes")),contains($lesionswound,"See"))),not(equal($integument_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'xerosis', 'pruritus','alopecia','lesionswound','integument_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_integ_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "All None"),and(and(and(equal($xerosis,"No"),equal($pruritus,"No")),equal($alopecia,"No")),equal($lesionswound,"No"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'xerosis', 'pruritus','alopecia','lesionswound','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_integ_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "Complete Individual Sections"),and(and(and(equal($xerosis,"No"),equal($pruritus,"No")),equal($alopecia,"No")),equal($lesionswound,"No"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'xerosis', 'pruritus','alopecia','lesionswound','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_lymph_dep.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymph_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(or(equal($section_notes_data,"All None"),or(or(equal($submandibular_ln,"See Lymphatic Notes"),equal($prescapular_ln,"See Lymphatic Notes")),equal($popliteal_ln_caninelagamorph,"See Lymphatic Notes"))),not(equal($lymphatic_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'submandibular_ln', 'prescapular_ln','popliteal_ln_caninelagamorph','lymphatic_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymph_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"All None"),and(and(equal($submandibular_ln,"Normal"),equal($prescapular_ln,"Normal")),equal($popliteal_ln_caninelagamorph,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'submandibular_ln', 'prescapular_ln','popliteal_ln_caninelagamorph','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymph_notes_species_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"All None"),and(and(equal($submandibular_ln,"Normal"),equal($prescapular_ln,"Normal")),not(equal($popliteal_ln_caninelagamorph,"See Lymphatic Notes")))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'submandibular_ln', 'prescapular_ln','popliteal_ln_caninelagamorph','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymph_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"Complete Individual Sections"),and(and(equal($submandibular_ln,"Normal"),equal($prescapular_ln,"Normal")),equal($popliteal_ln_caninelagamorph,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'submandibular_ln', 'prescapular_ln','popliteal_ln_caninelagamorph','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymphs_species_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"Complete Individual Sections"),and(and(equal($submandibular_ln,"Normal"),equal($prescapular_ln,"Normal")),not(equal($popliteal_ln_caninelagamorph,"See Lymphatic Notes")))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'submandibular_ln', 'prescapular_ln','popliteal_ln_caninelagamorph','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_cardiovascular_dep.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_cardio_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(and(isInList($section_notes_data,createList("All None", "Complete Individual Sections")),or(equal($cardiac_auscultation,"See Cardiovascular Notes"),equal($cardiac_rhythm,"See Cardiovascular Notes"))),not(equal($cardiovascular_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'cardiac_auscultation', 'cardiac_rhythm','cardiovascular_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_cardio_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"All None"),and(equal($cardiac_auscultation,"Normal"),equal($cardiac_rhythm,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'cardiac_auscultation', 'cardiac_rhythm','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_cardio_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data,"Complete Individual Sections"),and(equal($cardiac_auscultation,"Normal"),equal($cardiac_rhythm,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'cardiac_auscultation', 'cardiac_rhythm','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_nervous_dep.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_nervous_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(or(equal($section_notes_data, "All None"),isInList("See Nervous System Notes",createList($circlingpacing,$head_tilt,$head_pressing,$ataxia,$tremors,$cn2_optic_vision_plr_menace,$cn3_oculomotor_eyeeyelid_pos,$cn4_trochlear_eye_position,$cn5_trigeminal_masseter_symmet,$cn6_abducent_eye_position,$cn7_facial_facial_symmetry,$cn8_vestibulochoclear_head_til,$cn9_glosspharyngeal_swallow,$cn10_vagus_gi_auscultation_swa,$cn11_accessory_thoracic_muscle,$cn12_hypoglossal_tongue_moveme))),not(equal($nervous_system_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'circlingpacing', 'head_tilt','head_pressing','ataxia','tremors','cn2_optic_vision_plr_menace','cn3_oculomotor_eyeeyelid_pos','cn4_trochlear_eye_position','cn5_trigeminal_masseter_symmet','cn6_abducent_eye_position','cn7_facial_facial_symmetry','cn8_vestibulochoclear_head_til','cn9_glosspharyngeal_swallow','cn10_vagus_gi_auscultation_swa','cn11_accessory_thoracic_muscle','cn12_hypoglossal_tongue_moveme','nervous_system_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_nervous_notes_none'] = array(
  'trigger' => 'and(and(equal($section_notes_data, "All None"),
  and(and(and(and(and(and(equal($circlingpacing,"No"),equal($head_tilt,"No")),equal($head_pressing,"No")),equal($head_pressing,"No")),equal($ataxia,"No")),equal($tremors,"No")),
  and(and(and(and(and(and(equal($cn2_optic_vision_plr_menace,"Normal"),equal($cn3_oculomotor_eyeeyelid_pos,"Normal")),and(equal($cn4_trochlear_eye_position,"Normal"),equal($cn5_trigeminal_masseter_symmet,"Normal"))),and(equal($cn6_abducent_eye_position,"Normal"),equal($cn7_facial_facial_symmetry,"Normal"))),and(equal($cn8_vestibulochoclear_head_til,"Normal")
  ,equal($cn9_glosspharyngeal_swallow,"Normal"))),and(equal($cn10_vagus_gi_auscultation_swa,"Normal"),equal($cn11_accessory_thoracic_muscle,"Normal"))),equal($cn12_hypoglossal_tongue_moveme,"Normal")))),equal($format,"Organ Systems"))',
  'triggerFields' => array('section_notes_data', 'circlingpacing', 'head_tilt','head_pressing','ataxia','tremors','cn2_optic_vision_plr_menace','cn3_oculomotor_eyeeyelid_pos','cn4_trochlear_eye_position','cn5_trigeminal_masseter_symmet','cn6_abducent_eye_position','cn7_facial_facial_symmetry','cn8_vestibulochoclear_head_til','cn9_glosspharyngeal_swallow','cn10_vagus_gi_auscultation_swa','cn11_accessory_thoracic_muscle','cn12_hypoglossal_tongue_moveme','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_nervous_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "Complete Individual Sections"),
    and(and(and(and(and(and(equal($circlingpacing,"No"),equal($head_tilt,"No")),equal($head_pressing,"No")),equal($head_pressing,"No")),equal($ataxia,"No")),equal($tremors,"No")),
    and(and(and(and(and(and(equal($cn2_optic_vision_plr_menace,"Normal"),equal($cn3_oculomotor_eyeeyelid_pos,"Normal")),and(equal($cn4_trochlear_eye_position,"Normal"),equal($cn5_trigeminal_masseter_symmet,"Normal"))),and(equal($cn6_abducent_eye_position,"Normal"),equal($cn7_facial_facial_symmetry,"Normal"))),and(equal($cn8_vestibulochoclear_head_til,"Normal")
    ,equal($cn9_glosspharyngeal_swallow,"Normal"))),and(equal($cn10_vagus_gi_auscultation_swa,"Normal"),equal($cn11_accessory_thoracic_muscle,"Normal"))),equal($cn12_hypoglossal_tongue_moveme,"Normal")))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'circlingpacing', 'head_tilt','head_pressing','ataxia','tremors','cn2_optic_vision_plr_menace','cn3_oculomotor_eyeeyelid_pos','cn4_trochlear_eye_position','cn5_trigeminal_masseter_symmet','cn6_abducent_eye_position','cn7_facial_facial_symmetry','cn8_vestibulochoclear_head_til','cn9_glosspharyngeal_swallow','cn10_vagus_gi_auscultation_swa','cn11_accessory_thoracic_muscle','cn12_hypoglossal_tongue_moveme','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_muscu_dep.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_muscu_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(or(equal($section_notes_data, "All None"),or(or(or(equal($musculoskeletal_symmetry,"See Musculoskeletal Notes"),equal($muscle_atrophy,"See Musculoskeletal Notes")),equal($posture,"See Musculoskeletal Notes")),equal($stanceweight_bearing,"See Musculoskeletal Notes"))),not(equal($musculoskeletal_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'musculoskeletal_symmetry', 'muscle_atrophy','posture','stanceweight_bearing','musculoskeletal_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_muscu_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "All None"),and(and(and(equal($musculoskeletal_symmetry,"Normal"),equal($muscle_atrophy,"No")),equal($posture,"Normal")),equal($stanceweight_bearing,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'musculoskeletal_symmetry', 'muscle_atrophy','posture','stanceweight_bearing','lymphatic_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_muscu_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "Complete Individual Sections"),and(and(and(equal($musculoskeletal_symmetry,"Normal"),equal($muscle_atrophy,"No")),equal($posture,"Normal")),equal($stanceweight_bearing,"Normal"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'musculoskeletal_symmetry', 'muscle_atrophy','posture','stanceweight_bearing','lymphatic_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_resp_dep.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_resp_notes_yes'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(or(equal($section_notes_data, "All None"),or(or(or(equal($upper_airway_auscultation,"See Respiratory Notes"),equal($lung_auscultation,"See Respiratory Notes")),equal($respiratory_effort,"See Respiratory Notes")),equal($nasal_discharge,"See Respiratory Notes"))),not(equal($respiratory_data,"All Not Examined"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'upper_airway_auscultation', 'lung_auscultation','respiratory_effort','nasal_discharge','respiratory_data','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_yes")',
                'labels' => 'getDropdownValueSet("options_yes")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_resp_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "All None"),and(and(and(equal($upper_airway_auscultation,"Normal"),equal($lung_auscultation,"Normal")),equal($respiratory_effort,"Normal")),equal($nasal_discharge,"No"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'upper_airway_auscultation', 'lung_auscultation','respiratory_effort','nasal_discharge','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_resp_complete_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(and(equal($section_notes_data, "Complete Individual Sections"),and(and(and(equal($upper_airway_auscultation,"Normal"),equal($lung_auscultation,"Normal")),equal($respiratory_effort,"Normal")),equal($nasal_discharge,"No"))),equal($format,"Organ Systems"))',
    'triggerFields' => array('section_notes_data', 'upper_airway_auscultation', 'lung_auscultation','respiratory_effort','nasal_discharge','format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);


?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_urogen_dep.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_urogen_notes_yes'] = array(
  'hooks' => array("edit", "save"),
  'trigger' => 'and(and(or(equal($section_notes_data,"All None"),or(equal($external_genitalia,"See Urogenital Notes"),equal($urine,"See Urogenital Notes"))),not(equal($urogenital_data,"All Not Examined"))),equal($format,"Organ Systems"))',
  'triggerFields' => array('section_notes_data', 'external_genitalia', 'urine','urogenital_data','format'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetOptions',
      'params' => array(
        'target' => 'urogenital_notes_data',
        'keys' => 'getDropdownKeySet("options_yes")',
        'labels' => 'getDropdownValueSet("options_yes")'
      ),
    ),
  ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_urogen_notes_none'] = array(
  'hooks' => array("edit", "save"),
  'trigger' => 'and(and(equal($section_notes_data,"All None"),and(equal($external_genitalia,"Normal"),or(equal($urine,"Normal"),equal($urine,"Not Examined")))),equal($format,"Organ Systems"))',
  'triggerFields' => array('section_notes_data', 'external_genitalia', 'urine','format'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetOptions',
      'params' => array(
        'target' => 'urogenital_notes_data',
        'keys' => 'getDropdownKeySet("options_none")',
        'labels' => 'getDropdownValueSet("options_none")'
      ),
    ),
  ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_urogen_complete_sec'] = array(
  'hooks' => array("edit", "save"),
  'trigger' => 'and(and(equal($section_notes_data,"Complete Individual Sections"),and(equal($external_genitalia,"Normal"),or(equal($urine,"Normal"),equal($urine,"Not Examined")))),equal($format,"Organ Systems"))',
  'triggerFields' => array('section_notes_data', 'external_genitalia', 'urine','format'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetOptions',
      'params' => array(
        'target' => 'urogenital_notes_data',
        'keys' => 'getDropdownKeySet("options_yes_none")',
        'labels' => 'getDropdownValueSet("options_yes_none")'
      ),
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_data_field_dep.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_cardio_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($cardiovascular_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'cardiovascular_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($gastrointestinal_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'gastrointestinal_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_integ_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($integument_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'integument_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymph_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($lymphatic_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'lymphatic_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_muscu_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($musculoskeletal_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'musculoskeletal_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_nervous_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($nervous_system_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'nervous_system_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_ophth_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($ophthalmic_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'ophthalmic_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_resp_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($respiratory_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'respiratory_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_urogen_examined_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"All None"),equal($urogenital_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'urogenital_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'urogenital_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_cardio_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($cardiovascular_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'cardiovascular_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_gastro_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($gastrointestinal_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'gastrointestinal_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_integ_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($integument_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'integument_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_lymph_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($lymphatic_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'lymphatic_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_muscu_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($musculoskeletal_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'musculoskeletal_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_nervous_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($nervous_system_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'nervous_system_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_ophth_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($ophthalmic_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'ophthalmic_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_resp_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($respiratory_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'respiratory_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_urogen_examined_sec'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($section_notes_data,"Complete Individual Sections"),equal($urogenital_data,"All Not Examined"))',
    'triggerFields' => array('section_notes_data', 'urogenital_data'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'urogenital_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Dependencies/rt_format_dep.php


$dependencies['CIE_Clinical_Issue_Exam']['setoptions_format_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($format,"Open Notes"),equal($section_notes_data,"All None"))',
    'triggerFields' => array('section_notes_data', 'format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'urogenital_notes_data',
                'keys' => 'getDropdownKeySet("options_none")',
                'labels' => 'getDropdownValueSet("options_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_format_section_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'and(equal($format,"Open Notes"),equal($section_notes_data,"Complete Individual Sections"))',
    'triggerFields' => array('section_notes_data', 'format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'urogenital_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);

$dependencies['CIE_Clinical_Issue_Exam']['setoptions_format_organ_notes_none'] = array(
    'hooks' => array("edit", "save"),
    'trigger' => 'equal($format,"Organ Systems")',
    'triggerFields' => array('format'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'cardiovascular_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'gastrointestinal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'lymphatic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'integument_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'musculoskeletal_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'nervous_system_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'ophthalmic_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'respiratory_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'urogenital_notes_data',
                'keys' => 'getDropdownKeySet("options_yes_none")',
                'labels' => 'getDropdownValueSet("options_yes_none")'
            ),
        ),
    ),
);
?>
