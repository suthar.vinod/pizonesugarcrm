<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/LogicHooks/related_clinical_issues.php


$hook_array['after_save'][] = Array(
        //Processing index. For sorting the array.
        1,

        //Label. A string value to identify the hook.
        'after_save related_clinical_issues',

        //The PHP file where your class is located.
        'custom/modules/CIE_Clinical_Issue_Exam/related_clinical_issues.php',

        //The class the method is in.
        'RelatedClinicalIssues',

        //The method to call.
        'relatedClinicalIssuesMethod'
    );


?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/LogicHooks/relatedClinicalIssueSubpanel.php


$hook_array['after_relationship_add'][] = Array(
    //Processing index. For sorting the array.
    99,

    //Label. A string value to identify the hook.
    'after_relationship_add RelatedClinicalIssueSubpanel',

    //The PHP file where your class is located.
    'custom/modules/CIE_Clinical_Issue_Exam/relatedClinicalIssueSubpanel.php',

    //The class the method is in.
    'RelatedClinicalIssueSubpanel',

    //The method to call.
    'relatedClinicalIssueSubpanelMethod'
);




?>
