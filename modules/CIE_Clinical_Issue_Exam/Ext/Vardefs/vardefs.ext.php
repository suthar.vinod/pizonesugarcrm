<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/cie_clinical_issue_exam_co_clinical_observation_1_CIE_Clinical_Issue_Exam.php

// created: 2020-01-07 13:43:54
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_1"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'source' => 'non-db',
  'module' => 'CO_Clinical_Observation',
  'bean_name' => 'CO_Clinical_Observation',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'id_name' => 'cie_clinicecbbrvation_idb',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_1_name"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'save' => true,
  'id_name' => 'cie_clinicecbbrvation_idb',
  'link' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'table' => 'co_clinical_observation',
  'module' => 'CO_Clinical_Observation',
  'rname' => 'name',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinicecbbrvation_idb"] = array (
  'name' => 'cie_clinicecbbrvation_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE_ID',
  'id_name' => 'cie_clinicecbbrvation_idb',
  'link' => 'cie_clinical_issue_exam_co_clinical_observation_1',
  'table' => 'co_clinical_observation',
  'module' => 'CO_Clinical_Observation',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/cie_clinical_issue_exam_cie_clinical_issue_exam_1_CIE_Clinical_Issue_Exam.php

// created: 2020-01-07 13:46:57
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_cie_clinical_issue_exam_1"] = array (
  'name' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1',
  'source' => 'non-db',
  'module' => 'CIE_Clinical_Issue_Exam',
  'bean_name' => 'CIE_Clinical_Issue_Exam',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_L_TITLE',
  'id_name' => 'cie_clinic7f62ue_exam_idb',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_cie_clinical_issue_exam_1_right"] = array (
  'name' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_right',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1',
  'source' => 'non-db',
  'module' => 'CIE_Clinical_Issue_Exam',
  'bean_name' => 'CIE_Clinical_Issue_Exam',
  'side' => 'right',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_R_TITLE',
  'id_name' => 'cie_clinic2bf6ue_exam_ida',
  'link-type' => 'one',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_cie_clinical_issue_exam_1_name"] = array (
  'name' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_L_TITLE',
  'save' => true,
  'id_name' => 'cie_clinic2bf6ue_exam_ida',
  'link' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_right',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'name',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinic2bf6ue_exam_ida"] = array (
  'name' => 'cie_clinic2bf6ue_exam_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_R_TITLE_ID',
  'id_name' => 'cie_clinic2bf6ue_exam_ida',
  'link' => 'cie_clinical_issue_exam_cie_clinical_issue_exam_1_right',
  'table' => 'cie_clinical_issue_exam',
  'module' => 'CIE_Clinical_Issue_Exam',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/cie_clinical_issue_exam_m06_error_1_CIE_Clinical_Issue_Exam.php

// created: 2020-01-07 13:48:18
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_m06_error_1"] = array (
  'name' => 'cie_clinical_issue_exam_m06_error_1',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_m06_error_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_m06_error_1_name"] = array (
  'name' => 'cie_clinical_issue_exam_m06_error_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_M06_ERROR_TITLE',
  'save' => true,
  'id_name' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
  'link' => 'cie_clinical_issue_exam_m06_error_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'name',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_m06_error_1m06_error_idb"] = array (
  'name' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_M06_ERROR_TITLE_ID',
  'id_name' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
  'link' => 'cie_clinical_issue_exam_m06_error_1',
  'table' => 'm06_error',
  'module' => 'M06_Error',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/cie_clinical_issue_exam_co_clinical_observation_2_CIE_Clinical_Issue_Exam.php

// created: 2020-01-07 13:50:42
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["cie_clinical_issue_exam_co_clinical_observation_2"] = array (
  'name' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  'type' => 'link',
  'relationship' => 'cie_clinical_issue_exam_co_clinical_observation_2',
  'source' => 'non-db',
  'module' => 'CO_Clinical_Observation',
  'bean_name' => 'CO_Clinical_Observation',
  'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CO_CLINICAL_OBSERVATION_2_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'id_name' => 'cie_clinicddcbue_exam_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/sugarfield_test_system_name_c.php

 // created: 2020-01-07 13:53:17
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['duplicate_merge_dom_value']=0;
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['labelValue']='Test System Name';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['calculated']='true';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['formula']='related($anml_animals_cie_clinical_issue_exam_1,"name")';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['enforced']='true';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['test_system_name_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/sugarfield_cie_type_text_c.php

 // created: 2020-01-07 13:54:42
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['duplicate_merge_dom_value']=0;
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['labelValue']='CIE Type Text';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['calculated']='true';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['formula']='getDropdownValue("cie_type_list",$type_2)';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['enforced']='true';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['cie_type_text_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/anml_animals_cie_clinical_issue_exam_1_CIE_Clinical_Issue_Exam.php

// created: 2019-12-20 17:02:38
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["anml_animals_cie_clinical_issue_exam_1"] = array (
  'name' => 'anml_animals_cie_clinical_issue_exam_1',
  'type' => 'link',
  'relationship' => 'anml_animals_cie_clinical_issue_exam_1',
  'source' => 'non-db',
  'module' => 'ANML_Animals',
  'bean_name' => 'ANML_Animals',
  'side' => 'right',
  'vname' => 'LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'id_name' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
  'link-type' => 'one',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["anml_animals_cie_clinical_issue_exam_1_name"] = array (
  'name' => 'anml_animals_cie_clinical_issue_exam_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_ANML_ANIMALS_TITLE',
  'save' => true,
  'required' => true,
  'id_name' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
  'link' => 'anml_animals_cie_clinical_issue_exam_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'name',
);
$dictionary["CIE_Clinical_Issue_Exam"]["fields"]["anml_animals_cie_clinical_issue_exam_1anml_animals_ida"] = array (
  'name' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE_ID',
  'id_name' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
  'link' => 'anml_animals_cie_clinical_issue_exam_1',
  'table' => 'anml_animals',
  'module' => 'ANML_Animals',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/sugarfield_oral_cavity.php

 // created: 2020-01-17 10:31:05
$dictionary['CIE_Clinical_Issue_Exam']['fields']['oral_cavity']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/sugarfield_abdominal_palpation.php

 // created: 2020-01-17 10:31:59
$dictionary['CIE_Clinical_Issue_Exam']['fields']['abdominal_palpation']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/sugarfield_abdominal_auscultation.php

 // created: 2020-01-17 10:33:16
$dictionary['CIE_Clinical_Issue_Exam']['fields']['abdominal_auscultation']['dependency']='not(equal($gastrointestinal_data,""))';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['abdominal_auscultation']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/sugarfield_heat_location.php

 // created: 2020-01-22 12:34:07
$dictionary['CIE_Clinical_Issue_Exam']['fields']['heat_location']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/sugarfield_pain_location.php

 // created: 2020-01-22 12:34:25
$dictionary['CIE_Clinical_Issue_Exam']['fields']['pain_location']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/sugarfield_swelling_location.php

 // created: 2020-01-22 12:32:34
$dictionary['CIE_Clinical_Issue_Exam']['fields']['swelling_location']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/sugarfield_swelling_size.php

 // created: 2020-01-22 12:32:51
$dictionary['CIE_Clinical_Issue_Exam']['fields']['swelling_size']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/sugarfield_popliteal_ln_caninelagamorph.php

 // created: 2020-03-09 11:29:21
$dictionary['CIE_Clinical_Issue_Exam']['fields']['popliteal_ln_caninelagamorph']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/CIE_Clinical_Issue_Exam/Ext/Vardefs/sugarfield_name.php

 // created: 2020-03-17 11:27:51
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['required']=false;
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['unified_search']=false;
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['importable']='false';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['duplicate_merge']='disabled';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['merge_filter']='disabled';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['calculated']='true';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['formula']='concat($test_system_name_c," ",toString($exam_datetime)," ",$cie_type_text_c)';
$dictionary['CIE_Clinical_Issue_Exam']['fields']['name']['enforced']=true;

 
?>
