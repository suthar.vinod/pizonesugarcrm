<?php
// created: 2020-02-07 13:50:33
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'category' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_CATEGORY',
    'width' => 10,
  ),
  'type_2' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TYPE_2',
    'width' => 10,
  ),
  'cie_clinical_issue_exam_cie_clinical_issue_exam_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_L_TITLE',
    'id' => 'CIE_CLINIC2BF6UE_EXAM_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'CIE_Clinical_Issue_Exam',
    'target_record_key' => 'cie_clinic2bf6ue_exam_ida',
  ),
  'cie_clinical_issue_exam_m06_error_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1_FROM_M06_ERROR_TITLE',
    'id' => 'CIE_CLINICAL_ISSUE_EXAM_M06_ERROR_1M06_ERROR_IDB',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M06_Error',
    'target_record_key' => 'cie_clinical_issue_exam_m06_error_1m06_error_idb',
  ),
  'temperature' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_TEMPERATURE',
    'width' => 10,
  ),
  'pulse' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_PULSE',
    'width' => 10,
  ),
  'respiration_rate' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_RESPIRATION_RATE',
    'width' => 10,
  ),
  'resolution_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_RESOLUTION_DATE',
    'width' => 10,
    'default' => true,
  ),
);