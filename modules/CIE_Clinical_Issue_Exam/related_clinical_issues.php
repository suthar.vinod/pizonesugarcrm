<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class RelatedClinicalIssues {
    /*
      sets resolution date of related records according the parent record
     */

    static $ran = false;

    function relatedClinicalIssuesMethod($bean, $event, $arguments) {

        global $db;
        if (self::$ran == true) {
            return;
        }

        if ($bean->cie_clinic2bf6ue_exam_ida) {
            
            $relatedBean = BeanFactory::retrieveBean('CIE_Clinical_Issue_Exam', $bean->cie_clinic2bf6ue_exam_ida);
            if ($relatedBean) {
                if ($bean->resolution_date) {
                    $update_related_query = 'UPDATE cie_clinical_issue_exam SET resolution_date = ' . "'$bean->resolution_date'" . ' WHERE id =' . "'$relatedBean->id'";
                } else {
                    $update_related_query = 'UPDATE cie_clinical_issue_exam SET resolution_date = null WHERE id =' . "'$relatedBean->id'";
                }
                $db->query($update_related_query);
            }
            if ($relatedBean->load_relationship('cie_clinical_issue_exam_cie_clinical_issue_exam_1')) {
                $beanIds = $relatedBean->cie_clinical_issue_exam_cie_clinical_issue_exam_1->get();
                $ids = implode("','", $beanIds);
                if ($bean->resolution_date) {
                    $update_query = 'UPDATE cie_clinical_issue_exam SET resolution_date = ' . "'$bean->resolution_date'" . ' WHERE id IN ' . "('$ids')";
                } else {
                    $update_query = 'UPDATE cie_clinical_issue_exam SET resolution_date = null WHERE id IN ' . "('$ids')";
                }
                $db->query($update_query);
            }
        }
        self::$ran = true;
    }

}

