<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M01_Sales_Activity/Ext/Language/en_us.customaccounts_m01_sales_activity_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_M01_SALES_ACTIVITY_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M01_SALES_ACTIVITY_1_FROM_M01_SALES_ACTIVITY_TITLE_ID'] = 'Companies ID';
$mod_strings['LBL_ACCOUNTS_M01_SALES_ACTIVITY_1_FROM_M01_SALES_ACTIVITY_TITLE'] = 'Companies';

?>
<?php
// Merged from custom/Extension/modules/M01_Sales_Activity/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_RECORD'] = 'Create Sales Activity';
$mod_strings['LNK_LIST'] = 'View Sales';
$mod_strings['LBL_MODULE_NAME'] = 'Sales';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = '(Inactive) Sales';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New (Inactive) Sales';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import (Inactive) Sales vCard';
$mod_strings['LNK_IMPORT_M01_SALES_ACTIVITY'] = 'Import Sales Activity';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Sales Activities List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search (Inactive) Sales';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Sales';
$mod_strings['LBL_M01_SALES_ACTIVITY_FOCUS_DRAWER_DASHBOARD'] = 'Sales Focus Drawer';
$mod_strings['LBL_M01_SALES_ACTIVITY_RECORD_DASHBOARD'] = 'Sales Record Dashboard';

?>
