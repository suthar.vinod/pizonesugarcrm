<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RMS_Room/Ext/Vardefs/m06_error_rms_room_1_RMS_Room.php

// created: 2019-02-21 20:21:31
$dictionary["RMS_Room"]["fields"]["m06_error_rms_room_1"] = array (
  'name' => 'm06_error_rms_room_1',
  'type' => 'link',
  'relationship' => 'm06_error_rms_room_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_RMS_ROOM_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_rms_room_1m06_error_ida',
);

?>
<?php
// Merged from custom/Extension/modules/RMS_Room/Ext/Vardefs/sugarfield_building_c.php

 // created: 2019-02-21 19:32:43
$dictionary['RMS_Room']['fields']['building_c']['labelValue']='Building';
$dictionary['RMS_Room']['fields']['building_c']['dependency']='';
$dictionary['RMS_Room']['fields']['building_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/RMS_Room/Ext/Vardefs/sugarfield_capacity_c.php

 // created: 2019-02-21 19:33:24
$dictionary['RMS_Room']['fields']['capacity_c']['labelValue']='Capacity';
$dictionary['RMS_Room']['fields']['capacity_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RMS_Room']['fields']['capacity_c']['enforced']='';
$dictionary['RMS_Room']['fields']['capacity_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/RMS_Room/Ext/Vardefs/sugarfield_name.php

 // created: 2019-06-19 15:18:22
$dictionary['RMS_Room']['fields']['name']['len']='255';
$dictionary['RMS_Room']['fields']['name']['audited']=true;
$dictionary['RMS_Room']['fields']['name']['massupdate']=false;
$dictionary['RMS_Room']['fields']['name']['unified_search']=false;
$dictionary['RMS_Room']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['RMS_Room']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RMS_Room/Ext/Vardefs/sugarfield_room_number_c.php

 // created: 2019-08-09 11:42:47
$dictionary['RMS_Room']['fields']['room_number_c']['labelValue']='Room #';
$dictionary['RMS_Room']['fields']['room_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RMS_Room']['fields']['room_number_c']['enforced']='';
$dictionary['RMS_Room']['fields']['room_number_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/RMS_Room/Ext/Vardefs/sugarfield_room_type_c.php

 // created: 2022-02-01 07:39:21
$dictionary['RMS_Room']['fields']['room_type_c']['labelValue']='Type';
$dictionary['RMS_Room']['fields']['room_type_c']['dependency']='';
$dictionary['RMS_Room']['fields']['room_type_c']['required_formula']='';
$dictionary['RMS_Room']['fields']['room_type_c']['readonly_formula']='';
$dictionary['RMS_Room']['fields']['room_type_c']['visibility_grid']='';

 
?>
