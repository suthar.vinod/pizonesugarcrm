<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TC_NAMSA_Test_Codes/Ext/Layoutdefs/tc_namsa_test_codes_m03_work_product_code_1_TC_NAMSA_Test_Codes.php

 // created: 2021-12-07 12:00:27
$layout_defs["TC_NAMSA_Test_Codes"]["subpanel_setup"]['tc_namsa_test_codes_m03_work_product_code_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product_Code',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'get_subpanel_data' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/TC_NAMSA_Test_Codes/Ext/Layoutdefs/_overrideTC_NAMSA_Test_Codes_subpanel_tc_namsa_test_codes_m03_work_product_code_1.php

//auto-generated file DO NOT EDIT
$layout_defs['TC_NAMSA_Test_Codes']['subpanel_setup']['tc_namsa_test_codes_m03_work_product_code_1']['override_subpanel_name'] = 'TC_NAMSA_Test_Codes_subpanel_tc_namsa_test_codes_m03_work_product_code_1';

?>
