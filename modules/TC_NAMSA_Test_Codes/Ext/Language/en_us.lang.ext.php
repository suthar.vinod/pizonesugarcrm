<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TC_NAMSA_Test_Codes/Ext/Language/en_us.customtc_namsa_test_codes_m03_work_product_code_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE'] = 'Work Product Codes';
$mod_strings['LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_TC_NAMSA_TEST_CODES_TITLE'] = 'Work Product Codes';

?>
<?php
// Merged from custom/Extension/modules/TC_NAMSA_Test_Codes/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'NAMSA Product Code';
$mod_strings['LBL_DESCRIPTION'] = 'NAMSA Product Description';
$mod_strings['LBL_FUNCTIONAL_AREA'] = 'Functional Area';
$mod_strings['LBL_STANDARD_PROTOCOL_C_ERD_ERROR_DOCUMENTS_ID'] = 'Standard Protocol (related Error Documents ID)';
$mod_strings['LBL_STANDARD_PROTOCOL'] = 'Standard Protocol';

?>
