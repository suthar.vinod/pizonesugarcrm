<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TC_NAMSA_Test_Codes/Ext/Vardefs/sugarfield_name.php

 // created: 2021-12-07 11:58:36
$dictionary['TC_NAMSA_Test_Codes']['fields']['name']['audited']=true;
$dictionary['TC_NAMSA_Test_Codes']['fields']['name']['unified_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/TC_NAMSA_Test_Codes/Ext/Vardefs/sugarfield_description.php

 // created: 2021-12-07 11:59:14
$dictionary['TC_NAMSA_Test_Codes']['fields']['description']['unified_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/TC_NAMSA_Test_Codes/Ext/Vardefs/tc_namsa_test_codes_m03_work_product_code_1_TC_NAMSA_Test_Codes.php

// created: 2021-12-07 12:00:27
$dictionary["TC_NAMSA_Test_Codes"]["fields"]["tc_namsa_test_codes_m03_work_product_code_1"] = array (
  'name' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'type' => 'link',
  'relationship' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Code',
  'bean_name' => 'M03_Work_Product_Code',
  'vname' => 'LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_TC_NAMSA_TEST_CODES_TITLE',
  'id_name' => 'tc_namsa_t49d3t_codes_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/TC_NAMSA_Test_Codes/Ext/Vardefs/sugarfield_functional_area_c.php

 // created: 2021-12-07 12:04:35
$dictionary['TC_NAMSA_Test_Codes']['fields']['functional_area_c']['labelValue']='Functional Area';
$dictionary['TC_NAMSA_Test_Codes']['fields']['functional_area_c']['dependency']='';
$dictionary['TC_NAMSA_Test_Codes']['fields']['functional_area_c']['required_formula']='';
$dictionary['TC_NAMSA_Test_Codes']['fields']['functional_area_c']['readonly_formula']='';
$dictionary['TC_NAMSA_Test_Codes']['fields']['functional_area_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/TC_NAMSA_Test_Codes/Ext/Vardefs/sugarfield_erd_error_documents_id_c.php

 // created: 2021-12-07 12:05:49

 
?>
<?php
// Merged from custom/Extension/modules/TC_NAMSA_Test_Codes/Ext/Vardefs/sugarfield_standard_protocol_c.php

 // created: 2021-12-07 12:05:49
$dictionary['TC_NAMSA_Test_Codes']['fields']['standard_protocol_c']['labelValue']='Standard Protocol';
$dictionary['TC_NAMSA_Test_Codes']['fields']['standard_protocol_c']['dependency']='';
$dictionary['TC_NAMSA_Test_Codes']['fields']['standard_protocol_c']['required_formula']='';
$dictionary['TC_NAMSA_Test_Codes']['fields']['standard_protocol_c']['readonly_formula']='';

 
?>
