<?php
// WARNING: The contents of this file are auto-generated.


// created: 2021-12-07 12:00:27
$viewdefs['TC_NAMSA_Test_Codes']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TC_NAMSA_TEST_CODES_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'context' => 
  array (
    'link' => 'tc_namsa_test_codes_m03_work_product_code_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['TC_NAMSA_Test_Codes']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'tc_namsa_test_codes_m03_work_product_code_1',
  'view' => 'subpanel-for-tc_namsa_test_codes-tc_namsa_test_codes_m03_work_product_code_1',
);
