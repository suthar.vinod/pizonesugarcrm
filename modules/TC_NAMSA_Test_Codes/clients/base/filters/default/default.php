<?php
// created: 2021-12-07 12:08:22
$viewdefs['TC_NAMSA_Test_Codes']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'assigned_user_name' => 
    array (
    ),
    'functional_area_c' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'name' => 
    array (
    ),
    'description' => 
    array (
    ),
    'standard_protocol_c' => 
    array (
    ),
    'tag' => 
    array (
    ),
  ),
);