<?php

if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

class checkwpcfieldHook
{
	static $already_ran = false;
	public function checkwpcfield($bean, $event, $arguments)
	{
		if (self::$already_ran == true) return; //So that hook will only trigger once
		self::$already_ran = true;
		global $db, $current_user;
		$site_url = $GLOBALS['sugar_config']['site_url'];

			$wpCode = $bean->m03_work_product_code_id1_c;
			$WPCBean = BeanFactory::retrieveBean('M03_Work_Product_Code', $wpCode);	
			
			$GLOBALS['log']->fatal(' WP Name Check WPC Record == ' . $bean->name . ' SPA Date Reconciled Date ==> '. $bean->bc_spa_reconciled_date_c .' SPA Date Reconciled Date fetched==> ' . $bean->fetched_row['bc_spa_reconciled_date_c'] .' SPA Date TD ===> ' . $bean->spa_reconciled_date_tds_c);
			if (empty($bean->spa_reconciled_date_tds_c) && $bean->bc_spa_reconciled_date_c != $bean->fetched_row['bc_spa_reconciled_date_c']) {
                $bean->spa_reconciled_date_tds_c = $bean->name;
                $bean->wpchavevalue = 1;
				$GLOBALS['log']->fatal(' WP Name Check WPC Record wpchavevalue == ' . $bean->wpchavevalue);
            }
	}
}
