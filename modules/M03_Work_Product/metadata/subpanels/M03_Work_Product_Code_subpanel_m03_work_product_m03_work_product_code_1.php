<?php
// created: 2021-01-21 16:11:12
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'test_system_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TEST_SYSTEM',
    'width' => 10,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);