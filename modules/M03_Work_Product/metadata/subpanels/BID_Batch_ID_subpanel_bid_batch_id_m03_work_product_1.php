<?php
// created: 2022-04-26 10:58:43
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'work_product_status_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_WORK_PRODUCT_STATUS',
    'width' => 10,
  ),
  'work_product_compliance_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_WORK_PRODUCT_COMPLIANCE',
    'width' => 10,
  ),
  'study_director_script_c' => 
  array (
    'readonly' => false,
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_STUDY_DIRECTOR_SCRIPT',
    'id' => 'CONTACT_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Contacts',
    'target_record_key' => 'contact_id_c',
  ),
  'accounts_m03_work_product_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_NAME_FIELD_TITLE',
    'id' => 'ACCOUNTS_M03_WORK_PRODUCT_1ACCOUNTS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Accounts',
    'target_record_key' => 'accounts_m03_work_product_1accounts_ida',
  ),
  'contacts_m03_work_product_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_M03_WORK_PRODUCT_1CONTACTS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Contacts',
    'target_record_key' => 'contacts_m03_work_product_1contacts_ida',
  ),
  'm01_sales_m03_work_product_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE',
    'id' => 'M01_SALES_M03_WORK_PRODUCT_1M01_SALES_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'M01_Sales',
    'target_record_key' => 'm01_sales_m03_work_product_1m01_sales_ida',
  ),
);