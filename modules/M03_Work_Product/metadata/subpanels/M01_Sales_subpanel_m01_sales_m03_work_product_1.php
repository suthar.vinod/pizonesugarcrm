<?php
// created: 2022-04-05 11:55:19
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'title_description_c' => 
  array (
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_TITLE_DESCRIPTION',
    'width' => 10,
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'related_fields' => 
    array (
      0 => 'assigned_user_id',
    ),
    'vname' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'accounts_m03_work_product_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_NAME_FIELD_TITLE',
    'id' => 'ACCOUNTS_M03_WORK_PRODUCT_1ACCOUNTS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Accounts',
    'target_record_key' => 'accounts_m03_work_product_1accounts_ida',
  ),
  'contacts_m03_work_product_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_M03_WORK_PRODUCT_1CONTACTS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Contacts',
    'target_record_key' => 'contacts_m03_work_product_1contacts_ida',
  ),
  'timeline_type_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_TIMELINE_TYPE',
    'width' => 10,
    'default' => true,
  ),
  'first_procedure_c' => 
  array (
    'readonly' => false,
    'type' => 'date',
    'vname' => 'LBL_FIRST_PROCEDURE',
    'width' => 10,
    'default' => true,
  ),
  'work_product_status_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_WORK_PRODUCT_STATUS',
    'width' => 10,
  ),
  'test_price_c' => 
  array (
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_TEST_PRICE',
    'currency_format' => true,
    'width' => 10,
  ),
);