<?php
$popupMeta = array (
    'moduleMain' => 'M03_Work_Product',
    'varName' => 'M03_Work_Product',
    'orderBy' => 'm03_work_product.name',
    'whereClauses' => array (
  'name' => 'm03_work_product.name',
  'study_director_script_c' => 'm03_work_product.study_director_script_c',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'study_director_script_c',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'study_director_script_c' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_STUDY_DIRECTOR_SCRIPT',
    'id' => 'CONTACT_ID_C',
    'link' => true,
    'width' => 10,
    'name' => 'study_director_script_c',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'WORK_PRODUCT_STATUS_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_WORK_PRODUCT_STATUS',
    'width' => 10,
  ),
),
);
