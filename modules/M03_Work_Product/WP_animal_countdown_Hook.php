<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class WP_animalCountHook{
	function UpdateAnimalCountdownValues($bean, $event, $arguments){
		global $db,$current_user;
		
		$current_date = date("Y-m-d"); //current date
		if($bean->id!="" && ($bean->iacuc_approval_date_c!= $bean->fetched_row['iacuc_approval_date_c'])){

			$iacucDate 				= $bean->iacuc_approval_date_c;
			/* iacuc_first_annual_review_c and  iacuc_second_annual_review_c Calculations removed by further requirement on 17 June 2022 By: Harshit Shreshthi Ticket No: 2527*/
			//$iacucFirstReviewDate	= date("Y-m-t", strtotime(date("Y-m-d",strtotime($iacucDate))." + 1 year"));
			//$iacucSecondReviewDate	= date("Y-m-t", strtotime(date("Y-m-d",strtotime($iacucDate))." + 2 year"));
			$iacucThirdReviewDate	= date("Y-m-d", strtotime(date("Y-m-d",strtotime($iacucDate))." + 3 year"));
			$iacucThirdReviewDate	= date("Y-m-d", strtotime(date("Y-m-d",strtotime($iacucThirdReviewDate))." - 1 day"));
			//$bean->iacuc_first_annual_review_c	=  $iacucFirstReviewDate;	
			//$bean->iacuc_second_annual_review_c	=  $iacucSecondReviewDate;	
			$bean->iacuc_triennial_review_c 	=  $iacucThirdReviewDate;	
		}
		
		//m03_work_product_wpe_work_product_enrollment_1_c
		if($bean->id!="" && ($bean->id!="4a529aae-5165-3149-6db7-5702c7b98a5e" && $bean->id!="811ae58e-9281-2755-25d5-5702c7252696")){
			//$GLOBALS['log']->fatal('WP:Update Animal countdown.');
			$workProductId = $bean->id;
			$wp_bid			= $bean->bid_batch_id_m03_work_product_1bid_batch_id_ida;

			$onStudyWithoutTSCount		= 0;
			$onStudyWithTSCount			= 0;
			$totalOnStudyCount			= 0;
			$onStudyBackupWithTSCount	= 0;
			$onStudyBackupWithoutTSCount = 0;
			$totalOnStudyBackypCount	=  0;
			$WPTS 						= array();
			// Query to get On_Study/On_Study Transferred Count
			 
			
			/*Query to get total number of onStudy/onStudyTransferred Enrollment status Without Test System*/
			$studyWithoutTS_sql = "SELECT  WP_WPE.`m03_work_p9bf5ollment_idb` 
			FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE 
			LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p9bf5ollment_idb`=WPE_CUST.id_c 
			LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p9bf5ollment_idb`=TS.anml_anima9941ollment_idb 
			WHERE WP_WPE.`m03_work_p7d13product_ida`='".$workProductId."' AND 
			(WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') AND WP_WPE.deleted=0 AND (TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NULL )"; 
			//TS.deleted=1 OR TS.deleted IS NULL OR
			
			$studyWithoutTS_exec = $db->query($studyWithoutTS_sql);
             
			if ($studyWithoutTS_exec->num_rows > 0) 
			{
				$onStudyWithoutTSCount = $studyWithoutTS_exec->num_rows; //total rows without TS

				while($fetchWOTS = $db->fetchByAssoc($studyWithoutTS_exec)) {
					$WPTS[]	= $fetchWOTS['m03_work_p9bf5ollment_idb'];
				}
			}
			
			/*Query to get total number of onStudy/onStudyTransferred Enrollment status With Test System*/
			$studyWithTS_sql = "SELECT  WP_WPE.`m03_work_p9bf5ollment_idb` 
			FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE 
			LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p9bf5ollment_idb`=WPE_CUST.id_c 
			LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p9bf5ollment_idb`=TS.anml_anima9941ollment_idb 
			WHERE WP_WPE.`m03_work_p7d13product_ida`='".$workProductId."' 
			AND (WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') AND TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NOT NULL  AND WP_WPE.deleted=0 AND TS.deleted=0 GROUP BY TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida";
                        
			$studyWithTS_exec = $db->query($studyWithTS_sql);
                        
			if ($studyWithTS_exec->num_rows > 0) 
			{
				$onStudyWithTSCount = $studyWithTS_exec->num_rows; //total rows with TS
				while($fetchWTS = $db->fetchByAssoc($studyWithTS_exec)) {
					$WPTS[]	= $fetchWTS['m03_work_p9bf5ollment_idb'];
				}
			}
 

			/*=========================================================*/
			/*=========================================================*/			

			/*Query to get total number of onStudy/onStudyTransferred Enrollment status Without Test System*/
			$blanket_sql = "SELECT  WP_WPE.`m03_work_p90c4ollment_idb` 
						FROM `m03_work_product_wpe_work_product_enrollment_2_c` AS WP_WPE 
						LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p90c4ollment_idb`=WPE_CUST.id_c 
						LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p90c4ollment_idb`=TS.anml_anima9941ollment_idb 
					WHERE WP_WPE.`m03_work_p9f23product_ida`='".$workProductId."' AND 
						(WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') 
						AND WP_WPE.deleted=0 AND (TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NULL )"; 
			//TS.deleted=1 OR TS.deleted IS NULL OR
			
			$blanket_exec = $db->query($blanket_sql);
             
			if ($blanket_exec->num_rows > 0) 
			{
				//$onStudyWithoutTSCount = $studyWithoutTS_exec->num_rows; //total rows without TS

				while($fetchB1 = $db->fetchByAssoc($blanket_exec)) {
					//$GLOBALS['log']->fatal('WP===>:'.$fetchB1['m03_work_p90c4ollment_idb']);
					$WPTS[]	= $fetchB1['m03_work_p90c4ollment_idb'];
				}
			}
			
			/*Query to get total number of onStudy/onStudyTransferred Enrollment status With Test System*/
			$blanket2_sql = "SELECT  WP_WPE.`m03_work_p90c4ollment_idb` 
							FROM `m03_work_product_wpe_work_product_enrollment_2_c` AS WP_WPE 
								LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p90c4ollment_idb`=WPE_CUST.id_c 
								LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p90c4ollment_idb`=TS.anml_anima9941ollment_idb 
							WHERE WP_WPE.`m03_work_p9f23product_ida`='".$workProductId."' 
								AND (WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred')
								AND TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NOT NULL  AND WP_WPE.deleted=0 AND TS.deleted=0 
							GROUP BY TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida";
                        
			$blanket2_exec = $db->query($blanket2_sql);
                        
			if ($blanket2_exec->num_rows > 0) 
			{
				//$onStudyWithTSCount = $studyWithTS_exec->num_rows; //total rows with TS
				while($fetchB2 = $db->fetchByAssoc($blanket2_exec)) {
					//$GLOBALS['log']->fatal('WP22===>:'.$fetchB2['m03_work_p90c4ollment_idb']);
					$WPTS[]	= $fetchB2['m03_work_p90c4ollment_idb'];
				}
			}

			/*Changes according to Ticket # 2269 */
			if($wp_bid!=""){
				/*Query to get total number of onStudy/onStudyTransferred Enrollment status Without Test System*/
				$studyWithoutTS_BIDsql = "SELECT  BID_WPE.bid_batch_f386ollment_idb
				FROM `bid_batch_id_wpe_work_product_enrollment_1_c` AS BID_WPE 
					LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON BID_WPE.`bid_batch_f386ollment_idb`=WPE_CUST.id_c 
					LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON BID_WPE.`bid_batch_f386ollment_idb`=TS.anml_anima9941ollment_idb 
				WHERE BID_WPE.`bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida`='".$wp_bid."' 
					AND  (WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') 
					AND BID_WPE.deleted=0 AND (TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NULL ) "; 
				//TS.deleted=1 OR TS.deleted IS NULL OR

				$studyWithoutTS_BIDexec = $db->query($studyWithoutTS_BIDsql);

				if ($studyWithoutTS_BIDexec->num_rows > 0) 
				{
					$onStudyWithoutTSBIDCount = $studyWithoutTS_BIDexec->num_rows; //total rows without TS
					while($fetchWOTS_BID = $db->fetchByAssoc($studyWithoutTS_BIDexec)) {
						$WPTS[]	= $fetchWOTS_BID['bid_batch_f386ollment_idb'];
					}
				}

				/*Query to get total number of onStudy/onStudyTransferred Enrollment status With Test System*/
				$studyWithTS_BIDsql = "SELECT BID_WPE.bid_batch_f386ollment_idb
				FROM `bid_batch_id_wpe_work_product_enrollment_1_c` AS BID_WPE 
				LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON BID_WPE.`bid_batch_f386ollment_idb`=WPE_CUST.id_c 
				LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON BID_WPE.`bid_batch_f386ollment_idb`=TS.anml_anima9941ollment_idb 
				WHERE BID_WPE.`bid_batch_id_wpe_work_product_enrollment_1bid_batch_id_ida`='".$wp_bid."' 
				AND  (WPE_CUST.enrollment_status_c='On Study' OR WPE_CUST.enrollment_status_c='On Study Transferred') AND TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NOT NULL  AND BID_WPE.deleted=0 AND TS.deleted=0 GROUP BY TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida";

				$studyWithTS_BIDexec = $db->query($studyWithTS_BIDsql);

				if ($studyWithTS_BIDexec->num_rows > 0) 
				{
					$onStudyWithTSBIDCount = $studyWithTS_BIDexec->num_rows; //total rows with TS
					while($fetchWTS_BID = $db->fetchByAssoc($studyWithTS_BIDexec)) {
						$WPTS[]	= $fetchWTS_BID['bid_batch_f386ollment_idb'];
					}
				}

			}
			/*=========================================================*/
			/*=========================================================*/ 
			$totalOnStudyCount  = count(array_unique($WPTS)); //$onStudyWithTSCount+$onStudyWithoutTSCount; //Total Count of OnStudy/OnStudyTransferred
			//$totalOnStudyCount  = $onStudyWithTSCount+$onStudyWithoutTSCount; //Total Count of OnStudy/OnStudyTransferred
	
	
			// Query to get On_Backup Study Count
			/*Query to get total number of onStudyBackup Enrollment status Without Test System*/            
           $studyBackupWithoutTSsql = "SELECT  WP_WPE.`m03_work_p9bf5ollment_idb` 
							FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE 
							LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p9bf5ollment_idb`=WPE_CUST.id_c 
							LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p9bf5ollment_idb`=TS.anml_anima9941ollment_idb 
							WHERE WP_WPE.`m03_work_p7d13product_ida`='".$workProductId."' AND 
							(WPE_CUST.enrollment_status_c='On Study Backup') AND WP_WPE.deleted=0 AND 
							(TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NULL )"; 
            //TS.deleted=1 OR TS.deleted IS NULL OR             
			$StudyBckpWithoutTS_exec = $db->query($studyBackupWithoutTSsql);
            $onStudyBackupCount = '0';
			if ($StudyBckpWithoutTS_exec->num_rows > 0) 
			{
				$onStudyBackupWithoutTSCount = $StudyBckpWithoutTS_exec->num_rows; //total Backup rows without TS
			}
		   
		   /*Query to get total number of onStudyBackup Enrollment status With Test System*/   
		   $studyBackupWithTSsql = "SELECT  WP_WPE.`m03_work_p9bf5ollment_idb` 
									FROM `m03_work_product_wpe_work_product_enrollment_1_c` AS WP_WPE 
									LEFT JOIN `wpe_work_product_enrollment_cstm` AS WPE_CUST ON WP_WPE.`m03_work_p9bf5ollment_idb`=WPE_CUST.id_c 
									LEFT JOIN `anml_animals_wpe_work_product_enrollment_1_c` AS TS ON WP_WPE.`m03_work_p9bf5ollment_idb`=TS.anml_anima9941ollment_idb 
									WHERE WP_WPE.`m03_work_p7d13product_ida`='".$workProductId."' AND (WPE_CUST.enrollment_status_c='On Study Backup') 
									AND  TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida IS NOT NULL 
									AND WP_WPE.deleted=0 AND TS.deleted=0 GROUP BY TS.anml_animals_wpe_work_product_enrollment_1anml_animals_ida";
                        
			$StudyBckpWithTS_exec = $db->query($studyBackupWithTSsql);
            
			if ($StudyBckpWithTS_exec->num_rows > 0) 
			{
				$onStudyBackupWithTSCount = $StudyBckpWithTS_exec->num_rows; //total Backup rows with TS
			}
			$totalOnStudyBackypCount  = $onStudyBackupWithTSCount+$onStudyBackupWithoutTSCount; //Total Count of OnStudyBackup
			
			// Get Total value from Bean
			$totalPrimary	= $bean->total_animals_used_primary_c;
			$totalBackup	= $bean->total_animals_used_backup_c;
			
			//Calculate countdown Value by subtracting onStudy/Backup value from Total value 
			$Primary_Animals_Countdown	= $totalPrimary-$totalOnStudyCount;
			$Backup_Animals_Countdown	= $totalBackup-$totalOnStudyBackypCount;
			
			//Save countdown value into bean	
			$bean->primary_animals_countdown_c	=	$Primary_Animals_Countdown;
			$bean->back_up_animals_countdown_c	= 	$Backup_Animals_Countdown;
			
			if($bean->m03_work_product_id_c=="" && $bean->fetched_row['m03_work_product_id_c']!=""){
				$bean->blanket_protocol_hidden_c  = $bean->fetched_row['m03_work_product_id_c'];
			}
			
			$wpCode = $bean->m03_work_product_code_id1_c;
			if($bean->iacuc_approval_date_c!="" && ($wpCode=='9634c914-fe07-11e6-a663-02feb3423f0d' || $wpCode=='0527341e-d0d4-11e9-8661-06eddc549468')){
				$iacucDate =  $bean->iacuc_approval_date_c;
				$blnkQuery = "SELECT sum(IFNULL(WPCSTM.primary_animals_countdown_c,0)) PrimaryAnimalRemaining,
								sum(IFNULL(WPCSTM.back_up_animals_countdown_c,0)) BackupAnimalRemaining
								FROM m03_work_product AS WP
								LEFT JOIN  m03_work_product_erd_error_documents_1_c AS WPERD  ON 
								WP.id=WPERD.m03_work_product_erd_error_documents_1m03_work_product_ida AND WPERD.deleted=0

								LEFT JOIN  erd_error_documents ERD  
								ON ERD.id=WPERD.m03_work_product_erd_error_documents_1erd_error_documents_idb AND ERD.deleted=0
								
								LEFT JOIN m03_work_product_cstm WPCSTM  ON WP.id = WPCSTM.id_c
								LEFT JOIN m03_work_product WP1 ON WP1.id = WPCSTM.m03_work_product_id_c AND IFNULL(WP1.deleted,0)=0 

							WHERE 
								WPCSTM.m03_work_product_id_c='".$bean->id."'
								AND 
								WPCSTM.work_product_compliance_c IN ('NonGLP Discovery','NonGLP Structured','GLP','GLP Discontinued','nonGLP')
								AND 
								WPCSTM.first_procedure_c >= '".$iacucDate."' 
								AND  
								WP.deleted=0 AND IFNULL(WP1.deleted,0)=0";
				$blnkQuery_exec = $db->query($blnkQuery);
				$blnkSum	= 0;
				$primarySum = 0;
				$backupSum	= 0;
				if ($blnkQuery_exec->num_rows > 0) 
				{
					while($fetchBlnk = $db->fetchByAssoc($blnkQuery_exec)) {
						$primarySum	= $fetchBlnk['PrimaryAnimalRemaining'];
						$backupSum	= $fetchBlnk['BackupAnimalRemaining']; 
					}
					$blnkSum = $primarySum+$backupSum;
					$bean->blanket_animals_remaining_c = ($bean->total_animals_used_primary_c + $blnkSum); 
				}
				
			}
			
		}else{
			//$GLOBALS['log']->fatal('WP:Not Updating Animal countdown for :'.$bean->id);
		}
	}
}
?>