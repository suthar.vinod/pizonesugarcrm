<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class deleteCountdownAuditLog{
	function deleteCountdownAudit($bean, $event, $arguments) {
		global $db;
		$wpeID = $bean->id;		
		$queryAuditLog = "SELECT id,count(*) as NUM FROM `m03_work_product_audit` WHERE `field_name`='primary_animals_countdown_c' AND `parent_id`='".$wpeID."' group by before_value_string,after_value_string,date_created";
		
		//$GLOBALS['log']->fatal('queryAuditLog1 ====>:' . $queryAuditLog);
		$auditLogResult = $db->query($queryAuditLog);
		
		if($auditLogResult->num_rows > 0 ){
			while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
				if($fetchAuditLog['NUM']>1){
					$recordID = $fetchAuditLog['id'];
					$sql_DeleteAudit = "DELETE from `m03_work_product_audit` WHERE `field_name`='primary_animals_countdown_c' AND `parent_id`='".$wpeID."' AND id = '".$recordID."'";
					$db->query($sql_DeleteAudit);
				}				
			}
		} 
		
		
		$queryAuditLog = "SELECT id,count(*) as NUM FROM `m03_work_product_audit` WHERE `field_name`='blanket_animals_remaining_c' AND `parent_id`='".$wpeID."' group by before_value_string,after_value_string,date_created";
		//$GLOBALS['log']->fatal('queryAuditLog2 ====>:' . $queryAuditLog);
		$auditLogResult = $db->query($queryAuditLog);
		
		if($auditLogResult->num_rows > 0 ){
			while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
				if($fetchAuditLog['NUM']>1){
					$recordID = $fetchAuditLog['id'];
					$sql_DeleteAudit = "DELETE from `m03_work_product_audit` WHERE `field_name`='blanket_animals_remaining_c' AND `parent_id`='".$wpeID."' AND id = '".$recordID."'";
					$db->query($sql_DeleteAudit);
				}				
			}
		} 
	}
}
?>