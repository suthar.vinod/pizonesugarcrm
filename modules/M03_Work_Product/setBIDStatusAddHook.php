<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
class setBIDStatusReadOnlyAdd
{
    public function setBIDStatus($bean, $event, $arguments)
    {
        global $db, $current_user;
        if ($arguments['dataChanges']['work_product_status_c']['after'] == 'Archived') {
            $count       = 0;
            $batchId     = $bean->bid_batch_id_m03_work_product_1bid_batch_id_ida;
            if($batchId!=""){
                $beanBatch   = BeanFactory::retrieveBean('BID_Batch_ID', $batchId);
                // If relationship is loaded
                $beanBatch->load_relationship('bid_batch_id_m03_work_product_1');
                $linkedWP     = $beanBatch->bid_batch_id_m03_work_product_1->get();
                foreach ($linkedWP as $relatedWP) {
                    $WPBean1 = BeanFactory::getBean('M03_Work_Product', $relatedWP);
                    if ($WPBean1->work_product_status_c == 'Archived') {
                        $count++;
                    } else {
                        $count = 0;
                        break;
                    }
                }
                if ($count !== 0) {
                    //$beanBatch->status_c = 'Closed';
                    $updateQuery = "UPDATE bid_batch_id_cstm SET `status_c`='Closed' WHERE id_c='".$batchId."'"; 
				    $updateQuery_exec = $db->query($updateQuery);
                   
                    $source = '{"subject":{"_type":"logic-hook","class":"setBIDStatus","method":"setBIDStatus"},"attributes":[]}';
            
                    $auditEventid = create_guid();
                    $auditsql = 'INSERT INTO bid_batch_id_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $batchId . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"status_c","varchar","' . $beanBatch->status_c . '","Closed")';
                    $auditsqlResult = $db->query($auditsql);   
                    
                    $auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $batchId . "','BID_Batch_ID','" . $source . "')";
						$db->query($auditsqlStatus);
                      
                }
                //$beanBatch->status_readonly_c = $count;
                $updateQuerystatus = "UPDATE bid_batch_id_cstm SET `status_readonly_c`='".$count."' WHERE id_c='".$batchId."'"; 
				$updateQuery_exec_status = $db->query($updateQuerystatus);
                //$beanBatch->save();               
                
            }            
        }
    }
}
