<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class CopyTaskDesignWPHooknew
{
    static $already_ran = false;
    function copyTaskDesignnew($bean, $event, $arguments)
    {
        if (self::$already_ran == true) return; //So that hook will only trigger once
        self::$already_ran = true;

        global $db, $current_user;
        global $app_list_strings;
        //$GLOBALS['log']->fatal('CopyTaskDesignWPHook 14   ===> ' . print_r($arguments, 1));
        $GLOBALS['log']->fatal('bean->spa_reconciled_date_tds_c  === ' . $bean->spa_reconciled_date_tds_c);
        if ($bean->spa_reconciled_date_tds_c != "") {
            /* Ticket #1849 - Update TD auto-creation WPC workflow  */
            if ($bean->spa_reconciled_date_tds_c == "") {
                $bean->spa_reconciled_date_tds_c = $bean->bc_spa_reconciled_date_c;
                $bean->save();
            }
            $wpCode = $bean->m03_work_product_code_id1_c;
            $WPCBean = BeanFactory::retrieveBean('M03_Work_Product_Code', $wpCode);
            $WPCBean->load_relationship('m03_work_product_code_taskd_task_design_1');
            $relatedTD = $WPCBean->m03_work_product_code_taskd_task_design_1->get();
            //$GLOBALS['log']->fatal(' relatedTD ' . print_r($relatedTD, 1));

            $copyingTaskDesignIds = array();
            $tsArr = array();
            $tsRelationArr = array();

            $p1arr = array();

            foreach ($relatedTD as $TDId) {
                $clonedBean = \BeanFactory::getBean('TaskD_Task_Design', $TDId);
                $other_equipment = $clonedBean->other_equipment_c;
                $clonedBean->load_relationship("taskd_task_design_taskd_task_design_1");

                $sqlGetTaskDesignIds = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_ida as TDId
                                    FROM taskd_task_design_taskd_task_design_1_c 
                                    where taskd_task_design_taskd_task_design_1taskd_task_design_idb='$TDId' and deleted=0";
                $resultsSqlGetTaskDesignIds = $db->query($sqlGetTaskDesignIds);
                $rowSqlGetTaskDesignIds = $db->fetchByAssoc($resultsSqlGetTaskDesignIds);
                $taskDesignIds = $rowSqlGetTaskDesignIds['TDId'];

                //$GLOBALS['log']->fatal('sqlGetTaskDesignIds  === ' . $sqlGetTaskDesignIds);
                $wpBean = BeanFactory::getBean('M03_Work_Product', $bean->id);
                // $testSystemIds = $clonedBean->anml_animals_taskd_task_design_1->get();
                $clonedBean->id = create_guid();
                $tsArr[$clonedBean->id] = $TDId;
                $tsRelationArr[$TDId] = $taskDesignIds;
                $clonedBean->new_with_id = true;
                $clonedBean->fetched_row = null;
                $date_entered = date("Y-m-d H:i:s", time());

                $p1arr[$TDId] = $taskDesignIds;
                $a1arr[$clonedBean->id] = $TDId;
                $b1arr[$TDId] = $clonedBean->id;
                $clonedBean->type_2 = "Actual SP";
                $clonedBean->other_equipment_c = $other_equipment;
                //$clonedBean->u_units_id_c = $clonedBean->u_units_id_c;
                $copyingTaskDesignIds[] = $clonedBean->id;
                //$clonedBean->save();
                $clonedBean->m03_work_product_taskd_task_design_1->add($bean->id);
                //$GLOBALS['log']->fatal('Bean Name  ===> ' . $bean->name);
                $wpName = $bean->name;
                $task_type = $clonedBean->task_type;
                $custom_task = $clonedBean->custom_task;
                $standard_task = $clonedBean->standard_task;
                //$phase = $clonedBean->phase_c;
                $phase_dom = $app_list_strings['td_phase_list'];
                $phase = $phase_dom[$clonedBean->phase_c];
                $batch_id       = $clonedBean->bid_batch_id_taskd_task_design_1bid_batch_id_ida;
                $BatchBean      =   BeanFactory::getBean('BID_Batch_ID', $batch_id);
                $BatchName      =   $BatchBean->name;
                $clonedBean->date_entered = $date_entered;
                if ($task_type == 'Custom') {
                    $standard_task = '';
                    $standard_task_val = $custom_task;
                } else if ($task_type == 'Standard') {
                    $custom_task = '';
                    $standard_task_val = $standard_task;
                }
                if ($BatchName != "") {
                    $clonedBean->name = $wpName . ' ' . $phase . ' ' . $BatchName . ' ' . $standard_task_val;
                } else {
                    $clonedBean->name = $wpName . ' ' . $phase . ' ' . $standard_task_val;
                }
                if ($standard_task == 'Extract In') {
                    $clonedBean->start_integer = $wpBean->extraction_start_integer_c;
                    $clonedBean->end_integer = $wpBean->extraction_end_integer_c;
                    $clonedBean->integer_units = $wpBean->integer_units_c;
                    $clonedBean->u_units_id_c = $wpBean->u_units_id_c;
                    $unit_Bean = BeanFactory::getBean('U_Units', $clonedBean->u_units_id_c);
                    $unit_Bean_name = $unit_Bean->name;
                    $clonedBean->time_window = $unit_Bean_name . " " . $clonedBean->start_integer . "-" . $clonedBean->end_integer;
                    //$GLOBALS['log']->fatal(' Extract In wpBean->first_procedure_c ' . $wpBean->first_procedure_c);
                    //$offset1 =  -18000;
                    $offset1 =  21600; //daylight Saving
                    if ($wpBean->first_procedure_c != '') {
                        $datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
                        $actual_datetime       = date("Y-m-d 17:00:00", $datefirst_procedure_c);
                        //$GLOBALS['log']->fatal(' Extract IN clonedBean->actual_datetime ' . $clonedBean->actual_datetime);
                    }
                    if ($wpBean->first_procedure_c != '' && $clonedBean->relative == "NA") {

                        $datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
                        $clonedBean->actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
                        $clonedBean->save();
                        //$GLOBALS['log']->fatal(' Extract In NA clonedBean->actual_datetime ' . $clonedBean->actual_datetime);
                    }
                    if ($actual_datetime != "") {
                        if ($unit_Bean_name == "Day") {
                            if ($clonedBean->relative == "1st Tier") {
                                $result = date($actual_datetime);
                                $result = strtotime($result . '+' . $clonedBean->start_integer . ' days');
                                $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
                                if (date("Y", $result) != "1970") {
                                    $clonedBean->single_date_2_c = date("Y-m-d", $result);
                                }
                                $result = date($actual_datetime);
                                $result = strtotime($result . '+' . $clonedBean->end_integer . ' days');
                                $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
                                //$GLOBALS['log']->fatal(' Extract In DAY 1st Tier clonedBean->relative ' . $clonedBean->relative);
                            }
                        } else if ($unit_Bean_name == "Hour") {
                            if ($clonedBean->relative == "1st Tier") {
                                $result = date($actual_datetime);
                                $result = strtotime($result . '+' . $clonedBean->start_integer . ' hours');
                                $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
                                if (date("Y", $result) != "1970") {
                                    $clonedBean->single_date_2_c = date("Y-m-d", $result);
                                }
                                $result = date($actual_datetime);
                                $result = strtotime($result . '+' . $clonedBean->end_integer . ' hours');
                                $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
                            }
                        } else if ($unit_Bean_name == "Minute") {

                            if ($clonedBean->relative == "1st Tier") {
                                $result = date($actual_datetime);
                                $result = strtotime($result . '+' . $clonedBean->start_integer . ' minutes');
                                $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
                                if (date("Y", $result) != "1970") {
                                    $clonedBean->single_date_2_c = date("Y-m-d", $result);
                                }
                                $result = date($actual_datetime);
                                $result = strtotime($result . '+' . $clonedBean->end_integer . ' minutes');
                                $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
                            }
                        }

                        if (($clonedBean->relative == "1st Tier" || $clonedBean->relative == "NA")) {
                            $clonedBean->planned_start_datetime_2nd = null;
                            $clonedBean->planned_end_datetime_2nd = null;
                        }
                        if (($clonedBean->relative == "2nd Tier" || $clonedBean->relative == "NA")) {
                            $clonedBean->planned_start_datetime_1st = null;
                            $clonedBean->planned_end_datetime_1st = null;
                        }
                    }
                } else {
                    //$GLOBALS['log']->fatal(' standard_task ' . $standard_task);
                    //$offset1 =  18000;
                    $offset1 =  21600; //daylight Saving
                    if ($wpBean->first_procedure_c != '') {

                        $datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
                        $actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
                        // $GLOBALS['log']->fatal(' Extract OUT clonedBean->actual_datetime ' . $clonedBean->actual_datetime);
                    }
                    if ($wpBean->first_procedure_c != '' && $clonedBean->relative == "NA") {

                        $datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
                        $clonedBean->actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
                        $clonedBean->save();
                        //$GLOBALS['log']->fatal(' Extract OUTNA  clonedBean->actual_datetime ' . $clonedBean->actual_datetime);
                    }
                    $clonedBean->u_units_id_c = $clonedBean->u_units_id_c;
                    $clonedBean->start_integer = $clonedBean->start_integer;
                    $clonedBean->end_integer = $clonedBean->end_integer;
                    $clonedBean->integer_units = $clonedBean->integer_units;
                    //$clonedBean->u_units_id_c = $wpBean->u_units_id_c;
                    $unit_Bean = BeanFactory::getBean('U_Units', $clonedBean->u_units_id_c);
                    $unit_Bean_name = $unit_Bean->name;
                    $clonedBean->time_window = $unit_Bean_name . " " . $clonedBean->start_integer . "-" . $clonedBean->end_integer;
                    //$clonedBean->time_window = $clonedBean->time_window;

                    if ($actual_datetime != "") {
                        if ($unit_Bean_name == "Day") {
                            if ($clonedBean->relative == "1st Tier") {
                                $result = date($actual_datetime);
                                $result = strtotime($result . '+' . $clonedBean->start_integer . ' days');
                                $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
                                if (date("Y", $result) != "1970") {
                                    $clonedBean->single_date_2_c = date("Y-m-d", $result);
                                }
                                $result = date($actual_datetime);
                                $result = strtotime($result . '+' . $clonedBean->end_integer . ' days');
                                $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
                                //$GLOBALS['log']->fatal(' Extract OUT DAY 1st Tier clonedBean->relative ' . $clonedBean->relative);
                            }
                        } else if ($unit_Bean_name == "Hour") {
                            //$GLOBALS['log']->fatal(' Not Empty "Minute" clonedBean->relative ' . $clonedBean->relative);
                            if ($clonedBean->relative == "1st Tier") {
                                $result = date($actual_datetime);
                                $result = strtotime($result . '+' . $clonedBean->start_integer . ' hours');
                                $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
                                if (date("Y", $result) != "1970") {
                                    $clonedBean->single_date_2_c = date("Y-m-d", $result);
                                }
                                $result = date($actual_datetime);
                                $result = strtotime($result . '+' . $clonedBean->end_integer . ' hours');
                                $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
                            }
                        } else if ($unit_Bean_name == "Minute") {
                            if ($clonedBean->relative == "1st Tier") {
                                $result = date($actual_datetime);
                                $result = strtotime($result . '+' . $clonedBean->start_integer . ' minutes');
                                $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
                                if (date("Y", $result) != "1970") {
                                    $clonedBean->single_date_2_c = date("Y-m-d", $result);
                                }
                                $result = date($actual_datetime);
                                $result = strtotime($result . '+' . $clonedBean->end_integer . ' minutes');
                                $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
                            }
                        }

                        if (($clonedBean->relative == "1st Tier" || $clonedBean->relative == "NA")) {
                            $clonedBean->planned_start_datetime_2nd = null;
                            $clonedBean->planned_end_datetime_2nd = null;
                        }
                        if (($clonedBean->relative == "2nd Tier" || $clonedBean->relative == "NA")) {
                            $clonedBean->planned_start_datetime_1st = null;
                            $clonedBean->planned_end_datetime_1st = null;
                        }
                    }
                }

                $clonedBean->save();

                $auditsql = 'update taskd_task_design_audit 
                set field_name= "u_units_id_c",
                after_value_string="' . $clonedBean->u_units_id_c . '" 
                Where parent_id = "' . $clonedBean->id . '"
                AND field_name="integer_units"';
                $db->query($auditsql);

                $queryAuditLog = "SELECT id,count(*) as NUM FROM `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' 
                    AND `parent_id`='" . $clonedBean->id . "' group by before_value_string,after_value_string,date_created";
                $auditLogResult = $db->query($queryAuditLog);
                // $GLOBALS['log']->fatal('sqlGetTaskDesignIds  ===  APA '.$queryAuditLog);
                if ($auditLogResult->num_rows > 0) {
                    while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
                        if ($fetchAuditLog['NUM'] > 1) {
                            $recordID = $fetchAuditLog['id'];
                            $sql_DeleteAudit = "DELETE from `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' AND `parent_id`='" . $clonedBean->id . "' AND id = '" . $recordID . "'";
                            $db->query($sql_DeleteAudit);
                            // $GLOBALS['log']->fatal('sqlGetTaskDesignIds  ===  APA '.$sql_DeleteAudit);

                        }
                    }
                }
                /** Save audit log for all updated fields*/
                $source = '{"subject":{"_type":"logic-hook","class":"CopyTaskDesignToGD","method":"CopyTaskDesignToGD"},"attributes":[]}';
                if ($clonedBean->relative != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"relative","datetime","","' . $clonedBean->relative . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->category != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"category","datetime","","' . $clonedBean->category . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->type_2 != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"type_2","datetime","","' . $clonedBean->type_2 . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->task_type != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"task_type","datetime","","' . $clonedBean->task_type . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->standard_task != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"standard_task","datetime","","' . $clonedBean->standard_task . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->start_integer != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"start_integer","datetime","","' . $clonedBean->start_integer . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->end_integer != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"end_integer","datetime","","' . $clonedBean->end_integer . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);
                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->time_window != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"time_window","datetime","","' . $clonedBean->time_window . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->integer_units != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"integer_units","datetime","","' . $clonedBean->integer_units . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->planned_start_datetime_1st != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_start_datetime_1st","datetime","","' . $clonedBean->planned_start_datetime_1st . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->planned_end_datetime_1st != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_end_datetime_1st","datetime","","' . $clonedBean->planned_end_datetime_1st . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->planned_start_datetime_2nd != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_start_datetime_2nd","datetime","","' . $clonedBean->planned_start_datetime_2nd . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->planned_end_datetime_2nd != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_end_datetime_2nd","datetime","","' . $clonedBean->planned_end_datetime_2nd . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->custom_task != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"custom_task","datetime","","' . $clonedBean->custom_task . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->order_2_c != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"order_2_c","datetime","","' . $clonedBean->order_2_c . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->equipment_required != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"equipment_required","datetime","","' . $clonedBean->equipment_required . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->other_equipment_c != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"other_equipment_c","datetime","","' . $clonedBean->other_equipment_c . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->phase_c != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"phase_c","datetime","","' . $clonedBean->phase_c . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->description != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"description","datetime","","' . $clonedBean->description . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->type_of_personnel_2_c != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"type_of_personnel_2_c","datetime","","' . $clonedBean->type_of_personnel_2_c . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->duration_integer_min_c != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"duration_integer_min_c","datetime","","' . $clonedBean->duration_integer_min_c . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }
            }

            // $GLOBALS['log']->fatal('p1arr  === ' . print_r($p1arr, 1));
            // $GLOBALS['log']->fatal('a1arr  === ' . print_r($a1arr, 1));
            // $GLOBALS['log']->fatal('b1arr  === ' . print_r($b1arr, 1));
            foreach ($p1arr as $key => $value) {
                $newBeanId = $b1arr[$key];
                $toBeRelateID = array_search($value, $a1arr);
                //$GLOBALS['log']->fatal(' Hii 96 === ' . $newBeanId . ' >>> ' . $toBeRelateID);
                if ($toBeRelateID != "") {
                    $uniqId = create_guid();
                    $sqlInsertRelationship = "INSERT INTO `taskd_task_design_taskd_task_design_1_c` 
            (`id`, `date_modified`, `deleted`, `taskd_task_design_taskd_task_design_1taskd_task_design_ida`, `taskd_task_design_taskd_task_design_1taskd_task_design_idb`) 
            VALUES ('$uniqId', now(), '0', '$toBeRelateID', '$newBeanId')";
                    $resultsSqlGetTaskDesignIds = $db->query($sqlInsertRelationship);
                }
            }

            $currentWPBean = BeanFactory::retrieveBean('M03_Work_Product', $bean->id);
            $currentWPBean->load_relationship('m03_work_product_taskd_task_design_1');
            $relatedTD1 = $currentWPBean->m03_work_product_taskd_task_design_1->get();
            //$GLOBALS['log']->fatal(' relatedTD1 ' . print_r($relatedTD1, 1));
            foreach ($relatedTD1 as $tdID) {
                $tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $tdID);
                $u_units = $tdBean->u_units_id_c;
                $unit_Bean = BeanFactory::getBean('U_Units', $u_units);
                $unit_Bean_name = $unit_Bean->name;
                //$GLOBALS['log']->fatal(' unit_Bean_name ' . $unit_Bean_name);
                if ($unit_Bean_name == "Day") {
                    $integer_u = 'days';
                } elseif ($unit_Bean_name == "Hour") {
                    $integer_u = 'hours';
                } else {
                    $integer_u = 'minutes';
                }
                //$GLOBALS['log']->fatal(' tdID tdBean->relative ' . $tdBean->relative);
                if ($tdBean->relative == "1st Tier") {
                    $tdBean1 = BeanFactory::retrieveBean('TaskD_Task_Design', $tdBean->id);
                    $tdBean1->load_relationship('taskd_task_design_taskd_task_design_1');
                    $taskDesignId1 = $tdBean1->taskd_task_design_taskd_task_design_1->get();

                    $Get_2nd_tier = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_idb FROM  `taskd_task_design_taskd_task_design_1_c` WHERE `taskd_task_design_taskd_task_design_1taskd_task_design_ida` = '" . $tdBean->id . "' AND deleted = 0;";
                    $Get_Result = $db->query($Get_2nd_tier);

                    if ($Get_Result->num_rows > 0) {
                        while ($fetchResult = $db->fetchByAssoc($Get_Result)) {
                            $recordID = $fetchResult['taskd_task_design_taskd_task_design_1taskd_task_design_idb'];
                            //$GLOBALS['log']->fatal(' recordID ' . $recordID);
                            $tdBean2 = BeanFactory::retrieveBean('TaskD_Task_Design', $recordID);
                            //$GLOBALS['log']->fatal(' tdBean->planned_start_datetime_1st ' . $tdBean->planned_start_datetime_1st);
                            if ($tdBean->planned_start_datetime_1st != "") {
                                $result = date($tdBean->planned_start_datetime_1st);
                                $result = strtotime($result . '+' . $tdBean2->start_integer . ' ' . $integer_u);
                                $tdBean2->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
                                if (date("Y", $result) != "1970") {
                                    $tdBean2->single_date_2_c = date("Y-m-d", $result);
                                }
                            } else {
                                $tdBean2->planned_start_datetime_2nd = null;
                            }
                            //$GLOBALS['log']->fatal(' tdID1 tdBean->planned_end_datetime_1st ' . $tdBean->planned_end_datetime_1st);
                            if ($tdBean->planned_end_datetime_1st != "") {
                                $result = date($tdBean->planned_end_datetime_1st);
                                $result = strtotime($result . '+' . $tdBean2->end_integer . ' ' . $integer_u);
                                $tdBean2->planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
                            } else {
                                $tdBean2->planned_end_datetime_2nd = null;
                            }
                            $tdBean2->save();
                        }
                    }
                }
            }
        }
    }
}
