<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class setCheckofAuditAfterRelationAddHook
{
    static $already_ran = false;
    function setCheckofAuditAfterRelationAdd($bean, $event, $arguments)
    {
        if (self::$already_ran == true) return; //So that hook will only trigger once
        self::$already_ran = true;

        global $db, $current_user;

        $month = date("F", strtotime($bean->first_procedure_c));
        $WPC = $bean->work_product_compliance_c;

        if ($bean->first_procedure_c != "" && $bean->fetched_row['first_procedure_c'] == "") {
            $bean->load_relationship("m03_work_product_taskd_task_design_1");
            $TaskDesignId = $bean->m03_work_product_taskd_task_design_1->get();
            //$GLOBALS['log']->fatal(' relatedTD ' . print_r($TaskDesignId, 1));
            foreach ($TaskDesignId as $TDId) {
                $TdBean = BeanFactory::getBean('TaskD_Task_Design', $TDId);
                $type = $TdBean->type_2;
                $audit_phase = $TdBean->audit_phase_c;
                if ($type == "Actual SP" && ($month == "January" || $month == "April" || $month == "July" ||  $month == "October") && $audit_phase == "Sample Prep" && $WPC == "GLP") {
                    $TdBean->audit_2_c = 1;
                }
                if ($type == "Actual SP" && ($month == "February" || $month == "May" || $month == "August" ||  $month == "November") && $audit_phase == "Application" && $WPC == "GLP") {
                    $TdBean->audit_2_c = 1;
                }
                if ($type == "Actual SP" && $bean->m03_work_product_code_id1_c != "c81c2577-bd7c-16e3-1248-5785ac111a73" && ($month == "March" || $month == "June" || $month == "September" ||  $month == "December") && $audit_phase == "Evaluation" && $WPC == "GLP") {
                    $TdBean->audit_2_c = 1;
                }
                if ($type == "Actual SP" && $bean->m03_work_product_code_id1_c == "c81c2577-bd7c-16e3-1248-5785ac111a73" && ($month == "March" || $month == "June" || $month == "September" ||  $month == "December") && $audit_phase == "Application" && $WPC == "GLP") {
                    $TdBean->audit_2_c = 1;
                }
                $TdBean->save();
            }
        }
    }
}
