({
    extendsFrom: 'RecordView',
    initialize: function(options) {
        var pulledBlanketProtocolId = null;
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.onload_function, this);
        this.model.once("sync",
            function() {
                this.model.on(
                    "change:work_product_code_2_c",
                    this.function_Code,
                    this
                );
            },
            this
        );

        this.model.once("sync",
            function() {
                this.model.on(
                    "change:test_system_c",
                    this.getBlanketProtocolData,
                    this
                );
            },
            this
        );

        this.model.once("sync",
            function() {
                this.model.on(
                    "change:bid_batch_id_m03_work_product_1_name",
                    this.getScheduleDateFromBatchId,
                    this
                );
            },
            this
        );

        this.context.on('button:edit_button:click', this.disableBlanketProtocolField, this);
        this.context.on('button:cancel_button:click', this.disableBlanketProtocolField, this);
        this.model.addValidationTask('check_deliverable_field', _.bind(this._doValidatedeliverableFiled, this));
        // this.model.on('change:bid_batch_id_m03_work_product_1_name', this.getScheduleDateFromBatchId, this);


    },

    // 519 Validation remove for WPC Pathology
    _doValidatedeliverableFiled: function (fields, errors, callback) {
        var wp_status   = this.model.get('work_product_status_c');
        var wpcid       = this.model.get("m03_work_product_code_id1_c");
        var test_system = this.model.get("test_system_c");
        var first_procedure = this.model.get("first_procedure_c");
        var test_article_name = this.model.get("test_article_name_c");
        var study_director_script = this.model.get("study_director_script_c");
        var study_director_initiation = this.model.get("study_director_script_c");
        var test_article_description = this.model.get("test_article_description_c");
        var title_description = this.model.get("title_description_c");
        var sc_requirement = this.model.get("sc_requirement_c");

        console.log("WPC id==> ", wpcid);
        console.log("wp_status==> ", wp_status);
        if (wpcid != '' && wpcid != undefined) {
            var WP_API = "rest/v10/M03_Work_Product_Code/" + wpcid + "?fields=functional_area_c";
            App.api.call("get", WP_API, null, {
                success: function (WPC_Data) {
                    var wpcfaname = WPC_Data.functional_area_c;
                    console.log('wpcfaname : name : ', wpcfaname);
                    if (wpcfaname != 'Pathology') {
                        if ((wp_status == "Testing" || wp_status == "Archived") && _.isEmpty(test_system)) {
                            console.log('test_system', test_system);
                            errors['test_system_c'] = errors['test_system_c'] || {};
                            errors['test_system_c'].required = true;
                        }
                        if ((wp_status == "Testing" || wp_status == "Archived") && _.isEmpty(first_procedure)) {
                            console.log('first_procedure', first_procedure);
                            errors['first_procedure_c'] = errors['first_procedure_c'] || {};
                            errors['first_procedure_c'].required = true;
                        }
                        if ((wp_status == "Testing" || wp_status == "Archived") && _.isEmpty(test_article_name)) {
                            console.log('test_article_name', test_article_name);
                            errors['test_article_name_c'] = errors['test_article_name_c'] || {};
                            errors['test_article_name_c'].required = true;
                        }
                        if ((wp_status == "Testing" || wp_status == "Archived") && _.isEmpty(study_director_script)) {  
                            console.log('study_director_script', study_director_script);                          
                            errors['study_director_script_c'] = errors['study_director_script_c'] || {};
                            errors['study_director_script_c'].required = true;
                        }
                        if ((wp_status == "Testing" || wp_status == "Archived") && _.isEmpty(study_director_initiation)) {
                            console.log('study_director_initiation', study_director_initiation);       
                            errors['study_director_initiation_c'] = errors['study_director_initiation_c'] || {};
                            errors['study_director_initiation_c'].required = true;
                        }
                        if ((wp_status == "Testing" || wp_status == "Archived") && _.isEmpty(title_description)) {    
                            console.log('title_description', title_description);                               
                            errors['title_description_c'] = errors['title_description_c'] || {};
                            errors['title_description_c'].required = true;
                        }
                        if ((wp_status == "Testing" || wp_status == "Archived") && _.isEmpty(sc_requirement)) {
                            console.log('sc_requirement', sc_requirement);       
                            errors['sc_requirement_c'] = errors['sc_requirement_c'] || {};
                            errors['sc_requirement_c'].required = true;
                        }                        
                        callback(null, fields, errors);
                    } else {
                        callback(null, fields, errors);
                    }
                }
            });
        } else {
            callback(null, fields, errors);
        }
    },
    //func getScheduleDateFromBatchId->Added by Harshit Shreshthi for Ticket @2377
    getScheduleDateFromBatchId: function(){
        // console.log("Working==>");
        var self = this;
        var batchId = this.model.get("bid_batch_id_m03_work_product_1bid_batch_id_ida");
        // console.log("batchId===>",batchId);
        app.api.call("read", "rest/v10/BID_Batch_ID/" + batchId+"?fields=scheduled_date_c", null, {
            success: function (res) {
                 //console.log("Response Record view===>",val);
                 if(res.scheduled_date_c != undefined || res.scheduled_date_c != ""){
                     self.model.set("first_procedure_c",res.scheduled_date_c);
                 }
            }.bind(self),
        });
    },
    disableBlanketProtocolField: function() {
        if (disableBlanketField) {
            $('div[data-name="blanket_protocol_c"]').css('pointer-events', 'none');
        } else {
            $('div[data-name="blanket_protocol_c"]').css('pointer-events', 'unset');
        }
    },




    getBlanketProtocolData: function() {
        var self = this;
        var workCodeName = self.model.get("work_product_code_2_c");
        var testSys = self.model.get("test_system_c");
        if (!workCodeName) {
            return;
        } else {

            var testSysName = self.model.get("test_system_c");
            var id = self.model.get("m03_work_product_code_id1_c");

            if (!testSysName) {
                url = app.api.buildURL("M03_Work_Product_Code/" + id + "/get_blanket_protocol");
            } else {
                url = app.api.buildURL("M03_Work_Product_Code/" + id + "/" + testSysName + "/get_blanket_protocol");
            }

            app.api.call("GET", url, {

            }, {
                success: function(data) {
                    if (data != 0) {
                        pulledBlanketProtocolId = data.WPBId;
                        self.model.set("m03_work_product_id_c", data.WPBId);
                        self.model.set("blanket_protocol_c", data.WPBName);
                        $('input[name="blanket_protocol_c"]').prop('disabled', true);

                    } else {
                        pulledBlanketProtocolId = null;
                        self.model.set("m03_work_product_id_c", "");
                        self.model.set("blanket_protocol_c", "");
                        $('div[data-name="blanket_protocol_c"]').css('pointer-events', 'unset');
                        $('input[name="blanket_protocol_c"]').prop('disabled', false);
                    }

                },
                error: function(err) {}
            });
        }
    },
    function_Code: function() {

        var self = this;
        if (self.model.get('work_product_code_2_c') != "") {
            var id = self.model.get("m03_work_product_code_id1_c");
            app.api.call("GET", app.api.buildURL("M03_Work_Product_Code/" + id + "?fields=total_animal_number_c"), {

            }, {
                success: function(data) {
                    if (data.total_animal_number_c)
                        self.model.set('initial_animals_approved_pri_c', data.total_animal_number_c);
                },
                error: function(err) {}
            });

            var testSysName = self.model.get("test_system_c");
            if (!testSysName) {
                url = app.api.buildURL("M03_Work_Product_Code/" + id + "/get_blanket_protocol");
            } else {
                url = app.api.buildURL("M03_Work_Product_Code/" + id + "/" + testSysName + "/get_blanket_protocol");
            }
            app.api.call("GET", url, {

            }, {
                success: function(data) {
                    if (data != 0) {
                        pulledBlanketProtocolId = data.WPBId;
                        self.model.set("m03_work_product_id_c", data.WPBId);
                        self.model.set("blanket_protocol_c", data.WPBName);
                        $('input[name="blanket_protocol_c"]').prop('disabled', true);

                    } else {

                        pulledBlanketProtocolId = null;

                        self.model.set("m03_work_product_id_c", "");
                        self.model.set("blanket_protocol_c", "");
                        $('div[data-name="blanket_protocol_c"]').css('pointer-events', 'unset');

                        $('input[name="blanket_protocol_c"]').prop('disabled', false);
                    }

                },
                error: function(err) {}
            });



        } else {
            pulledBlanketProtocolId = null;
            self.model.set("m03_work_product_id_c", "");
            self.model.set("blanket_protocol_c", "");
            $('div[data-name="blanket_protocol_c"]').css('pointer-events', 'unset');
            $('input[name="blanket_protocol_c"]').prop('disabled', false);
        }
    },
    // handleSave: function() {

    //     var self = this;
    //     //Checking if blanket auto pulled or not
    //     var blanketName = this.model.get("blanket_protocol_c");
    //     var blanketId = this.model.get("m03_work_product_id_c");
    //     if ((typeof pulledBlanketProtocolId === 'undefined') || !pulledBlanketProtocolId) {
    //         if (blanketName) {
    //             this.linkWorkProducts().then(function(result) {

    //                 // if (result === "success") {
    //                 //     app.alert.dismiss("link_wps");
    //                 // }
    //             }.bind(this));
    //         } else {
    //             this.unLinkWorkProducts().then(function(result) {

    //                 // if (result === "success") {
    //                 //     app.alert.dismiss("link_wps");
    //                 // }
    //             }.bind(this));
    //         }
    //     }
    //     this._super('handleSave');
    //     this.model.fetch();

    // },

    // linkWorkProducts: function() {
    //     return new Promise(function(resolve, reject) {
    //         var currentWPID = this.model.get("id");
    //         var WPCID = this.model.get("m03_work_product_code_id1_c");

    //         // app.alert.show('link_wps', {
    //         //     level: 'info',
    //         //     messages: 'Linking Work Products...'
    //         // });

    //         url = app.api.buildURL("M03_Work_Product_Code/" + WPCID + "/" + currentWPID + "/link" + "/create");

    //         app.api.call("GET", url, {

    //         }, {
    //             success: function(data) {
    //                 resolve("success");

    //             },
    //             error: function(err) {
    //                 resolve("error");
    //             }
    //         });

    //     }.bind(this));
    // },
    // unLinkWorkProducts: function() {
    //     return new Promise(function(resolve, reject) {
    //         var currentWPID = this.model.get("id");
    //         var WPCID = this.model.get("m03_work_product_code_id1_c");

    //         // app.alert.show('link_wps', {
    //         //     level: 'info',
    //         //     messages: 'Unlinking Work Products...'
    //         // });

    //         url = app.api.buildURL("M03_Work_Product_Code/" + WPCID + "/" + currentWPID + "/unlink" + "/create");

    //         app.api.call("GET", url, {

    //         }, {
    //             success: function(data) {
    //                 resolve("success");

    //             },
    //             error: function(err) {
    //                 resolve("error");
    //             }
    //         });

    //     }.bind(this));
    // },

    onload_function: function() {
        // $('div[data-name="blanket_protocol_c"]').css('pointer-events', 'none');
        // $('span[data-fieldname="blanket_protocol_c"]').css('pointer-events', 'visible');

        if (this.model.get('work_product_status_c') == "Archived") {
            var wpid = this.model.get('id');
            app.api.call("create", "rest/v10/getUserRole", {
                module: 'M03_Work_Product',
                wpid: wpid,
            }, {
                success: function(Data) {
                    if (Data == "manager") {
                        $('div[data-name="master_schedule_company_name_c"]').css('pointer-events', 'unset');
                        $('div[data-name="master_schedule_sd_c"]').css('pointer-events', 'unset');
                    } else {
                        $('div[data-name="master_schedule_company_name_c"]').css('pointer-events', 'none');
                        $('div[data-name="master_schedule_sd_c"]').css('pointer-events', 'none');
                    }

                },
                error: function(error) {
                }
            });

        } else {
            $('div[data-name="master_schedule_company_name_c"]').css('pointer-events', 'unset');
            $('div[data-name="master_schedule_sd_c"]').css('pointer-events', 'unset');
        }

        //Prakash Jangir 28-Jan-2021 Bug Fix 826
        disableBlanketField = 0;
        var self = this;

        if (self.model.get('work_product_code_2_c') != "") {
            var id = self.model.get("m03_work_product_code_id1_c");
            var testSysName = self.model.get("test_system_c");
            if (!testSysName) {
                url = app.api.buildURL("M03_Work_Product_Code/" + id + "/get_blanket_protocol");
            } else {
                url = app.api.buildURL("M03_Work_Product_Code/" + id + "/" + testSysName + "/get_blanket_protocol");
            }
            app.api.call("GET", url, {

            }, {
                success: function(data) {
                    if (data != 0) {
                        disableBlanketField = 1;
                        //pulledBlanketProtocolId = data.WPBId;
                        $('div[data-name="blanket_protocol_c"]').css('pointer-events', 'none');
                        $('span[data-fieldname="blanket_protocol_c"] a').css('pointer-events', 'visible');

                    } else {
                        disableBlanketField = 0;
                        $('div[data-name="blanket_protocol_c"]').css('pointer-events', 'unset');
                    }

                },
                error: function(err) {}
            });
        }
    },


})