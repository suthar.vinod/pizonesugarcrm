<?php
$module_name = 'M03_Work_Product';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'readonly' => true,
                'required' => false,
                'default' => true,
                'enabled' => true,
                'link' => true,
                'width' => 'small',
              ),
              1 => 
              array (
                'name' => 'work_product_status_c',
                'label' => 'LBL_WORK_PRODUCT_STATUS',
                'enabled' => true,
                'default' => true,
                'width' => 'small',
              ),
              2 => 
              array (
                'name' => 'study_director_script_c',
                'label' => 'LBL_STUDY_DIRECTOR_SCRIPT',
                'enabled' => true,
                'id' => 'CONTACT_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'study_coordinator_relate_c',
                'label' => 'LBL_STUDY_COORDINATOR_RELATE',
                'enabled' => true,
                'id' => 'CONTACT_ID2_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'work_product_compliance_c',
                'label' => 'LBL_WORK_PRODUCT_COMPLIANCE',
                'enabled' => true,
                'default' => true,
                'width' => '70',
              ),
              5 => 
              array (
                'name' => 'm01_sales_m03_work_product_1_name',
                'label' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE',
                'enabled' => true,
                'id' => 'M01_SALES_M03_WORK_PRODUCT_1M01_SALES_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'contacts_m03_work_product_2_name',
                'label' => 'LBL_CONTACTS_M03_WORK_PRODUCT_2_NAME_FIELD_TITLE',
                'enabled' => true,
                'id' => 'CONTACTS_M03_WORK_PRODUCT_2CONTACTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'first_procedure_c',
                'label' => 'LBL_FIRST_PROCEDURE',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'last_procedure_c',
                'label' => 'LBL_LAST_PROCEDURE',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'study_director_initiation_c',
                'label' => 'LBL_STUDY_DIRECTOR_INITIATION',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'work_product_code_2_c',
                'label' => 'LBL_WORK_PRODUCT_CODE_2',
                'enabled' => true,
                'readonly' => false,
                'id' => 'M03_WORK_PRODUCT_CODE_ID1_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'title_description_c',
                'label' => 'LBL_TITLE_DESCRIPTION',
                'enabled' => true,
                'default' => true,
                'width' => 'large',
              ),
              12 => 
              array (
                'name' => 'accounts_m03_work_product_1_name',
                'label' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE',
                'enabled' => true,
                'id' => 'ACCOUNTS_M03_WORK_PRODUCT_1ACCOUNTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              13 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              14 => 
              array (
                'name' => 'bid_batch_id_m03_work_product_1_name',
                'label' => 'LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_BID_BATCH_ID_TITLE',
                'enabled' => true,
                'id' => 'BID_BATCH_ID_M03_WORK_PRODUCT_1BID_BATCH_ID_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              15 => 
              array (
                'name' => 'master_schedule_company_name_c',
                'label' => 'LBL_MASTER_SCHEDULE_COMPANY_NAME',
                'enabled' => true,
                'default' => true,
              ),
              16 => 
              array (
                'name' => 'iacuc_protocol_status_c',
                'label' => 'LBL_IACUC_PROTOCOL_STATUS',
                'enabled' => true,
                'default' => true,
              ),
              17 => 
              array (
                'name' => 'iacuc_approval_date_c',
                'label' => 'LBL_IACUC_APPROVAL_DATE',
                'enabled' => true,
                'default' => true,
              ),
              18 => 
              array (
                'name' => 'iacuc_closure_date_c',
                'label' => 'LBL_IACUC_CLOSURE_DATE',
                'enabled' => true,
                'default' => true,
              ),
              19 => 
              array (
                'name' => 'test_price_c',
                'label' => 'LBL_TEST_PRICE',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'readonly' => false,
                'currency_format' => true,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
