({
    extendsFrom: 'CreateView',


    initialize: function(options) {
        var pulledBlanketProtocolId;

        this._super('initialize', [options]);
        this.model.on('change:work_product_code_2_c', this.function_Code, this);
        this.model.on('change:test_system_c', this.getBlanketProtocolData, this);
        this.model.on('change:bid_batch_id_m03_work_product_1_name', this.getScheduleDateFromBatchId, this);
        //this.model.addValidationTask('check_namsa_test_code_c', _.bind(this._doValidateDueDate, this));
    },

    getScheduleDateFromBatchId: function(){
        var self = this;
        var batchId = this.model.get("bid_batch_id_m03_work_product_1bid_batch_id_ida");
        // console.log("batchId===>",batchId);
        app.api.call("read", "rest/v10/BID_Batch_ID/" + batchId+"?fields=scheduled_date_c", null, {
            success: function (res) {
                // console.log("Response===>",res.scheduled_date_c);
                if(res.scheduled_date_c != undefined || res.scheduled_date_c != ""){
                    self.model.set("first_procedure_c",res.scheduled_date_c);
                }
            }.bind(self),
        });
    },

    _doValidateDueDate: function(fields, errors, callback) {
        var self = this;
        var sales_id = self.model.get('m01_sales_m03_work_product_1m01_sales_ida');
        // console.log('sales_id', sales_id);
        if (sales_id != '' && sales_id != undefined) {
            var Sales_API = "rest/v10/M01_Sales/" + sales_id + "?fields=namsa_bde_referred_c";
            App.api.call("get", Sales_API, null, {
                success: function(Sales_Data) {
                    var sales_bade = Sales_Data.namsa_bde_referred_c;
                    // console.log('namsa_bde_referred_c data : name : ', sales_bade);
                    if (sales_bade == 'No' && _.isEmpty(self.model.get('namsa_test_code_c'))) {
                        // console.log('sales_bade', sales_bade);
                        errors['namsa_test_code_c'] = errors['namsa_test_code_c'] || {};
                        errors['namsa_test_code_c'].required = true;
                    }
                    callback(null, fields, errors);
                }
            });
        } else {
            // console.log('sales_id');
            //errors['functional_area_c'] = errors['functional_area_c'] || {};
            //errors['functional_area_c'].required = true;
            callback(null, fields, errors);
        }
    },

    getBlanketProtocolData: function() {
        var self = this;
        var workCodeName = self.model.get("work_product_code_2_c");

        //If WPC is empty then return
        if (!workCodeName)
            return;
        else {

            var testSysName = self.model.get("test_system_c");
            var id = self.model.get("m03_work_product_code_id1_c");

            //Api Url
            if (!testSysName) {
                url = app.api.buildURL("M03_Work_Product_Code/" + id + "/get_blanket_protocol");
            } else {
                url = app.api.buildURL("M03_Work_Product_Code/" + id + "/" + testSysName + "/get_blanket_protocol");
            }

            app.api.call("GET", url, {

            }, {
                success: function(data) {
                    // console.log(data);
                    if (data != 0) {
                        pulledBlanketProtocolId = data.WPBId;
                        self.model.set("m03_work_product_id_c", data.WPBId);
                        self.model.set("blanket_protocol_c", data.WPBName);
                        $('input[name="blanket_protocol_c"]').prop('disabled', true);

                    } else {
                        pulledBlanketProtocolId = null;
                        self.model.set("m03_work_product_id_c", "");
                        self.model.set("blanket_protocol_c", "");
                        $('input[name="blanket_protocol_c"]').prop('disabled', false);
                    }

                },
                error: function(err) {}
            });
        }
    },

    function_Code: function() {
        var self = this;
        if (self.model.get('work_product_code_2_c') != "") {
            var id = self.model.get("m03_work_product_code_id1_c");
            app.api.call("GET", app.api.buildURL("M03_Work_Product_Code/" + id + "?fields=total_animal_number_c"), {

            }, {
                success: function(data) {
                    if (!data.total_animal_number_c)
                        self.model.set('initial_animals_approved_pri_c', 0);
                    else
                        self.model.set('initial_animals_approved_pri_c', data.total_animal_number_c);
                },
                error: function(err) {
                    self.model.set('initial_animals_approved_pri_c', 0);
                }
            });

            var testSysName = self.model.get("test_system_c");
            if (!testSysName) {
                url = app.api.buildURL("M03_Work_Product_Code/" + id + "/get_blanket_protocol");
            } else {
                url = app.api.buildURL("M03_Work_Product_Code/" + id + "/" + testSysName + "/get_blanket_protocol");
            }
            app.api.call("GET", url, {

            }, {
                success: function(data) {
                    // console.log(data);
                    if (data != 0) {
                        pulledBlanketProtocolId = data.WPBId;
                        self.model.set("m03_work_product_id_c", data.WPBId);
                        self.model.set("blanket_protocol_c", data.WPBName);
                        $('input[name="blanket_protocol_c"]').prop('disabled', true);

                    } else {
                        pulledBlanketProtocolId = null;
                        self.model.set("m03_work_product_id_c", "");
                        self.model.set("blanket_protocol_c", "");
                        $('input[name="blanket_protocol_c"]').prop('disabled', false);
                    }

                },
                error: function(err) {}
            });



        } else {
            pulledBlanketProtocolId = null;
            self.model.set("m03_work_product_id_c", "");
            self.model.set("blanket_protocol_c", "");
            $('input[name="blanket_protocol_c"]').prop('disabled', false);
        }
    },

    // saveAndClose: function() {
    //     var self = this;
    //     var blanketId = this.model.get("m03_work_product_id_c");
    //     var blanketName = this.model.get("blanket_protocol_c");

    //     this.initiateSave(_.bind(function() {
    //         if (!pulledBlanketProtocolId) {
    //             if (blanketName) {
    //                 this.linkWorkProducts().then(function(result) {

    //                     // if (result === "success") {
    //                     //     app.alert.dismiss("link_wps");
    //                     // }
    //                     app.drawer.close(self.context, self.model);
    //                 }.bind(this));
    //             }
    //         }
    //         app.drawer.close(self.context, self.model);

    //     }, this));
    // },

    // linkWorkProducts: function() {
    //     return new Promise(function(resolve, reject) {
    //         var currentWPID = this.model.get("id");
    //         var WPCID = this.model.get("m03_work_product_code_id1_c");

    //         // app.alert.show('link_wps', {
    //         //     level: 'info',
    //         //     messages: 'Linking Work Products...'
    //         // });

    //         url = app.api.buildURL("M03_Work_Product_Code/" + WPCID + "/" + currentWPID + "/link" + "/create");

    //         app.api.call("GET", url, {

    //         }, {
    //             success: function(data) {
    //                 resolve("success");

    //             },
    //             error: function(err) {
    //                 resolve("error");
    //             }
    //         });

    //     }.bind(this));
    // },
})