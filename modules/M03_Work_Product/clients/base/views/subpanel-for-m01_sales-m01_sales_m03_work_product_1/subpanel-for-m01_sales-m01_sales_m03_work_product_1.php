<?php
// created: 2022-04-05 11:55:21
$viewdefs['M03_Work_Product']['base']['view']['subpanel-for-m01_sales-m01_sales_m03_work_product_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'title_description_c',
          'label' => 'LBL_TITLE_DESCRIPTION',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
          'enabled' => true,
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'accounts_m03_work_product_1_name',
          'label' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE',
          'enabled' => true,
          'id' => 'ACCOUNT_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'contacts_m03_work_product_1_name',
          'label' => 'LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE',
          'enabled' => true,
          'id' => 'CONTACT_ID1_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'timeline_type_c',
          'label' => 'LBL_TIMELINE_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'first_procedure_c',
          'label' => 'LBL_FIRST_PROCEDURE',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'work_product_status_c',
          'label' => 'LBL_WORK_PRODUCT_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'test_price_c',
          'label' => 'LBL_TEST_PRICE',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'readonly' => false,
          'currency_format' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'rowactions' => 
  array (
    'actions' => 
    array (
      0 => 
      array (
        'type' => 'rowaction',
        'css_class' => 'btn',
        'tooltip' => 'LBL_PREVIEW',
        'event' => 'list:preview:fire',
        'icon' => 'fa-eye',
        'acl_action' => 'view',
        'allow_bwc' => false,
      ),
      1 => 
      array (
        'type' => 'rowaction',
        'name' => 'edit_button',
        'icon' => 'fa-pencil',
        'label' => 'LBL_EDIT_BUTTON',
        'event' => 'list:editrow:fire',
        'acl_action' => 'edit',
        'allow_bwc' => true,
        'css_class' => 'disabled',
      ),
      2 => 
      array (
        'type' => 'unlink-action',
        'icon' => 'fa-chain-broken',
        'label' => 'LBL_UNLINK_BUTTON',
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);