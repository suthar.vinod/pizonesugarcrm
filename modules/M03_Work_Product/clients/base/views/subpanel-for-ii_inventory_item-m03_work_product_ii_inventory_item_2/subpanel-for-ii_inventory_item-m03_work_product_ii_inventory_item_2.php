<?php
// created: 2021-02-25 17:01:40
$viewdefs['M03_Work_Product']['base']['view']['subpanel-for-ii_inventory_item-m03_work_product_ii_inventory_item_2'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'm01_sales_m03_work_product_1_name',
          'label' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE',
          'enabled' => true,
          'id' => 'M01_SALES_M03_WORK_PRODUCT_1M01_SALES_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'study_director_script_c',
          'label' => 'LBL_STUDY_DIRECTOR_SCRIPT',
          'enabled' => true,
          'id' => 'CONTACT_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'study_coordinator_relate_c',
          'label' => 'LBL_STUDY_COORDINATOR_RELATE',
          'enabled' => true,
          'id' => 'CONTACT_ID2_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'first_procedure_c',
          'label' => 'LBL_FIRST_PROCEDURE',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'rowactions' => 
  array (
    'actions' => 
    array (
      0 => 
      array (
        'type' => 'rowaction',
        'css_class' => 'btn',
        'tooltip' => 'LBL_PREVIEW',
        'event' => 'list:preview:fire',
        'icon' => 'fa-eye',
        'acl_action' => 'view',
        'allow_bwc' => false,
      ),
      1 => 
      array (
        'type' => 'rowaction',
        'name' => 'edit_button',
        'icon' => 'fa-pencil',
        'label' => 'LBL_EDIT_BUTTON',
        'event' => 'list:editrow:fire',
        'acl_action' => 'edit',
        'allow_bwc' => true,
		    'css_class' => 'disabled'
      ),
      2 => 
      array (
        'type' => 'unlink-action',
        'icon' => 'fa-chain-broken',
        'label' => 'LBL_UNLINK_BUTTON',
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);