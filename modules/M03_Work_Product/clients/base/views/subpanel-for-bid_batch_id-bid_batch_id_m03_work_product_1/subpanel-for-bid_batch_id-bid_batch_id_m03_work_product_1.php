<?php
// created: 2022-04-26 10:58:44
$viewdefs['M03_Work_Product']['base']['view']['subpanel-for-bid_batch_id-bid_batch_id_m03_work_product_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'work_product_status_c',
          'label' => 'LBL_WORK_PRODUCT_STATUS',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'work_product_compliance_c',
          'label' => 'LBL_WORK_PRODUCT_COMPLIANCE',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'study_director_script_c',
          'label' => 'LBL_STUDY_DIRECTOR_SCRIPT',
          'enabled' => true,
          'readonly' => false,
          'id' => 'CONTACT_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'accounts_m03_work_product_1_name',
          'label' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_NAME_FIELD_TITLE',
          'enabled' => true,
          'id' => 'ACCOUNTS_M03_WORK_PRODUCT_1ACCOUNTS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'contacts_m03_work_product_1_name',
          'label' => 'LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE',
          'enabled' => true,
          'id' => 'CONTACTS_M03_WORK_PRODUCT_1CONTACTS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'm01_sales_m03_work_product_1_name',
          'label' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE',
          'enabled' => true,
          'id' => 'M01_SALES_M03_WORK_PRODUCT_1M01_SALES_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);