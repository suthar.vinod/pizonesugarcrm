({ 
  extendsFrom: 'SubpanelListView',
  initialize: function (options) {
    this._super('initialize', [options]);
    this.on('render', this._disableActions, this);
  },
  /*Disable edit button from the subpanel of M06_Error Module */
  _disableActions: function () {
    var parentModule = this.context.parent.get('module');
      if (_.isEqual(parentModule, "M06_Error")) {
          _.each(this.meta.rowactions.actions, function (action) {
              if (_.isEqual(action['icon'], "fa-pencil")) { // for edit button 
                delete action['event'];
                action['css_class'] = 'disabled';
              }
          });
      }
  }
})  