<?php
if (!defined('sugarEntry') || !sugarEntry) {
  die('Not A Valid Entry Point');
}
class CopyTDToWPFromTDApi extends SugarApi
{
  public function registerApiRest()
  {
    return array(
      'MyGetEndpointNum' => array(
        'reqType' => 'GET',
        'noLoginRequired' => false,
        'path' => array('M03_Work_Product', '?', '?', 'copy_task_design_to_wp'),
        'pathVars' => array('module', 'CopyFromwpid', 'CopyTowpid'),
        'method' => 'copy_task_design_to_wp',
        'shortHelp' => '',
        'longHelp' => '',
      ),
    );
  }
  /**#2384 : 28 April 2022 : Below code will copy the task design records from current Work product and create new TDs basend on copy tsd's into selected wp */
  public function copy_task_design_to_wp($api, $args)
  {
    $GLOBALS['log']->fatal('in file CopyTDToWPFromTDApi in fun copy_task_design_to_wp for Auto-create TD from copy wp to wp popup window');
    global $db, $current_user;
    $module = $args['module'];
    /**Copy from current wp **/
    $CopyFromwpid = $args['CopyFromwpid'];
    /**Copy to selected wp **/
    $CopyTowpid = $args['CopyTowpid'];
    $GLOBALS['log']->fatal('in file CopyTDToWPFromTDApi in fun copy_task_design_to_wp line 31 CopyTowpid : '.$CopyTowpid);
    $GLOBALS['log']->fatal('in file CopyTDToWPFromTDApi in fun copy_task_design_to_wp line 32 CopyTowpid : '.$CopyTowpid);
    $currentWPBean = BeanFactory::retrieveBean('M03_Work_Product', $CopyFromwpid);
    $copytoWPBean = BeanFactory::retrieveBean('M03_Work_Product', $CopyTowpid);
    $coytoWPname = $copytoWPBean->name;
    $GLOBALS['log']->fatal('in file CopyTDToWPFromTDApi in fun copy_task_design_to_wp line 35 coytoWPname : '.$coytoWPname);
    $currentWPBean->load_relationship('m03_work_product_taskd_task_design_1');
    $relatedTD = $currentWPBean->m03_work_product_taskd_task_design_1->get();
    /**Get all the tsd id's using loop and will assign the values from existing into new created records */
    foreach ($relatedTD as $TDId) {
      $clonedBean = BeanFactory::getBean('TaskD_Task_Design', $TDId);
      $type = $clonedBean->type_2;
      if ($type == "Actual SP") {
        $other_equipment = $clonedBean->other_equipment_c;
        $clonedBean->load_relationship("taskd_task_design_taskd_task_design_1");
        $clonedBean->load_relationship("m03_work_product_taskd_task_design_1");
        $clonedBean->load_relationship("bid_batch_id_taskd_task_design_1");
        $date_entered = date("Y-m-d H:i:s", time());
        /* Custom Query to get Task design Ids */
        $sqlGetTaskDesignIds = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_ida as TDId
                                    FROM taskd_task_design_taskd_task_design_1_c 
                                    where taskd_task_design_taskd_task_design_1taskd_task_design_idb='$TDId' and deleted=0";
        $resultsSqlGetTaskDesignIds = $db->query($sqlGetTaskDesignIds);
        $rowSqlGetTaskDesignIds = $db->fetchByAssoc($resultsSqlGetTaskDesignIds);
        $taskDesignIds = $rowSqlGetTaskDesignIds['TDId'];
        $clonedBean->id  = create_guid();
        $clonedBean->new_with_id = true;
        $clonedBean->parent_td_id_c = $TDId;
        $clonedBean->relative = $clonedBean->relative;
        $clonedBean->category = $clonedBean->category;
        $clonedBean->order_2_c = $clonedBean->order_2_c;
        $clonedBean->equipment_required = $clonedBean->equipment_required;
        $clonedBean->task_type = $clonedBean->task_type;
        $clonedBean->type_of_personnel_2_c = $clonedBean->type_of_personnel_2_c;
        $clonedBean->duration_integer_min_c = $clonedBean->duration_integer_min_c;
        $clonedBean->description = $clonedBean->description;
        $clonedBean->other_equipment_c = $other_equipment;
        $clonedBean->date_entered = $date_entered;
        $copyingTaskDesignIds[] = $clonedBean->id;
        $p1arr[$TDId] = $taskDesignIds;
        $a1arr[$clonedBean->id] = $TDId;
        $b1arr[$TDId] = $clonedBean->id;
        $clonedBean->u_units_id_c = $clonedBean->u_units_id_c;
        $clonedBean->start_integer = $clonedBean->start_integer;
        $clonedBean->end_integer = $clonedBean->end_integer;
        $clonedBean->phase_c = $clonedBean->phase_c;
        $clonedBean->custom_task = $clonedBean->custom_task;
        $clonedBean->standard_task = $clonedBean->standard_task;
        $clonedBean->type_2 = "Actual SP";
        $phase = $clonedBean->phase_c;
        $batch_id       = $clonedBean->bid_batch_id_taskd_task_design_1bid_batch_id_ida;
        $BatchBean      =   BeanFactory::getBean('BID_Batch_ID', $batch_id);
        $BatchName      =   $BatchBean->name;
        if ($phase != '') {
          $phaseVal = ' ' . $phase;
        } else {
          $phaseVal = '';
        }
        $relative = $clonedBean->relative;

        /**Calcualtion based on wp first procedure date */
        $offset1 =  21600; //daylight Saving
        $GLOBALS['log']->fatal('  copytoWPBea first_procedure_c line 89 ' . $copytoWPBean->first_procedure_c);
        if ($copytoWPBean->first_procedure_c != '') {
          $datefirst_procedure_c = strtotime($copytoWPBean->first_procedure_c);
          $actual_datetime       = date("Y-m-d 17:00:00", $datefirst_procedure_c);
          $GLOBALS['log']->fatal('  datefirst_procedure_c line 93 ' . $datefirst_procedure_c);
          $GLOBALS['log']->fatal(' actual_datetime line 94 ' . $actual_datetime);
        }
        if ($copytoWPBean->first_procedure_c != '' && $clonedBean->relative == "NA") {
          $datefirst_procedure_c = strtotime($copytoWPBean->first_procedure_c);
          $clonedBean->actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
          $clonedBean->save();
          //$GLOBALS['log']->fatal(' Extract In NA clonedBean->actual_datetime ' . $clonedBean->actual_datetime);
        }
        $unit_Bean = BeanFactory::getBean('U_Units', $clonedBean->u_units_id_c);
        $unit_Bean_name = $unit_Bean->name;
        if ($actual_datetime != "") {
          if ($unit_Bean_name == "Day") {
            if ($clonedBean->relative == "1st Tier") {
              $result = date($actual_datetime);
              $result = strtotime($result . '+' . $clonedBean->start_integer . ' days');
              $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);

              $result = date($actual_datetime);
              $result = strtotime($result . '+' . $clonedBean->end_integer . ' days');
              $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
            }
          } else if ($unit_Bean_name == "Hour") {
            if ($clonedBean->relative == "1st Tier") {
              $result = date($actual_datetime);
              $result = strtotime($result . '+' . $clonedBean->start_integer . ' hours');
              $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
              $GLOBALS['log']->fatal(' Extract IN clonedBean->actual_datetime  line 120' . $clonedBean->actual_datetime);
              $result = date($actual_datetime);
              $result = strtotime($result . '+' . $clonedBean->end_integer . ' hours');
              $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
            }
          } else if ($unit_Bean_name == "Minute") {

            if ($clonedBean->relative == "1st Tier") {
              $result = date($actual_datetime);
              $result = strtotime($result . '+' . $clonedBean->start_integer . ' minutes');
              $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);

              $result = date($actual_datetime);
              $result = strtotime($result . '+' . $clonedBean->end_integer . ' minutes');
              $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
            }
          }

          if (($clonedBean->relative == "1st Tier" || $clonedBean->relative == "NA")) {
            $clonedBean->planned_start_datetime_2nd = null;
            $clonedBean->planned_end_datetime_2nd = null;
          }
          if (($clonedBean->relative == "2nd Tier" || $clonedBean->relative == "NA")) {
            $clonedBean->planned_start_datetime_1st = null;
            $clonedBean->planned_end_datetime_1st = null;
          }
        }
        else
        {
          $GLOBALS['log']->fatal('In else part line 149');
          $clonedBean->planned_start_datetime_1st = null;
          $clonedBean->planned_end_datetime_1st = null;
          $clonedBean->planned_start_datetime_2nd = null;
          $clonedBean->planned_end_datetime_2nd = null;
          $clonedBean->actual_datetime = null;
          $clonedBean->single_date_c = null;
        }
        /************************ End of calculation *************************/

        $clonedBean->save();
        if ($CopyTowpid != '') {
          $clonedBean->m03_work_product_taskd_task_design_1->add($CopyTowpid);
        }
        /** Copy TDs with type : Actual WP only */
        if ($type == "Actual SP") {
          $task_type = $clonedBean->task_type;
          $custom_task = $clonedBean->custom_task;
          $standard_task = $clonedBean->standard_task;

          if ($task_type == 'Custom') {
            $standard_task = '';
            $standard_task_val = $custom_task;
          } else if ($task_type == 'Standard') {
            $custom_task = '';
            $standard_task_val = $standard_task;
          }
          /** Update the name based on batch design or wp */
          if ($BatchName != "") {
            $clonedBean->name = $coytoWPname . ' ' . $phase . ' ' . $BatchName . ' ' . $standard_task_val;
          } else {
            $clonedBean->name = $coytoWPname . ' ' . $phase . ' ' . $standard_task_val;
          }
          $clonedBean->save();
        }
        /** Delete the audit log if name field having same value in before and after string */
        $del_query = "Select * from `taskd_task_design_audit` WHERE `field_name`='name' AND `parent_id`='" . $clonedBean->id . "' ";
        $sql_DeleteAudit = $db->query($del_query);
        if ($sql_DeleteAudit->num_rows > 0) {
          while ($fetchAuditLog = $db->fetchByAssoc($sql_DeleteAudit)) {
            $before_val = $fetchAuditLog['before_value_string'];
            $after_val = $fetchAuditLog['after_value_string'];
            /** To ignore space we are using str replace so exact matching record will be delete */
            $before_val_st = str_replace(' ', '', $before_val);
            $after_val_st = str_replace(' ', '', $after_val);

            if ($before_val_st == $after_val_st) {
              $recordID = $fetchAuditLog['id'];
              $sql_Delete = "DELETE from `taskd_task_design_audit` WHERE `field_name`='name' AND `parent_id`='" . $clonedBean->id . "' AND id = '" . $recordID . "'";
              $db->query($sql_Delete);
            }
          }
        }

        /** Save audit log for all updated fields*/
        $source = '{"subject":{"_type":"logic-hook","class":"CopyTaskDesignToGD","method":"CopyTaskDesignToGD"},"attributes":[]}';
        if ($clonedBean->relative != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"relative","datetime","","' . $clonedBean->relative . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->category != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"category","datetime","","' . $clonedBean->category . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->type_2 != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"type_2","datetime","","' . $clonedBean->type_2 . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->task_type != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"task_type","datetime","","' . $clonedBean->task_type . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->standard_task != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"standard_task","datetime","","' . $clonedBean->standard_task . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->start_integer != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"start_integer","datetime","","' . $clonedBean->start_integer . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->end_integer != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"end_integer","datetime","","' . $clonedBean->end_integer . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);
          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->time_window != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"time_window","datetime","","' . $clonedBean->time_window . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->u_units_id_c != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"u_units_id_c","datetime","","' . $clonedBean->u_units_id_c . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->planned_start_datetime_1st != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_start_datetime_1st","datetime","","' . $clonedBean->planned_start_datetime_1st . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->planned_end_datetime_1st != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_end_datetime_1st","datetime","","' . $clonedBean->planned_end_datetime_1st . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->planned_start_datetime_2nd != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_start_datetime_2nd","datetime","","' . $clonedBean->planned_start_datetime_2nd . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->planned_end_datetime_2nd != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_end_datetime_2nd","datetime","","' . $clonedBean->planned_end_datetime_2nd . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->custom_task != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"custom_task","datetime","","' . $clonedBean->custom_task . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->order_2_c != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"order_2_c","datetime","","' . $clonedBean->order_2_c . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->equipment_required != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"equipment_required","datetime","","' . $clonedBean->equipment_required . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->other_equipment_c != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"other_equipment_c","datetime","","' . $clonedBean->other_equipment_c . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->phase_c != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"phase_c","datetime","","' . $clonedBean->phase_c . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->description != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"description","datetime","","' . $clonedBean->description . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->type_of_personnel_2_c != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"type_of_personnel_2_c","datetime","","' . $clonedBean->type_of_personnel_2_c . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }

        if ($clonedBean->duration_integer_min_c != '') {
          $auditEventid = create_guid();
          $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
          values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"duration_integer_min_c","datetime","","' . $clonedBean->duration_integer_min_c . '")';
          $auditsqlResult_1 = $db->query($auditsql_1);

          $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
          $db->query($auditsqlStatus_1);
        }
      }
    }
    /**Relationship of Tds with Tds */
    if (count($p1arr) > 0) {
      foreach ($p1arr as $key => $value) {
        $newBeanId = $b1arr[$key];
        $newBean = BeanFactory::getBean('TaskD_Task_Design', $newBeanId);
        $newBean->load_relationship("taskd_task_design_taskd_task_design_1_right");
        $toBeRelateID = array_search($value, $a1arr);

        if ($toBeRelateID != "") {
          $uniqId = create_guid();
          $sqlInsertRelationship = "INSERT INTO `taskd_task_design_taskd_task_design_1_c` 
              (`id`, `date_modified`, `deleted`, `taskd_task_design_taskd_task_design_1taskd_task_design_ida`, `taskd_task_design_taskd_task_design_1taskd_task_design_idb`) 
              VALUES ('" . $uniqId . "', now(), '0', '" . $toBeRelateID . "', '" . $newBeanId . "')";
          $resultsSqlGetTaskDesignIds = $db->query($sqlInsertRelationship);
        }
      }
    }
    /*********** Calculation in 2nd tier TDs  *******************************************/
    $copytoWPBean = BeanFactory::retrieveBean('M03_Work_Product', $CopyTowpid);
    $copytoWPBean->load_relationship('m03_work_product_taskd_task_design_1');
    $relatedTD1 = $copytoWPBean->m03_work_product_taskd_task_design_1->get();
    foreach ($relatedTD1 as $tdID) {
      $tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $tdID);
      $u_units = $tdBean->u_units_id_c;
      $unit_Bean = BeanFactory::getBean('U_Units', $u_units);
      $unit_Bean_name = $unit_Bean->name;
      if ($unit_Bean_name == "Day") {
        $integer_u = 'days';
      } elseif ($unit_Bean_name == "Hour") {
        $integer_u = 'hours';
      } else {
        $integer_u = 'minutes';
      }
      if ($tdBean->relative == "1st Tier") {
        $tdBean1 = BeanFactory::retrieveBean('TaskD_Task_Design', $tdBean->id);
        $tdBean1->load_relationship('taskd_task_design_taskd_task_design_1');
        $taskDesignId1 = $tdBean1->taskd_task_design_taskd_task_design_1->get();
        $Get_2nd_tier = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_idb FROM  `taskd_task_design_taskd_task_design_1_c` WHERE `taskd_task_design_taskd_task_design_1taskd_task_design_ida` = '" . $tdBean->id . "' AND deleted = 0;";
        $Get_Result = $db->query($Get_2nd_tier);

        if ($Get_Result->num_rows > 0) {
          while ($fetchResult = $db->fetchByAssoc($Get_Result)) {
            $recordID = $fetchResult['taskd_task_design_taskd_task_design_1taskd_task_design_idb'];
            $tdBean2 = BeanFactory::retrieveBean('TaskD_Task_Design', $recordID);
            if ($tdBean->planned_start_datetime_1st != "") {
              $result = date($tdBean->planned_start_datetime_1st);
              $result = strtotime($result . '+' . $tdBean2->start_integer . ' ' . $integer_u);
              $tdBean2->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);

              if (date("Y", $result) != "1970") {
                $tdBean2->single_date_c = date("Y-m-d", $result);
              }
            } else {
              $tdBean2->planned_start_datetime_2nd = null;
            }
            if ($tdBean->planned_end_datetime_1st != "") {
              $result = date($tdBean->planned_end_datetime_1st);
              $result = strtotime($result . '+' . $tdBean2->end_integer . ' ' . $integer_u);
              $tdBean2->planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
            } else {
              $tdBean2->planned_end_datetime_2nd = null;
            }
            $tdBean2->save();
          }
        }
      }
    }
    /** ** ** ** ** ** ** ** ** End of calculation in 2nd tier TDs ***********************************/

    if (count($copyingTaskDesignIds) > 0) {
      return "Task Designs have been copied successfully to the selected Work Product.";
    } else {
      return "Error! There is no task designs with Type = Actual - WP linked to selected work product. Please link it to a work product prior to copying.";
    }
  }
}
