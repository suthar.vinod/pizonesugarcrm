<?php 
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class getUserRoleApi extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'MyGetEndpoint' => array(
                'reqType'         => 'POST',
                'noLoginRequired' => false,
                'path'            => array('getUserRole'),
                'pathVars'        => array(),
                'method'          => 'getUserRole',
                'shortHelp'       => '',
                'longHelp'        => '',
            ), 
        );
    }

    public function getUserRole($api, $args)
    {
        global $current_user,$db;
        $userRole = '';
        $sql = "select * from acl_roles_users where role_id='fc0f19ea-41c8-11e9-bc3b-067d538c0c10' AND user_id='$current_user->id' AND deleted=0";
        $result = $db->query($sql);
        if($result->num_rows > 0){
            $userRole = 'manager';
        }
        return  $userRole;      
    }
}