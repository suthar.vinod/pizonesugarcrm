<?php

class RemoveBlanketRelationship
{

    function removeRelationship($bean, $event, $arguments)
    {

       // $bean->blanketRemoveID = $bean->fetched_row['m03_work_product_id_c'];
            global $db;

        if ($bean->m03_work_product_id_c) {

            if ($this->get_data($bean->m03_work_product_code_id1_c, $bean->test_system_c) == 0) {
                //Adding relationship
                $WPBBean = BeanFactory::retrieveBean('M03_Work_Product_Code', $bean->m03_work_product_code_id1_c, array(
                    'disable_row_level_security' => true
                ));
                $WPBBean->load_relationship("m03_work_product_m03_work_product_code_1");
                $WPBBean->m03_work_product_m03_work_product_code_1->add($bean->m03_work_product_id_c);
                $WPBBean->save();
                
            }
            
        } else {
            $DeletesqlRel = "DELETE FROM m03_work_product_m03_work_product_code_1_c WHERE m03_work_product_m03_work_product_code_1m03_work_product_ida = '".$bean->fetched_row['m03_work_product_id_c']."' AND m03_work_p5357ct_code_idb = '".$bean->m03_work_product_code_id1_c."' AND deleted=0"; 	
			$Blanket_exec = $db->query($DeletesqlRel);

        }
    }


    public function get_data($WPC_ID, $testSyName)
    {
        global $db;


        //Output data array
        $data = array();

        //Used for getting first WP Blanket record
        $i = 0;

        //Getting all WPB ids
        $sql = 'SELECT id as WPID FROM m03_work_product where id in(SELECT m03_work_product_m03_work_product_code_1m03_work_product_ida  
        FROM m03_work_product_m03_work_product_code_1_c where deleted=0 and m03_work_p5357ct_code_idb="' . $WPC_ID . '")
       and deleted=0 order by date_modified desc';

        $result = $db->query($sql);

        //If no Blanket protocol in WPC
        if ($result->num_rows == 0) {
            return 0;
        }

        //If there is only one record and test sys name is null
        if ($result->num_rows == 1 && $testSyName == "") {

            $rowNoTsys = $db->fetchByAssoc($result);
            $WPBId = $rowNoTsys['WPID'];
            $WPBBean = BeanFactory::retrieveBean('M03_Work_Product', $WPBId, array(
                'disable_row_level_security' => true
            ));

            $WPBName = $WPBBean->name;
            $testSysName = $WPBBean->test_system_c;

            //Getting first latest Blanket Protocol record if there is no test sys filter
            $data = array(
                "WPBId" => $WPBId,
                "WPBName" => $WPBName
            );

            return $data;
        }

        //We have multiple records and test sys is null
        if ($result->num_rows > 1 && $testSyName == "") {
            return 0;
        } else {

            while ($row = $db->fetchByAssoc($result)) {
                $WPBId = $row['WPID'];
                $WPBBean = BeanFactory::retrieveBean('M03_Work_Product', $WPBId, array(
                    'disable_row_level_security' => true
                ));

                $WPBName = $WPBBean->name;
                $testSysName = $WPBBean->test_system_c;

                //Matching all the record with passed test sys name
                if ($testSysName == $testSyName) {
                    $i = 1;
                    $data = array(
                        "WPBId" => $WPBId,
                        "WPBName" => $WPBName
                    );

                    return $data;
                }
            }
            //When test sys is not matched return null
            return 0;
        }
        return 0;
    }


}
