<?php

if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

class checkBiocompReportHook
{
	static $already_ran = false;
	public function checkBiocompReport($bean, $event, $arguments)
	{
		if (self::$already_ran == true) return; //So that hook will only trigger once
		self::$already_ran = true;
		global $db, $current_user;
		$site_url = $GLOBALS['sugar_config']['site_url'];


		if (
			$bean->id == $bean->fetched_row['id'] &&
			$bean->functional_area_c == "Standard Biocompatibility" &&
			$bean->fetched_row['contacts_m03_work_product_2contacts_ida'] == "" &&
			$bean->contacts_m03_work_product_2contacts_ida != "" &&
			$bean->last_procedure_c != "" && $bean->contacts_m03_work_product_2_name == ""
		) {

			$lead_bean	 = BeanFactory::getBean('Contacts', $bean->contacts_m03_work_product_2contacts_ida);
			$leadaudiName	= $lead_bean->name;

			//$sdLink		= '<a target="_blank" href="' . $site_url . '/#Contacts/' . $bean->contacts_m03_work_product_2contacts_ida . '" >' . $leadaudiName . '</a>';

			$current_date	= strtotime(date("Y-m-d")); //current date	

			$dateObj		= new TimeDate();
			$formattedCD	= $dateObj->to_display_date_time($bean->last_procedure_c, true, true, $current_user);
			$formattedCD 	= str_replace('-', '/', $formattedCD);
			$procedureDate	= strtotime($formattedCD);

			if ($procedureDate <= $current_date) {
				$bean->sendemailreport = 1;
			}
		}
	}
}
