<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_m03_work_product_facility_1_M03_Work_Product.php

 // created: 2016-01-25 02:57:18
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_m03_work_product_facility_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product_Facility',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_FACILITY_1_FROM_M03_WORK_PRODUCT_FACILITY_TITLE',
  'get_subpanel_data' => 'm03_work_product_m03_work_product_facility_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_m03_work_product_deliverable_1_M03_Work_Product.php

 // created: 2016-02-01 20:37:08
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_m03_work_product_deliverable_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product_Deliverable',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'get_subpanel_data' => 'm03_work_product_m03_work_product_deliverable_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_tasks_1_M03_Work_Product.php

 // created: 2016-02-25 20:01:03
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'm03_work_product_tasks_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_abc12_work_product_activities_1_M03_Work_Product.php

 // created: 2017-02-17 17:50:46
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_abc12_work_product_activities_1'] = array (
  'order' => 100,
  'module' => 'ABC12_Work_Product_Activities',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_ABC12_WORK_PRODUCT_ACTIVITIES_TITLE',
  'get_subpanel_data' => 'm03_work_product_abc12_work_product_activities_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_meetings_1_M03_Work_Product.php

 // created: 2017-05-02 19:55:41
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_meetings_1'] = array (
  'order' => 100,
  'module' => 'Meetings',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE',
  'get_subpanel_data' => 'm03_work_product_meetings_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_a1a_critical_phase_inspectio_1_M03_Work_Product.php

 // created: 2017-06-07 19:01:24
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_a1a_critical_phase_inspectio_1'] = array (
  'order' => 100,
  'module' => 'A1A_Critical_Phase_Inspectio',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'get_subpanel_data' => 'm03_work_product_a1a_critical_phase_inspectio_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_wpe_work_product_enrollment_1_M03_Work_Product.php

 // created: 2017-09-12 15:10:30
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_wpe_work_product_enrollment_1'] = array (
  'order' => 100,
  'module' => 'WPE_Work_Product_Enrollment',
  'subpanel_name' => 'ForM03_work_productM03_work_product_wpe_work_product_enrollment_1',
  'title_key' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'get_subpanel_data' => 'm03_work_product_wpe_work_product_enrollment_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m06_error_m03_work_product_1_M03_Work_Product.php

 // created: 2018-01-31 15:57:04
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m06_error_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'm06_error_m03_work_product_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m99_protocol_amendments_m03_work_product_M03_Work_Product.php

 // created: 2018-02-05 14:21:02
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m99_protocol_amendments_m03_work_product'] = array (
  'order' => 100,
  'module' => 'M99_Protocol_Amendments',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M99_PROTOCOL_AMENDMENTS_M03_WORK_PRODUCT_FROM_M99_PROTOCOL_AMENDMENTS_TITLE',
  'get_subpanel_data' => 'm99_protocol_amendments_m03_work_product',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_sw_study_workflow_1_M03_Work_Product.php

 // created: 2018-10-09 16:44:23
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_sw_study_workflow_1'] = array (
  'order' => 100,
  'module' => 'SW_Study_Workflow',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_SW_STUDY_WORKFLOW_TITLE',
  'get_subpanel_data' => 'm03_work_product_sw_study_workflow_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/rr_regulatory_response_m03_work_product_1_M03_Work_Product.php

 // created: 2019-07-09 11:53:38
$layout_defs["M03_Work_Product"]["subpanel_setup"]['rr_regulatory_response_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'RR_Regulatory_Response',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'get_subpanel_data' => 'rr_regulatory_response_m03_work_product_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_activities_1_calls_M03_Work_Product.php

 // created: 2019-08-05 12:02:55
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_activities_1_calls'] = array (
  'order' => 100,
  'module' => 'Calls',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_CALLS_FROM_CALLS_TITLE',
  'get_subpanel_data' => 'm03_work_product_activities_1_calls',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_activities_1_meetings_M03_Work_Product.php

 // created: 2019-08-05 12:03:27
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_activities_1_meetings'] = array (
  'order' => 100,
  'module' => 'Meetings',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE',
  'get_subpanel_data' => 'm03_work_product_activities_1_meetings',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_activities_1_notes_M03_Work_Product.php

 // created: 2019-08-05 12:03:53
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_activities_1_notes'] = array (
  'order' => 100,
  'module' => 'Notes',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_NOTES_FROM_NOTES_TITLE',
  'get_subpanel_data' => 'm03_work_product_activities_1_notes',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_activities_1_tasks_M03_Work_Product.php

 // created: 2019-08-05 12:04:19
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_activities_1_tasks'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_TASKS_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'm03_work_product_activities_1_tasks',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_activities_1_emails_M03_Work_Product.php

 // created: 2019-08-05 12:04:52
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_activities_1_emails'] = array (
  'order' => 100,
  'module' => 'Emails',
  'subpanel_name' => 'Default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_EMAILS_FROM_EMAILS_TITLE',
  'get_subpanel_data' => 'm03_work_product_activities_1_emails',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_edoc_email_documents_1_M03_Work_Product.php

 // created: 2019-08-27 11:37:04
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_edoc_email_documents_1'] = array (
  'order' => 100,
  'module' => 'EDoc_Email_Documents',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'm03_work_product_edoc_email_documents_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_erd_error_documents_1_M03_Work_Product.php

 // created: 2019-11-05 12:53:29
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_erd_error_documents_1'] = array (
  'order' => 100,
  'module' => 'Erd_Error_Documents',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'm03_work_product_erd_error_documents_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_wpe_work_product_enrollment_2_M03_Work_Product.php

 // created: 2020-12-15 06:35:00
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_wpe_work_product_enrollment_2'] = array (
  'order' => 100,
  'module' => 'WPE_Work_Product_Enrollment',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'get_subpanel_data' => 'm03_work_product_wpe_work_product_enrollment_2',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_m03_work_product_code_1_M03_Work_Product.php

 // created: 2021-01-21 14:15:48
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_m03_work_product_code_1'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product_Code',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'get_subpanel_data' => 'm03_work_product_m03_work_product_code_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_tsd1_test_system_design_1_1_M03_Work_Product.php

 // created: 2021-02-11 08:58:28
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_tsd1_test_system_design_1_1'] = array (
  'order' => 100,
  'module' => 'TSD1_Test_System_Design_1',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_TSD1_TEST_SYSTEM_DESIGN_1_TITLE',
  'get_subpanel_data' => 'm03_work_product_tsd1_test_system_design_1_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/meetings_m03_work_product_1_M03_Work_Product.php

 // created: 2021-04-01 09:05:41
$layout_defs["M03_Work_Product"]["subpanel_setup"]['meetings_m03_work_product_1'] = array (
  'order' => 100,
  'module' => 'Meetings',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_MEETINGS_TITLE',
  'get_subpanel_data' => 'meetings_m03_work_product_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_gd_group_design_1_M03_Work_Product.php

 // created: 2021-08-14 05:59:50
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_gd_group_design_1'] = array (
  'order' => 100,
  'module' => 'GD_Group_Design',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'get_subpanel_data' => 'm03_work_product_gd_group_design_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_taskd_task_design_1_M03_Work_Product.php

 // created: 2021-08-14 06:01:53
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_taskd_task_design_1'] = array (
  'order' => 100,
  'module' => 'TaskD_Task_Design',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'get_subpanel_data' => 'm03_work_product_taskd_task_design_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_ori_order_request_item_1_M03_Work_Product.php

 // created: 2021-10-19 10:30:05
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_ori_order_request_item_1'] = array (
  'order' => 100,
  'module' => 'ORI_Order_Request_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'get_subpanel_data' => 'm03_work_product_ori_order_request_item_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_poi_purchase_order_item_1_M03_Work_Product.php

 // created: 2021-10-19 11:00:50
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_poi_purchase_order_item_1'] = array (
  'order' => 100,
  'module' => 'POI_Purchase_Order_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'get_subpanel_data' => 'm03_work_product_poi_purchase_order_item_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_ii_inventory_item_1_M03_Work_Product.php

 // created: 2021-11-09 10:04:52
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'm03_work_product_ii_inventory_item_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_ii_inventory_item_2_M03_Work_Product.php

 // created: 2021-11-09 10:07:50
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_ii_inventory_item_2'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'm03_work_product_ii_inventory_item_2',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_im_inventory_management_1_M03_Work_Product.php

 // created: 2021-11-09 10:14:52
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_im_inventory_management_1'] = array (
  'order' => 100,
  'module' => 'IM_Inventory_Management',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'get_subpanel_data' => 'm03_work_product_im_inventory_management_1',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/WirelessLayoutdefs/m03_work_product_ic_inventory_collection_1_M03_Work_Product.php

 // created: 2021-11-09 10:26:31
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m03_work_product_ic_inventory_collection_1'] = array (
  'order' => 100,
  'module' => 'IC_Inventory_Collection',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'get_subpanel_data' => 'm03_work_product_ic_inventory_collection_1',
);

?>
