<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_m03_work_product_facility_1_M03_Work_Product.php

// created: 2016-01-25 02:57:18
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_m03_work_product_facility_1"] = array (
  'name' => 'm03_work_product_m03_work_product_facility_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_m03_work_product_facility_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Facility',
  'bean_name' => 'M03_Work_Product_Facility',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_FACILITY_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_m03_work_product_facility_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_m03_work_product_code_id1_c.php

 // created: 2016-01-28 16:27:33

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m01_sales_m03_work_product_1_M03_Work_Product.php

// created: 2016-01-29 21:10:09
$dictionary["M03_Work_Product"]["fields"]["m01_sales_m03_work_product_1"] = array (
  'name' => 'm01_sales_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'm01_sales_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M01_Sales',
  'bean_name' => 'M01_Sales',
  'side' => 'right',
  'vname' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm01_sales_m03_work_product_1m01_sales_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product"]["fields"]["m01_sales_m03_work_product_1_name"] = array (
  'name' => 'm01_sales_m03_work_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE',
  'save' => true,
  'id_name' => 'm01_sales_m03_work_product_1m01_sales_ida',
  'link' => 'm01_sales_m03_work_product_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'name',
);
$dictionary["M03_Work_Product"]["fields"]["m01_sales_m03_work_product_1m01_sales_ida"] = array (
  'name' => 'm01_sales_m03_work_product_1m01_sales_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID',
  'id_name' => 'm01_sales_m03_work_product_1m01_sales_ida',
  'link' => 'm01_sales_m03_work_product_1',
  'table' => 'm01_sales',
  'module' => 'M01_Sales',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_m03_work_product_deliverable_1_M03_Work_Product.php

// created: 2016-02-01 20:37:08
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_m03_work_product_deliverable_1"] = array (
  'name' => 'm03_work_product_m03_work_product_deliverable_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_m03_work_product_deliverable_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Deliverable',
  'bean_name' => 'M03_Work_Product_Deliverable',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_p0b66product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_tasks_1_M03_Work_Product.php

// created: 2016-02-25 20:01:03
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_tasks_1"] = array (
  'name' => 'm03_work_product_tasks_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_M03_WORK_PRODUCT_TASKS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_tasks_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/tasks_m03_work_product_1_M03_Work_Product.php

// created: 2016-02-25 20:23:10
$dictionary["M03_Work_Product"]["fields"]["tasks_m03_work_product_1"] = array (
  'name' => 'tasks_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'tasks_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'tasks_m03_work_product_1tasks_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product"]["fields"]["tasks_m03_work_product_1_name"] = array (
  'name' => 'tasks_m03_work_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_1_FROM_TASKS_TITLE',
  'save' => true,
  'id_name' => 'tasks_m03_work_product_1tasks_ida',
  'link' => 'tasks_m03_work_product_1',
  'table' => 'tasks',
  'module' => 'Tasks',
  'rname' => 'name',
);
$dictionary["M03_Work_Product"]["fields"]["tasks_m03_work_product_1tasks_ida"] = array (
  'name' => 'tasks_m03_work_product_1tasks_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_TASKS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID',
  'id_name' => 'tasks_m03_work_product_1tasks_ida',
  'link' => 'tasks_m03_work_product_1',
  'table' => 'tasks',
  'module' => 'Tasks',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-10-25 03:26:55
$dictionary['M03_Work_Product']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_work_product_id_sponsor_c.php

 // created: 2016-10-25 03:27:08
$dictionary['M03_Work_Product']['fields']['work_product_id_sponsor_c']['labelValue'] = 'Work Product ID (Sponsor)';
$dictionary['M03_Work_Product']['fields']['work_product_id_sponsor_c']['full_text_search']['enabled'] = true;
$dictionary['M03_Work_Product']['fields']['work_product_id_sponsor_c']['full_text_search']['searchable'] = false;
$dictionary['M03_Work_Product']['fields']['work_product_id_sponsor_c']['full_text_search']['boost'] = 1;
$dictionary['M03_Work_Product']['fields']['work_product_id_sponsor_c']['enforced'] = '';
$dictionary['M03_Work_Product']['fields']['work_product_id_sponsor_c']['dependency'] = '';


?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_abc12_work_product_activities_1_M03_Work_Product.php

// created: 2017-02-17 17:50:46
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_abc12_work_product_activities_1"] = array (
  'name' => 'm03_work_product_abc12_work_product_activities_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_abc12_work_product_activities_1',
  'source' => 'non-db',
  'module' => 'ABC12_Work_Product_Activities',
  'bean_name' => 'ABC12_Work_Product_Activities',
  'vname' => 'LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_p4898product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_meetings_1_M03_Work_Product.php

// created: 2017-05-02 19:55:40
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_meetings_1"] = array (
  'name' => 'm03_work_product_meetings_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_meetings_1',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_meetings_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_a1a_critical_phase_inspectio_1_M03_Work_Product.php

// created: 2017-06-07 19:01:24
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_a1a_critical_phase_inspectio_1"] = array (
  'name' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'source' => 'non-db',
  'module' => 'A1A_Critical_Phase_Inspectio',
  'bean_name' => 'A1A_Critical_Phase_Inspectio',
  'vname' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_p33edproduct_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_wpe_work_product_enrollment_1_M03_Work_Product.php

// created: 2017-09-12 15:10:30
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_wpe_work_product_enrollment_1"] = array (
  'name' => 'm03_work_product_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_p7d13product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m06_error_m03_work_product_1_M03_Work_Product.php

// created: 2018-01-31 15:57:04
$dictionary["M03_Work_Product"]["fields"]["m06_error_m03_work_product_1"] = array (
  'name' => 'm06_error_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'm06_error_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_m03_work_product_1m06_error_ida',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m99_protocol_amendments_m03_work_product_M03_Work_Product.php

// created: 2018-02-05 14:21:02
$dictionary["M03_Work_Product"]["fields"]["m99_protocol_amendments_m03_work_product"] = array (
  'name' => 'm99_protocol_amendments_m03_work_product',
  'type' => 'link',
  'relationship' => 'm99_protocol_amendments_m03_work_product',
  'source' => 'non-db',
  'module' => 'M99_Protocol_Amendments',
  'bean_name' => false,
  'vname' => 'LBL_M99_PROTOCOL_AMENDMENTS_M03_WORK_PRODUCT_FROM_M99_PROTOCOL_AMENDMENTS_TITLE',
  'id_name' => 'm99_protoc2862ndments_ida',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_sw_study_workflow_1_M03_Work_Product.php

// created: 2018-10-09 16:44:23
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_sw_study_workflow_1"] = array (
  'name' => 'm03_work_product_sw_study_workflow_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_sw_study_workflow_1',
  'source' => 'non-db',
  'module' => 'SW_Study_Workflow',
  'bean_name' => 'SW_Study_Workflow',
  'vname' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_sw_study_workflow_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/contacts_m03_work_product_1_M03_Work_Product.php

// created: 2018-12-21 23:14:19
$dictionary["M03_Work_Product"]["fields"]["contacts_m03_work_product_1"] = array (
  'name' => 'contacts_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'contacts_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'contacts_m03_work_product_1contacts_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product"]["fields"]["contacts_m03_work_product_1_name"] = array (
  'name' => 'contacts_m03_work_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_m03_work_product_1contacts_ida',
  'link' => 'contacts_m03_work_product_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["M03_Work_Product"]["fields"]["contacts_m03_work_product_1contacts_ida"] = array (
  'name' => 'contacts_m03_work_product_1contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID',
  'id_name' => 'contacts_m03_work_product_1contacts_ida',
  'link' => 'contacts_m03_work_product_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/accounts_m03_work_product_1_M03_Work_Product.php

// created: 2018-12-26 15:29:08
$dictionary["M03_Work_Product"]["fields"]["accounts_m03_work_product_1"] = array (
  'name' => 'accounts_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'accounts_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'accounts_m03_work_product_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product"]["fields"]["accounts_m03_work_product_1_name"] = array (
  'name' => 'accounts_m03_work_product_1_name',
  'type' => 'relate',
  'required' => 'true',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_m03_work_product_1accounts_ida',
  'link' => 'accounts_m03_work_product_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["M03_Work_Product"]["fields"]["accounts_m03_work_product_1accounts_ida"] = array (
  'name' => 'accounts_m03_work_product_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID',
  'id_name' => 'accounts_m03_work_product_1accounts_ida',
  'link' => 'accounts_m03_work_product_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_contact_id_c.php

 // created: 2019-01-16 21:03:05

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_study_director_script_c.php

 // created: 2019-02-05 19:47:28
$dictionary['M03_Work_Product']['fields']['study_director_script_c']['labelValue']='Study Director';
$dictionary['M03_Work_Product']['fields']['study_director_script_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_total_animal_numbers_c.php

 // created: 2019-02-12 15:39:24
$dictionary['M03_Work_Product']['fields']['total_animal_numbers_c']['labelValue']='Total Animal Numbers';
$dictionary['M03_Work_Product']['fields']['total_animal_numbers_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['total_animal_numbers_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['total_animal_numbers_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_test_system_description_c.php

 // created: 2019-02-12 15:41:29
$dictionary['M03_Work_Product']['fields']['test_system_description_c']['labelValue']='Test System Description';
$dictionary['M03_Work_Product']['fields']['test_system_description_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['test_system_description_c']['enforced']='false';
$dictionary['M03_Work_Product']['fields']['test_system_description_c']['dependency']='equal($test_system_c,"Bacterial Strain")';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_first_procedure_c.php

 // created: 2019-02-13 13:09:48
$dictionary['M03_Work_Product']['fields']['first_procedure_c']['labelValue']='First Procedure';
$dictionary['M03_Work_Product']['fields']['first_procedure_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['first_procedure_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_last_procedure_c.php

 // created: 2019-02-13 13:11:14
$dictionary['M03_Work_Product']['fields']['last_procedure_c']['labelValue']='Last Procedure';
$dictionary['M03_Work_Product']['fields']['last_procedure_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['last_procedure_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_study_director_initiation_c.php

 // created: 2019-02-13 13:14:02
$dictionary['M03_Work_Product']['fields']['study_director_initiation_c']['labelValue']='Study Director Initiation Date';
$dictionary['M03_Work_Product']['fields']['study_director_initiation_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['study_director_initiation_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_description.php

 // created: 2019-02-13 13:20:18
$dictionary['M03_Work_Product']['fields']['description']['audited']=true;
$dictionary['M03_Work_Product']['fields']['description']['massupdate']=false;
$dictionary['M03_Work_Product']['fields']['description']['comments']='Full text of the note';
$dictionary['M03_Work_Product']['fields']['description']['duplicate_merge']='enabled';
$dictionary['M03_Work_Product']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['M03_Work_Product']['fields']['description']['merge_filter']='disabled';
$dictionary['M03_Work_Product']['fields']['description']['unified_search']=false;
$dictionary['M03_Work_Product']['fields']['description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '0.5',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['description']['calculated']=false;
$dictionary['M03_Work_Product']['fields']['description']['rows']='6';
$dictionary['M03_Work_Product']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_extract_abnormality_c.php

 // created: 2019-02-13 13:20:48
$dictionary['M03_Work_Product']['fields']['extract_abnormality_c']['labelValue']='Extract Abnormality?';
$dictionary['M03_Work_Product']['fields']['extract_abnormality_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['extract_abnormality_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_describe_abnormality_c.php

 // created: 2019-02-13 13:22:00
$dictionary['M03_Work_Product']['fields']['describe_abnormality_c']['labelValue']='Describe Abnormality';
$dictionary['M03_Work_Product']['fields']['describe_abnormality_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['describe_abnormality_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['describe_abnormality_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2019-02-13 13:22:58
$dictionary['M03_Work_Product']['fields']['date_entered']['audited']=true;
$dictionary['M03_Work_Product']['fields']['date_entered']['comments']='Date record created';
$dictionary['M03_Work_Product']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['M03_Work_Product']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['M03_Work_Product']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['M03_Work_Product']['fields']['date_entered']['unified_search']=false;
$dictionary['M03_Work_Product']['fields']['date_entered']['calculated']=false;
$dictionary['M03_Work_Product']['fields']['date_entered']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_iacuc_protocol_status_c.php

 // created: 2019-02-13 13:36:27
$dictionary['M03_Work_Product']['fields']['iacuc_protocol_status_c']['labelValue']='IACUC Protocol Status';
$dictionary['M03_Work_Product']['fields']['iacuc_protocol_status_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['iacuc_protocol_status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_aps_study_id_c.php

 // created: 2019-02-13 13:53:04
$dictionary['M03_Work_Product']['fields']['aps_study_id_c']['labelValue']='Method Validation Work Product ID (if applicable)';
$dictionary['M03_Work_Product']['fields']['aps_study_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['aps_study_id_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['aps_study_id_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_analyte_c.php

 // created: 2019-02-13 13:54:14
$dictionary['M03_Work_Product']['fields']['analyte_c']['labelValue']='Analyte';
$dictionary['M03_Work_Product']['fields']['analyte_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['analyte_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['analyte_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_matrix_c.php

 // created: 2019-02-13 13:55:29
$dictionary['M03_Work_Product']['fields']['matrix_c']['labelValue']='Matrix';
$dictionary['M03_Work_Product']['fields']['matrix_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['matrix_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['matrix_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_instrument_c.php

 // created: 2019-02-13 13:56:39
$dictionary['M03_Work_Product']['fields']['instrument_c']['labelValue']='Instrument';
$dictionary['M03_Work_Product']['fields']['instrument_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['instrument_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_title_description_c.php

 // created: 2019-04-11 11:54:22
$dictionary['M03_Work_Product']['fields']['title_description_c']['labelValue']='Work Product Title';
$dictionary['M03_Work_Product']['fields']['title_description_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['title_description_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['title_description_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_specimen_retention_start_c.php

 // created: 2019-04-15 11:59:19
$dictionary['M03_Work_Product']['fields']['specimen_retention_start_c']['labelValue']='Specimen Retention Start Date';
$dictionary['M03_Work_Product']['fields']['specimen_retention_start_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['specimen_retention_start_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_two_week_notice_issued_c.php

 // created: 2019-04-15 12:10:31
$dictionary['M03_Work_Product']['fields']['two_week_notice_issued_c']['labelValue']='Two Week Notice Issued';
$dictionary['M03_Work_Product']['fields']['two_week_notice_issued_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['two_week_notice_issued_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_type_of_contact_c.php

 // created: 2019-04-15 12:15:08
$dictionary['M03_Work_Product']['fields']['type_of_contact_c']['labelValue']='Type of Contact';
$dictionary['M03_Work_Product']['fields']['type_of_contact_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['type_of_contact_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_date_of_last_contact_c.php

 // created: 2019-04-15 12:16:20
$dictionary['M03_Work_Product']['fields']['date_of_last_contact_c']['labelValue']='Date of Last Contact';
$dictionary['M03_Work_Product']['fields']['date_of_last_contact_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['date_of_last_contact_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_client_contact_notes_c.php

 // created: 2019-04-15 12:18:11
$dictionary['M03_Work_Product']['fields']['client_contact_notes_c']['labelValue']='Client Contact Notes';
$dictionary['M03_Work_Product']['fields']['client_contact_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['client_contact_notes_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['client_contact_notes_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_bc_spa_reconciled_date_c.php

 // created: 2019-04-15 12:50:55
$dictionary['M03_Work_Product']['fields']['bc_spa_reconciled_date_c']['labelValue']='SPA Reconciled Date';
$dictionary['M03_Work_Product']['fields']['bc_spa_reconciled_date_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['bc_spa_reconciled_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_study_data_location_c.php

 // created: 2019-04-15 13:00:59
$dictionary['M03_Work_Product']['fields']['study_data_location_c']['labelValue']='Archive Location';
$dictionary['M03_Work_Product']['fields']['study_data_location_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['study_data_location_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['study_data_location_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_specimens_c.php

 // created: 2019-04-15 13:02:36
$dictionary['M03_Work_Product']['fields']['specimens_c']['labelValue']='Specimens';
$dictionary['M03_Work_Product']['fields']['specimens_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['specimens_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_specimen_location_c.php

 // created: 2019-04-15 13:03:08
$dictionary['M03_Work_Product']['fields']['specimen_location_c']['labelValue']='Specimen Location';
$dictionary['M03_Work_Product']['fields']['specimen_location_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['specimen_location_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['specimen_location_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_data_retention_start_date_c.php

 // created: 2019-04-15 13:05:53
$dictionary['M03_Work_Product']['fields']['data_retention_start_date_c']['labelValue']='Data Retention Start Date';
$dictionary['M03_Work_Product']['fields']['data_retention_start_date_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['data_retention_start_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_data_scanned_date_c.php

 // created: 2019-04-15 13:08:00
$dictionary['M03_Work_Product']['fields']['data_scanned_date_c']['labelValue']='Study Scanned Date';
$dictionary['M03_Work_Product']['fields']['data_scanned_date_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['data_scanned_date_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['data_scanned_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_archived_date_c.php

 // created: 2019-04-15 13:08:40
$dictionary['M03_Work_Product']['fields']['archived_date_c']['labelValue']='Archived Date';
$dictionary['M03_Work_Product']['fields']['archived_date_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['archived_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_culprits_list_c.php

 // created: 2019-04-15 13:09:58
$dictionary['M03_Work_Product']['fields']['culprits_list_c']['labelValue']='Culprits List';
$dictionary['M03_Work_Product']['fields']['culprits_list_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['culprits_list_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_data_storage_comments_c.php

 // created: 2019-04-15 13:10:44
$dictionary['M03_Work_Product']['fields']['data_storage_comments_c']['labelValue']='Archive Comments';
$dictionary['M03_Work_Product']['fields']['data_storage_comments_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['data_storage_comments_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['data_storage_comments_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_data_retention_end_date_glp_c.php

 // created: 2019-04-16 11:43:45
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_glp_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_glp_c']['labelValue']='Data Retention End Date (GLP)';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_glp_c']['calculated']='true';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_glp_c']['formula']='addDays($data_retention_start_date_c,3650)';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_glp_c']['enforced']='true';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_glp_c']['dependency']='equal($work_product_compliance_c,"GLP")';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_shipped_destroyed_date_text_c.php

 // created: 2019-04-16 12:15:00
$dictionary['M03_Work_Product']['fields']['shipped_destroyed_date_text_c']['labelValue']='Shipped/Destroyed Date Text';
$dictionary['M03_Work_Product']['fields']['shipped_destroyed_date_text_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['shipped_destroyed_date_text_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['shipped_destroyed_date_text_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_contact_id2_c.php

 // created: 2019-04-16 12:23:27

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_name.php

 // created: 2019-04-17 11:37:13
$dictionary['M03_Work_Product']['fields']['name']['len']='255';
$dictionary['M03_Work_Product']['fields']['name']['audited']=true;
$dictionary['M03_Work_Product']['fields']['name']['massupdate']=false;
$dictionary['M03_Work_Product']['fields']['name']['unified_search']=false;
$dictionary['M03_Work_Product']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['M03_Work_Product']['fields']['name']['calculated']=false;
$dictionary['M03_Work_Product']['fields']['name']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_primary_aps_operator_relate_c.php

 // created: 2019-04-17 11:45:04
$dictionary['M03_Work_Product']['fields']['primary_aps_operator_relate_c']['labelValue']='Primary APS Operator Relate';
$dictionary['M03_Work_Product']['fields']['primary_aps_operator_relate_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_contact_id3_c.php

 // created: 2019-04-17 11:45:04

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_aps_principal_investigator_r_c.php

 // created: 2019-04-22 11:49:32
$dictionary['M03_Work_Product']['fields']['aps_principal_investigator_r_c']['labelValue']='APS Principal Investigator Relate';
$dictionary['M03_Work_Product']['fields']['aps_principal_investigator_r_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_contact_id1_c.php

 // created: 2019-04-22 11:49:32

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_contact_id4_c.php

 // created: 2019-04-22 11:53:53

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_contact_id5_c.php

 // created: 2019-05-28 12:16:55

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_analyst_c.php

 // created: 2019-05-28 12:16:55
$dictionary['M03_Work_Product']['fields']['analyst_c']['labelValue']='Analyst';
$dictionary['M03_Work_Product']['fields']['analyst_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_company_status_c.php

 // created: 2019-05-30 15:29:39
$dictionary['M03_Work_Product']['fields']['company_status_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['company_status_c']['labelValue']='Company Status';
$dictionary['M03_Work_Product']['fields']['company_status_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['company_status_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['company_status_c']['formula']='related($m01_sales_m03_work_product_1,"account_status_2_c")';
$dictionary['M03_Work_Product']['fields']['company_status_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['company_status_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_usda_category_multiselect_c.php

 // created: 2019-06-18 19:03:54
$dictionary['M03_Work_Product']['fields']['usda_category_multiselect_c']['labelValue']='USDA Category';
$dictionary['M03_Work_Product']['fields']['usda_category_multiselect_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['usda_category_multiselect_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_iacuc_approval_date_c.php

 // created: 2019-07-03 11:47:08
$dictionary['M03_Work_Product']['fields']['iacuc_approval_date_c']['labelValue']='IACUC Approval Date';
$dictionary['M03_Work_Product']['fields']['iacuc_approval_date_c']['formula']='$study_director_initiation_c';
$dictionary['M03_Work_Product']['fields']['iacuc_approval_date_c']['enforced']='false';
$dictionary['M03_Work_Product']['fields']['iacuc_approval_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/rr_regulatory_response_m03_work_product_1_M03_Work_Product.php

// created: 2019-07-09 11:53:38
$dictionary["M03_Work_Product"]["fields"]["rr_regulatory_response_m03_work_product_1"] = array (
  'name' => 'rr_regulatory_response_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'rr_regulatory_response_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'RR_Regulatory_Response',
  'bean_name' => 'RR_Regulatory_Response',
  'vname' => 'LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'id_name' => 'rr_regulat4120esponse_ida',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_iacuc_closure_date_c.php

 // created: 2019-07-25 11:48:58
$dictionary['M03_Work_Product']['fields']['iacuc_closure_date_c']['labelValue']='IACUC Closure Date';
$dictionary['M03_Work_Product']['fields']['iacuc_closure_date_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['iacuc_closure_date_c']['dependency']='equal($iacuc_protocol_status_c,"Inactive")';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_first_annual_review_c.php

 // created: 2019-07-25 11:59:45
$dictionary['M03_Work_Product']['fields']['first_annual_review_c']['labelValue']='First Annual Review Performed';
$dictionary['M03_Work_Product']['fields']['first_annual_review_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['first_annual_review_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_second_annual_review_c.php

 // created: 2019-07-25 12:00:50
$dictionary['M03_Work_Product']['fields']['second_annual_review_c']['labelValue']='Second Annual Review Performed';
$dictionary['M03_Work_Product']['fields']['second_annual_review_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['second_annual_review_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_triennial_review_c.php

 // created: 2019-07-25 12:02:24
$dictionary['M03_Work_Product']['fields']['triennial_review_c']['labelValue']='Triennial Review Performed';
$dictionary['M03_Work_Product']['fields']['triennial_review_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['triennial_review_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_activities_1_calls_M03_Work_Product.php

// created: 2019-08-05 12:02:55
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_activities_1_calls"] = array (
  'name' => 'm03_work_product_activities_1_calls',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_calls',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_CALLS_FROM_CALLS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_activities_1_meetings_M03_Work_Product.php

// created: 2019-08-05 12:03:27
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_activities_1_meetings"] = array (
  'name' => 'm03_work_product_activities_1_meetings',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_meetings',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_activities_1_notes_M03_Work_Product.php

// created: 2019-08-05 12:03:53
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_activities_1_notes"] = array (
  'name' => 'm03_work_product_activities_1_notes',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_notes',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_NOTES_FROM_NOTES_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_activities_1_tasks_M03_Work_Product.php

// created: 2019-08-05 12:04:19
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_activities_1_tasks"] = array (
  'name' => 'm03_work_product_activities_1_tasks',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_tasks',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_TASKS_FROM_TASKS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_activities_1_emails_M03_Work_Product.php

// created: 2019-08-05 12:04:52
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_activities_1_emails"] = array (
  'name' => 'm03_work_product_activities_1_emails',
  'type' => 'link',
  'relationship' => 'm03_work_product_activities_1_emails',
  'source' => 'non-db',
  'module' => 'Emails',
  'bean_name' => 'Email',
  'vname' => 'LBL_M03_WORK_PRODUCT_ACTIVITIES_1_EMAILS_FROM_EMAILS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_edoc_email_documents_1_M03_Work_Product.php

// created: 2019-08-27 11:37:03
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_edoc_email_documents_1"] = array (
  'name' => 'm03_work_product_edoc_email_documents_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_edoc_email_documents_1',
  'source' => 'non-db',
  'module' => 'EDoc_Email_Documents',
  'bean_name' => 'EDoc_Email_Documents',
  'vname' => 'LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_edoc_email_documents_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_sc_requirement_c.php

 // created: 2019-09-06 12:42:55
$dictionary['M03_Work_Product']['fields']['sc_requirement_c']['labelValue']='SC Requirement';
$dictionary['M03_Work_Product']['fields']['sc_requirement_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['sc_requirement_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_study_coordinator_relate_c.php

 // created: 2019-09-18 11:28:03
$dictionary['M03_Work_Product']['fields']['study_coordinator_relate_c']['labelValue']='Study Coordinator';
$dictionary['M03_Work_Product']['fields']['study_coordinator_relate_c']['dependency']='equal($sc_requirement_c,"Yes")';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_master_schedule_company_name_c.php

 // created: 2019-10-03 12:21:30
$dictionary['M03_Work_Product']['fields']['master_schedule_company_name_c']['labelValue']='Master Schedule Sponsor Name';
$dictionary['M03_Work_Product']['fields']['master_schedule_company_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['master_schedule_company_name_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['master_schedule_company_name_c']['dependency']='isInList($work_product_compliance_c,createList("GLP","Choose Compliance","GLP Not Performed","GLP Discontinued","Withdrawn","GLP_Amended_NonGLP"))';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_test_system_c.php

 // created: 2019-11-01 11:26:52
$dictionary['M03_Work_Product']['fields']['test_system_c']['labelValue']='Test System';
$dictionary['M03_Work_Product']['fields']['test_system_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['test_system_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_erd_error_documents_1_M03_Work_Product.php

// created: 2019-11-05 12:53:29
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_erd_error_documents_1"] = array (
  'name' => 'm03_work_product_erd_error_documents_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_erd_error_documents_1',
  'source' => 'non-db',
  'module' => 'Erd_Error_Documents',
  'bean_name' => 'Erd_Error_Documents',
  'vname' => 'LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'id_name' => 'm03_work_product_erd_error_documents_1erd_error_documents_idb',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_initial_animals_approved_bac_c.php

 // created: 2019-11-06 13:31:51
$dictionary['M03_Work_Product']['fields']['initial_animals_approved_bac_c']['labelValue']='Initial Back-Up Animals Approved';
$dictionary['M03_Work_Product']['fields']['initial_animals_approved_bac_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['initial_animals_approved_bac_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['initial_animals_approved_bac_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_animals_added_backup_c.php

 // created: 2019-11-06 13:33:30
$dictionary['M03_Work_Product']['fields']['animals_added_backup_c']['labelValue']='Additional Back-Up Animals Approved';
$dictionary['M03_Work_Product']['fields']['animals_added_backup_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['animals_added_backup_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['animals_added_backup_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_total_animals_used_backup_c.php

 // created: 2019-11-06 13:34:47
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['labelValue']='Total Back-Up Animals Approved';
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['formula']='add($animals_added_backup_c,$initial_animals_approved_bac_c)';
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['total_animals_used_backup_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_initial_weight_range_c.php

 // created: 2019-11-06 13:50:21
$dictionary['M03_Work_Product']['fields']['initial_weight_range_c']['labelValue']='Initial Weight Range';
$dictionary['M03_Work_Product']['fields']['initial_weight_range_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['initial_weight_range_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['initial_weight_range_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_weight_range_changed_to_c.php

 // created: 2019-11-06 13:51:15
$dictionary['M03_Work_Product']['fields']['weight_range_changed_to_c']['labelValue']='Weight Range Changed To';
$dictionary['M03_Work_Product']['fields']['weight_range_changed_to_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['weight_range_changed_to_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['weight_range_changed_to_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_iacuc_notes_c.php

 // created: 2019-11-06 13:53:02
$dictionary['M03_Work_Product']['fields']['iacuc_notes_c']['labelValue']='IACUC Notes';
$dictionary['M03_Work_Product']['fields']['iacuc_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['iacuc_notes_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['iacuc_notes_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_exemption_notes_c.php

 // created: 2019-11-07 12:41:19
$dictionary['M03_Work_Product']['fields']['exemption_notes_c']['labelValue']='Exemption Notes';
$dictionary['M03_Work_Product']['fields']['exemption_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['exemption_notes_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['exemption_notes_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_unspecified_sex_c.php

 // created: 2019-11-11 13:48:39
$dictionary['M03_Work_Product']['fields']['unspecified_sex_c']['labelValue']='Unspecified Sex';
$dictionary['M03_Work_Product']['fields']['unspecified_sex_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['unspecified_sex_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_initial_males_c.php

 // created: 2019-11-11 14:06:49
$dictionary['M03_Work_Product']['fields']['initial_males_c']['labelValue']='Initial Males';
$dictionary['M03_Work_Product']['fields']['initial_males_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['initial_males_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['initial_males_c']['dependency']='equal($unspecified_sex_c,false)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_initial_females_c.php

 // created: 2019-11-11 14:07:14
$dictionary['M03_Work_Product']['fields']['initial_females_c']['labelValue']='Initial Females';
$dictionary['M03_Work_Product']['fields']['initial_females_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['initial_females_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['initial_females_c']['dependency']='equal($unspecified_sex_c,false)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_added_males_c.php

 // created: 2019-11-11 14:07:42
$dictionary['M03_Work_Product']['fields']['added_males_c']['labelValue']='Added Males';
$dictionary['M03_Work_Product']['fields']['added_males_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['added_males_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['added_males_c']['dependency']='equal($unspecified_sex_c,false)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_added_females_c.php

 // created: 2019-11-11 14:08:05
$dictionary['M03_Work_Product']['fields']['added_females_c']['labelValue']='Added Females';
$dictionary['M03_Work_Product']['fields']['added_females_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['added_females_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['added_females_c']['dependency']='equal($unspecified_sex_c,false)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_total_males_c.php

 // created: 2019-11-11 14:08:31
$dictionary['M03_Work_Product']['fields']['total_males_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['total_males_c']['labelValue']='Total Males';
$dictionary['M03_Work_Product']['fields']['total_males_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['total_males_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['total_males_c']['formula']='add($initial_males_c,$added_males_c)';
$dictionary['M03_Work_Product']['fields']['total_males_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['total_males_c']['dependency']='equal($unspecified_sex_c,false)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_total_females_c.php

 // created: 2019-11-11 14:08:54
$dictionary['M03_Work_Product']['fields']['total_females_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['total_females_c']['labelValue']='Total Females';
$dictionary['M03_Work_Product']['fields']['total_females_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['total_females_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['total_females_c']['formula']='add($initial_females_c,$added_females_c)';
$dictionary['M03_Work_Product']['fields']['total_females_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['total_females_c']['dependency']='equal($unspecified_sex_c,false)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_m03_work_product_id_c.php

 // created: 2019-11-12 12:46:40

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_see_amendment_numbers_c.php

 // created: 2019-11-12 12:53:25
$dictionary['M03_Work_Product']['fields']['see_amendment_numbers_c']['labelValue']='Animal Usage Notes';
$dictionary['M03_Work_Product']['fields']['see_amendment_numbers_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['see_amendment_numbers_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['see_amendment_numbers_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_first_annual_review_complete_c.php

 // created: 2019-11-13 13:45:18
$dictionary['M03_Work_Product']['fields']['first_annual_review_complete_c']['labelValue']='First Annual Review Completed';
$dictionary['M03_Work_Product']['fields']['first_annual_review_complete_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['first_annual_review_complete_c']['dependency']='equal($first_annual_review_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_second_annual_review_complet_c.php

 // created: 2019-11-13 13:46:23
$dictionary['M03_Work_Product']['fields']['second_annual_review_complet_c']['labelValue']='Second Annual Review Completed';
$dictionary['M03_Work_Product']['fields']['second_annual_review_complet_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['second_annual_review_complet_c']['dependency']='equal($second_annual_review_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_triennial_review_completed_c.php

 // created: 2019-11-13 13:47:19
$dictionary['M03_Work_Product']['fields']['triennial_review_completed_c']['labelValue']='Triennial Review Completed';
$dictionary['M03_Work_Product']['fields']['triennial_review_completed_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['triennial_review_completed_c']['dependency']='equal($triennial_review_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_hazardous_agents_c.php

 // created: 2019-11-13 13:56:39
$dictionary['M03_Work_Product']['fields']['hazardous_agents_c']['labelValue']='Hazardous Agents (cb)';
$dictionary['M03_Work_Product']['fields']['hazardous_agents_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['hazardous_agents_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_no_exemptions_c.php

 // created: 2019-11-13 14:18:51
$dictionary['M03_Work_Product']['fields']['no_exemptions_c']['labelValue']='No Exemptions';
$dictionary['M03_Work_Product']['fields']['no_exemptions_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['no_exemptions_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_hazardous_substance_c.php

 // created: 2019-11-14 11:59:25
$dictionary['M03_Work_Product']['fields']['hazardous_substance_c']['labelValue']='Hazardous Agents';
$dictionary['M03_Work_Product']['fields']['hazardous_substance_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['M03_Work_Product']['fields']['hazardous_substance_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['hazardous_substance_c']['dependency']='equal($hazardous_agents_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_iacuc_first_annual_review_c.php

 // created: 2019-11-06 13:28:33
$dictionary['M03_Work_Product']['fields']['iacuc_first_annual_review_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['iacuc_first_annual_review_c']['labelValue']='First Annual Review Due';
$dictionary['M03_Work_Product']['fields']['iacuc_first_annual_review_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['iacuc_first_annual_review_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_iacuc_second_annual_review_c.php

 // created: 2019-11-06 13:29:22
$dictionary['M03_Work_Product']['fields']['iacuc_second_annual_review_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['iacuc_second_annual_review_c']['labelValue']='Second Annual Review Due';
$dictionary['M03_Work_Product']['fields']['iacuc_second_annual_review_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['iacuc_second_annual_review_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_iacuc_triennial_review_c.php

 // created: 2019-11-06 13:29:52
$dictionary['M03_Work_Product']['fields']['iacuc_triennial_review_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['iacuc_triennial_review_c']['labelValue']='Triennial Review Due';
$dictionary['M03_Work_Product']['fields']['iacuc_triennial_review_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['iacuc_triennial_review_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_back_up_animals_countdown_c.php

 // created: 2020-02-05 11:11:50
$dictionary['M03_Work_Product']['fields']['back_up_animals_countdown_c']['labelValue']='Backup Animals Remaining';
$dictionary['M03_Work_Product']['fields']['back_up_animals_countdown_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['back_up_animals_countdown_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['back_up_animals_countdown_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_assigned_to_for_coms_c.php

 // created: 2020-02-05 13:05:35
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['labelValue']='Assigned to for Coms';
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['formula']='concat(related($assigned_user_link,"first_name")," ",related($assigned_user_link,"last_name"))';
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['assigned_to_for_coms_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_data_retention_end_date_nong_c.php

 // created: 2020-04-27 18:21:42
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_nong_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_nong_c']['labelValue']='Data Retention End Date (nonGLP)';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_nong_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_nong_c']['formula']='addDays($data_retention_start_date_c,182)';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_nong_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['data_retention_end_date_nong_c']['dependency']='isInList($work_product_compliance_c,createList("nonGLP","Not Applicable","GLP Not Performed","GLP Discontinued","Withdrawn","GLP_Amended_NonGLP","NonGLP Structured","NonGLP Discovery"))';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_specimen_retention_end_date2_c.php

 // created: 2020-04-27 18:23:26
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_date2_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_date2_c']['labelValue']='Specimen Retention End Date (nonGLP)';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_date2_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_date2_c']['formula']='addDays($specimen_retention_start_c,182)';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_date2_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_date2_c']['dependency']='isInList($work_product_compliance_c,createList("nonGLP","NonGLP Discovery","NonGLP Structured"))';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_master_schedule_sd_c.php

 // created: 2020-05-21 11:05:07
$dictionary['M03_Work_Product']['fields']['master_schedule_sd_c']['labelValue']='Master Schedule Study Director';
$dictionary['M03_Work_Product']['fields']['master_schedule_sd_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['master_schedule_sd_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['master_schedule_sd_c']['dependency']='isInList($work_product_compliance_c,createList("GLP","Choose Compliance","GLP Not Performed","GLP Discontinued","Withdrawn","GLP_Amended_NonGLP"))';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_data_storage_phase_c.php

 // created: 2020-05-21 18:04:09
$dictionary['M03_Work_Product']['fields']['data_storage_phase_c']['labelValue']='Data Storage Phase';
$dictionary['M03_Work_Product']['fields']['data_storage_phase_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['data_storage_phase_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_expanded_study_result_c.php

 // created: 2020-05-21 18:05:29
$dictionary['M03_Work_Product']['fields']['expanded_study_result_c']['labelValue']='Expanded Study Result';
$dictionary['M03_Work_Product']['fields']['expanded_study_result_c']['dependency']='equal($study_outcome_type_c,"Expanded Study")';
$dictionary['M03_Work_Product']['fields']['expanded_study_result_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_study_duration_c.php

 // created: 2020-06-12 17:15:10
$dictionary['M03_Work_Product']['fields']['study_duration_c']['labelValue']='Study Duration';
$dictionary['M03_Work_Product']['fields']['study_duration_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['study_duration_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['study_duration_c']['formula']='ifElse(or(equal($last_procedure_c,""),equal($first_procedure_c,"")),"",subtract(daysUntil($last_procedure_c),daysUntil($first_procedure_c)))';
$dictionary['M03_Work_Product']['fields']['study_duration_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['study_duration_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_study_article_c.php

 // created: 2020-08-11 08:00:23
$dictionary['M03_Work_Product']['fields']['study_article_c']['labelValue']='Test Article(s)';
$dictionary['M03_Work_Product']['fields']['study_article_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['study_article_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_control_article_c.php

 // created: 2020-08-11 08:03:34
$dictionary['M03_Work_Product']['fields']['control_article_c']['labelValue']='Control Article(s)';
$dictionary['M03_Work_Product']['fields']['control_article_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['control_article_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_control_articles_location_c.php

 // created: 2020-08-11 08:04:45
$dictionary['M03_Work_Product']['fields']['control_articles_location_c']['labelValue']='Control Article(s) Location';
$dictionary['M03_Work_Product']['fields']['control_articles_location_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['control_articles_location_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['control_articles_location_c']['dependency']='equal($control_article_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_control_article_unique_ids_c.php

 // created: 2020-08-11 08:06:10
$dictionary['M03_Work_Product']['fields']['control_article_unique_ids_c']['labelValue']='Control Article Unique ID(s)';
$dictionary['M03_Work_Product']['fields']['control_article_unique_ids_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['control_article_unique_ids_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['control_article_unique_ids_c']['dependency']='equal($control_article_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_ta_unique_ids_c.php

 // created: 2020-08-11 08:08:06
$dictionary['M03_Work_Product']['fields']['ta_unique_ids_c']['labelValue']='Test Article Unique ID(s)';
$dictionary['M03_Work_Product']['fields']['ta_unique_ids_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['ta_unique_ids_c']['enforced']='false';
$dictionary['M03_Work_Product']['fields']['ta_unique_ids_c']['dependency']='equal($study_article_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_study_articles_location_c.php

 // created: 2020-08-11 08:11:24
$dictionary['M03_Work_Product']['fields']['study_articles_location_c']['labelValue']='Test Article(s) Location';
$dictionary['M03_Work_Product']['fields']['study_articles_location_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['study_articles_location_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['study_articles_location_c']['dependency']='equal($study_article_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_analytical_pi_c.php

 // created: 2020-10-22 06:15:16
$dictionary['M03_Work_Product']['fields']['analytical_pi_c']['labelValue']='Analytical PI';
$dictionary['M03_Work_Product']['fields']['analytical_pi_c']['dependency']='equal($analytical_deliverables_c,"Yes")';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_contact_id6_c.php

 // created: 2020-10-22 06:15:16

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_analytical_deliverables_c.php

 // created: 2020-10-26 11:16:39
$dictionary['M03_Work_Product']['fields']['analytical_deliverables_c']['labelValue']='Are there Analytical Deliverables?';
$dictionary['M03_Work_Product']['fields']['analytical_deliverables_c']['dependency']='not(equal($functional_area_c,"Analytical Services"))';
$dictionary['M03_Work_Product']['fields']['analytical_deliverables_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_blanket_animals_remaining_c.php

 // created: 2020-10-27 06:42:52
$dictionary['M03_Work_Product']['fields']['blanket_animals_remaining_c']['labelValue']='Blanket Protocol Animals Remaining';
$dictionary['M03_Work_Product']['fields']['blanket_animals_remaining_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['blanket_animals_remaining_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['blanket_animals_remaining_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_blanket_protocol_hidden_c.php

 // created: 2020-10-27 06:44:03
$dictionary['M03_Work_Product']['fields']['blanket_protocol_hidden_c']['labelValue']='blanket protocol hidden';
$dictionary['M03_Work_Product']['fields']['blanket_protocol_hidden_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['blanket_protocol_hidden_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['blanket_protocol_hidden_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_wpe_work_product_enrollment_2_M03_Work_Product.php

// created: 2020-12-15 06:35:00
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_wpe_work_product_enrollment_2"] = array (
  'name' => 'm03_work_product_wpe_work_product_enrollment_2',
  'type' => 'link',
  'relationship' => 'm03_work_product_wpe_work_product_enrollment_2',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_p9f23product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_initial_animals_approved_pri_c.php

 // created: 2020-12-15 12:28:52
$dictionary['M03_Work_Product']['fields']['initial_animals_approved_pri_c']['labelValue']='Initial Animals Approved';
$dictionary['M03_Work_Product']['fields']['initial_animals_approved_pri_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['initial_animals_approved_pri_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['initial_animals_approved_pri_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['initial_animals_approved_pri_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_animals_added_primary_c.php

 // created: 2020-12-15 12:29:37
$dictionary['M03_Work_Product']['fields']['animals_added_primary_c']['labelValue']='Additional Animals Approved';
$dictionary['M03_Work_Product']['fields']['animals_added_primary_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['animals_added_primary_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['animals_added_primary_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['animals_added_primary_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_total_animals_used_primary_c.php

 // created: 2020-12-15 12:30:29
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['labelValue']='Total Animals Approved';
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['formula']='add($initial_animals_approved_pri_c,$animals_added_primary_c)';
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['total_animals_used_primary_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_primary_animals_countdown_c.php

 // created: 2020-12-15 12:31:04
$dictionary['M03_Work_Product']['fields']['primary_animals_countdown_c']['labelValue']='Animals Remaining';
$dictionary['M03_Work_Product']['fields']['primary_animals_countdown_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['primary_animals_countdown_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['primary_animals_countdown_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['primary_animals_countdown_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_m03_work_product_code_1_M03_Work_Product.php

// created: 2021-01-21 14:15:48
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_m03_work_product_code_1"] = array (
  'name' => 'm03_work_product_m03_work_product_code_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_m03_work_product_code_1',
  'source' => 'non-db',
  'module' => 'M03_Work_Product_Code',
  'bean_name' => 'M03_Work_Product_Code',
  'vname' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'id_name' => 'm03_work_p5357ct_code_idb',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_blanket_protocol_c.php

 // created: 2021-01-22 21:58:36
$dictionary['M03_Work_Product']['fields']['blanket_protocol_c']['labelValue']='Blanket Protocol';
$dictionary['M03_Work_Product']['fields']['blanket_protocol_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['blanket_protocol_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_target_animals_c.php

 // created: 2021-02-04 10:53:02
$dictionary['M03_Work_Product']['fields']['target_animals_c']['labelValue']='Target Animals';
$dictionary['M03_Work_Product']['fields']['target_animals_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['target_animals_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['target_animals_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['target_animals_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_tsd1_test_system_design_1_1_M03_Work_Product.php

// created: 2021-02-11 08:58:28
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_tsd1_test_system_design_1_1"] = array (
  'name' => 'm03_work_product_tsd1_test_system_design_1_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_tsd1_test_system_design_1_1',
  'source' => 'non-db',
  'module' => 'TSD1_Test_System_Design_1',
  'bean_name' => 'TSD1_Test_System_Design_1',
  'vname' => 'LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_tsd1_test_system_design_1_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_quoted_protocol_hours_c.php

 // created: 2021-02-16 07:09:54
$dictionary['M03_Work_Product']['fields']['quoted_protocol_hours_c']['labelValue']='Quoted Protocol Hours';
$dictionary['M03_Work_Product']['fields']['quoted_protocol_hours_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['quoted_protocol_hours_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['quoted_protocol_hours_c']['dependency']='equal($functional_area_c,"ISR")';
$dictionary['M03_Work_Product']['fields']['quoted_protocol_hours_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_quoted_study_management_hrs_c.php

 // created: 2021-02-16 07:12:44
$dictionary['M03_Work_Product']['fields']['quoted_study_management_hrs_c']['labelValue']='Quoted Study Management Hours';
$dictionary['M03_Work_Product']['fields']['quoted_study_management_hrs_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['quoted_study_management_hrs_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['quoted_study_management_hrs_c']['dependency']='equal($functional_area_c,"ISR")';
$dictionary['M03_Work_Product']['fields']['quoted_study_management_hrs_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_quoted_reporting_hours_c.php

 // created: 2021-02-16 07:14:29
$dictionary['M03_Work_Product']['fields']['quoted_reporting_hours_c']['labelValue']='Quoted Reporting Hours';
$dictionary['M03_Work_Product']['fields']['quoted_reporting_hours_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['quoted_reporting_hours_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['quoted_reporting_hours_c']['dependency']='equal($functional_area_c,"ISR")';
$dictionary['M03_Work_Product']['fields']['quoted_reporting_hours_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_time_to_on_test_c.php

 // created: 2021-02-18 05:26:01
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['labelValue']='Time to On Test';
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['calculated']='true';
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['formula']='ifElse(or(equal($bc_spa_reconciled_date_c,""),equal($first_procedure_c,"")),"",subtract(daysUntil($first_procedure_c),daysUntil($bc_spa_reconciled_date_c)))';
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['enforced']='true';
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['time_to_on_test_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_study_workload_c.php

 // created: 2021-03-04 09:28:47
$dictionary['M03_Work_Product']['fields']['study_workload_c']['labelValue']='Study Workload';
$dictionary['M03_Work_Product']['fields']['study_workload_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['study_workload_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['study_workload_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['study_workload_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['study_workload_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_timeline_type_c.php

 // created: 2021-03-12 05:36:45
$dictionary['M03_Work_Product']['fields']['timeline_type_c']['labelValue']='Timeline Type';
$dictionary['M03_Work_Product']['fields']['timeline_type_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['timeline_type_c']['enforced']='false';
$dictionary['M03_Work_Product']['fields']['timeline_type_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['timeline_type_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['timeline_type_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/meetings_m03_work_product_1_M03_Work_Product.php

// created: 2021-04-01 09:05:41
$dictionary["M03_Work_Product"]["fields"]["meetings_m03_work_product_1"] = array (
  'name' => 'meetings_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'meetings_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_MEETINGS_TITLE',
  'id_name' => 'meetings_m03_work_product_1meetings_ida',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_actual_reporting_hours_c.php

 // created: 2021-04-01 09:14:39
$dictionary['M03_Work_Product']['fields']['actual_reporting_hours_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['actual_reporting_hours_c']['labelValue']='Actual Reporting Hours';
$dictionary['M03_Work_Product']['fields']['actual_reporting_hours_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['actual_reporting_hours_c']['formula']='add(
rollupConditionalSum(
	$m03_work_product_meetings_1,
	"actual_hours_worked_c",
	"scientific_activity_c",
	"Reporting"
),
rollupConditionalSum(
	$meetings_m03_work_product_1,
	"actual_hours_worked_c",
	"scientific_activity_c",
	"Reporting"
))';
$dictionary['M03_Work_Product']['fields']['actual_reporting_hours_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['actual_reporting_hours_c']['dependency']='equal($functional_area_c,"ISR")';
$dictionary['M03_Work_Product']['fields']['actual_reporting_hours_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['actual_reporting_hours_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_actual_protocol_hours_c.php

 // created: 2021-04-01 09:17:21
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['labelValue']='Actual Protocol Hours';
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['formula']='add(
rollupConditionalSum(
	$m03_work_product_meetings_1,
	"actual_hours_worked_c",
	"scientific_activity_c",
	"Protocol Development"
),
rollupConditionalSum(
	$meetings_m03_work_product_1,
	"actual_hours_worked_c",
	"scientific_activity_c",
	"Protocol Development"
))';
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['dependency']='equal($functional_area_c,"ISR")';
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['actual_protocol_hours_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_actual_study_management_hrs_c.php

 // created: 2021-04-01 09:18:25
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['labelValue']='Actual Study Management Hours';
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['formula']='add(
rollupConditionalSum(
	$m03_work_product_meetings_1,
	"actual_hours_worked_c",
	"scientific_activity_c",
	"Operations Oversight"
),
rollupConditionalSum(
	$meetings_m03_work_product_1,
	"actual_hours_worked_c",
	"scientific_activity_c",
	"Operations Oversight"
))';
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['dependency']='equal($functional_area_c,"ISR")';
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['actual_study_management_hrs_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_number_of_peaks_gcms_c.php

 // created: 2021-04-20 08:28:05
$dictionary['M03_Work_Product']['fields']['number_of_peaks_gcms_c']['labelValue']='Number of Peaks Identified for GCMS Analysis';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_gcms_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['number_of_peaks_gcms_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_gcms_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_gcms_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_gcms_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_number_of_peaks_lcms_c.php

 // created: 2021-04-20 08:29:27
$dictionary['M03_Work_Product']['fields']['number_of_peaks_lcms_c']['labelValue']='Number of Peaks Identified for LCMS Analysis';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_lcms_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['number_of_peaks_lcms_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_lcms_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_lcms_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['number_of_peaks_lcms_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_wpc_sc01_c.php

 // created: 2021-04-29 08:48:48
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['labelValue']='WPC SC01';
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['calculated']='true';
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['formula']='contains($name,"SC01")';
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['enforced']='true';
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['wpc_sc01_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_spa_review_to_on_test_c.php

 // created: 2021-06-17 08:34:47
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['labelValue']='SPA Review to On Test';
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['calculated']='true';
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['formula']='ifElse(or(equal(related($m01_sales_m03_work_product_1,"bc_study_article_received_c"),""),equal($first_procedure_c,"")),"",subtract(daysUntil($first_procedure_c),daysUntil(related($m01_sales_m03_work_product_1,"bc_study_article_received_c"))))';
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['enforced']='true';
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['spa_review_to_on_test_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_final_report_timeline_c.php

 // created: 2021-07-22 06:37:21
$dictionary['M03_Work_Product']['fields']['final_report_timeline_c']['labelValue']='Final Report Timeline (Weeks)';
$dictionary['M03_Work_Product']['fields']['final_report_timeline_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['final_report_timeline_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['final_report_timeline_c']['dependency']='equal($functional_area_c,"Standard Biocompatibility")';
$dictionary['M03_Work_Product']['fields']['final_report_timeline_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['final_report_timeline_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_report_timeline_type_c.php

 // created: 2021-07-22 06:50:02
$dictionary['M03_Work_Product']['fields']['report_timeline_type_c']['labelValue']='Report Timeline Type';
$dictionary['M03_Work_Product']['fields']['report_timeline_type_c']['dependency']='equal($functional_area_c,"Standard Biocompatibility")';
$dictionary['M03_Work_Product']['fields']['report_timeline_type_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['report_timeline_type_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['report_timeline_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_gd_group_design_1_M03_Work_Product.php

// created: 2021-08-14 05:59:50
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_gd_group_design_1"] = array (
  'name' => 'm03_work_product_gd_group_design_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_gd_group_design_1',
  'source' => 'non-db',
  'module' => 'GD_Group_Design',
  'bean_name' => 'GD_Group_Design',
  'vname' => 'LBL_M03_WORK_PRODUCT_GD_GROUP_DESIGN_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_gd_group_design_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_taskd_task_design_1_M03_Work_Product.php

// created: 2021-08-14 06:01:53
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_taskd_task_design_1"] = array (
  'name' => 'm03_work_product_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'vname' => 'LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_taskd_task_design_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_contact_id7_c.php

 // created: 2021-08-24 08:44:08

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_ori_order_request_item_1_M03_Work_Product.php

// created: 2021-10-19 10:30:05
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_ori_order_request_item_1"] = array (
  'name' => 'm03_work_product_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'vname' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_ori_order_request_item_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_poi_purchase_order_item_1_M03_Work_Product.php

// created: 2021-10-19 11:00:50
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_poi_purchase_order_item_1"] = array (
  'name' => 'm03_work_product_poi_purchase_order_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_poi_purchase_order_item_1',
  'source' => 'non-db',
  'module' => 'POI_Purchase_Order_Item',
  'bean_name' => 'POI_Purchase_Order_Item',
  'vname' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_poi_purchase_order_item_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_reason_for_expansion_c.php

 // created: 2021-10-26 09:07:44
$dictionary['M03_Work_Product']['fields']['reason_for_expansion_c']['labelValue']='Reason for Expansion';
$dictionary['M03_Work_Product']['fields']['reason_for_expansion_c']['dependency']='equal($study_outcome_type_c,"Expanded Study")';
$dictionary['M03_Work_Product']['fields']['reason_for_expansion_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['reason_for_expansion_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['reason_for_expansion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_reason_for_discontinuation_c.php

 // created: 2021-10-26 09:11:51
$dictionary['M03_Work_Product']['fields']['reason_for_discontinuation_c']['labelValue']='Reason for Discontinuation';
$dictionary['M03_Work_Product']['fields']['reason_for_discontinuation_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['reason_for_discontinuation_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['reason_for_discontinuation_c']['dependency']='equal($study_outcome_type_c,"Discontinued Study")';
$dictionary['M03_Work_Product']['fields']['reason_for_discontinuation_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['reason_for_discontinuation_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_study_outcome_type_c.php

 // created: 2021-10-26 09:18:48
$dictionary['M03_Work_Product']['fields']['study_outcome_type_c']['labelValue']='Study Outcome Type';
$dictionary['M03_Work_Product']['fields']['study_outcome_type_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['study_outcome_type_c']['required_formula']='and(equal($functional_area_c,"Standard Biocompatibility"),equal($work_product_status_c,"Archived"))';
$dictionary['M03_Work_Product']['fields']['study_outcome_type_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['study_outcome_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_work_product_status_c.php

 // created: 2021-10-26 09:30:27
$dictionary['M03_Work_Product']['fields']['work_product_status_c']['labelValue']='Work Product Phase';
$dictionary['M03_Work_Product']['fields']['work_product_status_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['work_product_status_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['work_product_status_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['work_product_status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_test_article_description_c.php

 // created: 2021-11-09 06:42:34
$dictionary['M03_Work_Product']['fields']['test_article_description_c']['labelValue']='Test Article Name (Obsolete)';
$dictionary['M03_Work_Product']['fields']['test_article_description_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['test_article_description_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['test_article_description_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['test_article_description_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['test_article_description_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_test_article_name_c.php

 // created: 2021-11-09 06:44:10
$dictionary['M03_Work_Product']['fields']['test_article_name_c']['labelValue']='Test Article Name ';
$dictionary['M03_Work_Product']['fields']['test_article_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['test_article_name_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['test_article_name_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['test_article_name_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['test_article_name_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_ii_inventory_item_1_M03_Work_Product.php

// created: 2021-11-09 10:04:52
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_ii_inventory_item_1"] = array (
  'name' => 'm03_work_product_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_ii_inventory_item_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_ii_inventory_item_2_M03_Work_Product.php

// created: 2021-11-09 10:07:50
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_ii_inventory_item_2"] = array (
  'name' => 'm03_work_product_ii_inventory_item_2',
  'type' => 'link',
  'relationship' => 'm03_work_product_ii_inventory_item_2',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_II_INVENTORY_ITEM_TITLE',
  'id_name' => 'm03_work_product_ii_inventory_item_2ii_inventory_item_idb',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_im_inventory_management_1_M03_Work_Product.php

// created: 2021-11-09 10:14:52
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_im_inventory_management_1"] = array (
  'name' => 'm03_work_product_im_inventory_management_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_im_inventory_management_1',
  'source' => 'non-db',
  'module' => 'IM_Inventory_Management',
  'bean_name' => 'IM_Inventory_Management',
  'vname' => 'LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_im_inventory_management_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/m03_work_product_ic_inventory_collection_1_M03_Work_Product.php

// created: 2021-11-09 10:26:31
$dictionary["M03_Work_Product"]["fields"]["m03_work_product_ic_inventory_collection_1"] = array (
  'name' => 'm03_work_product_ic_inventory_collection_1',
  'type' => 'link',
  'relationship' => 'm03_work_product_ic_inventory_collection_1',
  'source' => 'non-db',
  'module' => 'IC_Inventory_Collection',
  'bean_name' => 'IC_Inventory_Collection',
  'vname' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm03_work_product_ic_inventory_collection_1m03_work_product_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_specimen_retention_end_c.php

 // created: 2021-11-16 09:42:54
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['labelValue']='Specimen Retention End Date (GLP)';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['calculated']='1';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['formula']='addDays($specimen_retention_start_c,2555)';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['enforced']='1';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['dependency']='equal($work_product_compliance_c,"GLP")';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['specimen_retention_end_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_aaalac_and_usda_exemptions_c.php

 // created: 2021-11-18 10:20:13
$dictionary['M03_Work_Product']['fields']['aaalac_and_usda_exemptions_c']['labelValue']='AAALAC and USDA Exemptions';
$dictionary['M03_Work_Product']['fields']['aaalac_and_usda_exemptions_c']['dependency']='equal($no_exemptions_c,false)';
$dictionary['M03_Work_Product']['fields']['aaalac_and_usda_exemptions_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['aaalac_and_usda_exemptions_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['aaalac_and_usda_exemptions_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_extraction_start_integer_c.php

 // created: 2021-12-02 06:09:13
$dictionary['M03_Work_Product']['fields']['extraction_start_integer_c']['labelValue']='Extraction Start Integer';
$dictionary['M03_Work_Product']['fields']['extraction_start_integer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['extraction_start_integer_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['extraction_start_integer_c']['dependency']='equal($functional_area_c,"Standard Biocompatibility")';
$dictionary['M03_Work_Product']['fields']['extraction_start_integer_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['extraction_start_integer_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_extraction_end_integer_c.php

 // created: 2021-12-02 06:12:33
$dictionary['M03_Work_Product']['fields']['extraction_end_integer_c']['labelValue']='Extraction End Integer';
$dictionary['M03_Work_Product']['fields']['extraction_end_integer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['extraction_end_integer_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['extraction_end_integer_c']['dependency']='equal($functional_area_c,"Standard Biocompatibility")';
$dictionary['M03_Work_Product']['fields']['extraction_end_integer_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['extraction_end_integer_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_u_units_id_c.php

 // created: 2021-12-02 12:47:50

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_integer_units_c.php

 // created: 2021-12-02 12:47:50
$dictionary['M03_Work_Product']['fields']['integer_units_c']['labelValue']='Integer Units';
$dictionary['M03_Work_Product']['fields']['integer_units_c']['dependency']='equal($functional_area_c,"Standard Biocompatibility")';
$dictionary['M03_Work_Product']['fields']['integer_units_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['integer_units_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_required_in_lims_c.php

 // created: 2021-12-07 12:37:33
$dictionary['M03_Work_Product']['fields']['required_in_lims_c']['labelValue']='Required In LIMS?';
$dictionary['M03_Work_Product']['fields']['required_in_lims_c']['dependency']='equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No")';
$dictionary['M03_Work_Product']['fields']['required_in_lims_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['required_in_lims_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['required_in_lims_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_namsa_submission_id_c.php

 // created: 2021-12-07 12:39:05
$dictionary['M03_Work_Product']['fields']['namsa_submission_id_c']['labelValue']='NAMSA Submission ID';
$dictionary['M03_Work_Product']['fields']['namsa_submission_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['namsa_submission_id_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['namsa_submission_id_c']['dependency']='equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No")';
$dictionary['M03_Work_Product']['fields']['namsa_submission_id_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['namsa_submission_id_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_tc_namsa_test_codes_id_c.php

 // created: 2021-12-07 12:40:43

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_namsa_test_code_c.php

 // created: 2021-12-07 12:40:43
$dictionary['M03_Work_Product']['fields']['namsa_test_code_c']['labelValue']='NAMSA Test Code';
$dictionary['M03_Work_Product']['fields']['namsa_test_code_c']['dependency']='equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No")';
$dictionary['M03_Work_Product']['fields']['namsa_test_code_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['namsa_test_code_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_functional_area_c.php

 // created: 2021-12-07 12:44:56
$dictionary['M03_Work_Product']['fields']['functional_area_c']['labelValue']='Functional Area';
$dictionary['M03_Work_Product']['fields']['functional_area_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['functional_area_c']['required_formula']='not(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No"))';
$dictionary['M03_Work_Product']['fields']['functional_area_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['functional_area_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_work_product_compliance_c.php

 // created: 2021-12-07 12:46:04
$dictionary['M03_Work_Product']['fields']['work_product_compliance_c']['labelValue']='Work Product Compliance';
$dictionary['M03_Work_Product']['fields']['work_product_compliance_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['work_product_compliance_c']['required_formula']='not(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No"))';
$dictionary['M03_Work_Product']['fields']['work_product_compliance_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['work_product_compliance_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_work_product_code_2_c.php

 // created: 2021-12-07 12:47:21
$dictionary['M03_Work_Product']['fields']['work_product_code_2_c']['labelValue']='Work Product Code';
$dictionary['M03_Work_Product']['fields']['work_product_code_2_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['work_product_code_2_c']['required_formula']='not(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No"))';
$dictionary['M03_Work_Product']['fields']['work_product_code_2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_accounts_m03_work_product_1accounts_ida.php

 // created: 2021-12-07 12:49:12
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['name']='accounts_m03_work_product_1accounts_ida';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['type']='id';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['source']='non-db';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['vname']='LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['id_name']='accounts_m03_work_product_1accounts_ida';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['link']='accounts_m03_work_product_1';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['table']='accounts';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['module']='Accounts';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['rname']='id';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['reportable']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['side']='right';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['massupdate']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['duplicate_merge']='disabled';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['hideacl']=true;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['audited']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1accounts_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_accounts_m03_work_product_1_name.php

 // created: 2021-12-07 12:49:16
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['required']=true;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['audited']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['massupdate']=true;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['hidemassupdate']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['duplicate_merge']='enabled';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['duplicate_merge_dom_value']='1';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['merge_filter']='disabled';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['reportable']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['unified_search']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['calculated']=false;
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['required_formula']='not(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No"))';
$dictionary['M03_Work_Product']['fields']['accounts_m03_work_product_1_name']['vname']='LBL_ACCOUNTS_M03_WORK_PRODUCT_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_chronic_study_c.php

 // created: 2021-12-07 12:50:01
$dictionary['M03_Work_Product']['fields']['chronic_study_c']['labelValue']='Chronic Study Longer than 28 Days?';
$dictionary['M03_Work_Product']['fields']['chronic_study_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['chronic_study_c']['required_formula']='not(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No"))';
$dictionary['M03_Work_Product']['fields']['chronic_study_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['chronic_study_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_gds_complete_c.php

 // created: 2022-02-08 08:34:42
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['duplicate_merge_dom_value']=0;
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['labelValue']='GDs Complete';
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['calculated']='true';
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['formula']='rollupSum($m03_work_product_gd_group_design_1,"complete_yes_no_c")';
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['enforced']='true';
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['gds_complete_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_lead_data_reviewer_c.php

 // created: 2022-02-15 07:23:45
$dictionary['M03_Work_Product']['fields']['lead_data_reviewer_c']['labelValue']='QA Trainee Reviewer';
$dictionary['M03_Work_Product']['fields']['lead_data_reviewer_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['lead_data_reviewer_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['lead_data_reviewer_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_currency_id.php

 // created: 2022-04-05 07:08:21

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2022-04-05 07:08:21

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/bid_batch_id_m03_work_product_1_M03_Work_Product.php

// created: 2022-04-26 06:44:22
$dictionary["M03_Work_Product"]["fields"]["bid_batch_id_m03_work_product_1"] = array (
  'name' => 'bid_batch_id_m03_work_product_1',
  'type' => 'link',
  'relationship' => 'bid_batch_id_m03_work_product_1',
  'source' => 'non-db',
  'module' => 'BID_Batch_ID',
  'bean_name' => 'BID_Batch_ID',
  'side' => 'right',
  'vname' => 'LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product"]["fields"]["bid_batch_id_m03_work_product_1_name"] = array (
  'name' => 'bid_batch_id_m03_work_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_BID_BATCH_ID_TITLE',
  'save' => true,
  'id_name' => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
  'link' => 'bid_batch_id_m03_work_product_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'name',
);
$dictionary["M03_Work_Product"]["fields"]["bid_batch_id_m03_work_product_1bid_batch_id_ida"] = array (
  'name' => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID',
  'id_name' => 'bid_batch_id_m03_work_product_1bid_batch_id_ida',
  'link' => 'bid_batch_id_m03_work_product_1',
  'table' => 'bid_batch_id',
  'module' => 'BID_Batch_ID',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_parent_work_product_c.php

 // created: 2022-04-26 12:18:35
$dictionary['M03_Work_Product']['fields']['parent_work_product_c']['labelValue']='Parent Work Product';
$dictionary['M03_Work_Product']['fields']['parent_work_product_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['parent_work_product_c']['dependency']='not(equal(related($bid_batch_id_m03_work_product_1,"name"),""))';
$dictionary['M03_Work_Product']['fields']['parent_work_product_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_test_price_c.php

 // created: 2022-05-24 07:10:03
$dictionary['M03_Work_Product']['fields']['test_price_c']['labelValue']='Test Price';
$dictionary['M03_Work_Product']['fields']['test_price_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['test_price_c']['dependency']='isInList($functional_area_c,createList("Standard Biocompatibility","Biocompatibility"))';
$dictionary['M03_Work_Product']['fields']['test_price_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['M03_Work_Product']['fields']['test_price_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['test_price_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_aaalac_requirements_c.php

 // created: 2022-06-07 05:26:13
$dictionary['M03_Work_Product']['fields']['aaalac_requirements_c']['labelValue']='AAALAC Requirements';
$dictionary['M03_Work_Product']['fields']['aaalac_requirements_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['aaalac_requirements_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['aaalac_requirements_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['aaalac_requirements_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_usda_exemptions_c.php

 // created: 2022-06-07 05:29:53
$dictionary['M03_Work_Product']['fields']['usda_exemptions_c']['labelValue']='USDA Exemptions';
$dictionary['M03_Work_Product']['fields']['usda_exemptions_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['usda_exemptions_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['usda_exemptions_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['usda_exemptions_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/contacts_m03_work_product_2_M03_Work_Product.php

// created: 2022-07-07 07:02:30
$dictionary["M03_Work_Product"]["fields"]["contacts_m03_work_product_2"] = array (
  'name' => 'contacts_m03_work_product_2',
  'type' => 'link',
  'relationship' => 'contacts_m03_work_product_2',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'contacts_m03_work_product_2contacts_ida',
  'link-type' => 'one',
);
$dictionary["M03_Work_Product"]["fields"]["contacts_m03_work_product_2_name"] = array (
  'name' => 'contacts_m03_work_product_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_m03_work_product_2contacts_ida',
  'link' => 'contacts_m03_work_product_2',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["M03_Work_Product"]["fields"]["contacts_m03_work_product_2contacts_ida"] = array (
  'name' => 'contacts_m03_work_product_2contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE_ID',
  'id_name' => 'contacts_m03_work_product_2contacts_ida',
  'link' => 'contacts_m03_work_product_2',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_lead_auditor_relate_c.php

 // created: 2022-07-07 07:08:22
$dictionary['M03_Work_Product']['fields']['lead_auditor_relate_c']['labelValue']='Lead Auditor (Obsolete)';
$dictionary['M03_Work_Product']['fields']['lead_auditor_relate_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['lead_auditor_relate_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['lead_auditor_relate_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_contacts_m03_work_product_2contacts_ida.php

 // created: 2022-07-07 12:01:05
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['name']='contacts_m03_work_product_2contacts_ida';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['type']='id';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['source']='non-db';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['vname']='LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE_ID';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['id_name']='contacts_m03_work_product_2contacts_ida';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['link']='contacts_m03_work_product_2';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['table']='contacts';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['module']='Contacts';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['rname']='id';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['reportable']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['side']='right';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['massupdate']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['duplicate_merge']='disabled';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['hideacl']=true;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['audited']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2contacts_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_contacts_m03_work_product_2_name.php

 // created: 2022-07-07 12:01:05
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['audited']=true;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['massupdate']=true;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['hidemassupdate']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['duplicate_merge']='enabled';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['duplicate_merge_dom_value']='1';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['merge_filter']='disabled';
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['reportable']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['unified_search']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['calculated']=false;
$dictionary['M03_Work_Product']['fields']['contacts_m03_work_product_2_name']['vname']='LBL_CONTACTS_M03_WORK_PRODUCT_2_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_extraction_conditions_c.php

 // created: 2022-07-12 09:30:00
$dictionary['M03_Work_Product']['fields']['extraction_conditions_c']['labelValue']='Extraction Conditions';
$dictionary['M03_Work_Product']['fields']['extraction_conditions_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['extraction_conditions_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['extraction_conditions_c']['readonly_formula']='';
$dictionary['M03_Work_Product']['fields']['extraction_conditions_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_other_extraction_conditions_c.php

 // created: 2022-07-12 09:35:16
$dictionary['M03_Work_Product']['fields']['other_extraction_conditions_c']['labelValue']='Other Extraction Conditions';
$dictionary['M03_Work_Product']['fields']['other_extraction_conditions_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['other_extraction_conditions_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['other_extraction_conditions_c']['dependency']='equal($extraction_conditions_c,"Other")';
$dictionary['M03_Work_Product']['fields']['other_extraction_conditions_c']['required_formula']='equal($extraction_conditions_c,"Other")';
$dictionary['M03_Work_Product']['fields']['other_extraction_conditions_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_spa_reconciled_date_tds_c.php

 // created: 2022-08-09 09:57:36
$dictionary['M03_Work_Product']['fields']['spa_reconciled_date_tds_c']['labelValue']='Spa Reconciled Date TDs';
$dictionary['M03_Work_Product']['fields']['spa_reconciled_date_tds_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['spa_reconciled_date_tds_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['spa_reconciled_date_tds_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['spa_reconciled_date_tds_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['spa_reconciled_date_tds_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Vardefs/sugarfield_create_tds_c.php

 // created: 2022-08-09 10:01:16
$dictionary['M03_Work_Product']['fields']['create_tds_c']['labelValue']='create tds';
$dictionary['M03_Work_Product']['fields']['create_tds_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['M03_Work_Product']['fields']['create_tds_c']['enforced']='';
$dictionary['M03_Work_Product']['fields']['create_tds_c']['dependency']='';
$dictionary['M03_Work_Product']['fields']['create_tds_c']['required_formula']='';
$dictionary['M03_Work_Product']['fields']['create_tds_c']['readonly_formula']='';

 
?>
