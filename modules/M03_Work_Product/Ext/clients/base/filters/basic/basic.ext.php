<?php
// WARNING: The contents of this file are auto-generated.


$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterSameWorkProductPhaseTemplate',
    'name' => 'Filter Same Work Product',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'name' => array(
                '$equals' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);

$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductCodeTemplate',
    'name' => 'Filter Work Product Code',
    'filter_definition' => array(
        array(
            'm03_work_product_code_id1_c' => array(
                '$in' => array('0527341e-d0d4-11e9-8661-06eddc549468','9634c914-fe07-11e6-a663-02feb3423f0d','5e3a105c-1fa3-11eb-9b32-02fb813964b8'),
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);



$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplate',
    'name' => 'Filter Work Product Phase',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'name' => array(
                '$not_equals' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);


$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateLastNotAH01',
    'name' => 'Filter Work Product Phase 2',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'name' => array(
                '$equals' => 'APS001-AH01',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);


$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateLastNotAH01_In_AH15',
    'name' => 'Filter Work Product Phase NotAH01_In_AH15',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'name' => array(
                '$equals' => 'APS177-AH15',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);


$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateLastNotAH01_OnStudy_In_AH15',
    'name' => 'Filter Work Product Phase NotAH01_OnStudy_In_AH15',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'name' => array(
                '$equals' => 'APS177-AH15',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);


$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateNotAH01',
    'name' => 'Filter Work Product Phase 2',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);


$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateNotAH15',
    'name' => 'Filter Work Product Phase NotAH15',
    'filter_definition' => array(
        array(
            'work_product_code_2_c' => array(
              '$in' => array('426d29a9-0810-54b0-1834-5788f6505070','c81c2577-bd7c-16e3-1248-5785ac111a73','f6b39288-96f1-11e7-b45e-02feb3423f0d','00535bde-96f2-11e7-a240-02feb3423f0d'),
            ),
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'name' => array(
                '$not_equals' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);


$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateTestSystem',
    'name' => 'Filter Work Product Phase Test System',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
            'accounts_m03_work_product_1_name' => array(
                '$in' => '',
            ),
            'name' => array(
                '$not_equals' => 'APS177-AH15',
            ),
            
        ),
       
    ),
    'editable' => true,
    'is_template' => true,
);

$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateTestSystemAPS',
    'name' => 'Filter Work Product Phase Test System APS',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
            'accounts_m03_work_product_1_name' => array(
                '$in' => '',
            ),
        ),
       
    ),
    'editable' => true,
    'is_template' => true,
);

$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateTestSystems_In_AH15',
    'name' => 'Filter Work Product Phase Test System_In_AH15',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
            'accounts_m03_work_product_1_name' => array(
                '$in' => '',
            ),
            'name' => array(
                '$not_equals' => 'APS001-AH01',
            ),
            
        ),
       
    ),
    'editable' => true,
    'is_template' => true,
);

$viewdefs['M03_Work_Product']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterWorkProductPhaseTemplateTestSystems_In_APS',
    'name' => 'Filter Work Product Phase Test System_In_APS',
    'filter_definition' => array(
        array(
            'work_product_status_c' => array(
                '$in' => 'Testing',
            ),
            'iacuc_closure_date_c' => array(
                '$empty' => '',
            ),
            'primary_animals_countdown_c' => array(
                '$gt' => '0',
            ),
            'accounts_m03_work_product_1_name' => array(
                '$in' => '',
            ),
        ),
       
    ),
    'editable' => true,
    'is_template' => true,
);