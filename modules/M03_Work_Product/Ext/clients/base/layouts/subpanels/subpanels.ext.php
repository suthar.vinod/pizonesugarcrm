<?php
// WARNING: The contents of this file are auto-generated.


// created: 2017-06-07 19:01:24
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  ),
);

// created: 2017-02-17 17:50:46
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_ABC12_WORK_PRODUCT_ACTIVITIES_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_abc12_work_product_activities_1',
  ),
);

// created: 2019-08-05 12:02:55
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CALLS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_activities_1_calls',
  ),
);

// created: 2019-08-05 12:04:52
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_EMAILS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_activities_1_emails',
  ),
);

// created: 2019-08-05 12:03:27
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_MEETINGS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_activities_1_meetings',
  ),
);

// created: 2019-08-05 12:03:53
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_NOTES_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_activities_1_notes',
  ),
);

// created: 2019-08-05 12:04:19
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TASKS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_activities_1_tasks',
  ),
);

// created: 2019-08-27 11:37:03
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_edoc_email_documents_1',
  ),
);

// created: 2019-11-05 12:53:29
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_erd_error_documents_1',
  ),
);

// created: 2021-08-14 05:59:50
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_gd_group_design_1',
  ),
);

// created: 2021-11-09 10:26:31
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_ic_inventory_collection_1',
  ),
);

// created: 2021-11-09 10:04:52
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_ii_inventory_item_1',
  ),
);

// created: 2021-11-09 10:07:50
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_II_INVENTORY_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_ii_inventory_item_2',
  ),
);

// created: 2021-11-09 10:14:52
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_im_inventory_management_1',
  ),
);

// created: 2021-01-21 14:15:48
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_m03_work_product_code_1',
  ),
);

// created: 2016-02-01 20:37:08
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_m03_work_product_deliverable_1',
  ),
);

// created: 2016-01-25 02:57:18
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_FACILITY_1_FROM_M03_WORK_PRODUCT_FACILITY_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_m03_work_product_facility_1',
  ),
);

// created: 2017-05-02 19:55:40
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_meetings_1',
  ),
);

// created: 2021-10-19 10:30:05
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_ori_order_request_item_1',
  ),
);

// created: 2021-10-19 11:00:50
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_poi_purchase_order_item_1',
  ),
);

// created: 2018-10-09 16:44:23
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_SW_STUDY_WORKFLOW_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_sw_study_workflow_1',
  ),
);

// created: 2021-08-14 06:01:53
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_taskd_task_design_1',
  ),
);

// created: 2016-02-25 20:01:03
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_TASKS_1_FROM_TASKS_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_tasks_1',
  ),
);

// created: 2021-02-11 08:58:28
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_TSD1_TEST_SYSTEM_DESIGN_1_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_tsd1_test_system_design_1_1',
  ),
);

// created: 2017-09-12 15:10:30
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_wpe_work_product_enrollment_1',
  ),
);

// created: 2020-12-15 06:35:00
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'context' => 
  array (
    'link' => 'm03_work_product_wpe_work_product_enrollment_2',
  ),
);

// created: 2018-01-31 15:57:04
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M06_ERROR_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_m03_work_product_1',
  ),
);

// created: 2018-02-05 14:21:02
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M99_PROTOCOL_AMENDMENTS_M03_WORK_PRODUCT_FROM_M99_PROTOCOL_AMENDMENTS_TITLE',
  'context' => 
  array (
    'link' => 'm99_protocol_amendments_m03_work_product',
  ),
);

// created: 2021-04-01 09:05:41
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_MEETINGS_TITLE',
  'context' => 
  array (
    'link' => 'meetings_m03_work_product_1',
  ),
);

// created: 2019-07-09 11:53:38
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_RR_REGULATORY_RESPONSE_TITLE',
  'context' => 
  array (
    'link' => 'rr_regulatory_response_m03_work_product_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_a1a_critical_phase_inspectio_1',
  'view' => 'subpanel-for-m03_work_product-m03_work_product_a1a_critical_phase_inspectio_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_abc12_work_product_activities_1',
  'view' => 'subpanel-for-m03_work_product-m03_work_product_abc12_work_product_activities_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_edoc_email_documents_1',
  'view' => 'subpanel-for-m03_work_product-m03_work_product_edoc_email_documents_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_erd_error_documents_1',
  'view' => 'subpanel-for-m03_work_product-m03_work_product_erd_error_documents_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_gd_group_design_1',
  'view' => 'subpanel-for-m03_work_product-m03_work_product_gd_group_design_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_m03_work_product_deliverable_1',
  'view' => 'subpanel-for-m03_work_product-m03_work_product_m03_work_product_deliverable_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_meetings_1',
  'view' => 'subpanel-for-m03_work_product-m03_work_product_meetings_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_sw_study_workflow_1',
  'view' => 'subpanel-for-m03_work_product-m03_work_product_sw_study_workflow_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_taskd_task_design_1',
  'view' => 'subpanel-for-m03_work_product-m03_work_product_taskd_task_design_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_tsd1_test_system_design_1_1',
  'view' => 'subpanel-for-m03_work_product-m03_work_product_tsd1_test_system_design_1_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_wpe_work_product_enrollment_1',
  'view' => 'subpanel-for-m03_work_product-m03_work_product_wpe_work_product_enrollment_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm03_work_product_wpe_work_product_enrollment_2',
  'view' => 'subpanel-for-m03_work_product-m03_work_product_wpe_work_product_enrollment_2',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm06_error_m03_work_product_1',
  'view' => 'subpanel-for-m03_work_product-m06_error_m03_work_product_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['M03_Work_Product']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'meetings_m03_work_product_1',
  'view' => 'subpanel-for-m03_work_product-meetings_m03_work_product_1',
);
