<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Dependencies/required_final_report_timeline_field.php

$dependencies['M03_Work_Product']['final_report_timeline_required123'] = array(
    'hooks'         => array("view", "edit"),
    'trigger'       => 'true',
    'triggerFields' => array('date_entered','id','functional_area_c','m01_sales_m03_work_product_1','final_report_timeline_c','report_timeline_type_c','extraction_start_integer_c','extraction_end_integer_c','integer_units_c'),
	'onload'        => true,
    'actions'       => array(
        array( 
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'final_report_timeline_c',
                 'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),and(equal($functional_area_c,"Standard Biocompatibility"),or(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"Yes"),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),""))))',
            ),
        ), 
        array( 
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'report_timeline_type_c',
                 'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),and(equal($functional_area_c,"Standard Biocompatibility"),or(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"Yes"),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),""))))',
            ),
        ),
        array( 
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'extraction_start_integer_c',
                 'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),and(equal($functional_area_c,"Standard Biocompatibility"),or(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"Yes"),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),""))))',
            ),
        ),
        array( 
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'extraction_end_integer_c',
                 'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),and(equal($functional_area_c,"Standard Biocompatibility"),or(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"Yes"),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),""))))',
            ),
        ),
        array( 
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'integer_units_c',
                 'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-12-07"))),and(equal($functional_area_c,"Standard Biocompatibility"),or(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"Yes"),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),""))))',
            ),
        ),
    ),
);
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Dependencies/required_new_fields.php

/* $dependencies['M03_Work_Product']['new_fields_required1'] = array(
    'hooks' => array("edit", "view"),
    'trigger' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-10-04"))),or(equal(related($m01_sales_m03_work_product_1,"action_needed_c"),"NW"),equal(related($m01_sales_m03_work_product_1,"action_needed_c"),"Not Applicable")))',
    'triggerFields' => array('m01_sales_m03_work_product_1', 'action_needed_c', 'date_entered', 'id','required_in_lims_c','namsa_submission_id_c','final_report_uploaded_nw_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'required_in_lims_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'namsa_submission_id_c',
                'value' => 'true',
            ),
        ),
    ),
); */

/* $dependencies['M03_Work_Product']['required_1'] = array(
    'hooks' => array("view", "edit"),
    'trigger' => 'true',
    'triggerFields' => array('namsa_test_code_c', 'm01_sales_m03_work_product_1'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'namsa_test_code_c',
                'value' => 'and(or(equal($date_entered,""),isAfter($date_entered,date("2021-10-04"))),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"No"))',
            ),
        ),
    ),
); */


?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Dependencies/extract_fields.php

 
$dependencies['M03_Work_Product']['required_fields323'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('work_product_code_2_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'extraction_conditions_c',
                'value' => 'isInList($work_product_code_2_c, createList("SE01","SE01a","SE03","SE03a","SE09","SE12","SE12a","IR01","IR01a","IR12","IR12a","IR19","IR21","IR21a","IR29","IR31","IR31a","IR39","ST01","ST01a","ST09","ST10","ST10a","ST19","ST22","ST22a","ST23","ST23a","ST24","ST24a","ST25","ST25a","ST29","GE01","GE01a","GE09","GE10","GE10a","GE11","GE11a","GE12","GE12a","GE16","GE16a","GE19","GE30","GE30a","GE39","HE01","HE01a","HE03","HE03a","HE04","HE04a","HE09"))',
            ),
        ),  
    ),
);
$dependencies['M03_Work_Product']['required_fields32354'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('work_product_code_2_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'extraction_conditions_c',
                'value' => 'isInList($work_product_code_2_c, createList("SE01","SE01a","SE03","SE03a","SE09","SE12","SE12a","IR01","IR01a","IR12","IR12a","IR19","IR21","IR21a","IR29","IR31","IR31a","IR39","ST01","ST01a","ST09","ST10","ST10a","ST19","ST22","ST22a","ST23","ST23a","ST24","ST24a","ST25","ST25a","ST29","GE01","GE01a","GE09","GE10","GE10a","GE11","GE11a","GE12","GE12a","GE16","GE16a","GE19","GE30","GE30a","GE39","HE01","HE01a","HE03","HE03a","HE04","HE04a","HE09"))',
            ),
        ),  
    ),
);



?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Dependencies/task_design_relationship_dep.php


$dependencies['TaskD_Task_Design']['test_system_require_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'anml_animals_taskd_task_design_1_name',
                'value' => 'equal($type_2,"Actual")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'gd_group_design_taskd_task_design_1_name',
                'value' => 'equal($type_2,"Plan")',
            ),
        ),
    ),
);


$dependencies['TaskD_Task_Design']['test_system_visibility_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'anml_animals_taskd_task_design_1_name',
                'value' => 'equal($type_2,"Actual")',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'gd_group_design_taskd_task_design_1_name',
                'value' => 'equal($type_2,"Plan")',
            ),
        ),
    ),
);


/* $dependencies['TaskD_Task_Design']['task_design_require_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('relative'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1_name',
                'value' => 'or(equal($relative,"1st Tier"),equal($relative,"2nd Tier"))',
            ),
        ),
    ),
); */


$dependencies['TaskD_Task_Design']['task_design_visibility_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('relative'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1_name',
                'value' => 'or(equal($relative,"1st Tier"),equal($relative,"2nd Tier"))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1_name',
                'value' => 'or(equal($relative,"1st Tier"),equal($relative,"2nd Tier"))',
            ),
        ),
    ),
);


$dependencies['TaskD_Task_Design']['task_design_set_val01'] = array(
    'hooks' => array("all"),
    'trigger' => 'not(or(equal($relative,"1st Tier"),equal($relative,"2nd Tier")))',
    'triggerFields' => array('relative'),
    'onload' => true,
    'actions' => array( 
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
                'value' => '',
            ),
        ),
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1_name',
                'value' => '',
            ),
        ),
    ),
);

$dependencies['TaskD_Task_Design']['work_product_change02'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_taskd_task_design_1_name'),
    'onload' => false,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1taskd_task_design_ida',
                'value' => '',
            ),
        ),
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'taskd_task_design_taskd_task_design_1_name',
                'value' => '',
            ),
        ),
        
    ),
);


/*$dependencies['TaskD_Task_Design']['work_product_relationship_reuired01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_taskd_task_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => 'true',
            ),
        ),   
    ),
);*/
/*1490 : 19 Aug 2021 */

/* $dependencies['TaskD_Task_Design']['work_product_relationship_reuired01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => 'or(equal($type_2,"Plan"),equal($type_2,"Actual"),equal($type_2,"Actual SP"))',
            ),
        ),
    ),
);

$dependencies['TaskD_Task_Design']['work_product_relationship_visibility'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => 'or(equal($type_2,"Plan"),equal($type_2,"Actual"),equal($type_2,"Actual SP"))',
            ),
        ),
    ),
); */
/***/// 1490 end */

/*1491 : 19 Aug 2021 */

$dependencies['TaskD_Task_Design']['m03_work_product_code_taskd_task_design_1_name_reuired01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_code_taskd_task_design_1_name',
                'value' => 'or(equal($type_2,"Plan SP"))',
            ),
        ),
    ),
);

$dependencies['TaskD_Task_Design']['m03_work_product_code_taskd_task_design_1_name_visibility'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_code_taskd_task_design_1_name',
                'value' => 'or(equal($type_2,"Plan SP"))',
            ),
        ),
    ),
);

$dependencies['TaskD_Task_Design']['m03_work_product_code_taskd_task_design_1_name_setnull'] = array(
    'hooks' => array("all"),
    'trigger' => 'or(equal($type_2,"Plan"),equal($type_2,"Actual"),equal($type_2,"Actual SP"))',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'm03_work_product_code_taskd_task_design_1_name',
                'value' => '',
            ),
        ),
    ),
);
/**/// 1491 end */

$dependencies['TaskD_Task_Design']['work_product_relationship_setnull'] = array(
    'hooks' => array("all"),
    'trigger' => 'or(equal($type_2,"Plan SP"))',
    'triggerFields' => array('type_2'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => '',
            ),
        ),
    ),
);




/* 792: Task Design - Type field */
$dependencies['TaskD_Task_Design']['read_only_type_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('gd_group_design_taskd_task_design_1_name','anml_animals_taskd_task_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'type_2',
                'value' => 'or(greaterThan(strlen(related($anml_animals_taskd_task_design_1,"name")),0),greaterThan(strlen(related($gd_group_design_taskd_task_design_1,"name")),0))',
            ),
        ),
    ),
);

/* 1489: Task Design - Type field */
$dependencies['TaskD_Task_Design']['read_only_plan_type_dep01'] = array(
    'hooks' => array("save"),
    'trigger' => 'ifElse(not(equal(related($gd_group_design_taskd_task_design_1,"name"),"")),"true","false")',
    'triggerFields' => array('gd_group_design_taskd_task_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'type_2',
                'value' => 'true',
            ),
        ),
    ),
); 
$dependencies['TaskD_Task_Design']['read_only_plan_type_dep02'] = array(
    'hooks' => array("save"),
    'trigger' => 'ifElse(not(equal(related($anml_animals_taskd_task_design_1,"name"),"")),"true","false")',
    'triggerFields' => array('anml_animals_taskd_task_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'type_2',
                'value' => 'true',
            ),
        ),
    ),
); 
$dependencies['TaskD_Task_Design']['read_only_plan_type_dep02'] = array(
    'hooks' => array("save"),
    'trigger' => 'ifElse(not(equal(related($m03_work_product_code_taskd_task_design_1,"name"),"")),"true","false")',
    'triggerFields' => array('m03_work_product_code_taskd_task_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'type_2',
                'value' => 'true',
            ),
        ),
    ),
); 
$dependencies['TaskD_Task_Design']['gd_set_null_dep01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2'),
    'onload' => false,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => "gd_group_design_taskd_task_design_1gd_group_design_ida",
                'value' => '',
            ),
        ),
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'gd_group_design_taskd_task_design_1_name',
                'value' => '',
            ),
        ),
        
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => "anml_animals_taskd_task_design_1anml_animals_ida",
                'value' => '',
            ),
        ),
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'anml_animals_taskd_task_design_1_name',
                'value' => '',
            ),
        ),
    ),
);

$dependencies['TaskD_Task_Design']['actual_date_readonly'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2','relative'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'actual_datetime',
                'value' => 'and(equal($type_2,"Actual SP"),equal($relative,"NA"))',
            ),
        ),
    ),
);
/*2313 : 04 April 2022 */
/*
$dependencies['TaskD_Task_Design']['work_product_relationship_reuired01'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type_2','m03_work_product_taskd_task_design_1_name','bid_batch_id_taskd_task_design_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => 'or(or(equal($type_2,"Plan"),equal($type_2,"Actual")),and(equal($type_2,"Actual SP"),equal($bid_batch_id_taskd_task_design_1_name,"")))',
            ),
        ),
    ),
);
*/
$dependencies['TaskD_Task_Design']['WP_Visibility_dep43324'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_taskd_task_design_1_name','bid_batch_id_taskd_task_design_1_name'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => 'or(and(equal($bid_batch_id_taskd_task_design_1_name,""),not(equal($m03_work_product_taskd_task_design_1_name,""))),and(equal($bid_batch_id_taskd_task_design_1_name,""),equal($m03_work_product_taskd_task_design_1_name,"")))',
            ),
        ),
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'bid_batch_id_taskd_task_design_1_name',
                'value' => 'or(and(equal($m03_work_product_taskd_task_design_1_name,""),not(equal($bid_batch_id_taskd_task_design_1_name,""))),and(equal($m03_work_product_taskd_task_design_1_name,""),equal($bid_batch_id_taskd_task_design_1_name,"")))',
            ),
        ),
    ),
);
$dependencies['TaskD_Task_Design']['task_design_required_field'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('m03_work_product_taskd_task_design_1_name','bid_batch_id_taskd_task_design_1_name'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(        
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'm03_work_product_taskd_task_design_1_name',
                'value' => 'or(and(equal($bid_batch_id_taskd_task_design_1_name,""),not(equal($m03_work_product_taskd_task_design_1_name,""))),and(equal($bid_batch_id_taskd_task_design_1_name,""),equal($m03_work_product_taskd_task_design_1_name,"")))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'bid_batch_id_taskd_task_design_1_name',
                'value' => 'or(and(equal($m03_work_product_taskd_task_design_1_name,""),not(equal($bid_batch_id_taskd_task_design_1_name,""))),and(equal($m03_work_product_taskd_task_design_1_name,""),equal($bid_batch_id_taskd_task_design_1_name,"")))',
            ),
        ),
    ),
);
/***/// 2313 end */

$dependencies['TaskD_Task_Design']['days_from_initial_task_dep'] = array(
    'hooks' => array("all"), //not including save so that the value isn't stored in the DB
    'trigger' => 'true', //Optional, the trigger for the dependency. Defaults to 'true'.
    //'triggerFields' => array('status'), //unneeded for this example as its not field triggered
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetValue',
            'params' => array(
                'target' => 'days_from_initial_task',
                'value' => 'ifElse(equal($actual_datetime,""),"",abs(subtract(daysUntil(related($taskd_task_design_taskd_task_design_1_right,"actual_datetime")),daysUntil($actual_datetime))))'
            ),
        ),
    ),
);
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Dependencies/required_fields.php

$dependencies['M03_Work_Product']['setfirst_readonly2'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('first_procedure_c','bid_batch_id_m03_work_product_1_name'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'first_procedure_c',
                'value'  => 'ifElse(not(equal($bid_batch_id_m03_work_product_1_name,"")),true,false)',
            ),
        ),
    ),
); 
$dependencies['M03_Work_Product']['required_fields'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('work_product_status_c', 'work_product_compliance_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'name',
                'value' => 'isInList($work_product_status_c, createList("Testing","Archived"))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'master_schedule_sd_c',
                'value' => 'and(isInList($work_product_compliance_c,createList("GLP","Choose Compliance","GLP Not Performed","GLP Discontinued","Withdrawn","GLP_Amended_NonGLP")),isInList($work_product_status_c,createList("Testing","Archived")))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'master_schedule_company_name_c',
                'value' => 'and(isInList($work_product_compliance_c,createList("GLP","Choose Compliance","GLP Not Performed","GLP Discontinued","Withdrawn","GLP_Amended_NonGLP")),isInList($work_product_status_c,createList("Testing","Archived")))',
            ),
        ),
    ),
);

/*22 oct :584: analytical_deliverables field conditional required Bug #1868*/
$dependencies['M03_Work_Product']['analytical_deliverables_required'] = array(
    'hooks'         => array("view", "edit","save","all"),
    'trigger'       => 'true',
    'triggerFields' => array('date_entered','id','functional_area_c','m01_sales_m03_work_product_1','analytical_deliverables_c'),
	'onload'        => true,
    'actions'       => array(
        array( 
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'analytical_deliverables_c',
                'value' => 'ifElse(and(not(equal($functional_area_c,"Analytical Services")),isAfter($date_entered,date("2020-10-27")),or(equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),"Yes"),equal(related($m01_sales_m03_work_product_1,"namsa_bde_referred_c"),""))),true,false)',
            ),
        ), 
    ),
);


$dependencies['M03_Work_Product']['set_blanket_readonly'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('work_product_code_2_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetVisibility',
            'params' => array(
                'target' => 'blanket_animals_remaining_c',
                'value' => 'or(equal($work_product_code_2_c,"BCBLK"),equal($work_product_code_2_c,"PHBLK"))',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'blanket_animals_remaining_c',
                'value' => 'or(equal($work_product_code_2_c,"BCBLK"),equal($work_product_code_2_c,"PHBLK"))',
            ),
        ),
    ),
); 
 

/* Change By Laxmichand 03 December 2020 */

global $current_user,$db;
$sql = "select * from acl_roles_users where role_id='fc0f19ea-41c8-11e9-bc3b-067d538c0c10' AND user_id='$current_user->id' AND deleted=0";
$result = $db->query($sql);
if($result->num_rows == 0){

    $dependencies['M03_Work_Product']['wp_fields_work_product_status_c_dp'] = array(
        'hooks' => array("edit"),
        'trigger' => 'true',
        'triggerFields' => array('id'),
        'onload' => true,
        'actions' => array(
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'work_product_status_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
        ),
    );

    $dependencies['M03_Work_Product']['wp_fields_dp'] = array(
        'hooks' => array("edit"),
        'trigger' => 'true',
        'triggerFields' => array('work_product_status_c','name','work_product_compliance_c','master_schedule_company_name_c','study_director_initiation_c','master_schedule_sd_c','test_article_description_c','test_article_name_c','test_system_c','title_description_c'),
        'onload' => true,
        //Actions is a list of actions to fire when the trigger is true
        'actions' => array(
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'name',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'work_product_compliance_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'master_schedule_company_name_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'study_director_initiation_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'master_schedule_sd_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'test_article_description_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'test_article_name_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'test_system_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                'params' => array(
                    'target' => 'title_description_c',
                    'value' => 'equal($work_product_status_c, "Archived")',
                ),
            ),
        ),
    );
}

/*11 Feb 2021 : #855 */
$dependencies['M03_Work_Product']['required_quoted_protocol_hours_c'] = array(
    'hooks' => array("all") ,
    'trigger' => 'true',
    'triggerFields' => array('functional_area_c','date_entered','id','quoted_protocol_hours_c','quoted_study_management_hrs_c','quoted_reporting_hours_c') ,
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'quoted_protocol_hours_c',
                //'value' => 'and(equal($functional_area_c,"ISR"),isAfter($date_entered,date("2021-02-16")))', 
                'value' => 'or(equal($id,""),and(isAfter($date_entered,date("2021-02-16")),equal($functional_area_c,"ISR")))',              
            ) ,
        ) ,
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'quoted_study_management_hrs_c',
                //'value' => 'and(equal($functional_area_c,"ISR"),isAfter($date_entered,date("2021-02-16")))', 
                'value' => 'or(equal($id,""),and(isAfter($date_entered,date("2021-02-16")),equal($functional_area_c,"ISR")))',
            ) ,
        ) ,
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'quoted_reporting_hours_c',
                //'value' => 'and(equal($functional_area_c,"ISR"),isAfter($date_entered,date("2021-02-16")))', 
                'value' => 'or(equal($id,""),and(isAfter($date_entered,date("2021-02-16")),equal($functional_area_c,"ISR")))',
            ) ,
        ) ,
    ) ,
);


$dependencies['M03_Work_Product']['wpc_required_dep_2'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('work_product_status_c','work_product_code_2_c','number_of_peaks_gcms_c','number_of_peaks_lcms_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'number_of_peaks_gcms_c',
                'value' => 'ifElse(and(
                    equal($work_product_status_c, "Archived"),
                    or(
                        equal($work_product_code_2_c, "AS71"),equal($work_product_code_2_c, "AS72"),equal($work_product_code_2_c, "AS79")
                      )        
                        ),true,false)',
            ),
        ),
        array(
            'name' => 'SetRequired',
            'params' => array(
                'target' => 'number_of_peaks_lcms_c',
                'value' => 'ifElse(and(
                    equal($work_product_status_c, "Archived"),
                    or(
                        equal($work_product_code_2_c, "AS71"),equal($work_product_code_2_c, "AS72"),equal($work_product_code_2_c, "AS79")
                      )        
                        ),true,false)',
            ),
        ),
    ),
);


?>
