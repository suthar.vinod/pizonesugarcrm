<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.Protocol_Amendments.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M99_PROTOCOL_AMENDMENTS_M03_WORK_PRODUCT_FROM_M99_PROTOCOL_AMENDMENTS_TITLE'] = 'Protocol Amendments';
$mod_strings['LBL_M99_PROTOCOL_AMENDMENTS_M03_WORK_PRODUCT_FROM_M03_WORK_PRODUCT_TITLE'] = 'Protocol Amendments';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_m03_additional_wp_ids_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M03_ADDITIONAL_WP_IDS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_ADDITIONAL_WP_IDS_1_FROM_M03_ADDITIONAL_WP_IDS_TITLE'] = 'Additional WP IDs';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_m03_work_product_facility_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_FACILITY_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_FACILITY_1_FROM_M03_WORK_PRODUCT_FACILITY_TITLE'] = 'Work Product Facilities';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_m03_additional_wp_personnel_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M03_ADDITIONAL_WP_PERSONNEL_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_ADDITIONAL_WP_PERSONNEL_1_FROM_M03_ADDITIONAL_WP_PERSONNEL_TITLE'] = 'Additional WP Personnel';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M01_SALES_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm01_sales_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'Sales ID';
$mod_strings['LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_m03_work_product_deliverable_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm01_quote_document_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote Documents';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'M01_Quote_Document ID';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'SA Quote Documents';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm01_quote_document_m03_work_product_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_2_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote Documents';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'SA Quote Documents ID';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'SA Quote Documents';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_tasks_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_TASKS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_TASKS_1_FROM_TASKS_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customtasks_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'Tasks ID';
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_deliverable_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'Work Product Deliverables ID';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Product Deliverables';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_z1_animals_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_Z1_ANIMALS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_Z1_ANIMALS_1_FROM_Z1_ANIMALS_TITLE'] = 'Animals';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_abc12_work_product_activities_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_ABC12_WORK_PRODUCT_ACTIVITIES_TITLE'] = 'Work Product Activities';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_meetings_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Activities';
$mod_strings['LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Activities';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_a1a_critical_phase_inspectio_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE'] = 'Critical Phase Inspections';
$mod_strings['LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Critical Phase Inspections';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_wpe_work_product_enrollment_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Product Enrollment';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Product Enrollment';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm06_error_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M06_ERROR_TITLE'] = 'Errors';
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'Errors ID';
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Errors';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_sw_study_workflow_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_SW_STUDY_WORKFLOW_TITLE'] = 'Study Workflow';
$mod_strings['LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Study Workflow';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customcontacts_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Contacts';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customaccounts_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'Companies ID';
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Companies';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customrr_regulatory_response_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_RR_REGULATORY_RESPONSE_TITLE'] = 'Regulatory Responses';
$mod_strings['LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Regulatory Responses';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_activities_1_calls.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_CALLS_FROM_CALLS_TITLE'] = 'Calls';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_activities_1_meetings.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE'] = 'Communications';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_activities_1_notes.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_NOTES_FROM_NOTES_TITLE'] = 'Notes';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_activities_1_tasks.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_TASKS_FROM_TASKS_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_activities_1_emails.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_EMAILS_FROM_EMAILS_TITLE'] = 'Emails';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_edoc_email_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE'] = 'Email Documents';
$mod_strings['LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Email Documents';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_erd_error_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE'] = 'Controlled Documents';
$mod_strings['LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Controlled Documents';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_wpe_work_product_enrollment_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Product Enrollment';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Product Enrollment';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_m03_work_product_code_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE'] = 'Work Product Codes';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Product Codes';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_tsd1_test_system_design_1_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_TSD1_TEST_SYSTEM_DESIGN_1_TITLE'] = 'Test System Designs';
$mod_strings['LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Test System Designs';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.custommeetings_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_MEETINGS_TITLE'] = 'APS Activities (BC Panels Only)';
$mod_strings['LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'APS Activities (BC Panels Only)';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_gd_group_design_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE'] = 'Group Designs';
$mod_strings['LBL_M03_WORK_PRODUCT_GD_GROUP_DESIGN_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Group Designs';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_taskd_task_design_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE'] = 'Task Designs';
$mod_strings['LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Task Designs';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_ori_order_request_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Order Request Items';
$mod_strings['LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Order Request Items';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_poi_purchase_order_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE'] = 'Purchase Order Items';
$mod_strings['LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Purchase Order Items';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Inventory Items';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_ii_inventory_item_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'Inventory Items';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_im_inventory_management_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE'] = 'Inventory Management';
$mod_strings['LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Inventory Management';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customm03_work_product_ic_inventory_collection_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE'] = 'Inventory Collections';
$mod_strings['LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Inventory Collections';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.custombid_batch_id_m03_work_product_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_BID_BATCH_ID_TITLE'] = 'Batch IDs';
$mod_strings['LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Batch IDs';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/lt_LT.customcontacts_m03_work_product_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'Contacts';

?>
