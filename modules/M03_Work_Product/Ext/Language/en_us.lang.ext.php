<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.Protocol_Amendments.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M99_PROTOCOL_AMENDMENTS_M03_WORK_PRODUCT_FROM_M99_PROTOCOL_AMENDMENTS_TITLE'] = 'Protocol Amendments';
$mod_strings['LBL_M99_PROTOCOL_AMENDMENTS_M03_WORK_PRODUCT_FROM_M03_WORK_PRODUCT_TITLE'] = 'Protocol Amendments';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us..php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_RECORD_BODY'] = 'WP Description';
$mod_strings['LBL_SHOW_MORE'] = 'Details';
$mod_strings['LBL_SPONSOR'] = 'Company Name';
$mod_strings['LBL_SPONSOR_CONTACT'] = 'Company Contact';
$mod_strings['LBL_TITLE_DESCRIPTION'] = 'Work Product Title / Description';
$mod_strings['LBL_WORK_PRODUCT_CODE'] = 'Work Product Code (relate 2)';
$mod_strings['LBL_WORK_PRODUCT_COMPLIANCE'] = 'Work Product Compliance';
$mod_strings['LBL_WORK_PRODUCT_ID_APS'] = 'Work Product ID (APS)';
$mod_strings['LBL_WORK_PRODUCT_STATUS'] = 'Work Product Phase';
$mod_strings['LBL_WORK_PRODUCT_ID_SPONSOR'] = 'Work Product ID (Sponsor)';
$mod_strings['LBL_ASSIGNED_TO'] = 'Assigned to';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'IACUC ';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Contacts';
$mod_strings['LBL_NAME'] = 'APS Work Product ID';
$mod_strings['LBL_ASSIGNED_TO_NAME'] = 'APS Study Director / PI';
$mod_strings['LBL_WORK_PRODUCT_CODE_2_M03_WORK_PRODUCT_CODE_ID'] = 'Work Product Code 2 (related Work Product Code ID)';
$mod_strings['LBL_WORK_PRODUCT_CODE_2'] = 'Work Product Code';
$mod_strings['LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE'] = 'Sales Activity';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_DESCRIPTION'] = 'Work Product Notes';
$mod_strings['LBL_COMPANY_ACCOUNT_ID'] = 'Sponsor (related Company ID)';
$mod_strings['LBL_COMPANY'] = 'Sponsor 2';
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE'] = 'Company';
$mod_strings['LBL_STUDY_DIRECTOR'] = 'Study Director (Pre-Script)';
$mod_strings['LBL_STUDY_PATHOLOGIST'] = 'Study Pathologist';
$mod_strings['LBL_LEAD_AUDITOR'] = 'Lead Auditor';
$mod_strings['LBL_STUDY_DIRECTOR_INITIATION'] = 'Study Director Initiation Date';
$mod_strings['LBL_TEST_ARTICLE_DESCRIPTION'] = 'Test Article Description';
$mod_strings['LBL_DEPARTMENT'] = 'Department';
$mod_strings['LBL_DIVISION'] = 'Division';
$mod_strings['LBL_ARCHIVED_DATE'] = 'Archived Date';
$mod_strings['LBL_LOCATION'] = 'Location';
$mod_strings['LBL_REGULATORY_STANDARD'] = 'Regulatory Standard';
$mod_strings['LBL_VETERINARIAN'] = 'Study Veterinarian';
$mod_strings['LBL_APS_PRINCIPAL_INVESTIGATOR'] = 'APS Principal Investigator';
$mod_strings['LBL_IACUC_PROTOCOL_STATUS'] = 'IACUC Protocol Status';
$mod_strings['LBL_IACUC_YEARLY_REVIEW_DUE_DATE'] = 'IACUC Yearly Review Due Date';
$mod_strings['LBL_IACUC_CLOSURE_DATE'] = 'IACUC Closure Date';
$mod_strings['LBL_FIRST_PROCEDURE'] = 'First Procedure';
$mod_strings['LBL_LAST_PROCEDURE'] = 'Last Procedure';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Benchmarks';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Metrics';
$mod_strings['LBL_BC_SPA_RECONCILED_DATE'] = 'BC SPA Reconciled Date';
$mod_strings['LBL_TEST_SYSTEM'] = 'Test System';
$mod_strings['LBL_FUNCTIONAL_AREA'] = 'Functional Area';
$mod_strings['LBL_STUDY_OUTCOME_TYPE'] = 'Study Outcome Type';
$mod_strings['LBL_STUDY_ARTICLE_DISPOSITION'] = 'BC Study Article Disposition';
$mod_strings['LBL_IACUC_THREE_YEAR'] = 'IACUC 3 Year Renewal Date';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_2_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote &amp; Study Documents';
$mod_strings['LBL_TEST_SYSTEM_DESCRIPTION'] = 'Test System Description';
$mod_strings['LBL_DATA_STORAGE_COMMENTS'] = 'Data Storage Comments';
$mod_strings['LBL_CULPRITS_LIST'] = 'Culprits List';
$mod_strings['LBL_STUDY_DATA_LOCATION'] = 'Study Data Location';
$mod_strings['LBL_DATA_RETENTION_START_DATE'] = 'Data Retention Start Date';
$mod_strings['LBL_DATA_RETENTION_END_DATE'] = 'Data Retention End Date';
$mod_strings['LBL_DATA_SCANNED_DATE'] = 'Study Scanned Date';
$mod_strings['LBL_STUDY_ARCHIVED_DATE'] = 'Study Archived Date';
$mod_strings['LBL_SHIPPED_DESTROYED_DATE'] = 'Shipped / Destroyed Date';
$mod_strings['LBL_SPECIMENS'] = 'Specimens';
$mod_strings['LBL_SPECIMEN_LOCATION'] = 'Specimen Location';
$mod_strings['LBL_STUDY_ARTICLE'] = 'Study Article(s)';
$mod_strings['LBL_STUDY_ARTICLES_LOCATION'] = 'Study Article(s) Location';
$mod_strings['LBL_DATA_STORAGE_PHASE'] = 'Data Storage Phase';
$mod_strings['LBL_APPROX_DATA_STORAGE_SIZE'] = 'Approx Data Storage Size';
$mod_strings['LBL_RECORDVIEW_PANEL5'] = 'Archives';
$mod_strings['LBL_EXPANDED_STUDY_RESULT'] = 'Expanded Study Result';
$mod_strings['LBL_BUILDING'] = 'Building';
$mod_strings['LBL_WORK_PRODUCT_COMMENTS'] = 'Work Product Comments';
$mod_strings['LBL_RECORDVIEW_PANEL6'] = 'WP Notes';
$mod_strings['LBL_WPC'] = 'Work Product Code (text)';
$mod_strings['LBL_ANALYTE'] = 'Analyte';
$mod_strings['LBL_MATRIX'] = 'Matrix';
$mod_strings['LBL_INSTRUMENT'] = 'Instrument';
$mod_strings['LBL_RECORDVIEW_PANEL7'] = 'Analytical';
$mod_strings['LBL_COMPANY_STATUS'] = 'WP Account Status';
$mod_strings['LBL_APS_STUDY_ID'] = 'Method Validation Work Product ID (if applicable)';
$mod_strings['LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_ABC12_WORK_PRODUCT_ACTIVITIES_TITLE'] = 'Work Product Activities';
$mod_strings['LBL_PRIMARY_APS_OPERATOR'] = 'Primary APS Operator';
$mod_strings['LBL_RECORDVIEW_PANEL8'] = 'ISR';
$mod_strings['LBL_INITIAL_PROCEDURE_ON_TIME'] = 'Initial Procedure Late Start?';
$mod_strings['LBL_FOLLOWUP_ON_TIME'] = 'Follow-up Late Start?';
$mod_strings['LBL_TERMINAL_ON_TIME'] = 'Terminal Late Start?';
$mod_strings['LBL_LATE_START_DETAILS'] = 'Late Start Details';
$mod_strings['LBL_SECONDARY_APS_OPERATOR'] = 'Secondary APS Operator';
$mod_strings['LBL_STUDY_COORDINATOR'] = 'Study Coordinator';
$mod_strings['LBL_TOTAL_ANIMAL_NUMBERS'] = 'Total Animal Numbers';
$mod_strings['LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'APS Activities';
$mod_strings['LBL_LEAD_QAU_AUDITOR_USER_ID'] = 'Lead QAU Auditor (related User ID)';
$mod_strings['LBL_LEAD_QAU_AUDITOR'] = 'Lead QAU Auditor';
$mod_strings['LBL_EXTRACT_ABNORMALITY'] = 'Extract Abnormality?';
$mod_strings['LBL_DESCRIBE_ABNORMALITY'] = 'Describe Abnormality';
$mod_strings['LBL_STUDY_DURATION'] = 'Study Duration';
$mod_strings['LBL_USDA_EXEMPTION'] = 'USDA Exemption 1';
$mod_strings['LBL_AAALAC_EXEMPTIONS'] = 'AAALAC Exemption 1';
$mod_strings['LBL_HAZARDOUS_SUBSTANCE'] = 'Hazardous Substance';
$mod_strings['LBL_USDA_EXEMPTION_2'] = 'USDA Exemption 2';
$mod_strings['LBL_AAALAC_EXEMPTIONS_2'] = 'AAALAC Exemptions 2';
$mod_strings['LBL_USDA_CATEGORY'] = 'USDA Category';
$mod_strings['LBL_TEST_CONTROL_ARTICLE_CHECKIN'] = 'Test Control Article Check-In';
$mod_strings['LBL_RECORDVIEW_PANEL9'] = 'Study Coordinator Deliverables';
$mod_strings['LBL_CHRONIC_STUDY'] = 'Chronic Study Longer than 28 Days?';
$mod_strings['LBL_DURATION_GREATER_THAN_4_WEEK'] = 'Duration Greater Than 4 weeks?';
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M06_ERROR_TITLE'] = 'Observations';
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE'] = 'Contact';
$mod_strings['LBL_STUDY_DIRECTOR_SCRIPT_CONTACT_ID'] = 'Study Director (Script) (related Contact ID)';
$mod_strings['LBL_STUDY_DIRECTOR_SCRIPT'] = 'Study Director';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_m03_additional_wp_ids_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M03_ADDITIONAL_WP_IDS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_ADDITIONAL_WP_IDS_1_FROM_M03_ADDITIONAL_WP_IDS_TITLE'] = 'Additional WP IDs';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_m03_work_product_facility_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_FACILITY_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_FACILITY_1_FROM_M03_WORK_PRODUCT_FACILITY_TITLE'] = 'Work Product Facilities';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_m03_additional_wp_personnel_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M03_ADDITIONAL_WP_PERSONNEL_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_ADDITIONAL_WP_PERSONNEL_1_FROM_M03_ADDITIONAL_WP_PERSONNEL_TITLE'] = 'Additional WP Personnel';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_m01_sales_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M01_SALES_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M01_SALES_1_FROM_M01_SALES_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm01_sales_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE'] = 'Sales';
$mod_strings['LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'Sales ID';
$mod_strings['LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Sales';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_m03_work_product_deliverable_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm01_quote_document_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_1_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote Documents';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'M01_Quote_Document ID';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'SA Quote Documents';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm01_quote_document_m03_work_product_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_2_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote Documents';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'SA Quote Documents ID';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'SA Quote Documents';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_tasks_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_TASKS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_TASKS_1_FROM_TASKS_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customtasks_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'Tasks ID';
$mod_strings['LBL_TASKS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_deliverable_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'Work Product Deliverables ID';
$mod_strings['LBL_M03_WORK_PRODUCT_DELIVERABLE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Product Deliverables';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_z1_animals_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_Z1_ANIMALS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_Z1_ANIMALS_1_FROM_Z1_ANIMALS_TITLE'] = 'Animals';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_abc12_work_product_activities_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Products';
$mod_strings['LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_ABC12_WORK_PRODUCT_ACTIVITIES_TITLE'] = 'Work Product Activities';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_meetings_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Activities';
$mod_strings['LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'Activities';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_a1a_critical_phase_inspectio_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_A1A_CRITICAL_PHASE_INSPECTIO_TITLE'] = 'Critical Phase Inspections';
$mod_strings['LBL_M03_WORK_PRODUCT_A1A_CRITICAL_PHASE_INSPECTIO_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Critical Phase Inspections';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm06_error_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M06_ERROR_TITLE'] = 'Errors';
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'Errors ID';
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Errors';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_sw_study_workflow_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_SW_STUDY_WORKFLOW_TITLE'] = 'Study Workflow';
$mod_strings['LBL_M03_WORK_PRODUCT_SW_STUDY_WORKFLOW_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Study Workflow';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customcontacts_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Contacts';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customaccounts_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE_ID'] = 'Companies ID';
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Companies';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customrr_regulatory_response_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_RR_REGULATORY_RESPONSE_TITLE'] = 'Regulatory Responses';
$mod_strings['LBL_RR_REGULATORY_RESPONSE_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Regulatory Responses';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_activities_1_calls.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_CALLS_FROM_CALLS_TITLE'] = 'Calls';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_activities_1_meetings.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_MEETINGS_FROM_MEETINGS_TITLE'] = 'Communications';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_activities_1_notes.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_NOTES_FROM_NOTES_TITLE'] = 'Notes';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_activities_1_tasks.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_TASKS_FROM_TASKS_TITLE'] = 'Tasks';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_activities_1_emails.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ACTIVITIES_1_EMAILS_FROM_EMAILS_TITLE'] = 'Emails';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_edoc_email_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_EDOC_EMAIL_DOCUMENTS_TITLE'] = 'Email Documents';
$mod_strings['LBL_M03_WORK_PRODUCT_EDOC_EMAIL_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Email Documents';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_erd_error_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_ERD_ERROR_DOCUMENTS_TITLE'] = 'Controlled Documents';
$mod_strings['LBL_M03_WORK_PRODUCT_ERD_ERROR_DOCUMENTS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Controlled Documents';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_m03_work_product_code_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_CODE_TITLE'] = 'Work Product Codes';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_CODE_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Product Codes';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_tsd1_test_system_design_1_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_TSD1_TEST_SYSTEM_DESIGN_1_TITLE'] = 'Test System Designs';
$mod_strings['LBL_M03_WORK_PRODUCT_TSD1_TEST_SYSTEM_DESIGN_1_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Test System Designs';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.custommeetings_m03_work_product_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_MEETINGS_TITLE'] = 'APS Activities (BC Panels Only)';
$mod_strings['LBL_MEETINGS_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'APS Activities (BC Panels Only)';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_gd_group_design_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_GD_GROUP_DESIGN_1_FROM_GD_GROUP_DESIGN_TITLE'] = 'Group Designs';
$mod_strings['LBL_M03_WORK_PRODUCT_GD_GROUP_DESIGN_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Group Designs';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_taskd_task_design_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE'] = 'Task Designs';
$mod_strings['LBL_M03_WORK_PRODUCT_TASKD_TASK_DESIGN_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Task Designs';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_wpe_work_product_enrollment_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'WPA Blanket';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'WPA Blanket';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_wpe_work_product_enrollment_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'WPA Primary';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'WPA Primary';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_ori_order_request_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE'] = 'Order Request Items';
$mod_strings['LBL_M03_WORK_PRODUCT_ORI_ORDER_REQUEST_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Order Request Items';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_poi_purchase_order_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE'] = 'Purchase Order Items';
$mod_strings['LBL_M03_WORK_PRODUCT_POI_PURCHASE_ORDER_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Purchase Order Items';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Inventory Items';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_ii_inventory_item_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_M03_WORK_PRODUCT_II_INVENTORY_ITEM_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'Inventory Items';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_im_inventory_management_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_IM_INVENTORY_MANAGEMENT_TITLE'] = 'Inventory Management';
$mod_strings['LBL_M03_WORK_PRODUCT_IM_INVENTORY_MANAGEMENT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Inventory Management';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customm03_work_product_ic_inventory_collection_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_IC_INVENTORY_COLLECTION_TITLE'] = 'Inventory Collections';
$mod_strings['LBL_M03_WORK_PRODUCT_IC_INVENTORY_COLLECTION_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Inventory Collections';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.custombid_batch_id_m03_work_product_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_BID_BATCH_ID_TITLE'] = 'Batch IDs';
$mod_strings['LBL_BID_BATCH_ID_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Batch IDs';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.customcontacts_m03_work_product_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_CONTACTS_TITLE'] = 'Contacts';
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'Contacts';

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_RECORD_BODY'] = 'General Info';
$mod_strings['LBL_SHOW_MORE'] = 'Details';
$mod_strings['LBL_SPONSOR'] = 'Company Name';
$mod_strings['LBL_SPONSOR_CONTACT'] = 'Company Contact';
$mod_strings['LBL_TITLE_DESCRIPTION'] = 'Title';
$mod_strings['LBL_WORK_PRODUCT_CODE'] = 'Work Product Code (relate 2)';
$mod_strings['LBL_WORK_PRODUCT_COMPLIANCE'] = 'Work Product Compliance';
$mod_strings['LBL_WORK_PRODUCT_ID_APS'] = 'Work Product ID (APS)';
$mod_strings['LBL_WORK_PRODUCT_STATUS'] = 'Work Product Phase';
$mod_strings['LBL_WORK_PRODUCT_ID_SPONSOR'] = 'Work Product ID (Sponsor)';
$mod_strings['LBL_ASSIGNED_TO'] = 'Assigned to';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'IACUC Info';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Contact Info';
$mod_strings['LBL_NAME'] = 'APS Work Product ID';
$mod_strings['LBL_ASSIGNED_TO_NAME'] = 'APS Study Director / PI';
$mod_strings['LBL_WORK_PRODUCT_CODE_2_M03_WORK_PRODUCT_CODE_ID'] = 'Work Product Code 2 (related Work Product Code ID)';
$mod_strings['LBL_WORK_PRODUCT_CODE_2'] = 'Work Product Code';
$mod_strings['LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE'] = 'Sales Activity';
$mod_strings['LBL_M03_WORK_PRODUCT_M03_WORK_PRODUCT_DELIVERABLE_1_FROM_M03_WORK_PRODUCT_DELIVERABLE_TITLE'] = 'Work Product Deliverables';
$mod_strings['LBL_DESCRIPTION'] = 'Work Product Notes';
$mod_strings['LBL_COMPANY'] = 'Sponsor 2';
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE'] = 'Sponsor';
$mod_strings['LBL_STUDY_DIRECTOR'] = 'Study Director (Pre-Script)';
$mod_strings['LBL_STUDY_PATHOLOGIST'] = 'Study Pathologist';
$mod_strings['LBL_LEAD_AUDITOR'] = 'Lead Auditor old';
$mod_strings['LBL_STUDY_DIRECTOR_INITIATION'] = 'Study Director Initiation Date';
$mod_strings['LBL_TEST_ARTICLE_DESCRIPTION'] = 'Test Article Name (Obsolete)';
$mod_strings['LBL_DEPARTMENT'] = 'Department';
$mod_strings['LBL_DIVISION'] = 'Division';
$mod_strings['LBL_ARCHIVED_DATE'] = 'Archived Date';
$mod_strings['LBL_LOCATION'] = 'Location';
$mod_strings['LBL_REGULATORY_STANDARD'] = 'Regulatory Standard';
$mod_strings['LBL_VETERINARIAN'] = 'Study Veterinarian';
$mod_strings['LBL_APS_PRINCIPAL_INVESTIGATOR'] = 'APS Principal Investigator old';
$mod_strings['LBL_IACUC_PROTOCOL_STATUS'] = 'IACUC Protocol Status';
$mod_strings['LBL_IACUC_YEARLY_REVIEW_DUE_DATE'] = 'IACUC Yearly Review Due Date';
$mod_strings['LBL_IACUC_CLOSURE_DATE'] = 'IACUC Closure Date';
$mod_strings['LBL_FIRST_PROCEDURE'] = 'First Procedure';
$mod_strings['LBL_LAST_PROCEDURE'] = 'Last Procedure';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Benchmark Info';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Metrics';
$mod_strings['LBL_BC_SPA_RECONCILED_DATE'] = 'SPA Reconciled Date';
$mod_strings['LBL_TEST_SYSTEM'] = 'Test System';
$mod_strings['LBL_FUNCTIONAL_AREA'] = 'Functional Area';
$mod_strings['LBL_STUDY_OUTCOME_TYPE'] = 'Study Outcome Type';
$mod_strings['LBL_STUDY_ARTICLE_DISPOSITION'] = 'Study Article Disposition';
$mod_strings['LBL_IACUC_THREE_YEAR'] = 'IACUC 3 Year Renewal Date';
$mod_strings['LBL_M01_QUOTE_DOCUMENT_M03_WORK_PRODUCT_2_FROM_M01_QUOTE_DOCUMENT_TITLE'] = 'SA Quote &amp; Study Documents';
$mod_strings['LBL_TEST_SYSTEM_DESCRIPTION'] = 'Test System Description';
$mod_strings['LBL_DATA_STORAGE_COMMENTS'] = 'Archive Comments';
$mod_strings['LBL_CULPRITS_LIST'] = 'Culprits List';
$mod_strings['LBL_STUDY_DATA_LOCATION'] = 'Archive Location';
$mod_strings['LBL_DATA_RETENTION_START_DATE'] = 'Data Retention Start Date';
$mod_strings['LBL_DATA_RETENTION_END_DATE'] = 'Data Retention End Date';
$mod_strings['LBL_DATA_SCANNED_DATE'] = 'Study Scanned Date';
$mod_strings['LBL_STUDY_ARCHIVED_DATE'] = 'Study Archived Date';
$mod_strings['LBL_SHIPPED_DESTROYED_DATE'] = 'Shipped / Destroyed Date';
$mod_strings['LBL_SPECIMENS'] = 'Specimens';
$mod_strings['LBL_SPECIMEN_LOCATION'] = 'Specimen Location';
$mod_strings['LBL_STUDY_ARTICLE'] = 'Test Article(s)';
$mod_strings['LBL_STUDY_ARTICLES_LOCATION'] = 'Test Article(s) Location';
$mod_strings['LBL_DATA_STORAGE_PHASE'] = 'Data Storage Phase';
$mod_strings['LBL_APPROX_DATA_STORAGE_SIZE'] = 'Approx Data Storage Size';
$mod_strings['LBL_RECORDVIEW_PANEL5'] = 'Archive Info';
$mod_strings['LBL_EXPANDED_STUDY_RESULT'] = 'Expanded Study Result';
$mod_strings['LBL_BUILDING'] = 'Building';
$mod_strings['LBL_WORK_PRODUCT_COMMENTS'] = 'Work Product Comments';
$mod_strings['LBL_RECORDVIEW_PANEL6'] = 'Notes';
$mod_strings['LBL_WPC'] = 'Work Product Code (text)';
$mod_strings['LBL_ANALYTE'] = 'Analyte';
$mod_strings['LBL_MATRIX'] = 'Matrix';
$mod_strings['LBL_INSTRUMENT'] = 'Instrument';
$mod_strings['LBL_RECORDVIEW_PANEL7'] = 'Analytical';
$mod_strings['LBL_COMPANY_STATUS'] = 'Company Status';
$mod_strings['LBL_APS_STUDY_ID'] = 'Method Validation Work Product ID (if applicable)';
$mod_strings['LBL_M03_WORK_PRODUCT_ABC12_WORK_PRODUCT_ACTIVITIES_1_FROM_ABC12_WORK_PRODUCT_ACTIVITIES_TITLE'] = 'Work Product Activities';
$mod_strings['LBL_PRIMARY_APS_OPERATOR'] = 'Primary APS Operator';
$mod_strings['LBL_RECORDVIEW_PANEL8'] = 'Annual Review Dates and Performance';
$mod_strings['LBL_INITIAL_PROCEDURE_ON_TIME'] = 'Initial Procedure Late Start?';
$mod_strings['LBL_FOLLOWUP_ON_TIME'] = 'Follow-up Late Start?';
$mod_strings['LBL_TERMINAL_ON_TIME'] = 'Terminal Late Start?';
$mod_strings['LBL_LATE_START_DETAILS'] = 'Late Start Details';
$mod_strings['LBL_SECONDARY_APS_OPERATOR'] = 'Secondary APS Operator';
$mod_strings['LBL_STUDY_COORDINATOR'] = 'Study Coordinator (old dropdown)';
$mod_strings['LBL_TOTAL_ANIMAL_NUMBERS'] = 'Total Animal Numbers';
$mod_strings['LBL_M03_WORK_PRODUCT_MEETINGS_1_FROM_MEETINGS_TITLE'] = 'APS Activities';
$mod_strings['LBL_LEAD_QAU_AUDITOR'] = 'Lead QAU Auditor';
$mod_strings['LBL_EXTRACT_ABNORMALITY'] = 'Extract Abnormality?';
$mod_strings['LBL_DESCRIBE_ABNORMALITY'] = 'Describe Abnormality';
$mod_strings['LBL_STUDY_DURATION'] = 'Study Duration';
$mod_strings['LBL_USDA_EXEMPTION'] = 'USDA Exemption 1';
$mod_strings['LBL_AAALAC_EXEMPTIONS'] = 'AAALAC Exemption 1';
$mod_strings['LBL_HAZARDOUS_SUBSTANCE'] = 'Hazardous Agents';
$mod_strings['LBL_USDA_EXEMPTION_2'] = 'USDA Exemption 2';
$mod_strings['LBL_AAALAC_EXEMPTIONS_2'] = 'AAALAC Exemptions 2';
$mod_strings['LBL_USDA_CATEGORY'] = 'USDA Category (obsolete)';
$mod_strings['LBL_TEST_CONTROL_ARTICLE_CHECKIN'] = 'Test Control Article Check-In';
$mod_strings['LBL_RECORDVIEW_PANEL9'] = 'Animal Approval and Usage';
$mod_strings['LBL_CHRONIC_STUDY'] = 'Chronic Study Longer than 28 Days?';
$mod_strings['LBL_DURATION_GREATER_THAN_4_WEEK'] = 'Duration Greater Than 4 weeks?';
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M06_ERROR_TITLE'] = 'Communications';
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE'] = 'Sponsor Representative';
$mod_strings['LBL_STUDY_DIRECTOR_SCRIPT_CONTACT_ID'] = 'Study Director (Script) (related Contact ID)';
$mod_strings['LBL_STUDY_DIRECTOR_SCRIPT'] = 'Study Director';
$mod_strings['LBL_SPECIMEN_RETENTION_START'] = 'Specimen Retention Start Date';
$mod_strings['LBL_SPECIMEN_RETENTION_END'] = 'Specimen Retention End Date (GLP)';
$mod_strings['LBL_TWO_WEEK_NOTICE_ISSUED'] = 'Two Week Notice Issued';
$mod_strings['LBL_TYPE_OF_CONTACT'] = 'Type of Contact';
$mod_strings['LBL_DATE_OF_LAST_CONTACT'] = 'Date of Last Contact';
$mod_strings['LBL_CLIENT_CONTACT_NOTES'] = 'Client Contact Notes';
$mod_strings['LBL_SPECIMEN_RETENTION_END_DATE2'] = 'Specimen Retention End Date (nonGLP)';
$mod_strings['LBL_DATA_RETENTION_END_DATE_GLP'] = 'Data Retention End Date (GLP)';
$mod_strings['LBL_DATA_RETENTION_END_DATE_NONG'] = 'Data Retention End Date (nonGLP)';
$mod_strings['LBL_SHIPPED_DESTROYED_DATE_TEXT'] = 'Shipped/Destroyed Date Text';
$mod_strings['LBL_STUDY_COORDINATOR_RELATE_CONTACT_ID'] = 'Study Coordinator Relate (related Contact ID)';
$mod_strings['LBL_STUDY_COORDINATOR_RELATE'] = 'Study Coordinator';
$mod_strings['LBL_PRIMARY_APS_OPERATOR_RELATE_CONTACT_ID'] = 'Primary APS Operator Relate (related Contact ID)';
$mod_strings['LBL_PRIMARY_APS_OPERATOR_RELATE'] = 'Primary APS Operator';
$mod_strings['LBL_APS_PRINCIPAL_INVESTIGATOR_R_CONTACT_ID'] = 'APS Principal Investigator Relate (related Contact ID)';
$mod_strings['LBL_APS_PRINCIPAL_INVESTIGATOR_R'] = 'APS Principal Investigator';
$mod_strings['LBL_LEAD_AUDITOR_RELATE_CONTACT_ID'] = 'Lead Auditor Relate (related Contact ID)';
$mod_strings['LBL_LEAD_AUDITOR_RELATE'] = 'Lead Auditor (Obsolete)';
$mod_strings['LBL_ANALYST_CONTACT_ID'] = 'Analyst (related Contact ID)';
$mod_strings['LBL_ANALYST'] = 'Analyst';
$mod_strings['LBL_TIMELINE_TYPE'] = 'Timeline Type';
$mod_strings['LBL_IACUC_APPROVAL_DATE'] = 'IACUC Approval Date';
$mod_strings['LBL_IACUC_FIRST_ANNUAL_REVIEW'] = 'First Annual Review Due';
$mod_strings['LBL_IACUC_SECOND_ANNUAL_REVIEW'] = 'Second Annual Review Due';
$mod_strings['LBL_IACUC_TRIENNIAL_REVIEW'] = 'Triennial Review Due';
$mod_strings['LBL_INITIAL_ANIMALS_APPROVED_PRI'] = 'Initial Animals Approved';
$mod_strings['LBL_INITIAL_ANIMALS_APPROVED_BAC'] = 'Initial Back-Up Animals Approved';
$mod_strings['LBL_ANIMALS_ADDED'] = 'Animal(s) Added';
$mod_strings['LBL_ANIMALS_ADDED_PRIMARY'] = 'Additional Animals Approved';
$mod_strings['LBL_ANIMALS_ADDED_BACKUP'] = 'Additional Back-Up Animals Approved';
$mod_strings['LBL_TOTAL_ANIMALS_USED_PRIMARY'] = 'Total Animals Approved';
$mod_strings['LBL_TOTAL_ANIMALS_USED_BACKUP'] = 'Total Back-Up Animals Approved';
$mod_strings['LBL_USDA_CATEGORY_MULTISELECT'] = 'USDA Category';
$mod_strings['LBL_USDA_EXEMPTION_1_NOTES'] = 'USDA Exemption 1 Notes';
$mod_strings['LBL_USDA_EXEMPTION_2_NOTES'] = 'USDA Exemption 2 Notes';
$mod_strings['LBL_AAALAC_EXEMPTION_1_NOTES'] = 'AAALAC Exemption 1 Notes';
$mod_strings['LBL_AAALAC_EXEMPTION_2_NOTES'] = 'AAALAC Exemption 2 Notes';
$mod_strings['LBL_FIRST_ANNUAL_REVIEW'] = 'First Annual Review Performed';
$mod_strings['LBL_SECOND_ANNUAL_REVIEW'] = 'Second Annual Review Performed';
$mod_strings['LBL_TRIENNIAL_REVIEW'] = 'Triennial Review Performed';
$mod_strings['LBL_M03_WORK_PRODUCT_TASKS_1_FROM_TASKS_TITLE'] = 'Work Product Tasks';
$mod_strings['LBL_M03_WORK_PRODUCT_TASKS_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Work Product Tasks';
$mod_strings['LBL_SC_REQUIREMENT'] = 'SC Requirement';
$mod_strings['LBL_MASTER_SCHEDULE_COMPANY_NAME'] = 'Master Schedule Sponsor Name';
$mod_strings['LBL_RECORDVIEW_PANEL10'] = 'AAALAC and USDA Exemptions';
$mod_strings['LBL_SEE_AMENDMENT_NUMBERS'] = 'Animal Usage Notes';
$mod_strings['LBL_INITIAL_MALES'] = 'Initial Males';
$mod_strings['LBL_INITIAL_FEMALES'] = 'Initial Females';
$mod_strings['LBL_ADDED_MALES'] = 'Added Males';
$mod_strings['LBL_ADDED_FEMALES'] = 'Added Females';
$mod_strings['LBL_TOTAL_MALES'] = 'Total Males';
$mod_strings['LBL_TOTAL_FEMALES'] = 'Total Females';
$mod_strings['LBL_INITIAL_WEIGHT_RANGE'] = 'Initial Weight Range';
$mod_strings['LBL_WEIGHT_RANGE_CHANGED_TO'] = 'Weight Range Changed To';
$mod_strings['LBL_IACUC_NOTES'] = 'IACUC Notes';
$mod_strings['LBL_RECORDVIEW_PANEL11'] = 'IACUC Notes';
$mod_strings['LBL_FIRST_ANNUAL_REVIEW_COMPLETE'] = 'First Annual Review Completed';
$mod_strings['LBL_SECOND_ANNUAL_REVIEW_COMPLET'] = 'Second Annual Review Completed';
$mod_strings['LBL_TRIENNIAL_REVIEW_COMPLETED'] = 'Triennial Review Completed';
$mod_strings['LBL_EXEMPTION_NOTES'] = 'Exemption Notes';
$mod_strings['LBL_UNSPECIFIED_SEX'] = 'Unspecified Sex';
$mod_strings['LBL_HAZARDOUS_AGENTS'] = 'Hazardous Agents';
$mod_strings['LBL_BLANKET_PROTOCOL_M03_WORK_PRODUCT_ID'] = 'Blanket Protocol (related Work Product ID)';
$mod_strings['LBL_BLANKET_PROTOCOL'] = 'Blanket Protocol';
$mod_strings['LBL_AAALAC_AND_USDA_EXEMPTIONS'] = 'AAALAC and USDA Exemptions';
$mod_strings['LBL_NO_EXEMPTIONS'] = 'No Exemptions';
$mod_strings['LBL_PRIMARY_ANIMALS_COUNTDOWN_C'] = 'Animals Remaining';
$mod_strings['LBL_BACK_UP_ANIMALS_COUNTDOWN_C'] = 'Backup Animals Remaining';
$mod_strings['LBL_ASSIGNED_TO_FOR_COMS'] = 'Assigned to for Coms';
$mod_strings['LBL_MASTER_SCHEDULE_SD_C'] = 'Master Schedule Study Director';
$mod_strings['LBL_CONTROL_ARTICLE'] = 'Control Article(s)';
$mod_strings['LBL_CONTROL_ARTICLES_LOCATION'] = 'Control Article(s) Location';
$mod_strings['LBL_CONTROL_ARTICLE_UNIQUE_IDS'] = 'Control Article Unique ID(s)';
$mod_strings['LBL_TA_UNIQUE_IDS'] = 'Test Article Unique ID(s)';
$mod_strings['LBL_ANALYTICAL_DELIVERABLES'] = 'Are there Analytical Deliverables?';
$mod_strings['LBL_ANALYTICAL_PI_C_CONTACT_ID'] = 'Analytical PI (related Contact ID)';
$mod_strings['LBL_ANALYTICAL_PI'] = 'Analytical PI';
$mod_strings['LBL_BLANKET_ANIMALS_REMAINING_C'] = 'Blanket Protocol Animals Remaining';
$mod_strings['LBL_BLANKET_PROTOCOL_HIDDEN_C'] = 'blanket protocol hidden';
$mod_strings['LBL_MASTER_SCHEDULE_SD'] = 'Master Schedule Study Director';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'WPA Blanket';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_2_FROM_M03_WORK_PRODUCT_TITLE'] = 'WPA Blanket';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'WPA Primary';
$mod_strings['LBL_M03_WORK_PRODUCT_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'WPA Primary';
$mod_strings['LBL_TARGET_ANIMALS'] = 'Target Animals';
$mod_strings['LBL_QUOTED_PROTOCOL_HOURS'] = 'Quoted Protocol Hours';
$mod_strings['LBL_QUOTED_STUDY_MANAGEMENT_HRS'] = 'Quoted Study Management Hours';
$mod_strings['LBL_QUOTED_REPORTING_HOURS'] = 'Quoted Reporting Hours';
$mod_strings['LBL_ACTUAL_PROTOCOL_HOURS'] = 'Actual Protocol Hours';
$mod_strings['LBL_ACTUAL_REPORTING_HOURS'] = 'Actual Reporting Hours';
$mod_strings['LBL_ACTUAL_STUDY_MANAGEMENT_HRS'] = 'Actual Study Management Hours';
$mod_strings['LBL_TIME_TO_ON_TEST'] = 'Time to On Test';
$mod_strings['LBL_M03_WORK_PRODUCT_FOCUS_DRAWER_DASHBOARD'] = 'Work Products Focus Drawer';
$mod_strings['LBL_STUDY_WORKLOAD'] = 'Study Workload';
$mod_strings['LBL_FINAL_REPORT_TIMELINE'] = 'Final Report Timeline (Weeks)';
$mod_strings['LBL_FINAL_REPORT_TIMELINE_TYPE'] = 'Final Report Timeline Type';
$mod_strings['LBL_NUMBER_OF_PEAKS_GCMS'] = 'Number of Peaks Identified for GCMS Analysis';
$mod_strings['LBL_NUMBER_OF_PEAKS_LCMS'] = 'Number of Peaks Identified for LCMS Analysis';
$mod_strings['LBL_WPC_SC01'] = 'WPC SC01';
$mod_strings['LBL_SPA_REVIEW_TO_ON_TEST'] = 'SPA Review to On Test';
$mod_strings['LBL_REPORT_TIMELINE_TYPE'] = 'Report Timeline Type';
$mod_strings['LBL_LEAD_DATA_REVIEWER_C_CONTACT_ID'] = 'Lead Data Reviewer (related Contact ID)';
$mod_strings['LBL_LEAD_DATA_REVIEWER'] = 'QA Trainee Reviewer';
$mod_strings['LBL_M06_ERROR_M03_WORK_PRODUCT_1_FROM_M03_WORK_PRODUCT_TITLE'] = 'Communications';
$mod_strings['LBL_REQUIRED_IN_LIMS'] = 'Required In LIMS?';
$mod_strings['LBL_NAMSA_SUBMISSION_ID'] = 'NAMSA Submission ID';
$mod_strings['LBL_NAMSA_TEST_CODE'] = 'NAMSA Test Code';
$mod_strings['LBL_FINAL_REPORT_UPLOADED_NW'] = 'Final Report Uploaded (NW)';
$mod_strings['LBL_RECORDVIEW_PANEL12'] = 'NAMSA NW';
$mod_strings['LBL_REASON_FOR_EXPANSION'] = 'Reason for Expansion';
$mod_strings['LBL_REASON_FOR_DISCONTINUATION'] = 'Reason for Discontinuation';
$mod_strings['LBL_TEST_ARTICLE_NAME_C'] = 'Test Article Name ';
$mod_strings['LBL_EXTRACTION_START_INTEGER'] = 'Extraction Start Integer';
$mod_strings['LBL_EXTRACTION_END_INTEGER'] = 'Extraction End Integer';
$mod_strings['LBL_INTEGER_UNITS'] = 'Integer Units';
$mod_strings['LBL_INTEGER_UNITS_C_U_UNITS_ID'] = 'Integer Units (related Unit ID)';
$mod_strings['LBL_NAMSA_TEST_CODE_C_TC_NAMSA_TEST_CODES_ID'] = 'NAMSA Test Code (related NAMSA Test Code ID)';
$mod_strings['LBL_ACCOUNTS_M03_WORK_PRODUCT_1_NAME_FIELD_TITLE'] = 'Sponsor';
$mod_strings['LBL_SPA_RECONCILED_DATE_TDS'] = 'Spa Reconciled Date TDs';
$mod_strings['LBL_GDS_COMPLETE'] = 'GDs Complete';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_TEST_PRICE'] = 'Test Price';
$mod_strings['LBL_PARENT_WORK_PRODUCT'] = 'Parent Work Product';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_AAALAC_REQUIREMENTS'] = 'AAALAC Requirements';
$mod_strings['LBL_USDA_EXEMPTIONS'] = 'USDA Exemptions';
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_2_NAME_FIELD_TITLE'] = 'Lead Auditor';
$mod_strings['LBL_CONTACTS_M03_WORK_PRODUCT_2_FROM_CONTACTS_TITLE'] = 'Lead Auditor';
$mod_strings['LBL_EXTRACTION_CONDITIONS_C'] = 'Extraction Conditions';
$mod_strings['LBL_OTHER_EXTRACTION_CONDITIONS'] = 'Other Extraction Conditions';
$mod_strings['LBL_CREATE_TDS'] = 'create tds';
$mod_strings['LBL_RECORDVIEW_PANEL13'] = 'New Panel 13';

?>
