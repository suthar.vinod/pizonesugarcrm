<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/LogicHooks/update_WPE_hook.php


    $hook_array['after_save'][] = Array(
        1010,
        'Update WPE Blanket Field',
        'custom/modules/M03_Work_Product/WPE_WP_Blanket_Field_Hook.php',
        'WPE_WP_Blanket_Field_Hook',
        'updateWPBlanket'
    );
    

?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/LogicHooks/remove_relationship_blanket.php


    $hook_array['before_save'][] = Array(
        //Processing index. For sorting the array.
        5,

        //Label. A string value to identify the hook.
        'Remove Blanket Relationship',

        //The PHP file where your class is located.
        'custom/modules/M03_Work_Product/RemoveBlanketRelationship.php',

        //The class the method is in.
        'RemoveBlanketRelationship',

        //The method to call.
        'removeRelationship'
    );




?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/LogicHooks/logic_hooks.php

$hook_array['before_save'][] = Array(6,
'Generating system id',
'custom/modules/M03_Work_Product/customLogicHook.php',
'M03_Work_ProductCustomLogicHook',
'generateSystemIdForWorkProduct',);
 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/LogicHooks/link_controlled_doc.php

$hook_array['after_save'][] = Array(
19,
'Link Controlled Document to WP',
'custom/modules/M03_Work_Product/LinkControlledDocumentToWP.php',
'LinkControlledDocumentToWP',
'LinkControlledDocument',);
 
?>
<?php
// Merged from custom/Extension/modules/M03_Work_Product/Ext/LogicHooks/setStatusReadOnlyHook.php

$hook_array['after_save'][] = array(
    1023,
   'Set BID status field readonly',
   'custom/modules/M03_Work_Product/setBIDStatusAddHook.php',
    'setBIDStatusReadOnlyAdd',
    'setBIDStatus'
);
?>
