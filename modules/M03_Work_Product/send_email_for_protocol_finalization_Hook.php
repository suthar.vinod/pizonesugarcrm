<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class send_email_for_protocol_finalization_Hook{
	function sendMailForProtocol($bean, $event, $arguments){
		global $db,$current_user;
		$site_url = $GLOBALS['sugar_config']['site_url'];
	  

		if($bean->id!="" && ($bean->fetched_row['work_product_status_c']!="Testing" && $bean->work_product_status_c=="Testing")){
 
			$wpID  = $bean->id;
			$wpName  = $bean->name;

			$getTS_sql = "SELECT id FROM anml_animals AS TS LEFT JOIN anml_animals_cstm AS TSCSTM ON TS.id=TSCSTM.id_c WHERE m03_work_product_id_c='".$wpID."' AND TS.deleted=0"; 
			$getTS_exec = $db->query($getTS_sql);
             
			if ($getTS_exec->num_rows > 0) 
			{
				$wp_name = '<a target="_blank"href="' . $site_url . '/#M03_Work_Product/' . $wpID . '">' . $wpName . '</a>';

				$template = new EmailTemplate();
				$emailObj = new Email();

				$template->retrieve_by_string_fields(array('name' => 'Notification of Protocol Finalization', 'type' => 'email'));
				$template->body_html = str_replace('[WP_ID]', $wp_name, $template->body_html); 

				$defaults	= $emailObj->getSystemDefaultEmail();
				$mail		= new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->IsHTML(true);
				$mail->From		= $defaults['email'];
				$mail->FromName = $defaults['name'];
				$mail->Subject	= $template->subject;
				$mail->Body		= $template->body_html;		
				 
				$mail->AddAddress('ops_startupsanimals@apsemail.com');
				$mail->AddAddress('dlmccarty@apsemail.com');
				//$mail->AddBCC('mjohnson@apsemail.com'); //to be deleted
				//$mail->AddBCC('vsuthar@apsemail.com'); //to be deleted

				if (!$mail->Send()) {
					$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
				} else {
					$GLOBALS['log']->debug('email sent');
				}
				unset($mail);

			}
		}
	}
}
?>