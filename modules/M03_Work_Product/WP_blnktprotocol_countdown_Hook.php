<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class WP_blnktprotocol_countdown_Hook{
	function UpdateBlanketCountdown($bean, $event, $arguments){
		global $db,$current_user;
		
		if($bean->id!="" && ($bean->m03_work_product_id_c!="" || $bean->blanket_protocol_hidden_c!="") && ($bean->id!="4a529aae-5165-3149-6db7-5702c7b98a5e" && $bean->id!="811ae58e-9281-2755-25d5-5702c7252696")){
			$tobesaveWPid = ($bean->m03_work_product_id_c!="")?$bean->m03_work_product_id_c:$bean->blanket_protocol_hidden_c;

			if($bean->blanket_protocol_hidden_c!="")
			{
				$updateQuery = "UPDATE m03_work_product_cstm SET `blanket_protocol_hidden_c`='' WHERE id_c='".$bean->id."'"; 
				$updateQuery_exec = $db->query($updateQuery);
			}
			if($tobesaveWPid!=""){
				$wp_bean 		= BeanFactory::getBean('M03_Work_Product', $tobesaveWPid); 
				$iacucDate		= $wp_bean->iacuc_approval_date_c;
				$wpCode			= $wp_bean->m03_work_product_code_id1_c;
				$totalAnimal 	= $wp_bean->total_animals_used_primary_c;
				$blanket_animals_remaining_c = 0;
				
				if($iacucDate!="" && ($wpCode=='9634c914-fe07-11e6-a663-02feb3423f0d' || $wpCode=='0527341e-d0d4-11e9-8661-06eddc549468')){
					$blnkQuery = "SELECT sum(IFNULL(WPCSTM.primary_animals_countdown_c,0)) PrimaryAnimalRemaining,
										 sum(IFNULL(WPCSTM.back_up_animals_countdown_c,0)) BackupAnimalRemaining
								FROM m03_work_product AS WP
								LEFT JOIN  m03_work_product_erd_error_documents_1_c AS WPERD  ON 
								WP.id=WPERD.m03_work_product_erd_error_documents_1m03_work_product_ida AND WPERD.deleted=0

								LEFT JOIN  erd_error_documents ERD  
								ON ERD.id=WPERD.m03_work_product_erd_error_documents_1erd_error_documents_idb AND ERD.deleted=0
								
								LEFT JOIN m03_work_product_cstm WPCSTM  ON WP.id = WPCSTM.id_c
								LEFT JOIN m03_work_product WP1 ON WP1.id = WPCSTM.m03_work_product_id_c AND IFNULL(WP1.deleted,0)=0 

							WHERE 
								WPCSTM.m03_work_product_id_c='".$tobesaveWPid."'
								AND 
								WPCSTM.work_product_compliance_c IN ('NonGLP Discovery','NonGLP Structured','GLP','GLP Discontinued','nonGLP')
								AND 
								WPCSTM.first_procedure_c >= '".$iacucDate."' 
								AND  
								WP.deleted=0 AND IFNULL(WP1.deleted,0)=0"; 
					$blnkQuery_exec = $db->query($blnkQuery);
					$blnkSum	= 0;
					$primarySum = 0;
					$backupSum	= 0;
					if ($blnkQuery_exec->num_rows > 0) 
					{
						while($fetchBlnk = $db->fetchByAssoc($blnkQuery_exec)) {
							$primarySum	= $fetchBlnk['PrimaryAnimalRemaining'];
							$backupSum	= $fetchBlnk['BackupAnimalRemaining']; 
						}
						$blnkSum = $primarySum+$backupSum;
						$blanket_animals_remaining_c = ($totalAnimal + $blnkSum); 
					}
					
					$updateBlnkQuery = "UPDATE m03_work_product_cstm SET `blanket_animals_remaining_c`='".$blanket_animals_remaining_c."' WHERE id_c='".$tobesaveWPid."'"; 
					$db->query($updateBlnkQuery);
					
			} 
				 
			}
		}else{			
		}
	}
}
?>