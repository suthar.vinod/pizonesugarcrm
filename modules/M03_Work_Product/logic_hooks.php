<?php
$hook_version = 1;
$hook_array = Array();

// position, file, function 
$hook_array['before_save'] = Array(); 
$hook_array['before_save'][] = Array(
    1,
    'workflow',
    'include/workflow/WorkFlowHandler.php',
    'WorkFlowHandler',
    'WorkFlowHandler',
);
$hook_array['before_save'][] = Array(
    10,
    'Update Animal Countdown',
    'custom/modules/M03_Work_Product/WP_animal_countdown_Hook.php',
    'WP_animalCountHook',
    'UpdateAnimalCountdownValues',
);

$hook_array['before_save'][] = Array(
    12,
    'Send Email for Protocol Finalization',
    'custom/modules/M03_Work_Product/send_email_for_protocol_finalization_Hook.php',
    'send_email_for_protocol_finalization_Hook',
    'sendMailForProtocol',
); 

$hook_array['before_save'][] = Array(
    13,
    'Save WPD on First procedure change',
    'custom/modules/M03_Work_Product/update_wpd_final_due_date.php',
    'updateWpdFinalDueDateHook',
    'WpdFinalDueDate',
);

$hook_array['before_save'][] = Array(
    14,
    'Custom Email Workflow for Biocomp Report Queue',
    'custom/modules/M03_Work_Product/check_biocomp_report_hook.php',
    'checkBiocompReportHook',
    'checkBiocompReport',
);

$hook_array['before_save'][] = Array(
    21,
    'Dependent check of Audit box',
    'custom/modules/M03_Work_Product/setCheckofAuditAfterRelationAdd.php',
    'setCheckofAuditAfterRelationAddHook',
    'setCheckofAuditAfterRelationAdd',
);

$hook_array['before_save'][] = Array(
    22,
    'Custom WPC Field Check value first time',
    'custom/modules/M03_Work_Product/check_wpc_field_hook.php',
    'checkwpcfieldHook',
    'checkwpcfield',
);

$hook_array['before_save'][] = Array(
    16,
    'Custom Email Workflow for Lead Auditor field change',
    'custom/modules/M03_Work_Product/change_lead_auditor_send_email.php',
    'send_email_lead_auditor_class',
    'send_email_lead_auditor_Record',
);

$hook_array['after_save'][] = Array(
    11,
    'Update blanket_animals_remaining Countdown',
    'custom/modules/M03_Work_Product/WP_blnktprotocol_countdown_Hook.php',
    'WP_blnktprotocol_countdown_Hook',
    'UpdateBlanketCountdown',
);

$hook_array['after_save'][] = array(
    18,
    'Set Custom Calculation',
    'custom/modules/M03_Work_Product/setCustomCalculationAfterRelAddWP.php',
    'setCustomCalculationAfterRelAddWP',
    'setCalculationAfterRelAddWP',
);

$hook_array['after_save'][] = array(
    107,
    'Copy all task design in test system when we link WPA',
    'custom/modules/M03_Work_Product/CopyTaskDesignWPHook.php',
    'CopyTaskDesignWPHook',
    'copyTaskDesign',
);

$hook_array['after_save'][] = Array(
    20,
    'Custom Email Workflow for Biocomp Report Queue',
    'custom/modules/M03_Work_Product/send_email_for_biocomp_report.php',
    'sendEmailForBiocompReportHook',
    'sendEmailForBiocompReport',
);

$hook_array['process_record'][] = array(
	15,
    'delete duplicate Activities Audit log Record',
    'custom/modules/M03_Work_Product/deleteCountdownAuditLog.php',
    'deleteCountdownAuditLog',
    'deleteCountdownAudit'
);

?>