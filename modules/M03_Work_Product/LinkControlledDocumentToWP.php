<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class LinkControlledDocumentToWP {

    public function LinkControlledDocument($bean, $event, $arguments) {
        global $db, $current_user;
		 
        if($bean->m03_work_product_code_id1_c){
            $query = "SELECT WPC.name, WPC_CSTM.erd_error_documents_id_c FROM m03_work_product_code AS WPC 
                        LEFT JOIN m03_work_product_code_cstm AS WPC_CSTM ON WPC.id = WPC_CSTM.id_c
                        WHERE WPC.id = '".$bean->m03_work_product_code_id1_c."' and WPC.deleted=0";

                $result = $db->query($query);
 
                $row = $db->fetchByAssoc($result);
                
                $bean->load_relationship('m03_work_product_erd_error_documents_1');  
                
                $bean->m03_work_product_erd_error_documents_1->add($row['erd_error_documents_id_c']);
        }
		
    }
}
