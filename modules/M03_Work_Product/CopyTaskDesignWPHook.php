<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class CopyTaskDesignWPHook
{
    static $already_ran = false;
    function copyTaskDesign($bean, $event, $arguments)
    {
        if (self::$already_ran == true) return; //So that hook will only trigger once
        self::$already_ran = true;

        global $db, $current_user;
        global $app_list_strings;

        //$GLOBALS['log']->fatal(' WP Name WPC have Value Coped == ' . $bean->name . ' ====> ' . $bean->wpchavevalue );
        $GLOBALS['log']->fatal(' WP Name Check WPC Record Value Coped == ' . $bean->name . ' SPA Date Reconciled Date ==> '. $bean->bc_spa_reconciled_date_c .' WPC ID ==> ' . $bean->fetched_row['m03_work_product_code_id1_c'] .' WPC HAve Value ===> ' . $bean->wpchavevalue);

        if ($bean->wpchavevalue == 1 && $bean->m03_work_product_code_id1_c != "" && $bean->bc_spa_reconciled_date_c != "") {
            /* Ticket #1849 - Update TD auto-creation WPC workflow  */
            $GLOBALS['log']->fatal('in if conditon in file CopyTaskDesignWPHook line 20 bean name :  == ' . $bean->name);
            $wpCode = $bean->m03_work_product_code_id1_c;
            $GLOBALS['log']->fatal('in if conditon in file CopyTaskDesignWPHook line 22 wpCode :  == ' . $wpCode);
            $WPCBean = BeanFactory::retrieveBean('M03_Work_Product_Code', $wpCode);
            $WPCBean->load_relationship('m03_work_product_code_taskd_task_design_1');
            $relatedTD = $WPCBean->m03_work_product_code_taskd_task_design_1->get();
           // $GLOBALS['log']->fatal(' relatedTD ' . print_r($relatedTD, 1));

            $copyingTaskDesignIds = array();
            $tsArr = array();
            $tsRelationArr = array();

            $p1arr = array();
            $month = date("F", strtotime($bean->first_procedure_c));
            $WPC = $bean->work_product_compliance_c;

            foreach ($relatedTD as $TDId) {
                $clonedBean = \BeanFactory::getBean('TaskD_Task_Design', $TDId);
                $other_equipment = $clonedBean->other_equipment_c;
                $clonedBean->load_relationship("taskd_task_design_taskd_task_design_1");

                $sqlGetTaskDesignIds = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_ida as TDId
                                    FROM taskd_task_design_taskd_task_design_1_c 
                                    where taskd_task_design_taskd_task_design_1taskd_task_design_idb='$TDId' and deleted=0";
                $resultsSqlGetTaskDesignIds = $db->query($sqlGetTaskDesignIds);
                $rowSqlGetTaskDesignIds = $db->fetchByAssoc($resultsSqlGetTaskDesignIds);
                $taskDesignIds = $rowSqlGetTaskDesignIds['TDId'];


                $wpBean = BeanFactory::getBean('M03_Work_Product', $bean->id);
                // $testSystemIds = $clonedBean->anml_animals_taskd_task_design_1->get();
                $clonedBean->id = create_guid();
                $tsArr[$clonedBean->id] = $TDId;
                $tsRelationArr[$TDId] = $taskDesignIds;
                $clonedBean->new_with_id = true;
                $clonedBean->fetched_row = null;
                $date_entered = date("Y-m-d H:i:s", time());

                $p1arr[$TDId] = $taskDesignIds;
                $a1arr[$clonedBean->id] = $TDId;
                $b1arr[$TDId] = $clonedBean->id;
                $clonedBean->type_2 = "Actual SP";
                $clonedBean->other_equipment_c = $other_equipment;
                //$clonedBean->u_units_id_c = $clonedBean->u_units_id_c;
                $copyingTaskDesignIds[] = $clonedBean->id;
                //$clonedBean->save();
                $clonedBean->m03_work_product_taskd_task_design_1->add($bean->id);

                $wpName = $bean->name;
                $task_type = $clonedBean->task_type;
                $custom_task = $clonedBean->custom_task;
                $standard_task = $clonedBean->standard_task;
                //$phase = $clonedBean->phase_c;
                $phase_dom = $app_list_strings['td_phase_list'];
                $phase = $phase_dom[$clonedBean->phase_c];
                $batch_id       = $clonedBean->bid_batch_id_taskd_task_design_1bid_batch_id_ida;
                $BatchBean      =   BeanFactory::getBean('BID_Batch_ID', $batch_id);
                $BatchName      =   $BatchBean->name;
                $clonedBean->date_entered = $date_entered;
                if ($task_type == 'Custom') {
                    $standard_task = '';
                    $standard_task_val = $custom_task;
                } else if ($task_type == 'Standard') {
                    $custom_task = '';
                    $standard_task_val = $standard_task;
                }
                if ($BatchName != "") {
                    $clonedBean->name = $wpName . ' ' . $phase . ' ' . $BatchName . ' ' . $standard_task_val;
                } else if ($phase != "") {
                    $clonedBean->name = $wpName . ' ' . $phase . ' ' . $standard_task_val;
                } else {
                    $clonedBean->name = $wpName . ' ' . $standard_task_val;
                }

                /* 399 aurdit phase issue */

                $audit_phase = $clonedBean->audit_phase_c;

                if ($clonedBean->type_2 == "Actual SP" && ($month == "January" || $month == "April" || $month == "July" ||  $month == "October") && $audit_phase == "Sample Prep" && $WPC == "GLP") {
                    $clonedBean->audit_2_c = 1;
                }
                if ($clonedBean->type_2 == "Actual SP" && ($month == "February" || $month == "May" || $month == "August" ||  $month == "November") && $audit_phase == "Application" && $WPC == "GLP") {
                    $clonedBean->audit_2_c = 1;
                }
                if ($clonedBean->type_2 == "Actual SP" && $bean->m03_work_product_code_id1_c != "c81c2577-bd7c-16e3-1248-5785ac111a73" && ($month == "March" || $month == "June" || $month == "September" ||  $month == "December") && $audit_phase == "Evaluation" && $WPC == "GLP") {
                    $clonedBean->audit_2_c = 1;
                }
                if ($clonedBean->type_2 == "Actual SP" && $bean->m03_work_product_code_id1_c == "c81c2577-bd7c-16e3-1248-5785ac111a73" && ($month == "March" || $month == "June" || $month == "September" ||  $month == "December") && $audit_phase == "Application" && $WPC == "GLP") {
                    $clonedBean->audit_2_c = 1;
                }


                if ($standard_task == 'Extract In') {
                    $clonedBean->start_integer  = $wpBean->extraction_start_integer_c;
                    $clonedBean->end_integer    = $wpBean->extraction_end_integer_c;
                    $clonedBean->integer_units  = $wpBean->integer_units_c;
                    $clonedBean->u_units_id_c   = $wpBean->u_units_id_c;
                    $unit_Bean = BeanFactory::getBean('U_Units', $clonedBean->u_units_id_c);
                    $unit_Bean_name = $unit_Bean->name;
                    $clonedBean->time_window = $unit_Bean_name . " " . $clonedBean->start_integer . "-" . $clonedBean->end_integer;
                    if ($unit_Bean_name == "Day") {
                        $integer_u = 'days';
                    } elseif ($unit_Bean_name == "Hour") {
                        $integer_u = 'hours';
                    } else {
                        $integer_u = 'minutes';
                    }
                    //$offset1 =  -18000;
                    $offset1 =  21600; //daylight Saving
                    if ($wpBean->first_procedure_c != '') {
                        $datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
                        $actual_datetime       = date("Y-m-d 17:00:00", $datefirst_procedure_c);
                    }
                    if ($wpBean->first_procedure_c != '' && $clonedBean->relative == "NA") {
                        $datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
                        $clonedBean->actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
                        //$clonedBean->save();

                    }

                    if ($actual_datetime != "") {
                        if ($clonedBean->relative == "1st Tier") {
                            $result = date($actual_datetime);
                            $result = strtotime($result . '+' . $clonedBean->start_integer  . ' ' . $integer_u);
                            $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
                            if (date("Y", $result) != "1970") {
                                $clonedBean->single_date_2_c = date("Y-m-d", $result);
                            }
                            $result = date($actual_datetime);
                            $result = strtotime($result . '+' . $clonedBean->end_integer  . ' ' . $integer_u);
                            $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);

                        }
                    }
                } else {

                    if ($wpBean->first_procedure_c != '') {
                        $datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
                        $actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
                    }
                    if ($wpBean->first_procedure_c != '' && $clonedBean->relative == "NA") {

                        $datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
                        $clonedBean->actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
                        //$clonedBean->save();

                    }
                    $clonedBean->u_units_id_c = $clonedBean->u_units_id_c;
                    $clonedBean->start_integer = $clonedBean->start_integer;
                    $clonedBean->end_integer = $clonedBean->end_integer;
                    $clonedBean->integer_units = $clonedBean->integer_units;
                    //$clonedBean->u_units_id_c = $wpBean->u_units_id_c;
                    $unit_Bean = BeanFactory::getBean('U_Units', $clonedBean->u_units_id_c);
                    $unit_Bean_name = $unit_Bean->name;
                    $clonedBean->time_window = $unit_Bean_name . " " . $clonedBean->start_integer . "-" . $clonedBean->end_integer;
                    //$clonedBean->time_window = $clonedBean->time_window;
                    if ($unit_Bean_name == "Day") {
                        $integer_u = 'days';
                    } elseif ($unit_Bean_name == "Hour") {
                        $integer_u = 'hours';
                    } else {
                        $integer_u = 'minutes';
                    }
                    if ($actual_datetime != "") {
                        if ($clonedBean->relative == "1st Tier") {
                            $result = date($actual_datetime);
                            $result = strtotime($result . '+' . $clonedBean->start_integer  . ' ' . $integer_u);
                            $clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
                            if (date("Y", $result) != "1970") {
                                $clonedBean->single_date_2_c = date("Y-m-d", $result);
                            }
                            $result = date($actual_datetime);
                            $result = strtotime($result . '+' . $clonedBean->end_integer  . ' ' . $integer_u);
                            $clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);                           
                        }
                    }
                }

                $clonedBean->save();

                $auditsql = 'update taskd_task_design_audit 
                set field_name= "u_units_id_c",
                after_value_string="' . $clonedBean->u_units_id_c . '" 
                Where parent_id = "' . $clonedBean->id . '"
                AND field_name="integer_units"';
                $db->query($auditsql);

                $queryAuditLog = "SELECT id,count(*) as NUM FROM `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' 
                    AND `parent_id`='" . $clonedBean->id . "' group by before_value_string,after_value_string,date_created";
                $auditLogResult = $db->query($queryAuditLog);
                // $GLOBALS['log']->fatal('sqlGetTaskDesignIds  ===  APA '.$queryAuditLog);
                if ($auditLogResult->num_rows > 0) {
                    while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
                        if ($fetchAuditLog['NUM'] > 1) {
                            $recordID = $fetchAuditLog['id'];
                            $sql_DeleteAudit = "DELETE from `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' AND `parent_id`='" . $clonedBean->id . "' AND id = '" . $recordID . "'";
                            $db->query($sql_DeleteAudit);
                            // $GLOBALS['log']->fatal('sqlGetTaskDesignIds  ===  APA '.$sql_DeleteAudit);

                        }
                    }
                }

                /** Save audit log for all updated fields*/
                /* $source = '{"subject":{"_type":"logic-hook","class":"CopyTaskDesignToGD","method":"CopyTaskDesignToGD"},"attributes":[]}';                 


                if ($clonedBean->planned_start_datetime_1st != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_start_datetime_1st","datetime","","' . $clonedBean->planned_start_datetime_1st . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->planned_end_datetime_1st != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_end_datetime_1st","datetime","","' . $clonedBean->planned_end_datetime_1st . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->planned_start_datetime_2nd != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_start_datetime_2nd","datetime","","' . $clonedBean->planned_start_datetime_2nd . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->planned_end_datetime_2nd != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"planned_end_datetime_2nd","datetime","","' . $clonedBean->planned_end_datetime_2nd . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }

                if ($clonedBean->u_units_id_c != '') {
                    $auditEventid = create_guid();
                    $auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                    values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"u_units_id_c","datetime","","' . $clonedBean->u_units_id_c . '")';
                    $auditsqlResult_1 = $db->query($auditsql_1);

                    $auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
                    $db->query($auditsqlStatus_1);
                }*/

            }

            // $GLOBALS['log']->fatal('p1arr  === ' . print_r($p1arr, 1));
            // $GLOBALS['log']->fatal('a1arr  === ' . print_r($a1arr, 1));
            // $GLOBALS['log']->fatal('b1arr  === ' . print_r($b1arr, 1));
            foreach ($p1arr as $key => $value) {
                $newBeanId = $b1arr[$key];
                $toBeRelateID = array_search($value, $a1arr);

                if ($toBeRelateID != "") {
                    $uniqId = create_guid();
                    $sqlInsertRelationship = "INSERT INTO `taskd_task_design_taskd_task_design_1_c` 
            (`id`, `date_modified`, `deleted`, `taskd_task_design_taskd_task_design_1taskd_task_design_ida`, `taskd_task_design_taskd_task_design_1taskd_task_design_idb`) 
            VALUES ('$uniqId', now(), '0', '$toBeRelateID', '$newBeanId')";
                    $resultsSqlGetTaskDesignIds = $db->query($sqlInsertRelationship);
                }
            }

            $currentWPBean = BeanFactory::retrieveBean('M03_Work_Product', $bean->id);
            $currentWPBean->load_relationship('m03_work_product_taskd_task_design_1');
            $relatedTD1 = $currentWPBean->m03_work_product_taskd_task_design_1->get();
            //$GLOBALS['log']->fatal(' relatedTD1 ' . print_r($relatedTD1, 1));
            foreach ($relatedTD1 as $tdID) {
                $tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $tdID);
                $u_units = $tdBean->u_units_id_c;
                $unit_Bean = BeanFactory::getBean('U_Units', $u_units);
                $unit_Bean_name = $unit_Bean->name;

                if ($unit_Bean_name == "Day") {
                    $integer_u = 'days';
                } elseif ($unit_Bean_name == "Hour") {
                    $integer_u = 'hours';
                } else {
                    $integer_u = 'minutes';
                }
                //$GLOBALS['log']->fatal(' tdID tdBean->relative ' . $tdBean->relative);
                if ($tdBean->relative == "1st Tier") {
                    $tdBean1 = BeanFactory::retrieveBean('TaskD_Task_Design', $tdBean->id);
                    $tdBean1->load_relationship('taskd_task_design_taskd_task_design_1');
                    $taskDesignId1 = $tdBean1->taskd_task_design_taskd_task_design_1->get();

                    $Get_2nd_tier = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_idb FROM  `taskd_task_design_taskd_task_design_1_c` WHERE `taskd_task_design_taskd_task_design_1taskd_task_design_ida` = '" . $tdBean->id . "' AND deleted = 0;";
                    $Get_Result = $db->query($Get_2nd_tier);

                    if ($Get_Result->num_rows > 0) {
                        while ($fetchResult = $db->fetchByAssoc($Get_Result)) {
                            $recordID = $fetchResult['taskd_task_design_taskd_task_design_1taskd_task_design_idb'];

                            $tdBean2 = BeanFactory::retrieveBean('TaskD_Task_Design', $recordID);

                            if ($tdBean->planned_start_datetime_1st != "") {
                                $result = date($tdBean->planned_start_datetime_1st);
                                $result = strtotime($result . '+' . $tdBean2->start_integer . ' ' . $integer_u);
                                $tdBean2->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
                                if (date("Y", $result) != "1970") {
                                    $tdBean2->single_date_2_c = date("Y-m-d", $result);
                                }
                            } else {
                                $tdBean2->planned_start_datetime_2nd = null;
                            }

                            if ($tdBean->planned_end_datetime_1st != "") {
                                $result = date($tdBean->planned_end_datetime_1st);
                                $result = strtotime($result . '+' . $tdBean2->end_integer . ' ' . $integer_u);
                                $tdBean2->planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
                            } else {
                                $tdBean2->planned_end_datetime_2nd = null;
                            }
                            $tdBean2->save();
                        }
                    }
                }
            }
        }
    }
}
