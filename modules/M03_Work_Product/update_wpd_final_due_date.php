<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class updateWpdFinalDueDateHook
{

    public function WpdFinalDueDate($bean, $event, $arguments)
    {
        global $db, $current_user;

        if (isset($arguments['isUpdate']) && $arguments['isUpdate'] == true && ($bean->first_procedure_c != $bean->fetched_row['first_procedure_c'] || $bean->final_report_timeline_c != $bean->fetched_row['final_report_timeline_c'] || $bean->bc_spa_reconciled_date_c != $bean->fetched_row['bc_spa_reconciled_date_c'] || $bean->report_timeline_type_c != $bean->fetched_row['report_timeline_type_c'])) {
            $num_rows = $this->get_record_wpd($bean->name);
            //$GLOBALS['log']->fatal('bean->name : '.$bean->name); 
            if ($bean->functional_area_c == 'Standard Biocompatibility' && $bean->report_timeline_type_c == 'By First Procedure' && ($bean->first_procedure_c != "" || $bean->first_procedure_c != null)) {

                $bean->load_relationship("m03_work_product_m03_work_product_deliverable_1");
                $WPDIDs = $bean->m03_work_product_m03_work_product_deliverable_1->get();

                foreach ($WPDIDs as $wpdId) {
                    $WPDBean = BeanFactory::retrieveBean('M03_Work_Product_Deliverable',  $wpdId);

                    if ($WPDBean->deliverable_c == "Final Report") {
                        if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
                            $Days = ' + 0 days';
							$finalDueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $Days));
							
                        } else {
                            $Days = 7 * $bean->final_report_timeline_c;
                            $finalDueDate = date('Y-m-d', strtotime($bean->first_procedure_c . ' + ' . $Days . ' days'));
                           
                        }

                        if ($num_rows > 0) {
                            if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
                                //$DueDays = ' - 5 days';
                                //$DueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $DueDays));
								$minusDays = 5; 
								$DueDate = $this->getDaysCalculation($bean->first_procedure_c,$minusDays);
						
                            } else {
                                $DueDays = (7 * $bean->final_report_timeline_c);// - 5
                                $DueDate1 = date('Y-m-d', strtotime($bean->first_procedure_c . ' + ' . $DueDays . ' days'));
								$minusDays = 5; 
								$DueDate = $this->getDaysCalculation($DueDate1,$minusDays);
                            }
                        } else {
                            if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
                                //$DueDays = ' - 3 days';
                                //$DueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $DueDays));
								$minusDays = 3; 
								$DueDate = $this->getDaysCalculation($bean->first_procedure_c,$minusDays);
								
                            } else {
                                $DueDays = (7 * $bean->final_report_timeline_c);// - 3
                                $DueDate1 = date('Y-m-d', strtotime($bean->first_procedure_c . ' + ' . $DueDays . ' days'));
								$minusDays = 3; 
								$DueDate = $this->getDaysCalculation($DueDate1,$minusDays);
                            }
                        }

                        $WPDBean->final_due_date_c = $finalDueDate;
                        $WPDBean->due_date_c = $DueDate;
                        $WPDBean->save();
                    } else if ($WPDBean->deliverable_c == "Histopathology Report") {
                        if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
                            $Days = ' + 0 days';
                            $DueDays = ' - 6 days';
							$finalDueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $Days));
                            //$DueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $DueDays));
							$minusDays = 6; 
							$DueDate = $this->getDaysCalculation($bean->first_procedure_c,$minusDays);
                        } else {
                            $Days = 7 * $bean->final_report_timeline_c;
                            $finalDueDate = date('Y-m-d', strtotime($bean->first_procedure_c . ' + ' . $Days . ' days'));

                            if ($bean->final_report_timeline_c == 1) {
                                //$DueDays = ' - 1 days';
                                //$DueDate = date('Y-m-d', strtotime($bean->first_procedure_c . $DueDays));
								$minusDays = 1; 
								$DueDate = $this->getDaysCalculation($bean->first_procedure_c,$minusDays);
                            } else {
                                $DueDays	= (7 * $bean->final_report_timeline_c);// - 6
                                $DueDate1	= date('Y-m-d', strtotime($bean->first_procedure_c . ' + ' . $DueDays . ' days'));
								$minusDays	= 6; 
								$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
                            }
                        }

                        $WPDBean->final_due_date_c = $finalDueDate;
                        $WPDBean->due_date_c = $DueDate;
                        $WPDBean->save();
                    }
                }
            } else if ($bean->functional_area_c == 'Standard Biocompatibility' && $bean->report_timeline_type_c == 'By SPA Reconciliation' && ($bean->bc_spa_reconciled_date_c != "" || $bean->bc_spa_reconciled_date_c != null)) {

                $bean->load_relationship("m03_work_product_m03_work_product_deliverable_1");
                $WPDIDs = $bean->m03_work_product_m03_work_product_deliverable_1->get();

                foreach ($WPDIDs as $wpdId) {
                    $WPDBean = BeanFactory::retrieveBean('M03_Work_Product_Deliverable',  $wpdId);

                    //$GLOBALS['log']->fatal('F deliverable_c :'.$WPDBean->deliverable_c); 
                    if ($WPDBean->deliverable_c == "Final Report") {
                        if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
                            $Days = ' + 0 days';
                            //$DueDays = ' - 7 days';

                            $finalDueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $Days));
                            //$DueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c.$DueDays));
                        } else {
                            $Days = 7 * $bean->final_report_timeline_c;
                            //$DueDays = (7 * $bean->final_report_timeline_c) - 7;

                            $finalDueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . ' + ' . $Days . ' days'));
                            //$DueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c. ' + '.$DueDays.' days'));
                        }

                        if ($num_rows > 0) {
                            if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
                                //$DueDays = ' - 5 days';
                                //$DueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $DueDays));
								$minusDays	= 5; 
								$DueDate	= $this->getDaysCalculation($bean->bc_spa_reconciled_date_c,$minusDays);
								
                            } else {
                                $DueDays = (7 * $bean->final_report_timeline_c) ;//- 5
                                $DueDate1 = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . ' + ' . $DueDays . ' days'));
								$minusDays	= 5; 
								$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
                            }
                        } else {
                            if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
                                //$DueDays = ' - 3 days';
                                //$DueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $DueDays));
								$minusDays	= 3; 
								$DueDate	= $this->getDaysCalculation($bean->bc_spa_reconciled_date_c,$minusDays);
                            } else {
                                $DueDays = (7 * $bean->final_report_timeline_c) ;//- 3
                                $DueDate1 = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . ' + ' . $DueDays . ' days'));
								$minusDays	= 3; 
								$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
                            }
                        }

                        $WPDBean->final_due_date_c = $finalDueDate;
                        $WPDBean->due_date_c = $DueDate;
                        $WPDBean->save();
                    } else if ($WPDBean->deliverable_c == "Histopathology Report") {
                        if ($bean->final_report_timeline_c == '' || $bean->final_report_timeline_c == null) {
                            $Days = ' + 0 days';
                            $DueDays = ' - 6 days';

                            $finalDueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $Days));
                            //$DueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $DueDays));
							$minusDays	= 6; 
							$DueDate	= $this->getDaysCalculation($bean->bc_spa_reconciled_date_c,$minusDays);
                        } else {
                            $Days = 7 * $bean->final_report_timeline_c;
                            $finalDueDate = date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . ' + ' . $Days . ' days'));

                            if ($bean->final_report_timeline_c == 1) {
                                $DueDays	= ' - 1 days';
                                $DueDate	= date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . $DueDays));
								$minusDays	= 1; 
								$DueDate	= $this->getDaysCalculation($bean->bc_spa_reconciled_date_c,$minusDays);
                            } else {
                                $DueDays	= (7 * $bean->final_report_timeline_c) ;//- 6
                                $DueDate1	= date('Y-m-d', strtotime($bean->bc_spa_reconciled_date_c . ' + ' . $DueDays . ' days'));
								$minusDays	= 6; 
								$DueDate	= $this->getDaysCalculation($DueDate1,$minusDays);
                            }
                        }

                        $WPDBean->final_due_date_c = $finalDueDate;
                        $WPDBean->due_date_c = $DueDate;
                        $WPDBean->save();
                    }
                }
            }
        }
    }
	
	function getDaysCalculation($actualDate,$subDays){
		$t = strtotime($actualDate);
		for($i=$subDays; $i>0; $i--){
			$oneDay = 86400;// add 1 day to timestamp
			$nextDay = date('w', ($t-$oneDay));// get what day it is next day
			// if it's Saturday or Sunday get $i-1
			if($nextDay == 0 || $nextDay == 6) {
				$i++;
			}
			$t = $t-$oneDay;// modify timestamp, add 1 day
		} 	
		return date('Y-m-d',$t);		
	}
	
    function get_record_wpd($wpname)
    {
        //$GLOBALS['log']->fatal("num_rows2:WP " . $wpname);
        global $db, $current_user;
        $queryCustom = "SELECT  
		WPD.id AS id		
		FROM `m03_work_product_deliverable` AS WPD
			LEFT JOIN `m03_work_product_deliverable_cstm` AS WPDCSTM 
				ON WPD.id= WPDCSTM.id_c
			RIGHT JOIN m03_work_product_m03_work_product_deliverable_1_c AS WP_WPD  
				ON  WPD.id = WP_WPD.m03_work_pe584verable_idb AND WP_WPD.deleted=0
			LEFT JOIN m03_work_product AS WP
				ON WP_WPD.m03_work_p0b66product_ida=WP.id AND WP.deleted=0
			LEFT JOIN m03_work_product_cstm AS WPCSTM
				ON WP.id=WPCSTM.id_c
		WHERE  WP.name = '" . $wpname . "' 
		AND WPDCSTM.deliverable_c ='Histopathology Report' AND WPD.deleted = 0
		ORDER BY WPDCSTM.due_date_c DESC";
        $queryResult = $db->query($queryCustom);
        $fetchResult = $db->fetchByAssoc($queryResult);
        $num_rows = $queryResult->num_rows;
        //$GLOBALS['log']->fatal("num_rows:WP " . $num_rows);
        return $num_rows;
    }
}
