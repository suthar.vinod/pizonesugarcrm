<?php

class M03_Work_ProductCustomLogicHook {

    function generateSystemIdForWorkProduct($bean, $event, $arguments) {
        global $db, $current_user;
        $isIdExist = $bean->fetched_row['id'];
        $oldWPName = $bean->name;
        //new record is created
        if (!$isIdExist) {
            $workProdCodesID = $bean->m03_work_product_code_id1_c;
            $query = "SELECT name FROM m03_work_product_code WHERE id = '$workProdCodesID' AND deleted = 0 LIMIT 0,1";
            $obj = $db->query($query);
            $res = $db->fetchByAssoc($obj);
            $nameProd = $res['name'];
            $account_id_c = $bean->account_id_c;
            $rel_account_id = $bean->accounts_m03_work_product_1accounts_ida;
            $workId = $bean->id;

            $query = "SELECT client_code_c FROM accounts_cstm WHERE id_c = '$rel_account_id' LIMIT 0,1 ";
            $obj = $db->query($query);
            $res = $db->fetchByAssoc($obj);
            $clientCode = $res['client_code_c'];
            $sequentialNo = '';
            $new_workPordID = '';

            $query1 = "SELECT MAX(CAST(SUBSTRING(name FROM 4) AS UNSIGNED)) AS name FROM m03_work_product INNER JOIN accounts_m03_work_product_1_c ON m03_work_product.id = accounts_m03_work_product_1_c.accounts_m03_work_product_1m03_work_product_idb
			 WHERE accounts_m03_work_product_1_c.accounts_m03_work_product_1accounts_ida = '" . $rel_account_id . "'
                         AND m03_work_product.deleted = '0' 
                         AND accounts_m03_work_product_1_c.deleted = '0'
			 AND m03_work_product.name LIKE '".$clientCode."%'
			 AND m03_work_product.id != '" . $bean->id . "' ";



            //$GLOBALS['log']->fatal("Query for get most recently created WP");
            //$GLOBALS['log']->fatal($query1);
            $_seqNo = $db->query($query1);
            $resultSeqNo = $db->fetchByAssoc($_seqNo);
            $sequentialNo = $resultSeqNo['name'];
            //$GLOBALS['log']->fatal("sequentialNo:" . $sequentialNo);
            if (!empty($sequentialNo)) {
                 
                $new_number = $sequentialNo + 1;
            } else {
                $new_number = 001;
            }

            $number = str_pad(strval($new_number), 3, "0", STR_PAD_LEFT);


            
            if (!empty($clientCode)) {
                $new_workPordID = $clientCode . $number . '-' . $nameProd;
            } else {
                $new_workPordID = $number . '-' . $nameProd;
            }

            $bean->name = $new_workPordID;
        }
    }
}
