<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class WPE_WP_Blanket_Field_Hook {

    public function updateWPBlanket($bean, $event, $arguments) {
        if (isset($arguments['isUpdate']) && $arguments['isUpdate'] == true) {
            global $db, $current_user;

            $WP_id = $bean->id;
            $blanket_id = $bean->m03_work_product_id_c;

            if (isset($arguments['dataChanges']['m03_work_product_id_c'])) {
                
                $newName = "";
                if ($blanket_id != "") {
                    $afterString = $arguments['dataChanges']['m03_work_product_id_c']['after'];
                    $newNameSql = 'SELECT name FROM m03_work_product WHERE id="' . $afterString . '" and deleted=0';
                    $newNameSqlResult = $db->query($newNameSql);
                    if ($newNameSqlResult->num_rows > 0) {
						$newNameRow = $db->fetchByAssoc($newNameSqlResult);
                        $newName = $newNameRow['name'];
                    }
                } 
                
                $query = 'SELECT distinct m03_work_p9bf5ollment_idb AS WPEID FROM m03_work_product_wpe_work_product_enrollment_1_c where m03_work_p7d13product_ida="' . $WP_id . '" and deleted=0';
                $result = $db->query($query);
                if ($result->num_rows > 0) {
                    while ($row = $db->fetchByAssoc($result)) {
                        $WPE_ID = $row['WPEID'];
                        $wpe_work_product_enrollment = BeanFactory::retrieveBean('WPE_Work_Product_Enrollment', $WPE_ID);
                        $wpe_work_product_enrollment->load_relationship("m03_work_product_wpe_work_product_enrollment_2");

                        if ($blanket_id != "") {
                            if ($blanket_id != $wpe_work_product_enrollment->m03_work_p9f23product_ida) {
                                $wpe_work_product_enrollment->m03_work_p9f23product_ida = $blanket_id;
                                $wpe_work_product_enrollment->processed = true;
                                $wpe_work_product_enrollment->save();
                                $wpe_work_product_enrollment->m03_work_product_wpe_work_product_enrollment_2->add($blanket_id);
                            }
                        } else {
                            if ($wpe_work_product_enrollment->m03_work_p9f23product_ida) {
                                $blanket_id_to_delete = $wpe_work_product_enrollment->m03_work_p9f23product_ida;
                                $wpe_work_product_enrollment->processed = true;
                                $wpe_work_product_enrollment->save();
                                $wpe_work_product_enrollment->m03_work_product_wpe_work_product_enrollment_2->delete($WPE_ID, $blanket_id_to_delete);
                            }
                        }
                        
                        $oldBlanketName = "";
                        $beforeString = $arguments['dataChanges']['m03_work_product_id_c']['before'];
                        $auditCheckSql = 'SELECT name FROM `m03_work_product` WHERE id = "' . $beforeString . '" and deleted=0';
                        $auditCheck = $db->query($auditCheckSql);

                        if ($auditCheck->num_rows > 0) {
                            $auditRow = $db->fetchByAssoc($auditCheck);
                            $oldBlanketName = $auditRow['name'];
                        }

                        $event_id = create_guid();
                        $auditsql = 'INSERT INTO wpe_work_product_enrollment_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) values("' . create_guid() . '","' . $WPE_ID . '","' . $event_id . '",now(),"' . $current_user->id . '",now(),"Work Products","varchar","' . $oldBlanketName . '","' . $newName . '")';

                        $auditsqlResult = $db->query($auditsql);
                        
                        if ($auditsqlResult) {
                            //Inserting audit data in audit_events table
                            $source = '{"subject":{"_type":"logic-hook","class":"UpdateWPEBlanket","method":"updateBlanket"},"attributes":[]}';
                            $sql = "INSERT INTO audit_events(id,type,parent_id,module_name,source,data,date_created) values('" . $event_id . "','update','" . $WPE_ID . "','WPE_Work_Product_Enrollment','" . $source . "',NULL,now())";
                            $db->query($sql);
                        }
                    }
                }
            }
        }
    }

}
