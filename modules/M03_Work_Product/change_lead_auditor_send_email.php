<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class send_email_lead_auditor_class
{
	function send_email_lead_auditor_Record($bean, $event, $arguments)
	{

		global $db;
		$site_url = $GLOBALS['sugar_config']['site_url'];
		$workProductId	= "";
		$current_date = date("Y-m-d"); //current date
		//$GLOBALS['log']->fatal('CopyTaskDesignWPHook 14   ===> ' . print_r($arguments, 1));
		
		$Contact_bean = BeanFactory::getBean('Contacts', $bean->contacts_m03_work_product_2contacts_ida);
		
		if ($bean->id == $bean->fetched_row['id'] && $bean->contacts_m03_work_product_2_name != $Contact_bean->name) {			
			
			$WP_ID 		= $bean->id;
			$WP_name 		= $bean->name;
			
			$LUID  = $bean->contacts_m03_work_product_2contacts_ida;
			
			/*Get WP Lead Auditor EMail*/
			
			//Send Email/
			$template = new EmailTemplate();
			$emailObj = new Email();
			
			$Contact_bean = BeanFactory::getBean('Contacts', $LUID);
			$company[] = $Contact_bean->account_name;
			
			//$GLOBALS['log']->fatal("===company==>".print_r($company,1));

			$AuditorEmailAddress = $Contact_bean->emailAddress->getPrimaryAddress($Contact_bean);			

			$template->retrieve_by_string_fields(array('name' => 'Lead Auditor Notification', 'type' => 'email'));

			$WPLink = '<a target="_blank"href="' . $site_url . '/#M03_Work_Product/' . $WP_ID . '">' . $WP_name . '</a>';
			
			$template->body_html = str_replace('[WP_Name]', $WPLink, $template->body_html);
			// You have been assigned as the Author for [insert hyperlink to Work Product Deliverable record using WPD name].  Please review.

			//$GLOBALS['log']->fatal('Email Template:=>' . $template->body_html);

			$defaults	= $emailObj->getSystemDefaultEmail();
			$mail		= new SugarPHPMailer();
			$mail->setMailerForSystem();
			$mail->IsHTML(true);
			$mail->From		= $defaults['email'];
			$mail->FromName = $defaults['name'];
			$mail->Subject	= $template->subject;
			$mail->Body		= $template->body_html;
			$wp_emailAdd_dev[] = 'vsuthar@apsemail.com';

			//$mail->AddAddress('fxs_mjohnson@apsemail.com');
			//$mail->AddAddress('vsuthar@apsemail.com');
			//$company 			= array('american preclinical services','NAMSA');
			
			$mail->AddAddress($AuditorEmailAddress);

			//If their exist some valid email addresses then send email
			if (!empty($wp_emailAdd_dev)) {
				if (!$mail->Send()) {
					$GLOBALS['log']->debug("Fail to Send Email, Please check settings");
				} else {
					$GLOBALS['log']->debug('email sent to Lead Auditor');
				}
				unset($mail);
			}
		}
	}

}
