<?php

if (!defined('sugarEntry') || !sugarEntry)
  die('Not A Valid Entry Point');

class setCustomCalculationAfterRelAddWP
{
  //static $already_ran = false;
  function setCalculationAfterRelAddWP($bean, $event, $arguments)
  {
    // if (self::$already_ran == true)
    // return;   //So that hook will only trigger once
    // self::$already_ran = true;
    global $db, $current_user;

    $bean->load_relationship("m03_work_product_taskd_task_design_1");
    $TaskDesignId = $bean->m03_work_product_taskd_task_design_1->get();

    if (isset($arguments['dataChanges']['first_procedure_c'])) {      
      $datefirst_procedure_c = strtotime($bean->first_procedure_c);

      foreach ($TaskDesignId as $tdID) {
        $tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $tdID);
        $type   = $tdBean->type_2;
        $relative = $tdBean->relative;
        if ($type == 'Actual SP' && $relative == 'NA') {
          $tdBean->actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
          $tdBean->save();
        }
      }

      $currentWPBean = BeanFactory::retrieveBean('M03_Work_Product', $bean->id);
      $currentWPBean->load_relationship('m03_work_product_taskd_task_design_1');
      $relatedTD1 = $currentWPBean->m03_work_product_taskd_task_design_1->get();
      foreach ($relatedTD1 as $tdID) {
        $tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $tdID);
        $u_units = $tdBean->u_units_id_c;
        $unit_Bean = BeanFactory::getBean('U_Units', $u_units);
        $unit_Bean_name = $unit_Bean->name;
        if ($unit_Bean_name == "Day") {
          $integer_u = 'days';
        } elseif ($unit_Bean_name == "Hour") {
          $integer_u = 'hours';
        } else {
          $integer_u = 'minutes';
        }

        if ($tdBean->relative == "NA") {
          //$GLOBALS['log']->fatal('tdBean->id NA Line 50  === ' . $tdBean->id);
          $tdBean1 = BeanFactory::retrieveBean('TaskD_Task_Design', $tdBean->id);
          $tdBean1->load_relationship('taskd_task_design_taskd_task_design_1');
          $taskDesignId1 = $tdBean1->taskd_task_design_taskd_task_design_1->get();
          $Get_1st_tier = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_idb FROM  `taskd_task_design_taskd_task_design_1_c` WHERE `taskd_task_design_taskd_task_design_1taskd_task_design_ida` = '" . $tdBean->id . "' AND deleted = 0";
          $Get_Result = $db->query($Get_1st_tier);

          if ($Get_Result->num_rows > 0) {
            while ($fetchResult = $db->fetchByAssoc($Get_Result)) {
              $recordID = $fetchResult['taskd_task_design_taskd_task_design_1taskd_task_design_idb'];
              //$GLOBALS['log']->fatal(' recordID ' . $recordID);
              $tdBean2 = BeanFactory::retrieveBean('TaskD_Task_Design', $recordID);
              //$GLOBALS['log']->fatal(' tdBean->planned_start_datetime_1st ' . $tdBean->planned_start_datetime_1st);
              if ($tdBean->planned_start_datetime_1st != "") {
                $result = date($tdBean->planned_start_datetime_1st);
                $result = strtotime($result . '+' . $tdBean2->start_integer . ' ' . $integer_u);
                if (date("Y", $result) != "1970") {
                  $tdBean2->single_date_2_c = date("Y-m-d", $result);
                  //$GLOBALS['log']->fatal(' single_date_2_c WP 2nd Tier ' . $tdBean2->single_date_2_c);
                }
              }
              
              $tdBean2->save();
            }
          }
        }

        if ($tdBean->relative == "1st Tier") {
          $tdBean1 = BeanFactory::retrieveBean('TaskD_Task_Design', $tdBean->id);
          $tdBean1->load_relationship('taskd_task_design_taskd_task_design_1');
          $taskDesignId1 = $tdBean1->taskd_task_design_taskd_task_design_1->get();
          $Get_2nd_tier = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_idb FROM  `taskd_task_design_taskd_task_design_1_c` WHERE `taskd_task_design_taskd_task_design_1taskd_task_design_ida` = '" . $tdBean->id . "' AND deleted = 0;";
          $Get_Result = $db->query($Get_2nd_tier);

          if ($Get_Result->num_rows > 0) {
            while ($fetchResult = $db->fetchByAssoc($Get_Result)) {
              $recordID = $fetchResult['taskd_task_design_taskd_task_design_1taskd_task_design_idb'];
              $tdBean2 = BeanFactory::retrieveBean('TaskD_Task_Design', $recordID);
              if ($tdBean->planned_start_datetime_1st != "") {
                $result = date($tdBean->planned_start_datetime_1st);
                $result = strtotime($result . '+' . $tdBean2->start_integer . ' ' . $integer_u);
                if (date("Y", $result) != "1970") {
                  $tdBean2->single_date_2_c = date("Y-m-d", $result);
                }
              } 
              $tdBean2->save();
            }
          }
        }
      }
    }
  }
}
