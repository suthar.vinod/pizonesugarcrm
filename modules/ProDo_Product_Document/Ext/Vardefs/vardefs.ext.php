<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ProDo_Product_Document/Ext/Vardefs/prod_product_prodo_product_document_1_ProDo_Product_Document.php

// created: 2020-09-10 08:30:09
$dictionary["ProDo_Product_Document"]["fields"]["prod_product_prodo_product_document_1"] = array (
  'name' => 'prod_product_prodo_product_document_1',
  'type' => 'link',
  'relationship' => 'prod_product_prodo_product_document_1',
  'source' => 'non-db',
  'module' => 'Prod_Product',
  'bean_name' => 'Prod_Product',
  'side' => 'right',
  'vname' => 'LBL_PROD_PRODUCT_PRODO_PRODUCT_DOCUMENT_1_FROM_PRODO_PRODUCT_DOCUMENT_TITLE',
  'id_name' => 'prod_product_prodo_product_document_1prod_product_ida',
  'link-type' => 'one',
);
$dictionary["ProDo_Product_Document"]["fields"]["prod_product_prodo_product_document_1_name"] = array (
  'name' => 'prod_product_prodo_product_document_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROD_PRODUCT_PRODO_PRODUCT_DOCUMENT_1_FROM_PROD_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'prod_product_prodo_product_document_1prod_product_ida',
  'link' => 'prod_product_prodo_product_document_1',
  'table' => 'prod_product',
  'module' => 'Prod_Product',
  'rname' => 'name',
);
$dictionary["ProDo_Product_Document"]["fields"]["prod_product_prodo_product_document_1prod_product_ida"] = array (
  'name' => 'prod_product_prodo_product_document_1prod_product_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROD_PRODUCT_PRODO_PRODUCT_DOCUMENT_1_FROM_PRODO_PRODUCT_DOCUMENT_TITLE_ID',
  'id_name' => 'prod_product_prodo_product_document_1prod_product_ida',
  'link' => 'prod_product_prodo_product_document_1',
  'table' => 'prod_product',
  'module' => 'Prod_Product',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
