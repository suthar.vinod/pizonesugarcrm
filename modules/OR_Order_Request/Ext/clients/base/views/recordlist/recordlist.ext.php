<?php
// WARNING: The contents of this file are auto-generated.



$viewdefs['OR_Order_Request']['base']['view']['recordlist']['selection']['actions'][] = array(
    'name' => 'copy_order_request_button',
    'label' => 'LBL_COPY_ORDER_REQUEST_BUTTON',
    'type' => 'button',
    'primary' => true,
    'events' => array(
        'click' => 'list:copyorderrequestrecords:fire',
    ),
    'acl_action' => 'copyorderrequest',
);
