<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/OR_Order_Request/Ext/Layoutdefs/or_order_request_ori_order_request_item_1_OR_Order_Request.php

 // created: 2021-10-19 09:18:33
$layout_defs["OR_Order_Request"]["subpanel_setup"]['or_order_request_ori_order_request_item_1'] = array (
  'order' => 100,
  'module' => 'ORI_Order_Request_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'get_subpanel_data' => 'or_order_request_ori_order_request_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/OR_Order_Request/Ext/Layoutdefs/_overrideOR_Order_Request_subpanel_or_order_request_ori_order_request_item_1.php

//auto-generated file DO NOT EDIT
$layout_defs['OR_Order_Request']['subpanel_setup']['or_order_request_ori_order_request_item_1']['override_subpanel_name'] = 'OR_Order_Request_subpanel_or_order_request_ori_order_request_item_1';

?>
