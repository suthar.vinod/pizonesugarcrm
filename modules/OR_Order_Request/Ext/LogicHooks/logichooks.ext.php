<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/OR_Order_Request/Ext/LogicHooks/setRequestByinORI.php

$hook_array['after_save'][] = Array(
    '20',
    'Set Request By name in ORI records',
    'custom/modules/OR_Order_Request/SetRequestByinORI.php',
    'SetRequestByinORI',
    'SetRequestBy_ori',
);


?>
<?php
// Merged from custom/Extension/modules/OR_Order_Request/Ext/LogicHooks/checkRequestByHook.php

$hook_array['before_save'][] = Array(
    '20',
    'Check Request By value',
    'custom/modules/OR_Order_Request/checkRequestByValue.php',
    'checkRequestByValue',
    'checkRequestBy',
);


?>
