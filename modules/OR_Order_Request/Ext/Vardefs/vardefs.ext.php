<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/OR_Order_Request/Ext/Vardefs/or_order_request_ori_order_request_item_1_OR_Order_Request.php

// created: 2021-03-03 11:30:08
$dictionary["OR_Order_Request"]["fields"]["or_order_request_ori_order_request_item_1"] = array (
  'name' => 'or_order_request_ori_order_request_item_1',
  'type' => 'link',
  'relationship' => 'or_order_request_ori_order_request_item_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'vname' => 'LBL_OR_ORDER_REQUEST_ORI_ORDER_REQUEST_ITEM_1_FROM_OR_ORDER_REQUEST_TITLE',
  'id_name' => 'or_order_request_ori_order_request_item_1or_order_request_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/OR_Order_Request/Ext/Vardefs/sugarfield_name.php


 // created: 2020-07-30 09:06:26
$dictionary['OR_Order_Request']['fields']['name']['required']=false;
$dictionary['OR_Order_Request']['fields']['name']['unified_search']=false;
$dictionary['OR_Order_Request']['fields']['name']['readonly']=true;

?>
<?php
// Merged from custom/Extension/modules/OR_Order_Request/Ext/Vardefs/sugarfield_status_c.php

 // created: 2021-12-28 07:21:03
$dictionary['OR_Order_Request']['fields']['status_c']['labelValue']='Status';
$dictionary['OR_Order_Request']['fields']['status_c']['dependency']='';
$dictionary['OR_Order_Request']['fields']['status_c']['required_formula']='';
$dictionary['OR_Order_Request']['fields']['status_c']['readonly_formula']='';
$dictionary['OR_Order_Request']['fields']['status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/OR_Order_Request/Ext/Vardefs/sugarfield_notes_c.php

 // created: 2022-03-31 11:41:45
$dictionary['OR_Order_Request']['fields']['notes_c']['labelValue']='Notes';
$dictionary['OR_Order_Request']['fields']['notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['OR_Order_Request']['fields']['notes_c']['enforced']='';
$dictionary['OR_Order_Request']['fields']['notes_c']['dependency']='';
$dictionary['OR_Order_Request']['fields']['notes_c']['required_formula']='';
$dictionary['OR_Order_Request']['fields']['notes_c']['readonly']='1';
$dictionary['OR_Order_Request']['fields']['notes_c']['readonly_formula']='not(equal($status_c,"Open"))';

 
?>
