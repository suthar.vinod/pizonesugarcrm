<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class SetRequestByinORI
{
    function SetRequestBy_ori($bean, $event, $arguments) {
        global $db; 
        if($bean->updateORI==1) 
        {
            $ori_ids = $bean->or_order_request_ori_order_request_item_1->get();
            if(count($ori_ids)>0){
                foreach ($ori_ids as $ORIId) {
                    $oriBean = \BeanFactory::getBean('ORI_Order_Request_Item', $ORIId);
                    $oriBean->save();
                }
            }
        }        
    }
}
