<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class SetORStatusHook
{

    public function setORStatus($bean, $event, $arguments) {
        $module = $arguments['module'];
        $modelId = $arguments['id'];

        $relatedModule = $arguments['related_module'];
        $relatedId = $arguments['related_id'];
 
        //$GLOBALS['log']->fatal('setORStatus1122 arguments  ' . print_r($arguments,1));
        if ($module === 'OR_Order_Request' && $relatedModule === 'ORI_Order_Request_Item') {
            $this->calcStatus($bean, $modelId, $event, $arguments);
        } else if ($relatedModule === 'ORI_Order_Request_Item' && $module === 'OR_Order_Request') {
            $this->calcStatus($bean, $relatedId, $event, $arguments);
        }
    }

    public function calcStatus($bean, $id, $event, $arguments) {
        global $db;
         
        $OR_Order_Request = BeanFactory::retrieveBean('OR_Order_Request', $id); 
    
        $OR_Order_Request->load_relationship('or_order_request_ori_order_request_item_1');
        $ORIids = $OR_Order_Request->or_order_request_ori_order_request_item_1->get();
        
         
        if($event != 'after_relationship_delete'){
            array_push($ORIids,$arguments['related_id']);
            $ORIids = array_unique($ORIids);
        }

        $po_id = "";
        $POArray = array();
        for($a=0;$a<count($ORIids);$a++){
            $ORI_Order_Request_Item = BeanFactory::retrieveBean('ORI_Order_Request_Item', $ORIids[$a]);
            $po_id = $ORI_Order_Request_Item->po_purchase_order_ori_order_request_item_1po_purchase_order_ida;
            
            if($po_id){                   
                array_push($POArray, $po_id);
            }else{
                array_push($POArray, "");
            }
            
        }
        $POArray1 = array_unique($POArray);
        //$GLOBALS['log']->fatal('POArray : '.print_r($POArray1,1));
        if(!empty($POArray1)){
            if(in_array("",$POArray1)) {

                $POArrayString = implode(" ",$POArray1);
                if( trim( $POArrayString ) == "" ){
                    $OR_Order_Request->status_c = "Open";
                    $OR_Order_Request->save();
                }else {
                    $OR_Order_Request->status_c = "Partially Submitted";
                    $OR_Order_Request->save();
                }                                
            } else {
                $OR_Order_Request->status_c = "Fully Submitted";
                $OR_Order_Request->save();
            }
        }else{
            $OR_Order_Request->status_c = "Open";
            $OR_Order_Request->save();
        }
    }

    public function updateORStatusFromORILinking($bean, $event, $arguments) {
        global $db;
        //$GLOBALS['log']->fatal('args Lax ' . print_r($arguments,1));
        if(isset($arguments['isUpdate']) && $arguments['isUpdate'] == true){
            $bean->load_relationship('or_order_request_ori_order_request_item_1');
            $ORIids = $bean->or_order_request_ori_order_request_item_1->get();
            
            $po_id = "";
            $POArray = array();
            for($a=0;$a<count($ORIids);$a++){
                $ORI_Order_Request_Item = BeanFactory::retrieveBean('ORI_Order_Request_Item', $ORIids[$a]);
                $po_id = $ORI_Order_Request_Item->po_purchase_order_ori_order_request_item_1po_purchase_order_ida;
                
                if($po_id){                   
                    array_push($POArray, $po_id);
                }else{
                    array_push($POArray, "");
                }            
            }

            $POArray1 = array_unique($POArray);
            if(!empty($POArray1)){
                if(in_array("",$POArray1)) {
                    $POArrayString = implode(" ",$POArray1);
                    if( trim( $POArrayString ) == "" ){
                        if($bean->status_c == "Open" || $bean->status_c == "Complete"){
                            $bean->status_c = $bean->status_c;
                        }                        
                    }else {
                        $bean->status_c = "Partially Submitted";
                    }                                
                } else {
                    $bean->status_c = "Fully Submitted";
                }
            }else{
                if($bean->status_c == "Open" || $bean->status_c == "Complete"){
                    $bean->status_c = $bean->status_c;
                }
            }
        }
    }
}
