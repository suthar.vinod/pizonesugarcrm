<?php
	$hook_version = 1;
	$hook_array = Array();
	// position, file, function 
	$hook_array['before_save'] = Array(); 
	$hook_array['before_save'][] = Array('1','OR Name Creation','custom/modules/OR_Order_Request/createORName.php','createORName','createORAutoName');

	$hook_array['after_relationship_add'][] = array(
		11,
		'After relating a ORI record resave the bean so the status field is calculated',
		'custom/modules/OR_Order_Request/SetORStatus.php',
		'SetORStatusHook',
		'setORStatus',
	);
	
	$hook_array['after_relationship_delete'][] = array(
		14,
		'After relating a ORI record resave the bean so the status field is calculated',
		'custom/modules/OR_Order_Request/SetORStatus.php',
		'SetORStatusHook',
		'setORStatus',
	);

	$hook_array['before_save'][] = array(
		7,
		'Update OR status according ORI subpanel if PO is linked',
		'custom/modules/OR_Order_Request/SetORStatus.php',
		'SetORStatusHook',
		'updateORStatusFromORILinking',
	);
?>