<?php
class createORName{
	static $already_ran = false;  // To make sure LogicHooks will execute just onces
    function createORAutoName($bean, $event, $arguments) {
		if (self::$already_ran == true) return;   //So that hook will only trigger once
        self::$already_ran = true;
        global $db;
		
		$current_year = date("y");
        if ($arguments['isUpdate'] == false) {
			$query = "SELECT MAX(CAST(SUBSTRING(name,6,4) AS UNSIGNED)) AS or_name FROM or_order_request WHERE name LIKE 'OR{$current_year}%' and deleted = 0 ";
			$result = $db->query($query);
			$row = $db->fetchByAssoc($result);
			$system_id = $row['or_name'];
			$GLOBALS['log']->fatal("system id is ". $system_id);
			
			if (!empty($system_id)) {
				$number = (int) $system_id;
				$number = $number + 1;
			}else{
				$number = 1;
			}
			
			$nextNumber  = str_pad($number,4,"0",STR_PAD_LEFT);
			$code .= 'OR'.$current_year.'-'.$nextNumber;
				
			if ($code != "") {
				$bean->name = $code;
			}


		}	

	}
}