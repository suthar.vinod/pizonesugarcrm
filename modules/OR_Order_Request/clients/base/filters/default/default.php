<?php
// created: 2022-03-24 13:43:16
$viewdefs['OR_Order_Request']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'assigned_user_name' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'department' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'name' => 
    array (
    ),
    'notes_c' => 
    array (
    ),
    'request_by' => 
    array (
    ),
    'request_date' => 
    array (
    ),
    'status_c' => 
    array (
    ),
  ),
);