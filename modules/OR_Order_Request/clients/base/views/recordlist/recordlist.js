/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @extends View.Views.Base.RecordlistView
 */
({
    extendsFrom: 'RecordlistView',

    _bindEvents: function () {
        var self = this;

        self._super("_bindEvents");

        if (self.layout) {
            self.layout.on('list:copyorderrequestrecords:fire', self.copyorderrequestrecordsClicked, self);

        }
    },
    copyorderrequestrecordsClicked: function () {
        var self = this;
        var openDrawer = 1;
        var tsAssignedWPArr = [];
        var tsAllocatedWPArr = [];

        var self = this;
        app.alert.show('message-id', {
            level: 'confirmation',
            messages: "Are you sure you want to copy these OR's and the linked ORI record(s)?",
            autoClose: false,
            onConfirm: function () {
                console.log("Start Process");
               app.alert.show('inProgress-id', {
                                level: 'process',
                                title: 'In Process...' //change title to modify display from 'Loading...'
                            });
                            var orderID = [];
                _.each(self.fields, function (field) {
                    if (field.$el.find('input[data-check="one"]:checked').length > 0) {
                        var tsName = field.model.get('name');
                        
                        orderID.push(field.model.get('id'));
                    }
                });

                console.log(orderID);

                setTimeout(function(){ 
                            
                    app.api.call("create", "rest/v10/copy_order_request", {
                        recordId: orderID,
                    }, {
                        success: function (Data) {
                            console.log(Data);
                            if (Data == 1) {
                                app.alert.dismiss('inProgress-id');
                                app.alert.show('or_copied', {
                                    level: 'success',
                                    messages: 'Selected Order Request has been copied successfully!',
                                    autoClose: true
                
                                });
                            }
                        },
                        error: function (error) {
                            console.log(error);
                            app.alert.dismiss('inProgress-id');
                            app.alert.show('or_copied', {
                                level: 'error',
                                messages: error
                            });
                        }
                    });
                 }, 5000);
                //$('.refresh').trigger('click');
                //this.context.on('button:refreshList:click', this.get_cost, this);

                
                
            },
            onCancel: function () { console.log("Cancelled."); },
        });

    },
})