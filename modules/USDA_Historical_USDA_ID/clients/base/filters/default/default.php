<?php
// created: 2021-03-04 09:19:35
$viewdefs['USDA_Historical_USDA_ID']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'assigned_user_name' => 
    array (
    ),
    'date_of_reidentification' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'name' => 
    array (
    ),
    'description' => 
    array (
    ),
    'old_usda_id_c' => 
    array (
    ),
    'anml_animals_usda_historical_usda_id_1_name' => 
    array (
    ),
  ),
);