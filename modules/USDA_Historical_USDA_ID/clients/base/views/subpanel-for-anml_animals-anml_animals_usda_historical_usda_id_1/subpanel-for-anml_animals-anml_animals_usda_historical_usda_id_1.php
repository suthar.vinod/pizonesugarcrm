<?php
// created: 2021-03-09 05:06:05
$viewdefs['USDA_Historical_USDA_ID']['base']['view']['subpanel-for-anml_animals-anml_animals_usda_historical_usda_id_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'old_usda_id_c',
          'label' => 'LBL_OLD_USDA_ID',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'date_of_reidentification',
          'label' => 'LBL_DATE_OF_REIDENTIFICATION',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'readonly' => false,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'anml_animals_usda_historical_usda_id_1_name',
          'label' => 'LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_ANML_ANIMALS_TITLE',
          'enabled' => true,
          'id' => 'ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1ANML_ANIMALS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);