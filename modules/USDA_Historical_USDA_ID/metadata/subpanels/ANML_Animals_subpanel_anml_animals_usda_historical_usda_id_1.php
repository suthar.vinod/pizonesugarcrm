<?php
// created: 2021-03-09 05:06:03
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'old_usda_id_c' => 
  array (
    'readonly_formula' => '',
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_OLD_USDA_ID',
    'width' => 10,
    'default' => true,
  ),
  'date_of_reidentification' => 
  array (
    'readonly' => false,
    'type' => 'date',
    'vname' => 'LBL_DATE_OF_REIDENTIFICATION',
    'width' => 10,
    'default' => true,
  ),
  'description' => 
  array (
    'type' => 'text',
    'readonly' => false,
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
  ),
  'anml_animals_usda_historical_usda_id_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_ANML_ANIMALS_TITLE',
    'id' => 'ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1ANML_ANIMALS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'ANML_Animals',
    'target_record_key' => 'anml_animals_usda_historical_usda_id_1anml_animals_ida',
  ),
);