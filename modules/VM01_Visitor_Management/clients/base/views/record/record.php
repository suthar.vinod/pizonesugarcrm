<?php
$module_name = 'VM01_Visitor_Management';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'VM01_Visitor_Management',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'first_name_c',
                'label' => 'LBL_FIRST_NAME',
              ),
              1 => 
              array (
                'name' => 'last_name_c',
                'label' => 'LBL_LAST_NAME',
              ),
              2 => 
              array (
                'name' => 'company_c',
                'label' => 'LBL_COMPANY',
              ),
              3 => 
              array (
                'name' => 'email_address_c',
                'label' => 'LBL_EMAIL_ADDRESS',
              ),
              4 => 
              array (
                'name' => 'office_address_c',
                'label' => 'LBL_OFFICE_ADDRESS',
              ),
              5 => 
              array (
                'name' => 'office_phone_c',
                'label' => 'LBL_OFFICE_PHONE',
              ),
              6 => 
              array (
                'name' => 'meetingwith_location_c',
                'label' => 'LBL_MEETINGWITH_LOCATION',
              ),
              7 => 
              array (
                'name' => 'visit_status_c',
                'label' => 'LBL_VISIT_STATUS',
              ),
              8 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              9 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              10 => 
              array (
                'name' => 'experience_rating',
              ),
              11 => 
              array (
              ),
              12 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'symptoms_past_48_hrs_c',
                'label' => 'LBL_SYMPTOMS_PAST_48_HRS',
                'span' => 12,
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'explain_symptoms_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLAIN_SYMPTOMS',
                'span' => 12,
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'isolatingquarantining_c',
                'label' => 'LBL_ISOLATINGQUARANTINING',
                'span' => 12,
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'explain_isolationquarantine_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLAIN_ISOLATIONQUARANTINE',
                'span' => 12,
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'physical_contact_c',
                'label' => 'LBL_PHYSICAL_CONTACT',
                'span' => 12,
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'explain_contact_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLAIN_CONTACT',
                'span' => 12,
              ),
              6 => 
              array (
                'name' => 'approved_visit_c',
                'label' => 'LBL_APPROVED_VISIT',
              ),
              7 => 
              array (
                'name' => 'manager_reviewed_c',
                'label' => 'LBL_MANAGER_REVIEWED',
              ),
              8 => 
              array (
                'name' => 'visitor_approval_notes_c',
                'studio' => 'visible',
                'label' => 'LBL_VISITOR_APPROVAL_NOTES',
                'span' => 12,
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => false,
            'panelDefault' => 'collapsed',
            'name' => 'LBL_RECORDVIEW_PANEL4',
            'label' => 'LBL_RECORDVIEW_PANEL4',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'mandatory_presence_c',
                'label' => 'LBL_MANDATORY_PRESENCE',
              ),
              1 => 
              array (
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'explanation_presence_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLANATION_PRESENCE',
                'span' => 12,
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'outside_us_c',
                'label' => 'LBL_OUTSIDE_US',
              ),
              4 => 
              array (
              ),
              5 => 
              array (
                'name' => 'explanation_outside_us_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLANATION_OUTSIDE_US',
                'span' => 12,
              ),
              6 => 
              array (
                'readonly' => false,
                'name' => 'contact_covid_19_c',
                'label' => 'LBL_CONTACT_COVID_19',
              ),
              7 => 
              array (
              ),
              8 => 
              array (
                'name' => 'explanation_contact_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLANATION_CONTACT',
                'span' => 12,
              ),
              9 => 
              array (
                'name' => 'test_result_quarantine_c',
                'label' => 'LBL_TEST_RESULT_QUARANTINE',
              ),
              10 => 
              array (
              ),
              11 => 
              array (
                'name' => 'explanation_testquarantine_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLANATION_TESTQUARANTINE',
                'span' => 12,
              ),
              12 => 
              array (
                'readonly' => false,
                'name' => 'acknowledgement_no_symptoms_c',
                'label' => 'LBL_ACKNOWLEDGEMENT_NO_SYMPTOMS',
              ),
              13 => 
              array (
                'name' => 'workplace_process_c',
                'label' => 'LBL_WORKPLACE_PROCESS',
              ),
              14 => 
              array (
                'name' => 'visitor_confirmed_case_c',
                'label' => 'LBL_VISITOR_CONFIRMED_CASE',
              ),
              15 => 
              array (
              ),
              16 => 
              array (
                'name' => 'explanation_for_confirmed_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLANATION_FOR_CONFIRMED',
                'span' => 12,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
