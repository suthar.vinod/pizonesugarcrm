<?php
$module_name = 'VM01_Visitor_Management';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'first_name_c',
                'label' => 'LBL_FIRST_NAME',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'last_name_c',
                'label' => 'LBL_LAST_NAME',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'company_c',
                'label' => 'LBL_COMPANY',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'email_address_c',
                'label' => 'LBL_EMAIL_ADDRESS',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'aps_host_procedure_room_c',
                'label' => 'LBL_APS_HOST_PROCEDURE_ROOM',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'visit_status_c',
                'label' => 'LBL_VISIT_STATUS',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'experience_rating',
                'label' => 'LBL_RATING',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              12 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
