<?php
$module_name = 'VM01_Visitor_Management';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'first_name_c',
                'label' => 'LBL_FIRST_NAME',
              ),
              1 => 
              array (
                'name' => 'last_name_c',
                'label' => 'LBL_LAST_NAME',
              ),
              2 => 
              array (
                'name' => 'company_c',
                'label' => 'LBL_COMPANY',
              ),
              3 => 
              array (
                'name' => 'email_address_c',
                'label' => 'LBL_EMAIL_ADDRESS',
              ),
              4 => 
              array (
                'name' => 'office_address_c',
                'label' => 'LBL_OFFICE_ADDRESS',
              ),
              5 => 
              array (
                'name' => 'office_phone_c',
                'label' => 'LBL_OFFICE_PHONE',
              ),
              6 => 
              array (
                'name' => 'meetingwith_location_c',
                'label' => 'LBL_MEETINGWITH_LOCATION',
              ),
              7 => 
              array (
                'name' => 'visit_status_c',
                'label' => 'LBL_VISIT_STATUS',
              ),
              8 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              9 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              10 => 
              array (
                'name' => 'experience_rating',
              ),
              11 => 
              array (
                'span' => 12,
              ),
              12 => 
              array (
                'name' => 'description',
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'mandatory_presence_c',
                'label' => 'LBL_MANDATORY_PRESENCE',
              ),
              1 => 
              array (
                'name' => 'explanation_presence_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLANATION_PRESENCE',
              ),
              2 => 
              array (
                'name' => 'outside_us_c',
                'label' => 'LBL_OUTSIDE_US',
              ),
              3 => 
              array (
                'name' => 'explanation_outside_us_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLANATION_OUTSIDE_US',
              ),
              4 => 
              array (
                'name' => 'contact_covid_19_c',
                'label' => 'LBL_CONTACT_COVID_19',
              ),
              5 => 
              array (
                'name' => 'explanation_contact_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLANATION_CONTACT',
              ),
              6 => 
              array (
                'name' => 'test_result_quarantine_c',
                'label' => 'LBL_TEST_RESULT_QUARANTINE',
              ),
              7 => 
              array (
                'name' => 'explanation_testquarantine_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLANATION_TESTQUARANTINE',
              ),
              8 => 
              array (
                'name' => 'workplace_process_c',
                'label' => 'LBL_WORKPLACE_PROCESS',
              ),
              9 => 
              array (
                'name' => 'visitor_confirmed_case_c',
                'label' => 'LBL_VISITOR_CONFIRMED_CASE',
              ),
              10 => 
              array (
                'name' => 'explanation_for_confirmed_c',
                'studio' => 'visible',
                'label' => 'LBL_EXPLANATION_FOR_CONFIRMED',
              ),
              11 => 
              array (
                'name' => 'approved_visit_c',
                'label' => 'LBL_APPROVED_VISIT',
              ),
              12 => 
              array (
                'name' => 'manager_reviewed_c',
                'label' => 'LBL_MANAGER_REVIEWED',
              ),
              13 => 
              array (
                'name' => 'visitor_approval_notes_c',
                'studio' => 'visible',
                'label' => 'LBL_VISITOR_APPROVAL_NOTES',
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);
