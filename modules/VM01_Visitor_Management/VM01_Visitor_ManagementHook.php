<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * @class: VM01_Visitor_ManagementHook
 * @description: Class for implementing all VM01_Visitor_ManagementHook related hooks
 * All the logic hooks related with VM01_Visitor_ManagementHook module is defined in this class.
 */
class VM01_Visitor_ManagementHook
{
    /**
     * Function: beforeSave
     *
     * All before_save logic hooks is inside this function.
     *
     * @param object $bean
     * @param string $event
     * @param array $arguments
     */
    public function beforeSave($bean, $event, $arguments)
    {
        // for newly created records only
        if ($bean->id != $bean->fetched_row['id']) {
            if (empty($bean->name)) {
                $bean->name = $this->initalizeAutoIncrementNumber($bean->table_name);
            }
        }
    }
    /**
     * For doing auto-increment logic on Name field
     * @param type $api
     * @param type $args
     * @return string
     */
    public function initalizeAutoIncrementNumber($tableName)
    {
        // get years in 2 digit
        $date = date('y');
        $prefix = 'VM'.$date;
        /*
         * Adding addition of extracting Integer portion
         * from Name(text) field and doing increment in
         * number as per logic
         */
        //  Lock Table
        $sql_lock = "LOCK TABLES {$tableName} READ";
        $GLOBALS['db']->query($sql_lock);

        $query = "SELECT MAX(SUBSTRING(name, 6, length(name)-2)) AS number
                    FROM ".$tableName."
                WHERE deleted = 0 AND name LIKE '".$prefix."-%' AND name REGEXP ('[0-9]') LIMIT 0,1";
        $result = $GLOBALS['db']->query($query);

        //  Unlock Table
        $sql_unlock = "UNLOCK TABLES";
        $GLOBALS['db']->query($sql_unlock);
        /*
         * If found any max number per same year
         * then increment it else intalize it as
         * new number for year VM{YY}-XXXXX
         */
        if ($GLOBALS['db']->getRowCount($result) > 0) {
            $row = $GLOBALS['db']->fetchByAssoc($result);
            $number = $row['number']+1;
            $name = $prefix.'-'.str_pad($number,5,"0",STR_PAD_LEFT);
        } else {
            $name = $prefix.'-00001';
        }
        $GLOBALS['log']->debug('Auto Increment Name Val :: ',$name);
        return $name;
    }
}
