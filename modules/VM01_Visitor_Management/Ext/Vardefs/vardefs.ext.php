<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/office_address_c.php


$dictionary['VM01_Visitor_Management']['fields']['office_address_c'] = array(
    'name' => 'office_address_c',
    'vname' => 'LBL_OFFICE_ADDRESS',
    'type' => 'text',
    'dbType' => 'varchar',
    'len' => 150,
);

?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/experience_rating.php


$dictionary['VM01_Visitor_Management']['fields']['experience_rating'] = array(
    'name' => 'experience_rating',
    'vname' => 'LBL_RATING',
    'type' => 'int',
);

?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/office_phone_c.php


$dictionary['VM01_Visitor_Management']['fields']['office_phone_c'] = array(
    'name' => 'office_phone_c',
    'vname' => 'LBL_OFFICE_PHONE',
    'type' => 'varchar',
    'len' => 20,
);

?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_name.php

 // created: 2019-08-26 18:17:56
$dictionary['VM01_Visitor_Management']['fields']['name']['required']=false;
$dictionary['VM01_Visitor_Management']['fields']['name']['readonly']=true;
$dictionary['VM01_Visitor_Management']['fields']['name']['len']='255';
$dictionary['VM01_Visitor_Management']['fields']['name']['audited']=true;
$dictionary['VM01_Visitor_Management']['fields']['name']['massupdate']=false;
$dictionary['VM01_Visitor_Management']['fields']['name']['unified_search']=false;
$dictionary['VM01_Visitor_Management']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['VM01_Visitor_Management']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_aps_host_procedure_room_c.php

 // created: 2019-08-26 18:18:49
$dictionary['VM01_Visitor_Management']['fields']['aps_host_procedure_room_c']['labelValue']='APS Host/Procedure Room';
$dictionary['VM01_Visitor_Management']['fields']['aps_host_procedure_room_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['aps_host_procedure_room_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['aps_host_procedure_room_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_company_c.php

 // created: 2019-08-26 18:19:40
$dictionary['VM01_Visitor_Management']['fields']['company_c']['labelValue']='Company';
$dictionary['VM01_Visitor_Management']['fields']['company_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['company_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['company_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_email_address_c.php

 // created: 2019-08-27 11:57:28
$dictionary['VM01_Visitor_Management']['fields']['email_address_c']['labelValue']='Email Address';
$dictionary['VM01_Visitor_Management']['fields']['email_address_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['email_address_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['email_address_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_first_name_c.php

 // created: 2019-08-27 11:58:21
$dictionary['VM01_Visitor_Management']['fields']['first_name_c']['labelValue']='First Name';
$dictionary['VM01_Visitor_Management']['fields']['first_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['first_name_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['first_name_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_last_name_c.php

 // created: 2019-08-27 11:58:51
$dictionary['VM01_Visitor_Management']['fields']['last_name_c']['labelValue']='Last Name';
$dictionary['VM01_Visitor_Management']['fields']['last_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['last_name_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['last_name_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_visit_status_c.php

 // created: 2019-08-27 12:00:07
$dictionary['VM01_Visitor_Management']['fields']['visit_status_c']['labelValue']='Visit Status';
$dictionary['VM01_Visitor_Management']['fields']['visit_status_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['visit_status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_experience_rating_c.php

 // created: 2019-08-27 12:01:22
$dictionary['VM01_Visitor_Management']['fields']['experience_rating_c']['labelValue']='Experience Rating';
$dictionary['VM01_Visitor_Management']['fields']['experience_rating_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['experience_rating_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['experience_rating_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_office_address_c.php

 // created: 2019-08-27 12:02:44
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['audited']=true;
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['massupdate']=false;
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['duplicate_merge']='enabled';
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['duplicate_merge_dom_value']='1';
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['merge_filter']='disabled';
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['unified_search']=false;
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['calculated']=false;
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['rows']='4';
$dictionary['VM01_Visitor_Management']['fields']['office_address_c']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_office_phone_c.php

 // created: 2019-08-27 12:03:36
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['len']='20';
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['audited']=true;
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['massupdate']=false;
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['duplicate_merge']='enabled';
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['duplicate_merge_dom_value']='1';
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['merge_filter']='disabled';
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['unified_search']=false;
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['office_phone_c']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_description.php

 // created: 2019-08-27 12:04:58
$dictionary['VM01_Visitor_Management']['fields']['description']['audited']=true;
$dictionary['VM01_Visitor_Management']['fields']['description']['massupdate']=false;
$dictionary['VM01_Visitor_Management']['fields']['description']['comments']='Full text of the note';
$dictionary['VM01_Visitor_Management']['fields']['description']['duplicate_merge']='enabled';
$dictionary['VM01_Visitor_Management']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['VM01_Visitor_Management']['fields']['description']['merge_filter']='disabled';
$dictionary['VM01_Visitor_Management']['fields']['description']['unified_search']=false;
$dictionary['VM01_Visitor_Management']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['VM01_Visitor_Management']['fields']['description']['calculated']=false;
$dictionary['VM01_Visitor_Management']['fields']['description']['rows']='6';
$dictionary['VM01_Visitor_Management']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_approved_visit_c.php

 // created: 2020-10-13 11:11:55
$dictionary['VM01_Visitor_Management']['fields']['approved_visit_c']['labelValue']='Approved to Visit APS?';
$dictionary['VM01_Visitor_Management']['fields']['approved_visit_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['approved_visit_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explanation_contact_c.php

 // created: 2020-10-13 11:16:12
$dictionary['VM01_Visitor_Management']['fields']['explanation_contact_c']['labelValue']='Explanation of COVID-19 Contact';
$dictionary['VM01_Visitor_Management']['fields']['explanation_contact_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explanation_contact_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explanation_contact_c']['dependency']='equal($contact_covid_19_c,"Yes")';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explanation_for_confirmed_c.php

 // created: 2020-10-13 11:16:55
$dictionary['VM01_Visitor_Management']['fields']['explanation_for_confirmed_c']['labelValue']='Explanation for Confirmed COVID-19 Case';
$dictionary['VM01_Visitor_Management']['fields']['explanation_for_confirmed_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explanation_for_confirmed_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explanation_for_confirmed_c']['dependency']='equal($visitor_confirmed_case_c,"Yes")';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explanation_outside_us_c.php

 // created: 2020-10-13 11:17:40
$dictionary['VM01_Visitor_Management']['fields']['explanation_outside_us_c']['labelValue']='Explanation for Outside of US';
$dictionary['VM01_Visitor_Management']['fields']['explanation_outside_us_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explanation_outside_us_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explanation_outside_us_c']['dependency']='equal($outside_us_c,"Yes")';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explanation_presence_c.php

 // created: 2020-10-13 11:18:19
$dictionary['VM01_Visitor_Management']['fields']['explanation_presence_c']['labelValue']='Explanation for Mandatory Presence';
$dictionary['VM01_Visitor_Management']['fields']['explanation_presence_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explanation_presence_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explanation_presence_c']['dependency']='equal($mandatory_presence_c,"Yes")';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_visitor_approval_notes_c.php

 // created: 2020-10-13 12:02:53
$dictionary['VM01_Visitor_Management']['fields']['visitor_approval_notes_c']['labelValue']='Visitor Approval Notes';
$dictionary['VM01_Visitor_Management']['fields']['visitor_approval_notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['visitor_approval_notes_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['visitor_approval_notes_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_manager_reviewed_c.php

 // created: 2020-11-03 10:16:58
$dictionary['VM01_Visitor_Management']['fields']['manager_reviewed_c']['labelValue']='APS Reviewed?';
$dictionary['VM01_Visitor_Management']['fields']['manager_reviewed_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['manager_reviewed_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_test_result_quarantine_c.php

 // created: 2020-11-24 08:41:46
$dictionary['VM01_Visitor_Management']['fields']['test_result_quarantine_c']['labelValue']='Are you or anyone in your household awaiting a COVID-19 test result, or have you or anyone in your household been asked to quarantine?';
$dictionary['VM01_Visitor_Management']['fields']['test_result_quarantine_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['test_result_quarantine_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['test_result_quarantine_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explanation_testquarantine_c.php

 // created: 2020-11-24 08:43:47
$dictionary['VM01_Visitor_Management']['fields']['explanation_testquarantine_c']['labelValue']='Explanation for Test and/or Quarantine';
$dictionary['VM01_Visitor_Management']['fields']['explanation_testquarantine_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explanation_testquarantine_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explanation_testquarantine_c']['dependency']='equal($test_result_quarantine_c,"Yes")';
$dictionary['VM01_Visitor_Management']['fields']['explanation_testquarantine_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_experience_rating.php

 // created: 2021-01-26 09:07:38
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['len']='11';
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['audited']=true;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['massupdate']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['hidemassupdate']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['duplicate_merge']='enabled';
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['duplicate_merge_dom_value']='1';
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['merge_filter']='disabled';
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['unified_search']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['calculated']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['enable_range_search']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['min']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['max']=false;
$dictionary['VM01_Visitor_Management']['fields']['experience_rating']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_symptoms_past_48_hrs_c.php

 // created: 2021-12-14 12:23:18
$dictionary['VM01_Visitor_Management']['fields']['symptoms_past_48_hrs_c']['labelValue']='Regardless of vaccination status, has visitor experienced any of the symptoms in the list below in the past 48 hours?';
$dictionary['VM01_Visitor_Management']['fields']['symptoms_past_48_hrs_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['symptoms_past_48_hrs_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['symptoms_past_48_hrs_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['symptoms_past_48_hrs_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_isolatingquarantining_c.php

 // created: 2021-12-14 12:27:57
$dictionary['VM01_Visitor_Management']['fields']['isolatingquarantining_c']['labelValue']='Is visitor isolating or quarantining because they tested positive for COVID-19 or are worried that they may be sick with COVID-19?';
$dictionary['VM01_Visitor_Management']['fields']['isolatingquarantining_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['isolatingquarantining_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['isolatingquarantining_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['isolatingquarantining_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explain_isolationquarantine_c.php

 // created: 2021-12-14 12:32:31
$dictionary['VM01_Visitor_Management']['fields']['explain_isolationquarantine_c']['labelValue']='Explain Isolation/Quarantine';
$dictionary['VM01_Visitor_Management']['fields']['explain_isolationquarantine_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explain_isolationquarantine_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explain_isolationquarantine_c']['dependency']='equal($isolatingquarantining_c,"Yes")';
$dictionary['VM01_Visitor_Management']['fields']['explain_isolationquarantine_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['explain_isolationquarantine_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_physical_contact_c.php

 // created: 2021-12-14 12:34:23
$dictionary['VM01_Visitor_Management']['fields']['physical_contact_c']['labelValue']='Has visitor been in close physical contact in the last 14 days with: anyone who is known to have laboratory-confirmed COVID-19? OR Anyone who has any symptoms consistent with COVID-19?';
$dictionary['VM01_Visitor_Management']['fields']['physical_contact_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['physical_contact_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['physical_contact_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['physical_contact_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explain_contact_c.php

 // created: 2021-12-14 12:35:39
$dictionary['VM01_Visitor_Management']['fields']['explain_contact_c']['labelValue']='Explain Contact';
$dictionary['VM01_Visitor_Management']['fields']['explain_contact_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explain_contact_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explain_contact_c']['dependency']='equal($physical_contact_c,"Yes")';
$dictionary['VM01_Visitor_Management']['fields']['explain_contact_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['explain_contact_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_explain_symptoms_c.php

 // created: 2022-01-04 12:44:17
$dictionary['VM01_Visitor_Management']['fields']['explain_symptoms_c']['labelValue']='Explain Symptoms';
$dictionary['VM01_Visitor_Management']['fields']['explain_symptoms_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['VM01_Visitor_Management']['fields']['explain_symptoms_c']['enforced']='';
$dictionary['VM01_Visitor_Management']['fields']['explain_symptoms_c']['dependency']='equal($symptoms_past_48_hrs_c,"Yes")';
$dictionary['VM01_Visitor_Management']['fields']['explain_symptoms_c']['required_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['explain_symptoms_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_mandatory_presence_c.php

 // created: 2022-01-07 06:37:26
$dictionary['VM01_Visitor_Management']['fields']['mandatory_presence_c']['labelValue']='Is visitor\'s presence at APS mandatory for the success of the study?';
$dictionary['VM01_Visitor_Management']['fields']['mandatory_presence_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['mandatory_presence_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['mandatory_presence_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_outside_us_c.php

 // created: 2022-01-07 06:38:04
$dictionary['VM01_Visitor_Management']['fields']['outside_us_c']['labelValue']='Has visitor been outside the US anytime in the last 14 days?';
$dictionary['VM01_Visitor_Management']['fields']['outside_us_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['outside_us_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['outside_us_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_contact_covid_19_c.php

 // created: 2022-01-07 06:38:58
$dictionary['VM01_Visitor_Management']['fields']['contact_covid_19_c']['labelValue']='Has visitor come in contact (or if visitor is a physician that treated a patient) with a confirmed case of COVID-19 in the last 14 days?';
$dictionary['VM01_Visitor_Management']['fields']['contact_covid_19_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['contact_covid_19_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['contact_covid_19_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_visitor_confirmed_case_c.php

 // created: 2022-01-07 06:39:33
$dictionary['VM01_Visitor_Management']['fields']['visitor_confirmed_case_c']['labelValue']='Has there been a confirmed COVID-19 case at visitor\'s place of business within the past 14 days?';
$dictionary['VM01_Visitor_Management']['fields']['visitor_confirmed_case_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['visitor_confirmed_case_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['visitor_confirmed_case_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_acknowledgement_no_symptoms_c.php

 // created: 2022-01-07 12:37:33
$dictionary['VM01_Visitor_Management']['fields']['acknowledgement_no_symptoms_c']['labelValue']='Visitor read and understood that they should enter the facility only if they have had none of the COVID-19 symptoms for a period of at least 7 days';
$dictionary['VM01_Visitor_Management']['fields']['acknowledgement_no_symptoms_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['acknowledgement_no_symptoms_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['acknowledgement_no_symptoms_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/Vardefs/sugarfield_workplace_process_c.php

 // created: 2022-01-07 12:38:38
$dictionary['VM01_Visitor_Management']['fields']['workplace_process_c']['labelValue']='Is there a procedure in place at visitor\'s workplace to notify test sites (APS) that they have visited if a case of COVID-19 has been confirmed at their place of business?';
$dictionary['VM01_Visitor_Management']['fields']['workplace_process_c']['dependency']='';
$dictionary['VM01_Visitor_Management']['fields']['workplace_process_c']['readonly_formula']='';
$dictionary['VM01_Visitor_Management']['fields']['workplace_process_c']['visibility_grid']='';

 
?>
