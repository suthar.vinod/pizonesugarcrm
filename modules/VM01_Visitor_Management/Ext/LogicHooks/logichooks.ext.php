<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/VM01_Visitor_Management/Ext/LogicHooks/beforeSave.php


$hook_version = 1;
// If hook_array isn't set before anywhere
if (!isset($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['before_save'])) {
    $hook_array['before_save'] = array();
}
$hook_array['before_save'][] = array(
    count($hook_array['before_save']),
    'Before save logic hook',
    'custom/modules/VM01_Visitor_Management/VM01_Visitor_ManagementHook.php',
    'VM01_Visitor_ManagementHook',
    'beforeSave'
);

?>
