<?php
global $db;
$site_url = $GLOBALS['sugar_config']['site_url'];

$wpCode = 'a3c3bffc-96f2-11e7-a448-06d4fd13a43b';
$WPCBean = BeanFactory::retrieveBean('M03_Work_Product_Code', $wpCode);
$WPCBean->load_relationship('m03_work_product_code_taskd_task_design_1');
$relatedTD = $WPCBean->m03_work_product_code_taskd_task_design_1->get();
//$GLOBALS['log']->fatal(' relatedTD ' . print_r($relatedTD, 1));

$copyingTaskDesignIds = array();
$tsArr = array();
$tsRelationArr = array();

$p1arr = array();
$WpBody = "<h2>List of WP records of Auto-create TDs in one time event : </h2><br/><table width='1250px' cellspacing='5' cellpadding='6' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 20px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th bgcolor='#b3d1ff' align='left'>Task Design </th>
			<th bgcolor='#b3d1ff' align='left'>Related Task Design</th>
			
			</tr>";
foreach ($relatedTD as $TDId) {
	$clonedBean = BeanFactory::getBean('TaskD_Task_Design', $TDId);
	$other_equipment = $clonedBean->other_equipment_c;
	$clonedBean->load_relationship("taskd_task_design_taskd_task_design_1");

	$sqlGetTaskDesignIds = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_ida as TDId
                                    FROM taskd_task_design_taskd_task_design_1_c 
                                    where taskd_task_design_taskd_task_design_1taskd_task_design_idb='$TDId' and deleted=0";
	$resultsSqlGetTaskDesignIds = $db->query($sqlGetTaskDesignIds);
	$rowSqlGetTaskDesignIds = $db->fetchByAssoc($resultsSqlGetTaskDesignIds);
	$taskDesignIds = $rowSqlGetTaskDesignIds['TDId'];

	//$GLOBALS['log']->fatal('sqlGetTaskDesignIds  === ' . $sqlGetTaskDesignIds);
	$wpBean = BeanFactory::getBean('M03_Work_Product', '8fced5ea-f633-11ec-a459-065a04235e91');
	// $testSystemIds = $clonedBean->anml_animals_taskd_task_design_1->get();
	$clonedBean->id = create_guid();
	$tsArr[$clonedBean->id] = $TDId;
	$tsRelationArr[$TDId] = $taskDesignIds;
	$clonedBean->new_with_id = true;
	$clonedBean->fetched_row = null;
	$date_entered = date("Y-m-d H:i:s", time());

	$p1arr[$TDId] = $taskDesignIds;
	$a1arr[$clonedBean->id] = $TDId;
	$b1arr[$TDId] = $clonedBean->id;
	$clonedBean->type_2 = "Actual SP";
	$clonedBean->other_equipment_c = $other_equipment;
	//$clonedBean->u_units_id_c = $clonedBean->u_units_id_c;
	$copyingTaskDesignIds[] = $clonedBean->id;
	//$clonedBean->save();
	$clonedBean->m03_work_product_taskd_task_design_1->add($bean->id);
	//$GLOBALS['log']->fatal('Bean Name  ===> ' . $bean->name);
	$wpName = $bean->name;
	$task_type = $clonedBean->task_type;
	$custom_task = $clonedBean->custom_task;
	$standard_task = $clonedBean->standard_task;
	//$phase = $clonedBean->phase_c;
	$phase_dom = $app_list_strings['td_phase_list'];
	$phase = $phase_dom[$clonedBean->phase_c];
	$batch_id       = $clonedBean->bid_batch_id_taskd_task_design_1bid_batch_id_ida;
	$BatchBean      =   BeanFactory::getBean('BID_Batch_ID', $batch_id);
	$BatchName      =   $BatchBean->name;
	$clonedBean->date_entered = $date_entered;
	if ($task_type == 'Custom') {
		$standard_task = '';
		$standard_task_val = $custom_task;
	} else if ($task_type == 'Standard') {
		$custom_task = '';
		$standard_task_val = $standard_task;
	}
	if ($BatchName != "") {
		$clonedBean->name = $wpName . ' ' . $phase . ' ' . $BatchName . ' ' . $standard_task_val;
	} else {
		$clonedBean->name = $wpName . ' ' . $phase . ' ' . $standard_task_val;
	}


	if ($srno % 2 == 0)
		$stylr = "style='background-color: #e6e6e6;'";
	else
		$stylr = "style='background-color: #f3f3f3;'";
	if ($CountTD < 1) {
		$WpBody .= "<tr><td " . $stylr . ">" . $srno++ . "</td>
                <td " . $stylr . "><a target='_blank' href='" . $site_url . "/#TaskD_Task_Design/" . $TDId . "'>" . $TDId . "</td>
				<td " . $stylr . "><a target='_blank' href='" . $site_url . "/#TaskD_Task_Design/" . $taskDesignIds . "'>" . $taskDesignIds . "</td>
               
                </tr>";
	}
}


echo "<br><br>" . $WpBody .= "</table>";
