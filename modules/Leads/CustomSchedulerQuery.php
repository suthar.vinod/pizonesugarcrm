<?php 
 
  global $db;
    $current_date = date("Y-m-d"); //current date
    $wp_emailAdd = array(); // contains the email addresses of Study Director and its Manager
	$sd_array = array(); //Contains id For Study Directors
    
	//Get site url so that appropriate Work Product can be shown in the Email Template
    $site_url = $GLOBALS['sugar_config']['site_url'];

    //Prepare the Email Template to send to appropriate members
    $template = new EmailTemplate();
    $emailObj = new Email();

  $queryCustom = "SELECT 
                    commu.id AS communicationID, 
                    commu.name AS CommunicationName,  
                    commu_cstm.target_reinspection_date_c, 
					commu_cstm.error_category_c,  					
                    commu_cstm.reinspection_date_c, 
                    commu_wp.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
                    wp.name  AS workProductName,
					wp_cstm.contact_id_c,
					commu_cstm.error_classification_c,commu_cstm.subtype_c,commu_cstm.resolution_c
            FROM `m06_error` AS commu
                    INNER JOIN m06_error_cstm AS commu_cstm
                    ON  commu.id = commu_cstm.id_c

                    RIGHT JOIN m06_error_m03_work_product_1_c AS commu_wp
                    ON  commu.id = commu_wp.m06_error_m03_work_product_1m06_error_ida
					
					INNER JOIN m03_work_product AS wp
                    ON  commu_wp.m06_error_m03_work_product_1m03_work_product_idb=wp.id 
					
					 INNER JOIN m03_work_product_cstm AS wp_cstm
                    ON  wp.id=wp_cstm.id_c 
		WHERE  
			commu_cstm.error_category_c != 'Internal Feedback' 
			AND commu_cstm.error_category_c != 'Feedback' 
			AND YEAR(commu_cstm.target_reinspection_date_c) = YEAR(CURDATE()) 
			AND commu_cstm.target_reinspection_date_c <'" . $current_date . "' 
            AND commu_cstm.reinspection_date_c IS NULL 
            AND commu_wp.m06_error_m03_work_product_1m03_work_product_idb IS NOT NULL
			AND commu_wp.deleted = 0
			AND commu.deleted = 0
			AND( commu_cstm.resolution_c IS NULL 
				OR commu_cstm.reinspection_date_c IS NULL 
				OR commu_cstm.error_classification_c IS NULL  
				OR commu_cstm.error_classification_c ='Choose One'
				OR commu_cstm.subtype_c IS NULL)
			
		ORDER BY commu.name ASC";
			 

    $queryCommResult = $db->query($queryCustom);
 
	echo "<table width='100%' cellpadding='3' cellspacing='3'><tr><th>Sr. No.</th><th>Comm ID</th><th>Comm Name</th><th>Work Product ID</th><th>WP Name</th><th>Target Date</th><th>Category</th><th>Actual date</th><th>SD Assessment</th><th>Classification </th><th>Sub Type</th></tr>";
	
    while ($fetchCommResult = $db->fetchByAssoc($queryCommResult)) {
		
		echo "<tr><td>".$cnt."</td><td>".$fetchCommResult['communicationID']."</td><td>".$fetchCommResult['CommunicationName']."</td><td>".$fetchCommResult['workProductID']."</td><td>".$fetchCommResult['workProductName']."</td><td>".$fetchCommResult['target_reinspection_date_c']."</td><td>".$fetchCommResult['error_category_c']."</td><td>".$fetchCommResult['reinspection_date_c']."</td><td>".$fetchCommResult['resolution_c']."</td><td>".$fetchCommResult['error_classification_c']."</td><td>".$fetchCommResult['subtype_c']."</td></tr>";
		$cnt++;
	}	
    echo "</table>";
	$cnt = 0;
	 
	
 
     
    
	 
?>