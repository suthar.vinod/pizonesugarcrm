<?php 
 
global $db,$current_user;
  
$queryCustom         = "SELECT * FROM `m06_error_audit` WHERE `parent_id`='983680A5-136F-4AE6-9ACD-51862E727321' order by date_created desc";
$queryCustomResult   = $db->query($queryCustom);
$table = "<table border='1' width='100%' cellpadding='2' cellspacing='0' class='table dataTable table-striped'><tr><th width='5%'>srno</th><th width='10%'>Field</th><th width='10%'>DataType</th><th width='10%'>Old Value String</th><th width='10%'>New Value String</th><th width='10%'>Old Value</th><th width='10%'>New Value</th><th width='10%'>Changed Date</th></tr>";
   $srno = 0;
   while ($fetchCResult = $db->fetchByAssoc($queryCustomResult)) {
      $field_name          = $fetchCResult['field_name'];
      $data_type           = $fetchCResult['data_type'];
      $before_value_string = $fetchCResult['before_value_string'];
      $after_value_string  = $fetchCResult['after_value_string'];
      $before_value_text   = $fetchCResult['before_value_text'];
      $after_value_text    = $fetchCResult['after_value_text'];
      $date_created            = $fetchCResult['date_created'];

      $srno++;

      if($srno%2==0)
         $trStyle = "background-color: #eee;";
      else     
      $trStyle = "background-color: #fff;";
      $table .="<tr class='single' style='".$trStyle."' ><td align='center' style='border: 1px solid;'>".$srno."</td>  <td align='center' style='border: 1px solid;'>".$field_name."</td><td align='center' style='border: 1px solid;'>".$data_type."</td> <td align='center' style='border: 1px solid;'>".$before_value_string."</td> <td align='center' style='border: 1px solid;'>".$after_value_string."</td><td align='center' style='border: 1px solid;'>".$before_value_text."</td> <td align='center' style='border: 1px solid;'>".$after_value_text."</td><td align='center' style='border: 1px solid;'>".$date_created."</td></tr>";
   
   }   
   $table .="</table>";
	echo $table;
?>