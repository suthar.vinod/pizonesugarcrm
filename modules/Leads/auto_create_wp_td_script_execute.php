<?php
global $db;
$site_url = $GLOBALS['sugar_config']['site_url'];
$limit = $_GET['limit'];
if ($limit != '') {
	$limit = $limit;
} else {
	$limit = "5";
}
$query = "SELECT IFNULL(m03_work_product.id,'') wpid
	,IFNULL(m03_work_product.name,'') m03_work_product_name,m03_work_product_code1.id AS wpcid,m03_work_product_code1.name AS wpc_name
	,IFNULL(m03_work_product_cstm.work_product_status_c,'') work_product_status_c,m03_work_product_cstm.functional_area_c AS functional_area_c,m03_work_product_cstm.bc_spa_reconciled_date_c bc_spa_reconciled_date_c,m03_work_product.date_entered - INTERVAL 300 MINUTE date_entered
FROM m03_work_product
LEFT JOIN  m03_work_product_taskd_task_design_1_c l1_1 ON m03_work_product.id=l1_1.m03_work_product_taskd_task_design_1m03_work_product_ida AND l1_1.deleted=0

LEFT JOIN  taskd_task_design l1 ON l1.id=l1_1.m03_work_product_taskd_task_design_1taskd_task_design_idb AND l1.deleted=0
LEFT JOIN m03_work_product_cstm m03_work_product_cstm ON m03_work_product.id = m03_work_product_cstm.id_c
LEFT JOIN m03_work_product_code m03_work_product_code1 ON m03_work_product_code1.id = m03_work_product_cstm.m03_work_product_code_id1_c AND IFNULL(m03_work_product_code1.deleted,0)=0 

 WHERE (((m03_work_product_cstm.functional_area_c = 'Standard Biocompatibility'
) AND ((coalesce(LENGTH(m03_work_product_cstm.bc_spa_reconciled_date_c), 0) <> 0)) AND ((coalesce(LENGTH(m03_work_product_cstm.m03_work_product_code_id1_c), 0) <> 0)) AND ((coalesce(LENGTH(l1.id), 0) = 0)))) 
AND  m03_work_product.deleted=0 
 AND IFNULL(m03_work_product_code1.deleted,0)=0 limit $limit";
$queryResult = $db->query($query);
$srno = 1;
$WpBody = "<h2>List of WP records of Auto-createD TDs : </h2><br/><table width='1250px' cellspacing='5' cellpadding='6' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 20px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th bgcolor='#b3d1ff' align='left'>Work Product </th>
			</tr>";

while ($fetchResult = $db->fetchByAssoc($queryResult)) {
	$wpid    = $fetchResult['wpid'];
	/**Get the task design records linked to fetched work producut */
	$tsddataQuery = "SELECT count(m03_work_product_taskd_task_design_1taskd_task_design_idb) as CountTD FROM m03_work_product_taskd_task_design_1_c
	   where m03_work_product_taskd_task_design_1m03_work_product_ida ='" . $wpid . "' and deleted=0";
	$restsddataQuery = $db->query($tsddataQuery);
	$rowtsddataQuery = $db->fetchByAssoc($restsddataQuery);
	$CountTD = $rowtsddataQuery['CountTD'];
	$m03_work_product_name	 		= $fetchResult['m03_work_product_name'];
	$wpcid	 						= $fetchResult['wpcid'];
	/**Get the task design linked to work product code */
	$wpctsQuery = "SELECT count(m03_work_product_code_taskd_task_design_1taskd_task_design_idb) AS CountWPCTD FROM m03_work_product_code_taskd_task_design_1_c
	   where m03_work_p7fc0ct_code_ida='" . $wpcid . "' and deleted=0";
	$reswpctdQuery = $db->query($wpctsQuery);
	$rowwpctdQuery = $db->fetchByAssoc($reswpctdQuery);
	$CountWPCTD = $rowwpctdQuery['CountWPCTD'];
	/************************************************ */
	$wpc_name	 					= $fetchResult['wpc_name'];
	$work_product_status_c	 		= $fetchResult['work_product_status_c'];
	$functional_area_c	 			= $fetchResult['functional_area_c'];
	$bc_spa_reconciled_date_c	 	= $fetchResult['bc_spa_reconciled_date_c'];
	$date_entered	 				= $fetchResult['date_entered'];


	/**#2327 : Auto Create TD  from wpc to wp */
	/** Get the wpc bean and fetch the linked Task design records from wpc record */
	$WPCBean = BeanFactory::retrieveBean('M03_Work_Product_Code', $wpcid);
	$WPCBean->load_relationship('m03_work_product_code_taskd_task_design_1');
	$relatedTD = $WPCBean->m03_work_product_code_taskd_task_design_1->get();

	$copyingTaskDesignIds = array();
	$tsArr = array();
	$tsRelationArr = array();

	$p1arr = array();

	foreach ($relatedTD as $TDId) {
		$clonedBean = \BeanFactory::getBean('TaskD_Task_Design', $TDId);
		$other_equipment = $clonedBean->other_equipment_c;
		$clonedBean->load_relationship("taskd_task_design_taskd_task_design_1");

		$sqlGetTaskDesignIds = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_ida as TDId
								FROM taskd_task_design_taskd_task_design_1_c 
								where taskd_task_design_taskd_task_design_1taskd_task_design_idb='$TDId' and deleted=0";
		$resultsSqlGetTaskDesignIds = $db->query($sqlGetTaskDesignIds);
		$rowSqlGetTaskDesignIds = $db->fetchByAssoc($resultsSqlGetTaskDesignIds);
		$taskDesignIds = $rowSqlGetTaskDesignIds['TDId'];

		$wpBean = BeanFactory::getBean('M03_Work_Product', $wpid);
		$clonedBean->id = create_guid();
		$tsArr[$clonedBean->id] = $TDId;
		$tsRelationArr[$TDId] = $taskDesignIds;
		$clonedBean->new_with_id = true;
		$clonedBean->fetched_row = null;
		$date_entered = date("Y-m-d H:i:s", time());

		$p1arr[$TDId] = $taskDesignIds;
		$a1arr[$clonedBean->id] = $TDId;
		$b1arr[$TDId] = $clonedBean->id;
		$clonedBean->type_2 = "Actual SP";
		$clonedBean->other_equipment_c = $other_equipment;
		$copyingTaskDesignIds[] = $clonedBean->id;
		$clonedBean->m03_work_product_taskd_task_design_1->add($wpid);
		$wpName = $m03_work_product_name;
		$task_type = $clonedBean->task_type;
		$custom_task = $clonedBean->custom_task;
		$standard_task = $clonedBean->standard_task;
		$phase = $clonedBean->phase_c;
		$batch_id       = $clonedBean->bid_batch_id_taskd_task_design_1bid_batch_id_ida;
		$BatchBean      =   BeanFactory::getBean('BID_Batch_ID', $batch_id);
		$BatchName      =   $BatchBean->name;
		$clonedBean->parent_td_id_c = $TDId;
		$clonedBean->date_entered = $date_entered;
		if ($task_type == 'Custom') {
			$standard_task = '';
			$standard_task_val = $custom_task;
		} else if ($task_type == 'Standard') {
			$custom_task = '';
			$standard_task_val = $standard_task;
		}

		if ($BatchName != "") {
			$clonedBean->name = $wpName . ' ' . $phase . ' ' . $BatchName . ' ' . $standard_task_val;
		} else {
			$clonedBean->name = $wpName . ' ' . $phase . ' ' . $standard_task_val;
		}

		if ($standard_task == 'Extract In') {
			//if ($wpBean->extraction_start_integer_c != '' && $wpBean->extraction_end_integer_c != '' && $wpBean->u_units_id_c != '') {
				$clonedBean->start_integer = $wpBean->extraction_start_integer_c;
				$clonedBean->end_integer = $wpBean->extraction_end_integer_c;
				$clonedBean->integer_units = $wpBean->integer_units_c;
				$clonedBean->u_units_id_c = $wpBean->u_units_id_c;
				$unit_Bean = BeanFactory::getBean('U_Units', $clonedBean->u_units_id_c);
				$unit_Bean_name = $unit_Bean->name;
				$clonedBean->time_window = $unit_Bean_name . " " . $clonedBean->start_integer . "-" . $clonedBean->end_integer;
				$offset1 =  21600; //daylight Saving
				if ($wpBean->first_procedure_c != '') {
					$datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
					$actual_datetime       = date("Y-m-d 17:00:00", $datefirst_procedure_c);
					//$GLOBALS['log']->fatal(' Extract IN clonedBean->actual_datetime ' . $clonedBean->actual_datetime);
				}
				if ($wpBean->first_procedure_c != '' && $clonedBean->relative == "NA") {
					$datefirst_procedure_c = strtotime($wpBean->first_procedure_c) + $offset1;
					$clonedBean->actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
					$clonedBean->save();
				}
				if ($actual_datetime != "") {
					if ($unit_Bean_name == "Day") {
						if ($clonedBean->relative == "1st Tier") {
							$result = date($actual_datetime);
							$result = strtotime($result . '+' . $clonedBean->start_integer . ' days');
							$clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);

							$result = date($actual_datetime);
							$result = strtotime($result . '+' . $clonedBean->end_integer . ' days');
							$clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
						}
					} else if ($unit_Bean_name == "Hour") {
						if ($clonedBean->relative == "1st Tier") {
							$result = date($actual_datetime);
							$result = strtotime($result . '+' . $clonedBean->start_integer . ' hours');
							$clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
							//$GLOBALS['log']->fatal(' Extract IN clonedBean->actual_datetime ' . $clonedBean->actual_datetime);
							$result = date($actual_datetime);
							$result = strtotime($result . '+' . $clonedBean->end_integer . ' hours');
							$clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
						}
					} else if ($unit_Bean_name == "Minute") {

						if ($clonedBean->relative == "1st Tier") {
							$result = date($actual_datetime);
							$result = strtotime($result . '+' . $clonedBean->start_integer . ' minutes');
							$clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);

							$result = date($actual_datetime);
							$result = strtotime($result . '+' . $clonedBean->end_integer . ' minutes');
							$clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
						}
					}

					if (($clonedBean->relative == "1st Tier" || $clonedBean->relative == "NA")) {
						$clonedBean->planned_start_datetime_2nd = null;
						$clonedBean->planned_end_datetime_2nd = null;
					}
					if (($clonedBean->relative == "2nd Tier" || $clonedBean->relative == "NA")) {
						$clonedBean->planned_start_datetime_1st = null;
						$clonedBean->planned_end_datetime_1st = null;
					}
				}
			//}
		} else {
			$offset1 =  21600; //daylight Saving
			if ($wpBean->first_procedure_c != '') {
				$datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
				$actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
			}
			if ($wpBean->first_procedure_c != '' && $clonedBean->relative == "NA") {
				$datefirst_procedure_c = strtotime($wpBean->first_procedure_c) + $offset1;
				$clonedBean->actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
				$clonedBean->save();
			}
			$clonedBean->u_units_id_c = $clonedBean->u_units_id_c;
			$unit_Bean = BeanFactory::getBean('U_Units', $clonedBean->u_units_id_c);
			$unit_Bean_name = $unit_Bean->name;
			//$GLOBALS['log']->fatal(' Extract out unit_Bean_name line 198 ' . $unit_Bean_name);
			//$GLOBALS['log']->fatal(' Extract out actual_datetime line 199 ' . $actual_datetime);
			$clonedBean->start_integer = $clonedBean->start_integer;
			$clonedBean->end_integer = $clonedBean->end_integer;
			//$clonedBean->integer_units = $clonedBean->integer_units;
			//$clonedBean->time_window = $clonedBean->time_window;
			$clonedBean->time_window = $unit_Bean_name . " " . $clonedBean->start_integer . "-" . $clonedBean->end_integer;

			if ($actual_datetime != "") {
				if ($unit_Bean_name == "Day") {
					//$GLOBALS['log']->fatal(' Extract out unit_Bean_name in day line 207 ' . $unit_Bean_name);
					//$GLOBALS['log']->fatal(' Extract out relative in day 1st tier line 208 ' . $clonedBean->relative);
					if ($clonedBean->relative == "1st Tier") {
						//$GLOBALS['log']->fatal(' Extract out relative in day 1st tier line 209 ' . $clonedBean->relative);
						$result = date($actual_datetime);
						$result = strtotime($result . '+' . $clonedBean->start_integer . ' days');
						$clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);

						$result = date($actual_datetime);
						$result = strtotime($result . '+' . $clonedBean->end_integer . ' days');
						$clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
					}
				} else if ($unit_Bean_name == "Hour") {
					//$GLOBALS['log']->fatal(' Extract out unit_Bean_name in day 1st tier line 220 ' . $unit_Bean_name);
					if ($clonedBean->relative == "1st Tier") {
						//$GLOBALS['log']->fatal(' Extract out relative in day 1st tier line 221 ' . $clonedBean->relative);
						$result = date($actual_datetime);
						$result = strtotime($result . '+' . $clonedBean->start_integer . ' hours');
						$clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);

						$result = date($actual_datetime);
						$result = strtotime($result . '+' . $clonedBean->end_integer . ' hours');
						$clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
					}
				} else if ($unit_Bean_name == "Minute") {
					//$GLOBALS['log']->fatal(' Extract out unit_Bean_name in day 1st tier line 232 ' . $unit_Bean_name);
					//$GLOBALS['log']->fatal(' Extract out relative in day 1st tier line 233 ' . $clonedBean->relative);
					if ($clonedBean->relative == "1st Tier") {
						//$GLOBALS['log']->fatal(' Extract out relative in day 1st tier line 235 ' . $clonedBean->relative);
						$result = date($actual_datetime);
						$result = strtotime($result . '+' . $clonedBean->start_integer . ' minutes');
						$clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);

						$result = date($actual_datetime);
						$result = strtotime($result . '+' . $clonedBean->end_integer . ' minutes');
						$clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
					}
				}

				if (($clonedBean->relative == "1st Tier" || $clonedBean->relative == "NA")) {
					$clonedBean->planned_start_datetime_2nd = null;
					$clonedBean->planned_end_datetime_2nd = null;
				}
				if (($clonedBean->relative == "2nd Tier" || $clonedBean->relative == "NA")) {
					$clonedBean->planned_start_datetime_1st = null;
					$clonedBean->planned_end_datetime_1st = null;
				}
			}
		}

		$clonedBean->save();

		$auditsql = 'update taskd_task_design_audit 
			set field_name= "u_units_id_c",
			after_value_string="' . $clonedBean->u_units_id_c . '" 
			Where parent_id = "' . $clonedBean->id . '"
			AND field_name="integer_units"';
		$db->query($auditsql);

		$queryAuditLog = "SELECT id,count(*) as NUM FROM `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' 
				AND `parent_id`='" . $clonedBean->id . "' group by before_value_string,after_value_string,date_created";
		$auditLogResult = $db->query($queryAuditLog);
		if ($auditLogResult->num_rows > 0) {
			while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
				if ($fetchAuditLog['NUM'] > 1) {
					$recordID = $fetchAuditLog['id'];
					$sql_DeleteAudit = "DELETE from `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' AND `parent_id`='" . $clonedBean->id . "' AND id = '" . $recordID . "'";
					$db->query($sql_DeleteAudit);
				}
			}
		}
	}
	/** To link task design to task design record */
	foreach ($p1arr as $key => $value) {
		$newBeanId = $b1arr[$key];
		$toBeRelateID = array_search($value, $a1arr);
		if ($toBeRelateID != "") {
			$uniqId = create_guid();
			$sqlInsertRelationship = "INSERT INTO `taskd_task_design_taskd_task_design_1_c` 
		(`id`, `date_modified`, `deleted`, `taskd_task_design_taskd_task_design_1taskd_task_design_ida`, `taskd_task_design_taskd_task_design_1taskd_task_design_idb`) 
		VALUES ('$uniqId', now(), '0', '$toBeRelateID', '$newBeanId')";
			$resultsSqlGetTaskDesignIds = $db->query($sqlInsertRelationship);
		}
	}

	$currentWPBean = BeanFactory::retrieveBean('M03_Work_Product', $wpid);
	$currentWPBean->load_relationship('m03_work_product_taskd_task_design_1');
	$relatedTD1 = $currentWPBean->m03_work_product_taskd_task_design_1->get();
	foreach ($relatedTD1 as $tdID) {
		$tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $tdID);
		$u_units = $tdBean->u_units_id_c;
		$unit_Bean = BeanFactory::getBean('U_Units', $u_units);
		$unit_Bean_name = $unit_Bean->name;
		if ($unit_Bean_name == "Day") {
			$integer_u = 'days';
		} elseif ($unit_Bean_name == "Hour") {
			$integer_u = 'hours';
		} else {
			$integer_u = 'minutes';
		}
		if ($tdBean->relative == "1st Tier") {
			$tdBean1 = BeanFactory::retrieveBean('TaskD_Task_Design', $tdBean->id);
			$tdBean1->load_relationship('taskd_task_design_taskd_task_design_1');
			$taskDesignId1 = $tdBean1->taskd_task_design_taskd_task_design_1->get();

			$Get_2nd_tier = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_idb FROM  `taskd_task_design_taskd_task_design_1_c` WHERE `taskd_task_design_taskd_task_design_1taskd_task_design_ida` = '" . $tdBean->id . "' AND deleted = 0;";
			$Get_Result = $db->query($Get_2nd_tier);

			if ($Get_Result->num_rows > 0) {
				while ($fetchResult = $db->fetchByAssoc($Get_Result)) {
					$recordID = $fetchResult['taskd_task_design_taskd_task_design_1taskd_task_design_idb'];
					$tdBean2 = BeanFactory::retrieveBean('TaskD_Task_Design', $recordID);
					if ($tdBean->planned_start_datetime_1st != "") {
						$result = date($tdBean->planned_start_datetime_1st);
						$result = strtotime($result . '+' . $tdBean2->start_integer . ' ' . $integer_u);
						$tdBean2->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
						if (date("Y", $result) != "1970") {
							$tdBean2->single_date_c = date("Y-m-d", $result);
						}
					} else {
						$tdBean2->planned_start_datetime_2nd = null;
					}
					if ($tdBean->planned_end_datetime_1st != "") {
						$result = date($tdBean->planned_end_datetime_1st);
						$result = strtotime($result . '+' . $tdBean2->end_integer . ' ' . $integer_u);
						$tdBean2->planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
					} else {
						$tdBean2->planned_end_datetime_2nd = null;
					}
					$tdBean2->save();
				}
			}
		}
	}

	/*********************************** */

	/** To print the wp record which has been update with td creation*/
	$WpBody .= "<tr><td " . $stylr . ">" . $srno++ . "</td>
	<td " . $stylr . "><a target='_blank' href='" . $site_url . "/#M03_Work_Product/" . $wpid . "'>" . $m03_work_product_name . "</td>
	</tr>";
	/*** */
}
echo "<br><br>" . $WpBody .= "</table>";
