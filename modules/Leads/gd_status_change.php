<?php
global $db;
$current_date = date("Y-m-d");
$yesterdaydate	= date("Y-m-d", strtotime("-1 days", strtotime($current_date)));
$current_date	= $current_date . ' 05:59:59';
$yesterdaydate	= $yesterdaydate . ' 06:00:00';

echo "<br>===>" . $query = "SELECT IFNULL(gd_group_design.id,'') primaryid
	,IFNULL(gd_group_design.name,'') gd_name
	,IFNULL(gd_group_design_cstm.status_c,'') GD_GROUP_DESIGN_CSTM_SA89250,IFNULL(gd_group_design.id,'') gd_group_design_id
	,l1_cstm.contact_id_c contact_id,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contacts_name,
	gd_audit.after_value_string AS after_value,
	gd_audit.date_created AS Date_c
	FROM gd_group_design
	 INNER JOIN  m03_work_product_gd_group_design_1_c l1_1 ON gd_group_design.id=l1_1.m03_work_product_gd_group_design_1gd_group_design_idb AND l1_1.deleted=0
	
	 INNER JOIN  m03_work_product l1 ON l1.id=l1_1.m03_work_product_gd_group_design_1m03_work_product_ida AND l1.deleted=0
	LEFT JOIN gd_group_design_cstm gd_group_design_cstm ON gd_group_design.id = gd_group_design_cstm.id_c
	LEFT JOIN m03_work_product_cstm l1_cstm ON l1.id = l1_cstm.id_c
	LEFT JOIN contacts contacts1 ON contacts1.id = l1_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
	LEFT JOIN gd_group_design_audit gd_audit ON gd_group_design.id = gd_audit.parent_id
	 WHERE (((l1_cstm.test_system_c IN ('Bovine','Canine','Caprine','Micro Yucatans','PorcineGeneral','Porcine Got','Porcine','Porcine Yuc','Porcine and Bovine','Porcine_Canine_Ovine','Yorkshire Yucatan')
	))) 
	AND gd_audit.after_value_string ='complete'
	AND (gd_audit.date_created - INTERVAL 360 MINUTE >= '" . $yesterdaydate . "' AND gd_audit.date_created - INTERVAL 360 MINUTE <= '" . $current_date . "')
	AND  gd_group_design.deleted=0 
	 AND IFNULL(contacts1.deleted,0)=0 group by gd_audit.parent_id";

$queryResult = $db->query($query);

$srno = 1;

$tableBody = "<table width='1050px' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th bgcolor='#b3d1ff' align='left'> ID </th>
			<th bgcolor='#b3d1ff' align='left'> GD Name </th>
			<th bgcolor='#b3d1ff' align='left'> SD Name </th>	
			<th bgcolor='#b3d1ff' align='left'> SD ID </th>	
			<th bgcolor='#b3d1ff' align='left'> SD Email </th>	
			</tr>";


while ($fetchResult = $db->fetchByAssoc($queryResult)) {
	$id	 			= $fetchResult['primaryid'];
	$GDname	 	= $fetchResult['gd_name'];
	$SD_ID	 		= $fetchResult['contact_id'];
	$SD_name	 	= $fetchResult['contacts_name'];
	/*Get Study Director*/
	if ($SD_ID != "") {
		$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
		$studyDirectorName	= $stdy_bean->name;
		$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
	}
	if ($srno % 2 == 0)
		$stylr = "style='background-color: #e6e6e6;'";
	else
		$stylr = "style='background-color: #f3f3f3;'";


	if ($id != "") {
		$tableBody .= "<tr><td " . $stylr . ">" . $srno++ . "</td>
											<td " . $stylr . ">" . $id . "</td> 
											<td " . $stylr . ">" . $GDname . "</td> 							
											<td " . $stylr . ">" . $SD_ID . "</td> 											
											<td " . $stylr . ">" . $SD_name . "</td>
											<td " . $stylr . ">" . $SDEmailAddress . "</td>												
											</tr>";

		// echo "<br>==>" . $query1 = "UPDATE m03_work_product_deliverable_cstm SET contact_id1_c = '" . $SD_ID . "' WHERE id_c = '" . $id2 . "'";
		// $db->query($query1);  
		//echo 'Done';
	}
}


echo "<br><br>" . $tableBody .= "</table>";
