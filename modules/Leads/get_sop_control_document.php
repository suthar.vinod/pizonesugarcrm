<?php
global $db;
$site_url	= $GLOBALS['sugar_config']['site_url'];
$sop		= $_GET['sop'];  
    $query = "SELECT 
				IFNULL(erd_error_documents.id,'') primaryid,
				erd_error_documents_cstm.doc_id_c,
				IFNULL(erd_error_documents.name,'') erd_error_documents_name,
				erd_error_documents.date_modified - INTERVAL 360 MINUTE ERD_ModifiedDate,
				erd_error_documents.date_entered - INTERVAL 360 MINUTE ERD_CreatedDate,
				erd_error_documents.deleted
		FROM erd_error_documents
		LEFT JOIN erd_error_documents_cstm ON erd_error_documents.id = erd_error_documents_cstm.id_c		
		WHERE (((erd_error_documents_cstm.doc_id_c LIKE '".$sop."%'))) 
		ORDER BY ERD_CreatedDate DESC";
$queryResult = $db->query($query);
	 
$srno = 1;

	$emailBody = "<table width='1050px' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
		<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
		<th bgcolor='#b3d1ff' align='left'>Document ID </th>
		<th bgcolor='#b3d1ff' align='left'>Name</th>
		<th bgcolor='#b3d1ff' align='left'>Date Modified</th>
		<th bgcolor='#b3d1ff' align='left'>Date Created</th>
		<th bgcolor='#b3d1ff' align='left'>Deleted Status</th>";
		
		
	while ($fetchResult = $db->fetchByAssoc($queryResult)) {
	   $primaryid	 	= $fetchResult['primaryid'];
	   $doc_id_c		= $fetchResult['doc_id_c'];
	   $erd_name	 	= $fetchResult['erd_error_documents_name']; 
	   $date_modified	= $fetchResult['ERD_ModifiedDate'];
	   $date_created	= $fetchResult['ERD_CreatedDate'];
	   $deleted	 		= $fetchResult['deleted']; 
	  
	   if($srno%2==0)
		$stylr = "style='background-color: #e6e6e6;'";
	  else
		$stylr = "style='background-color: #f3f3f3;'";
				
		$emailBody .= "<tr><td ".$stylr.">".$srno++."</td>
						<td ".$stylr."><a href='".$site_url."/#Erd_Error_Documents/".$primaryid."' target='_blank'>".$doc_id_c."</a></td> 					
						<td ".$stylr.">".$erd_name."</td>
						<td ".$stylr.">".$date_modified."</td>
						<td ".$stylr.">".$date_created."</td>
						<td ".$stylr.">".$deleted."</td>
						</tr>";
			
	 }
 
  echo  $emailBody .="</table>"; 

		
?>