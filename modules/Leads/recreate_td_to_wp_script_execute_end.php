<?php
global $db;
$site_url = $GLOBALS['sugar_config']['site_url'];

$wpid = $_GET['wpid'];
$wpcid = $_GET['wpcid'];
	/* $wpid    = 'd705376e-0f34-11ed-a507-06be4ff698d4';	
	$wpcid	 = '427ec3f7-d8bd-962f-e186-5785ac5a0b23'; */	
	
	$WpBody = "<h2>List of WP records of Auto-createD TDs : </h2><br/><table width='1250px' cellspacing='5' cellpadding='6' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 20px;'>
	<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
	<th bgcolor='#b3d1ff' align='left'>Work Product </th>
	</tr>";

	/**#2327 : Auto Create TD  from wpc to wp */
	/** Get the wpc bean and fetch the linked Task design records from wpc record */
	$WPCBean = BeanFactory::retrieveBean('M03_Work_Product_Code', $wpcid);
	$WPCBean->load_relationship('m03_work_product_code_taskd_task_design_1');
	$relatedTD = $WPCBean->m03_work_product_code_taskd_task_design_1->get();

	$copyingTaskDesignIds = array();
	$tsArr = array();
	$tsRelationArr = array();

	$p1arr = array();
	$relatedTD = array_slice($relatedTD, 55); 
	foreach ($relatedTD as $TDId) {
		$clonedBean = \BeanFactory::getBean('TaskD_Task_Design', $TDId);
		$other_equipment = $clonedBean->other_equipment_c;
		$clonedBean->load_relationship("taskd_task_design_taskd_task_design_1");

		$sqlGetTaskDesignIds = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_ida as TDId
								FROM taskd_task_design_taskd_task_design_1_c 
								where taskd_task_design_taskd_task_design_1taskd_task_design_idb='$TDId' and deleted=0";
		$resultsSqlGetTaskDesignIds = $db->query($sqlGetTaskDesignIds);
		$rowSqlGetTaskDesignIds = $db->fetchByAssoc($resultsSqlGetTaskDesignIds);
		$taskDesignIds = $rowSqlGetTaskDesignIds['TDId'];

		$wpBean = BeanFactory::getBean('M03_Work_Product', $wpid);
		$clonedBean->id = create_guid();
		$tsArr[$clonedBean->id] = $TDId;
		$tsRelationArr[$TDId] = $taskDesignIds;
		$clonedBean->new_with_id = true;
		$clonedBean->fetched_row = null;
		$date_entered = date("Y-m-d H:i:s", time());

		$p1arr[$TDId] = $taskDesignIds;
		$a1arr[$clonedBean->id] = $TDId;
		$b1arr[$TDId] = $clonedBean->id;
		$clonedBean->type_2 = "Actual SP";
		$clonedBean->other_equipment_c = $other_equipment;
		$copyingTaskDesignIds[] = $clonedBean->id;
		$clonedBean->m03_work_product_taskd_task_design_1->add($wpid);
		$wpName = $wpBean->name;
		$task_type = $clonedBean->task_type;
		$custom_task = $clonedBean->custom_task;
		$standard_task = $clonedBean->standard_task;
		$phase = $clonedBean->phase_c;
		$batch_id       = $clonedBean->bid_batch_id_taskd_task_design_1bid_batch_id_ida;
		$BatchBean      =   BeanFactory::getBean('BID_Batch_ID', $batch_id);
		$BatchName      =   $BatchBean->name;
		$clonedBean->parent_td_id_c = $TDId;
		$clonedBean->date_entered = $date_entered;
		if ($task_type == 'Custom') {
			$standard_task = '';
			$standard_task_val = $custom_task;
		} else if ($task_type == 'Standard') {
			$custom_task = '';
			$standard_task_val = $standard_task;
		}

		if ($BatchName != "") {
			$clonedBean->name = $wpName . ' ' . $phase . ' ' . $BatchName . ' ' . $standard_task_val;
		} else {
			$clonedBean->name = $wpName . ' ' . $phase . ' ' . $standard_task_val;
		}

		if ($standard_task == 'Extract In') {
			//if ($wpBean->extraction_start_integer_c != '' && $wpBean->extraction_end_integer_c != '' && $wpBean->u_units_id_c != '') {
				$clonedBean->start_integer = $wpBean->extraction_start_integer_c;
				$clonedBean->end_integer = $wpBean->extraction_end_integer_c;
				$clonedBean->integer_units = $wpBean->integer_units_c;
				$clonedBean->u_units_id_c = $wpBean->u_units_id_c;
				$unit_Bean = BeanFactory::getBean('U_Units', $clonedBean->u_units_id_c);
				$unit_Bean_name = $unit_Bean->name;
				$clonedBean->time_window = $unit_Bean_name . " " . $clonedBean->start_integer . "-" . $clonedBean->end_integer;
				$offset1 =  21600; //daylight Saving
				if ($wpBean->first_procedure_c != '') {
					$datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
					$actual_datetime       = date("Y-m-d 17:00:00", $datefirst_procedure_c);
					//$GLOBALS['log']->fatal(' Extract IN clonedBean->actual_datetime ' . $clonedBean->actual_datetime);
				}
				if ($wpBean->first_procedure_c != '' && $clonedBean->relative == "NA") {
					$datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
                        $clonedBean->actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
				}
				if ($actual_datetime != "") {
					if ($unit_Bean_name == "Day") {
						if ($clonedBean->relative == "1st Tier") {
							$result = date($actual_datetime);
							$result = strtotime($result . '+' . $clonedBean->start_integer . ' days');
							$clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
							if (date("Y", $result) != "1970") {
								$clonedBean->single_date_2_c = date("Y-m-d", $result);
							}
							$result = date($actual_datetime);
							$result = strtotime($result . '+' . $clonedBean->end_integer . ' days');
							$clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
							//$GLOBALS['log']->fatal(' Extract In DAY 1st Tier clonedBean->relative ' . $clonedBean->relative);
						}
					} else if ($unit_Bean_name == "Hour") {
						if ($clonedBean->relative == "1st Tier") {
							$result = date($actual_datetime);
							$result = strtotime($result . '+' . $clonedBean->start_integer . ' hours');
							$clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
							if (date("Y", $result) != "1970") {
								$clonedBean->single_date_2_c = date("Y-m-d", $result);
							}
							$result = date($actual_datetime);
							$result = strtotime($result . '+' . $clonedBean->end_integer . ' hours');
							$clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
						}
					} else if ($unit_Bean_name == "Minute") {

						if ($clonedBean->relative == "1st Tier") {
							$result = date($actual_datetime);
							$result = strtotime($result . '+' . $clonedBean->start_integer . ' minutes');
							$clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
							if (date("Y", $result) != "1970") {
								$clonedBean->single_date_2_c = date("Y-m-d", $result);
							}
							$result = date($actual_datetime);
							$result = strtotime($result . '+' . $clonedBean->end_integer . ' minutes');
							$clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
						}
					}

					if (($clonedBean->relative == "1st Tier" || $clonedBean->relative == "NA")) {
						$clonedBean->planned_start_datetime_2nd = null;
						$clonedBean->planned_end_datetime_2nd = null;
					}
					if (($clonedBean->relative == "2nd Tier" || $clonedBean->relative == "NA")) {
						$clonedBean->planned_start_datetime_1st = null;
						$clonedBean->planned_end_datetime_1st = null;
					}
				}
			//}
		} else {
			$offset1 =  21600; //daylight Saving
			if ($wpBean->first_procedure_c != '') {
				$datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
				$actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
			}
			if ($wpBean->first_procedure_c != '' && $clonedBean->relative == "NA") {
				$datefirst_procedure_c = strtotime($wpBean->first_procedure_c);
                $clonedBean->actual_datetime = date("Y-m-d 17:00:00", $datefirst_procedure_c);
			}
			$clonedBean->u_units_id_c = $clonedBean->u_units_id_c;
			$unit_Bean = BeanFactory::getBean('U_Units', $clonedBean->u_units_id_c);
			$unit_Bean_name = $unit_Bean->name;
			//$GLOBALS['log']->fatal(' Extract out unit_Bean_name line 198 ' . $unit_Bean_name);
			//$GLOBALS['log']->fatal(' Extract out actual_datetime line 199 ' . $actual_datetime);
			$clonedBean->start_integer = $clonedBean->start_integer;
			$clonedBean->end_integer = $clonedBean->end_integer;
			//$clonedBean->integer_units = $clonedBean->integer_units;
			//$clonedBean->time_window = $clonedBean->time_window;
			$clonedBean->time_window = $unit_Bean_name . " " . $clonedBean->start_integer . "-" . $clonedBean->end_integer;

			if ($actual_datetime != "") {
				if ($unit_Bean_name == "Day") {
					if ($clonedBean->relative == "1st Tier") {
						$result = date($actual_datetime);
						$result = strtotime($result . '+' . $clonedBean->start_integer . ' days');
						$clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
						if (date("Y", $result) != "1970") {
							$clonedBean->single_date_2_c = date("Y-m-d", $result);
						}
						$result = date($actual_datetime);
						$result = strtotime($result . '+' . $clonedBean->end_integer . ' days');
						$clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
						//$GLOBALS['log']->fatal(' Extract OUT DAY 1st Tier clonedBean->relative ' . $clonedBean->relative);
					}
				} else if ($unit_Bean_name == "Hour") {
					//$GLOBALS['log']->fatal(' Not Empty "Minute" clonedBean->relative ' . $clonedBean->relative);
					if ($clonedBean->relative == "1st Tier") {
						$result = date($actual_datetime);
						$result = strtotime($result . '+' . $clonedBean->start_integer . ' hours');
						$clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
						if (date("Y", $result) != "1970") {
							$clonedBean->single_date_2_c = date("Y-m-d", $result);
						}
						$result = date($actual_datetime);
						$result = strtotime($result . '+' . $clonedBean->end_integer . ' hours');
						$clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
					}
				} else if ($unit_Bean_name == "Minute") {
					if ($clonedBean->relative == "1st Tier") {
						$result = date($actual_datetime);
						$result = strtotime($result . '+' . $clonedBean->start_integer . ' minutes');
						$clonedBean->planned_start_datetime_1st = date("Y-m-d H:i:s", $result);
						if (date("Y", $result) != "1970") {
							$clonedBean->single_date_2_c = date("Y-m-d", $result);
						}
						$result = date($actual_datetime);
						$result = strtotime($result . '+' . $clonedBean->end_integer . ' minutes');
						$clonedBean->planned_end_datetime_1st = date("Y-m-d H:i:s", $result);
					}
				}

				if (($clonedBean->relative == "1st Tier" || $clonedBean->relative == "NA")) {
					$clonedBean->planned_start_datetime_2nd = null;
					$clonedBean->planned_end_datetime_2nd = null;
				}
				if (($clonedBean->relative == "2nd Tier" || $clonedBean->relative == "NA")) {
					$clonedBean->planned_start_datetime_1st = null;
					$clonedBean->planned_end_datetime_1st = null;
				}
			}
		}

		$clonedBean->save();

		$auditsql = 'update taskd_task_design_audit 
			set field_name= "u_units_id_c",
			after_value_string="' . $clonedBean->u_units_id_c . '" 
			Where parent_id = "' . $clonedBean->id . '"
			AND field_name="integer_units"';
		$db->query($auditsql);

		$queryAuditLog = "SELECT id,count(*) as NUM FROM `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' 
				AND `parent_id`='" . $clonedBean->id . "' group by before_value_string,after_value_string,date_created";
		$auditLogResult = $db->query($queryAuditLog);
		if ($auditLogResult->num_rows > 0) {
			while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
				if ($fetchAuditLog['NUM'] > 1) {
					$recordID = $fetchAuditLog['id'];
					$sql_DeleteAudit = "DELETE from `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' AND `parent_id`='" . $clonedBean->id . "' AND id = '" . $recordID . "'";
					$db->query($sql_DeleteAudit);
				}
			}
		}

		
	}
	/** To link task design to task design record */
	foreach ($p1arr as $key => $value) {
		$newBeanId = $b1arr[$key];
		$toBeRelateID = array_search($value, $a1arr);
		if ($toBeRelateID != "") {
			$uniqId = create_guid();
			$sqlInsertRelationship = "INSERT INTO `taskd_task_design_taskd_task_design_1_c` 
		(`id`, `date_modified`, `deleted`, `taskd_task_design_taskd_task_design_1taskd_task_design_ida`, `taskd_task_design_taskd_task_design_1taskd_task_design_idb`) 
		VALUES ('$uniqId', now(), '0', '$toBeRelateID', '$newBeanId')";
			$resultsSqlGetTaskDesignIds = $db->query($sqlInsertRelationship);
		}
	}

	$currentWPBean = BeanFactory::retrieveBean('M03_Work_Product', $wpid);
	$currentWPBean->load_relationship('m03_work_product_taskd_task_design_1');
	$relatedTD1 = $currentWPBean->m03_work_product_taskd_task_design_1->get();
	foreach ($relatedTD1 as $tdID) {
		$tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $tdID);
		$u_units = $tdBean->u_units_id_c;
		$unit_Bean = BeanFactory::getBean('U_Units', $u_units);
		$unit_Bean_name = $unit_Bean->name;
		if ($unit_Bean_name == "Day") {
			$integer_u = 'days';
		} elseif ($unit_Bean_name == "Hour") {
			$integer_u = 'hours';
		} else {
			$integer_u = 'minutes';
		}
		if ($tdBean->relative == "1st Tier") {
			$tdBean1 = BeanFactory::retrieveBean('TaskD_Task_Design', $tdBean->id);
			$tdBean1->load_relationship('taskd_task_design_taskd_task_design_1');
			$taskDesignId1 = $tdBean1->taskd_task_design_taskd_task_design_1->get();

			$Get_2nd_tier = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_idb FROM  `taskd_task_design_taskd_task_design_1_c` WHERE `taskd_task_design_taskd_task_design_1taskd_task_design_ida` = '" . $tdBean->id . "' AND deleted = 0;";
			$Get_Result = $db->query($Get_2nd_tier);

			if ($Get_Result->num_rows > 0) {
				while ($fetchResult = $db->fetchByAssoc($Get_Result)) {
					$recordID = $fetchResult['taskd_task_design_taskd_task_design_1taskd_task_design_idb'];
					//$GLOBALS['log']->fatal(' recordID ' . $recordID);
					$tdBean2 = BeanFactory::retrieveBean('TaskD_Task_Design', $recordID);
					//$GLOBALS['log']->fatal(' tdBean->planned_start_datetime_1st ' . $tdBean->planned_start_datetime_1st);
					if ($tdBean->planned_start_datetime_1st != "") {
						$result = date($tdBean->planned_start_datetime_1st);
						$result = strtotime($result . '+' . $tdBean2->start_integer . ' ' . $integer_u);
						$tdBean2->planned_start_datetime_2nd = date("Y-m-d H:i:s", $result);
						if (date("Y", $result) != "1970") {
							$tdBean2->single_date_2_c = date("Y-m-d", $result);
						}
					} else {
						$tdBean2->planned_start_datetime_2nd = null;
					}
					//$GLOBALS['log']->fatal(' tdID1 tdBean->planned_end_datetime_1st ' . $tdBean->planned_end_datetime_1st);
					if ($tdBean->planned_end_datetime_1st != "") {
						$result = date($tdBean->planned_end_datetime_1st);
						$result = strtotime($result . '+' . $tdBean2->end_integer . ' ' . $integer_u);
						$tdBean2->planned_end_datetime_2nd = date("Y-m-d H:i:s", $result);
					} else {
						$tdBean2->planned_end_datetime_2nd = null;
					}
					$tdBean2->save();
				}
			}
		}
	}

	/*********************************** */

	/** To print the wp record which has been update with td creation*/
	$WpBody .= "<tr><td " . $stylr . ">1</td>
	<td " . $stylr . "><a target='_blank' href='" . $site_url . "/#M03_Work_Product/" . $wpid . "'>" . $wpName . "</td>
	</tr>";
	/*** */

echo "<br><br>" . $WpBody .= "</table>";
