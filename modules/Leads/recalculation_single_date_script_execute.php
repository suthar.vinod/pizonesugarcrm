<?php
global $db;
$site_url = $GLOBALS['sugar_config']['site_url'];
$limit = $_GET['limit'];
if ($limit != '') {
	$limit = $limit;
} else {
	$limit = "5";
}
$query = "SELECT IFNULL(taskd_task_design.id,'') primaryid
,IFNULL(taskd_task_design.name,'') taskd_task_design_name
,IFNULL(taskd_task_design.type_2,'') taskd_task_design_type_2,taskd_task_design.actual_datetime - INTERVAL 300 MINUTE actual_datetime,taskd_task_design.planned_start_datetime_1st - INTERVAL 300 MINUTE planned_start_datetime_1st,taskd_task_design.planned_start_datetime_2nd - INTERVAL 300 MINUTE planned_start_datetime_2nd,taskd_task_design.scheduled_start_datetime - INTERVAL 300 MINUTE scheduled_start_datetime,IFNULL(taskd_task_design.relative,'') taskd_task_design_relative,taskd_task_design_cstm.cron_updated_c,taskd_task_design_cstm.single_date_2_c
FROM taskd_task_design
LEFT JOIN taskd_task_design_cstm taskd_task_design_cstm ON taskd_task_design.id = taskd_task_design_cstm.id_c
 WHERE (((((taskd_task_design.actual_datetime - INTERVAL 300 MINUTE > '2022-05-04 01:00:00'
) OR (taskd_task_design.planned_start_datetime_1st - INTERVAL 300 MINUTE > '2022-05-04 01:00:00'
) OR (taskd_task_design.planned_start_datetime_2nd - INTERVAL 300 MINUTE > '2022-05-04 01:00:00'
) OR (taskd_task_design.scheduled_start_datetime - INTERVAL 300 MINUTE > '2022-05-04 01:00:00'
))) AND (((taskd_task_design.type_2 NOT IN ('Plan','Plan SP') OR  taskd_task_design.type_2 IS NULL 
) AND (taskd_task_design_cstm.cron_updated_c != 'yes' OR (taskd_task_design_cstm.cron_updated_c IS NULL)
)))))  
AND  taskd_task_design.deleted=0 limit $limit";
$queryResult = $db->query($query);

$srno = 1;

$WpBody = "<h2>List of TDs with Updated calculation of single date : </h2><br/><table width='1550px' cellspacing='5' cellpadding='6' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 20px;'>
		<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
		<th bgcolor='#b3d1ff' align='left'>Name </th>
		<th bgcolor='#b3d1ff' align='left'>Type</th>
		<th bgcolor='#b3d1ff' align='left'>Actual Date/Time</th>
		<th bgcolor='#b3d1ff' align='left'>Planned Start Date/Time (1st Tier)</th>
		<th bgcolor='#b3d1ff' align='left'>Planned Start Date/Time (2nd Tier) </th>
		<th bgcolor='#b3d1ff' align='left'>Scheduled Start Date/Time </th>	
		<th bgcolor='#b3d1ff' align='left'>Single Date </th>			
		</tr>";


while ($fetchResult = $db->fetchByAssoc($queryResult)) {
	$tdid    									= $fetchResult['primaryid'];
	$tdBean = BeanFactory::retrieveBean('TaskD_Task_Design', $tdid);
	$taskd_task_design_name	 					= $fetchResult['taskd_task_design_name'];
	$type	 									= $fetchResult['taskd_task_design_type_2'];
	$actual_datetime	 						= $fetchResult['actual_datetime'];
	$planned_start_datetime_1st	 				= $fetchResult['planned_start_datetime_1st'];
	$planned_start_datetime_2nd	 				= $fetchResult['planned_start_datetime_2nd'];
	$scheduled_start_datetime	 				= $fetchResult['scheduled_start_datetime'];
	$single_date_2_c	 						= $fetchResult['single_date_2_c'];
	$taskd_task_design_relative	 				= $fetchResult['taskd_task_design_relative'];
	$cron_updated_c	 							= $fetchResult['cron_updated_c'];
	//echo "td bean id => ".$tdBean->id.'<br/>';
	/**Calculation of single date 2 c field for td records */
	if ($taskd_task_design_relative == "NA") {
		$single_date = strtotime($actual_datetime);
		if (!empty($actual_datetime)) {
			$single_date_2_c = date("Y-m-d", $single_date);
		} else if (!empty($scheduled_start_datetime)) {
			$single_date = strtotime($scheduled_start_datetime);
			$single_date_2_c = date("Y-m-d", $single_date);
		}
	} elseif ($taskd_task_design_relative == "1st Tier") {
		if (!empty($actual_datetime)) {
			$single_date = strtotime($actual_datetime);
			$single_date_2_c = date("Y-m-d", $single_date);
		} else if (!empty($planned_start_datetime_1st)) {
			$single_date = strtotime($planned_start_datetime_1st);
			$single_date_2_c = date("Y-m-d", $single_date);
		} else if (!empty($scheduled_start_datetime)) {
			$single_date = strtotime($scheduled_start_datetime);
			$single_date_2_c = date("Y-m-d", $single_date);
		}
	} elseif ($taskd_task_design_relative == "2nd Tier") {
		if (!empty($actual_datetime)) {
			$single_date = strtotime($actual_datetime);
			$single_date_2_c = date("Y-m-d", $single_date);
		} else if (!empty($planned_start_datetime_2nd)) {
			$single_date = strtotime($planned_start_datetime_2nd);
			$single_date_2_c = date("Y-m-d", $single_date);
		} else if (!empty($scheduled_start_datetime)) {
			$single_date = strtotime($scheduled_start_datetime);
			$single_date_2_c = date("Y-m-d", $single_date);
		}
	}
	$tdBean->single_date_2_c = $single_date_2_c;
	$tdBean->cron_updated_c = "yes";
	$tdBean->save();
	/*** end of single date 2 c field calculation */

	if ($srno % 2 == 0)
		$stylr = "style='background-color: #e6e6e6;'";
	else
		$stylr = "style='background-color: #f3f3f3;'";

	$WpBody .= "<tr><td " . $stylr . ">" . $srno++ . "</td>
					<td " . $stylr . "><a target='_blank' href='" . $site_url . "/#TaskD_Task_Design/" . $tdid . "'>" . $taskd_task_design_name . "</td>	
					<td ".$stylr.">".$type."</td>
						<td ".$stylr.">".$actual_datetime."</td>						
						<td ".$stylr.">".$planned_start_datetime_1st."</td>
						<td ".$stylr.">".$planned_start_datetime_2nd."</td>
						<td ".$stylr.">".$scheduled_start_datetime."</td>
						<td ".$stylr.">".$single_date_2_c."</td>
					</tr>";
}

echo "<br><br>" . $WpBody .= "</table>";
