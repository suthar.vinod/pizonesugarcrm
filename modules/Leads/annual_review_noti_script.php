<?php 
global $db;
$current_date = date("Y-m-d"); //current date
$next_30_date = date("Y-m-d", strtotime("+30 days",strtotime($current_date)));
$next_15_date = date("Y-m-d", strtotime("+15 days",strtotime($current_date)));
$next_5_date = date("Y-m-d", strtotime("+5 days",strtotime($current_date)));
$site_url		= $GLOBALS['sugar_config']['site_url'];
$domainToSearch = 'apsemail.com';
   $query = "SELECT  WP.id AS workProductID,
   WP.name AS workProductName, 
   WP.assigned_user_id AS Assigneduser,
   WPCSTM.contact_id_c AS StudyDirector,
   WPCSTM.iacuc_first_annual_review_c,
   WPCSTM.iacuc_second_annual_review_c,
   WPCSTM.iacuc_triennial_review_c,
   WPCSTM.first_annual_review_c,
   WPCSTM.second_annual_review_c,
   WPCSTM.triennial_review_c,
CONCAT(SDContact.first_name,' ', SDContact.last_name) AS StudyDirectorName							
FROM `m03_work_product` AS WP
LEFT JOIN m03_work_product_cstm AS WPCSTM
   ON WP.id = WPCSTM.id_c
LEFT JOIN contacts AS SDContact
ON SDContact.id=WPCSTM.contact_id_c
WHERE  WPCSTM.iacuc_protocol_status_c =  'Active' 
AND (WPCSTM.iacuc_first_annual_review_c = '".$next_30_date."'
OR WPCSTM.iacuc_second_annual_review_c = '".$next_30_date."'
OR WPCSTM.iacuc_triennial_review_c = '".$next_30_date."'
OR WPCSTM.iacuc_first_annual_review_c = '".$next_15_date."'
OR WPCSTM.iacuc_second_annual_review_c = '".$next_15_date."'
OR WPCSTM.iacuc_triennial_review_c = '".$next_15_date."'
OR (WPCSTM.iacuc_first_annual_review_c <= '".$next_5_date."' AND  WPCSTM.iacuc_first_annual_review_c >= '".$current_date."')
OR (WPCSTM.iacuc_second_annual_review_c <= '".$next_5_date."' AND  WPCSTM.iacuc_second_annual_review_c >= '".$current_date."')
OR (WPCSTM.iacuc_triennial_review_c <= '".$next_5_date."' AND  WPCSTM.iacuc_triennial_review_c >= '".$current_date."')
)
AND WP.deleted = 0 
ORDER BY StudyDirectorName ASC";
echo '==> Query ==> '.$query.'<br/>';
	$queryResult = $db->query($query);
		 
	$srno = 1;
	
	$emailBody = "<h2>List of Records Notification of Annual Reviews workflow : </h2><br/><br/><br/><table width='1450px' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>			
			<th bgcolor='#b3d1ff' align='left'> WorkProduct Name </th>
			<th bgcolor='#b3d1ff' align='left'> Assigned User_id </th>
			<th bgcolor='#b3d1ff' align='left'>SD Name</th>
			<th bgcolor='#b3d1ff' align='left'>firstReviewDate</th>
			<th bgcolor='#b3d1ff' align='left'>secondReviewDate</th>
			<th bgcolor='#b3d1ff' align='left'>thirdReviewDate</th>			
			<th bgcolor='#b3d1ff' align='left'>firstReviewDone</th>
			<th bgcolor='#b3d1ff' align='left'>secondReviewDone</th>
			<th bgcolor='#b3d1ff' align='left'>thirdReviewDone</th>
			<th bgcolor='#b3d1ff' align='left'>dueDays</th>";
			
			
    while ($fetchResult = $db->fetchByAssoc($queryResult)) {
        $workProductID		= $fetchResult['workProductID'];
		$workProductName	= $fetchResult['workProductName'];
		$assignedUser_id	= $fetchResult['Assigneduser']; 
        $SD_ID				= $fetchResult['StudyDirector'];
        $SD_Name			= $fetchResult['StudyDirectorName'];
        $firstReviewDate			= $fetchResult['iacuc_first_annual_review_c'];
        $secondReviewDate			= $fetchResult['iacuc_second_annual_review_c'];
        $thirdReviewDate			= $fetchResult['iacuc_triennial_review_c'];
        $firstReviewDone			= $fetchResult['first_annual_review_c'];
        $secondReviewDone			= $fetchResult['second_annual_review_c'];
        $thirdReviewDone			= $fetchResult['triennial_review_c'];
		$dueDays					= "";

		if ($workProductID != "") {
			
			$wpName  = '<a target="_blank" href="'.$site_url.'/#M03_Work_Product/'.$workProductID.'" >'.$workProductName. '</a>';
			
			if(($firstReviewDate==$next_30_date && $firstReviewDone==0) || ($secondReviewDate==$next_30_date && $secondReviewDone==0) || ($thirdReviewDate==$next_30_date && $thirdReviewDone==0) ){
				$dueDays = "in 30 days";
			} 
			
			if(($firstReviewDate==$next_15_date && $firstReviewDone==0) || ($secondReviewDate==$next_15_date && $secondReviewDone==0) || ($thirdReviewDate==$next_15_date && $thirdReviewDone==0) ){
				$dueDays = "in 15 days";
			}
			
			if(($firstReviewDate>=$current_date && $firstReviewDate<=$next_5_date && $firstReviewDone==0))
			{
				$firstReviewDate = date("m-d-Y", strtotime($firstReviewDate));
				$dueDays 		 = " on ".$firstReviewDate;
			}
			
			if($secondReviewDate>=$current_date && $secondReviewDate<=$next_5_date && $secondReviewDone==0) 
			{
				$secondReviewDate = date("m-d-Y", strtotime($secondReviewDate));
				$dueDays 		  = " on ".$secondReviewDate;
			}
			
			if($thirdReviewDate>=$current_date && $thirdReviewDate<=$next_5_date && $thirdReviewDone==0){
				$thirdReviewDate = date("m-d-Y", strtotime($thirdReviewDate));
				$dueDays 		 = " on ".$thirdReviewDate;
			}
			
			

			/*Get Study Director*/
			if($SD_ID!=""){
				$stdy_bean	 = BeanFactory::getBean('Contacts', $SD_ID);
				$studyDirectorName	= $stdy_bean->name;
				$SDEmailAddress		= $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
				
				$domain_name = substr(strrchr($SDEmailAddress, "@"), 1);
				if (trim(strtolower($domain_name) != $domainToSearch)) {
					$SDEmailAddress 	= "";
					$studyDirectorName	= "";
				}
			}
			
			
				
		}	
	 
	   
	   if($srno%2==0)
		$stylr = "style='background-color: #e6e6e6;'";
	  else
		$stylr = "style='background-color: #f3f3f3;'";
			$emailBody .= "<tr><td ".$stylr.">".$srno++."</td>						
			<td ".$stylr."><a target='_blank' href='https://aps.sugarondemand.com/#M03_Work_Product/$workProductID'>".$workProductName."</td> 					
			<td ".$stylr.">".$assignedUser_id."</td>
			<td ".$stylr.">".$SD_Name."</td>
			<td ".$stylr.">".$firstReviewDate."</td>
			<td ".$stylr.">".$secondReviewDate."</td>
			<td ".$stylr.">".$thirdReviewDate."</td>												
			<td ".$stylr.">".$firstReviewDone."</td>
			<td ".$stylr.">".$secondReviewDone."</td>
			<td ".$stylr.">".$thirdReviewDone."</td>
			<td ".$stylr.">".$dueDays."</td>
			</tr>";
	
			
	 }
	 
	 echo $emailBody .="</table>";
