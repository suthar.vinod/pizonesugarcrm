<?php

global $db;
	$current_date = date("Y-m-d"); //current date	
	/**/
	$reportSQL = "SELECT 
						IFNULL(m03_work_product_deliverable.id,'') primaryid,m03_work_product_deliverable_cstm.date_qa_received_c,
						IFNULL(l1.id,'') wp_id,IFNULL(l1.name,'') wp_name,m03_work_product_deliverable_cstm.internal_final_due_date_c wpFinalDueDate,
						l1_cstm.contact_id_c l1_cstm_contact_id_c,LTRIM(RTRIM(CONCAT(IFNULL(contacts1.first_name,''),' ',IFNULL(contacts1.last_name,'')))) contacts1_name,IFNULL(l1_cstm.functional_area_c,'') wp_functional_area_c
						FROM m03_work_product_deliverable
					INNER JOIN  m03_work_product_m03_work_product_deliverable_1_c l1_1 ON m03_work_product_deliverable.id=l1_1.m03_work_pe584verable_idb AND l1_1.deleted=0

					 INNER JOIN  m03_work_product l1 ON l1.id=l1_1.m03_work_p0b66product_ida AND l1.deleted=0
					LEFT JOIN m03_work_product_deliverable_cstm m03_work_product_deliverable_cstm ON m03_work_product_deliverable.id = m03_work_product_deliverable_cstm.id_c
					LEFT JOIN m03_work_product_cstm l1_cstm ON l1.id = l1_cstm.id_c
					LEFT JOIN contacts contacts1 ON contacts1.id = l1_cstm.contact_id_c AND IFNULL(contacts1.deleted,0)=0 
					LEFT JOIN contacts contacts2 ON contacts2.id = l1_cstm.contact_id4_c AND IFNULL(contacts2.deleted,0)=0 

					WHERE (((l1_cstm.functional_area_c = 'Standard Biocompatibility') AND (l1_cstm.work_product_compliance_c = 'GLP')
					AND ((coalesce(LENGTH(l1_cstm.contact_id4_c), 0) = 0)) AND (m03_work_product_deliverable_cstm.deliverable_status_c <> 'Completed' OR ( m03_work_product_deliverable_cstm.deliverable_status_c IS NULL  AND  'Completed' IS NOT NULL )) AND ((coalesce(LENGTH(m03_work_product_deliverable_cstm.date_qa_received_c), 0) <> 0)) AND (m03_work_product_deliverable_cstm.deliverable_c = 'Final Report'
					))) 
					AND  m03_work_product_deliverable.deleted=0 
					 AND IFNULL(contacts1.deleted,0)=0 
					 ORDER BY CASE WHEN (IFNULL(l1_cstm.functional_area_c,'')='' OR IFNULL(l1_cstm.functional_area_c,'') IS NULL) THEN 0
					WHEN IFNULL(l1_cstm.functional_area_c,'')='Analytical Services' THEN 1
					WHEN IFNULL(l1_cstm.functional_area_c,'')='Bioskills' THEN 2
					WHEN IFNULL(l1_cstm.functional_area_c,'')='Biocompatibility' THEN 3
					WHEN IFNULL(l1_cstm.functional_area_c,'')='Standard Biocompatibility' THEN 4
					WHEN IFNULL(l1_cstm.functional_area_c,'')='Histology Services' THEN 5
					WHEN IFNULL(l1_cstm.functional_area_c,'')='ISR' THEN 6
					WHEN IFNULL(l1_cstm.functional_area_c,'')='Pathology Services' THEN 7
					WHEN IFNULL(l1_cstm.functional_area_c,'')='Pharmacology' THEN 8
					WHEN IFNULL(l1_cstm.functional_area_c,'')='Regulatory' THEN 9
					WHEN IFNULL(l1_cstm.functional_area_c,'')='Toxicology' THEN 10 ELSE 11 END ASC
					,wpFinalDueDate ASC";

	$reportResult	= $db->query($reportSQL);
	
	if($reportResult->num_rows>0){
		$site_url		= $GLOBALS['sugar_config']['site_url'];
		//Prepare the Email Template to send to appropriate members
		$template = new EmailTemplate();
		$emailObj = new Email();		
		$template->retrieve_by_string_fields(array('name' => 'Biocomp Report Queue', 'type' => 'email'));
		
		//Email Body Header
		$emailBody = "<table width='100%' cellspacing='2' cellpadding='2' border='1' style='font-family: Tahoma;font-size: 13px;line-height: 18px;'><tr><th bgcolor='#b3d1ff' align='left'>Date Received</th><th bgcolor='#b3d1ff' align='left'>Study ID</th><th bgcolor='#b3d1ff' align='left'>Internal Due Date</th><th bgcolor='#b3d1ff' align='left'>Study Director</th></tr>";
					
		while($fetchReportResult = $db->fetchByAssoc($reportResult)) {
			$wpdID				= $fetchReportResult['primaryid'];
			$dateQA_Received	= $fetchReportResult['date_qa_received_c'];
			$wpID				= $fetchReportResult['wp_id'];
			$wpName				= $fetchReportResult['wp_name'];
			$wpFinalDueDate		= $fetchReportResult['wpFinalDueDate'];
			$SDID				= $fetchReportResult['l1_cstm_contact_id_c'];
			$SDName				= $fetchReportResult['contacts1_name'];
			
			$qaDateObj			= new TimeDate();
			$formattedCD		= $qaDateObj->to_display_date_time($dateQA_Received, true, true, $current_user);
			$formattedCD 		= str_replace('-', '/', $formattedCD);
			$dateQA_Received	= date("m/d/Y",strtotime($formattedCD));
						
			$formattedFinalDate		= $qaDateObj->to_display_date_time($wpFinalDueDate, true, true, $current_user);
			$formattedFinalDate 	= str_replace('-', '/', $formattedFinalDate);
			$wpFinalDueDate			= date("m/d/Y",strtotime($formattedFinalDate));
				
						
			$wpLink		= '<a target="_blank" href="'.$site_url.'/#M03_Work_Product/'.$wpID.'" >'.$wpName . '</a>';
			$sdLink		= '<a target="_blank" href="'.$site_url.'/#Contacts/'.$SDID.'" >'.$SDName . '</a>';
						
			$emailBody .= "<tr><td>".$dateQA_Received."==".$fetchReportResult['date_qa_received_c']."</td><td>".$wpLink."</td><td>".$wpFinalDueDate."==".$fetchReportResult['wpFinalDueDate']."</td><td>".$sdLink."</td></tr>";
		}
	
		$emailBody .="</table>";
		echo $template->body_html = str_replace('[Consolidate_Report]', $emailBody, $template->body_html);
					
		 
	}