<?php 
global $db;

   $query = "Select id, name, category,type_2,other_type_c,related_to_c,date_entered, date_modified, modified_user_id,created_by 
   from ii_inventory_item 
   left join ii_inventory_item_cstm AS II_cstmtbl ON II_cstmtbl.id_c = ii_inventory_item.id
	where category='Specimen' and type_2='Other' and other_type_c!='' and deleted =0 ORDER BY date_modified desc";
	$queryResult = $db->query($query);
		 
	$srno = 1;
	
	$emailBody = "<h2>List of II records of category Specimen and Type Other : </h2><br/><br/><br/><table width='1450px' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th bgcolor='#b3d1ff' align='left'> ID </th>
			<th bgcolor='#b3d1ff' align='left'> Name </th>
			<th bgcolor='#b3d1ff' align='left'>Category</th>
			<th bgcolor='#b3d1ff' align='left'>Type</th>
			<th bgcolor='#b3d1ff' align='left'>Other Specimen Type</th>
			<th bgcolor='#b3d1ff' align='left'>Related To</th>
			<th bgcolor='#b3d1ff' align='left'>Date Created</th>
			<th bgcolor='#b3d1ff' align='left'>Date Modified</th>
			<th bgcolor='#b3d1ff' align='left'>Modified By</th>
			<th bgcolor='#b3d1ff' align='left'>Created By</th>";
			
			
    while ($fetchResult = $db->fetchByAssoc($queryResult)) {
       $id	 = $fetchResult['id'];
	   $name	 				= $fetchResult['name'];
	   $category	 			= $fetchResult['category'];
	   $type_2	 				= $fetchResult['type_2'];
	   $other_type_c	 		= $fetchResult['other_type_c'];
	   $related_to_c	 		= $fetchResult['related_to_c'];
	   $date_entered	 		= $fetchResult['date_entered']; 
	   $date_modified	 		= $fetchResult['date_modified'];
	   $modified_user_id	 	= $fetchResult['modified_user_id']; 
	   $created_by	 			= $fetchResult['created_by'];

	   $clonedBean = BeanFactory::retrieveBean("II_Inventory_Item", $id);
	   
	   $sqlWp = 'SELECT name FROM m03_work_product WHERE id = "' . $clonedBean->m03_work_product_ii_inventory_item_1m03_work_product_ida . '"';
	   $resultWp = $db->query($sqlWp);
	   $rowWp = $db->fetchByAssoc($resultWp);
	   $WpName = $rowWp['name'];


	   $arrPullTSId = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
	   $arrPullUSDAId = array("Lagomorph", "Rat", "Murine", "Hamster", "Guinea Pig");

	   $testSystemBean = BeanFactory::retrieveBean("ANML_Animals", $clonedBean->anml_animals_ii_inventory_item_1anml_animals_ida);
	   $species = $testSystemBean->species_2_c;
	   $TsNameAppend = "";
	   if (in_array($species, $arrPullTSId)) {
		   $TsNameAppend = $testSystemBean->name;
	   } else if (in_array($species, $arrPullUSDAId)) {
		   if ($testSystemBean->usda_id_c != "") {
			   $TsNameAppend = $testSystemBean->usda_id_c;
		   } else {
			   $TsNameAppend = $testSystemBean->name;
		   }
	   } else {
		   $TsNameAppend = $testSystemBean->name;
	   }

	   /**Update the current name with add other type */
	   if ($related_to_c == "Sales" || $related_to_c == "Work Product") 
	   {
			$IIname = explode(" ",$name);
			$lastthreedigit = substr($name, -3);
			$IIname0 = $IIname[0];
			$IIname1 = $IIname[1];
			$IIname2 = $IIname[2];
			$nametoupdate = $IIname0 . ' ' . $other_type_c . ' ' . $lastthreedigit;

	   }
	   else
	   {
			$IIname = explode(" ",$name);
			$lastthreedigit = substr($name, -3);
			$IIname0 = $IIname[0];
			$IIname1 = $IIname[1];
			$IIname2 = $IIname[2];
			$IIname3 = $IIname[3];
			$nametoupdate = $WpName . ' ' . $TsNameAppend . ' ' . $other_type_c . ' ' . $lastthreedigit;
			
	   }
	  // echo '<pre>';
	   //print_r($IIname);
	   if($name !=$nametoupdate)
	   {
		$updatesql = 'update ii_inventory_item set name="' . $nametoupdate . '" Where id = "' . $id . '"';
		echo $updatesql."<br/>";
		$db->query($updatesql);
	   }
	  
	   
	   
	   
			
	 }
	 
	
	
?>