<?php 
global $db;
$site_url = $GLOBALS['sugar_config']['site_url'];

    $query = "SELECT IFNULL(m03_work_product.id,'') wpid
	,IFNULL(m03_work_product.name,'') m03_work_product_name,m03_work_product_code1.id AS wpcid,m03_work_product_code1.name AS wpc_name
	,IFNULL(m03_work_product_cstm.work_product_status_c,'') work_product_status_c,m03_work_product_cstm.functional_area_c AS functional_area_c,m03_work_product_cstm.bc_spa_reconciled_date_c bc_spa_reconciled_date_c
FROM m03_work_product
LEFT JOIN  m03_work_product_taskd_task_design_1_c l1_1 ON m03_work_product.id=l1_1.m03_work_product_taskd_task_design_1m03_work_product_ida AND l1_1.deleted=0

LEFT JOIN  taskd_task_design l1 ON l1.id=l1_1.m03_work_product_taskd_task_design_1taskd_task_design_idb AND l1.deleted=0
LEFT JOIN m03_work_product_cstm m03_work_product_cstm ON m03_work_product.id = m03_work_product_cstm.id_c
LEFT JOIN m03_work_product_code m03_work_product_code1 ON m03_work_product_code1.id = m03_work_product_cstm.m03_work_product_code_id1_c AND m03_work_product_code1.deleted=0 

  WHERE m03_work_product.id IN ('46bce672-852b-11ec-9c47-02fb813964b8','52b458a0-7fb5-11ec-aebe-06eddc549468','87e66260-aeaa-11ec-8064-0221985176a7','52f5c08a-a46b-11ec-b0fb-06eddc549468','531597a8-6e5a-11ec-a268-065a04235e91','4b194a82-9e24-11ec-ab36-06ef2d010d1d','53ec0574-9987-11ec-b6c0-06e41dba421a','803918a6-6da9-11ec-97ba-029395602a62','6641d244-7a06-11ec-a5d0-02fb813964b8','ced8024a-90dd-11ec-9170-065a04235e91','5a42542e-90df-11ec-ac1f-0221985176a7','b4766d86-a958-11ec-9297-065a04235e91','b489bfcc-7450-11ec-b250-0221985176a7','b6a80d24-a635-11ec-b736-02fb813964b8','b7dfb3ea-a635-11ec-82f3-0221985176a7','bdcb1344-9bea-11ec-a1e3-02fb813964b8','be26c74c-9b2d-11ec-b8bd-0221985176a7','4c85922c-6e68-11ec-a518-029395602a62','4ceb3d52-9e24-11ec-92fb-06eddc549468','4e579208-6e68-11ec-b7ef-0221985176a7','fa50c444-935c-11ec-ac0b-065a04235e91','5e35b716-b1ef-11ec-82c9-065a04235e91','5e8b29c2-abb3-11ec-89cb-065a04235e91','efe5a36a-7243-11ec-b40d-02fb813964b8','5efce17a-6e5a-11ec-aa27-06eddc549468','c4501c1c-904d-11ec-9afe-06e41dba421a','13704e3a-6e3d-11ec-a913-06e41dba421a','711bb1f0-94d0-11ec-8d02-0221985176a7','5af98894-6e5a-11ec-b1e9-06eddc549468','b9de4788-a635-11ec-af6f-029395602a62','d1b44b72-90dd-11ec-9442-06eddc549468','d1ebad92-99a2-11ec-9b4c-029395602a62','d1f2ab40-8a9a-11ec-8973-065a04235e91','432a755c-b02c-11ec-8ddc-029395602a62','e9cb393a-99a4-11ec-b7d8-06ef2d010d1d','c35b4438-a481-11ec-a5bf-029395602a62','9375fd60-b6a8-11ec-8a79-06ef2d010d1d','93c71fd0-a3a5-11ec-8108-0221985176a7','16e13d36-6e51-11ec-a20c-06e41dba421a','de59b526-9fe0-11ec-9d34-02fb813964b8','df172ec6-82c6-11ec-a5e3-06eddc549468','68532dda-7a06-11ec-96c2-06eddc549468','e6ebd920-b5c6-11ec-8018-029395602a62','f12c3a40-a632-11ec-8cc1-06eddc549468','87a06664-94df-11ec-84e5-029395602a62','8a7d7942-a16b-11ec-ac5f-029395602a62','e670a01e-aede-11ec-80d6-06e41dba421a','8e30c47c-9a45-11ec-952f-029395602a62','e699485c-9e44-11ec-9afc-029395602a62','90a3aea4-9a36-11ec-8805-06eddc549468','e000a402-7bb9-11ec-888a-06ef2d010d1d','915925bc-a6e4-11ec-9b42-06ef2d010d1d','938507e8-b44f-11ec-8a67-02fb813964b8','f1adfa5c-1bcd-11ec-b783-06eddc549468','3e27eb7e-852b-11ec-a8ea-06ef2d010d1d','3e83a44c-ac5d-11ec-9571-06eddc549468','3f7cb556-6e68-11ec-a230-06eddc549468','c832e4d6-9b33-11ec-9663-06eddc549468','3fd0aaf4-9a5e-11ec-a635-06e41dba421a','451d4840-6e68-11ec-995e-06eddc549468','45a23124-9413-11ec-a91a-06eddc549468','40ac21d8-9a5e-11ec-adb5-06ef2d010d1d','4735f08e-9b07-11ec-84f5-0221985176a7','fe37d79a-2161-11ec-b403-06eddc549468','ead56e2c-99a4-11ec-a2d3-065a04235e91','f2bc4070-8f63-11ec-869f-02fb813964b8','f2dfc7d4-9422-11ec-8b6f-06ef2d010d1d','f30678b2-7243-11ec-8642-06eddc549468','f352b420-95b6-11ec-b6d3-029395602a62','8bc594dc-abc6-11ec-98c8-06ef2d010d1d','c1678774-a3df-11ec-a4b8-065a04235e91','1ce5aa86-7bcc-11ec-9c3a-06ef2d010d1d','c34dbb0a-a185-11ec-808f-0221985176a7','1cf3f202-a94a-11ec-836f-065a04235e91','552a2694-6e5a-11ec-b9ab-065a04235e91','e11480a0-b02c-11ec-9fe5-06ef2d010d1d','7b92676e-74aa-11ec-b9f8-065a04235e91','d3e0d104-a3de-11ec-a94f-0221985176a7','1e3fabc0-997c-11ec-a63e-02fb813964b8','7c6107b0-935c-11ec-ad0b-029395602a62','c9896804-999a-11ec-b1a5-02fb813964b8','62450c42-7a06-11ec-88a6-0221985176a7','e12b8586-82c6-11ec-88d8-06ef2d010d1d','a6dada64-9fc0-11ec-91a8-0221985176a7','2e28a690-6e61-11ec-85c4-02fb813964b8','7d6e35be-b513-11ec-90b1-029395602a62','6aea5e10-7a06-11ec-9228-065a04235e91','ca68a044-9415-11ec-b5b7-06eddc549468','37901bd8-a560-11ec-93f6-06eddc549468','2f1ae212-94df-11ec-a42c-065a04235e91','afff3b1c-9e4e-11ec-bd1c-06e41dba421a','31fa5d0e-6e61-11ec-8558-0221985176a7','35db6fc6-6e61-11ec-8c5a-0221985176a7','37bcf968-6e61-11ec-bee9-029395602a62','3924f072-a560-11ec-8b19-065a04235e91','3ccc2256-73b3-11ec-9b66-06eddc549468','3eb6f28a-73b3-11ec-9169-0221985176a7','424a5f00-9a5e-11ec-9dbf-02fb813964b8','ec83cebc-9689-11ec-99f6-02fb813964b8','49373370-9b07-11ec-893b-02fb813964b8','f425ccd4-b6a9-11ec-bdb4-06eddc549468','38669bcc-9b29-11ec-8f0e-0221985176a7','86cf982e-aeaa-11ec-8518-06eddc549468','cbeb57b8-9415-11ec-9898-0221985176a7','05561eae-7bbe-11ec-b37a-02fb813964b8','b5ddd636-9e3c-11ec-a20d-0221985176a7','643b2c6a-9b31-11ec-8896-06e41dba421a','b5e18308-62a5-11ec-95ce-065a04235e91','cc664f58-7bb1-11ec-b6b8-065a04235e91','cd00f94a-90dd-11ec-b7e6-06e41dba421a','6d3b486e-b750-11ec-b886-065a04235e91','72dc90f2-7a06-11ec-af7b-0221985176a7','7bb2824e-9a41-11ec-840f-06e41dba421a','83c812ec-6da9-11ec-a593-065a04235e91','e86da6aa-78a1-11ec-a865-065a04235e91','8e2a143c-9663-11ec-9950-029395602a62','ee772ba4-7bb5-11ec-9db9-06eddc549468','9b1953c0-baa2-11ec-acd5-06ef2d010d1d','eeccb4b4-6e4c-11ec-a4f8-06e41dba421a','8ec4aebc-9f09-11ec-ae7e-06e41dba421a','8fcbc5ea-94df-11ec-8de1-06ef2d010d1d','84f1ade8-ac5d-11ec-8f28-029395602a62','13dc307e-77d8-11ec-993e-06eddc549468','5919c084-6e5a-11ec-b677-06e41dba421a','3c2dbd4e-852b-11ec-90dc-06e41dba421a','1ac41070-a94a-11ec-9654-0221985176a7','1add711a-a15b-11ec-95b7-0221985176a7','204ca9a4-997c-11ec-8162-029395602a62','20d976d2-6e51-11ec-8fe6-02fb813964b8','213ad520-997c-11ec-80b0-06eddc549468','873d013c-a3d0-11ec-b33d-029395602a62','b8c6f8c2-a635-11ec-8ebf-065a04235e91')
AND  m03_work_product.deleted=0";
echo "query ==>".$query.'<br/>';
	$queryResult = $db->query($query);
		 
	$srno = 1;
	
	$WpBody = "<h2>List of WP records of Auto-create TDs in one time event : </h2><br/><table width='1250px' cellspacing='5' cellpadding='6' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 20px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th bgcolor='#b3d1ff' align='left'>Work Product </th>
			<th bgcolor='#b3d1ff' align='left'>Functional Area</th>
			<th bgcolor='#b3d1ff' align='left'>Work Product Phase</th>
			<th bgcolor='#b3d1ff' align='left'>Work Product Code</th>
			<th bgcolor='#b3d1ff' align='left'>SPA Reconciled Date</th>
			<th bgcolor='#b3d1ff' align='left'># Of TD Linked to WP</th>
			<th bgcolor='#b3d1ff' align='left'># Of TD Linked to WPC</th>
			</tr>";
			
			
    while ($fetchResult = $db->fetchByAssoc($queryResult)) {
       $wpid    = $fetchResult['wpid'];
	   /**Get the task design records linked to fetched work producut */
	   $tsddataQuery = "SELECT count(m03_work_product_taskd_task_design_1taskd_task_design_idb) as CountTD FROM m03_work_product_taskd_task_design_1_c
	   where m03_work_product_taskd_task_design_1m03_work_product_ida ='".$wpid."' and deleted=0";
	   $restsddataQuery = $db->query($tsddataQuery);
	   $rowtsddataQuery = $db->fetchByAssoc($restsddataQuery);
	   $CountTD = $rowtsddataQuery['CountTD'];
	   $m03_work_product_name	 		= $fetchResult['m03_work_product_name']; 
	   $wpcid	 						= $fetchResult['wpcid'];
	   /**Get the task design linked to work product code */
	   $wpctsQuery = "SELECT count(m03_work_product_code_taskd_task_design_1taskd_task_design_idb) AS CountWPCTD FROM m03_work_product_code_taskd_task_design_1_c
	   where m03_work_p7fc0ct_code_ida='".$wpcid."' and deleted=0";
	   $reswpctdQuery = $db->query($wpctsQuery);
	   $rowwpctdQuery = $db->fetchByAssoc($reswpctdQuery);
	   $CountWPCTD = $rowwpctdQuery['CountWPCTD'];
	   /************************************************ */
	   $wpc_name	 					= $fetchResult['wpc_name'];
	   $work_product_status_c	 		= $fetchResult['work_product_status_c']; 
	   $functional_area_c	 				= $fetchResult['functional_area_c'];
	   $bc_spa_reconciled_date_c	 	= $fetchResult['bc_spa_reconciled_date_c'];
	   $date_entered	 				= $fetchResult['date_entered'];
	   
	   if($srno%2==0)
		$stylr = "style='background-color: #e6e6e6;'";
	  else
		$stylr = "style='background-color: #f3f3f3;'";
		if($CountTD < 1)
		{
			$WpBody .= "<tr><td ".$stylr.">".$srno++."</td>
						<td ".$stylr."><a target='_blank' href='" . $site_url . "/#M03_Work_Product/" . $wpid. "'>".$m03_work_product_name."</td>
						<td ".$stylr.">".$work_product_status_c."</td>
						<td ".$stylr.">".$functional_area_c."</td>
						<td ".$stylr."><a target='_blank'  href='" . $site_url . "/#M03_Work_Product_Code/" . $wpcid. "'>".$wpc_name."</td>
						<td ".$stylr.">".$bc_spa_reconciled_date_c."</td>
						<td ".$stylr.">".$CountTD."</td>
						<td ".$stylr.">".$CountWPCTD."</td>
						</tr>";
		}	
		
			
	 }
	 
	 echo "<br><br>".$WpBody .="</table>";
