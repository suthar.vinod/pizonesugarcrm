<?php
global $db;
$site_url = $GLOBALS['sugar_config']['site_url'];

$CopyFromid = $_GET['CopyFromid'];
$CopyToid = $_GET['CopyToid'];
$first = $_GET['first'];
$last = $_GET['last'];

$i = 1;

$currentGDBean = BeanFactory::retrieveBean('GD_Group_Design', $CopyFromid);
$copytoGDBean = BeanFactory::retrieveBean('GD_Group_Design', $CopyToid);
$coytoGDname = $copytoGDBean->name;
//echo $coytoGDname;
$newworkProductId = $copytoGDBean->m03_work_product_gd_group_design_1m03_work_product_ida;
$newworkProductname = $copytoGDBean->m03_work_product_gd_group_design_1_name;
$currentGDBean->load_relationship('gd_group_design_taskd_task_design_1');
$relatedTD = $currentGDBean->gd_group_design_taskd_task_design_1->get();

$copyingTaskDesignIds = array();

foreach ($relatedTD as $TDId) {
	if ($i >= $first && $i <= $last)
	 {
		$clonedBean = BeanFactory::getBean('TaskD_Task_Design', $TDId);
		$other_equipment = $clonedBean->other_equipment_c;
		$clonedBean->load_relationship("taskd_task_design_taskd_task_design_1");
		$clonedBean->load_relationship("gd_group_design_taskd_task_design_1");
		//$clonedBean->load_relationship("anml_animals_taskd_task_design_1");
		$workProductId = $newworkProductId;
		$groupDesignIds = $CopyToid;
		$date_entered = date("Y-m-d H:i:s", time());
		/* Custom Query to get Task design Ids */
		$sqlGetTaskDesignIds = "SELECT taskd_task_design_taskd_task_design_1taskd_task_design_ida as TDId
								FROM taskd_task_design_taskd_task_design_1_c 
								where taskd_task_design_taskd_task_design_1taskd_task_design_idb='$TDId' and deleted=0";
		$resultsSqlGetTaskDesignIds = $db->query($sqlGetTaskDesignIds);
		$rowSqlGetTaskDesignIds = $db->fetchByAssoc($resultsSqlGetTaskDesignIds);
		$taskDesignIds = $rowSqlGetTaskDesignIds['TDId'];
		//$testSystemIds = $clonedBean->anml_animals_taskd_task_design_1->get();
		$clonedBean->id  = create_guid();
		$clonedBean->new_with_id = true;
		$clonedBean->relative = $clonedBean->relative;
		$clonedBean->category = $clonedBean->category;
		$clonedBean->other_equipment_c = $other_equipment;
		$clonedBean->date_entered = $date_entered;
		$copyingTaskDesignIds[] = $clonedBean->id;
		$p1arr[$TDId] = $taskDesignIds;
		$a1arr[$clonedBean->id] = $TDId;
		$b1arr[$TDId] = $clonedBean->id;
		$clonedBean->u_units_id_c = $clonedBean->u_units_id_c;
		$clonedBean->end_integer = $clonedBean->end_integer;
		$type = $clonedBean->type_2;
		$relative = $clonedBean->relative;
		if ($relative == "NA") {
			$clonedBean->planned_end_datetime_1st = null;
			$clonedBean->planned_start_datetime_1st = null;
			$clonedBean->planned_start_datetime_2nd = null;
			$clonedBean->planned_end_datetime_2nd = null;
		} else if ($relative == "1st Tier") {
			$clonedBean->actual_datetime = null;
			$clonedBean->scheduled_start_datetime = null;
			$clonedBean->scheduled_end_datetime = null;
			$clonedBean->planned_start_datetime_2nd = null;
			$clonedBean->planned_end_datetime_2nd = null;
		} else if ($relative == "2nd Tier") {
			$clonedBean->actual_datetime = null;
			$clonedBean->scheduled_start_datetime = null;
			$clonedBean->scheduled_end_datetime = null;
			$clonedBean->planned_start_datetime_1st = null;
			$clonedBean->planned_end_datetime_1st = null;
		}

		if ($type == "Plan") {
			$clonedBean->actual_datetime = null;
			$clonedBean->scheduled_start_datetime = null;
			$clonedBean->scheduled_end_datetime = null;
			$clonedBean->planned_start_datetime_1st = null;
			$clonedBean->planned_end_datetime_1st = null;
			$clonedBean->planned_start_datetime_2nd = null;
			$clonedBean->planned_end_datetime_2nd = null;
		}
		$clonedBean->save();
		if ($workProductId != '') {
			$clonedBean->m03_work_product_taskd_task_design_1->add($workProductId);
		}
		/** we have comment the code to fix the bug 2116 : Mismatched Group Designs on Task Designs : 11 feb 2022 */
		/*  if ($taskDesignIds != '') {
	$clonedBean->taskd_task_design_taskd_task_design_1_right->add($taskDesignIds);        
  } */
		/* if ($testSystemIds != '') {
			$clonedBean->anml_animals_taskd_task_design_1->add($testSystemIds);
		} */
		if ($CopyToid != '') {
			$clonedBean->gd_group_design_taskd_task_design_1->add($CopyToid);
		}
		if ($clonedBean->type_2 == "Plan") {
			$sql_DeleteAudit = "DELETE from `taskd_task_design_audit` 
					WHERE `field_name` in ('actual_datetime','scheduled_start_datetime','scheduled_end_datetime','planned_start_datetime_1st','planned_end_datetime_1st','planned_start_datetime_2nd','planned_end_datetime_2nd')  
					AND `parent_id`='" . $clonedBean->id . "'";
			$db->query($sql_DeleteAudit);
		}
		$auditsql = 'update taskd_task_design_audit 
					 set field_name= "u_units_id_c",
					 after_value_string="' . $clonedBean->u_units_id_c . '" 
					 Where parent_id = "' . $clonedBean->id . '"
					 AND field_name="integer_units"';
		$db->query($auditsql);
		$queryAuditLog = "SELECT id,count(*) as NUM FROM `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' 
		AND `parent_id`='" . $clonedBean->id . "' group by before_value_string,after_value_string,date_created";
		$auditLogResult = $db->query($queryAuditLog);
		if ($auditLogResult->num_rows > 0) {
			while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
				if ($fetchAuditLog['NUM'] > 1) {
					$recordID = $fetchAuditLog['id'];
					$sql_DeleteAudit = "DELETE from `taskd_task_design_audit` WHERE `field_name`='u_units_id_c' AND `parent_id`='" . $clonedBean->id . "' AND id = '" . $recordID . "'";
					$db->query($sql_DeleteAudit);
				}
			}
		}
		if ($type == "Plan") {
			/*#1353 : Custom name calculation 10 June 2021 */
			$wpBean = BeanFactory::getBean('M03_Work_Product', $workProductId[0]);
			$wpName =  $wpBean->name;
			$gdBean = BeanFactory::getBean('GD_Group_Design', $CopyToid);
			$gdName =  $gdBean->name;
			$task_type = $clonedBean->task_type;

			$gdNameNumber = "";
			if ($gdName != '') {
				$gdNameNumber = substr($gdName, strrpos($gdName, ' ') + 1);
			}

			$custom_task = $clonedBean->custom_task;
			$standard_task = $clonedBean->standard_task;

			if ($task_type == 'Custom') {
				$standard_task = '';
				$standard_task_val = $custom_task;
			} else if ($task_type == 'Standard') {
				$custom_task = '';
				$standard_task_val = $standard_task;
			}
			$clonedBean->name = $wpName . ' ' . $gdNameNumber . ' ' . $standard_task_val;
			$clonedBean->save();
			/*EOC #1353 : Custom name calculation 10 June 2021 */

			$source = '{"subject":{"_type":"logic-hook","class":"CopyTaskDesignToGD","method":"CopyTaskDesignToGD"},"attributes":[]}';
			if ($clonedBean->relative != '') {
				$auditEventid = create_guid();
				$auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
	  values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"relative","datetime","","' . $clonedBean->relative . '")';
				$auditsqlResult_1 = $db->query($auditsql_1);

				$auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
				$db->query($auditsqlStatus_1);
			}

			if ($clonedBean->category != '') {
				$auditEventid = create_guid();
				$auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
	  values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"category","datetime","","' . $clonedBean->category . '")';
				$auditsqlResult_1 = $db->query($auditsql_1);

				$auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
				$db->query($auditsqlStatus_1);
			}

			if ($clonedBean->type_2 != '') {
				$auditEventid = create_guid();
				$auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
	  values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"type_2","datetime","","' . $clonedBean->type_2 . '")';
				$auditsqlResult_1 = $db->query($auditsql_1);

				$auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
				$db->query($auditsqlStatus_1);
			}

			if ($clonedBean->task_type != '') {
				$auditEventid = create_guid();
				$auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
	  values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"task_type","datetime","","' . $clonedBean->task_type . '")';
				$auditsqlResult_1 = $db->query($auditsql_1);

				$auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
				$db->query($auditsqlStatus_1);
			}

			if ($clonedBean->standard_task != '') {
				$auditEventid = create_guid();
				$auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
	  values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"standard_task","datetime","","' . $clonedBean->standard_task . '")';
				$auditsqlResult_1 = $db->query($auditsql_1);

				$auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
				$db->query($auditsqlStatus_1);
			}

			if ($clonedBean->start_integer != '') {
				$auditEventid = create_guid();
				$auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
	  values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"start_integer","datetime","","' . $clonedBean->start_integer . '")';
				$auditsqlResult_1 = $db->query($auditsql_1);

				$auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
				$db->query($auditsqlStatus_1);
			}

			if ($clonedBean->end_integer != '') {
				$auditEventid = create_guid();
				$auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
	  values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"end_integer","datetime","","' . $clonedBean->end_integer . '")';
				$auditsqlResult_1 = $db->query($auditsql_1);
				$auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
				$db->query($auditsqlStatus_1);
			}

			if ($clonedBean->time_window != '') {
				$auditEventid = create_guid();
				$auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
	  values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"time_window","datetime","","' . $clonedBean->time_window . '")';
				$auditsqlResult_1 = $db->query($auditsql_1);

				$auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
				$db->query($auditsqlStatus_1);
			}

			if ($clonedBean->custom_task != '') {
				$auditEventid = create_guid();
				$auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
	  values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"custom_task","datetime","","' . $clonedBean->custom_task . '")';
				$auditsqlResult_1 = $db->query($auditsql_1);

				$auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
				$db->query($auditsqlStatus_1);
			}

			if ($clonedBean->order_2_c != '') {
				$auditEventid = create_guid();
				$auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
	  values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"order_2_c","datetime","","' . $clonedBean->order_2_c . '")';
				$auditsqlResult_1 = $db->query($auditsql_1);

				$auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
				$db->query($auditsqlStatus_1);
			}

			if ($clonedBean->equipment_required != '') {
				$auditEventid = create_guid();
				$auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
	  values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"equipment_required","datetime","","' . $clonedBean->equipment_required . '")';
				$auditsqlResult_1 = $db->query($auditsql_1);

				$auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
				$db->query($auditsqlStatus_1);
			}

			if ($clonedBean->other_equipment_c != '') {
				$auditEventid = create_guid();
				$auditsql_1 = 'INSERT INTO taskd_task_design_audit (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
	  values("' . create_guid() . '","' . $clonedBean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"other_equipment_c","datetime","","' . $clonedBean->other_equipment_c . '")';
				$auditsqlResult_1 = $db->query($auditsql_1);

				$auditsqlStatus_1 = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $clonedBean->id . "','TaskD_Task_Design','" . $source . "')";
				$db->query($auditsqlStatus_1);
			}
		}
	}
	$i++;
}

 foreach ($p1arr as $key => $value) {
	$newBeanId = $b1arr[$key];
	$newBean = BeanFactory::getBean('TaskD_Task_Design', $newBeanId);
	$newBean->load_relationship("taskd_task_design_taskd_task_design_1_right");
	$toBeRelateID = array_search($value, $a1arr);
	/**11 feb 2022 changes : bug fixes 2116 */
/* if ($toBeRelateID != "") {
	$newBean->save();
	$newBean->taskd_task_design_taskd_task_design_1_right->add($toBeRelateID);
  } */
if ($toBeRelateID != "") {
		$uniqId = create_guid();
		$sqlInsertRelationship = "INSERT INTO `taskd_task_design_taskd_task_design_1_c` 
		(`id`, `date_modified`, `deleted`, `taskd_task_design_taskd_task_design_1taskd_task_design_ida`, `taskd_task_design_taskd_task_design_1taskd_task_design_idb`) 
		VALUES ('" . $uniqId . "', now(), '0', '" . $toBeRelateID . "', '" . $newBeanId . "')";
		$resultsSqlGetTaskDesignIds = $db->query($sqlInsertRelationship);
	}

	if ($newBean->type_2 == "Plan") {
		$sql_DeleteAudit = "DELETE from `taskd_task_design_audit` 
					WHERE `field_name` in ('actual_datetime','scheduled_start_datetime','scheduled_end_datetime','planned_start_datetime_1st','planned_end_datetime_1st','planned_start_datetime_2nd','planned_end_datetime_2nd')  
					AND `parent_id`='" . $newBean->id . "'";
		$db->query($sql_DeleteAudit);
	}
} 
/*********************************** */



if (count($copyingTaskDesignIds) > 0) {
	echo "Task Designs have been copied successfully into selected Group Design.";
} else {
	echo "Error! Something went wrong";
}
