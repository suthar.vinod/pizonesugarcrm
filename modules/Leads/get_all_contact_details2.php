<?php 
	global $db;
 
    $query = 'SELECT contacts.first_name, contacts.last_name, concat(contacts.first_name," ", contacts.last_name) AS name, concat(contacts.first_name," ", contacts.last_name) AS FullName, contacts.id, contacts.salutation, contacts.salutation as SalutationDisplayLabel,contacts.title, contacts.department, accounts.name AS company_name,contacts.phone_mobile, contacts.phone_work, contacts.phone_home, contacts.phone_other, contacts.phone_fax, contacts.primary_address_street, contacts.primary_address_city, contacts.primary_address_state, contacts.primary_address_postalcode, contacts.primary_address_country, email_addresses.email_address as email, ca_company_address.name AS FullAddress FROM contacts 
				LEFT JOIN accounts_contacts ON contacts.id = accounts_contacts.contact_id
				LEFT JOIN accounts ON accounts.id = accounts_contacts.account_id
				LEFT JOIN accounts_ca_company_address_1_c ON accounts_ca_company_address_1_c.accounts_ca_company_address_1accounts_ida = accounts_contacts.account_id
				LEFT JOIN ca_company_address ON accounts_ca_company_address_1_c.accounts_ca_company_address_1ca_company_address_idb = ca_company_address.id
				LEFT JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id = contacts.id
				LEFT JOIN email_addresses ON email_addresses.id = email_addr_bean_rel.email_address_id
				WHERE (contacts.first_name != "" OR contacts.first_name != NULL) GROUP BY  contacts.id ORDER BY contacts.first_name, contacts.last_name ASC';

	$queryResult = $db->query($query);
		 
	$srno = 1;
	
	$tableBody = "<table width='1050px' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th bgcolor='#b3d1ff' align='left'>First Name</th>
			<th bgcolor='#b3d1ff' align='left'>Last Name</th>			
			<th bgcolor='#b3d1ff' align='left'>Salutation</th>
			<th bgcolor='#b3d1ff' align='left'>Full Name</th>
			<th bgcolor='#b3d1ff' align='left'>Title</th>
			<th bgcolor='#b3d1ff' align='left'>Email Address</th>
			<th bgcolor='#b3d1ff' align='left'>Department</th>
			<th bgcolor='#b3d1ff' align='left'>Company Name</th> 
			<th bgcolor='#b3d1ff' align='left'>Mobile Phone</th>
			<th bgcolor='#b3d1ff' align='left'>Primary Office Phone</th>
			<th bgcolor='#b3d1ff' align='left'>Home Phone</th>
			<th bgcolor='#b3d1ff' align='left'>Other Phone</th>
			<th bgcolor='#b3d1ff' align='left'>Primary Address Street</th>
			<th bgcolor='#b3d1ff' align='left'>Primary Address City</th>
			<th bgcolor='#b3d1ff' align='left'>Primary Address State</th>
			<th bgcolor='#b3d1ff' align='left'>Primary Address Postal Code</th>
			<th bgcolor='#b3d1ff' align='left'>Primary Address Country</th>
			<th bgcolor='#b3d1ff' align='left'>Full Address</th>
			</tr>";
			
			
    while ($fetchResult = $db->fetchByAssoc($queryResult)) {
        $first_name	 	= $fetchResult['first_name'];
		$last_name	 		    = $fetchResult['last_name'];
		$ID 		= $fetchResult['id'];
		$salutation		= $fetchResult['salutation'];
		$SalutationDisplayLabel	= $fetchResult['SalutationDisplayLabel'];
		$title	= $fetchResult['title'];
		$department	= $fetchResult['department'];
		$company_name	= $fetchResult['company_name'];
		$phone_mobile	= $fetchResult['phone_mobile'];
		$phone_work	= $fetchResult['phone_work'];
		$phone_home	= $fetchResult['phone_home'];
		$phone_other	= $fetchResult['phone_other'];
		$phone_fax	= $fetchResult['phone_fax'];
		$primary_address_street	= $fetchResult['primary_address_street'];
		$primary_address_city	= $fetchResult['primary_address_city'];
		$primary_address_state	= $fetchResult['primary_address_state'];
		$primary_address_postalcode	= $fetchResult['primary_address_postalcode'];
		$primary_address_country	= $fetchResult['primary_address_country'];
		$name 		= $fetchResult['name'];
		$FullName		    = $fetchResult['FullName'];
	    $FullAddress	= $fetchResult['FullAddress'];  
		$email_address	= $fetchResult['email'];		
		 	
		if($srno%2==0)
			$stylr = "style='background-color: #e6e6e6;'";
		else
			$stylr = "style='background-color: #f3f3f3;'";

		$tableBody .= "<tr><td ".$stylr.">".$srno++."</td>
							<td ".$stylr.">".$first_name."</td> 
							<td ".$stylr.">".$last_name."</td>
							<td ".$stylr.">".$salutation."</td>
							<td ".$stylr.">".$FullName."</td> 
							<td ".$stylr.">".$title."</td>
							<td ".$stylr.">".$email_address."</td>
							<td ".$stylr.">".$department."</td> 
							<td ".$stylr.">".$company_name."</td>
							<td ".$stylr.">".$phone_mobile."</td> 
							<td ".$stylr.">".$phone_work."</td>
							<td ".$stylr.">".$phone_home."</td> 
							<td ".$stylr.">".$phone_other."</td>
							<td ".$stylr.">".$primary_address_street."</td>
							<td ".$stylr.">".$primary_address_city."</td>
							<td ".$stylr.">".$primary_address_state."</td>
							<td ".$stylr.">".$primary_address_postalcode."</td>
							<td ".$stylr.">".$primary_address_country."</td>
							<td ".$stylr.">".$FullAddress."</td>	 		
							</tr>";		
	}	 
	echo "<br><br>".$tableBody .="</table>";
