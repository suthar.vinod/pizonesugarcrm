<?php 
global $db;

   $query = "SELECT
   IFNULL(ori_order_request_item.id, '') primaryid,
   IFNULL(ori_order_request_item.name, '') name,
   IFNULL(ori_order_request_item_cstm.department_new_c, '') department_new_c,
   IFNULL(l1.id, '') l1_id,
   IFNULL(l1.name, '') prod_product_ori_order_request_item_1_name,
   IFNULL(l1.product_id_2, '') prod_product_ori_order_request_item_1_product_id_2,
   IFNULL(ori_order_request_item.product_name, '') product_name,
   IFNULL(ori_order_request_item.product_id_2, '') product_id_2,
   ori_order_request_item_cstm.user_id_c,
   concat(users.first_name,' ',users.last_name) AS requestedBy
   FROM ori_order_request_item 
   LEFT JOIN prod_product_ori_order_request_item_1_c l1_1 
   ON ori_order_request_item.id = l1_1.prod_product_ori_order_request_item_1ori_order_request_item_idb AND l1_1.deleted = 0 
   LEFT JOIN prod_product l1 
   ON l1.id = l1_1.prod_product_ori_order_request_item_1prod_product_ida AND l1.deleted = 0 
   LEFT JOIN ori_order_request_item_cstm ori_order_request_item_cstm 
   ON ori_order_request_item.id = ori_order_request_item_cstm.id_c 
   LEFT JOIN accounts accounts1 
   ON accounts1.id = l1.account_id1_c AND IFNULL(accounts1.deleted, 0) = 0 
   LEFT JOIN users ON users.id = ori_order_request_item_cstm.user_id_c
   WHERE (((((( l1.account_id1_c = '8ac9ba12-da67-11eb-8c87-02fb813964b8') ) 
   OR (ori_order_request_item.vendor_name = 'Hill's Pet'))) 
   AND (((ori_order_request_item.status = 'Requested' )))))
   AND ori_order_request_item.deleted = 0";
   
   echo $query.'<br/>';
   
   $queryResult = $db->query($query);
   echo '<pre>'; print_r($queryResult);
		 
	$srno = 1;
	
	$emailBody = "<h2>List of ORI sidecar records based on vendor name from PO, Status = Requested : </h2><br/><br/><br/><table width='1450px' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>			
			<th bgcolor='#b3d1ff' align='left'> Name </th>
			<th bgcolor='#b3d1ff' align='left'> department_new_c </th>
			<th bgcolor='#b3d1ff' align='left'>prod_product_ori_order_request_item_1_name</th>
			<th bgcolor='#b3d1ff' align='left'>product_name</th>
			<th bgcolor='#b3d1ff' align='left'>product_id_2</th>
			<th bgcolor='#b3d1ff' align='left'>user_id_c</th>			
			<th bgcolor='#b3d1ff' align='left'>requestedBy</th>";
			
			
    while ($fetchResult = $db->fetchByAssoc($queryResult)) {

		if ($row['product_id_2'] != "") {
			$productId = $row['product_id_2'];
		  } else {
			$productId = $row['prod_product_ori_order_request_item_1_product_id_2'];
		  }

       $name	 									= $fetchResult['name'];
	   $department_new_c	 						= $fetchResult['department_new_c'];
	   $prod_product_ori_order_request_item_1_name  = $fetchResult['prod_product_ori_order_request_item_1_name'];
	   $product_name	 							= $fetchResult['product_name'];
	   $product_id_2	 							= $productId;
	   $user_id_c	 								= $fetchResult['user_id_c']; 
	   $requestedBy	 								= $fetchResult['requestedBy'];
	  
	   if($srno%2==0)
		$stylr = "style='background-color: #e6e6e6;'";
	  else
		$stylr = "style='background-color: #f3f3f3;'";
				
		$emailBody .= "<tr><td ".$stylr.">".$srno++."</td>						
						<td ".$stylr.">".$name."</td> 					
						<td ".$stylr.">".$department_new_c."</td>
						<td ".$stylr.">".$prod_product_ori_order_request_item_1_name."</td>
						<td ".$stylr.">".$product_name."</td>
						<td ".$stylr.">".$product_id_2."</td>
						<td ".$stylr.">".$user_id_c."</td>												
						<td ".$stylr.">".$requestedBy."</td>
						</tr>";
			
	 }
	 
	 echo $emailBody .="</table>";
	
?>