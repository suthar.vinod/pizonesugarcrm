<?php 
global $db;
	$entered_date1 = '2022-01-01 00:00:00';
	echo "<br>===>".$query = "SELECT * FROM `m01_sales_audit` WHERE date_created>'".$entered_date1."' and `field_name`= 'sales_activity_quote_req_c' ORDER BY date_created desc";
	$queryResult = $db->query($query);
	$srno = 1;
	$site_url		= $GLOBALS['sugar_config']['site_url'];
	$emailBody = "<table width='1450px' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th width='100px' bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th width='150px' bgcolor='#b3d1ff' align='left'> Parent ID </th>
			<th width='150px' bgcolor='#b3d1ff' align='left'>Field Name</th>
			<th width='100px' bgcolor='#b3d1ff' align='left'>Data Type</th>
			<th width='200px' bgcolor='#b3d1ff' align='left'>Date Created</th>
			<th width='200px' bgcolor='#b3d1ff' align='left'>Changed by User</th>
			<th width='200px' bgcolor='#b3d1ff' align='left'>Before Value</th>
			<th width='200px' bgcolor='#b3d1ff' align='left'>After Value </th></tr>";
			
			
    while ($fetchResult = $db->fetchByAssoc($queryResult)) {
       $id	 = $fetchResult['id'];
	   $parent_id	 		= $fetchResult['parent_id'];
	   $event_id	 		= $fetchResult['event_id']; 
	   $date_created	 	= $fetchResult['date_created'];
	   $created_by	 		= $fetchResult['created_by']; 
	   $date_updated	 	= $fetchResult['date_updated'];
	   $field_name	 		= $fetchResult['field_name']; 
	   $data_type	 		= $fetchResult['data_type'];
	   $before_value_string	 = $fetchResult['before_value_string']; 
	   $after_value_string	 = $fetchResult['after_value_string'];
	   $before_value_text	 = $fetchResult['before_value_text']; 
	   $after_value_text	 = $fetchResult['after_value_text'];
	   
	   if($srno%2==0)
		$stylr = "style='background-color: #e6e6e6;'";
	  else
		$stylr = "style='background-color: #f3f3f3;'";
	
	  $saleBean  = BeanFactory::retrieveBean('M01_Sales', $parent_id);
	  
	  $saleLink	= '<a target="_blank" href="'. $site_url . '/#M01_Sales/' . $parent_id . '" >' . $saleBean->name . '</a>';
	    
	  $userName = "";
	 $queryUser = "SELECT * FROM `users` WHERE id>= '".$created_by."' AND deleted=0";
		$resultUser = $db->query($queryUser);
		$fetchUser = $db->fetchByAssoc($resultUser);
		$userName	 =  $fetchUser['user_name']." (".$fetchUser['first_name']." ".$fetchUser['last_name'].")";
	
				
		$emailBody .= "<tr><td ".$stylr.">".$srno++."</td>
						<td ".$stylr."> ".$saleLink."</td> 
						<td ".$stylr.">".$field_name."</td>
						<td ".$stylr.">".$data_type."</td>					
						<td ".$stylr.">".$date_created."</td>
						<td ".$stylr.">".$userName."</td>
						<td ".$stylr.">".$before_value_string."</td>
						<td ".$stylr.">".$after_value_string."</td>				
						</tr>";
			
	 }
	 
	 echo "<br><br>".$emailBody .="</table>";
?>