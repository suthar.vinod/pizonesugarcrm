<?php 

	 global $db;
    $current_date = date("Y-m-d"); //current date
 
       $queryCustom = "SELECT 
                    commu.id AS communicationID, 
                    commu.name AS CommunicationName,  
                    commu_cstm.target_reinspection_date_c, 
					commu_cstm.error_category_c,  					
                    commu_cstm.reinspection_date_c, 
                    commu_wp.m06_error_m03_work_product_1m03_work_product_idb AS workProductID,
                    wp.name  AS workProductName,
					wp_cstm.contact_id_c
            FROM `m06_error` AS commu
                    INNER JOIN m06_error_cstm AS commu_cstm
                    ON  commu.id = commu_cstm.id_c

                    RIGHT JOIN m06_error_m03_work_product_1_c AS commu_wp
                    ON  commu.id = commu_wp.m06_error_m03_work_product_1m06_error_ida
					
					INNER JOIN m03_work_product AS wp
                    ON  commu_wp.m06_error_m03_work_product_1m03_work_product_idb=wp.id 
					
					 INNER JOIN m03_work_product_cstm AS wp_cstm
                    ON  wp.id=wp_cstm.id_c 
		WHERE  
			commu_cstm.error_category_c != 'Internal Feedback' 
			AND commu_cstm.error_category_c != 'Feedback' 
			AND YEAR(commu_cstm.target_reinspection_date_c) = YEAR(CURDATE()) 
			AND commu_cstm.target_reinspection_date_c <'" . $current_date . "'
			AND commu_wp.m06_error_m03_work_product_1m03_work_product_idb IS NOT NULL
			AND commu_wp.deleted = 0
			AND commu.deleted = 0
			AND( 
			
				(  wp_cstm.work_product_compliance_c != 'NonGLP Discovery'
                 AND (commu_cstm.resolution_c IS NULL 
				OR commu_cstm.reinspection_date_c IS NULL 
				OR commu_cstm.error_classification_c IS NULL  
				OR commu_cstm.error_classification_c ='Choose One'
				OR commu_cstm.subtype_c IS NULL)
                )
            OR
               (
                   wp_cstm.work_product_compliance_c = 'NonGLP Discovery'
                   OR  commu_cstm.reinspection_date_c IS NULL 
              )  
			  
			  )
			
		ORDER BY commu_cstm.target_reinspection_date_c ASC";
		
		$queryCommResult = $db->query($queryCustom);
    $table = "<table border='1' cellpadding='2' cellspacing='2' width='100%'><tr><td>srno</td><td>communicationID</td><td>workProductID</td><td>CommunicationName</td><td>workProductName</td><td>target_reinspection_date_c</td><td>contact_id_c</td></tr>";
	$srno = 0;
    while ($fetchCommResult = $db->fetchByAssoc($queryCommResult)) {

        $workProductID = $fetchCommResult['workProductID'];
        $communicationID = $fetchCommResult['communicationID'];
        $communicationName = $fetchCommResult['CommunicationName'];
        $workProductName = $fetchCommResult['workProductName'];
		$assessmentDate  = $fetchCommResult['target_reinspection_date_c'];
		$SD_ID = $fetchCommResult['contact_id_c'];
		$srno++;
		 $table .="<tr><td>".$srno."</td><td>".$communicationID."</td><td>".$workProductID."</td><td>".$communicationName."</td><td>".$workProductName."</td><td>".$assessmentDate."</td><td>".$SD_ID."</td></tr>";
		
	}	
	
	 $table .="</table>";
	 echo $table;
    
	 
	

?>