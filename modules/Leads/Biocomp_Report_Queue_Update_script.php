<?php
global $db;
$site_url = $GLOBALS['sugar_config']['site_url'];
$date = $_GET['date'];
$reportSQL = "SELECT WP.id,WP.name,CWP.contacts_m03_work_product_2contacts_ida,CWP.date_modified FROM  m03_work_product AS WP
LEFT JOIN `contacts_m03_work_product_2_c` AS CWP ON WP.id = CWP.contacts_m03_work_product_2m03_work_product_idb WHERE CWP.date_modified>'" . $date . "'";

$reportResult	= $db->query($reportSQL);
$srno = 1;

$tableBody = "<table width='1050px' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
				<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
				<th bgcolor='#b3d1ff' align='left'> Study ID </th>
				<th bgcolor='#b3d1ff' align='left'> Lead Auditor </th>	
				<th bgcolor='#b3d1ff' align='left'> Date Modified </th>			
			</tr>";
if ($reportResult->num_rows > 0) {
	while ($fetchReportResult = $db->fetchByAssoc($reportResult)) {
		$wpID				= $fetchReportResult['id'];
		$wpName				= $fetchReportResult['name'];
		$ContactID			= $fetchReportResult['contacts_m03_work_product_2contacts_ida'];
		$date_modified		= $fetchReportResult['date_modified'];
		
		$lead_bean	 = BeanFactory::getBean('Contacts', $ContactID);
		$leadaudiName	= $lead_bean->name;

		$wpLink		= '<a target="_blank" href="' . $site_url . '/#M03_Work_Product/' . $wpID . '" >' . $wpName . '</a>';
		$sdLink		= '<a target="_blank" href="' . $site_url . '/#Contacts/' . $ContactID . '" >' . $leadaudiName . '</a>';

		if ($srno % 2 == 0)
			$stylr = "style='background-color: #e6e6e6;'";
		else
			$stylr = "style='background-color: #f3f3f3;'";


		$tableBody .= "<tr><td " . $stylr . ">" . $srno++ . "</td>
									<td " . $stylr . ">" . $wpLink . "</td> 							
									<td " . $stylr . ">" . $sdLink . "</td> 
									<td " . $stylr . ">" . $date_modified . "</td>												
									</tr>";
	}
}
echo "<br><br>" . $tableBody .= "</table>";
