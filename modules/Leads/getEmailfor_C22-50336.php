<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');


$user_email                = array(); //Selected Responsible Personal and related Team users emails
$submitterID = 'c23e7dfc-4e85-11eb-99ea-06e41dba421a';

//Getting submitter email
$submitter_email = getRecordEmail("bean_id = '{$submitterID}'");
$submitter = BeanFactory::getBean('Contacts', $submitterID);
$template_data['email'] = $submitter_email[0];
$submitter_emailid = $submitter_email[0];
 

 

// Geting Manages based of Submitter //

if ($submitter->contact_id_c != "") {
    $submitter_manager_email    = "";
    $submitter_manager_id        = $submitter->contact_id_c;
    $submitter_manager_email    = getRecordEmail("bean_id = '{$submitter_manager_id}'");
    $submitter_manager_bean        = BeanFactory::getBean('Contacts', $submitter_manager_id);
    //$GLOBALS['log']->fatal('submitter_manager_email ==>' . print_r($submitter_manager_email, 1));
    if (verifyContactForNotification($submitter_manager_bean->account_name, $submitter_manager_email[0])) {
        array_push($user_email, $submitter_manager_email[0]);
    }
}
//Getting One Up Manager of Submitter //
if ($submitter->contact_id1_c != "") {
    $submitter_oneupmanager_email = "";
    $submitter_oneupmanager_id = $submitter->contact_id1_c;
    $submitter_oneupmanager_email = getRecordEmail("bean_id = '{$submitter_oneupmanager_id}'");
    $submitter_oneupmanager_bean = BeanFactory::getBean('Contacts', $submitter_oneupmanager_id);
    //$GLOBALS['log']->fatal('submitter_oneupmanager_email ==>'.print_r($submitter_oneupmanager_email,1));

    if (verifyContactForNotification($submitter_oneupmanager_bean->account_name, $submitter_oneupmanager_email[0])) {
        array_push($user_email, $submitter_oneupmanager_email[0]);
    }
}


//Preparing list of selected work product , their names and related study directors

$k = 0;
$workProductID  = '39a2921a-fd6d-11ec-80ce-0221985176a7'; // DHA834-ST25a
if ($workProductID != "") {

    $product = BeanFactory::getBean('M03_Work_Product', $workProductID);

    if ($product->functional_area_c != "") {
        $functionalArea = $product->functional_area_c;

        $functionalUserQuery = "select c.id as functionalUserID from contacts c join contacts_cstm cc on cc.id_c = c.id where cc.functional_area_owner_c LIKE '%" . $functionalArea . "%' AND c.deleted=0";
        $functionalUserResult = $GLOBALS['db']->query($functionalUserQuery);

        if ($functionalUserResult->num_rows > 0) {
            while ($rowFunctionalUser = $GLOBALS['db']->fetchByAssoc($functionalUserResult)) {

                $functionalUserID        = $rowFunctionalUser['functionalUserID'];
                $cnt_bean                = BeanFactory::getBean('Contacts', $functionalUserID);
                $account_name            = $cnt_bean->account_name;
                $primaryEmailAddress     = $cnt_bean->emailAddress->getPrimaryAddress($cnt_bean);
                if ($primaryEmailAddress != false && verifyContactForNotification($account_name, $primaryEmailAddress)) {
                    array_push($user_email, $primaryEmailAddress);
                }
            }
        }
    }
    $study_director_id         = $product->contact_id_c;
    $study_coordinator_id       = $product->contact_id2_c;

    $stdy_bean = BeanFactory::getBean('Contacts', $study_director_id);
    $account_name = $stdy_bean->account_name;
    $primaryEmailAddress = $stdy_bean->emailAddress->getPrimaryAddress($stdy_bean);
    if ($primaryEmailAddress != false && verifyContactForNotification($account_name, $primaryEmailAddress)) {

        //$GLOBALS['log']->fatal('Study Director Email : ' . $primaryEmailAddress);
        array_push($user_email, $primaryEmailAddress);
    }

    $stdyCrd_bean = BeanFactory::getBean('Contacts', $study_coordinator_id);
    $account_name = $stdyCrd_bean->account_name;
    $primaryEmailAddress = $stdyCrd_bean->emailAddress->getPrimaryAddress($stdyCrd_bean);
    if ($primaryEmailAddress != false && verifyContactForNotification($account_name, $primaryEmailAddress)) {
        array_push($user_email, $primaryEmailAddress);
    }
}




//Preparing template data array to populate variables in APS Deviation email template              			 

$commID            = 'a779e020-5bbb-11ed-ab18-065a04235e91';
echo "Communication ID == ".$commName        = 'C22-50336';

echo "<br>";

/*End logic for Ticket #495, #594*/



$company = 'american preclinical services';


$selected_teams = array("Work Product Schedule Outcome","Deceased Animal Group",'Operations Support');

foreach($selected_teams as $team_name){
	$con_email_addresses = get_related_contacts_email_addresses($team_name);
	if (!empty($con_email_addresses)) {
		foreach ($con_email_addresses as $email) {
			if (verifyContactForNotification($company, $email)) {
				array_push($user_email, $email);
			}
		}
	}

	$email_addresses = get_related_teams($team_name);
	if (!empty($email_addresses)) {
		foreach ($email_addresses as $email) {
			if (verifyContactForNotification($company, $email)) {
				array_push($user_email, $email);
			}
		}
	}
	
}



$user_email = array_unique($user_email);

echo "<pre>"; $sr=0;
foreach($user_email as $emailid){
	echo "<br>".$sr++." ".$emailid;
}
echo "<br>".$sr++." ".$submitter_emailid; 




/**
 * Retrieves email address form bean id from email related bean table
 * @param string $record
 * @return string email_address|null
 */
function getRecordEmail($where = '')
{
    $email_addresses = array();
    $query = "SELECT email_addresses.email_address
                    FROM `email_addresses`
                    INNER JOIN
                        `email_addr_bean_rel` ON email_addr_bean_rel.email_address_id = email_addresses.id
                    WHERE email_addr_bean_rel.deleted = 0 AND email_addresses.deleted='0' AND email_addresses.invalid_email='0' AND email_addresses.opt_out='0'
                    AND email_addr_bean_rel." . $where;
    $res = $GLOBALS['db']->query($query);
    if ($res->num_rows >= 1) {
        while ($row = $GLOBALS["db"]->fetchByAssoc($res)) {
            $email_address[] = $row['email_address'];
        }
        return $email_address;
    }
   
    return '';
}

/**
 * Verify if contact account name is 'american preclinical services'
 * and email address domain is 'apsemail.com' otherwise return false
 * @param string $account_name
 * @param string $email
 * @return bool true|false
 */
function verifyContactForNotification($account_name, $email)
{
    $companyToSearch = array('american preclinical services', 'NAMSA');
    $domainToSearch = array('apsemail.com', 'namsa.com');
    if (in_array(substr($email, strrpos($email, '@') + 1), $domainToSearch) && in_array($account_name, $companyToSearch) || in_array(strtolower($account_name), $companyToSearch)) {
        return true;
    }
    return false;
}



/**
 * Members which are associated with team, return there email addresses
 * @param type $team_name
 * @return type array
 */
function get_related_teams($team_name)
{
    $email_addresses = array();

    $get_team_members = "SELECT email_addresses.email_address FROM teams "
        . "INNER JOIN team_memberships ON teams.id = team_memberships.team_id "
        . "INNER JOIN users ON team_memberships.user_id = users.id "
        . "INNER JOIN email_addr_bean_rel ON users.id = email_addr_bean_rel.bean_id "
        . "INNER JOIN email_addresses ON email_addr_bean_rel.email_address_id = email_addresses.id "
        . "WHERE teams.name = '" . $team_name . "' AND teams.deleted = 0 AND team_memberships.deleted = 0 AND users.deleted = 0 ";

    $team_members = $GLOBALS['db']->query($get_team_members);
    while ($team_member_address = $GLOBALS["db"]->fetchByAssoc($team_members)) {
        if (!empty($team_member_address['email_address'])) {
            $email_addresses[] = $team_member_address['email_address'];
        }
    }

    return $email_addresses;
}

/**
 * Contacts which are associated with team, return there email addresses
 * @param type $team_name
 * @return type array
 */
function get_related_contacts_email_addresses($team_name)
{

    $contacts_email_addreses = array();

    $query = new SugarQuery();
    $query->from(BeanFactory::newBean('Teams'), array('alias' => 'teams'));
    $query->joinTable('team_sets_teams', array(
        'joinType' => 'INNER',
        'alias' => 'tst',
        'linkingTable' => true,
    ))->on()->equalsField('teams.id', 'tst.team_id');
    $query->joinTable('contacts', array(
        'joinType' => 'INNER',
        'alias' => 'c',
        'linkingTable' => true,
    ))->on()->equalsField('tst.team_set_id', 'c.team_set_id');
    $query->joinTable('email_addr_bean_rel', array(
        'joinType' => 'INNER',
        'alias' => 'eabr',
        'linkingTable' => true,
    ))->on()->equalsField('c.id', 'eabr.bean_id');
    $query->joinTable('email_addresses', array(
        'joinType' => 'INNER',
        'alias' => 'ea',
        'linkingTable' => true,
    ))->on()->equalsField('eabr.email_address_id', 'ea.id');

    $query->where()
        ->equals('teams.name', $team_name)
        ->equals('teams.deleted', 0)
        ->equals('tst.deleted', 0)
        ->equals('c.deleted', 0)
        ->equals('eabr.deleted', 0)
		->equals('eabr.primary_address', 1);
    $query->select('ea.email_address');
    $compile_query = $query->compile();
    $result_query = $query->execute();

    foreach ($result_query as $key => $innerArray) {
        foreach ($innerArray as $key => $email_address) {
            $contacts_email_addreses[] = $email_address;
        }
    }

    return $contacts_email_addreses;
}