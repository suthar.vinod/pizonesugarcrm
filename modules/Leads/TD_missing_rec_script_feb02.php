<?php 
global $db;
$startDate ="2022-01-16";
$endDate = "2022-02-02";
   $query = "SELECT * FROM `taskd_task_design` where deleted=1 and DATE(date_modified)>='".$startDate."' AND DATE(date_modified)<'".$endDate."'  ORDER BY date_modified desc";
	$queryResult = $db->query($query);
		 
	$srno = 1;
	
	$emailBody = "<h2>List of deleted TD records : </h2><br/><br/><br/><table width='1450px' cellspacing='5' cellpadding='4' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 18px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th bgcolor='#b3d1ff' align='left'> Name </th>
			<th bgcolor='#b3d1ff' align='left'>Date Created</th>
			<th bgcolor='#b3d1ff' align='left'>Date Modified</th>
			<th bgcolor='#b3d1ff' align='left'>Modified By</th>
			<th bgcolor='#b3d1ff' align='left'>Created By</th>";
			
			
    while ($fetchResult = $db->fetchByAssoc($queryResult)) {
       $id	 = $fetchResult['id'];
	   $name	 				= $fetchResult['name'];
	   $date_entered	 		= $fetchResult['date_entered']; 
	   $date_modified	 		= $fetchResult['date_modified'];
	   $modified_user_id	 	= $fetchResult['modified_user_id']; 
	   $created_by	 			= $fetchResult['created_by'];

	  /*  if($date_entered!=""){
        $date_entered = date("Y-m-d",strtotime($date_entered));
        $offset1 =  -21600; //daylight Saving
        $offset_time = strtotime($date_entered) + $offset1;
        $date_entered = date("Y-m-d H:i:s",$offset_time);                
		}

		if($date_modified!=""){
			$date_modified = date("Y-m-d",strtotime($date_modified));
			$offset1 =  -21600; //daylight Saving
			$offset_time = strtotime($date_modified) + $offset1;
			$date_modified = date("Y-m-d H:i:s",$offset_time);                
			} */
		$date_entered = date_convert($date_entered, 'UTC', 'CST', 'Y-m-d H:i:s');
		$date_modified = date_convert($date_modified, 'UTC', 'CST', 'Y-m-d H:i:s');
	   
	   $sqlUser = "SELECT CONCAT(first_name,' ',last_name) AS userName FROM users WHERE id='".$modified_user_id."'";
	   $queryUserResult = $db->query($sqlUser);
	   while ($fetchUser = $db->fetchByAssoc($queryUserResult)) {
			$modifiedUser	 = $fetchUser['userName']; 
		}
		$sqlUser1 = "SELECT CONCAT(first_name,' ',last_name) AS userName FROM users WHERE id='".$created_by."'";
	   $queryUserResult1 = $db->query($sqlUser1);
	   while ($fetchUser1 = $db->fetchByAssoc($queryUserResult1)) {
			$created_byUser	 = $fetchUser1['userName']; 
		}
	   
	   if($srno%2==0)
		$stylr = "style='background-color: #e6e6e6;'";
	  else
		$stylr = "style='background-color: #f3f3f3;'";
				
		$emailBody .= "<tr><td ".$stylr.">".$srno++."</td>
						<td ".$stylr."><a target='_blank' href='https://aps.sugarondemand.com/#TaskD_Task_Design/$id'>".$name."</td> 					
						<td ".$stylr.">".$date_entered."</td>						
						<td ".$stylr.">".$date_modified."</td>
						<td ".$stylr.">".$modifiedUser."</td>
						<td ".$stylr.">".$created_byUser."</td>						
						</tr>";
			
	 }
	 
	 echo $emailBody .="</table>";
	 function date_convert($time, $oldTZ, $newTZ, $format) {
		// create old time
		$d = new \DateTime($time, new \DateTimeZone($oldTZ));
		// convert to new tz
		$d->setTimezone(new \DateTimeZone($newTZ));
	
		// output with new format
		return $d->format($format);
	}
?>