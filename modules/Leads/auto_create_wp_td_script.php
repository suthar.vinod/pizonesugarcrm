<?php 
global $db;
$site_url = $GLOBALS['sugar_config']['site_url'];
    $query = "SELECT IFNULL(m03_work_product.id,'') wpid
	,IFNULL(m03_work_product.name,'') m03_work_product_name,m03_work_product_code1.id AS wpcid,m03_work_product_code1.name AS wpc_name
	,IFNULL(m03_work_product_cstm.work_product_status_c,'') work_product_status_c,m03_work_product_cstm.functional_area_c AS functional_area_c,m03_work_product_cstm.bc_spa_reconciled_date_c bc_spa_reconciled_date_c,m03_work_product.date_entered - INTERVAL 300 MINUTE date_entered
FROM m03_work_product
LEFT JOIN  m03_work_product_taskd_task_design_1_c l1_1 ON m03_work_product.id=l1_1.m03_work_product_taskd_task_design_1m03_work_product_ida AND l1_1.deleted=0

LEFT JOIN  taskd_task_design l1 ON l1.id=l1_1.m03_work_product_taskd_task_design_1taskd_task_design_idb AND l1.deleted=0
LEFT JOIN m03_work_product_cstm m03_work_product_cstm ON m03_work_product.id = m03_work_product_cstm.id_c
LEFT JOIN m03_work_product_code m03_work_product_code1 ON m03_work_product_code1.id = m03_work_product_cstm.m03_work_product_code_id1_c AND IFNULL(m03_work_product_code1.deleted,0)=0 

 WHERE (((m03_work_product_cstm.functional_area_c = 'Standard Biocompatibility'
) AND ((coalesce(LENGTH(m03_work_product_cstm.bc_spa_reconciled_date_c), 0) <> 0)) AND ((coalesce(LENGTH(m03_work_product_cstm.m03_work_product_code_id1_c), 0) <> 0)) AND ((coalesce(LENGTH(l1.id), 0) = 0)))) 
AND  m03_work_product.deleted=0 
 AND IFNULL(m03_work_product_code1.deleted,0)=0";
	$queryResult = $db->query($query);
		 
	$srno = 1;
	
	$WpBody = "<h2>List of WP records of Auto-create TDs in one time event : </h2><br/><table width='1250px' cellspacing='5' cellpadding='6' border='1' style='font-family: Tahoma;font-size: 14px;line-height: 20px;'>
			<tr><th bgcolor='#b3d1ff' align='left'>Sr. No.</th>
			<th bgcolor='#b3d1ff' align='left'>Work Product </th>
			<th bgcolor='#b3d1ff' align='left'>Functional Area</th>
			<th bgcolor='#b3d1ff' align='left'>Work Product Phase</th>
			<th bgcolor='#b3d1ff' align='left'>Work Product Code</th>
			<th bgcolor='#b3d1ff' align='left'>SPA Reconciled Date</th>
			<th bgcolor='#b3d1ff' align='left'># Of TD Linked to WP</th>
			<th bgcolor='#b3d1ff' align='left'># Of TD Linked to WPC</th>
			</tr>";
			
			
    while ($fetchResult = $db->fetchByAssoc($queryResult)) {
       $wpid    = $fetchResult['wpid'];
	   /**Get the task design records linked to fetched work producut */
	   $tsddataQuery = "SELECT count(m03_work_product_taskd_task_design_1taskd_task_design_idb) as CountTD FROM m03_work_product_taskd_task_design_1_c
	   where m03_work_product_taskd_task_design_1m03_work_product_ida ='".$wpid."' and deleted=0";
	   $restsddataQuery = $db->query($tsddataQuery);
	   $rowtsddataQuery = $db->fetchByAssoc($restsddataQuery);
	   $CountTD = $rowtsddataQuery['CountTD'];
	   $m03_work_product_name	 		= $fetchResult['m03_work_product_name']; 
	   $wpcid	 						= $fetchResult['wpcid'];
	   /**Get the task design linked to work product code */
	   $wpctsQuery = "SELECT count(m03_work_product_code_taskd_task_design_1taskd_task_design_idb) AS CountWPCTD FROM m03_work_product_code_taskd_task_design_1_c
	   where m03_work_p7fc0ct_code_ida='".$wpcid."' and deleted=0";
	   $reswpctdQuery = $db->query($wpctsQuery);
	   $rowwpctdQuery = $db->fetchByAssoc($reswpctdQuery);
	   $CountWPCTD = $rowwpctdQuery['CountWPCTD'];
	   /************************************************ */
	   $wpc_name	 					= $fetchResult['wpc_name'];
	   $work_product_status_c	 		= $fetchResult['work_product_status_c']; 
	   $functional_area_c	 				= $fetchResult['functional_area_c'];
	   $bc_spa_reconciled_date_c	 	= $fetchResult['bc_spa_reconciled_date_c'];
	   $date_entered	 				= $fetchResult['date_entered'];
	   
	   if($srno%2==0)
		$stylr = "style='background-color: #e6e6e6;'";
	  else
		$stylr = "style='background-color: #f3f3f3;'";
		if($CountTD < 1)
		{
			$WpBody .= "<tr><td ".$stylr.">".$srno++."</td>
						<td ".$stylr."><a target='_blank' href='" . $site_url . "/#M03_Work_Product/" . $wpid. "'>".$m03_work_product_name."</td>
						<td ".$stylr.">".$work_product_status_c."</td>
						<td ".$stylr.">".$functional_area_c."</td>
						<td ".$stylr."><a target='_blank'  href='" . $site_url . "/#M03_Work_Product_Code/" . $wpcid. "'>".$wpc_name."</td>
						<td ".$stylr.">".$bc_spa_reconciled_date_c."</td>
						<td ".$stylr.">".$CountTD."</td>
						<td ".$stylr.">".$CountWPCTD."</td>
						</tr>";
		}	
		
			
	 }
	 
	 echo "<br><br>".$WpBody .="</table>";
