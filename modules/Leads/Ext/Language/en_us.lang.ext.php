<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_DESCRIPTION'] = 'Company Description';
$mod_strings['LBL_ACCOUNT_ID'] = 'Company ID';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Company Name:';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Company Name';
$mod_strings['LBL_CONVERTED_ACCOUNT'] = 'Converted Company:';
$mod_strings['LNK_SELECT_ACCOUNTS'] = ' OR Select Company';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Create Company';
$mod_strings['LBL_OPPORTUNITY_AMOUNT'] = 'Opportunity Amount:';
$mod_strings['LBL_OPPORTUNITY_ID'] = 'Opportunity ID';
$mod_strings['LBL_OPPORTUNITY_NAME'] = 'Opportunity Name:';
$mod_strings['LBL_CONVERTED_OPP'] = 'Converted Opportunity:';

?>
