<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class Get_Latest_usda_record {

    public function usda_record($bean, $event, $arguments) {
        global $db, $current_user;
        $module = $arguments['module'];
        $modelId = $arguments['id'];

        $relatedModule = $arguments['related_module'];
        $relatedId = $arguments['related_id']; 
		 
		//$usda_id = $this->retrieveMostRecentUSDA_ID($ANML_Animals);
        if ($module === 'ANML_Animals' && $relatedModule === 'USDA_Historical_USDA_ID') {        
            global $timedate;
            if ($arguments['related_module'] == 'USDA_Historical_USDA_ID' && $arguments['link'] == 'anml_animals_usda_historical_usda_id_1' ) {
                $relatedBean = BeanFactory::getBean("USDA_Historical_USDA_ID", $arguments['related_id']);
                $convertedDate = $timedate->fromString($relatedBean->date_entered);
                $newDbDate = strtotime($convertedDate->format("Y-m-d H:i:s"));    
                $conn = $GLOBALS['db']->getConnection();
                $sql = "SELECT usda.name,usda.date_entered FROM usda_historical_usda_id usda 
                INNER JOIN anml_animals_usda_historical_usda_id_1_c au ON usda.id = au.anml_anima2e7fusda_id_idb AND au.deleted = 0 AND au.anml_animals_usda_historical_usda_id_1anml_animals_ida = '{$bean->id}' ORDER BY date_entered DESC LIMIT 1";
                $rslt = $conn->executeQuery($sql);
                $row = $rslt->fetch();
                if (($newDbDate >= strtotime($row['date_entered'])) && $event == "after_relationship_add") {
                    $usda_name = $relatedBean->name;
                }else if ( $row['name'] == "" && $event == "after_relationship_delete"){
					$sql2 = "SELECT usda_id_hidden_c FROM anml_animals_cstm WHERE id_c = '{$bean->id}' ";
					$rslt2 = $conn->executeQuery($sql2);
					$row2 = $rslt2->fetch();
                    $usda_name = $row2['usda_id_hidden_c'];
				}else{
                    $usda_name = $row['name'];
                }
                $bean->usda_id_c = $usda_name;
                
                $conn->executeQuery("UPDATE anml_animals_cstm SET usda_id_c = '{$usda_name}' WHERE id_c = '{$bean->id}'");
				$source = '{"subject":{"_type":"logic-hook","class":"Get_Latest_usda_record","method":"usda_record"},"attributes":[]}';
				
				$startbeansql = "SELECT after_value_string FROM anml_animals_audit WHERE field_name = 'usda_id_c' and parent_id = '{$bean->id}' ORDER BY date_updated DESC LIMIT 1";				
				$beanrslt = $conn->executeQuery($startbeansql);
				$beanrow = $beanrslt->fetch();
				$startbean = $beanrow['after_value_string'];
				
                if($bean->com_usda_id == ""){
                    $auditEventid = create_guid();
                    $auditsql = 'INSERT INTO anml_animals_audit  (id,parent_id,event_id,date_created,created_by,date_updated,field_name,data_type,before_value_string,after_value_string) 
                        values("' . create_guid() . '","' . $bean->id . '","' . $auditEventid . '",now(),"' . $current_user->id . '",now(),"usda_id_c","varchar","' . $startbean . '","' . $usda_name . '")';
                    $auditsqlResult = $db->query($auditsql);

                    $auditsqlStatus = "INSERT INTO audit_events (id,type,parent_id,module_name,source) values ('" . $auditEventid . "','update', '" . $bean->id . "','ANML_Animals','" . $source . "')";
                    $db->query($auditsqlStatus);
                }
                
				$sql_DeleteAudit = "DELETE from `anml_animals_audit` WHERE `field_name`='usda_id_c' AND `parent_id`='{$bean->id}' and before_value_string=after_value_string";
				$db->query($sql_DeleteAudit);
            }
        }
    }  
}
 