<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class update_wpa_record
{

    public function usda_WPA($bean, $event, $arguments)
    {
        global $db, $current_user;
        $beanId    = $bean->id;
        $usdaID    = $bean->usda_id_c; 
        if($bean->usda_id_c!=$bean->fetched_row['usda_id_c']){

            $sql = "SELECT * FROM `anml_animals_wpe_work_product_enrollment_1_c`
            WHERE anml_animals_wpe_work_product_enrollment_1anml_animals_ida = '" . $beanId . "'";
    
            $resultRecord = $db->query($sql);
    
            if ($resultRecord->num_rows > 0) {            
    
                while ($rowRecord = $db->fetchByAssoc($resultRecord)) {
                    $wpaID     = $rowRecord['anml_anima9941ollment_idb'];
                    $current_date = date("Y-m-d h:i:s");                    
                    $wpaBean = BeanFactory::retrieveBean('WPE_Work_Product_Enrollment',$wpaID);                    
                    $wpaBean->usda_id_c = $usdaID;
                    $wpaBean->save();                    
                } 
            }

        } 
    }
}
