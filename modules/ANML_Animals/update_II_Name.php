<?php
class updateIINameOnUpdateUSDAId
{
	//static $already_ran = false;  // To make sure LogicHooks will execute just onces
	function updateIIName($bean, $event, $arguments)
	{
		/* if (self::$already_ran == true)
			return;   //So that hook will only trigger once
		self::$already_ran = true; */
		//$bean = ANML_Animals;			

		$arrPullTSId = array("", "Bovine", "Canine", "Caprine", "Ovine", "Porcine");
		$arrPullUSDAId = array("Lagomorph", "Rat", "Mouse", "Hamster", "Guinea Pig");

		$speciesId = $bean->s_species_id_c;
		$speciesBean = BeanFactory::retrieveBean("S_Species", $speciesId);
		$species = $speciesBean->name;

		$bean->load_relationship("anml_animals_ii_inventory_item_1");
		$II_Ids = $bean->anml_animals_ii_inventory_item_1->get();

		$nameAppend = "";
		if ($bean->usda_id_c != "") {
			
			if ($bean->usda_id_c != $bean->fetched_row['usda_id_c']) {
				
				foreach ($II_Ids as $IIIDS) {
					$IIBean = BeanFactory::retrieveBean("II_Inventory_Item", $IIIDS);
					$nameAppend = "";
					$clonedBeanArr = explode(" ", $IIBean->name);
					if (in_array($species, $arrPullTSId)) {
						$nameAppend = $bean->name;
						$clonedBeanArr[1] = $nameAppend;
					} else if (in_array($species, $arrPullUSDAId)) {
						if ($bean->usda_id_c != "") {
							$nameAppend = $bean->usda_id_c;
							$NameString = strpos($IIBean->name, $bean->name);
							if ($NameString !== false) {
								$II_newName = str_replace($bean->name, $nameAppend, $IIBean->name);
								$IIBean->name = $II_newName;
							} else {
								$II_newName = str_replace($bean->fetched_row['usda_id_c'], $nameAppend, $IIBean->name);
								$IIBean->name = $II_newName;
							}
						} else {
							$nameAppend = $bean->name;
						}
						$clonedBeanArr[1] = $nameAppend;
					} else {
						$nameAppend = $bean->name;
						$clonedBeanArr[1] = $nameAppend;
					}
					$IIBean->name = implode(" ", $clonedBeanArr);
					$IIBean->save();
				}
			}
		} else {
			
			foreach ($II_Ids as $IIIDS) {
				$IIBean = BeanFactory::retrieveBean("II_Inventory_Item", $IIIDS);
				if ($bean->fetched_row['usda_id_c'] != "") {
					$II_newName = str_replace($bean->fetched_row['usda_id_c'], $bean->name, $IIBean->name);
				} else {
					$NameString = strpos($IIBean->name, $bean->name);
					if ($NameString !== false) {
						$II_newName = str_replace($bean->name, $bean->name, $IIBean->name);
					} else {
						$nameVar = explode(" ", $IIBean->name);
						$nameVar[1] = $bean->name;
						$II_newName = implode(' ', $nameVar);
					}
				}
				$IIBean->name = $II_newName;
				$IIBean->save();
			}
		}
	}
}
