<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class add_usda_id_hidden {
    public function add_usda_id($bean, $event, $arguments) {		
        $usda_id = $bean->usda_id_c;
        $bean->usda_id_hidden_c = $usda_id;
    }  
}
 