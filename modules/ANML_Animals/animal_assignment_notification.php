<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once("modules/Emails/Email.php");
require_once('modules/EmailTemplates/EmailTemplate.php');
require_once('include/SugarPHPMailer.php');
class WPE_animalNotificationAfterRelationAddHook{
	function SendAnimalAssignmentNotification($bean, $event, $arguments){
		global $db;
		 
		if($bean->id!=""&& $bean->m03_work_product_id_c!="" && ($bean->m03_work_product_id_c!=$bean->fetched_row['m03_work_product_id_c'])){
			$animal_id = $bean->id; // Test System ID
			$animal_name = $bean->name; // Test System ID		
			
			$animal_roomid = $bean->rms_room_id_c;
			
			if($animal_roomid=='11633546-7965-11e9-9c7a-029395602a62' || $animal_roomid=='117c10ca-7965-11e9-b77d-029395602a62' || $animal_roomid=='118f9064-7965-11e9-a677-029395602a62' || $animal_roomid=='11a312ec-7965-11e9-b6a3-029395602a62' || $animal_roomid=='11b67a3a-7965-11e9-8bc7-029395602a62' )
			{
					
					//Get  room Name from == > table - rms_room
				$roomBean 	= BeanFactory::getBean('RMS_Room', $animal_roomid);
				$room_name	= $roomBean->name;
				 
				// Get Animal Assignment Notification Template
				$template = new EmailTemplate();
				$template->retrieve_by_string_fields(array('name' => 'Animal Assignment Notification', 'type' => 'email'));
				// Set values in Template
				$site_url 		= $GLOBALS['sugar_config']['site_url'];
				$animal_link	= '<a target="_blank" href="'.$site_url.'/#ANML_Animals/'. $animal_id .'">'.$animal_name.'</a>';
				$room_link 		= '<a target="_blank" href="' . $site_url . '/#RMS_Room/' . $animal_roomid .'">'.$room_name.'</a>';
				
				$template->body_html = str_replace('[ANIMAL_NAME]', $animal_link, $template->body_html);
				$template->body_html = str_replace('[ANIMAL_ROOM]', $room_link, $template->body_html);
				
				
				$emailObj = new Email();
				$defaults = $emailObj->getSystemDefaultEmail();
				$mail = new SugarPHPMailer();
				$mail->setMailerForSystem();
				$mail->IsHTML(true);
				$mail->From = $defaults['email'];
				$mail->FromName = $defaults['name'];
				if(!empty($template->subject))
					$mail->Subject = $template->subject;
				else
					$mail->Subject = "";
			
				if (!empty($template->body_html)) {
					$mail->Body = $template->body_html;
				} else {
					$mail->Body = "";
				}
				
				$mail->AddAddress('nuitermarkt@apsemail.com'); //to be deleted
				//$mail->AddAddress('fxs_mjohnson@apsemail.com'); //to be deleted
				$mail->AddBCC('vsuthar@apsemail.com'); //to be deleted
				if (!$mail->Send()) {
					$GLOBALS['log']->fatal("247: Fail to Send Email, Please check settings");
				} else {
					$GLOBALS['log']->fatal('247: email sent');
				}

			} 
		}
	}
}
?>