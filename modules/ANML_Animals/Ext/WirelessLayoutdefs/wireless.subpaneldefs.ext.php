<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/WirelessLayoutdefs/anml_animals_wpe_work_product_enrollment_1_ANML_Animals.php

 // created: 2017-09-12 15:03:53
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_wpe_work_product_enrollment_1'] = array (
  'order' => 100,
  'module' => 'WPE_Work_Product_Enrollment',
  'subpanel_name' => 'ForAnml_animalsAnml_animals_wpe_work_product_enrollment_1',
  'title_key' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'get_subpanel_data' => 'anml_animals_wpe_work_product_enrollment_1',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/WirelessLayoutdefs/m06_error_anml_animals_1_ANML_Animals.php

 // created: 2018-01-31 16:00:43
$layout_defs["ANML_Animals"]["subpanel_setup"]['m06_error_anml_animals_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_ANML_ANIMALS_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'm06_error_anml_animals_1',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/WirelessLayoutdefs/anml_animals_rt_room_transfer_1_ANML_Animals.php

 // created: 2019-02-20 13:19:54
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_rt_room_transfer_1'] = array (
  'order' => 100,
  'module' => 'RT_Room_Transfer',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_RT_ROOM_TRANSFER_TITLE',
  'get_subpanel_data' => 'anml_animals_rt_room_transfer_1',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/WirelessLayoutdefs/anml_animals_tsdoc_test_system_documents_1_ANML_Animals.php

 // created: 2019-09-30 12:27:41
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_tsdoc_test_system_documents_1'] = array (
  'order' => 100,
  'module' => 'TSdoc_Test_System_Documents',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_TSDOC_TEST_SYSTEM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'anml_animals_tsdoc_test_system_documents_1',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/WirelessLayoutdefs/anml_animals_w_weight_1_ANML_Animals.php

 // created: 2020-01-07 13:24:35
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_w_weight_1'] = array (
  'order' => 100,
  'module' => 'W_Weight',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_W_WEIGHT_TITLE',
  'get_subpanel_data' => 'anml_animals_w_weight_1',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/WirelessLayoutdefs/anml_animals_cie_clinical_issue_exam_1_ANML_Animals.php

 // created: 2020-01-07 13:40:49
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_cie_clinical_issue_exam_1'] = array (
  'order' => 100,
  'module' => 'CIE_Clinical_Issue_Exam',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'get_subpanel_data' => 'anml_animals_cie_clinical_issue_exam_1',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/WirelessLayoutdefs/anml_animals_co_clinical_observation_1_ANML_Animals.php

 // created: 2020-01-07 14:17:22
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_co_clinical_observation_1'] = array (
  'order' => 100,
  'module' => 'CO_Clinical_Observation',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'get_subpanel_data' => 'anml_animals_co_clinical_observation_1',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/WirelessLayoutdefs/anml_animals_usda_historical_usda_id_1_ANML_Animals.php

 // created: 2021-02-23 10:42:04
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_usda_historical_usda_id_1'] = array (
  'order' => 100,
  'module' => 'USDA_Historical_USDA_ID',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_USDA_HISTORICAL_USDA_ID_TITLE',
  'get_subpanel_data' => 'anml_animals_usda_historical_usda_id_1',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/WirelessLayoutdefs/anml_animals_taskd_task_design_1_ANML_Animals.php

 // created: 2021-08-14 06:05:14
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_taskd_task_design_1'] = array (
  'order' => 100,
  'module' => 'TaskD_Task_Design',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'get_subpanel_data' => 'anml_animals_taskd_task_design_1',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/WirelessLayoutdefs/anml_animals_ii_inventory_item_1_ANML_Animals.php

 // created: 2021-11-09 10:02:11
$layout_defs["ANML_Animals"]["subpanel_setup"]['anml_animals_ii_inventory_item_1'] = array (
  'order' => 100,
  'module' => 'II_Inventory_Item',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'get_subpanel_data' => 'anml_animals_ii_inventory_item_1',
);

?>
