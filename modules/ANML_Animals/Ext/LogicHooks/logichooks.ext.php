<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/LogicHooks/autoGenHookRegistration.php


//$hook_version = 1;
//$hook_array['after_relationship_add'][] = array(
//    1001,
//    'Manage Category and status of Hook',
//    'custom/modules/ANML_Animals/LogicHooksImplementations/updateCategoryAndStatusHookAN.php',
//    'HookWPERelManagement',
//    'manageCategoryAndStatus'
//);
//$hook_array['after_relationship_delete'][] = array(
//    1001,
//    'Manage Category and status of Hook',
//    'custom/modules/ANML_Animals/LogicHooksImplementations/updateCategoryAndStatusHookAN.php',
//    'HookWPERelManagement',
//    'manageCategoryAndStatus'
//);
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/LogicHooks/roomLink.php

$hook_version = 1;
if (!isset($hook_array)) {
	$hook_array = array();
}
if (!isset($hook_array['after_relationship_add'])) {
	$hook_array['after_relationship_add'] = array();
}
$hook_array['after_relationship_add'][] = array(
   count($hook_array['after_relationship_add']),
   'Link Room from Related Room Transfer record with latest Transfer Date',
   'custom/modules/ANML_Animals/LogicHooksImplementations/updateRoom.php',
   'updateRoomClass',
   'updateRoom'
);

$hook_version = 1;
if (!isset($hook_array)) {
	$hook_array = array();
}
if (!isset($hook_array['after_relationship_delete'])) {
	$hook_array['after_relationship_delete'] = array();
}
$hook_array['after_relationship_delete'][] = array(
   count($hook_array['after_relationship_delete']),
   'Link Room from Related Room Transfer record with latest Transfer Date',
   'custom/modules/ANML_Animals/LogicHooksImplementations/updateRoom.php',
   'updateRoomClass',
   'updateRoom'
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/LogicHooks/autoNamingForTestSystems.php
  

//$hook_version = 1;
//$hook_array['after_save'][] = array(
//    count($hook_array['before_save']),
//    'Auto Naming sequence for Test System records',
//    'custom/modules/ANML_Animals/TestSystemLogicHook.php',
//    'TestSystemLogicHook',
//    'generateNameForTestSystems'
//);


?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/LogicHooks/logic_hooks.php


$hook_array['before_save'][] = array(
   '1',
   'Set default value to Stock for Enrollment Status',
   'custom/modules/ANML_Animals/LogicHooksImplementations/customAnimalsLogicHook.php',
   'customAnimalsLogicHook',
   'setStatustoStock'
);


?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/LogicHooks/CreateWorkProductEnrollment_hook.php


$hook_array['after_save'][] = array(
   '1',
   'Create Work Product Record on Creation of Test system Record',
   'custom/modules/ANML_Animals/LogicHooksImplementations/TestSystem_WorkProductEnrollmentHook.php',
   'TestSystem_WorkProductEnrollmentHook',
   'createRelatedWorkProductEnrollment'
);


?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/LogicHooks/usda_id_hidden.php


$hook_array['before_save'][] = array(
    '17',
    'Add USDA ID in Hidden Field',
    'custom/modules/ANML_Animals/add_usda_id_hidden.php',
    'add_usda_id_hidden',
    'add_usda_id'
 );
?>
