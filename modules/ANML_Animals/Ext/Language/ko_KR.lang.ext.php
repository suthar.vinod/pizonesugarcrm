<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/ko_KR.customanml_animals_wpe_work_product_enrollment_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/ko_KR.customanml_animals_wpe_work_product_enrollment_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Product Enrollment';
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE'] = 'Work Product Enrollment';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Product Enrollment';
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE'] = 'Work Product Enrollment';


?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/ko_KR.customwpe_work_product_enrollment_anml_animals_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Product Enrollment';
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE'] = 'Work Product Enrollment';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/ko_KR.customm06_error_anml_animals_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_ANML_ANIMALS_1_FROM_M06_ERROR_TITLE'] = 'Errors';
$mod_strings['LBL_M06_ERROR_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE'] = 'Errors';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/ko_KR.customanml_animals_rt_room_transfer_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_RT_ROOM_TRANSFER_TITLE'] = 'Room Transfers';
$mod_strings['LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_ANML_ANIMALS_TITLE'] = 'Room Transfers';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/ko_KR.customanml_animals_tsdoc_test_system_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_TSDOC_TEST_SYSTEM_DOCUMENTS_TITLE'] = 'Test System Documents';
$mod_strings['LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_ANML_ANIMALS_TITLE'] = 'Test System Documents';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/ko_KR.customanml_animals_w_weight_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_W_WEIGHT_TITLE'] = 'Weights';
$mod_strings['LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_ANML_ANIMALS_TITLE'] = 'Weights';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/ko_KR.customanml_animals_cie_clinical_issue_exam_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE'] = 'Clinical Issues & Exams';
$mod_strings['LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_ANML_ANIMALS_TITLE'] = 'Clinical Issues & Exams';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/ko_KR.customanml_animals_co_clinical_observation_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE'] = 'Clinical Observations';
$mod_strings['LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_ANML_ANIMALS_TITLE'] = 'Clinical Observations';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/ko_KR.customanml_animals_usda_historical_usda_id_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_USDA_HISTORICAL_USDA_ID_TITLE'] = 'Historical USDA IDs';
$mod_strings['LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_ANML_ANIMALS_TITLE'] = 'Historical USDA IDs';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/ko_KR.customanml_animals_taskd_task_design_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE'] = 'Task Designs';
$mod_strings['LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_ANML_ANIMALS_TITLE'] = 'Task Designs';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/ko_KR.customanml_animals_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_ANML_ANIMALS_TITLE'] = 'Inventory Items';

?>
