<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.AnimalsModuleCustomizations.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Animal';
$mod_strings['LBL_ABNORMALITY'] = 'Abnormality';
$mod_strings['LBL_ANIMAL_ID'] = 'Animal ID';
$mod_strings['LBL_BODYWEIGHT'] = 'Bodyweight';
$mod_strings['LBL_BREED'] = 'Breed';
$mod_strings['LBL_DATE_LAST_WEIGHED'] = 'Date Last Weighed';
$mod_strings['LBL_DATE_RECEIVED'] = 'Date Received';
$mod_strings['LBL_DECEASED'] = 'Deceased';
$mod_strings['LBL_DATE_OF_BIRTH'] = 'Date of Birth';
$mod_strings['LBL_ROOM'] = 'Room';
$mod_strings['LBL_SEX'] = 'Sex';
$mod_strings['LBL_SPECIES'] = 'Species';
$mod_strings['LBL_USDA_CATEGORY'] = 'USDA Category';
$mod_strings['LBL_USDA_ID'] = 'USDA ID';
$mod_strings['LBL_VENDOR'] = 'Vendor';
$mod_strings['LBL_VACCINE_A'] = 'Vaccine A';
$mod_strings['LBL_VACCINE_B'] = 'Vaccine B';
$mod_strings['LBL_VACCINE_C'] = 'Vaccine C';
$mod_strings['LBL_INITIAL_A'] = 'Initial A';
$mod_strings['LBL_INITIAL_B'] = 'Initial B';
$mod_strings['LBL_INITIAL_C'] = 'Initial C';
$mod_strings['LBL_BOOSTER_A1'] = 'Booster A1';
$mod_strings['LBL_BOOSTER_A2'] = 'Booster A2';
$mod_strings['LBL_BOOSTER_A3'] = 'Booster A3';
$mod_strings['LBL_BOOSTER_A4'] = 'Booster A4';
$mod_strings['LBL_BOOSTER_B1'] = 'Booster B1';
$mod_strings['LBL_BOOSTER_B2'] = 'Booster B2';
$mod_strings['LBL_BOOSTER_B3'] = 'Booster B3';
$mod_strings['LBL_BOOSTER_B4'] = 'Booster B4';
$mod_strings['LBL_BOOSTER_C1'] = 'Booster C1';
$mod_strings['LBL_BOOSTER_C2'] = 'Booster C2';
$mod_strings['LBL_BOOSTER_C3'] = 'Booster C3';
$mod_strings['LBL_BOOSTER_C4'] = 'Booster C4';
$mod_strings['LBL_TERMINATION_DATE'] = 'Termination Date';
$mod_strings['LBL_HUSBANDRY_STATUS'] = 'Husbandry Status';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Husbandry';
$mod_strings['LBL_SHOW_MORE'] = 'Demographics';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Vaccinations';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Work Product Enrollment';
$mod_strings['LBL_WP_ENROLLMENT'] = 'WP Enrollment';
$mod_strings['LBL_ASSIGNED_TO_WP'] = 'Assigned to WP';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.enrollment_status.php.AnimalsModuleCustomizations.lang.php


$mod_strings['LBL_ENROLLMENT_STATUS'] = 'Enrollment Status';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.APS_Sprint2_Package.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANIMAL_ROOM_RMS_ROOM_ID'] = 'Room (related Room ID)';
$mod_strings['LBL_ANIMAL_ROOM'] = 'Room';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.rt_mass_update_AnimalsModule.php


$mod_strings['LBL_RT_MASS_CREATE_BUTTON'] = 'RT Mass Create';
$mod_strings['LBL_RT_MASS_CREATION'] = 'RTs Mass Creation';
$mod_strings['LBL_CREATE_RTS_BUTTON_LABEL'] = 'Create RTs';
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.wpe_mass_update.php.AnimalsModuleCustomizations.lang.php


$mod_strings['LBL_WPE_MASS_CREATE_BUTTON'] = 'WPA Mass Create';
$mod_strings['LBL_WPE_MASS_CREATION'] = 'WPAs Mass Creation';
$mod_strings['LBL_CREATE_WPES_BUTTON_LABEL'] = 'Create WPAs';
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_ANML_ANIMALS_1_FROM_M06_ERROR_TITLE'] = 'Communications';
$mod_strings['LBL_SHOW_MORE'] = 'General Info';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'New Panel 3';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Current Info';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Notes';
$mod_strings['LBL_FIRST_PROCEDURE_DATE'] = 'First Procedure Date';
$mod_strings['LBL_NAME'] = 'Test System';
$mod_strings['LNK_NEW_RECORD'] = 'Create Test System';
$mod_strings['LNK_LIST'] = 'View Test Systems';
$mod_strings['LBL_MODULE_NAME'] = 'Test Systems';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Test System';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Test System';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Test System vCard';
$mod_strings['LNK_IMPORT_ANML_ANIMALS'] = 'Import Test Systems';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Test Systems List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Test System';
$mod_strings['LBL_ANML_ANIMALS_SUBPANEL_TITLE'] = 'Test Systems';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Test Systems';
$mod_strings['LBL_ANIMAL_ID'] = 'Test System ID';
$mod_strings['LBL_ASSIGNED_TO_WP'] = 'Current Work Product Assignment';
$mod_strings['LBL_ROOM'] = 'Room (out of use)';
$mod_strings['LBL_DECEASED_CB'] = 'Deceased CB';
$mod_strings['LBL_VENDOR_RELATE_ACCOUNT_ID'] = 'Vendor (related Company ID)';
$mod_strings['LBL_VENDOR_RELATE'] = 'Vendor';
$mod_strings['LBL_DECEASED_CHECKBOX'] = 'Deceased';
$mod_strings['LBL_VENDOR'] = 'Vendor (obsolete)';
$mod_strings['LBL_DECEASED'] = 'Deceased (obsolete)';
$mod_strings['LBL_NAME_CONCAT'] = 'Name Concat';
$mod_strings['LBL_BODYWEIGHT'] = 'Weight';
$mod_strings['LBL_SPECIES_2_S_SPECIES_ID'] = 'Species (relate) (related Species ID)';
$mod_strings['LBL_SPECIES_2'] = 'Species';
$mod_strings['LBL_SPECIES'] = 'Species (out of use)';
$mod_strings['LBL_ALLOCATED_WORK_PRODUCT_C_M03_WORK_PRODUCT_ID'] = 'Allocated Work Product (related Work Product ID)';
$mod_strings['LBL_ALLOCATED_WORK_PRODUCT'] = 'Allocated Work Product';
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Product Assignment';
$mod_strings['LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE'] = 'Clinical Issues &amp; Exams';
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Product Assignments';
$mod_strings['LBL_CONFIRMED_ON_STUDY'] = 'Confirmed On Study';
$mod_strings['LBL_USDA_ID_HIDDEN'] = 'USDA ID Hidden';
$mod_strings['LBL_ANML_ANIMALS_FOCUS_DRAWER_DASHBOARD'] = 'Test Systems Focus Drawer';
$mod_strings['LBL_ANML_ANIMALS_RECORD_DASHBOARD'] = 'Test Systems Record Dashboard';
$mod_strings['LBL_BOOSTER_A5'] = 'Booster A5';
$mod_strings['LBL_BOOSTER_A6'] = 'Booster A6';
$mod_strings['LBL_BOOSTER_B5'] = 'Booster B5';
$mod_strings['LBL_BOOSTER_B6'] = 'Booster B6';
$mod_strings['LBL_BOOSTER_C5'] = 'Booster C5';
$mod_strings['LBL_BOOSTER_C6'] = 'Booster C6';
$mod_strings['LBL_USDA_ID_HIDDEN_C2_C'] = 'Hidden USDA ID 2';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.customanml_animals_wpe_work_product_enrollment_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.customanml_animals_wpe_work_product_enrollment_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Product Assignment';
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE'] = 'Work Product Assignment';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Product Assignment';
$mod_strings['LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE'] = 'Work Product Assignment';


?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.customwpe_work_product_enrollment_anml_animals_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE'] = 'Work Product Enrollment';
$mod_strings['LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE'] = 'Work Product Enrollment';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.customanml_animals_rt_room_transfer_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_RT_ROOM_TRANSFER_TITLE'] = 'Room Transfers';
$mod_strings['LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_ANML_ANIMALS_TITLE'] = 'Room Transfers';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.customanml_animals_tsdoc_test_system_documents_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_TSDOC_TEST_SYSTEM_DOCUMENTS_TITLE'] = 'Test System Documents';
$mod_strings['LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_ANML_ANIMALS_TITLE'] = 'Test System Documents';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.customanml_animals_w_weight_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_W_WEIGHT_TITLE'] = 'Weights';
$mod_strings['LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_ANML_ANIMALS_TITLE'] = 'Weights';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.customanml_animals_cie_clinical_issue_exam_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE'] = 'Clinical Issues & Exams';
$mod_strings['LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_ANML_ANIMALS_TITLE'] = 'Clinical Issues & Exams';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.customanml_animals_co_clinical_observation_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE'] = 'Clinical Observations';
$mod_strings['LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_ANML_ANIMALS_TITLE'] = 'Clinical Observations';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.customanml_animals_usda_historical_usda_id_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_USDA_HISTORICAL_USDA_ID_TITLE'] = 'Historical USDA IDs';
$mod_strings['LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_ANML_ANIMALS_TITLE'] = 'Historical USDA IDs';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.customanml_animals_taskd_task_design_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE'] = 'Task Designs';
$mod_strings['LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_ANML_ANIMALS_TITLE'] = 'Task Designs';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.customm06_error_anml_animals_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_M06_ERROR_ANML_ANIMALS_1_FROM_M06_ERROR_TITLE'] = 'Communications';
$mod_strings['LBL_M06_ERROR_ANML_ANIMALS_1_FROM_ANML_ANIMALS_TITLE'] = 'Communications';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Language/en_us.customanml_animals_ii_inventory_item_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE'] = 'Inventory Items';
$mod_strings['LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_ANML_ANIMALS_TITLE'] = 'Inventory Items';

?>
