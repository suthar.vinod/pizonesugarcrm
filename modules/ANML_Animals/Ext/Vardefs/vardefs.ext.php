<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/nameFieldCustomization.php


$dictionary['ANML_Animals']['fields']['usda_category_c']['readonly'] = 'true';
//$dictionary['ANML_Animals']['fields']['name']['readonly'] = 'true';

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_wpe_work_product_enrollment_1_ANML_Animals.php

// created: 2017-09-12 15:03:53
$dictionary["ANML_Animals"]["fields"]["anml_animals_wpe_work_product_enrollment_1"] = array (
  'name' => 'anml_animals_wpe_work_product_enrollment_1',
  'type' => 'link',
  'relationship' => 'anml_animals_wpe_work_product_enrollment_1',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_wpe_work_product_enrollment_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/wpe_work_product_enrollment_anml_animals_1_ANML_Animals.php

// created: 2017-09-12 15:08:34
$dictionary["ANML_Animals"]["fields"]["wpe_work_product_enrollment_anml_animals_1"] = array (
  'name' => 'wpe_work_product_enrollment_anml_animals_1',
  'type' => 'link',
  'relationship' => 'wpe_work_product_enrollment_anml_animals_1',
  'source' => 'non-db',
  'module' => 'WPE_Work_Product_Enrollment',
  'bean_name' => 'WPE_Work_Product_Enrollment',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'id_name' => 'wpe_work_p83ebollment_ida',
);
$dictionary["ANML_Animals"]["fields"]["wpe_work_product_enrollment_anml_animals_1_name"] = array (
  'name' => 'wpe_work_product_enrollment_anml_animals_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'save' => true,
  'id_name' => 'wpe_work_p83ebollment_ida',
  'link' => 'wpe_work_product_enrollment_anml_animals_1',
  'table' => 'wpe_work_product_enrollment',
  'module' => 'WPE_Work_Product_Enrollment',
  'rname' => 'name',
);
$dictionary["ANML_Animals"]["fields"]["wpe_work_p83ebollment_ida"] = array (
  'name' => 'wpe_work_p83ebollment_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_WPE_WORK_PRODUCT_ENROLLMENT_ANML_ANIMALS_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE_ID',
  'id_name' => 'wpe_work_p83ebollment_ida',
  'link' => 'wpe_work_product_enrollment_anml_animals_1',
  'table' => 'wpe_work_product_enrollment',
  'module' => 'WPE_Work_Product_Enrollment',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_comments_c.php

 // created: 2017-10-06 13:36:46
$dictionary['ANML_Animals']['fields']['comments_c']['labelValue']='Comments';
$dictionary['ANML_Animals']['fields']['comments_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['comments_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['comments_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/m06_error_anml_animals_1_ANML_Animals.php

// created: 2018-01-31 16:00:43
$dictionary["ANML_Animals"]["fields"]["m06_error_anml_animals_1"] = array (
  'name' => 'm06_error_anml_animals_1',
  'type' => 'link',
  'relationship' => 'm06_error_anml_animals_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_ANML_ANIMALS_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_anml_animals_1m06_error_ida',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_usda_id_c.php

 // created: 2019-04-11 10:54:54
$dictionary['ANML_Animals']['fields']['usda_id_c']['labelValue']='USDA ID';
$dictionary['ANML_Animals']['fields']['usda_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['usda_id_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['usda_id_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_usda_category_c.php

 // created: 2019-04-11 10:57:58
$dictionary['ANML_Animals']['fields']['usda_category_c']['labelValue']='USDA Category';
$dictionary['ANML_Animals']['fields']['usda_category_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['usda_category_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_first_procedure_date_c.php

 // created: 2019-04-11 10:58:49
$dictionary['ANML_Animals']['fields']['first_procedure_date_c']['labelValue']='First Procedure Date';
$dictionary['ANML_Animals']['fields']['first_procedure_date_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['first_procedure_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_termination_date_c.php

 // created: 2019-04-11 10:59:27
$dictionary['ANML_Animals']['fields']['termination_date_c']['labelValue']='Termination Date';
$dictionary['ANML_Animals']['fields']['termination_date_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['termination_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_date_of_birth_c.php

 // created: 2019-04-11 11:01:40
$dictionary['ANML_Animals']['fields']['date_of_birth_c']['labelValue']='Date of Birth';
$dictionary['ANML_Animals']['fields']['date_of_birth_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['date_of_birth_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_date_received_c.php

 // created: 2019-04-11 11:02:48
$dictionary['ANML_Animals']['fields']['date_received_c']['labelValue']='Date Received';
$dictionary['ANML_Animals']['fields']['date_received_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['date_received_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_date_last_weighed_c.php

 // created: 2019-04-11 11:04:20
$dictionary['ANML_Animals']['fields']['date_last_weighed_c']['labelValue']='Date Last Weighed';
$dictionary['ANML_Animals']['fields']['date_last_weighed_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['date_last_weighed_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_initial_a_c.php

 // created: 2019-04-11 11:12:09
$dictionary['ANML_Animals']['fields']['initial_a_c']['labelValue']='Initial A';
$dictionary['ANML_Animals']['fields']['initial_a_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['initial_a_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_a1_c.php

 // created: 2019-04-11 11:12:49
$dictionary['ANML_Animals']['fields']['booster_a1_c']['labelValue']='Booster A1';
$dictionary['ANML_Animals']['fields']['booster_a1_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_a1_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_a2_c.php

 // created: 2019-04-11 11:13:26
$dictionary['ANML_Animals']['fields']['booster_a2_c']['labelValue']='Booster A2';
$dictionary['ANML_Animals']['fields']['booster_a2_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_a2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_a3_c.php

 // created: 2019-04-11 11:14:26
$dictionary['ANML_Animals']['fields']['booster_a3_c']['labelValue']='Booster A3';
$dictionary['ANML_Animals']['fields']['booster_a3_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_a3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_a4_c.php

 // created: 2019-04-11 11:15:02
$dictionary['ANML_Animals']['fields']['booster_a4_c']['labelValue']='Booster A4';
$dictionary['ANML_Animals']['fields']['booster_a4_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_a4_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_initial_b_c.php

 // created: 2019-04-11 11:16:26
$dictionary['ANML_Animals']['fields']['initial_b_c']['labelValue']='Initial B';
$dictionary['ANML_Animals']['fields']['initial_b_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['initial_b_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_b1_c.php

 // created: 2019-04-11 11:17:11
$dictionary['ANML_Animals']['fields']['booster_b1_c']['labelValue']='Booster B1';
$dictionary['ANML_Animals']['fields']['booster_b1_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_b1_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_b2_c.php

 // created: 2019-04-11 11:17:58
$dictionary['ANML_Animals']['fields']['booster_b2_c']['labelValue']='Booster B2';
$dictionary['ANML_Animals']['fields']['booster_b2_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_b2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_b3_c.php

 // created: 2019-04-11 11:18:38
$dictionary['ANML_Animals']['fields']['booster_b3_c']['labelValue']='Booster B3';
$dictionary['ANML_Animals']['fields']['booster_b3_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_b3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_b4_c.php

 // created: 2019-04-11 11:19:09
$dictionary['ANML_Animals']['fields']['booster_b4_c']['labelValue']='Booster B4';
$dictionary['ANML_Animals']['fields']['booster_b4_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_b4_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_initial_c.php

 // created: 2019-04-11 11:29:56
$dictionary['ANML_Animals']['fields']['initial_c']['labelValue']='Initial C';
$dictionary['ANML_Animals']['fields']['initial_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['initial_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_c1_c.php

 // created: 2019-04-11 11:30:41
$dictionary['ANML_Animals']['fields']['booster_c1_c']['labelValue']='Booster C1';
$dictionary['ANML_Animals']['fields']['booster_c1_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_c1_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_c2_c.php

 // created: 2019-04-11 11:32:14
$dictionary['ANML_Animals']['fields']['booster_c2_c']['labelValue']='Booster C2';
$dictionary['ANML_Animals']['fields']['booster_c2_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_c2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_c3_c.php

 // created: 2019-04-11 11:33:16
$dictionary['ANML_Animals']['fields']['booster_c3_c']['labelValue']='Booster C3';
$dictionary['ANML_Animals']['fields']['booster_c3_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_c3_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_c4_c.php

 // created: 2019-04-11 11:34:18
$dictionary['ANML_Animals']['fields']['booster_c4_c']['labelValue']='Booster C4';
$dictionary['ANML_Animals']['fields']['booster_c4_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_c4_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_rms_room_id_c.php

 // created: 2019-05-18 07:16:55

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_rt_room_transfer_1_ANML_Animals.php

// created: 2019-02-20 13:19:54
$dictionary["ANML_Animals"]["fields"]["anml_animals_rt_room_transfer_1"] = array (
  'name' => 'anml_animals_rt_room_transfer_1',
  'type' => 'link',
  'relationship' => 'anml_animals_rt_room_transfer_1',
  'source' => 'non-db',
  'module' => 'RT_Room_Transfer',
  'bean_name' => 'RT_Room_Transfer',
  'vname' => 'LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_rt_room_transfer_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_account_id_c.php

 // created: 2019-07-02 05:24:54

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_name.php

 // created: 2019-07-09 12:08:31
$dictionary['ANML_Animals']['fields']['name']['len']='255';
$dictionary['ANML_Animals']['fields']['name']['audited']=true;
$dictionary['ANML_Animals']['fields']['name']['massupdate']=false;
$dictionary['ANML_Animals']['fields']['name']['unified_search']=false;
$dictionary['ANML_Animals']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['ANML_Animals']['fields']['name']['calculated']='true';
$dictionary['ANML_Animals']['fields']['name']['importable']='false';
$dictionary['ANML_Animals']['fields']['name']['duplicate_merge']='disabled';
$dictionary['ANML_Animals']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['ANML_Animals']['fields']['name']['merge_filter']='disabled';
$dictionary['ANML_Animals']['fields']['name']['required']=true;
$dictionary['ANML_Animals']['fields']['name']['formula']='$animal_id_c';
$dictionary['ANML_Animals']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_name_concat_c.php

 // created: 2019-09-11 13:19:29
$dictionary['ANML_Animals']['fields']['name_concat_c']['duplicate_merge_dom_value']=0;
$dictionary['ANML_Animals']['fields']['name_concat_c']['labelValue']='Name Concat';
$dictionary['ANML_Animals']['fields']['name_concat_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['name_concat_c']['calculated']='true';
$dictionary['ANML_Animals']['fields']['name_concat_c']['formula']='concat($animal_id_c," ","(",$usda_id_c,")")';
$dictionary['ANML_Animals']['fields']['name_concat_c']['enforced']='true';
$dictionary['ANML_Animals']['fields']['name_concat_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_tsdoc_test_system_documents_1_ANML_Animals.php

// created: 2019-09-30 12:27:41
$dictionary["ANML_Animals"]["fields"]["anml_animals_tsdoc_test_system_documents_1"] = array (
  'name' => 'anml_animals_tsdoc_test_system_documents_1',
  'type' => 'link',
  'relationship' => 'anml_animals_tsdoc_test_system_documents_1',
  'source' => 'non-db',
  'module' => 'TSdoc_Test_System_Documents',
  'bean_name' => 'TSdoc_Test_System_Documents',
  'vname' => 'LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_tsdoc_test_system_documents_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_animal_room_c.php

 // created: 2019-11-15 11:20:53
$dictionary['ANML_Animals']['fields']['animal_room_c']['labelValue']='Room';
$dictionary['ANML_Animals']['fields']['animal_room_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['animal_room_c']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_bodyweight_c.php

 // created: 2019-11-27 12:27:06
$dictionary['ANML_Animals']['fields']['bodyweight_c']['labelValue']='Weight';
$dictionary['ANML_Animals']['fields']['bodyweight_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['bodyweight_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_s_species_id_c.php

 // created: 2019-12-02 13:50:25

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/enrollment_status.php


$dictionary['ANML_Animals']['fields']['enrollment_status'] = array(
    'name' => "enrollment_status",
    'default' => "Choose One",
    'importable' => "true",
    'len' => 100,
    'no_default' => false,
    'options' => "enrollment_status_list",
    'reportable' => true,
    'required' => false,
    'size' => 20,
    'type' => "enum",
    'unified_search' => false,
    'vname' => "LBL_ENROLLMENT_STATUS",
    'label' => "LBL_ENROLLMENT_STATUS",
    'readonly' => true,
);


?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_assigned_to_wp_c.php

 // created: 2019-04-11 11:08:52
$dictionary['ANML_Animals']['fields']['assigned_to_wp_c']['labelValue']='Assigned to WP';
$dictionary['ANML_Animals']['fields']['assigned_to_wp_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['assigned_to_wp_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['assigned_to_wp_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['assigned_to_wp_c']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/custom_import_index.php


$dictionary['ANML_Animals']['indices'][] = array(
     'name' => 'idx_usda_and_animal_cstm',
     'type' => 'index',
     'fields' => array(
         0 => 'usda_id_c',
         1 => 'animal_id_c',
     ),
     'source' => 'non-db',
);



?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_w_weight_1_ANML_Animals.php

// created: 2020-01-07 13:24:35
$dictionary["ANML_Animals"]["fields"]["anml_animals_w_weight_1"] = array (
  'name' => 'anml_animals_w_weight_1',
  'type' => 'link',
  'relationship' => 'anml_animals_w_weight_1',
  'source' => 'non-db',
  'module' => 'W_Weight',
  'bean_name' => 'W_Weight',
  'vname' => 'LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_w_weight_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_cie_clinical_issue_exam_1_ANML_Animals.php

// created: 2020-01-07 13:40:49
$dictionary["ANML_Animals"]["fields"]["anml_animals_cie_clinical_issue_exam_1"] = array (
  'name' => 'anml_animals_cie_clinical_issue_exam_1',
  'type' => 'link',
  'relationship' => 'anml_animals_cie_clinical_issue_exam_1',
  'source' => 'non-db',
  'module' => 'CIE_Clinical_Issue_Exam',
  'bean_name' => 'CIE_Clinical_Issue_Exam',
  'vname' => 'LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_cie_clinical_issue_exam_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_co_clinical_observation_1_ANML_Animals.php

// created: 2020-01-07 14:17:22
$dictionary["ANML_Animals"]["fields"]["anml_animals_co_clinical_observation_1"] = array (
  'name' => 'anml_animals_co_clinical_observation_1',
  'type' => 'link',
  'relationship' => 'anml_animals_co_clinical_observation_1',
  'source' => 'non-db',
  'module' => 'CO_Clinical_Observation',
  'bean_name' => 'CO_Clinical_Observation',
  'vname' => 'LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_co_clinical_observation_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_vaccine_c.php

 // created: 2020-05-21 18:00:56
$dictionary['ANML_Animals']['fields']['vaccine_c']['labelValue']='Vaccine C';
$dictionary['ANML_Animals']['fields']['vaccine_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['vaccine_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_vaccine_a_c.php

 // created: 2020-05-21 17:59:20
$dictionary['ANML_Animals']['fields']['vaccine_a_c']['labelValue']='Vaccine A';
$dictionary['ANML_Animals']['fields']['vaccine_a_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['vaccine_a_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_vaccine_b_c.php

 // created: 2020-05-21 18:00:17
$dictionary['ANML_Animals']['fields']['vaccine_b_c']['labelValue']='Vaccine B';
$dictionary['ANML_Animals']['fields']['vaccine_b_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['vaccine_b_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_sex_c.php

 // created: 2020-05-21 17:58:55
$dictionary['ANML_Animals']['fields']['sex_c']['labelValue']='Sex';
$dictionary['ANML_Animals']['fields']['sex_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['sex_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_breed_c.php

 // created: 2020-06-11 12:36:58
$dictionary['ANML_Animals']['fields']['breed_c']['labelValue']='Breed';
$dictionary['ANML_Animals']['fields']['breed_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['breed_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_m03_work_product_id_c.php

 // created: 2020-12-15 06:31:18

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_enrollment_status.php

 // created: 2020-12-15 12:39:26
$dictionary['ANML_Animals']['fields']['enrollment_status']['audited']=true;
$dictionary['ANML_Animals']['fields']['enrollment_status']['massupdate']=false;
$dictionary['ANML_Animals']['fields']['enrollment_status']['duplicate_merge']='enabled';
$dictionary['ANML_Animals']['fields']['enrollment_status']['duplicate_merge_dom_value']='1';
$dictionary['ANML_Animals']['fields']['enrollment_status']['merge_filter']='disabled';
$dictionary['ANML_Animals']['fields']['enrollment_status']['calculated']=false;
$dictionary['ANML_Animals']['fields']['enrollment_status']['dependency']=false;
$dictionary['ANML_Animals']['fields']['enrollment_status']['default']='';
$dictionary['ANML_Animals']['fields']['enrollment_status']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_usda_historical_usda_id_1_ANML_Animals.php

// created: 2021-02-23 10:42:04
$dictionary["ANML_Animals"]["fields"]["anml_animals_usda_historical_usda_id_1"] = array (
  'name' => 'anml_animals_usda_historical_usda_id_1',
  'type' => 'link',
  'relationship' => 'anml_animals_usda_historical_usda_id_1',
  'source' => 'non-db',
  'module' => 'USDA_Historical_USDA_ID',
  'bean_name' => 'USDA_Historical_USDA_ID',
  'vname' => 'LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_usda_historical_usda_id_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_usda_id_hidden_c.php

 // created: 2021-02-23 10:47:52
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c']['labelValue']='USDA ID Hidden';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_confirmed_on_study_c.php

 // created: 2021-01-05 13:51:02
$dictionary['ANML_Animals']['fields']['confirmed_on_study_c']['labelValue']='Confirmed On Study';
$dictionary['ANML_Animals']['fields']['confirmed_on_study_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['confirmed_on_study_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['confirmed_on_study_c']['massupdate']=true;
 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_abnormality_c.php

 // created: 2019-04-11 11:00:08
$dictionary['ANML_Animals']['fields']['abnormality_c']['labelValue']='Abnormality';
$dictionary['ANML_Animals']['fields']['abnormality_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['abnormality_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['abnormality_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_deceased_checkbox_c.php

 // created: 2021-03-09 05:02:15
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['duplicate_merge_dom_value']=0;
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['labelValue']='Deceased';
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['calculated']='true';
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['formula']='greaterThan(strlen(toString($termination_date_c)),0)';
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['enforced']='true';
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['deceased_checkbox_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_a5_c.php

 // created: 2021-05-18 07:08:43
$dictionary['ANML_Animals']['fields']['booster_a5_c']['labelValue']='Booster A5';
$dictionary['ANML_Animals']['fields']['booster_a5_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_a5_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['booster_a5_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['booster_a5_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_a6_c.php

 // created: 2021-05-18 07:10:32
$dictionary['ANML_Animals']['fields']['booster_a6_c']['labelValue']='Booster A6';
$dictionary['ANML_Animals']['fields']['booster_a6_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_a6_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['booster_a6_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['booster_a6_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_b5_c.php

 // created: 2021-05-18 07:12:46
$dictionary['ANML_Animals']['fields']['booster_b5_c']['labelValue']='Booster B5';
$dictionary['ANML_Animals']['fields']['booster_b5_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_b5_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['booster_b5_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['booster_b5_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_b6_c.php

 // created: 2021-05-18 07:14:33
$dictionary['ANML_Animals']['fields']['booster_b6_c']['labelValue']='Booster B6';
$dictionary['ANML_Animals']['fields']['booster_b6_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_b6_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['booster_b6_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['booster_b6_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_c5_c.php

 // created: 2021-05-18 07:16:40
$dictionary['ANML_Animals']['fields']['booster_c5_c']['labelValue']='Booster C5';
$dictionary['ANML_Animals']['fields']['booster_c5_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_c5_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['booster_c5_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['booster_c5_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_booster_c6_c.php

 // created: 2021-05-18 07:18:34
$dictionary['ANML_Animals']['fields']['booster_c6_c']['labelValue']='Booster C6';
$dictionary['ANML_Animals']['fields']['booster_c6_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['booster_c6_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['booster_c6_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['booster_c6_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_usda_id_hidden_c2_c.php

 // created: 2021-05-24 09:09:22
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['duplicate_merge_dom_value']=0;
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['labelValue']='Hidden USDA ID 2';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['calculated']='true';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['formula']='related($anml_animals_usda_historical_usda_id_1,"name")';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['enforced']='true';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['usda_id_hidden_c2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_allocated_work_product_c.php

 // created: 2021-06-03 11:16:45
$dictionary['ANML_Animals']['fields']['allocated_work_product_c']['labelValue']='Allocated Work Product';
$dictionary['ANML_Animals']['fields']['allocated_work_product_c']['dependency']='equal($assigned_to_wp_c,"APS001-AH01")';
$dictionary['ANML_Animals']['fields']['allocated_work_product_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['allocated_work_product_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_taskd_task_design_1_ANML_Animals.php

// created: 2021-08-14 06:05:14
$dictionary["ANML_Animals"]["fields"]["anml_animals_taskd_task_design_1"] = array (
  'name' => 'anml_animals_taskd_task_design_1',
  'type' => 'link',
  'relationship' => 'anml_animals_taskd_task_design_1',
  'source' => 'non-db',
  'module' => 'TaskD_Task_Design',
  'bean_name' => 'TaskD_Task_Design',
  'vname' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_taskd_task_design_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_vendor_relate_c.php

 // created: 2021-08-19 07:29:44
$dictionary['ANML_Animals']['fields']['vendor_relate_c']['labelValue']='Vendor';
$dictionary['ANML_Animals']['fields']['vendor_relate_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['vendor_relate_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['vendor_relate_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/anml_animals_ii_inventory_item_1_ANML_Animals.php

// created: 2021-11-09 10:02:11
$dictionary["ANML_Animals"]["fields"]["anml_animals_ii_inventory_item_1"] = array (
  'name' => 'anml_animals_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'anml_animals_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_ANML_ANIMALS_TITLE',
  'id_name' => 'anml_animals_ii_inventory_item_1anml_animals_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_animal_id_c.php

 // created: 2022-08-03 09:55:47
$dictionary['ANML_Animals']['fields']['animal_id_c']['labelValue']='Test System ID';
$dictionary['ANML_Animals']['fields']['animal_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ANML_Animals']['fields']['animal_id_c']['enforced']='';
$dictionary['ANML_Animals']['fields']['animal_id_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['animal_id_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['animal_id_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/ANML_Animals/Ext/Vardefs/sugarfield_species_2_c.php

 // created: 2022-08-30 09:54:46
$dictionary['ANML_Animals']['fields']['species_2_c']['labelValue']='Species';
$dictionary['ANML_Animals']['fields']['species_2_c']['dependency']='';
$dictionary['ANML_Animals']['fields']['species_2_c']['required_formula']='';
$dictionary['ANML_Animals']['fields']['species_2_c']['readonly_formula']='';

 
?>
