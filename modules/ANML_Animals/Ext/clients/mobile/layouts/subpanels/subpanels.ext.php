<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-01-07 13:40:49
$viewdefs['ANML_Animals']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ANML_ANIMALS_CIE_CLINICAL_ISSUE_EXAM_1_FROM_CIE_CLINICAL_ISSUE_EXAM_TITLE',
  'context' => 
  array (
    'link' => 'anml_animals_cie_clinical_issue_exam_1',
  ),
);

// created: 2020-01-07 14:17:22
$viewdefs['ANML_Animals']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ANML_ANIMALS_CO_CLINICAL_OBSERVATION_1_FROM_CO_CLINICAL_OBSERVATION_TITLE',
  'context' => 
  array (
    'link' => 'anml_animals_co_clinical_observation_1',
  ),
);

// created: 2021-11-09 10:02:11
$viewdefs['ANML_Animals']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ANML_ANIMALS_II_INVENTORY_ITEM_1_FROM_II_INVENTORY_ITEM_TITLE',
  'context' => 
  array (
    'link' => 'anml_animals_ii_inventory_item_1',
  ),
);

// created: 2019-02-20 13:19:54
$viewdefs['ANML_Animals']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ANML_ANIMALS_RT_ROOM_TRANSFER_1_FROM_RT_ROOM_TRANSFER_TITLE',
  'context' => 
  array (
    'link' => 'anml_animals_rt_room_transfer_1',
  ),
);

// created: 2021-08-14 06:05:14
$viewdefs['ANML_Animals']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ANML_ANIMALS_TASKD_TASK_DESIGN_1_FROM_TASKD_TASK_DESIGN_TITLE',
  'context' => 
  array (
    'link' => 'anml_animals_taskd_task_design_1',
  ),
);

// created: 2019-09-30 12:27:41
$viewdefs['ANML_Animals']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ANML_ANIMALS_TSDOC_TEST_SYSTEM_DOCUMENTS_1_FROM_TSDOC_TEST_SYSTEM_DOCUMENTS_TITLE',
  'context' => 
  array (
    'link' => 'anml_animals_tsdoc_test_system_documents_1',
  ),
);

// created: 2021-02-23 10:42:04
$viewdefs['ANML_Animals']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ANML_ANIMALS_USDA_HISTORICAL_USDA_ID_1_FROM_USDA_HISTORICAL_USDA_ID_TITLE',
  'context' => 
  array (
    'link' => 'anml_animals_usda_historical_usda_id_1',
  ),
);

// created: 2020-01-07 13:24:35
$viewdefs['ANML_Animals']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ANML_ANIMALS_W_WEIGHT_1_FROM_W_WEIGHT_TITLE',
  'context' => 
  array (
    'link' => 'anml_animals_w_weight_1',
  ),
);

// created: 2017-09-12 15:03:53
$viewdefs['ANML_Animals']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ANML_ANIMALS_WPE_WORK_PRODUCT_ENROLLMENT_1_FROM_WPE_WORK_PRODUCT_ENROLLMENT_TITLE',
  'context' => 
  array (
    'link' => 'anml_animals_wpe_work_product_enrollment_1',
  ),
);

// created: 2018-01-31 16:00:43
$viewdefs['ANML_Animals']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_ANML_ANIMALS_1_FROM_M06_ERROR_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_anml_animals_1',
  ),
);

// created: 2017-09-12 15:03:53
$viewdefs['ANML_Animals']['mobile']['layout']['subpanels']['components'][] = array (
  'override_subpanel_list_view' => 
  array (
    'link' => 'anml_animals_wpe_work_product_enrollment_1',
    'view' => 'subpanel-for-anml_animalsanml_animals_wpe_work_product_enrollment_1',
  ),
);