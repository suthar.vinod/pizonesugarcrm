<?php
// WARNING: The contents of this file are auto-generated.



$viewdefs['ANML_Animals']['base']['view']['recordlist']['selection']['actions'][] = array(
    'name' => 'rt_mass_create_button',
    'label' => 'LBL_RT_MASS_CREATE_BUTTON',
    'type' => 'button',
    'primary' => true,
    'events' => array(
        'click' => 'list:rtmasscreaterecords:fire',
    ),
    'acl_action' => 'massupdate',
);



$viewdefs['ANML_Animals']['base']['view']['recordlist']['selection']['actions'][] = array(
    'name' => 'wpe_mass_create_button',
    'label' => 'LBL_WPE_MASS_CREATE_BUTTON',
    'type' => 'button',
    'primary' => true,
    'events' => array(
        'click' => 'list:wpemasscreaterecords:fire',
    ),
    'acl_action' => 'massupdate',
);
