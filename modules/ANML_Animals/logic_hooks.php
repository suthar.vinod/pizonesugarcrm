<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will        
// be automatically rebuilt in the future. 
$hook_version = 1;
$hook_array = Array();
// position, file, function 
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array('1','workflow','include/workflow/WorkFlowHandler.php','WorkFlowHandler','WorkFlowHandler',);
$hook_array['process_record'][] = array(
    '11',
    'delete allocated wp in Test system audit Record',
    'custom/modules/ANML_Animals/deleteAuditLogAllocatedWP.php',
    'deleteAuditLogAllocatedWP',
    'deleteAllocatedWP'
);

$hook_array['after_relationship_add'][] = array(
   '12',
   'After relating a Historical USDA ID records linked to a Test System',
   'custom/modules/ANML_Animals/Get_usda_record.php',
   'Get_Latest_usda_record',
   'usda_record',
);

$hook_array['after_relationship_delete'][] = array(
   '14',
   'After relating a Historical USDA ID records linked to a Test System',
   'custom/modules/ANML_Animals/Get_usda_record.php',
   'Get_Latest_usda_record',
   'usda_record',
);

$hook_array['before_save'][] = array(
   '15',
   'Update USDA ID for WPA ',
   'custom/modules/ANML_Animals/update_wpa_record.php',
   'update_wpa_record',
   'usda_WPA',
);

$hook_array['before_save'][] = array(
   100,
  'Send Email for Animal Assignment Notification in Work Product After Relationship Add', 
      'custom/modules/ANML_Animals/animal_assignment_notification.php', 
      'WPE_animalNotificationAfterRelationAddHook', 
      'SendAnimalAssignmentNotification'
);

$hook_array['before_save'][] = array(
   '17',
   'Update II Name on update USDA ID',
   'custom/modules/ANML_Animals/update_II_Name.php',
   'updateIINameOnUpdateUSDAId',
   'updateIIName',
);

?>