<?php
$popupMeta = array (
    'moduleMain' => 'ANML_Animals',
    'varName' => 'ANML_Animals',
    'orderBy' => 'anml_animals.name',
    'whereClauses' => array (
  'name' => 'anml_animals.name',
  'assigned_user_id' => 'anml_animals.assigned_user_id',
  'favorites_only' => 'anml_animals.favorites_only',
  'assigned_to_wp_c' => 'anml_animals_cstm.assigned_to_wp_c',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'assigned_user_id',
  5 => 'favorites_only',
  6 => 'assigned_to_wp_c',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => 10,
  ),
  'favorites_only' => 
  array (
    'name' => 'favorites_only',
    'label' => 'LBL_FAVORITES_FILTER',
    'type' => 'bool',
    'width' => 10,
  ),
  'assigned_to_wp_c' => 
  array (
    'readonly' => true,
    'type' => 'varchar',
    'label' => 'LBL_ASSIGNED_TO_WP',
    'width' => 10,
    'name' => 'assigned_to_wp_c',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'USDA_ID_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_USDA_ID',
    'width' => 10,
    'default' => true,
    'name' => 'usda_id_c',
  ),
  'ASSIGNED_TO_WP_C' => 
  array (
    'readonly' => true,
    'type' => 'varchar',
    'label' => 'LBL_ASSIGNED_TO_WP',
    'width' => 10,
    'default' => true,
  ),
),
);
