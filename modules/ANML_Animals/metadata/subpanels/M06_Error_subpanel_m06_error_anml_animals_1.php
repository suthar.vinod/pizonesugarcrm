<?php
// created: 2021-09-07 18:32:53
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'usda_id_c' => 
  array (
    'readonly' => false,
    'type' => 'varchar',
    'vname' => 'LBL_USDA_ID',
    'width' => 10,
    'default' => true,
  ),
  'animal_room_c' => 
  array (
    'readonly' => true,
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_ANIMAL_ROOM',
    'id' => 'RMS_ROOM_ID_C',
    'link' => true,
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'RMS_Room',
    'target_record_key' => 'rms_room_id_c',
  ),
);