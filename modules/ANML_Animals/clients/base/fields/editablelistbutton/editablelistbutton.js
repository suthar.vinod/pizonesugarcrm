/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.EditablelistbuttonField
 * @alias SUGAR.App.view.fields.BaseEditablelistbuttonField
 * @extends View.Fields.Base.ButtonField
 */
({
    extendsFrom: 'BaseEditablelistbuttonField',
    /**
     * @inheritdoc
     */
    events: {
        'mousedown [name=inline-save]': 'saveClicked',
        'mousedown [name=inline-cancel]': 'cancelClicked'
    },
    _render: function() {
        this._super('_render');
    },
    /**
     * Overriding and not calling parent _loadTemplate as those are based off view/actions and we
     * specifically need it based off the modelView set by the parent layout for this row model
     *
     * @inheritdoc
     */
    _loadTemplate: function() {
        this._super('_loadTemplate');
    },
     /* #1274 : 10 May 2021 : gsingh */
    saveClicked: function(evt) {
        var allocated_work_product_c = this.model.get('allocated_work_product_c');
        var rowid = this.model.get('id');
        var self = this;
        console.log('allocated_work_product_c : ', allocated_work_product_c);
        var TS_API = "rest/v10/ANML_Animals/" + rowid + "?fields=allocated_work_product_c";
        App.api.call("get", TS_API, null, {
            success: function(TSData) {
                var old_allocated_WP_value = TSData.allocated_work_product_c;
                console.log('TS_API data : ', TSData);
                console.log('TS_API data : allocated_work_product_c : ', old_allocated_WP_value);
                if (old_allocated_WP_value !='' && old_allocated_WP_value != allocated_work_product_c && allocated_work_product_c != '') {
                    app.alert.show('Allocated_WP_check_pop_up', {
                        level: 'confirmation',
                        messages: 'These animals are already allocated, are you sure you want to re-assign?',
                        autoClose: false,
                        confirm: {
                            label: "Yes"
                        },
                        cancel: {
                            label: "No"
                        },
                        onCancel: function() {
                            self.cancelEdit();
                        },
                        onConfirm: function() {
                            self.saveModel();
                        }
                    });
                } else {
                    self.saveModel();
                }
            }
        });


    },
     /* EOC #1274 : 10 May 2021 : gsingh */
})