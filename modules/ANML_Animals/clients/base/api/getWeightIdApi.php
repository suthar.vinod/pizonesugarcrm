<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class getWeightIdApi extends SugarApi {

    public function registerApiRest() {
        return array(
            'getWeightIdData' => array(
                'reqType' => 'POST',
                'noLoginRequired' => false,
                'path' => array('getWeightId'),
                'pathVars' => array('getWeightId'),
                'method' => 'getWeightId',
                'shortHelp' => '',
                'longHelp' => '',
            ),
        );
    }

    
    function getWeightId($api, $args) {
		global $db,$current_user;
		 
		$module = $args['module'];
		$moduleId = $args['moduleId'];
		
		$wpe_sql = "SELECT id FROM anml_animals_w_weight_1_c where anml_animals_w_weight_1anml_animals_ida = '".$moduleId."' AND deleted = 0 ORDER BY date_modified DESC LIMIT 1;";
			$wpe_exec = $db->query($wpe_sql);
			if ($wpe_exec->num_rows > 0) 
			{ 
				$resultWP = $db->fetchByAssoc($wpe_exec);
				$Weight_id = $resultWP['id'];
				return $Weight_id;
			}
		 
    }
	
}

