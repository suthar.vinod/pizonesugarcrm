<?php
$module_name = 'ANML_Animals';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'usda_id_c',
                'label' => 'LBL_USDA_ID',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'abnormality_c',
                'label' => 'LBL_ABNORMALITY',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'bodyweight_c',
                'label' => 'LBL_BODYWEIGHT',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'date_last_weighed_c',
                'label' => 'LBL_DATE_LAST_WEIGHED',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'assigned_to_wp_c',
                'label' => 'LBL_ASSIGNED_TO_WP',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'allocated_work_product_c',
                'label' => 'LBL_ALLOCATED_WORK_PRODUCT',
                'enabled' => true,
                'id' => 'M03_WORK_PRODUCT_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'first_procedure_date_c',
                'label' => 'LBL_FIRST_PROCEDURE_DATE',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'termination_date_c',
                'label' => 'LBL_TERMINATION_DATE',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'deceased_checkbox_c',
                'label' => 'LBL_DECEASED_CHECKBOX',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'confirmed_on_study_c',
                'label' => 'LBL_CONFIRMED_ON_STUDY',
                'enabled' => true,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'date_received_c',
                'label' => 'LBL_DATE_RECEIVED',
                'enabled' => true,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'animal_room_c',
                'label' => 'LBL_ANIMAL_ROOM',
                'enabled' => true,
                'id' => 'RMS_ROOM_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              13 => 
              array (
                'name' => 'sex_c',
                'label' => 'LBL_SEX',
                'enabled' => true,
                'default' => true,
              ),
              14 => 
              array (
                'name' => 'date_of_birth_c',
                'label' => 'LBL_DATE_OF_BIRTH',
                'enabled' => true,
                'default' => true,
              ),
              15 => 
              array (
                'name' => 'vendor_relate_c',
                'label' => 'LBL_VENDOR_RELATE',
                'enabled' => true,
                'id' => 'ACCOUNT_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              16 => 
              array (
                'name' => 'breed_c',
                'label' => 'LBL_BREED',
                'enabled' => true,
                'default' => true,
              ),
              17 => 
              array (
                'name' => 'usda_category_c',
                'label' => 'LBL_USDA_CATEGORY',
                'enabled' => true,
                'readonly' => 'true',
                'default' => true,
              ),
              18 => 
              array (
                'name' => 'enrollment_status',
                'label' => 'LBL_ENROLLMENT_STATUS',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
