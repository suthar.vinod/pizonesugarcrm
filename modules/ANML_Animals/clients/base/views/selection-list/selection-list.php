<?php
$module_name = 'ANML_Animals';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'usda_id_c',
                'label' => 'LBL_USDA_ID',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'assigned_to_wp_c',
                'label' => 'LBL_ASSIGNED_TO_WP',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
