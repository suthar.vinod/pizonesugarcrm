/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @extends View.Views.Base.RecordlistView
 */
({
    extendsFrom: 'RecordlistView',
    initialize: function (options) {
        this._super('initialize', [options]);
    },
    /* #1274 : 10 May 2021 : gsingh */
    /* override method addActions in the js controller of RecordListView  */
    addActions: function () {
        console.log('addActions');
        this._super("addActions");
    },
    /** * EOC #1274 : 10 May 2021 : gsingh */
    _bindEvents: function () {
        var self = this;

        self._super("_bindEvents");
        if (self.layout) {
            self.layout.on('list:wpemasscreaterecords:fire', self.wpemasscreaterecordsClicked, self);
            self.layout.on('list:rtmasscreaterecords:fire', self.rtmasscreaterecordsClicked, self);
        }
    },
    wpemasscreaterecordsClicked: function () {
        var self = this;
        var openDrawer = 1;
        var tsAssignedWPArr = [];
        var tsAllocatedWPArr = [];
        _.each(self.fields, function (field) {  //alert("dsfdd");
            if (field.$el.find('input[data-check="one"]:checked').length > 0) {

                var tsName = field.model.get('name');
                var tsAssignedWP = field.model.get('assigned_to_wp_c');
                var tsAllocatedWP = field.model.get('m03_work_product_id_c');
                var tsEnrollmentStatus = field.model.get('enrollment_status');
                var tsTerminationDate = field.model.get('termination_date_c');

                if (tsName == "") {
                    openDrawer = 0;
                }

                if (tsAssignedWP == "APS001-AH01") {
                    if (tsAllocatedWP != "") {
                        tsAssignedWPArr.push(tsAssignedWP);
                        tsAllocatedWPArr.push(tsAllocatedWP);
                        if (tsAllocatedWPArr.every((val, i, arr) => val === arr[0]) && tsAllocatedWPArr.length == tsAssignedWPArr.length) {
                            openDrawer = 1;
                        } else {
                            openDrawer = 0;
                        }
                    } else {
                        openDrawer = 1;
                        tsAssignedWPArr.push(tsAssignedWP);
                    }
                } else {
                    tsAssignedWPArr.push(tsAssignedWP);
                }

                if (tsTerminationDate != "") {
                    openDrawer = 0;
                }

                /* #1453 : 06-08-2021 : LCS */
                if (tsAssignedWP != "APS001-AH01" && tsAssignedWPArr.every((val, i, arr) => val === arr[0])) {
                    if (tsEnrollmentStatus == "On Study Transferred" || tsEnrollmentStatus == "Not Used") {
                        openDrawer = 1;
                    } else {
                        openDrawer = 0;
                    }

                } else if (tsAssignedWP != "APS001-AH01" && tsEnrollmentStatus != "Not Used") {
                    openDrawer = 0;
                }


            }
        });
        var uniqueArr = tsAssignedWPArr.filter(function (itm, i, tsAssignedWPArr) {
            return i == tsAssignedWPArr.indexOf(itm);
        });
        if (uniqueArr.length > 1) {
            openDrawer = 0;
        }

        if (openDrawer) {
            app.drawer.open({
                layout: 'wpemasscreate',
                context: {
                    create: true,
                    module: 'WPE_Work_Product_Enrollment',
                }
            });
        } else {
            return false;
        }
    },
    rtmasscreaterecordsClicked: function () {
        app.drawer.open({
            layout: 'rtmasscreate',
            context: {
                create: true,
                module: 'RT_Room_Transfer',
            }
        },

        );
    },

})