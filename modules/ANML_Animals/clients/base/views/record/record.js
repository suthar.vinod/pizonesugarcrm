({
    extendsFrom: "RecordView",
    initialize: function (options) {
        this._super("initialize", [options]);
        this.model.on("data:sync:complete", this.onload_function, this);
        this.context.on('button:edit_button:click', this.onload_function, this);
    },
    onload_function: function () {
        $("[track='click:actiondropdown']").removeClass("disabled");
        var self = this;
        var module_Id = this.model.get("id");
        if (module_Id != "") {
            var url = app.api.buildURL("getWeightId");
            var method = "create";
            var data = {
                module: "W_Weight",
                moduleId: module_Id,
            };
            var callback = {
                success: _.bind(function successCB(res) {
                    console.log('data', res);
                    if (res != '' && res != null) {
                        $('input[name="bodyweight_c"]').prop('disabled', true);
                        $('div[data-name="bodyweight_c"]').css('pointer-events', 'none');
                    } else {
                        $('input[name="bodyweight_c"]').prop('disabled', false);
                        $('div[data-name="bodyweight_c"]').css('pointer-events', 'unset');
                    }
                }, this),
            };
            app.api.call(method, url, data, callback);
        } else {
            $('input[name="bodyweight_c"]').prop('disabled', false);
            $('div[data-name="bodyweight_c"]').css('pointer-events', 'unset');
        }

        App.api.call("get", "rest/v10/ANML_Animals/" + module_Id + "?fields=first_procedure_date_c,termination_date_c", null, {
            success: function (data) {
                console.log("ANML_Animals ", data);
                if (data.first_procedure_date_c != "" && data.first_procedure_date_c != null) {
                    //console.log('first_procedure_date_c ');
                    self.model.set("first_procedure_date_c", data.first_procedure_date_c);
                }
                if (data.termination_date_c != "" && data.termination_date_c != null) {
                    //console.log('termination_date_c ');
                    self.model.set("termination_date_c", data.termination_date_c);
                }
            }
        });
    },
    /*#1274  : Allocated WP check pop up : 07 May 2021 */
    handleSave: function () {
        var self = this;
        var allocated_work_product_c = self.model.get("allocated_work_product_c");
        var TSid = self.model.get("id");
        var TS_API =
            "rest/v10/ANML_Animals/" + TSid + "?fields=allocated_work_product_c";

        App.api.call("get", TS_API, null, {
            success: function (WData) {
                var old_allocated_WP_value = WData.allocated_work_product_c;
                console.log("TS_API data : ", WData);
                console.log(
                    "TS_API data : allocated_work_product_c : ",
                    old_allocated_WP_value
                );
                if (
                    old_allocated_WP_value != "" &&
                    old_allocated_WP_value != allocated_work_product_c &&
                    allocated_work_product_c != ""
                ) {
                    app.alert.show("Allocated_WP_check_pop_up", {
                        level: "confirmation",
                        messages: "These animals are already allocated, are you sure you want to re-assign?",
                        autoClose: false,
                        confirm: {
                            label: "Yes",
                        },
                        cancel: {
                            label: "No",
                        },
                        onCancel: function () {
                            return;
                        },
                        onConfirm: function () {
                            self._super("handleSave");
                        },
                    });
                } else {
                    self._super("handleSave");
                }
            },
        });
    },
});