({
    extendsFrom: 'MassupdateView',
    initialize: function(options) {
        this._super('initialize', [options]);
    },
    saveClicked: function(evt) {
        if (this.defaultOption.name == 'allocated_work_product_c') {           
            /* #1274 : 10 May 2021 : gsingh */
            var new_allocated_wp_id = $('[name=allocated_work_product_c]').val();
            console.log('new_allocated_wp_id : ', new_allocated_wp_id);
            /** To get the name of new allocated wp */            
            var WP_API = "rest/v10/M03_Work_Product/" + new_allocated_wp_id + "?fields=name";
            var WP_Name_Data = [];
            //setTimeout(function() {
                App.api.call("get", WP_API, null, {
                    success: function(WP_Data) {
                        var new_allocated_wp_name = WP_Data.name;
                        console.log('WP_API data : name : ', new_allocated_wp_name);
                        WP_Name_Data.push(new_allocated_wp_name);
                        console.log('mass update allocated_work_product_c');                   
                    }
                 });
            //}, 1000);
            var allocated_wp_name = WP_Name_Data[0];           
            var wpArr = _.map(this.context.get('mass_collection').models, function(selected_model) { 
                        var old_allocated_wp = selected_model.get('allocated_work_product_c');
                        console.log('old allocate wp value : ', old_allocated_wp);
                        console.log('allocated_wp_name : ', allocated_wp_name);                       
                        if (selected_model.get('assigned_to_wp_c') == 'APS001-AH01') {

                            if (new_allocated_wp_id !='' && old_allocated_wp !='' && old_allocated_wp != allocated_wp_name && allocated_wp_name != '') {                              
                                return "ConfirmYes";
                            }
                            return "Yes";
                        } else {
                            return "No";
                        }
                
            });
            
        
             /* EOC #1274 : 10 May 2021 : gsingh */          
             if (wpArr.includes("No")) {
                app.alert.show("allocated_error", {
                    level: "error",
                    messages: "Please Select All Items whose Current Work Product Assignment is equal to APS001-AH01",

                });
            } else if (wpArr.includes("ConfirmYes")) {               
                var self = this;
                app.alert.show('Allocated_WP_check_pop_up', {
                    level: 'confirmation',
                    messages: 'These animals are already allocated, are you sure you want to re-assign?',
                    autoClose: false,
                    confirm: {
                        label: "Yes"
                    },
                    cancel: {
                        label: "No"
                    },
                    onCancel: function() {                       
                        return;
                    },
                    onConfirm: function() {
                        self._super('saveClicked', [evt]);
                    }
                });
            } 
            else {
                this._super('saveClicked', [evt]);
            }      

        } else {
            this._super('saveClicked', [evt]);
        }
    },
})