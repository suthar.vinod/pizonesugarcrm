<?php
// created: 2021-05-13 16:48:37
$viewdefs['ANML_Animals']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'abnormality_c' => 
    array (
    ),
    'allocated_work_product_c' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'booster_a1_c' => 
    array (
    ),
    'booster_a2_c' => 
    array (
    ),
    'booster_a3_c' => 
    array (
    ),
    'booster_a4_c' => 
    array (
    ),
    'booster_a5_c' => 
    array (
    ),
    'booster_a6_c' => 
    array (
    ),
    'booster_b1_c' => 
    array (
    ),
    'booster_b2_c' => 
    array (
    ),
    'booster_b3_c' => 
    array (
    ),
    'booster_b4_c' => 
    array (
    ),
    'booster_b5_c' => 
    array (
    ),
    'booster_b6_c' => 
    array (
    ),
    'booster_c1_c' => 
    array (
    ),
    'booster_c2_c' => 
    array (
    ),
    'booster_c3_c' => 
    array (
    ),
    'booster_c4_c' => 
    array (
    ),
    'booster_c5_c' => 
    array (
    ),
    'booster_c6_c' => 
    array (
    ),
    'breed_c' => 
    array (
    ),
    'confirmed_on_study_c' => 
    array (
    ),
    'assigned_to_wp_c' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_last_weighed_c' => 
    array (
    ),
    'date_of_birth_c' => 
    array (
    ),
    'date_received_c' => 
    array (
    ),
    'deceased_checkbox_c' => 
    array (
    ),
    'enrollment_status' => 
    array (
    ),
    'first_procedure_date_c' => 
    array (
    ),
    'initial_a_c' => 
    array (
    ),
    'initial_b_c' => 
    array (
    ),
    'initial_c' => 
    array (
    ),
    'comments_c' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'animal_room_c' => 
    array (
    ),
    'sex_c' => 
    array (
    ),
    'species_2_c' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'termination_date_c' => 
    array (
    ),
    'name' => 
    array (
    ),
    'animal_id_c' => 
    array (
    ),
    'usda_id_c' => 
    array (
    ),
    'bodyweight_c' => 
    array (
    ),
    'wpe_work_product_enrollment_anml_animals_1_name' => 
    array (
    ),
    'usda_category_c' => 
    array (
    ),
    'vendor_relate_c' => 
    array (
    ),
    'vaccine_a_c' => 
    array (
    ),
    'vaccine_b_c' => 
    array (
    ),
    'vaccine_c' => 
    array (
    ),
  ),
);