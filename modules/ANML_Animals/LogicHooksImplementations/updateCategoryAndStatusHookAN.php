<?php

class HookWPERelManagement {

    /**
     * 
      Animal Module
      -  We would like the USDA Category pulled from the WPE module; however, it needs to pull in the highest category (E>D>C>B) associated to that animal
      o    e.g. WPE on 4/10 has USDA category D, but WPE on 5/10 has USDA category C; the animal module would then show the USDA category as a D
      -  We would like the enrollment status in the animal module to mirror of the most recent, non-deleted (active) Enrollment Status in the Work Product Enrollment record associated to that animal
      o    e.g. WPE on 4/10 states Back-up Used, and WPE on 5/10 states �Non-na�ve Stock�; the enrollment status within the animal module for that animal would then state �non-na�ve stock�
     *
     */
    public function manageCategoryAndStatus($bean, $event, $arguments) {
        //  If its then update Status and Category
        if ($arguments['related_module'] == 'WPE_Work_Product_Enrollment' && $arguments['link'] == 'anml_animals_wpe_work_product_enrollment_1') {
            $values = $this->_getValues($bean, $arguments['related_id']);

            $categoryValue = $values['category'];
            $statusValue = $values['status'];
            //  Save parent Bean.
            if (!isset($bean->customSavingBean)) {
                if (!empty($categoryValue)) {
                    $bean->usda_category_c = $categoryValue;
                }
                if (!empty($statusValue)) {
                    $bean->enrollment_status = $statusValue;
                }

                $bean->customSavingBean = true;
                $bean->save();
            }
        }
    }

    private function _getValues(&$bean, $relatedId = NULL) {
        //  Fetch related Beans
        $bean->load_relationship('anml_animals_wpe_work_product_enrollment_1');
        $wpeRecords = $bean->anml_animals_wpe_work_product_enrollment_1->getBeans();

        $dm = '';
        $value = array();
        $value['category'] = '';
        $value['status'] = '';

        if (!array_key_exists($relatedId, $wpeRecords) || count($wpeRecords) == 0) {
            $wpe = BeanFactory::getBean('WPE_Work_Product_Enrollment', $relatedId, array('disable_row_level_security' => true));

            if (count($wpeRecords) == 0) {
                $wpeRecords = array();
            }

            $wpeRecords[$relatedId] = $wpe;
        }

        foreach ($wpeRecords as $wpeRecord) {
            //  Work for Category
            if (empty($value['category'])) {
                $value['category'] = ord($wpeRecord->usda_category_c);
            }

            $newValue = ord($wpeRecord->usda_category_c);
            if ($newValue > $value['category']) {
                $value['category'] = $newValue;
            }

            //  Work for Status
            if (empty($dm)) {
                $dm = $wpeRecord->date_modified;
            }

            if ($wpeRecord->date_modified >= $dm) {
                $status = $wpeRecord->enrollment_status_c;
                $value['status'] = $status;
            }
        }

        if (!empty($value['category'])) {
            $value['category'] = chr($value['category']);
        }

        return $value;
    }

}
