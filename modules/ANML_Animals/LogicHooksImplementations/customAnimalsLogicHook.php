<?php

class customAnimalsLogicHook {

    /**
     * Set the default Enrollment Status to Stock for Importing Records
     * @param type $bean
     * @param type $event
     * @param type $arguments
     */
    public function setStatustoStock($bean, $event, $arguments) {
        //It is a new record
        if ($bean->fetched_row['id'] == "") {
            $bean->assigned_to_wp_c     = 'APS001-AH01';
            $bean->enrollment_status    = 'On Study';
            $bean->usda_category_c      = 'B';
        }
    }
}