<?php

class updateRoomClass {
    /**
     * 
      If a room transfer record is unlinked or deleted the Test System Room field should update to the next relevant linked Room Transfer record
     *
     */
    public function updateRoom($bean, $event, $arguments) {
        global $timedate;
        if ($arguments['related_module'] == 'RT_Room_Transfer' && $arguments['link'] == 'anml_animals_rt_room_transfer_1') {
            $relatedBean = BeanFactory::getBean("RT_Room_Transfer", $arguments['related_id']);
            $convertedDate = $timedate->fromString($relatedBean->transfer_date_c);
            $newDbDate = $convertedDate->format("Y-m-d");

            $conn = $GLOBALS['db']->getConnection();
            $sql = "SELECT t.id, tc.rms_room_id_c, tc.transfer_date_c 
            FROM rt_room_transfer t 
            INNER JOIN rt_room_transfer_cstm tc ON t.id = tc.id_c AND t.deleted = 0 
            INNER JOIN anml_animals_rt_room_transfer_1_c tr ON t.id = tr.anml_animals_rt_room_transfer_1rt_room_transfer_idb AND tr.deleted = 0 AND tr.anml_animals_rt_room_transfer_1anml_animals_ida = '{$bean->id}' ORDER BY tc.transfer_date_c DESC, date_entered DESC LIMIT 1";
            $rslt = $conn->executeQuery($sql);
            $row = $rslt->fetch();

            if ((empty($row['transfer_date_c']) || $newDbDate >= $row['transfer_date_c']) && $event == "after_relationship_add") {
                $rms_room_id_c = $relatedBean->rms_room_id_c;
            }else{
                $rms_room_id_c = $row['rms_room_id_c'];
            }

            $conn->executeQuery("UPDATE anml_animals_cstm SET rms_room_id_c = '{$rms_room_id_c}' WHERE id_c = '{$bean->id}'");
        }
    }
}
