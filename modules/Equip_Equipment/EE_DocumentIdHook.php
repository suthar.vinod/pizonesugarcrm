<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class EE_DocumentIdHook {

    function UpdateEE_DocumentIdField($bean, $event, $arguments) {
        global $db;
        
        if ($bean->name != "" && $bean->erd_error_documents_id_c !='') {
            
            $sql = "SELECT * FROM `erd_error_documents_cstm` WHERE `id_c` = '" . $bean->erd_error_documents_id_c . "'";

            $result = $db->query($sql);
            if ($result->num_rows > 0) {
                $row = $db->fetchByAssoc($result);
                $doc_id_c = $row['doc_id_c'];   
                $bean->document_id_c = $doc_id_c;
            }
        }
    }

}

?>