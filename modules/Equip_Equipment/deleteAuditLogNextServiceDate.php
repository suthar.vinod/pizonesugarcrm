<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class deleteAuditLogNextServiceDate {

    function deleteNextServiceDate($bean, $event, $arguments) {
        global $db;

        $efId = $bean->id;

        $queryAuditLog = "SELECT id,count(*) as NUM FROM `equip_equipment_audit` WHERE `field_name`='next_service_date_c' AND `parent_id`='".$efId."' group by before_value_string,after_value_string,date(date_created)";
        $auditLogResult = $db->query($queryAuditLog);

        if($auditLogResult->num_rows > 0 ){
            while ($fetchAuditLog = $db->fetchByAssoc($auditLogResult)) {
                if($fetchAuditLog['NUM']>1){
                    $recordID = $fetchAuditLog['id'];
                    $sql_DeleteAudit = "DELETE from `equip_equipment_audit` WHERE `field_name`='next_service_date_c' AND `parent_id`='".$efId."' AND id = '".$recordID."'";
                    $db->query($sql_DeleteAudit);
                }				
            }
        } 
    }

}

?>