<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/LogicHooks/logic_hooks.php


$hook_array['before_save'][] = Array(1,'Set default values for status and software field','custom/modules/Equip_Equipment/customLogicHook.php','customLogicHook','setDefaultForStatus',);
$hook_array['before_save'][] = Array('1','workflow','include/workflow/WorkFlowHandler.php','WorkFlowHandler','WorkFlowHandler',);
$hook_array['after_relationship_add'][] = Array(
    1,
    '',
    'custom/modules/Equip_Equipment/customLogicHook.php',
    'customLogicHook',
    'populateLastServiceDate',
);
$hook_array['after_relationship_delete'][] = Array(
    1,
    '',
    'custom/modules/Equip_Equipment/customLogicHook.php',
    'customLogicHook',
    'populateLastServiceDate',
);

$hook_array['after_relationship_delete'][] = Array(
    2,
    'Update software fields in E&F and last service date in E&FS',
    'custom/modules/Equip_Equipment/customLogicHook.php',
    'customLogicHook',
    'updateSoftwareAndFirmware',);

?>
