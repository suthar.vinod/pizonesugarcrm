<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/WirelessLayoutdefs/m06_error_equip_equipment_1_Equip_Equipment.php

 // created: 2018-01-31 16:07:58
$layout_defs["Equip_Equipment"]["subpanel_setup"]['m06_error_equip_equipment_1'] = array (
  'order' => 100,
  'module' => 'M06_Error',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_M06_ERROR_TITLE',
  'get_subpanel_data' => 'm06_error_equip_equipment_1',
);

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/WirelessLayoutdefs/equip_equipment_efd_equipment_facility_doc_1_Equip_Equipment.php

 // created: 2019-02-19 17:46:13
$layout_defs["Equip_Equipment"]["subpanel_setup"]['equip_equipment_efd_equipment_facility_doc_1'] = array (
  'order' => 100,
  'module' => 'EFD_Equipment_Facility_Doc',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EFD_EQUIPMENT_FACILITY_DOC_TITLE',
  'get_subpanel_data' => 'equip_equipment_efd_equipment_facility_doc_1',
);

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/WirelessLayoutdefs/equip_equipment_efr_equipment_facility_recor_2_Equip_Equipment.php

 // created: 2019-02-20 15:09:02
$layout_defs["Equip_Equipment"]["subpanel_setup"]['equip_equipment_efr_equipment_facility_recor_2'] = array (
  'order' => 100,
  'module' => 'EFR_Equipment_Facility_Recor',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE',
  'get_subpanel_data' => 'equip_equipment_efr_equipment_facility_recor_2',
);

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/WirelessLayoutdefs/equip_equipment_efs_equipment_facility_servi_1_Equip_Equipment.php

 // created: 2019-02-25 15:17:34
$layout_defs["Equip_Equipment"]["subpanel_setup"]['equip_equipment_efs_equipment_facility_servi_1'] = array (
  'order' => 100,
  'module' => 'EFS_Equipment_Facility_Servi',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE',
  'get_subpanel_data' => 'equip_equipment_efs_equipment_facility_servi_1',
);

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/WirelessLayoutdefs/equip_equipment_sv_service_vendor_1_Equip_Equipment.php

 // created: 2020-01-15 13:01:13
$layout_defs["Equip_Equipment"]["subpanel_setup"]['equip_equipment_sv_service_vendor_1'] = array (
  'order' => 100,
  'module' => 'SV_Service_Vendor',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_EQUIP_EQUIPMENT_SV_SERVICE_VENDOR_1_FROM_SV_SERVICE_VENDOR_TITLE',
  'get_subpanel_data' => 'equip_equipment_sv_service_vendor_1',
);

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/WirelessLayoutdefs/accounts_equip_equipment_1_Equip_Equipment.php

 // created: 2020-11-03 06:37:20
$layout_defs["Equip_Equipment"]["subpanel_setup"]['accounts_equip_equipment_1'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'accounts_equip_equipment_1',
);

?>
