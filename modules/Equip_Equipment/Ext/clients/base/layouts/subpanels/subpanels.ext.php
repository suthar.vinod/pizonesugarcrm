<?php
// WARNING: The contents of this file are auto-generated.


// created: 2020-11-03 06:37:20
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_ACCOUNTS_TITLE',
  'context' => 
  array (
    'link' => 'accounts_equip_equipment_1',
  ),
);

// created: 2019-02-19 17:46:13
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EFD_EQUIPMENT_FACILITY_DOC_TITLE',
    'context' =>
        array(
            'link' => 'equip_equipment_efd_equipment_facility_doc_1',
        ),
    'override_subpanel_list_view' => 'subpanel-for-equipment-facility-doc',
    'override_paneltop_view' => 'panel-top-for-equipment-facility-doc'
);

// created: 2019-02-20 15:09:02
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE',
  'context' => 
  array (
    'link' => 'equip_equipment_efr_equipment_facility_recor_2',
  ),
);

// created: 2019-02-25 15:17:34
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE',
  'context' => 
  array (
    'link' => 'equip_equipment_efs_equipment_facility_servi_1',
  ),
);

// created: 2020-01-15 13:01:13
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_EQUIP_EQUIPMENT_SV_SERVICE_VENDOR_1_FROM_SV_SERVICE_VENDOR_TITLE',
  'context' => 
  array (
    'link' => 'equip_equipment_sv_service_vendor_1',
  ),
);

// created: 2018-01-31 16:07:58
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_M06_ERROR_TITLE',
  'context' => 
  array (
    'link' => 'm06_error_equip_equipment_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'accounts_equip_equipment_1',
  'view' => 'subpanel-for-equip_equipment-accounts_equip_equipment_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'equip_equipment_efd_equipment_facility_doc_1',
  'view' => 'subpanel-for-equip_equipment-equip_equipment_efd_equipment_facility_doc_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'equip_equipment_efr_equipment_facility_recor_2',
  'view' => 'subpanel-for-equip_equipment-equip_equipment_efr_equipment_facility_recor_2',
);


//auto-generated file DO NOT EDIT
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'equip_equipment_efs_equipment_facility_servi_1',
  'view' => 'subpanel-for-equip_equipment-equip_equipment_efs_equipment_facility_servi_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'equip_equipment_sv_service_vendor_1',
  'view' => 'subpanel-for-equip_equipment-equip_equipment_sv_service_vendor_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Equip_Equipment']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'm06_error_equip_equipment_1',
  'view' => 'subpanel-for-equip_equipment-m06_error_equip_equipment_1',
);
