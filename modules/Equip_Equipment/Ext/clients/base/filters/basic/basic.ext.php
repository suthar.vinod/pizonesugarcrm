<?php
// WARNING: The contents of this file are auto-generated.


$viewdefs['Equip_Equipment']['base']['filter']['basic']['filters'][] = array(
    'id' => 'FilterEquipmentTemplateLocation',
    'name' => 'Filter Location',
    'filter_definition' => array(
        array(
            'building_c' => array(
                '$in' => '',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);




$viewdefs['Equip_Equipment']['base']['filter']['basic']['filters'][] = array(
    'id' => 'filterEquipmentTemplate',
    'name' => 'LBL_FILTER_EQUIPMENT_TEMPLATE',
    'filter_definition' => array(
        array(
            'name' => array(
                '$contains' => 'Scale',
            ),
        )
    ),
    'editable' => true,
    'is_template' => true,
);