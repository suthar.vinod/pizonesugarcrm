<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/m06_error_equip_equipment_1_Equip_Equipment.php

// created: 2018-01-31 16:07:58
$dictionary["Equip_Equipment"]["fields"]["m06_error_equip_equipment_1"] = array (
  'name' => 'm06_error_equip_equipment_1',
  'type' => 'link',
  'relationship' => 'm06_error_equip_equipment_1',
  'source' => 'non-db',
  'module' => 'M06_Error',
  'bean_name' => 'M06_Error',
  'vname' => 'LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_M06_ERROR_TITLE',
  'id_name' => 'm06_error_equip_equipment_1m06_error_ida',
);

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/equip_equipment_efd_equipment_facility_doc_1_Equip_Equipment.php

// created: 2019-02-19 17:46:13
$dictionary["Equip_Equipment"]["fields"]["equip_equipment_efd_equipment_facility_doc_1"] = array (
  'name' => 'equip_equipment_efd_equipment_facility_doc_1',
  'type' => 'link',
  'relationship' => 'equip_equipment_efd_equipment_facility_doc_1',
  'source' => 'non-db',
  'module' => 'EFD_Equipment_Facility_Doc',
  'bean_name' => 'EFD_Equipment_Facility_Doc',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'id_name' => 'equip_equipment_efd_equipment_facility_doc_1equip_equipment_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_serial_number_c.php

 // created: 2019-02-20 14:22:23
$dictionary['Equip_Equipment']['fields']['serial_number_c']['labelValue']='Serial Number';
$dictionary['Equip_Equipment']['fields']['serial_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['serial_number_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['serial_number_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_room_c.php

 // created: 2019-02-22 15:56:28
$dictionary['Equip_Equipment']['fields']['room_c']['labelValue']='Room';
$dictionary['Equip_Equipment']['fields']['room_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_rms_room_id_c.php

 // created: 2019-02-22 15:56:28

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_notes_c.php

 // created: 2019-02-20 15:00:34
$dictionary['Equip_Equipment']['fields']['notes_c']['labelValue']='Notes';
$dictionary['Equip_Equipment']['fields']['notes_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['notes_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['notes_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_name.php

 // created: 2019-02-20 14:13:23
$dictionary['Equip_Equipment']['fields']['name']['len']='255';
$dictionary['Equip_Equipment']['fields']['name']['audited']=true;
$dictionary['Equip_Equipment']['fields']['name']['massupdate']=false;
$dictionary['Equip_Equipment']['fields']['name']['unified_search']=false;
$dictionary['Equip_Equipment']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.0',
  'searchable' => true,
);
$dictionary['Equip_Equipment']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_model_c.php

 // created: 2019-02-20 14:21:43
$dictionary['Equip_Equipment']['fields']['model_c']['labelValue']='Model';
$dictionary['Equip_Equipment']['fields']['model_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['model_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['model_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_manufacturer_c.php

 // created: 2019-02-20 14:20:58
$dictionary['Equip_Equipment']['fields']['manufacturer_c']['labelValue']='Manufacturer';
$dictionary['Equip_Equipment']['fields']['manufacturer_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_erd_error_documents_id_c.php

 // created: 2019-02-20 14:41:56

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_erd_error_documents_id1_c.php

 // created: 2019-02-20 14:46:20

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_department_c.php

 // created: 2019-02-20 14:35:20
$dictionary['Equip_Equipment']['fields']['department_c']['labelValue']='Department';
$dictionary['Equip_Equipment']['fields']['department_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['department_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_applicable_sop_c.php

 // created: 2019-02-20 14:41:56
$dictionary['Equip_Equipment']['fields']['applicable_sop_c']['labelValue']='Applicable SOP';
$dictionary['Equip_Equipment']['fields']['applicable_sop_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_applicable_form_c.php

 // created: 2019-02-20 14:46:20
$dictionary['Equip_Equipment']['fields']['applicable_form_c']['labelValue']='Applicable Form';
$dictionary['Equip_Equipment']['fields']['applicable_form_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_account_id_c.php

 // created: 2019-02-20 14:14:07

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_account_id1_c.php

 // created: 2019-02-20 14:20:58

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/equip_equipment_efs_equipment_facility_servi_1_Equip_Equipment.php

// created: 2019-02-25 15:17:34
$dictionary["Equip_Equipment"]["fields"]["equip_equipment_efs_equipment_facility_servi_1"] = array (
  'name' => 'equip_equipment_efs_equipment_facility_servi_1',
  'type' => 'link',
  'relationship' => 'equip_equipment_efs_equipment_facility_servi_1',
  'source' => 'non-db',
  'module' => 'EFS_Equipment_Facility_Servi',
  'bean_name' => 'EFS_Equipment_Facility_Servi',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'id_name' => 'equip_equia9d9uipment_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/equip_equipment_efr_equipment_facility_recor_2_Equip_Equipment.php

// created: 2019-02-20 15:09:02
$dictionary["Equip_Equipment"]["fields"]["equip_equipment_efr_equipment_facility_recor_2"] = array (
  'name' => 'equip_equipment_efr_equipment_facility_recor_2',
  'type' => 'link',
  'relationship' => 'equip_equipment_efr_equipment_facility_recor_2',
  'source' => 'non-db',
  'module' => 'EFR_Equipment_Facility_Recor',
  'bean_name' => 'EFR_Equipment_Facility_Recor',
  'vname' => 'LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EQUIP_EQUIPMENT_TITLE',
  'id_name' => 'equip_equi01e2uipment_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_current_software_firmware_c.php

 // created: 2019-02-20 14:23:10
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['labelValue']='Current Software/Firmware';
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['type']= 'varchar';
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['required']=false;
$dictionary['Equip_Equipment']['fields']['current_software_firmware_c']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_owner_c.php

 // created: 2019-06-18 15:18:04
$dictionary['Equip_Equipment']['fields']['owner_c']['labelValue']='Owner';
$dictionary['Equip_Equipment']['fields']['owner_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_classification_c.php

 // created: 2019-06-18 15:18:56
$dictionary['Equip_Equipment']['fields']['classification_c']['labelValue']='Classification';
$dictionary['Equip_Equipment']['fields']['classification_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['classification_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_building_c.php

 // created: 2019-06-18 15:21:10
$dictionary['Equip_Equipment']['fields']['building_c']['labelValue']='Building';
$dictionary['Equip_Equipment']['fields']['building_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['building_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_is_status_retired_c.php

 // created: 2019-07-10 07:14:24
$dictionary['Equip_Equipment']['fields']['is_status_retired_c']['labelValue']='is status retired';
$dictionary['Equip_Equipment']['fields']['is_status_retired_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['is_status_retired_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['is_status_retired_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_classification_check_c.php

 // created: 2019-09-16 07:34:48
$dictionary['Equip_Equipment']['fields']['classification_check_c']['labelValue']='Classification Check';
$dictionary['Equip_Equipment']['fields']['classification_check_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['classification_check_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['classification_check_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_binder_location_c.php

 // created: 2019-09-26 12:00:09
$dictionary['Equip_Equipment']['fields']['binder_location_c']['labelValue']='Binder Location';
$dictionary['Equip_Equipment']['fields']['binder_location_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['binder_location_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_last_service_date_c.php

 // created: 2019-02-28 12:54:29
$dictionary['Equip_Equipment']['fields']['last_service_date_c']['labelValue']='Last Service Date';
$dictionary['Equip_Equipment']['fields']['last_service_date_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['last_service_date_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['last_service_date_c']['readonly']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/equip_equipment_sv_service_vendor_1_Equip_Equipment.php

// created: 2020-01-15 13:01:13
$dictionary["Equip_Equipment"]["fields"]["equip_equipment_sv_service_vendor_1"] = array (
  'name' => 'equip_equipment_sv_service_vendor_1',
  'type' => 'link',
  'relationship' => 'equip_equipment_sv_service_vendor_1',
  'source' => 'non-db',
  'module' => 'SV_Service_Vendor',
  'bean_name' => 'SV_Service_Vendor',
  'vname' => 'LBL_EQUIP_EQUIPMENT_SV_SERVICE_VENDOR_1_FROM_EQUIP_EQUIPMENT_TITLE',
  'id_name' => 'equip_equipment_sv_service_vendor_1equip_equipment_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_document_id_c.php

 // created: 2020-03-09 03:39:00
$dictionary['Equip_Equipment']['fields']['document_id_c']['labelValue']='Document Id';
$dictionary['Equip_Equipment']['fields']['document_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['document_id_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['document_id_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_iso_17025_certification_c.php

 // created: 2019-02-20 14:55:37
$dictionary['Equip_Equipment']['fields']['iso_17025_certification_c']['labelValue']='ISO 17025 Certification Required?';
$dictionary['Equip_Equipment']['fields']['iso_17025_certification_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['iso_17025_certification_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_out_of_service_date_c.php

 // created: 2020-10-06 06:40:54
$dictionary['Equip_Equipment']['fields']['out_of_service_date_c']['labelValue']='Out of Service Date';
$dictionary['Equip_Equipment']['fields']['out_of_service_date_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['out_of_service_date_c']['dependency']='isInList($status_c,createList("Inactive","Out for Service","Retired"))';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_iso_17025_units_of_measure_c.php

 // created: 2020-10-29 09:06:43
$dictionary['Equip_Equipment']['fields']['iso_17025_units_of_measure_c']['labelValue']='ISO 17025 Units of Measure';
$dictionary['Equip_Equipment']['fields']['iso_17025_units_of_measure_c']['dependency']='equal($iso_17025_certification_c,"Yes")';
$dictionary['Equip_Equipment']['fields']['iso_17025_units_of_measure_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/accounts_equip_equipment_1_Equip_Equipment.php

// created: 2020-11-03 06:37:20
$dictionary["Equip_Equipment"]["fields"]["accounts_equip_equipment_1"] = array (
  'name' => 'accounts_equip_equipment_1',
  'type' => 'link',
  'relationship' => 'accounts_equip_equipment_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_equip_equipment_1accounts_ida',
);

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_status_c.php

 // created: 2020-12-02 14:50:37
$dictionary['Equip_Equipment']['fields']['status_c']['labelValue']='Status';
$dictionary['Equip_Equipment']['fields']['status_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['status_c']['required_formula']='';
$dictionary['Equip_Equipment']['fields']['status_c']['visibility_grid']=array (
  'trigger' => 'classification_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Analytical Services' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Business Development' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Facilities' => 
    array (
      0 => '',
      1 => 'Active',
      2 => 'Inactive',
    ),
    'Finance' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Histology Services' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Human_Resources' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'In life Large Animal Care' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'In life Large Animal Research' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'In life Small Animal Care' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'In life Small Animal Research' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Information_Technology' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Interventional Surgical Research' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Lab Services' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Operations Support' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Pathology Services' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Pharmacology' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Process Improvement' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Quality Assurance Unit' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Regulatory Services' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Scientific' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Software Development' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Toxicology' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Veterinary Services' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Inlife Large Animal prepRecovery' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Inactive',
      4 => 'Out for Service',
      5 => 'Quarantined',
      6 => 'Retired',
    ),
    'Data Generator With Electronic Records' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Out for Service',
      4 => 'Quarantined',
      5 => 'Retired',
      6 => 'Inactive',
    ),
    'Data Generator Without Electronic Records' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Out for Service',
      4 => 'Quarantined',
      5 => 'Retired',
      6 => 'Inactive',
    ),
    'Facility Task' => 
    array (
      0 => '',
      1 => 'Active',
      2 => 'Retired',
      3 => 'Inactive',
    ),
    'Non Data Generator' => 
    array (
      0 => '',
      1 => 'Active GLP compliant',
      2 => 'Active GLP non compliant',
      3 => 'Out for Service',
      4 => 'Quarantined',
      5 => 'Retired',
      6 => 'Inactive',
    ),
    'Non GLP Equipment' => 
    array (
      0 => '',
      1 => 'Active GLP non compliant',
      2 => 'Out for Service',
      3 => 'Quarantined',
      4 => 'Retired',
      5 => 'Inactive',
    ),
  ),
);
 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_vendor_c.php

 // created: 2020-12-08 09:27:02
$dictionary['Equip_Equipment']['fields']['vendor_c']['labelValue']='Vendor';
$dictionary['Equip_Equipment']['fields']['vendor_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['vendor_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['vendor_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['vendor_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_po_c.php

 // created: 2020-12-08 09:27:45
$dictionary['Equip_Equipment']['fields']['po_c']['labelValue']='PO';
$dictionary['Equip_Equipment']['fields']['po_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['po_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['po_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['po_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_expense_or_capitalize_c.php

 // created: 2020-12-08 09:30:14
$dictionary['Equip_Equipment']['fields']['expense_or_capitalize_c']['labelValue']='Expense or Capitalize?';
$dictionary['Equip_Equipment']['fields']['expense_or_capitalize_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['expense_or_capitalize_c']['required_formula']='';
$dictionary['Equip_Equipment']['fields']['expense_or_capitalize_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_asset_id_c.php

 // created: 2020-12-08 09:31:29
$dictionary['Equip_Equipment']['fields']['asset_id_c']['labelValue']='Asset ID';
$dictionary['Equip_Equipment']['fields']['asset_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['asset_id_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['asset_id_c']['dependency']='equal($expense_or_capitalize_c,"Capitalize")';
$dictionary['Equip_Equipment']['fields']['asset_id_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_disposition_status_c.php

 // created: 2020-12-08 09:32:50
$dictionary['Equip_Equipment']['fields']['disposition_status_c']['labelValue']='Disposition Status';
$dictionary['Equip_Equipment']['fields']['disposition_status_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Equip_Equipment']['fields']['disposition_status_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['disposition_status_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['disposition_status_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_manufacturer_eol_c.php

 // created: 2021-02-25 05:50:13
$dictionary['Equip_Equipment']['fields']['manufacturer_eol_c']['labelValue']='Manufacturer EOL';
$dictionary['Equip_Equipment']['fields']['manufacturer_eol_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['manufacturer_eol_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['manufacturer_eol_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_aps_planned_eol_c.php

 // created: 2021-02-25 05:51:32
$dictionary['Equip_Equipment']['fields']['aps_planned_eol_c']['labelValue']='APS Planned EOL';
$dictionary['Equip_Equipment']['fields']['aps_planned_eol_c']['enforced']='';
$dictionary['Equip_Equipment']['fields']['aps_planned_eol_c']['dependency']='';
$dictionary['Equip_Equipment']['fields']['aps_planned_eol_c']['required_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Vardefs/sugarfield_next_service_date_c.php

 // created: 2021-04-06 05:59:19
$dictionary['Equip_Equipment']['fields']['next_service_date_c']['labelValue']='Next Service Date';
$dictionary['Equip_Equipment']['fields']['next_service_date_c']['formula']='addDays($last_service_date_c,$service_interval_days_c)';
$dictionary['Equip_Equipment']['fields']['next_service_date_c']['enforced']='false';
$dictionary['Equip_Equipment']['fields']['next_service_date_c']['dependency']='greaterThan(strlen(toString($next_service_date_c)),0)';
$dictionary['Equip_Equipment']['fields']['next_service_date_c']['required_formula']='';
$dictionary['Equip_Equipment']['fields']['next_service_date_c']['readonly_formula']='';

 
?>
