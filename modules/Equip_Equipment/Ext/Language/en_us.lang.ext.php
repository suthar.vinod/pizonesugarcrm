<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Language/en_us.customm06_error_equip_equipment_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Language/en_us.customm06_error_equip_equipment_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_M06_ERROR_TITLE'] = 'Errors';
$mod_strings['LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Errors';

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_M06_ERROR_TITLE'] = 'Errors';
$mod_strings['LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Errors';


?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Language/en_us.filterEquipmentTemplate.php


$mod_strings['LBL_FILTER_EQUIPMENT_TEMPLATE'] = 'Scale Equipment and Facilities';
?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Language/en_us.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EQUIPMENT_NAME'] = 'Equipment Name';
$mod_strings['LBL_SERIAL_NUMBER'] = 'Serial Number';
$mod_strings['LBL_RECORD_BODY'] = 'General Info';
$mod_strings['LNK_NEW_RECORD'] = 'Create Equipment &amp; Facility';
$mod_strings['LNK_LIST'] = 'View Equipment &amp; Facilities';
$mod_strings['LBL_MODULE_NAME'] = 'Equipment &amp; Facilities';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Equipment &amp; Facility';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Equipment &amp; Facility';
$mod_strings['LNK_IMPORT_VCARD'] = 'Import Equipment &amp; Facility vCard';
$mod_strings['LNK_IMPORT_EQUIP_EQUIPMENT'] = 'Import Equipment &amp; Facilities';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Equipment &amp; Facility List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Search Equipment &amp; Facility';
$mod_strings['LBL_EQUIP_EQUIPMENT_SUBPANEL_TITLE'] = 'Equipment &amp; Facilities';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'My Equipment &amp; Facility &amp; Facilities';
$mod_strings['LBL_M06_ERROR_EQUIP_EQUIPMENT_1_FROM_M06_ERROR_TITLE'] = 'Communications';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EFD_EQUIPMENT_FACILITY_DOC_TITLE'] = 'Equipment &amp; Facility Documents';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment &amp; Facility Records';
$mod_strings['LBL_OWNER_ACCOUNT_ID'] = 'Owner (related Company ID)';
$mod_strings['LBL_OWNER'] = 'Owner';
$mod_strings['LBL_CLASSIFICATION'] = 'Classification';
$mod_strings['LBL_CURRENT_STATUS'] = 'Status (Obsolete)';
$mod_strings['LBL_MANUFACTURER_ACCOUNT_ID'] = 'Manufacturer (related Company ID)';
$mod_strings['LBL_MANUFACTURER'] = 'Manufacturer';
$mod_strings['LBL_MODEL'] = 'Model';
$mod_strings['LBL_CURRENT_SOFTWARE_FIRMWARE'] = 'Current Software/Firmware';
$mod_strings['LBL_DEPARTMENT'] = 'Department';
$mod_strings['LBL_BINDER_LOCATION'] = 'Binder Location';
$mod_strings['LBL_APPLICABLE_SOP_ERD_ERROR_DOCUMENTS_ID'] = 'Applicable SOP (related Error Documents ID)';
$mod_strings['LBL_APPLICABLE_SOP'] = 'Applicable SOP';
$mod_strings['LBL_APPLICABLE_FORM_ERD_ERROR_DOCUMENTS_ID'] = 'Applicable Form (related Controlled Document ID)';
$mod_strings['LBL_APPLICABLE_FORM'] = 'Applicable Form';
$mod_strings['LBL_BUILDING'] = 'Building';
$mod_strings['LBL_ROOM'] = 'Room';
$mod_strings['LBL_SERVICE_INTERVAL_DAYS'] = 'Service Interval (Days)';
$mod_strings['LBL_LAST_SERVICE_DATE'] = 'Last Service Date';
$mod_strings['LBL_NEXT_SERVICE_DATE'] = 'Next Service Date';
$mod_strings['LBL_ISO_17025_CERTIFICATION_REQUIRED'] = 'ISO 17025 Certification Required?';
$mod_strings['LBL_ISO_17025_UNITS_OF_MEASURE'] = 'ISO 17025 Units of Measure';
$mod_strings['LBL_NOTES'] = 'Notes';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Location';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Service Info';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'ISO 17025 Info';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Notes';
$mod_strings['LBL_ROOM_RMS_ROOM_ID'] = 'Room (related Room ID)';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment &amp; Facility Records';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE'] = 'Equipment &amp; Facility Services';
$mod_strings['LBL_IS_STATUS_RETIRED'] = 'is status retired';
$mod_strings['LBL_QUALIFICATION_REQUIRED'] = 'Qualification Required (Obsolete)';
$mod_strings['LBL_CLASSIFICATION_CHECK'] = 'Classification Check';
$mod_strings['LBL_DOCUMENT_ID_C'] = 'Document Id';
$mod_strings['LBL_STATUS'] = 'Status';
$mod_strings['LBL_OUT_OF_SERVICE_DATE'] = 'Out of Service Date';
$mod_strings['LBL_VENDOR'] = 'Vendor';
$mod_strings['LBL_PO'] = 'PO';
$mod_strings['LBL_EXPENSE_OR_CAPITALIZE'] = 'Expense or Capitalize?';
$mod_strings['LBL_ASSET_ID'] = 'Asset ID';
$mod_strings['LBL_DISPOSITION_STATUS'] = 'Disposition Status';
$mod_strings['LBL_RECORDVIEW_PANEL6'] = 'Finance';
$mod_strings['LBL_STATUS_C'] = 'Status';
$mod_strings['LBL_MANUFACTURER_EOL'] = 'Manufacturer EOL';
$mod_strings['LBL_APS_PLANNED_EOL'] = 'APS Planned EOL';
$mod_strings['LBL_REASON_FOR_CHANGE'] = 'Reason For Change';
$mod_strings['LBL_RECORDVIEW_PANEL5'] = 'Finance';
$mod_strings['LBL_EQUIP_EQUIPMENT_FOCUS_DRAWER_DASHBOARD'] = 'Equipment &amp; Facilities Focus Drawer';
$mod_strings['LBL_EQUIP_EQUIPMENT_RECORD_DASHBOARD'] = 'Equipment &amp; Facilities Record Dashboard';

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Language/en_us.customequip_equipment_accounts_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EQUIP_EQUIPMENT_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Companies';
$mod_strings['LBL_EQUIP_EQUIPMENT_ACCOUNTS_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Companies';

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Language/en_us.customequip_equipment_efd_equipment_facility_doc_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EFD_EQUIPMENT_FACILITY_DOC_TITLE'] = 'Equipment & Facility Documents';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFD_EQUIPMENT_FACILITY_DOC_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment & Facility Documents';

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Language/en_us.customequip_equipment_efr_equipment_facility_recor_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment & Facility Records';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment & Facility Records';

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Language/en_us.customequip_equipment_efr_equipment_facility_recor_2.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EFR_EQUIPMENT_FACILITY_RECOR_TITLE'] = 'Equipment &amp; Facility Records';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFR_EQUIPMENT_FACILITY_RECOR_2_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment &amp; Facility Records';

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Language/en_us.customequip_equipment_efs_equipment_facility_servi_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EFS_EQUIPMENT_FACILITY_SERVI_TITLE'] = 'Equipment & Facility Services';
$mod_strings['LBL_EQUIP_EQUIPMENT_EFS_EQUIPMENT_FACILITY_SERVI_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Equipment & Facility Services';

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Language/en_us.customequip_equipment_sv_service_vendor_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EQUIP_EQUIPMENT_SV_SERVICE_VENDOR_1_FROM_SV_SERVICE_VENDOR_TITLE'] = 'Service Vendors';
$mod_strings['LBL_EQUIP_EQUIPMENT_SV_SERVICE_VENDOR_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Service Vendors';

?>
<?php
// Merged from custom/Extension/modules/Equip_Equipment/Ext/Language/en_us.customaccounts_equip_equipment_1.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_ACCOUNTS_TITLE'] = 'Service Vendor';
$mod_strings['LBL_ACCOUNTS_EQUIP_EQUIPMENT_1_FROM_EQUIP_EQUIPMENT_TITLE'] = 'Service Vendor';

?>
