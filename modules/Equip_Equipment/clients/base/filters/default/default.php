<?php
// created: 2021-02-22 13:55:00
$viewdefs['Equip_Equipment']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'applicable_form_c' => 
    array (
    ),
    'applicable_sop_c' => 
    array (
    ),
    'aps_planned_eol_c' => 
    array (
    ),
    'asset_id_c' => 
    array (
    ),
    'binder_location_c' => 
    array (
    ),
    'building_c' => 
    array (
    ),
    'classification_c' => 
    array (
    ),
    'current_software_firmware_c' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'department_c' => 
    array (
    ),
    'disposition_status_c' => 
    array (
    ),
    'expense_or_capitalize_c' => 
    array (
    ),
    'iso_17025_certification_c' => 
    array (
    ),
    'iso_17025_units_of_measure_c' => 
    array (
    ),
    'last_service_date_c' => 
    array (
    ),
    'manufacturer_c' => 
    array (
    ),
    'manufacturer_eol_c' => 
    array (
    ),
    'model_c' => 
    array (
    ),
    'name' => 
    array (
    ),
    'next_service_date_c' => 
    array (
    ),
    'out_of_service_date_c' => 
    array (
    ),
    'owner_c' => 
    array (
    ),
    'po_c' => 
    array (
    ),
    'room_c' => 
    array (
    ),
    'serial_number_c' => 
    array (
    ),
    'status_c' => 
    array (
    ),
    'vendor_c' => 
    array (
    ),
  ),
);