({
    extendsFrom: 'RelateField',
    initialize: function (options) {
        this._super('initialize', [options]);

    },
    openSelectDrawer: function () {
         
        if (this.getSearchModule() == "RMS_Room" && this.model.get('building_c') != "" && this.model.get('building_c') != null) {
            this.selectRooms();
            $("button.btn.btn-invisible.btn-dark").css("display", "none");
            $(".filter-definition-container").css('pointer-events', 'none');
            $(".choice-filter-clickable").css('pointer-events', 'none');
        } else {
            this._super('openSelectDrawer');
            $("button.btn.btn-invisible.btn-dark").css("display", "block");
            $(".filter-definition-container").css('pointer-events', 'unset');
            $(".choice-filter-clickable").css('pointer-events', 'unset');

        }
       
    },
	
	selectRooms: function () {
        var building_c = this.model.get('building_c');
        var filterOptions = new app.utils.FilterOptions()
                .config({
                    'initial_filter': 'FilterRMS_RoomTemplate',
                    'initial_filter_label': 'Building',
                    'filter_populate': {
                        'building_c': building_c,
                    },
                    'filter_relate': {
                        'building_c': building_c,
                    },
                })
                .populateRelate(this.model)
                .format();
        //this custom code will effect for all relate fields in Enrollment module.But we need initial filter only for Equip_Equipment relate field.
        filterOptions = (this.getSearchModule() == "RMS_Room") ? filterOptions : this.getFilterOptions();
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                fields: this.getSearchFields(),
                filterOptions: filterOptions,
            }
        }, _.bind(this.setValue, this));
    },
	  /*To disable search text box filter of Test System */
    search: _.debounce(function (query) {
        var term = query.term || '',
                self = this,
                searchModule = this.getSearchModule(),
                params = {},
                limit = self.def.limit || 5,
                relatedModuleField = this.getRelatedModuleField();

        if (query.context) {
            params.offset = this.searchCollection.next_offset;
        }

        //console.log(params);
        params.filter = this.buildFilterDefinition(term);

        this.searchCollection.fetch({
            //Don't show alerts for this request
            showAlerts: false,
            update: true,
            remove: _.isUndefined(params.offset),
            fields: this.getSearchFields(),
            context: self,
            params: params,
            //limit: limit,
            success: function (data) {
                var fetch = {results: [], more: data.next_offset > 0, context: data};
                if (fetch.more) {
                    var fieldEl = self.$(self.fieldTag),
                            //For teamset widget, we should specify which index element to be filled in
                            plugin = (fieldEl.length > 1) ? $(fieldEl.get(self.currentIndex)).data("select2") : fieldEl.data("select2"),
                            height = plugin.searchmore.children("li:first").children(":first").outerHeight(),
                            //0.2 makes scroll not to touch the bottom line which avoid fetching next record set
                            maxHeight = height * (limit - .2);
                    plugin.results.css("max-height", maxHeight);
                }
                _.each(data.models, function (model, index) {
                    if (params.offset && index < params.offset) {
                        return;
                    }
                    fetch.results.push({
                        id: model.id,
                        text: model.get(relatedModuleField) + ''
                    });
                });
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback(fetch);
                }
            },
            error: function () {
                if (query.callback && _.isFunction(query.callback)) {
                    query.callback({results: []});
                }
                app.logger.error("Unable to fetch the bean collection.");
            }
        });

    }, app.config.requiredElapsed || 500),
   
	_getCustomFilterRoom: function () {
        var building_c = this.model.get('building_c');
        return [{
                '$or': [
                    {'building_c': building_c}
                ]
            }];
    },
    /**
     * @override
     */
    buildFilterDefinition: function (searchTerm) {
        var parentFilter = this._super('buildFilterDefinition', [searchTerm]);
        if (this.getSearchModule() == 'RMS_Room' && this.model.get('building_c') != "" && this.model.get('building_c') != null) {
            return parentFilter.concat(this._getCustomFilterRoom());
        } else {
            return parentFilter;
        }
    },
    /**EOC Disable search filter code */
})