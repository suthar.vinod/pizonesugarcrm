<?php
$module_name = 'Equip_Equipment';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'owner_c',
                'studio' => 'visible',
                'label' => 'LBL_OWNER',
              ),
              1 => 
              array (
                'name' => 'classification_c',
                'label' => 'LBL_CLASSIFICATION',
              ),
              2 => 
              array (
                'name' => 'department_c',
                'label' => 'LBL_DEPARTMENT',
              ),
              3 => 
              array (
                'name' => 'manufacturer_c',
                'studio' => 'visible',
                'label' => 'LBL_MANUFACTURER',
              ),
              4 => 
              array (
                'name' => 'status_c',
                'label' => 'LBL_STATUS_C',
              ),
              5 => 
              array (
                'name' => 'out_of_service_date_c',
                'label' => 'LBL_OUT_OF_SERVICE_DATE',
              ),
              6 => 
              array (
                'name' => 'model_c',
                'label' => 'LBL_MODEL',
              ),
              7 => 
              array (
                'name' => 'serial_number_c',
                'label' => 'LBL_SERIAL_NUMBER',
              ),
              8 => 
              array (
                'name' => 'current_software_firmware_c',
                'label' => 'LBL_CURRENT_SOFTWARE_FIRMWARE',
                'span' => 12,
              ),
              9 => 
              array (
                'name' => 'applicable_sop_c',
                'studio' => 'visible',
                'label' => 'LBL_APPLICABLE_SOP',
              ),
              10 => 
              array (
                'name' => 'applicable_form_c',
                'studio' => 'visible',
                'label' => 'LBL_APPLICABLE_FORM',
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'building_c',
                'label' => 'LBL_BUILDING',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'room_c',
                'studio' => 'visible',
                'label' => 'LBL_ROOM',
                'span' => 12,
              ),
              2 => 
              array (
                'name' => 'binder_location_c',
                'label' => 'LBL_BINDER_LOCATION',
                'span' => 12,
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'last_service_date_c',
                'label' => 'LBL_LAST_SERVICE_DATE',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'next_service_date_c',
                'label' => 'LBL_NEXT_SERVICE_DATE',
                'span' => 12,
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'iso_17025_certification_c',
                'label' => 'LBL_ISO_17025_CERTIFICATION_REQUIRED',
              ),
              1 => 
              array (
                'name' => 'iso_17025_units_of_measure_c',
                'label' => 'LBL_ISO_17025_UNITS_OF_MEASURE',
              ),
            ),
          ),
          5 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL6',
            'label' => 'LBL_RECORDVIEW_PANEL6',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'vendor_c',
                'label' => 'LBL_VENDOR',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'po_c',
                'label' => 'LBL_PO',
                'span' => 12,
              ),
              2 => 
              array (
                'name' => 'expense_or_capitalize_c',
                'label' => 'LBL_EXPENSE_OR_CAPITALIZE',
              ),
              3 => 
              array (
                'name' => 'asset_id_c',
                'label' => 'LBL_ASSET_ID',
              ),
              4 => 
              array (
                'name' => 'disposition_status_c',
                'label' => 'LBL_DISPOSITION_STATUS',
                'span' => 12,
              ),
            ),
          ),
          6 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL4',
            'label' => 'LBL_RECORDVIEW_PANEL4',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'notes_c',
                'studio' => 'visible',
                'label' => 'LBL_NOTES',
                'span' => 12,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);
