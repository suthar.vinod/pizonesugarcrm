<?php
// created: 2020-11-03 06:40:12
$viewdefs['Equip_Equipment']['base']['view']['subpanel-for-accounts-accounts_equip_equipment_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'status_c',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'serial_number_c',
          'label' => 'LBL_SERIAL_NUMBER',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'department_c',
          'label' => 'LBL_DEPARTMENT',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'classification_c',
          'label' => 'LBL_CLASSIFICATION',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'building_c',
          'label' => 'LBL_BUILDING',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);