<?php
$module_name = 'Equip_Equipment';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'manufacturer_c',
                'label' => 'LBL_MANUFACTURER',
                'enabled' => true,
                'id' => 'ACCOUNT_ID1_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'model_c',
                'label' => 'LBL_MODEL',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'serial_number_c',
                'label' => 'LBL_SERIAL_NUMBER',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'last_service_date_c',
                'label' => 'LBL_LAST_SERVICE_DATE',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'next_service_date_c',
                'label' => 'LBL_NEXT_SERVICE_DATE',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'building_c',
                'label' => 'LBL_BUILDING',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'room_c',
                'label' => 'LBL_ROOM',
                'enabled' => true,
                'id' => 'RMS_ROOM_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'department_c',
                'label' => 'LBL_DEPARTMENT',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'status_c',
                'label' => 'LBL_STATUS',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
