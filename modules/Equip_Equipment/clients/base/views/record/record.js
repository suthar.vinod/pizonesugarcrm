({
    extendsFrom: 'RecordView',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.model.on('data:sync:complete', this.function_change_field_data, this);
    },
    function_change_field_data: function () {
        var erd_error_documents_id_c = this.model.get('erd_error_documents_id_c');
        $.ajax({
            type: 'GET',
            dataType: "JSON",
            url: "index.php?entryPoint=getEquipmentDataEP",
            data: {erd_error_documents_id_c:erd_error_documents_id_c},
            success: function (data) {
                //console.log('doc_id_c', data);
                if (data.doc_id_c != '') {
                    $('span[data-fieldname="applicable_sop_c"] .detail a').text(data.doc_id_c);
                    $('span[data-fieldname="applicable_sop_c"] .detail a').attr('data-original-title',data.doc_id_c);
                }
            }
        });

    },
})


