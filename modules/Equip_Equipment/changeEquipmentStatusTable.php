<?php

global $db;
 

$query = "SELECT  IFNULL(equip_equipment.id,'') primaryid,
				  IFNULL(equip_equipment.name,'') equip_equipment_name,
				  IFNULL(equip_equipment_cstm.department_c,'') EF_Department,
				  equip_equipment_cstm.current_status_c EF_Status,
							equip_equipment_cstm.status_c EF_NewStatus,
							equip_equipment.date_entered EF_Date,
							equip_equipment_cstm.department_c EF_Department
			FROM equip_equipment
				LEFT JOIN equip_equipment_cstm equip_equipment_cstm 
				ON equip_equipment.id = equip_equipment_cstm.id_c
			WHERE equip_equipment.deleted=0 ";
	
$queryResult = $db->query($query);
$srno = 0;
while ($fetchResult = $db->fetchByAssoc($queryResult)) {
	$primaryid				 = $fetchResult['primaryid'];
	$newStatus		 	 	 = $fetchResult['EF_NewStatus'];
	$status	 			 	 = $fetchResult['EF_Status'];
	$department	 			 = $fetchResult['EF_Department'];
	
	if($status=="Active")
		$newStatus = "Active GLP compliant";
	
	if($status=="Inactive")
		$newStatus = "Inactive";
	
	if($status=="Out for Service")
		$newStatus = "Out for Service";
	
	if($status=="Retired")
		$newStatus = "Retired";
	
	if($status=="Quarantined" || $status=="" )
		$newStatus = "Quarantined";
	
	if($department=="Facilities")
	{
		if($status=="Active" || $status=="")
			$newStatus = "Active";
		if($status=="Inactive")
			$newStatus = "Inactive";
	} 
	
	echo "<br>===>".$updateQuery = "UPDATE `equip_equipment_cstm` SET `status_c`='".$newStatus."' WHERE id_c='".$primaryid."' "; 
    $db->query($updateQuery);     
		
	}
 
?>