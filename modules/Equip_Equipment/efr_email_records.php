<?php
//Add job in job string

echo "<h2>Initial In Service/Completed In Service workflow reminder for E&F</h2>";

echo " <br>";

global $db;
if (isset($_GET['check_date'])) {


   $current_date = $_GET['check_date']; //current date

   $site_url = $GLOBALS['sugar_config']['site_url'];
   $domainToSearch = 'apsemail.com';

   $efIdArray = array();
   $eArr = array();
   $queryCustom = "SELECT id FROM efr_equipment_facility_recor where date(date_entered) = '$current_date' and deleted=0";


   $efrQuery = $db->query($queryCustom);


   while ($fetchResult = $db->fetchByAssoc($efrQuery)) {
      $efrId = $fetchResult['id'];

      $sql = "SELECT equip_equi01e2uipment_ida FROM equip_equipment_efr_equipment_facility_recor_2_c where equip_equiffd2y_recor_idb='$efrId' and deleted=0";

      $sqlResult = $db->query($sql);
      $fetchResult1 = $db->fetchByAssoc($sqlResult);
      $efId = $fetchResult1['equip_equi01e2uipment_ida'];
      if ($efId != "") {
         $efIdArray[] = $efId;
      }
   }
   $efIdArray = array_unique($efIdArray);

   $p = 0;
   foreach ($efIdArray as $eId) {
      $p = 0;
      $efBean = BeanFactory::retrieveBean('Equip_Equipment', $eId, array('disable_row_level_security' => true));
      $efBean->load_relationship('equip_equipment_efr_equipment_facility_recor_2');
      $relateEfrIds = $efBean->equip_equipment_efr_equipment_facility_recor_2->get();
      if ($efBean->status_c != 'Retired') {
         foreach ($relateEfrIds as $efrrId) {
            $efBean = BeanFactory::retrieveBean('EFR_Equipment_Facility_Recor', $efrrId, array('disable_row_level_security' => true));
            if ($p == 1)
               continue;
            if (strpos($efBean->type_2_c, 'Initial In Service') && (!strpos($efBean->type_2_c, 'Initial In Service 2'))) {
               $p = 1;
            } else {
               if (strpos($efBean->type_2_c, 'Initial In Service 2'))
                  $eArr[$efrrId] = $eId;
            }
         }
      }
      
      if ($p > 0) {
         foreach ($eArr as $value) {
            $key = array_search($eId, $eArr);
            unset($eArr[$key]);
         }
      }
   }

   echo "(EFR_Equipment_Facility_Recor - Equip_Equipment) <br><br>";
   foreach ($eArr as $erfval => $efVal) {
      $efrBean = BeanFactory::retrieveBean('EFR_Equipment_Facility_Recor', $erfval, array('disable_row_level_security' => true));
      $efrName = $efrBean->name;
      $efBean = BeanFactory::retrieveBean('Equip_Equipment', $efVal, array('disable_row_level_security' => true));
      $efName = $efBean->name;
      //Send Email/
      ///Upload Document Reminder Template 
      $template = new EmailTemplate();
      $emailObj = new Email();

      $template->retrieve_by_string_fields(array('name' => 'Initial In Service - Completed In Service workflow reminder for EF', 'type' => 'email'));

      echo $efrLink = '<a target="_blank"href="' . $site_url . '/#EFR_Equipment_Facility_Recor/' . $erfval . '">' . $efrName . '</a>';
      echo " - ";
      echo $efLink = '<a target="_blank"href="' . $site_url . '/#Equip_Equipment/' . $efVal . '">' . $efName . '</a>';

      echo " <br><br> ";
   }
   echo "<h2>Total Mail : " . count($eArr) . "</h2>";
} else {
   echo "Oops! Invalid Request";
}
