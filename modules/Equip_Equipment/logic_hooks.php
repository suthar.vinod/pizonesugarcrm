<?php

   $hook_version = 1;
   $hook_array = Array();

   $hook_array['before_save'] = Array();
   $hook_array['before_save'][] = Array(
      1, 
      'Equipment Facilities Change the fields', 
      'custom/modules/Equip_Equipment/EE_DocumentIdHook.php', 
      'EE_DocumentIdHook', 
      'UpdateEE_DocumentIdField' 
   );

   $hook_array['after_retrieve'][] = array(
      '11',
      'Delete Duplicate next_service_date_c in Audit',
      'custom/modules/Equip_Equipment/deleteAuditLogNextServiceDate.php',
      'deleteAuditLogNextServiceDate',
      'deleteNextServiceDate'
  );


?>