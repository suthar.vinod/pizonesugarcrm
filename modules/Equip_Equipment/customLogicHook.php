<?php

class customLogicHook {

    function setDefaultForStatus($bean, $event, $arguments) {
        global $db;
		 $td = new TimeDate();
        $ID = $bean->id;
        $isIdExist = $bean->fetched_row['id'];
		
		$queryGetMaxServiceDate = "SELECT  max(EFRC.service_date_c) MAXServiceDate FROM efr_equipment_facility_recor  AS EEFR "
                . "LEFT JOIN efr_equipment_facility_recor_cstm AS EFRC ON EEFR.id = EFRC.id_c "
                . "LEFT JOIN equip_equipment_efr_equipment_facility_recor_2_c AS EEJOINEFR ON EFRC.id_c = EEJOINEFR.equip_equiffd2y_recor_idb "
                . "LEFT JOIN equip_equipment AS EE ON EEJOINEFR.equip_equi01e2uipment_ida = EE.id "
                . "LEFT JOIN equip_equipment_cstm AS EEC ON EE.id = EEC.id_c "
                . "WHERE EE.id = '" . $ID . "' AND EEFR.deleted = 0 AND EEJOINEFR.deleted = 0 "
                . "ORDER BY EFRC.service_date_c DESC LIMIT 0,1";

        $getResult = $db->query($queryGetMaxServiceDate);
		
		if ($row = $db->fetchByAssoc($getResult)) {
			  $last_date = $row['MAXServiceDate'];
			  
			  $bean->last_service_date_c = $last_date;
		} 
		
		$queryForGetSoftwareValue = "SELECT  EFRC.software_firmware_c  FROM efr_equipment_facility_recor AS EFR "
                    . "LEFT JOIN efr_equipment_facility_recor_cstm AS EFRC ON EFR.id = EFRC.id_c "
                    . "LEFT JOIN equip_equipment_efr_equipment_facility_recor_2_c  AS EEJOINEFR  "
                    . "ON EFRC.id_c = EEJOINEFR.equip_equiffd2y_recor_idb "
                    . "LEFT JOIN equip_equipment AS EE  ON EEJOINEFR.equip_equi01e2uipment_ida = EE.id "
                    . "WHERE EE.id = '" . $ID . "' and EFR.deleted = 0 AND EEJOINEFR.deleted = 0 "
                    . "AND (EFRC.software_firmware_c is not null OR EFRC.software_firmware_c != '') "
                    . "ORDER BY EFR.date_entered DESC  LIMIT 0,1";
        $getSoftware = $db->query($queryForGetSoftwareValue);
        if ($rowSoftware = $db->fetchByAssoc($getSoftware)) {
			  $softwareFirmware = $rowSoftware['software_firmware_c'];
			  $bean->current_software_firmware_c = $softwareFirmware;
		} 
		if($bean->status_c=='Retired')
		{
			$bean->next_service_date_c = ''; 
			$queryForGetLatestServiceID = "SELECT  EFS.id  FROM efs_equipment_facility_servi AS EFS " 
                    . "LEFT JOIN equip_equipment_efs_equipment_facility_servi_1_c  AS EEJOINEFS  "
                    . "ON EFS.id = EEJOINEFS.equip_equi3f6dy_servi_idb "
                    . "LEFT JOIN equip_equipment AS EE  ON EEJOINEFS.equip_equia9d9uipment_ida = EE.id "
                    . "WHERE EE.id = '" . $ID . "' and EFS.deleted = 0 AND EEJOINEFS.deleted = 0 " 
                    . "ORDER BY EFS.date_entered DESC";
					
			$getServiceID = $db->query($queryForGetLatestServiceID);
			
			if ($getServiceID->num_rows > 0) {
				while($rowServiceID = $db->fetchByAssoc($getServiceID)){
					 
					$latestServiceID = $rowServiceID['id'];	
					$efsBean = BeanFactory::getBean('EFS_Equipment_Facility_Servi', $latestServiceID);
					$efsBean->next_service_date_c = '';
					$efsBean->inactive_service_c = 1;
					$efsBean->is_status_retired_c = '1';
					$efsBean->save();
				}
				
			}	
		}else{
			$queryForGetLatestServiceID = "SELECT  EFS.id,EFSC.last_service_date_c,EFSC.next_service_date_c,EFSC.service_interval_days_c FROM efs_equipment_facility_servi AS EFS " 
                    . "LEFT JOIN equip_equipment_efs_equipment_facility_servi_1_c  AS EEJOINEFS  "
                    . "ON EFS.id = EEJOINEFS.equip_equi3f6dy_servi_idb "
                    . "LEFT JOIN equip_equipment AS EE  ON EEJOINEFS.equip_equia9d9uipment_ida = EE.id "
					. "LEFT JOIN efs_equipment_facility_servi_cstm AS EFSC  ON EFS.id = EFSC.id_c "
                    . "WHERE EE.id = '" . $ID . "' and EFS.deleted = 0 AND EEJOINEFS.deleted = 0 " 
                    . "ORDER BY EFS.date_entered DESC ";
					
			$getServiceID = $db->query($queryForGetLatestServiceID);
			$dateArr = array(); 
			if ($getServiceID->num_rows > 0) {
				while($rowServiceID = $db->fetchByAssoc($getServiceID)){
					$latestServiceID = $rowServiceID['id'];	
					$serviceLastdate = $rowServiceID['last_service_date_c'];	
					$serviceNextdate = $rowServiceID['next_service_date_c'];	
					$serviceInterval = $rowServiceID['service_interval_days_c'];
					
					

					$next_service_date_c = date("Y-m-d", strtotime($serviceLastdate . ' + ' . $serviceInterval . ' days'));
					$date = $td->fromString($next_service_date_c);
					$service_date1 = $td->asDbDate($date);	
					
					$efsBean = BeanFactory::getBean('EFS_Equipment_Facility_Servi', $latestServiceID);
					
					$efsBean->is_status_retired_c	= '';
					if($efsBean->inactive_service_c!='1')
					{
						$efsBean->inactive_service_c	= 0;
						$efsBean->next_service_date_c	= $service_date1;
						$dateArr[]  =  strtotime($service_date1);
					}else{$efsBean->next_service_date_c	= '';}	
					$efsBean->savefromEF			= '1'; 
					$efsBean->save(); 
				
				//$bean->next_service_date_c = $service_date1;
				}
				sort($dateArr);
				if(count($dateArr)>0)
					$bean->next_service_date_c = ($dateArr[0]>0)?date("Y-m-d",$dateArr[0]):"";
			}
			
		}		


		
	}

    /**
     * a) Populates Equipment & Facilities Last service date with the recent related
     * Equipment & Facility Records record's largest Service Date.
     * b) Populates Equipment & Facilities Next service date with the recent related
     * Equipment & Facility Services module record's smallest Next Service Date.
     *
     * @param object $bean
     * @param string $event
     * @param array $arguments
     */
	function populateLastServiceDate($bean, $event, $arguments) {

        if ($arguments['related_module'] == 'EFS_Equipment_Facility_Servi' && $arguments['link'] == 'equip_equipment_efs_equipment_facility_servi_1') {
            global $db;
            $td = new TimeDate();

            $date_null = 'null';

            $relatedBean = BeanFactory::getBean($arguments['related_module'], $arguments['related_id']);
            $field = "next_service_date_c";

           

            if ($relatedBean->last_service_date_c != '') {

                //if Inactive Serive checkbox of the related bean is checked
                if ($relatedBean->inactive_service_c == true) {
                    $service_date1 = 'null';
                } else {
					if($bean->status_c!="Retired"){
					
                    $next_service_date_c = date("Y-m-d", strtotime($relatedBean->last_service_date_c . ' + ' . $relatedBean->service_interval_days_c . ' days'));
                    $date = $td->fromString($next_service_date_c);
                    $service_date1 = $td->asDbDate($date);
					}else{$service_date1 = 'null'; }
                }
                $queryForNextServiceDate = "SELECT min(efs_cstm.next_service_date_c) AS next_service_date_c FROM equip_equipment ee "
                        . "INNER JOIN equip_equipment_efs_equipment_facility_servi_1_c eefs ON ee.id = eefs.equip_equia9d9uipment_ida "
                        . "INNER JOIN efs_equipment_facility_servi efs ON eefs.equip_equi3f6dy_servi_idb = efs.id "
                        . "INNER JOIN  efs_equipment_facility_servi_cstm efs_cstm ON efs.id = efs_cstm.id_c "
                        . "WHERE ee.deleted = 0 AND eefs.deleted = 0 AND efs.deleted = 0 AND ee.id = '" . $arguments['id'] . "'";

                $result = $db->query($queryForNextServiceDate);

                if ($row = $db->fetchByAssoc($result)) {
                    $service_date2 = $row['next_service_date_c'];
                    

                    //Populate the Next Service Date with the minimum Next Service Date of related
                    //module, if related all records Next Service Date is null only when populate parent date
                    //with null, otherwise populate with the minimum one.
                    if ((!empty($service_date1) && $service_date1 != 'null') && (empty($service_date2) || $service_date2 == '')) {
                        $bean->next_service_date_c = $service_date1;  
                    } else if ((!empty($service_date2) && $service_date2 != 'null') && empty($service_date1)) {
                        $bean->next_service_date_c = $service_date2; 
                    } else if ($service_date1 < $service_date2 && (!empty($service_date1) && $service_date1 != null) && (!empty($service_date2) && $service_date2 != null)) {
                        $bean->next_service_date_c = $service_date1; 
                    } else if ($service_date2 < $service_date1 && (!empty($service_date1) && $service_date1 != null) && (!empty($service_date2) && $service_date2 != null)) {
                        $bean->next_service_date_c = $service_date2;  
                    } else {
                        $bean->next_service_date_c = $date_null; 
                    }
                } else {
                    if ($relatedBean->inactive_service_c == true) {
                        $bean->next_service_date_c = $service_date1;

                        $allRecord =0;
                        $allInactive = 0;

                        $queryCountAll = "SELECT count(id) AS allRecord FROM equip_equipment_efs_equipment_facility_servi_1_c AS EEFS
                                            LEFT JOIN efs_equipment_facility_servi_cstm AS EFSC ON EEFS.equip_equi3f6dy_servi_idb = EFSC.id_c
                                            WHERE EEFS.equip_equia9d9uipment_ida='".$bean->id."' ";
                        $resultAllRecord = $db->query($queryCountAll);
                        if ($rowAllRecord = $db->fetchByAssoc($resultAllRecord)) {
                            $allRecord = $rowAllRecord['allRecord'];                    
                        }   
                        
                        $queryCountInactive = "SELECT count(id) AS allRecord FROM equip_equipment_efs_equipment_facility_servi_1_c AS EEFS
                        LEFT JOIN efs_equipment_facility_servi_cstm AS EFSC ON EEFS.equip_equi3f6dy_servi_idb = EFSC.id_c
                        WHERE EEFS.equip_equia9d9uipment_ida='".$bean->id."' AND EFSC.inactive_service_c=1";
                        $resultAllInactive = $db->query($queryCountInactive);
                        if ($rowAllInactive = $db->fetchByAssoc($resultAllInactive)) {
                            $allInactive = $rowAllInactive['allRecord'];                    
                        }  

                        if(($allInactive>0 && $allRecord>0 && $allInactive == $allRecord) || $bean->status_c=="Retired" ){ // && $bean->status_c=="Retired"
                            
                            $bean->next_service_date_c = $date_null;  
                        }else{
                            $bean->next_service_date_c = $service_date1; 
                        } 
                    } else {
                        $bean->next_service_date_c = $service_date1; 
                    }
                }
                
            }
            //$bean->last_service_date_c = '2020-09-14';  
            //Update Last Service Date
            //$this->getTheLatestLastServiceDate($bean);
        }
        if ($arguments['related_module'] == 'EFR_Equipment_Facility_Recor' && $arguments['link'] == 'equip_equipment_efr_equipment_facility_recor_2') {

            $this->getTheLatestLastServiceDate($bean);
        }
    }
	    
    function getTheLatestLastServiceDate($bean) {

        global $db;         
        //Update the Last service date in Equipment and Facility Module with maximum Last serive date 
        $queryForLastServiceDate = "SELECT max(efr_cstm.service_date_c) AS service_date_c  FROM efr_equipment_facility_recor efr "
                . "INNER JOIN efr_equipment_facility_recor_cstm efr_cstm ON efr.id = efr_cstm.id_c "
                . "INNER JOIN equip_equipment_efr_equipment_facility_recor_2_c ee_efr ON efr_cstm.id_c = ee_efr.equip_equiffd2y_recor_idb "
                . "INNER JOIN equip_equipment ee ON ee_efr.equip_equi01e2uipment_ida = ee.id "
                . "INNER JOIN equip_equipment_cstm ee_cstm ON ee.id = ee_cstm.id_c "
                . "WHERE ee.deleted = 0 AND ee_efr.deleted = 0 AND efr.deleted = 0 AND ee.id = '" . $bean->id . "' LIMIT 0,1 ";

        $result = $db->query($queryForLastServiceDate);

        if ($row = $db->fetchByAssoc($result)) {
            $lastServiceDate = $row['service_date_c'];

            $queryForUpdate = "UPDATE equip_equipment_cstm SET last_service_date_c = '" . $lastServiceDate . "' WHERE id_c = '" . $bean->id . "'";
            $result = $db->query($queryForUpdate);
            if (!$result)
               
			$bean->last_service_date_c = $lastServiceDate; 
        }
    }

    function updateSoftwareAndFirmware($bean) {

        global $db;

        $queryForUpdateStatusAndSoftwareField = "SELECT efrc.software_firmware_c from efr_equipment_facility_recor as efr LEFT JOIN efr_equipment_facility_recor_cstm as efrc ON efr.id = efrc.id_c LEFT JOIN equip_equipment_efr_equipment_facility_recor_2_c as eeefr ON efrc.id_c = eeefr.equip_equiffd2y_recor_idb LEFT JOIN equip_equipment as ee ON eeefr.equip_equi01e2uipment_ida = ee.id WHERE ee.id = '" . $bean->id . "' and efr.deleted = 0 AND eeefr.deleted = 0 AND (efrc.software_firmware_c is not null OR efrc.software_firmware_c != '') ORDER BY efr.date_entered DESC LIMIT 0,1";
        $statusAndSoftware = $db->query($queryForUpdateStatusAndSoftwareField);
        $result = $db->fetchByAssoc($statusAndSoftware);
        if (!empty($result['software_firmware_c'])) {
            $software_firmwarename = $result['software_firmware_c'];
        } else {
            $software_firmwarename = "";
        }

        $bean->current_software_firmware_c = $software_firmwarename;
        $bean->save();
    }
	
	
	 function getEquipAndFacilityIdByEFServiceId($pID) {
        global $db;
        $query = "SELECT ee.id FROM efs_equipment_facility_servi efs INNER JOIN equip_equipment_efs_equipment_facility_servi_1_c ee_efs "
                . "ON efs.id = ee_efs.equip_equi3f6dy_servi_idb INNER JOIN equip_equipment ee ON ee_efs.equip_equia9d9uipment_ida = ee.id "
                . "WHERE efs.id = '" . $pID . "'";

        $result = $db->query($query);
        if ($row = $db->fetchByAssoc($result)) {
            $eeId = $row['id'];
            return $eeId;
        }
    }
}