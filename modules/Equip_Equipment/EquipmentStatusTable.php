<?php

global $db;

     echo "<br>===>".$query = "SELECT 
							IFNULL(equip_equipment.id,'') primaryid,
							IFNULL(equip_equipment.name,'') equip_equipment_name,
							IFNULL(equip_equipment_cstm.department_c,'') EF_Department,
							equip_equipment_cstm.current_status_c EF_Status,
							equip_equipment_cstm.status_c EF_NewStatus,
							equip_equipment.date_entered EF_Date,
							equip_equipment_cstm.out_of_service_date_c EF_ServiceDate							
						FROM equip_equipment
						LEFT JOIN equip_equipment_cstm equip_equipment_cstm 
							ON equip_equipment.id = equip_equipment_cstm.id_c

						WHERE equip_equipment.deleted=0 ";
	
    $queryResult = $db->query($query);
	
	 $table = "<table border='1' width='100%' cellpadding='2' cellspacing='2'> <tr> <th>srno</th>  <th>primaryid</th> <th>equip_equipment_name</th> <th>Department</th>	  <th>Old Status</th> <th>New Status</th>  <th>Out of Service Date</th>	 </tr>";

$srno = 0;
    while ($fetchResult = $db->fetchByAssoc($queryResult)) {
		
	   $primaryid				 = $fetchResult['primaryid'];
	   $equip_equipment_name	 = $fetchResult['equip_equipment_name'];
	   $department	 			 = $fetchResult['EF_Department'];
	   $newStatus		 	 	 = $fetchResult['EF_NewStatus'];
	   $status	 			 	 = $fetchResult['EF_Status'];
	   $dateCreated	 			 = $fetchResult['EF_Date'];
	   $EF_ServiceDate	 			 = $fetchResult['EF_ServiceDate'];
	    
	   $srno++;
	$table .="<tr><td align='center'>".$srno."</td> <td align='center'>".$primaryid."</td><td align='center'>".$equip_equipment_name." </td><td align='center'>".$department."</td> <td align='center'>".$status."</td> <td align='center'>".$newStatus."</td><td align='center'>".$EF_ServiceDate."</td> </tr>";
         
		
	}
	 $table .="</table>";
	 echo "<br><br>";
	 echo $table;
?>