<?php
$popupMeta = array (
    'moduleMain' => 'Equip_Equipment',
    'varName' => 'Equip_Equipment',
    'orderBy' => 'equip_equipment.name',
    'whereClauses' => array (
  'name' => 'equip_equipment.name',
  'serial_number_c' => 'equip_equipment_cstm.serial_number_c',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'serial_number_c',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'serial_number_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SERIAL_NUMBER',
    'width' => 10,
    'name' => 'serial_number_c',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'SERIAL_NUMBER_C' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SERIAL_NUMBER',
    'width' => 10,
    'default' => true,
  ),
),
);
