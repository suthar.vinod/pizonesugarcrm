<?php
// created: 2018-01-31 17:32:54
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'serial_number_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_SERIAL_NUMBER',
    'width' => 10,
    'default' => true,
  ),
);