<?php
// created: 2020-11-03 06:40:12
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'status_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_STATUS',
    'width' => 10,
  ),
  'serial_number_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_SERIAL_NUMBER',
    'width' => 10,
    'default' => true,
  ),
  'department_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_DEPARTMENT',
    'width' => 10,
  ),
  'classification_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_CLASSIFICATION',
    'width' => 10,
  ),
  'building_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_BUILDING',
    'width' => 10,
  ),
);