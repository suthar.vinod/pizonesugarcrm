<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class ReceivedItemFilterInIIHook {

    function ReceivedItemFilterInIIMethod($bean, $event, $arguments) {
        
        $module = $arguments['module'];
        $modelId = $arguments['id'];

        $relatedModule = $arguments['related_module'];
        $relatedId = $arguments['related_id'];

        if ($module === 'RI_Received_Items' && $relatedModule === 'II_Inventory_Item') {
            $this->calcSumOfQuantity($bean, $modelId, $event, $arguments);
        } else if ($relatedModule === 'RI_Received_Items' && $module === 'II_Inventory_Item') {
            $this->calcSumOfQuantity($bean, $relatedId, $event, $arguments);
        }

    }

    function calcSumOfQuantity($bean, $id, $event, $arguments) {
        global $db;
        if($id != ""){
            $RI_Received_Items = BeanFactory::retrieveBean('RI_Received_Items', $id); 
    
            $RI_Received_Items->load_relationship('ri_received_items_ii_inventory_item_1');
            $IIIds = $RI_Received_Items->ri_received_items_ii_inventory_item_1->get();
            
            if($event != 'after_relationship_delete'){
                array_push($IIIds,$arguments['related_id']);
                $IIIds = array_unique($IIIds);
            }

            $total = 0;
            for($a=0;$a<count($IIIds);$a++){
                $II_Inventory_Item = BeanFactory::retrieveBean('II_Inventory_Item', $IIIds[$a]);
                $quantity_2_c = $II_Inventory_Item->quantity_2_c;             
                $total = $total+$quantity_2_c;            
            } 
        
            $flag = '';
            if($RI_Received_Items->quantity_received > $total){
                $flag = '1';
                $RI_Received_Items->linked_ii_sum_flag_c = $flag;
                $RI_Received_Items->save(); 
            }else{
                $flag = '0';
                $RI_Received_Items->linked_ii_sum_flag_c = $flag;
                $RI_Received_Items->save(); 
            }
        }
    } 
    
    function ReceivedItemFilterInIIMethodBeforeSave($bean) {
        global $db;
         
        if($bean->id != "" && $bean->quantity_received != $bean->fetched_row['quantity_received']){
            $bean->load_relationship('ri_received_items_ii_inventory_item_1');
            $IIIds = $bean->ri_received_items_ii_inventory_item_1->get();
            
            $total = 0;
            for($a=0;$a<count($IIIds);$a++){
                $II_Inventory_Item = BeanFactory::retrieveBean('II_Inventory_Item', $IIIds[$a]);
                $quantity_2_c = $II_Inventory_Item->quantity_2_c;             
                $total = $total+$quantity_2_c;            
            } 

            $flag = '';
            if($bean->quantity_received > $total){
                $flag = '1';
                $bean->linked_ii_sum_flag_c = $flag;
            }else{
                $flag = '0';
                $bean->linked_ii_sum_flag_c = $flag;
            }
        }
       
    }

    function ReceivedItemFilterInIIMethodIIAfterSave($bean,$event, $arguments) {
        global $db;
 
        if (isset($arguments['dataChanges']['quantity_2_c']) && $bean->ri_received_items_ii_inventory_item_1ri_received_items_ida != "") {
            $RIid = $bean->ri_received_items_ii_inventory_item_1ri_received_items_ida;
            $RI_Received_Items = BeanFactory::retrieveBean('RI_Received_Items', $RIid);
            
            $RI_Received_Items->load_relationship('ri_received_items_ii_inventory_item_1');
            $IIIds = $RI_Received_Items->ri_received_items_ii_inventory_item_1->get();
                
            array_push($IIIds,$bean->id);
            $IIIds = array_unique($IIIds);
            
            $total = 0;
            for($a=0;$a<count($IIIds);$a++){
                $II_Inventory_Item = BeanFactory::retrieveBean('II_Inventory_Item', $IIIds[$a]);
                $quantity_2_c = $II_Inventory_Item->quantity_2_c;             
                $total = $total+$quantity_2_c;            
            } 
        
            $flag = '';
            if($RI_Received_Items->quantity_received > $total){
                $flag = '1';
                $RI_Received_Items->linked_ii_sum_flag_c = $flag;
                $RI_Received_Items->save();
            }else{
                $flag = '0';
                $RI_Received_Items->linked_ii_sum_flag_c = $flag;
                $RI_Received_Items->save();
            }
        }
    }
}
