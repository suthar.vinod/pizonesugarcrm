<?php
// created: 2021-08-04 19:56:13
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'quantity_received' => 
  array (
    'readonly' => false,
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_QUANTITY_RECEIVED',
    'width' => 10,
  ),
  'received_date' => 
  array (
    'readonly' => false,
    'type' => 'date',
    'vname' => 'LBL_RECEIVED_DATE',
    'width' => 10,
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'related_fields' => 
    array (
      0 => 'assigned_user_id',
    ),
    'vname' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'poi_purchase_order_item_ri_received_items_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
    'id' => 'POI_PURCHA665CER_ITEM_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'POI_Purchase_Order_Item',
    'target_record_key' => 'poi_purcha665cer_item_ida',
  ),
  'ori_order_request_item_ri_received_items_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
    'id' => 'ORI_ORDER_6B32ST_ITEM_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'ORI_Order_Request_Item',
    'target_record_key' => 'ori_order_6b32st_item_ida',
  ),
);