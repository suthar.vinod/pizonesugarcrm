<?php
// created: 2021-08-27 12:52:37
$viewdefs['RI_Received_Items']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'assigned_user_name' => 
    array (
    ),
    'name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'quantity_received' => 
    array (
    ),
    'received_date' => 
    array (
    ),
    'type_2' => 
    array (
    ),
    'ori_order_request_item_ri_received_items_1_name' => 
    array (
    ),
    'poi_purchase_order_item_ri_received_items_1_name' => 
    array (
    ),
    'linked_ii_sum_flag_c' => 
    array (
    ),
  ),
);