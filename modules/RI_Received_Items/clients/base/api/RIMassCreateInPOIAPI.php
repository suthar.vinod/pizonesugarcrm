<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class RIMassCreateInPOIAPI extends SugarApi
{

    public function registerApiRest()
    {
        return array(
            'riMassCreate' => array(
                'reqType' => 'POST',
                'path' => array('ri_mass_create_poi'),
                'pathVars' => array(''),
                'method' => 'riMassCreate',
                'shortHelp' => 'Cretes RIs from the POI sent in this request',
                'longHelp' => 'Cretes RIs from the POI sent in this request',
            ),
        );
    }

    public function riMassCreate($api, $args)
    {
        $response = array();
        $response['status'] = FALSE;
        $response['Message'] = "Saving failed";

        try {
            //  Save it to DB
            $this->_riMassCreateToDB($args);

            $response['status'] = TRUE;
            $response['Message'] = "Saved successfully";
        } catch (Exception $exc) {
            //$GLOBALS['log']->fatal("RIMassCreateInPOIAPI : riMassCreate() -> " . $exc->getMessage());
            $response['status'] = FALSE;
            $response['Message'] = "Saving failed";
        }

        return $response;
    }

    private function _riMassCreateToDB($args)
    {
        try {
            $valuesToSet = $args['data']['values'];
            $ids = $args['data']['ids'];
            
            foreach ($ids as $id) {
                if (!empty($valuesToSet)) {

                    $childBean = BeanFactory::newBean("RI_Received_Items", null, array('disable_row_level_security' => true));
                    foreach ($valuesToSet as $fieldName => $value) {
                        $childBean->$fieldName = $value;
                    }

                    $childBean->poi_purcha665cer_item_ida = $id;

                    $childBean->save();
                }
            }
        } catch (Exception $exc) {
            $GLOBALS['log']->fatal("RIMassCreateInPOIAPI : _riMassCreateToDB() -> " . $exc->getMessage());
        }
    }
}
