<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class RIMassCreateInORIAPI extends SugarApi
{

    public function registerApiRest()
    {
        return array(
            'riMassCreate' => array(
                'reqType' => 'POST',
                'path' => array('ri_mass_create_ori'),
                'pathVars' => array(''),
                'method' => 'riMassCreate',
                'shortHelp' => 'Cretes RIs from the ORI sent in this request',
                'longHelp' => 'Cretes RIs from the ORI sent in this request',
            ),
        );
    }

    public function riMassCreate($api, $args)
    {
        $response = array();
        $response['status'] = FALSE;
        $response['Message'] = "Saving failed";

        try {
            //  Save it to DB
            $this->_riMassCreateToDB($args);

            $response['status'] = TRUE;
            $response['Message'] = "Saved successfully";
        } catch (Exception $exc) {
            $response['status'] = FALSE;
            $response['Message'] = "Saving failed";
        }

        return $response;
    }

    private function _riMassCreateToDB($args)
    {
        try {
            $valuesToSet = $args['data']['values'];
            $ids = $args['data']['ids'];
            //$GLOBALS['log']->fatal("$args " . print_r($args, 1));
            foreach ($ids as $id) {
                if (!empty($valuesToSet)) {

                    $childBean = BeanFactory::newBean("RI_Received_Items", null, array('disable_row_level_security' => true));
                    foreach ($valuesToSet as $fieldName => $value) {
                        $childBean->$fieldName = $value;
                    }

                    $childBean->ori_order_6b32st_item_ida = $id;

                    $childBean->save();
                }
            }
        } catch (Exception $exc) {
            $GLOBALS['log']->fatal("RIMassCreateInORIAPI : _riMassCreateToDB() -> " . $exc->getMessage());
        }
    }
}
