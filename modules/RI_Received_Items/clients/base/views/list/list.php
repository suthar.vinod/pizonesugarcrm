<?php
$module_name = 'RI_Received_Items';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'quantity_received',
                'label' => 'LBL_QUANTITY_RECEIVED',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'product_name_c',
                'label' => 'LBL_PRODUCT_NAME',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'received_date',
                'label' => 'LBL_RECEIVED_DATE',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              5 => 
              array (
                'name' => 'poi_purchase_order_item_ri_received_items_1_name',
                'label' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_NAME_FIELD_TITLE',
                'enabled' => true,
                'id' => 'POI_PURCHA665CER_ITEM_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'ori_order_request_item_ri_received_items_1_name',
                'label' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_NAME_FIELD_TITLE',
                'enabled' => true,
                'id' => 'ORI_ORDER_6B32ST_ITEM_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
