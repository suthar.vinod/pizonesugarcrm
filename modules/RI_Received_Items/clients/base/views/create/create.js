({
  extendsFrom: 'CreateView',
  initialize: function (options) {
    this._super('initialize', [options]);
    this.model.on("change:ori_order_request_item_ri_received_items_1_name change:type_2", this.function_ORI, this);
    this.model.on("change:poi_purchase_order_item_ri_received_items_1_name change:type_2", this.function_POI, this);
  },

  function_ORI: function () {
    var ORIVal = this.model.get('ori_order_6b32st_item_ida');
    var type2c = this.model.get('type_2');
    if (ORIVal != "" && type2c == "ORI") {
      if (ORIVal != null) {
        var self = this;
        App.api.call("get", "rest/v10/ORI_Order_Request_Item/" + ORIVal + "?fields=quality_requirement,prod_product_ori_order_request_item_1prod_product_ida,po_purchase_order_ori_order_request_item_1po_purchase_order_ida", null, {
          success: function (Data) {
            var ORI_QR = Data.quality_requirement;
            var ORI_Prd = Data.prod_product_ori_order_request_item_1prod_product_ida;
            if (ORI_QR != "" && ORI_QR != "None") {
              app.alert.show('message-id', {
                level: 'warning',
                messages: 'There is a Quality Requirement for this.  Please review needed documentation prior to releasing the item(s) for use.',
                autoClose: false
              });
            }
            if (ORI_Prd != "") {
              App.api.call("get", "rest/v10/Prod_Product/" + ORI_Prd + "?fields=quality_requirement", null, {
                success: function (Data1) {
                  var Prd_QR = Data1.quality_requirement;
                  // console.log("Prd_QR data ==" + Prd_QR);
                  if (Prd_QR != "" && Prd_QR != "None") {
                    app.alert.show('message-id', {
                      level: 'warning',
                      messages: 'There is a Quality Requirement for this.  Please review needed documentation prior to releasing the item(s) for use.',
                      autoClose: false
                    });
                  }
                }
              });
            }
          }

        });
      }
    }
  },

  function_POI: function () {
    var POIVal = this.model.get('poi_purcha665cer_item_ida');
    var type2c = this.model.get('type_2');
    if (POIVal != "" && type2c == "POI") {
      if (POIVal != null) {
        var self = this;
        App.api.call("get", "rest/v10/POI_Purchase_Order_Item/" + POIVal + "?fields=prod_product_poi_purchase_order_item_1prod_product_ida", null, {
          success: function (Data) {
            var POI_Prd = Data.prod_product_poi_purchase_order_item_1prod_product_ida;
            if (POI_Prd != "") {
              App.api.call("get", "rest/v10/Prod_Product/" + POI_Prd + "?fields=quality_requirement", null, {
                success: function (Data1) {
                  var Prd_QR = Data1.quality_requirement;
                  // console.log("Prd_QR data ==" + Prd_QR);
                  if (Prd_QR != "" && Prd_QR != "None") {
                    app.alert.show('message-id', {
                      level: 'warning',
                      messages: 'There is a Quality Requirement for this.  Please review needed documentation prior to releasing the item(s) for use.',
                      autoClose: false
                    });
                  }
                }
              });
            }
          }

        });
      }
    }
  },

})