<?php
// created: 2021-08-04 19:56:13
$viewdefs['RI_Received_Items']['base']['view']['subpanel-for-ori_order_request_item-ori_order_request_item_ri_received_items_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'quantity_received',
          'label' => 'LBL_QUANTITY_RECEIVED',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'received_date',
          'label' => 'LBL_RECEIVED_DATE',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'assigned_user_id',
          ),
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'poi_purchase_order_item_ri_received_items_1_name',
          'label' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
          'enabled' => true,
          'id' => 'POI_PURCHA665CER_ITEM_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'ori_order_request_item_ri_received_items_1_name',
          'label' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
          'enabled' => true,
          'id' => 'ORI_ORDER_6B32ST_ITEM_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);