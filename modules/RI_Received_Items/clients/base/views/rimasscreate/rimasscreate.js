/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @extends View.Views.Base.CreateView
 */
({
    extendsFrom: 'CreateView',

    /**
     * Initialize the view and prepare the model with default button metadata
     * for the current layout.
     */
    initialize: function (options) {
        var self = this;
        self._super("initialize", [options]);
        self.model.set('viewtitle', app.lang.get('LBL_RI_MASS_CREATION', 'RI_Received_Items'));
        self.context.on('button:ri_mass_update_button:click', this.riMassCreate, this);
         
    },
 
 
    riMassCreate: function () {
        //console.log('aaa');
        var self = this;
        var allFields = self.getFields(self.module, self.model);
        var fieldsToValidate = {};
        for (var fieldKey in allFields) {
            if (app.acl.hasAccessToModel('edit', self.model, fieldKey)) {
                _.extend(fieldsToValidate, _.pick(allFields, fieldKey));
            }
        }
        self.model.doValidate(fieldsToValidate, _.bind(self.processRIMassCreate, self));
    },
     processRIMassCreate: function (isValid) {
        var self = this; 
        console.log('isValid',isValid)
        if (isValid) {
            $('a.rowaction.btn.btn-primary ').addClass('disabled');
            riIdsToSend = [];
            var awpErrorMsg = [];
            var tdErrorMsg = [];
            models = self.context.parent.get('mass_collection').models;

            var riPromises = []
            var riidsArr = []

            _.each(models, function (selectedModel) {
                console.log('selectedModel',selectedModel);
                var riid = selectedModel.get('id');
                var riName = selectedModel.get('name'); 
                console.log('riid',riid);
                console.log('riName',riName);
                riidsArr.push(riid);
 
                riPromises.push(riid);
            });

            Promise.all(riPromises).then(function (results) {
                console.log('riidsArr',riidsArr);             
                if (riidsArr.length > 0) {
                    //  Get this model's values
                    var valuesToSave = {};
                    var fieldsToIterate = self.meta.panels[1].fields;
                    
                    _.each(fieldsToIterate, function (field) {
                        valuesToSave[field.name] = (_.isUndefined(self.model.get(field.name))) ? '' : self.model.get(field.name);
                         valuesToSave['assigned_user_id'] =  self.model.get('assigned_user_id');
                    });            
                    var module = self.context.parent.get('module');                        
                    var dataToSend = { ids: riidsArr, values: valuesToSave };
                    self.saveRIs(dataToSend,module);
                }
            }.bind(this))
        }
    },
     
    saveRIs: function (dataToSend,module) {
        var self = this;
        console.log('dataToSend',dataToSend);
        //  Building URL
        var url = "";
        if(module == "POI_Purchase_Order_Item"){
            url = app.api.buildURL('ri_mass_create_poi/');
        }else if(module == "ORI_Order_Request_Item"){
            url = app.api.buildURL('ri_mass_create_ori/');
        }
        
        if(url !=""){
            app.alert.show("riMassCreation", {
                level: 'process',
                title: "Saving",
                autoClose: false,
            });
            app.api.call('create', url, { data: dataToSend }, {
                success: function (serverData) {
                    self.toggleButtons(true);
                    if (serverData.status) {
                        var messageLevel = 'success';
                    } else {
                        var messageLevel = 'error';
                    }
                    app.alert.dismiss('riMassCreation');
                    app.alert.show("rieMassCreation", {
                        level: messageLevel,
                        messages: serverData.Message,
                        autoClose: false, 
                    });
                    app.drawer.close();
                },
                error: function (serverData) {
                    self.toggleButtons(true);
                    app.alert.dismiss('riMassCreation');
                    console.log("In Error riMassCreation ", serverData);
                    app.alert.show("rieMassCreation", {
                        level: 'error',
                        messages: 'Saving failed',
                        autoClose: false,
                    });
                },
            });
        }
    }
})
