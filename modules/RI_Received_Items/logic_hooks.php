<?php

$hook_version = 1;
$hook_array = Array();
$hook_array['after_save'][] = array(
    1,
    'After Save Change Received Date',
    'custom/modules/RI_Received_Items/ChangePOIStatus.php',
    'ChangePOIStatusHook',
    'ChangePOIStatusMethod',
);
$hook_array['after_save'][] = array(
    10,
    'After Save Change ORI Status',
    'custom/modules/RI_Received_Items/ChangeORIStatus.php',
    'ChangeORIStatussHook',
    'ChangeORIStatusMethod',
);
$hook_array['before_save'][] = array(
    12,
    'After Save Change ORI Status',
    'custom/modules/RI_Received_Items/ReceivedItemFilterInII.php',
    'ReceivedItemFilterInIIHook',
    'ReceivedItemFilterInIIMethodBeforeSave',
);
$hook_array['after_relationship_add'][] = array(
    13,
    'After Save Change ORI Status',
    'custom/modules/RI_Received_Items/ReceivedItemFilterInII.php',
    'ReceivedItemFilterInIIHook',
    'ReceivedItemFilterInIIMethod',
);
$hook_array['after_relationship_delete'][] = array(
    13,
    'After Save Change ORI Status',
    'custom/modules/RI_Received_Items/ReceivedItemFilterInII.php',
    'ReceivedItemFilterInIIHook',
    'ReceivedItemFilterInIIMethod',
);
?>