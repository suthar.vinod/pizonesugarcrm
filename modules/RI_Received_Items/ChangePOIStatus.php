<?php

class ChangePOIStatusHook
{
    static $already_ran = false;
    function ChangePOIStatusMethod($bean, $event, $arguments)
    {
        if (isset($arguments['dataChanges']['quantity_received']) && $bean->poi_purcha665cer_item_ida != "") {
            global $db;
            if (self::$already_ran == true) return; //So that hook will only trigger once
            self::$already_ran = true;
            $purchase_order_item = BeanFactory::retrieveBean('POI_Purchase_Order_Item', $bean->poi_purcha665cer_item_ida);

            $purchase_order_item->load_relationship('po_purchase_order_poi_purchase_order_item_1');
            $purchase_orderID = $purchase_order_item->po_purchase_order_poi_purchase_order_item_1->get();

            $PO_Purchase_Order = BeanFactory::retrieveBean('PO_Purchase_Order', $purchase_orderID[0]);
            $POStatus = $PO_Purchase_Order->status_c;
            $purchase_order_item->load_relationship('poi_purchase_order_item_ri_received_items_1');
            $relatedRI = $purchase_order_item->poi_purchase_order_item_ri_received_items_1->get();

            array_push($relatedRI, $bean->id);
            $relatedRIIds = array_unique($relatedRI);

            $total = 0;
            for ($a = 0; $a < count($relatedRIIds); $a++) {
                $RI_Received_Items = BeanFactory::retrieveBean('RI_Received_Items', $relatedRIIds[$a]);
                $quantity_received = $RI_Received_Items->quantity_received;
                $total = $total + $quantity_received;
            }
            $unit_quantity_ordered = $purchase_order_item->unit_quantity_ordered;
            if (($POStatus == "Pending" || $POStatus == "Cancelled") && $unit_quantity_ordered != "") {
                $purchase_order_item->status = "Pending";
                $purchase_order_item->save();
            } elseif ($POStatus == "Submitted" && $purchase_order_item->order_date != "" && count($relatedRIIds) <= 0) {
                $purchase_order_item->status = "Ordered";
                $purchase_order_item->save();
            } elseif ($purchase_order_item->order_date != "" && $unit_quantity_ordered <= $total) {
                $purchase_order_item->status = "Inventory";
                $purchase_order_item->save();
            } elseif ($purchase_order_item->order_date != "" && count($relatedRIIds) > 0) {
                $purchase_order_item->status = "Partially Received";
                $purchase_order_item->save();
            }
            /**Bug fix #2324 PO Status issue : 25 April 2022 : Gsingh */
            $this->checkPOIStatusAndSetPoStatus($purchase_order_item);
        }
    }

    function checkPOIStatusAndSetPoStatus($purchase_order_item)
    {
        if ($purchase_order_item->po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida != "") {

            $poid = $purchase_order_item->po_purchase_order_poi_purchase_order_item_1po_purchase_order_ida;

            $currentPOBean = BeanFactory::retrieveBean('PO_Purchase_Order', $poid);
            $currentPOBean->load_relationship('po_purchase_order_poi_purchase_order_item_1');
            $currentPOBean->load_relationship('po_purchase_order_ori_order_request_item_1');
            /** Load poi and ori bean to get all the linked record of poi and ori */
            $relatedPOI = $currentPOBean->po_purchase_order_poi_purchase_order_item_1->get();
            $relatedORI = $currentPOBean->po_purchase_order_ori_order_request_item_1->get();
            /**linkedrecstatus array : To store the status valye of all the linked poi and ori records  */
            $linkedrecstatus = array();

            /*find all the lined poi record and get the status value */
            foreach ($relatedPOI as $POIid) {
                $clonedBeanPOI = BeanFactory::getBean('POI_Purchase_Order_Item', $POIid);
                $poi_status = $clonedBeanPOI->status;
                if ($poi_status != 'Inventory') {
                    $linkedrecstatus[] = $poi_status;
                }
            }
            /*find all the lined ori record and get the status value */
            foreach ($relatedORI as $ORIid) {
                $clonedBeanORI = BeanFactory::getBean('ORI_Order_Request_Item', $ORIid);
                $ori_status = $clonedBeanORI->status;
                if ($ori_status != 'Inventory') {
                    $linkedrecstatus[] = $ori_status;
                }
            }
            /**25 nov 2021 : #1778 : Status field on Purchase Orders to automatically update to read only 
             * Complete if all of the ORIs and POIs linked to the PO have Status = Fully Received. */
            if ((count($linkedrecstatus) == 0) && (count($relatedPOI) != 0 || count($relatedORI) != 0)) {
                /**To store the old value of status field , we are using status_old_c field */
                $currentPOBean->status_old_c = $currentPOBean->fetched_row['status_c'];
                $currentPOBean->status_c = "Complete";
                $currentPOBean->save();
            } else {
                /**In case status valye changed from full recevied to other then it will pull the old value to status field */
                if ($currentPOBean->status_old_c != '') {
                    $currentPOBean->status_c = $currentPOBean->status_old_c;
                    $currentPOBean->save();
                }
            }
        }
    }
}
