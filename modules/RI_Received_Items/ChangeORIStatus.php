<?php

class ChangeORIStatussHook {
    static $already_ran = false;
    function ChangeORIStatusMethod($bean, $event, $arguments) {
        global $db;
        if (self::$already_ran == true) return; //So that hook will only trigger once
        self::$already_ran = true;
        if (isset($arguments['dataChanges']['quantity_received']) && $bean->ori_order_6b32st_item_ida !="") 
        { 
            $order_request_item = BeanFactory::retrieveBean('ORI_Order_Request_Item',$bean->ori_order_6b32st_item_ida);

            $order_request_item->load_relationship('po_purchase_order_ori_order_request_item_1');
            $relatedPO = $order_request_item->po_purchase_order_ori_order_request_item_1->get();

            $order_request_item->load_relationship('ori_order_request_item_ri_received_items_1');
            $relatedRI = $order_request_item->ori_order_request_item_ri_received_items_1->get();
            
            array_push($relatedRI,$bean->id);
            $relatedRIIds = array_unique($relatedRI);
            $total = 0;
            for($a=0;$a<count($relatedRIIds);$a++){
                $RI_Received_Items = BeanFactory::retrieveBean('RI_Received_Items', $relatedRIIds[$a]);
                $quantity_received = $RI_Received_Items->quantity_received;             
                $total = $total+$quantity_received;            
            } 
            $request_date = $order_request_item->request_date;
            $unit_quantity_requested = $order_request_item->unit_quantity_requested;

            if ($request_date != ""  && count($relatedPO) > 0 && count($relatedRIIds) <= 0) {
                $order_request_item->status = "Ordered"; 
                $order_request_item->save();           
            } else if($request_date != ""  && count($relatedPO) <= 0 && count($relatedRIIds) <= 0) { 
                $order_request_item->status = "Requested";
                $order_request_item->save();            
            }  else  if(count($relatedRIIds) > 0 && $unit_quantity_requested <= $total){                
                $order_request_item->status = "Inventory";  
                $order_request_item->save();          
            } else if(count($relatedRIIds) > 0 && $unit_quantity_requested != "") { 
                $order_request_item->status = "Partially Received"; 
                $order_request_item->save();         
            }   
            $this->checkORIStatusAndSetPoStatus($order_request_item);
             
        }
    }

    function checkORIStatusAndSetPoStatus($order_request_item) {
        if($order_request_item->po_purchase_order_ori_order_request_item_1po_purchase_order_ida !=""){
           
            $poid = $order_request_item->po_purchase_order_ori_order_request_item_1po_purchase_order_ida;
             
            $currentPOBean = BeanFactory::retrieveBean('PO_Purchase_Order', $poid);
            $currentPOBean->load_relationship('po_purchase_order_poi_purchase_order_item_1');
            $currentPOBean->load_relationship('po_purchase_order_ori_order_request_item_1');
            /** Load poi and ori bean to get all the linked record of poi and ori */
            $relatedPOI = $currentPOBean->po_purchase_order_poi_purchase_order_item_1->get();
            $relatedORI = $currentPOBean->po_purchase_order_ori_order_request_item_1->get();
            /**linkedrecstatus array : To store the status valye of all the linked poi and ori records  */
            $linkedrecstatus = array();
            /*find all the lined poi record and get the status value */
            foreach ($relatedPOI as $POIid) {
                $clonedBeanPOI = BeanFactory::getBean('POI_Purchase_Order_Item', $POIid);
                $poi_status = $clonedBeanPOI->status;
                if ($poi_status != 'Inventory') {
                $linkedrecstatus[] = $poi_status;
                }
            }
            /*find all the lined ori record and get the status value */
            foreach ($relatedORI as $ORIid) {
                $clonedBeanORI = BeanFactory::getBean('ORI_Order_Request_Item', $ORIid);
                $ori_status = $clonedBeanORI->status;
                if ($ori_status != 'Inventory') {
                $linkedrecstatus[] = $ori_status;
                }
            }
            /**25 nov 2021 : #1778 : Status field on Purchase Orders to automatically update to read only 
             * Complete if all of the ORIs and POIs linked to the PO have Status = Fully Received. */
            if ((count($linkedrecstatus) == 0) && (count($relatedPOI) != 0 || count($relatedORI) != 0)) {
            /**To store the old value of status field , we are using status_old_c field */
                $currentPOBean->status_old_c = $currentPOBean->fetched_row['status_c'];
                $currentPOBean->status_c = "Complete";
                $currentPOBean->save();
            } else {
                /**In case status valye changed from full recevied to other then it will pull the old value to status field */
                if ($currentPOBean->status_old_c != '') {
                $currentPOBean->status_c = $currentPOBean->status_old_c;
                $currentPOBean->save();
                }
            }
             
        }
    }
} 