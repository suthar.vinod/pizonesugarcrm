<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RI_Received_Items/Ext/Vardefs/ori_order_request_item_ri_received_items_1_RI_Received_Items.php

// created: 2021-08-04 19:53:25
$dictionary["RI_Received_Items"]["fields"]["ori_order_request_item_ri_received_items_1"] = array (
  'name' => 'ori_order_request_item_ri_received_items_1',
  'type' => 'link',
  'relationship' => 'ori_order_request_item_ri_received_items_1',
  'source' => 'non-db',
  'module' => 'ORI_Order_Request_Item',
  'bean_name' => 'ORI_Order_Request_Item',
  'side' => 'right',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'id_name' => 'ori_order_6b32st_item_ida',
  'link-type' => 'one',
);
$dictionary["RI_Received_Items"]["fields"]["ori_order_request_item_ri_received_items_1_name"] = array (
  'name' => 'ori_order_request_item_ri_received_items_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_ORI_ORDER_REQUEST_ITEM_TITLE',
  'save' => true,
  'id_name' => 'ori_order_6b32st_item_ida',
  'link' => 'ori_order_request_item_ri_received_items_1',
  'table' => 'ori_order_request_item',
  'module' => 'ORI_Order_Request_Item',
  'rname' => 'name',
);
$dictionary["RI_Received_Items"]["fields"]["ori_order_6b32st_item_ida"] = array (
  'name' => 'ori_order_6b32st_item_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE_ID',
  'id_name' => 'ori_order_6b32st_item_ida',
  'link' => 'ori_order_request_item_ri_received_items_1',
  'table' => 'ori_order_request_item',
  'module' => 'ORI_Order_Request_Item',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/RI_Received_Items/Ext/Vardefs/poi_purchase_order_item_ri_received_items_1_RI_Received_Items.php

// created: 2021-08-04 19:45:33
$dictionary["RI_Received_Items"]["fields"]["poi_purchase_order_item_ri_received_items_1"] = array (
  'name' => 'poi_purchase_order_item_ri_received_items_1',
  'type' => 'link',
  'relationship' => 'poi_purchase_order_item_ri_received_items_1',
  'source' => 'non-db',
  'module' => 'POI_Purchase_Order_Item',
  'bean_name' => 'POI_Purchase_Order_Item',
  'side' => 'right',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'id_name' => 'poi_purcha665cer_item_ida',
  'link-type' => 'one',
);
$dictionary["RI_Received_Items"]["fields"]["poi_purchase_order_item_ri_received_items_1_name"] = array (
  'name' => 'poi_purchase_order_item_ri_received_items_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_POI_PURCHASE_ORDER_ITEM_TITLE',
  'save' => true,
  'id_name' => 'poi_purcha665cer_item_ida',
  'link' => 'poi_purchase_order_item_ri_received_items_1',
  'table' => 'poi_purchase_order_item',
  'module' => 'POI_Purchase_Order_Item',
  'rname' => 'name',
);
$dictionary["RI_Received_Items"]["fields"]["poi_purcha665cer_item_ida"] = array (
  'name' => 'poi_purcha665cer_item_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE_ID',
  'id_name' => 'poi_purcha665cer_item_ida',
  'link' => 'poi_purchase_order_item_ri_received_items_1',
  'table' => 'poi_purchase_order_item',
  'module' => 'POI_Purchase_Order_Item',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/RI_Received_Items/Ext/Vardefs/sugarfield_linked_ii_sum_flag_c.php

 // created: 2021-09-01 09:23:48
$dictionary['RI_Received_Items']['fields']['linked_ii_sum_flag_c']['labelValue']='Linked II Sum Flag';
$dictionary['RI_Received_Items']['fields']['linked_ii_sum_flag_c']['enforced']='';
$dictionary['RI_Received_Items']['fields']['linked_ii_sum_flag_c']['dependency']='';
$dictionary['RI_Received_Items']['fields']['linked_ii_sum_flag_c']['readonly']='1';
$dictionary['RI_Received_Items']['fields']['linked_ii_sum_flag_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/RI_Received_Items/Ext/Vardefs/sugarfield_name.php

 // created: 2021-08-04 20:04:20
$dictionary['RI_Received_Items']['fields']['name']['importable']='false';
$dictionary['RI_Received_Items']['fields']['name']['duplicate_merge']='disabled';
$dictionary['RI_Received_Items']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['RI_Received_Items']['fields']['name']['merge_filter']='disabled';
$dictionary['RI_Received_Items']['fields']['name']['unified_search']=false;
$dictionary['RI_Received_Items']['fields']['name']['calculated']='true';
$dictionary['RI_Received_Items']['fields']['name']['formula']='concat("RI ",related($poi_purchase_order_item_ri_received_items_1,"name"),related($ori_order_request_item_ri_received_items_1,"name")," ",toString($received_date))';
$dictionary['RI_Received_Items']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/RI_Received_Items/Ext/Vardefs/ri_received_items_ii_inventory_item_1_RI_Received_Items.php

// created: 2021-11-09 09:52:04
$dictionary["RI_Received_Items"]["fields"]["ri_received_items_ii_inventory_item_1"] = array (
  'name' => 'ri_received_items_ii_inventory_item_1',
  'type' => 'link',
  'relationship' => 'ri_received_items_ii_inventory_item_1',
  'source' => 'non-db',
  'module' => 'II_Inventory_Item',
  'bean_name' => 'II_Inventory_Item',
  'vname' => 'LBL_RI_RECEIVED_ITEMS_II_INVENTORY_ITEM_1_FROM_RI_RECEIVED_ITEMS_TITLE',
  'id_name' => 'ri_received_items_ii_inventory_item_1ri_received_items_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/RI_Received_Items/Ext/Vardefs/sugarfield_product_name_c.php

 // created: 2022-02-22 08:15:05
$dictionary['RI_Received_Items']['fields']['product_name_c']['duplicate_merge_dom_value']=0;
$dictionary['RI_Received_Items']['fields']['product_name_c']['labelValue']='Product Name';
$dictionary['RI_Received_Items']['fields']['product_name_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RI_Received_Items']['fields']['product_name_c']['calculated']='true';
$dictionary['RI_Received_Items']['fields']['product_name_c']['formula']='concat(related($poi_purchase_order_item_ri_received_items_1,"products_name_c"),related($ori_order_request_item_ri_received_items_1,"products_name_c"))';
$dictionary['RI_Received_Items']['fields']['product_name_c']['enforced']='true';
$dictionary['RI_Received_Items']['fields']['product_name_c']['dependency']='';
$dictionary['RI_Received_Items']['fields']['product_name_c']['required_formula']='';
$dictionary['RI_Received_Items']['fields']['product_name_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/RI_Received_Items/Ext/Vardefs/sugarfield_ori_order_request_item_ri_received_items_1_name.php

 // created: 2022-02-22 08:18:31
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['required']=true;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['audited']=true;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['massupdate']=true;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['hidemassupdate']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['duplicate_merge']='enabled';
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['duplicate_merge_dom_value']='1';
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['merge_filter']='disabled';
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['reportable']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['unified_search']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['calculated']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['dependency']='equal($type_2,"ORI")';
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['required_formula']='equal($type_2,"ORI")';
$dictionary['RI_Received_Items']['fields']['ori_order_request_item_ri_received_items_1_name']['vname']='LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/RI_Received_Items/Ext/Vardefs/sugarfield_ori_order_6b32st_item_ida.php

 // created: 2022-02-22 08:18:31
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['name']='ori_order_6b32st_item_ida';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['type']='id';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['source']='non-db';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['vname']='LBL_ORI_ORDER_REQUEST_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE_ID';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['id_name']='ori_order_6b32st_item_ida';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['link']='ori_order_request_item_ri_received_items_1';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['table']='ori_order_request_item';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['module']='ORI_Order_Request_Item';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['rname']='id';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['reportable']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['side']='right';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['massupdate']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['duplicate_merge']='disabled';
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['hideacl']=true;
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['audited']=false;
$dictionary['RI_Received_Items']['fields']['ori_order_6b32st_item_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/RI_Received_Items/Ext/Vardefs/sugarfield_poi_purchase_order_item_ri_received_items_1_name.php

 // created: 2022-02-22 08:19:05
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['required']=true;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['audited']=true;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['massupdate']=true;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['hidemassupdate']=false;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['duplicate_merge']='enabled';
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['duplicate_merge_dom_value']='1';
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['merge_filter']='disabled';
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['reportable']=false;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['unified_search']=false;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['calculated']=false;
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['dependency']='equal($type_2,"POI")';
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['required_formula']='equal($type_2,"POI")';
$dictionary['RI_Received_Items']['fields']['poi_purchase_order_item_ri_received_items_1_name']['vname']='LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/RI_Received_Items/Ext/Vardefs/sugarfield_poi_purcha665cer_item_ida.php

 // created: 2022-02-22 08:19:05
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['name']='poi_purcha665cer_item_ida';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['type']='id';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['source']='non-db';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['vname']='LBL_POI_PURCHASE_ORDER_ITEM_RI_RECEIVED_ITEMS_1_FROM_RI_RECEIVED_ITEMS_TITLE_ID';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['id_name']='poi_purcha665cer_item_ida';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['link']='poi_purchase_order_item_ri_received_items_1';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['table']='poi_purchase_order_item';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['module']='POI_Purchase_Order_Item';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['rname']='id';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['reportable']=false;
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['side']='right';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['massupdate']=false;
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['duplicate_merge']='disabled';
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['hideacl']=true;
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['audited']=false;
$dictionary['RI_Received_Items']['fields']['poi_purcha665cer_item_ida']['importable']='true';

 
?>
