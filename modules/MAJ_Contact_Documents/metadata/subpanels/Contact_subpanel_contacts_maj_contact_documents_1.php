<?php
// created: 2020-09-08 09:40:42
$subpanel_layout['list_fields'] = array (
  'document_name' => 
  array (
    'name' => 'document_name',
    'vname' => 'LBL_LIST_DOCUMENT_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'uploadfile' => 
  array (
    'type' => 'file',
    'vname' => 'LBL_FILE_UPLOAD',
    'width' => 10,
    'default' => true,
  ),
  'category_id' => 
  array (
    'type' => 'enum',
    'vname' => 'LBL_SF_CATEGORY',
    'width' => 10,
    'default' => true,
  ),
  'effective_date_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_EFFECTIVE_DATE',
    'width' => 10,
    'default' => true,
  ),
  'approved_prd_level_c' => 
  array (
    'type' => 'multienum',
    'default' => true,
    'vname' => 'LBL_APPROVED_PRD_LEVEL',
    'width' => 10,
  ),
);