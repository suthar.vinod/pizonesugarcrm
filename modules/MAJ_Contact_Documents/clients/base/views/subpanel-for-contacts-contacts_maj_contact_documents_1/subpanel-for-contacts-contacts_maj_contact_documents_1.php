<?php
// created: 2020-09-08 09:40:42
$viewdefs['MAJ_Contact_Documents']['base']['view']['subpanel-for-contacts-contacts_maj_contact_documents_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'document_name',
          'label' => 'LBL_NAME',
          'enabled' => true,
          'link' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'uploadfile',
          'label' => 'LBL_FILE_UPLOAD',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'category_id',
          'label' => 'LBL_SF_CATEGORY',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'effective_date_c',
          'label' => 'LBL_EFFECTIVE_DATE',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'approved_prd_level_c',
          'label' => 'LBL_APPROVED_PRD_LEVEL',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);