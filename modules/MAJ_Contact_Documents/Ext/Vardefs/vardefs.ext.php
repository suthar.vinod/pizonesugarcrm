<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/MAJ_Contact_Documents/Ext/Vardefs/sugarfield_document_name.php

 // created: 2019-08-15 11:30:28
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['audited']=true;
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['massupdate']=false;
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['duplicate_merge']='enabled';
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['duplicate_merge_dom_value']='1';
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['merge_filter']='disabled';
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['unified_search']=false;
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.82',
  'searchable' => true,
);
$dictionary['MAJ_Contact_Documents']['fields']['document_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/MAJ_Contact_Documents/Ext/Vardefs/sugarfield_category_id.php

 // created: 2019-08-15 11:31:35
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['required']=true;
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['audited']=true;
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['massupdate']=true;
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['options']='contact_document_list';
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['duplicate_merge']='enabled';
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['duplicate_merge_dom_value']='1';
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['merge_filter']='disabled';
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['reportable']=true;
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['unified_search']=false;
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['calculated']=false;
$dictionary['MAJ_Contact_Documents']['fields']['category_id']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/MAJ_Contact_Documents/Ext/Vardefs/sugarfield_effective_date_c.php

 // created: 2019-08-15 11:32:03
$dictionary['MAJ_Contact_Documents']['fields']['effective_date_c']['labelValue']='Effective Date';
$dictionary['MAJ_Contact_Documents']['fields']['effective_date_c']['enforced']='';
$dictionary['MAJ_Contact_Documents']['fields']['effective_date_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/MAJ_Contact_Documents/Ext/Vardefs/contacts_maj_contact_documents_1_MAJ_Contact_Documents.php

// created: 2019-08-15 11:34:01
$dictionary["MAJ_Contact_Documents"]["fields"]["contacts_maj_contact_documents_1"] = array (
  'name' => 'contacts_maj_contact_documents_1',
  'type' => 'link',
  'relationship' => 'contacts_maj_contact_documents_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_MAJ_CONTACT_DOCUMENTS_TITLE',
  'id_name' => 'contacts_maj_contact_documents_1contacts_ida',
  'link-type' => 'one',
);
$dictionary["MAJ_Contact_Documents"]["fields"]["contacts_maj_contact_documents_1_name"] = array (
  'name' => 'contacts_maj_contact_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_maj_contact_documents_1contacts_ida',
  'link' => 'contacts_maj_contact_documents_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["MAJ_Contact_Documents"]["fields"]["contacts_maj_contact_documents_1contacts_ida"] = array (
  'name' => 'contacts_maj_contact_documents_1contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_MAJ_CONTACT_DOCUMENTS_1_FROM_MAJ_CONTACT_DOCUMENTS_TITLE_ID',
  'id_name' => 'contacts_maj_contact_documents_1contacts_ida',
  'link' => 'contacts_maj_contact_documents_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/MAJ_Contact_Documents/Ext/Vardefs/sugarfield_approved_prd_level_c.php

 // created: 2020-09-08 11:29:39
$dictionary['MAJ_Contact_Documents']['fields']['approved_prd_level_c']['labelValue']='Approved PRD Level';
$dictionary['MAJ_Contact_Documents']['fields']['approved_prd_level_c']['visibility_grid']=array (
  'trigger' => 'category_id',
  'values' => 
  array (
    '' => 
    array (
    ),
    'CV' => 
    array (
    ),
    'CV and PRD' => 
    array (
      0 => '',
      1 => 'Assistant',
      2 => 'Primary',
    ),
    'PRD' => 
    array (
      0 => '',
      1 => 'Assistant',
      2 => 'Primary',
    ),
  ),
);

 
?>
