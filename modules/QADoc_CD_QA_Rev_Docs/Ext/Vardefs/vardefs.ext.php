<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/QADoc_CD_QA_Rev_Docs/Ext/Vardefs/qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1_QADoc_CD_QA_Rev_Docs.php

// created: 2021-02-11 07:50:25
$dictionary["QADoc_CD_QA_Rev_Docs"]["fields"]["qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1"] = array (
  'name' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1',
  'type' => 'link',
  'relationship' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1',
  'source' => 'non-db',
  'module' => 'QARev_CD_QA_Reviews',
  'bean_name' => 'QARev_CD_QA_Reviews',
  'side' => 'right',
  'vname' => 'LBL_QAREV_CD_QA_REVIEWS_QADOC_CD_QA_REV_DOCS_1_FROM_QADOC_CD_QA_REV_DOCS_TITLE',
  'id_name' => 'qarev_cd_q5440reviews_ida',
  'link-type' => 'one',
);
$dictionary["QADoc_CD_QA_Rev_Docs"]["fields"]["qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1_name"] = array (
  'name' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_QAREV_CD_QA_REVIEWS_QADOC_CD_QA_REV_DOCS_1_FROM_QAREV_CD_QA_REVIEWS_TITLE',
  'save' => true,
  'id_name' => 'qarev_cd_q5440reviews_ida',
  'link' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1',
  'table' => 'qarev_cd_qa_reviews',
  'module' => 'QARev_CD_QA_Reviews',
  'rname' => 'name',
);
$dictionary["QADoc_CD_QA_Rev_Docs"]["fields"]["qarev_cd_q5440reviews_ida"] = array (
  'name' => 'qarev_cd_q5440reviews_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_QAREV_CD_QA_REVIEWS_QADOC_CD_QA_REV_DOCS_1_FROM_QADOC_CD_QA_REV_DOCS_TITLE_ID',
  'id_name' => 'qarev_cd_q5440reviews_ida',
  'link' => 'qarev_cd_qa_reviews_qadoc_cd_qa_rev_docs_1',
  'table' => 'qarev_cd_qa_reviews',
  'module' => 'QARev_CD_QA_Reviews',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
