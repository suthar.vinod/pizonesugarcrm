<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/wpa_subpanel.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/wpaSubpanel/JSGroupings/RemoveWPASubpanelButtons.js'] = 'include/javascript/sugar_grp7.min.js';

        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/wpaMassCreateExtension.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/wpaCreation/JSGroupings/wpaMassCreateExtension.js'] = 'include/javascript/sugar_grp7.min.js';
		}
        break;
    }
}
?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/wpaCreateExtension1.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/wpaCreation/JSGroupings/wpaCreateExtension1.js'] = 'include/javascript/sugar_grp7.min.js';
        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/service_price_subpanel.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/ServicePriceSubpanel/JSGroupings/RemoveSPSubpanelButtons.js'] = 'include/javascript/sugar_grp7.min.js';

        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/ValidationCheckDateBeforeSaveWPD.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/WPDCustomization/JSGroupings/WPDCreateViewExtension.js'] = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/piz_system/WPDCustomization/JSGroupings/WPDRecordViewExtension.js'] = 'include/javascript/sugar_grp7.min.js';
        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/w_weightCreateView.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/w_weightCreateView/w_weightCreateView.js'] = 'include/javascript/sugar_grp7.min.js';
        }

        break;
    }
}
?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/M03_Work_Product_Deliverable_CreateView.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/Work_Product_DeliverableCreateView/M03_Work_Product_Deliverable_CreateView.js'] = 'include/javascript/sugar_grp7.min.js';
        }

        break;
    }
}
?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/PopulateTDSubpanelsFromTS.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/PopulateTDSubpanelsFromTS/JSGroupings/RemoveTDSubpanelCreateButton.js'] = 'include/javascript/sugar_grp7.min.js';            
        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/TaskDesignCustomization.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/TaskDesignCustomization/JSGroupings/RemoveIISubpanelButtons.js'] = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/TaskDesignCustomization/JSGroupings/RemoveTDSubpanelButtons.js'] = 'include/javascript/sugar_grp7.min.js';
        }
        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/PopulateInventorySubpanelsFromWorkProduct.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/PopulateInventorySubpanelsFromWorkProduct/JSGroupings/WorkProductHideInventorySubpanels.js'] = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/PopulateInventorySubpanelsFromWorkProduct/JSGroupings/RemoveIISubpanelCreateButton.js']      = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/PopulateInventorySubpanelsFromWorkProduct/JSGroupings/RemoveIMSubpanelCreateButton.js']      = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/PopulateInventorySubpanelsFromWorkProduct/JSGroupings/RemoveTDSubpanelCreateButton.js']      = 'include/javascript/sugar_grp7.min.js';
        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/ORICustomizations.php

//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/ORI/JSGroupings/ORICreateCustomization.js']   = 'include/javascript/sugar_grp7.min.js';
        }
        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/addCopyButtonOrderRequest.php

//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
			  $js_groupings[$key]['custom/src/piz_system/orderRequestAddCopyButton/JSGroupings/orderRequestAddCopyButtonOnRecordView.js']             = 'include/javascript/sugar_grp7.min.js';               
        }
        break;
    }
}
?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/poiCreateViewCustomizations.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/PurchaseOrderItemCreateView/purchaseOrderItemCreateViewExtension.js'] = 'include/javascript/sugar_grp7.min.js';
        }

        break;
    }
}
?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/CopyAllButton.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings)
{
    foreach  ($groupings as $file => $target)
    {
    	//if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js')
        {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/GDCustomization/JSGroupings/CopyAllButton.js'] = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/piz_system/GDCustomization/JSGroupings/HandlebarCopyAllHelper.js'] = 'include/javascript/sugar_grp7.min.js';
           
        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/InventoryManagementRecordViewExtension.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/InventoryManagementRecordViewExtension.js'] = 'include/javascript/sugar_grp7.min.js';

        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/Label_IC_ConfigurationAndPrinting.php

//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/Label_IC_ConfigurationAndPrinting/JSGroupings/InventoryCollectionAddPrintButton.js'] = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/piz_system/Label_IC_ConfigurationAndPrinting/JSGroupings/InventoryCollectionAddPrintButtonOnRecordView.js'] = 'include/javascript/sugar_grp7.min.js';
        }
        break;
    }
}
?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/InventoryManagementCustomizations.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/RemoveWorkProductsSubpanelButtons.js']   = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/RemoveIMSubpanelButtons.js']             = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/ExtendInventoryManagementCreateView.js'] = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/AddFlagToFilterIMSelectionRecords.js']   = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/HideUnnecessarySubpanels.js']            = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/IIHideLinkDropdown.js']                  = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementCustomizations/JSGroupings/RemoveIISubpanelButtons.js']             = 'include/javascript/sugar_grp7.min.js';
        }
        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/MultiSelectionLinkOverride.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings)
{
    foreach  ($groupings as $file => $target)
    {
    	//if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js')
        {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementLinkMultipleItems/JSGroupings/MultiSelectionLinkOverride.js'] = 'include/javascript/sugar_grp7.min.js';          
        }

        break;
    }
}
 
?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/InventoryItemRecordViewExtension.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/InventoryItemCreation/JSGroupings/InventoryItemRecordViewExtension.js'] = 'include/javascript/sugar_grp7.min.js';

        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/InventoryItemCreateExtension.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/InventoryItemCreation/JSGroupings/InventoryItemCreateExtension.js'] = 'include/javascript/sugar_grp7.min.js';

        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/LabelConfigurationAndPrinting.php

//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
			$js_groupings[$key]['custom/src/piz_system/LabelConfigurationAndPrinting/JSGroupings/AddJsBarcode.js']                 = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/piz_system/LabelConfigurationAndPrinting/JSGroupings/InventoryItemAddPrintButton.js']             = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/piz_system/LabelConfigurationAndPrinting/JSGroupings/InventoryItemAddPrintButtonOnRecordView.js'] = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/piz_system/LabelConfigurationAndPrinting/JSGroupings/HandlebarBarcodeHelper.js']                  = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/piz_system/LabelConfigurationAndPrinting/JSGroupings/LabelTemplateHeaderpane.js']                 = 'include/javascript/sugar_grp7.min.js';
        }
        break;
    }
}
?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/AddScanButton.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings)
{
    foreach  ($groupings as $file => $target)
    {
    	//if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js')
        {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/InventoryManagementLinkMultipleItems/JSGroupings/AddScanButton.js'] = 'include/javascript/sugar_grp7.min.js';
           
        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/InventoryItemSubpanelsRemoveSubpanel.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/InventoryItemCreation/JSGroupings/InventoryItemSubpanelsRemoveSubpanel.js'] = 'include/javascript/sugar_grp7.min.js';
            $js_groupings[$key]['custom/src/wsystems/InventoryItemCustomization/JSGroupings/RemoveIISubpanelButtonsORIPOI.js'] = 'include/javascript/sugar_grp7.min.js';
        }

        break;
    }
}
?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/AddFlagToFilterProductOnORI.php

//Loop through the groupings to find grouping file you want to append to
// foreach ($js_groupings as $key => $groupings) {
//     foreach ($groupings as $file => $target) {
//         //if the target grouping is found
//         if ($target == 'include/javascript/sugar_grp7.min.js') {
//             //append the custom JavaScript file
//             $js_groupings[$key]['custom/src/wsystems/ORI/JSGroupings/AddFlagToFilterProductOnORI.js']   = 'include/javascript/sugar_grp7.min.js';
//         }
//         break;
//     }
// }

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/RemoveCreateBtnFromWPDSubpanel.php

//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file         
            $js_groupings[$key]['custom/src/piz_system/SubpanleCreateBtnCustomization/JSGroupings/RemoveIISubpanelCreateButton.js'] = 'include/javascript/sugar_grp7.min.js';            
            $js_groupings[$key]['custom/src/piz_system/SubpanleCreateBtnCustomization/JSGroupings/RemoveICSubpanelCreateButton.js'] = 'include/javascript/sugar_grp7.min.js';            
        }
        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/RiCreateViewCustomizations.php

//Loop through the groupings to find grouping file you want to append to
// foreach ($js_groupings as $key => $groupings) {
//     foreach ($groupings as $file => $target) {
//         //if the target grouping is found
//         if ($target == 'include/javascript/sugar_grp7.min.js') {
//             //append the custom JavaScript file
//             $js_groupings[$key]['custom/src/piz_system/ReceivedItemCreateView/JSGroupings/receivedItemCreateViewExtension.js'] = 'include/javascript/sugar_grp7.min.js';
//         }
//         break;
//     }
// }

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/ReceivedItemsCreateExtension.php

//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/ReceivedItemCreateCustomization/JSGroupings/receivedItemCreateExtension.js'] = 'include/javascript/sugar_grp7.min.js';
        }
        break;
    }
}
?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/RemoveCreateBtnFromProdProductSubpanel.php

//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file         
            $js_groupings[$key]['custom/src/piz_system/SubpanleCreateBtnCustomization/JSGroupings/RemoveORISubpanelCreateButton.js'] = 'include/javascript/sugar_grp7.min.js';            
            $js_groupings[$key]['custom/src/piz_system/SubpanleCreateBtnCustomization/JSGroupings/RemovePOISubpanelCreateButton.js'] = 'include/javascript/sugar_grp7.min.js';            
        }
        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/RemoveCreateBtnFromBatchIdSubpanel.php

//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file         
            $js_groupings[$key]['custom/src/piz_system/SubpanleCreateBtnCustomization/JSGroupings/RemoveCreateButtonUnderBatchIDSubpanel.js'] = 'include/javascript/sugar_grp7.min.js';            
            $js_groupings[$key]['custom/src/piz_system/SubpanleCreateBtnCustomization/JSGroupings/RemoveCreateButtonUnderComSubpanel.js'] = 'include/javascript/sugar_grp7.min.js';            
        }
        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/CopyAllBDButton.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings)
{
    foreach  ($groupings as $file => $target)
    {
    	//if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js')
        {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/Batch_Design_Customization/JSGroupings/CopyAllBDButton.js'] = 'include/javascript/sugar_grp7.min.js';
           
        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/BatchIDFromWorkProduct.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/wsystems/BatchIDCreateViewCustomization/JSGroupings/BatchIDCreateView.js'] = 'include/javascript/sugar_grp7.min.js';
        }
        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/CopyAllTOWPButton.php


//Loop through the groupings to find grouping file you want to append to
foreach ($js_groupings as $key => $groupings)
{
    foreach  ($groupings as $file => $target)
    {
    	//if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js')
        {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/src/piz_system/WP_Customization/JSGroupings/CopyAllTOWPButton.js'] = 'include/javascript/sugar_grp7.min.js';
           
        }

        break;
    }
}

?>
