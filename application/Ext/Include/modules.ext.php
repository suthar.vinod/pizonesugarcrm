<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Include/Work_Product_Personnel.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['WPP_Work_Product_Personnel'] = 'WPP_Work_Product_Personnel';
$beanFiles['WPP_Work_Product_Personnel'] = 'modules/WPP_Work_Product_Personnel/WPP_Work_Product_Personnel.php';
$moduleList[] = 'WPP_Work_Product_Personnel';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Sales.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['M01_Sales'] = 'M01_Sales';
$beanFiles['M01_Sales'] = 'modules/M01_Sales/M01_Sales.php';
$moduleList[] = 'M01_Sales';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Sales_Activity_Division_Department.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['M02_SA_Division_Department_'] = 'M02_SA_Division_Department_';
$beanFiles['M02_SA_Division_Department_'] = 'modules/M02_SA_Division_Department_/M02_SA_Division_Department_.php';
$moduleList[] = 'M02_SA_Division_Department_';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Work_Products.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['M03_Work_Product'] = 'M03_Work_Product';
$beanFiles['M03_Work_Product'] = 'modules/M03_Work_Product/M03_Work_Product.php';
$moduleList[] = 'M03_Work_Product';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Common.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['m06_Note_To_File'] = 'm06_Note_To_File';
$beanFiles['m06_Note_To_File'] = 'modules/m06_Note_To_File/m06_Note_To_File.php';
$moduleList[] = 'm06_Note_To_File';
$beanList['m06_APS_Communication'] = 'm06_APS_Communication';
$beanFiles['m06_APS_Communication'] = 'modules/m06_APS_Communication/m06_APS_Communication.php';
$moduleList[] = 'm06_APS_Communication';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Sales_Management.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['M01_Sales_Activity_Quote'] = 'M01_Sales_Activity_Quote';
$beanFiles['M01_Sales_Activity_Quote'] = 'modules/M01_Sales_Activity_Quote/M01_Sales_Activity_Quote.php';
$moduleList[] = 'M01_Sales_Activity_Quote';
$beanList['M01_Quote_Document'] = 'M01_Quote_Document';
$beanFiles['M01_Quote_Document'] = 'modules/M01_Quote_Document/M01_Quote_Document.php';
$moduleList[] = 'M01_Quote_Document';
$beanList['M01_SA_Division_Department'] = 'M01_SA_Division_Department';
$beanFiles['M01_SA_Division_Department'] = 'modules/M01_SA_Division_Department/M01_SA_Division_Department.php';
$moduleList[] = 'M01_SA_Division_Department';
$beanList['M01_Sales_Activity'] = 'M01_Sales_Activity';
$beanFiles['M01_Sales_Activity'] = 'modules/M01_Sales_Activity/M01_Sales_Activity.php';
$moduleList[] = 'M01_Sales_Activity';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Errors.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['M06_Error'] = 'M06_Error';
$beanFiles['M06_Error'] = 'modules/M06_Error/M06_Error.php';
$moduleList[] = 'M06_Error';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Time_Keeping.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['ABC12_Work_Product_Activities'] = 'ABC12_Work_Product_Activities';
$beanFiles['ABC12_Work_Product_Activities'] = 'modules/ABC12_Work_Product_Activities/ABC12_Work_Product_Activities.php';
$moduleList[] = 'ABC12_Work_Product_Activities';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Activity_Notes.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['AN01_Activity_Notes'] = 'AN01_Activity_Notes';
$beanFiles['AN01_Activity_Notes'] = 'modules/AN01_Activity_Notes/AN01_Activity_Notes.php';
$moduleList[] = 'AN01_Activity_Notes';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Quality_Assurance.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['A1A_CPI_Findings'] = 'A1A_CPI_Findings';
$beanFiles['A1A_CPI_Findings'] = 'modules/A1A_CPI_Findings/A1A_CPI_Findings.php';
$moduleList[] = 'A1A_CPI_Findings';
$beanList['A1A_Critical_Phase_Inspectio'] = 'A1A_Critical_Phase_Inspectio';
$beanFiles['A1A_Critical_Phase_Inspectio'] = 'modules/A1A_Critical_Phase_Inspectio/A1A_Critical_Phase_Inspectio.php';
$moduleList[] = 'A1A_Critical_Phase_Inspectio';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/HR_Management.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['HR01_Resumes'] = 'HR01_Resumes';
$beanFiles['HR01_Resumes'] = 'modules/HR01_Resumes/HR01_Resumes.php';
$moduleList[] = 'HR01_Resumes';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/IntelliDocs.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['idoc_signers'] = 'idoc_signers';
$beanFiles['idoc_signers'] = 'modules/idoc_signers/idoc_signers.php';
$modules_exempt_from_availability_check['idoc_signers'] = 'idoc_signers';
$report_include_modules['idoc_signers'] = 'idoc_signers';
$modInvisList[] = 'idoc_signers';
$beanList['idoc_events'] = 'idoc_events';
$beanFiles['idoc_events'] = 'modules/idoc_events/idoc_events.php';
$modules_exempt_from_availability_check['idoc_events'] = 'idoc_events';
$report_include_modules['idoc_events'] = 'idoc_events';
$modInvisList[] = 'idoc_events';
$beanList['idoc_documents'] = 'idoc_documents';
$beanFiles['idoc_documents'] = 'modules/idoc_documents/idoc_documents.php';
$modules_exempt_from_availability_check['idoc_documents'] = 'idoc_documents';
$report_include_modules['idoc_documents'] = 'idoc_documents';
$modInvisList[] = 'idoc_documents';
$beanList['idoc_templates'] = 'idoc_templates';
$beanFiles['idoc_templates'] = 'modules/idoc_templates/idoc_templates.php';
$modules_exempt_from_availability_check['idoc_templates'] = 'idoc_templates';
$report_include_modules['idoc_templates'] = 'idoc_templates';
$modInvisList[] = 'idoc_templates';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Animals.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['ANML_Animals'] = 'ANML_Animals';
$beanFiles['ANML_Animals'] = 'modules/ANML_Animals/ANML_Animals.php';
$moduleList[] = 'ANML_Animals';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Work_Product_Enrollment.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['WPE_Work_Product_Enrollment'] = 'WPE_Work_Product_Enrollment';
$beanFiles['WPE_Work_Product_Enrollment'] = 'modules/WPE_Work_Product_Enrollment/WPE_Work_Product_Enrollment.php';
$moduleList[] = 'WPE_Work_Product_Enrollment';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Tradeshow_Management.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['TM_Tradeshow_Management'] = 'TM_Tradeshow_Management';
$beanFiles['TM_Tradeshow_Management'] = 'modules/TM_Tradeshow_Management/TM_Tradeshow_Management.php';
$moduleList[] = 'TM_Tradeshow_Management';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Visitor_Management.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['VM01_Visitor_Management'] = 'VM01_Visitor_Management';
$beanFiles['VM01_Visitor_Management'] = 'modules/VM01_Visitor_Management/VM01_Visitor_Management.php';
$moduleList[] = 'VM01_Visitor_Management';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Equipment.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['Equip_Equipment'] = 'Equip_Equipment';
$beanFiles['Equip_Equipment'] = 'modules/Equip_Equipment/Equip_Equipment.php';
$moduleList[] = 'Equip_Equipment';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Error_Employees.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['Ere_Error_Employees'] = 'Ere_Error_Employees';
$beanFiles['Ere_Error_Employees'] = 'modules/Ere_Error_Employees/Ere_Error_Employees.php';
$moduleList[] = 'Ere_Error_Employees';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Error_Documents.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['Erd_Error_Documents'] = 'Erd_Error_Documents';
$beanFiles['Erd_Error_Documents'] = 'modules/Erd_Error_Documents/Erd_Error_Documents.php';
$moduleList[] = 'Erd_Error_Documents';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Protocol_Amendments.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['M99_Protocol_Amendments'] = 'M99_Protocol_Amendments';
$beanFiles['M99_Protocol_Amendments'] = 'modules/M99_Protocol_Amendments/M99_Protocol_Amendments.php';
$moduleList[] = 'M99_Protocol_Amendments';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Acquired_Companies.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['AC01_Acquired_Companies'] = 'AC01_Acquired_Companies';
$beanFiles['AC01_Acquired_Companies'] = 'modules/AC01_Acquired_Companies/AC01_Acquired_Companies.php';
$moduleList[] = 'AC01_Acquired_Companies';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Error_Departments.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['ED_Errors_Department'] = 'ED_Errors_Department';
$beanFiles['ED_Errors_Department'] = 'modules/ED_Errors_Department/ED_Errors_Department.php';
$moduleList[] = 'ED_Errors_Department';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Tradeshow_Activities.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['TA_Tradeshow_Activities'] = 'TA_Tradeshow_Activities';
$beanFiles['TA_Tradeshow_Activities'] = 'modules/TA_Tradeshow_Activities/TA_Tradeshow_Activities.php';
$moduleList[] = 'TA_Tradeshow_Activities';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Anatomy_Database.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['AD_Anatomy_Database'] = 'AD_Anatomy_Database';
$beanFiles['AD_Anatomy_Database'] = 'modules/AD_Anatomy_Database/AD_Anatomy_Database.php';
$moduleList[] = 'AD_Anatomy_Database';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Error_QC_Employees.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['ErQC_Error_QC_Employees'] = 'ErQC_Error_QC_Employees';
$beanFiles['ErQC_Error_QC_Employees'] = 'modules/ErQC_Error_QC_Employees/ErQC_Error_QC_Employees.php';
$moduleList[] = 'ErQC_Error_QC_Employees';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Deviation_Employees.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['DE_Deviation_Employees'] = 'DE_Deviation_Employees';
$beanFiles['DE_Deviation_Employees'] = 'modules/DE_Deviation_Employees/DE_Deviation_Employees.php';
$moduleList[] = 'DE_Deviation_Employees';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Study_Workflow.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['SW_Study_Workflow'] = 'SW_Study_Workflow';
$beanFiles['SW_Study_Workflow'] = 'modules/SW_Study_Workflow/SW_Study_Workflow.php';
$moduleList[] = 'SW_Study_Workflow';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Company_Documents.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['CD_Company_Documents'] = 'CD_Company_Documents';
$beanFiles['CD_Company_Documents'] = 'modules/CD_Company_Documents/CD_Company_Documents.php';
$moduleList[] = 'CD_Company_Documents';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Tradeshow_Documents.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['TD_Tradeshow_Documents'] = 'TD_Tradeshow_Documents';
$beanFiles['TD_Tradeshow_Documents'] = 'modules/TD_Tradeshow_Documents/TD_Tradeshow_Documents.php';
$moduleList[] = 'TD_Tradeshow_Documents';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Room.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['RMS_Room'] = 'RMS_Room';
$beanFiles['RMS_Room'] = 'modules/RMS_Room/RMS_Room.php';
$moduleList[] = 'RMS_Room';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Equipment_Facility_Records.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['EFR_Equipment_Facility_Recor'] = 'EFR_Equipment_Facility_Recor';
$beanFiles['EFR_Equipment_Facility_Recor'] = 'modules/EFR_Equipment_Facility_Recor/EFR_Equipment_Facility_Recor.php';
$moduleList[] = 'EFR_Equipment_Facility_Recor';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Room_Transfer.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['RT_Room_Transfer'] = 'RT_Room_Transfer';
$beanFiles['RT_Room_Transfer'] = 'modules/RT_Room_Transfer/RT_Room_Transfer.php';
$moduleList[] = 'RT_Room_Transfer';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Equipment_Facility_Document.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['EFD_Equipment_Facility_Doc'] = 'EFD_Equipment_Facility_Doc';
$beanFiles['EFD_Equipment_Facility_Doc'] = 'modules/EFD_Equipment_Facility_Doc/EFD_Equipment_Facility_Doc.php';
$moduleList[] = 'EFD_Equipment_Facility_Doc';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Equipment_Facility_Service.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['EFS_Equipment_Facility_Servi'] = 'EFS_Equipment_Facility_Servi';
$beanFiles['EFS_Equipment_Facility_Servi'] = 'modules/EFS_Equipment_Facility_Servi/EFS_Equipment_Facility_Servi.php';
$moduleList[] = 'EFS_Equipment_Facility_Servi';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Company_Address.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['CA_Company_Address'] = 'CA_Company_Address';
$beanFiles['CA_Company_Address'] = 'modules/CA_Company_Address/CA_Company_Address.php';
$moduleList[] = 'CA_Company_Address';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Regulatory_Response.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['RR_Regulatory_Response'] = 'RR_Regulatory_Response';
$beanFiles['RR_Regulatory_Response'] = 'modules/RR_Regulatory_Response/RR_Regulatory_Response.php';
$moduleList[] = 'RR_Regulatory_Response';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Regulatory_Response_Document.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['RRD_Regulatory_Response_Doc'] = 'RRD_Regulatory_Response_Doc';
$beanFiles['RRD_Regulatory_Response_Doc'] = 'modules/RRD_Regulatory_Response_Doc/RRD_Regulatory_Response_Doc.php';
$moduleList[] = 'RRD_Regulatory_Response_Doc';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Contact_Documents.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['MAJ_Contact_Documents'] = 'MAJ_Contact_Documents';
$beanFiles['MAJ_Contact_Documents'] = 'modules/MAJ_Contact_Documents/MAJ_Contact_Documents.php';
$moduleList[] = 'MAJ_Contact_Documents';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Email_Documents.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['EDoc_Email_Documents'] = 'EDoc_Email_Documents';
$beanFiles['EDoc_Email_Documents'] = 'modules/EDoc_Email_Documents/EDoc_Email_Documents.php';
$moduleList[] = 'EDoc_Email_Documents';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Email_Template.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['ET_Email_Template'] = 'ET_Email_Template';
$beanFiles['ET_Email_Template'] = 'modules/ET_Email_Template/ET_Email_Template.php';
$moduleList[] = 'ET_Email_Template';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Company_Name.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['CN_Company_Name'] = 'CN_Company_Name';
$beanFiles['CN_Company_Name'] = 'modules/CN_Company_Name/CN_Company_Name.php';
$moduleList[] = 'CN_Company_Name';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Test_System_Documents.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['TSdoc_Test_System_Documents'] = 'TSdoc_Test_System_Documents';
$beanFiles['TSdoc_Test_System_Documents'] = 'modules/TSdoc_Test_System_Documents/TSdoc_Test_System_Documents.php';
$moduleList[] = 'TSdoc_Test_System_Documents';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Units.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['U_Units'] = 'U_Units';
$beanFiles['U_Units'] = 'modules/U_Units/U_Units.php';
$moduleList[] = 'U_Units';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Species.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['S_Species'] = 'S_Species';
$beanFiles['S_Species'] = 'modules/S_Species/S_Species.php';
$moduleList[] = 'S_Species';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Weight.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['W_Weight'] = 'W_Weight';
$beanFiles['W_Weight'] = 'modules/W_Weight/W_Weight.php';
$moduleList[] = 'W_Weight';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Clinical_Observation.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['CO_Clinical_Observation'] = 'CO_Clinical_Observation';
$beanFiles['CO_Clinical_Observation'] = 'modules/CO_Clinical_Observation/CO_Clinical_Observation.php';
$moduleList[] = 'CO_Clinical_Observation';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Clinical_Issue_Exam.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['CIE_Clinical_Issue_Exam'] = 'CIE_Clinical_Issue_Exam';
$beanFiles['CIE_Clinical_Issue_Exam'] = 'modules/CIE_Clinical_Issue_Exam/CIE_Clinical_Issue_Exam.php';
$moduleList[] = 'CIE_Clinical_Issue_Exam';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Service_Vendor.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['SV_Service_Vendor'] = 'SV_Service_Vendor';
$beanFiles['SV_Service_Vendor'] = 'modules/SV_Service_Vendor/SV_Service_Vendor.php';
$moduleList[] = 'SV_Service_Vendor';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Product.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['Prod_Product'] = 'Prod_Product';
$beanFiles['Prod_Product'] = 'modules/Prod_Product/Prod_Product.php';
$moduleList[] = 'Prod_Product';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/GDP_Examples.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['GE_GDP_Examples'] = 'GE_GDP_Examples';
$beanFiles['GE_GDP_Examples'] = 'modules/GE_GDP_Examples/GE_GDP_Examples.php';
$moduleList[] = 'GE_GDP_Examples';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Product_Document.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['ProDo_Product_Document'] = 'ProDo_Product_Document';
$beanFiles['ProDo_Product_Document'] = 'modules/ProDo_Product_Document/ProDo_Product_Document.php';
$moduleList[] = 'ProDo_Product_Document';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Work_Product_Management.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['M03_Additional_WP_Personnel'] = 'M03_Additional_WP_Personnel';
$beanFiles['M03_Additional_WP_Personnel'] = 'modules/M03_Additional_WP_Personnel/M03_Additional_WP_Personnel.php';
$moduleList[] = 'M03_Additional_WP_Personnel';
$beanList['M03_Work_Product_Facility'] = 'M03_Work_Product_Facility';
$beanFiles['M03_Work_Product_Facility'] = 'modules/M03_Work_Product_Facility/M03_Work_Product_Facility.php';
$moduleList[] = 'M03_Work_Product_Facility';
$beanList['M03_Work_Product_Code'] = 'M03_Work_Product_Code';
$beanFiles['M03_Work_Product_Code'] = 'modules/M03_Work_Product_Code/M03_Work_Product_Code.php';
$moduleList[] = 'M03_Work_Product_Code';
$beanList['M03_Additional_WP_IDs'] = 'M03_Additional_WP_IDs';
$beanFiles['M03_Additional_WP_IDs'] = 'modules/M03_Additional_WP_IDs/M03_Additional_WP_IDs.php';
$moduleList[] = 'M03_Additional_WP_IDs';
$beanList['M03_Work_Product'] = 'M03_Work_Product';
$beanFiles['M03_Work_Product'] = 'modules/M03_Work_Product/M03_Work_Product.php';
$moduleList[] = 'M03_Work_Product';
$beanList['M03_Work_Product_Deliverable'] = 'M03_Work_Product_Deliverable';
$beanFiles['M03_Work_Product_Deliverable'] = 'custom/modules/M03_Work_Product_Deliverable/M03_Work_Product_Deliverable.php';
$moduleList[] = 'M03_Work_Product_Deliverable';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Service_Pricing.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['SP_Service_Pricing'] = 'SP_Service_Pricing';
$beanFiles['SP_Service_Pricing'] = 'modules/SP_Service_Pricing/SP_Service_Pricing.php';
$moduleList[] = 'SP_Service_Pricing';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Controlled_Document_QA_Reviews.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['QARev_CD_QA_Reviews'] = 'QARev_CD_QA_Reviews';
$beanFiles['QARev_CD_QA_Reviews'] = 'modules/QARev_CD_QA_Reviews/QARev_CD_QA_Reviews.php';
$moduleList[] = 'QARev_CD_QA_Reviews';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Controlled_Documents_QA_Review_Docs.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['QADoc_CD_QA_Rev_Docs'] = 'QADoc_CD_QA_Rev_Docs';
$beanFiles['QADoc_CD_QA_Rev_Docs'] = 'modules/QADoc_CD_QA_Rev_Docs/QADoc_CD_QA_Rev_Docs.php';
$moduleList[] = 'QADoc_CD_QA_Rev_Docs';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Historical_USDA_ID.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['USDA_Historical_USDA_ID'] = 'USDA_Historical_USDA_ID';
$beanFiles['USDA_Historical_USDA_ID'] = 'modules/USDA_Historical_USDA_ID/USDA_Historical_USDA_ID.php';
$moduleList[] = 'USDA_Historical_USDA_ID';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Account_Number.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['AN_Account_Number'] = 'AN_Account_Number';
$beanFiles['AN_Account_Number'] = 'modules/AN_Account_Number/AN_Account_Number.php';
$moduleList[] = 'AN_Account_Number';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Test_System_Design_1.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['TSD1_Test_System_Design_1'] = 'TSD1_Test_System_Design_1';
$beanFiles['TSD1_Test_System_Design_1'] = 'custom/modules/TSD1_Test_System_Design_1/TSD1_Test_System_Design_1.php';
$moduleList[] = 'TSD1_Test_System_Design_1';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Work_Product_Group.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['WPG_Work_Product_Group'] = 'WPG_Work_Product_Group';
$beanFiles['WPG_Work_Product_Group'] = 'modules/WPG_Work_Product_Group/WPG_Work_Product_Group.php';
$moduleList[] = 'WPG_Work_Product_Group';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Species_Census.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['SC_Species_Census'] = 'SC_Species_Census';
$beanFiles['SC_Species_Census'] = 'modules/SC_Species_Census/SC_Species_Census.php';
$moduleList[] = 'SC_Species_Census';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Controlled_Document_Utilization.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['CDU_CD_Utilization'] = 'CDU_CD_Utilization';
$beanFiles['CDU_CD_Utilization'] = 'modules/CDU_CD_Utilization/CDU_CD_Utilization.php';
$moduleList[] = 'CDU_CD_Utilization';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Task_Design.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['TaskD_Task_Design'] = 'TaskD_Task_Design';
$beanFiles['TaskD_Task_Design'] = 'modules/TaskD_Task_Design/TaskD_Task_Design.php';
$moduleList[] = 'TaskD_Task_Design';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Group_Design.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['GD_Group_Design'] = 'GD_Group_Design';
$beanFiles['GD_Group_Design'] = 'modules/GD_Group_Design/GD_Group_Design.php';
$moduleList[] = 'GD_Group_Design';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Order_Request_Item.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['ORI_Order_Request_Item'] = 'ORI_Order_Request_Item';
$beanFiles['ORI_Order_Request_Item'] = 'modules/ORI_Order_Request_Item/ORI_Order_Request_Item.php';
$moduleList[] = 'ORI_Order_Request_Item';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Order_Request.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['OR_Order_Request'] = 'OR_Order_Request';
$beanFiles['OR_Order_Request'] = 'modules/OR_Order_Request/OR_Order_Request.php';
$moduleList[] = 'OR_Order_Request';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Purchase_Order_Item.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['POI_Purchase_Order_Item'] = 'POI_Purchase_Order_Item';
$beanFiles['POI_Purchase_Order_Item'] = 'modules/POI_Purchase_Order_Item/POI_Purchase_Order_Item.php';
$moduleList[] = 'POI_Purchase_Order_Item';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Purchase_Order.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['PO_Purchase_Order'] = 'PO_Purchase_Order';
$beanFiles['PO_Purchase_Order'] = 'modules/PO_Purchase_Order/PO_Purchase_Order.php';
$moduleList[] = 'PO_Purchase_Order';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Received_Items.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['RI_Received_Items'] = 'RI_Received_Items';
$beanFiles['RI_Received_Items'] = 'modules/RI_Received_Items/RI_Received_Items.php';
$moduleList[] = 'RI_Received_Items';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Inventory_Collection.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['IC_Inventory_Collection'] = 'IC_Inventory_Collection';
$beanFiles['IC_Inventory_Collection'] = 'modules/IC_Inventory_Collection/IC_Inventory_Collection.php';
$moduleList[] = 'IC_Inventory_Collection';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Inventory_Item.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['II_Inventory_Item'] = 'II_Inventory_Item';
$beanFiles['II_Inventory_Item'] = 'modules/II_Inventory_Item/II_Inventory_Item.php';
$moduleList[] = 'II_Inventory_Item';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Inventory_Management.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['IM_Inventory_Management'] = 'IM_Inventory_Management';
$beanFiles['IM_Inventory_Management'] = 'modules/IM_Inventory_Management/IM_Inventory_Management.php';
$moduleList[] = 'IM_Inventory_Management';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/NAMSA_Subcontracting_Companies.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['NSC_NAMSA_Sub_Companies'] = 'NSC_NAMSA_Sub_Companies';
$beanFiles['NSC_NAMSA_Sub_Companies'] = 'modules/NSC_NAMSA_Sub_Companies/NSC_NAMSA_Sub_Companies.php';
$moduleList[] = 'NSC_NAMSA_Sub_Companies';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/NAMSA_Test_Codes.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['TC_NAMSA_Test_Codes'] = 'TC_NAMSA_Test_Codes';
$beanFiles['TC_NAMSA_Test_Codes'] = 'modules/TC_NAMSA_Test_Codes/TC_NAMSA_Test_Codes.php';
$moduleList[] = 'TC_NAMSA_Test_Codes';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Operator_Tracking.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['OT_Operator_Tracking'] = 'OT_Operator_Tracking';
$beanFiles['OT_Operator_Tracking'] = 'modules/OT_Operator_Tracking/OT_Operator_Tracking.php';
$moduleList[] = 'OT_Operator_Tracking';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/CAPA_Files.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['CF_CAPA_Files'] = 'CF_CAPA_Files';
$beanFiles['CF_CAPA_Files'] = 'modules/CF_CAPA_Files/CF_CAPA_Files.php';
$moduleList[] = 'CF_CAPA_Files';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/CAPA.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['CAPA_CAPA'] = 'CAPA_CAPA';
$beanFiles['CAPA_CAPA'] = 'modules/CAPA_CAPA/CAPA_CAPA.php';
$moduleList[] = 'CAPA_CAPA';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Batch_ID.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['BID_Batch_ID'] = 'BID_Batch_ID';
$beanFiles['BID_Batch_ID'] = 'modules/BID_Batch_ID/BID_Batch_ID.php';
$moduleList[] = 'BID_Batch_ID';


?>
