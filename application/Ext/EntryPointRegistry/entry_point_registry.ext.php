<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/updateHistoricData.php


$entry_point_registry['updateHistoricData'] = array(
    'file' => 'custom/updateHistoricData.php',
    'auth' => false
);

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/deleteAdditionalRecordsInWP.php


$entry_point_registry['deleteAdditionalRecordsInWP'] = array(
    'file' => 'custom/deleteAdditionalRecordsInWP.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyCompanyContactToContact.php


 $entry_point_registry['copyCompanyContactToContact'] = array(
    'file' => 'custom/copyCompanyContactToContact.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/deleteAdditionalRecordsInSA.php


$entry_point_registry['deleteAdditionalRecordsInSA'] = array(
    'file' => 'custom/deleteAdditionalRecordsInSA.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyCompanyAndCompanyContactToRespectiveFields.php


$entry_point_registry['copyCompanyAndCompanyContactToRespectiveFields'] = array(
    'file' => 'custom/copyCompanyAndCompanyContactToRespectiveFields.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyStudyDirectorToStudyDirectorScriptField.php


 $entry_point_registry['copyStudyDirectorToStudyDirectorScriptField'] = array(
    'file' => 'custom/copyStudyDirectorToStudyDirectorScriptField.php',
    'auth' => true
  );

//http://localhost/aps_latest2/index.php?entryPoint=copyStudyDirectorToStudyDirectorScriptField

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/insert_update_field_metadata.php


$entry_point_registry['insert_update_field_metadata'] = array(
    'file' => 'custom/insert_update_field_metadata.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/setCategoryToSponsor.php


$entry_point_registry['setCategoryToSponsor'] = array(
    'file' => 'custom/setCategoryToSponsor.php',
    'auth' => true
  );



?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/rename_obser_employee_since_fJan.php


$entry_point_registry['rename_obser_employee_since_fJan'] = array(
    'file' => 'custom/rename_obser_employee_since_fJan.php',
    'auth' => true
  );

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/rename_obser_since_fJan.php


$entry_point_registry['rename_obser_since_fJan'] = array(
    'file' => 'custom/rename_obser_since_fJan.php',
    'auth' => true
  );

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/migrateOldFieldsToNew.php


$entry_point_registry['migrateOldFieldsToNew'] = array(
    'file' => 'custom/migrateOldFieldsToNew.php',
    'auth' => true
);


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/migrateField.php


$entry_point_registry['migrateField'] = array(
    'file' => 'custom/migrateField.php',
    'auth' => true
  );

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/migrateDateDiscovered.php


$entry_point_registry['migrateDateDiscovered'] = array(
    'file' => 'custom/migrateDateDiscovered.php',
    'auth' => true
  );

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/attachAnimalsToRoom.php


$entry_point_registry['attachAnimalsToRoom'] = array(
    'file' => 'custom/attachAnimalsToRoom.php',
    'auth' => true
  );

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/deleteEntriesInContactsDE.php


$entry_point_registry['deleteEntriesInContactsDE'] = array(
    'file' => 'custom/deleteEntriesInContactsDE.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/insert_update_field_metadata_sprint2.php


$entry_point_registry['insert_update_field_metadata_sprint2'] = array(
    'file' => 'custom/insert_update_field_metadata_sprint2.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyPrimaryApsOptToPrimaryApsOptRelate.php


$entry_point_registry['copyPrimaryApsOptToPrimaryApsOptRelate'] = array(
    'file' => 'custom/copyPrimaryApsOptToPrimaryApsOptRelate.php',
    'auth' => true
  );



?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyDocumentStatusToStatusId.php


$entry_point_registry['copyDocumentStatusToStatusId'] = array(
    'file' => 'custom/copyDocumentStatusToStatusId.php',
    'auth' => true
  );



?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyDocumentTypeToCategoryId.php


$entry_point_registry['copyDocumentTypeToCategoryId'] = array(
    'file' => 'custom/copyDocumentTypeToCategoryId.php',
    'auth' => true
  );



?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyLeadAuditorToLeadAuditorRelate.php


$entry_point_registry['copyLeadAuditorToLeadAuditorRelate'] = array(
    'file' => 'custom/copyLeadAuditorToLeadAuditorRelate.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyStudyCoordinatorToStudyCoordinatorRelate.php


$entry_point_registry['copyStudyCoordinatorToStudyCoordinatorRelate'] = array(
    'file' => 'custom/copyStudyCoordinatorToStudyCoordinatorRelate.php',
    'auth' => true
  );



?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyObserEmployeeFromEmployeeToContactsModule.php

//copyObserEmployeeFromEmployeeTpContactsModule
$entry_point_registry['copyObserEmployeeFromEmployeeToContactsModule'] = array(
    'file' => 'custom/copyObserEmployeeFromEmployeeToContactsModule.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyShippedDestroyedDateFieldInShippedDestroyedText.php


$entry_point_registry['copyShippedDestroyedDateFieldInShippedDestroyedText'] = array(
    'file' => 'custom/copyShippedDestroyedDateFieldInShippedDestroyedText.php',
    'auth' => true
  );




?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyPathologistAndStudyVeterinarianToAuthorField.php


$entry_point_registry['copyPathologistAndStudyVeterinarianToAuthorField'] = array(
    'file' => 'custom/copyPathologistAndStudyVeterinarianToAuthorField.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyPrincipalInvestigatorToPrincipalInvestigatorRelate.php


$entry_point_registry['copyPrincipalInvestigatorToPrincipalInvestigatorRelate'] = array(
    'file' => 'custom/copyPrincipalInvestigatorToPrincipalInvestigatorRelate.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/setConfigVar.php


$entry_point_registry['setConfigVar'] = array(
    'file' => 'custom/setConfigVar.php',
    'auth' => true
  );

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/updateLastServiceDateInEF.php


$entry_point_registry['updateLastServiceDateInEF'] = array(
    'file' => 'custom/updateLastServiceDateInEF.php',
    'auth' => true
  );

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/myCustomDataMigrate.php


$entry_point_registry['myCustomDataMigrate'] = array(
    'file' => 'custom/myCustomDataMigrate.php',
    'auth' => false
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/addHomeInSpecialQueryModule.php


$entry_point_registry['addHomeInSpecialQueryModule'] = array(
    'file' => 'custom/addHomeInSpecialQueryModule.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyTestSystemVendorToRelateField.php


 $entry_point_registry['copyTestSystemVendorToRelateField'] = array(
    'file' => 'custom/copyTestSystemVendorToRelateField.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/copyTestSystemDeceasedToCheckbox.php


 $entry_point_registry['copyTestSystemDeceasedToCheckbox'] = array(
    'file' => 'custom/copyTestSystemDeceasedToCheckbox.php',
    'auth' => true
  );




?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/fixRegulatoryResponseName.php


$entry_point_registry['fixRegulatoryResponseName'] = array(
    'file' => 'custom/fixRegulatoryResponseName.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/populateIntegerFieldInObservationModule.php

//populateIntegerFieldInObservationModule

$entry_point_registry['populateIntegerFieldInObservationModule'] = array(
    'file' => 'custom/populateIntegerFieldInObservationModule.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/migrateFieldData.php


$entry_point_registry['migrateFieldData'] = array(
    'file' => 'custom/include/Helpers/migrateFieldData.php',
    'auth' => true
);
?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/wpeSequenceCorrection.php


$entry_point_registry['wpeSequenceCorrection'] = array(
    'file' => 'custom/include/Helpers/wpeSequenceCorrection.php',
    'auth' => true
  );


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/getEquipmentDataEP.php

 $entry_point_registry['getEquipmentDataEP'] = array(
    'file' => 'custom/include/EntryPoints/ProcessEquipmentDataRequest.php',
    'auth' => true
);

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/getReportData.php

 $entry_point_registry['getReportData'] = array(
    'file' => 'custom/include/EntryPoints/ProcessReportData.php',
    'auth' => true
);

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/getGenReportData.php

 $entry_point_registry['getGenReportData'] = array(
    'file' => 'custom/include/EntryPoints/ProcessGenReportData.php',
    'auth' => true
);

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/getTaskDesignData.php

  $entry_point_registry['CopyTaskDesign'] = array(
    'file' => 'custom/modules/GD_Group_Design/CopyTaskDesignToGD.php',
    'auth' => true
);


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/getGdDropdownofCurrentWP.php

  $entry_point_registry['GdlistOfCurrentWp'] = array(
    'file' => 'custom/modules/GD_Group_Design/GetGdofCurrentWP.php',
    'auth' => true
);


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/getInventoryItem.php

 $entry_point_registry['getInventoryItem'] = array(
    'file' => 'custom/include/EntryPoints/ProcessInventoryItemRequest.php',
    'auth' => true
);

?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/CurrentWPGetGDlist.php

  $entry_point_registry['CurrentWPGdlist'] = array(
    'file' => 'custom/modules/GD_Group_Design/CurrentWPGdlist.php',
    'auth' => true
);


?>
<?php
// Merged from custom/Extension/application/Ext/EntryPointRegistry/getGdDropdownofWP.php

  $entry_point_registry['GetGdlist'] = array(
    'file' => 'custom/modules/GD_Group_Design/GetGdlistFromWp.php',
    'auth' => false
);


?>
