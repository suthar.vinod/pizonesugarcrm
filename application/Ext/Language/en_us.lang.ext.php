<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_projects_priority_options.php

 // created: 2016-01-06 02:33:50

$app_list_strings['projects_priority_options']=array (
  'high' => 'Standard',
  'medium' => 'Expedited',
  'low' => 'Priority Expedited',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_workproductcategory.php

 // created: 2016-01-06 03:54:50

$app_list_strings['workproductcategory']=array (
  'Product' => 'Product',
  'Service' => 'Service',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_work_product_type_list.php

 // created: 2016-01-06 04:02:08

$app_list_strings['work_product_type_list']=array (
  'Select' => 'Select',
  'Sensitization' => 'Sensitization',
  'Irritation' => 'Irritation',
  'Cardiovascular' => 'Cardiovascular',
  'Genotoxicity' => 'Genotoxicity',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_task_name_list.php

 // created: 2016-01-06 23:05:26

$app_list_strings['task_name_list']=array (
  'blank' => 'Blank',
  'protocol' => 'Protocol',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_altered_bid_terms_list.php

 // created: 2016-01-07 14:56:24

$app_list_strings['altered_bid_terms_list']=array (
  'yes' => 'Yes',
  'no' => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_lead_source_dom.php

 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Existing Customer' => 'Existing Customer',
  'Web Site' => 'Web Site',
  'Cold Call' => 'Cold Call',
  'Self Generated' => 'Self Generated',
  'Referral' => 'Referral',
  'Conference' => 'Conference',
  'Trade Show' => 'Trade Show',
  'Campaign' => 'Campaign',
  'Support Portal User Registration' => 'FlightLog User Registration',
  'Email' => 'Email',
  'Employee' => 'Employee',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_job_function_list.php

 // created: 2016-01-07 17:50:41

$app_list_strings['job_function_list']=array (
  'blank' => '',
  'research and development' => 'R & D',
  'preclinical' => 'Preclinical',
  'consultant' => 'Consultant',
  'regulatory' => 'Regulatory',
  'clinical' => 'Clinical',
  'management' => 'Management',
  'executive management' => 'Executive Management',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_specialty_list.php

 // created: 2016-01-07 19:31:59

$app_list_strings['specialty_list']=array (
  'blank' => '',
  'cardiovascular' => 'Cardiovascular',
  'neurovascular' => 'Neurovascular',
  'orthopaedic' => 'Orthopaedic',
  'general pharmacology' => 'General Pharmacology',
  'oem' => 'OEM',
  'diabetes' => 'Diabetes',
  'urogenital' => 'Urogenital',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Work_Product_Personnel.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['WPP_Work_Product_Personnel'] = 'Work Product Personnel ';
$app_list_strings['moduleList']['WPP_WPP2'] = 'Work_Product_Personnel';
$app_list_strings['moduleListSingular']['WPP_Work_Product_Personnel'] = 'Work Product Person';
$app_list_strings['moduleListSingular']['WPP_WPP2'] = 'Work_Product_Personnel';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sales_activity_stage_list.php

 // created: 2016-01-13 22:44:15

$app_list_strings['sales_activity_stage_list']=array (
  'select' => 'Select...',
  'lead' => 'Lead',
  'opportunity' => 'Opportunity',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Sales.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['M01_Sales'] = 'Sales';
$app_list_strings['moduleListSingular']['M01_Sales'] = 'Sale';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_SalesActivityCategory.php

 // created: 2016-01-21 02:20:34

$app_list_strings['SalesActivityCategory']=array (
  'Select' => 'Select',
  'Lead' => 'Lead',
  'Opportunity' => 'Opportunity',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Sales_Activity_Division_Department.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['M02_SA_Division_Department_'] = 'SA Divisions Departments ';
$app_list_strings['moduleListSingular']['M02_SA_Division_Department_'] = 'SA Division Department ';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Work_Products.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['M03_Work_Product'] = 'Work Products';
$app_list_strings['moduleListSingular']['M03_Work_Product'] = 'Work Product';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Common.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['m06_Communication'] = 'APS Communications';
$app_list_strings['moduleList']['m06_Note_To_File'] = 'Notes_To_File';
$app_list_strings['moduleList']['m06_APS_Communication'] = 'APS Communications';
$app_list_strings['moduleListSingular']['m06_Communication'] = 'APS Communication';
$app_list_strings['moduleListSingular']['m06_Note_To_File'] = 'Note_To_File';
$app_list_strings['moduleListSingular']['m06_APS_Communication'] = 'APS Communication';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Work_Product_Management.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['M03_Work_Product'] = 'Work Products';
$app_list_strings['moduleList']['M03_Work_Product_Deliverable'] = 'Work Product Deliverables';
$app_list_strings['moduleList']['M03_Work_Product_Personnel'] = 'Additional WP Personnel';
$app_list_strings['moduleList']['M03_Work_Product_Facility'] = 'Work Product Facilities';
$app_list_strings['moduleList']['M03_Work_Product_IDs'] = 'Additional WP IDs';
$app_list_strings['moduleList']['M03_work_product_code'] = 'Work Product Codes';
$app_list_strings['moduleList']['M03_Additional_WP_Personnel'] = 'Additional WP Personnel';
$app_list_strings['moduleList']['M03_Additional_WP_IDs'] = 'Additional WP IDs';
$app_list_strings['moduleList']['M03_Work_Product_Code'] = 'Work Product Codes';
$app_list_strings['moduleListSingular']['M03_Work_Product'] = 'Work Product';
$app_list_strings['moduleListSingular']['M03_Work_Product_Deliverable'] = 'Work Product Deliverables';
$app_list_strings['moduleListSingular']['M03_Work_Product_Personnel'] = 'Additional WP Person';
$app_list_strings['moduleListSingular']['M03_Work_Product_Facility'] = 'Work Product Facility';
$app_list_strings['moduleListSingular']['M03_Work_Product_IDs'] = 'Additional WP ID';
$app_list_strings['moduleListSingular']['M03_work_product_code'] = 'Work Product Code';
$app_list_strings['moduleListSingular']['M03_Additional_WP_Personnel'] = 'Additional WP Person';
$app_list_strings['moduleListSingular']['M03_Additional_WP_IDs'] = 'Additional WP ID';
$app_list_strings['moduleListSingular']['M03_Work_Product_Code'] = 'Work Product Code';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Sales_Management.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['m01_sales_activity_quote_document_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Sales'] = 'Sales';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_category_dom'][''] = '';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Active'] = 'Active';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Draft'] = 'Draft';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Expired'] = 'Expired';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Pending'] = 'Pending';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_sales_activity_quote_document_status_dom'][''] = '';
$app_list_strings['moduleList']['M01_Quote_Document'] = 'Quote Documents';
$app_list_strings['moduleList']['M01_Quote'] = 'Sales Activity Quotes';
$app_list_strings['moduleList']['M01_SA_Division_Department'] = 'SA_Division_Departments';
$app_list_strings['moduleList']['M01_Sales_Activity'] = 'Sales Activities';
$app_list_strings['moduleList']['M01_Sales_Activity_Quote'] = 'Sales Activity Quotes';
$app_list_strings['moduleList']['M01_Sales'] = 'Sales';
$app_list_strings['moduleList']['M01_Sale'] = 'Sales Activities';
$app_list_strings['moduleList']['M01_SA_Division_Department_'] = 'SA_Division_Departments';
$app_list_strings['moduleListSingular']['M01_Quote_Document'] = 'Quote Document';
$app_list_strings['moduleListSingular']['M01_Quote'] = 'Sales Activity Quote';
$app_list_strings['moduleListSingular']['M01_SA_Division_Department'] = 'SA_Division_Department';
$app_list_strings['moduleListSingular']['M01_Sales_Activity'] = 'Sales Activity';
$app_list_strings['moduleListSingular']['M01_Sales_Activity_Quote'] = 'Sales Activity Quote';
$app_list_strings['moduleListSingular']['M01_Sales'] = 'Sale';
$app_list_strings['moduleListSingular']['M01_Sale'] = 'Sales Activity';
$app_list_strings['moduleListSingular']['M01_SA_Division_Department_'] = 'SA_Division_Department';
$app_list_strings['m01_quote_document_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['m01_quote_document_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['m01_quote_document_category_dom']['Sales'] = 'Sales';
$app_list_strings['m01_quote_document_category_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_quote_document_category_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_quote_document_category_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_category_dom'][''] = '';
$app_list_strings['m01_quote_document_status_dom']['Active'] = 'Active';
$app_list_strings['m01_quote_document_status_dom']['Draft'] = 'Draft';
$app_list_strings['m01_quote_document_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_status_dom']['Expired'] = 'Expired';
$app_list_strings['m01_quote_document_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_quote_document_status_dom']['Pending'] = 'Pending';
$app_list_strings['m01_quote_document_status_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_quote_document_status_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_quote_document_status_dom'][''] = '';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Marketing'] = 'Marketing';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Sales'] = 'Sales';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom'][''] = '';
$app_list_strings['m01_quote_document_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_quote_document_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_quote_document_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_subcategory_dom']['Marketing'] = 'Marketing';
$app_list_strings['m01_quote_document_subcategory_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['m01_quote_document_subcategory_dom']['Sales'] = 'Sales';
$app_list_strings['m01_quote_document_subcategory_dom'][''] = '';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Active'] = 'Active';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Draft'] = 'Draft';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Expired'] = 'Expired';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Pending'] = 'Pending';
$app_list_strings['m01_quote_document_priority_dom']['Active'] = 'Active';
$app_list_strings['m01_quote_document_priority_dom']['Draft'] = 'Draft';
$app_list_strings['m01_quote_document_priority_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_priority_dom']['Expired'] = 'Expired';
$app_list_strings['m01_quote_document_priority_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_quote_document_priority_dom']['Pending'] = 'Pending';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Active'] = 'Active';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Draft'] = 'Draft';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Expired'] = 'Expired';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Pending'] = 'Pending';
$app_list_strings['m01_quote_document_resolution_dom']['Active'] = 'Active';
$app_list_strings['m01_quote_document_resolution_dom']['Draft'] = 'Draft';
$app_list_strings['m01_quote_document_resolution_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_resolution_dom']['Expired'] = 'Expired';
$app_list_strings['m01_quote_document_resolution_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_quote_document_resolution_dom']['Pending'] = 'Pending';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Active'] = 'Active';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Draft'] = 'Draft';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Expired'] = 'Expired';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Pending'] = 'Pending';
$app_list_strings['m01_quote_document_type_dom']['Active'] = 'Active';
$app_list_strings['m01_quote_document_type_dom']['Draft'] = 'Draft';
$app_list_strings['m01_quote_document_type_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_type_dom']['Expired'] = 'Expired';
$app_list_strings['m01_quote_document_type_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_quote_document_type_dom']['Pending'] = 'Pending';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Errors.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['M06_Error'] = 'Errors';
$app_list_strings['moduleListSingular']['M06_Error'] = 'Error';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_document_template_type_dom.php

 // created: 2016-02-22 20:05:19

$app_list_strings['document_template_type_dom']=array (
  '' => '',
  'nda' => 'NDA',
  'purchase order' => 'Purchase Order (PO)',
  'signed quote' => 'Signed Quote',
  'study design document' => 'Study Design Document',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_document_status_dom.php

 // created: 2016-02-23 20:07:52

$app_list_strings['document_status_dom']=array (
  'Active' => 'Active',
  'Signed' => 'Signed',
  'Lost' => 'Lost',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_internal_department_list.php

 // created: 2016-02-29 18:14:19

$app_list_strings['internal_department_list']=array (
  'Choose One...' => 'Choose One...',
  'Pathology Services' => 'Pathology Services',
  'Analytical Services' => 'Analytical Services',
  'In_Life' => 'In-Life',
  'IVT' => 'IVT',
  'Scientific' => 'Scientific',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_pathologist_list.php

 // created: 2016-03-01 22:14:29

$app_list_strings['pathologist_list']=array (
  'lphillips' => 'lphillps',
  'ipolyakov' => 'ipolyakov',
  'ashucker' => 'ashucker',
  'None' => 'None',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_task_status_dom.php

 // created: 2016-03-04 18:06:01

$app_list_strings['task_status_dom']=array (
  'Not Completed' => 'Not Completed',
  'Completed' => 'Completed',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_regulatory.php

 // created: 2016-03-07 20:18:49

$app_list_strings['regulatory']=array (
  'ISO' => 'ISO',
  'USP' => 'USP',
  'JMHLW' => 'JMHLW',
  'None' => 'None',
  'Choose Regulatory Standard' => 'Choose Regulatory Standard',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_Division_list.php

 // created: 2016-03-07 20:24:49

$app_list_strings['Division_list']=array (
  'Blank' => 'Blank',
  'Analytical_Services' => 'Analytical Services',
  'Corporate' => 'Corporate',
  'InLife_Services' => 'In-Life Services',
  'InVitro_Services' => 'In-Vitro Services',
  'Pathology_Services' => 'Pathology_Services',
  'Regulatory_Services' => 'Regulatory Services',
  'Choose Division' => 'Choose Division',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_Department_list.php

 // created: 2016-03-07 20:26:18

$app_list_strings['Department_list']=array (
  'Select' => 'Select',
  'Analytical' => 'Analytical',
  'Biocompatibility' => 'Biocompatibility',
  'Gross Pathology' => 'Gross Pathology',
  'Histology' => 'Histology',
  'Interventional Surgical' => 'Interventional / Surgical',
  'Microbiology' => 'Microbiology',
  'Pharmacology' => 'Pharmacology',
  'Toxicology' => 'Toxicology',
  'Cytotoxicity' => 'Cytotoxicity',
  'Hemocompatibility' => 'Hemocompatibility',
  'Genotoxicity' => 'Genotoxicity',
  'Sensitization' => 'Sensitization',
  'Irritation' => 'Irritation',
  'Systemic Toxicity' => 'Systemic Toxicity',
  'Intramuscular' => 'Intramuscular',
  'Choose Department' => 'Choose Department',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sales_activity_status_list.php

 // created: 2016-03-28 21:13:49

$app_list_strings['sales_activity_status_list']=array (
  'select' => 'Select...',
  'open' => 'Open',
  'won' => 'Won',
  'closed' => 'Closed',
  'duplicate' => 'Duplicate',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sales_activity_type_list.php

 // created: 2016-04-03 17:38:23

$app_list_strings['sales_activity_type_list']=array (
  'Within 1 month' => 'Within 1 month',
  'Within 1 to 3 months' => 'Within 1 to 3 months',
  'Within 3 to 6 months' => 'Within 3 to 6 months',
  'Within 6 to 12 months' => 'Within 6 to 12 months',
  'Greater than 12 months' => 'Greater than 12 months',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_department_manager_approval_list.php

 // created: 2016-04-11 21:58:12

$app_list_strings['department_manager_approval_list']=array (
  '' => '',
  'Approved' => 'Approved',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_study.php

 // created: 2016-04-12 14:25:08

$app_list_strings['study']=array (
  'Choose One' => 'Choose One',
  'Discard' => 'Discard',
  'Return Unused' => 'Return Unused',
  'Return Used and Unused' => 'Return Used and Unused',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_invoicing.php

 // created: 2016-04-12 14:59:34

$app_list_strings['invoicing']=array (
  'Choose one...' => 'Choose one...',
  '40_50_10' => '40%/50%/10%',
  '50_50' => '50%/50%',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_study_pathologist_list.php

 // created: 2016-04-19 16:28:31

$app_list_strings['study_pathologist_list']=array (
  'Choose Pathologist...' => 'Choose Pathologist...',
  'Igor Polyakov' => 'Igor Polyakov',
  'Lynette Phillips' => 'Lynette Phillips',
  'Muhammad Ahsan' => 'Muhammad Ahsan',
  'Adrienne Schucker' => 'Adrienne Schucker',
  'None' => 'None',
  'External Personnel' => 'External Personnel',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_Med_Device_Category_and_Contact.php

 // created: 2016-04-26 15:20:30

$app_list_strings['Med_Device_Category_and_Contact']=array (
  'Surface device mucosal membrane' => 'Surface device - Mucosal Membrane',
  'Surface device breached or compromised surface' => 'Surface device - Breach/Compromised Surface',
  'Ext communicating device blood path indirect' => 'Ext communicating device - Blood path, indirect',
  'Ext communicating device tissue bone dentin' => 'Ext communicating device - Tissue/Bone/Dentin',
  'Ext communicating device circulating blood' => 'Ext communicating device - Circulating Blood',
  'Implant device tissue bone' => 'Implant device - Tissue/bone',
  'Implant device blood' => 'Implant device - Blood',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_patient_contact_duration_list.php

 // created: 2016-04-26 15:59:12

$app_list_strings['patient_contact_duration_list']=array (
  'Limited' => 'Limited (24 hours or less)',
  'Prolonged' => 'Prolonged (24 hours - 30 days)',
  'Permanent' => 'Permanent (greater than 30 days)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_followup_status_list.php

 // created: 2016-05-21 02:31:56

$app_list_strings['followup_status_list']=array (
  'active' => 'Active',
  'inactive' => 'Inactive',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_quote_status_list.php

 // created: 2016-08-09 16:11:40

$app_list_strings['quote_status_list']=array (
  'Choose_one' => 'Choose One...',
  'inactive' => 'Inactive',
  'active' => 'Active',
  'change_order' => 'Change Order',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_animal_model_list.php

 // created: 2016-08-19 22:00:14

$app_list_strings['animal_model_list']=array (
  'Choose one...' => 'Choose one...',
  'naive_porcine' => 'Naive Porcine',
  'naive_canine' => 'Naive Canine',
  'naive_ovine' => 'Naive Ovine',
  'naive_rabbit' => 'Naive Lagamorph',
  'myocaridal_infarction' => 'Myocardial Infarction',
  'rabbit_elastase_aneurysm' => 'Rabbit Elastase Aneurysm ',
  'canine_aneurysm' => 'Canine Aneurysm',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_device_type_list.php

 // created: 2016-08-19 22:09:42

$app_list_strings['device_type_list']=array (
  'Choose one...' => 'Choose one...',
  'ep_catheter' => 'EP Catheter',
  'coronary_stent' => 'Coronary Stent',
  'peripheral_stent' => 'Peripheral Stent',
  'aneurysm_filler' => 'Aneurysm Filler/Diverter',
  'heart_valve' => 'Heart Valve',
  'intravascular_catheter' => 'Intravascular Catheter',
  'neuromodulation' => 'Neuromodulation',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_deliverable_owner_list.php

 // created: 2016-11-21 20:37:18

$app_list_strings['deliverable_owner_list']=array (
  'ablakstvedt' => 'ablakstvedt',
  'sbronstad' => 'edobrava',
  'ebauer' => 'ebauer',
  'showard' => 'showard',
  'kcatalano' => 'kcatalano',
  'Add manager of deliverable' => 'Add manager of deliverable',
  'None' => 'None',
  'mheying' => 'mheying',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_signed_quote_c_list.php

 // created: 2017-01-23 16:20:18

$app_list_strings['signed_quote_c_list']=array (
  'Yes' => 'Yes',
  'No' => 'No',
  'Pricelist' => 'Pricelist',
  'Not Provided' => 'Not Provided',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_estimated_hours_list.php

 // created: 2017-02-09 22:23:15

$app_list_strings['estimated_hours_list']=array (
  '0.5' => '0.5',
  1 => '1',
  '1.5' => '1.5',
  2 => '2',
  '2.5' => '2.5',
  3 => '3',
  '3.5' => '3.5',
  4 => '4',
  '4.5' => '4.5',
  5 => '5',
  '5.5' => '5.5',
  6 => '6',
  '6.5' => '6.5',
  7 => '7',
  '7.5' => '7.5',
  8 => '8',
  '8.5' => '8.5',
  9 => '9',
  '9.5' => '9.5',
  10 => '10',
  '10.5' => '10.5',
  11 => '11',
  '11.5' => '11.5',
  12 => '12',
  '12.5' => '12.5',
  13 => '13',
  '13.5' => '13.5',
  14 => '14',
  '14.5' => '14.5',
  15 => '15',
  '15.5' => '15.5',
  16 => '16',
  '16.5' => '16.5',
  17 => '17',
  '17.5' => '17.5',
  18 => '18',
  '18.5' => '18.5',
  19 => '19',
  '19.5' => '19.5',
  20 => '20',
  'Choose One' => 'Choose One',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Time_Keeping.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['ABC12_Work_Product_Activities'] = 'Work Product Activities';
$app_list_strings['moduleListSingular']['ABC12_Work_Product_Activities'] = 'Work Product Activities';
$app_list_strings['Department_list']['Select'] = 'Select';
$app_list_strings['Department_list']['Analytical'] = 'Analytical';
$app_list_strings['Department_list']['Biocompatibility'] = 'Biocompatibility';
$app_list_strings['Department_list']['Gross Pathology'] = 'Gross Pathology';
$app_list_strings['Department_list']['Histology'] = 'Histology';
$app_list_strings['Department_list']['Interventional Surgical'] = 'Interventional / Surgical';
$app_list_strings['Department_list']['Microbiology'] = 'Microbiology';
$app_list_strings['Department_list']['Pharmacology'] = 'Pharmacology';
$app_list_strings['Department_list']['Toxicology'] = 'Toxicology';
$app_list_strings['Department_list']['Cytotoxicity'] = 'Cytotoxicity';
$app_list_strings['Department_list']['Hemocompatibility'] = 'Hemocompatibility';
$app_list_strings['Department_list']['Genotoxicity'] = 'Genotoxicity';
$app_list_strings['Department_list']['Sensitization'] = 'Sensitization';
$app_list_strings['Department_list']['Irritation'] = 'Irritation';
$app_list_strings['Department_list']['Systemic Toxicity'] = 'Systemic Toxicity';
$app_list_strings['Department_list']['Intramuscular'] = 'Intramuscular';
$app_list_strings['Department_list']['Choose Department'] = 'Choose Department';
$app_list_strings['activity_list']['Choose One'] = 'Choose One';
$app_list_strings['activity_list']['Literature Research'] = 'Literature Research';
$app_list_strings['activity_list']['Protocol Development'] = 'Protocol Development';
$app_list_strings['activity_list']['Study Article Reconciliation'] = 'Study Article Reconciliation';
$app_list_strings['activity_list']['Procedure Oversight'] = 'Procedure Oversight';
$app_list_strings['activity_list']['Data Entry'] = 'Data Entry';
$app_list_strings['activity_list']['Report Editing'] = 'Report Editing ';
$app_list_strings['activity_list']['Report Drafting'] = 'Report Drafting';
$app_list_strings['activity_list']['Report Finalization'] = 'Report Finalization';
$app_list_strings['activity_list']['Slide Evaluation'] = 'Slide Evaluation';
$app_list_strings['activity_list']['Conference Call'] = 'Sponsor Conference Call';
$app_list_strings['activity_list']['Internal Meeting'] = 'Internal Meeting';
$app_list_strings['activity_list']['Sponsor Meeting'] = 'Sponsor Meeting';
$app_list_strings['activity_list']['Protocol Audit'] = 'Protocol Audit';
$app_list_strings['activity_list']['Critical Phase Audit'] = 'Critical Phase Audit';
$app_list_strings['activity_list']['Report Audit'] = 'Report Audit';
$app_list_strings['activity_list']['Inspection Report Drafting'] = 'Inspection Report Drafting';
$app_list_strings['activity_list']['Necropsy Oversight'] = 'Necropsy Oversight';
$app_list_strings['activity_list']['Sample Preparation'] = 'Sample Preparation';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_primary_aps_operator_list.php

 // created: 2017-03-14 15:19:19

$app_list_strings['primary_aps_operator_list']=array (
  'Choose One' => 'Choose One',
  'Michael Jorgenson' => 'Michael Jorgenson',
  'Allan Camrud' => 'Allan Camrud',
  'Mark Beckel' => 'Mark Beckel',
  'Tyler LaMont' => 'Tyler LaMont',
  'Joseph Vislisel' => 'Joseph Vislisel',
  'Christina Gross' => 'Christina Gross',
  'Elizabeth Carter' => 'Elizabeth Carter',
  'Chris Lafean' => 'Chris Lafean',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_product_type_list.php

 // created: 2017-03-15 19:49:45

$app_list_strings['product_type_list']=array (
  'Solid' => 'Solid',
  'Liquid' => 'Liquid',
  'Gel' => 'Gel',
  'Unknown' => 'Unknown',
  'Choose one...' => 'Choose One...',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_Product_Contact_Category.php

 // created: 2017-03-15 19:51:21

$app_list_strings['Product_Contact_Category']=array (
  'Surface Contacting' => 'Surface Contacting',
  'External Communicating' => 'External Communicating',
  'Implant' => 'Implant',
  'Unknown' => 'Unknown',
  'Choose one...' => 'Choose One...',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_contact_duration_list.php

 // created: 2017-03-15 20:10:35

$app_list_strings['contact_duration_list']=array (
  'Limited' => 'Limited',
  'Prolonged_30' => 'Prolonged ',
  'Permanent' => 'Permanent',
  'Unknown' => 'Unknown',
  'Choose one...' => 'Choose One...',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_contact_type_list.php

 // created: 2017-03-15 20:32:30

$app_list_strings['contact_type_list']=array (
  'Choose one...' => 'Choose One...',
  'Unknown' => 'Unknown',
  'Skin_Limited' => 'Skin - Limited',
  'Skin_Prolonged' => 'Skin - Prolonged',
  'Skin_Permanent' => 'Skin - Permanent',
  'Mucosal Membrane_Limited' => 'Mucosal Membrane - Limited',
  'Mucosal Membrane_Prolonged' => 'Mucosal Membrane - Prolonged',
  'Mucosal Membrane_Permanent' => 'Mucosal Membrane - Permanent',
  'Compromised Surface_Limited' => 'Compromised Surface - Limited',
  'Compromised Surface_Prolonged' => 'Compromised Surface - Prolonged',
  'Compromised Surface_Permanent' => 'Compromised Surface - Permanent',
  'Blood Path_Indirect_Limited' => 'Blood Path, Indirect - Limited',
  'Blood Path_Indirect_Prolonged' => 'Blood Path, Indirect - Prolonged',
  'Blood Path_Indirect_Permanent' => 'Blood Path, Indirect - Permanent',
  'Tissue_Bone_Dentin_Limited' => 'Tissue/Bone/Dentin - Limited',
  'Tissue_Bone_Dentin_Prolonged' => 'Tissue/Bone/Dentin - Prolonged',
  'Tissue_Bone_Dentin_Permanent' => 'Tissue/Bone/Dentin - Permanent',
  'Circulating Blood_Limited' => 'Circulating Blood - Limited',
  'Circulating Blood_Prolonged' => 'Circulating Blood - Prolonged',
  'Circulating Blood_Permanent' => 'Circulating Blood - Permanent',
  'Tissue_Bone_Limited' => 'Tissue/Bone - Limited',
  'Tissue_Bone_Prolonged' => 'Tissue/Bone - Prolonged',
  'Tissue_Bone_Permanent' => 'Tissue/Bone - Permanent',
  'Blood_Limited' => 'Blood - Limited',
  'Blood_Prolonged' => 'Blood - Prolonged',
  'Blood_Permanent' => 'Blood - Permanent',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_Cytotoxicity_Testing.php

 // created: 2017-03-15 20:57:38

$app_list_strings['Cytotoxicity_Testing']=array (
  'MEM Elution' => 'MEM Elution - CY40',
  'MEM Serial Dilution' => 'MEM Serial Dilution - CY41',
  'Liquid Cytotoxicity' => 'Liquid Cytotoxicity - CY10',
  'Bacterial Endotoxin' => 'Bacterial Endotoxin - CY50',
  'Bacterial Endotoxin_Lot Release' => 'Bacterial Endotoxin, Lot Release - CY51',
  'Agar Overlay' => 'Agar Overlay, Solid - CY01',
  'Agar Overlay_Liquid' => 'Agar Overlay, Liquid - CY02',
  'Direct Contact' => 'Direct Contact - CY20',
  'Neutral Red Uptake' => 'Neutral Red Uptake - CY30',
  'MTT' => 'MTT - CY70',
  'Colony Formation' => 'Colony Formation - CY60',
  'Choose one...' => 'Choose One...',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_activity_list.php

 // created: 2017-03-20 21:10:57

$app_list_strings['activity_list']=array (
  'Choose One' => 'Choose One',
  'Protocol Dev or Auditing' => 'Protocol Development',
  'Procedure Oversight' => 'Operations Oversight',
  'Data Entry and Analysis' => 'Data Entry & Analysis',
  'Reporting and Auditing' => 'Reporting',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_business_develop_activity_list.php

 // created: 2017-05-02 16:30:19

$app_list_strings['business_develop_activity_list']=array (
  '' => '',
  'Conference Call' => 'Conference Call',
  'Client_Visit_Onsite' => 'Onsite Client Visit',
  'Client_Visit_Offsite' => 'Offsite Client Visit',
  'Tradeshow' => 'Tradeshow',
  'Procedure Coverage' => 'Procedure Coverage',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_quality_assurance_activity_list.php

 // created: 2017-05-02 17:02:42

$app_list_strings['quality_assurance_activity_list']=array (
  '' => '',
  'Protocol Audit' => 'Protocol Audit',
  'Implant Procedure Audit' => 'Implant Procedure Audit',
  'Follow_Up Procedure Audit' => 'Follow-Up Procedure Audit',
  'Term Procedure Audit' => 'Term Procedure Audit',
  'Data Audit' => 'Data Audit',
  'Report Audit' => 'Report Audit',
  'Audit Findings Reporting' => 'Audit Findings Reporting',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_regulatory_activity_list.php

 // created: 2017-05-02 17:25:08

$app_list_strings['regulatory_activity_list']=array (
  '' => '',
  'Regulatory Consulting' => 'Regulatory Consulting',
  'FDA Response Writing' => 'FDA Response Writing',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Activity_Notes.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['an01_activity_notes_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['an01_activity_notes_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['an01_activity_notes_category_dom']['Sales'] = 'Sales';
$app_list_strings['an01_activity_notes_category_dom'][''] = '';
$app_list_strings['an01_activity_notes_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['an01_activity_notes_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['an01_activity_notes_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['an01_activity_notes_subcategory_dom'][''] = '';
$app_list_strings['an01_activity_notes_status_dom']['Active'] = 'Active';
$app_list_strings['an01_activity_notes_status_dom']['Draft'] = 'Draft';
$app_list_strings['an01_activity_notes_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['an01_activity_notes_status_dom']['Expired'] = 'Expired';
$app_list_strings['an01_activity_notes_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['an01_activity_notes_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['AN01_Activity_Notes'] = 'Activity Notes';
$app_list_strings['moduleListSingular']['AN01_Activity_Notes'] = 'Activity Note';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_activity_personnel_list.php

 // created: 2017-05-12 19:42:46

$app_list_strings['activity_personnel_list']=array (
  '' => '',
  'Business Development' => 'BD',
  'Scientific' => 'SC',
  'Quality Assurance' => 'QS',
  'Pathology' => 'PS',
  'Analytical' => 'AS',
  'Regulatory' => 'RS',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Quality_Assurance.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['moduleList']['A1A_CPI_Findings'] = 'CPI Findings';
$app_list_strings['moduleListSingular']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['moduleListSingular']['A1A_CPI_Findings'] = 'CPI Findings';
$app_list_strings['phase_of_inspection_list']['Choose One'] = 'Choose One';
$app_list_strings['phase_of_inspection_list']['Sample Preparation'] = 'Sample Preparation';
$app_list_strings['phase_of_inspection_list']['Addition of Article to Test System'] = 'Addition of Article to Test System';
$app_list_strings['phase_of_inspection_list']['Evaluation'] = 'Evaluation';
$app_list_strings['phase_of_inspection_list']['Analytical'] = 'Analytical';
$app_list_strings['phase_of_inspection_list']['Clinical Pathology'] = 'Clinical Pathology';
$app_list_strings['phase_of_inspection_list']['Contributing Scientist Report'] = 'Contributing Scientist Report';
$app_list_strings['phase_of_inspection_list']['Initial Procedure'] = 'Initial Procedure';
$app_list_strings['phase_of_inspection_list']['In Life'] = 'In-Life';
$app_list_strings['phase_of_inspection_list']['Followup Procedure'] = 'Follow-up Procedure';
$app_list_strings['phase_of_inspection_list']['Terminal Procedure'] = 'Terminal Procedure';
$app_list_strings['phase_of_inspection_list']['Necropsy'] = 'Necropsy';
$app_list_strings['phase_of_inspection_list']['Tissue Trimming'] = 'Tissue Trimming';
$app_list_strings['phase_of_inspection_list']['Histology'] = 'Histology';
$app_list_strings['phase_of_inspection_list']['Data Review'] = 'Data Review';
$app_list_strings['phase_of_inspection_list']['Interim Report'] = 'Interim Report';
$app_list_strings['phase_of_inspection_list']['Animal Health Report'] = 'Animal Health Report';
$app_list_strings['phase_of_inspection_list']['Pathology Report'] = 'Pathology Report';
$app_list_strings['phase_of_inspection_list']['Final Report'] = 'Final Report';
$app_list_strings['phase_of_inspection_list']['Final Report Amendment'] = 'Final Report Amendment';
$app_list_strings['cpi_finding_category_list']['Choose One'] = 'Choose One';
$app_list_strings['cpi_finding_category_list']['Documentation Error'] = 'Documentation Error';
$app_list_strings['cpi_finding_category_list']['Missed Task General '] = 'Missed Task - General ';
$app_list_strings['cpi_finding_category_list']['Missed Task Protocol'] = 'Missed Task - Protocol';
$app_list_strings['cpi_finding_category_list']['Missed Task SOP'] = 'Missed Task SOP';
$app_list_strings['cpi_finding_category_list']['Equipment'] = 'Equipment';
$app_list_strings['cpi_finding_category_list']['Timed Event'] = 'Timed Event';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_new_deliverable_c_list.php

 // created: 2017-06-09 21:01:43

$app_list_strings['new_deliverable_c_list']=array (
  '' => '',
  'Analytical Data Tables' => 'Analytical Data Tables',
  'Analytical Report' => 'Analytical Report',
  'Analytical Validation Report' => 'Analytical Validation Report',
  'Animal Health_Clin Path Report' => 'Animal Health Report',
  'Final Report' => 'Final Report',
  'Gross Pathology Report' => 'Gross Pathology Report',
  'Histopathology Report' => 'Histopathology Report',
  'In_Life Report' => 'In-Life Report',
  'Interim Analytical Report' => 'Interim Analytical Report',
  'Interim Animal Health Report' => 'Interim Animal Health Report',
  'Interim Gross Path Report' => 'Interim Gross Path Report',
  'Interim Histopathology Report' => 'Interim Histopathology Report',
  'Interim In_Life Report' => 'Interim In-Life Report',
  'Pharmacology Data Tables' => 'Pharmacology Data Tables',
  'Pharmacology Report' => 'Pharmacology Report',
  'SEM Report' => 'SEM Report',
  'Slide Shipping' => 'Slide Shipping',
  'Tissue Shipping' => 'Tissue Shipping',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_pathology_activity_list.php

 // created: 2017-06-09 21:10:54

$app_list_strings['pathology_activity_list']=array (
  '' => '',
  'Necropsy' => 'Necropsy',
  'Faxitron' => 'Faxitron',
  'Tissue Receipt' => 'Tissue Receipt',
  'Tissue Trimming' => 'Tissue Trimming',
  'Tissue Shipping' => 'Tissue Shipping',
  'Paraffin Embedding' => 'Paraffin Embedding',
  'Plastic Embedding' => 'Plastic Embedding',
  'Slide Completion' => 'Slide Completion',
  'Slide Shipping' => 'Slide Shipping',
  'SEM Imaging' => 'SEM Imaging',
  'Histomorphometry' => 'Histomorphometry',
  'Slide Review' => 'Slide Review',
  'Gross Necropsy Report Writing' => 'Gross Necropsy Report Writing',
  'Pathology Report Writing' => 'Pathology Report Writing',
  'Out of Office' => 'Out of Office',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_reminder_time_options.php

 // created: 2017-06-16 19:19:43

$app_list_strings['reminder_time_options']=array (
  -1 => 'None',
  60 => '1 minute prior',
  300 => '5 minutes prior',
  600 => '10 minutes prior',
  900 => '15 minutes prior',
  1800 => '30 minutes prior',
  3600 => '1 hour prior',
  7200 => '2 hours prior',
  10800 => '3 hours prior',
  18000 => '5 hours prior',
  86400 => '1 day prior',
  604800 => '7 days prior',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_analytical_equipment_list_list.php

 // created: 2017-06-16 21:18:08

$app_list_strings['analytical_equipment_list_list']=array (
  '' => '',
  'ISQ LT' => 'GCMS – ISQ LT',
  'TSQ Quantiva 1' => 'LCMS – TSQ Quantiva 1',
  'TSQ Quantiva 2' => 'LCMS – TSQ Quantiva 2',
  'QE Focus' => 'LCMS – QE Focus',
  'Nicolet iS10' => 'FTIR – Nicolet iS10',
  'ASE350' => 'ASE – ASE350',
  'Agilent 7500 CS' => 'ICPMS – Agilent 7500 CS',
  'Precellys Evolution' => 'Homogenizer – Precellys Evolution',
  'Cryolys' => 'Homogenizer Chiller – Cryolys',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.HR_Management.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['hr01_resumes_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['hr01_resumes_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['hr01_resumes_category_dom']['Sales'] = 'Sales';
$app_list_strings['hr01_resumes_category_dom'][''] = '';
$app_list_strings['hr01_resumes_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['hr01_resumes_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['hr01_resumes_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['hr01_resumes_subcategory_dom'][''] = '';
$app_list_strings['hr01_resumes_status_dom']['Active'] = 'Active';
$app_list_strings['hr01_resumes_status_dom']['Draft'] = 'Draft';
$app_list_strings['hr01_resumes_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['hr01_resumes_status_dom']['Expired'] = 'Expired';
$app_list_strings['hr01_resumes_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['hr01_resumes_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['HR01_Resumes'] = 'Resumes';
$app_list_strings['moduleListSingular']['HR01_Resumes'] = 'Resume';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_job_category_list.php

 // created: 2017-07-20 16:44:55

$app_list_strings['job_category_list']=array (
  '' => '',
  'Analytical' => 'Analytical',
  'Animal Care' => 'Animal Care',
  'Business Development' => 'Business Development',
  'Histology' => 'Histology',
  'IVT' => 'IVT',
  'Other' => 'Other',
  'Pathologist' => 'Pathologist',
  'Pharmacology' => 'Pharmacology',
  'Quality Assurance' => 'Quality Assurance',
  'Regulatory Services' => 'Regulatory Services',
  'Research Tech_SA' => 'Research Tech - SA',
  'Research Tech_LA' => 'Research Tech - LA',
  'Scientist' => 'Scientist',
  'Surgical Tech' => 'Surgical Tech',
  'Toxicology' => 'Toxicology',
  'Veterinarian' => 'Veterinarian',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.IntelliDocs.php


/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['idoc_documents_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['idoc_documents_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['idoc_documents_category_dom']['Sales'] = 'Sales';
$app_list_strings['idoc_documents_category_dom'][''] = '';
$app_list_strings['idoc_documents_status_dom']['Active'] = 'Active';
$app_list_strings['idoc_documents_status_dom']['Draft'] = 'Draft';
$app_list_strings['idoc_documents_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['idoc_documents_status_dom']['Expired'] = 'Expired';
$app_list_strings['idoc_documents_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['idoc_documents_status_dom']['Pending'] = 'Pending';
$app_list_strings['idoc_documents_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['idoc_documents_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['idoc_documents_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['idoc_documents_subcategory_dom'][''] = '';
$app_list_strings['moduleList']['idoc_documents'] = 'Flexidocs Documents';
$app_list_strings['moduleList']['idoc_signers'] = 'Flexidocs Signers';
$app_list_strings['moduleList']['idoc_events'] = 'Flexidocs Events';
$app_list_strings['moduleList']['idoc_doc_events'] = 'Events';
$app_list_strings['moduleList']['idoc_document_templates'] = 'Document Templates';
$app_list_strings['moduleListSingular']['idoc_documents'] = 'Flexidocs Document';
$app_list_strings['moduleListSingular']['idoc_signers'] = 'Flexidocs Signer';
$app_list_strings['moduleListSingular']['idoc_events'] = 'Flexidocs Event';
$app_list_strings['moduleListSingular']['idoc_doc_events'] = 'Event';
$app_list_strings['moduleListSingular']['idoc_document_templates'] = 'Document Template';
$app_list_strings['idoc_document_status_list']['merged'] = 'Document Generated';
$app_list_strings['idoc_document_status_list']['document_converted'] = 'Document Converted';
$app_list_strings['idoc_document_status_list']['manually_uploaded'] = 'Manually Uploaded';
$app_list_strings['idoc_document_status_list']['sent_for_signing'] = 'Sent For Signing';
$app_list_strings['idoc_document_status_list']['signature_request_viewed'] = 'Signature Request Viewed';
$app_list_strings['idoc_document_status_list']['partially_signed'] = 'Document Partially Signed';
$app_list_strings['idoc_document_status_list']['signed'] = 'Signed by All Parties';
$app_list_strings['idoc_document_status_list']['letter_sent'] = 'Letter Sent';
$app_list_strings['idoc_document_status_list']['signature_error'] = 'Signature Error';
$app_list_strings['idoc_document_status_list']['signature_cancelled'] = 'Signature Cancelled';
$app_list_strings['idoc_document_status_list'][''] = '';
$app_list_strings['esign_type_list']['DocuSign'] = 'DocuSign';
$app_list_strings['esign_type_list']['EchoSign'] = 'EchoSign';
$app_list_strings['esign_type_list']['HelloSign'] = 'HelloSign';
$app_list_strings['esign_type_list'][''] = '';
$app_list_strings['idoc_events_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['idoc_events_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['idoc_events_category_dom']['Sales'] = 'Sales';
$app_list_strings['idoc_events_category_dom'][''] = '';
$app_list_strings['idoc_events_status_dom']['Active'] = 'Active';
$app_list_strings['idoc_events_status_dom']['Draft'] = 'Draft';
$app_list_strings['idoc_events_status_dom']['Expired'] = 'Expired';
$app_list_strings['idoc_events_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['idoc_events_status_dom']['Pending'] = 'Pending';
$app_list_strings['idoc_events_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['idoc_events_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['idoc_events_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['idoc_events_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['idoc_events_subcategory_dom'][''] = '';
$app_list_strings['idoc_doc_events_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['idoc_doc_events_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['idoc_doc_events_category_dom']['Sales'] = 'Sales';
$app_list_strings['idoc_doc_events_category_dom'][''] = '';
$app_list_strings['idoc_doc_events_status_dom']['Active'] = 'Active';
$app_list_strings['idoc_doc_events_status_dom']['Draft'] = 'Draft';
$app_list_strings['idoc_doc_events_status_dom']['Expired'] = 'Expired';
$app_list_strings['idoc_doc_events_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['idoc_doc_events_status_dom']['Pending'] = 'Pending';
$app_list_strings['idoc_doc_events_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['idoc_doc_events_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['idoc_doc_events_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['idoc_doc_events_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['idoc_doc_events_subcategory_dom'][''] = '';
$app_list_strings['moduleList']['idoc_templates'] = 'Flexidocs Templates';
$app_list_strings['moduleListSingular']['idoc_templates'] = 'Flexidocs Template';
$app_list_strings['cstm_document_status']['active'] = 'Active';
$app_list_strings['cstm_document_status']['inactive'] = 'Inactive';
$app_strings['LBL_INTELLIDOCS_DOCUMENT_STATUS'] = 'Document Status';
$app_strings['LBL_TIMELINE_LIMIT'] = 'Timeline limit';
// country code list
$app_list_strings['country_code'] = array(
	'AF' => 'Afghanistan',
	'AX' => 'Aland Islands',
	'AL' => 'Albania',
	'DZ' => 'Algeria',
	'AS' => 'American Samoa',
	'AD' => 'Andorra',
	'AO' => 'Angola',
	'AI' => 'Anguilla',
	'AQ' => 'Antarctica',
	'AG' => 'Antigua And Barbuda',
	'AR' => 'Argentina',
	'AM' => 'Armenia',
	'AW' => 'Aruba',
	'AU' => 'Australia',
	'AT' => 'Austria',
	'AZ' => 'Azerbaijan',
	'BS' => 'Bahamas',
	'BH' => 'Bahrain',
	'BD' => 'Bangladesh',
	'BB' => 'Barbados',
	'BY' => 'Belarus',
	'BE' => 'Belgium',
	'BZ' => 'Belize',
	'BJ' => 'Benin',
	'BM' => 'Bermuda',
	'BT' => 'Bhutan',
	'BO' => 'Bolivia',
	'BA' => 'Bosnia And Herzegovina',
	'BW' => 'Botswana',
	'BV' => 'Bouvet Island',
	'BR' => 'Brazil',
	'IO' => 'British Indian Ocean Territory',
	'BN' => 'Brunei Darussalam',
	'BG' => 'Bulgaria',
	'BF' => 'Burkina Faso',
	'BI' => 'Burundi',
	'KH' => 'Cambodia',
	'CM' => 'Cameroon',
	'CA' => 'Canada',
	'CV' => 'Cape Verde',
	'KY' => 'Cayman Islands',
	'CF' => 'Central African Republic',
	'TD' => 'Chad',
	'CL' => 'Chile',
	'CN' => 'China',
	'CX' => 'Christmas Island',
	'CC' => 'Cocos (Keeling) Islands',
	'CO' => 'Colombia',
	'KM' => 'Comoros',
	'CG' => 'Congo',
	'CD' => 'Congo, Democratic Republic',
	'CK' => 'Cook Islands',
	'CR' => 'Costa Rica',
	'CI' => 'Cote D\'Ivoire',
	'HR' => 'Croatia',
	'CU' => 'Cuba',
	'CY' => 'Cyprus',
	'CZ' => 'Czech Republic',
	'DK' => 'Denmark',
	'DJ' => 'Djibouti',
	'DM' => 'Dominica',
	'DO' => 'Dominican Republic',
	'EC' => 'Ecuador',
	'EG' => 'Egypt',
	'SV' => 'El Salvador',
	'GQ' => 'Equatorial Guinea',
	'ER' => 'Eritrea',
	'EE' => 'Estonia',
	'ET' => 'Ethiopia',
	'FK' => 'Falkland Islands (Malvinas)',
	'FO' => 'Faroe Islands',
	'FJ' => 'Fiji',
	'FI' => 'Finland',
	'FR' => 'France',
	'GF' => 'French Guiana',
	'PF' => 'French Polynesia',
	'TF' => 'French Southern Territories',
	'GA' => 'Gabon',
	'GM' => 'Gambia',
	'GE' => 'Georgia',
	'DE' => 'Germany',
	'GH' => 'Ghana',
	'GI' => 'Gibraltar',
	'GR' => 'Greece',
	'GL' => 'Greenland',
	'GD' => 'Grenada',
	'GP' => 'Guadeloupe',
	'GU' => 'Guam',
	'GT' => 'Guatemala',
	'GG' => 'Guernsey',
	'GN' => 'Guinea',
	'GW' => 'Guinea-Bissau',
	'GY' => 'Guyana',
	'HT' => 'Haiti',
	'HM' => 'Heard Island & Mcdonald Islands',
	'VA' => 'Holy See (Vatican City State)',
	'HN' => 'Honduras',
	'HK' => 'Hong Kong',
	'HU' => 'Hungary',
	'IS' => 'Iceland',
	'IN' => 'India',
	'ID' => 'Indonesia',
	'IR' => 'Iran, Islamic Republic Of',
	'IQ' => 'Iraq',
	'IE' => 'Ireland',
	'IM' => 'Isle Of Man',
	'IL' => 'Israel',
	'IT' => 'Italy',
	'JM' => 'Jamaica',
	'JP' => 'Japan',
	'JE' => 'Jersey',
	'JO' => 'Jordan',
	'KZ' => 'Kazakhstan',
	'KE' => 'Kenya',
	'KI' => 'Kiribati',
	'KR' => 'Korea',
	'KW' => 'Kuwait',
	'KG' => 'Kyrgyzstan',
	'LA' => 'Lao People\'s Democratic Republic',
	'LV' => 'Latvia',
	'LB' => 'Lebanon',
	'LS' => 'Lesotho',
	'LR' => 'Liberia',
	'LY' => 'Libyan Arab Jamahiriya',
	'LI' => 'Liechtenstein',
	'LT' => 'Lithuania',
	'LU' => 'Luxembourg',
	'MO' => 'Macao',
	'MK' => 'Macedonia',
	'MG' => 'Madagascar',
	'MW' => 'Malawi',
	'MY' => 'Malaysia',
	'MV' => 'Maldives',
	'ML' => 'Mali',
	'MT' => 'Malta',
	'MH' => 'Marshall Islands',
	'MQ' => 'Martinique',
	'MR' => 'Mauritania',
	'MU' => 'Mauritius',
	'YT' => 'Mayotte',
	'MX' => 'Mexico',
	'FM' => 'Micronesia, Federated States Of',
	'MD' => 'Moldova',
	'MC' => 'Monaco',
	'MN' => 'Mongolia',
	'ME' => 'Montenegro',
	'MS' => 'Montserrat',
	'MA' => 'Morocco',
	'MZ' => 'Mozambique',
	'MM' => 'Myanmar',
	'NA' => 'Namibia',
	'NR' => 'Nauru',
	'NP' => 'Nepal',
	'NL' => 'Netherlands',
	'AN' => 'Netherlands Antilles',
	'NC' => 'New Caledonia',
	'NZ' => 'New Zealand',
	'NI' => 'Nicaragua',
	'NE' => 'Niger',
	'NG' => 'Nigeria',
	'NU' => 'Niue',
	'NF' => 'Norfolk Island',
	'MP' => 'Northern Mariana Islands',
	'NO' => 'Norway',
	'OM' => 'Oman',
	'PK' => 'Pakistan',
	'PW' => 'Palau',
	'PS' => 'Palestinian Territory, Occupied',
	'PA' => 'Panama',
	'PG' => 'Papua New Guinea',
	'PY' => 'Paraguay',
	'PE' => 'Peru',
	'PH' => 'Philippines',
	'PN' => 'Pitcairn',
	'PL' => 'Poland',
	'PT' => 'Portugal',
	'PR' => 'Puerto Rico',
	'QA' => 'Qatar',
	'RE' => 'Reunion',
	'RO' => 'Romania',
	'RU' => 'Russian Federation',
	'RW' => 'Rwanda',
	'BL' => 'Saint Barthelemy',
	'SH' => 'Saint Helena',
	'KN' => 'Saint Kitts And Nevis',
	'LC' => 'Saint Lucia',
	'MF' => 'Saint Martin',
	'PM' => 'Saint Pierre And Miquelon',
	'VC' => 'Saint Vincent And Grenadines',
	'WS' => 'Samoa',
	'SM' => 'San Marino',
	'ST' => 'Sao Tome And Principe',
	'SA' => 'Saudi Arabia',
	'SN' => 'Senegal',
	'RS' => 'Serbia',
	'SC' => 'Seychelles',
	'SL' => 'Sierra Leone',
	'SG' => 'Singapore',
	'SK' => 'Slovakia',
	'SI' => 'Slovenia',
	'SB' => 'Solomon Islands',
	'SO' => 'Somalia',
	'ZA' => 'South Africa',
	'GS' => 'South Georgia And Sandwich Isl.',
	'ES' => 'Spain',
	'LK' => 'Sri Lanka',
	'SD' => 'Sudan',
	'SR' => 'Suriname',
	'SJ' => 'Svalbard And Jan Mayen',
	'SZ' => 'Swaziland',
	'SE' => 'Sweden',
	'CH' => 'Switzerland',
	'SY' => 'Syrian Arab Republic',
	'TW' => 'Taiwan',
	'TJ' => 'Tajikistan',
	'TZ' => 'Tanzania',
	'TH' => 'Thailand',
	'TL' => 'Timor-Leste',
	'TG' => 'Togo',
	'TK' => 'Tokelau',
	'TO' => 'Tonga',
	'TT' => 'Trinidad And Tobago',
	'TN' => 'Tunisia',
	'TR' => 'Turkey',
	'TM' => 'Turkmenistan',
	'TC' => 'Turks And Caicos Islands',
	'TV' => 'Tuvalu',
	'UG' => 'Uganda',
	'UA' => 'Ukraine',
	'AE' => 'United Arab Emirates',
	'GB' => 'United Kingdom',
	'US' => 'United States',
	'UM' => 'United States Outlying Islands',
	'UY' => 'Uruguay',
	'UZ' => 'Uzbekistan',
	'VU' => 'Vanuatu',
	'VE' => 'Venezuela',
	'VN' => 'Viet Nam',
	'VG' => 'Virgin Islands, British',
	'VI' => 'Virgin Islands, U.S.',
	'WF' => 'Wallis And Futuna',
	'EH' => 'Western Sahara',
	'YE' => 'Yemen',
	'ZM' => 'Zambia',
	'ZW' => 'Zimbabwe',
);
	



?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Animals.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['ANML_Animals'] = 'Animals';
$app_list_strings['moduleListSingular']['ANML_Animals'] = 'Animal';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Work_Product_Enrollment.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['WPE_Work_Product_Enrollment'] = 'Work Product Enrollment';
$app_list_strings['moduleListSingular']['WPE_Work_Product_Enrollment'] = 'Work Product Enrollment';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.AnimalsModuleCustomizations.php
 
$app_list_strings['sex_list'] = array (
  'Choose One' => 'Choose One',
  'Female' => 'Female',
  'Castrated Male' => 'Castrated Male',
  'Male' => 'Male',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['enrollment_status_list'] = array (
  'Choose One' => 'Choose One',
  'Assigned Backup' => 'Assigned Backup',
  'Assigned On Study' => 'Assigned On Study',
  'Deceased' => 'Deceased',
  'Non naive Stock' => 'Non-naive Stock',
  'Not Used' => 'Not Used',
  'On Study' => 'On Study',
  'On Study Backup' => 'On Study Backup',
  'On Study Transferred' => 'On Study Transferred',
  'Stock' => 'Stock',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['vaccine_a_list'] = array (
  'Choose One' => 'Choose One',
  'Mycoplasma' => 'Mycoplasma',
  'Lawsonia' => 'Lawsonia',
  'Rabies' => 'Rabies',
  'Bordetella' => 'Bordetella',
  'DHPP' => 'DHPP',
  'CDT' => 'CDT',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['species_list'] = array (
  'Choose One' => 'Choose One',
  'Bovine' => 'Bovine',
  'Canine' => 'Canine',
  'Caprine' => 'Caprine',
  'Guinea Pig' => 'Guinea Pig',
  'Hamster' => 'Hamster',
  'Lagomorph' => 'Lagomoprh',
  'Mouse' => 'Mouse',
  'Ovine' => 'Ovine',
  'Porcine' => 'Porcine',
  'Rat' => 'Rat',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['Breed_List'] = array (
  'Choose One' => 'Choose One',
  'Athymic Nude' => 'Athymic Nude',
  'Beagle' => 'Beagle',
  'CD1' => 'CD1',
  'CF1' => 'CF1',
  'Cross Breed' => 'Cross Breed',
  'Golden Syrian' => 'Golden Syrian',
  'Gottingen' => 'Gottingen',
  'Hartley' => 'Hartley',
  'Holstein' => 'Holstein',
  'Mongrel' => 'Mongrel',
  'ND4' => 'ND4',
  'New Zealand White' => 'New Zealand White',
  'Polypay' => 'Polypay',
  'Sinclair' => 'Sinclair',
  'Sprague Dawley' => 'Sprague Dawley',
  'Suffolk X' => 'Suffolk X',
  'Watanabe' => 'Watanabe',
  'Yorkshire X' => 'Yorkshire X',
  'Yucatan' => 'Yucatan',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['vendor_list'] = array (
  'Choose One' => 'Choose One',
  'APS' => 'APS',
  'Bakkom Rabbitry' => 'Bakkom Rabbitry',
  'Charles River' => 'Charles River',
  'Elm Hill' => 'Elm Hill',
  'Envigo' => 'Envigo',
  'Lonestar Laboratory Swine' => 'Lonestar Laboratory Swine',
  'Manthei Hogs' => 'Manthei Hogs',
  'Marshall BioResources' => 'Marshall BioResources',
  'Neaton Polupays' => 'Neaton Polupays',
  'Oak Hill Genetics' => 'Oak Hill Genetics',
  'Purdue University' => 'Purdue University',
  'Rigland Farms' => 'Rigland Farms',
  'S and S Farms' => 'S & S Farms',
  'Sinclair BioResources' => 'Sinclair BioResources',
  'Twin Valley Kennels' => 'Twin Valley Kennels',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['room_list'] = array (
  'Choose One' => 'Choose One',
  89452 => '8945-2',
  89453 => '8945-3',
  89454 => '8945-4',
  89455 => '8945-5',
  89456 => '8945-6',
  89457 => '8945-7',
  89458 => '8945-8',
  89459 => '8945-9',
  894510 => '8945-10',
  894511 => '8945-11',
  894512 => '8945-12',
  894513 => '8945-13',
  894514 => '8945-14',
  894515 => '8945-15',
  894516 => '8945-16',
  894519 => '8945-19',
  894520 => '8945-20',
  894521 => '8945-21',
  894522 => '8945-22',
  894523 => '8945-23',
  894524 => '8945-24',
  894525 => '8945-25',
  894526 => '8945-26',
  894527 => '8945-27',
  894528 => '8945-28',
  894529 => '8945-29',
  894530 => '8945-30',
  894531 => '8945-31',
  894532 => '8945-32',
  894533 => '8945-33',
  894534 => '8945-34',
  78035 => '780-35',
  78036 => '780-36',
  78037 => '780-37',
  78038 => '780-38',
  78039 => '780-39',
  89601 => '8960-1',
  89602 => '8960-2',
  89603 => '8960-3',
  89604 => '8960-4',
  89605 => '8960-5',
  89606 => '8960-6',
  89607 => '8960-7',
  89608 => '8960-8',
  89609 => '8960-9',
  896010 => '8960-10',
  896011 => '8960-11',
  896012 => '8960-12',
  896013 => '8960-13',
  896014 => '8960-14',
  896015 => '8960-15',
  896016 => '8960-16',
  896017 => '8960-17',
  896018 => '8960-18',
  896019 => '8960-19',
  896020 => '8960-20',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['usda_category_list'] = array (
  'B' => 'B',
  'C' => 'C',
  'D' => 'D',
  'E' => 'E',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.WPEsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.WPEsModuleCustomizations.php
 
$app_list_strings['usda_category_list'] = array (
  'B' => 'B',
  'C' => 'C',
  'D' => 'D',
  'E' => 'E',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['enrollment_status_list'] = array (
  'Choose One' => 'Choose One',
  'Assigned Backup' => 'Assigned Backup',
  'Assigned On Study' => 'Assigned On Study',
  'Deceased' => 'Deceased',
  'Non naive Stock' => 'Non-naive Stock',
  'Not Used' => 'Not Used',
  'On Study' => 'On Study',
  'On Study Backup' => 'On Study Backup',
  'On Study Transferred' => 'On Study Transferred',
  'Stock' => 'Stock',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.customa1a_critical_phase_inspectio_activities_1_calls.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.customa1a_critical_phase_inspectio_activities_1_calls.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$app_list_strings['record_type_display']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['parent_type_display']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['record_type_display_notes']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$app_list_strings['record_type_display']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['parent_type_display']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['record_type_display_notes']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Tradeshow_Management.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['TM_Tradeshow_Management'] = 'Tradeshow_Management';
$app_list_strings['moduleListSingular']['TM_Tradeshow_Management'] = 'Tradeshow_Management';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sales_focus_list.php

 // created: 2018-01-17 22:50:38

$app_list_strings['sales_focus_list']=array (
  '' => '',
  'Full APS Program' => 'Full APS Program',
  'Biocompatibility' => 'Biocompatibility',
  'Analytical' => 'Analytical',
  'Biocompatibility_Analytical' => 'Biocompatibility/Analytical',
  'Bioskills' => 'Bioskills',
  'ISR' => 'ISR',
  'Pharmacology' => 'Pharmacology',
  'Regulatory' => 'Regulatory',
  'Toxicology' => 'Toxicology',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_primary_activity_list.php

 // created: 2018-01-18 19:41:58

$app_list_strings['primary_activity_list']=array (
  '' => '',
  'Attend' => 'Attend',
  'Exhibit' => 'Exhibit',
  'Presentation' => 'Podium Presentation',
  'Poster Presentation' => 'Poster Presentation',
  'Sponsor' => 'Sponsor',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Visitor_Management.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['VM01_Visitor_Management'] = 'Visitor Management';
$app_list_strings['moduleListSingular']['VM01_Visitor_Management'] = 'Visitor Management';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_visit_status_list.php

 // created: 2018-01-22 20:56:17

$app_list_strings['visit_status_list']=array (
  '' => '',
  'Signed In' => 'Signed In',
  'Signed Out' => 'Signed Out',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_error_occured_on_weekend_list.php

 // created: 2018-01-30 16:50:09

$app_list_strings['error_occured_on_weekend_list']=array (
  'No' => 'No',
  'Yes' => 'Yes',
  'Choose One' => 'Choose One',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Equipment.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['Equip_Equipment'] = 'Equipment';
$app_list_strings['moduleListSingular']['Equip_Equipment'] = 'Equipment';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Error_Employees.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['Ere_Error_Employees'] = 'Errors Employees';
$app_list_strings['moduleListSingular']['Ere_Error_Employees'] = 'Error Employee';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Error_Documents.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['Erd_Error_Documents'] = 'Errors Documents';
$app_list_strings['moduleListSingular']['Erd_Error_Documents'] = 'Error Documents';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Protocol_Amendments.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['M99_Protocol_Amendments'] = 'Protocol Amendments';
$app_list_strings['moduleListSingular']['M99_Protocol_Amendments'] = 'Protocol Amendment';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Acquired_Companies.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['AC01_Acquired_Companies'] = 'Acquired Companies';
$app_list_strings['moduleListSingular']['AC01_Acquired_Companies'] = 'Acquired Company';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Error_Departments.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['ED_Errors_Department'] = 'Errors Departments';
$app_list_strings['moduleListSingular']['ED_Errors_Department'] = 'Errors Department';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_deceased_list.php

 // created: 2018-04-12 13:31:22

$app_list_strings['deceased_list']=array (
  'No' => 'No',
  'Yes' => 'Yes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Tradeshow_Activities.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['TA_Tradeshow_Activities'] = 'Tradeshow Activities';
$app_list_strings['moduleListSingular']['TA_Tradeshow_Activities'] = 'Tradeshow Activity';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_lead_quality_list.php

 // created: 2018-04-23 17:16:36

$app_list_strings['lead_quality_list']=array (
  '' => '',
  'Low' => 'Low',
  'Medium' => 'Medium',
  'High' => 'High',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_test_control_article_checkin_list.php

 // created: 2018-04-30 18:05:47

$app_list_strings['test_control_article_checkin_list']=array (
  'Not Completedq' => 'Not Completed',
  'Completed' => 'Completed',
  'Not Applicable' => 'Not Applicable',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sales_activity_source_list.php

 // created: 2018-05-08 19:51:27

$app_list_strings['sales_activity_source_list']=array (
  '' => '',
  'Current Sponsor' => 'Current Sponsor',
  'In Coming Cold Call' => 'In Coming Cold Call',
  'Trade Show' => 'Trade Show',
  'Referral' => 'Referral',
  'Website' => 'APS Website',
  'Third Party Website' => 'Third Party Website',
  'Surpass' => 'Surpass',
  'Cold Call by APS' => 'Cold Call by APS',
  'Advertisement' => 'Advertisement',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Anatomy_Database.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['AD_Anatomy_Database'] = 'Anatomy Database';
$app_list_strings['moduleListSingular']['AD_Anatomy_Database'] = 'Anatomy Database';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_imaging_modality_list.php

 // created: 2018-05-21 16:38:21

$app_list_strings['imaging_modality_list']=array (
  '' => '',
  'Angiography' => 'Angiography',
  'CT' => 'CT',
  'Ex_Vivo' => 'Ex-Vivo',
  'IVUS' => 'IVUS',
  'MRI' => 'MRI',
  'OCT' => 'OCT',
  'Ultrasound' => 'Ultrasound',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_anatomical_location_list.php

 // created: 2018-05-21 18:42:27

$app_list_strings['anatomical_location_list']=array (
  '' => '',
  'Aorta_mid' => 'Aorta (Thoracic)',
  'Aorta_Arch' => 'Aorta (Arch)',
  'Aortic_Annulus' => 'Aortic Annulus',
  'Aortic_arch_length' => 'Aortic Arch Length',
  'C_C_max' => 'Mitral C-C',
  'LCX_Mean_Diameter' => 'LCX Mean Diameter',
  'LAD_Mean_Diameter' => 'LAD Mean Diameter',
  'RCA_Mean_Diameter' => 'RCA Mean Diameter',
  'LEF_Mean_Diameter' => 'LEF Mean Diameter',
  'REF_Mean_Diameter' => 'REF Mean Diameter',
  'LIF_Mean_Diameter' => 'LIF Mean Diameter',
  'RIF_Mean_Diameter' => 'RIF Mean Diameter',
  'LII_Mean_Diameter' => 'LII Mean Diameter',
  'RII_Mean_Diameter' => 'RII Mean Diameter',
  'LEI_Mean_Diameter' => 'LEI Mean Diameter',
  'REI_Mean_Diameter' => 'REI Mean Diameter',
  'Right_Renal_Mean Diameter' => 'Right Renal Mean Diameter',
  'Left_Renal_Mean_Diameter' => 'Left Renal Mean Diameter',
  'Mitral_AP' => 'Mitral A-P',
  'Right_Carotid_Artery' => 'Right Carotid Mean Diameter',
  'Left_Carotid' => 'Left Carotid Mean Diameter',
  'Left_Axillary_Artery_Mean_Diameter' => 'Left Axillary Artery Mean Diameter',
  'Left_Costocervical_Artery_Mean_Diameter' => 'Left Costocervical Artery Mean Diameter',
  'Right_Costocervical_Artery_Mean_Diameter' => 'Right Costocervical Artery Mean Diameter',
  'Left_Internal_Mammary_Artery_Mean_Diameter' => 'Left Internal Mammary Artery Mean Diameter',
  'Right_Internal_Mammary_Artery_Mean_Diameter' => 'Right Internal Mammary Artery Mean Diameter',
  'Left_Internal_Thoracic_Artery_Mean_Diameter' => 'Left Internal Thoracic Artery Mean Diameter',
  'Right_Internal_Thoracic_Artery_Mean_Diameter' => 'Right Internal Thoracic Artery Mean Diameter',
  'Left_Vertebral_Artery_Mean_Diameter' => 'Left Vertebral Artery Mean Diameter',
  'Right_Verterbral_Artery_Mean_Diameter' => 'Right Verterbral Artery Mean Diameter',
  'Mesenteric_Artery_Mean_Diameter' => 'Mesenteric Artery Mean Diameter',
  'Right_Subclavian_Mean_Diameter' => 'Right Subclavian Mean Diameter',
  'LA Height' => 'LA Height',
  'Right_Femoral_Vein Diameter' => 'Right Femoral Vein Diameter',
  'Left_Femoral_Vein_Diameter' => 'Left Femoral Vein Diameter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_weight_range_list.php

 // created: 2018-05-21 19:52:21

$app_list_strings['weight_range_list']=array (
  '' => '',
  '0_10' => '0 - 10 kg',
  '11_20' => '11 - 20 kg',
  '20_30' => '20 - 30 kg',
  '30_45' => '30 - 45 kg',
  '45_60' => '45 - 60 kg',
  '60_80' => '60 - 80 kg',
  '80_100' => '80 - 100 kg',
  '100_plus' => '100+ kg',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Error_QC_Employees.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['ErQC_Error_QC_Employees'] = 'Errors QC Employees';
$app_list_strings['moduleListSingular']['ErQC_Error_QC_Employees'] = 'Error QC Employee';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_portal_account_activated_list.php

 // created: 2018-06-12 14:13:57

$app_list_strings['portal_account_activated_list']=array (
  'No' => 'No',
  'Yes' => 'Yes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_procedure_room_type_list.php

 // created: 2018-06-26 18:52:22

$app_list_strings['procedure_room_type_list']=array (
  '' => '',
  'Cath_Lab' => 'Cath Lab',
  'OR' => 'OR',
  'Necropsy' => 'Necropsy',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_required_equipment_list.php

 // created: 2018-06-26 19:01:12

$app_list_strings['required_equipment_list']=array (
  '' => '',
  'IVUS' => 'IVUS',
  'OCT' => 'OCT',
  'PowerLab' => 'PowerLab',
  'Endoscopy_Tower' => 'Endoscopy Tower',
  'Portable_CARM' => 'Portable C-Arm',
  'Ultrasound' => 'Ultrasound',
  'Velocity' => 'Velocity',
  'Bard' => 'Bard',
  'RF Generator' => 'RF Generator',
  'EP Combination' => 'EP Combination',
  'Cardiac CT' => 'Cardiac CT',
  'Other CT' => 'Other CT',
  'Hourly Holding' => 'Hourly Holding',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_regulatory_region_list.php

 // created: 2018-06-26 19:10:09

$app_list_strings['regulatory_region_list']=array (
  '' => '',
  'FDA' => 'FDA',
  'CE' => 'CE',
  'JMHLW' => 'JMHLW',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_required_reports_list.php

 // created: 2018-06-26 19:14:50

$app_list_strings['required_reports_list']=array (
  '' => '',
  'Animal Health' => 'Animal Health',
  'Gross Pathology' => 'Gross Pathology',
  'Gross and Histopathology' => 'Gross and Histopathology',
  'Analytical Report' => 'Analytical Report',
  'In_Life Report' => 'In-Life Report',
  'Final Report' => 'Final Report',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_cpi_finding_category_list.php

 // created: 2018-09-06 18:12:59

$app_list_strings['cpi_finding_category_list']=array (
  '' => '',
  'Protocol' => 'Protocol',
  'SOP' => 'SOP',
  'GLP' => 'GLP',
  'ISO' => 'ISO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_timeline_type_list.php

 // created: 2018-09-13 19:49:36

$app_list_strings['timeline_type_list']=array (
  '' => '',
  'Standard' => 'Standard',
  'Expedited' => 'Expedited',
  'Priority Expedited' => 'Priority Expedited',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_chronic_study_list.php

 // created: 2018-09-14 13:29:18

$app_list_strings['chronic_study_list']=array (
  '' => '',
  'No' => 'No',
  'Yes' => 'Yes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_duration_greater_than_4_week_list.php

 // created: 2018-09-14 13:32:15

$app_list_strings['duration_greater_than_4_week_list']=array (
  '' => '',
  'No' => 'No',
  'Yes' => 'Yes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_view_all_company_projects_list.php

 // created: 2018-09-17 17:36:04

$app_list_strings['view_all_company_projects_list']=array (
  '' => '',
  'No' => 'No',
  'Yes' => 'Yes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_deviation_type_list.php

 // created: 2018-07-30 20:28:31

$app_list_strings['deviation_type_list']=array (
  '' => '',
  'Entry' => 'Entry',
  'Review' => 'Review',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_responsible_personnel_list.php

 // created: 2018-07-06 13:53:55

$app_list_strings['responsible_personnel_list']=array (
  'Entry' => 'Entry',
  'Review' => 'Review',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Deviation_Employees.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['DE_Deviation_Employees'] = 'Deviation Employees';
$app_list_strings['moduleListSingular']['DE_Deviation_Employees'] = 'Deviation Employees';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_is_manager_list.php

 // created: 2018-08-21 09:06:45

$app_list_strings['is_manager_list']=array (
  '' => '',
  'yes' => 'Yes',
  'no' => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_employee_status_dom.php

 // created: 2018-09-25 12:50:07

$app_list_strings['employee_status_dom']=array (
  'Active' => 'Active',
  'Leave of Absence' => 'Leave of Absence',
  'Non APS Study Director' => 'Non APS Study Director',
  'Terminated' => 'Terminated',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_functional_area_list.php

 // created: 2018-09-25 15:41:09

$app_list_strings['functional_area_list']=array (
  '' => '',
  'Analytical Services' => 'Analytical',
  'Animal Holding' => 'Animal Holding',
  'Consulting Services' => 'Consulting Services',
  'Cytotoxicity' => 'Cytotoxicity',
  'Genotoxicity' => 'Genotoxicity',
  'Hemocompatibility' => 'Hemocompatibility',
  'Implantation' => 'Implantation',
  'Interventional Surgical' => 'Interventional and Surgical',
  'Irritation' => 'Irritation',
  'Miscellaneous Testing' => 'Miscellaneous Testing',
  'Pathology' => 'Pathology',
  'Pharmacology' => 'Pharmacology',
  'Regulatory' => 'Regulatory',
  'Sensitization' => 'Sensitization',
  'Systemic Toxicity' => 'Systemic Toxicity',
  'Toxicity' => 'Toxicity',
  'Training' => 'Training',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Study_Workflow.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['SW_Study_Workflow'] = 'Study Workflow';
$app_list_strings['moduleListSingular']['SW_Study_Workflow'] = 'Study Workflow';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_departments_list.php

 // created: 2018-11-14 22:20:28

$app_list_strings['departments_list']=array (
  '' => '',
  'AS' => 'AS',
  'DVM' => 'DVM',
  'Facility' => 'Facility',
  'ISR' => 'ISR',
  'IVP' => 'IVT',
  'LA AC' => 'LA AC',
  'LA OP' => 'LA OP',
  'PS' => 'PS',
  'SA AC' => 'SA AC',
  'SA OP' => 'SA OP',
  'SCI' => 'SCI',
  'SP' => 'SP',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_instrument_list.php

 // created: 2018-11-15 14:38:45

$app_list_strings['instrument_list']=array (
  '' => '',
  'FTIR' => 'FTIR',
  'GCMS' => 'GC/MS',
  'ICPMS' => 'ICP-MS',
  'LCMS' => 'LC/MS',
  'LCMS GCMS ICPMS' => 'LC/MS, GC/MS, ICP/MS',
  'LCMS GCMS ICPMS FTIR' => 'LC/MS, GC/MS, ICP/MS, FTIR',
  'LCUV' => 'LC/UV',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_lead_auditor_list.php

 // created: 2018-11-20 15:07:49

$app_list_strings['lead_auditor_list']=array (
  'Choose Auditor' => 'Choose Auditor...',
  'Amy Malloy' => 'Amy Malloy',
  'Dani Zehowski' => 'Dani Zehowski',
  'Emily Markuson' => 'Emily Markuson',
  'Kristen Varas' => 'Kristen Varas',
  'Nick McCune' => 'Nick McCune',
  'Katie Jenkins' => 'Katie Jenkins',
  'Nicole Klee' => 'Nicole Klee',
  'Stephanie Beane' => 'Stephanie Beane',
  'Erica VanReeth' => 'Erica VanReeth',
  'Karen Bastyr' => 'Karen Bastyr',
  'Kris Ruppelius' => 'Kris Ruppelius',
  'Tammy Fossum' => 'Tammy Fossum',
  'BC Auditor Group' => 'BC Auditor Group',
  'External Contractor' => 'External Contractor',
  'None' => 'None',
  'Kevin Catalano' => 'Kevin Catalano',
  'Brenda Bailey' => 'Brenda Bailey',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_analytical_activity_list.php

 // created: 2018-11-21 17:55:35

$app_list_strings['analytical_activity_list']=array (
  '' => '',
  'Analytical Report Writing' => 'Analytical Report Writing',
  'Data Analysis' => 'Data Analysis',
  'Data Delivery To Toxicologist' => 'Data Delivery To Toxicologist',
  'Exhaustive exaggerated extraction start' => 'Exhaustive/Exaggerated Extraction Start',
  'Instrument Maintenance' => 'Instrument Maintenance',
  'Method Development' => 'Method Development',
  'Out of Office' => 'Out of Office',
  'Protocol Development' => 'Protocol Development',
  'Sample Analysis' => 'Sample Analysis',
  'Sample Prep' => 'Sample Prep',
  'Simulated Use Extraction Start' => 'Simulated Use Extraction Start',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_required.php

 // created: 2018-11-28 22:21:10

$app_list_strings['required']=array (
  '' => '',
  'H_E' => 'H&E',
  'Trichrome' => 'Trichrome',
  'Von Kossa' => 'Von Kossa',
  'Carstairs' => 'Carstairs',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_necropsy_type_list.php

 // created: 2018-11-28 22:43:26

$app_list_strings['necropsy_type_list']=array (
  '' => '',
  'Device_Target Tissue' => 'Device/Target Tissue',
  'Complete' => 'Complete',
  'Complete without Brain' => 'Complete w/o Brain',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_slide_size_type_list.php

 // created: 2018-11-29 17:34:30

$app_list_strings['slide_size_type_list']=array (
  '' => '',
  'Large' => 'Large',
  'Standard' => 'Standard',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_histopath_processing_type_list.php

 // created: 2018-11-29 17:41:51

$app_list_strings['histopath_processing_type_list']=array (
  '' => '',
  'Paraffin' => 'Paraffin',
  'Plastic' => 'Plastic',
  'Frozen' => 'Frozen',
  'SEM' => 'SEM',
  'Faxitron' => 'Faxitron',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.customm01_sales_activities_1_calls.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.customm01_sales_activities_1_calls.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$app_list_strings['record_type_display']['M01_Sales'] = 'Sales';
$app_list_strings['parent_type_display']['M01_Sales'] = 'Sales';
$app_list_strings['record_type_display_notes']['M01_Sales'] = 'Sales';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$app_list_strings['record_type_display']['M01_Sales'] = 'Sales';
$app_list_strings['parent_type_display']['M01_Sales'] = 'Sales';
$app_list_strings['record_type_display_notes']['M01_Sales'] = 'Sales';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_vendor_list.php

 // created: 2019-01-25 15:32:23

$app_list_strings['vendor_list']=array (
  'Choose One' => 'Choose One',
  'APS' => 'APS',
  'Bakkom Rabbitry' => 'Bakkom Rabbitry',
  'Central Minnesota Livestock Sourcing' => 'Central Minnesota Livestock Sourcing',
  'Charles River' => 'Charles River',
  'Covance' => 'Covance',
  'Elm Hill' => 'Elm Hill',
  'Envigo' => 'Envigo',
  'Exemplar' => 'Exemplar',
  'Jackson Labs' => 'Jackson Labs',
  'Lonestar Laboratory Swine' => 'Lonestar Laboratory Swine',
  'Manthei Hogs' => 'Manthei Hogs',
  'Marshall BioResources' => 'Marshall BioResources',
  'Neaton Polupays' => 'Neaton Polupays',
  'Oak Hill Genetics' => 'Oak Hill Genetics',
  'Oakwood' => 'Oakwood',
  'Ovis' => 'Ovis',
  'Pozzi Ranch' => 'Pozzi Ranch',
  'Purdue University' => 'Purdue University',
  'Recombinetics' => 'Recombinetics',
  'Rigland Farms' => 'Rigland Farms',
  'S and S Farms' => 'S & S Farms',
  'Simonsen Laboratories' => 'Simonsen Laboratories',
  'Sinclair BioResources' => 'Sinclair BioResources',
  'Twin Valley Kennels' => 'Twin Valley Kennels',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_document_status_list.php

 // created: 2019-01-29 00:06:47

$app_list_strings['document_status_list']=array (
  'Active' => 'Active',
  'Obsolete' => 'Obsolete',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Company_Documents.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['cd_company_documents_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['cd_company_documents_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['cd_company_documents_category_dom']['Sales'] = 'Sales';
$app_list_strings['cd_company_documents_category_dom'][''] = '';
$app_list_strings['cd_company_documents_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['cd_company_documents_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['cd_company_documents_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['cd_company_documents_subcategory_dom'][''] = '';
$app_list_strings['cd_company_documents_status_dom']['Active'] = 'Active';
$app_list_strings['cd_company_documents_status_dom']['Draft'] = 'Draft';
$app_list_strings['cd_company_documents_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['cd_company_documents_status_dom']['Expired'] = 'Expired';
$app_list_strings['cd_company_documents_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['cd_company_documents_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['CD_Company_Documents'] = 'Company Documents';
$app_list_strings['moduleListSingular']['CD_Company_Documents'] = 'Company Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Tradeshow_Documents.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['td_tradeshow_documents_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['td_tradeshow_documents_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['td_tradeshow_documents_category_dom']['Sales'] = 'Sales';
$app_list_strings['td_tradeshow_documents_category_dom'][''] = '';
$app_list_strings['td_tradeshow_documents_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['td_tradeshow_documents_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['td_tradeshow_documents_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['td_tradeshow_documents_subcategory_dom'][''] = '';
$app_list_strings['td_tradeshow_documents_status_dom']['Active'] = 'Active';
$app_list_strings['td_tradeshow_documents_status_dom']['Draft'] = 'Draft';
$app_list_strings['td_tradeshow_documents_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['td_tradeshow_documents_status_dom']['Expired'] = 'Expired';
$app_list_strings['td_tradeshow_documents_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['td_tradeshow_documents_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['TD_Tradeshow_Documents'] = 'Tradeshow Documents';
$app_list_strings['moduleListSingular']['TD_Tradeshow_Documents'] = 'Tradeshow Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_meeting_status_dom.php

 // created: 2019-03-13 11:59:45

$app_list_strings['meeting_status_dom']=array (
  'Not Held' => 'Canceled',
  'Completed' => 'Completed',
  'Held' => 'Held',
  'Obsolete' => 'Obsolete',
  'Planned' => 'Scheduled',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_document_type_list.php

 // created: 2019-04-05 11:15:00

$app_list_strings['document_type_list']=array (
  '' => '',
  'Change Order' => 'Change Order',
  'PO' => 'PO',
  'Study Quote' => 'Quote',
  'Quote Revision' => 'Quote Revision',
  'Signed Change Order' => 'Signed Change Order',
  'Signed Quote' => 'Signed Quote',
  'Signed Quote Revision' => 'Signed Quote Revision',
  'Signed SOW' => 'Signed SOW',
  'SOW' => 'SOW',
  'Study Design Document' => 'Study Design',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_study_coordinator_list.php

 // created: 2019-04-11 10:06:32

$app_list_strings['study_coordinator_list']=array (
  'Choose One' => 'Choose One',
  'Amy Puetz' => 'Amy Puetz',
  'Anton Crane' => 'Anton Crane',
  'Ben Vos' => 'Ben Vos',
  'Dan Kroeninger' => 'Dan Kroeninger',
  'DeeAnn Deiss' => 'DeeAnn Deiss',
  'Emily Visedo' => 'Emily Visedo',
  'Emma Reiss' => 'Emma Reiss',
  'Emma Squires Sperling' => 'Emma Squires-Sperling',
  'Erica VanReeth' => 'Erica VanReeth',
  'Hannah Schmidt' => 'Hannah Schmidt',
  'Kailyn Singh' => 'Kailyn Singh',
  'Katelyn Mortensen' => 'Katelyn Mortensen',
  'Kerry Trotter' => 'Kerry Trotter',
  'Liz Clark' => 'Liz Clark',
  'Liz Roby' => 'Liz Roby',
  'Steven Mamer' => 'Steven Mamer',
  'Tom Van Valkenburg' => 'Tom Van Valkenburg',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_room_list.php

 // created: 2019-04-11 10:15:10

$app_list_strings['room_list']=array (
  7801 => '780-1',
  7802 => '780-2',
  7803 => '780-3',
  7804 => '780-4',
  7805 => '780-5',
  89452 => '8945-2',
  89453 => '8945-3',
  89454 => '8945-4',
  89455 => '8945-5',
  89456 => '8945-6',
  89457 => '8945-7',
  89458 => '8945-8',
  89459 => '8945-9',
  894510 => '8945-10',
  894511 => '8945-11',
  894512 => '8945-12',
  894513 => '8945-13',
  894514 => '8945-14',
  894515 => '8945-15',
  894516 => '8945-16',
  894519 => '8945-19',
  894520 => '8945-20',
  894521 => '8945-21',
  894522 => '8945-22',
  894523 => '8945-23',
  894524 => '8945-24',
  894525 => '8945-25',
  894526 => '8945-26',
  894527 => '8945-27',
  894528 => '8945-28',
  894529 => '8945-29',
  894530 => '8945-30',
  894531 => '8945-31',
  894532 => '8945-32',
  894533 => '8945-33',
  894534 => '8945-34',
  894535 => '8945-35',
  894536 => '8945-36',
  894537 => '8945-37',
  894538 => '8945-38',
  89601 => '8960-1',
  89602 => '8960-2',
  89603 => '8960-3',
  89604 => '8960-4',
  89605 => '8960-5',
  89606 => '8960-6',
  89607 => '8960-7',
  89608 => '8960-8',
  89609 => '8960-9',
  896010 => '8960-10',
  896011 => '8960-11',
  896012 => '8960-12',
  896013 => '8960-13',
  896014 => '8960-14',
  896015 => '8960-15',
  896016 => '8960-16',
  896017 => '8960-17',
  896018 => '8960-18',
  896019 => '8960-19',
  896020 => '8960-20',
  'Choose One' => 'Choose One',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_archiving_type_of_contact_list.php

 // created: 2019-04-15 12:14:15

$app_list_strings['archiving_type_of_contact_list']=array (
  '' => '',
  '1st Contact Email' => '1st Contact - Email',
  '1st Contact Phone' => '1st Contact - Phone',
  '2nd Contact Email' => '2nd Contact - Email',
  '2nd Contact Phone' => '2nd Contact - Phone',
  '3rd Contact Email' => '3rd Contact - Email',
  '3rd Contact Phone' => '3rd Contact - Phone',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_department_id_list.php

 // created: 2019-04-24 11:27:38

$app_list_strings['department_id_list']=array (
  '' => '',
  'AS' => 'AS',
  'DVM' => 'DVM',
  'Facility' => 'Facility',
  'HS' => 'HS',
  'ISR' => 'ISR',
  'IVT' => 'IVT',
  'LA AC' => 'LA AC',
  'LA OP' => 'LA OP',
  'PH OP' => 'PH OP',
  'PS' => 'PS',
  'SA AC' => 'SA AC',
  'SA OP' => 'SA OP',
  'SCI' => 'SCI',
  'SP' => 'SP',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_veterinarian_list.php

 // created: 2019-05-02 12:18:42

$app_list_strings['veterinarian_list']=array (
  'Choose Veterinarian' => 'Choose Veterinarian...',
  'apinto' => 'Alejandra Pinto',
  'cnorman' => 'Claire Norman',
  'ebauer' => 'Emily Drake ',
  'jvislisel' => 'Joe Vislisel',
  'lszenay' => 'Lesley Szenay',
  'None' => 'none',
  'tpavek' => 'Todd Pavek',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_study_director_list.php

 // created: 2019-05-07 19:59:42

$app_list_strings['study_director_list']=array (
  '' => '',
  'Abby Beltrame' => 'Abby Beltrame',
  'Adam Blakstvedt' => 'Adam Blakstvedt',
  'Adrienne Schucker' => 'Adrienne Schucker',
  'Amapola Balancio' => 'Amapola Balancio',
  'Amy Puetz' => 'Amy Puetz',
  'Athanasios Peppas' => 'Athanasios Peppas',
  'Ben Vos' => 'Ben Vos',
  'Bich Nguyen' => 'Bich Nguyen',
  'Brian Alzua' => 'Brian Alzua',
  'Carlus Dingfelder' => 'Carlus Dingfelder',
  'Catherine Pipenhagen' => 'Catherine Pipenhagen',
  'Cheng Yang ' => 'Cheng Yang',
  'Cheryl Marker' => 'Cheryl Marker',
  'Choose APS PI' => 'Choose One...',
  'Chris Lafean' => 'Chris Lafean',
  'Chris Perry' => 'Chris Piere',
  'Christina Gross' => 'Christina Gross',
  'Corey Leet' => 'Corey Leet',
  'Dalia Shabashov Stone' => 'Dalia Shabashov Stone',
  'DeeAnn Deiss' => 'DeeAnn Deiss',
  'Dongmei May Yan' => 'Dongmei (May) Yan',
  'Elizabeth Kurpiers' => 'Elizabeth Kurpiers',
  'Emily Tobin' => 'Emily Tobin',
  'Emily Visedo' => 'Emily Visedo',
  'Emma Squires Sperling' => 'Emma Squires-Sperling',
  'Eric_Dobrave' => 'Eric Dobrava',
  'Erica Van Reeth' => 'Erica Van Reeth',
  'Erik Steinmetz' => 'Erik Steinmetz',
  'Gabe Quinn' => 'Gabe Quinn',
  'Haris Sih' => 'Haris Sih',
  'Heather Ackerson' => 'Heather Ackerson',
  'Heidi Dierks' => 'Heidi Dierks',
  'Ian Dean' => 'Ian Dean',
  'Igor Polyakov' => 'Igor Polyakov',
  'Jason Thorsten' => 'Jason Thorsten',
  'Jeff Benham' => 'Jeff Benham',
  'Jennifer Alkire' => 'Jennifer Alkire',
  'Jennifer Lane' => 'Jennifer Lane',
  'Jesse Heitke' => 'Jesse Heitke',
  'Jessica Rotschafer' => 'Jessica Rotschafer',
  'Jim Pomonis' => 'Jim Pomonis',
  'Joe Vislisel' => 'Joe Vislisel',
  'Jon Raybuck' => 'Jon Raybuck',
  'Kent Grove' => 'Kent Grove',
  'Kim Berg' => 'Kim Berg',
  'Kim Castle' => 'Kim Castle',
  'Kristen Nelson' => 'Kristen Nelson',
  'Kristin Booth' => 'Kristin Booth',
  'LaDonna Camrud' => 'LaDonna Camrud',
  'Laura Christoferson' => 'Laura Horton',
  'Lauren Frederick' => 'Lauren Frederick',
  'Leigh Kleinert' => 'Leigh Kleinert',
  'Liane Teplitsky' => 'Liane Teplitsky',
  'Libette Rowan' => 'Libette Rowan',
  'Liisa Carter' => 'Liisa Carter',
  'Lisa Clausen' => 'Lisa Clausen',
  'Liz Clark' => 'Liz Clark',
  'Liz ROby' => 'Liz Roby',
  'Lynette Phillips' => 'Lynette Phillips',
  'Marie Blommel' => 'Marie Blommel',
  'Mark Peterson' => 'Mark Peterson',
  'Mark Smith' => 'Mark Smith',
  'Marla Emery' => 'Marla Emery',
  'Marlene Heying' => 'Marlene Heying',
  'Matt Cunningham' => 'Matt Cunningham',
  'Megan OBrien' => 'Megan O\'Brien',
  'Melissa Carlson' => 'Melissa Carlson',
  'Michael Frie' => 'Michael Frie',
  'Mindy Jedlicki' => 'Mindy Jedlicki',
  'Muhammad Ashan' => 'Muhammad Ashan',
  'Nicole Son' => 'Nicole Son',
  'Other' => 'Other',
  'Patrick Stocker' => 'Patrick Stocker',
  'Qing Wang' => 'Qing Wang',
  'Rebekka Molenaar' => 'Rebekka Molenaar',
  'Rhonda Albright' => 'Rhonda Swenson',
  'Sarah Bronstad' => 'Sarah Bronstad',
  'Sarah Howard' => 'Sarah Howard',
  'Sarah Kaltenbach' => 'Sarah Kaltenbach',
  'Sarah Schaefers' => 'Sarah Schaefers',
  'Scott Barnhill' => 'Scott Barnhill',
  'Steve Deline' => 'Steve Deline',
  'Taylor Celski' => 'Taylor Celski',
  'Tess Gilbert' => 'Tess Gilbert',
  'Thomas van Valkenburg' => 'Thomas van Valkenburg',
  'Thomas Van Valkenburg' => 'Thomas Van Valkenburg',
  'Tim Schatz' => 'Tim Schatz',
  'Toby Rogers' => 'Toby Rogers',
  'Vanessa Lopes Berkas' => 'Vanessa Lopes-Berkas',
  'Wendy Mathews' => 'Wendy Mathews',
  'Yan Chen' => 'Yan Chen',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_category_list_m06_error.php

$app_list_strings['category_list_m06_error']=array (
  'Choose One' => 'Choose One',
  'Critical Phase Inspection' => 'Critical Phase Inspection',
  'Deceased Animal' => 'Deceased Animal',
  'Maintenance Request' => 'Maintenance Request',
  'Observation' => 'Observation',
  'Rejected Animal' => 'Rejected Animal',
  'Vet Check' => 'Vet Check',
);


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_method_of_qualification_list.php

 // created: 2019-02-26 21:06:07

$app_list_strings['method_of_qualification_list']=array (
  '' => '',
  'APS Management Approval' => 'APS Management Approval',
  'CV' => 'CV',
  'Questionnaire' => 'Questionnaire',
  'Site Visit' => 'Site Visit',
  'TBD' => 'TBD',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_current_vendor_status_list.php

 // created: 2019-02-20 18:15:36

$app_list_strings['current_vendor_status_list']=array (
  '' => '',
  'Approved' => 'Approved',
  'Disapproved' => 'Disapproved',
  'Pending Approval' => 'Pending Approval',
  'Retired' => 'Retired',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_company_status_list.php

 // created: 2019-02-20 17:00:31

$app_list_strings['company_status_list']=array (
  '' => '',
  'Account Current' => 'Current',
  'Hold Data' => 'Hold Data',
  'Study Pre_Payment' => 'Pre-payment',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Equipment_Facility_Document.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['efd_equipment_facility_doc_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['efd_equipment_facility_doc_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['efd_equipment_facility_doc_category_dom']['Sales'] = 'Sales';
$app_list_strings['efd_equipment_facility_doc_category_dom'][''] = '';
$app_list_strings['efd_equipment_facility_doc_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['efd_equipment_facility_doc_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['efd_equipment_facility_doc_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['efd_equipment_facility_doc_subcategory_dom'][''] = '';
$app_list_strings['efd_equipment_facility_doc_status_dom']['Active'] = 'Active';
$app_list_strings['efd_equipment_facility_doc_status_dom']['Draft'] = 'Draft';
$app_list_strings['efd_equipment_facility_doc_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['efd_equipment_facility_doc_status_dom']['Expired'] = 'Expired';
$app_list_strings['efd_equipment_facility_doc_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['efd_equipment_facility_doc_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['EFD_Equipment_Facility_Doc'] = 'Equipment & Facility Documents';
$app_list_strings['moduleListSingular']['EFD_Equipment_Facility_Doc'] = 'Equipment & Facility Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_vendor_status_list.php

 // created: 2019-02-20 13:42:59

$app_list_strings['vendor_status_list']=array (
  '' => '',
  'Approved' => 'Approved',
  'Disapproved' => 'Disapproved',
  'Pending Approval' => 'Pending Approval',
  'Retired' => 'Retired',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_vendor_level_list.php

 // created: 2019-02-20 17:30:35

$app_list_strings['vendor_level_list']=array (
  '' => '',
  '01' => '01',
  '02' => '02',
  '03' => '03',
  'Not Applicable' => 'Not Applicable',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Room_Transfer.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['RT_Room_Transfer'] = 'Room Transfers';
$app_list_strings['moduleListSingular']['RT_Room_Transfer'] = 'Room Transfer';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Room.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['RMS_Room'] = 'Rooms';
$app_list_strings['moduleListSingular']['RMS_Room'] = 'Room';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Equipment_Facility_Service.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['EFS_Equipment_Facility_Servi'] = 'Equipment & Facility Services';
$app_list_strings['moduleListSingular']['EFS_Equipment_Facility_Servi'] = 'Equipment & Facility Service';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Equipment_Facility_Records.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['efr_equipment_facility_recor_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['efr_equipment_facility_recor_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['efr_equipment_facility_recor_category_dom']['Sales'] = 'Sales';
$app_list_strings['efr_equipment_facility_recor_category_dom'][''] = '';
$app_list_strings['efr_equipment_facility_recor_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['efr_equipment_facility_recor_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['efr_equipment_facility_recor_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['efr_equipment_facility_recor_subcategory_dom'][''] = '';
$app_list_strings['efr_equipment_facility_recor_status_dom']['Active'] = 'Active';
$app_list_strings['efr_equipment_facility_recor_status_dom']['Draft'] = 'Draft';
$app_list_strings['efr_equipment_facility_recor_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['efr_equipment_facility_recor_status_dom']['Expired'] = 'Expired';
$app_list_strings['efr_equipment_facility_recor_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['efr_equipment_facility_recor_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['EFR_Equipment_Facility_Recor'] = 'Equipment & Facility Records';
$app_list_strings['moduleListSingular']['EFR_Equipment_Facility_Recor'] = 'Equipment & Facility Record';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_company_type.php

 // created: 2019-02-20 15:20:25

$app_list_strings['company_type']=array (
  'blank' => '',
  'consultant' => 'Consultant',
  'sponsor' => 'Sponsor',
  'Sponsor and Vendor' => 'Sponsor and Vendor',
  'vendor' => 'Vendor',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_reason_for_transfer_list.php

 // created: 2019-02-20 13:15:16

$app_list_strings['reason_for_transfer_list']=array (
  'Consolidation' => 'Consolidation',
  'Sanitation' => 'Sanitation',
  'Peri operative Relocation' => 'Peri-operative Relocation',
  'Isolation' => 'Isolation',
  'Receipt' => 'Receipt',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_usda_category_list.php

 // created: 2019-06-17 12:10:47

$app_list_strings['usda_category_list']=array (
  'B' => 'B (Holding Protocols Only)',
  'C' => 'C',
  'D' => 'D',
  'E' => 'E',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_iacuc.php

 // created: 2019-06-18 18:46:12

$app_list_strings['iacuc']=array (
  'Active' => 'Active',
  'Inactive' => 'Inactive',
  'None' => 'No Review Required',
  'Choose IACUC Status' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_equipment_and_facility_document_type_list.php

 // created: 2019-06-19 15:49:14

$app_list_strings['equipment_and_facility_document_type_list']=array (
  '' => '',
  'Equipment Qualification' => 'Equipment Qualification',
  'Manual' => 'Manual',
  'Part 11 Gap Analysis' => 'Part 11 Gap Analysis',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Company_Address.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['CA_Company_Address'] = 'Company Addresses';
$app_list_strings['moduleListSingular']['CA_Company_Address'] = 'Company Address';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_address_type_list.php

 // created: 2019-07-03 12:07:35

$app_list_strings['address_type_list']=array (
  '' => '',
  'Billing' => 'Billing',
  'General' => 'General',
  'Shipping' => 'Shipping',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Regulatory_Response.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['RR_Regulatory_Response'] = 'Regulatory Responses';
$app_list_strings['moduleListSingular']['RR_Regulatory_Response'] = 'Regulatory Response';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Regulatory_Response_Document.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['rrd_regulatory_response_doc_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['rrd_regulatory_response_doc_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['rrd_regulatory_response_doc_category_dom']['Sales'] = 'Sales';
$app_list_strings['rrd_regulatory_response_doc_category_dom'][''] = '';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom'][''] = '';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Active'] = 'Active';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Draft'] = 'Draft';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Expired'] = 'Expired';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['RRD_Regulatory_Response_Doc'] = 'Regulatory Response Documents';
$app_list_strings['moduleListSingular']['RRD_Regulatory_Response_Doc'] = 'Regulatory Response Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_vendor_type_list.php

 // created: 2019-07-10 12:01:59

$app_list_strings['vendor_type_list']=array (
  '' => '',
  'Equipment' => 'Equipment',
  'Product' => 'Product',
  'Service' => 'Service',
  'Test System' => 'Test System',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_binder_location_list.php

 // created: 2019-07-23 12:14:26

$app_list_strings['binder_location_list']=array (
  '' => '',
  'Not Applicable' => 'Not Applicable',
  'With Equipment' => 'With Equipment',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_response_status_list.php

 // created: 2019-07-30 11:45:19

$app_list_strings['response_status_list']=array (
  '' => '',
  'Complete' => 'Complete',
  'Draft Sent' => 'Draft Sent',
  'In Progress' => 'In Progress',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_anticipated_study_start_timeline_list.php

 // created: 2019-07-31 11:43:13

$app_list_strings['anticipated_study_start_timeline_list']=array (
  '' => '',
  '3 4 weeks' => '3-4 weeks',
  '1 2 months' => '1-2 months',
  '2 3 months' => '2-3 months',
  '3 6 months' => '3-6 months',
  '6 12 months' => '6-12 months',
  '12 months' => '12+ months',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_win_probability_list.php

 // created: 2019-08-01 17:01:07

$app_list_strings['win_probability_list']=array (
  '' => '',
  'zeropercent' => '0%',
  25 => '25%',
  50 => '50%',
  75 => '75%',
  100 => '100%',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.customm03_work_product_activities_1_calls.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.customm03_work_product_activities_1_calls.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$app_list_strings['record_type_display']['M03_Work_Product'] = 'Work Products';
$app_list_strings['parent_type_display']['M03_Work_Product'] = 'Work Products';
$app_list_strings['record_type_display_notes']['M03_Work_Product'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$app_list_strings['record_type_display']['M03_Work_Product'] = 'Work Products';
$app_list_strings['parent_type_display']['M03_Work_Product'] = 'Work Products';
$app_list_strings['record_type_display_notes']['M03_Work_Product'] = 'Work Products';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_USDA_Exemption.php

 // created: 2019-08-08 11:45:52

$app_list_strings['USDA_Exemption']=array (
  'None' => 'None',
  'Choose_One' => '',
  'Conscious_Restraint' => 'Conscious Restraint',
  'Exercise_Restriction' => 'Exercise Restriction (dogs)',
  'Food_Water_Restriction' => 'Food/Water Restriction',
  'Multiple Major_1' => 'Multiple Major Sx (1/protocol)',
  'Neuromuscular_Blocker' => 'Neuromuscular Blocker',
  'Toxicity_Study' => 'Toxicity Study',
  'Training' => 'Personnel Training',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Contact_Documents.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['maj_contact_documents_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['maj_contact_documents_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['maj_contact_documents_category_dom']['Sales'] = 'Sales';
$app_list_strings['maj_contact_documents_category_dom'][''] = '';
$app_list_strings['maj_contact_documents_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['maj_contact_documents_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['maj_contact_documents_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['maj_contact_documents_subcategory_dom'][''] = '';
$app_list_strings['maj_contact_documents_status_dom']['Active'] = 'Active';
$app_list_strings['maj_contact_documents_status_dom']['Draft'] = 'Draft';
$app_list_strings['maj_contact_documents_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['maj_contact_documents_status_dom']['Expired'] = 'Expired';
$app_list_strings['maj_contact_documents_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['maj_contact_documents_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['MAJ_Contact_Documents'] = 'Contact Documents';
$app_list_strings['moduleListSingular']['MAJ_Contact_Documents'] = 'Contact Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_contact_document_list.php

 // created: 2019-08-15 11:31:15

$app_list_strings['contact_document_list']=array (
  '' => '',
  'CV' => 'CV',
  'CV and PRD' => 'CV and PRD',
  'PRD' => 'PRD',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Email_Documents.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['edoc_email_documents_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['edoc_email_documents_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['edoc_email_documents_category_dom']['Sales'] = 'Sales';
$app_list_strings['edoc_email_documents_category_dom'][''] = '';
$app_list_strings['edoc_email_documents_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['edoc_email_documents_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['edoc_email_documents_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['edoc_email_documents_subcategory_dom'][''] = '';
$app_list_strings['edoc_email_documents_status_dom']['Active'] = 'Active';
$app_list_strings['edoc_email_documents_status_dom']['Draft'] = 'Draft';
$app_list_strings['edoc_email_documents_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['edoc_email_documents_status_dom']['Expired'] = 'Expired';
$app_list_strings['edoc_email_documents_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['edoc_email_documents_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['EDoc_Email_Documents'] = 'Email Documents';
$app_list_strings['moduleListSingular']['EDoc_Email_Documents'] = 'Email Document';
$app_list_strings['department_list']['Analytical Services'] = 'Analytical';
$app_list_strings['department_list']['Business Development'] = 'Business Development';
$app_list_strings['department_list']['Facilities'] = 'Facilities';
$app_list_strings['department_list']['Finance'] = 'Finance';
$app_list_strings['department_list']['Histology Services'] = 'Histology Services';
$app_list_strings['department_list']['Human_Resources'] = 'Human Resources';
$app_list_strings['department_list']['In life Large Animal Care'] = 'In-life Large Animal Care';
$app_list_strings['department_list']['In life Large Animal Research'] = 'In-life Large Animal Research';
$app_list_strings['department_list']['In life Small Animal Care'] = 'In-life Small Animal Care';
$app_list_strings['department_list']['In life Small Animal Research'] = 'In-life Small Animal Research';
$app_list_strings['department_list']['Information_Technology'] = 'Information Technology';
$app_list_strings['department_list']['Interventional Surgical Research'] = 'Interventional Surgical Research';
$app_list_strings['department_list']['Lab Services'] = 'Lab Services';
$app_list_strings['department_list']['Operations Support'] = 'Operations Support';
$app_list_strings['department_list']['Pathology Services'] = 'Pathology Services';
$app_list_strings['department_list']['Pharmacology'] = 'Pharmacology Services';
$app_list_strings['department_list']['Process Improvement'] = 'Process Improvement';
$app_list_strings['department_list']['Quality Assurance Unit'] = 'Quality Assurance Unit';
$app_list_strings['department_list']['Regulatory Services'] = 'Regulatory Services';
$app_list_strings['department_list']['Scientific'] = 'Scientific';
$app_list_strings['department_list']['Software Development'] = 'Software Development';
$app_list_strings['department_list']['Toxicology'] = 'Toxicology Services';
$app_list_strings['department_list']['Veterinary Services'] = 'Veterinary Services';
$app_list_strings['department_list'][''] = '';
$app_list_strings['ed_type_list']['Animal Enrollment'] = 'Animal Enrollment';
$app_list_strings['ed_type_list']['Animal Health Status to SD'] = 'Animal Health Status to SD';
$app_list_strings['ed_type_list']['Early Term Early Death'] = 'Early Term/Early Death';
$app_list_strings['ed_type_list']['Test Article Failures'] = 'Test Article Failures';
$app_list_strings['ed_type_list']['Unblinding of Study Personnel'] = 'Unblinding of Study Personnel';
$app_list_strings['ed_type_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_equipment_and_facilities_status_list.php

 // created: 2019-09-16 12:15:31

$app_list_strings['equipment_and_facilities_status_list']=array (
  '' => '',
  'Active' => 'Active',
  'Inactive' => 'Inactive',
  'Out for Service' => 'Out for Service',
  'Retired' => 'Retired',
  'Returned to Sponsor' => 'Returned to Sponsor',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Email_Template.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['et_email_template_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['et_email_template_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['et_email_template_category_dom']['Sales'] = 'Sales';
$app_list_strings['et_email_template_category_dom'][''] = '';
$app_list_strings['et_email_template_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['et_email_template_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['et_email_template_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['et_email_template_subcategory_dom'][''] = '';
$app_list_strings['et_email_template_status_dom']['Active'] = 'Active';
$app_list_strings['et_email_template_status_dom']['Draft'] = 'Draft';
$app_list_strings['et_email_template_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['et_email_template_status_dom']['Expired'] = 'Expired';
$app_list_strings['et_email_template_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['et_email_template_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['ET_Email_Template'] = 'Email Templates';
$app_list_strings['moduleListSingular']['ET_Email_Template'] = 'Email Template';
$app_list_strings['department_list']['Analytical Services'] = 'Analytical';
$app_list_strings['department_list']['Business Development'] = 'Business Development';
$app_list_strings['department_list']['Facilities'] = 'Facilities';
$app_list_strings['department_list']['Finance'] = 'Finance';
$app_list_strings['department_list']['Histology Services'] = 'Histology Services';
$app_list_strings['department_list']['Human_Resources'] = 'Human Resources';
$app_list_strings['department_list']['In life Large Animal Care'] = 'In-life Large Animal Care';
$app_list_strings['department_list']['In life Large Animal Research'] = 'In-life Large Animal Research';
$app_list_strings['department_list']['In life Small Animal Care'] = 'In-life Small Animal Care';
$app_list_strings['department_list']['In life Small Animal Research'] = 'In-life Small Animal Research';
$app_list_strings['department_list']['Information_Technology'] = 'Information Technology';
$app_list_strings['department_list']['Interventional Surgical Research'] = 'Interventional Surgical Research';
$app_list_strings['department_list']['Lab Services'] = 'Lab Services';
$app_list_strings['department_list']['Operations Support'] = 'Operations Support';
$app_list_strings['department_list']['Pathology Services'] = 'Pathology Services';
$app_list_strings['department_list']['Pharmacology'] = 'Pharmacology Services';
$app_list_strings['department_list']['Process Improvement'] = 'Process Improvement';
$app_list_strings['department_list']['Quality Assurance Unit'] = 'Quality Assurance Unit';
$app_list_strings['department_list']['Regulatory Services'] = 'Regulatory Services';
$app_list_strings['department_list']['Scientific'] = 'Scientific';
$app_list_strings['department_list']['Software Development'] = 'Software Development';
$app_list_strings['department_list']['Toxicology'] = 'Toxicology Services';
$app_list_strings['department_list']['Veterinary Services'] = 'Veterinary Services';
$app_list_strings['department_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Company_Name.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['CN_Company_Name'] = 'Company Names';
$app_list_strings['moduleListSingular']['CN_Company_Name'] = 'Company Name';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Test_System_Documents.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['tsdoc_test_system_documents_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['tsdoc_test_system_documents_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['tsdoc_test_system_documents_category_dom']['Sales'] = 'Sales';
$app_list_strings['tsdoc_test_system_documents_category_dom'][''] = '';
$app_list_strings['tsdoc_test_system_documents_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['tsdoc_test_system_documents_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['tsdoc_test_system_documents_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['tsdoc_test_system_documents_subcategory_dom'][''] = '';
$app_list_strings['tsdoc_test_system_documents_status_dom']['Active'] = 'Active';
$app_list_strings['tsdoc_test_system_documents_status_dom']['Draft'] = 'Draft';
$app_list_strings['tsdoc_test_system_documents_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['tsdoc_test_system_documents_status_dom']['Expired'] = 'Expired';
$app_list_strings['tsdoc_test_system_documents_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['tsdoc_test_system_documents_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['TSdoc_Test_System_Documents'] = 'Test System Documents';
$app_list_strings['moduleListSingular']['TSdoc_Test_System_Documents'] = 'Test System Document';
$app_list_strings['test_system_document_category_list']['tbd'] = 'tbd';
$app_list_strings['test_system_document_category_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_scientific_activity_list.php

 // created: 2019-10-03 12:07:55

$app_list_strings['scientific_activity_list']=array (
  '' => '',
  'Conference Attendance' => 'Conference Attendance',
  'Data Entry_Analysis' => 'Data Entry & Analysis',
  'Out of Office' => 'Out of Office',
  'Procedure_Assignment' => 'Procedure Assignment',
  'Protocol Development' => 'Protocol Development',
  'Reporting' => 'Reporting',
  'Operations Oversight' => 'Study Management',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_aaalac_exemptions_list.php

 // created: 2019-11-06 14:10:33

$app_list_strings['aaalac_exemptions_list']=array (
  'None' => 'None',
  'Survival_Surgery' => 'Survival Surgery',
  'Multiple_Survival_Surgery' => 'Multiple Survival Surgeries',
  'Hazardous_Substances' => 'Hazardous Agents',
  'Choose_One' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_record_type_display_notes.php

// created: 2019-11-20 12:28:02
$app_list_strings['record_type_display_notes']['Meetings'] = 'Communication (Sugar)';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Units.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['U_Units'] = 'Units';
$app_list_strings['moduleListSingular']['U_Units'] = 'Unit';
$app_list_strings['unit_type_list']['Area'] = 'Area';
$app_list_strings['unit_type_list']['Length'] = 'Length';
$app_list_strings['unit_type_list']['Percent'] = 'Percent';
$app_list_strings['unit_type_list']['Time'] = 'Time';
$app_list_strings['unit_type_list']['Volume'] = 'Volume';
$app_list_strings['unit_type_list']['Volume per Time'] = 'Volume per Time';
$app_list_strings['unit_type_list']['Volume per Weight'] = 'Volume per Weight';
$app_list_strings['unit_type_list']['Volume per Weight per Time'] = 'Volume per Weight per Time';
$app_list_strings['unit_type_list']['Weight'] = 'Weight';
$app_list_strings['unit_type_list']['Weight per Time'] = 'Weight per Time';
$app_list_strings['unit_type_list']['Weight per Volume'] = 'Weight per Volume';
$app_list_strings['unit_type_list']['Weight per Volume per Time'] = 'Weight per Volume per Time';
$app_list_strings['unit_type_list']['Weight per Weight'] = 'Weight per Weight';
$app_list_strings['unit_type_list']['Weight per Weight per Time'] = 'Weight per Weight per Time';
$app_list_strings['unit_type_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Species.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['S_Species'] = 'Species';
$app_list_strings['moduleListSingular']['S_Species'] = 'Species';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_corrective_action_options_list.php

 // created: 2019-12-06 12:55:01

$app_list_strings['corrective_action_options_list']=array (
  '' => '',
  'SOP Update' => 'APS Controlled Document Revision',
  'Further Investigation of Event' => 'Further Investigation of Event',
  'None' => 'None',
  'Other' => 'Other',
  'Protocol Amendment' => 'Protocol Amendment',
  'Protocol Requirement Review' => 'Protocol Requirement Review',
  'Report Amendment' => 'Report Amendment',
  'Protocol Specific Training' => 'Task Retraining',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_test_system_document_category_list.php

 // created: 2019-12-11 12:49:41

$app_list_strings['test_system_document_category_list']=array (
  '' => '',
  'VDL Diagnostic Result' => 'VDL Diagnostic Result',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Weight.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['W_Weight'] = 'Weights';
$app_list_strings['moduleListSingular']['W_Weight'] = 'Weight';
$app_list_strings['yes_no_list']['No'] = 'No';
$app_list_strings['yes_no_list']['Yes'] = 'Yes';
$app_list_strings['yes_no_list'][''] = '';
$app_list_strings['reason_for_weight_list']['Receipt'] = 'Receipt';
$app_list_strings['reason_for_weight_list']['Protocol Assignment'] = 'Protocol Assignment';
$app_list_strings['reason_for_weight_list']['Substance Administration'] = 'Substance Administration';
$app_list_strings['reason_for_weight_list']['Monthly Monitoring'] = 'Monthly Monitoring';
$app_list_strings['reason_for_weight_list']['Veterinarian Order'] = 'Veterinarian Order';
$app_list_strings['reason_for_weight_list']['Housing Assessment'] = 'Housing Assessment';
$app_list_strings['reason_for_weight_list']['Feed Assessment'] = 'Feed Assessment';
$app_list_strings['reason_for_weight_list']['Ad Hoc'] = 'Ad Hoc';
$app_list_strings['reason_for_weight_list'][''] = '';
$app_list_strings['no_change_change_list']['Changed'] = 'Changed';
$app_list_strings['no_change_change_list']['No Change Required'] = 'No Change Required';
$app_list_strings['no_change_change_list'][''] = '';
$app_list_strings['yes_list']['Yes'] = 'Yes';
$app_list_strings['yes_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Clinical_Observation.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['CO_Clinical_Observation'] = 'Clinical Observations';
$app_list_strings['moduleListSingular']['CO_Clinical_Observation'] = 'Clinical Observation';
$app_list_strings['yes_no_list']['No'] = 'No';
$app_list_strings['yes_no_list']['Yes'] = 'Yes';
$app_list_strings['yes_no_list'][''] = '';
$app_list_strings['observation_list']['Normal'] = 'Normal';
$app_list_strings['observation_list']['See Observation Text'] = 'See Observation Text';
$app_list_strings['observation_list'][''] = '';
$app_list_strings['housing_list']['Group'] = 'Group';
$app_list_strings['housing_list']['Single'] = 'Single';
$app_list_strings['housing_list'][''] = '';
$app_list_strings['Protocol_Ad_Hoc_Type_list']['Ad Hoc'] = 'Ad Hoc';
$app_list_strings['Protocol_Ad_Hoc_Type_list']['Protocol'] = 'Protocol';
$app_list_strings['Protocol_Ad_Hoc_Type_list'][''] = '';
$app_list_strings['taken_not_taken_list']['Not Taken'] = 'Not Taken';
$app_list_strings['taken_not_taken_list']['Taken'] = 'Taken';
$app_list_strings['taken_not_taken_list'][''] = '';
$app_list_strings['yes_list']['Yes'] = 'Yes';
$app_list_strings['yes_list'][''] = '';
$app_list_strings['clin_obs_type_list']['Ad Hoc'] = 'Ad Hoc';
$app_list_strings['clin_obs_type_list']['Morbidity and Mortality'] = 'Morbidity and Mortality';
$app_list_strings['clin_obs_type_list']['Pain Assessment'] = 'Pain Assessment';
$app_list_strings['clin_obs_type_list']['Post Operative AM'] = 'Post Operative Day AM';
$app_list_strings['clin_obs_type_list']['Post Operative Day 1 AM'] = 'Post Operative Day 1 AM';
$app_list_strings['clin_obs_type_list']['Post Operative PM'] = 'Post Operative PM';
$app_list_strings['clin_obs_type_list']['Standard'] = 'Standard';
$app_list_strings['clin_obs_type_list'][''] = '';
$app_list_strings['observation_selection_list']['Cough'] = 'Cough';
$app_list_strings['observation_selection_list']['Diarrhea'] = 'Diarrhea';
$app_list_strings['observation_selection_list']['Inappetence'] = 'Inappetence';
$app_list_strings['observation_selection_list']['Lameness'] = 'Lameness';
$app_list_strings['observation_selection_list']['LesionWound'] = 'Lesion/Wound';
$app_list_strings['observation_selection_list']['Mentation'] = 'Mentation';
$app_list_strings['observation_selection_list']['Nasal Discharge'] = 'Nasal Discharge';
$app_list_strings['observation_selection_list'][''] = '';
$app_list_strings['post_operative_pain_level_list'][1] = '1';
$app_list_strings['post_operative_pain_level_list'][2] = '2';
$app_list_strings['post_operative_pain_level_list'][3] = '3';
$app_list_strings['post_operative_pain_level_list'][''] = '';
$app_list_strings['post_operative_pain_level_list']['zero'] = '0';
$app_list_strings['pain_level_observation_list']['Droopy Ears'] = 'Droopy Ears';
$app_list_strings['pain_level_observation_list']['Hunched'] = 'Hunched';
$app_list_strings['pain_level_observation_list']['Inappetence'] = 'Inappetence';
$app_list_strings['pain_level_observation_list']['Mentation'] = 'Mentation';
$app_list_strings['pain_level_observation_list']['Teeth Grinding'] = 'Teeth Grinding';
$app_list_strings['pain_level_observation_list']['Vocalization'] = 'Vocalization';
$app_list_strings['pain_level_observation_list'][''] = '';
$app_list_strings['co_appetite_list']['Anorexic'] = 'Anorexic (all feed remaining)';
$app_list_strings['co_appetite_list']['Mild Inappetent'] = 'Mild Inappetent (~1/4-1/2 feed remaining)';
$app_list_strings['co_appetite_list']['Moderate Inappetent'] = 'Moderate Inappetent (~1/2-3/4 feed remaining)';
$app_list_strings['co_appetite_list']['Normal'] = 'Normal';
$app_list_strings['co_appetite_list']['Not Assessed'] = 'Not Assessed (Group Housed)';
$app_list_strings['co_appetite_list'][''] = '';
$app_list_strings['fecal_assessment_list']['Absent'] = 'Absent';
$app_list_strings['fecal_assessment_list']['Diarrhea'] = 'Diarrhea';
$app_list_strings['fecal_assessment_list']['Normal'] = 'Normal';
$app_list_strings['fecal_assessment_list']['Not Assessed'] = 'Not Assessed (Group Housed)';
$app_list_strings['fecal_assessment_list']['Scant'] = 'Scant';
$app_list_strings['fecal_assessment_list']['Soft'] = 'Soft';
$app_list_strings['fecal_assessment_list'][''] = '';
$app_list_strings['mentation_list']['BAR'] = 'BAR';
$app_list_strings['mentation_list']['LethargicDepressed'] = 'Lethargic/Depressed';
$app_list_strings['mentation_list']['QAR'] = 'QAR';
$app_list_strings['mentation_list']['Unresponsive'] = 'Unresponsive';
$app_list_strings['mentation_list'][''] = '';
$app_list_strings['normal_abnormal_list']['Normal'] = 'Normal';
$app_list_strings['normal_abnormal_list']['Abnormal'] = 'Abnormal';
$app_list_strings['normal_abnormal_list'][''] = '';
$app_list_strings['lameness_location_list']['LH'] = 'LH';
$app_list_strings['lameness_location_list']['LF'] = 'LF';
$app_list_strings['lameness_location_list']['RH'] = 'RH';
$app_list_strings['lameness_location_list']['RF'] = 'RF';
$app_list_strings['lameness_location_list']['Bilateral hindlimb'] = 'Bilateral hindlimb';
$app_list_strings['lameness_location_list']['Bilateral forelimb'] = 'Bilateral forelimb';
$app_list_strings['lameness_location_list'][''] = '';
$app_list_strings['co_lameness_severity_list']['Mild'] = 'Mild (toe tapping)';
$app_list_strings['co_lameness_severity_list']['Moderate'] = 'Moderate (limping/raised at standing)';
$app_list_strings['co_lameness_severity_list']['Severe'] = 'Severe (non weight bearing)';
$app_list_strings['co_lameness_severity_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Clinical_Issue_Exam.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['CIE_Clinical_Issue_Exam'] = 'Clinical Issues & Exams';
$app_list_strings['moduleListSingular']['CIE_Clinical_Issue_Exam'] = 'Clinical Issue & Exam';
$app_list_strings['examiner_type_list']['Technicain'] = 'Technician';
$app_list_strings['examiner_type_list']['Veterinarian'] = 'Veterinarian';
$app_list_strings['examiner_type_list'][''] = '';
$app_list_strings['cie_category_list']['Physical Exam'] = 'Physical Exam';
$app_list_strings['cie_category_list']['Vet Check Clin Ob Related'] = 'Vet Check (Clin Ob Related)';
$app_list_strings['cie_category_list']['Vet Check Not Clin Ob Related'] = 'Vet Check (Not Clin Ob Related)';
$app_list_strings['cie_category_list'][''] = '';
$app_list_strings['cie_type_list']['Follow Up'] = 'Follow Up';
$app_list_strings['cie_type_list']['Initial'] = 'Initial';
$app_list_strings['cie_type_list']['Resolution'] = 'Resolution';
$app_list_strings['cie_type_list'][''] = '';
$app_list_strings['required_not_required']['Required'] = 'Required';
$app_list_strings['required_not_required']['Not Required'] = 'Not Required';
$app_list_strings['required_not_required'][''] = '';
$app_list_strings['follow_up_by_list']['By Technician'] = 'By Technician';
$app_list_strings['follow_up_by_list']['By Veterinarian'] = 'By Veterinarian';
$app_list_strings['follow_up_by_list']['By Technician or Veterinarian'] = 'By Technician or Veterinarian';
$app_list_strings['follow_up_by_list'][''] = '';
$app_list_strings['resolution_allowed_by_list']['Technician'] = 'Technician';
$app_list_strings['resolution_allowed_by_list']['Veterinarian'] = 'Veterinarian';
$app_list_strings['resolution_allowed_by_list']['Technician or Veterinarian'] = 'Technician or Veterinarian';
$app_list_strings['resolution_allowed_by_list'][''] = '';
$app_list_strings['allnormal_allnotexamined_completesection_list']['All Normal'] = 'All Normal';
$app_list_strings['allnormal_allnotexamined_completesection_list']['All Not Examined'] = 'All Not Examined';
$app_list_strings['allnormal_allnotexamined_completesection_list']['Complete Section'] = 'Complete Section';
$app_list_strings['allnormal_allnotexamined_completesection_list'][''] = '';
$app_list_strings['mucous_membranes_color_list']['Cyanotic'] = 'Cyanotic';
$app_list_strings['mucous_membranes_color_list']['Injected'] = 'Injected';
$app_list_strings['mucous_membranes_color_list']['Not Examined'] = 'Not Examined';
$app_list_strings['mucous_membranes_color_list']['Pale'] = 'Pale';
$app_list_strings['mucous_membranes_color_list']['Pink'] = 'Pink';
$app_list_strings['mucous_membranes_color_list'][''] = '';
$app_list_strings['mucous_membranes_tactile_list']['Dry'] = 'Dry';
$app_list_strings['mucous_membranes_tactile_list']['Moist'] = 'Moist';
$app_list_strings['mucous_membranes_tactile_list']['Not Assessed'] = 'Not Examined';
$app_list_strings['mucous_membranes_tactile_list']['Tacky'] = 'Tacky';
$app_list_strings['mucous_membranes_tactile_list'][''] = '';
$app_list_strings['crt_sec_list']['less _than_2'] = 'Less than 2';
$app_list_strings['crt_sec_list']['one_to_two'] = '1-2';
$app_list_strings['crt_sec_list']['greater_than_2'] = 'Greater than 2';
$app_list_strings['crt_sec_list']['Not Assessed'] = 'Not Examined';
$app_list_strings['crt_sec_list'][''] = '';
$app_list_strings['bcs_list'][1] = '1';
$app_list_strings['bcs_list']['1 5'] = '1.5';
$app_list_strings['bcs_list'][2] = '2';
$app_list_strings['bcs_list']['2 5'] = '2.5';
$app_list_strings['bcs_list'][3] = '3';
$app_list_strings['bcs_list']['3 5'] = '3.5';
$app_list_strings['bcs_list'][4] = '4';
$app_list_strings['bcs_list']['4 5'] = '4.5';
$app_list_strings['bcs_list'][5] = '5';
$app_list_strings['bcs_list']['Not Assessed'] = 'Not Examined';
$app_list_strings['bcs_list'][''] = '';
$app_list_strings['hydration_list']['Adequate'] = 'Adequate';
$app_list_strings['hydration_list']['Inadequate'] = 'Inadequate';
$app_list_strings['hydration_list']['Marginal'] = 'Marginal';
$app_list_strings['hydration_list']['Not Examined'] = 'Not Examined';
$app_list_strings['hydration_list'][''] = '';
$app_list_strings['cie_mentation_list']['BAR'] = 'BAR';
$app_list_strings['cie_mentation_list']['LethargicDepressed'] = 'Lethargic/Depressed';
$app_list_strings['cie_mentation_list']['Not Assessed'] = 'Not Examined';
$app_list_strings['cie_mentation_list']['QAR'] = 'QAR';
$app_list_strings['cie_mentation_list']['Unresponsive'] = 'Unresponsive';
$app_list_strings['cie_mentation_list'][''] = '';
$app_list_strings['cie_appetite_list']['Anorexic'] = 'Anorexic (all feed remaining)';
$app_list_strings['cie_appetite_list']['Group Housed'] = 'Group Housed';
$app_list_strings['cie_appetite_list']['Mild Inappetent'] = 'Mild Inappetent (~1/4-1/2 feed remaining';
$app_list_strings['cie_appetite_list']['Moderate Inappetent'] = 'Moderate Inappetent (~1/2-3/4 feed remaining)';
$app_list_strings['cie_appetite_list']['Normal'] = 'Normal';
$app_list_strings['cie_appetite_list']['Not Examined'] = 'Not Examined';
$app_list_strings['cie_appetite_list'][''] = '';
$app_list_strings['duration_of_inappetence_list']['1st Occurrence'] = '1st Occurrence';
$app_list_strings['duration_of_inappetence_list']['greater than 1'] = '>1 day';
$app_list_strings['duration_of_inappetence_list']['greater than 2'] = '>2 days';
$app_list_strings['duration_of_inappetence_list']['greater than 3'] = '>3 days';
$app_list_strings['duration_of_inappetence_list']['greater than 4'] = '>4 days';
$app_list_strings['duration_of_inappetence_list']['greater than 5'] = '>5 days';
$app_list_strings['duration_of_inappetence_list']['greater than 6'] = '>6 days';
$app_list_strings['duration_of_inappetence_list']['greater than 7'] = '>7 days';
$app_list_strings['duration_of_inappetence_list']['Not Applicable'] = 'Not Applicable';
$app_list_strings['duration_of_inappetence_list']['Not Examined'] = 'Not Examined';
$app_list_strings['duration_of_inappetence_list'][''] = '';
$app_list_strings['cie_feces_list']['Absent'] = 'Absent';
$app_list_strings['cie_feces_list']['Diarrhea'] = 'Diarrhea';
$app_list_strings['cie_feces_list']['Group Housed'] = 'Group Housed';
$app_list_strings['cie_feces_list']['Normal'] = 'Normal';
$app_list_strings['cie_feces_list']['Not Examined'] = 'Not Examined';
$app_list_strings['cie_feces_list']['Scant'] = 'Scant';
$app_list_strings['cie_feces_list']['Soft'] = 'Soft';
$app_list_strings['cie_feces_list'][''] = '';
$app_list_strings['oral_cavity_list']['Normal'] = 'Normal';
$app_list_strings['oral_cavity_list']['Not Examined'] = 'Not Examined';
$app_list_strings['oral_cavity_list']['See Additional Subjective Observations'] = 'See Subjective Notes';
$app_list_strings['oral_cavity_list'][''] = '';
$app_list_strings['no_ne_sas_list']['No'] = 'No';
$app_list_strings['no_ne_sas_list']['Not Examined'] = 'Not Examined';
$app_list_strings['no_ne_sas_list']['See Additional Subjective'] = 'See Subjective Notes';
$app_list_strings['no_ne_sas_list'][''] = '';
$app_list_strings['normal_abnormal_ne_list']['Abnormal'] = 'Abnormal';
$app_list_strings['normal_abnormal_ne_list']['Normal'] = 'Normal';
$app_list_strings['normal_abnormal_ne_list']['Not Examined'] = 'Not Examined';
$app_list_strings['normal_abnormal_ne_list'][''] = '';
$app_list_strings['cie_lameness_location_list']['Bilateral Forelimb'] = 'Bilateral Forelimb';
$app_list_strings['cie_lameness_location_list']['Bilateral Hindlimb'] = 'Bilateral Hindlimb';
$app_list_strings['cie_lameness_location_list']['LF'] = 'LF';
$app_list_strings['cie_lameness_location_list']['LH'] = 'LH';
$app_list_strings['cie_lameness_location_list']['Not Applicable'] = 'Not Applicable';
$app_list_strings['cie_lameness_location_list']['RF'] = 'RF';
$app_list_strings['cie_lameness_location_list']['RH'] = 'RH';
$app_list_strings['cie_lameness_location_list'][''] = '';
$app_list_strings['cie_lameness_severity_list']['Mild'] = 'Mild (toe tapping)';
$app_list_strings['cie_lameness_severity_list']['Moderate'] = 'Moderate (limping/raised at standing)';
$app_list_strings['cie_lameness_severity_list']['Not Applicable'] = 'Not Applicable';
$app_list_strings['cie_lameness_severity_list']['Severe'] = 'Severe (non weight bearing)';
$app_list_strings['cie_lameness_severity_list'][''] = '';
$app_list_strings['musculoskeletal_palpation_list']['Heat'] = 'Heat';
$app_list_strings['musculoskeletal_palpation_list']['Heat and Pain'] = 'Heat and Pain';
$app_list_strings['musculoskeletal_palpation_list']['Normal'] = 'Normal';
$app_list_strings['musculoskeletal_palpation_list']['Not Examined'] = 'Not Examined';
$app_list_strings['musculoskeletal_palpation_list']['Pain'] = 'Pain';
$app_list_strings['musculoskeletal_palpation_list']['Swelling'] = 'Swelling';
$app_list_strings['musculoskeletal_palpation_list']['Swelling and Heat'] = 'Swelling and Heat';
$app_list_strings['musculoskeletal_palpation_list']['Swelling and Pain'] = 'Swelling and Pain';
$app_list_strings['musculoskeletal_palpation_list']['Swelling Heat Pain'] = 'Swelling, Heat, and Pain';
$app_list_strings['musculoskeletal_palpation_list'][''] = '';
$app_list_strings['ACT_plan_list']['Maintain Protocol ACT interval'] = 'Maintain Protocol ACT interval';
$app_list_strings['ACT_plan_list']['Other'] = 'Other';
$app_list_strings['ACT_plan_list']['Recheck ACT early due to high value concern'] = 'Recheck ACT early due to high value concern';
$app_list_strings['ACT_plan_list']['Recheck ACT early due to low value concern'] = 'Recheck ACT early due to low value concern';
$app_list_strings['ACT_plan_list']['Recheck ACT immediately due to adherent response'] = 'Recheck ACT immediately due to adherent response';
$app_list_strings['ACT_plan_list']['Heparin administered to raise ACT level'] = 'Heparin administered to raise ACT level';
$app_list_strings['ACT_plan_list']['Recheck ACT early due to highlow value concern'] = 'Recheck ACT early due to high/low value concern';
$app_list_strings['ACT_plan_list']['Recheck ACT immediately due to cartridge failure'] = 'Recheck ACT immediately due to cartridge failure';
$app_list_strings['ACT_plan_list'][''] = '';
$app_list_strings['heat_severity_list']['Not Applicable'] = 'Not Applicable';
$app_list_strings['heat_severity_list']['Mild'] = 'Mild';
$app_list_strings['heat_severity_list']['Moderate'] = 'Moderate';
$app_list_strings['heat_severity_list']['Severe'] = 'Severe';
$app_list_strings['heat_severity_list'][''] = '';
$app_list_strings['yes_no_ne_list']['No'] = 'No';
$app_list_strings['yes_no_ne_list']['Not Examined'] = 'Not Examined';
$app_list_strings['yes_no_ne_list']['Yes'] = 'Yes';
$app_list_strings['yes_no_ne_list'][''] = '';
$app_list_strings['yes_none_list']['Yes'] = 'Yes';
$app_list_strings['yes_none_list']['None'] = 'None';
$app_list_strings['yes_none_list'][''] = '';
$app_list_strings['lesionswound_list']['No'] = 'No';
$app_list_strings['lesionswound_list']['Not Examined'] = 'Not Examined';
$app_list_strings['lesionswound_list']['See Location and Additional Subjective'] = 'See Location and/or Integument Notes';
$app_list_strings['lesionswound_list'][''] = '';
$app_list_strings['taken_not_taken_list']['Not Taken'] = 'Not Taken';
$app_list_strings['taken_not_taken_list']['Taken'] = 'Taken';
$app_list_strings['taken_not_taken_list'][''] = '';
$app_list_strings['cie_related_to_list']['Likely Herd Health'] = 'Likely Herd Health';
$app_list_strings['cie_related_to_list']['Likely Study'] = 'Likely Study';
$app_list_strings['cie_related_to_list']['Undetermined'] = 'Undetermined';
$app_list_strings['cie_related_to_list'][''] = '';
$app_list_strings['organ_system_list']['Cardiovascular'] = 'Cardiovascular';
$app_list_strings['organ_system_list']['GI'] = 'Gastrointestinal';
$app_list_strings['organ_system_list']['Integument'] = 'Integument';
$app_list_strings['organ_system_list']['Lymphatic'] = 'Lymphatic';
$app_list_strings['organ_system_list']['Musculoskeletal'] = 'Musculoskeletal';
$app_list_strings['organ_system_list']['Nervous System'] = 'Nervous System';
$app_list_strings['organ_system_list']['Ophthalmic'] = 'Ophthalmic';
$app_list_strings['organ_system_list']['Respiratory'] = 'Respiratory';
$app_list_strings['organ_system_list']['Systemic'] = 'Systemic';
$app_list_strings['organ_system_list']['Urogenital'] = 'Urogenital';
$app_list_strings['organ_system_list'][''] = '';
$app_list_strings['clinical_condition_list']['Heart Murmur'] = 'Heart Murmur';
$app_list_strings['clinical_condition_list']['Other'] = 'Other';
$app_list_strings['clinical_condition_list']['Diarrhea'] = 'Diarrhea';
$app_list_strings['clinical_condition_list']['Dental Disease'] = 'Dental Disease';
$app_list_strings['clinical_condition_list']['Vomiting'] = 'Vomiting';
$app_list_strings['clinical_condition_list']['Bloat'] = 'Bloat';
$app_list_strings['clinical_condition_list']['Melena'] = 'Melena';
$app_list_strings['clinical_condition_list']['Internal Parasites'] = 'Internal Parasites';
$app_list_strings['clinical_condition_list']['Ectoparasite'] = 'Ectoparasite';
$app_list_strings['clinical_condition_list']['Rash'] = 'Rash';
$app_list_strings['clinical_condition_list']['Blisters'] = 'Blisters';
$app_list_strings['clinical_condition_list']['Swelling'] = 'Swelling';
$app_list_strings['clinical_condition_list']['Mass'] = 'Mass';
$app_list_strings['clinical_condition_list']['Wound'] = 'Wound';
$app_list_strings['clinical_condition_list']['Erythemia'] = 'Erythemia';
$app_list_strings['clinical_condition_list']['Pruritis'] = 'Pruritis';
$app_list_strings['clinical_condition_list']['Alopecia'] = 'Alopecia';
$app_list_strings['clinical_condition_list']['Conformation'] = 'Conformation';
$app_list_strings['clinical_condition_list']['Ataxia'] = 'Ataxia';
$app_list_strings['clinical_condition_list']['Head Tilt'] = 'Head Tilt';
$app_list_strings['clinical_condition_list']['Seizure'] = 'Seizure';
$app_list_strings['clinical_condition_list']['Corneal Opacity'] = 'Corneal Opacity';
$app_list_strings['clinical_condition_list']['Hyphema'] = 'Hyphema';
$app_list_strings['clinical_condition_list']['Study related'] = 'Study related';
$app_list_strings['clinical_condition_list']['Blepharospasm'] = 'Blepharospasm';
$app_list_strings['clinical_condition_list']['Cough'] = 'Cough';
$app_list_strings['clinical_condition_list']['Nasal Discharge'] = 'Nasal Discharge';
$app_list_strings['clinical_condition_list']['Respiratory distress'] = 'Respiratory distress';
$app_list_strings['clinical_condition_list']['Respiratory Stridor'] = 'Respiratory Stridor';
$app_list_strings['clinical_condition_list']['Lung lesions'] = 'Lung lesions';
$app_list_strings['clinical_condition_list']['Pyrexic'] = 'Pyrexic';
$app_list_strings['clinical_condition_list']['Obesity'] = 'Obesity';
$app_list_strings['clinical_condition_list']['Weight Loss'] = 'Weight Loss';
$app_list_strings['clinical_condition_list']['Malaise'] = 'Malaise';
$app_list_strings['clinical_condition_list']['Dehiscene'] = 'Dehiscene';
$app_list_strings['clinical_condition_list']['Recumbency'] = 'Recumbency';
$app_list_strings['clinical_condition_list']['Lameness'] = 'Lameness';
$app_list_strings['clinical_condition_list']['Hematuria'] = 'Hematuria';
$app_list_strings['clinical_condition_list']['Paraphimosis'] = 'Paraphimosis';
$app_list_strings['clinical_condition_list']['Straining'] = 'Straining';
$app_list_strings['clinical_condition_list']['Vulvar Discharge'] = 'Vulvar Discharge';
$app_list_strings['clinical_condition_list']['Vulvar Swelling'] = 'Vulvar Swelling';
$app_list_strings['clinical_condition_list'][''] = '';
$app_list_strings['cie_plan_location_list']['Medical Treatment Form'] = 'Medical Treatment Form';
$app_list_strings['cie_plan_location_list']['Plan Dropdown'] = 'Plan Dropdown';
$app_list_strings['cie_plan_location_list']['Plan Dropdown and Plan Narrative'] = 'Plan Dropdown and Plan Notes';
$app_list_strings['cie_plan_location_list']['Plan Narrative'] = 'Plan Notes';
$app_list_strings['cie_plan_location_list'][''] = '';
$app_list_strings['cie_plan_dropdown_list']['Submit blood sample to clin path for CBC'] = 'Submit blood sample to clin path for CBC';
$app_list_strings['cie_plan_dropdown_list']['Submit blood sample to clin path for CBCserum chemistry'] = 'Submit blood sample to clin path for CBC/serum chemistry';
$app_list_strings['cie_plan_dropdown_list']['Submit blood sample to clin path for serum chemistry'] = 'Submit blood sample to clin path for serum chemistry';
$app_list_strings['cie_plan_dropdown_list']['Submit culture sample to VDL'] = 'Submit culture sample to VDL';
$app_list_strings['cie_plan_dropdown_list']['Submit fecal sample to VDL'] = 'Submit fecal sample to VDL';
$app_list_strings['cie_plan_dropdown_list']['Submit fresh tissue sample to VDL'] = 'Submit fresh tissue sample to VDL';
$app_list_strings['cie_plan_dropdown_list'][''] = '';
$app_list_strings['cie_diagnostic_result_list']['Abscess'] = 'Abscess';
$app_list_strings['cie_diagnostic_result_list']['Barbaring'] = 'Barbaring';
$app_list_strings['cie_diagnostic_result_list']['Brachyspira'] = 'Brachyspira';
$app_list_strings['cie_diagnostic_result_list']['Choke'] = 'Choke';
$app_list_strings['cie_diagnostic_result_list']['Conjunctivitis'] = 'Conjunctivitis';
$app_list_strings['cie_diagnostic_result_list']['Corneal Edema'] = 'Corneal Edema';
$app_list_strings['cie_diagnostic_result_list']['Corneal Ulcer'] = 'Corneal Ulcer';
$app_list_strings['cie_diagnostic_result_list']['Dental Disease'] = 'Dental Disease';
$app_list_strings['cie_diagnostic_result_list']['Dermatitis'] = 'Dermatitis';
$app_list_strings['cie_diagnostic_result_list']['Environment'] = 'Environment';
$app_list_strings['cie_diagnostic_result_list']['Estrus'] = 'Estrus';
$app_list_strings['cie_diagnostic_result_list']['Fight'] = 'Fight';
$app_list_strings['cie_diagnostic_result_list']['Fleas'] = 'Fleas';
$app_list_strings['cie_diagnostic_result_list']['Foreign Body'] = 'Foreign Body';
$app_list_strings['cie_diagnostic_result_list']['Fracture'] = 'Fracture';
$app_list_strings['cie_diagnostic_result_list']['full thickness dehiscence'] = 'full thickness dehiscence';
$app_list_strings['cie_diagnostic_result_list']['Giardia'] = 'Giardia';
$app_list_strings['cie_diagnostic_result_list']['hematomaseroma'] = 'hematoma/seroma';
$app_list_strings['cie_diagnostic_result_list']['Incisional Swelling'] = 'Incisional Swelling';
$app_list_strings['cie_diagnostic_result_list']['Infection'] = 'Infection';
$app_list_strings['cie_diagnostic_result_list']['Injection Site Abscess'] = 'Injection Site Abscess';
$app_list_strings['cie_diagnostic_result_list']['Intubation'] = 'Intubation';
$app_list_strings['cie_diagnostic_result_list']['Lawsonia'] = 'Lawsonia';
$app_list_strings['cie_diagnostic_result_list']['Lice'] = 'Lice';
$app_list_strings['cie_diagnostic_result_list']['Mange'] = 'Mange';
$app_list_strings['cie_diagnostic_result_list']['Melenoma'] = 'Melanoma';
$app_list_strings['cie_diagnostic_result_list']['Otitis externa'] = 'Otitis externa';
$app_list_strings['cie_diagnostic_result_list']['Otitis interna'] = 'Otitis interna';
$app_list_strings['cie_diagnostic_result_list']['Pain'] = 'Pain';
$app_list_strings['cie_diagnostic_result_list']['ParesisParalysis'] = 'Paresis/Paralysis';
$app_list_strings['cie_diagnostic_result_list']['Physiological'] = 'Physiological';
$app_list_strings['cie_diagnostic_result_list']['Porcine Parvovirus'] = 'Porcine Parvovirus';
$app_list_strings['cie_diagnostic_result_list']['Ring worm'] = 'Ring worm';
$app_list_strings['cie_diagnostic_result_list']['Roundworm'] = 'Roundworm';
$app_list_strings['cie_diagnostic_result_list']['Rubbing'] = 'Rubbing';
$app_list_strings['cie_diagnostic_result_list']['Sarcoma'] = 'Sarcoma';
$app_list_strings['cie_diagnostic_result_list']['Self Mutilation'] = 'Self Mutilation';
$app_list_strings['cie_diagnostic_result_list']['Skin only'] = 'Skin only';
$app_list_strings['cie_diagnostic_result_list']['Strongile'] = 'Strongile';
$app_list_strings['cie_diagnostic_result_list']['Study Related'] = 'Study Related';
$app_list_strings['cie_diagnostic_result_list']['Tender Foot'] = 'Tender Foot';
$app_list_strings['cie_diagnostic_result_list']['Ticks'] = 'Ticks';
$app_list_strings['cie_diagnostic_result_list']['Trauma'] = 'Trauma';
$app_list_strings['cie_diagnostic_result_list']['Unknown'] = 'Unknown';
$app_list_strings['cie_diagnostic_result_list']['Upper Respiratory Infection'] = 'Upper Respiratory Infection';
$app_list_strings['cie_diagnostic_result_list']['Urinary obstruction'] = 'Urinary obstruction';
$app_list_strings['cie_diagnostic_result_list']['valve regurg'] = 'valve regurg';
$app_list_strings['cie_diagnostic_result_list']['VSD'] = 'VSD';
$app_list_strings['cie_diagnostic_result_list'][''] = '';
$app_list_strings['cie_format_list']['Open Notes'] = 'Open Notes';
$app_list_strings['cie_format_list']['Organ Systems'] = 'Organ Systems';
$app_list_strings['cie_format_list'][''] = '';
$app_list_strings['tpr_method_list']['By CO'] = 'By CO';
$app_list_strings['tpr_method_list']['By Examiner'] = 'By Examiner';
$app_list_strings['tpr_method_list']['By Technician'] = 'By Technician for Exam';
$app_list_strings['tpr_method_list'][''] = '';
$app_list_strings['cie_organ_system_list']['Cardiovascular'] = 'Cardiovascular';
$app_list_strings['cie_organ_system_list']['Gastrointestinal'] = 'Gastrointestinal';
$app_list_strings['cie_organ_system_list']['Integument'] = 'Integument';
$app_list_strings['cie_organ_system_list']['Lymphatic'] = 'Lymphatic';
$app_list_strings['cie_organ_system_list']['Musculoskeletal'] = 'Musculoskeletal';
$app_list_strings['cie_organ_system_list']['Nervous System'] = 'Nervous System';
$app_list_strings['cie_organ_system_list']['Ophthalmic'] = 'Ophthalmic';
$app_list_strings['cie_organ_system_list']['Respiratory'] = 'Respiratory';
$app_list_strings['cie_organ_system_list']['Systemic'] = 'Systemic';
$app_list_strings['cie_organ_system_list']['Urogenital'] = 'Urogenital';
$app_list_strings['cie_organ_system_list'][''] = '';
$app_list_strings['section_data_list']['All Fields Normal'] = 'All Fields Normal';
$app_list_strings['section_data_list']['Complete Individual Sections'] = 'Complete Individual Sections';
$app_list_strings['section_data_list'][''] = '';
$app_list_strings['normal_ne_ginotes_subnotes_list']['Normal'] = 'Normal';
$app_list_strings['normal_ne_ginotes_subnotes_list']['Not Examined'] = 'Not Examined';
$app_list_strings['normal_ne_ginotes_subnotes_list']['See Gastrointestinal Notes'] = 'See Gastrointestinal Notes';
$app_list_strings['normal_ne_ginotes_subnotes_list'][''] = '';
$app_list_strings['normal_ne_seerespnote_list']['Normal'] = 'Normal';
$app_list_strings['normal_ne_seerespnote_list']['Not Examined'] = 'Not Examined';
$app_list_strings['normal_ne_seerespnote_list']['See Respiratory Notes'] = 'See Respiratory Notes';
$app_list_strings['normal_ne_seerespnote_list'][''] = '';
$app_list_strings['no_ne_seerespnote_list']['No'] = 'No';
$app_list_strings['no_ne_seerespnote_list']['Not Examined'] = 'Not Examined';
$app_list_strings['no_ne_seerespnote_list']['See Respiratory Notes'] = 'See Respiratory Notes';
$app_list_strings['no_ne_seerespnote_list'][''] = '';
$app_list_strings['no_ne_snsn_list']['No'] = 'No';
$app_list_strings['no_ne_snsn_list']['Not Examined'] = 'Not Examined';
$app_list_strings['no_ne_snsn_list']['See Nervous System Notes'] = 'See Nervous System Notes';
$app_list_strings['no_ne_snsn_list'][''] = '';
$app_list_strings['normal_ne_snsn_list']['Normal'] = 'Normal';
$app_list_strings['normal_ne_snsn_list']['Not Examined'] = 'Not Examined';
$app_list_strings['normal_ne_snsn_list']['See Nervous System Notes'] = 'See Nervous System Notes';
$app_list_strings['normal_ne_snsn_list'][''] = '';
$app_list_strings['normal_ne_sln_list']['Normal'] = 'Normal';
$app_list_strings['normal_ne_sln_list']['Not Examined'] = 'Not Examined';
$app_list_strings['normal_ne_sln_list']['See Lymphatic Notes'] = 'See Lymphatic Notes';
$app_list_strings['normal_ne_sln_list'][''] = '';
$app_list_strings['normal_ne_scn_list']['Normal'] = 'Normal';
$app_list_strings['normal_ne_scn_list']['Not Examined'] = 'Not Examined';
$app_list_strings['normal_ne_scn_list']['See Cardiovascular Notes'] = 'See Cardiovascular Notes';
$app_list_strings['normal_ne_scn_list'][''] = '';
$app_list_strings['normal_ne_smn_list']['Normal'] = 'Normal';
$app_list_strings['normal_ne_smn_list']['Not Examined'] = 'Not Examined';
$app_list_strings['normal_ne_smn_list']['See Musculoskeletal Notes'] = 'See Musculoskeletal Notes';
$app_list_strings['normal_ne_smn_list'][''] = '';
$app_list_strings['no_ne_smn_list']['No'] = 'No';
$app_list_strings['no_ne_smn_list']['Not Examined'] = 'Not Examined';
$app_list_strings['no_ne_smn_list']['See Musculoskeletal Notes'] = 'See Musculoskeletal Notes';
$app_list_strings['no_ne_smn_list'][''] = '';
$app_list_strings['no_ne_sin_list']['No'] = 'No';
$app_list_strings['no_ne_sin_list']['Not Examined'] = 'Not Examined';
$app_list_strings['no_ne_sin_list']['See Integument Notes'] = 'See Integument Notes';
$app_list_strings['no_ne_sin_list'][''] = '';
$app_list_strings['normal_ne_son_list']['Normal'] = 'Normal';
$app_list_strings['normal_ne_son_list']['Not Examined'] = 'Not Examined';
$app_list_strings['normal_ne_son_list']['See Ophthalmic Notes'] = 'See Ophthalmic Notes';
$app_list_strings['normal_ne_son_list'][''] = '';
$app_list_strings['no_ne_son_list']['No'] = 'No';
$app_list_strings['no_ne_son_list']['Not Examined'] = 'Not Examined';
$app_list_strings['no_ne_son_list']['See Ophthalmic Notes'] = 'See Ophthalmic Notes';
$app_list_strings['no_ne_son_list'][''] = '';
$app_list_strings['normal_ne_sun_list']['Normal'] = 'Normal';
$app_list_strings['normal_ne_sun_list']['Not Examined'] = 'Not Examined';
$app_list_strings['normal_ne_sun_list']['See Urogenital Notes'] = 'See Urogenital Notes';
$app_list_strings['normal_ne_sun_list'][''] = '';
$app_list_strings['section_notes_data_list']['All None'] = 'All None';
$app_list_strings['section_notes_data_list']['Complete Individual Sections'] = 'Complete Individual Sections';
$app_list_strings['section_notes_data_list'][''] = '';
$app_list_strings['performed_by_tech_exam_transcribed_not_performed']['Not Performed'] = 'Not Performed';
$app_list_strings['performed_by_tech_exam_transcribed_not_performed']['Performed by Examiner'] = 'Performed by Examiner';
$app_list_strings['performed_by_tech_exam_transcribed_not_performed']['Performed by Technician'] = 'Performed by Technician';
$app_list_strings['performed_by_tech_exam_transcribed_not_performed']['Transcribed from Clin Ob'] = 'Transcribed from Clin Ob';
$app_list_strings['performed_by_tech_exam_transcribed_not_performed'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.tech_list.php


$app_list_strings['tech_list']= array(
'Technician' => 'Technician',
);


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.vet_list.php


$app_list_strings['vet_list']= array(
'Veterinarian' => 'Veterinarian',
);


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.examiner_type_list.php


$app_list_strings['examiner_type_list']= array(
    '' => '',
'Technician' => 'Technician',
'Veterinarian' => 'Veterinarian'
);


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.CO_dependency_lang.php


$app_list_strings["rt_taken_list"] = array(
    'Taken' => 'Taken'
);

$app_list_strings["rt_taken_both_list"] = array(
    '' => '',
    'Taken' => 'Taken',
    'Not Taken' => 'Not Taken'
);

$app_list_strings["rt_not_taken_list"] = array(
    'Not Taken' => 'Not Taken'
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Service_Vendor.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['SV_Service_Vendor'] = 'Service Vendors';
$app_list_strings['moduleListSingular']['SV_Service_Vendor'] = 'Service Vendor';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_normal_see_gas_list.php

 // created: 2020-01-17 10:15:43

$app_list_strings['normal_see_gas_list']=array (
  'Normal' => 'Normal',
  'See Gastrointestinal Notes' => 'See Gastrointestinal Notes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_oral_cavity_list_not_exam.php

 // created: 2020-01-17 10:16:53

$app_list_strings['oral_cavity_list_not_exam']=array (
  'Not Examined' => 'Not Examined',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_oral_cavity_list_normal.php

 // created: 2020-01-17 10:18:00

$app_list_strings['oral_cavity_list_normal']=array (
  'Normal' => 'Normal',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_normal_sln_list.php

 // created: 2020-01-17 10:28:47

$app_list_strings['normal_sln_list']=array (
  'Normal' => 'Normal',
  'See Lymphatic Notes' => 'See Lymphatic Notes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_co_gait_abnormal_list.php

 // created: 2019-12-21 13:58:54

$app_list_strings['gait_abnormal_list']=array (
  'Abnormal' => 'Abnormal',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_co_gait_normal_list.php

 // created: 2019-12-21 13:58:54

$app_list_strings['gait_normal_list']=array (
  'Normal' => 'Normal',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_co_appetite_list.php

 // created: 2019-12-21 13:58:54

$app_list_strings['appetite_dd_list']=array (
  '' => '',
  'Normal' => 'Normal',
  'Mild Inappetent' => 'Mild Inappetent (~1/4-1/2 feed remaining)',
  'Moderate Inappetent' => 'Moderate Inappetent (~1/2-3/4 feed remaining)',
  'Anorexic' => 'Anorexic (all feed remaining)',
  'Not Assessed' => 'Not Assessed (Group Housed)',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_co_mentation_dd_list.php


$app_list_strings['mentation_dd_list']=array (
  '' => '',
  'BAR' => 'BAR',
  'QAR' => 'QAR',
  'LethargicDepressed' => 'Lethargic/Depressed',
  'Unresponsive' => 'Unresponsive',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_co_feces_dd_list.php


$app_list_strings['feces_dd_list']=array (
  '' => '',
  'Normal' => 'Normal',
   'Soft' => 'Soft',
   'Diarrhea' => 'Diarrhea',
   'Scant' => 'Scant',
  'Absent' => 'Absent',
  'Not Assessed' => 'Not Assessed (Group Housed)',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_mentation_bar.php


$app_list_strings['mentation_bar_list'] = array(
    'BAR' => 'BAR',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_normal_list.php


$app_list_strings['app_feces_normal_list'] = array(
    'Normal' => 'Normal',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.notes_data_lang.php


$app_list_strings['options_yes'] = array(
  'Yes' => 'Yes'
);

$app_list_strings['options_none'] = array(
  'None' => 'None'
);

$app_list_strings['options_yes_none'] = array(
  '' => '',
  'Yes' => 'Yes',
  'None' => 'None'
);


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_shipping_delivery_list.php

 // created: 2020-02-04 13:20:54

$app_list_strings['shipping_delivery_list']=array (
  '' => '',
  'Direct' => 'Direct',
  '2 HR' => '2 HR',
  'Same Day' => 'Same Day',
  '1 HR' => '1 HR',
  '3 HR' => '3 HR',
  'International Priority' => 'International Priority',
  'First Priority Overnight' => 'First Priority Overnight',
  'Priority Overnight' => 'Priority Overnight',
  '2 Day' => '2 Day',
  'Ground' => 'Ground',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_shipping_conditions_list.php

 // created: 2020-02-04 13:25:53

$app_list_strings['shipping_conditions_list']=array (
  '' => '',
  'Ambient' => 'Ambient',
  'Refrigeratedon ice packs' => 'Refrigerated/on ice packs',
  'Frozen 12on ice packs' => 'Frozen (-12/on ice packs)',
  'Frozen 70on dry ice' => 'Frozen (-70/on dry ice)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_material_category_list.php

 // created: 2020-02-04 13:32:10

$app_list_strings['material_category_list']=array (
  '' => '',
  'Biological Substance' => 'Biological Substance',
  'Category B' => 'Category B',
  'NA' => 'NA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_hazardous_contents_list.php

 // created: 2020-02-06 13:30:25

$app_list_strings['hazardous_contents_list']=array (
  '' => '',
  'Biological Substance Category B' => 'Biological Substance – Category B',
  'Ethanol ETOH' => 'Ethanol (ETOH)',
  'Glutaraldehyde' => 'Glutaraldehyde',
  'Hexane' => 'Hexane',
  'Methanol' => 'Methanol',
  'NA' => 'NA',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_deliverable_type_list.php

 // created: 2020-03-16 11:48:25

$app_list_strings['deliverable_type_list']=array (
  '' => '',
  'BloodPlasmaSerum' => 'Blood/Plasma/Serum',
  'Feed' => 'Feed',
  'Hay' => 'Hay',
  'Paraffin Blocks' => 'Paraffin Blocks',
  'Slide' => 'Slide',
  'Study Materials' => 'Study Materials',
  'Tissue' => 'Tissue',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_category_id_list.php
 
$app_list_strings['category_id_list'] = array (
  '' => '',
  'Material Transfer Agreement' => 'Material Transfer Agreement',
  'MSA' => 'MSA',
  'MSA Amendment' => 'MSA Amendment',
  'NDA_CDA' => 'NDA / CDA',
  'Qualification' => 'Qualification',
  'Quality Agreement' => 'Quality Agreement',
  'Supplemental' => 'Supplemental',
  'Supplier Survey' => 'Supplier Survey',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_shipping_company_list.php
 
$app_list_strings['shipping_company_list'] = array (
  '' => '',
  'Fed Ex' => 'Fed Ex',
  'Hand Carry' => 'Hand Carry',
  'On Time' => 'On Time',
  'World Courier' => 'World Courier',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_quoteco_status_list.php
 
$app_list_strings['quoteco_status_list'] = array (
  'Under Review' => 'Under Review',
  'Sponsor Approved' => 'Sponsor Approved',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sales_activity_quote_req_list.php

 // created: 2020-05-13 17:11:27

$app_list_strings['sales_activity_quote_req_list']=array (
  '' => '',
  'Duplicate' => 'Duplicate',
  'Internal APS Project' => 'Internal APS Project',
  'Invoice_Additional_Testing_Performed' => 'Invoice - Additional Testing Performed',
  'Invoicing_Complete' => 'Invoicing Complete',
  'Invoicing_Initiated' => 'Invoicing In Progress',
  'Lead' => 'Lead',
  'Lead_Long Term Follow Up' => 'Lead - Long Term Follow Up',
  'Lost' => 'Lost',
  'MSA for Review' => 'MSA for Review',
  'NDA for Review' => 'NDA for Review',
  'Not Performed' => 'Not Performed',
  'Pricelist Submitted' => 'Pricelist Submitted',
  'draft for review' => 'Quote Draft for Review',
  'Quote Expired Long Term FollowUp' => 'Quote Expired - Long Term Follow-Up',
  'Out for Sponsor Review' => 'Quote Out for Sponsor Review',
  'Required' => 'Quote Required',
  'Quote_Required_Scheduled' => 'Quote Required - Scheduled',
  'Revision Required' => 'Quote Revision Required',
  'Send MSA to Sponsor' => 'Send MSA to Sponsor',
  'Send NDA to Sponsor' => 'Send NDA to Sponsor',
  'Send to Sponsor' => 'Send Quote to Sponsor',
  'Send SOW to Sponsor' => 'Send SOW to Sponsor',
  'SOW for Review' => 'SOW for Review',
  'Study Plan Development' => 'Study Plan Development',
  'Unknown' => 'Unknown',
  'Won' => 'Won - Invoice',
  'Won Quote Revision for Review' => 'Won - Quote Revision for Review',
  'Won Quote Revision Required' => 'Won - Quote Revision Required',
  'Won_waiting' => 'Won - Waiting on Signed Quote/PO',
  'Won Quote Revision Out for Review' => 'Won- Quote Revision Out for Sponsor Review ',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_vaccine_c_list.php

 // created: 2020-05-13 17:22:13

$app_list_strings['vaccine_c_list']=array (
  '' => '',
  'Bordetella' => 'Bordetella',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_study_compliance_list.php

 // created: 2020-05-13 17:15:47

$app_list_strings['study_compliance_list']=array (
  '' => '',
  'NonGLP Discovery' => 'NonGLP - Discovery',
  'NonGLP Structured' => 'NonGLP - Structured',
  'GLP' => 'GLP',
  'ISO' => 'ISO',
  'USP' => 'USP',
  'JMHLW' => 'JMHLW',
  'Not Applicable' => 'Not Applicable',
  'non_GLP' => 'non-GLP',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_pathologist_workload_list.php

 // created: 2020-05-13 18:24:21

$app_list_strings['pathologist_workload_list']=array (
  '' => '',
  'Low' => 'Low',
  'Medium' => 'Medium',
  'High' => 'High',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_company_division_list.php

 // created: 2020-05-13 16:34:56

$app_list_strings['company_division_list']=array (
  '' => '',
  'Analytical Services' => 'Analytical Services',
  'Bioskills' => 'Bioskills',
  'Biocompatibility' => 'Custom Biocompatibility',
  'Standard Biocompatibility' => 'Standard Biocompatibility',
  'Histology Services' => 'Histology Services',
  'ISR' => 'ISR',
  'Pathology Services' => 'Pathology Services',
  'Pharmacology' => 'Pharmacology',
  'Regulatory' => 'Regulatory',
  'Toxicology' => 'Toxicology',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sex_list.php

 // created: 2020-05-13 17:18:31

$app_list_strings['sex_list']=array (
  '' => '',
  'Female' => 'Female',
  'Castrated Male' => 'Castrated Male',
  'Male' => 'Male',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_expanded_study_result_list.php

 // created: 2020-05-13 17:26:17

$app_list_strings['expanded_study_result_list']=array (
  '' => '',
  'Not Applicable' => 'Not Applicable',
  'Passed Study' => 'Passed Study',
  'Failed Study' => 'Failed Study',
  'Completed Study' => 'Completed Study',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_bc_study_article_received_list.php

 // created: 2020-05-13 16:49:14

$app_list_strings['bc_study_article_received_list']=array (
  '' => '',
  'Estimated Receipt' => 'Estimated Receipt',
  'Partially Received' => 'Partially Received',
  'Received' => 'Received',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_reason_for_amendment_list.php

 // created: 2020-05-13 18:25:52

$app_list_strings['reason_for_amendment_list']=array (
  '' => '',
  'Typographical Error' => 'Typographical and/or Grammatical Error',
  'Data Entry Error' => 'Data Entry Error',
  'Error in Final Report PDF' => 'Final Report PDF Compilation Error ',
  'Sponsor Request' => 'Sponsor Request  for Information Update',
  'Sponsor Request for Additional Analysis' => 'Sponsor Request for Additional Analysis',
  'Audit Finding' => 'Audit Finding',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_invoice_installment_percent_list.php

 // created: 2020-05-13 16:50:34

$app_list_strings['invoice_installment_percent_list']=array (
  '' => '',
  'None' => 'None',
  10 => '10%',
  25 => '25%',
  40 => '40%',
  50 => '50%',
  60 => '60%',
  75 => '75%',
  100 => '100%',
  '1x Monthly' => '1x Monthly',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_vaccine_a_list.php

 // created: 2020-05-13 17:19:47

$app_list_strings['vaccine_a_list']=array (
  '' => '',
  'Mycoplasma' => 'Mycoplasma',
  'Rabies' => 'Rabies',
  'CDT' => 'CDT',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_data_storage_phase_list.php

 // created: 2020-05-13 17:25:01

$app_list_strings['data_storage_phase_list']=array (
  '' => '',
  'Carousel' => 'Carousel',
  'Reporting' => 'Reporting',
  'Complete and In Review' => 'Complete and In Review',
  'Scanned' => 'Scanned',
  'Archived' => 'Archived',
  'Shipped or Returned' => 'Shipped or Returned',
  'Destroyed' => 'Destroyed',
  'Study Not Performed' => 'Study Not Performed',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_work_product_compliance_list.php

 // created: 2020-05-13 18:10:38

$app_list_strings['work_product_compliance_list']=array (
  'Not Applicable' => 'Not Applicable',
  'NonGLP Discovery' => 'NonGLP - Discovery',
  'NonGLP Structured' => 'NonGLP - Structured',
  'GLP' => 'GLP',
  'GLP Not Performed' => 'GLP, Not Performed',
  'GLP Discontinued' => 'GLP, Discontinued',
  'Withdrawn' => 'Obsoleted, valid 08-13-14 to 04-27-18 GLP, Withdrawn',
  'GLP_Amended_NonGLP' => 'Obsoleted, valid 08-13-14 to 04-27-18 GLP, Amended nonGLP',
  'nonGLP' => 'nonGLP',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_reason_for_lost_quote.php

 // created: 2020-05-13 17:08:00

$app_list_strings['reason_for_lost_quote']=array (
  '' => '',
  'no response from client' => 'No Response from Client',
  'lab proximity to company' => 'Lab Proximity to Company',
  'lower quote from competitive lab' => 'Lower Quote from Competitive Lab',
  'scheduling issue' => 'Scheduling Issue',
  'experience' => 'Lab Experience',
  'lack of required equipment' => 'Lack of Required Equipment',
  'lack of timely response' => 'Lack of Timely Response',
  'report timelines' => 'Report Timelines',
  'capacity' => 'Capacity',
  'WL' => 'WL',
  'Chose another lab' => 'Selected a Different Lab',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_quote_review_priority_list.php

 // created: 2020-05-13 18:36:27

$app_list_strings['quote_review_priority_list']=array (
  '' => '',
  'low' => 'Low',
  'medium' => 'Medium',
  'high' => 'High',
  'critical' => 'Critical',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_vaccine_b_c_list.php

 // created: 2020-05-13 17:20:56

$app_list_strings['vaccine_b_c_list']=array (
  '' => '',
  'DHPP' => 'DHPP',
  'Lawsonia' => 'Lawsonia',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_Yes_No.php

 // created: 2020-05-13 16:29:50

$app_list_strings['Yes_No']=array (
  '' => '',
  'Yes' => 'Yes',
  'No' => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_yes_no_radio_list.php

 // created: 2020-06-09 11:43:27

$app_list_strings['yes_no_radio_list']=array (
  'Yes' => 'Yes',
  'No' => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_meeting_type_dom.php

 // created: 2020-06-09 22:16:42
$app_list_strings['meeting_type_dom']=array (
  '' => '',
  'Conference Call' => 'Conference Call',
  'In Person' => 'In Person',
  'Online' => 'Online',
  'Tradeshow' => 'Tradeshow',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_equipment_and_facility_records_type_list.php

 // created: 2020-06-16 09:04:45

$app_list_strings['equipment_and_facility_records_type_list']=array (
  '' => '',
  'Calibration' => 'Calibration',
  'Chain of Custody' => 'Chain of Custody',
  'Cleaning' => 'Cleaning',
  'Initial In Service' => 'Completed In-Service',
  'Equipment Qualification' => 'Performance Verification',
  'Initial In Service 2' => 'Initial in Service',
  'Inspection' => 'Inspection',
  'Part 11 Gap Analysis' => 'Part 11 Gap Analysis',
  'Preventative Maintenance' => 'Preventative Maintenance',
  'Repair' => 'Repair',
  'Routine Maintenance' => 'Routine Maintenance',
  'Service Contract' => 'Service Contract',
  'SoftwareFirmware Change' => 'Software/Firmware Change',
  'Verification' => 'Verification',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_work_type_list.php

 // created: 2020-06-25 09:58:04

$app_list_strings['work_type_list']=array (
  '' => '',
  'Per Quote' => 'Per Quote',
  'Additional Work' => 'Additional Work',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_spa.php

 // created: 2020-07-24 12:57:13

$app_list_strings['spa']=array (
  'Not Started' => 'Not Started',
  'Waiting on Payment' => 'Waiting on Payment',
  'Sponsor Submitted' => 'Sponsor Submitted',
  'Waiting on Sponsor' => 'Waiting on Sponsor',
  'APS Approved' => 'APS Approved',
  'APS Reviewed' => 'APS Reviewed (Obsolete)',
  'Sponsor Finalized' => 'Sponsor Finalized (Obsolete)',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_company_category_list.php

 // created: 2020-07-31 13:57:20

$app_list_strings['company_category_list']=array (
  '' => '',
  'Sponsor' => 'Sponsor',
  'Sponsor and Vendor' => 'Sponsor and Vendor',
  'Vendor' => 'Vendor',
  'Manufacturer' => 'Manufacturer',
  'Sponsor and Manufacturer' => 'Sponsor and Manufacturer',
  'Vendor and Manufacturer' => 'Vendor and Manufacturer',
  'SponsorVendor Manufacturer' => 'Sponsor, Vendor, and Manufacturer',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_product_status_list.php

 // created: 2020-08-13 15:49:47

$app_list_strings['product_status_list']=array (
  'Active' => 'Active',
  'Out of Production' => 'Out of Production',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_product_notes_multiselect_list.php

 // created: 2020-08-13 15:52:10

$app_list_strings['product_notes_multiselect_list']=array (
  '' => '',
  'Cannot Procure' => 'Cannot Procure',
  'Long Lead Time' => 'Long Lead Time',
  'Often Backordered' => 'Often Backordered',
  'Short Supply' => 'Short Supply',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.GDP_Examples.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['ge_gdp_examples_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['ge_gdp_examples_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['ge_gdp_examples_category_dom']['Sales'] = 'Sales';
$app_list_strings['ge_gdp_examples_category_dom'][''] = '';
$app_list_strings['ge_gdp_examples_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['ge_gdp_examples_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['ge_gdp_examples_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['ge_gdp_examples_subcategory_dom'][''] = '';
$app_list_strings['ge_gdp_examples_status_dom']['Active'] = 'Active';
$app_list_strings['ge_gdp_examples_status_dom']['Draft'] = 'Draft';
$app_list_strings['ge_gdp_examples_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['ge_gdp_examples_status_dom']['Expired'] = 'Expired';
$app_list_strings['ge_gdp_examples_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['ge_gdp_examples_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['GE_GDP_Examples'] = 'GDP Examples';
$app_list_strings['moduleListSingular']['GE_GDP_Examples'] = 'GDP Example';
$app_list_strings['gdp_ex_area_list']['Analytical'] = 'Analytical';
$app_list_strings['gdp_ex_area_list']['Histo'] = 'Histo';
$app_list_strings['gdp_ex_area_list']['IVTClin PathSterilization'] = 'IVT/Clin Path/Sterilization';
$app_list_strings['gdp_ex_area_list']['LA ATVets'] = 'LA AT/Vets';
$app_list_strings['gdp_ex_area_list']['LA ISR OpsVets'] = 'LA ISR Ops/Vets';
$app_list_strings['gdp_ex_area_list']['LA PRTVets'] = 'LA PRT/Vets';
$app_list_strings['gdp_ex_area_list']['LA RTVets'] = 'LA RT/Vets';
$app_list_strings['gdp_ex_area_list']['Pathology'] = 'Pathology';
$app_list_strings['gdp_ex_area_list']['PIFacilities'] = 'PI/Facilities';
$app_list_strings['gdp_ex_area_list']['SA ATVets'] = 'SA AT/Vets';
$app_list_strings['gdp_ex_area_list']['SA RTVets'] = 'SA RT/Vets';
$app_list_strings['gdp_ex_area_list']['Sample Prep'] = 'Sample Prep';
$app_list_strings['gdp_ex_area_list']['Scientific'] = 'Scientific';
$app_list_strings['gdp_ex_area_list'][''] = '';
$app_list_strings['pos_neg_list']['Negative'] = 'Negative';
$app_list_strings['pos_neg_list']['Positive'] = 'Positive';
$app_list_strings['pos_neg_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_product_allowed_storage_list.php

 // created: 2020-09-03 12:23:17

$app_list_strings['product_allowed_storage_list']=array (
  '' => '',
  'Ambient Temperature' => 'Ambient Temperature',
  'Room Temperature' => 'Room Temperature',
  'Refrigerated' => 'Refrigerated',
  'Frozen' => 'Frozen',
  'Ultra Frozen in Liquid Nitrogen' => 'Ultra Frozen in Liquid Nitrogen',
  'Ultra Frozen in Freezer' => 'Ultra Frozen in Freezer',
  'To Be Determined' => 'To Be Determined',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_prd_level_list.php

 // created: 2020-09-08 09:38:33

$app_list_strings['prd_level_list']=array (
  '' => '',
  'Assistant' => 'Assistant',
  'Primary' => 'Primary',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Product_Document.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['prodo_product_document_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['prodo_product_document_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['prodo_product_document_category_dom']['Sales'] = 'Sales';
$app_list_strings['prodo_product_document_category_dom'][''] = '';
$app_list_strings['prodo_product_document_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['prodo_product_document_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['prodo_product_document_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['prodo_product_document_subcategory_dom'][''] = '';
$app_list_strings['prodo_product_document_status_dom']['Active'] = 'Active';
$app_list_strings['prodo_product_document_status_dom']['Draft'] = 'Draft';
$app_list_strings['prodo_product_document_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['prodo_product_document_status_dom']['Expired'] = 'Expired';
$app_list_strings['prodo_product_document_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['prodo_product_document_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['ProDo_Product_Document'] = 'Product Documents';
$app_list_strings['moduleListSingular']['ProDo_Product_Document'] = 'Product Document';
$app_list_strings['quality_requirement_list']['Certificate'] = 'Certificate';
$app_list_strings['quality_requirement_list']['Certificate of Analysis'] = 'Certificate of Analysis';
$app_list_strings['quality_requirement_list']['Certificate of Compliance'] = 'Certificate of Compliance';
$app_list_strings['quality_requirement_list']['Certificate of Conformity'] = 'Certificate of Conformity';
$app_list_strings['quality_requirement_list']['Certificate of Quality'] = 'Certificate of Quality';
$app_list_strings['quality_requirement_list']['Certificate of Sterility'] = 'Certificate of Sterility';
$app_list_strings['quality_requirement_list']['None'] = 'None';
$app_list_strings['quality_requirement_list']['Performance Report'] = 'Performance Report';
$app_list_strings['quality_requirement_list']['Quality Control Production Certificate'] = 'Quality Control & Production Certificate';
$app_list_strings['quality_requirement_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Product.php
 
$app_list_strings['moduleList']['Prod_Product'] = 'Products';
$app_list_strings['moduleListSingular']['Prod_Product'] = 'Product';
$app_list_strings['product_category_list']=array (
  '' => '',
  'Autoclave supplies' => 'Autoclave supplies',
  'Balloon Catheter' => 'Balloon Catheter',
  'Bandaging' => 'Bandaging',
  'Blood draw supply' => 'Blood draw supply',
  'Cannula' => 'Cannula',
  'Catheter' => 'Catheter',
  'Chemical' => 'Chemical',
  'Cleaning Agent' => 'Cleaning Agent',
  'Cleaning Supplies' => 'Cleaning Supplies',
  'Contrast' => 'Contrast',
  'Drug' => 'Drug',
  'Endotracheal TubeSupplies' => 'Endotracheal Tube/Supplies',
  'Enrichment Toy' => 'Enrichment Toy',
  'Equipment' => 'Equipment',
  'ETO supplies' => 'ETO supplies',
  'Feeding supplies' => 'Feeding supplies',
  'Graft' => 'Graft',
  'Inflation Device' => 'Inflation Device',
  'Office Supply' => 'Office Supply',
  'Other' => 'Other',
  'Reagent' => 'Reagent',
  'Sheath' => 'Sheath',
  'Shipping supplies' => 'Shipping supplies',
  'Solution' => 'Solution',
  'Stent' => 'Stent',
  'Surgical Instrument' => 'Surgical Instrument',
  'Surgical site prep' => 'Surgical site prep',
  'Suture' => 'Suture',
  'Tubing' => 'Tubing',
  'Wire' => 'Wire',
);

$app_list_strings['pro_subtype_list']=array (
  '' => '',
  'Access Catheter' => 'Access Catheter',
  'Adaptor' => 'Adaptor',
  'Angioplasty Balloon' => 'Angioplasty Balloon',
  'Aortic Root Cannula' => 'Aortic Root Cannula',
  'Arterial Cannula' => 'Arterial Cannula',
  'Bare Metal Stent' => 'Bare Metal Stent',
  'Barium Contrast' => 'Barium Contrast',
  'Binders and supplies' => 'Binders and supplies',
  'Bottle' => 'Bottle',
  'Breathing circuit' => 'Breathing circuit',
  'Brush' => 'Brush',
  'Butterfly Catheter' => 'Butterfly Catheter',
  'CD case' => 'CD case',
  'Central Venous Catheter' => 'Central Venous Catheter',
  'Clipper/Clipper Blade' => 'Clipper/Clipper Blade',
  'Coated' => 'Coated',
  'Cold Pack' => 'Cold Pack',
  'Connector' => 'Connector',
  'Contrast Spike' => 'Contrast Spike',
  'Corrugated box' => 'Corrugated box',
  'Cryovial' => 'Cryovial',
  'Cutting Balloon' => 'Cutting Balloon',
  'Diagnostic Catheter' => 'Diagnostic Catheter',
  'Dressing' => 'Dressing',
  'Drug Coated Balloon' => 'Drug Coated Balloon',
  'Drug Coated Stent' => 'Drug Coated Stent',
  'Dry Ice' => 'Dry Ice',
  'Enrichment treats' => 'Enrichment treats',
  'EP Catheter' => 'EP Catheter',
  'ePTFE' => 'ePTFE',
  'Essentials Kit' => 'Essentials Kit',
  'ET Tube' => 'ET Tube',
  'Ethibond' => 'Ethibond',
  'Ethilon' => 'Ethilon',
  'Exchange Wire' => 'Exchange Wire',
  'Extension Catheter' => 'Extension Catheter',
  'Extension line' => 'Extension line',
  'Extension Tubing' => 'Extension Tubing',
  'Feeding Tube' => 'Feeding Tube',
  'Flammable' => 'Flammable',
  'Floor scrubber supplies' => 'Floor scrubber supplies',
  'Foley Catheter' => 'Foley Catheter',
  'Guide Catheter' => 'Guide Catheter',
  'Guide Wire' => 'Guide Wire',
  'High Pressure' => 'High Pressure',
  'High Pressure Tubing' => 'High Pressure Tubing',
  'Incision site cover' => 'Incision site cover',
  'Indicator label' => 'Indicator label',
  'Injectable' => 'Injectable',
  'Injection port' => 'Injection port',
  'Insulated shipper' => 'Insulated shipper',
  'Introducer Sheath' => 'Introducer Sheath',
  'Invasive Blood Pressure Supplies' => 'Invasive Blood Pressure Supplies',
  'Iodinated Contrast' => 'Iodinated Contrast',
  'IV Catheter' => 'IV Catheter',
  'Knitted' => 'Knitted',
  'Label' => 'Label',
  'Laminating Sheets' => 'Laminating Sheets',
  'Machine' => 'Machine',
  'Mapping Catheter' => 'Mapping Catheter',
  'Marking Wire' => 'Marking Wire',
  'Micro Catheter' => 'Micro Catheter',
  'Mila Catheter' => 'Mila Catheter',
  'Monocryl' => 'Monocryl',
  'Needle' => 'Needle',
  'Needless port' => 'Needless port',
  'Non Flammable' => 'Non-Flammable',
  'Oral' => 'Oral',
  'Other' => 'Other',
  'OTW over the wire' => 'OTW (over the wire)',
  'Padding' => 'Padding',
  'Pallet' => 'Pallet',
  'Paper' => 'Paper',
  'Patches' => 'Patches',
  'PDS' => 'PDS',
  'Pediatric Cannula' => 'Pediatric Cannula',
  'Perc Needle' => 'Perc Needle',
  'Pledget' => 'Pledget',
  'POBA Balloon' => 'POBA Balloon',
  'Pressure Catheter' => 'Pressure Catheter',
  'Primary diet' => 'Primary diet',
  'Probe Cover' => 'Probe Cover',
  'Prolene' => 'Prolene',
  'PTA Balloon' => 'PTA Balloon',
  'Replacement Part' => 'Replacement Part',
  'Silk' => 'Silk',
  'SpongePad' => 'Sponge/Pad',
  'Steerable Sheath' => 'Steerable Sheath',
  'Stopcock' => 'Stopcock',
  'Stylet' => 'Stylet',
  'Sub layer wrap' => 'Sub layer wrap',
  'Supplement diet' => 'Supplement diet',
  'Support Catheter' => 'Support Catheter',
  'Support Wire' => 'Support Wire',
  'Surgical scrub' => 'Surgical scrub',
  'Syringe' => 'Syringe',
  'Tape' => 'Tape',
  'Ti Cron' => 'Ti-Cron',
  'Top layer wrap' => 'Top layer wrap',
  'Topical' => 'Topical',
  'Torque Device' => 'Torque Device',
  'Transseptal Catheter' => 'Transseptal Catheter',
  'Trash bag' => 'Trash bag',
  'Tuohy' => 'Tuohy',
  'Vacutainer' => 'Vacutainer',
  'Venous Cannula' => 'Venous Cannula',
  'Vessel Cannula' => 'Vessel Cannula',
  'Vicryl' => 'Vicryl',
  'Woven' => 'Woven',
  'Wrap' => 'Wrap',
);
$app_list_strings['quality_requirement_list'] = array (
  'Certificate' => 'Certificate',
  'Certificate of Analysis' => 'Certificate of Analysis',
  'Certificate of Compliance' => 'Certificate of Compliance',
  'Certificate of Conformity' => 'Certificate of Conformity',
  'Certificate of Quality' => 'Certificate of Quality',
  'Certificate of Sterility' => 'Certificate of Sterility',
  'None' => 'None',
  'Performance Report' => 'Performance Report',
  'Quality Control Production Certificate' => 'Quality Control & Production Certificate',
  '' => '',
);$app_list_strings['purchase_unit_list'] = array (
  'Each' => 'Each',
  'Bottle' => 'Bottle',
  'Box' => 'Box',
  'Case' => 'Case',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_service_vendor_type_list.php

 // created: 2020-10-23 13:39:10

$app_list_strings['service_vendor_type_list']=array (
  '' => '',
  'Calibration vendor' => 'Calibration vendor',
  'PM vendor' => 'PM vendor',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_iso_17025_units_of_measure_list.php

 // created: 2020-10-26 18:17:53

$app_list_strings['iso_17025_units_of_measure_list']=array (
  '' => '',
  'CO2' => 'CO2',
  'Dimension' => 'Dimension',
  'Length' => 'Length',
  'Mass' => 'Mass',
  'Relative Humidity' => 'Relative Humidity',
  'RPM' => 'RPM',
  'Temperature' => 'Temperature',
  'Time' => 'Time',
  'Voltage' => 'Voltage',
  'Volume' => 'Volume',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_disposition_type_deliverable_list.php

 // created: 2020-11-17 08:14:30

$app_list_strings['disposition_type_deliverable_list']=array (
  '' => '',
  'Shipping' => 'Shipping',
  'Transfer' => 'Transfer',
  'Discard' => 'Discard',
  'Archive' => 'Archive',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_why_it_happened_c_list.php

 // created: 2020-10-29 16:08:48

$app_list_strings['why_it_happened_c_list']=array (
  '' => '',
  'Known' => 'Yes',
  'Unknown Needs Investigation' => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_classification_list.php

 // created: 2020-11-30 19:27:32

$app_list_strings['classification_list']=array (
  '' => '',
  'Data Generator With Electronic Records' => 'Data Generator With Electronic Records',
  'Data Generator Without Electronic Records' => 'Data Generator Without Electronic Records',
  'Facility Task' => 'Facility Task',
  'Non Data Generator' => 'Non Data Generator',
  'Non GLP Equipment' => 'Non GLP Equipment',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_expense_or_capitalize_list.php

 // created: 2020-12-08 09:29:58

$app_list_strings['expense_or_capitalize_list']=array (
  '' => '',
  'Expense' => 'Expense',
  'Capitalize' => 'Capitalize',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_ef_status_list.php

 // created: 2020-12-03 15:06:36

$app_list_strings['ef_status_list']=array (
  '' => '',
  'Active' => 'Active',
  'Active GLP compliant' => 'Active, GLP compliant',
  'Active GLP non compliant' => 'Active, GLP non-compliant',
  'Out for Service' => 'Out for Service',
  'Quarantined' => 'Quarantined',
  'Retired' => 'Retired',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_management_assessment_train_list.php

 // created: 2020-10-16 05:38:21

$app_list_strings['management_assessment_train_list']=array (
  '' => '',
  'Major Change or New' => 'Employee performed activities under this Controlled Document while overdue; Controlled Document Revision = Major Change or New',
  'Revision Change or Minor Change' => 'Employee performed activities under this Controlled Document while overdue; Controlled Document Revision = Revision Change Only or Minor Change',
  'Employee did NOT perform SOP tasks while overdue' => 'Employee did NOT undertake activities under this Controlled Document while overdue',
  'The Controlled Document is a standalone document No Controlled Document activities are associated' => 'The Controlled Document is a standalone document. No Controlled Document activities are associated with the document.',
  'Overdue refresher training' => 'Employee performed activities under this Controlled Document when their Refresher Training was overdue',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_impactful_list.php

 // created: 2020-10-15 11:02:53

$app_list_strings['impactful_list']=array (
  '' => '',
  'This Deviation was impactful' => 'This Deviation was impactful.',
  'This Deviation was NOT impactful' => 'This Deviation was NOT impactful.',
  'Pending needs further assessment before finalizing' => 'Pending, needs further assessment before finalizing.',
  'This Adverse Event was impactful' => 'This Adverse Event was impactful.',
  'This Adverse Event was NOT impactful' => 'This Adverse Event was NOT impactful.',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_ed_type_list.php

 // created: 2020-12-15 12:05:33

$app_list_strings['ed_type_list']=array (
  '' => '',
  'Animal Enrollment' => 'Animal Enrollment',
  'Animal Health Status to SD' => 'Animal Health Status to SD',
  'Business Development' => 'Business Development',
  'Client Correspondence' => 'Client Correspondence',
  'COM email' => 'COM email',
  'Early Term Early Death' => 'Early Term/Early Death',
  'External Feedback' => 'External Feedback',
  'External Feedback Follow Up' => 'External Feedback Follow Up',
  'Interinstitutional Assurance' => 'Interinstitutional Assurance',
  'Necropsy Findings' => 'Necropsy Findings',
  'Other' => 'Other',
  'SD Assessment Confirmation' => 'SD Assessment Confirmation',
  'SD COM Notification and Acknowledgment' => 'SD COM Notification and Acknowledgment',
  'Test Article Failures' => 'Test Article Failures',
  'Unblinding of Study Personnel' => 'Unblinding of Study Personnel',
  'Vet Findings' => 'Vet Findings',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_enrollment_status_list.php

 // created: 2020-12-15 12:39:02

$app_list_strings['enrollment_status_list']=array (
  '' => '',
  'On Study' => 'On Study',
  'On Study Transferred' => 'Transferred',
  'Not Used' => 'Not Used',
  'Assigned Backup' => '(Obsolete) Assigned Backup',
  'Assigned On Study' => '(Obsolete) Assigned On Study',
  'Non naive Stock' => '(Obsolete) Non-naive Stock',
  'Enrolled Not Used' => '(Obsolete) Enrolled – Not Used',
  'On Study Backup' => '(Obsolete) On Study Backup',
  'Choose One' => '(Obsolete) Stock',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_high_level_root_cause_action_list.php

 // created: 2020-12-22 14:35:20

$app_list_strings['high_level_root_cause_action_list']=array (
  '' => '',
  'Isolated Process Error' => 'Isolated Process',
  'Isolated Human Error' => 'Isolated Human',
  'Repeat Process Error' => 'Repeat Process',
  'Repeat Human Error' => 'Repeat Human',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Service_Pricing.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Service_Pricing.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['SP_Service_Pricing'] = 'Service Pricing';
$app_list_strings['moduleListSingular']['SP_Service_Pricing'] = 'Service Pricing';
$app_list_strings['service_type_list']['Analytical'] = 'Analytical';
$app_list_strings['service_type_list']['Bioskills'] = 'Bioskills';
$app_list_strings['service_type_list']['Clinical Pathology'] = 'Clinical Pathology';
$app_list_strings['service_type_list']['Histology'] = 'Histology';
$app_list_strings['service_type_list']['ISR'] = 'ISR';
$app_list_strings['service_type_list']['IVT'] = 'IVT';
$app_list_strings['service_type_list']['LAIL'] = 'LAIL';
$app_list_strings['service_type_list']['Pathology'] = 'Pathology';
$app_list_strings['service_type_list']['Phamacology'] = 'Phamacology';
$app_list_strings['service_type_list']['QA'] = 'QA';
$app_list_strings['service_type_list']['SAIL'] = 'SAIL';
$app_list_strings['service_type_list']['Sample Prep'] = 'Sample Prep';
$app_list_strings['service_type_list']['Scientific'] = 'Scientific';
$app_list_strings['service_type_list']['Toxicology'] = 'Toxicology';
$app_list_strings['service_type_list'][''] = '';
$app_list_strings['service_subtype_list']['ACT'] = 'ACT';
$app_list_strings['service_subtype_list']['AnesthesiaFluids'] = 'Anesthesia/Fluids';
$app_list_strings['service_subtype_list']['Decalcification'] = 'Decalcification';
$app_list_strings['service_subtype_list']['Deliverable'] = 'Deliverable';
$app_list_strings['service_subtype_list']['Dosing'] = 'Dosing';
$app_list_strings['service_subtype_list']['Equipment'] = 'Equipment';
$app_list_strings['service_subtype_list']['Formulation'] = 'Formulation';
$app_list_strings['service_subtype_list']['Gross Morphometry'] = 'Gross Morphometry';
$app_list_strings['service_subtype_list']['Histology Report'] = 'Histology Report';
$app_list_strings['service_subtype_list']['Histomorphometry'] = 'Histomorphometry';
$app_list_strings['service_subtype_list']['Husbandry'] = 'Husbandry';
$app_list_strings['service_subtype_list']['Induction'] = 'Induction';
$app_list_strings['service_subtype_list']['LM Interpretation'] = 'LM Interpretation';
$app_list_strings['service_subtype_list']['Method Development'] = 'Method Development';
$app_list_strings['service_subtype_list']['Method Validation'] = 'Method Validation';
$app_list_strings['service_subtype_list']['MicroCT'] = 'MicroCT';
$app_list_strings['service_subtype_list']['Miscellaneous'] = 'Miscellaneous';
$app_list_strings['service_subtype_list']['Paraffin Embedding'] = 'Paraffin Embedding';
$app_list_strings['service_subtype_list']['Per Diem'] = 'Per Diem';
$app_list_strings['service_subtype_list']['Plastic Embedding'] = 'Plastic Embedding';
$app_list_strings['service_subtype_list']['Prep'] = 'Prep';
$app_list_strings['service_subtype_list']['Procedure Room'] = 'Procedure Room';
$app_list_strings['service_subtype_list']['Procedure Room Overhead'] = 'Procedure Room Overhead';
$app_list_strings['service_subtype_list']['Recovery'] = 'Recovery';
$app_list_strings['service_subtype_list']['Room Setup'] = 'Room Setup';
$app_list_strings['service_subtype_list']['Sample Analysis'] = 'Sample Analysis';
$app_list_strings['service_subtype_list']['Sectioning and Staining'] = 'Sectioning and Staining';
$app_list_strings['service_subtype_list']['SEM'] = 'SEM';
$app_list_strings['service_subtype_list']['SEM Interpretation'] = 'SEM Interpretation';
$app_list_strings['service_subtype_list']['Shipping'] = 'Shipping';
$app_list_strings['service_subtype_list']['Slide Scan'] = 'Slide Scan';
$app_list_strings['service_subtype_list']['Specimen Collection'] = 'Specimen Collection';
$app_list_strings['service_subtype_list']['Tissue Processing'] = 'Tissue Processing';
$app_list_strings['service_subtype_list']['Tissue Trimming'] = 'Tissue Trimming';
$app_list_strings['service_subtype_list']['Other'] = 'Other';
$app_list_strings['service_subtype_list'][''] = '';
$app_list_strings['service_pricing_roles_list']['Assistant'] = 'Assistant';
$app_list_strings['service_pricing_roles_list']['Cadaver Technician'] = 'Cadaver Technician';
$app_list_strings['service_pricing_roles_list']['Cardiologist'] = 'Cardiologist';
$app_list_strings['service_pricing_roles_list']['Chemist'] = 'Chemist';
$app_list_strings['service_pricing_roles_list']['Consulting Surgeon'] = 'Consulting Surgeon';
$app_list_strings['service_pricing_roles_list']['CT Technician'] = 'CT Technician';
$app_list_strings['service_pricing_roles_list']['Data Recorder'] = 'Data Recorder';
$app_list_strings['service_pricing_roles_list']['Equipment Technician'] = 'Equipment Technician';
$app_list_strings['service_pricing_roles_list']['Float Technician'] = 'Float Technician';
$app_list_strings['service_pricing_roles_list']['Interventionalist'] = 'Interventionalist';
$app_list_strings['service_pricing_roles_list']['Ophthalmologist'] = 'Ophthalmologist';
$app_list_strings['service_pricing_roles_list']['Pathologist'] = 'Pathologist';
$app_list_strings['service_pricing_roles_list']['Perfusionist'] = 'Perfusionist';
$app_list_strings['service_pricing_roles_list']['Prosector'] = 'Prosector';
$app_list_strings['service_pricing_roles_list']['QA Auditor'] = 'QA Auditor';
$app_list_strings['service_pricing_roles_list']['QC Specialist'] = 'QC Specialist';
$app_list_strings['service_pricing_roles_list']['Research Technician'] = 'Research Technician';
$app_list_strings['service_pricing_roles_list']['Study Director'] = 'Study Director';
$app_list_strings['service_pricing_roles_list']['Surgeon'] = 'Surgeon';
$app_list_strings['service_pricing_roles_list']['Surgical Technician'] = 'Surgical Technician';
$app_list_strings['service_pricing_roles_list']['Ultrasonographer'] = 'Ultrasonographer';
$app_list_strings['service_pricing_roles_list']['Veterinarian'] = 'Veterinarian';
$app_list_strings['service_pricing_roles_list'][''] = '';
$app_list_strings['service_format_list']['Combination'] = 'Combination';
$app_list_strings['service_format_list']['Hourly'] = 'Hourly';
$app_list_strings['service_format_list']['Task'] = 'Task';
$app_list_strings['service_format_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['SP_Service_Pricing'] = 'Service Pricing';
$app_list_strings['moduleListSingular']['SP_Service_Pricing'] = 'Service Pricing';
$app_list_strings['service_type_list']['Analytical'] = 'Analytical';
$app_list_strings['service_type_list']['Bioskills'] = 'Bioskills';
$app_list_strings['service_type_list']['Clinical Pathology'] = 'Clinical Pathology';
$app_list_strings['service_type_list']['Histology'] = 'Histology';
$app_list_strings['service_type_list']['ISR'] = 'ISR';
$app_list_strings['service_type_list']['IVT'] = 'IVT';
$app_list_strings['service_type_list']['LAIL'] = 'LAIL';
$app_list_strings['service_type_list']['Pathology'] = 'Pathology';
$app_list_strings['service_type_list']['Phamacology'] = 'Phamacology';
$app_list_strings['service_type_list']['QA'] = 'QA';
$app_list_strings['service_type_list']['SAIL'] = 'SAIL';
$app_list_strings['service_type_list']['Sample Prep'] = 'Sample Prep';
$app_list_strings['service_type_list']['Scientific'] = 'Scientific';
$app_list_strings['service_type_list']['Toxicology'] = 'Toxicology';
$app_list_strings['service_type_list'][''] = '';
$app_list_strings['service_subtype_list']['ACT'] = 'ACT';
$app_list_strings['service_subtype_list']['AnesthesiaFluids'] = 'Anesthesia/Fluids';
$app_list_strings['service_subtype_list']['Decalcification'] = 'Decalcification';
$app_list_strings['service_subtype_list']['Deliverable'] = 'Deliverable';
$app_list_strings['service_subtype_list']['Dosing'] = 'Dosing';
$app_list_strings['service_subtype_list']['Equipment'] = 'Equipment';
$app_list_strings['service_subtype_list']['Formulation'] = 'Formulation';
$app_list_strings['service_subtype_list']['Gross Morphometry'] = 'Gross Morphometry';
$app_list_strings['service_subtype_list']['Histology Report'] = 'Histology Report';
$app_list_strings['service_subtype_list']['Histomorphometry'] = 'Histomorphometry';
$app_list_strings['service_subtype_list']['Husbandry'] = 'Husbandry';
$app_list_strings['service_subtype_list']['Induction'] = 'Induction';
$app_list_strings['service_subtype_list']['LM Interpretation'] = 'LM Interpretation';
$app_list_strings['service_subtype_list']['Method Development'] = 'Method Development';
$app_list_strings['service_subtype_list']['Method Validation'] = 'Method Validation';
$app_list_strings['service_subtype_list']['MicroCT'] = 'MicroCT';
$app_list_strings['service_subtype_list']['Miscellaneous'] = 'Miscellaneous';
$app_list_strings['service_subtype_list']['Paraffin Embedding'] = 'Paraffin Embedding';
$app_list_strings['service_subtype_list']['Per Diem'] = 'Per Diem';
$app_list_strings['service_subtype_list']['Plastic Embedding'] = 'Plastic Embedding';
$app_list_strings['service_subtype_list']['Prep'] = 'Prep';
$app_list_strings['service_subtype_list']['Procedure Room'] = 'Procedure Room';
$app_list_strings['service_subtype_list']['Procedure Room Overhead'] = 'Procedure Room Overhead';
$app_list_strings['service_subtype_list']['Recovery'] = 'Recovery';
$app_list_strings['service_subtype_list']['Room Setup'] = 'Room Setup';
$app_list_strings['service_subtype_list']['Sample Analysis'] = 'Sample Analysis';
$app_list_strings['service_subtype_list']['Sectioning and Staining'] = 'Sectioning and Staining';
$app_list_strings['service_subtype_list']['SEM'] = 'SEM';
$app_list_strings['service_subtype_list']['SEM Interpretation'] = 'SEM Interpretation';
$app_list_strings['service_subtype_list']['Shipping'] = 'Shipping';
$app_list_strings['service_subtype_list']['Slide Scan'] = 'Slide Scan';
$app_list_strings['service_subtype_list']['Specimen Collection'] = 'Specimen Collection';
$app_list_strings['service_subtype_list']['Tissue Processing'] = 'Tissue Processing';
$app_list_strings['service_subtype_list']['Tissue Trimming'] = 'Tissue Trimming';
$app_list_strings['service_subtype_list']['Other'] = 'Other';
$app_list_strings['service_subtype_list'][''] = '';
$app_list_strings['service_pricing_roles_list']['Assistant'] = 'Assistant';
$app_list_strings['service_pricing_roles_list']['Cadaver Technician'] = 'Cadaver Technician';
$app_list_strings['service_pricing_roles_list']['Cardiologist'] = 'Cardiologist';
$app_list_strings['service_pricing_roles_list']['Chemist'] = 'Chemist';
$app_list_strings['service_pricing_roles_list']['Consulting Surgeon'] = 'Consulting Surgeon';
$app_list_strings['service_pricing_roles_list']['CT Technician'] = 'CT Technician';
$app_list_strings['service_pricing_roles_list']['Data Recorder'] = 'Data Recorder';
$app_list_strings['service_pricing_roles_list']['Equipment Technician'] = 'Equipment Technician';
$app_list_strings['service_pricing_roles_list']['Float Technician'] = 'Float Technician';
$app_list_strings['service_pricing_roles_list']['Interventionalist'] = 'Interventionalist';
$app_list_strings['service_pricing_roles_list']['Ophthalmologist'] = 'Ophthalmologist';
$app_list_strings['service_pricing_roles_list']['Pathologist'] = 'Pathologist';
$app_list_strings['service_pricing_roles_list']['Perfusionist'] = 'Perfusionist';
$app_list_strings['service_pricing_roles_list']['Prosector'] = 'Prosector';
$app_list_strings['service_pricing_roles_list']['QA Auditor'] = 'QA Auditor';
$app_list_strings['service_pricing_roles_list']['QC Specialist'] = 'QC Specialist';
$app_list_strings['service_pricing_roles_list']['Research Technician'] = 'Research Technician';
$app_list_strings['service_pricing_roles_list']['Study Director'] = 'Study Director';
$app_list_strings['service_pricing_roles_list']['Surgeon'] = 'Surgeon';
$app_list_strings['service_pricing_roles_list']['Surgical Technician'] = 'Surgical Technician';
$app_list_strings['service_pricing_roles_list']['Ultrasonographer'] = 'Ultrasonographer';
$app_list_strings['service_pricing_roles_list']['Veterinarian'] = 'Veterinarian';
$app_list_strings['service_pricing_roles_list'][''] = '';
$app_list_strings['service_format_list']['Combination'] = 'Combination';
$app_list_strings['service_format_list']['Hourly'] = 'Hourly';
$app_list_strings['service_format_list']['Task'] = 'Task';
$app_list_strings['service_format_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_species_sp_list.php

 // created: 2021-01-12 11:35:48

$app_list_strings['species_sp_list']=array (
  '' => '',
  'Bovine' => 'Bovine',
  'Canine' => 'Canine',
  'Caprine' => 'Caprine',
  'Guinea Pig' => 'Guinea Pig',
  'Hamster' => 'Hamster',
  'Lagomorph' => 'Lagomorph',
  'Murine' => 'Murine',
  'Ovine' => 'Ovine',
  'Porcine' => 'Porcine',
  'Rat' => 'Rat',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_error_classification_c_list.php

 // created: 2021-01-19 15:15:04

$app_list_strings['error_classification_c_list']=array (
  '' => '',
  'Adverse Event' => '(Obsolete) Adverse Event',
  'Error' => 'Deviation',
  'Duplicate' => 'Duplicate',
  'Job well done' => 'Job well done',
  'Notification' => 'Notification',
  'Observation' => 'Observation (Obsoleted 12/2/19)',
  'Opportunity for Improvement' => 'Opportunity for Improvement',
  'Pending' => 'Pending',
  'Impactful Deviation' => 'Impactful Deviation (Obsolete 11/22/19)',
  'Non Impactful Deviation' => 'Reportable Deviation (Obsoleted 03/29/19)',
  'Unforeseen Circumstance Unanticipated Response' => 'Unforeseen Circumstance/Unanticipated Response (UCUR) (Obsoleted 05/20/19)',
  'Withdrawn Error' => 'Withdrawn Deviation (Obsoleted 03/29/19)',
  'A' => 'A',
  'M' => 'M',
  'S' => 'S',
  'C' => 'C',
  'NA' => 'NA',
  'Major Change or New' => '(Obsolete) Employee performed activities under this Controlled Document while overdue; Controlled Document Revision = Major Change or New',
  'Revision Change or Minor Change' => '(Obsolete) Employee performed activities under this Controlled Document while overdue; Controlled Document Revision = Revision Change Only or Minor Change',
  'Employee did NOT perform SOP tasks while overdue' => '(Obsolete) Employee did NOT undertake activities under this Controlled Document while overdue',
  'The Controlled Document is a standalone document. No Controlled Document activities are associated' => '(Obsolete) The Controlled Document is a standalone document. No Controlled Document activities are associated with the document.',
  'Overdue refresher training' => '(Obsolete) Employee performed activities under this Controlled Document when their Refresher Training was overdue',
  'Adverse Event Study Article Related' => 'Adverse Event (Study Article Related)',
  'Adverse Event Procedure Related' => 'Adverse Event (Procedure Related)',
  'Adverse Event Test System Model Related' => 'Adverse Event (Test System Model Related)',
  'Adverse Event Etiology Unknown' => 'Adverse Event (Etiology Unknown)',
  'Addressed CPI COM' => 'Addressed CPI COM',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_iacuc_deficiency_class_list.php

 // created: 2021-01-22 14:26:42

$app_list_strings['iacuc_deficiency_class_list']=array (
  '' => '',
  'NA' => 'NA',
  'A' => 'A (Acceptable)',
  'M' => 'M (Minor)',
  'S' => 'S (Significant)',
  'C' => 'C (Change in Program)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_service_format_list.php

 // created: 2021-01-28 09:11:22

$app_list_strings['service_format_list']=array (
  '' => '',
  'Combination' => 'Combination',
  'Each' => 'Each',
  'Hourly' => 'Hourly',
  'Task' => 'Task',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_service_type_list.php

 // created: 2021-01-28 15:02:15

$app_list_strings['service_type_list']=array (
  '' => '',
  'Analytical' => 'Analytical',
  'Bioskills' => 'Bioskills',
  'Clinical Pathology' => 'Clinical Pathology',
  'Histology' => 'Histology',
  'In Life Service' => 'In Life Service',
  'ISR' => 'ISR',
  'IVT' => 'IVT',
  'LAIL' => 'LAIL',
  'Operations' => 'Operations',
  'Pathology' => 'Pathology',
  'Phamacology' => 'Phamacology',
  'QA' => 'QA',
  'SAIL' => 'SAIL',
  'Sample Prep' => 'Sample Prep',
  'Scientific' => 'Scientific',
  'Standard Biocompatibility' => 'Standard Biocompatibility',
  'Toxicology' => 'Toxicology',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Controlled_Document_QA_Reviews.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['QARev_CD_QA_Reviews'] = 'Controlled Document QA Reviews';
$app_list_strings['moduleListSingular']['QARev_CD_QA_Reviews'] = 'Controlled Document QA Review';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Controlled_Documents_QA_Review_Docs.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Controlled_Documents_QA_Review_Docs.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Controlled_Documents_QA_Review_Docs.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Sales';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Active';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Draft';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Expired';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Sales';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Active';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Draft';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Expired';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Sales';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Active';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Draft';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Expired';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Test_System_Design_1.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['TSD1_Test_System_Design_1'] = 'Test System Designs';
$app_list_strings['moduleListSingular']['TSD1_Test_System_Design_1'] = 'Test System Design';
$app_list_strings['number_list']['01'] = '01';
$app_list_strings['number_list']['02'] = '02';
$app_list_strings['number_list']['03'] = '03';
$app_list_strings['number_list']['04'] = '04';
$app_list_strings['number_list']['05'] = '05';
$app_list_strings['number_list']['06'] = '06';
$app_list_strings['number_list']['07'] = '07';
$app_list_strings['number_list']['08'] = '08';
$app_list_strings['number_list']['09'] = '09';
$app_list_strings['number_list'][10] = '10';
$app_list_strings['number_list'][''] = '';
$app_list_strings['Breed_List']['Alpine'] = 'Alpine';
$app_list_strings['Breed_List']['Athymic Nude'] = 'Athymic Nude';
$app_list_strings['Breed_List']['BALBc'] = 'BALB/c';
$app_list_strings['Breed_List']['Beagle'] = 'Beagle';
$app_list_strings['Breed_List']['C3H HeJ'] = 'C3H/HeJ';
$app_list_strings['Breed_List']['C57'] = 'C57';
$app_list_strings['Breed_List']['CD Hairless'] = 'CD Hairless';
$app_list_strings['Breed_List']['CD1'] = 'CD1';
$app_list_strings['Breed_List']['CF1'] = 'CF1';
$app_list_strings['Breed_List']['Cross Breed'] = 'Cross Breed';
$app_list_strings['Breed_List']['Dorset'] = 'Dorset';
$app_list_strings['Breed_List']['Dutch Belted'] = 'Dutch Belted';
$app_list_strings['Breed_List']['Freisen'] = 'Freisen';
$app_list_strings['Breed_List']['Golden Syrian'] = 'Golden Syrian';
$app_list_strings['Breed_List']['Gottingen'] = 'Gottingen';
$app_list_strings['Breed_List']['Hanford'] = 'Hanford';
$app_list_strings['Breed_List']['Hartley'] = 'Hartley';
$app_list_strings['Breed_List']['Holstein'] = 'Holstein';
$app_list_strings['Breed_List']['Jersey'] = 'Jersey';
$app_list_strings['Breed_List']['Lamancha'] = 'Lamancha';
$app_list_strings['Breed_List']['LDLR'] = 'LDLR Yucatan';
$app_list_strings['Breed_List']['Micro Yucatan'] = 'Micro-Yucatan';
$app_list_strings['Breed_List']['Mongrel'] = 'Mongrel';
$app_list_strings['Breed_List']['Myotonic'] = 'Myotonic';
$app_list_strings['Breed_List']['ND4'] = 'ND4';
$app_list_strings['Breed_List']['New Zealand White'] = 'New Zealand White';
$app_list_strings['Breed_List']['Ossabaw'] = 'Ossabaw';
$app_list_strings['Breed_List']['Polypay'] = 'Polypay';
$app_list_strings['Breed_List']['Saanen'] = 'Saanen';
$app_list_strings['Breed_List']['Sinclair'] = 'Sinclair';
$app_list_strings['Breed_List']['SKH1 Hairless'] = 'SKH1 Hairless';
$app_list_strings['Breed_List']['Sprague Dawley'] = 'Sprague Dawley';
$app_list_strings['Breed_List']['Suffolk'] = 'Suffolk';
$app_list_strings['Breed_List']['Suffolk X'] = 'Suffolk X';
$app_list_strings['Breed_List']['Toggenburg'] = 'Toggenburg';
$app_list_strings['Breed_List']['Watanabe'] = 'Watanabe';
$app_list_strings['Breed_List']['Wistar'] = 'Wistar';
$app_list_strings['Breed_List']['Yorkshire X'] = 'Yorkshire X';
$app_list_strings['Breed_List']['Yucatan'] = 'Yucatan';
$app_list_strings['Breed_List'][''] = '';
$app_list_strings['strain_list']['E coli WP2 uvrA'] = 'E.coli WP2-uvrA';
$app_list_strings['strain_list']['Salmonella TA97a'] = 'Salmonella- TA97a';
$app_list_strings['strain_list']['Salmonella TA98'] = 'Salmonella- TA98';
$app_list_strings['strain_list']['Salmonella TA100'] = 'Salmonella- TA100';
$app_list_strings['strain_list']['Salmonella TA1535'] = 'Salmonella- TA1535';
$app_list_strings['strain_list']['Salmonella TA102'] = 'Salmonella- TA102';
$app_list_strings['strain_list']['Salmonella TA1537'] = 'Salmonella- TA1537';
$app_list_strings['strain_list'][''] = '';
$app_list_strings['tsd_sex_list']['M F CM'] = 'M, F, CM';
$app_list_strings['tsd_sex_list']['CM or F'] = 'CM or F';
$app_list_strings['tsd_sex_list']['M or F'] = 'M or F';
$app_list_strings['tsd_sex_list']['M'] = 'M';
$app_list_strings['tsd_sex_list']['F'] = 'F';
$app_list_strings['tsd_sex_list']['CM'] = 'CM';
$app_list_strings['tsd_sex_list']['CM or M'] = 'CM or M';
$app_list_strings['tsd_sex_list']['Equal MF'] = 'Equal M/F';
$app_list_strings['tsd_sex_list']['Equal MFCM'] = 'Equal M/F/CM';
$app_list_strings['tsd_sex_list']['Equal CMF'] = 'Equal CM/F';
$app_list_strings['tsd_sex_list']['Equal CMM'] = 'Equal CM/M';
$app_list_strings['tsd_sex_list']['Qty Specific Required'] = 'Qty. Specific Required';
$app_list_strings['tsd_sex_list'][''] = '';
$app_list_strings['tsd1_sex_list']['CM'] = 'CM';
$app_list_strings['tsd1_sex_list']['CM or F'] = 'CM or F';
$app_list_strings['tsd1_sex_list']['CM or M'] = 'CM or M';
$app_list_strings['tsd1_sex_list']['Equal CMF'] = 'Equal CM/F';
$app_list_strings['tsd1_sex_list']['Equal CMM'] = 'Equal CM/M';
$app_list_strings['tsd1_sex_list']['Equal MF'] = 'Equal M/F';
$app_list_strings['tsd1_sex_list']['Equal MFCM'] = 'Equal M/F/CM';
$app_list_strings['tsd1_sex_list']['F'] = 'F';
$app_list_strings['tsd1_sex_list']['M'] = 'M';
$app_list_strings['tsd1_sex_list']['M or F'] = 'M or F';
$app_list_strings['tsd1_sex_list']['M F CM'] = 'M, F, CM';
$app_list_strings['tsd1_sex_list']['Quantity Specific'] = 'Quantity Specific';
$app_list_strings['tsd1_sex_list'][''] = '';
$app_list_strings['animal_source_type_list']['Multiple Acceptable'] = 'Multiple Acceptable';
$app_list_strings['animal_source_type_list']['Single'] = 'Single';
$app_list_strings['animal_source_type_list']['Specific'] = 'Specific';
$app_list_strings['animal_source_type_list'][''] = '';
$app_list_strings['min_age_type_list']['Actual'] = 'Actual';
$app_list_strings['min_age_type_list']['2 months'] = '> 2 months';
$app_list_strings['min_age_type_list']['9 months'] = '> 9 months';
$app_list_strings['min_age_type_list']['Age appropriate to weight'] = 'Age appropriate to weight';
$app_list_strings['min_age_type_list'][''] = '';
$app_list_strings['max_age_type_list']['Actual'] = 'Actual';
$app_list_strings['max_age_type_list']['36 months'] = '36 months';
$app_list_strings['max_age_type_list']['Age appropriate to weight'] = 'Age appropriate to weight';
$app_list_strings['max_age_type_list'][''] = '';
$app_list_strings['tsd_wt_type_list']['Actual'] = 'Actual';
$app_list_strings['tsd_wt_type_list']['Actual or having suitable anatomy'] = 'Actual or having suitable anatomy';
$app_list_strings['tsd_wt_type_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_stability_considerations_list.php

 // created: 2021-02-12 12:47:12

$app_list_strings['stability_considerations_list']=array (
  '' => '',
  'NA' => 'NA',
  'No' => 'No',
  'Yes' => 'Yes',
  'Unknown' => 'Unknown',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Historical_USDA_ID.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['USDA_Historical_USDA_ID'] = 'Historical USDA IDs';
$app_list_strings['moduleListSingular']['USDA_Historical_USDA_ID'] = 'Historical USDA ID';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Account_Number.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['AN_Account_Number'] = 'Account Numbers';
$app_list_strings['moduleListSingular']['AN_Account_Number'] = 'Account Number';
$app_list_strings['account_type_list']['Ship to'] = 'Ship to';
$app_list_strings['account_type_list']['Order'] = 'Order';
$app_list_strings['account_type_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_wps_outcome_wpe_activities_list.php

 // created: 2021-02-17 16:56:25

$app_list_strings['wps_outcome_wpe_activities_list']=array (
  '' => '',
  'Invasive Procedure' => 'Invasive Procedure',
  'Study Article Exposure' => 'Study Article Exposure',
  'Study Specific Data Recorded' => 'Study Specific Data Recorded',
  'Other Activities' => 'Other Activities',
  'Performed Per Protocol' => 'Performed Per Protocol',
  'Preventative Health Care' => 'Preventative Health Care',
  'Vet Order' => 'Vet Order',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_wps_outcome_activities_list.php

 // created: 2021-02-17 16:54:48

$app_list_strings['wps_outcome_activities_list']=array (
  '' => '',
  'Invasive Procedure' => 'Invasive Procedure',
  'Study Article Exposure' => 'Study Article Exposure',
  'Study Specific Data Recorded' => 'Study Specific Data Recorded',
  'Only Other Protocol Activities' => 'Other Protocol Activities',
  'Performed Per Protocol' => 'Performed Per Protocol',
  'Preventative Health Care' => 'Preventative Health Care',
  'Vet Order' => 'Vet Order',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_paraffin_plastic_list.php

 // created: 2021-04-06 09:19:12

$app_list_strings['paraffin_plastic_list']=array (
  '' => '',
  'Paraffin' => 'Paraffin',
  'Plastic' => 'Plastic',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Work_Product_Group.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['WPG_Work_Product_Group'] = 'Work Product Groups';
$app_list_strings['moduleListSingular']['WPG_Work_Product_Group'] = 'Work Product Group';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Species_Census.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['SC_Species_Census'] = 'Species Census';
$app_list_strings['moduleListSingular']['SC_Species_Census'] = 'Species Census';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_moduleListSingular.php

// created: 2021-05-27 08:43:27
$app_list_strings['moduleListSingular']['Home'] = 'Home';
$app_list_strings['moduleListSingular']['Dashboards'] = 'Dashboard';
$app_list_strings['moduleListSingular']['Styleguide'] = 'Styleguide';
$app_list_strings['moduleListSingular']['Contacts'] = 'Contact';
$app_list_strings['moduleListSingular']['Accounts'] = 'Company';
$app_list_strings['moduleListSingular']['Opportunities'] = 'Opportunity';
$app_list_strings['moduleListSingular']['Cases'] = 'Case';
$app_list_strings['moduleListSingular']['Notes'] = 'Note';
$app_list_strings['moduleListSingular']['Calls'] = 'Call';
$app_list_strings['moduleListSingular']['Emails'] = 'Email';
$app_list_strings['moduleListSingular']['Meetings'] = 'Communication (Sugar)';
$app_list_strings['moduleListSingular']['Tasks'] = 'Task';
$app_list_strings['moduleListSingular']['Calendar'] = 'Calendar';
$app_list_strings['moduleListSingular']['Leads'] = 'Lead';
$app_list_strings['moduleListSingular']['Manufacturers'] = 'Manufacturer';
$app_list_strings['moduleListSingular']['VisualPipeline'] = 'VisualPipeline';
$app_list_strings['moduleListSingular']['ConsoleConfiguration'] = 'ConsoleConfiguration';
$app_list_strings['moduleListSingular']['MobileDevices'] = 'MobileDevice';
$app_list_strings['moduleListSingular']['SugarLive'] = 'SugarLive';
$app_list_strings['moduleListSingular']['Contracts'] = 'Contract';
$app_list_strings['moduleListSingular']['Quotes'] = 'Quote';
$app_list_strings['moduleListSingular']['Products'] = 'Quoted Line Item';
$app_list_strings['moduleListSingular']['ProductCategories'] = 'Product Category';
$app_list_strings['moduleListSingular']['ProductBundles'] = 'Product Bundle';
$app_list_strings['moduleListSingular']['ProductBundleNotes'] = 'Product Bundle Note';
$app_list_strings['moduleListSingular']['RevenueLineItems'] = 'Revenue Line Item';
$app_list_strings['moduleListSingular']['WebLogicHooks'] = 'Web Logic Hook';
$app_list_strings['moduleListSingular']['Reports'] = 'Report';
$app_list_strings['moduleListSingular']['Forecasts'] = 'Forecast';
$app_list_strings['moduleListSingular']['ForecastWorksheets'] = 'Forecast Worksheet';
$app_list_strings['moduleListSingular']['ForecastManagerWorksheets'] = 'Forecast Manager Worksheet';
$app_list_strings['moduleListSingular']['Quotas'] = 'Quota';
$app_list_strings['moduleListSingular']['MergeRecords'] = 'Merge Record';
$app_list_strings['moduleListSingular']['Teams'] = 'Team';
$app_list_strings['moduleListSingular']['TeamNotices'] = 'Team Notice';
$app_list_strings['moduleListSingular']['Activities'] = 'Activity';
$app_list_strings['moduleListSingular']['ActivityStream'] = 'Activity Stream';
$app_list_strings['moduleListSingular']['Bugs'] = 'Bug';
$app_list_strings['moduleListSingular']['Feeds'] = 'RSS';
$app_list_strings['moduleListSingular']['iFrames'] = 'My Sites';
$app_list_strings['moduleListSingular']['TimePeriods'] = 'Time Period';
$app_list_strings['moduleListSingular']['TaxRates'] = 'Tax Rate';
$app_list_strings['moduleListSingular']['ContractTypes'] = 'Contract Type';
$app_list_strings['moduleListSingular']['Schedulers'] = 'Scheduler';
$app_list_strings['moduleListSingular']['Campaigns'] = 'Campaign';
$app_list_strings['moduleListSingular']['CampaignLog'] = 'Campaign Log';
$app_list_strings['moduleListSingular']['Project'] = 'Project';
$app_list_strings['moduleListSingular']['ProjectTask'] = 'Project Task';
$app_list_strings['moduleListSingular']['Prospects'] = 'Target';
$app_list_strings['moduleListSingular']['ProspectLists'] = 'Target List';
$app_list_strings['moduleListSingular']['CampaignTrackers'] = 'Campaign Tracker';
$app_list_strings['moduleListSingular']['Documents'] = 'Document';
$app_list_strings['moduleListSingular']['DocumentRevisions'] = 'Document Revision';
$app_list_strings['moduleListSingular']['Connectors'] = 'Connector';
$app_list_strings['moduleListSingular']['Roles'] = 'Role';
$app_list_strings['moduleListSingular']['Notifications'] = 'Notification';
$app_list_strings['moduleListSingular']['Sync'] = 'Sync';
$app_list_strings['moduleListSingular']['PdfManager'] = 'PDF Manager';
$app_list_strings['moduleListSingular']['DataArchiver'] = 'Data Archiver';
$app_list_strings['moduleListSingular']['ArchiveRuns'] = 'Archive Runs';
$app_list_strings['moduleListSingular']['ReportMaker'] = ' Advanced Report';
$app_list_strings['moduleListSingular']['DataSets'] = 'Data Format';
$app_list_strings['moduleListSingular']['CustomQueries'] = 'Custom Query';
$app_list_strings['moduleListSingular']['pmse_Inbox'] = 'Process';
$app_list_strings['moduleListSingular']['pmse_Project'] = 'Process Definition';
$app_list_strings['moduleListSingular']['pmse_Business_Rules'] = 'Process Business Rule';
$app_list_strings['moduleListSingular']['pmse_Emails_Templates'] = 'Process Email Template';
$app_list_strings['moduleListSingular']['BusinessCenters'] = 'Business Center';
$app_list_strings['moduleListSingular']['Shifts'] = 'Shift';
$app_list_strings['moduleListSingular']['ShiftExceptions'] = 'Shift Exceptions';
$app_list_strings['moduleListSingular']['Purchases'] = 'Purchase';
$app_list_strings['moduleListSingular']['PurchasedLineItems'] = 'Purchased Line Item';
$app_list_strings['moduleListSingular']['PushNotifications'] = 'PushNotification';
$app_list_strings['moduleListSingular']['WorkFlow'] = 'Workflow';
$app_list_strings['moduleListSingular']['EAPM'] = 'External Account';
$app_list_strings['moduleListSingular']['Worksheet'] = 'Worksheet';
$app_list_strings['moduleListSingular']['Users'] = 'User';
$app_list_strings['moduleListSingular']['SugarFavorites'] = 'SugarFavorites';
$app_list_strings['moduleListSingular']['Employees'] = 'Employee';
$app_list_strings['moduleListSingular']['Administration'] = 'Administration';
$app_list_strings['moduleListSingular']['ACLRoles'] = 'Role';
$app_list_strings['moduleListSingular']['InboundEmail'] = 'Inbound Email';
$app_list_strings['moduleListSingular']['Releases'] = 'Release';
$app_list_strings['moduleListSingular']['Queues'] = 'Queue';
$app_list_strings['moduleListSingular']['EmailMarketing'] = 'Email Marketing';
$app_list_strings['moduleListSingular']['EmailTemplates'] = 'Email Template';
$app_list_strings['moduleListSingular']['SNIP'] = 'Email Archiving';
$app_list_strings['moduleListSingular']['SavedSearch'] = 'Saved Search';
$app_list_strings['moduleListSingular']['UpgradeWizard'] = 'Upgrade Wizard';
$app_list_strings['moduleListSingular']['Trackers'] = 'Tracker';
$app_list_strings['moduleListSingular']['TrackerPerfs'] = 'Tracker Performance';
$app_list_strings['moduleListSingular']['TrackerSessions'] = 'Tracker Session';
$app_list_strings['moduleListSingular']['TrackerQueries'] = 'Tracker Query';
$app_list_strings['moduleListSingular']['FAQ'] = 'FAQ';
$app_list_strings['moduleListSingular']['Newsletters'] = 'Newsletter';
$app_list_strings['moduleListSingular']['OAuthKeys'] = 'OAuth Consumer Key';
$app_list_strings['moduleListSingular']['OAuthTokens'] = 'OAuth Token';
$app_list_strings['moduleListSingular']['Filters'] = 'Filter';
$app_list_strings['moduleListSingular']['Comments'] = 'Comment';
$app_list_strings['moduleListSingular']['CommentLog'] = 'Comment Log';
$app_list_strings['moduleListSingular']['Currencies'] = 'Currency';
$app_list_strings['moduleListSingular']['ProductTemplates'] = 'Product';
$app_list_strings['moduleListSingular']['ProductTypes'] = 'Product Type';
$app_list_strings['moduleListSingular']['Shippers'] = 'Shipping Provider';
$app_list_strings['moduleListSingular']['Subscriptions'] = 'Subscription';
$app_list_strings['moduleListSingular']['UserSignatures'] = 'Email Signature';
$app_list_strings['moduleListSingular']['Feedbacks'] = 'Feedback';
$app_list_strings['moduleListSingular']['Tags'] = 'Tag';
$app_list_strings['moduleListSingular']['Categories'] = 'Category';
$app_list_strings['moduleListSingular']['OutboundEmail'] = 'Email Setting';
$app_list_strings['moduleListSingular']['EmailParticipants'] = 'Email Participant';
$app_list_strings['moduleListSingular']['DataPrivacy'] = 'Data Privacy';
$app_list_strings['moduleListSingular']['ReportSchedules'] = 'Report Schedule';
$app_list_strings['moduleListSingular']['Holidays'] = 'Holiday';
$app_list_strings['moduleListSingular']['ChangeTimers'] = 'Change Timer';
$app_list_strings['moduleListSingular']['Messages'] = 'Message';
$app_list_strings['moduleListSingular']['Library'] = 'Library';
$app_list_strings['moduleListSingular']['EmailAddresses'] = 'Email Address';
$app_list_strings['moduleListSingular']['Words'] = 'Word';
$app_list_strings['moduleListSingular']['Sugar_Favorites'] = 'Favorite';
$app_list_strings['moduleListSingular']['KBDocuments'] = 'Knowledge Base Document';
$app_list_strings['moduleListSingular']['KBContents'] = 'Knowledge Base Article';
$app_list_strings['moduleListSingular']['KBArticles'] = 'Knowledge Base Article';
$app_list_strings['moduleListSingular']['KBContentTemplates'] = 'Knowledge Base Template';
$app_list_strings['moduleListSingular']['EmbeddedFiles'] = 'Embedded File';
$app_list_strings['moduleListSingular']['M02_SA_Division_Department_'] = 'SA Division Department';
$app_list_strings['moduleListSingular']['M03_Work_Product_IDs'] = 'Additional WP ID';
$app_list_strings['moduleListSingular']['M03_Work_Product_Personnel'] = 'Additional WP Person';
$app_list_strings['moduleListSingular']['m06_APS_Communication'] = 'APS Communication';
$app_list_strings['moduleListSingular']['M01_SA_Division_Department'] = 'SA_Division_Department';
$app_list_strings['moduleListSingular']['m06_Note_To_File'] = 'Note_To_File';
$app_list_strings['moduleListSingular']['M01_Sales_Activity_Quote'] = 'Sales Activity Quote';
$app_list_strings['moduleListSingular']['M01_Sales'] = 'Sale';
$app_list_strings['moduleListSingular']['M01_Sales_Activity'] = 'Sales Activity';
$app_list_strings['moduleListSingular']['M01_Quote_Document'] = 'Quote Document';
$app_list_strings['moduleListSingular']['ABC12_Work_Product_Activities'] = 'Work Product Activities';
$app_list_strings['moduleListSingular']['WPP_Work_Product_Personnel'] = 'Work Product Person';
$app_list_strings['moduleListSingular']['WPP_WPP2'] = 'Work_Product_Personnel';
$app_list_strings['moduleListSingular']['M03_Work_Product'] = 'Work Product';
$app_list_strings['moduleListSingular']['m06_Communication'] = 'APS Communication';
$app_list_strings['moduleListSingular']['M03_Work_Product_Deliverable'] = 'Work Product Deliverables';
$app_list_strings['moduleListSingular']['M03_Work_Product_Facility'] = 'Work Product Facility';
$app_list_strings['moduleListSingular']['M03_work_product_code'] = 'Work Product Code';
$app_list_strings['moduleListSingular']['M03_Additional_WP_Personnel'] = 'Additional WP Person';
$app_list_strings['moduleListSingular']['M03_Additional_WP_IDs'] = 'Additional WP ID';
$app_list_strings['moduleListSingular']['M03_Work_Product_Code'] = 'Work Product Code';
$app_list_strings['moduleListSingular']['M01_Quote'] = 'Sales Activity Quote';
$app_list_strings['moduleListSingular']['M01_Sale'] = 'Sales Activity';
$app_list_strings['moduleListSingular']['M01_SA_Division_Department_'] = 'SA_Division_Department';
$app_list_strings['moduleListSingular']['M06_Error'] = 'Communication';
$app_list_strings['moduleListSingular']['AN01_Activity_Notes'] = 'Activity Note';
$app_list_strings['moduleListSingular']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['moduleListSingular']['A1A_CPI_Findings'] = 'CPI Findings';
$app_list_strings['moduleListSingular']['HR01_Resumes'] = 'Resume';
$app_list_strings['moduleListSingular']['idoc_documents'] = 'Flexidocs Document';
$app_list_strings['moduleListSingular']['idoc_signers'] = 'Flexidocs Signer';
$app_list_strings['moduleListSingular']['idoc_events'] = 'Flexidocs Event';
$app_list_strings['moduleListSingular']['idoc_doc_events'] = 'Event';
$app_list_strings['moduleListSingular']['idoc_document_templates'] = 'Document Template';
$app_list_strings['moduleListSingular']['idoc_templates'] = 'Flexidocs Template';
$app_list_strings['moduleListSingular']['ANML_Animals'] = 'Test System';
$app_list_strings['moduleListSingular']['WPE_Work_Product_Enrollment'] = 'Work Product Assignment';
$app_list_strings['moduleListSingular']['TM_Tradeshow_Management'] = 'Tradeshow_Management';
$app_list_strings['moduleListSingular']['VM01_Visitor_Management'] = 'Visitor Management';
$app_list_strings['moduleListSingular']['Equip_Equipment'] = 'Equipment';
$app_list_strings['moduleListSingular']['Ere_Error_Employees'] = 'Error Employee';
$app_list_strings['moduleListSingular']['Erd_Error_Documents'] = 'Error Documents';
$app_list_strings['moduleListSingular']['M99_Protocol_Amendments'] = 'Protocol Amendment';
$app_list_strings['moduleListSingular']['AC01_Acquired_Companies'] = 'Acquired Company';
$app_list_strings['moduleListSingular']['ED_Errors_Department'] = 'Errors Department';
$app_list_strings['moduleListSingular']['TA_Tradeshow_Activities'] = 'Tradeshow Activity';
$app_list_strings['moduleListSingular']['AD_Anatomy_Database'] = 'Anatomy Database';
$app_list_strings['moduleListSingular']['ErQC_Error_QC_Employees'] = 'Error QC Employee';
$app_list_strings['moduleListSingular']['DE_Deviation_Employees'] = 'Responsible Personnel';
$app_list_strings['moduleListSingular']['SW_Study_Workflow'] = 'Study Workflow';
$app_list_strings['moduleListSingular']['CD_Company_Documents'] = 'Company Document';
$app_list_strings['moduleListSingular']['TD_Tradeshow_Documents'] = 'Tradeshow Document';
$app_list_strings['moduleListSingular']['RMS_Room'] = 'Room';
$app_list_strings['moduleListSingular']['RT_Room_Transfer'] = 'Room Transfer';
$app_list_strings['moduleListSingular']['EFR_Equipment_Facility_Recor'] = 'Equipment &amp; Facility Record';
$app_list_strings['moduleListSingular']['EFS_Equipment_Facility_Servi'] = 'Equipment &amp; Facility Service';
$app_list_strings['moduleListSingular']['EFD_Equipment_Facility_Doc'] = 'Equipment &amp; Facility Document';
$app_list_strings['moduleListSingular']['CA_Company_Address'] = 'Company Address';
$app_list_strings['moduleListSingular']['RR_Regulatory_Response'] = 'Regulatory Response';
$app_list_strings['moduleListSingular']['RRD_Regulatory_Response_Doc'] = 'Regulatory Response Document';
$app_list_strings['moduleListSingular']['MAJ_Contact_Documents'] = 'Contact Document';
$app_list_strings['moduleListSingular']['EDoc_Email_Documents'] = 'Email Document';
$app_list_strings['moduleListSingular']['ET_Email_Template'] = 'Email Template';
$app_list_strings['moduleListSingular']['CN_Company_Name'] = 'Company Name';
$app_list_strings['moduleListSingular']['TSdoc_Test_System_Documents'] = 'Test System Document';
$app_list_strings['moduleListSingular']['U_Units'] = 'Unit';
$app_list_strings['moduleListSingular']['S_Species'] = 'Species';
$app_list_strings['moduleListSingular']['W_Weight'] = 'Weight';
$app_list_strings['moduleListSingular']['CO_Clinical_Observation'] = 'Clinical Observation';
$app_list_strings['moduleListSingular']['CIE_Clinical_Issue_Exam'] = 'Clinical Issue &amp; Exam';
$app_list_strings['moduleListSingular']['SV_Service_Vendor'] = 'Service Vendor';
$app_list_strings['moduleListSingular']['ops_Backups'] = 'Backup';
$app_list_strings['moduleListSingular']['GE_GDP_Examples'] = 'GDP Example';
$app_list_strings['moduleListSingular']['ProDo_Product_Document'] = 'Product Document';
$app_list_strings['moduleListSingular']['Prod_Product'] = 'Product';
$app_list_strings['moduleListSingular']['SP_Service_Pricing'] = 'Service Pricing';
$app_list_strings['moduleListSingular']['QARev_CD_QA_Reviews'] = 'Controlled Document QA Review';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';
$app_list_strings['moduleListSingular']['TSD1_Test_System_Design_1'] = 'Test System Design';
$app_list_strings['moduleListSingular']['USDA_Historical_USDA_ID'] = 'Historical USDA ID';
$app_list_strings['moduleListSingular']['AN_Account_Number'] = 'Account Number';
$app_list_strings['moduleListSingular']['WPG_Work_Product_Group'] = 'Work Product Group';
$app_list_strings['moduleListSingular']['SC_Species_Census'] = 'Species Census';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Controlled_Document_Utilization.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['CDU_CD_Utilization'] = 'Controlled Document Utilizations';
$app_list_strings['moduleListSingular']['CDU_CD_Utilization'] = 'Controlled Document Utilizations';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_moduleList.php

// created: 2021-06-29 08:08:14
$app_list_strings['moduleList']['Home'] = 'Home';
$app_list_strings['moduleList']['Contacts'] = 'Contacts';
$app_list_strings['moduleList']['Accounts'] = 'Companies';
$app_list_strings['moduleList']['Opportunities'] = 'Opportunities';
$app_list_strings['moduleList']['Cases'] = 'Cases';
$app_list_strings['moduleList']['Notes'] = 'Notes';
$app_list_strings['moduleList']['Calls'] = 'Calls';
$app_list_strings['moduleList']['Emails'] = 'Emails';
$app_list_strings['moduleList']['Meetings'] = 'Communications (Sugar)';
$app_list_strings['moduleList']['Tasks'] = 'Tasks';
$app_list_strings['moduleList']['Calendar'] = 'Calendar';
$app_list_strings['moduleList']['Leads'] = 'Leads';
$app_list_strings['moduleList']['Currencies'] = 'Currencies';
$app_list_strings['moduleList']['Contracts'] = 'Contracts';
$app_list_strings['moduleList']['Quotes'] = 'Quotes';
$app_list_strings['moduleList']['Products'] = 'Quoted Line Items';
$app_list_strings['moduleList']['WebLogicHooks'] = 'Web Logic Hooks';
$app_list_strings['moduleList']['ProductCategories'] = 'Product Categories';
$app_list_strings['moduleList']['ProductTypes'] = 'Product Types';
$app_list_strings['moduleList']['ProductTemplates'] = 'Product Catalog';
$app_list_strings['moduleList']['ProductBundles'] = 'Product Bundles';
$app_list_strings['moduleList']['ProductBundleNotes'] = 'Product Bundle Notes';
$app_list_strings['moduleList']['Reports'] = 'Reports';
$app_list_strings['moduleList']['Forecasts'] = 'Forecasts';
$app_list_strings['moduleList']['ForecastWorksheets'] = 'Forecast Worksheets';
$app_list_strings['moduleList']['ForecastManagerWorksheets'] = 'Forecast Manager Worksheets';
$app_list_strings['moduleList']['MergeRecords'] = 'Merge Records';
$app_list_strings['moduleList']['VisualPipeline'] = 'Visual Pipeline';
$app_list_strings['moduleList']['ConsoleConfiguration'] = 'Console Configuration';
$app_list_strings['moduleList']['SugarLive'] = 'SugarLive';
$app_list_strings['moduleList']['Quotas'] = 'Quotas';
$app_list_strings['moduleList']['Teams'] = 'Teams';
$app_list_strings['moduleList']['TeamNotices'] = 'Team Notices';
$app_list_strings['moduleList']['Manufacturers'] = 'Manufacturers';
$app_list_strings['moduleList']['Activities'] = 'Activities';
$app_list_strings['moduleList']['Comments'] = 'Comments';
$app_list_strings['moduleList']['Subscriptions'] = 'Subscriptions';
$app_list_strings['moduleList']['Bugs'] = 'Bugs';
$app_list_strings['moduleList']['Feeds'] = 'RSS';
$app_list_strings['moduleList']['iFrames'] = 'My Sites';
$app_list_strings['moduleList']['TimePeriods'] = 'Time Periods';
$app_list_strings['moduleList']['TaxRates'] = 'Tax Rates';
$app_list_strings['moduleList']['ContractTypes'] = 'Contract Types';
$app_list_strings['moduleList']['Schedulers'] = 'Schedulers';
$app_list_strings['moduleList']['Project'] = 'Projects';
$app_list_strings['moduleList']['ProjectTask'] = 'Project Tasks';
$app_list_strings['moduleList']['Campaigns'] = 'Campaigns';
$app_list_strings['moduleList']['CampaignLog'] = 'Campaign Log';
$app_list_strings['moduleList']['CampaignTrackers'] = 'Campaign Trackers';
$app_list_strings['moduleList']['Documents'] = 'Documents';
$app_list_strings['moduleList']['DocumentRevisions'] = 'Document Revisions';
$app_list_strings['moduleList']['Connectors'] = 'Connectors';
$app_list_strings['moduleList']['Roles'] = 'Roles';
$app_list_strings['moduleList']['Notifications'] = 'Notifications';
$app_list_strings['moduleList']['Sync'] = 'Sync';
$app_list_strings['moduleList']['ReportMaker'] = 'Advanced Reports';
$app_list_strings['moduleList']['DataSets'] = 'Data Formats';
$app_list_strings['moduleList']['CustomQueries'] = 'Custom Queries';
$app_list_strings['moduleList']['pmse_Inbox'] = 'Processes';
$app_list_strings['moduleList']['pmse_Project'] = 'Process Definitions';
$app_list_strings['moduleList']['pmse_Business_Rules'] = 'Process Business Rules';
$app_list_strings['moduleList']['pmse_Emails_Templates'] = 'Process Email Templates';
$app_list_strings['moduleList']['BusinessCenters'] = 'Business Centers';
$app_list_strings['moduleList']['Shifts'] = 'Shifts';
$app_list_strings['moduleList']['ShiftExceptions'] = 'Shift Exceptions';
$app_list_strings['moduleList']['Purchases'] = 'Purchases';
$app_list_strings['moduleList']['PurchasedLineItems'] = 'Purchased Line Items';
$app_list_strings['moduleList']['MobileDevices'] = 'MobileDevices';
$app_list_strings['moduleList']['PushNotifications'] = 'PushNotifications';
$app_list_strings['moduleList']['WorkFlow'] = 'Workflow Definitions';
$app_list_strings['moduleList']['EAPM'] = 'External Accounts';
$app_list_strings['moduleList']['Worksheet'] = 'Worksheet';
$app_list_strings['moduleList']['Users'] = 'Users';
$app_list_strings['moduleList']['Employees'] = 'Employees';
$app_list_strings['moduleList']['Administration'] = 'Administration';
$app_list_strings['moduleList']['ACLRoles'] = 'Roles';
$app_list_strings['moduleList']['InboundEmail'] = 'Inbound Email';
$app_list_strings['moduleList']['Releases'] = 'Releases';
$app_list_strings['moduleList']['Prospects'] = 'Targets';
$app_list_strings['moduleList']['Queues'] = 'Queues';
$app_list_strings['moduleList']['EmailMarketing'] = 'Email Marketing';
$app_list_strings['moduleList']['EmailTemplates'] = 'Email Templates';
$app_list_strings['moduleList']['SNIP'] = 'Email Archiving';
$app_list_strings['moduleList']['ProspectLists'] = 'Target Lists';
$app_list_strings['moduleList']['SavedSearch'] = 'Saved Searches';
$app_list_strings['moduleList']['UpgradeWizard'] = 'Upgrade Wizard';
$app_list_strings['moduleList']['Trackers'] = 'Trackers';
$app_list_strings['moduleList']['TrackerPerfs'] = 'Tracker Performance';
$app_list_strings['moduleList']['TrackerSessions'] = 'Tracker Sessions';
$app_list_strings['moduleList']['TrackerQueries'] = 'Tracker Queries';
$app_list_strings['moduleList']['FAQ'] = 'FAQ';
$app_list_strings['moduleList']['Newsletters'] = 'Newsletters';
$app_list_strings['moduleList']['SugarFavorites'] = 'Favorites';
$app_list_strings['moduleList']['PdfManager'] = 'PDF Manager';
$app_list_strings['moduleList']['DataArchiver'] = 'Data Archiver';
$app_list_strings['moduleList']['ArchiveRuns'] = 'Archive Runs';
$app_list_strings['moduleList']['OAuthKeys'] = 'OAuth Consumer Keys';
$app_list_strings['moduleList']['OAuthTokens'] = 'OAuth Tokens';
$app_list_strings['moduleList']['Filters'] = 'Filters';
$app_list_strings['moduleList']['UserSignatures'] = 'Email Signatures';
$app_list_strings['moduleList']['Shippers'] = 'Shipping Providers';
$app_list_strings['moduleList']['Styleguide'] = 'Styleguide';
$app_list_strings['moduleList']['Feedbacks'] = 'Feedbacks';
$app_list_strings['moduleList']['Tags'] = 'Tags';
$app_list_strings['moduleList']['Categories'] = 'Categories';
$app_list_strings['moduleList']['Dashboards'] = 'Dashboards';
$app_list_strings['moduleList']['OutboundEmail'] = 'Email Settings';
$app_list_strings['moduleList']['EmailParticipants'] = 'Email Participants';
$app_list_strings['moduleList']['DataPrivacy'] = 'Data Privacy';
$app_list_strings['moduleList']['ReportSchedules'] = 'Report Schedules';
$app_list_strings['moduleList']['CommentLog'] = 'Comment Log';
$app_list_strings['moduleList']['Holidays'] = 'Holidays';
$app_list_strings['moduleList']['ChangeTimers'] = 'Change Timers';
$app_list_strings['moduleList']['Messages'] = 'Messages';
$app_list_strings['moduleList']['Library'] = 'Library';
$app_list_strings['moduleList']['EmailAddresses'] = 'Email Address';
$app_list_strings['moduleList']['Words'] = 'Words';
$app_list_strings['moduleList']['Sugar_Favorites'] = 'Favorites';
$app_list_strings['moduleList']['KBDocuments'] = 'Knowledge Base Document';
$app_list_strings['moduleList']['KBContents'] = 'Knowledge Base';
$app_list_strings['moduleList']['KBArticles'] = 'Knowledge Base Article';
$app_list_strings['moduleList']['KBContentTemplates'] = 'Knowledge Base Template';
$app_list_strings['moduleList']['EmbeddedFiles'] = 'Embedded Files';
$app_list_strings['moduleList']['M03_Work_Product_IDs'] = 'Additional WP IDs';
$app_list_strings['moduleList']['M03_Work_Product_Personnel'] = 'Additional WP Personnel';
$app_list_strings['moduleList']['M01_Sales_Activity_Quote'] = 'Sales Activity Quotes';
$app_list_strings['moduleList']['m06_APS_Communication'] = 'APS Communications';
$app_list_strings['moduleList']['M01_SA_Division_Department'] = 'SA_Division_Departments';
$app_list_strings['moduleList']['m06_Note_To_File'] = 'Notes_To_File';
$app_list_strings['moduleList']['M01_Sales'] = 'Sales';
$app_list_strings['moduleList']['M01_Sales_Activity'] = 'Sales Activities';
$app_list_strings['moduleList']['WPP_Work_Product_Personnel'] = 'Work Product Personnel';
$app_list_strings['moduleList']['M02_SA_Division_Department_'] = 'SA Divisions Departments';
$app_list_strings['moduleList']['M01_Quote_Document'] = 'SA Documents';
$app_list_strings['moduleList']['ABC12_Work_Product_Activities'] = 'Work Product Activities';
$app_list_strings['moduleList']['WPP_WPP2'] = 'Work_Product_Personnel';
$app_list_strings['moduleList']['M03_Work_Product'] = 'Work Products';
$app_list_strings['moduleList']['m06_Communication'] = 'APS Communications';
$app_list_strings['moduleList']['M03_Work_Product_Deliverable'] = 'Work Product Deliverables';
$app_list_strings['moduleList']['M03_Work_Product_Facility'] = 'Work Product Facilities';
$app_list_strings['moduleList']['M03_work_product_code'] = 'Work Product Codes';
$app_list_strings['moduleList']['M03_Additional_WP_Personnel'] = 'Additional WP Personnel';
$app_list_strings['moduleList']['M03_Additional_WP_IDs'] = 'Additional WP IDs';
$app_list_strings['moduleList']['M03_Work_Product_Code'] = 'Work Product Codes';
$app_list_strings['moduleList']['M01_Quote'] = 'Sales Activity Quotes';
$app_list_strings['moduleList']['M01_Sale'] = 'Sales Activities';
$app_list_strings['moduleList']['M01_SA_Division_Department_'] = 'SA_Division_Departments';
$app_list_strings['moduleList']['M06_Error'] = 'Communications';
$app_list_strings['moduleList']['AN01_Activity_Notes'] = 'Activity Notes';
$app_list_strings['moduleList']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['moduleList']['A1A_CPI_Findings'] = 'CPI Findings';
$app_list_strings['moduleList']['HR01_Resumes'] = 'Resumes';
$app_list_strings['moduleList']['idoc_documents'] = 'Flexidocs Documents';
$app_list_strings['moduleList']['idoc_signers'] = 'Flexidocs Signers';
$app_list_strings['moduleList']['idoc_events'] = 'Flexidocs Events';
$app_list_strings['moduleList']['idoc_doc_events'] = 'Events';
$app_list_strings['moduleList']['idoc_document_templates'] = 'Document Templates';
$app_list_strings['moduleList']['idoc_templates'] = 'Flexidocs Templates';
$app_list_strings['moduleList']['ANML_Animals'] = 'Test Systems';
$app_list_strings['moduleList']['WPE_Work_Product_Enrollment'] = 'Work Product Assignments';
$app_list_strings['moduleList']['TM_Tradeshow_Management'] = 'Tradeshow Management';
$app_list_strings['moduleList']['VM01_Visitor_Management'] = 'Visitor Management';
$app_list_strings['moduleList']['Equip_Equipment'] = 'Equipment &amp; Facilities';
$app_list_strings['moduleList']['Ere_Error_Employees'] = 'APS Employees';
$app_list_strings['moduleList']['Erd_Error_Documents'] = 'Controlled Documents';
$app_list_strings['moduleList']['M99_Protocol_Amendments'] = 'Protocol Amendments';
$app_list_strings['moduleList']['AC01_Acquired_Companies'] = 'Acquired Companies';
$app_list_strings['moduleList']['ED_Errors_Department'] = 'Errors Departments';
$app_list_strings['moduleList']['TA_Tradeshow_Activities'] = 'Tradeshow Activities';
$app_list_strings['moduleList']['AD_Anatomy_Database'] = 'Anatomy Database';
$app_list_strings['moduleList']['ErQC_Error_QC_Employees'] = 'Errors QC Employees';
$app_list_strings['moduleList']['DE_Deviation_Employees'] = 'Responsible Personnel';
$app_list_strings['moduleList']['SW_Study_Workflow'] = 'Study Workflow';
$app_list_strings['moduleList']['CD_Company_Documents'] = 'Company Documents';
$app_list_strings['moduleList']['TD_Tradeshow_Documents'] = 'Tradeshow Documents';
$app_list_strings['moduleList']['RMS_Room'] = 'Rooms';
$app_list_strings['moduleList']['RT_Room_Transfer'] = 'Room Transfers';
$app_list_strings['moduleList']['EFR_Equipment_Facility_Recor'] = 'Equipment &amp; Facility Records';
$app_list_strings['moduleList']['EFS_Equipment_Facility_Servi'] = 'Equipment &amp; Facility Services';
$app_list_strings['moduleList']['EFD_Equipment_Facility_Doc'] = 'Equipment &amp; Facility Documents';
$app_list_strings['moduleList']['CA_Company_Address'] = 'Company Addresses';
$app_list_strings['moduleList']['RR_Regulatory_Response'] = 'Regulatory Responses';
$app_list_strings['moduleList']['RRD_Regulatory_Response_Doc'] = 'Regulatory Response Documents';
$app_list_strings['moduleList']['MAJ_Contact_Documents'] = 'Contact Documents';
$app_list_strings['moduleList']['EDoc_Email_Documents'] = 'Email Documents';
$app_list_strings['moduleList']['ET_Email_Template'] = 'Email Templates';
$app_list_strings['moduleList']['CN_Company_Name'] = 'Company Names';
$app_list_strings['moduleList']['TSdoc_Test_System_Documents'] = 'Test System Documents';
$app_list_strings['moduleList']['U_Units'] = 'Units';
$app_list_strings['moduleList']['S_Species'] = 'Species';
$app_list_strings['moduleList']['W_Weight'] = 'Weights';
$app_list_strings['moduleList']['CO_Clinical_Observation'] = 'Clinical Observations';
$app_list_strings['moduleList']['CIE_Clinical_Issue_Exam'] = 'Clinical Issues &amp; Exams';
$app_list_strings['moduleList']['SV_Service_Vendor'] = 'Service Vendors';
$app_list_strings['moduleList']['ops_Backups'] = 'Backups';
$app_list_strings['moduleList']['GE_GDP_Examples'] = 'GDP Examples';
$app_list_strings['moduleList']['ProDo_Product_Document'] = 'Product Documents';
$app_list_strings['moduleList']['Prod_Product'] = 'Products';
$app_list_strings['moduleList']['SP_Service_Pricing'] = 'Service Pricing';
$app_list_strings['moduleList']['QARev_CD_QA_Reviews'] = 'Controlled Document QA Reviews';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleList']['TSD1_Test_System_Design_1'] = 'Test System Designs';
$app_list_strings['moduleList']['USDA_Historical_USDA_ID'] = 'Historical USDA IDs';
$app_list_strings['moduleList']['AN_Account_Number'] = 'Account Numbers';
$app_list_strings['moduleList']['WPG_Work_Product_Group'] = 'Work Product Groups';
$app_list_strings['moduleList']['SC_Species_Census'] = 'Species Census';
$app_list_strings['moduleList']['CDU_CD_Utilization'] = 'CD Utilizations/Deviation Rates';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_deviation_rate_basis_list.php

 // created: 2021-06-24 10:50:30

$app_list_strings['deviation_rate_basis_list']=array (
  '' => '',
  'Animal Days' => 'Animal Days',
  'Procedures' => 'Procedures',
  'Rate Not Calculated' => 'Rate Not Calculated',
  'Studies' => 'Studies',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_final_report_timeline_type_list.php

 // created: 2021-07-22 06:41:34

$app_list_strings['final_report_timeline_type_list']=array (
  '' => '',
  'By First Procedure' => 'By First Procedure',
  'By SPA Reconciliation' => 'By SPA Reconciliation',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_vc_related_to_list.php

 // created: 2021-07-20 11:18:27

$app_list_strings['vc_related_to_list']=array (
  '' => '',
  'Study' => 'Study',
  'non Study' => 'non-Study',
  'Recheck' => 'Recheck',
  'Unknown at This Time' => 'Unknown at This Time',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_red_yellow_green_list.php

 // created: 2021-07-22 09:09:42

$app_list_strings['red_yellow_green_list']=array (
  '' => '',
  'Red' => 'Red',
  'Yellow' => 'Yellow',
  'Green' => 'Green',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_category_list.php

 // created: 2021-08-05 07:02:55

$app_list_strings['category_list']=array (
  '' => '',
  'Critical Phase Inspection' => 'Critical Phase Inspection',
  'Daily QC' => 'Daily QC',
  'Data Book QC' => 'Data Book QC',
  'Deceased Animal' => 'Deceased Animal (Obsolete)',
  'Equipment Maintenance Request' => 'Equipment Maintenance Request',
  'External Audit' => 'External Audit',
  'Feedback' => 'External Feedback',
  'Maintenance Request' => 'Facility Maintenance Request',
  'Gross Pathology' => 'Gross Pathology',
  'IACUC Deficiency' => 'IACUC Deficiency',
  'Internal Feedback' => 'Internal Feedback',
  'Process Audit' => 'Process Audit',
  'Real time study conduct' => 'Real time study conduct',
  'Rejected Animal' => 'Rejected/Unused Animal (Obsolete)',
  'Retrospective Data QC' => 'Retrospective Data QC',
  'Study Specific Charge' => 'Study Specific Charge',
  'Test Failure' => 'Test Failure',
  'Training' => 'Training',
  'Vet Check' => 'Vet Check',
  'Weekly Sweep' => 'Weekly Sweep',
  'Work Product Schedule Outcome' => 'Work Product Outcome',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_minutes_per_slide_list.php

 // created: 2021-08-12 09:02:16

$app_list_strings['minutes_per_slide_list']=array (
  '' => '',
  3 => '3',
  5 => '5',
  10 => '10',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_procedure_room_list.php

 // created: 2021-08-13 06:34:38

$app_list_strings['procedure_room_list']=array (
  1 => 'Cath Lab 1',
  2 => 'Cath Lab 2',
  3 => 'Cath Lab 3',
  4 => 'Cath Lab 4',
  5 => 'CT',
  6 => 'Histology Lab',
  7 => 'Necropsy',
  8 => 'OR1',
  9 => 'OR2',
  10 => 'Cath Lab 5',
  11 => 'OR4',
  12 => 'OR5',
  13 => 'Sample Prep Lab',
  14 => 'IVT Lab',
  15 => 'Clin Path Lab',
  16 => 'Blood Loop Lab',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Task_Design.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['TaskD_Task_Design'] = 'Task Designs';
$app_list_strings['moduleListSingular']['TaskD_Task_Design'] = 'Task Design';
$app_list_strings['yes_no_list']['Yes'] = 'Yes';
$app_list_strings['yes_no_list']['No'] = 'No';
$app_list_strings['yes_no_list'][''] = '';
$app_list_strings['task_type_list']['Custom'] = 'Custom';
$app_list_strings['task_type_list']['Standard'] = 'Custom';
$app_list_strings['task_type_list'][''] = '';
$app_list_strings['standard_task_list']['Anesthetized Blood Draw'] = 'Anesthetized Blood Draw';
$app_list_strings['standard_task_list']['Awake Blood Draw'] = 'Awake Blood Draw';
$app_list_strings['standard_task_list']['Bandage Change'] = 'Bandage Change';
$app_list_strings['standard_task_list']['Battery ChangeCharge'] = 'Battery Change/Charge';
$app_list_strings['standard_task_list']['Body Weight'] = 'Body Weight';
$app_list_strings['standard_task_list']['CT'] = 'CT';
$app_list_strings['standard_task_list']['Diet Mods'] = 'Diet Mods';
$app_list_strings['standard_task_list']['EKGECG'] = 'EKG/ECG';
$app_list_strings['standard_task_list']['Gait Exam'] = 'Gait Exam';
$app_list_strings['standard_task_list']['Neruo Exam'] = 'Neruo Exam';
$app_list_strings['standard_task_list']['Physical Exam'] = 'Physical Exam';
$app_list_strings['standard_task_list']['Ultrasound'] = 'Ultrasound';
$app_list_strings['standard_task_list']['Urinalysis'] = 'Urinalysis';
$app_list_strings['standard_task_list'][''] = '';
$app_list_strings['equipment_required_list']['12 lead'] = '12-lead';
$app_list_strings['equipment_required_list']['Bard'] = 'Bard';
$app_list_strings['equipment_required_list']['CT'] = 'CT';
$app_list_strings['equipment_required_list']['EKGECG'] = 'EKG/ECG';
$app_list_strings['equipment_required_list']['Fluroscopy'] = 'Fluroscopy';
$app_list_strings['equipment_required_list']['PowerLab'] = 'PowerLab';
$app_list_strings['equipment_required_list']['Ultrasound APS'] = 'Ultrasound (APS)';
$app_list_strings['equipment_required_list']['Ultrasound Jim B'] = 'Ultrasound (Jim B.)';
$app_list_strings['equipment_required_list'][''] = '';
$app_list_strings['taskdesign_task_type_list']['Custom'] = 'Custom';
$app_list_strings['taskdesign_task_type_list']['Standard'] = 'Standard';
$app_list_strings['taskdesign_task_type_list'][''] = '';
$app_list_strings['plan_actual_list']['Plan'] = 'Plan';
$app_list_strings['plan_actual_list']['Actual'] = 'Actual';
$app_list_strings['plan_actual_list'][''] = '';
$app_list_strings['procedure_activity_list']['Activity'] = 'Activity';
$app_list_strings['procedure_activity_list']['Procedure'] = 'Procedure';
$app_list_strings['procedure_activity_list'][''] = '';
$app_list_strings['td_tier_relative_list']['NA'] = 'N/A';
$app_list_strings['td_tier_relative_list']['1st Tier'] = '1st Tier';
$app_list_strings['td_tier_relative_list']['2nd Tier'] = '2nd Tier';
$app_list_strings['td_tier_relative_list'][''] = '';
$app_list_strings['task_procedure_list']['Task'] = 'Task';
$app_list_strings['task_procedure_list']['Procedure'] = 'Procedure';
$app_list_strings['task_procedure_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Group_Design.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Group_Design.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Group_Design.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['GD_Group_Design'] = 'Group Designs';
$app_list_strings['moduleListSingular']['GD_Group_Design'] = 'Group Design';
$app_list_strings['record_status_list']['Active'] = 'Active';
$app_list_strings['record_status_list']['Archived'] = 'Archived';
$app_list_strings['record_status_list'][''] = '';
$app_list_strings['number_list']['01'] = '01';
$app_list_strings['number_list']['02'] = '02';
$app_list_strings['number_list']['03'] = '03';
$app_list_strings['number_list']['04'] = '04';
$app_list_strings['number_list']['05'] = '05';
$app_list_strings['number_list']['06'] = '06';
$app_list_strings['number_list']['07'] = '07';
$app_list_strings['number_list']['08'] = '08';
$app_list_strings['number_list']['09'] = '09';
$app_list_strings['number_list'][10] = '10';
$app_list_strings['number_list'][''] = '';
$app_list_strings['study_article_type_list']['Test'] = 'Test';
$app_list_strings['study_article_type_list']['Control'] = 'Control';
$app_list_strings['study_article_type_list']['Test Control'] = 'Test & Control';
$app_list_strings['study_article_type_list']['Sham'] = 'Sham';
$app_list_strings['study_article_type_list'][''] = '';
$app_list_strings['quantity_type_list']['Exact'] = 'Exact';
$app_list_strings['quantity_type_list']['Max'] = 'Max';
$app_list_strings['quantity_type_list'][''] = '';
$app_list_strings['gd_study_article_type_list']['Control'] = 'Control';
$app_list_strings['gd_study_article_type_list']['Sham'] = 'Sham';
$app_list_strings['gd_study_article_type_list']['Test'] = 'Test';
$app_list_strings['gd_study_article_type_list']['Test Control'] = 'Test & Control';
$app_list_strings['gd_study_article_type_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['GD_Group_Design'] = 'Group Designs';
$app_list_strings['moduleListSingular']['GD_Group_Design'] = 'Group Design';
$app_list_strings['record_status_list']['Active'] = 'Active';
$app_list_strings['record_status_list']['Archived'] = 'Archived';
$app_list_strings['record_status_list'][''] = '';
$app_list_strings['number_list']['01'] = '01';
$app_list_strings['number_list']['02'] = '02';
$app_list_strings['number_list']['03'] = '03';
$app_list_strings['number_list']['04'] = '04';
$app_list_strings['number_list']['05'] = '05';
$app_list_strings['number_list']['06'] = '06';
$app_list_strings['number_list']['07'] = '07';
$app_list_strings['number_list']['08'] = '08';
$app_list_strings['number_list']['09'] = '09';
$app_list_strings['number_list'][10] = '10';
$app_list_strings['number_list'][''] = '';
$app_list_strings['study_article_type_list']['Test'] = 'Test';
$app_list_strings['study_article_type_list']['Control'] = 'Control';
$app_list_strings['study_article_type_list']['Test Control'] = 'Test & Control';
$app_list_strings['study_article_type_list']['Sham'] = 'Sham';
$app_list_strings['study_article_type_list'][''] = '';
$app_list_strings['quantity_type_list']['Exact'] = 'Exact';
$app_list_strings['quantity_type_list']['Max'] = 'Max';
$app_list_strings['quantity_type_list'][''] = '';
$app_list_strings['gd_study_article_type_list']['Control'] = 'Control';
$app_list_strings['gd_study_article_type_list']['Sham'] = 'Sham';
$app_list_strings['gd_study_article_type_list']['Test'] = 'Test';
$app_list_strings['gd_study_article_type_list']['Test Control'] = 'Test & Control';
$app_list_strings['gd_study_article_type_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['GD_Group_Design'] = 'Group Designs';
$app_list_strings['moduleListSingular']['GD_Group_Design'] = 'Group Design';
$app_list_strings['record_status_list']['Active'] = 'Active';
$app_list_strings['record_status_list']['Archived'] = 'Archived';
$app_list_strings['record_status_list'][''] = '';
$app_list_strings['number_list']['01'] = '01';
$app_list_strings['number_list']['02'] = '02';
$app_list_strings['number_list']['03'] = '03';
$app_list_strings['number_list']['04'] = '04';
$app_list_strings['number_list']['05'] = '05';
$app_list_strings['number_list']['06'] = '06';
$app_list_strings['number_list']['07'] = '07';
$app_list_strings['number_list']['08'] = '08';
$app_list_strings['number_list']['09'] = '09';
$app_list_strings['number_list'][10] = '10';
$app_list_strings['number_list'][''] = '';
$app_list_strings['study_article_type_list']['Test'] = 'Test';
$app_list_strings['study_article_type_list']['Control'] = 'Control';
$app_list_strings['study_article_type_list']['Test Control'] = 'Test & Control';
$app_list_strings['study_article_type_list']['Sham'] = 'Sham';
$app_list_strings['study_article_type_list'][''] = '';
$app_list_strings['quantity_type_list']['Exact'] = 'Exact';
$app_list_strings['quantity_type_list']['Max'] = 'Max';
$app_list_strings['quantity_type_list'][''] = '';
$app_list_strings['gd_study_article_type_list']['Control'] = 'Control';
$app_list_strings['gd_study_article_type_list']['Sham'] = 'Sham';
$app_list_strings['gd_study_article_type_list']['Test'] = 'Test';
$app_list_strings['gd_study_article_type_list']['Test Control'] = 'Test & Control';
$app_list_strings['gd_study_article_type_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_reason_for_weight_list.php

 // created: 2021-08-12 12:40:42

$app_list_strings['reason_for_weight_list']=array (
  '' => '',
  'Other' => 'Other',
  'Protocol Assignment' => 'Protocol Assignment',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_test_system_list.php

 // created: 2021-09-08 14:15:36

$app_list_strings['test_system_list']=array (
  '' => '',
  'CHO_K1' => ' Cell Line CHO-K1',
  'ACRFAAKAACOOH and ACRFAACAACOOH' => 'AC-RFAAKAA-COOH, AC-RFAACAA-COOH',
  'Bacterial Strain' => 'Bacterial Strain',
  'Benchtop' => 'Benchtop (Non-Tissue)',
  'Bovine' => 'Bovine',
  'Bovine Blood' => 'Bovine Blood',
  'Canine' => 'Canine',
  'Canine Blood' => 'Canine Blood',
  'Caprine' => 'Caprine',
  'Cell line NCTC clone 929 L929 cells' => 'Cell line NCTC clone 929 (L-929 cells)',
  'Cell line L5178Y TKMouse Lymphoma cells' => 'Cell line L5178Y TK+/- (Mouse Lymphoma cells)',
  'Cell line Balbc 3t3 clone A31' => 'Cell line Balb/c 3t3 clone A31',
  'Cell line V79 Chinese hamster lung fibroblast' => 'Cell line V79 Chinese hamster lung fibroblast',
  'Cell line THP1 human monocytic leukemia cells' => 'Cell line THP-1 (human monocytic leukemia cells)',
  'Cell Line CHOK1 Chinese hamster ovary cells' => 'Cell Line CHO-K1 (Chinese hamster ovary cells)',
  'Cell Line K562' => 'Cell Line K-562',
  'Chinchilla' => 'Chinchilla',
  'Extract' => 'Extract of Study Article',
  'Guinea Pig' => 'Guinea Pig',
  'Hamster' => 'Hamster',
  'Human Blood' => 'Human Blood',
  'Human Cadaver' => 'Human Cadaver',
  'Human Plasma' => 'Human Plasma',
  'Human pRBC' => 'Human pRBC',
  'Lagomorph' => 'Lagomorph',
  'Lagomorph WHHL' => 'Lagomorph - WHHL',
  'Lagomorph Blood' => 'Lagomorph Blood',
  'Limulus Amebocyte Lysate LAL' => 'Limulus Amebocyte Lysate (LAL)',
  'Micro Yucatans' => 'Micro Yucatans',
  'Mouse' => 'Mouse',
  'Non biologic' => 'Non-biologic',
  'Normal Human Serum' => 'Normal Human Serum',
  'Not Applicable' => 'Not Applicable',
  'Ossabaw' => 'Ossabaw',
  'Ovine' => 'Ovine',
  'Ovine Blood' => 'Ovine Blood',
  'Ovine Blood and Human Blood' => 'Ovine Blood and Human Blood',
  'PorcineGeneral' => 'Porcine',
  'Porcine Got' => 'Porcine - Gottingen',
  'Porcine' => 'Porcine - Yorkshire',
  'Porcine Yuc' => 'Porcine - Yucatan',
  'Porcine and Bovine' => 'Porcine and Bovine',
  'Porcine Blood' => 'Porcine Blood',
  'Porcine_Canine_Ovine' => 'Porcine, Canine, and Ovine',
  'Rat' => 'Rat',
  'RhE' => 'RhE',
  'Wetlab' => 'Wetlab',
  'Yorkshire Yucatan' => 'Yorkshire & Yucatan',
  'Balb_c 3t3 Cell Line' => 'Cell Line Balb/c 3t3 (Obsolete)',
  'Cell Line L5178Y' => 'Cell Line L5178Y TK+/- (Obsolete)',
  'Cell Line L929' => 'Cell Line L929 (Obsolete)',
  'Cell Line THP 1' => 'Cell Line THP-1 (Obsolete)',
  'Cell Line V79' => 'Cell Line V79 (Obsolete)',
  'Multiple' => 'Multiple (Obsolete)',
  'Normal_Human_Serum_C3a_Quidel_Kit' => 'Normal Human Serum & C3a Quidel Kit (obsoleted 3/20/2020)',
  'Normal Human Serum and SC5b9 Quidel kit' => 'Normal Human Serum and SC5b-9 Quidel kit (obsoleted 3/20/2020)',
  'Human Serum' => 'Normal Human Serum and C3a/SC5b-9 Quidel Kit (obsoleted 3/20/2020)',
  'Limulus Amebocyte Lysate and Control Standard Endotoxin' => 'Limulus Amebocyte Lysate and Control Standard Endotoxin (obsoleted 4/24/2020)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_action_needed_list.php

 // created: 2021-10-14 06:55:33

$app_list_strings['action_needed_list']=array (
  '' => '',
  'Not Applicable' => 'Not Applicable',
  'MPLS' => 'MPLS',
  'NW' => 'NW',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Order_Request_Item.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['ORI_Order_Request_Item'] = 'Order Request Items';
$app_list_strings['moduleListSingular']['ORI_Order_Request_Item'] = 'Order Request Item';
$app_list_strings['inventory_item_owner_list']['APS'] = 'APS';
$app_list_strings['inventory_item_owner_list']['APS Supplied Client Owned'] = 'APS Supplied, Client Owned';
$app_list_strings['inventory_item_owner_list'][''] = '';
$app_list_strings['ORI_type_list']['In System Vendor Non Specific'] = 'In System, Vendor Non-Specific';
$app_list_strings['ORI_type_list']['In System Vendor Specific'] = 'In System, Vendor Specific';
$app_list_strings['ORI_type_list']['Not In System Vendor Non Specific'] = 'Not In System, Vendor Non-Specific';
$app_list_strings['ORI_type_list']['Not In System Vendor Specific'] = 'Not In System, Vendor Specific';
$app_list_strings['ORI_type_list'][''] = '';
$app_list_strings['product_category_list']['Balloon Catheter'] = 'Balloon Catheter';
$app_list_strings['product_category_list']['Cannula'] = 'Cannula';
$app_list_strings['product_category_list']['Catheter'] = 'Catheter';
$app_list_strings['product_category_list']['Chemical'] = 'Chemical';
$app_list_strings['product_category_list']['Cleaning Agent'] = 'Cleaning Agent';
$app_list_strings['product_category_list']['Contrast'] = 'Contrast';
$app_list_strings['product_category_list']['Drug'] = 'Drug';
$app_list_strings['product_category_list']['Graft'] = 'Graft';
$app_list_strings['product_category_list']['Inflation Device'] = 'Inflation Device';
$app_list_strings['product_category_list']['Reagent'] = 'Reagent';
$app_list_strings['product_category_list']['Sheath'] = 'Sheath';
$app_list_strings['product_category_list']['Stent'] = 'Stent';
$app_list_strings['product_category_list']['Suture'] = 'Suture';
$app_list_strings['product_category_list']['Wire'] = 'Wire';
$app_list_strings['product_category_list'][''] = '';
$app_list_strings['purchase_unit_list']['Each'] = 'Each';
$app_list_strings['purchase_unit_list']['Bottle'] = 'Bottle';
$app_list_strings['purchase_unit_list']['Box'] = 'Box';
$app_list_strings['purchase_unit_list']['Case'] = 'Case';
$app_list_strings['purchase_unit_list'][''] = '';
$app_list_strings['ORI_status_list']['Backordered'] = 'Backordered';
$app_list_strings['ORI_status_list']['Inventory'] = 'Inventory';
$app_list_strings['ORI_status_list']['Ordered'] = 'Ordered';
$app_list_strings['ORI_status_list']['Received'] = 'Received';
$app_list_strings['ORI_status_list']['Requested'] = 'Requested';
$app_list_strings['ORI_status_list'][''] = '';
$app_list_strings['poi_ori_related_to_list']['Internal Use'] = 'Internal Use';
$app_list_strings['poi_ori_related_to_list']['Sales'] = 'Sales';
$app_list_strings['poi_ori_related_to_list']['Work Product'] = 'Work Product';
$app_list_strings['poi_ori_related_to_list'][''] = '';
$app_list_strings['POI_owner_list']['APS Owned'] = 'APS Owned';
$app_list_strings['POI_owner_list']['APS Supplied Client Owned'] = 'APS Supplied, Client Owned';
$app_list_strings['POI_owner_list'][''] = '';
$app_list_strings['pro_subtype_list']['Access Catheter'] = 'Access Catheter';
$app_list_strings['pro_subtype_list']['Angioplasty Balloon'] = 'Angioplasty Balloon';
$app_list_strings['pro_subtype_list']['Aortic Root Cannula'] = 'Aortic Root Cannula';
$app_list_strings['pro_subtype_list']['Arterial Cannula'] = 'Arterial Cannula';
$app_list_strings['pro_subtype_list']['Bare Metal Stent'] = 'Bare Metal Stent';
$app_list_strings['pro_subtype_list']['Barium Contrast'] = 'Barium Contrast';
$app_list_strings['pro_subtype_list']['Central Venous Catheter'] = 'Central Venous Catheter';
$app_list_strings['pro_subtype_list']['Coated'] = 'Coated';
$app_list_strings['pro_subtype_list']['Cutting Balloon'] = 'Cutting Balloon';
$app_list_strings['pro_subtype_list']['Diagnostic Catheter'] = 'Diagnostic Catheter';
$app_list_strings['pro_subtype_list']['Drug Coated Balloon'] = 'Drug Coated Balloon';
$app_list_strings['pro_subtype_list']['Drug Coated Stent'] = 'Drug Coated Stent';
$app_list_strings['pro_subtype_list']['EP Catheter'] = 'EP Catheter';
$app_list_strings['pro_subtype_list']['ePTFE'] = 'ePTFE';
$app_list_strings['pro_subtype_list']['Ethibond'] = 'Ethibond';
$app_list_strings['pro_subtype_list']['Ethilon'] = 'Ethilon';
$app_list_strings['pro_subtype_list']['Exchange Wire'] = 'Exchange Wire';
$app_list_strings['pro_subtype_list']['Extension Catheter'] = 'Extension Catheter';
$app_list_strings['pro_subtype_list']['Foley Catheter'] = 'Foley Catheter';
$app_list_strings['pro_subtype_list']['Guide Catheter'] = 'Guide Catheter';
$app_list_strings['pro_subtype_list']['Guide Wire'] = 'Guide Wire';
$app_list_strings['pro_subtype_list']['High Pressure'] = 'High Pressure';
$app_list_strings['pro_subtype_list']['Injectable'] = 'Injectable';
$app_list_strings['pro_subtype_list']['Introducer Sheath'] = 'Introducer Sheath';
$app_list_strings['pro_subtype_list']['Iodinated Contrast'] = 'Iodinated Contrast';
$app_list_strings['pro_subtype_list']['IV Catheter'] = 'IV Catheter';
$app_list_strings['pro_subtype_list']['Knitted'] = 'Knitted';
$app_list_strings['pro_subtype_list']['Mapping Catheter'] = 'Mapping Catheter';
$app_list_strings['pro_subtype_list']['Marking Wire'] = 'Marking Wire';
$app_list_strings['pro_subtype_list']['Micro Catheter'] = 'Micro Catheter';
$app_list_strings['pro_subtype_list']['Mila Catheter'] = 'Mila Catheter';
$app_list_strings['pro_subtype_list']['Monocryl'] = 'Monocryl';
$app_list_strings['pro_subtype_list']['Oral'] = 'Oral';
$app_list_strings['pro_subtype_list']['OTW over the wire'] = 'OTW (over the wire)';
$app_list_strings['pro_subtype_list']['Patches'] = 'Patches';
$app_list_strings['pro_subtype_list']['PDS'] = 'PDS';
$app_list_strings['pro_subtype_list']['Pediatric Cannula'] = 'Pediatric Cannula';
$app_list_strings['pro_subtype_list']['Pledget'] = 'Pledget';
$app_list_strings['pro_subtype_list']['POBA Balloon'] = 'POBA Balloon';
$app_list_strings['pro_subtype_list']['Pressure Catheter'] = 'Pressure Catheter';
$app_list_strings['pro_subtype_list']['Prolene'] = 'Prolene';
$app_list_strings['pro_subtype_list']['PTA Balloon'] = 'PTA Balloon';
$app_list_strings['pro_subtype_list']['Silk'] = 'Silk';
$app_list_strings['pro_subtype_list']['Steerable Sheath'] = 'Steerable Sheath';
$app_list_strings['pro_subtype_list']['Support Catheter'] = 'Support Catheter';
$app_list_strings['pro_subtype_list']['Support Wire'] = 'Support Wire';
$app_list_strings['pro_subtype_list']['Ti Cron'] = 'Ti-Cron';
$app_list_strings['pro_subtype_list']['Transseptal Catheter'] = 'Transseptal Catheter';
$app_list_strings['pro_subtype_list']['Venous Cannula'] = 'Venous Cannula';
$app_list_strings['pro_subtype_list']['Vessel Cannula'] = 'Vessel Cannula';
$app_list_strings['pro_subtype_list']['Vicryl'] = 'Vicryl';
$app_list_strings['pro_subtype_list']['Woven'] = 'Woven';
$app_list_strings['pro_subtype_list'][''] = '';
$app_list_strings['inventory_item_storage_condition_list']['30 45'] = '30 - 45°C';
$app_list_strings['inventory_item_storage_condition_list']['Ambient'] = 'Ambient';
$app_list_strings['inventory_item_storage_condition_list']['Room Temperature'] = 'Room Temperature';
$app_list_strings['inventory_item_storage_condition_list']['Refrigerate'] = 'Refrigerate';
$app_list_strings['inventory_item_storage_condition_list']['Frozen'] = 'Frozen';
$app_list_strings['inventory_item_storage_condition_list']['Ultra Frozen'] = 'Ultra Frozen';
$app_list_strings['inventory_item_storage_condition_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Order_Request.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['OR_Order_Request'] = 'Order Requests';
$app_list_strings['moduleListSingular']['OR_Order_Request'] = 'Order Request';
$app_list_strings['department_list']['Analytical Services'] = 'Analytical';
$app_list_strings['department_list']['Business Development'] = 'Business Development';
$app_list_strings['department_list']['Facilities'] = 'Facilities';
$app_list_strings['department_list']['Finance'] = 'Finance';
$app_list_strings['department_list']['Histology Services'] = 'Histology Services';
$app_list_strings['department_list']['Human_Resources'] = 'Human Resources';
$app_list_strings['department_list']['In life Large Animal Care'] = 'In-life Large Animal Care';
$app_list_strings['department_list']['In life Large Animal Research'] = 'In-life Large Animal Research';
$app_list_strings['department_list']['In life Small Animal Care'] = 'In-life Small Animal Care';
$app_list_strings['department_list']['In life Small Animal Research'] = 'In-life Small Animal Research';
$app_list_strings['department_list']['Information_Technology'] = 'Information Technology';
$app_list_strings['department_list']['Interventional Surgical Research'] = 'Interventional Surgical Research';
$app_list_strings['department_list']['Lab Services'] = 'Lab Services';
$app_list_strings['department_list']['Operations Support'] = 'Operations Support';
$app_list_strings['department_list']['Pathology Services'] = 'Pathology Services';
$app_list_strings['department_list']['Pharmacology'] = 'Pharmacology Services';
$app_list_strings['department_list']['Process Improvement'] = 'Process Improvement';
$app_list_strings['department_list']['Quality Assurance Unit'] = 'Quality Assurance Unit';
$app_list_strings['department_list']['Regulatory Services'] = 'Regulatory Services';
$app_list_strings['department_list']['Scientific'] = 'Scientific';
$app_list_strings['department_list']['Software Development'] = 'Software Development';
$app_list_strings['department_list']['Toxicology'] = 'Toxicology Services';
$app_list_strings['department_list']['Veterinary Services'] = 'Veterinary Services';
$app_list_strings['department_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Purchase_Order_Item.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['POI_Purchase_Order_Item'] = 'Purchase Order Items';
$app_list_strings['moduleListSingular']['POI_Purchase_Order_Item'] = 'Purchase Order Item';
$app_list_strings['POI_owner_list']['APS Owned'] = 'APS Owned';
$app_list_strings['POI_owner_list']['APS Supplied Client Owned'] = 'APS Supplied, Client Owned';
$app_list_strings['POI_owner_list'][''] = '';
$app_list_strings['department_list']['Analytical Services'] = 'Analytical';
$app_list_strings['department_list']['Business Development'] = 'Business Development';
$app_list_strings['department_list']['Facilities'] = 'Facilities';
$app_list_strings['department_list']['Finance'] = 'Finance';
$app_list_strings['department_list']['Histology Services'] = 'Histology Services';
$app_list_strings['department_list']['Human_Resources'] = 'Human Resources';
$app_list_strings['department_list']['In life Large Animal Care'] = 'In-life Large Animal Care';
$app_list_strings['department_list']['In life Large Animal Research'] = 'In-life Large Animal Research';
$app_list_strings['department_list']['In life Small Animal Care'] = 'In-life Small Animal Care';
$app_list_strings['department_list']['In life Small Animal Research'] = 'In-life Small Animal Research';
$app_list_strings['department_list']['Information_Technology'] = 'Information Technology';
$app_list_strings['department_list']['Interventional Surgical Research'] = 'Interventional Surgical Research';
$app_list_strings['department_list']['Lab Services'] = 'Lab Services';
$app_list_strings['department_list']['Operations Support'] = 'Operations Support';
$app_list_strings['department_list']['Pathology Services'] = 'Pathology Services';
$app_list_strings['department_list']['Pharmacology'] = 'Pharmacology Services';
$app_list_strings['department_list']['Process Improvement'] = 'Process Improvement';
$app_list_strings['department_list']['Quality Assurance Unit'] = 'Quality Assurance Unit';
$app_list_strings['department_list']['Regulatory Services'] = 'Regulatory Services';
$app_list_strings['department_list']['Scientific'] = 'Scientific';
$app_list_strings['department_list']['Software Development'] = 'Software Development';
$app_list_strings['department_list']['Toxicology'] = 'Toxicology Services';
$app_list_strings['department_list']['Veterinary Services'] = 'Veterinary Services';
$app_list_strings['department_list'][''] = '';
$app_list_strings['poi_type_list']['In System'] = 'In System';
$app_list_strings['poi_type_list']['Not In System'] = 'Not In System';
$app_list_strings['poi_type_list'][''] = '';
$app_list_strings['product_category_list']['Balloon Catheter'] = 'Balloon Catheter';
$app_list_strings['product_category_list']['Cannula'] = 'Cannula';
$app_list_strings['product_category_list']['Catheter'] = 'Catheter';
$app_list_strings['product_category_list']['Chemical'] = 'Chemical';
$app_list_strings['product_category_list']['Cleaning Agent'] = 'Cleaning Agent';
$app_list_strings['product_category_list']['Contrast'] = 'Contrast';
$app_list_strings['product_category_list']['Drug'] = 'Drug';
$app_list_strings['product_category_list']['Equipment'] = 'Equipment';
$app_list_strings['product_category_list']['Graft'] = 'Graft';
$app_list_strings['product_category_list']['Inflation Device'] = 'Inflation Device';
$app_list_strings['product_category_list']['Reagent'] = 'Reagent';
$app_list_strings['product_category_list']['Sheath'] = 'Sheath';
$app_list_strings['product_category_list']['Solution'] = 'Solution';
$app_list_strings['product_category_list']['Stent'] = 'Stent';
$app_list_strings['product_category_list']['Suture'] = 'Suture';
$app_list_strings['product_category_list']['Wire'] = 'Wire';
$app_list_strings['product_category_list'][''] = '';
$app_list_strings['purchase_unit_list']['Each'] = 'Each';
$app_list_strings['purchase_unit_list']['Bottle'] = 'Bottle';
$app_list_strings['purchase_unit_list']['Box'] = 'Box';
$app_list_strings['purchase_unit_list']['Case'] = 'Case';
$app_list_strings['purchase_unit_list'][''] = '';
$app_list_strings['oi_status_list']['Backordered'] = 'Backordered';
$app_list_strings['oi_status_list']['Inventory'] = 'Inventory';
$app_list_strings['oi_status_list']['Ordered'] = 'Ordered';
$app_list_strings['oi_status_list']['Received'] = 'Received';
$app_list_strings['oi_status_list'][''] = '';
$app_list_strings['poi_ori_related_to_list']['Internal Use'] = 'Internal Use';
$app_list_strings['poi_ori_related_to_list']['Sales'] = 'Sales';
$app_list_strings['poi_ori_related_to_list']['Work Product'] = 'Work Product';
$app_list_strings['poi_ori_related_to_list'][''] = '';
$app_list_strings['pro_subtype_list']['Access Catheter'] = 'Access Catheter';
$app_list_strings['pro_subtype_list']['Angioplasty Balloon'] = 'Angioplasty Balloon';
$app_list_strings['pro_subtype_list']['Aortic Root Cannula'] = 'Aortic Root Cannula';
$app_list_strings['pro_subtype_list']['Arterial Cannula'] = 'Arterial Cannula';
$app_list_strings['pro_subtype_list']['Bare Metal Stent'] = 'Bare Metal Stent';
$app_list_strings['pro_subtype_list']['Barium Contrast'] = 'Barium Contrast';
$app_list_strings['pro_subtype_list']['Central Venous Catheter'] = 'Central Venous Catheter';
$app_list_strings['pro_subtype_list']['Coated'] = 'Coated';
$app_list_strings['pro_subtype_list']['Cutting Balloon'] = 'Cutting Balloon';
$app_list_strings['pro_subtype_list']['Diagnostic Catheter'] = 'Diagnostic Catheter';
$app_list_strings['pro_subtype_list']['Drug Coated Balloon'] = 'Drug Coated Balloon';
$app_list_strings['pro_subtype_list']['Drug Coated Stent'] = 'Drug Coated Stent';
$app_list_strings['pro_subtype_list']['EP Catheter'] = 'EP Catheter';
$app_list_strings['pro_subtype_list']['ePTFE'] = 'ePTFE';
$app_list_strings['pro_subtype_list']['Ethibond'] = 'Ethibond';
$app_list_strings['pro_subtype_list']['Ethilon'] = 'Ethilon';
$app_list_strings['pro_subtype_list']['Exchange Wire'] = 'Exchange Wire';
$app_list_strings['pro_subtype_list']['Extension Catheter'] = 'Extension Catheter';
$app_list_strings['pro_subtype_list']['Foley Catheter'] = 'Foley Catheter';
$app_list_strings['pro_subtype_list']['Guide Catheter'] = 'Guide Catheter';
$app_list_strings['pro_subtype_list']['Guide Wire'] = 'Guide Wire';
$app_list_strings['pro_subtype_list']['High Pressure'] = 'High Pressure';
$app_list_strings['pro_subtype_list']['Injectable'] = 'Injectable';
$app_list_strings['pro_subtype_list']['Introducer Sheath'] = 'Introducer Sheath';
$app_list_strings['pro_subtype_list']['Iodinated Contrast'] = 'Iodinated Contrast';
$app_list_strings['pro_subtype_list']['IV Catheter'] = 'IV Catheter';
$app_list_strings['pro_subtype_list']['Knitted'] = 'Knitted';
$app_list_strings['pro_subtype_list']['Mapping Catheter'] = 'Mapping Catheter';
$app_list_strings['pro_subtype_list']['Marking Wire'] = 'Marking Wire';
$app_list_strings['pro_subtype_list']['Micro Catheter'] = 'Micro Catheter';
$app_list_strings['pro_subtype_list']['Mila Catheter'] = 'Mila Catheter';
$app_list_strings['pro_subtype_list']['Monocryl'] = 'Monocryl';
$app_list_strings['pro_subtype_list']['Oral'] = 'Oral';
$app_list_strings['pro_subtype_list']['OTW over the wire'] = 'OTW (over the wire)';
$app_list_strings['pro_subtype_list']['Patches'] = 'Patches';
$app_list_strings['pro_subtype_list']['PDS'] = 'PDS';
$app_list_strings['pro_subtype_list']['Pediatric Cannula'] = 'Pediatric Cannula';
$app_list_strings['pro_subtype_list']['Pledget'] = 'Pledget';
$app_list_strings['pro_subtype_list']['POBA Balloon'] = 'POBA Balloon';
$app_list_strings['pro_subtype_list']['Pressure Catheter'] = 'Pressure Catheter';
$app_list_strings['pro_subtype_list']['Prolene'] = 'Prolene';
$app_list_strings['pro_subtype_list']['PTA Balloon'] = 'PTA Balloon';
$app_list_strings['pro_subtype_list']['Silk'] = 'Silk';
$app_list_strings['pro_subtype_list']['Steerable Sheath'] = 'Steerable Sheath';
$app_list_strings['pro_subtype_list']['Support Catheter'] = 'Support Catheter';
$app_list_strings['pro_subtype_list']['Support Wire'] = 'Support Wire';
$app_list_strings['pro_subtype_list']['Ti Cron'] = 'Ti-Cron';
$app_list_strings['pro_subtype_list']['Transseptal Catheter'] = 'Transseptal Catheter';
$app_list_strings['pro_subtype_list']['Venous Cannula'] = 'Venous Cannula';
$app_list_strings['pro_subtype_list']['Vessel Cannula'] = 'Vessel Cannula';
$app_list_strings['pro_subtype_list']['Vicryl'] = 'Vicryl';
$app_list_strings['pro_subtype_list']['Woven'] = 'Woven';
$app_list_strings['pro_subtype_list'][''] = '';
$app_list_strings['inventory_item_storage_condition_list']['30 45'] = '30 - 45°C';
$app_list_strings['inventory_item_storage_condition_list']['Ambient'] = 'Ambient';
$app_list_strings['inventory_item_storage_condition_list']['Room Temperature'] = 'Room Temperature';
$app_list_strings['inventory_item_storage_condition_list']['Refrigerate'] = 'Refrigerate';
$app_list_strings['inventory_item_storage_condition_list']['Frozen'] = 'Frozen';
$app_list_strings['inventory_item_storage_condition_list']['Ultra Frozen'] = 'Ultra Frozen';
$app_list_strings['inventory_item_storage_condition_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Purchase_Order.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['PO_Purchase_Order'] = 'Purchase Order';
$app_list_strings['moduleListSingular']['PO_Purchase_Order'] = 'Purchase Orders';
$app_list_strings['po_equipment_list']['Not Required'] = 'Not Required';
$app_list_strings['po_equipment_list']['Required Not Received'] = 'Required Not Received';
$app_list_strings['po_equipment_list']['Required and Received'] = 'Required and Received';
$app_list_strings['po_equipment_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Received_Items.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['RI_Received_Items'] = 'Received Items';
$app_list_strings['moduleListSingular']['RI_Received_Items'] = 'Received Item';
$app_list_strings['ri_type_2_list']['ORI'] = 'ORI';
$app_list_strings['ri_type_2_list']['POI'] = 'POI';
$app_list_strings['ri_type_2_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_purchase_unit_list.php

 // created: 2020-08-28 18:21:09

$app_list_strings['purchase_unit_list']=array (
  '' => '',
  'Each' => 'Each',
  'Bottle' => 'Bottle',
  'Box' => 'Box',
  'Case' => 'Case',
  'Pallet' => 'Pallet',
  'Roll' => 'Roll',
  'Sleeve' => 'Sleeve',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_oi_status_list.php

 // created: 2021-09-02 17:02:44

$app_list_strings['oi_status_list']=array (
  '' => '',
  'Backordered' => 'Backordered',
  'Inventory' => 'Fully Received',
  'Ordered' => 'Ordered',
  'Partially Received' => 'Partially Received',
  'Pending' => 'Pending',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_ORI_status_list.php

 // created: 2021-09-02 17:04:32

$app_list_strings['ORI_status_list']=array (
  '' => '',
  'Backordered' => 'Backordered',
  'Inventory' => 'Fully Received',
  'Ordered' => 'Ordered',
  'Partially Received' => 'Partially Received',
  'Requested' => 'Requested',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_poi_ori_related_to_list.php

 // created: 2021-02-16 18:31:28

$app_list_strings['poi_ori_related_to_list']=array (
  'Internal Use' => 'Internal Use',
  'Sales' => 'Sales Activity',
  'Work Product' => 'Work Product',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_POI_owner_list.php

 // created: 2021-02-25 09:21:46

$app_list_strings['POI_owner_list']=array (
  '' => '',
  'APS Owned' => 'APS Owned',
  'APS Supplied Client Owned' => 'APS Supplied, Client Owned',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_grant_submission_sa_2_list.php

 // created: 2021-10-26 09:03:27

$app_list_strings['grant_submission_sa_2_list']=array (
  '' => '',
  'DoD' => 'DoD',
  'NA' => 'N/A',
  'NIH' => 'NIH',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_reason_for_expansion_list.php

 // created: 2021-10-26 09:06:46

$app_list_strings['reason_for_expansion_list']=array (
  '' => '',
  'Invalid' => 'Invalid',
  'Equivocal' => 'Equivocal',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_bc_study_outcome_type_list.php

 // created: 2021-10-20 04:32:50

$app_list_strings['bc_study_outcome_type_list']=array (
  '' => '',
  'Completed Study' => 'Completed Study',
  'Passed Study' => 'Passed Study',
  'Failed Study' => 'Failed Study',
  'Expanded Study' => 'Expanded Study',
  'Discontinued Study' => 'Discontinued Study',
  'None' => 'None (Obsoleted 26Oct2021)',
  'Aborted Study' => 'Aborted Study (Obsoleted 26Oct2021)',
  'CAB Study' => 'CAB Study (Obsoleted 26Oct2021)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_gd_study_article_type_list.php

 // created: 2021-10-28 08:51:13

$app_list_strings['gd_study_article_type_list']=array (
  'Control' => 'Control',
  'Sham' => 'Sham',
  'Test' => 'Test',
  'Test Control' => 'Test & Control',
  'Other' => 'Other',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Inventory_Collection.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['IC_Inventory_Collection'] = 'Inventory Collections';
$app_list_strings['moduleListSingular']['IC_Inventory_Collection'] = 'Inventory Collection';
$app_list_strings['ic_related_to_list']['Internal Use'] = 'Internal Use';
$app_list_strings['ic_related_to_list']['Work Product'] = 'Work Product';
$app_list_strings['ic_related_to_list'][''] = '';
$app_list_strings['inventory_item_storage_medium_list']['4 Gluteraldehyde'] = '4% Gluteraldehyde';
$app_list_strings['inventory_item_storage_medium_list']['4 Paraformaldehyde'] = '4% Paraformaldehyde';
$app_list_strings['inventory_item_storage_medium_list']['10 Neutral Buffered Formalin'] = '10% Neutral Buffered Formalin';
$app_list_strings['inventory_item_storage_medium_list']['Davidsons Fixative'] = 'Davidson\'s Fixative';
$app_list_strings['inventory_item_storage_medium_list']['None'] = 'None';
$app_list_strings['inventory_item_storage_medium_list']['Normal Saline'] = 'Normal Saline';
$app_list_strings['inventory_item_storage_medium_list']['Water'] = 'Water';
$app_list_strings['inventory_item_storage_medium_list'][''] = '';
$app_list_strings['ic_current_storage_list']['Ambient'] = 'Ambient';
$app_list_strings['ic_current_storage_list']['Frozen'] = 'Frozen';
$app_list_strings['ic_current_storage_list']['Refrigerate'] = 'Refrigerate';
$app_list_strings['ic_current_storage_list']['Room Temperature'] = 'Room Temperature';
$app_list_strings['ic_current_storage_list']['Ultra Frozen'] = 'Ultra Frozen';
$app_list_strings['ic_current_storage_list'][''] = '';
$app_list_strings['location_list'][780] = '780 86th Ave.';
$app_list_strings['location_list'][8945] = '8945 Evergreen Blvd.';
$app_list_strings['location_list'][8960] = '8960 Evergreen Blvd.';
$app_list_strings['location_list'][9055] = '9055 Evergreen Blvd.';
$app_list_strings['location_list'][''] = '';
$app_list_strings['location_shelf_list'][1] = '1';
$app_list_strings['location_shelf_list'][''] = '';
$app_list_strings['location_cabinet_list'][1] = '1';
$app_list_strings['location_cabinet_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Inventory_Item.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['II_Inventory_Item'] = 'Inventory Items';
$app_list_strings['moduleListSingular']['II_Inventory_Item'] = 'Inventory Item';
$app_list_strings['related_to_list']['Internal Use'] = 'Internal Use';
$app_list_strings['related_to_list']['Multiple Test Systems'] = 'Multiple Test Systems';
$app_list_strings['related_to_list']['Sales'] = 'Sales';
$app_list_strings['related_to_list']['Single Test System'] = 'Single Test System';
$app_list_strings['related_to_list']['Work Product'] = 'Work Product';
$app_list_strings['related_to_list'][''] = '';
$app_list_strings['inventory_item_category_list']['Product'] = 'Product';
$app_list_strings['inventory_item_category_list']['Record'] = 'Record';
$app_list_strings['inventory_item_category_list']['Specimen'] = 'Specimen';
$app_list_strings['inventory_item_category_list']['Study Article'] = 'Study Article';
$app_list_strings['inventory_item_category_list'][''] = '';
$app_list_strings['inventory_item_type_list']['Accessory Article'] = 'Accessory Article';
$app_list_strings['inventory_item_type_list']['Block'] = 'Block';
$app_list_strings['inventory_item_type_list']['Control Article'] = 'Control Article';
$app_list_strings['inventory_item_type_list']['Culture'] = 'Culture';
$app_list_strings['inventory_item_type_list']['Data Book'] = 'Data Book';
$app_list_strings['inventory_item_type_list']['Data Sheet'] = 'Data Sheet';
$app_list_strings['inventory_item_type_list']['Equipment Facility Record'] = 'Equipment / Facility Record';
$app_list_strings['inventory_item_type_list']['Extract'] = 'Extract';
$app_list_strings['inventory_item_type_list']['Fecal'] = 'Fecal';
$app_list_strings['inventory_item_type_list']['Hard Drive'] = 'Hard Drive';
$app_list_strings['inventory_item_type_list']['Plasma'] = 'Plasma';
$app_list_strings['inventory_item_type_list']['Protocol Book'] = 'Protocol Book';
$app_list_strings['inventory_item_type_list']['Serum'] = 'Serum';
$app_list_strings['inventory_item_type_list']['Slide'] = 'Slide';
$app_list_strings['inventory_item_type_list']['Test Article'] = 'Test Article';
$app_list_strings['inventory_item_type_list']['Tissue'] = 'Tissue';
$app_list_strings['inventory_item_type_list']['Urine'] = 'Urine';
$app_list_strings['inventory_item_type_list']['Whole Blood'] = 'Whole Blood';
$app_list_strings['inventory_item_type_list'][''] = '';
$app_list_strings['inventory_item_owner_list']['APS'] = 'APS Owned';
$app_list_strings['inventory_item_owner_list']['APS Supplied Client Owned'] = 'APS Supplied, Client Owned';
$app_list_strings['inventory_item_owner_list']['Client'] = 'Client Owned';
$app_list_strings['inventory_item_owner_list'][''] = '';
$app_list_strings['inventory_item_status_list']['Planned Inventory'] = 'Planned Inventory';
$app_list_strings['inventory_item_status_list']['Onsite Inventory'] = 'Onsite Inventory';
$app_list_strings['inventory_item_status_list']['Offsite Inventory'] = 'Offsite Inventory';
$app_list_strings['inventory_item_status_list']['Onsite Archived'] = 'Onsite Archived';
$app_list_strings['inventory_item_status_list']['Offsite Archived'] = 'Offsite Archived';
$app_list_strings['inventory_item_status_list']['Discarded'] = 'Discarded';
$app_list_strings['inventory_item_status_list']['Obsolete'] = 'Obsolete';
$app_list_strings['inventory_item_status_list']['Invoiced'] = 'Invoiced';
$app_list_strings['inventory_item_status_list'][''] = '';
$app_list_strings['unique_id_type_list']['Internal'] = 'Internal Only';
$app_list_strings['unique_id_type_list']['External'] = 'External & Internal';
$app_list_strings['unique_id_type_list'][''] = '';
$app_list_strings['inventory_item_sterilization_list']['ETO Sterilized'] = 'ETO Sterilized';
$app_list_strings['inventory_item_sterilization_list']['Gamma Sterilized'] = 'Gamma Sterilized';
$app_list_strings['inventory_item_sterilization_list']['Non sterile'] = 'Non-sterile';
$app_list_strings['inventory_item_sterilization_list']['Steam Sterilized'] = 'Steam Sterilized';
$app_list_strings['inventory_item_sterilization_list'][''] = '';
$app_list_strings['timepoint_type_list']['Ad Hoc'] = 'Ad Hoc';
$app_list_strings['timepoint_type_list']['Defined'] = 'Defined';
$app_list_strings['timepoint_type_list']['None'] = 'None';
$app_list_strings['timepoint_type_list'][''] = '';
$app_list_strings['processing_method_list'][1] = '1';
$app_list_strings['processing_method_list'][''] = '';
$app_list_strings['expiration_test_article_list']['Known'] = 'Known';
$app_list_strings['expiration_test_article_list']['Pending'] = 'Pending';
$app_list_strings['expiration_test_article_list']['Unknown'] = 'Unknown';
$app_list_strings['expiration_test_article_list'][''] = '';
$app_list_strings['inventory_item_storage_condition_list']['Ambient Temperature'] = 'Ambient Temperature';
$app_list_strings['inventory_item_storage_condition_list']['Frozen'] = 'Frozen';
$app_list_strings['inventory_item_storage_condition_list']['Refrigerated'] = 'Refrigerated';
$app_list_strings['inventory_item_storage_condition_list']['Room Temperature'] = 'Room Temperature';
$app_list_strings['inventory_item_storage_condition_list']['Ultra Frozen in Freezer'] = 'Ultra Frozen in Freezer';
$app_list_strings['inventory_item_storage_condition_list']['Ultra Frozen in Liquid Nitrogen'] = 'Ultra Frozen in Liquid Nitrogen';
$app_list_strings['inventory_item_storage_condition_list'][''] = '';
$app_list_strings['inventory_item_storage_medium_list']['4 Gluteraldehyde'] = '4% Gluteraldehyde';
$app_list_strings['inventory_item_storage_medium_list']['4 Paraformaldehyde'] = '4% Paraformaldehyde';
$app_list_strings['inventory_item_storage_medium_list']['10 Neutral Buffered Formalin'] = '10% Neutral Buffered Formalin';
$app_list_strings['inventory_item_storage_medium_list']['Davidsons Fixative'] = 'Davidson\'s Fixative';
$app_list_strings['inventory_item_storage_medium_list']['None'] = 'None';
$app_list_strings['inventory_item_storage_medium_list']['Normal Saline'] = 'Normal Saline';
$app_list_strings['inventory_item_storage_medium_list']['Water'] = 'Water';
$app_list_strings['inventory_item_storage_medium_list'][''] = '';
$app_list_strings['im_location_type_list']['Equipment'] = 'Equipment';
$app_list_strings['im_location_type_list']['Room'] = 'Room';
$app_list_strings['im_location_type_list']['RoomCabinet'] = 'Room/Cabinet';
$app_list_strings['im_location_type_list']['RoomShelf'] = 'Room/Shelf';
$app_list_strings['im_location_type_list']['Cabinet'] = 'Cabinet';
$app_list_strings['im_location_type_list']['Shelf'] = 'Shelf';
$app_list_strings['im_location_type_list'][''] = '';
$app_list_strings['location_list'][780] = '780 86th Ave.';
$app_list_strings['location_list'][8945] = '8945 Evergreen Blvd.';
$app_list_strings['location_list'][8960] = '8960 Evergreen Blvd.';
$app_list_strings['location_list'][9055] = '9055 Evergreen Blvd.';
$app_list_strings['location_list'][''] = '';
$app_list_strings['location_shelf_list'][1] = '1';
$app_list_strings['location_shelf_list'][''] = '';
$app_list_strings['location_cabinet_list'][1] = '1';
$app_list_strings['location_cabinet_list'][''] = '';
$app_list_strings['ii_related_to_c_list']['Internal Use'] = 'Internal Use';
$app_list_strings['ii_related_to_c_list']['Multiple Test Systems'] = 'Multiple Test Systems';
$app_list_strings['ii_related_to_c_list']['Order Request Item'] = 'Order Request Item';
$app_list_strings['ii_related_to_c_list']['Purchase Order Item'] = 'Purchase Order Item';
$app_list_strings['ii_related_to_c_list']['Sales'] = 'Sales';
$app_list_strings['ii_related_to_c_list']['Single Test System'] = 'Single Test System';
$app_list_strings['ii_related_to_c_list']['Work Product'] = 'Work Product';
$app_list_strings['ii_related_to_c_list'][''] = '';
$app_list_strings['product_category_list']['Balloon Catheter'] = 'Balloon Catheter';
$app_list_strings['product_category_list']['Cannula'] = 'Cannula';
$app_list_strings['product_category_list']['Catheter'] = 'Catheter';
$app_list_strings['product_category_list']['Chemical'] = 'Chemical';
$app_list_strings['product_category_list']['Cleaning Agent'] = 'Cleaning Agent';
$app_list_strings['product_category_list']['Contrast'] = 'Contrast';
$app_list_strings['product_category_list']['Drug'] = 'Drug';
$app_list_strings['product_category_list']['Equipment'] = 'Equipment';
$app_list_strings['product_category_list']['Graft'] = 'Graft';
$app_list_strings['product_category_list']['Inflation Device'] = 'Inflation Device';
$app_list_strings['product_category_list']['Reagent'] = 'Reagent';
$app_list_strings['product_category_list']['Sheath'] = 'Sheath';
$app_list_strings['product_category_list']['Solution'] = 'Solution';
$app_list_strings['product_category_list']['Stent'] = 'Stent';
$app_list_strings['product_category_list']['Suture'] = 'Suture';
$app_list_strings['product_category_list']['Wire'] = 'Wire';
$app_list_strings['product_category_list'][''] = '';
$app_list_strings['inventory_item_subtype_list']['Sample Prep BAS logs for equipmentrooms'] = 'Sample Prep BAS logs for equipment/rooms';
$app_list_strings['inventory_item_subtype_list']['Stock Animal Weights'] = 'Stock Animal Weights';
$app_list_strings['inventory_item_subtype_list']['Analytical Calibration Check for Adjustable Pipettes'] = 'Analytical - Calibration Check for Adjustable Pipettes';
$app_list_strings['inventory_item_subtype_list']['8960 Room Logs'] = '8960 Room Logs';
$app_list_strings['inventory_item_subtype_list']['OR3 OR4 OR5 Daily Maintenance Checklist'] = 'OR3 OR4 OR5 Daily Maintenance Checklist';
$app_list_strings['inventory_item_subtype_list']['CL1 CL2 CL3 CL4 Daily Maintenance Checklist'] = 'CL1 CL2 CL3 CL4 Daily Maintenance Checklist';
$app_list_strings['inventory_item_subtype_list']['8945 Prep Daily Maintenance Checklist'] = '8945 Prep Daily Maintenance Checklist';
$app_list_strings['inventory_item_subtype_list']['Cycle Log'] = 'Cycle Log';
$app_list_strings['inventory_item_subtype_list']['Cycle Printer Output'] = 'Cycle Printer Output';
$app_list_strings['inventory_item_subtype_list']['Biological Sample Test Log'] = 'Biological Sample Test Log';
$app_list_strings['inventory_item_subtype_list']['Siemens Fluroroscopy Patient and Leonardo Workstation Log'] = 'Siemens Fluroroscopy Patient and Leonardo Workstation Log';
$app_list_strings['inventory_item_subtype_list']['Change of DICOM Metadata Notification'] = 'Change of DICOM Metadata Notification';
$app_list_strings['inventory_item_subtype_list']['Baxter COM2 Cardiac Output Computer Data Collection'] = 'Baxter COM-2 Cardiac Output Computer Data Collection';
$app_list_strings['inventory_item_subtype_list']['Verification of the Cardiopulmonary Bypass Perfusion Systems Roller Head'] = 'Verification of the Cardiopulmonary Bypass Perfusion Systems Roller Head';
$app_list_strings['inventory_item_subtype_list']['8945 8960 Animal Room Temp Reports'] = '8945 & 8960 Animal Room Temp Reports';
$app_list_strings['inventory_item_subtype_list']['8945 8960 9055 Data Storage Temp Reports'] = '8945, 8960, 9055 Data Storage Temp Reports';
$app_list_strings['inventory_item_subtype_list']['8945 80 Freezer Store Room Refrigerator Temp Report'] = '8945 -80 Freezer & Store Room Refrigerator Temp Report';
$app_list_strings['inventory_item_subtype_list']['9055 Controlled Receiving Temp Report'] = '9055 Controlled Receiving Temp Report';
$app_list_strings['inventory_item_subtype_list']['Hood Monitoring Logs'] = 'Hood Monitoring Logs';
$app_list_strings['inventory_item_subtype_list']['Sample Prep Equipment Monitoring Logs'] = 'Sample Prep Equipment Monitoring Logs';
$app_list_strings['inventory_item_subtype_list']['Sample Prep RefrigeratorFreezer Logs'] = 'Sample Prep Refrigerator/Freezer Logs';
$app_list_strings['inventory_item_subtype_list']['Sample Prep Microbiological Mointoring Forms'] = 'Sample Prep Microbiological Mointoring Forms';
$app_list_strings['inventory_item_subtype_list']['Sterilizer Receipts'] = 'Sterilizer Receipts';
$app_list_strings['inventory_item_subtype_list']['Cagewash Tracking'] = 'Cagewash Tracking';
$app_list_strings['inventory_item_subtype_list']['8960 Feed Receipt'] = '8960 Feed Receipt';
$app_list_strings['inventory_item_subtype_list']['8960 Environmental Monitoring'] = '8960 Environmental Monitoring';
$app_list_strings['inventory_item_subtype_list']['Reagent Usage Log'] = 'Reagent Usage Log';
$app_list_strings['inventory_item_subtype_list']['Solution Preparation Log'] = 'Solution Preparation Log';
$app_list_strings['inventory_item_subtype_list']['Pathology Refrigerator Freezer Temperature Log'] = 'Pathology Refrigerator & Freezer Temperature Log';
$app_list_strings['inventory_item_subtype_list']['Histology Oven Temperature'] = 'Histology Oven Temperature';
$app_list_strings['inventory_item_subtype_list']['Cryostat Temperature Log'] = 'Cryostat Temperature Log';
$app_list_strings['inventory_item_subtype_list']['Hazardous Waste Inspection Log'] = 'Hazardous Waste Inspection Log';
$app_list_strings['inventory_item_subtype_list']['Tissue Processor Log'] = 'Tissue Processor Log';
$app_list_strings['inventory_item_subtype_list']['SpeedVac Use Log'] = 'SpeedVac Use Log';
$app_list_strings['inventory_item_subtype_list']['Sterilizer Cycle Log'] = 'Sterilizer Cycle Log';
$app_list_strings['inventory_item_subtype_list']['Depyrogenator Cycle Log'] = 'Depyrogenator Cycle Log';
$app_list_strings['inventory_item_subtype_list']['Dishwasher Cycle Log'] = 'Dishwasher Cycle Log';
$app_list_strings['inventory_item_subtype_list']['Depyrogenator Cycle Printouts'] = 'Depyrogenator Cycle Printouts';
$app_list_strings['inventory_item_subtype_list']['BalanceScale Logs'] = 'Balance/Scale Logs';
$app_list_strings['inventory_item_subtype_list']['CaliperGage Block Logs'] = 'Caliper/Gage Block Logs';
$app_list_strings['inventory_item_subtype_list']['FTIR Sample Analysis Form'] = 'FTIR Sample Analysis Form';
$app_list_strings['inventory_item_subtype_list']['Analytical Refrigerator and Freezer Temperature Log'] = 'Analytical Refrigerator and Freezer Temperature Log';
$app_list_strings['inventory_item_subtype_list']['Analytical Routine Equipment Maintenance Log'] = 'Analytical Routine Equipment Maintenance Log';
$app_list_strings['inventory_item_subtype_list']['Analtyical Facilty Equipment Log'] = 'Analtyical Facilty Equipment Log';
$app_list_strings['inventory_item_subtype_list']['Controlled Substances Log'] = 'Controlled Substances Log';
$app_list_strings['inventory_item_subtype_list']['8945 TempHumidity Logs feed room fridgefreezers Animal Care'] = '8945 Temp/Humidity Logs (feed room & fridge/freezers - Animal Care)';
$app_list_strings['inventory_item_subtype_list']['8945 Micro monitoring logs'] = '8945 Micro monitoring logs';
$app_list_strings['inventory_item_subtype_list']['Feed Receipt Logs'] = 'Feed Receipt Logs';
$app_list_strings['inventory_item_subtype_list']['MaintenanceTemp Records 8960'] = 'Maintenance/Temp Records - 8960';
$app_list_strings['inventory_item_subtype_list']['Hemavet'] = 'Hemavet';
$app_list_strings['inventory_item_subtype_list']['8945 Recovery Fridge'] = '8945 Recovery Fridge';
$app_list_strings['inventory_item_subtype_list']['8945 Ancillary Equipment Sanitation'] = '8945 Ancillary Equipment Sanitation';
$app_list_strings['inventory_item_subtype_list']['8960 Microbiological Monitoring'] = '8960 Microbiological Monitoring';
$app_list_strings['inventory_item_subtype_list']['Formalin Recycling Form'] = 'Formalin Recycling Form';
$app_list_strings['inventory_item_subtype_list']['Aldehyde Solution Neutralizing Form'] = 'Aldehyde Solution Neutralizing Form';
$app_list_strings['inventory_item_subtype_list']['Perfusion Tank Log'] = 'Perfusion Tank Log';
$app_list_strings['inventory_item_subtype_list']['Balance and Scale Validation Logs'] = 'Balance and Scale Validation Logs';
$app_list_strings['inventory_item_subtype_list']['Freezer Inventory Log'] = 'Freezer Inventory Log';
$app_list_strings['inventory_item_subtype_list']['Calibration Records 8960'] = 'Calibration Records - 8960';
$app_list_strings['inventory_item_subtype_list']['Biologicals for Autoclave 8960'] = 'Biologicals for Autoclave - 8960';
$app_list_strings['inventory_item_subtype_list']['CP Slides IVT'] = 'CP Slides (IVT)';
$app_list_strings['inventory_item_subtype_list']['IVT Equipment Monitoring Logs'] = 'IVT Equipment Monitoring Logs';
$app_list_strings['inventory_item_subtype_list']['IVT Montitoring System Temp Graphs'] = 'IVT Montitoring System Temp Graphs';
$app_list_strings['inventory_item_subtype_list']['8945 8960 RV Filter Change Logs'] = '8945, 8960, RV Filter Change Logs';
$app_list_strings['inventory_item_subtype_list']['8945 8960 9055 Generator Operation Checklist'] = '8945, 8960, 9055 Generator Operation Checklist';
$app_list_strings['inventory_item_subtype_list']['Animal Receipt Logs'] = 'Animal Receipt Logs';
$app_list_strings['inventory_item_subtype_list']['8960 Weekly Maintenance Shower Eyewash'] = '8960 Weekly Maintenance (Shower & Eyewash)';
$app_list_strings['inventory_item_subtype_list']['Analytical Services pH Calibration Log'] = 'Analytical Services pH Calibration Log';
$app_list_strings['inventory_item_subtype_list']['Analytical Services Solution Log Book Stock Solutions'] = 'Analytical Services Solution Log Book- Stock Solutions';
$app_list_strings['inventory_item_subtype_list']['Analytical Services Solution Log Book Stock Dilutions'] = 'Analytical Services Solution Log Book- Stock Dilutions';
$app_list_strings['inventory_item_subtype_list']['Analytical Services Solution Log Book Standard Curves'] = 'Analytical Services Solution Log Book- Standard Curves';
$app_list_strings['inventory_item_subtype_list']['Analytical Services Solution Log Book Buffers Mobile Phases Other'] = 'Analytical Services Solution Log Book- Buffers, Mobile Phases, Other';
$app_list_strings['inventory_item_subtype_list']['Analytical Services Solution Preparation'] = 'Analytical Services Solution Preparation';
$app_list_strings['inventory_item_subtype_list']['Analytical Services Standard Curve Spiking Solution Preparation'] = 'Analytical Services Standard Curve Spiking Solution Preparation';
$app_list_strings['inventory_item_subtype_list']['LAL Standard Curves'] = 'LAL Standard Curves';
$app_list_strings['inventory_item_subtype_list']['CascadeBFTII QC'] = 'Cascade/BFTII QC';
$app_list_strings['inventory_item_subtype_list']['Spectrophotometer Weekly Verification'] = 'Spectrophotometer Weekly Verification';
$app_list_strings['inventory_item_subtype_list']['Solution Preparation Documents'] = 'Solution Preparation Documents';
$app_list_strings['inventory_item_subtype_list']['Cell SplittingMedia Change Forms Mouse Lymphoma Balb L929 V79 THP Chok1 other cell lines'] = 'Cell Splitting/Media Change Forms - Mouse Lymphoma, Balb, L929, V79, THP, Chok1, other cell lines';
$app_list_strings['inventory_item_subtype_list']['Cell Plating Records'] = 'Cell Plating Records';
$app_list_strings['inventory_item_subtype_list']['Serum Heat Inactivation'] = 'Serum Heat Inactivation';
$app_list_strings['inventory_item_subtype_list']['ProtoCOL3 Colony Counter Validation Records'] = 'ProtoCOL3 Colony Counter Validation Records';
$app_list_strings['inventory_item_subtype_list']['Cell Revival Records'] = 'Cell Revival Records';
$app_list_strings['inventory_item_subtype_list']['Aliquot Creation Logs'] = 'Aliquot Creation Logs';
$app_list_strings['inventory_item_subtype_list']['Flow Cytometer CST maintenance'] = 'Flow Cytometer CST / maintenance';
$app_list_strings['inventory_item_subtype_list']['Moxi'] = 'Moxi';
$app_list_strings['inventory_item_subtype_list']['Blood Loop Controls'] = 'Blood Loop Controls';
$app_list_strings['inventory_item_subtype_list']['iSTAT Validation'] = 'iSTAT Validation';
$app_list_strings['inventory_item_subtype_list']['Scale Validation'] = 'Scale Validation';
$app_list_strings['inventory_item_subtype_list']['Glove Box'] = 'Glove Box';
$app_list_strings['inventory_item_subtype_list']['Hazardous Waste Log'] = 'Hazardous Waste Log';
$app_list_strings['inventory_item_subtype_list']['Osmometer Log'] = 'Osmometer Log';
$app_list_strings['inventory_item_subtype_list']['pH Log'] = 'pH Log';
$app_list_strings['inventory_item_subtype_list']['Equipment Routine Maintenance'] = 'Equipment Routine Maintenance';
$app_list_strings['inventory_item_subtype_list'][''] = '';
$app_list_strings['tissues_list']['Adrenal Gland LT'] = 'Adrenal Gland - LT';
$app_list_strings['tissues_list']['Adrenal Gland RT'] = 'Adrenal Gland - RT';
$app_list_strings['tissues_list']['Bone Marrow Femur'] = 'Bone Marrow - Femur';
$app_list_strings['tissues_list']['Bone Marrow Rib'] = 'Bone Marrow - Rib';
$app_list_strings['tissues_list']['Bone Marrow Sternum'] = 'Bone Marrow - Sternum';
$app_list_strings['tissues_list']['Brain'] = 'Brain';
$app_list_strings['tissues_list']['Brain Cerebellum'] = 'Brain - Cerebellum';
$app_list_strings['tissues_list']['Brain Cerebrum'] = 'Brain - Cerebrum';
$app_list_strings['tissues_list']['Brainstem'] = 'Brainstem';
$app_list_strings['tissues_list']['Device'] = 'Device';
$app_list_strings['tissues_list']['Esophageal Sphincter'] = 'Esophageal Sphincter';
$app_list_strings['tissues_list']['Esophagus'] = 'Esophagus';
$app_list_strings['tissues_list']['Eye LT'] = 'Eye - LT';
$app_list_strings['tissues_list']['Eye RT'] = 'Eye - RT';
$app_list_strings['tissues_list']['Gallbladder'] = 'Gallbladder';
$app_list_strings['tissues_list']['Heart'] = 'Heart';
$app_list_strings['tissues_list']['Heart Aortic Valve'] = 'Heart - Aortic Valve';
$app_list_strings['tissues_list']['Heart Atrial Septum'] = 'Heart - Atrial Septum';
$app_list_strings['tissues_list']['Heart Coronary Sinus'] = 'Heart - Coronary Sinus';
$app_list_strings['tissues_list']['Heart IVS'] = 'Heart - IVS';
$app_list_strings['tissues_list']['Heart LA'] = 'Heart - LA';
$app_list_strings['tissues_list']['Heart LV'] = 'Heart - LV';
$app_list_strings['tissues_list']['Heart LVRVIVS'] = 'Heart - LV/RV/IVS';
$app_list_strings['tissues_list']['Heart Mitral Valve'] = 'Heart - Mitral Valve';
$app_list_strings['tissues_list']['Heart Pulmonary Valve'] = 'Heart - Pulmonary Valve';
$app_list_strings['tissues_list']['Heart RA'] = 'Heart - RA';
$app_list_strings['tissues_list']['Heart RV'] = 'Heart - RV';
$app_list_strings['tissues_list']['Heart Tricuspid Valve'] = 'Heart - Tricuspid Valve';
$app_list_strings['tissues_list']['Implant Site'] = 'Implant Site';
$app_list_strings['tissues_list']['Implant Site 1'] = 'Implant Site - 1';
$app_list_strings['tissues_list']['Implant Site 10'] = 'Implant Site - 10';
$app_list_strings['tissues_list']['Implant Site 11'] = 'Implant Site - 11';
$app_list_strings['tissues_list']['Implant Site 12'] = 'Implant Site - 12';
$app_list_strings['tissues_list']['Implant Site 2'] = 'Implant Site - 2';
$app_list_strings['tissues_list']['Implant Site 3'] = 'Implant Site - 3';
$app_list_strings['tissues_list']['Implant Site 4'] = 'Implant Site - 4';
$app_list_strings['tissues_list']['Implant Site 5'] = 'Implant Site - 5';
$app_list_strings['tissues_list']['Implant Site 6'] = 'Implant Site - 6';
$app_list_strings['tissues_list']['Implant Site 7'] = 'Implant Site - 7';
$app_list_strings['tissues_list']['Implant Site 8'] = 'Implant Site - 8';
$app_list_strings['tissues_list']['Implant Site 9'] = 'Implant Site - 9';
$app_list_strings['tissues_list']['Implant Site LT'] = 'Implant Site - LT';
$app_list_strings['tissues_list']['Implant Site LT Caud'] = 'Implant Site - LT Caud';
$app_list_strings['tissues_list']['Implant Site LT Cran'] = 'Implant Site - LT Cran';
$app_list_strings['tissues_list']['Implant Site LT Mid'] = 'Implant Site - LT Mid';
$app_list_strings['tissues_list']['Implant Site RT'] = 'Implant Site - RT';
$app_list_strings['tissues_list']['Implant Site RT Caud'] = 'Implant Site - RT Caud';
$app_list_strings['tissues_list']['Implant Site RT Cran'] = 'Implant Site - RT Cran';
$app_list_strings['tissues_list']['Implant Site RT Mid'] = 'Implant Site - RT Mid';
$app_list_strings['tissues_list']['Intestine Large'] = 'Intestine, Large';
$app_list_strings['tissues_list']['Intestine Large Cecum'] = 'Intestine, Large - Cecum';
$app_list_strings['tissues_list']['Intestine Large Colon'] = 'Intestine, Large - Colon';
$app_list_strings['tissues_list']['Intestine Large Rectum'] = 'Intestine, Large - Rectum';
$app_list_strings['tissues_list']['Intestine Small'] = 'Intestine, Small';
$app_list_strings['tissues_list']['Intestine Small Duodenum'] = 'Intestine, Small - Duodenum';
$app_list_strings['tissues_list']['Intestine Small Ileum'] = 'Intestine, Small - Ileum';
$app_list_strings['tissues_list']['Intestine Small Jejunum'] = 'Intestine, Small - Jejunum';
$app_list_strings['tissues_list']['Kidney LT'] = 'Kidney, LT';
$app_list_strings['tissues_list']['Kidney LT Caud'] = 'Kidney, LT - Caud';
$app_list_strings['tissues_list']['Kidney LT Cran'] = 'Kidney, LT - Cran';
$app_list_strings['tissues_list']['Kidney LT Mid'] = 'Kidney, LT - Mid';
$app_list_strings['tissues_list']['Kidney RT Caud'] = 'Kidney, RT - Caud';
$app_list_strings['tissues_list']['Kidney RT Cran'] = 'Kidney, RT - Cran';
$app_list_strings['tissues_list']['Kidney RT Mid'] = 'Kidney, RT - Mid';
$app_list_strings['tissues_list']['Kidney RT Whole'] = 'Kidney, RT - Whole';
$app_list_strings['tissues_list']['Liver'] = 'Liver';
$app_list_strings['tissues_list']['Liver Caudate Lobe'] = 'Liver - Caudate Lobe';
$app_list_strings['tissues_list']['Liver LT Lat Lobe'] = 'Liver - LT Lat Lobe';
$app_list_strings['tissues_list']['Liver LT Med Lobe'] = 'Liver - LT Med Lobe';
$app_list_strings['tissues_list']['Liver Quadrate Lobe'] = 'Liver - Quadrate Lobe';
$app_list_strings['tissues_list']['Liver RT Lat Lobe'] = 'Liver - RT Lat Lobe';
$app_list_strings['tissues_list']['Liver RT Med Lobe'] = 'Liver - RT Med Lobe';
$app_list_strings['tissues_list']['Liver RT Med Lobe wGallbladder'] = 'Liver - RT Med Lobe w/Gallbladder';
$app_list_strings['tissues_list']['LN Hepatic'] = 'LN - Hepatic';
$app_list_strings['tissues_list']['LN LT Axillary'] = 'LN - LT Axillary';
$app_list_strings['tissues_list']['LN LT Cervical'] = 'LN - LT Cervical';
$app_list_strings['tissues_list']['LN LT Iliac'] = 'LN - LT Iliac';
$app_list_strings['tissues_list']['LN LT Inguinal'] = 'LN - LT Inguinal';
$app_list_strings['tissues_list']['LN LT Lumbar'] = 'LN - LT Lumbar';
$app_list_strings['tissues_list']['LN LT Popliteal'] = 'LN - LT Popliteal';
$app_list_strings['tissues_list']['LN LT Renal'] = 'LN - LT Renal';
$app_list_strings['tissues_list']['LN Mediastinal'] = 'LN - Mediastinal';
$app_list_strings['tissues_list']['LN Mesenteric'] = 'LN - Mesenteric';
$app_list_strings['tissues_list']['LN Pericardial'] = 'LN - Pericardial';
$app_list_strings['tissues_list']['LN RT Axillary'] = 'LN - RT Axillary';
$app_list_strings['tissues_list']['LN RT Cervical'] = 'LN - RT Cervical';
$app_list_strings['tissues_list']['LN RT Iliac'] = 'LN - RT Iliac';
$app_list_strings['tissues_list']['LN RT Inguinal'] = 'LN - RT Inguinal';
$app_list_strings['tissues_list']['LN RT Lumbar'] = 'LN - RT Lumbar';
$app_list_strings['tissues_list']['LN RT Popliteal'] = 'LN - RT Popliteal';
$app_list_strings['tissues_list']['LN RT Renal'] = 'LN - RT Renal';
$app_list_strings['tissues_list']['LN Subaortic'] = 'LN - Subaortic';
$app_list_strings['tissues_list']['LN Tracheobronchial'] = 'LN - Tracheobronchial';
$app_list_strings['tissues_list']['Lungs'] = 'Lungs';
$app_list_strings['tissues_list']['Lungs Accessory'] = 'Lungs - Accessory';
$app_list_strings['tissues_list']['Lungs LT Caud'] = 'Lungs - LT Caud';
$app_list_strings['tissues_list']['Lungs LT Cran'] = 'Lungs - LT Cran';
$app_list_strings['tissues_list']['Lungs LT Cran Caud Lobe'] = 'Lungs - LT Cran, Caud Lobe';
$app_list_strings['tissues_list']['Lungs LT Cran Cran Lobe'] = 'Lungs - LT Cran, Cran Lobe';
$app_list_strings['tissues_list']['Lungs RT Caud'] = 'Lungs - RT Caud';
$app_list_strings['tissues_list']['Lungs RT Cran'] = 'Lungs - RT Cran';
$app_list_strings['tissues_list']['Lungs RT Cran Caud Lobe'] = 'Lungs - RT Cran, Caud Lobe';
$app_list_strings['tissues_list']['Lungs RT Cran Cran Lobe'] = 'Lungs - RT Cran, Cran Lobe';
$app_list_strings['tissues_list']['Lungs RT Mid'] = 'Lungs - RT Mid';
$app_list_strings['tissues_list']['Mammary Gland'] = 'Mammary Gland';
$app_list_strings['tissues_list']['Nerve LT Phrenic'] = 'Nerve - LT Phrenic';
$app_list_strings['tissues_list']['Nerve LT Sciatic'] = 'Nerve - LT Sciatic';
$app_list_strings['tissues_list']['Nerve LT Vagus'] = 'Nerve - LT Vagus';
$app_list_strings['tissues_list']['Nerve RT Phrenic'] = 'Nerve - RT Phrenic';
$app_list_strings['tissues_list']['Nerve RT Sciatic'] = 'Nerve - RT Sciatic';
$app_list_strings['tissues_list']['Nerve RT Vagus'] = 'Nerve - RT Vagus';
$app_list_strings['tissues_list']['Pancreas'] = 'Pancreas';
$app_list_strings['tissues_list']['Pituitary Gland'] = 'Pituitary Gland';
$app_list_strings['tissues_list']['Pyloric Sphincter'] = 'Pyloric Sphincter';
$app_list_strings['tissues_list']['Repro Tract Epididymides'] = 'Repro Tract - Epididymides';
$app_list_strings['tissues_list']['Repro Tract LT Ovary'] = 'Repro Tract - LT Ovary';
$app_list_strings['tissues_list']['Repro Tract LT Teste'] = 'Repro Tract - LT Teste';
$app_list_strings['tissues_list']['Repro Tract LT Uterine Horn'] = 'Repro Tract - LT Uterine Horn';
$app_list_strings['tissues_list']['Repro Tract Ovaries'] = 'Repro Tract - Ovaries';
$app_list_strings['tissues_list']['Repro Tract Prostate'] = 'Repro Tract - Prostate';
$app_list_strings['tissues_list']['Repro Tract RT Ovary'] = 'Repro Tract - RT Ovary';
$app_list_strings['tissues_list']['Repro Tract RT Teste'] = 'Repro Tract - RT Teste';
$app_list_strings['tissues_list']['Repro Tract RT Uterine Horn'] = 'Repro Tract - RT Uterine Horn';
$app_list_strings['tissues_list']['Repro Tract Seminal Vesicles'] = 'Repro Tract - Seminal Vesicles';
$app_list_strings['tissues_list']['Repro Tract Testes'] = 'Repro Tract - Testes';
$app_list_strings['tissues_list']['Repro Tract Uterus'] = 'Repro Tract - Uterus';
$app_list_strings['tissues_list']['Repro Tract Vagina'] = 'Repro Tract - Vagina';
$app_list_strings['tissues_list']['Salivary Glands'] = 'Salivary Glands';
$app_list_strings['tissues_list']['Skeletal Muscle Biceps'] = 'Skeletal Muscle - Biceps';
$app_list_strings['tissues_list']['Skeletal Muscle Diaphragm'] = 'Skeletal Muscle - Diaphragm';
$app_list_strings['tissues_list']['Skin'] = 'Skin';
$app_list_strings['tissues_list']['Skin LT Caud'] = 'Skin - LT Caud';
$app_list_strings['tissues_list']['Skin LT Cran'] = 'Skin - LT Cran';
$app_list_strings['tissues_list']['Skin LT Mid'] = 'Skin - LT Mid';
$app_list_strings['tissues_list']['Skin Lumbar'] = 'Skin - Lumbar';
$app_list_strings['tissues_list']['Skin RT Caud'] = 'Skin - RT Caud';
$app_list_strings['tissues_list']['Skin RT Cran'] = 'Skin - RT Cran';
$app_list_strings['tissues_list']['Skin RT Mid'] = 'Skin - RT Mid';
$app_list_strings['tissues_list']['Skin Thoracic'] = 'Skin - Thoracic';
$app_list_strings['tissues_list']['Skull'] = 'Skull';
$app_list_strings['tissues_list']['Spinal Cord'] = 'Spinal Cord';
$app_list_strings['tissues_list']['Spinal Cord Cervical'] = 'Spinal Cord - Cervical';
$app_list_strings['tissues_list']['Spinal Cord Lumbar'] = 'Spinal Cord - Lumbar';
$app_list_strings['tissues_list']['Spinal Cord Thoracic'] = 'Spinal Cord - Thoracic';
$app_list_strings['tissues_list']['Spleen'] = 'Spleen';
$app_list_strings['tissues_list']['Spleen Dist'] = 'Spleen - Dist';
$app_list_strings['tissues_list']['Spleen Mid'] = 'Spleen - Mid';
$app_list_strings['tissues_list']['Spleen Prox'] = 'Spleen - Prox';
$app_list_strings['tissues_list']['Stomach'] = 'Stomach';
$app_list_strings['tissues_list']['Stomach Body'] = 'Stomach - Body';
$app_list_strings['tissues_list']['Stomach Cardia'] = 'Stomach - Cardia';
$app_list_strings['tissues_list']['Stomach Fundus'] = 'Stomach - Fundus';
$app_list_strings['tissues_list']['Stomach Pylorus'] = 'Stomach - Pylorus';
$app_list_strings['tissues_list']['Thymus'] = 'Thymus';
$app_list_strings['tissues_list']['Thyroid LT with Parathyroid'] = 'Thyroid - LT with Parathyroid';
$app_list_strings['tissues_list']['Thyroid RT with Parathyroid'] = 'Thyroid - RT with Parathyroid';
$app_list_strings['tissues_list']['Thyroid with Parathyroids'] = 'Thyroid - with Parathyroids';
$app_list_strings['tissues_list']['Thyroids with Parathyroids'] = 'Thyroids - with Parathyroids';
$app_list_strings['tissues_list']['Tongue'] = 'Tongue';
$app_list_strings['tissues_list']['Trachea'] = 'Trachea';
$app_list_strings['tissues_list']['Trachea Dist'] = 'Trachea - Dist';
$app_list_strings['tissues_list']['Trachea Mid'] = 'Trachea - Mid';
$app_list_strings['tissues_list']['Trachea Prox'] = 'Trachea - Prox';
$app_list_strings['tissues_list']['Ureter'] = 'Ureter';
$app_list_strings['tissues_list']['Ureter LT'] = 'Ureter, LT';
$app_list_strings['tissues_list']['Ureter RT'] = 'Ureter, RT';
$app_list_strings['tissues_list']['Urethra'] = 'Urethra';
$app_list_strings['tissues_list']['Urethra Dist'] = 'Urethra - Dist';
$app_list_strings['tissues_list']['Urethra Mid'] = 'Urethra - Mid';
$app_list_strings['tissues_list']['Urethra Prox'] = 'Urethra - Prox';
$app_list_strings['tissues_list']['Urinary Bladder'] = 'Urinary Bladder';
$app_list_strings['tissues_list']['Vessel Aorta'] = 'Vessel - Aorta';
$app_list_strings['tissues_list']['Vessel Aorta Dist'] = 'Vessel - Aorta (Dist)';
$app_list_strings['tissues_list']['Vessel Aorta Prox'] = 'Vessel - Aorta (Prox)';
$app_list_strings['tissues_list']['Vessel Aorta Abdominal'] = 'Vessel - Aorta, Abdominal';
$app_list_strings['tissues_list']['Vessel Aorta Ascending'] = 'Vessel - Aorta, Ascending';
$app_list_strings['tissues_list']['Vessel Aorta Descending'] = 'Vessel - Aorta, Descending';
$app_list_strings['tissues_list']['Vessel Aorta Dist Ref'] = 'Vessel - Aorta, Dist Ref';
$app_list_strings['tissues_list']['Vessel Aorta Prox Ref'] = 'Vessel - Aorta, Prox Ref';
$app_list_strings['tissues_list']['Vessel Aorta Thoracic'] = 'Vessel - Aorta, Thoracic';
$app_list_strings['tissues_list']['Vessel Aortic Arch'] = 'Vessel - Aortic Arch';
$app_list_strings['tissues_list']['Vessel Axillary LT'] = 'Vessel - Axillary, LT';
$app_list_strings['tissues_list']['Vessel Axillary RT'] = 'Vessel - Axillary, RT';
$app_list_strings['tissues_list']['Vessel Carotid Dist LT'] = 'Vessel - Carotid (Dist), LT';
$app_list_strings['tissues_list']['Vessel Carotid Dist RT'] = 'Vessel - Carotid (Dist), RT';
$app_list_strings['tissues_list']['Vessel Carotid Prox LT'] = 'Vessel - Carotid (Prox), LT';
$app_list_strings['tissues_list']['Vessel Carotid Prox RT'] = 'Vessel - Carotid (Prox), RT';
$app_list_strings['tissues_list']['Vessel Carotid LT'] = 'Vessel - Carotid, LT';
$app_list_strings['tissues_list']['Vessel Carotid LT Dist Ref'] = 'Vessel - Carotid, LT Dist Ref';
$app_list_strings['tissues_list']['Vessel Carotid LT Prox Ref'] = 'Vessel - Carotid, LT Prox Ref';
$app_list_strings['tissues_list']['Vessel Carotid RT'] = 'Vessel - Carotid, RT';
$app_list_strings['tissues_list']['Vessel Carotid RT Dist Ref'] = 'Vessel - Carotid, RT Dist Ref';
$app_list_strings['tissues_list']['Vessel Carotid RT Prox Ref'] = 'Vessel - Carotid, RT Prox Ref';
$app_list_strings['tissues_list']['Vessel Costocervical LT'] = 'Vessel - Costocervical, LT';
$app_list_strings['tissues_list']['Vessel Costocervical RT'] = 'Vessel - Costocervical, RT';
$app_list_strings['tissues_list']['Vessel IVC'] = 'Vessel - IVC';
$app_list_strings['tissues_list']['Vessel Jugular LT Ext'] = 'Vessel - Jugular, LT Ext';
$app_list_strings['tissues_list']['Vessel Jugular LT Int'] = 'Vessel - Jugular, LT Int';
$app_list_strings['tissues_list']['Vessel Jugular RT Ext'] = 'Vessel - Jugular, RT Ext';
$app_list_strings['tissues_list']['Vessel Jugular RT Int'] = 'Vessel - Jugular, RT Int';
$app_list_strings['tissues_list']['Vessel LAD'] = 'Vessel - LAD';
$app_list_strings['tissues_list']['Vessel LAD Dist'] = 'Vessel - LAD (Dist)';
$app_list_strings['tissues_list']['Vessel LAD Prox'] = 'Vessel - LAD (Prox)';
$app_list_strings['tissues_list']['Vessel LAD Dist Ref'] = 'Vessel - LAD, Dist Ref';
$app_list_strings['tissues_list']['Vessel LAD Prox Ref'] = 'Vessel - LAD, Prox Ref';
$app_list_strings['tissues_list']['Vessel LCX'] = 'Vessel - LCX';
$app_list_strings['tissues_list']['Vessel LCX Dist'] = 'Vessel - LCX (Dist)';
$app_list_strings['tissues_list']['Vessel LCX Prox'] = 'Vessel - LCX (Prox)';
$app_list_strings['tissues_list']['Vessel LCX Dist Ref'] = 'Vessel - LCX, Dist Ref';
$app_list_strings['tissues_list']['Vessel LCX Prox Ref'] = 'Vessel - LCX, Prox Ref';
$app_list_strings['tissues_list']['Vessel LEF'] = 'Vessel - LEF';
$app_list_strings['tissues_list']['Vessel LEF Dist'] = 'Vessel - LEF (Dist)';
$app_list_strings['tissues_list']['Vessel LEF Dist Dist Ref'] = 'Vessel - LEF (Dist), Dist Ref';
$app_list_strings['tissues_list']['Vessel LEF Dist Prox Ref'] = 'Vessel - LEF (Dist), Prox Ref';
$app_list_strings['tissues_list']['Vessel LEF Prox'] = 'Vessel - LEF (Prox)';
$app_list_strings['tissues_list']['Vessel LEF Prox Dist Ref'] = 'Vessel - LEF (Prox), Dist Ref';
$app_list_strings['tissues_list']['Vessel LEF Prox Prox Ref'] = 'Vessel - LEF (Prox), Prox Ref';
$app_list_strings['tissues_list']['Vessel LEF Circ'] = 'Vessel - LEF Circ.';
$app_list_strings['tissues_list']['Vessel LEF Vein'] = 'Vessel - LEF Vein';
$app_list_strings['tissues_list']['Vessel LEF Dist Ref'] = 'Vessel - LEF, Dist Ref';
$app_list_strings['tissues_list']['Vessel LEF Prox Ref'] = 'Vessel - LEF, Prox Ref';
$app_list_strings['tissues_list']['Vessel LEI'] = 'Vessel - LEI';
$app_list_strings['tissues_list']['Vessel LEI Dist'] = 'Vessel - LEI (Dist)';
$app_list_strings['tissues_list']['Vessel LEI Dist Dist Ref'] = 'Vessel - LEI (Dist), Dist Ref';
$app_list_strings['tissues_list']['Vessel LEI Dist Prox Ref'] = 'Vessel - LEI (Dist), Prox Ref';
$app_list_strings['tissues_list']['Vessel LEI Prox'] = 'Vessel - LEI (Prox)';
$app_list_strings['tissues_list']['Vessel LEI Prox Dist Ref'] = 'Vessel - LEI (Prox), Dist Ref';
$app_list_strings['tissues_list']['Vessel LEI Prox Prox Ref'] = 'Vessel - LEI (Prox), Prox Ref';
$app_list_strings['tissues_list']['Vessel LEI Circ'] = 'Vessel - LEI Circ.';
$app_list_strings['tissues_list']['Vessel LEI Vein'] = 'Vessel - LEI Vein';
$app_list_strings['tissues_list']['Vessel LEI Dist Ref'] = 'Vessel - LEI, Dist Ref';
$app_list_strings['tissues_list']['Vessel LEI Prox Ref'] = 'Vessel - LEI, Prox Ref';
$app_list_strings['tissues_list']['Vessel LIF'] = 'Vessel - LIF';
$app_list_strings['tissues_list']['Vessel LIF Vein'] = 'Vessel - LIF Vein';
$app_list_strings['tissues_list']['Vessel LII'] = 'Vessel - LII';
$app_list_strings['tissues_list']['Vessel LIMA'] = 'Vessel - LIMA';
$app_list_strings['tissues_list']['Vessel LIMA Dist'] = 'Vessel - LIMA (Dist)';
$app_list_strings['tissues_list']['Vessel LIMA Prox'] = 'Vessel - LIMA (Prox)';
$app_list_strings['tissues_list']['Vessel LIMA Dist Ref'] = 'Vessel - LIMA, Dist Ref';
$app_list_strings['tissues_list']['Vessel LIMA Prox Ref'] = 'Vessel - LIMA, Prox Ref';
$app_list_strings['tissues_list']['Vessel LIPV'] = 'Vessel - LIPV';
$app_list_strings['tissues_list']['Vessel LITA'] = 'Vessel - LITA';
$app_list_strings['tissues_list']['Vessel LITA Dist'] = 'Vessel - LITA (Dist)';
$app_list_strings['tissues_list']['Vessel LITA Prox'] = 'Vessel - LITA (Prox)';
$app_list_strings['tissues_list']['Vessel LITA Dist Ref'] = 'Vessel - LITA, Dist Ref';
$app_list_strings['tissues_list']['Vessel LITA Prox Ref'] = 'Vessel - LITA, Prox Ref';
$app_list_strings['tissues_list']['Vessel LPA'] = 'Vessel - LPA';
$app_list_strings['tissues_list']['Vessel LSPV'] = 'Vessel - LSPV';
$app_list_strings['tissues_list']['Vessel Main PA'] = 'Vessel - Main PA';
$app_list_strings['tissues_list']['Vessel RCA'] = 'Vessel - RCA';
$app_list_strings['tissues_list']['Vessel RCA Dist'] = 'Vessel - RCA (Dist)';
$app_list_strings['tissues_list']['Vessel RCA Prox'] = 'Vessel - RCA (Prox)';
$app_list_strings['tissues_list']['Vessel RCA Dist Ref'] = 'Vessel - RCA, Dist Ref';
$app_list_strings['tissues_list']['Vessel RCA Prox Ref'] = 'Vessel - RCA, Prox Ref';
$app_list_strings['tissues_list']['Vessel REF'] = 'Vessel - REF';
$app_list_strings['tissues_list']['Vessel REF Dist'] = 'Vessel - REF (Dist)';
$app_list_strings['tissues_list']['Vessel REF Dist Dist Ref'] = 'Vessel - REF (Dist), Dist Ref';
$app_list_strings['tissues_list']['Vessel REF Dist Prox Ref'] = 'Vessel - REF (Dist), Prox Ref';
$app_list_strings['tissues_list']['Vessel REF Prox'] = 'Vessel - REF (Prox)';
$app_list_strings['tissues_list']['Vessel REF Prox Dist Ref'] = 'Vessel - REF (Prox), Dist Ref';
$app_list_strings['tissues_list']['Vessel REF Prox Prox Ref'] = 'Vessel - REF (Prox), Prox Ref';
$app_list_strings['tissues_list']['Vessel REF Circ'] = 'Vessel - REF Circ.';
$app_list_strings['tissues_list']['Vessel REF Vein'] = 'Vessel - REF Vein';
$app_list_strings['tissues_list']['Vessel REF Dist Ref'] = 'Vessel - REF, Dist Ref';
$app_list_strings['tissues_list']['Vessel REF Prox Ref'] = 'Vessel - REF, Prox Ref';
$app_list_strings['tissues_list']['Vessel REI'] = 'Vessel - REI';
$app_list_strings['tissues_list']['Vessel REI Dist'] = 'Vessel - REI (Dist)';
$app_list_strings['tissues_list']['Vessel REI Dist Dist Ref'] = 'Vessel - REI (Dist) Dist Ref';
$app_list_strings['tissues_list']['Vessel REI Dist Prox Ref'] = 'Vessel - REI (Dist), Prox Ref';
$app_list_strings['tissues_list']['Vessel REI Prox'] = 'Vessel - REI (Prox)';
$app_list_strings['tissues_list']['Vessel REI Prox Dist Ref'] = 'Vessel - REI (Prox), Dist Ref';
$app_list_strings['tissues_list']['Vessel REI Prox Prox Ref'] = 'Vessel - REI (Prox), Prox Ref';
$app_list_strings['tissues_list']['Vessel REI Circ'] = 'Vessel - REI Circ.';
$app_list_strings['tissues_list']['Vessel REI Vein'] = 'Vessel - REI Vein';
$app_list_strings['tissues_list']['Vessel REI Dist Ref'] = 'Vessel - REI, Dist Ref';
$app_list_strings['tissues_list']['Vessel REI Prox Ref'] = 'Vessel - REI, Prox Ref';
$app_list_strings['tissues_list']['Vessel Renal Vein LT'] = 'Vessel - Renal Vein, LT';
$app_list_strings['tissues_list']['Vessel Renal Vein RT'] = 'Vessel - Renal Vein, RT';
$app_list_strings['tissues_list']['Vessel Renal LT'] = 'Vessel - Renal, LT';
$app_list_strings['tissues_list']['Vessel Renal LT Caud Branch'] = 'Vessel - Renal, LT Caud Branch';
$app_list_strings['tissues_list']['Vessel Renal LT Cran Branch'] = 'Vessel - Renal, LT Cran Branch';
$app_list_strings['tissues_list']['Vessel Renal RT'] = 'Vessel - Renal, RT';
$app_list_strings['tissues_list']['Vessel Renal RT Caud Branch'] = 'Vessel - Renal, RT Caud Branch';
$app_list_strings['tissues_list']['Vessel Renal RT Cran Branch'] = 'Vessel - Renal, RT Cran Branch';
$app_list_strings['tissues_list']['Vessel RIF'] = 'Vessel - RIF';
$app_list_strings['tissues_list']['Vessel RIF Vein'] = 'Vessel - RIF Vein';
$app_list_strings['tissues_list']['Vessel RII'] = 'Vessel - RII';
$app_list_strings['tissues_list']['Vessel RIMA'] = 'Vessel - RIMA';
$app_list_strings['tissues_list']['Vessel RIMA Dist'] = 'Vessel - RIMA (Dist)';
$app_list_strings['tissues_list']['Vessel RIMA Prox'] = 'Vessel - RIMA (Prox)';
$app_list_strings['tissues_list']['Vessel RIMA Dist Ref'] = 'Vessel - RIMA, Dist Ref';
$app_list_strings['tissues_list']['Vessel RIMA Prox Ref'] = 'Vessel - RIMA, Prox Ref';
$app_list_strings['tissues_list']['Vessel RIPV'] = 'Vessel - RIPV';
$app_list_strings['tissues_list']['Vessel RITA'] = 'Vessel - RITA';
$app_list_strings['tissues_list']['Vessel RITA Dist'] = 'Vessel - RITA (Dist)';
$app_list_strings['tissues_list']['Vessel RITA Prox'] = 'Vessel - RITA (Prox)';
$app_list_strings['tissues_list']['Vessel RITA Dist Ref'] = 'Vessel - RITA, Dist Ref';
$app_list_strings['tissues_list']['Vessel RITA Prox Ref'] = 'Vessel - RITA, Prox Ref';
$app_list_strings['tissues_list']['Vessel RPA'] = 'Vessel - RPA';
$app_list_strings['tissues_list']['Vessel RSPV'] = 'Vessel - RSPV';
$app_list_strings['tissues_list']['Vessel Saphenous LT'] = 'Vessel - Saphenous, LT';
$app_list_strings['tissues_list']['Vessel Saphenous RT'] = 'Vessel - Saphenous, RT';
$app_list_strings['tissues_list']['Vessel Splenic'] = 'Vessel - Splenic';
$app_list_strings['tissues_list']['Vessel Splenic Vein'] = 'Vessel - Splenic Vein';
$app_list_strings['tissues_list']['Vessel SVC'] = 'Vessel - SVC';
$app_list_strings['tissues_list']['Vessel Vetebral LT'] = 'Vessel - Vetebral, LT';
$app_list_strings['tissues_list']['Vessel Vetebral RT'] = 'Vessel - Vetebral, RT';
$app_list_strings['tissues_list'][''] = '';
$app_list_strings['pro_subtype_list']['Access Catheter'] = 'Access Catheter';
$app_list_strings['pro_subtype_list']['Angioplasty Balloon'] = 'Angioplasty Balloon';
$app_list_strings['pro_subtype_list']['Aortic Root Cannula'] = 'Aortic Root Cannula';
$app_list_strings['pro_subtype_list']['Arterial Cannula'] = 'Arterial Cannula';
$app_list_strings['pro_subtype_list']['Bare Metal Stent'] = 'Bare Metal Stent';
$app_list_strings['pro_subtype_list']['Barium Contrast'] = 'Barium Contrast';
$app_list_strings['pro_subtype_list']['Central Venous Catheter'] = 'Central Venous Catheter';
$app_list_strings['pro_subtype_list']['Coated'] = 'Coated';
$app_list_strings['pro_subtype_list']['Cutting Balloon'] = 'Cutting Balloon';
$app_list_strings['pro_subtype_list']['Diagnostic Catheter'] = 'Diagnostic Catheter';
$app_list_strings['pro_subtype_list']['Drug Coated Balloon'] = 'Drug Coated Balloon';
$app_list_strings['pro_subtype_list']['Drug Coated Stent'] = 'Drug Coated Stent';
$app_list_strings['pro_subtype_list']['EP Catheter'] = 'EP Catheter';
$app_list_strings['pro_subtype_list']['ePTFE'] = 'ePTFE';
$app_list_strings['pro_subtype_list']['Ethibond'] = 'Ethibond';
$app_list_strings['pro_subtype_list']['Ethilon'] = 'Ethilon';
$app_list_strings['pro_subtype_list']['Exchange Wire'] = 'Exchange Wire';
$app_list_strings['pro_subtype_list']['Extension Catheter'] = 'Extension Catheter';
$app_list_strings['pro_subtype_list']['Foley Catheter'] = 'Foley Catheter';
$app_list_strings['pro_subtype_list']['Guide Catheter'] = 'Guide Catheter';
$app_list_strings['pro_subtype_list']['Guide Wire'] = 'Guide Wire';
$app_list_strings['pro_subtype_list']['High Pressure'] = 'High Pressure';
$app_list_strings['pro_subtype_list']['Injectable'] = 'Injectable';
$app_list_strings['pro_subtype_list']['Introducer Sheath'] = 'Introducer Sheath';
$app_list_strings['pro_subtype_list']['Iodinated Contrast'] = 'Iodinated Contrast';
$app_list_strings['pro_subtype_list']['IV Catheter'] = 'IV Catheter';
$app_list_strings['pro_subtype_list']['Knitted'] = 'Knitted';
$app_list_strings['pro_subtype_list']['Mapping Catheter'] = 'Mapping Catheter';
$app_list_strings['pro_subtype_list']['Marking Wire'] = 'Marking Wire';
$app_list_strings['pro_subtype_list']['Micro Catheter'] = 'Micro Catheter';
$app_list_strings['pro_subtype_list']['Mila Catheter'] = 'Mila Catheter';
$app_list_strings['pro_subtype_list']['Monocryl'] = 'Monocryl';
$app_list_strings['pro_subtype_list']['Oral'] = 'Oral';
$app_list_strings['pro_subtype_list']['OTW over the wire'] = 'OTW (over the wire)';
$app_list_strings['pro_subtype_list']['Patches'] = 'Patches';
$app_list_strings['pro_subtype_list']['PDS'] = 'PDS';
$app_list_strings['pro_subtype_list']['Pediatric Cannula'] = 'Pediatric Cannula';
$app_list_strings['pro_subtype_list']['Pledget'] = 'Pledget';
$app_list_strings['pro_subtype_list']['POBA Balloon'] = 'POBA Balloon';
$app_list_strings['pro_subtype_list']['Pressure Catheter'] = 'Pressure Catheter';
$app_list_strings['pro_subtype_list']['Prolene'] = 'Prolene';
$app_list_strings['pro_subtype_list']['PTA Balloon'] = 'PTA Balloon';
$app_list_strings['pro_subtype_list']['Silk'] = 'Silk';
$app_list_strings['pro_subtype_list']['Steerable Sheath'] = 'Steerable Sheath';
$app_list_strings['pro_subtype_list']['Support Catheter'] = 'Support Catheter';
$app_list_strings['pro_subtype_list']['Support Wire'] = 'Support Wire';
$app_list_strings['pro_subtype_list']['Ti Cron'] = 'Ti-Cron';
$app_list_strings['pro_subtype_list']['Transseptal Catheter'] = 'Transseptal Catheter';
$app_list_strings['pro_subtype_list']['Venous Cannula'] = 'Venous Cannula';
$app_list_strings['pro_subtype_list']['Vessel Cannula'] = 'Vessel Cannula';
$app_list_strings['pro_subtype_list']['Vicryl'] = 'Vicryl';
$app_list_strings['pro_subtype_list']['Woven'] = 'Woven';
$app_list_strings['pro_subtype_list'][''] = '';
$app_list_strings['collection_date_time_type_list']['Date at Record Creation'] = 'Date at Record Creation';
$app_list_strings['collection_date_time_type_list']['Date Time at Record Creation'] = 'Date & Time at Record Creation';
$app_list_strings['collection_date_time_type_list']['Date at Specimen Collection'] = 'Date at Specimen Collection';
$app_list_strings['collection_date_time_type_list']['Date Time at Specimen Collection'] = 'Date & Time at Specimen Collection';
$app_list_strings['collection_date_time_type_list'][''] = '';
$app_list_strings['known_unknown_list']['Known'] = 'Known';
$app_list_strings['known_unknown_list']['Unknown'] = 'Unknown';
$app_list_strings['known_unknown_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Inventory_Management.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['IM_Inventory_Management'] = 'Inventory Management';
$app_list_strings['moduleListSingular']['IM_Inventory_Management'] = 'Inventory Management';
$app_list_strings['related_to_list']['Inventory Collection'] = 'Inventory Collection';
$app_list_strings['related_to_list']['Inventory Item'] = 'Inventory Item';
$app_list_strings['related_to_list'][''] = '';
$app_list_strings['inventory_management_type_list']['Archive Offsite'] = 'Archive Offsite';
$app_list_strings['inventory_management_type_list']['Archive Onsite'] = 'Archive Onsite';
$app_list_strings['inventory_management_type_list']['Create Inventory Collection'] = 'Create Inventory Collection';
$app_list_strings['inventory_management_type_list']['Discard'] = 'Discard';
$app_list_strings['inventory_management_type_list']['External Transfer'] = 'External Transfer';
$app_list_strings['inventory_management_type_list']['Internal Transfer'] = 'Internal Transfer';
$app_list_strings['inventory_management_type_list']['Obsolete'] = 'Obsolete';
$app_list_strings['inventory_management_type_list']['Processing'] = 'Processing';
$app_list_strings['inventory_management_type_list']['Separate Inventory Items'] = 'Separate Inventory Collection Item(s)';
$app_list_strings['inventory_management_type_list'][''] = '';
$app_list_strings['im_condition_list']['Ambient Temperature'] = 'Ambient Temperature';
$app_list_strings['im_condition_list']['Room Temperature'] = 'Room Temperature';
$app_list_strings['im_condition_list']['Refrigerated'] = 'Refrigerated';
$app_list_strings['im_condition_list']['On Ice'] = 'On Ice';
$app_list_strings['im_condition_list']['Frozen on Dry Ice'] = 'Frozen on Dry Ice';
$app_list_strings['im_condition_list']['Frozen in Dry IceAcetone'] = 'Frozen in Dry Ice/Acetone';
$app_list_strings['im_condition_list']['Frozen in Liquid Nitrogen'] = 'Frozen in Liquid Nitrogen';
$app_list_strings['im_condition_list']['Frozen'] = 'Frozen';
$app_list_strings['im_condition_list']['Ultra Frozen'] = 'Ultra Frozen';
$app_list_strings['im_condition_list'][''] = '';
$app_list_strings['processing_type_list'][1] = '1';
$app_list_strings['processing_type_list'][''] = '';
$app_list_strings['processing_transfer_condition_list']['Ambient Temperature'] = 'Ambient Temperature';
$app_list_strings['processing_transfer_condition_list']['Room Temperature'] = 'Room Temperature';
$app_list_strings['processing_transfer_condition_list']['Refrigerated'] = 'Refrigerated';
$app_list_strings['processing_transfer_condition_list']['On Ice'] = 'On Ice';
$app_list_strings['processing_transfer_condition_list']['Frozen on Dry Ice'] = 'Frozen on Dry Ice';
$app_list_strings['processing_transfer_condition_list']['Frozen in Dry Ice Acetone'] = 'Frozen in Dry Ice / Acetone';
$app_list_strings['processing_transfer_condition_list'][''] = '';
$app_list_strings['end_condition_acceptable_list']['No'] = 'No';
$app_list_strings['end_condition_acceptable_list']['Yes'] = 'Yes';
$app_list_strings['end_condition_acceptable_list']['Yes with Comments'] = 'Yes with Comments';
$app_list_strings['end_condition_acceptable_list'][''] = '';
$app_list_strings['im_location_type_list']['Equipment'] = 'Equipment';
$app_list_strings['im_location_type_list']['Room'] = 'Room';
$app_list_strings['im_location_type_list']['RoomCabinet'] = 'Room/Cabinet';
$app_list_strings['im_location_type_list']['RoomShelf'] = 'Room/Shelf';
$app_list_strings['im_location_type_list']['Cabinet'] = 'Cabinet';
$app_list_strings['im_location_type_list']['Shelf'] = 'Shelf';
$app_list_strings['im_location_type_list'][''] = '';
$app_list_strings['location_list'][780] = '780 86th Ave.';
$app_list_strings['location_list'][8945] = '8945 Evergreen Blvd.';
$app_list_strings['location_list'][8960] = '8960 Evergreen Blvd.';
$app_list_strings['location_list'][9055] = '9055 Evergreen Blvd.';
$app_list_strings['location_list'][''] = '';
$app_list_strings['location_shelf_list'][1] = '1';
$app_list_strings['location_shelf_list'][''] = '';
$app_list_strings['location_cabinet_list'][1] = '1';
$app_list_strings['location_cabinet_list'][''] = '';
$app_list_strings['inventory_item_category_list']['Product'] = 'Product';
$app_list_strings['inventory_item_category_list']['Record'] = 'Record';
$app_list_strings['inventory_item_category_list']['Solution'] = 'Solution';
$app_list_strings['inventory_item_category_list']['Specimen'] = 'Specimen';
$app_list_strings['inventory_item_category_list']['Study Article'] = 'Study Article';
$app_list_strings['inventory_item_category_list'][''] = '';
$app_list_strings['im_category_list']['Product'] = 'Product';
$app_list_strings['im_category_list']['Record'] = 'Record';
$app_list_strings['im_category_list']['Specimen'] = 'Specimen';
$app_list_strings['im_category_list']['Study Article'] = 'Study Article';
$app_list_strings['im_category_list'][''] = '';
$app_list_strings['im_type_list']['Record'] = 'Record';
$app_list_strings['im_type_list']['Specimen'] = 'Specimen';
$app_list_strings['im_type_list']['Study Article'] = 'Study Article';
$app_list_strings['im_type_list'][''] = '';
$app_list_strings['inventory_item_type_list']['Block'] = 'Block';
$app_list_strings['inventory_item_type_list']['Control Article'] = 'Control Article';
$app_list_strings['inventory_item_type_list']['Culture'] = 'Culture';
$app_list_strings['inventory_item_type_list']['Data Book'] = 'Data Book';
$app_list_strings['inventory_item_type_list']['Drug'] = 'Drug';
$app_list_strings['inventory_item_type_list']['Equipment Facility Record'] = 'Equipment / Facility Record';
$app_list_strings['inventory_item_type_list']['Extract'] = 'Extract';
$app_list_strings['inventory_item_type_list']['Fecal'] = 'Fecal';
$app_list_strings['inventory_item_type_list']['Hard drive'] = 'Hard drive';
$app_list_strings['inventory_item_type_list']['Plasma'] = 'Plasma';
$app_list_strings['inventory_item_type_list']['Protocol Book'] = 'Protocol Book';
$app_list_strings['inventory_item_type_list']['Serum'] = 'Serum';
$app_list_strings['inventory_item_type_list']['Slide'] = 'Slide';
$app_list_strings['inventory_item_type_list']['Test Article'] = 'Test Article';
$app_list_strings['inventory_item_type_list']['Tissue'] = 'Tissue';
$app_list_strings['inventory_item_type_list']['Urine'] = 'Urine';
$app_list_strings['inventory_item_type_list']['Whole Blood'] = 'Whole Blood';
$app_list_strings['inventory_item_type_list']['Accessory Article'] = 'Accessory Article';
$app_list_strings['inventory_item_type_list']['Data Sheet'] = 'Data Sheet';
$app_list_strings['inventory_item_type_list']['Hard Drive'] = 'Hard Drive';
$app_list_strings['inventory_item_type_list'][''] = '';
$app_list_strings['processing_method_list'][1] = '1';
$app_list_strings['processing_method_list'][''] = '';
$app_list_strings['complete_incomplete_list']['Complete'] = 'Complete';
$app_list_strings['complete_incomplete_list']['Incomplete'] = 'Incomplete';
$app_list_strings['complete_incomplete_list'][''] = '';
$app_list_strings['inventory_item_storage_medium_list']['4 Gluteraldehyde'] = '4% Gluteraldehyde';
$app_list_strings['inventory_item_storage_medium_list']['4 Paraformaldehyde'] = '4% Paraformaldehyde';
$app_list_strings['inventory_item_storage_medium_list']['10 Neutral Buffered Formalin'] = '10% Neutral Buffered Formalin';
$app_list_strings['inventory_item_storage_medium_list']['Davidsons Fixative'] = 'Davidson\'s Fixative';
$app_list_strings['inventory_item_storage_medium_list']['None'] = 'None';
$app_list_strings['inventory_item_storage_medium_list']['Normal Saline'] = 'Normal Saline';
$app_list_strings['inventory_item_storage_medium_list']['Water'] = 'Water';
$app_list_strings['inventory_item_storage_medium_list'][''] = '';
$app_list_strings['sales_wp_list']['Internal Use'] = 'Internal Use';
$app_list_strings['sales_wp_list']['Sales'] = 'Sales';
$app_list_strings['sales_wp_list']['Work Product'] = 'Work Product';
$app_list_strings['sales_wp_list'][''] = '';
$app_list_strings['item_collection_list']['Inventory Item'] = 'Inventory Item';
$app_list_strings['item_collection_list']['Inventory Collection'] = 'Inventory Collection';
$app_list_strings['item_collection_list'][''] = '';
$app_list_strings['composition_list']['Inventory Item'] = 'Inventory Item';
$app_list_strings['composition_list']['Inventory Collection'] = 'Inventory Collection';
$app_list_strings['composition_list'][''] = '';
$app_list_strings['inventory_item_storage_condition_list']['Ambient Temperature'] = 'Ambient Temperature';
$app_list_strings['inventory_item_storage_condition_list']['Frozen'] = 'Frozen';
$app_list_strings['inventory_item_storage_condition_list']['Refrigerated'] = 'Refrigerated';
$app_list_strings['inventory_item_storage_condition_list']['Room Temperature'] = 'Room Temperature';
$app_list_strings['inventory_item_storage_condition_list']['Ultra Frozen in Freezer'] = 'Ultra Frozen in Freezer';
$app_list_strings['inventory_item_storage_condition_list']['Ultra Frozen in Liquid Nitrogen'] = 'Ultra Frozen in Liquid Nitrogen';
$app_list_strings['inventory_item_storage_condition_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_ im_specimen_category_list.php

 // created: 2020-09-30 09:34:11

$app_list_strings[' im_specimen_category_list']=array (
  '' => '',
  'Block' => 'Block',
  'Culture' => 'Culture',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Plasma' => 'Plasma',
  'Serum' => 'Serum',
  'Slide' => 'Slide',
  'Tissue' => 'Tissue',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_ im_record_category_list.php

 // created: 2020-09-30 09:31:21

$app_list_strings[' im_record_category_list']=array (
  '' => '',
  'Data Book' => 'Data Book',
  'Data Sheet' => 'Data Sheet',
  'Equipment Facility Record' => 'Equipment / Facility Record',
  'Hard Drive' => 'Hard Drive',
  'Protocol Book' => 'Protocol Book',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_im_record_category_list.php

 // created: 2020-09-30 10:31:26

$app_list_strings['im_record_category_list']=array (
  '' => '',
  'Data Book' => 'Data Book',
  'Data Sheet' => 'Data Sheet',
  'Equipment Facility Record' => 'Equipment / Facility Record',
  'Hard Drive' => 'Hard Drive',
  'Protocol Book' => 'Protocol Book',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_ im_study_article_category_list.php

 // created: 2020-09-30 09:37:11

$app_list_strings[' im_study_article_category_list']=array (
  '' => '',
  'Accessory Article' => 'Accessory Article',
  'Control Article' => 'Control Article',
  'Test Article' => 'Test Article',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_known_unknown_list.php

 // created: 2020-09-23 08:29:59

$app_list_strings['known_unknown_list']=array (
  '' => '',
  'Known' => 'Known',
  'Unknown' => 'Unknown',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_Inventory_Item.php
 
$app_list_strings['unique_id_type_list'] = array (
  'Internal' => 'Internal Only',
  'External' => 'External & Internal',
  '' => '',
);$app_list_strings['inventory_item_category_list'] = array (
  'Product' => 'Product',
  'Record' => 'Record',
  'Solution' => 'Solution',
  'Specimen' => 'Specimen',
  '' => '',
);$app_list_strings['inventory_item_type_list'] = array (
  'Accessory Product' => 'Accessory Product',
  'Block' => 'Block',
  'Chemical' => 'Chemical',
  'Cleaning Agent' => 'Cleaning Agent',
  'Control Article' => 'Control Article',
  'Culture' => 'Culture',
  'Data Book' => 'Data Book',
  'Data Sheet' => 'Data Sheet',
  'Drug' => 'Drug',
  'Equipment Facility Record' => 'Equipment / Facility Record',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Hard drive' => 'Hard drive',
  'Plasma' => 'Plasma',
  'Protocol Book' => 'Protocol Book',
  'Reagent' => 'Reagent',
  'Serum' => 'Serum',
  'Slide' => 'Slide',
  'Test Article' => 'Test Article',
  'Tissue' => 'Tissue',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
  '' => '',
);$app_list_strings['inventory_item_subtype_list'] = array (
  'Sample Prep BAS logs for equipmentrooms' => 'Sample Prep BAS logs for equipment/rooms',
  'Stock Animal Weights' => 'Stock Animal Weights',
  'Analytical Calibration Check for Adjustable Pipettes' => 'Analytical - Calibration Check for Adjustable Pipettes',
  '8960 Room Logs' => '8960 Room Logs',
  'OR3 OR4 OR5 Daily Maintenance Checklist' => 'OR3 OR4 OR5 Daily Maintenance Checklist',
  'CL1 CL2 CL3 CL4 Daily Maintenance Checklist' => 'CL1 CL2 CL3 CL4 Daily Maintenance Checklist',
  '8945 Prep Daily Maintenance Checklist' => '8945 Prep Daily Maintenance Checklist',
  'Cycle Log' => 'Cycle Log',
  'Cycle Printer Output' => 'Cycle Printer Output',
  'Biological Sample Test Log' => 'Biological Sample Test Log',
  'Siemens Fluroroscopy Patient and Leonardo Workstation Log' => 'Siemens Fluroroscopy Patient and Leonardo Workstation Log',
  'Change of DICOM Metadata Notification' => 'Change of DICOM Metadata Notification',
  'Baxter COM2 Cardiac Output Computer Data Collection' => 'Baxter COM-2 Cardiac Output Computer Data Collection',
  'Verification of the Cardiopulmonary Bypass Perfusion Systems Roller Head' => 'Verification of the Cardiopulmonary Bypass Perfusion Systems Roller Head',
  '8945 8960 Animal Room Temp Reports' => '8945 & 8960 Animal Room Temp Reports',
  '8945 8960 9055 Data Storage Temp Reports' => '8945, 8960, 9055 Data Storage Temp Reports',
  '8945 80 Freezer Store Room Refrigerator Temp Report' => '8945 -80 Freezer & Store Room Refrigerator Temp Report',
  '9055 Controlled Receiving Temp Report' => '9055 Controlled Receiving Temp Report',
  'Hood Monitoring Logs' => 'Hood Monitoring Logs',
  'Sample Prep Equipment Monitoring Logs' => 'Sample Prep Equipment Monitoring Logs',
  'Sample Prep RefrigeratorFreezer Logs' => 'Sample Prep Refrigerator/Freezer Logs',
  'Sample Prep Microbiological Mointoring Forms' => 'Sample Prep Microbiological Mointoring Forms',
  'Sterilizer Receipts' => 'Sterilizer Receipts',
  'Cagewash Tracking' => 'Cagewash Tracking',
  '8960 Feed Receipt' => '8960 Feed Receipt',
  '8960 Environmental Monitoring' => '8960 Environmental Monitoring',
  'Reagent Usage Log' => 'Reagent Usage Log',
  'Solution Preparation Log' => 'Solution Preparation Log',
  'Pathology Refrigerator Freezer Temperature Log' => 'Pathology Refrigerator & Freezer Temperature Log',
  'Histology Oven Temperature' => 'Histology Oven Temperature',
  'Cryostat Temperature Log' => 'Cryostat Temperature Log',
  'Hazardous Waste Inspection Log' => 'Hazardous Waste Inspection Log',
  'Tissue Processor Log' => 'Tissue Processor Log',
  'SpeedVac Use Log' => 'SpeedVac Use Log',
  'Sterilizer Cycle Log' => 'Sterilizer Cycle Log',
  'Depyrogenator Cycle Log' => 'Depyrogenator Cycle Log',
  'Dishwasher Cycle Log' => 'Dishwasher Cycle Log',
  'Depyrogenator Cycle Printouts' => 'Depyrogenator Cycle Printouts',
  'BalanceScale Logs' => 'Balance/Scale Logs',
  'CaliperGage Block Logs' => 'Caliper/Gage Block Logs',
  'FTIR Sample Analysis Form' => 'FTIR Sample Analysis Form',
  'Analytical Refrigerator and Freezer Temperature Log' => 'Analytical Refrigerator and Freezer Temperature Log',
  'Analytical Routine Equipment Maintenance Log' => 'Analytical Routine Equipment Maintenance Log',
  'Analtyical Facilty Equipment Log' => 'Analtyical Facilty Equipment Log',
  'Controlled Substances Log' => 'Controlled Substances Log',
  '8945 TempHumidity Logs feed room fridgefreezers Animal Care' => '8945 Temp/Humidity Logs (feed room & fridge/freezers - Animal Care)',
  '8945 Micro monitoring logs' => '8945 Micro monitoring logs',
  'Feed Receipt Logs' => 'Feed Receipt Logs',
  'MaintenanceTemp Records 8960' => 'Maintenance/Temp Records - 8960',
  'Hemavet' => 'Hemavet',
  '8945 Recovery Fridge' => '8945 Recovery Fridge',
  '8945 Ancillary Equipment Sanitation' => '8945 Ancillary Equipment Sanitation',
  '8960 Microbiological Monitoring' => '8960 Microbiological Monitoring',
  'Formalin Recycling Form' => 'Formalin Recycling Form',
  'Aldehyde Solution Neutralizing Form' => 'Aldehyde Solution Neutralizing Form',
  'Perfusion Tank Log' => 'Perfusion Tank Log',
  'Balance and Scale Validation Logs' => 'Balance and Scale Validation Logs',
  'Freezer Inventory Log' => 'Freezer Inventory Log',
  'Calibration Records 8960' => 'Calibration Records - 8960',
  'Biologicals for Autoclave 8960' => 'Biologicals for Autoclave - 8960',
  'CP Slides IVT' => 'CP Slides (IVT)',
  'IVT Equipment Monitoring Logs' => 'IVT Equipment Monitoring Logs',
  'IVT Montitoring System Temp Graphs' => 'IVT Montitoring System Temp Graphs',
  '8945 8960 RV Filter Change Logs' => '8945, 8960, RV Filter Change Logs',
  '8945 8960 9055 Generator Operation Checklist' => '8945, 8960, 9055 Generator Operation Checklist',
  'Animal Receipt Logs' => 'Animal Receipt Logs',
  '8960 Weekly Maintenance Shower Eyewash' => '8960 Weekly Maintenance (Shower & Eyewash)',
  'Analytical Services pH Calibration Log' => 'Analytical Services pH Calibration Log',
  'Analytical Services Solution Log Book Stock Solutions' => 'Analytical Services Solution Log Book- Stock Solutions',
  'Analytical Services Solution Log Book Stock Dilutions' => 'Analytical Services Solution Log Book- Stock Dilutions',
  'Analytical Services Solution Log Book Standard Curves' => 'Analytical Services Solution Log Book- Standard Curves',
  'Analytical Services Solution Log Book Buffers Mobile Phases Other' => 'Analytical Services Solution Log Book- Buffers, Mobile Phases, Other',
  'Analytical Services Solution Preparation' => 'Analytical Services Solution Preparation',
  'Analytical Services Standard Curve Spiking Solution Preparation' => 'Analytical Services Standard Curve Spiking Solution Preparation',
  'LAL Standard Curves' => 'LAL Standard Curves',
  'CascadeBFTII QC' => 'Cascade/BFTII QC',
  'Spectrophotometer Weekly Verification' => 'Spectrophotometer Weekly Verification',
  'Solution Preparation Documents' => 'Solution Preparation Documents',
  'Cell SplittingMedia Change Forms Mouse Lymphoma Balb L929 V79 THP Chok1 other cell lines' => 'Cell Splitting/Media Change Forms - Mouse Lymphoma, Balb, L929, V79, THP, Chok1, other cell lines',
  'Cell Plating Records' => 'Cell Plating Records',
  'Serum Heat Inactivation' => 'Serum Heat Inactivation',
  'ProtoCOL3 Colony Counter Validation Records' => 'ProtoCOL3 Colony Counter Validation Records',
  'Cell Revival Records' => 'Cell Revival Records',
  'Aliquot Creation Logs' => 'Aliquot Creation Logs',
  'Flow Cytometer CST maintenance' => 'Flow Cytometer CST / maintenance',
  'Moxi' => 'Moxi',
  'Blood Loop Controls' => 'Blood Loop Controls',
  'iSTAT Validation' => 'iSTAT Validation',
  'Scale Validation' => 'Scale Validation',
  'Glove Box' => 'Glove Box',
  'Hazardous Waste Log' => 'Hazardous Waste Log',
  'Osmometer Log' => 'Osmometer Log',
  'pH Log' => 'pH Log',
  'Equipment Routine Maintenance' => 'Equipment Routine Maintenance',
  '' => '',
);$app_list_strings['inventory_item_status_list'] = array (
  'Planned Inventory' => 'Planned Inventory',
  'Onsite Inventory' => 'Onsite Inventory',
  'Onsite Inventory Expired' => 'Onsite Inventory (Expired)',
  'Offsite Inventory' => 'Offsite Inventory',
  'Offsite Inventory Expired' => 'Offsite Inventory (Expired)',
  'Onsite Archived' => 'Onsite Archived',
  'Onsite Archived Expired' => 'Onsite Archived (Expired)',
  'Offsite Archived' => 'Offsite Archived',
  'Offsite Archived Expired' => 'Offsite Archived (Expired)',
  'Destroyed' => 'Destroyed',
  '' => '',
);$app_list_strings['related_to_list'] = array (
  'Internal Use' => 'Internal Use',
  'Multiple Test Systems' => 'Multiple Test Systems',
  'Sales' => 'Sales',
  'Single Test System' => 'Single Test System',
  'Work Product' => 'Work Product',
  '' => '',
);$app_list_strings['inventory_item_sterilization_list'] = array (
  'ETO Sterilized' => 'ETO Sterilized',
  'Gamma Sterilized' => 'Gamma Sterilized',
  'Non sterile' => 'Non-sterile',
  'Steam Sterilized' => 'Steam Sterilized',
  '' => '',
);$app_list_strings['timepoint_type_list'] = array (
  'Ad Hoc' => 'Ad Hoc',
  'None' => 'None',
  'Work Product Schedule' => 'Work Product Schedule',
  '' => '',
);$app_list_strings['processing_method_list'] = array (
  1 => '1',
  '' => '',
);$app_list_strings['expiration_test_article_list'] = array (
  'Known' => 'Known',
  'Pending' => 'Pending',
  'Unknown' => 'Unknown',
  '' => '',
);$app_list_strings['inventory_item_storage_condition_list'] = array (
  '30 45' => '30 - 45°C',
  'Ambient' => 'Ambient',
  'Room Temperature' => 'Room Temperature',
  'Refrigerate' => 'Refrigerate',
  'Frozen' => 'Frozen',
  'Ultra Frozen' => 'Ultra Frozen',
  '' => '',
);$app_list_strings['inventory_item_storage_medium_list'] = array (
  '4 Gluteraldehyde' => '4% Gluteraldehyde',
  '4 Paraformaldehyde' => '4% Paraformaldehyde',
  '10 Neutral Buffered Formalin' => '10% Neutral Buffered Formalin',
  'Davidsons Fixative' => 'Davidson\'s Fixative',
  'None' => 'None',
  'Normal Saline' => 'Normal Saline',
  'Water' => 'Water',
  '' => '',
);$app_list_strings['im_location_type_list'] = array (
  'Equipment' => 'Equipment',
  'Room' => 'Room',
  'RoomCabinet' => 'Room/Cabinet',
  'RoomShelf' => 'Room/Shelf',
  'Cabinet' => 'Cabinet',
  'Shelf' => 'Shelf',
  '' => '',
);$app_list_strings['location_list'] = array (
  780 => '780 86th Ave.',
  8945 => '8945 Evergreen Blvd.',
  8960 => '8960 Evergreen Blvd.',
  9055 => '9055 Evergreen Blvd.',
  '' => '',
);$app_list_strings['location_shelf_list'] = array (
  1 => '1',
  '' => '',
);$app_list_strings['location_cabinet_list'] = array (
  1 => '1',
  '' => '',
);$app_list_strings['inventory_item_owner_list'] = array (
  'APS Owned' => 'APS Owned',
  'APS Supplied' => 'APS Supplied',
  'Client' => 'Client',
  'Client Owned' => 'Client Owned',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_im_study_article_category_list.php

 // created: 2020-09-30 10:35:20

$app_list_strings['im_study_article_category_list']=array (
  '' => '',
  'Accessory Article' => 'Accessory Article',
  'Control Article' => 'Control Article',
  'Test Article' => 'Test Article',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_im_specimen_category_list.php

 // created: 2020-09-30 10:34:12

$app_list_strings['im_specimen_category_list']=array (
  '' => '',
  'Block' => 'Block',
  'Culture' => 'Culture',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Plasma' => 'Plasma',
  'Serum' => 'Serum',
  'Slide' => 'Slide',
  'Tissue' => 'Tissue',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_inventory_item_subtype_list.php

 // created: 2020-07-07 18:57:35

$app_list_strings['inventory_item_subtype_list']=array (
  'Sample Prep BAS logs for equipmentrooms' => 'Sample Prep BAS logs for equipment/rooms',
  'Stock Animal Weights' => 'Stock Animal Weights',
  'Analytical Calibration Check for Adjustable Pipettes' => 'Analytical - Calibration Check for Adjustable Pipettes',
  '8960 Room Logs' => '8960 Room Logs',
  'OR3 OR4 OR5 Daily Maintenance Checklist' => 'OR3 OR4 OR5 Daily Maintenance Checklist',
  'CL1 CL2 CL3 CL4 Daily Maintenance Checklist' => 'CL1 CL2 CL3 CL4 Daily Maintenance Checklist',
  '8945 Prep Daily Maintenance Checklist' => '8945 Prep Daily Maintenance Checklist',
  'Cycle Log' => 'Cycle Log',
  'Cycle Printer Output' => 'Cycle Printer Output',
  'Biological Sample Test Log' => 'Biological Sample Test Log',
  'Siemens Fluroroscopy Patient and Leonardo Workstation Log' => 'Siemens Fluroroscopy Patient and Leonardo Workstation Log',
  'Change of DICOM Metadata Notification' => 'Change of DICOM Metadata Notification',
  'Baxter COM2 Cardiac Output Computer Data Collection' => 'Baxter COM-2 Cardiac Output Computer Data Collection',
  'Verification of the Cardiopulmonary Bypass Perfusion Systems Roller Head' => 'Verification of the Cardiopulmonary Bypass Perfusion Systems Roller Head',
  '8945 8960 Animal Room Temp Reports' => '8945 & 8960 Animal Room Temp Reports',
  '8945 8960 9055 Data Storage Temp Reports' => '8945, 8960, 9055 Data Storage Temp Reports',
  '8945 80 Freezer Store Room Refrigerator Temp Report' => '8945 -80 Freezer & Store Room Refrigerator Temp Report',
  '9055 Controlled Receiving Temp Report' => '9055 Controlled Receiving Temp Report',
  'Hood Monitoring Logs' => 'Hood Monitoring Logs',
  'Sample Prep Equipment Monitoring Logs' => 'Sample Prep Equipment Monitoring Logs',
  'Sample Prep RefrigeratorFreezer Logs' => 'Sample Prep Refrigerator/Freezer Logs',
  'Sample Prep Microbiological Mointoring Forms' => 'Sample Prep Microbiological Mointoring Forms',
  'Sterilizer Receipts' => 'Sterilizer Receipts',
  'Cagewash Tracking' => 'Cagewash Tracking',
  '8960 Feed Receipt' => '8960 Feed Receipt',
  '8960 Environmental Monitoring' => '8960 Environmental Monitoring',
  'Reagent Usage Log' => 'Reagent Usage Log',
  'Solution Preparation Log' => 'Solution Preparation Log',
  'Pathology Refrigerator Freezer Temperature Log' => 'Pathology Refrigerator & Freezer Temperature Log',
  'Histology Oven Temperature' => 'Histology Oven Temperature',
  'Cryostat Temperature Log' => 'Cryostat Temperature Log',
  'Hazardous Waste Inspection Log' => 'Hazardous Waste Inspection Log',
  'Tissue Processor Log' => 'Tissue Processor Log',
  'SpeedVac Use Log' => 'SpeedVac Use Log',
  'Sterilizer Cycle Log' => 'Sterilizer Cycle Log',
  'Depyrogenator Cycle Log' => 'Depyrogenator Cycle Log',
  'Dishwasher Cycle Log' => 'Dishwasher Cycle Log',
  'Depyrogenator Cycle Printouts' => 'Depyrogenator Cycle Printouts',
  'BalanceScale Logs' => 'Balance/Scale Logs',
  'CaliperGage Block Logs' => 'Caliper/Gage Block Logs',
  'FTIR Sample Analysis Form' => 'FTIR Sample Analysis Form',
  'Analytical Refrigerator and Freezer Temperature Log' => 'Analytical Refrigerator and Freezer Temperature Log',
  'Analytical Routine Equipment Maintenance Log' => 'Analytical Routine Equipment Maintenance Log',
  'Analtyical Facilty Equipment Log' => 'Analtyical Facilty Equipment Log',
  'Controlled Substances Log' => 'Controlled Substances Log',
  '8945 TempHumidity Logs feed room fridgefreezers Animal Care' => '8945 Temp/Humidity Logs (feed room & fridge/freezers - Animal Care)',
  '8945 Micro monitoring logs' => '8945 Micro monitoring logs',
  'Feed Receipt Logs' => 'Feed Receipt Logs',
  'MaintenanceTemp Records 8960' => 'Maintenance/Temp Records - 8960',
  'Hemavet' => 'Hemavet',
  '8945 Recovery Fridge' => '8945 Recovery Fridge',
  '8945 Ancillary Equipment Sanitation' => '8945 Ancillary Equipment Sanitation',
  '8960 Microbiological Monitoring' => '8960 Microbiological Monitoring',
  'Formalin Recycling Form' => 'Formalin Recycling Form',
  'Aldehyde Solution Neutralizing Form' => 'Aldehyde Solution Neutralizing Form',
  'Perfusion Tank Log' => 'Perfusion Tank Log',
  'Balance and Scale Validation Logs' => 'Balance and Scale Validation Logs',
  'Freezer Inventory Log' => 'Freezer Inventory Log',
  'Calibration Records 8960' => 'Calibration Records - 8960',
  'Biologicals for Autoclave 8960' => 'Biologicals for Autoclave - 8960',
  'CP Slides IVT' => 'CP Slides (IVT)',
  'IVT Equipment Monitoring Logs' => 'IVT Equipment Monitoring Logs',
  'IVT Montitoring System Temp Graphs' => 'IVT Montitoring System Temp Graphs',
  '8945 8960 RV Filter Change Logs' => '8945, 8960, RV Filter Change Logs',
  '8945 8960 9055 Generator Operation Checklist' => '8945, 8960, 9055 Generator Operation Checklist',
  'Animal Receipt Logs' => 'Animal Receipt Logs',
  '8960 Weekly Maintenance Shower Eyewash' => '8960 Weekly Maintenance (Shower & Eyewash)',
  'Analytical Services pH Calibration Log' => 'Analytical Services pH Calibration Log',
  'Analytical Services Solution Log Book Stock Solutions' => 'Analytical Services Solution Log Book- Stock Solutions',
  'Analytical Services Solution Log Book Stock Dilutions' => 'Analytical Services Solution Log Book- Stock Dilutions',
  'Analytical Services Solution Log Book Standard Curves' => 'Analytical Services Solution Log Book- Standard Curves',
  'Analytical Services Solution Log Book Buffers Mobile Phases Other' => 'Analytical Services Solution Log Book- Buffers, Mobile Phases, Other',
  'Analytical Services Solution Preparation' => 'Analytical Services Solution Preparation',
  'Analytical Services Standard Curve Spiking Solution Preparation' => 'Analytical Services Standard Curve Spiking Solution Preparation',
  'LAL Standard Curves' => 'LAL Standard Curves',
  'CascadeBFTII QC' => 'Cascade/BFTII QC',
  'Spectrophotometer Weekly Verification' => 'Spectrophotometer Weekly Verification',
  'Solution Preparation Documents' => 'Solution Preparation Documents',
  'Cell SplittingMedia Change Forms Mouse Lymphoma Balb L929 V79 THP Chok1 other cell lines' => 'Cell Splitting/Media Change Forms - Mouse Lymphoma, Balb, L929, V79, THP, Chok1, other cell lines',
  'Cell Plating Records' => 'Cell Plating Records',
  'Serum Heat Inactivation' => 'Serum Heat Inactivation',
  'ProtoCOL3 Colony Counter Validation Records' => 'ProtoCOL3 Colony Counter Validation Records',
  'Cell Revival Records' => 'Cell Revival Records',
  'Aliquot Creation Logs' => 'Aliquot Creation Logs',
  'Flow Cytometer CST maintenance' => 'Flow Cytometer CST / maintenance',
  'Moxi' => 'Moxi',
  'Blood Loop Controls' => 'Blood Loop Controls',
  'iSTAT Validation' => 'iSTAT Validation',
  'Scale Validation' => 'Scale Validation',
  'Glove Box' => 'Glove Box',
  'Hazardous Waste Log' => 'Hazardous Waste Log',
  'Osmometer Log' => 'Osmometer Log',
  'pH Log' => 'pH Log',
  'Equipment Routine Maintenance' => 'Equipment Routine Maintenance',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_stability_considerations_ii_list.php

 // created: 2021-11-09 11:10:12

$app_list_strings['stability_considerations_ii_list']=array (
  '' => '',
  'No' => 'No',
  'Yes' => 'Yes',
  'Unknown' => 'Unknown',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_yes_no_na_list.php

 // created: 2021-11-09 11:16:43

$app_list_strings['yes_no_na_list']=array (
  '' => '',
  'Yes' => 'Yes',
  'No' => 'No',
  'NA' => 'NA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_inventory_item_category_list.php

 // created: 2021-11-10 00:26:20

$app_list_strings['inventory_item_category_list']=array (
  'Product' => 'Product',
  'Record' => 'Record',
  'Solution' => 'Solution',
  'Specimen' => 'Specimen',
  '' => '',
  'Study Article' => 'Study Article',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_inventory_item_owner_list.php

 // created: 2021-11-10 00:31:56

$app_list_strings['inventory_item_owner_list']=array (
  '' => '',
  'APS Owned' => 'APS Owned',
  'APS Supplied' => 'APS Supplied',
  'Client Owned' => 'Client Owned',
  'APS Supplied Client Owned' => 'APS Supplied, Client Owned',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_im_location_type_list.php

 // created: 2021-11-10 00:50:52

$app_list_strings['im_location_type_list']=array (
  '' => '',
  'Cabinet' => 'Cabinet',
  'Equipment' => 'Equipment',
  'Known' => 'Known',
  'Room' => 'Room',
  'RoomCabinet' => 'Room/Cabinet',
  'RoomShelf' => 'Room/Shelf',
  'Shelf' => 'Shelf',
  'Unknown' => 'Unknown',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_inventory_item_status_list.php

 // created: 2021-11-10 01:01:23

$app_list_strings['inventory_item_status_list']=array (
  '' => '',
  'Offsite Archived' => 'Offsite Archived',
  'Offsite Archived Expired' => 'Offsite Archived (Expired)',
  'Offsite Inventory' => 'Offsite Inventory',
  'Offsite Inventory Expired' => 'Offsite Inventory (Expired)',
  'Onsite Archived' => 'Onsite Archived',
  'Onsite Archived Expired' => 'Onsite Archived (Expired)',
  'Onsite Inventory' => 'Onsite Inventory',
  'Onsite Inventory Expired' => 'Onsite Inventory (Expired)',
  'Planned Inventory' => 'Planned Inventory',
  'Discarded' => 'Discarded',
  'Exhausted' => 'Exhausted',
  'Invoiced' => 'Invoiced',
  'Obsolete' => 'Obsolete',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_timepoint_type_list.php

 // created: 2021-11-10 02:21:35

$app_list_strings['timepoint_type_list']=array (
  '' => '',
  'AcuteBaseline' => 'Acute/Baseline',
  'AcuteTermination' => 'Acute/Termination',
  'Ad Hoc' => 'Ad Hoc',
  'ChronicBaseline' => 'Chronic/Baseline',
  'ChronicTreatment' => 'Chronic/Treatment',
  'Defined' => 'Defined',
  'Follow up' => 'Follow-up',
  'Model CreationBaseline' => 'Model Creation/Baseline',
  'None' => 'None',
  'Receipt' => 'Receipt',
  'Termination' => 'Termination',
  'Vet Order' => 'Vet Order',
  'Work Product Schedule' => 'Work Product Schedule',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_inventory_item_storage_condition_list.php

 // created: 2021-11-10 02:34:15

$app_list_strings['inventory_item_storage_condition_list']=array (
  '' => '',
  'Ambient Temperature' => 'Ambient Temperature',
  'Frozen' => 'Frozen',
  'Refrigerated' => 'Refrigerated',
  'Room Temperature' => 'Room Temperature',
  'To Be Determined' => 'To Be Determined',
  'Ultra Frozen in Freezer' => 'Ultra Frozen in Freezer',
  'Ultra Frozen in Liquid Nitrogen' => 'Ultra Frozen in Liquid Nitrogen',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_yes_no_list.php

 // created: 2021-11-10 14:11:40

$app_list_strings['yes_no_list']=array (
  '' => '',
  'Yes' => 'Yes',
  'No' => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_aaalac_and_usda_exemptions_list.php

 // created: 2021-11-18 10:19:23

$app_list_strings['aaalac_and_usda_exemptions_list']=array (
  '' => '',
  'Conscious Restraint' => 'Conscious Restraint (USDA and AAALAC)',
  'Exercise Restriction dogs' => ' Exercise Restriction (dogs) (USDA and AAALAC)',
  'FoodWater Restriction' => 'Food/Water Restriction (aside from approved fasting) (USDA and AAALAC)',
  'Multiple Major Surgeries 1protocol' => 'Multiple Major Survival Surgery (1/protocol) (USDA and AAALAC)',
  'Multiple Survival Surgeries' => 'Multiple Survival Surgery (AAALAC)',
  'Neuromuscular Blocker' => ' Neuromuscular Blocker/Paralytics (AAALAC)',
  'Personnel Training' => ' Personnel Training (AAALAC)',
  'Survival Surgery' => 'Survival Surgery (AAALAC)',
  'Toxicity Study' => 'Toxicity Study (AAALAC)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_tsd_wt_type_list.php

 // created: 2021-11-30 12:30:22

$app_list_strings['tsd_wt_type_list']=array (
  '' => '',
  'Actual' => 'Actual',
  'Actual or having suitable anatomy' => 'Actual or having suitable anatomy',
  'Weight Appropriate to Age' => 'Weight Appropriate to Age',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_procedure_type_com_list.php

 // created: 2021-12-07 09:02:56

$app_list_strings['procedure_type_com_list']=array (
  '' => '',
  'Acute' => 'Acute',
  'Initial' => 'Initial',
  'Follow up' => 'Follow up',
  'Screening' => 'Screening',
  'Termination' => 'Termination',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_TSD_purpose_list.php

 // created: 2021-12-07 09:34:22

$app_list_strings['TSD_purpose_list']=array (
  '' => '',
  'Protect item or device from animal no additional weight added' => 'Protect item or device from animal (no additional weight added)',
  'Hold device or item additional weight added' => 'Hold device or item (additional weight added)',
  'Connected to external items' => 'Connected to external items (machines, computers, devices, etc.)',
  'Restrict animals movement only while working with the animal' => 'Restrict animal’s movement only while working with the animal',
  'Complete awake imaging where the animal is required to remain calm and still' => 'Complete awake imaging where the animal is required to remain calm and still',
  'Complete awake imaging where animal is required to be in a specific position for imaging' => 'Complete awake imaging where animal is required to be in a specific position for imaging',
  'Perform procedure on animal while in sling' => 'Perform procedure on animal while in sling',
  'Gain access to limbs or specific area' => 'Gain access to limbs or specific area',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_housing_requirements_list.php

 // created: 2021-12-07 09:55:32

$app_list_strings['housing_requirements_list']=array (
  '' => '',
  'Animals require their own room' => 'Animals require their own room',
  'Electronics present in room' => 'Electronics present in room',
  'Additional space in room required' => 'Additional space in room required',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_number_list.php

 // created: 2021-12-07 10:32:47

$app_list_strings['number_list']=array (
  '' => '',
  '01' => '01',
  '02' => '02',
  '03' => '03',
  '04' => '04',
  '05' => '05',
  '06' => '06',
  '07' => '07',
  '08' => '08',
  '09' => '09',
  10 => '10',
  11 => '11',
  12 => '12',
  13 => '13',
  14 => '14',
  15 => '15',
  16 => '16',
  17 => '17',
  18 => '18',
  19 => '19',
  20 => '20',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.NAMSA_Subcontracting_Companies.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['NSC_NAMSA_Sub_Companies'] = 'NAMSA Subcontracting Companies';
$app_list_strings['moduleListSingular']['NSC_NAMSA_Sub_Companies'] = 'NAMSA Subcontracting Company';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.NAMSA_Test_Codes.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.NAMSA_Test_Codes.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['TC_NAMSA_Test_Codes'] = 'NAMSA Test Codes';
$app_list_strings['moduleListSingular']['TC_NAMSA_Test_Codes'] = 'NAMSA Test Code';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['TC_NAMSA_Test_Codes'] = 'NAMSA Test Codes';
$app_list_strings['moduleListSingular']['TC_NAMSA_Test_Codes'] = 'NAMSA Test Code';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sales_study_article_status_list.php

 // created: 2021-12-07 12:27:57

$app_list_strings['sales_study_article_status_list']=array (
  '' => '',
  'To Ship From NW' => 'To Ship From NW',
  'Shipped to MPLS' => 'Shipped to MPLS',
  'Sponsor to Ship Directly to MPLS' => 'Sponsor to Ship Directly to MPLS',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_degree_of_immobility_list.php

 // created: 2021-12-14 09:08:41

$app_list_strings['degree_of_immobility_list']=array (
  '' => '',
  'Minimal' => 'Minimal',
  'Partial' => 'Partial',
  'Minimal Partial' => 'Minimal & Partial',
  'Complete' => 'Complete',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_type_of_acclimation_list.php

 // created: 2021-12-14 09:09:41

$app_list_strings['type_of_acclimation_list']=array (
  '' => '',
  'JacketHarness' => 'Jacket/Harness',
  'E collar' => 'E-collar',
  'C collar' => 'C-collar',
  'Manual restraint' => 'Manual restraint',
  'Leash' => 'Leash',
  'Other' => 'Other',
  'Crossties' => 'Crossties',
  'Sling' => 'Sling',
  'Imaging Cart' => 'Imaging Cart',
  'Pyrogen testingRabbit restrainer' => 'Pyrogen testing/Rabbit restrainer',
  'Crossties Jacket' => 'Crossties & Jacket',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_inspection_results_list.php

 // created: 2021-12-21 08:32:40

$app_list_strings['inspection_results_list']=array (
  '' => '',
  'No Findings' => 'No Findings',
  'Findings' => 'Findings',
  'Observations' => 'Observations (Obsolete)',
  'Findings_Observations' => 'Findings & Observations (Obsolete)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_or_status_list.php

 // created: 2021-12-28 07:20:36

$app_list_strings['or_status_list']=array (
  '' => '',
  'Open' => 'Open',
  'Complete' => 'Complete',
  'Partially Submitted' => 'Partially Submitted',
  'Fully Submitted' => 'Fully Submitted',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_tpr_reference_list.php

 // created: 2022-01-07 06:42:33

$app_list_strings['tpr_reference_list']=array (
  '' => '',
  'Clinical Observations Form' => 'F-S-IL-GN-OP-001-01 – Clinical Observations Form',
  'PARPPC Observations' => 'F-S-IL-GN-OP-001-02 – PAR-PPC Observations – Multiple Incision Form',
  'Anesthesia Recovery Monitoring' => 'F-S-IL-GN-OP-007-01 - Anesthesia Recovery Monitoring',
  'Animal Prep Form' => 'F-S-IL-GN-OP-013-01 – Animal Prep Form',
  'MultiAnimal Procedure Prep Form' => 'F-S-IL-GN-OP-013-02 – Multi-Animal Procedure Prep Form',
  'MultiAnimal Recovery Form' => 'F-S-IL-GN-OP-068-01 - Multi-Animal Recovery Form',
  'Physical Examination Form' => 'F-S-IL-GN-VT-001-01 – Physical Examination Form',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_inventory_item_type_list.php

 // created: 2022-01-18 07:04:56

$app_list_strings['inventory_item_type_list']=array (
  '' => '',
  'Accessory Article' => 'Accessory Article',
  'Accessory Product' => 'Accessory Product',
  'Balloons' => 'Balloons',
  'Block' => 'Block',
  'Chemical' => 'Chemical',
  'Cleaning Agent' => 'Cleaning Agent',
  'Control Article' => 'Control Article',
  'Culture' => 'Culture',
  'Data Book' => 'Data Book',
  'Data Sheet' => 'Data Sheet',
  'Drug' => 'Drug',
  'EDTA Plasma' => 'EDTA Plasma',
  'Equipment Facility Record' => 'Equipment / Facility Record',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Hard drive' => 'Hard drive',
  'Hard Drive' => 'Hard Drive',
  'Na Heparin Plasma' => 'Na Heparin Plasma',
  'NaCit Plasma' => 'NaCit Plasma',
  'Other' => 'Other',
  'Protocol Book' => 'Protocol Book',
  'Reagent' => 'Reagent',
  'Serum' => 'Serum',
  'Slide' => 'Slide',
  'Test Article' => 'Test Article',
  'Tissue' => 'Tissue',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_universal_inventory_management_type_list.php

 // created: 2022-01-18 07:18:35

$app_list_strings['universal_inventory_management_type_list']=array (
  '' => '',
  'Accessory Article' => 'Accessory Article',
  'Accessory Product' => 'Accessory Product',
  'Autoclave supplies' => 'Autoclave supplies',
  'Balloon Catheter' => 'Balloon Catheter',
  'Bandaging' => 'Bandaging',
  'Block' => 'Block',
  'Blood draw supply' => 'Blood draw supply',
  'Cannula' => 'Cannula',
  'Catheter' => 'Catheter',
  'Chemical' => 'Chemical',
  'Cleaning Agent' => 'Cleaning Agent',
  'Cleaning Supplies' => 'Cleaning Supplies',
  'Contrast' => 'Contrast',
  'Control Article' => 'Control Article',
  'Culture' => 'Culture',
  'Data Book' => 'Data Book',
  'Data Sheet' => 'Data Sheet',
  'Drug' => 'Drug',
  'EDTA Plasma' => 'EDTA Plasma',
  'Endotracheal TubeSupplies' => 'Endotracheal Tube/Supplies',
  'Enrichment Toy' => 'Enrichment Toy',
  'Equipment' => 'Equipment',
  'Equipment Facility Record' => 'Equipment / Facility Record',
  'ETO supplies' => 'ETO supplies',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Feeding supplies' => 'Feeding supplies',
  'Graft' => 'Graft',
  'Hard drive' => 'Hard drive',
  'Hard Drive' => 'Hard Drive',
  'Inflation Device' => 'Inflation Device',
  'NA Heparin Plasma' => 'NA Heparin Plasma',
  'NaCit Plasma' => 'NaCit Plasma',
  'Office Supply' => 'Office Supply',
  'Other' => 'Other',
  'Protocol Book' => 'Protocol Book',
  'Reagent' => 'Reagent',
  'Serum' => 'Serum',
  'Sheath' => 'Sheath',
  'Shipping supplies' => 'Shipping supplies',
  'Slide' => 'Slide',
  'Solution' => 'Solution',
  'Stent' => 'Stent',
  'Surgical Instrument' => 'Surgical Instrument',
  'Surgical site prep' => 'Surgical site prep',
  'Suture' => 'Suture',
  'Test Article' => 'Test Article',
  'Tissue' => 'Tissue',
  'Tubing' => 'Tubing',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
  'Wire' => 'Wire',
  'Balloons' => 'Balloons',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_type_specimen_list.php

 // created: 2022-01-25 09:17:11

$app_list_strings['type_specimen_list']=array (
  '' => '',
  'Balloons' => 'Balloons',
  'Block' => 'Block',
  'Culture' => 'Culture',
  'EDTA Plasma' => 'EDTA Plasma',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Na Heparin Plasma' => 'Na Heparin Plasma',
  'NaCit Plasma' => 'NaCit Plasma',
  'Other' => 'Other',
  'Serum' => 'Serum',
  'Slide' => 'Slide',
  'Tissue' => 'Tissue',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_phase_of_inspection_list.php

 // created: 2022-02-01 07:31:01

$app_list_strings['phase_of_inspection_list']=array (
  '' => '',
  'Application' => 'Application',
  'Analytical' => 'Analytical',
  'Animal Health Report' => 'Animal Health Report',
  'Blood Collection' => 'Blood Collection',
  'Blood Evaluation' => 'Blood Evaluation',
  'Body Weights Observations' => 'Body Weights/Observations',
  'Cell Processing' => 'Cell Processing',
  'Colony Counting' => 'Colony Counting',
  'Contributing Scientist Report' => 'Contributing Scientist Report',
  'Controlled Document Review' => 'Controlled Document Review',
  'Data Review' => 'Data Review',
  'Dose' => 'Dose',
  'Evaluation' => 'Evaluation',
  'Faxitron' => 'Faxitron',
  'Final Report' => 'Final Report',
  'Followup Procedure' => 'Follow-up Procedure',
  'Histology' => 'Histology',
  'Implant' => 'Implant',
  'In Life' => 'In-Life',
  'In_Life Report' => 'In-Life Report',
  'Interim M_M report' => 'Interim Animal Health Report',
  'Interim Pathology Report' => 'Interim Pathology Report',
  'Interim Report' => 'Interim Report',
  'Model Creation' => 'Model Creation',
  'Morphometry' => 'Morphometry',
  'Necropsy' => 'Necropsy',
  'Pathology Report' => 'Pathology Report',
  'Protocol Amendment' => 'Protocol Amendment',
  'Protocol Review' => 'Protocol Review',
  'Final Report Amendment' => 'Report Amendment',
  'Sample Preparation' => 'Sample Preparation',
  'Sample Receipt' => 'Sample Receipt',
  'SEM Report' => 'SEM Report',
  'Staining' => 'Staining',
  'Terminal Procedure' => 'Terminal Procedure',
  'Tissue Trimming' => 'Tissue Trimming',
  'Treatment' => 'Treatment',
  'Other_Non Study Specific' => 'Other - Non Study Specific',
  'Out of Office' => 'Out of Office',
  'Training Second Review' => 'Training – Second Review',
  'Training CPI' => 'Training – CPI',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_room_type_list.php

 // created: 2022-02-01 07:38:49

$app_list_strings['room_type_list']=array (
  '' => '',
  'Animal Housing' => 'Animal Housing',
  'Archive' => 'Archive',
  'Behavior' => 'Behavior',
  'Cath Lab' => 'Cath Lab',
  'CT' => 'CT',
  'IT' => 'IT',
  'Lab' => 'Lab',
  'Necropsy' => 'Necropsy',
  'OR' => 'OR',
  'Shop' => 'Shop',
  'Storage' => 'Storage',
  'Various Locations' => 'Various Locations',
  'Warehouse' => 'Warehouse',
  'Wetlab' => 'Wetlab',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Operator_Tracking.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['OT_Operator_Tracking'] = 'Operator Tracking';
$app_list_strings['moduleListSingular']['OT_Operator_Tracking'] = 'Operator Tracking';
$app_list_strings['operator_role_list']['Primary'] = 'Primary';
$app_list_strings['operator_role_list']['Secondary'] = 'Secondary';
$app_list_strings['operator_role_list']['Observer'] = 'Observer';
$app_list_strings['operator_role_list'][''] = '';
$app_list_strings['ot_category_list'][1] = '1';
$app_list_strings['ot_category_list'][''] = '';
$app_list_strings['ot_type_list'][1] = '1';
$app_list_strings['ot_type_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.CAPA_Files.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['cf_capa_files_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['cf_capa_files_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['cf_capa_files_category_dom']['Sales'] = 'Sales';
$app_list_strings['cf_capa_files_category_dom'][''] = '';
$app_list_strings['cf_capa_files_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['cf_capa_files_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['cf_capa_files_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['cf_capa_files_subcategory_dom'][''] = '';
$app_list_strings['cf_capa_files_status_dom']['Active'] = 'Active';
$app_list_strings['cf_capa_files_status_dom']['Draft'] = 'Draft';
$app_list_strings['cf_capa_files_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['cf_capa_files_status_dom']['Expired'] = 'Expired';
$app_list_strings['cf_capa_files_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['cf_capa_files_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['CF_CAPA_Files'] = 'CAPA Files';
$app_list_strings['moduleListSingular']['CF_CAPA_Files'] = 'CAPA File';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.CAPA.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['CAPA_CAPA'] = 'CAPAs';
$app_list_strings['moduleListSingular']['CAPA_CAPA'] = 'CAPA';
$app_list_strings['ca_andor_pa_list']['CA'] = 'CA';
$app_list_strings['ca_andor_pa_list']['PA'] = 'PA';
$app_list_strings['ca_andor_pa_list'][''] = '';
$app_list_strings['risk_assessment_list']['Minor'] = 'Minor – No further investigation required. Complete CAPA Review.';
$app_list_strings['risk_assessment_list']['Significant'] = 'Significant – Investigation Report and Corrective Action Plan required.';
$app_list_strings['risk_assessment_list']['Not applicable'] = 'Not applicable – Preventative Action Plan required.';
$app_list_strings['risk_assessment_list'][''] = '';
$app_list_strings['yes_no_list']['Yes'] = 'Yes';
$app_list_strings['yes_no_list']['No'] = 'No';
$app_list_strings['yes_no_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_ca_andor_pa_dd_list.php

 // created: 2022-02-03 08:09:56

$app_list_strings['ca_andor_pa_dd_list']=array (
  '' => '',
  'CA' => 'CA',
  'PA' => 'PA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_frequency_score_list.php

 // created: 2022-02-03 08:24:46

$app_list_strings['frequency_score_list']=array (
  '' => '',
  'NA' => 'NA',
  1 => '1 - Rarely',
  2 => '2 - Occasionally',
  3 => '3 - Frequently',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_severity_score_list.php

 // created: 2022-02-03 08:27:26

$app_list_strings['severity_score_list']=array (
  '' => '',
  'NA' => 'NA',
  1 => '1 - Minor',
  3 => '3 - Significant',
  5 => '5 - Critical',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_GD_status_list.php

 // created: 2022-02-03 09:43:55

$app_list_strings['GD_status_list']=array (
  '' => '',
  'In Development' => 'In Development',
  'Complete' => 'Complete',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_task_type_list.php

 // created: 2022-02-03 09:59:20

$app_list_strings['task_type_list']=array (
  '' => '',
  'Document Creation' => 'Document Creation',
  'Document Review' => 'Document Review',
  'Financial' => 'Financial',
  'Histopathology' => 'Histopathology',
  'Invoice' => 'Invoice',
  'Lead Follow Up' => 'Lead Follow Up',
  'Maintenance Request' => 'Maintenance Request',
  'Procedure Scheduling' => 'Procedure Scheduling',
  'Quote Follow Up' => 'Quote Follow Up',
  'Quote Review' => 'Quote Review',
  'Schedule Matrix Review' => 'Schedule Matrix Review',
  'Send Necropsy Findings' => 'Send Necropsy Findings',
  'Sponsor Communication' => 'Sponsor Communication',
  'Sponsor Follow Up' => 'Sponsor Follow Up',
  'Sponsor Quote Review' => 'Sponsor Quote Review',
  'Tradeshow' => 'Tradeshow',
  'Vet Check' => 'Vet Check',
  'Work Product' => 'Work Product',
  'Custom' => 'Custom',
  'Standard' => 'Custom',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_ot_category_list.php

 // created: 2022-02-08 07:41:30

$app_list_strings['ot_category_list']=array (
  '' => '',
  'Cardiopulmonary Bypass' => 'Cardiopulmonary Bypass',
  'CatheterWire' => 'Catheter/Wire',
  'Circulatory Assist' => 'Circulatory Assist',
  'Electrophysiology' => 'Electrophysiology',
  'Embolization' => 'Embolization',
  'Endoscopy' => 'Endoscopy',
  'General Cardiac' => 'General Cardiac',
  'Hemostasis' => 'Hemostasis',
  'Integumentary' => 'Integumentary',
  'Leads' => 'Leads',
  'Muscoskeletal' => 'Muscoskeletal',
  'Non Cardiac Ablation' => 'Non Cardiac Ablation',
  'Other' => 'Other',
  'StentsBalloons' => 'Stents/Balloons',
  'Transplant' => 'Transplant',
  'Valve' => 'Valve',
  'Vascular Anastomosis' => 'Vascular Anastomosis',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_ot_type_list.php

 // created: 2022-02-08 07:42:47

$app_list_strings['ot_type_list']=array (
  '' => '',
  'Ablation' => 'Ablation',
  'Mapping' => 'Mapping',
  'AblationMapping' => 'Ablation/Mapping',
  'Peripheral' => 'Peripheral',
  'Coronary' => 'Coronary',
  'Gastrointestinal' => 'Gastrointestinal',
  'Pulmonary' => 'Pulmonary',
  'Urinary' => 'Urinary',
  'Stent Graft' => 'Stent Graft',
  'Surgical Mitral' => 'Surgical Mitral',
  'Transcatheter Mitral' => 'Transcatheter Mitral',
  'Surgical Tricuspid' => 'Surgical Tricuspid',
  'Transcatheter Tricuspid' => 'Transcatheter Tricuspid',
  'Surgical Aortic' => 'Surgical Aortic',
  'Transcatheter Aortic' => 'Transcatheter Aortic',
  'Surgical Pulmonary' => 'Surgical Pulmonary',
  'Transcatheter Pulmonary' => 'Transcatheter Pulmonary',
  'Interpositional' => 'Interpositional',
  'AVF' => 'AVF',
  'AVG' => 'AVG',
  'CABG' => 'CABG',
  'Cebrebral' => 'Cebrebral',
  'Cardiac' => 'Cardiac',
  'Protection' => 'Protection',
  'Renal' => 'Renal',
  'Thigh' => 'Thigh',
  'Bladder' => 'Bladder',
  'Prostate' => 'Prostate',
  'Surgical LVAD' => 'Surgical LVAD',
  'Transcatheter LVAD' => 'Transcatheter LVAD',
  'Aortic' => 'Aortic',
  'ECMO' => 'ECMO',
  'Liver' => 'Liver',
  'Kidney' => 'Kidney',
  'Left Atrial Closure' => 'Left Atrial Closure',
  'Septal Treatment' => 'Septal Treatment',
  'Thrombectomy' => 'Thrombectomy',
  'Gastroscopy' => 'Gastroscopy',
  'Bronchoscopy' => 'Bronchoscopy',
  'Laprascopy' => 'Laprascopy',
  'Thorascopy' => 'Thorascopy',
  'Intercardiac' => 'Intercardiac',
  'Epicardial' => 'Epicardial',
  'Epidural' => 'Epidural',
  'Vascular Closure' => 'Vascular Closure',
  'Hemostatic Agents' => 'Hemostatic Agents',
  'Vessel Sealing Device' => 'Vessel Sealing Device',
  'Wound Creation' => 'Wound Creation',
  'Bandaging' => 'Bandaging',
  'Surface modulation' => 'Surface modulation',
  'Cardiac Evaluation' => 'Cardiac Evaluation',
  'Peripheral Evaluation' => 'Peripheral Evaluation',
  'TendonLigament Repair' => 'Tendon/Ligament Repair',
  'Orthopedics' => 'Orthopedics',
  'Cartilage Defect' => 'Cartilage Defect',
  'Joint injection' => 'Joint injection',
  'Other' => 'Other',
  'Aneurysm Elastase' => 'Aneurysm Elastase',
  'Aneurysm Surgical' => 'Aneurysm Surgical',
  'Standard' => 'Standard',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.addModuleLink.php


//create the links label
$app_strings['LNK_REPORTS_C'] = 'All Notification for a Study';
$app_strings['LNK_ALLNOTIFICATIONFORSTUDY'] = 'All Notification for a Study';
$app_strings['LNK_ALLDEVIATIONSFORSTUDY'] = 'All Deviation For Study';
$app_strings['LNK_ALLADVERSEEVENTSFORSTUDY'] = 'All Adverse Events for a Study';
$app_strings['LNK_MOSTRECENTWPAFORTESTSYSTEM'] = 'WPA Report';
$app_strings['LNK_CUSTOMCOMREPORT'] = 'Pyrogen Animal Use';
$app_strings['LNK_CUSTOMTSWEIGHT'] = 'Custom Test System Weight';
$app_strings['LNK_CUSTOMTSCAPACITY'] = 'Custom Capacity Report';
$app_strings['LNK_CUSTOMTSWPAALLOCATION'] = 'Ops Support Assignments Review';
$app_strings['LNK_CUSTOMTSDEVIATIONPERANIMAL'] = 'Deviations per Animal by Department';
$app_strings['LNK_CUSTOMTSPATHOLOGYWPD'] = 'Custom report for Pathology WPDs';
$app_strings['LNK_CUSTOMOUTTOSPONSORWPD'] = 'WPDs Out to Sponsor for 30+ Days';
$app_strings['LNK_DAILYSCHEDULINGNEEDS'] = 'Daily Scheduling Needs - LA';
$app_strings['LNK_DAILYSCHEDULINGNEEDSSA'] = 'Daily Scheduling Needs - SA';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_organ_system_list.php

 // created: 2022-02-17 07:17:17

$app_list_strings['organ_system_list']=array (
  '' => '',
  'Cardiovascular' => 'Cardiovascular',
  'Clin Path' => 'Clin Path',
  'GI' => 'Gastrointestinal',
  'Integumentary' => 'Integumentary',
  'Lymphatic' => 'Lymphatic',
  'Musculoskeletal' => 'Musculoskeletal',
  'Neurologic' => 'Neurologic',
  'Not Applicable' => 'Not Applicable',
  'Ophthalmic' => 'Ophthalmic',
  'Respiratory' => 'Respiratory',
  'Surgical' => 'Surgical',
  'Systemic' => 'Systemic',
  'Urogential' => 'Urogenital',
  'Unknown ADR' => 'Unknown-ADR (Obsolete)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_po_status_list.php

 // created: 2022-02-22 07:07:34

$app_list_strings['po_status_list']=array (
  '' => '',
  'Pending' => 'Pending',
  'Submitted' => 'Submitted',
  'Complete' => 'Complete',
  'Cancelled' => 'Cancelled',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_deliverable_status_list.php

 // created: 2022-02-22 07:36:28

$app_list_strings['deliverable_status_list']=array (
  '' => '',
  'Pending method development' => 'Pending method development',
  'In Progress' => 'In Progress',
  'Waiting on Sponsor' => 'Out to Sponsor, Waiting on Redlines',
  'Waiting_on_Subcontracted_Report' => 'Waiting on Subcontracted Report',
  'Waiting On Sponsor Ready to Audit' => 'Out to Sponsor, Ready to Audit',
  'Completed' => 'Completed',
  'Overdue' => 'Overdue',
  'Sent to Study DirectorPrinciple Investigator' => 'Sent to Study Director/Principle Investigator',
  'Sponsor Retracted' => 'Sponsor Retracted',
  'None' => 'None',
  'Not Performed' => 'Not Performed',
  'Completed Account on Hold' => 'Completed, Account on Hold',
  'Returned to Sponsor' => 'Returned to Sponsor',
  'Sent to Third Party' => 'Sent to Third Party',
  'Discarded' => 'Discarded',
  'Out to Study Director Waiting on Redlines' => 'Out to Study Director, Waiting on Redlines',
  'In Data Review' => 'In Data Review',
  'Ready for Pathologist' => 'Ready for Pathologist',
  'Ready for Study Director' => 'Ready for Study Director',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_ii_related_to_c_list.php

 // created: 2022-02-24 08:23:25

$app_list_strings['ii_related_to_c_list']=array (
  '' => '',
  'Internal Use' => 'Internal Use',
  'Multiple Test Systems' => 'Test System(s)',
  'Order Request Item' => 'Order Request Item',
  'Purchase Order Item' => 'Purchase Order Item',
  'Sales' => 'Sales',
  'Work Product' => 'Work Product',
  'Single Test System' => 'Single Test System (Obsolete)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_specimen_type_list.php

 // created: 2022-02-24 13:54:36

$app_list_strings['specimen_type_list']=array (
  '' => '',
  'Balloons' => 'Balloons',
  'Culture' => 'Culture',
  'EDTA Plasma' => 'EDTA Plasma',
  'Fecal' => 'Fecal',
  'NaCit Plasma' => 'NaCit Plasma',
  'Na Heparin Plasma' => 'Na Heparin Plasma',
  'Other' => 'Other',
  'Serum' => 'Serum',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_specimen_type_no_other_c_list.php

 // created: 2022-02-24 13:56:09

$app_list_strings['specimen_type_no_other_c_list']=array (
  '' => '',
  'Balloons' => 'Balloons',
  'Culture' => 'Culture',
  'EDTA Plasma' => 'EDTA Plasma',
  'Fecal' => 'Fecal',
  'NaCit Plasma' => 'NaCit Plasma',
  'Na Heparin Plasma' => 'Na Heparin Plasma',
  'Serum' => 'Serum',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_integration_work_stream_list.php

 // created: 2022-03-15 07:23:37

$app_list_strings['integration_work_stream_list']=array (
  '' => '',
  'Accounting Finance Tax' => 'Accounting/Finance/Tax',
  'Building Buildouts' => 'Building Buildouts',
  'Commercial Plan' => 'Commercial Plan',
  'Communications Plans' => 'Communications Plans',
  'Cultural Integration Plan' => 'Cultural Integration Plan',
  'eQMS' => 'eQMS',
  'Financial Force ERP' => 'Financial Force ERP',
  'Human Resources' => 'Human Resources',
  'Integration Strategy Governance' => 'Integration Strategy & Governance',
  'IT' => 'IT',
  'Labware' => 'Labware',
  'Metrics for Success' => 'Metrics for Success',
  'NAMSA 360 Single Portal' => 'NAMSA 360 (Single Portal)',
  'Operations Biosafety Programs' => 'Operations: Biosafety Programs',
  'Operations Brooklyn Park' => 'Operations: Brooklyn Park',
  'Operations Coon Rapids' => 'Operations: Coon Rapids',
  'Operations Coon Rapids Build Out' => 'Operations: Coon Rapids Build Out',
  'Procurement Supply Chain' => 'Procurement / Supply Chain',
  'PSA  Financial Force' => 'PSA – Financial Force',
  'Quality' => 'Quality',
  'Sugar Software' => 'Sugar Software',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_plan_actual_list.php

 // created: 2022-03-15 07:31:07

$app_list_strings['plan_actual_list']=array (
  '' => '',
  'Actual SP' => 'Actual - WP',
  'Actual' => 'Actual - TS Specific',
  'Plan' => 'Plan - TS',
  'Plan SP' => 'Plan - WP',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_td_audit_phase_list.php

 // created: 2022-03-24 08:53:17

$app_list_strings['td_audit_phase_list']=array (
  '' => '',
  'Sample Prep' => 'Sample Prep',
  'Application' => 'Application',
  'Evaluation' => 'Evaluation',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sale_document_category_list.php

 // created: 2022-03-31 11:46:56

$app_list_strings['sale_document_category_list']=array (
  '' => '',
  'Change Order' => 'Change Order',
  'Interinstitutional Assurance' => 'Interinstitutional Assurance',
  'PO' => 'PO',
  'Pricelist Submission' => 'Pricelist Submission',
  'Publication' => 'Publication',
  'Study Quote' => 'Quote',
  'Signed Change Order' => 'Signed Change Order',
  'Signed Quote' => 'Signed Quote',
  'Signed SOW' => 'Signed SOW',
  'SOW' => 'SOW',
  'Study Design Document' => 'Study Design',
  'Subcontractor Quote' => 'Subcontractor Quote',
  'Quote Revision' => 'Quote Revision (Obsolete)',
  'Signed Quote Revision' => 'Signed Quote Revision/PO (Obsolete)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_pro_subtype_list.php

 // created: 2022-03-23 12:23:58

$app_list_strings['pro_subtype_list']=array (
  '' => '',
  'Access Catheter' => 'Access Catheter',
  'Adaptor' => 'Adaptor',
  'Angioplasty Balloon' => 'Angioplasty Balloon',
  'Aortic Root Cannula' => 'Aortic Root Cannula',
  'Arterial Cannula' => 'Arterial Cannula',
  'Bare Metal Stent' => 'Bare Metal Stent',
  'Barium Contrast' => 'Barium Contrast',
  'Binders and supplies' => 'Binders and supplies',
  'Bottle' => 'Bottle',
  'Breathing circuit' => 'Breathing circuit',
  'Brush' => 'Brush',
  'Butterfly Catheter' => 'Butterfly Catheter',
  'CD case' => 'CD case',
  'Central Venous Catheter' => 'Central Venous Catheter',
  'Clipper/Clipper Blade' => 'Clipper/Clipper Blade',
  'Coated' => 'Coated',
  'Cold Pack' => 'Cold Pack',
  'Connector' => 'Connector',
  'Contrast Spike' => 'Contrast Spike',
  'Corrugated box' => 'Corrugated box',
  'Cryovial' => 'Cryovial',
  'Cutting Balloon' => 'Cutting Balloon',
  'Diagnostic Catheter' => 'Diagnostic Catheter',
  'Dressing' => 'Dressing',
  'Drug Coated Balloon' => 'Drug Coated Balloon',
  'Drug Coated Stent' => 'Drug Coated Stent',
  'Dry Ice' => 'Dry Ice',
  'Enrichment treats' => 'Enrichment treats',
  'EP Catheter' => 'EP Catheter',
  'ePTFE' => 'ePTFE',
  'Essentials Kit' => 'Essentials Kit',
  'ET Tube' => 'ET Tube',
  'Ethibond' => 'Ethibond',
  'Ethilon' => 'Ethilon',
  'Exchange Wire' => 'Exchange Wire',
  'Extension Catheter' => 'Extension Catheter',
  'Extension line' => 'Extension line',
  'Extension Tubing' => 'Extension Tubing',
  'Feeding Tube' => 'Feeding Tube',
  'Flammable' => 'Flammable',
  'Floor scrubber supplies' => 'Floor scrubber supplies',
  'Foley Catheter' => 'Foley Catheter',
  'Guide Catheter' => 'Guide Catheter',
  'Guide Wire' => 'Guide Wire',
  'High Pressure' => 'High Pressure',
  'High Pressure Tubing' => 'High Pressure Tubing',
  'Incision site cover' => 'Incision site cover',
  'Indicator label' => 'Indicator label',
  'Injectable' => 'Injectable',
  'Injection port' => 'Injection port',
  'Insulated shipper' => 'Insulated shipper',
  'Introducer Sheath' => 'Introducer Sheath',
  'Invasive Blood Pressure Supplies' => 'Invasive Blood Pressure Supplies',
  'Iodinated Contrast' => 'Iodinated Contrast',
  'IV Catheter' => 'IV Catheter',
  'Knitted' => 'Knitted',
  'Label' => 'Label',
  'Laminating Sheets' => 'Laminating Sheets',
  'Machine' => 'Machine',
  'Mapping Catheter' => 'Mapping Catheter',
  'Marking Wire' => 'Marking Wire',
  'Micro Catheter' => 'Micro Catheter',
  'Mila Catheter' => 'Mila Catheter',
  'Monocryl' => 'Monocryl',
  'Needle' => 'Needle',
  'Needless port' => 'Needless port',
  'Non Flammable' => 'Non-Flammable',
  'Oral' => 'Oral',
  'Other' => 'Other',
  'OTW over the wire' => 'OTW (over the wire)',
  'Padding' => 'Padding',
  'Pallet' => 'Pallet',
  'Paper' => 'Paper',
  'Patches' => 'Patches',
  'PDS' => 'PDS',
  'Pediatric Cannula' => 'Pediatric Cannula',
  'Perc Needle' => 'Perc Needle',
  'Pledget' => 'Pledget',
  'POBA Balloon' => 'POBA Balloon',
  'Pressure Catheter' => 'Pressure Catheter',
  'Primary diet' => 'Primary diet',
  'Probe Cover' => 'Probe Cover',
  'Prolene' => 'Prolene',
  'PTA Balloon' => 'PTA Balloon',
  'Replacement Part' => 'Replacement Part',
  'Silk' => 'Silk',
  'SpongePad' => 'Sponge/Pad',
  'Steerable Sheath' => 'Steerable Sheath',
  'Stopcock' => 'Stopcock',
  'Stylet' => 'Stylet',
  'Sub layer wrap' => 'Sub layer wrap',
  'Supplement diet' => 'Supplement diet',
  'Support Catheter' => 'Support Catheter',
  'Support Wire' => 'Support Wire',
  'Surgical scrub' => 'Surgical scrub',
  'Syringe' => 'Syringe',
  'Tape' => 'Tape',
  'Ti Cron' => 'Ti-Cron',
  'Top layer wrap' => 'Top layer wrap',
  'Topical' => 'Topical',
  'Torque Device' => 'Torque Device',
  'Transseptal Catheter' => 'Transseptal Catheter',
  'Trash bag' => 'Trash bag',
  'Tuohy' => 'Tuohy',
  'Vacutainer' => 'Vacutainer',
  'Venous Cannula' => 'Venous Cannula',
  'Vessel Cannula' => 'Vessel Cannula',
  'Vicryl' => 'Vicryl',
  'Woven' => 'Woven',
  'Wrap' => 'Wrap',
  'Hemoconcentrator' => 'Hemoconcentrator',
  'Oxygenator' => 'Oxygenator',
  'Reservoir' => 'Reservoir',
  'Pump pack' => 'Pump pack',
  'Tubing' => 'Tubing',
  'Safety glasses' => 'Safety glasses',
  'Ear protection' => 'Ear protection',
  'Face shield' => 'Face shield',
  'Mask' => 'Mask',
  'Bouffanthair net' => 'Bouffant/hair net',
  'Coveralloverall' => 'Coverall/overall',
  'Shoe covers' => 'Shoe covers',
  'Boots' => 'Boots',
  'Gloves non sterile' => 'Gloves non sterile',
  'Gloves sterile' => 'Gloves sterile',
  'Surgical gown sterile' => 'Surgical gown sterile',
  'Surgical gown non sterile' => 'Surgical gown non sterile',
  'Reducer' => 'Reducer',
  'WYE' => 'WYE',
  'Monoderm' => 'Monoderm',
  'PDO' => 'PDO',
  'MonoAdjustable Loop' => 'Mono-Adjustable Loop',
  'Fluid' => 'Fluid',
  'Tissue' => 'Tissue',
  'Imaging Catheter' => 'Imaging Catheter',
  'Beaker' => 'Beaker',
  'Flask' => 'Flask',
  'Drape' => 'Drape',
  'Aortic Punch' => 'Aortic Punch',
  'Biopsy Punch' => 'Biopsy Punch',
  'Hand Scrub' => 'Hand Scrub',
  'Gauze' => 'Gauze',
  'ECG supply' => 'ECG supply',
  'Cautery Supply' => 'Cautery Supply',
  'Table drape' => 'Table drape',
  'Sponge' => 'Sponge',
  'Blade' => 'Blade',
  'CT supply' => 'CT supply',
  'Inhalant' => 'Inhalant',
  'Intranasal' => 'Intranasal',
  'IV Solution' => 'IV Solution',
  'Container' => 'Container',
  'Peel Away' => 'Peel Away',
  'Patient Drape' => 'Patient Drape',
  'Drainage supply' => 'Drainage supply',
  'SharpsBiohazard' => 'Sharps/Biohazard',
  'Consumable' => 'Consumable',
  'Grade A Glassware' => 'Grade A Glassware',
  'Beakers' => 'Beakers',
  'Pipette Tips' => 'Pipette Tips',
  'Tubes' => 'Tubes',
  'Vial Closures' => 'Vial Closures',
  'LCMS Columns' => 'LCMS Columns',
  'LCMS Parts' => 'LCMS Parts',
  'GCMS Columns' => 'GCMS Columns',
  'GCMS Parts' => 'GCMS Parts',
  'ICPMS Parts' => 'ICPMS Parts',
  'Software' => 'Software',
  'Solvents Aqueous' => 'Solvents - Aqueous',
  'Solvents Organic' => 'Solvents - Organic',
  'Control Matrix' => 'Control Matrix',
  'Analytes' => 'Analytes',
  'Flammable Solution' => 'Flammable Solution',
  'Corrosive Solution' => 'Corrosive Solution',
  'Toxic Solution' => 'Toxic Solution',
  'Storage Racks' => 'Storage Racks',
  'Storage container' => 'Storage container',
  'Misc' => 'Misc.',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_product_category_list.php

 // created: 2022-03-31 12:04:51

$app_list_strings['product_category_list']=array (
  '' => '',
  'Autoclave supplies' => 'Autoclave supplies',
  'Balloon Catheter' => 'Balloon Catheter',
  'Bandaging' => 'Bandaging',
  'Blood draw supply' => 'Blood draw supply',
  'Bypass supply' => 'Bypass supply',
  'Cannula' => 'Cannula',
  'Catheter' => 'Catheter',
  'Chemical' => 'Chemical',
  'Cleaning Agent' => 'Cleaning Agent',
  'Cleaning Supplies' => 'Cleaning Supplies',
  'Contrast' => 'Contrast',
  'Control Matrix' => 'Control Matrix',
  'Drug' => 'Drug',
  'Endotracheal TubeSupplies' => 'Endotracheal Tube/Supplies',
  'Enrichment Toy' => 'Enrichment Toy',
  'Equipment' => 'Equipment',
  'ETO supplies' => 'ETO supplies',
  'Feeding supplies' => 'Feeding supplies',
  'Glassware' => 'Glassware',
  'Graft' => 'Graft',
  'Inflation Device' => 'Inflation Device',
  'Interventional Supplies' => 'Interventional Supplies',
  'Lab Supplies' => 'Lab Supplies',
  'Office Supply' => 'Office Supply',
  'Other' => 'Other',
  'Plastic Ware' => 'Plastic Ware',
  'PPE supply' => 'PPE supply',
  'Reagent' => 'Reagent',
  'Sheath' => 'Sheath',
  'Shipping supplies' => 'Shipping supplies',
  'Solution' => 'Solution',
  'Stent' => 'Stent',
  'Surgical Instrument' => 'Surgical Instrument',
  'Surgical site prep' => 'Surgical site prep',
  'Surgical Supplies' => 'Surgical Supplies',
  'Suture' => 'Suture',
  'Tubing' => 'Tubing',
  'Wire' => 'Wire',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_Breed_List.php

 // created: 2022-04-05 07:21:33

$app_list_strings['Breed_List']=array (
  '' => '',
  'Alpine' => 'Alpine',
  'Athymic Nude' => 'Athymic Nude',
  'BALBc' => 'BALB/c',
  'Beagle' => 'Beagle',
  'C3H HeJ' => 'C3H/HeJ',
  'C57' => 'C57',
  'CD Hairless' => 'CD Hairless',
  'CD1' => 'CD1',
  'CF1' => 'CF1',
  'Cross Breed' => 'Cross Breed - NonSpecific',
  'Dorset' => 'Dorset',
  'Dorset X' => 'Dorset X',
  'Dutch Belted' => 'Dutch Belted',
  'Freisen' => 'Freisen',
  'Golden Syrian' => 'Golden Syrian',
  'Gottingen' => 'Gottingen',
  'Hampshire' => 'Hampshire',
  'Hampshire X' => 'Hampshire X',
  'Hanford' => 'Hanford',
  'Hartley' => 'Hartley',
  'Holstein' => 'Holstein',
  'Jersey' => 'Jersey',
  'Lamancha' => 'Lamancha',
  'Landrace Cross' => 'Landrace Cross',
  'LDLR' => 'LDLR Yucatan',
  'Lewis' => 'Lewis',
  'Micro Yucatan' => 'Micro-Yucatan',
  'Mongrel' => 'Mongrel',
  'Myotonic' => 'Myotonic',
  'ND4' => 'ND4',
  'New Zealand White' => 'New Zealand White',
  'Ossabaw' => 'Ossabaw',
  'Polypay' => 'Polypay',
  'Polypay X' => 'Polypay X',
  'Saanen' => 'Saanen',
  'Sinclair' => 'Sinclair',
  'SKH1 Hairless' => 'SKH1 Hairless',
  'Sprague Dawley' => 'Sprague Dawley',
  'Suffolk' => 'Suffolk',
  'Suffolk X' => 'Suffolk X',
  'Toggenburg' => 'Toggenburg',
  'Watanabe' => 'Watanabe',
  'Wistar' => 'Wistar',
  'Yorkshire X' => 'Yorkshire X',
  'Yucatan' => 'Yucatan',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_ii_test_type_list.php

 // created: 2022-04-12 09:03:57

$app_list_strings['ii_test_type_list']=array (
  '' => '',
  'Aerobic and Anaerobic Culture ID Bacteria and Fungi' => 'Aerobic and Anaerobic Culture, ID-Bacteria and Fungi',
  'Aerobic Culture and ID Bacteria only' => 'Aerobic Culture and ID-Bacteria only',
  'Aerobic Culture and ID Fungi only' => 'Aerobic Culture and ID-Fungi only',
  'Amylase andor Lipase' => 'Amylase and/or Lipase',
  'Anti Factor Xa' => 'Anti-Factor Xa',
  'Blood Culture' => 'Blood Culture',
  'CBC' => 'CBC',
  'CBC plus Heartworm' => 'CBC plus Heartworm',
  'CBC with Reticulocytes' => 'CBC with Reticulocytes',
  'Chemistry' => 'Chemistry',
  'Coagulation' => 'Coagulation',
  'Cryptosporidium ELISA' => 'Cryptosporidium ELISA',
  'Custom Chemistry' => 'Custom Chemistry',
  'D Dimer' => 'D-Dimer',
  'Fecal Ova and Parasite' => 'Fecal Ova and Parasite',
  'Fibrinogen' => 'Fibrinogen',
  'Free Hemoglobin' => 'Free Hemoglobin',
  'Giardia ELISA' => 'Giardia ELISA',
  'Haptoglobin' => 'Haptoglobin',
  'Ionized Calcium' => 'Ionized Calcium',
  'Other' => 'Other',
  'PK in Balloons' => 'Pharmacokinetic (PK) in Balloons',
  'PK in Plasma' => 'Pharmacokinetic (PK) in Plasma',
  'PK in Serum' => 'Pharmacokinetic (PK) in Serum',
  'PK in Tissue' => 'Pharmacokinetic (PK) in Tissue',
  'PK in Whole Blood' => 'Pharmacokinetic (PK) in Whole Blood',
  'PT aPTT' => 'PT, aPTT',
  'PT aPTT Fibrinogen' => 'PT, aPTT, Fibrinogen',
  'Troponin 1' => 'Troponin 1',
  'Urinalysis' => 'Urinalysis',
  'Urine ProteinCreatinine Ratio' => 'Urine Protein/Creatinine Ratio',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_task_procedure_list.php

 // created: 2022-04-21 08:33:25

$app_list_strings['task_procedure_list']=array (
  'Task' => 'Task',
  'Procedure' => 'Procedure',
  'Unscheduled Task' => 'Unscheduled Task',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Batch_ID.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['BID_Batch_ID'] = 'Batch IDs';
$app_list_strings['moduleListSingular']['BID_Batch_ID'] = 'Batch ID';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_bid_status_list.php

 // created: 2022-04-26 07:09:10

$app_list_strings['bid_status_list']=array (
  '' => '',
  'Open' => 'Open',
  'Testing' => 'Testing',
  'Closed' => 'Closed',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_biod_dose_route_list.php

 // created: 2022-04-26 07:17:37

$app_list_strings['biod_dose_route_list']=array (
  '' => '',
  'IV' => 'IV',
  'IP' => 'IP',
  'IV IP' => 'IV & IP',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_td_phase_list.php

 // created: 2022-04-25 17:09:07

$app_list_strings['td_phase_list']=array (
  '' => '',
  'Ind I' => 'Ind I',
  'SLS' => 'SLS',
  'Ind II' => 'Ind II',
  'Ind 1' => 'Ind 1',
  'Ind 2' => 'Ind 2',
  'Ind III' => 'Ind 3',
  'Ind IV' => 'Ind 4',
  'Ind V' => 'Ind 5',
  'Ind VI' => 'Ind 6',
  'Ind VII' => 'Ind 7',
  'Ind VIII' => 'Ind 8',
  'Ind IX' => 'Ind 9',
  'Challenge' => 'Challenge',
  'Dose 1' => 'Dose 1',
  'Dose 2' => 'Dose 2',
  'Dose 3' => 'Dose 3',
  'Dose 4' => 'Dose 4',
  'Dose 5' => 'Dose 5',
  'Day 0' => 'Day 0',
  'Day 1' => 'Day 1',
  'Day 2' => 'Day 2',
  'Day 3' => 'Day 3',
  'Day 4' => 'Day 4',
  'Day 5' => 'Day 5',
  'Day 6' => 'Day 6',
  'Day 7' => 'Day 7',
  'Day 8' => 'Day 8',
  'Day 9' => 'Day 9',
  'Day 10' => 'Day 10',
  'Day 11' => 'Day 11',
  'Day 12' => 'Day 12',
  'Day 13' => 'Day 13',
  'Day 14' => 'Day 14',
  'Day 15' => 'Day 15',
  'Day 16' => 'Day 16',
  'Day 17' => 'Day 17',
  'Day 18' => 'Day 18',
  'Day 19' => 'Day 19',
  'Day 20' => 'Day 20',
  'Day 21' => 'Day 21',
  'Day 22' => 'Day 22',
  'Day 23' => 'Day 23',
  'Day 24' => 'Day 24',
  'Day 25' => 'Day 25',
  'Day 26' => 'Day 26',
  'Day 27' => 'Day 27',
  'Day 28' => 'Day 28',
  'Day 29' => 'Day 29',
  'Day 30' => 'Day 30',
  'Day 31' => 'Day 31',
  'Day 32' => 'Day 32',
  'Day 33' => 'Day 33',
  'Day 34' => 'Day 34',
  'Day 35' => 'Day 35',
  'Day 36' => 'Day 36',
  'Day 37' => 'Day 37',
  'Day 38' => 'Day 38',
  'Day 39' => 'Day 39',
  'Day 40' => 'Day 40',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_inventory_management_type_list.php

 // created: 2022-05-19 07:21:44

$app_list_strings['inventory_management_type_list']=array (
  '' => '',
  'Archive Offsite' => 'Archive Offsite',
  'Archive Onsite' => 'Archive Onsite',
  'Create Inventory Collection' => 'Create Inventory Collection',
  'Discard' => 'Discard',
  'Exhausted' => 'Exhausted',
  'External Transfer' => 'External Transfer',
  'Internal Transfer Analysis' => 'Internal Transfer - Analysis',
  'Internal Transfer' => 'Internal Transfer - Storage',
  'Missing' => 'Missing',
  'Obsolete' => 'Obsolete',
  'Separate Inventory Items' => 'Separate Inventory Collection Item(s)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_wpc_type_list.php

 // created: 2022-05-24 07:05:18

$app_list_strings['wpc_type_list']=array (
  '' => '',
  'IR0' => 'IR0',
  'IR1' => 'IR1',
  'IR2' => 'IR2',
  'IR3' => 'IR3',
  'SE0' => 'SE0',
  'SE1' => 'SE1',
  'ST0' => 'ST0',
  'ST1' => 'ST1',
  'ST2' => 'ST2',
  'ST3' => 'ST3',
  'ST4' => 'ST4',
  'IM0' => 'IM0',
  'CY0' => 'CY0',
  'CY1' => 'CY1',
  'CY2' => 'CY2',
  'CY3' => 'CY3',
  'CY4' => 'CY4',
  'CY5' => 'CY5',
  'CY6' => 'CY6',
  'CY7' => 'CY7',
  'HE0' => 'HE0',
  'HE1' => 'HE1',
  'HE2' => 'HE2',
  'HE3' => 'HE3',
  'HE4' => 'HE4',
  'HE5' => 'HE5',
  'HE7' => 'HE7',
  'HE8' => 'HE8',
  'HE9' => 'HE9',
  'GE0' => 'GE0',
  'GE1' => 'GE1',
  'GE2' => 'GE2',
  'GE3' => 'GE3',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_work_product_status_list.php

 // created: 2022-05-24 07:15:38

$app_list_strings['work_product_status_list']=array (
  'Pending' => 'Pending',
  'In Development' => 'In Development',
  'Testing' => 'Testing',
  'Reporting' => 'Reporting',
  'Archived' => 'Complete',
  'Transferred to NW' => 'Transferred to NW',
  'Withdrawn' => 'Study Not Performed',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_aaalac_requirements_list.php

 // created: 2022-06-07 05:25:36

$app_list_strings['aaalac_requirements_list']=array (
  '' => '',
  'Neuromuscular BlockerParalytics' => 'Neuromuscular Blocker/Paralytics',
  'Personnel Training' => 'Personnel Training',
  'Survival Surgery' => 'Survival Surgery',
  'Toxicity Study' => 'Toxicity Study',
  'None' => 'None',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_usda_exemptions_list.php

 // created: 2022-06-07 05:29:34

$app_list_strings['usda_exemptions_list']=array (
  '' => '',
  'Conscious Restraint' => 'Conscious Restraint',
  'Canine Exercise Restrictions' => 'Canine Exercise Restrictions',
  'FoodWater Restriction' => 'Food/Water Restriction (aside from approved fasting)',
  'Multiple Major Survival Surgeries on a Single Protocol' => 'Multiple Major Survival Surgeries on a Single Protocol',
  'Survival Surgeries on Multiple Protocols' => 'Survival Surgeries on Multiple Protocols',
  'None' => 'None',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_department_list.php

 // created: 2022-06-16 05:44:24

$app_list_strings['department_list']=array (
  '' => '',
  'Analytical Services' => 'Analytical',
  'Business Development' => 'Business Development',
  'Clinical Pathology' => 'Clinical Pathology',
  'Facilities' => 'Facilities',
  'Finance' => 'Finance',
  'Histology Services' => 'Histology Services',
  'Human_Resources' => 'Human Resources',
  'In Vitro Testing' => 'In Vitro Testing',
  'In life Large Animal Care' => 'In-life Large Animal Care',
  'Inlife Large Animal prepRecovery' => 'In-life Large Animal Prep/Recovery',
  'In life Large Animal Research' => 'In-life Large Animal Research',
  'In life Small Animal Care' => 'In-life Small Animal Care',
  'In life Small Animal Research' => 'In-life Small Animal Research',
  'In life Small Animal Surgical Research' => 'In-life Small Animal Surgical Research',
  'Information_Technology' => 'Information Technology',
  'Interventional Surgical Research' => 'Interventional Surgical Research',
  'Interventional Surgical Research Operators' => 'Interventional Surgical Research Operators',
  'Interventional Surgical Research Technicians' => 'Interventional Surgical Research Technicians',
  'Operations' => 'Operations',
  'Operations Support' => 'Operations Support',
  'Pathology Services' => 'Pathology Services',
  'Pharmacology' => 'Pharmacology Services',
  'Process Improvement' => 'Process Improvement',
  'Quality Assurance Unit' => 'Quality Assurance Unit',
  'Regulatory Services' => 'Regulatory Services',
  'Sample Preparation' => 'Sample Preparation',
  'Scientific' => 'Scientific',
  'Software Development' => 'Software Development',
  'Toxicology' => 'Toxicology Services',
  'Veterinary Services' => 'Veterinary Services',
  'Employee' => 'Associate',
  'Lab Services' => 'Lab Services (Obsolete)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_location_list.php

 // created: 2022-06-21 06:26:40

$app_list_strings['location_list']=array (
  780 => '780 86th Ave.',
  8945 => '8945 Evergreen Blvd.',
  8960 => '8960 Evergreen Blvd.',
  9055 => '9055 Evergreen Blvd.',
  'Remote' => 'Remote',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_extraction_conditions_list.php

 // created: 2022-07-12 09:29:22

$app_list_strings['extraction_conditions_list']=array (
  '' => '',
  '37C' => '37C',
  '50C' => '50C',
  '70C' => '70C',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_payment_terms_list.php

 // created: 2022-07-28 06:41:23

$app_list_strings['payment_terms_list']=array (
  '' => '',
  'check_with_finance' => 'Check with Finance',
  'aps account' => 'APS Account - Net 30',
  'Net 30' => 'Net 30',
  'Net 45' => 'Net 45',
  'Net 60' => 'Net 60',
  'Net 77' => 'Net 77',
  'Net 90' => 'Net 90',
  'new client' => 'New Client - per bid',
  'Per MSA' => 'Per MSA',
  'pre_pay' => 'Pre-pay',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_error_category_list.php

 // created: 2022-10-04 05:47:53

$app_list_strings['error_category_list']=array (
  '' => '',
  'Complaint' => 'Complaint',
  'Early Term' => 'Early Term (Obsolete)',
  'Facility' => 'Facility',
  'Kudos' => 'Kudos',
  'Major' => 'Major',
  'Minor' => 'Minor',
  'OFI' => 'OFI',
  'Program' => 'Program',
  'Sttudy' => 'Study',
  'Trainee' => 'Trainee (Obsolete)',
  'Trainer' => 'Trainer (Obsolete)',
  'Found Dead' => 'Unscheduled Death (Obsolete)',
  'Expanded Study' => 'Expanded Study',
  'Failed Study' => 'Failed Study',
  'Aborted Study' => 'Aborted Study',
  'CAB' => 'CAB',
  'Invalid Study' => 'Invalid Study',
  'Scientific' => 'Scientific',
  'Operations' => 'All Operations',
  'Initial Issue' => 'Initial Issue',
  'Repeat Issue' => 'Repeat Issue',
  'Resolution Notification' => 'Resolution Notification',
  'Observation at Transfer' => 'Observation at Transfer',
  'Clinical Pathology Review' => 'Clinical Pathology Review',
  'Remaining Departments' => 'Remaining Departments',
  'Re_identification' => 'Re-identification',
  'Herd Health' => 'Herd Health',
  'Deceased Animal' => 'Deceased Animal',
  'Not Sedated Study Article NOT Administered Surgical Access NOT Performed' => 'Not Sedated, Study Article NOT Administered, Surgical Access NOT Performed (Obsolete)',
  'Study Article Administered' => 'Study Article Administered (Obsolete)',
  'Study Article NOT Administered Surgical Access NOT Performed' => 'Sedated, Study Article NOT Administered, Surgical Access NOT Performed (Obsolete)',
  'Study Article NOT Administered Surgical Access Performed' => 'Study Article NOT Administered, Surgical Access Performed (Obsolete)',
  'Unused_No Medication' => 'Unused/No Medication (Obsolete)',
  'Unused_Medication Administered' => 'Unused/Medication Administered (Obsolete)',
  'SPA Bug' => 'SPA Bug',
  'Performed Per Protocol' => 'Performed Per Protocol',
  'Rejected' => 'Rejected',
  'Unsuccessful Procedure and Survived' => 'Unsuccessful Procedure and Survived',
  'Procedure Death' => 'Procedure Death',
  'Early Term Outcome' => 'Early Term',
  'Found Deceased' => 'Found Deceased',
  'Not Used' => 'Not Used',
  'Early Death' => 'Early Death',
  'Failed Sham' => 'Failed Sham',
  'Passed Sham' => 'Passed Sham',
  'Failed Pyrogen' => 'Failed Pyrogen',
  'Passed Pyrogen' => 'Passed Pyrogen',
  'Trainee Initial Training' => 'Trainee: Initial Training',
  'Trainee Refresher Training' => 'Trainee: Refresher Training',
  'Trainer Initial Training' => 'Trainer: Initial Training',
  'Trainer Refresher Training' => 'Trainer: Refresher Training',
  'Necropsy Findings Notification' => 'Necropsy Findings Notification',
  'Not used shared BU' => 'Not used – shared BU',
  'Sedation for Animal Health' => 'Sedation for Animal Health',
  'Laboratory Operations' => 'Laboratory Operations',
  'Operations Support' => 'Operations Support',
  'Large Animal Operations' => 'Large Animal Operations',
  'Small Animal Operations' => 'Small Animal Operations',
  'Analytical Operations' => 'Analytical Operations',
  'Inlife Operations' => 'In-life Operations',
  'Pathology Operations' => 'Pathology Operations',
  'Standard Biocomp TS Selection' => 'Standard Biocomp TS Selection',
  'Re Challenge' => 'Re-Challenge',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_deliverable_list.php

 // created: 2022-10-25 12:42:02

$app_list_strings['deliverable_list']=array (
  '' => '',
  'Abnormal Extract' => 'Abnormal Extract',
  'Bioanalytical Data Table' => 'Analytical Data Tables',
  'Analytical ExhaustiveExaggerated Extraction' => 'Analytical Exhaustive/Exaggerated Extraction',
  'Analytical Report' => 'Analytical Final Report',
  'Analytical Headspace Analysis' => 'Analytical Headspace Analysis',
  'Analytical Interim Validation Report' => 'Analytical Interim Validation Report',
  'Analytical Method' => 'Analytical Method',
  'Bioanalytical Method Development' => 'Analytical Method Development',
  'Analytical Nickel Ion Release Extraction' => 'Analytical Nickel Ion Release Extraction',
  'Analytical Pre testing' => 'Analytical Pre-testing',
  'Bioanalytical Protocol Dvelopment' => 'Analytical Protocol Development',
  'Bioanalytical Specimen Analysis' => 'Analytical Sample Analysis',
  'Bioanalytical Sample Prep' => 'Analytical Sample Prep',
  'Bioanalytical Stability Analysis' => 'Analytical Stability Analysis',
  'Bioanalytical Validation Report' => 'Analytical Validation Report',
  'Animal Health Report' => 'Animal Health/Clinical Pathology Report',
  'Animal Selection Populate TSD' => 'Animal Selection – Populate TSD',
  'Animal Selection Procedure Use' => 'Animal Selection – Procedure Use',
  'BC Abnormal Study Article' => 'BC Abnormal Study Article',
  'Biological Evaluation Plan' => 'Biological Evaluation Plan',
  'Chemical and Physical Characterization of Particulates' => 'Chemical and Physical Characterization of Particulates',
  'Contributing Scientist Report Amendment' => 'Contributing Scientist Report Amendment',
  'Critical Phase Audit' => 'Critical Phase Audit',
  'Decalcification' => 'Decalcification',
  'Early DeathTermination Investigation Report' => 'Early Death/Termination Investigation Report',
  'Faxitron' => 'Faxitron',
  'Final Report' => 'Final Report',
  'Final Report Amendment' => 'Final Report Amendment',
  'Frozen Slide Completion' => 'Frozen Slide Completion',
  'GLP Discontinuation Report' => 'GLP Discontinuation Report',
  'Gross Morphometry' => 'Gross Morphometry',
  'Gross Pathology Report' => 'Gross Pathology Report',
  'Histology Controls' => 'Histology Control(s)',
  'Histomorphometry' => 'Histomorphometry',
  'Histomorphometry Report' => 'Histomorphometry Report',
  'Histopathology Methods Report' => 'Histopathology Methods Report',
  'Histopathology Report2' => 'Histopathology Report (histology only)',
  'IACUC Submission' => 'IACUC Submission',
  'In Life Report' => 'In Life Report',
  'Interim Animal Health Report' => 'Interim Animal Health/Clinical Pathology Report',
  'Interim Gross Pathology Report' => 'Interim Gross Pathology Report',
  'Interim Histopathology Report2' => 'Interim Histopathology Report (histology only)',
  'Interim Histopathology Report' => 'Interim Pathology Report (gross and histology)',
  'Interim In Life Report' => 'Interim In Life Report',
  'Interim Report' => 'Interim Report',
  'Paraffin Slide Completion' => 'Paraffin Slide Completion',
  'Pathology Misc' => 'Pathology Misc.',
  'Histopathology Notes' => 'Pathology Notes',
  'Pathology Protocol Review' => 'Pathology Protocol Review',
  'Histopathology Report' => 'Pathology Report (gross and histology)',
  'Pathology Unknown' => 'Pathology - Unknown',
  'Pharmacokinetic Report' => 'Pharmacokinetic Report',
  'Pharmacology Data Summary' => 'Pharmacology Data Summary',
  'EXAKT Slide Completion' => 'Plastic EXAKT Slide Completion',
  'Plastic Microtome Slide Completion' => 'Plastic Microtome Slide Completion',
  'Protocol Development' => 'Protocol Development',
  'QA Data Review' => 'QA Data Review',
  'QC Data Review' => 'QC Data Review',
  'Recuts' => 'Recuts',
  'Regulatory Response' => 'Regulatory Response',
  'Risk Assessment Report' => 'Risk Assessment Report',
  'Risk Assessment Report Review' => 'Risk Assessment Report Review',
  'Sample Prep Design' => 'Sample Prep Design',
  'SEM' => 'SEM',
  'SEND Report' => 'SEND Report',
  'Slide Imaging' => 'Slide Imaging',
  'Slide Scanning' => 'Slide Scanning',
  'SpecimenSample Disposition' => 'Specimen/Sample Disposition',
  'Sponsor Contracted Contributing Scientist Report' => 'Sponsor Contracted Contributing Scientist Report',
  'Statistical Summary' => 'Statistical Summary',
  'Statistical Review' => 'Statistical Review',
  'Statistics Report' => 'Statistics Report',
  'Summary Certificate of ISO 10993' => 'Summary Certificate of ISO 10993',
  'Summary Certificate of USP Class VI Testing' => 'Summary Certificate of USP Class VI Testing',
  'Tissue Receipt' => 'Tissue Receipt',
  'Tissue Trimming' => 'Tissue Trimming',
  'TWT Protocol Review' => 'TWT Protocol Review',
  'TWT Report Review' => 'TWT Report Review',
  'Waiting on external test site' => 'Waiting on External Test Site',
  'Histopathology and Histomorphometry Report' => 'Histopathology and Histomorphometry Report (Obsolete 10/31/2019)',
  'Animal Selection' => 'Animal Selection (Obsolete)',
  'SEM Report' => 'SEM Report (Obsolete 10/31/2019)',
  'SEM Prep' => 'SEM Prep (Obsolete 10/31/2019)',
  'Shipping Request' => 'Shipping Request (Obsolete 3/5/2020)',
  'Slide Completion' => 'Slide Completion (Obsolete 4/24/2019)',
  'Slide Shipping' => 'Slide Shipping (Obsolete 1/31/2020)',
  'Tissue Shipping' => 'Tissue Shipping (Obsolete 1/31/2020)',
  'Analytical Simulated Use Extraction' => 'Analytical Simulated Use Extraction',
  'Analytical Exhaustive Extraction' => 'Analytical Exhaustive Extraction',
  'Analytical Exaggerated Extraction' => 'Analytical Exaggerated Extraction',
  'Analytical Headspace GCMS Analysis' => 'Analytical Headspace GCMS Analysis',
  'Analytical Direct Inject GCMS Analysis' => 'Analytical Direct Inject GCMS Analysis',
  'Analytical LCMS Analysis' => 'Analytical LCMS Analysis',
  'Analytical ICPMS Analysis' => 'Analytical ICPMS Analysis',
  'Analytical Headspace GCMS Interpretation' => 'Analytical Headspace GCMS Interpretation',
  'Analytical Direct Inject GCMS Interpretation' => 'Analytical Direct Inject GCMS Interpretation',
  'Analytical LCMS Interpretation' => 'Analytical LCMS Interpretation',
  'Analytical ICPMS Interpretation' => 'Analytical ICPMS Interpretation',
  'Analytical GCMS Sample Preparation' => 'Analytical GCMS Sample Preparation',
  'Analytical LCMS Sample Preparation' => 'Analytical LCMS Sample Preparation',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_equipment_required_list.php

 // created: 2022-10-27 05:52:42

$app_list_strings['equipment_required_list']=array (
  '' => '',
  '12 lead' => '12-lead',
  'Angio' => 'Angio',
  'Any special Gas Requirements' => 'Any special Gas Requirements',
  'Automated Nociception Analyzer' => 'Automated Nociception Analyzer',
  'Bard' => 'Bard',
  'Bypass' => 'Bypass',
  'C Arm' => 'C-Arm (if in an OR)',
  'Cardiac Output' => 'Cardiac Output',
  'CT' => 'CT',
  'Digital Randall Selitto Device dRS' => 'Digital Randall-Selitto Device (dRS)',
  'Electronic Von Frey eVF' => 'Electronic Von Frey (eVF)',
  'Endoscopic Tower' => 'Endoscopic Tower',
  'Flow Probe' => 'Flow Probe',
  'Fluroscopy' => 'Fluroscopy',
  'Hargreaves Hg' => 'Hargreaves (Hg)',
  'Hot Cold Plate' => 'Hot-Cold Plate',
  'IABP' => 'IABP',
  'IVUS' => 'IVUS',
  'OCT' => 'OCT',
  'Other' => 'Other',
  'Pacemaker handheld' => 'Pacemaker - handheld',
  'Pacemaker Micropacer' => 'Pacemaker - Micropacer',
  'PowerLab' => 'PowerLab',
  'Rotarod' => 'Rotarod',
  'Ultrasound APS' => 'Ultrasound (APS)',
  'Ultrasound Jim B' => 'Ultrasound (Jim B.)',
  'UVB' => 'UVB',
  'Velocity' => 'Velocity',
  'Von Frey vF' => 'Von Frey (vF)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_completion_status_list.php

 // created: 2022-11-08 05:44:12

$app_list_strings['completion_status_list']=array (
  'Complete' => 'Complete',
  'Incomplete' => 'Incomplete',
  'Confirmation Needed' => 'Confirmation(s) Needed',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_standard_task_list.php

 // created: 2022-11-08 05:54:03

$app_list_strings['standard_task_list']=array (
  '' => '',
  'Acute' => 'Acute',
  'Anesthetized Blood Draw' => 'Anesthetized Blood Draw',
  'Awake Blood Draw' => 'Awake Blood Draw',
  'Bandage Change' => 'Bandage Change',
  'Battery ChangeCharge' => 'Battery Change/Charge',
  'Behavioral Assessment' => 'Behavioral Assessment',
  'Body Weight' => 'Body Weight',
  'Chronic' => 'Chronic',
  'CT' => 'CT',
  'Device Checks' => 'Device Checks',
  'Dosing' => 'Dosing',
  'EKGECG' => 'EKG/ECG',
  'Extract In' => 'Extract In',
  'Extract Out' => 'Extract Out',
  'Follow up' => 'Follow-up',
  'Gait Exam' => 'Gait Exam',
  'Implant' => 'Implant',
  'Model Creation' => 'Model Creation',
  'Neruo Exam' => 'Neruo Exam',
  'Physical Exam' => 'Physical Exam',
  'Protocol Specific Acclimation' => 'Protocol Specific Acclimation',
  'Protocol Specific Exercise' => 'Protocol Specific Exercise',
  'Protocol Specific Screening' => 'Protocol Specific Screening',
  'Provide Material' => 'Provide Material',
  'Sample Preparation' => 'Sample Preparation',
  'Stimulation' => 'Stimulation',
  'Termination' => 'Termination',
  'Treatment' => 'Treatment',
  'Ultrasound' => 'Ultrasound',
  'Urinalysis' => 'Urinalysis',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_diagnosis_list.php

 // created: 2022-11-08 09:12:07

$app_list_strings['diagnosis_list']=array (
  '' => '',
  'A Fib' => 'Atrial Fibrillation',
  'Abscess' => 'Abscess',
  'Actinomyces hyovaginalis' => 'Actinomyces hyovaginalis',
  'Adenovirus' => 'Adenovirus',
  'Allergic' => 'Allergic',
  'Anemia' => 'Anemia',
  'Anesthesia recovery' => 'Anesthesia',
  'Animal Altercation' => 'Animal Altercation',
  'Anticoagulation therapy' => 'Anticoagulation therapy',
  'Aortic valve regurgitation' => 'Aortic valve regurgitation',
  'Arthritis' => 'Arthritis',
  'Aspiration' => 'Aspiration',
  'Atrial Fibrillation' => 'Atrial Fibrillation',
  'BALT hyperplasia' => 'BALT hyperplasia',
  'Barbaring' => 'Barbering',
  'BehaviorTemperament' => 'Behavior/Temperament',
  'Bloat' => 'Bloat',
  'Bordetella bronchiseptica' => 'Bordetella bronchiseptica',
  'Brachyspira' => 'Brachyspira',
  'Broken tooth' => 'Fractured tooth',
  'Burn' => 'Burn',
  'Cardiac' => 'Cardiac',
  'Cardiac disease' => 'Cardiac disease',
  'Cardiogenic' => 'Cardiogenic',
  'Carpal Valgus' => 'Carpal Valgus',
  'Carpal Varus' => 'Carpal Varus',
  'Cataract' => 'Cataract',
  'Cellulitis' => 'Cellulitis',
  'Central nervous' => 'Central nervous',
  'Central Venous Line Infection' => 'Central Venous Line Infection',
  'Choke' => 'Choke',
  'Chronic inflammation' => 'Chronic inflammation',
  'CLA' => 'Caseous Lymphadenitis',
  'Clawhoof avulsion' => 'Claw/hoof avulsion',
  'Clostridium perfringens' => 'Clostridium perfringens',
  'Coagulopathy' => 'Coagulopathy',
  'Coccidia' => 'Coccidia',
  'Coccidiosis' => 'Coccidiosis',
  'Colitis' => 'Colitis',
  'Congenital' => 'Congenital',
  'Conjunctivitis' => 'Conjunctivitis',
  'Corneal Edema' => 'Corneal Edema',
  'Corneal Ulcer' => 'Corneal Ulcer',
  'Cryptosporidium' => 'Cryptosporidium',
  'Cyniclomyces guttulatus' => 'Cyniclomyces guttulatus',
  'Cystitis' => 'Cystitis',
  'Dental Disease' => 'Dental Disease',
  'Dermatitis' => 'Dermatitis',
  'Device related' => 'Device related',
  'Diabetes' => 'Diabetes',
  'Dietary indiscretion' => 'Dietary indiscretion',
  'Dietary intolerance' => 'Dietary intolerance',
  'Disuse' => 'Disuse',
  'Dystrophy' => 'Dystrophy',
  'E coli' => 'Escherichia coli',
  'Ear Tag infections' => 'Ear Tag infections',
  'Endocarditis' => 'Endocarditis',
  'Endometritis' => 'Endometritis',
  'Enterococcus gallinarum' => 'Enterococcus gallinarum',
  'Environment' => 'Environment',
  'Epistaxis' => 'Epistaxis',
  'Erysipelas' => 'Erysipelas',
  'Estrus' => 'Estrus',
  'Extracranial' => 'Extracranial',
  'Fleas' => 'Fleas',
  'Foreign Body' => 'Foreign Body',
  'Fracture' => 'Fracture',
  'Fusobacterium' => 'Fusobacterium',
  'Gastric reflux' => 'Gastric reflux',
  'GI disease' => 'GI disease',
  'GI procedure prep' => 'GI procedure prep',
  'GI prep' => 'GI prep',
  'Giardia' => 'Giardia',
  'Glaesserella parasuis' => 'Glaesserella parasuis',
  'Hematoma' => 'Hematoma',
  'Hemorrhage' => 'Hemorrhage',
  'Hemorrhagic gastroenteritis' => 'Hemorrhagic gastroenteritis',
  'Hepatic disease' => 'Hepatic disease',
  'Horners' => 'Horner\'s Syndrome',
  'Hygroma' => 'Hygroma',
  'Idiopathic' => 'Idiopathic',
  'Ileus' => 'Ileus',
  'Incidental' => 'Incidental',
  'Incisional hernia' => 'Incisional hernia',
  'Infection' => 'Infection',
  'Inflammation' => 'Inflammation',
  'Influenza' => 'Influenza',
  'Inguinal hernia' => 'Inguinal hernia',
  'Injection site' => 'Injection site',
  'Injury' => 'Injury',
  'Interdigital Cyst' => 'Interdigital Cyst',
  'Intracranial' => 'Intracranial',
  'Intubation' => 'Intubation',
  'Joint disease' => 'Joint disease',
  'Keratoconjunctivitis Sicca KCS' => 'Keratoconjunctivitis Sicca (KCS)',
  'Klebsiella pneumoniae' => 'Klebsiella pneumoniae',
  'Laminitis' => 'Laminitis',
  'Laryngitis' => 'Laryngitis',
  'Lawsonia' => 'Lawsonia intercellularis',
  'Lice' => 'Lice',
  'Malignant hyperthermia' => 'Malignant hyperthermia',
  'Mange' => 'Mange',
  'Mastitis' => 'Mastitis',
  'Medication' => 'Medication',
  'Medication Overdose' => 'Medication Overdose',
  'Melenoma' => 'Melanoma',
  'Mites' => 'Mites',
  'Mitral valve regurgitation' => 'Mitral valve regurgitation',
  'Monesia' => 'Monesia',
  'Musculoskeletal' => 'Musculoskeletal',
  'Mycoplasma' => 'Mycoplasma',
  'Myodegeneration' => 'Myodegeneration',
  'Neoplasia' => 'Neoplasia',
  'Neurologic' => 'Neurologic',
  'Non cardiogenic' => 'Non-cardiogenic',
  'Normal' => 'Normal',
  'Normal for PostOperative Recovery' => 'Normal for Post-Operative Recovery',
  'OPPV' => 'Ovine Progressive Pneumonia Virus',
  'Oral disease' => 'Oral disease',
  'Orf' => 'Orf',
  'Organomegaly' => 'Organomegaly',
  'Orthopedic injury' => 'Orthopedic injury',
  'Otitis externa' => 'Otitis externa',
  'Otitis interna' => 'Otitis interna',
  'Overgrown Hooves' => 'Overgrown Hooves',
  'Pain' => 'Pain',
  'Pancreatitis' => 'Pancreatitis',
  'Parasitism' => 'Parasitism',
  'Pasteurella multocida' => 'Pasteurella multocida',
  'Patellar Luxation' => 'Patellar Luxation',
  'Pericarditis' => 'Pericarditis',
  'Peripheral nervous' => 'Peripheral nervous',
  'Peritonitis' => 'Peritonitis',
  'Perivascular Administration' => 'Perivascular Administration',
  'Pneumonia' => 'Pneumonia',
  'Pododermatitis' => 'Pododermatitis',
  'Porcine circovirus PCV' => 'Porcine circovirus (PCV)',
  'Porcine Parvovirus' => 'Porcine Parvovirus',
  'Post renal' => 'Post-renal Azotemia',
  'Pre renal' => 'Pre-Renal Azotemia',
  'Pregnancy' => 'Pregnancy',
  'Pseudomonas' => 'Pseudomonas',
  'Cherry Eye' => 'Cherry Eye',
  'Pulmonary disease' => 'Pulmonary disease',
  'Pulmonic valve regurgitation' => 'Pulmonic valve regurgitation',
  'Purpura' => 'Purpura',
  'Pyelonephritis' => 'Pyelonephritis',
  'Pyoderma' => 'Pyoderma',
  'Pyuria' => 'Pyuria',
  'Recent Weaning' => 'Recent Weaning',
  'Renal' => 'Renal Azotemia',
  'Reverse sneezing' => 'Reverse sneezing',
  'Ring worm' => 'Ring worm',
  'Rotavirus' => 'Rotavirus',
  'Rodent Urological Syndrome' => 'Rodent Urological Syndrome',
  'Roundworm' => 'Roundworm',
  'Rubbing' => 'Rubbing',
  'Saccharomycosis' => 'Saccharomycosis',
  'Salmonella' => 'Salmonella',
  'Sarcoma' => 'Sarcoma',
  'Self Mutilation' => 'Self Mutilation',
  'Septic Arthritis' => 'Septic Arthritis',
  'Seroma' => 'Seroma',
  'Shearing Injury' => 'Shearing Injury',
  'Staph aureus' => 'Staphylococcus aureus',
  'Staph other' => 'Staphylococcus sp.',
  'Staphylococcus chromogenes' => 'Staphylococcus chromogenes',
  'Strep' => 'Streptococcus sp.',
  'Strep suis' => 'Streptococcus suis',
  'Stress' => 'Stress',
  'Strongile' => 'Strongyle',
  'Study Related' => 'Study Related',
  'Suture Reaction' => 'Suture Reaction',
  'Tachycardia' => 'Tachycardia',
  'Tender Foot' => 'Tender Foot',
  'Ticks' => 'Ticks',
  'Toxicity' => 'Toxicity',
  'Transfusion' => 'Transfusion',
  'T pyogenes' => 'Trueperella pyogenes',
  'Umbilical hernia' => 'Umbilical hernia',
  'Undefined' => 'Undefined',
  'Upper Respiratory Infection' => 'Upper Respiratory Infection',
  'Urinary disease' => 'Urinary disease',
  'Urinary obstruction' => 'Urinary obstruction',
  'Urolithasis' => 'Urolithasis',
  'Vaccine' => 'Vaccine',
  'Vaginitis' => 'Vaginitis',
  'Vasculitis' => 'Vasculitis',
  'VSD' => 'Ventricular septal defect',
  'Volvulus' => 'Volvulus',
  'Yucatan' => 'Yucatan (Obsolete)',
  'Valve Regurg' => 'Valve Regurg',
  'Vaccine site' => 'Vaccine site (Obsolete)',
  'Urticaria' => 'Urticaria (Obsolete)',
  'Unknown' => 'Unknown (Obsolete)',
  'Ulcer' => 'Ulcer (Obsolete)',
  'Trueperella' => 'Trueperella (Obsolete)',
  'Trauma' => 'Trauma (Obsolete)',
  'Tartar' => 'Tartar (Obsolete)',
  'Secondary GI' => 'Secondary GI (Obsolete)',
  'Rumen' => 'Rumen (Obsolete)',
  'Pulmonary' => 'Pulmonary (Obsolete)',
  'Physiological' => 'Physiological (Obsolete)',
  'Pleural Effusion' => 'Pleural Effusion (Obsolete)',
  'Porphyrinuria' => 'Porphyrinuria (Obsolete)',
  'Post Op' => 'Post-Op (Obsolete)',
  'Skin only' => 'Skin only (Obsolete)',
  'Soft Tissue Injury' => 'Soft Tissue Injury (Obsolete)',
  'Primary GI' => 'Primary GI (Obsolete)',
  'Acquired' => 'Acquired (Obsolete)',
  'Post clipping' => 'Post-clipping (Obsolete)',
  'Bradycardia' => 'Bradycardia (Obsolete)',
  'Congestion' => 'Congestion (Obsolete)',
  'Full thickness dehiscence' => 'Dehiscence (Obsolete)',
  'Edema' => 'Edema (Obsolete)',
  'Gingivitis' => 'Gingivitis (Obsolete)',
  'Heart block' => 'Heart block (Obsolete)',
  'Hematomaseroma' => 'Hematoma/seroma (Obsolete)',
  'Hyphema' => 'Hyphema (Obsolete)',
  'Hemoglobinuria' => 'Hemoglobinuria (Obsolete)',
  'Hoof Crack' => 'Hoof Crack (Obsolete)',
  'Hematuria' => 'Hematuria (Obsolete)',
  'Hypoglycemia' => 'Hypoglycemia (Obsolete)',
  'Hypotension' => 'Hypotension (Obsolete)',
  'Incisional Swelling' => 'Incisional Swelling (Obsolete)',
  'Injection Site Abscess' => 'Injection Site Abscess (Obsolete)',
  'Joint injury' => 'Joint injury (Obsolete)',
  'Microphthalmia' => 'Microphthalmia (Obsolete)',
  'Monogastric' => 'Monogastric (Obsolete)',
  'Muffled heart sounds' => 'Muffled heart sounds (Obsolete)',
  'Nausea' => 'Nausea (Obsolete)',
  'Neoplasia undefined' => 'Neoplasia-undefined (Obsolete)',
  'Neovascularization Pannus' => 'Neovascularization/ Pannus (Obsolete)',
  'NSAID' => 'NSAID (Obsolete)',
  'Oral injury' => 'Oral injury (Obsolete)',
  'Parasitic' => 'Parasitic (Obsolete)',
  'ParesisParalysis' => 'Paresis/Paralysis (Obsolete)',
  'Peritoneal effusion' => 'Peritoneal effusion (Obsolete)',
  'Physiologic' => 'Physiologic (Obsolete)',
  'Feed transition' => 'Feed transition',
  'Mannheimia haemolytica' => 'Mannheimia haemolytica',
  'Sialocele' => 'Sialocele',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_condition_list.php

 // created: 2022-11-08 09:13:28

$app_list_strings['condition_list']=array (
  '' => '',
  'Abdominal distension' => 'Abdominal distension',
  'Abrasion' => 'Abrasion',
  'Abscess' => 'Abscess',
  'ADR' => 'ADR (Obsolete)',
  'Alopecia' => 'Alopecia',
  'Anaphylaxis' => 'Anaphylaxis',
  'Anesthesia' => 'Anesthesia (Obsolete)',
  'Apnea' => 'Apnea',
  'Arrhythmia' => 'Arrhythmia',
  'Ataxia' => 'Ataxia',
  'Atrophy' => 'Atrophy',
  'Azotemia' => 'Azotemia (Obsolete)',
  'Bandage abnormality' => 'Bandage abnormality',
  'Obesity' => 'BCS greater than 3.5',
  'Underweight' => 'BCS less than 2.5',
  'Blepharospasm' => 'Blepharospasm',
  'Blindness' => 'Blindness',
  'Blisters' => 'Blisters',
  'Bloat' => 'Bloat',
  'Bradycardia' => 'Bradycardia',
  'Bruising' => 'Bruising',
  'Chemosis' => 'Chemosis',
  'Choke' => 'Choke',
  'Conformation' => 'Conformation',
  'Conjunctivits' => 'Conjunctivits',
  'Constipation' => 'Constipation',
  'Corneal Edema' => 'Corneal Edema',
  'Corneal Opacity' => 'Corneal Opacity',
  'Coughing' => 'Coughing',
  'Deafness' => 'Deafness',
  'Dehiscence' => 'Dehiscence',
  'Dehydration' => 'Dehydration',
  'Dental disease' => 'Dental Tartar',
  'Diarrhea' => 'Diarrhea',
  'Discharge' => 'Discharge',
  'Discolored urine' => 'Discolored urine',
  'Ectoparasite' => 'Ectoparasite',
  'Enophthalmos' => 'Enophthalmos',
  'Erythema' => 'Erythema',
  'Head Tilt' => 'Head Tilt',
  'Heart murmur' => 'Heart murmur',
  'Hematemesis' => 'Hematemesis',
  'Hematochezia' => 'Hematochezia',
  'Hematuria' => 'Hematuria',
  'Hemorrhage' => 'Hemorrhage',
  'Hyperpigmentation' => 'Hyperpigmentation',
  'Hypothermia' => 'Hypothermia',
  'Hypoxia' => 'Hypoxia',
  'Icteric' => 'Icterus',
  'Ileus' => 'Ileus (Obsolete)',
  'Inappetence' => 'Inappetence',
  'Infection' => 'Infection (Obsolete)',
  'Internal Parasites' => 'Internal Parasites',
  'Intubation' => 'Intubation (Obsolete)',
  'Joint Swelling' => 'Joint Swelling',
  'Laceration' => 'Laceration',
  'Lame' => 'Lame',
  'Lung lesions' => 'Lung lesions',
  'Lymphadenopathy' => 'Lymphadenopathy',
  'Malaise' => 'Malaise',
  'Mass' => 'Mass (Obsolete)',
  'Melena' => 'Melena',
  'Nasal discharge' => 'Nasal Discharge',
  'Necropsy Finding' => 'Necropsy Finding (Obsolete)',
  'Nodule' => 'Nodule',
  'Normal Exam' => 'Normal Exam',
  'Nystagmus' => 'Nystagmus',
  'Other' => 'Other',
  'Otitis Externa' => 'Otitis Externa (Obsolete)',
  'Pallor' => 'Pallor',
  'PapulesPustules' => 'Papules',
  'Paralysis' => 'Paralysis',
  'Paraphimosis' => 'Paraphimosis',
  'Paresis' => 'Paresis',
  'Pericardial effusion' => 'Pericardial effusion',
  'Peritoneal effusionAscites' => 'Peritoneal effusion/Ascites',
  'PetechiaEcchyomoses' => 'Petechia/Ecchyomoses',
  'Pleural effusion' => 'Pleural effusion',
  'Poor Confirmation' => 'Poor Confirmation (Obsolete)',
  'Post Op Exam' => 'Post-Op Exam (Obsolete)',
  'Post op Swelling' => 'Post-op Swelling (Obsolete)',
  'Preputial Swelling' => 'Preputial Swelling',
  'Prolonged CRT Cyanosis' => 'Prolonged CRT / Cyanosis (Obsolete)',
  'Pruritis' => 'Pruritis',
  'Ptyalism' => 'Ptyalism',
  'Pyrexia' => 'Pyrexia',
  'QAcclimation Activity' => 'Q/Acclimation Activity',
  'Rash' => 'Rash',
  'Rectal Bleeding' => 'Rectal Bleeding',
  'Rectal Prolapse' => 'Rectal Prolapse',
  'Recumbency' => 'Recumbency',
  'Respiratory distress' => 'Respiratory distress (Obsolete)',
  'Respiratory Stridor' => 'Respiratory Stridor',
  'Routine Exam' => 'Routine Exam (Obsolete)',
  'Scabbing' => 'Scabbing',
  'Scleral Injection' => 'Scleral Injection',
  'Seizure' => 'Seizure',
  'Self mutilation' => 'Self-mutilation',
  'Shock' => 'Shock',
  'StrainingDysuria' => 'Straining/Dysuria',
  'Swelling' => 'Swelling',
  'Syncope' => 'Syncope',
  'Tachycardia' => 'Tachycardia',
  'Tachypnea' => 'Tachypnea',
  'Trauma' => 'Trauma',
  'Udder Enlargement' => 'Udder Enlargement',
  'Ulcer' => 'Ulcer',
  'Ulceration' => 'Ulceration',
  'Urinary obstruction' => 'Urinary obstruction',
  'Vomiting' => 'Vomiting',
  'Vulvar discharge' => 'Vulvar Discharge',
  'Vulvar Swelling' => 'Vulvar Swelling',
  'Weight loss' => 'Weight Loss',
  'Wound' => 'Wound',
  'Horners' => 'Horner\'s (Obsolete)',
  'Entropion' => 'Entropion',
  'Ectropion' => 'Ectropion',
  'Hypotension' => 'Hypotension',
  'Hypertension' => 'Hypertension',
  'Gingivitis' => 'Gingivitis',
  'Nausea' => 'Nausea',
  'Regurgitation' => 'Regurgitation',
  'Edema' => 'Edema',
  'Hoof crack' => 'Hoof crack',
  'Pustules' => 'Pustules',
  'Urticaria' => 'Urticaria',
  'Hyphema' => 'Hyphema',
  'Microphthalmia' => 'Microphthalmia',
  'NeovascularizationPannus' => 'Neovascularization / Pannus',
  'Dyspnea' => 'Dyspnea',
  'Hemoptysis' => 'Hemoptysis',
  'Hypoglycemia' => 'Hypoglycemia',
  'Bilirubinuria' => 'Bilirubinuria',
  'Hemoglobinuria' => 'Hemoglobinuria',
  'Porphyrinuria' => 'Porphyrinuria',
  'Pyuria' => 'Pyuria',
  'Heart block' => 'Heart block',
  'Muffled heart sounds' => 'Muffled heart sounds',
  'Catheter Site Swelling' => 'Catheter Site Swelling',
  'Incisional swelling' => 'Incisional swelling',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_amendment_request_list.php

 // created: 2022-12-01 05:37:11

$app_list_strings['amendment_request_list']=array (
  '' => '',
  'APS Error' => 'NAMSA',
  'Sponsor Error' => 'Sponsor ',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_error_type_list.php

 // created: 2022-12-01 09:46:38

$app_list_strings['error_type_list']=array (
  'ACT out of range' => '(Obsolete) ACT value out of range',
  'Ancillary Article' => '(Obsolete) Ancillary Article',
  'Animal Health' => '(Obsolete) Animal Health',
  'Animal Weight' => '(Obsolete) Animal Weight',
  'Control Article' => '(Obsolete) Control Article',
  'Controlled Documents Not QCed' => '(Obsolete) Controlled Documents: Not QCed',
  'Documentation Error Other' => '(Obsolete) Documentation Error (Other)',
  'Equipment' => '(Obsolete) Equipment',
  'GLP Noncompliance' => '(Obsolete) GLP Noncompliance',
  'Incorrect Disregard' => '(Obsolete) Incorrect – Disregard',
  'Materials andor Methods' => '(Obsolete) Materials and/or Methods',
  'Medication or Dosing' => '(Obsolete) Medication/Dosing',
  'Missed Task General Protocol or Sop' => '(Obsolete) Missed Task - General (Protocol and SOP)',
  'Missed Task General Protocol' => '(Obsolete) Missed Task - General (Protocol Only)',
  'Missed Task General SOP' => '(Obsolete) Missed Task - General (SOP Only)',
  'Other' => '(Obsolete) Other',
  'Photo Nonconformance' => '(Obsolete) Photo Nonconformance',
  'Random FT glitch' => '(Obsolete) Random FT glitch',
  'Rejected Animal' => '(Obsolete) Rejected Animal',
  'Sample Prep Error' => '(Obsolete) Sample Prep Error',
  'Start up' => '(Obsolete) Start up',
  'Study Article' => '(Obsolete) Study Article',
  'Success Criteria Failure' => '(Obsolete) Success Criteria Failure',
  'Test Article' => '(Obsolete) Test Article',
  'Test System Model' => '(Obsolete) Test System Model',
  'Undetermined' => '(Obsolete) Undetermined',
  'Unused Animal' => '(Obsolete) Unused Animal',
  'Validity Criteria Failure' => '(Obsolete) Validity Criteria Failure',
  '' => '',
  'ACT iSTAT error code' => 'ACT error code',
  'ACT iSTAT starout' => 'ACT star-out',
  'ACT time interval missed' => 'ACT time interval missed',
  'ACT value out of range 1000 sec' => 'ACT value out of range: >1000 sec',
  'ACT value out of range high' => 'ACT value out of range: high',
  'ACT value out of range low' => 'ACT value out of range: low',
  'Additional activity performed' => 'Additional activity performed',
  'Analytical Method Criteria Failure' => 'Analytical: method criteria failure',
  'Analytical standards noncompliance' => 'Analytical: standard(s) noncompliance',
  'Anesthesia Change Without Comment' => 'Anesthesia: change without comment',
  'Anesthesia Incorrect Change' => 'Anesthesia: incorrect change',
  'Anesthesia Monitor Interval Missed' => 'Anesthesia: monitor interval missed',
  'Animal Escape' => 'Animal Escape',
  'Animal Housing Inappropriately Group Housed' => 'Animal Housing: Inappropriately Group Housed',
  'Animal Identification incorrect documentation' => 'Animal Identification: incorrect documentation',
  'Animal Identification incorrect identification' => 'Animal Identification: incorrect identification',
  'Animal Procurement Incorrect' => 'Animal Procurement Incorrect',
  'Animal Re identification' => 'Animal Re-identification',
  'Animal Room Incorrect' => 'Animal Room Incorrect',
  'Animal Weight out of range' => 'Animal Weight: out of range',
  'Animal Weight suspect data' => 'Animal Weight: suspect data',
  'Animal Inadequate Enrichment' => 'Animal: Inadequate Enrichment',
  'Animal Incorrect Assignment' => 'Animal: Incorrect Assignment',
  'Associate did not perform before due date' => 'Associate did not perform before due date',
  'Associate Out of Office' => 'Associate Out of Office',
  'Catheter Concerns' => 'Catheter Concerns',
  'Clinical Pathology Insufficient Volume' => 'Clinical Pathology: Insufficient Volume',
  'Clinical Pathology missing analyte' => 'Clinical Pathology: missing analyte',
  'Clinical Pathology missing vet review' => 'Clinical Pathology: missing vet review',
  'Clinical Pathology sample quality issue' => 'Clinical Pathology: sample quality issue',
  'Controlled Documents Incorrect Document Used' => 'Controlled Documents: Incorrect Document Used',
  'Controlled Documents Not Archived' => 'Controlled Documents: Not Archived',
  'Utilized noncurrent controlled doc' => 'Controlled Documents: Utilized Out-of-date revision',
  'Data Transfer not performed' => 'Data Transfer: not performed',
  'Disregard issue correctable' => 'Disregard: issue correctable',
  'Disregard unnecessary COM' => 'Disregard: unnecessary COM',
  'Documentation illegiblewriteover' => 'Documentation: illegible/writeover',
  'Documentation invalid entry' => 'Documentation: invalid entry',
  'Documentation missing data' => 'Documentation: missing data',
  'Documentation Error' => 'Documentation: missing Date/Initials',
  'Documentation missing footnote' => 'Documentation: missing footnote',
  'Documentation missingincomplete verification' => 'Documentation: missing/incomplete verification',
  'Duplicate' => 'Duplicate',
  'Equipment Malfunction' => 'Equipment: Malfunction',
  'Equipment OOS' => 'Equipment: OOS',
  'Equipment PMCal Overdue' => 'Equipment: PM/Cal Overdue',
  'Equipment Tag Missing' => 'Equipment: Tag Missing',
  'Chemical Reagent Expired' => 'Expired: Chemical / Reagent',
  'Cleaning Supply Expired' => 'Expired: Cleaning Supply',
  'Drug Expired' => 'Expired: Drug',
  'Lab Supply Expired' => 'Expired: Lab Supply',
  'GLP Study Article Expired' => 'Expired: Study Article',
  'Study Article Abnormality' => 'Extraction Abnormality: Study Article',
  'Extract Abnormality' => 'Extraction Abnormality: Vehicle',
  'FastTrack Glitch' => 'FastTrack Glitch',
  'FastTrack Incorrect' => 'FastTrack Incorrect',
  'Fluid Administration' => 'Fluid Administration: out of range',
  'Humidity OOR not OOS' => 'Humidity OOR, not OOS',
  'Incomplete or missed prep task' => 'Incomplete or missed prep task',
  'Incorrect labels' => 'Incorrect label',
  'Intubation Concerns' => 'Intubation Concerns',
  'Chemical Reagent Label Error' => 'Label Error: Chemical / Reagent Label',
  'Cleaning Supply Label Error' => 'Label Error: Cleaning Supply',
  'Drug Label Error' => 'Label Error: Drug',
  'Label Missing' => 'Label Error: Missing Label',
  'Specimen Label Error' => 'Label Error: Specimen',
  'GLP Study Article Label Error' => 'Label Error: Study Article',
  'MedicationDosing Wrong Dose' => 'Medication/Dosing: Wrong Dose',
  'MedicationDosing Wrong route' => 'Medication/Dosing: Wrong route',
  'Missed Exam scheduling' => 'Missed Exam scheduling',
  'Missed task scheduling' => 'Missed task scheduling',
  'Missed Task Acclimation' => 'Missed Task: Acclimation',
  'Missed Task Bandaging' => 'Missed Task: Bandaging',
  'Missed Task Cage Cleaning' => 'Missed Task: Cage Cleaning',
  'Missed Task Clinical Pathology' => 'Missed Task: Clinical Pathology',
  'Missed Deliverable Generation' => 'Missed Task: Deliverable Generation',
  'Missed Task Exam' => 'Missed Task: Exam',
  'Missed Task Exercise' => 'Missed Task: Exercise',
  'Missed Task Food' => 'Missed Task: Feeding',
  'Missed Task Histology' => 'Missed Task: Histology',
  'Missed Task Imaging' => 'Missed Task: Imaging',
  'Missed Task Microbiology' => 'Missed Task: Microbiological Monitoring',
  'Missed Task Necropsy' => 'Missed Task: Necropsy',
  'Missed Task Observation' => 'Missed Task: Observation',
  'Missed Task Prep' => 'Missed Task: Prep',
  'Missed Task Preventive Health' => 'Missed Task: Preventive Health',
  'Missed Task Protocol Activity' => 'Missed Task: Protocol Activity',
  'Missed Task QuarantineIsolation Release' => 'Missed Task: Quarantine/Isolation Release',
  'Missed Task Re Marking' => 'Missed Task: Re-Marking',
  'Missed Task Specimen Collection' => 'Missed Task: Specimen Collection',
  'Missed Task Specimen Processing' => 'Missed Task: Specimen Processing',
  'Missed Task Substance Administration' => 'Missed Task: Substance Administration',
  'Missed Task Urinary Management' => 'Missed Task: Urinary Management',
  'Missed Task Vet Order' => 'Missed Task: Vet Order',
  'Missed Task Vitals Monitor' => 'Missed Task: Vitals Monitor',
  'Missed Task Water' => 'Missed Task: Watering',
  'Missed Task Weight' => 'Missed Task: Weight',
  'Missed Task Wound Care' => 'Missed Task: Wound Care',
  'Missing forms' => 'Missing form',
  'Not Applicable' => 'Not Applicable',
  'Photo Nonconformance Image Quality' => 'Photo Nonconformance: Image Quality',
  'Photo Nonconformance Labeling Issue' => 'Photo Nonconformance: Labeling Issue',
  'Post op Form incorrect' => 'Post-op Form incorrect',
  'Pre op Form incorrect' => 'Pre-op Form incorrect',
  'Procedure Time Interval Missed' => 'Procedure Time Interval Missed',
  'Report Compilation Error' => 'Report Compilation Error',
  'Sample Prep Error Analytical' => 'Sample Prep Error: Analytical',
  'Sample Prep Error Extract Manipulation' => 'Sample Prep Error: Extract Manipulation',
  'Sample Prep Error Extraction Duration' => 'Sample Prep Error: Extraction Duration',
  'Sample Prep Error Extraction Ratio' => 'Sample Prep Error: Extraction Ratio',
  'Sample Prep Error Extraction Temperature' => 'Sample Prep Error: Extraction Temperature',
  'Sample Prep Error Loss of Vehicle Glassware' => 'Sample Prep Error: Loss of Vehicle (Glassware)',
  'Sample Prep Error Loss of Vehicle WhirlPak' => 'Sample Prep Error: Loss of Vehicle (WhirlPak)',
  'Sample Prep Error Study article became unsuitable for use' => 'Sample Prep Error: Study article(s) became unsuitable for use',
  'Sample Prep Error Wrong components' => 'Sample Prep Error: Wrong components',
  'Sample Prep Error Wrong storage' => 'Sample Prep Error: Wrong storage',
  'Sample Prep Error Wrong TACA concentration' => 'Sample Prep Error: Wrong TA/CA concentration',
  'Sample Prep Error Wrong vehicle' => 'Sample Prep Error: Wrong vehicle',
  'Sample Prep Near Miss Extraction' => 'Sample Prep Near Miss: Extraction',
  'Sample Prep Near Miss Prep' => 'Sample Prep Near Miss: Prep',
  'Scheduling Matrix not updated' => 'Scheduling Matrix not updated',
  'SPA Bug' => 'SPA Bug',
  'Specimen Collection form incorrect' => 'Specimen Collection form incorrect',
  'Standards Noncompliance' => 'Standard(s) Noncompliance',
  'Startups worksheet incorrect' => 'Startups worksheet incorrect',
  'Sterilization Failed Indicator' => 'Sterilization: Failed Indicator',
  'Storage Conditions chemicalreagent' => 'Storage Conditions: chemical/reagent',
  'Storage Conditions cleaning supply' => 'Storage Conditions: cleaning supply',
  'Storage Conditions specimen' => 'Storage Conditions: specimen',
  'Storage Conditions study article' => 'Storage Conditions: study article',
  'Surgical Site Concerns' => 'Surgical Site Concerns',
  'Task Incorrect Scheduling' => 'Task: Incorrect Scheduling',
  'Temp OOR not OOS' => 'Temp OOR, not OOS',
  'Training task performed with inadequate training' => 'Training: task performed with inadequate training',
  'Training task performed without training' => 'Training: task performed without training',
  'Unsatisfactory Study Article Receipt' => 'Unsatisfactory Study Article Receipt',
  'Water testing missed retest' => 'Water testing: missed retest',
  'WPO Not Submitted' => 'WPO Not Submitted',
  'Wrong Training assignment to associate' => 'Wrong Training assignment to associate',
);
?>
