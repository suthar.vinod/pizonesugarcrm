<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_projects_priority_options.php

 // created: 2016-01-06 02:33:50

$app_list_strings['projects_priority_options']=array (
  'high' => 'Wysoki',
  'medium' => 'Średni',
  'low' => 'Niski',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_workproductcategory.php

 // created: 2016-01-06 03:54:49

$app_list_strings['workproductcategory']=array (
  'Product' => 'Product',
  'Service' => 'Service',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_work_product_type_list.php

 // created: 2016-01-06 04:02:07

$app_list_strings['work_product_type_list']=array (
  'Sensitization' => 'Sensitization',
  'Irritation' => 'Irritation',
  'Cardiovascular' => 'Cardiovascular',
  'Genotoxicity' => 'Genotoxicity',
  'Select' => 'Select',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_task_name_list.php

 // created: 2016-01-06 23:05:26

$app_list_strings['task_name_list']=array (
  'blank' => 'Blank',
  'protocol' => 'Protocol',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_altered_bid_terms_list.php

 // created: 2016-01-07 14:56:24

$app_list_strings['altered_bid_terms_list']=array (
  'yes' => 'Yes',
  'no' => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_lead_source_dom.php

 // created: 2016-01-07 15:55:33

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Telemarketing',
  'Existing Customer' => 'Istniejący kontrahent',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Pracownik',
  'Conference' => 'Uczestnik konferencji',
  'Trade Show' => 'Uczestnik prezentacji',
  'Web Site' => 'Ze strony WWW',
  'Email' => 'E-mail',
  'Campaign' => 'Kampania',
  'Support Portal User Registration' => 'Rejestracja użytkownika portalu wsparcia',
  'Other' => 'Inne',
  'Referral' => 'Referral',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_job_function_list.php

 // created: 2016-01-07 17:50:41

$app_list_strings['job_function_list']=array (
  'blank' => 'Blank',
  'research and development' => 'R & D',
  'preclinical' => 'Preclinical',
  'consultant' => 'Consultant',
  'regulatory' => 'Regulatory',
  'clinical' => 'Clinical',
  'management' => 'Management',
  'executive management' => 'Executive Management',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_specialty_list.php

 // created: 2016-01-07 19:31:58

$app_list_strings['specialty_list']=array (
  'cardiovascular' => 'Cardiovascular',
  'neurovascular' => 'Neurovascular',
  'orthopaedic' => 'Orthopaedic',
  'blank' => 'Blank',
  'general pharmacology' => 'General Pharmacology',
  'oem' => 'OEM',
  'diabetes' => 'Diabetes',
  'urogenital' => 'Urogenital',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_sales_activity_stage_list.php

 // created: 2016-01-13 22:44:14

$app_list_strings['sales_activity_stage_list']=array (
  'lead' => 'Lead',
  'opportunity' => 'Opportunity',
  'select' => 'Select...',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_SalesActivityCategory.php

 // created: 2016-01-21 02:20:34

$app_list_strings['SalesActivityCategory']=array (
  'Lead' => 'Lead',
  'Select' => 'Select',
  'Opportunity' => 'Opportunity',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Sales_Management.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['m01_sales_activity_quote_document_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Sales'] = 'Sales';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_category_dom'][''] = '';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Active'] = 'Active';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Draft'] = 'Draft';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Expired'] = 'Expired';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Pending'] = 'Pending';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_sales_activity_quote_document_status_dom'][''] = '';
$app_list_strings['moduleList']['M01_Quote_Document'] = 'Quote Documents';
$app_list_strings['moduleList']['M01_Quote'] = 'Sales Activity Quotes';
$app_list_strings['moduleList']['M01_SA_Division_Department'] = 'SA_Division_Departments';
$app_list_strings['moduleList']['M01_Sales_Activity'] = 'Sales Activities';
$app_list_strings['moduleList']['M01_Sales_Activity_Quote'] = 'Sales Activity Quotes';
$app_list_strings['moduleList']['M01_Sales'] = 'Sales';
$app_list_strings['moduleList']['M01_Sale'] = 'Sales Activities';
$app_list_strings['moduleList']['M01_SA_Division_Department_'] = 'SA_Division_Departments';
$app_list_strings['moduleListSingular']['M01_Quote_Document'] = 'Quote Document';
$app_list_strings['moduleListSingular']['M01_Quote'] = 'Sales Activity Quote';
$app_list_strings['moduleListSingular']['M01_SA_Division_Department'] = 'SA_Division_Department';
$app_list_strings['moduleListSingular']['M01_Sales_Activity'] = 'Sales Activity';
$app_list_strings['moduleListSingular']['M01_Sales_Activity_Quote'] = 'Sales Activity Quote';
$app_list_strings['moduleListSingular']['M01_Sales'] = 'Sale';
$app_list_strings['moduleListSingular']['M01_Sale'] = 'Sales Activity';
$app_list_strings['moduleListSingular']['M01_SA_Division_Department_'] = 'SA_Division_Department';
$app_list_strings['m01_quote_document_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['m01_quote_document_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['m01_quote_document_category_dom']['Sales'] = 'Sales';
$app_list_strings['m01_quote_document_category_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_quote_document_category_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_quote_document_category_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_category_dom'][''] = '';
$app_list_strings['m01_quote_document_status_dom']['Active'] = 'Active';
$app_list_strings['m01_quote_document_status_dom']['Draft'] = 'Draft';
$app_list_strings['m01_quote_document_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_status_dom']['Expired'] = 'Expired';
$app_list_strings['m01_quote_document_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_quote_document_status_dom']['Pending'] = 'Pending';
$app_list_strings['m01_quote_document_status_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_quote_document_status_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_quote_document_status_dom'][''] = '';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Marketing'] = 'Marketing';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Sales'] = 'Sales';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom'][''] = '';
$app_list_strings['m01_quote_document_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_quote_document_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_quote_document_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_subcategory_dom']['Marketing'] = 'Marketing';
$app_list_strings['m01_quote_document_subcategory_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['m01_quote_document_subcategory_dom']['Sales'] = 'Sales';
$app_list_strings['m01_quote_document_subcategory_dom'][''] = '';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Active'] = 'Active';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Draft'] = 'Draft';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Expired'] = 'Expired';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Pending'] = 'Pending';
$app_list_strings['m01_quote_document_priority_dom']['Active'] = 'Active';
$app_list_strings['m01_quote_document_priority_dom']['Draft'] = 'Draft';
$app_list_strings['m01_quote_document_priority_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_priority_dom']['Expired'] = 'Expired';
$app_list_strings['m01_quote_document_priority_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_quote_document_priority_dom']['Pending'] = 'Pending';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Active'] = 'Active';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Draft'] = 'Draft';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Expired'] = 'Expired';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Pending'] = 'Pending';
$app_list_strings['m01_quote_document_resolution_dom']['Active'] = 'Active';
$app_list_strings['m01_quote_document_resolution_dom']['Draft'] = 'Draft';
$app_list_strings['m01_quote_document_resolution_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_resolution_dom']['Expired'] = 'Expired';
$app_list_strings['m01_quote_document_resolution_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_quote_document_resolution_dom']['Pending'] = 'Pending';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Active'] = 'Active';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Draft'] = 'Draft';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Expired'] = 'Expired';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Pending'] = 'Pending';
$app_list_strings['m01_quote_document_type_dom']['Active'] = 'Active';
$app_list_strings['m01_quote_document_type_dom']['Draft'] = 'Draft';
$app_list_strings['m01_quote_document_type_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_type_dom']['Expired'] = 'Expired';
$app_list_strings['m01_quote_document_type_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_quote_document_type_dom']['Pending'] = 'Pending';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_meeting_type_dom.php

 // created: 2016-01-26 22:16:42

$app_list_strings['meeting_type_dom']=array (
  'Other' => 'Inne',
  'Conference Call' => 'Conference Call',
  'In Person' => 'In Person',
  'Online' => 'Online',
  'Select One' => 'Select One...',
  'Tradeshow' => 'Tradeshow',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_document_template_type_dom.php

 // created: 2016-02-22 20:05:18

$app_list_strings['document_template_type_dom']=array (
  '' => '',
  'nda' => 'Umowa o poufności (NDA)',
  'purchase order' => 'Purchase Order (PO)',
  'signed quote' => 'Signed Quote',
  'study design document' => 'Study Design Docunment',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_document_status_dom.php

 // created: 2016-02-23 20:07:51

$app_list_strings['document_status_dom']=array (
  'Signed' => 'Signed',
  'Lost' => 'Lost',
  'Active' => 'Active',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_internal_department_list.php

 // created: 2016-02-29 18:14:18

$app_list_strings['internal_department_list']=array (
  'Pathology Services' => 'Pathology Services',
  'Analytical Services' => 'Analytical Services',
  'In_Life' => 'In-Life',
  'IVT' => 'IVT',
  'Scientific' => 'Scientific',
  'Choose One...' => 'Choose One...',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_pathologist_list.php

 // created: 2016-03-01 22:14:29

$app_list_strings['pathologist_list']=array (
  'lphillips' => 'lphillps',
  'ipolyakov' => 'ipolyakov',
  'ashucker' => 'ashucker',
  'None' => 'None',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_task_status_dom.php

 // created: 2016-03-04 18:06:00

$app_list_strings['task_status_dom']=array (
  'Completed' => 'Zakończone',
  'Not Completed' => 'Not Completed',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_regulatory.php

 // created: 2016-03-07 20:18:49

$app_list_strings['regulatory']=array (
  'ISO' => 'ISO',
  'USP' => 'USP',
  'JMHLW' => 'JMHLW',
  'None' => 'None',
  'Choose Regulatory Standard' => 'Choose Regulatory Standard',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_Division_list.php

 // created: 2016-03-07 20:24:49

$app_list_strings['Division_list']=array (
  'Corporate' => 'Corporate',
  'InLife_Services' => 'In-Life Services',
  'InVitro_Services' => 'In-Vitro Services',
  'Pathology_Services' => 'Pathology_Services',
  'Analytical_Services' => 'Analytical Services',
  'Regulatory_Services' => 'Regulatory Services',
  'Blank' => 'Blank',
  'Choose Division' => 'Choose Division',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_Department_list.php

 // created: 2016-03-07 20:26:18

$app_list_strings['Department_list']=array (
  'Biocompatibility' => 'Biocompatibility',
  'Interventional Surgical' => 'Interventional / Surgical',
  'Pharmacology' => 'Pharmacology',
  'Analytical' => 'Analytical',
  'Toxicology' => 'Toxicology',
  'Microbiology' => 'Microbiology',
  'Gross Pathology' => 'Gross Pathology',
  'Histology' => 'Histology',
  'Select' => 'Select',
  'Cytotoxicity' => 'Cytotoxicity',
  'Hemocompatibility' => 'Hemocompatibility',
  'Genotoxicity' => 'Genotoxicity',
  'Sensitization' => 'Sensitization',
  'Irritation' => 'Irritation',
  'Systemic Toxicity' => 'Systemic Toxicity',
  'Intramuscular' => 'Intramuscular',
  'Choose Department' => 'Choose Department',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_bc_study_outcome_type_list.php

 // created: 2016-03-08 15:54:45

$app_list_strings['bc_study_outcome_type_list']=array (
  'Completed Study' => 'Completed Study',
  'Passed Study' => 'Passed Study',
  'Failed Study' => 'Failed Study',
  'Expanded Study' => 'Expanded Study',
  'Aborted Study' => 'Aborted Study',
  'CAB Study' => 'CAB Study',
  'None' => 'None',
  'Choose SOT' => 'Choose SOT',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_sales_activity_status_list.php

 // created: 2016-03-28 21:13:49

$app_list_strings['sales_activity_status_list']=array (
  'open' => 'Open',
  'select' => 'Select...',
  'closed' => 'Closed',
  'won' => 'Won',
  'duplicate' => 'Duplicate',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_sales_activity_type_list.php

 // created: 2016-04-03 17:38:23

$app_list_strings['sales_activity_type_list']=array (
  'Within 1 month' => 'Within 1 month',
  'Within 1 to 3 months' => 'Within 1 to 3 months',
  'Within 3 to 6 months' => 'Within 3 to 6 months',
  'Within 6 to 12 months' => 'Within 6 to 12 months',
  'Greater than 12 months' => 'Greater than 12 months',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_study_compliance_list.php

 // created: 2016-04-04 22:50:59

$app_list_strings['study_compliance_list']=array (
  'Select One...' => 'Select One...',
  'non_GLP' => 'non-GLP',
  'GLP' => 'GLP',
  'ISO' => 'ISO',
  'USP' => 'USP',
  'JMHLW' => 'JMHLW',
  'Not Applicable' => 'Not Applicable',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_Yes_No.php

 // created: 2016-04-08 22:25:04

$app_list_strings['Yes_No']=array (
  'Yes' => 'Yes',
  'No' => 'No',
  'Choose one...' => 'Choose one...',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_department_manager_approval_list.php

 // created: 2016-04-11 21:58:12

$app_list_strings['department_manager_approval_list']=array (
  'Approved' => 'Approved',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_study.php

 // created: 2016-04-12 14:25:07

$app_list_strings['study']=array (
  'Choose One' => 'Choose One',
  'Discard' => 'Discard',
  'Return Unused' => 'Return Unused',
  'Return Used and Unused' => 'Return Used and Unused',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_invoicing.php

 // created: 2016-04-12 14:59:33

$app_list_strings['invoicing']=array (
  'Choose one...' => 'Choose one...',
  '40_50_10' => '40%/50%/10%',
  '50_50' => '50%/50%',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_data_storage_phase_list.php

 // created: 2016-04-18 16:41:36

$app_list_strings['data_storage_phase_list']=array (
  'Carousel' => 'Carousel',
  'Reporting' => 'Reporting',
  'Complete and In Review' => 'Complete and In Review',
  'Scanned' => 'Scanned',
  'Archived' => 'Archived',
  'Shipped or Returned' => 'Shipped or Returned',
  'Destroyed' => 'Destroyed',
  'Study Not Performed' => 'Study Not Performed',
  'Choose One' => 'Choose one',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_study_pathologist_list.php

 // created: 2016-04-19 16:28:30

$app_list_strings['study_pathologist_list']=array (
  'Choose Pathologist...' => 'Choose Pathologist...',
  'Igor Polyakov' => 'Igor Polyakov',
  'Lynette Phillips' => 'Lynette Phillips',
  'Adrienne Schucker' => 'Adrienne Schucker',
  'None' => 'None',
  'External Personnel' => 'External Personnel',
  'Muhammad Ahsan' => 'Muhammad Ahsan',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_expanded_study_result_list.php

 // created: 2016-04-20 13:31:05

$app_list_strings['expanded_study_result_list']=array (
  'Choose One' => 'Choose One',
  'Not Applicable' => 'Not Applicable',
  'Passed Study' => 'Passed Study',
  'Failed Study' => 'Failed Study',
  'Completed Study' => 'Completed Study',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_Med_Device_Category_and_Contact.php

 // created: 2016-04-26 15:20:29

$app_list_strings['Med_Device_Category_and_Contact']=array (
  'Surface device mucosal membrane' => 'Surface device - Mucosal Membrane',
  'Surface device breached or compromised surface' => 'Surface device - Breach/Compromised Surface',
  'Ext communicating device blood path indirect' => 'Ext communicating device - Blood path, indirect',
  'Ext communicating device tissue bone dentin' => 'Ext communicating device - Tissue/Bone/Dentin',
  'Ext communicating device circulating blood' => 'Ext communicating device - Circulating Blood',
  'Implant device tissue bone' => 'Implant device - Tissue/bone',
  'Implant device blood' => 'Implant device - Blood',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_patient_contact_duration_list.php

 // created: 2016-04-26 15:59:12

$app_list_strings['patient_contact_duration_list']=array (
  'Limited' => 'Limited (24 hours or less)',
  'Prolonged' => 'Prolonged (24 hours - 30 days)',
  'Permanent' => 'Permanent (greater than 30 days)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_followup_status_list.php

 // created: 2016-05-21 02:31:56

$app_list_strings['followup_status_list']=array (
  'active' => 'Active',
  'inactive' => 'Inactive',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_quote_status_list.php

 // created: 2016-08-09 16:11:39

$app_list_strings['quote_status_list']=array (
  'Choose_one' => 'Choose One...',
  'inactive' => 'Inactive',
  'active' => 'Active',
  'change_order' => 'Change Order',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_animal_model_list.php

 // created: 2016-08-19 22:00:13

$app_list_strings['animal_model_list']=array (
  'Choose one...' => 'Choose one...',
  'naive_porcine' => 'Naive Porcine',
  'naive_canine' => 'Naive Canine',
  'naive_ovine' => 'Naive Ovine',
  'naive_rabbit' => 'Naive Lagamorph',
  'myocaridal_infarction' => 'Myocardial Infarction',
  'rabbit_elastase_aneurysm' => 'Rabbit Elastase Aneurysm ',
  'canine_aneurysm' => 'Canine Aneurysm',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_device_type_list.php

 // created: 2016-08-19 22:09:41

$app_list_strings['device_type_list']=array (
  'Choose one...' => 'Choose one...',
  'ep_catheter' => 'EP Catheter',
  'coronary_stent' => 'Coronary Stent',
  'peripheral_stent' => 'Peripheral Stent',
  'aneurysm_filler' => 'Aneurysm Filler/Diverter',
  'heart_valve' => 'Heart Valve',
  'intravascular_catheter' => 'Intravascular Catheter',
  'neuromodulation' => 'Neuromodulation',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_bc_study_article_received_list.php

 // created: 2016-10-31 13:34:42

$app_list_strings['bc_study_article_received_list']=array (
  'Estimated Receipt' => 'Estimated Receipt',
  'Partially Received' => 'Partially Received',
  'Received' => 'Received',
  'Choose One' => 'Choose One',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_deliverable_owner_list.php

 // created: 2016-11-21 20:37:17

$app_list_strings['deliverable_owner_list']=array (
  'ablakstvedt' => 'ablakstvedt',
  'sbronstad' => 'sbronstad',
  'ebauer' => 'ebauer',
  'showard' => 'showard',
  'kcatalano' => 'kcatalano',
  'Add manager of deliverable' => 'Add manager of deliverable',
  'None' => 'None',
  'mheying' => 'mheying',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_pathologist_workload_list.php

 // created: 2016-12-01 14:17:11

$app_list_strings['pathologist_workload_list']=array (
  'Choose One' => 'Choose One',
  'Low' => 'Low',
  'Medium' => 'Medium',
  'High' => 'High',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_quote_review_priority_list.php

 // created: 2016-12-27 15:23:00

$app_list_strings['quote_review_priority_list']=array (
  'choose_one' => 'Choose one...',
  'low' => 'Low',
  'medium' => 'Medium',
  'high' => 'High',
  'critical' => 'Critical',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_signed_quote_c_list.php

 // created: 2017-01-23 16:20:16

$app_list_strings['signed_quote_c_list']=array (
  'Yes' => 'Yes',
  'No' => 'No',
  'Not Provided' => 'Not Provided',
  'Pricelist' => 'Pricelist',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_estimated_hours_list.php

 // created: 2017-02-09 22:23:14

$app_list_strings['estimated_hours_list']=array (
  '0.5' => '0.5',
  1 => '1',
  '1.5' => '1.5',
  2 => '2',
  '2.5' => '2.5',
  3 => '3',
  '3.5' => '3.5',
  4 => '4',
  '4.5' => '4.5',
  5 => '5',
  '5.5' => '5.5',
  6 => '6',
  '6.5' => '6.5',
  7 => '7',
  '7.5' => '7.5',
  8 => '8',
  '8.5' => '8.5',
  9 => '9',
  '9.5' => '9.5',
  10 => '10',
  '10.5' => '10.5',
  11 => '11',
  '11.5' => '11.5',
  12 => '12',
  '12.5' => '12.5',
  13 => '13',
  '13.5' => '13.5',
  14 => '14',
  '14.5' => '14.5',
  15 => '15',
  '15.5' => '15.5',
  16 => '16',
  '16.5' => '16.5',
  17 => '17',
  '17.5' => '17.5',
  18 => '18',
  '18.5' => '18.5',
  19 => '19',
  '19.5' => '19.5',
  20 => '20',
  'Choose One' => 'Choose One',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_primary_aps_operator_list.php

 // created: 2017-03-14 15:19:19

$app_list_strings['primary_aps_operator_list']=array (
  'Michael Jorgenson' => 'Michael Jorgenson',
  'Allan Camrud' => 'Allan Camrud',
  'Mark Beckel' => 'Mark Beckel',
  'Tyler LaMont' => 'Tyler LaMont',
  'Joseph Vislisel' => 'Joseph Vislisel',
  'Christina Gross' => 'Christina Gross',
  'Elizabeth Carter' => 'Elizabeth Carter',
  'Chris Lafean' => 'Chris Lafean',
  'Choose One' => 'Choose One',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_product_type_list.php

 // created: 2017-03-15 19:49:44

$app_list_strings['product_type_list']=array (
  'Unknown' => 'Unknown',
  'Choose one...' => 'Choose One...',
  'Solid' => 'Solid',
  'Liquid' => 'Liquid',
  'Gel' => 'Gel',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_Product_Contact_Category.php

 // created: 2017-03-15 19:51:20

$app_list_strings['Product_Contact_Category']=array (
  'Surface Contacting' => 'Surface Contacting',
  'External Communicating' => 'External Communicating',
  'Implant' => 'Implant',
  'Unknown' => 'Unknown',
  'Choose one...' => 'Choose One...',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_contact_duration_list.php

 // created: 2017-03-15 20:10:34

$app_list_strings['contact_duration_list']=array (
  'Limited' => 'Limited',
  'Prolonged_30' => 'Prolonged, ',
  'Permanent' => 'Permanent, >30 days',
  'Unknown' => 'Unknown',
  'Choose one...' => 'Choose One...',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_contact_type_list.php

 // created: 2017-03-15 20:32:29

$app_list_strings['contact_type_list']=array (
  'Unknown' => 'Unknown',
  'Choose one...' => 'Choose One...',
  'Skin_Limited' => 'Skin - Limited',
  'Skin_Prolonged' => 'Skin - Prolonged',
  'Skin_Permanent' => 'Skin - Permanent',
  'Mucosal Membrane_Limited' => 'Mucosal Membrane - Limited',
  'Mucosal Membrane_Prolonged' => 'Mucosal Membrane - Prolonged',
  'Mucosal Membrane_Permanent' => 'Mucosal Membrane - Permanent',
  'Compromised Surface_Limited' => 'Compromised Surface - Limited',
  'Compromised Surface_Prolonged' => 'Compromised Surface - Prolonged',
  'Compromised Surface_Permanent' => 'Compromised Surface - Permanent',
  'Blood Path_Indirect_Limited' => 'Blood Path, Indirect - Limited',
  'Blood Path_Indirect_Prolonged' => 'Blood Path, Indirect - Prolonged',
  'Blood Path_Indirect_Permanent' => 'Blood Path, Indirect - Permanent',
  'Tissue_Bone_Dentin_Limited' => 'Tissue/Bone/Dentin - Limited',
  'Tissue_Bone_Dentin_Prolonged' => 'Tissue/Bone/Dentin - Prolonged',
  'Tissue_Bone_Dentin_Permanent' => 'Tissue/Bone/Dentin - Permanent',
  'Circulating Blood_Limited' => 'Circulating Blood - Limited',
  'Circulating Blood_Prolonged' => 'Circulating Blood - Prolonged',
  'Circulating Blood_Permanent' => 'Circulating Blood - Permanent',
  'Tissue_Bone_Limited' => 'Tissue/Bone - Limited',
  'Tissue_Bone_Prolonged' => 'Tissue/Bone - Prolonged',
  'Tissue_Bone_Permanent' => 'Tissue/Bone - Permanent',
  'Blood_Limited' => 'Blood - Limited',
  'Blood_Prolonged' => 'Blood - Prolonged',
  'Blood_Permanent' => 'Blood - Permanent',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_Cytotoxicity_Testing.php

 // created: 2017-03-15 20:57:37

$app_list_strings['Cytotoxicity_Testing']=array (
  'MEM Elution' => 'MEM Elution - CY40',
  'MEM Serial Dilution' => 'MEM Serial Dilution - CY41',
  'Liquid Cytotoxicity' => 'Liquid Cytotoxicity - CY10',
  'Bacterial Endotoxin' => 'Bacterial Endotoxin - CY50',
  'Bacterial Endotoxin_Lot Release' => 'Bacterial Endotoxin, Lot Release - CY51',
  'Agar Overlay' => 'Agar Overlay, Solid - CY01',
  'Agar Overlay_Liquid' => 'Agar Overlay, Liquid - CY02',
  'Direct Contact' => 'Direct Contact - CY20',
  'Neutral Red Uptake' => 'Neutral Red Uptake - CY30',
  'MTT' => 'MTT - CY70',
  'Colony Formation' => 'Colony Formation - CY60',
  'Choose one...' => 'Choose One...',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_activity_list.php

 // created: 2017-03-20 21:10:56

$app_list_strings['activity_list']=array (
  'Choose One' => 'Choose One',
  'Procedure Oversight' => 'Operations Oversight or Conduct',
  'Reporting and Auditing' => 'Reporting or Report Auditing',
  'Data Entry and Analysis' => 'Data Entry & Analysis',
  'Protocol Dev or Auditing' => 'Protocol Development or Auditing',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_invoice_installment_percent_list.php

 // created: 2017-04-13 19:17:44

$app_list_strings['invoice_installment_percent_list']=array (
  'Choose One' => 'Choose One',
  'None' => 'None',
  25 => '25',
  50 => '50',
  100 => '100',
  '1x Monthly' => '1x Monthly',
  40 => '40%',
  60 => '60%',
  10 => '10%',
  75 => '75%',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_business_develop_activity_list.php

 // created: 2017-05-02 16:30:15

$app_list_strings['business_develop_activity_list']=array (
  '' => '',
  'Conference Call' => 'Conference Call',
  'Client_Visit_Onsite' => 'Onsite Client Visit',
  'Client_Visit_Offsite' => 'Offsite Client Visit',
  'Tradeshow' => 'Tradeshow',
  'Procedure Coverage' => 'Procedure Coverage',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_quality_assurance_activity_list.php

 // created: 2017-05-02 17:02:40

$app_list_strings['quality_assurance_activity_list']=array (
  '' => '',
  'Protocol Audit' => 'Protocol Audit',
  'Implant Procedure Audit' => 'Implant Procedure Audit',
  'Follow_Up Procedure Audit' => 'Follow-Up Procedure Audit',
  'Term Procedure Audit' => 'Term Procedure Audit',
  'Data Audit' => 'Data Audit',
  'Report Audit' => 'Report Audit',
  'Audit Findings Reporting' => 'Audit Findings Reporting',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_regulatory_activity_list.php

 // created: 2017-05-02 17:25:06

$app_list_strings['regulatory_activity_list']=array (
  '' => '',
  'Regulatory Consulting' => 'Regulatory Consulting',
  'FDA Response Writing' => 'FDA Response Writing',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Activity_Notes.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['an01_activity_notes_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['an01_activity_notes_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['an01_activity_notes_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['an01_activity_notes_category_dom'][''] = '';
$app_list_strings['an01_activity_notes_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['an01_activity_notes_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['an01_activity_notes_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['an01_activity_notes_subcategory_dom'][''] = '';
$app_list_strings['an01_activity_notes_status_dom']['Active'] = 'Aktywny';
$app_list_strings['an01_activity_notes_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['an01_activity_notes_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['an01_activity_notes_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['an01_activity_notes_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['an01_activity_notes_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['AN01_Activity_Notes'] = 'Activity Notes';
$app_list_strings['moduleListSingular']['AN01_Activity_Notes'] = 'Activity Note';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_activity_personnel_list.php

 // created: 2017-05-12 19:42:45

$app_list_strings['activity_personnel_list']=array (
  '' => '',
  'Business Development' => 'Business Development',
  'Scientific' => 'Scientific',
  'Quality Assurance' => 'Quality Assurance',
  'Pathology' => 'Pathology',
  'Analytical' => 'Analytical',
  'Regulatory' => 'Regulatory',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_new_deliverable_c_list.php

 // created: 2017-06-09 21:01:42

$app_list_strings['new_deliverable_c_list']=array (
  '' => '',
  'Analytical Data Tables' => 'Analytical Data Tables',
  'Analytical Report' => 'Analytical Report',
  'Analytical Validation Report' => 'Analytical Validation Report',
  'Animal Health_Clin Path Report' => 'Animal Health Report',
  'Final Report' => 'Final Report',
  'Gross Pathology Report' => 'Gross Pathology Report',
  'Histopathology Report' => 'Histopathology Report',
  'In_Life Report' => 'In-Life Report',
  'Interim Analytical Report' => 'Interim Analytical Report',
  'Interim Animal Health Report' => 'Interim Animal Health Report',
  'Interim Gross Path Report' => 'Interim Gross Path Report',
  'Interim Histopathology Report' => 'Interim Histopathology Report',
  'Interim In_Life Report' => 'Interim In-Life Report',
  'Pharmacology Data Tables' => 'Pharmacology Data Tables',
  'Pharmacology Report' => 'Pharmacology Report',
  'SEM Report' => 'SEM Report',
  'Slide Shipping' => 'Slide Shipping',
  'Tissue Shipping' => 'Tissue Shipping',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_pathology_activity_list.php

 // created: 2017-06-09 21:10:54

$app_list_strings['pathology_activity_list']=array (
  '' => '',
  'Necropsy' => 'Necropsy',
  'Tissue Trimming' => 'Tissue Trimming',
  'SEM Imaging' => 'SEM Imaging',
  'Histomorphometry' => 'Histomorphometry',
  'Slide Review' => 'Slide Review',
  'Gross Necropsy Report Writing' => 'Gross Necropsy Report Writing',
  'Pathology Report Writing' => 'Pathology Report Writing',
  'Faxitron' => 'Faxitron',
  'Tissue Receipt' => 'Tissue Receipt',
  'Tissue Shipping' => 'Tissue Shipping',
  'Paraffin Embedding' => 'Paraffin Embedding',
  'Plastic Embedding' => 'Plastic Embedding',
  'Slide Completion' => 'Slide Completion',
  'Slide Shipping' => 'Slide Shipping',
  'Out of Office' => 'Out of Office',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_reminder_time_options.php

 // created: 2017-06-16 19:19:42

$app_list_strings['reminder_time_options']=array (
  -1 => 'Brak',
  60 => '1 minutę wcześniej',
  300 => '5 minut wcześniej',
  600 => '10 minut wcześniej',
  900 => '15 minut wcześniej',
  1800 => '30 minut wcześniej',
  3600 => '1 godzinę wcześniej',
  7200 => '2 godziny wcześniej',
  10800 => '3 godziny wcześniej',
  18000 => '5 godzin wcześniej',
  86400 => '1 dzień wcześniej',
  604800 => '7 days prior',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_analytical_equipment_list_list.php

 // created: 2017-06-16 21:18:06

$app_list_strings['analytical_equipment_list_list']=array (
  '' => '',
  'ISQ LT' => 'GCMS – ISQ LT',
  'TSQ Quantiva 1' => 'LCMS – TSQ Quantiva 1',
  'TSQ Quantiva 2' => 'LCMS – TSQ Quantiva 2',
  'QE Focus' => 'LCMS – QE Focus',
  'Nicolet iS10' => 'FTIR – Nicolet iS10',
  'ASE350' => 'ASE – ASE350',
  'Agilent 7500 CS' => 'ICPMS – Agilent 7500 CS',
  'Precellys Evolution' => 'Homogenizer – Precellys Evolution',
  'Cryolys' => 'Homogenizer Chiller – Cryolys',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.HR_Management.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['hr01_resumes_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['hr01_resumes_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['hr01_resumes_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['hr01_resumes_category_dom'][''] = '';
$app_list_strings['hr01_resumes_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['hr01_resumes_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['hr01_resumes_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['hr01_resumes_subcategory_dom'][''] = '';
$app_list_strings['hr01_resumes_status_dom']['Active'] = 'Aktywny';
$app_list_strings['hr01_resumes_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['hr01_resumes_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['hr01_resumes_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['hr01_resumes_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['hr01_resumes_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['HR01_Resumes'] = 'Resumes';
$app_list_strings['moduleListSingular']['HR01_Resumes'] = 'Resume';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_job_category_list.php

 // created: 2017-07-20 16:44:54

$app_list_strings['job_category_list']=array (
  'Analytical' => 'Analytical',
  'Animal Care' => 'Animal Care',
  'Business Development' => 'Business Development',
  'Histology' => 'Histology',
  'IVT' => 'IVT',
  'Other' => 'Other',
  'Pathologist' => 'Pathologist',
  'Pharmacology' => 'Pharmacology',
  'Quality Assurance' => 'Quality Assurance',
  'Regulatory Services' => 'Regulatory Services',
  'Research Tech_SA' => 'Research Tech - SA',
  'Research Tech_LA' => 'Research Tech - LA',
  'Scientist' => 'Scientist',
  'Surgical Tech' => 'Surgical Tech',
  'Toxicology' => 'Toxicology',
  'Veterinarian' => 'Veterinarian',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.AnimalsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.AnimalsModuleCustomizations.php
 
$app_list_strings['enrollment_status_list'] = array (
  'Choose One' => 'Choose One',
  'Assigned Backup' => 'Assigned Backup',
  'Assigned On Study' => 'Assigned On Study',
  'Non naive Stock' => 'Non-naive Stock',
  'Not Used' => 'Not Used',
  'On Study' => 'On Study',
  'On Study Backup' => 'On Study Backup',
  'On Study Transferred' => 'On Study Transferred',
  'Stock' => 'Stock',
  'Deceased' => 'Deceased',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sex_list'] = array (
  'Choose One' => 'Choose One',
  'Female' => 'Female',
  'Castrated Male' => 'Castrated Male',
  'Male' => 'Male',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['vaccine_a_list'] = array (
  'Choose One' => 'Choose One',
  'Mycoplasma' => 'Mycoplasma',
  'Lawsonia' => 'Lawsonia',
  'Rabies' => 'Rabies',
  'Bordetella' => 'Bordetella',
  'DHPP' => 'DHPP',
  'CDT' => 'CDT',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['room_list'] = array (
  'Choose One' => 'Choose One',
  89452 => '8945-2',
  89453 => '8945-3',
  89454 => '8945-4',
  89455 => '8945-5',
  89456 => '8945-6',
  89457 => '8945-7',
  89458 => '8945-8',
  89459 => '8945-9',
  894510 => '8945-10',
  894511 => '8945-11',
  894512 => '8945-12',
  894513 => '8945-13',
  894514 => '8945-14',
  894515 => '8945-15',
  894516 => '8945-16',
  894519 => '8945-19',
  894520 => '8945-20',
  894521 => '8945-21',
  894522 => '8945-22',
  894523 => '8945-23',
  894524 => '8945-24',
  894525 => '8945-25',
  894526 => '8945-26',
  894527 => '8945-27',
  894528 => '8945-28',
  894529 => '8945-29',
  894530 => '8945-30',
  894531 => '8945-31',
  894532 => '8945-32',
  894533 => '8945-33',
  894534 => '8945-34',
  78035 => '780-35',
  78036 => '780-36',
  78037 => '780-37',
  78038 => '780-38',
  78039 => '780-39',
  89601 => '8960-1',
  89602 => '8960-2',
  89603 => '8960-3',
  89604 => '8960-4',
  89605 => '8960-5',
  89606 => '8960-6',
  89607 => '8960-7',
  89608 => '8960-8',
  89609 => '8960-9',
  896010 => '8960-10',
  896011 => '8960-11',
  896012 => '8960-12',
  896013 => '8960-13',
  896014 => '8960-14',
  896015 => '8960-15',
  896016 => '8960-16',
  896017 => '8960-17',
  896018 => '8960-18',
  896019 => '8960-19',
  896020 => '8960-20',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['species_list'] = array (
  'Canine' => 'Canine',
  'Caprine' => 'Caprine',
  'Guinea Pig' => 'Guinea Pig',
  'Hamster' => 'Hamster',
  'Lagomorph' => 'Lagamorph',
  'Mouse' => 'Mouse',
  'Rat' => 'Rat',
  'Choose One' => 'Choose One',
  'Porcine' => 'Porcine',
  'Bovine' => 'Bovine',
  'Ovine' => 'Ovine',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['vendor_list'] = array (
  'Choose One' => 'Choose One',
  'APS' => 'APS',
  'Bakkom Rabbitry' => 'Bakkom Rabbitry',
  'Charles River' => 'Charles River',
  'Elm Hill' => 'Elm Hill',
  'Envigo' => 'Envigo',
  'Lonestar Laboratory Swine' => 'Lonestar Laboratory Swine',
  'Manthei Hogs' => 'Manthei Hogs',
  'Marshall BioResources' => 'Marshall BioResources',
  'Neaton Polupays' => 'Neaton Polupays',
  'Oak Hill Genetics' => 'Oak Hill Genetics',
  'Purdue University' => 'Purdue University',
  'Rigland Farms' => 'Rigland Farms',
  'S and S Farms' => 'S & S Farms',
  'Sinclair BioResources' => 'Sinclair BioResources',
  'Twin Valley Kennels' => 'Twin Valley Kennels',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['Breed_List'] = array (
  'Beagle' => 'Beagle',
  'CD1' => 'CD1',
  'CF1' => 'CF1',
  'Gottingen' => 'Gottingen',
  'Hartley' => 'Hartley',
  'Holstein' => 'Holstein',
  'Mongrel' => 'Mongrel',
  'ND4' => 'ND4',
  'New Zealand White' => 'New Zealand White',
  'Choose One' => 'Choose One',
  'Athymic Nude' => 'Athymic Nude',
  'Cross Breed' => 'Cross Breed',
  'Golden Syrian' => 'Golden Syrian',
  'Polypay' => 'Polypay',
  'Sinclair' => 'Sinclair',
  'Sprague Dawley' => 'Sprague Dawley',
  'Suffolk X' => 'Suffolk X',
  'Watanabe' => 'Watanabe',
  'Yorkshire X' => 'Yorkshire X',
  'Yucatan' => 'Yucatan',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['usda_category_list'] = array (
  'B' => 'B',
  'C' => 'C',
  'D' => 'D',
  'E' => 'E',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.WPEsModuleCustomizations.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.WPEsModuleCustomizations.php
 
$app_list_strings['usda_category_list'] = array (
  'B' => 'B',
  'C' => 'C',
  'D' => 'D',
  'E' => 'E',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['enrollment_status_list'] = array (
  'Choose One' => 'Choose One',
  'Assigned Backup' => 'Assigned Backup',
  'Assigned On Study' => 'Assigned On Study',
  'Non naive Stock' => 'Non-naive Stock',
  'Not Used' => 'Not Used',
  'On Study' => 'On Study',
  'On Study Backup' => 'On Study Backup',
  'On Study Transferred' => 'On Study Transferred',
  'Stock' => 'Stock',
  'Deceased' => 'Deceased',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.customa1a_critical_phase_inspectio_activities_1_calls.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.customa1a_critical_phase_inspectio_activities_1_calls.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$app_list_strings['record_type_display']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['parent_type_display']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['record_type_display_notes']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$app_list_strings['record_type_display']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['parent_type_display']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';
$app_list_strings['record_type_display_notes']['A1A_Critical_Phase_Inspectio'] = 'Critical Phase Inspections';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_sales_focus_list.php

 // created: 2018-01-17 22:50:38

$app_list_strings['sales_focus_list']=array (
  '' => '',
  'Full APS Program' => 'Full APS Program',
  'Biocompatibility' => 'Biocompatibility',
  'Analytical' => 'Analytical',
  'Biocompatibility_Analytical' => 'Biocompatibility/Analytical',
  'Bioskills' => 'Bioskills',
  'ISR' => 'ISR',
  'Pharmacology' => 'Pharmacology',
  'Regulatory' => 'Regulatory',
  'Toxicology' => 'Toxicology',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_primary_activity_list.php

 // created: 2018-01-18 19:41:57

$app_list_strings['primary_activity_list']=array (
  '' => '',
  'Attend' => 'Attend',
  'Exhibit' => 'Exhibit',
  'Presentation' => 'Presentation',
  'Poster Presentation' => 'Poster Presentation',
  'Sponsor' => 'Sponsor',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_visit_status_list.php

 // created: 2018-01-22 20:56:16

$app_list_strings['visit_status_list']=array (
  '' => '',
  'Signed In' => 'Signed In',
  'Signed Out' => 'Signed Out',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_error_occured_on_weekend_list.php

 // created: 2018-01-30 16:50:09

$app_list_strings['error_occured_on_weekend_list']=array (
  'No' => 'No',
  'Yes' => 'Yes',
  'Choose One' => 'Choose One',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_vaccine_a_list.php

 // created: 2018-02-20 19:21:53

$app_list_strings['vaccine_a_list']=array (
  'Choose One' => 'Choose One',
  'Mycoplasma' => 'Mycoplasma',
  'Rabies' => 'Rabies',
  'CDT' => 'CDT',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_vaccine_b_c_list.php

 // created: 2018-02-20 19:23:58

$app_list_strings['vaccine_b_c_list']=array (
  'Choose One' => 'Choose One',
  'DHPP' => 'DHPP',
  'Lawsonia' => 'Lawsonia',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_vaccine_c_list.php

 // created: 2018-02-20 19:25:22

$app_list_strings['vaccine_c_list']=array (
  'Choose One' => 'Choose One',
  'Bordetella' => 'Bordetella',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_deceased_list.php

 // created: 2018-04-12 13:31:21

$app_list_strings['deceased_list']=array (
  'No' => 'No',
  'Yes' => 'Yes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_reason_for_amendment_list.php

 // created: 2018-04-13 13:06:32

$app_list_strings['reason_for_amendment_list']=array (
  'Typographical Error' => 'Typographical Error',
  'Data Entry Error' => 'Data Entry Error',
  'Error in Final Report PDF' => 'Error in Final Report PDF',
  'Sponsor Request' => 'Sponsor Request',
  'Sponsor Request for Additional Analysis' => 'Sponsor Request for Additional Analysis',
  'Choose one' => 'Choose One',
  'Audit Finding' => 'Audit Finding',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_lead_quality_list.php

 // created: 2018-04-23 17:16:36

$app_list_strings['lead_quality_list']=array (
  '' => '',
  'Low' => 'Low',
  'Medium' => 'Medium',
  'High' => 'High',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_test_control_article_checkin_list.php

 // created: 2018-04-30 18:05:47

$app_list_strings['test_control_article_checkin_list']=array (
  'Not Completedq' => 'Not Completed',
  'Completed' => 'Completed',
  'Not Applicable' => 'Not Applicable',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_sales_activity_source_list.php

 // created: 2018-05-08 19:51:26

$app_list_strings['sales_activity_source_list']=array (
  'Current Sponsor' => 'Current Sponsor',
  'In Coming Cold Call' => 'In Coming Cold Call',
  'Website' => 'Website',
  'Advertisement' => 'Advertisement',
  'Trade Show' => 'Trade Show',
  'Cold Call by APS' => 'Cold Call by APS',
  '' => '',
  'Referral' => 'Referral',
  'Third Party Website' => 'Third Party Website',
  'Surpass' => 'Surpass',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_imaging_modality_list.php

 // created: 2018-05-21 16:38:20

$app_list_strings['imaging_modality_list']=array (
  '' => '',
  'Angiography' => 'Angiography',
  'CT' => 'CT',
  'Ex_Vivo' => 'Ex-Vivo',
  'IVUS' => 'IVUS',
  'MRI' => 'MRI',
  'OCT' => 'OCT',
  'Ultrasound' => 'Ultrasound',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_anatomical_location_list.php

 // created: 2018-05-21 18:42:26

$app_list_strings['anatomical_location_list']=array (
  '' => '',
  'Aorta_mid' => 'Aorta (Mid)',
  'Aorta_Arch' => 'Aorta (Arch)',
  'Aortic_Annulus' => 'Aortic Annulus',
  'Aortic_arch_length' => 'Aortic Arch Length',
  'C_C_max' => 'Mitral C-C',
  'LCX_Mean_Diameter' => 'LCX Mean Diameter',
  'LAD_Mean_Diameter' => 'LAD Mean Diameter',
  'RCA_Mean_Diameter' => 'RCA Mean Diameter',
  'LEF_Mean_Diameter' => 'LEF Mean Diameter',
  'REF_Mean_Diameter' => 'REF Mean Diameter',
  'LIF_Mean_Diameter' => 'LIF Mean Diameter',
  'RIF_Mean_Diameter' => 'RIF Mean Diameter',
  'LII_Mean_Diameter' => 'LII Mean Diameter',
  'RII_Mean_Diameter' => 'RII Mean Diameter',
  'LEI_Mean_Diameter' => 'LEI Mean Diameter',
  'REI_Mean_Diameter' => 'REI Mean Diameter',
  'Right_Renal_Mean Diameter' => 'Right Renal Mean Diameter',
  'Left_Renal_Mean_Diameter' => 'Left Renal Mean Diameter',
  'Mitral_AP' => 'Mitral AP',
  'Right_Carotid_Artery' => 'Right Carotid Mean Diameter',
  'Left_Carotid' => 'Left Carotid Mean Diameter',
  'Left_Axillary_Artery_Mean_Diameter' => 'Left Axillary Artery Mean Diameter',
  'Left_Costocervical_Artery_Mean_Diameter' => 'Left Costocervical Artery Mean Diameter',
  'Right_Costocervical_Artery_Mean_Diameter' => 'Right Costocervical Artery Mean Diameter',
  'Left_Internal_Mammary_Artery_Mean_Diameter' => 'Left Internal Mammary Artery Mean Diameter',
  'Right_Internal_Mammary_Artery_Mean_Diameter' => 'Right Internal Mammary Artery Mean Diameter',
  'Left_Internal_Thoracic_Artery_Mean_Diameter' => 'Left Internal Thoracic Artery Mean Diameter',
  'Right_Internal_Thoracic_Artery_Mean_Diameter' => 'Right Internal Thoracic Artery Mean Diameter',
  'Left_Vertebral_Artery_Mean_Diameter' => 'Left Vertebral Artery Mean Diameter',
  'Right_Verterbral_Artery_Mean_Diameter' => 'Right Verterbral Artery Mean Diameter',
  'Mesenteric_Artery_Mean_Diameter' => 'Mesenteric Artery Mean Diameter',
  'Right_Subclavian_Mean_Diameter' => 'Right Subclavian Mean Diameter',
  'LA Height' => 'LA Height',
  'Right_Femoral_Vein Diameter' => 'Right Femoral Vein Diameter',
  'Left_Femoral_Vein_Diameter' => 'Left Femoral Vein Diameter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_weight_range_list.php

 // created: 2018-05-21 19:52:20

$app_list_strings['weight_range_list']=array (
  '' => '',
  '0_10' => '0 - 10 kgs',
  '11_20' => '11 - 20 kgs',
  '20_30' => '20 - 30 kgs',
  '30_45' => '30 - 45 kgs',
  '45_60' => '45 - 60 kgs',
  '60_80' => '60 - 80 kgs',
  '80_100' => '80 - 100 kgs',
  '100_plus' => '100+ kgs',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_portal_account_activated_list.php

 // created: 2018-06-12 14:13:57

$app_list_strings['portal_account_activated_list']=array (
  'No' => 'No',
  'Yes' => 'Yes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_procedure_room_type_list.php

 // created: 2018-06-26 18:52:22

$app_list_strings['procedure_room_type_list']=array (
  '' => '',
  'Cath_Lab' => 'Cath Lab',
  'OR' => 'OR',
  'Necropsy' => 'Necropsy',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_required_equipment_list.php

 // created: 2018-06-26 19:01:12

$app_list_strings['required_equipment_list']=array (
  '' => '',
  'IVUS' => 'IVUS',
  'OCT' => 'OCT',
  'PowerLab' => 'PowerLab',
  'Endoscopy_Tower' => 'Endoscopy Tower',
  'Portable_CARM' => 'Portable C-Arm',
  'Ultrasound' => 'Ultrasound',
  'Velocity' => 'Velocity',
  'Bard' => 'Bard',
  'RF Generator' => 'RF Generator',
  'EP Combination' => 'EP Combination',
  'Cardiac CT' => 'Cardiac CT',
  'Other CT' => 'Other CT',
  'Hourly Holding' => 'Hourly Holding',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_regulatory_region_list.php

 // created: 2018-06-26 19:10:09

$app_list_strings['regulatory_region_list']=array (
  '' => '',
  'FDA' => 'FDA',
  'CE' => 'CE',
  'JMHLW' => 'JMHLW',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_required_reports_list.php

 // created: 2018-06-26 19:14:49

$app_list_strings['required_reports_list']=array (
  '' => '',
  'Animal Health' => 'Animal Health',
  'Gross Pathology' => 'Gross Pathology',
  'Gross and Histopathology' => 'Gross and Histopathology',
  'Analytical Report' => 'Analytical Report',
  'In_Life Report' => 'In-Life Report',
  'Final Report' => 'Final Report',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_cpi_finding_category_list.php

 // created: 2018-09-06 18:12:58

$app_list_strings['cpi_finding_category_list']=array (
  '' => '',
  'Protocol' => 'Protocol',
  'SOP' => 'SOP',
  'GLP' => 'GLP',
  'ISO' => 'ISO',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_timeline_type_list.php

 // created: 2018-09-13 19:49:35

$app_list_strings['timeline_type_list']=array (
  'Standard' => 'Standard',
  'Expedited' => 'Expedited',
  '' => '',
  'Priority Expedited' => 'Priority Expedited',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_chronic_study_list.php

 // created: 2018-09-14 13:29:18

$app_list_strings['chronic_study_list']=array (
  '' => '',
  'No' => 'No',
  'Yes' => 'Yes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_duration_greater_than_4_week_list.php

 // created: 2018-09-14 13:32:14

$app_list_strings['duration_greater_than_4_week_list']=array (
  '' => '',
  'No' => 'No',
  'Yes' => 'Yes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_view_all_company_projects_list.php

 // created: 2018-09-17 17:36:04

$app_list_strings['view_all_company_projects_list']=array (
  'Yes' => 'Yes',
  'No' => 'No',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_responsible_personnel_list.php

 // created: 2018-07-06 13:53:55

$app_list_strings['responsible_personnel_list']=array (
  'Entry' => 'Entry',
  'Review' => 'Review',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_is_manager_list.php

 // created: 2018-08-21 09:06:45

$app_list_strings['is_manager_list']=array (
  'yes' => 'Yes',
  'no' => 'No',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_deviation_type_list.php

 // created: 2018-07-30 20:28:30

$app_list_strings['deviation_type_list']=array (
  '' => '',
  'Entry' => 'Entry',
  'Review' => 'Review',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_company_division_list.php

 // created: 2018-09-24 19:06:02

$app_list_strings['company_division_list']=array (
  'Biocompatibility' => 'Biocompatibility',
  'ISR' => 'ISR',
  'Pharmacology' => 'Pharmacology',
  'Analytical Services' => 'Analytical Services',
  'Pathology Services' => 'Pathology Services',
  'Toxicology' => 'Toxicology',
  'Choose Functional Area' => 'Choose Functional Area',
  'Bioskills' => 'Bioskills',
  'Regulatory' => 'Regulatory',
  '' => '',
  'Standard Biocompatibility' => 'Standard Biocompatibility',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_employee_status_dom.php

 // created: 2018-09-25 12:50:07

$app_list_strings['employee_status_dom']=array (
  'Active' => 'Aktywny',
  'Terminated' => 'Zakończone',
  'Leave of Absence' => 'Nieobecny',
  'Non APS Study Director' => 'Non APS Study Director',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_functional_area_list.php

 // created: 2018-09-25 15:41:08

$app_list_strings['functional_area_list']=array (
  '' => '',
  'Analytical Services' => 'Analytical',
  'Animal Holding' => 'Animal Holding',
  'Consulting Services' => 'Consulting Services',
  'Cytotoxicity' => 'Cytotoxicity',
  'Genotoxicity' => 'Genotoxicity',
  'Hemocompatibility' => 'Hemocompatibility',
  'Implantation' => 'Implantation',
  'Interventional Surgical' => 'Interventional and Surgical',
  'Irritation' => 'Irritation',
  'Miscellaneous Testing' => 'Miscellaneous Testing',
  'Pathology' => 'Pathology',
  'Pharmacology' => 'Pharmacology',
  'Sensitization' => 'Sensitization',
  'Systemic Toxicity' => 'Systemic Toxicity',
  'Toxicity' => 'Toxicity',
  'Regulatory' => 'Regulatory',
  'Training' => 'Training',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_reason_for_lost_quote.php

 // created: 2018-10-23 19:11:44

$app_list_strings['reason_for_lost_quote']=array (
  'Choose One' => 'Choose One...',
  'lab proximity to company' => 'Lab Proximity to Company',
  'lower quote from competitive lab' => 'Lower Quote from Competitive Lab',
  'scheduling issue' => 'Scheduling Issue',
  'experience' => 'Staff Experience',
  'lack of required equipment' => 'Lack of Required Equipment',
  'report timelines' => 'Report Timelines',
  'capacity' => 'Capacity',
  'lack of timely response' => 'Lack of Timely Response',
  'no response from client' => 'No Response from Client',
  'Chose another lab' => 'Selected a Different Lab',
  'WL' => 'WL',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_departments_list.php

 // created: 2018-11-14 22:20:27

$app_list_strings['departments_list']=array (
  '' => '',
  'SA AC' => 'SA AC',
  'LA AC' => 'LA AC',
  'SP' => 'SP',
  'IVP' => 'IVP',
  'SA OP' => 'SA OP',
  'DVM' => 'DVM',
  'LA OP' => 'LA OP',
  'ISR' => 'ISR',
  'PS' => 'PS',
  'SCI' => 'SCI',
  'AS' => 'AS',
  'Facility' => 'Facility',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_instrument_list.php

 // created: 2018-11-15 14:38:44

$app_list_strings['instrument_list']=array (
  'LCMS' => 'LC/MS',
  'GCMS' => 'GC/MS',
  'ICPMS' => 'ICP-MS',
  'FTIR' => 'FTIR',
  '' => '',
  'LCMS GCMS ICPMS' => 'LC/MS, GC/MS, ICP/MS',
  'LCMS GCMS ICPMS FTIR' => 'LC/MS, GC/MS, ICP/MS, FTIR',
  'LCUV' => 'LC/UV',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_lead_auditor_list.php

 // created: 2018-11-20 15:07:49

$app_list_strings['lead_auditor_list']=array (
  'Choose Auditor' => 'Choose Auditor...',
  'Brenda Bailey' => 'Brenda Bailey',
  'Tammy Fossum' => 'Tammy Fossum',
  'Emily Markuson' => 'Emily Markuson',
  'Kristen Varas' => 'Kristen Varas',
  'None' => 'None',
  'BC Auditor Group' => 'BC Auditor Group',
  'Kevin Catalano' => 'Kevin Catalano',
  'Nicole Klee' => 'Nicole Klee',
  'External Contractor' => 'External Contractor',
  'Erica VanReeth' => 'Erica VanReeth',
  'Dani Zehowski' => 'Dani Zehowski',
  'Karen Bastyr' => 'Karen Bastyr',
  'Nick McCune' => 'Nick McCune',
  'Katie Jenkins' => 'Katie Jenkins',
  'Kris Ruppelius' => 'Kris Ruppelius',
  'Stephanie Beane' => 'Stephanie Beane',
  'Amy Malloy' => 'Amy Malloy',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_analytical_activity_list.php

 // created: 2018-11-21 17:55:35

$app_list_strings['analytical_activity_list']=array (
  '' => '',
  'Method Development' => 'Method Development',
  'Protocol Development' => 'Protocol Development',
  'Sample Prep' => 'Sample Prep',
  'Sample Analysis' => 'Sample Analysis',
  'Analytical Report Writing' => 'Analytical Report Writing',
  'Out of Office' => 'Out of Office',
  'Data Analysis' => 'Data Analysis',
  'Instrument Maintenance' => 'Instrument Maintenance',
  'Data Delivery To Toxicologist' => 'Data Delivery To Toxicologist',
  'Exhaustive exaggerated extraction start' => 'Exhaustive/Exaggerated Extraction Start',
  'Simulated Use Extraction Start' => 'Simulated Use Extraction Start',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_required.php

 // created: 2018-11-28 22:21:10

$app_list_strings['required']=array (
  '' => '',
  'H_E' => 'H&E',
  'Trichrome' => 'Trichrome',
  'Von Kossa' => 'Von Kossa',
  'Carstairs' => 'Carstairs',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_necropsy_type_list.php

 // created: 2018-11-28 22:43:26

$app_list_strings['necropsy_type_list']=array (
  '' => '',
  'Device_Target Tissue' => 'Device/Target Tissue',
  'Complete' => 'Complete',
  'Complete without Brain' => 'Complete w/o Brain',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_slide_size_type_list.php

 // created: 2018-11-29 17:34:29

$app_list_strings['slide_size_type_list']=array (
  '' => '',
  'Large' => 'Large',
  'Standard' => 'Standard',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_histopath_processing_type_list.php

 // created: 2018-11-29 17:41:50

$app_list_strings['histopath_processing_type_list']=array (
  '' => '',
  'Paraffin' => 'Paraffin',
  'Plastic' => 'Plastic',
  'Frozen' => 'Frozen',
  'SEM' => 'SEM',
  'Faxitron' => 'Faxitron',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.customm01_sales_activities_1_calls.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.customm01_sales_activities_1_calls.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$app_list_strings['record_type_display']['M01_Sales'] = 'Sales';
$app_list_strings['parent_type_display']['M01_Sales'] = 'Sales';
$app_list_strings['record_type_display_notes']['M01_Sales'] = 'Sales';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$app_list_strings['record_type_display']['M01_Sales'] = 'Sales';
$app_list_strings['parent_type_display']['M01_Sales'] = 'Sales';
$app_list_strings['record_type_display_notes']['M01_Sales'] = 'Sales';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_spa.php

 // created: 2019-01-24 20:04:35

$app_list_strings['spa']=array (
  'Sponsor Submitted' => 'Sponsor Submitted',
  'APS Reviewed' => 'APS Reviewed',
  'Sponsor Finalized' => 'Sponsor Finalized',
  'APS Approved' => 'APS Approved',
  'Choose One' => 'Choose One',
  'Waiting on Payment' => 'Waiting on Payment',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_vendor_list.php

 // created: 2019-01-25 15:32:23

$app_list_strings['vendor_list']=array (
  'Choose One' => 'Choose One',
  'APS' => 'APS',
  'Bakkom Rabbitry' => 'Bakkom Rabbitry',
  'Charles River' => 'Charles River',
  'Elm Hill' => 'Elm Hill',
  'Envigo' => 'Envigo',
  'Lonestar Laboratory Swine' => 'Lonestar Laboratory Swine',
  'Manthei Hogs' => 'Manthei Hogs',
  'Marshall BioResources' => 'Marshall BioResources',
  'Neaton Polupays' => 'Neaton Polupays',
  'Oak Hill Genetics' => 'Oak Hill Genetics',
  'Purdue University' => 'Purdue University',
  'Rigland Farms' => 'Rigland Farms',
  'S and S Farms' => 'S & S Farms',
  'Sinclair BioResources' => 'Sinclair BioResources',
  'Twin Valley Kennels' => 'Twin Valley Kennels',
  'Exemplar' => 'Exemplar',
  'Covance' => 'Covance',
  'Oakwood' => 'Oakwood',
  'Jackson Labs' => 'Jackson Labs',
  'Simonsen Laboratories' => 'Simonsen Laboratories',
  'Central Minnesota Livestock Sourcing' => 'Central Minnesota Livestock Sourcing',
  'Ovis' => 'Ovis',
  'Pozzi Ranch' => 'Pozzi Ranch',
  'Recombinetics' => 'Recombinetics',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_document_status_list.php

 // created: 2019-01-29 00:06:47

$app_list_strings['document_status_list']=array (
  'Active' => 'Active',
  'Obsolete' => 'Obsolete',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Company_Documents.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['cd_company_documents_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['cd_company_documents_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['cd_company_documents_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['cd_company_documents_category_dom'][''] = '';
$app_list_strings['cd_company_documents_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['cd_company_documents_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['cd_company_documents_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['cd_company_documents_subcategory_dom'][''] = '';
$app_list_strings['cd_company_documents_status_dom']['Active'] = 'Aktywny';
$app_list_strings['cd_company_documents_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['cd_company_documents_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['cd_company_documents_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['cd_company_documents_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['cd_company_documents_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['CD_Company_Documents'] = 'Company Documents';
$app_list_strings['moduleListSingular']['CD_Company_Documents'] = 'Company Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Tradeshow_Documents.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['td_tradeshow_documents_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['td_tradeshow_documents_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['td_tradeshow_documents_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['td_tradeshow_documents_category_dom'][''] = '';
$app_list_strings['td_tradeshow_documents_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['td_tradeshow_documents_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['td_tradeshow_documents_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['td_tradeshow_documents_subcategory_dom'][''] = '';
$app_list_strings['td_tradeshow_documents_status_dom']['Active'] = 'Aktywny';
$app_list_strings['td_tradeshow_documents_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['td_tradeshow_documents_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['td_tradeshow_documents_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['td_tradeshow_documents_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['td_tradeshow_documents_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['TD_Tradeshow_Documents'] = 'Tradeshow Documents';
$app_list_strings['moduleListSingular']['TD_Tradeshow_Documents'] = 'Tradeshow Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_meeting_status_dom.php

 // created: 2019-03-13 11:59:45

$app_list_strings['meeting_status_dom']=array (
  'Planned' => 'Zaplanowane',
  'Held' => 'Przeprowadzone',
  'Not Held' => 'Nieprzeprowadzone',
  'Completed' => 'Completed',
  'Obsolete' => 'Obsolete',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_document_type_list.php

 // created: 2019-04-05 11:15:00

$app_list_strings['document_type_list']=array (
  'Study Quote' => 'Study Quote',
  'Study Design Document' => 'Study Design Document/Synopsis',
  '' => '',
  'Quote Revision' => 'Quote Revision',
  'Signed Quote' => 'Signed Quote',
  'PO' => 'PO',
  'Change Order' => 'Change Order',
  'Signed Change Order' => 'Signed Change Order',
  'Signed Quote Revision' => 'Signed Quote Revision',
  'Signed SOW' => 'Signed SOW',
  'SOW' => 'SOW',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_study_coordinator_list.php

 // created: 2019-04-11 10:06:32

$app_list_strings['study_coordinator_list']=array (
  'Emily Visedo' => 'Emily Visedo',
  'DeeAnn Deiss' => 'DeeAnn Deiss',
  'Liz Roby' => 'Liz Roby',
  'Tom Van Valkenburg' => 'Tom Van Valkenburg',
  'Choose One' => 'Choose One',
  'Ben Vos' => 'Ben Vos',
  'Emma Squires Sperling' => 'Emma Squires-Sperling',
  'Anton Crane' => 'Anton Crane',
  'Liz Clark' => 'Liz Clark',
  'Amy Puetz' => 'Amy Puetz',
  'Erica VanReeth' => 'Erica VanReeth',
  'Dan Kroeninger' => 'Dan Kroeninger',
  'Kerry Trotter' => 'Kerry Trotter',
  'Hannah Schmidt' => 'Hannah Schmidt',
  'Steven Mamer' => 'Steven Mamer',
  'Emma Reiss' => 'Emma Reiss',
  'Kailyn Singh' => 'Kailyn Singh',
  'Katelyn Mortensen' => 'Katelyn Mortensen',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_room_list.php

 // created: 2019-04-11 10:15:10

$app_list_strings['room_list']=array (
  'Choose One' => 'Choose One',
  89452 => '8945-2',
  89453 => '8945-3',
  89454 => '8945-4',
  89455 => '8945-5',
  89456 => '8945-6',
  89457 => '8945-7',
  89458 => '8945-8',
  89459 => '8945-9',
  894510 => '8945-10',
  894511 => '8945-11',
  894512 => '8945-12',
  894513 => '8945-13',
  894514 => '8945-14',
  894515 => '8945-15',
  894516 => '8945-16',
  894519 => '8945-19',
  894520 => '8945-20',
  894521 => '8945-21',
  894522 => '8945-22',
  894523 => '8945-23',
  894524 => '8945-24',
  894525 => '8945-25',
  894526 => '8945-26',
  894527 => '8945-27',
  894528 => '8945-28',
  894529 => '8945-29',
  894530 => '8945-30',
  894531 => '8945-31',
  894532 => '8945-32',
  894533 => '8945-33',
  894534 => '8945-34',
  89601 => '8960-1',
  89602 => '8960-2',
  89603 => '8960-3',
  89604 => '8960-4',
  89605 => '8960-5',
  89606 => '8960-6',
  89607 => '8960-7',
  89608 => '8960-8',
  89609 => '8960-9',
  896010 => '8960-10',
  896011 => '8960-11',
  896012 => '8960-12',
  896013 => '8960-13',
  896014 => '8960-14',
  896015 => '8960-15',
  896016 => '8960-16',
  896017 => '8960-17',
  896018 => '8960-18',
  896019 => '8960-19',
  896020 => '8960-20',
  894535 => '8945-35',
  894536 => '8945-36',
  7801 => '780-1',
  7802 => '780-2',
  7803 => '780-3',
  7804 => '780-4',
  7805 => '780-5',
  894537 => '8945-37',
  894538 => '8945-38',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_archiving_type_of_contact_list.php

 // created: 2019-04-15 12:14:14

$app_list_strings['archiving_type_of_contact_list']=array (
  '' => '',
  '1st Contact Email' => '1st Contact - Email',
  '1st Contact Phone' => '1st Contact - Phone',
  '2nd Contact Email' => '2nd Contact - Email',
  '2nd Contact Phone' => '2nd Contact - Phone',
  '3rd Contact Email' => '3rd Contact - Email',
  '3rd Contact Phone' => '3rd Contact - Phone',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_work_product_compliance_list.php

 // created: 2019-04-15 12:44:55

$app_list_strings['work_product_compliance_list']=array (
  'nonGLP' => 'nonGLP',
  'GLP' => 'GLP',
  'GLP_Amended_NonGLP' => 'GLP, Amended nonGLP',
  'Choose Compliance' => 'Choose Compliance',
  'Not Applicable' => 'Not Applicable',
  'Withdrawn' => 'GLP, Withdrawn',
  'GLP Not Performed' => 'GLP, Not Performed',
  'GLP Discontinued' => 'GLP, Discontinued',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_department_id_list.php

 // created: 2019-04-24 11:27:38

$app_list_strings['department_id_list']=array (
  'SA AC' => 'SA AC',
  'LA AC' => 'LA AC',
  'SP' => 'SP',
  'IVT' => 'IVT',
  'SA OP' => 'SA OP',
  'DVM' => 'DVM',
  'LA OP' => 'LA OP',
  'ISR' => 'ISR',
  'PS' => 'PS',
  'SCI' => 'SCI',
  'AS' => 'AS',
  'Facility' => 'Facility',
  '' => '',
  'PH OP' => 'PH OP',
  'HS' => 'HS',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_veterinarian_list.php

 // created: 2019-05-02 12:18:42

$app_list_strings['veterinarian_list']=array (
  'ebauer' => 'ebauer',
  'jvislisel' => 'jvislisel',
  'None' => 'none',
  'Choose Veterinarian' => 'Choose Veterinarian',
  'apinto' => 'Alejandra Pinto',
  'cnorman' => 'Claire Norman',
  'lszenay' => 'Lesley Szenay',
  'tpavek' => 'Todd Pavek',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_study_director_list.php

 // created: 2019-05-07 19:59:42

$app_list_strings['study_director_list']=array (
  'Laura Christoferson' => 'Laura Christoferson (LC)',
  'Cheryl Marker' => 'Cheryl Marker (CM)',
  'Heather Ackerson' => 'Heather Ackerson (HA)',
  'Scott Barnhill' => 'Scott Barnhill (SHB)',
  'Carlus Dingfelder' => 'Carlus Dingfelder (CD)',
  'Christina Gross' => 'Christina Gross (CG)',
  'Kristen Nelson' => 'Kristen Nelson (KN)',
  'Gabe Quinn' => 'Gabe Quinn (GQ)',
  'Cheng Yang ' => 'Cheng Yang (CY)',
  'Liisa Carter' => 'Liisa Carter (EC)',
  'Jesse Heitke' => 'Jesse Heitke (JH)',
  'Chris Lafean' => 'Chris Lafean (CL)',
  'Emily Tobin' => 'Emily Tobin (ET)',
  'Taylor Celski' => 'Taylor Celski (TC)',
  'Lauren Frederick' => 'Lauren Frederick (LF)',
  'Sarah Kaltenbach' => 'Sarah Kaltenbach (SK)',
  'Thomas Van Valkenburg' => 'Thomas Van Valkenburg (TV)',
  'Steve Deline' => 'Steve Deline (SD)',
  'Melissa Carlson' => 'Melissa Carlson',
  'Choose APS PI' => 'Choose APS PI',
  'Other' => 'Other',
  'Tim Schatz' => 'Tim Schatz',
  'Joe Vislisel' => 'Joe Vislisel',
  'Patrick Stocker' => 'Patrick Stocker',
  'Marie Blommel' => 'Marie Blommel',
  'Erik Steinmetz' => 'Erik Steinmetz',
  'Jason Thorsten' => 'Jason Thorsten',
  'Vanessa Lopes Berkas' => 'Vanessa Lopes-Berkas',
  'Kent Grove' => 'Kent Grove',
  'Adam Blakstvedt' => 'Adam Blakstvedt',
  'Yan Chen' => 'Yan Chen',
  'Sarah Howard' => 'Sarah Howard',
  'Michael Frie' => 'Michael Frie',
  'Mark Smith' => 'Mark Smith',
  'Mark Peterson' => 'Mark Peterson',
  'Bich Nguyen' => 'Bich Nguyen',
  'Sarah Bronstad' => 'Sarah Bronstad',
  'Jim Pomonis' => 'Jim Pomonis',
  'Matt Cunningham' => 'Matt Cunningham',
  'Abby Beltrame' => 'Abby Beltrame',
  'Jeff Benham' => 'Jeff Benham',
  'Qing Wang' => 'Qing Wang',
  'Jennifer Lane' => 'Jennifer Lane',
  'LaDonna Camrud' => 'LaDonna Camrud',
  'Jennifer Alkire' => 'Jennifer Alkire',
  'Chris Perry' => 'Chris Perry',
  'Liane Teplitsky' => 'Liane Teplitsky',
  'Haris Sih' => 'Haris Sih',
  'Tess Gilbert' => 'Tess Gilbert',
  'Rebekka Molenaar' => 'Rebekka Molenaar',
  'Thomas van Valkenburg' => 'Thomas van Valkenburg',
  'Eric_Dobrave' => 'Eric Dobrava',
  'Corey Leet' => 'Corey Leet',
  'Liz ROby' => 'Liz Roby',
  'DeeAnn Deiss' => 'DeeAnn Deiss',
  'Marla Emery' => 'Marla Emery',
  'Megan OBrien' => 'Megan O\'Brien',
  'Muhammad Ashan' => 'Muhammad Ashan',
  'Adrienne Schucker' => 'Adrienne Schucker',
  'Lynette Phillips' => 'Lynette Phillips',
  'Igor Polyakov' => 'Igor Polyakov',
  'Liz Clark' => 'Liz Clark',
  'Sarah Schaefers' => 'Sarah Schaefers',
  'Marlene Heying' => 'Marlene Heying',
  'Kim Castle' => 'Kim Castle',
  'Jon Raybuck' => 'Jon Raybuck',
  'Leigh Kleinert' => 'Leigh Kleinert',
  'Ben Vos' => 'Ben Vos',
  'Amapola Balancio' => 'Amapola Balancio',
  'Emma Squires Sperling' => 'Emma Squires-Sperling',
  'Elizabeth Kurpiers' => 'Elizabeth Kurpiers',
  'Nicole Son' => 'Nicole Son',
  'Wendy Mathews' => 'Wendy Mathews',
  '' => '',
  'Kristin Booth' => 'Kristin Booth',
  'Catherine Pipenhagen' => 'Catherine Pipenhagen',
  'Lisa Clausen' => 'Lisa Clausen',
  'Athanasios Peppas' => 'Athanasios Peppas',
  'Dongmei May Yan' => 'Dongmei (May) Yan',
  'Dalia Shabashov Stone' => 'Dalia Shabashov Stone',
  'Rhonda Albright' => 'Rhonda Albright',
  'Heidi Dierks' => 'Heidi Dierks',
  'Kim Berg' => 'Kim Berg',
  'Mindy Jedlicki' => 'Mindy Jedlicki',
  'Emily Visedo' => 'Emily Visedo',
  'Libette Rowan' => 'Libette Rowan',
  'Toby Rogers' => 'Toby Rogers',
  'Erica Van Reeth' => 'Erica Van Reeth',
  'Brian Alzua' => 'Brian Alzua',
  'Ian Dean' => 'Ian Dean',
  'Amy Puetz' => 'Amy Puetz',
  'Jessica Rotschafer' => 'Jessica Rotschafer',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Equipment_Facility_Document.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['efd_equipment_facility_doc_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['efd_equipment_facility_doc_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['efd_equipment_facility_doc_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['efd_equipment_facility_doc_category_dom'][''] = '';
$app_list_strings['efd_equipment_facility_doc_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['efd_equipment_facility_doc_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['efd_equipment_facility_doc_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['efd_equipment_facility_doc_subcategory_dom'][''] = '';
$app_list_strings['efd_equipment_facility_doc_status_dom']['Active'] = 'Aktywny';
$app_list_strings['efd_equipment_facility_doc_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['efd_equipment_facility_doc_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['efd_equipment_facility_doc_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['efd_equipment_facility_doc_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['efd_equipment_facility_doc_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['EFD_Equipment_Facility_Doc'] = 'Equipment & Facility Documents';
$app_list_strings['moduleListSingular']['EFD_Equipment_Facility_Doc'] = 'Equipment & Facility Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Equipment_Facility_Records.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['efr_equipment_facility_recor_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['efr_equipment_facility_recor_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['efr_equipment_facility_recor_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['efr_equipment_facility_recor_category_dom'][''] = '';
$app_list_strings['efr_equipment_facility_recor_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['efr_equipment_facility_recor_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['efr_equipment_facility_recor_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['efr_equipment_facility_recor_subcategory_dom'][''] = '';
$app_list_strings['efr_equipment_facility_recor_status_dom']['Active'] = 'Aktywny';
$app_list_strings['efr_equipment_facility_recor_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['efr_equipment_facility_recor_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['efr_equipment_facility_recor_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['efr_equipment_facility_recor_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['efr_equipment_facility_recor_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['EFR_Equipment_Facility_Recor'] = 'Equipment & Facility Records';
$app_list_strings['moduleListSingular']['EFR_Equipment_Facility_Recor'] = 'Equipment & Facility Record';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_iso_17025_units_of_measure_list.php

 // created: 2019-02-20 14:58:21

$app_list_strings['iso_17025_units_of_measure_list']=array (
  '' => '',
  'CO2' => 'CO2',
  'Dimension' => 'Dimension',
  'gm' => 'gm',
  'kg' => 'kg',
  'Length' => 'Length',
  'Mass' => 'Mass',
  'Not Applicable' => 'Not Applicable',
  'Relative Humidity' => 'Relative Humidity',
  'RPM' => 'RPM',
  'Temperature' => 'Temperature',
  'Time' => 'Time',
  'Voltage' => 'Voltage',
  'Volume' => 'Volume',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_current_vendor_status_list.php

 // created: 2019-02-20 18:15:35

$app_list_strings['current_vendor_status_list']=array (
  '' => '',
  'Approved' => 'Approved',
  'Disapproved' => 'Disapproved',
  'Pending Approval' => 'Pending Approval',
  'Retired' => 'Retired',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_company_category_list.php

 // created: 2019-02-26 20:58:51

$app_list_strings['company_category_list']=array (
  '' => '',
  'Sponsor' => 'Sponsor',
  'Sponsor and Vendor' => 'Sponsor and Vendor',
  'Vendor' => 'Vendor',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_company_status_list.php

 // created: 2019-02-20 17:00:31

$app_list_strings['company_status_list']=array (
  'Account Current' => 'Account Current',
  'Hold Data' => 'Hold Data',
  '' => '',
  'Study Pre_Payment' => 'Study Pre-Payment',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_company_type.php

 // created: 2019-02-20 15:20:25

$app_list_strings['company_type']=array (
  'blank' => 'Blank',
  'vendor' => 'Vendor',
  'sponsor' => 'Sponsor',
  'consultant' => 'Consultant',
  'Sponsor and Vendor' => 'Sponsor and Vendor',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_reason_for_transfer_list.php

 // created: 2019-02-20 13:15:16

$app_list_strings['reason_for_transfer_list']=array (
  'Consolidation' => 'Consolidation',
  'Sanitation' => 'Sanitation',
  'Peri operative Relocation' => 'Peri-operative Relocation',
  'Isolation' => 'Isolation',
  'Receipt' => 'Receipt',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_method_of_qualification_list.php

 // created: 2019-02-26 21:06:07

$app_list_strings['method_of_qualification_list']=array (
  '' => '',
  'APS Management Approval' => 'APS Management Approval',
  'CV' => 'CV',
  'Questionnaire' => 'Questionnaire',
  'Site Visit' => 'Site Visit',
  'TBD' => 'TBD',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_vendor_status_list.php

 // created: 2019-02-20 13:42:59

$app_list_strings['vendor_status_list']=array (
  '' => '',
  'Approved' => 'Approved',
  'Disapproved' => 'Disapproved',
  'Pending Approval' => 'Pending Approval',
  'Retired' => 'Retired',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_vendor_level_list.php

 // created: 2019-02-20 17:30:35

$app_list_strings['vendor_level_list']=array (
  '' => '',
  '01' => '01',
  '02' => '02',
  '03' => '03',
  'Not Applicable' => 'Not Applicable',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_category_id_list.php

 // created: 2019-02-20 13:34:00

$app_list_strings['category_id_list']=array (
  '' => '',
  'MSA' => 'MSA',
  'MSA Amendment' => 'MSA Amendment',
  'NDA_CDA' => 'NDA / CDA',
  'Quality Agreement' => 'Quality Agreement',
  'Material Transfer Agreement' => 'Material Transfer Agreement',
  'Qualification' => 'Qualification',
  'Supplemental' => 'Supplemental',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_usda_category_list.php

 // created: 2019-06-17 12:10:47

$app_list_strings['usda_category_list']=array (
  'B' => 'B',
  'C' => 'C',
  'D' => 'D',
  'E' => 'E',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_iacuc.php

 // created: 2019-06-18 18:46:12

$app_list_strings['iacuc']=array (
  'Active' => 'Active',
  'Inactive' => 'Inactive',
  'None' => 'None',
  'Choose IACUC Status' => 'Choose IACUC Status',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_equipment_and_facility_document_type_list.php

 // created: 2019-06-19 15:49:14

$app_list_strings['equipment_and_facility_document_type_list']=array (
  '' => '',
  'Manual' => 'Manual',
  'Part 11 Gap Analysis' => 'Part 11 Gap Analysis',
  'Equipment Qualification' => 'Equipment Qualification',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_address_type_list.php

 // created: 2019-07-03 12:07:35

$app_list_strings['address_type_list']=array (
  '' => '',
  'Billing' => 'Billing',
  'General' => 'General',
  'Shipping' => 'Shipping',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Regulatory_Response_Document.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['rrd_regulatory_response_doc_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['rrd_regulatory_response_doc_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['rrd_regulatory_response_doc_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['rrd_regulatory_response_doc_category_dom'][''] = '';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom'][''] = '';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Active'] = 'Aktywny';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['RRD_Regulatory_Response_Doc'] = 'Regulatory Response Documents';
$app_list_strings['moduleListSingular']['RRD_Regulatory_Response_Doc'] = 'Regulatory Response Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_vendor_type_list.php

 // created: 2019-07-10 12:01:58

$app_list_strings['vendor_type_list']=array (
  '' => '',
  'Equipment' => 'Equipment',
  'Product' => 'Product',
  'Service' => 'Service',
  'Test System' => 'Test System',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_binder_location_list.php

 // created: 2019-07-23 12:14:26

$app_list_strings['binder_location_list']=array (
  '' => '',
  'Not Applicable' => 'Not Applicable',
  'With Equipment' => 'With Equipment',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_response_status_list.php

 // created: 2019-07-30 11:45:19

$app_list_strings['response_status_list']=array (
  '' => '',
  'Complete' => 'Complete',
  'In Progress' => 'In Progress',
  'Draft Sent' => 'Draft Sent',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_anticipated_study_start_timeline_list.php

 // created: 2019-07-31 11:43:13

$app_list_strings['anticipated_study_start_timeline_list']=array (
  '' => '',
  '3 4 weeks' => '3-4 weeks',
  '1 2 months' => '1-2 months',
  '2 3 months' => '2-3 months',
  '3 6 months' => '3-6 months',
  '6 12 months' => '6-12 months',
  '12 months' => '12+ months',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_win_probability_list.php

 // created: 2019-08-01 17:01:07

$app_list_strings['win_probability_list']=array (
  100 => '100%',
  25 => '25%',
  50 => '50%',
  75 => '75%',
  'zeropercent' => '0%',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.customm03_work_product_activities_1_calls.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.customm03_work_product_activities_1_calls.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$app_list_strings['record_type_display']['M03_Work_Product'] = 'Work Products';
$app_list_strings['parent_type_display']['M03_Work_Product'] = 'Work Products';
$app_list_strings['record_type_display_notes']['M03_Work_Product'] = 'Work Products';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$app_list_strings['record_type_display']['M03_Work_Product'] = 'Work Products';
$app_list_strings['parent_type_display']['M03_Work_Product'] = 'Work Products';
$app_list_strings['record_type_display_notes']['M03_Work_Product'] = 'Work Products';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_USDA_Exemption.php

 // created: 2019-08-08 11:45:51

$app_list_strings['USDA_Exemption']=array (
  'Conscious_Restraint' => 'Conscious Restraint',
  'Exercise_Restriction' => 'Exercise Restriction (dogs)',
  'Food_Water_Restriction' => 'Food/Water Restriction',
  'Multiple Major_1' => 'Multiple Major (1 protocol)',
  'Neuromuscular_Blocker' => 'Neuromuscular Blocker',
  'Training' => 'Training',
  'Choose_One' => 'Choose One',
  'Toxicity_Study' => 'Toxicity Study',
  'None' => 'None',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Contact_Documents.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['maj_contact_documents_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['maj_contact_documents_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['maj_contact_documents_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['maj_contact_documents_category_dom'][''] = '';
$app_list_strings['maj_contact_documents_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['maj_contact_documents_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['maj_contact_documents_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['maj_contact_documents_subcategory_dom'][''] = '';
$app_list_strings['maj_contact_documents_status_dom']['Active'] = 'Aktywny';
$app_list_strings['maj_contact_documents_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['maj_contact_documents_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['maj_contact_documents_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['maj_contact_documents_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['maj_contact_documents_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['MAJ_Contact_Documents'] = 'Contact Documents';
$app_list_strings['moduleListSingular']['MAJ_Contact_Documents'] = 'Contact Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_contact_document_list.php

 // created: 2019-08-15 11:31:14

$app_list_strings['contact_document_list']=array (
  '' => '',
  'CV' => 'CV',
  'CV and PRD' => 'CV and PRD',
  'PRD' => 'PRD',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Email_Documents.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['edoc_email_documents_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['edoc_email_documents_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['edoc_email_documents_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['edoc_email_documents_category_dom'][''] = '';
$app_list_strings['edoc_email_documents_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['edoc_email_documents_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['edoc_email_documents_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['edoc_email_documents_subcategory_dom'][''] = '';
$app_list_strings['edoc_email_documents_status_dom']['Active'] = 'Aktywny';
$app_list_strings['edoc_email_documents_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['edoc_email_documents_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['edoc_email_documents_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['edoc_email_documents_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['edoc_email_documents_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['EDoc_Email_Documents'] = 'Email Documents';
$app_list_strings['moduleListSingular']['EDoc_Email_Documents'] = 'Email Document';
$app_list_strings['department_list']['Analytical Services'] = 'Analytical';
$app_list_strings['department_list']['Business Development'] = 'Business Development';
$app_list_strings['department_list']['Facilities'] = 'Facilities';
$app_list_strings['department_list']['Finance'] = 'Finance';
$app_list_strings['department_list']['Histology Services'] = 'Histology Services';
$app_list_strings['department_list']['Human_Resources'] = 'Human Resources';
$app_list_strings['department_list']['In life Large Animal Care'] = 'In-life Large Animal Care';
$app_list_strings['department_list']['In life Large Animal Research'] = 'In-life Large Animal Research';
$app_list_strings['department_list']['In life Small Animal Care'] = 'In-life Small Animal Care';
$app_list_strings['department_list']['In life Small Animal Research'] = 'In-life Small Animal Research';
$app_list_strings['department_list']['Information_Technology'] = 'Information Technology';
$app_list_strings['department_list']['Interventional Surgical Research'] = 'Interventional Surgical Research';
$app_list_strings['department_list']['Lab Services'] = 'Lab Services';
$app_list_strings['department_list']['Operations Support'] = 'Operations Support';
$app_list_strings['department_list']['Pathology Services'] = 'Pathology Services';
$app_list_strings['department_list']['Pharmacology'] = 'Pharmacology Services';
$app_list_strings['department_list']['Process Improvement'] = 'Process Improvement';
$app_list_strings['department_list']['Quality Assurance Unit'] = 'Quality Assurance Unit';
$app_list_strings['department_list']['Regulatory Services'] = 'Regulatory Services';
$app_list_strings['department_list']['Scientific'] = 'Scientific';
$app_list_strings['department_list']['Software Development'] = 'Software Development';
$app_list_strings['department_list']['Toxicology'] = 'Toxicology Services';
$app_list_strings['department_list']['Veterinary Services'] = 'Veterinary Services';
$app_list_strings['department_list'][''] = '';
$app_list_strings['ed_type_list']['Animal Enrollment'] = 'Animal Enrollment';
$app_list_strings['ed_type_list']['Animal Health Status to SD'] = 'Animal Health Status to SD';
$app_list_strings['ed_type_list']['Early Term Early Death'] = 'Early Term/Early Death';
$app_list_strings['ed_type_list']['Test Article Failures'] = 'Test Article Failures';
$app_list_strings['ed_type_list']['Unblinding of Study Personnel'] = 'Unblinding of Study Personnel';
$app_list_strings['ed_type_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_classification_list.php

 // created: 2019-09-16 11:47:52

$app_list_strings['classification_list']=array (
  '' => '',
  'Non Data Generator' => 'Non Data Generator',
  'Data Generator With Electronic Records' => 'Data Generator With Electronic Records',
  'Data Generator Without Electronic Records' => 'Data Generator Without Electronic Records',
  'Non GLP Equipment' => 'Non GLP Equipment',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_equipment_and_facilities_status_list.php

 // created: 2019-09-16 12:15:31

$app_list_strings['equipment_and_facilities_status_list']=array (
  '' => '',
  'Active' => 'Active',
  'Inactive' => 'Inactive',
  'Out for Service' => 'Out for Service',
  'Retired' => 'Retired',
  'Returned to Sponsor' => 'Returned to Sponsor',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Email_Template.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['et_email_template_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['et_email_template_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['et_email_template_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['et_email_template_category_dom'][''] = '';
$app_list_strings['et_email_template_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['et_email_template_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['et_email_template_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['et_email_template_subcategory_dom'][''] = '';
$app_list_strings['et_email_template_status_dom']['Active'] = 'Aktywny';
$app_list_strings['et_email_template_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['et_email_template_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['et_email_template_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['et_email_template_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['et_email_template_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['ET_Email_Template'] = 'Email Templates';
$app_list_strings['moduleListSingular']['ET_Email_Template'] = 'Email Template';
$app_list_strings['department_list']['Analytical Services'] = 'Analytical';
$app_list_strings['department_list']['Business Development'] = 'Business Development';
$app_list_strings['department_list']['Facilities'] = 'Facilities';
$app_list_strings['department_list']['Finance'] = 'Finance';
$app_list_strings['department_list']['Histology Services'] = 'Histology Services';
$app_list_strings['department_list']['Human_Resources'] = 'Human Resources';
$app_list_strings['department_list']['In life Large Animal Care'] = 'In-life Large Animal Care';
$app_list_strings['department_list']['In life Large Animal Research'] = 'In-life Large Animal Research';
$app_list_strings['department_list']['In life Small Animal Care'] = 'In-life Small Animal Care';
$app_list_strings['department_list']['In life Small Animal Research'] = 'In-life Small Animal Research';
$app_list_strings['department_list']['Information_Technology'] = 'Information Technology';
$app_list_strings['department_list']['Interventional Surgical Research'] = 'Interventional Surgical Research';
$app_list_strings['department_list']['Lab Services'] = 'Lab Services';
$app_list_strings['department_list']['Operations Support'] = 'Operations Support';
$app_list_strings['department_list']['Pathology Services'] = 'Pathology Services';
$app_list_strings['department_list']['Pharmacology'] = 'Pharmacology Services';
$app_list_strings['department_list']['Process Improvement'] = 'Process Improvement';
$app_list_strings['department_list']['Quality Assurance Unit'] = 'Quality Assurance Unit';
$app_list_strings['department_list']['Regulatory Services'] = 'Regulatory Services';
$app_list_strings['department_list']['Scientific'] = 'Scientific';
$app_list_strings['department_list']['Software Development'] = 'Software Development';
$app_list_strings['department_list']['Toxicology'] = 'Toxicology Services';
$app_list_strings['department_list']['Veterinary Services'] = 'Veterinary Services';
$app_list_strings['department_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Test_System_Documents.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['tsdoc_test_system_documents_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['tsdoc_test_system_documents_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['tsdoc_test_system_documents_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['tsdoc_test_system_documents_category_dom'][''] = '';
$app_list_strings['tsdoc_test_system_documents_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['tsdoc_test_system_documents_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['tsdoc_test_system_documents_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['tsdoc_test_system_documents_subcategory_dom'][''] = '';
$app_list_strings['tsdoc_test_system_documents_status_dom']['Active'] = 'Aktywny';
$app_list_strings['tsdoc_test_system_documents_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['tsdoc_test_system_documents_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['tsdoc_test_system_documents_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['tsdoc_test_system_documents_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['tsdoc_test_system_documents_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['TSdoc_Test_System_Documents'] = 'Test System Documents';
$app_list_strings['moduleListSingular']['TSdoc_Test_System_Documents'] = 'Test System Document';
$app_list_strings['test_system_document_category_list']['tbd'] = 'tbd';
$app_list_strings['test_system_document_category_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_sales_activity_quote_req_list.php

 // created: 2019-09-30 12:34:28

$app_list_strings['sales_activity_quote_req_list']=array (
  'Required' => 'Required',
  'Revision Required' => 'Revision Required',
  'draft for review' => 'Draft for Review',
  'Send to Sponsor' => 'Send to Sponsor',
  'Out for Sponsor Review' => 'Out for Sponsor Review',
  'Won' => 'Won',
  'Lost' => 'Lost/Unknown',
  'Duplicate' => 'Duplicate',
  'Choose SA Status' => 'Choose SA Status',
  'Lead' => 'Lead',
  'Not Performed' => 'Not Performed',
  'Invoicing_Initiated' => 'Invoicing Initiated',
  'Invoicing_Complete' => 'Invoicing Complete',
  'Unknown' => 'Unknown',
  'Won_waiting' => 'Won - Waiting on Signed Quote/PO',
  'Quote Expired Long Term FollowUp' => 'Quote Expired - Long Term Follow-Up',
  'Invoice_Additional_Testing_Performed' => 'Invoice - Additional Testing Performed',
  'Lead_Long Term Follow Up' => 'Lead - Long Term Follow Up',
  'Quote_Required_Scheduled' => 'Quote Required - Scheduled',
  'Won Quote Revision Required' => 'Won - Quote Revision Required',
  'Won Quote Revision Out for Review' => 'Won- Quote Revision Out for Review',
  'Study Plan Development' => 'Study Plan Development',
  'Won Quote Revision for Review' => 'Won - Quote Revision for Review',
  '' => '',
  'Internal APS Project' => 'Internal APS Project',
  'NDA for Review' => 'NDA for Review',
  'Send NDA to Sponsor' => 'Send NDA to Sponsor',
  'Pricelist Submitted' => 'Pricelist Submitted',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_scientific_activity_list.php

 // created: 2019-10-03 12:07:55

$app_list_strings['scientific_activity_list']=array (
  '' => '',
  'Protocol Development' => 'Protocol Development',
  'Operations Oversight' => 'Operations Oversight',
  'Data Entry_Analysis' => 'Data Entry & Analysis',
  'Reporting' => 'Reporting',
  'Procedure_Assignment' => 'Procedure Assignment',
  'Conference Attendance' => 'Conference Attendance',
  'Out of Office' => 'Out of Office',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_aaalac_exemptions_list.php

 // created: 2019-11-06 14:10:32

$app_list_strings['aaalac_exemptions_list']=array (
  'Choose_One' => 'Choose One',
  'Hazardous_Substances' => 'Hazardous Substances',
  'Survival_Surgery' => 'Survival Surgery',
  'Multiple_Survival_Surgery' => 'Multiple Survival Surgery',
  'None' => 'None',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_vc_related_to_list.php

 // created: 2019-11-07 12:47:19

$app_list_strings['vc_related_to_list']=array (
  '' => '',
  'Study' => 'Study',
  'non Study' => 'non-Study',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_error_classification_c_list.php

 // created: 2019-12-02 13:26:18

$app_list_strings['error_classification_c_list']=array (
  'Error' => 'Error',
  'Non Impactful Deviation' => 'Non-Impactful Deviation',
  'Impactful Deviation' => 'Impactful Deviation',
  'Choose One' => 'Choose One',
  'Withdrawn Error' => 'Withdrawn Error',
  'Observation' => 'Observation',
  'Pending' => 'Pending',
  'Adverse Event' => 'Adverse Event',
  'Unforeseen Circumstance Unanticipated Response' => 'Unforeseen Circumstance/Unanticipated Response (UCUR) (Obsoleted 05/20/19)',
  'Duplicate' => 'Duplicate',
  'Notification' => 'Notification',
  '' => '',
  'Opportunity for Improvement' => 'Opportunity for Improvement',
  'Job well done' => 'Job well done',
  'A' => 'A',
  'M' => 'M',
  'S' => 'S',
  'C' => 'C',
  'NA' => 'NA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_corrective_action_options_list.php

 // created: 2019-12-06 12:55:00

$app_list_strings['corrective_action_options_list']=array (
  '' => '',
  'None' => 'None',
  'Other' => 'Other',
  'Protocol Amendment' => 'Protocol Amendment',
  'Protocol Specific Training' => 'Protocol Specific Training',
  'SOP Update' => 'SOP Update',
  'Protocol Requirement Review' => 'Protocol Requirement Review',
  'Further Investigation of Event' => 'Further Investigation of Event',
  'Report Amendment' => 'Report Amendment',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_test_system_document_category_list.php

 // created: 2019-12-11 12:49:41

$app_list_strings['test_system_document_category_list']=array (
  '' => '',
  'VDL Diagnostic Result' => 'VDL Diagnostic Result',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_normal_see_gas_list.php

 // created: 2020-01-17 10:15:42

$app_list_strings['normal_see_gas_list']=array (
  'Normal' => 'Normal',
  'See Gastrointestinal Notes' => 'See Gastrointestinal Notes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_oral_cavity_list_not_exam.php

 // created: 2020-01-17 10:16:53

$app_list_strings['oral_cavity_list_not_exam']=array (
  'Not Examined' => 'Not Examined',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_oral_cavity_list_normal.php

 // created: 2020-01-17 10:18:00

$app_list_strings['oral_cavity_list_normal']=array (
  'Normal' => 'Normal',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_normal_sln_list.php

 // created: 2020-01-17 10:28:46

$app_list_strings['normal_sln_list']=array (
  'Normal' => 'Normal',
  'See Lymphatic Notes' => 'See Lymphatic Notes',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_shipping_company_list.php

 // created: 2020-02-04 13:17:12

$app_list_strings['shipping_company_list']=array (
  '' => '',
  'Fed Ex' => 'Fed Ex',
  'On Time' => 'On Time',
  'World Courier' => 'World Courier',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_shipping_delivery_list.php

 // created: 2020-02-04 13:20:54

$app_list_strings['shipping_delivery_list']=array (
  '' => '',
  'Direct' => 'Direct',
  'Same Day' => 'Same Day',
  '1 HR' => '1 HR',
  '3 HR' => '3 HR',
  'International Priority' => 'International Priority',
  'First Priority Overnight' => 'First Priority Overnight',
  'Priority Overnight' => 'Priority Overnight',
  '2 Day' => '2 Day',
  'Ground' => 'Ground',
  '2 HR' => '2 HR',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_shipping_conditions_list.php

 // created: 2020-02-04 13:25:53

$app_list_strings['shipping_conditions_list']=array (
  '' => '',
  'Ambient' => 'Ambient',
  'Refrigeratedon ice packs' => 'Refrigerated/on ice packs',
  'Frozen 12on ice packs' => 'Frozen (-12/on ice packs)',
  'Frozen 70on dry ice' => 'Frozen (-70/on dry ice)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_material_category_list.php

 // created: 2020-02-04 13:32:10

$app_list_strings['material_category_list']=array (
  '' => '',
  'Biological Substance' => 'Biological Substance',
  'Category B' => 'Category B',
  'NA' => 'NA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_hazardous_contents_list.php

 // created: 2020-02-06 13:30:25

$app_list_strings['hazardous_contents_list']=array (
  '' => '',
  'Ethanol ETOH' => 'Ethanol (ETOH)',
  'Glutaraldehyde' => 'Glutaraldehyde',
  'Hexane' => 'Hexane',
  'Methanol' => 'Methanol',
  'NA' => 'NA',
  'Other' => 'Other',
  'Biological Substance Category B' => 'Biological Substance – Category B',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_deliverable_type_list.php

 // created: 2020-03-16 11:48:24

$app_list_strings['deliverable_type_list']=array (
  '' => '',
  'Hay' => 'Hay',
  'Slide' => 'Slide',
  'Tissue' => 'Tissue',
  'BloodPlasmaSerum' => 'Blood/Plasma/Serum',
  'Paraffin Blocks' => 'Paraffin Blocks',
  'Study Materials' => 'Study Materials',
  'Feed' => 'Feed',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_quoteco_status_list.php

 // created: 2020-06-11 09:17:12

$app_list_strings['quoteco_status_list']=array (
  'Under Review' => 'Under Review',
  'Sponsor Approved' => 'Sponsor Approved',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_equipment_and_facility_records_type_list.php

 // created: 2020-06-16 09:04:45

$app_list_strings['equipment_and_facility_records_type_list']=array (
  '' => '',
  'Calibration' => 'Calibration',
  'Chain of Custody' => 'Chain of Custody',
  'Initial In Service' => 'Initial In-Service',
  'Inspection' => 'Inspection',
  'Preventative Maintenance' => 'Preventative Maintenance',
  'Repair' => 'Repair',
  'SoftwareFirmware Change' => 'Software/Firmware Change',
  'Cleaning' => 'Cleaning',
  'Equipment Qualification' => 'Equipment Qualification',
  'Service Contract' => 'Service Contract',
  'Verification' => 'Verification',
  'Part 11 Gap Analysis' => 'Part 11 Gap Analysis',
  'Routine Maintenance' => 'Routine Maintenance',
  'Initial In Service 2' => 'Initial in Service',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_why_it_happened_c_list.php

 // created: 2020-06-17 04:00:29

$app_list_strings['why_it_happened_c_list']=array (
  '' => '',
  'Known' => 'Known',
  'Unknown Needs Investigation' => 'Unknown - Needs Investigation',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_work_type_list.php

 // created: 2020-06-25 09:58:04

$app_list_strings['work_type_list']=array (
  '' => '',
  'Per Quote' => 'Per Quote',
  'Additional Work' => 'Additional Work',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_product_status_list.php

 // created: 2020-08-20 08:07:02

$app_list_strings['product_status_list']=array (
  'Active' => 'Active',
  'Out of Production' => 'Out of Production',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_product_notes_multiselect_list.php

 // created: 2020-08-20 08:10:14

$app_list_strings['product_notes_multiselect_list']=array (
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.GDP_Examples.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['ge_gdp_examples_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['ge_gdp_examples_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['ge_gdp_examples_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['ge_gdp_examples_category_dom'][''] = '';
$app_list_strings['ge_gdp_examples_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['ge_gdp_examples_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['ge_gdp_examples_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['ge_gdp_examples_subcategory_dom'][''] = '';
$app_list_strings['ge_gdp_examples_status_dom']['Active'] = 'Aktywny';
$app_list_strings['ge_gdp_examples_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['ge_gdp_examples_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['ge_gdp_examples_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['ge_gdp_examples_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['ge_gdp_examples_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['GE_GDP_Examples'] = 'GDP Examples';
$app_list_strings['moduleListSingular']['GE_GDP_Examples'] = 'GDP Example';
$app_list_strings['gdp_ex_area_list']['Analytical'] = 'Analytical';
$app_list_strings['gdp_ex_area_list']['Histo'] = 'Histo';
$app_list_strings['gdp_ex_area_list']['IVTClin PathSterilization'] = 'IVT/Clin Path/Sterilization';
$app_list_strings['gdp_ex_area_list']['LA ATVets'] = 'LA AT/Vets';
$app_list_strings['gdp_ex_area_list']['LA ISR OpsVets'] = 'LA ISR Ops/Vets';
$app_list_strings['gdp_ex_area_list']['LA PRTVets'] = 'LA PRT/Vets';
$app_list_strings['gdp_ex_area_list']['LA RTVets'] = 'LA RT/Vets';
$app_list_strings['gdp_ex_area_list']['Pathology'] = 'Pathology';
$app_list_strings['gdp_ex_area_list']['PIFacilities'] = 'PI/Facilities';
$app_list_strings['gdp_ex_area_list']['SA ATVets'] = 'SA AT/Vets';
$app_list_strings['gdp_ex_area_list']['SA RTVets'] = 'SA RT/Vets';
$app_list_strings['gdp_ex_area_list']['Sample Prep'] = 'Sample Prep';
$app_list_strings['gdp_ex_area_list']['Scientific'] = 'Scientific';
$app_list_strings['gdp_ex_area_list'][''] = '';
$app_list_strings['pos_neg_list']['Negative'] = 'Negative';
$app_list_strings['pos_neg_list']['Positive'] = 'Positive';
$app_list_strings['pos_neg_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_prd_level_list.php

 // created: 2020-09-08 09:38:33

$app_list_strings['prd_level_list']=array (
  '' => '',
  'Assistant' => 'Assistant',
  'Primary' => 'Primary',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Product_Document.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['prodo_product_document_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['prodo_product_document_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['prodo_product_document_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['prodo_product_document_category_dom'][''] = '';
$app_list_strings['prodo_product_document_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['prodo_product_document_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['prodo_product_document_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['prodo_product_document_subcategory_dom'][''] = '';
$app_list_strings['prodo_product_document_status_dom']['Active'] = 'Aktywny';
$app_list_strings['prodo_product_document_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['prodo_product_document_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['prodo_product_document_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['prodo_product_document_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['prodo_product_document_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['ProDo_Product_Document'] = 'Product Documents';
$app_list_strings['moduleListSingular']['ProDo_Product_Document'] = 'Product Document';
$app_list_strings['quality_requirement_list']['Certificate'] = 'Certificate';
$app_list_strings['quality_requirement_list']['Certificate of Analysis'] = 'Certificate of Analysis';
$app_list_strings['quality_requirement_list']['Certificate of Compliance'] = 'Certificate of Compliance';
$app_list_strings['quality_requirement_list']['Certificate of Conformity'] = 'Certificate of Conformity';
$app_list_strings['quality_requirement_list']['Certificate of Quality'] = 'Certificate of Quality';
$app_list_strings['quality_requirement_list']['Certificate of Sterility'] = 'Certificate of Sterility';
$app_list_strings['quality_requirement_list']['None'] = 'None';
$app_list_strings['quality_requirement_list']['Performance Report'] = 'Performance Report';
$app_list_strings['quality_requirement_list']['Quality Control Production Certificate'] = 'Quality Control & Production Certificate';
$app_list_strings['quality_requirement_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_purchase_unit_list.php

 // created: 2020-09-24 11:47:37

$app_list_strings['purchase_unit_list']=array (
  'Each' => 'Each',
  'Bottle' => 'Bottle',
  'Box' => 'Box',
  'Case' => 'Case',
  '' => '',
  'Pallet' => 'Pallet',
  'Roll' => 'Roll',
  'Sleeve' => 'Sleeve',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_ef_status_list.php

 // created: 2020-10-06 06:36:26

$app_list_strings['ef_status_list']=array (
  '' => '',
  'Active' => 'Active',
  'Active GLP compliant' => 'Active, GLP compliant',
  'Active GLP non compliant' => 'Active, GLP non-compliant',
  'Inactive' => 'Inactive',
  'Out for Service' => 'Out for Service',
  'Quarantined' => 'Quarantined',
  'Retired' => 'Retired',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_disposition_type_deliverable_list.php

 // created: 2020-11-17 08:14:29

$app_list_strings['disposition_type_deliverable_list']=array (
  '' => '',
  'Shipping' => 'Shipping',
  'Discard' => 'Discard',
  'Archive' => 'Archive',
  'Transfer' => 'Transfer',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_expense_or_capitalize_list.php

 // created: 2020-12-08 09:29:58

$app_list_strings['expense_or_capitalize_list']=array (
  '' => '',
  'Expense' => 'Expense',
  'Capitalize' => 'Capitalize',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_high_level_root_cause_action_list.php

 // created: 2020-12-15 04:50:51

$app_list_strings['high_level_root_cause_action_list']=array (
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_impactful_list.php

 // created: 2020-12-15 05:01:15

$app_list_strings['impactful_list']=array (
  '' => '',
  'This Deviation was impactful' => 'This Deviation was impactful.',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_management_assessment_train_list.php

 // created: 2020-12-15 05:04:27

$app_list_strings['management_assessment_train_list']=array (
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_iacuc_deficiency_class_list.php

 // created: 2020-12-15 05:10:11

$app_list_strings['iacuc_deficiency_class_list']=array (
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_wps_outcome_wpe_activities_list.php

 // created: 2020-12-15 06:26:30

$app_list_strings['wps_outcome_wpe_activities_list']=array (
  '' => '',
  'Invasive Procedure' => 'Invasive Procedure',
  'Study Article Exposure' => 'Study Article Exposure',
  'Study Specific Data Recorded' => 'Study Specific Data Recorded',
  'Other Activities' => 'Other Activities',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_ed_type_list.php

 // created: 2020-12-15 12:05:33

$app_list_strings['ed_type_list']=array (
  'Animal Enrollment' => 'Animal Enrollment',
  'Animal Health Status to SD' => 'Animal Health Status to SD',
  'Early Term Early Death' => 'Early Term/Early Death',
  'Test Article Failures' => 'Test Article Failures',
  'Unblinding of Study Personnel' => 'Unblinding of Study Personnel',
  '' => '',
  'Business Development' => 'Business Development',
  'External Feedback' => 'External Feedback',
  'External Feedback Follow Up' => 'External Feedback Follow Up',
  'Necropsy Findings' => 'Necropsy Findings',
  'Vet Findings' => 'Vet Findings',
  'SD Assessment Confirmation' => 'SD Assessment Confirmation',
  'COM email' => 'COM email',
  'Client Correspondence' => 'Client Correspondence',
  'Interinstitutional Assurance' => 'Interinstitutional Assurance',
  'Other' => 'Other',
  'SD COM Notification and Acknowledgment' => 'SD COM Notification and Acknowledgment',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_enrollment_status_list.php

 // created: 2020-12-15 12:39:02

$app_list_strings['enrollment_status_list']=array (
  'Choose One' => 'Choose One',
  'Assigned Backup' => 'Assigned Backup',
  'Assigned On Study' => 'Assigned On Study',
  'Non naive Stock' => 'Non-naive Stock',
  'Not Used' => 'Not Used',
  'On Study' => 'On Study',
  'On Study Backup' => 'On Study Backup',
  'On Study Transferred' => 'On Study Transferred',
  'Enrolled Not Used' => 'Enrolled – Not Used',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_species_sp_list.php

 // created: 2021-01-12 11:35:48

$app_list_strings['species_sp_list']=array (
  '' => '',
  'Bovine' => 'Bovine',
  'Canine' => 'Canine',
  'Caprine' => 'Caprine',
  'Guinea Pig' => 'Guinea Pig',
  'Hamster' => 'Hamster',
  'Lagomorph' => 'Lagomorph',
  'Murine' => 'Murine',
  'Ovine' => 'Ovine',
  'Porcine' => 'Porcine',
  'Rat' => 'Rat',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_service_type_list.php

 // created: 2021-01-12 11:40:53

$app_list_strings['service_type_list']=array (
  '' => '',
  'Analytical' => 'Analytical',
  'Bioskills' => 'Bioskills',
  'Clinical Pathology' => 'Clinical Pathology',
  'Histology' => 'Histology',
  'In Life Service' => 'In Life Service',
  'ISR' => 'ISR',
  'IVT' => 'IVT',
  'LAIL' => 'LAIL',
  'Pathology' => 'Pathology',
  'Phamacology' => 'Phamacology',
  'QA' => 'QA',
  'SAIL' => 'SAIL',
  'Sample Prep' => 'Sample Prep',
  'Scientific' => 'Scientific',
  'Toxicology' => 'Toxicology',
  'Standard Biocompatibility' => 'Standard Biocompatibility',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_service_format_list.php

 // created: 2021-01-28 09:11:22

$app_list_strings['service_format_list']=array (
  'Combination' => 'Combination',
  'Hourly' => 'Hourly',
  'Task' => 'Task',
  '' => '',
  'Each' => 'Each',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Controlled_Documents_QA_Review_Docs.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Controlled_Documents_QA_Review_Docs.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.Controlled_Documents_QA_Review_Docs.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Aktywny';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Aktywny';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['qadoc_cd_qa_rev_docs_category_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['qadoc_cd_qa_rev_docs_subcategory_dom'][''] = '';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Active'] = 'Aktywny';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['qadoc_cd_qa_rev_docs_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Documents';
$app_list_strings['moduleListSingular']['QADoc_CD_QA_Rev_Docs'] = 'QA Review Document';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_stability_considerations_list.php

 // created: 2021-02-18 05:39:42

$app_list_strings['stability_considerations_list']=array (
  '' => '',
  'NA' => 'NA',
  'No' => 'No',
  'Yes' => 'Yes',
  'Unknown' => 'Unknown',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_paraffin_plastic_list.php

 // created: 2021-04-06 09:19:12

$app_list_strings['paraffin_plastic_list']=array (
  '' => '',
  'Paraffin' => 'Paraffin',
  'Plastic' => 'Plastic',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_test_system_list.php

 // created: 2021-05-20 08:59:45

$app_list_strings['test_system_list']=array (
  'Porcine' => 'Porcine - Yorkshire',
  'Porcine Yuc' => 'Porcine - Yucatan',
  'Canine' => 'Canine',
  'Ovine' => 'Ovine',
  'Caprine' => 'Caprine',
  'Bovine' => 'Bovine',
  'Cell Line L929' => 'Cell Line L929',
  'Guinea Pig' => 'Guinea Pig',
  'Rat' => 'Rat',
  'Mouse' => 'Mouse',
  'Human Serum' => 'Normal Human Serum and C3a/SC5b-9 Quidel Kit',
  'Human Plasma' => 'Human Plasma',
  'Human Blood' => 'Human Blood',
  'Bacterial Strain' => 'Bacterial Strain',
  'Porcine Got' => 'Porcine - Gottingen',
  'Ovine Blood' => 'Ovine Blood',
  'Lagomorph' => 'Lagomorph',
  'Lagomorph WHHL' => 'Lagomorph - WHHL',
  'Lagomorph Blood' => 'Lagomorph Blood',
  'Human pRBC' => 'Human pRBC',
  'Human Cadaver' => 'Human Cadaver',
  'Multiple' => 'Multiple',
  'Limulus Amebocyte Lysate and Control Standard Endotoxin' => 'Limulus Amebocyte Lysate and Control Standard Endotoxin',
  'Cell Line L5178Y' => 'Cell Line L5178Y TK+/-',
  'Cell Line V79' => 'Cell Line V79',
  'Hamster' => 'Hamster',
  'CHO_K1' => 'CHO-K1 Cell Line',
  'Balb_c 3t3 Cell Line' => 'Cell Line Balb/c 3t3 ',
  'Extract' => 'Extract of Study Article',
  'Benchtop' => 'Benchtop (Non-Tissue)',
  'Wetlab' => 'Wetlab',
  'Yorkshire Yucatan' => 'Yorkshire & Yucatan',
  'Bovine Blood' => 'Bovine Blood',
  'Porcine Blood' => 'Porcine Blood',
  'Normal Human Serum and SC5b9 Quidel kit' => 'Normal Human Serum and SC5b-9 Quidel kit',
  'Micro Yucatans' => 'Micro Yucatans',
  '' => '',
  'ACRFAAKAACOOH and ACRFAACAACOOH' => 'AC-RFAAKAA-COOH, AC-RFAACAA-COOH',
  'Cell Line THP 1' => 'Cell Line THP-1',
  'Porcine and Bovine' => 'Porcine and Bovine',
  'RhE' => 'RhE',
  'Porcine_Canine_Ovine' => 'Porcine, Canine, and Ovine',
  'Normal_Human_Serum_C3a_Quidel_Kit' => 'Normal Human Serum & C3a Quidel Kit',
  'Ossabaw' => 'Ossabaw',
  'Non biologic' => 'Non-biologic',
  'Ovine Blood and Human Blood' => 'Ovine Blood and Human Blood',
  'Not Applicable' => 'Not Applicable',
  'Normal Human Serum' => 'Normal Human Serum',
  'Limulus Amebocyte Lysate LAL' => 'Limulus Amebocyte Lysate (LAL)',
  'Canine Blood' => 'Canine Blood',
  'Cell line NCTC clone 929 L929 cells' => 'Cell line NCTC clone 929 (L-929 cells)',
  'Cell line L5178Y TKMouse Lymphoma cells' => 'Cell line L5178Y TK+/- (Mouse Lymphoma cells)',
  'Cell line Balbc 3t3 clone A31' => 'Cell line Balb/c 3t3 clone A31',
  'Cell line V79 Chinese hamster lung fibroblast' => 'Cell line V79 Chinese hamster lung fibroblast',
  'Cell line THP1 human monocytic leukemia cells' => 'Cell line THP-1 (human monocytic leukemia cells)',
  'Cell Line CHOK1 Chinese hamster ovary cells' => 'Cell Line CHO-K1 (Chinese hamster ovary cells)',
  'Cell Line K562' => 'Cell Line K-562',
  'Chinchilla' => 'Chinchilla',
  'PorcineGeneral' => 'Porcine',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_deviation_rate_basis_list.php

 // created: 2021-06-29 08:44:30

$app_list_strings['deviation_rate_basis_list']=array (
  '' => '',
  'Animal Days' => 'Animal Days',
  'Procedures' => 'Procedures',
  'Rate Not Calculated' => 'Rate Not Calculated',
  'Studies' => 'Studies',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_final_report_timeline_type_list.php

 // created: 2021-07-22 06:41:33

$app_list_strings['final_report_timeline_type_list']=array (
  '' => '',
  'By First Procedure' => 'By First Procedure',
  'By SPA Reconciliation' => 'By SPA Reconciliation',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_red_yellow_green_list.php

 // created: 2021-07-22 09:09:42

$app_list_strings['red_yellow_green_list']=array (
  '' => '',
  'Red' => 'Red',
  'Yellow' => 'Yellow',
  'Green' => 'Green',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_category_list.php

 // created: 2021-08-05 07:02:54

$app_list_strings['category_list']=array (
  'Critical Phase Inspection' => 'Critical Phase Inspection',
  'Deceased Animal' => 'Deceased Animal',
  'Maintenance Request' => 'Maintenance Request',
  'Rejected Animal' => 'Rejected Animal',
  'Vet Check' => 'Vet Check',
  '' => '',
  'Daily QC' => 'Daily QC',
  'Data Book QC' => 'Data Book QC',
  'Real time study conduct' => 'Real time study conduct',
  'Weekly Sweep' => 'Weekly Sweep',
  'Feedback' => 'Feedback',
  'External Audit' => 'External Audit',
  'Process Audit' => 'Process Audit',
  'Internal Feedback' => 'Internal Feedback',
  'Gross Pathology' => 'Gross Pathology',
  'IACUC Deficiency' => 'IACUC Deficiency',
  'Training' => 'Training',
  'Retrospective Data QC' => 'Retrospective Data QC',
  'Equipment Maintenance Request' => 'Equipment Maintenance Request',
  'Test Failure' => 'Test Failure',
  'Work Product Schedule Outcome' => 'Work Product Outcome',
  'Study Specific Charge' => 'Study Specific Charge',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_minutes_per_slide_list.php

 // created: 2021-08-12 09:02:16

$app_list_strings['minutes_per_slide_list']=array (
  '' => '',
  3 => '3',
  5 => '5',
  10 => '10',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_procedure_room_list.php

 // created: 2021-08-13 06:34:38

$app_list_strings['procedure_room_list']=array (
  1 => 'Cath Lab 1',
  2 => 'Cath Lab 2',
  3 => 'Cath Lab 3',
  4 => 'Cath Lab 4',
  5 => 'CT',
  6 => 'Histology Lab',
  7 => 'Necropsy',
  8 => 'OR1',
  9 => 'OR2',
  10 => 'OR3',
  11 => 'OR4',
  12 => 'OR5',
  13 => 'Sample Prep Lab',
  14 => 'IVT Lab',
  15 => 'Clin Path Lab',
  16 => 'Blood Loop Lab',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_action_needed_list.php

 // created: 2021-10-14 06:55:33

$app_list_strings['action_needed_list']=array (
  '' => '',
  'Not Applicable' => 'Not Applicable',
  'MPLS' => 'MPLS',
  'NW' => 'NW',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_grant_submission_sa_2_list.php

 // created: 2021-10-26 09:03:27

$app_list_strings['grant_submission_sa_2_list']=array (
  '' => '',
  'DoD' => 'DoD',
  'NA' => 'N/A',
  'NIH' => 'NIH',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_reason_for_expansion_list.php

 // created: 2021-10-26 09:06:45

$app_list_strings['reason_for_expansion_list']=array (
  '' => '',
  'Invalid' => 'Invalid',
  'Equivocal' => 'Equivocal',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_gd_study_article_type_list.php

 // created: 2021-10-28 08:51:13

$app_list_strings['gd_study_article_type_list']=array (
  'Control' => 'Control',
  'Sham' => 'Sham',
  'Test' => 'Test',
  'Test Control' => 'Test & Control',
  '' => '',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_stability_considerations_ii_list.php

 // created: 2021-11-09 11:10:11

$app_list_strings['stability_considerations_ii_list']=array (
  '' => '',
  'No' => 'No',
  'Yes' => 'Yes',
  'Unknown' => 'Unknown',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_yes_no_na_list.php

 // created: 2021-11-09 11:16:42

$app_list_strings['yes_no_na_list']=array (
  '' => '',
  'Yes' => 'Yes',
  'No' => 'No',
  'NA' => 'NA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_inventory_item_category_list.php

 // created: 2021-11-10 00:26:20

$app_list_strings['inventory_item_category_list']=array (
  'Product' => 'Product',
  'Record' => 'Record',
  'Solution' => 'Solution',
  'Specimen' => 'Specimen',
  '' => '',
  'Study Article' => 'Study Article',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_inventory_item_owner_list.php

 // created: 2021-11-10 00:31:56

$app_list_strings['inventory_item_owner_list']=array (
  '' => '',
  'APS Owned' => 'APS Owned',
  'APS Supplied' => 'APS Supplied',
  'Client Owned' => 'Client Owned',
  'APS Supplied Client Owned' => 'APS Supplied, Client Owned',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_im_location_type_list.php

 // created: 2021-11-10 00:50:52

$app_list_strings['im_location_type_list']=array (
  'Equipment' => 'Equipment',
  'Room' => 'Room',
  'RoomCabinet' => 'Room/Cabinet',
  'RoomShelf' => 'Room/Shelf',
  'Cabinet' => 'Cabinet',
  'Shelf' => 'Shelf',
  '' => '',
  'Known' => 'Known',
  'Unknown' => 'Unknown',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_inventory_item_status_list.php

 // created: 2021-11-10 01:01:23

$app_list_strings['inventory_item_status_list']=array (
  'Planned Inventory' => 'Planned Inventory',
  'Onsite Inventory' => 'Onsite Inventory',
  'Onsite Inventory Expired' => 'Onsite Inventory (Expired)',
  'Offsite Inventory' => 'Offsite Inventory',
  'Offsite Inventory Expired' => 'Offsite Inventory (Expired)',
  'Onsite Archived' => 'Onsite Archived',
  'Onsite Archived Expired' => 'Onsite Archived (Expired)',
  'Offsite Archived' => 'Offsite Archived',
  'Offsite Archived Expired' => 'Offsite Archived (Expired)',
  '' => '',
  'Discarded' => 'Discarded',
  'Exhausted' => 'Exhausted',
  'Invoiced' => 'Invoiced',
  'Obsolete' => 'Obsolete',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_timepoint_type_list.php

 // created: 2021-11-10 02:21:35

$app_list_strings['timepoint_type_list']=array (
  'Ad Hoc' => 'Ad Hoc',
  'None' => 'None',
  'Work Product Schedule' => 'Work Product Schedule',
  '' => '',
  'AcuteBaseline' => 'Acute/Baseline',
  'AcuteTermination' => 'Acute/Termination',
  'ChronicBaseline' => 'Chronic/Baseline',
  'ChronicTreatment' => 'Chronic/Treatment',
  'Defined' => 'Defined',
  'Follow up' => 'Follow-up',
  'Model CreationBaseline' => 'Model Creation/Baseline',
  'Receipt' => 'Receipt',
  'Termination' => 'Termination',
  'Vet Order' => 'Vet Order',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_inventory_item_storage_condition_list.php

 // created: 2021-11-10 02:34:15

$app_list_strings['inventory_item_storage_condition_list']=array (
  '' => '',
  'Ambient Temperature' => 'Ambient Temperature',
  'Room Temperature' => 'Room Temperature',
  'Refrigerated' => 'Refrigerated',
  'Frozen' => 'Frozen',
  'Ultra Frozen in Liquid Nitrogen' => 'Ultra Frozen in Liquid Nitrogen',
  'Ultra Frozen in Freezer' => 'Ultra Frozen in Freezer',
  'To Be Determined' => 'To Be Determined',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_yes_no_list.php

 // created: 2021-11-10 14:11:39

$app_list_strings['yes_no_list']=array (
  '' => '',
  'Yes' => 'Yes',
  'No' => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_aaalac_and_usda_exemptions_list.php

 // created: 2021-11-18 10:19:23

$app_list_strings['aaalac_and_usda_exemptions_list']=array (
  '' => '',
  'Conscious Restraint' => 'Conscious Restraint',
  'Exercise Restriction dogs' => 'Exercise Restriction (dogs)',
  'FoodWater Restriction' => 'Food/Water Restriction',
  'Multiple Major Surgeries 1protocol' => 'Multiple Major Surgeries (1/protocol)',
  'Multiple Survival Surgeries' => 'Multiple Survival Surgeries',
  'Neuromuscular Blocker' => 'Neuromuscular Blocker',
  'Personnel Training' => 'Personnel Training',
  'Survival Surgery' => 'Survival Surgery',
  'Toxicity Study' => 'Toxicity Study',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_tsd_wt_type_list.php

 // created: 2021-11-30 12:30:22

$app_list_strings['tsd_wt_type_list']=array (
  'Actual' => 'Actual',
  'Actual or having suitable anatomy' => 'Actual or having suitable anatomy',
  '' => '',
  'Weight Appropriate to Age' => 'Weight Appropriate to Age',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_td_phase_list.php

 // created: 2021-12-02 09:32:55

$app_list_strings['td_phase_list']=array (
  '' => '',
  'Ind I' => 'Ind I',
  'SLS' => 'SLS',
  'Ind II' => 'Ind II',
  'Ind IV' => 'Ind IV',
  'Ind V' => 'Ind V',
  'Ind VI' => 'Ind VI',
  'Ind VII' => 'Ind VII',
  'Ind VIII' => 'Ind VIII',
  'Ind IX' => 'Ind IX',
  'Challenge' => 'Challenge',
  'Dose 1' => 'Dose 1',
  'Dose 2' => 'Dose 2',
  'Dose 3' => 'Dose 3',
  'Dose 4' => 'Dose',
  'Dose 5' => 'Dose 5',
  'Day 0' => 'Day 0',
  'Day 1' => 'Day 1',
  'Day 2' => 'Day 2',
  'Day 3' => 'Day 3',
  'Day 4' => 'Day 4',
  'Day 5' => 'Day 5',
  'Day 6' => 'Day 6',
  'Day 7' => 'Day 7',
  'Day 8' => 'Day 8',
  'Day 9' => 'Day 9',
  'Day 10' => 'Day 10',
  'Day 11' => 'Day 11',
  'Day 12' => 'Day 12',
  'Day 13' => 'Day 13',
  'Day 14' => 'Day 14',
  'Day 15' => 'Day 15',
  'Day 16' => 'Day 16',
  'Day 17' => 'Day 17',
  'Day 18' => 'Day 18',
  'Day 19' => 'Day 19',
  'Day 20' => 'Day 20',
  'Day 21' => 'Day 21',
  'Day 22' => 'Day 22',
  'Day 23' => 'Day 23',
  'Day 24' => 'Day 24',
  'Day 25' => 'Day 25',
  'Day 26' => 'Day 26',
  'Day 27' => 'Day 27',
  'Day 28' => 'Day 28',
  'Day 29' => 'Day 29',
  'Day 30' => 'Day 30',
  'Day 31' => 'Day 31',
  'Day 32' => 'Day 32',
  'Day 33' => 'Day 33',
  'Day 34' => 'Day 34',
  'Day 35' => 'Day 35',
  'Day 36' => 'Day 36',
  'Day 37' => 'Day 37',
  'Day 38' => 'Day 38',
  'Day 39' => 'Day 30',
  'Day 40' => 'Day 40',
  'Ind III' => 'Ind III',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_procedure_type_com_list.php

 // created: 2021-12-07 09:02:56

$app_list_strings['procedure_type_com_list']=array (
  '' => '',
  'Initial' => 'Initial',
  'Follow up' => 'Follow-up',
  'Termination' => 'Termination',
  'Acute' => 'Acute',
  'Screening' => 'Screening',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_TSD_purpose_list.php

 // created: 2021-12-07 09:34:21

$app_list_strings['TSD_purpose_list']=array (
  '' => '',
  'Protect item or device from animal no additional weight added' => 'Protect item or device from animal (no additional weight added)',
  'Hold device or item additional weight added' => 'Hold device or item (additional weight added)',
  'Connected to external items' => 'Connected to external items (machines, computers, devices, etc.)',
  'Restrict animals movement only while working with the animal' => 'Restrict animal’s movement only while working with the animal',
  'Complete awake imaging where the animal is required to remain calm and still' => 'Complete awake imaging where the animal is required to remain calm and still',
  'Complete awake imaging where animal is required to be in a specific position for imaging' => 'Complete awake imaging where animal is required to be in a specific position for imaging',
  'Perform procedure on animal while in sling' => 'Perform procedure on animal while in sling',
  'Gain access to limbs or specific area' => 'Gain access to limbs or specific area',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_housing_requirements_list.php

 // created: 2021-12-07 09:55:31

$app_list_strings['housing_requirements_list']=array (
  '' => '',
  'Animals require their own room' => 'Animals require their own room',
  'Electronics present in room' => 'Electronics present in room',
  'Additional space in room required' => 'Additional space in room required',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_number_list.php

 // created: 2021-12-07 10:32:46

$app_list_strings['number_list']=array (
  '01' => '01',
  '02' => '02',
  '03' => '03',
  '04' => '04',
  '05' => '05',
  '06' => '06',
  '07' => '07',
  '08' => '08',
  '09' => '09',
  10 => '10',
  '' => '',
  11 => '11',
  12 => '12',
  13 => '13',
  14 => '14',
  15 => '15',
  16 => '16',
  17 => '17',
  18 => '18',
  19 => '19',
  20 => '20',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_sales_study_article_status_list.php

 // created: 2021-12-07 12:27:56

$app_list_strings['sales_study_article_status_list']=array (
  '' => '',
  'To Ship From NW' => 'To Ship From NW',
  'Shipped to MPLS' => 'Shipped to MPLS',
  'Sponsor to Ship Directly to MPLS' => 'Sponsor to Ship Directly to MPLS',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_degree_of_immobility_list.php

 // created: 2021-12-14 09:08:40

$app_list_strings['degree_of_immobility_list']=array (
  '' => '',
  'Minimal' => 'Minimal',
  'Partial' => 'Partial',
  'Complete' => 'Complete',
  'Minimal Partial' => 'Minimal & Partial',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_type_of_acclimation_list.php

 // created: 2021-12-14 09:09:41

$app_list_strings['type_of_acclimation_list']=array (
  '' => '',
  'JacketHarness' => 'Jacket/Harness',
  'E collar' => 'E-collar',
  'C collar' => 'C-collar',
  'Manual restraint' => 'Manual restraint',
  'Leash' => 'Leash',
  'Other' => 'Other',
  'Crossties' => 'Crossties',
  'Sling' => 'Sling',
  'Imaging Cart' => 'Imaging Cart',
  'Pyrogen testingRabbit restrainer' => 'Pyrogen testing/Rabbit restrainer',
  'Crossties Jacket' => 'Crossties & Jacket',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_inspection_results_list.php

 // created: 2021-12-21 08:32:39

$app_list_strings['inspection_results_list']=array (
  '' => '',
  'No Findings' => 'No Findings',
  'Findings' => 'Findings',
  'Observations' => 'Observations',
  'Findings_Observations' => 'Findings & Observations',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_or_status_list.php

 // created: 2021-12-28 07:20:35

$app_list_strings['or_status_list']=array (
  '' => '',
  'Open' => 'Open',
  'Complete' => 'Complete',
  'Partially Submitted' => 'Partially Submitted',
  'Fully Submitted' => 'Fully Submitted',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_tpr_reference_list.php

 // created: 2022-01-07 06:42:33

$app_list_strings['tpr_reference_list']=array (
  '' => '',
  'Clinical Observations Form' => 'F-S-IL-GN-OP-001-01 – Clinical Observations Form',
  'PARPPC Observations' => 'F-S-IL-GN-OP-001-02 – PAR-PPC Observations – Multiple Incision Form',
  'Anesthesia Recovery Monitoring' => 'F-S-IL-GN-OP-007-01 - Anesthesia Recovery Monitoring',
  'Animal Prep Form' => 'F-S-IL-GN-OP-013-01 – Animal Prep Form',
  'MultiAnimal Procedure Prep Form' => 'F-S-IL-GN-OP-013-02 – Multi-Animal Procedure Prep Form',
  'MultiAnimal Recovery Form' => 'F-S-IL-GN-OP-068-01 - Multi-Animal Recovery Form',
  'Physical Examination Form' => 'F-S-IL-GN-VT-001-01 – Physical Examination Form',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_inventory_item_type_list.php

 // created: 2022-01-18 07:04:55

$app_list_strings['inventory_item_type_list']=array (
  'Accessory Product' => 'Accessory Product',
  'Block' => 'Block',
  'Chemical' => 'Chemical',
  'Cleaning Agent' => 'Cleaning Agent',
  'Control Article' => 'Control Article',
  'Culture' => 'Culture',
  'Data Book' => 'Data Book',
  'Data Sheet' => 'Data Sheet',
  'Drug' => 'Drug',
  'Equipment Facility Record' => 'Equipment / Facility Record',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Hard drive' => 'Hard drive',
  'Protocol Book' => 'Protocol Book',
  'Reagent' => 'Reagent',
  'Serum' => 'Serum',
  'Slide' => 'Slide',
  'Test Article' => 'Test Article',
  'Tissue' => 'Tissue',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
  '' => '',
  'Accessory Article' => 'Accessory Article',
  'EDTA Plasma' => 'EDTA Plasma',
  'Hard Drive' => 'Hard Drive',
  'Na Heparin Plasma' => 'Na Heparin Plasma',
  'NaCit Plasma' => 'NaCit Plasma',
  'Other' => 'Other',
  'Balloons' => 'Balloons',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_universal_inventory_management_type_list.php

 // created: 2022-01-18 07:18:34

$app_list_strings['universal_inventory_management_type_list']=array (
  '' => '',
  'Accessory Article' => 'Accessory Article',
  'Accessory Product' => 'Accessory Product',
  'Autoclave supplies' => 'Autoclave supplies',
  'Balloon Catheter' => 'Balloon Catheter',
  'Bandaging' => 'Bandaging',
  'Block' => 'Block',
  'Blood draw supply' => 'Blood draw supply',
  'Cannula' => 'Cannula',
  'Catheter' => 'Catheter',
  'Chemical' => 'Chemical',
  'Cleaning Agent' => 'Cleaning Agent',
  'Cleaning Supplies' => 'Cleaning Supplies',
  'Contrast' => 'Contrast',
  'Control Article' => 'Control Article',
  'Culture' => 'Culture',
  'Data Book' => 'Data Book',
  'Data Sheet' => 'Data Sheet',
  'Drug' => 'Drug',
  'EDTA Plasma' => 'EDTA Plasma',
  'Endotracheal TubeSupplies' => 'Endotracheal Tube/Supplies',
  'Enrichment Toy' => 'Enrichment Toy',
  'Equipment' => 'Equipment',
  'Equipment Facility Record' => 'Equipment / Facility Record',
  'ETO supplies' => 'ETO supplies',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Feeding supplies' => 'Feeding supplies',
  'Graft' => 'Graft',
  'Hard drive' => 'Hard drive',
  'Hard Drive' => 'Hard Drive',
  'Inflation Device' => 'Inflation Device',
  'NA Heparin Plasma' => 'NA Heparin Plasma',
  'NaCit Plasma' => 'NaCit Plasma',
  'Office Supply' => 'Office Supply',
  'Other' => 'Other',
  'Protocol Book' => 'Protocol Book',
  'Reagent' => 'Reagent',
  'Serum' => 'Serum',
  'Sheath' => 'Sheath',
  'Shipping supplies' => 'Shipping supplies',
  'Slide' => 'Slide',
  'Solution' => 'Solution',
  'Stent' => 'Stent',
  'Surgical Instrument' => 'Surgical Instrument',
  'Surgical site prep' => 'Surgical site prep',
  'Suture' => 'Suture',
  'Test Article' => 'Test Article',
  'Tissue' => 'Tissue',
  'Tubing' => 'Tubing',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
  'Wire' => 'Wire',
  'Balloons' => 'Balloons',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_type_specimen_list.php

 // created: 2022-01-25 09:17:11

$app_list_strings['type_specimen_list']=array (
  '' => '',
  'Balloons' => 'Balloons',
  'Block' => 'Block',
  'Culture' => 'Culture',
  'EDTA Plasma' => 'EDTA Plasma',
  'Extract' => 'Extract',
  'Fecal' => 'Fecal',
  'Na Heparin Plasma' => 'Na Heparin Plasma',
  'NaCit Plasma' => 'NaCit Plasma',
  'Other' => 'Other',
  'Serum' => 'Serum',
  'Slide' => 'Slide',
  'Tissue' => 'Tissue',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_phase_of_inspection_list.php

 // created: 2022-02-01 07:31:00

$app_list_strings['phase_of_inspection_list']=array (
  'Sample Preparation' => 'Sample Preparation',
  'Evaluation' => 'Evaluation',
  'Analytical' => 'Analytical',
  'Contributing Scientist Report' => 'Contributing Scientist Report',
  'In Life' => 'In-Life',
  'Followup Procedure' => 'Follow-up Procedure',
  'Terminal Procedure' => 'Terminal Procedure',
  'Necropsy' => 'Necropsy',
  'Tissue Trimming' => 'Tissue Trimming',
  'Histology' => 'Histology',
  'Data Review' => 'Data Review',
  'Interim Report' => 'Interim Report',
  'Animal Health Report' => 'Animal Health Report',
  'Pathology Report' => 'Pathology Report',
  'Final Report' => 'Final Report',
  'Final Report Amendment' => 'Final Report Amendment',
  'Blood Collection' => 'Blood Collection',
  'Blood Evaluation' => 'Blood Evaluation',
  'Body Weights Observations' => 'Body Weights/Observations',
  'Cell Processing' => 'Cell Processing',
  'Colony Counting' => 'Colony Counting',
  'Dose' => 'Dose',
  'Faxitron' => 'Faxitron',
  'Implant' => 'Implant',
  'In_Life Report' => 'In-Life Report',
  'Interim M_M report' => 'Interim Animal Health Report',
  'Interim Pathology Report' => 'Interim Pathology Report',
  'Morphometry' => 'Morphometry',
  'Sample Receipt' => 'Sample Receipt',
  'SEM Report' => 'SEM Report',
  'Staining' => 'Staining',
  'Treatment' => 'Treatment',
  'Application' => 'Application',
  'Out of Office' => 'Out of Office',
  'Protocol Review' => 'Protocol Review',
  'Other_Non Study Specific' => 'Other - Non Study Specific',
  'Model Creation' => 'Model Creation',
  '' => '',
  'Controlled Document Review' => 'Controlled Document Review',
  'Protocol Amendment' => 'Protocol Amendment',
  'Training Second Review' => 'Training – Second Review',
  'Training CPI' => 'Training – CPI',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_room_type_list.php

 // created: 2022-02-01 07:38:49

$app_list_strings['room_type_list']=array (
  '' => '',
  'Animal Housing' => 'Animal Housing',
  'Archive' => 'Archive',
  'Cath Lab' => 'Cath Lab',
  'IT' => 'IT',
  'Lab' => 'Lab',
  'OR' => 'OR',
  'Shop' => 'Shop',
  'Storage' => 'Storage',
  'Various Locations' => 'Various Locations',
  'Warehouse' => 'Warehouse',
  'CT' => 'CT',
  'Necropsy' => 'Necropsy',
  'Wetlab' => 'Wetlab',
  'Behavior' => 'Behavior',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.CAPA_Files.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['cf_capa_files_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['cf_capa_files_category_dom']['Knowledege Base'] = 'Baza wiedzy';
$app_list_strings['cf_capa_files_category_dom']['Sales'] = 'Sprzedaż';
$app_list_strings['cf_capa_files_category_dom'][''] = '';
$app_list_strings['cf_capa_files_subcategory_dom']['Marketing Collateral'] = 'Materiały marketingowe';
$app_list_strings['cf_capa_files_subcategory_dom']['Product Brochures'] = 'Katalogi produktów';
$app_list_strings['cf_capa_files_subcategory_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['cf_capa_files_subcategory_dom'][''] = '';
$app_list_strings['cf_capa_files_status_dom']['Active'] = 'Aktywny';
$app_list_strings['cf_capa_files_status_dom']['Draft'] = 'Wersja robocza';
$app_list_strings['cf_capa_files_status_dom']['FAQ'] = 'Często zadawane pytania';
$app_list_strings['cf_capa_files_status_dom']['Expired'] = 'Przedawniony';
$app_list_strings['cf_capa_files_status_dom']['Under Review'] = 'Recenzowany';
$app_list_strings['cf_capa_files_status_dom']['Pending'] = 'Oczekujące';
$app_list_strings['moduleList']['CF_CAPA_Files'] = 'CAPA Files';
$app_list_strings['moduleListSingular']['CF_CAPA_Files'] = 'CAPA File';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_ca_andor_pa_dd_list.php

 // created: 2022-02-03 08:09:55

$app_list_strings['ca_andor_pa_dd_list']=array (
  '' => '',
  'CA' => 'CA',
  'PA' => 'PA',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_frequency_score_list.php

 // created: 2022-02-03 08:24:46

$app_list_strings['frequency_score_list']=array (
  '' => '',
  'NA' => 'NA',
  1 => '1 - Rarely',
  2 => '2 - Occasionally',
  3 => '3 - Frequently',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_severity_score_list.php

 // created: 2022-02-03 08:27:26

$app_list_strings['severity_score_list']=array (
  '' => '',
  'NA' => 'NA',
  1 => '1 - Minor',
  3 => '3 - Significant',
  5 => '5 - Critical',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_GD_status_list.php

 // created: 2022-02-03 09:43:54

$app_list_strings['GD_status_list']=array (
  '' => '',
  'In Development' => 'In Development',
  'Complete' => 'Complete',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_task_type_list.php

 // created: 2022-02-03 09:59:19

$app_list_strings['task_type_list']=array (
  'Lead Follow Up' => 'Lead Follow Up',
  'Document Creation' => 'Document Creation',
  'Document Review' => 'Document Review',
  'Quote Follow Up' => 'Quote Follow Up',
  'Sponsor Follow Up' => 'Sponsor Follow Up',
  'Financial' => 'Financial',
  'Work Product' => 'Work Product',
  'Procedure Scheduling' => 'Procedure Scheduling',
  'Quote Review' => 'Quote Review',
  'Sponsor Communication' => 'Sponsor Communication',
  'Sponsor Quote Review' => 'Sponsor Quote Review',
  'Histopathology' => 'Histopathology',
  'Invoice' => 'Invoice',
  'Maintenance Request' => 'Maintenance Request',
  'Vet Check' => 'Vet Check',
  'Send Necropsy Findings' => 'Send Necropsy Findings',
  'Tradeshow' => 'Tradeshow',
  '' => '',
  'Schedule Matrix Review' => 'Schedule Matrix Review',
  'Custom' => 'Custom',
  'Standard' => 'Custom',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_ot_category_list.php

 // created: 2022-02-08 07:41:30

$app_list_strings['ot_category_list']=array (
  '' => '',
  'CatheterWire' => 'Catheter/Wire',
  'Circulatory Assist' => 'Circulatory Assist',
  'Electrophysiology' => 'Electrophysiology',
  'Embolization' => 'Embolization',
  'Endoscopy' => 'Endoscopy',
  'General Cardiac' => 'General Cardiac',
  'Hemostasis' => 'Hemostasis',
  'Integumentary' => 'Integumentary',
  'Leads' => 'Leads',
  'Muscoskeletal' => 'Muscoskeletal',
  'Non Cardiac Ablation' => 'Non Cardiac Ablation',
  'Other' => 'Other',
  'StentsBalloons' => 'Stents/Balloons',
  'Transplant' => 'Transplant',
  'Valve' => 'Valve',
  'Vascular Anastomosis' => 'Vascular Anastomosis',
  'Cardiopulmonary Bypass' => 'Cardiopulmonary Bypass',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_ot_type_list.php

 // created: 2022-02-08 07:42:46

$app_list_strings['ot_type_list']=array (
  '' => '',
  'Ablation' => 'Ablation',
  'Mapping' => 'Mapping',
  'AblationMapping' => 'Ablation/Mapping',
  'Peripheral' => 'Peripheral',
  'Coronary' => 'Coronary',
  'Gastrointestinal' => 'Gastrointestinal',
  'Pulmonary' => 'Pulmonary',
  'Urinary' => 'Urinary',
  'Stent Graft' => 'Stent Graft',
  'Surgical Mitral' => 'Surgical Mitral',
  'Transcatheter Mitral' => 'Transcatheter Mitral',
  'Surgical Tricuspid' => 'Surgical Tricuspid',
  'Transcatheter Tricuspid' => 'Transcatheter Tricuspid',
  'Surgical Aortic' => 'Surgical Aortic',
  'Transcatheter Aortic' => 'Transcatheter Aortic',
  'Surgical Pulmonary' => 'Surgical Pulmonary',
  'Transcatheter Pulmonary' => 'Transcatheter Pulmonary',
  'Interpositional' => 'Interpositional',
  'AVF' => 'AVF',
  'AVG' => 'AVG',
  'CABG' => 'CABG',
  'Cebrebral' => 'Cebrebral',
  'Cardiac' => 'Cardiac',
  'Protection' => 'Protection',
  'Renal' => 'Renal',
  'Thigh' => 'Thigh',
  'Bladder' => 'Bladder',
  'Prostate' => 'Prostate',
  'Surgical LVAD' => 'Surgical LVAD',
  'Transcatheter LVAD' => 'Transcatheter LVAD',
  'Aortic' => 'Aortic',
  'ECMO' => 'ECMO',
  'Liver' => 'Liver',
  'Kidney' => 'Kidney',
  'Left Atrial Closure' => 'Left Atrial Closure',
  'Septal Treatment' => 'Septal Treatment',
  'Thrombectomy' => 'Thrombectomy',
  'Gastroscopy' => 'Gastroscopy',
  'Bronchoscopy' => 'Bronchoscopy',
  'Laprascopy' => 'Laprascopy',
  'Thorascopy' => 'Thorascopy',
  'Intercardiac' => 'Intercardiac',
  'Epicardial' => 'Epicardial',
  'Epidural' => 'Epidural',
  'Vascular Closure' => 'Vascular Closure',
  'Hemostatic Agents' => 'Hemostatic Agents',
  'Vessel Sealing Device' => 'Vessel Sealing Device',
  'Wound Creation' => 'Wound Creation',
  'Bandaging' => 'Bandaging',
  'Surface modulation' => 'Surface modulation',
  'Cardiac Evaluation' => 'Cardiac Evaluation',
  'Peripheral Evaluation' => 'Peripheral Evaluation',
  'TendonLigament Repair' => 'Tendon/Ligament Repair',
  'Orthopedics' => 'Orthopedics',
  'Cartilage Defect' => 'Cartilage Defect',
  'Joint injection' => 'Joint injection',
  'Other' => 'Other',
  'Aneurysm Elastase' => 'Aneurysm Elastase',
  'Aneurysm Surgical' => 'Aneurysm Surgical',
  'Standard' => 'Standard',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_organ_system_list.php

 // created: 2022-02-17 07:17:16

$app_list_strings['organ_system_list']=array (
  '' => '',
  'Cardiovascular' => 'Cardiovascular',
  'GI' => 'GI',
  'Integumentary' => 'Integumentary',
  'Musculoskeletal' => 'Musculoskeletal',
  'Neurologic' => 'Neurologic',
  'Ophthalmic' => 'Ophthalmic',
  'Respiratory' => 'Respiratory',
  'Surgical' => 'Surgical',
  'Systemic' => 'Systemic',
  'Unknown ADR' => 'Unknown-ADR',
  'Urogential' => 'Urogential',
  'Lymphatic' => 'Lymphatic',
  'Clin Path' => 'Clin Path',
  'Not Applicable' => 'Not Applicable',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_po_status_list.php

 // created: 2022-02-22 07:07:34

$app_list_strings['po_status_list']=array (
  '' => '',
  'Pending' => 'Pending',
  'Submitted' => 'Submitted',
  'Complete' => 'Complete',
  'Cancelled' => 'Cancelled',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_deliverable_status_list.php

 // created: 2022-02-22 07:36:28

$app_list_strings['deliverable_status_list']=array (
  'In Progress' => 'In Progress',
  'Waiting on Sponsor' => 'Waiting on Sponsor',
  'Completed' => 'Completed',
  'Overdue' => 'Overdue',
  'Sponsor Retracted' => 'Sponsor Retracted',
  'None' => 'None',
  'Waiting_on_Subcontracted_Report' => 'Waiting on Subcontracted Report',
  'Not Performed' => 'Not Performed',
  'Waiting On Sponsor Ready to Audit' => 'Out to Sponsor, Ready to Audi',
  'Completed Account on Hold' => 'Completed, Account on Hold',
  'Returned to Sponsor' => 'Returned to Sponsor',
  'Sent to Third Party' => 'Sent to Third Party',
  'Discarded' => 'Discarded',
  'Sent to Study DirectorPrinciple Investigator' => 'Sent to Study Director/Principle Investigator',
  '' => '',
  'Pending method development' => 'Pending method development',
  'Out to Study Director Waiting on Redlines' => 'Out to Study Director, Waiting on Redlines',
  'In Data Review' => 'In Data Review',
  'Ready for Pathologist' => 'Ready for Pathologist',
  'Ready for Study Director' => 'Ready for Study Director',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_ii_related_to_c_list.php

 // created: 2022-02-24 08:23:25

$app_list_strings['ii_related_to_c_list']=array (
  'Internal Use' => 'Internal Use',
  'Multiple Test Systems' => 'Multiple Test Systems',
  'Order Request Item' => 'Order Request Item',
  'Purchase Order Item' => 'Purchase Order Item',
  'Sales' => 'Sales',
  'Single Test System' => 'Single Test System',
  'Work Product' => 'Work Product',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_specimen_type_list.php

 // created: 2022-02-24 13:54:36

$app_list_strings['specimen_type_list']=array (
  '' => '',
  'Culture' => 'Culture',
  'Fecal' => 'Fecal',
  'NaCit Plasma' => 'NaCit Plasma',
  'Na Heparin Plasma' => 'Na Heparin Plasma',
  'Other' => 'Other',
  'Serum' => 'Serum',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
  'Balloons' => 'Balloons',
  'EDTA Plasma' => 'EDTA Plasma',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_specimen_type_no_other_c_list.php

 // created: 2022-02-24 13:56:08

$app_list_strings['specimen_type_no_other_c_list']=array (
  '' => '',
  'Culture' => 'Culture',
  'Fecal' => 'Fecal',
  'NaCit Plasma' => 'NaCit Plasma',
  'Na Heparin Plasma' => 'Na Heparin Plasma',
  'Serum' => 'Serum',
  'Urine' => 'Urine',
  'Whole Blood' => 'Whole Blood',
  'Balloons' => 'Balloons',
  'EDTA Plasma' => 'EDTA Plasma',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_integration_work_stream_list.php

 // created: 2022-03-15 07:23:36

$app_list_strings['integration_work_stream_list']=array (
  '' => '',
  'Accounting Finance Tax' => 'Accounting/Finance/Tax',
  'Commercial Plan' => 'Commercial Plan',
  'Communications Plans' => 'Communications Plans',
  'Cultural Integration Plan' => 'Cultural Integration Plan',
  'Human Resources' => 'Human Resources',
  'Integration Strategy Governance' => 'Integration Strategy & Governance',
  'IT' => 'IT',
  'Metrics for Success' => 'Metrics for Success',
  'Operations Biosafety Programs' => 'Operations: Biosafety Programs',
  'Operations Brooklyn Park' => 'Operations: Brooklyn Park',
  'Operations Coon Rapids' => 'Operations: Coon Rapids',
  'Operations Coon Rapids Build Out' => 'Operations: Coon Rapids Build Out',
  'Procurement Supply Chain' => 'Procurement / Supply Chain',
  'Quality' => 'Quality',
  'Sugar Software' => 'Sugar Software',
  'Building Buildouts' => 'Building Buildouts',
  'eQMS' => 'eQMS',
  'Financial Force ERP' => 'Financial Force ERP',
  'Labware' => 'Labware',
  'NAMSA 360 Single Portal' => 'NAMSA 360 (Single Portal)',
  'PSA  Financial Force' => 'PSA – Financial Force',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_plan_actual_list.php

 // created: 2022-03-15 07:31:07

$app_list_strings['plan_actual_list']=array (
  '' => '',
  'Plan' => 'Plan - Group Design',
  'Actual' => 'Actual - TS Specific',
  'Actual SP' => 'Actual - Sample Prep',
  'Plan SP' => 'Plan - Sample Prep',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_td_audit_phase_list.php

 // created: 2022-03-24 08:53:17

$app_list_strings['td_audit_phase_list']=array (
  '' => '',
  'Sample Prep' => 'Sample Prep',
  'Application' => 'Application',
  'Evaluation' => 'Evaluation',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_sale_document_category_list.php

 // created: 2022-03-31 11:46:56

$app_list_strings['sale_document_category_list']=array (
  '' => '',
  'Study Design Document' => 'Study Design',
  'Study Quote' => 'Quote',
  'Quote Revision' => 'Quote Revision',
  'Signed Quote' => 'Signed Quote',
  'Signed Quote Revision' => 'Signed Quote Revision',
  'PO' => 'PO',
  'Change Order' => 'Change Order',
  'Signed Change Order' => 'Signed Change Order',
  'SOW' => 'SOW',
  'Signed SOW' => 'Signed SOW',
  'Interinstitutional Assurance' => 'Interinstitutional Assurance',
  'Pricelist Submission' => 'Pricelist Submission',
  'Publication' => 'Publication',
  'Subcontractor Quote' => 'Subcontractor Quote',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_product_category_list.php

 // created: 2022-03-31 12:04:51

$app_list_strings['product_category_list']=array (
  '' => '',
  'Autoclave supplies' => 'Autoclave supplies',
  'Balloon Catheter' => 'Balloon Catheter',
  'Bandaging' => 'Bandaging',
  'Blood draw supply' => 'Blood draw supply',
  'Cannula' => 'Cannula',
  'Catheter' => 'Catheter',
  'Chemical' => 'Chemical',
  'Cleaning Agent' => 'Cleaning Agent',
  'Cleaning Supplies' => 'Cleaning Supplies',
  'Contrast' => 'Contrast',
  'Drug' => 'Drug',
  'Endotracheal TubeSupplies' => 'Endotracheal Tube/Supplies',
  'Enrichment Toy' => 'Enrichment Toy',
  'Equipment' => 'Equipment',
  'ETO supplies' => 'ETO supplies',
  'Feeding supplies' => 'Feeding supplies',
  'Graft' => 'Graft',
  'Inflation Device' => 'Inflation Device',
  'Office Supply' => 'Office Supply',
  'Other' => 'Other',
  'Reagent' => 'Reagent',
  'Sheath' => 'Sheath',
  'Shipping supplies' => 'Shipping supplies',
  'Solution' => 'Solution',
  'Stent' => 'Stent',
  'Surgical Instrument' => 'Surgical Instrument',
  'Surgical site prep' => 'Surgical site prep',
  'Suture' => 'Suture',
  'Tubing' => 'Tubing',
  'Wire' => 'Wire',
  'Bypass supply' => 'Bypass supply',
  'PPE supply' => 'PPE supply',
  'Control Matrix' => 'Control Matrix',
  'Glassware' => 'Glassware',
  'Interventional Supplies' => 'Interventional Supplies',
  'Lab Supplies' => 'Lab Supplies',
  'Plastic Ware' => 'Plastic Ware',
  'Surgical Supplies' => 'Surgical Supplies',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_Breed_List.php

 // created: 2022-04-05 07:21:32

$app_list_strings['Breed_List']=array (
  'Beagle' => 'Beagle',
  'CD1' => 'CD1',
  'CF1' => 'CF1',
  'Gottingen' => 'Gottingen',
  'Hartley' => 'Hartley',
  'Holstein' => 'Holstein',
  'Mongrel' => 'Mongrel',
  'ND4' => 'ND4',
  'New Zealand White' => 'New Zealand White',
  'Athymic Nude' => 'Athymic Nude',
  'Cross Breed' => 'Cross Breed',
  'Golden Syrian' => 'Golden Syrian',
  'Polypay' => 'Polypay',
  'Sinclair' => 'Sinclair',
  'Sprague Dawley' => 'Sprague Dawley',
  'Suffolk X' => 'Suffolk X',
  'Watanabe' => 'Watanabe',
  'Yorkshire X' => 'Yorkshire X',
  'Yucatan' => 'Yucatan',
  'C57' => 'C57',
  'Wistar' => 'Wistar',
  'CD Hairless' => 'CD Hairless',
  'SKH1 Hairless' => 'SKH1 Hairless',
  'Micro Yucatan' => 'Micro-Yucatan',
  'C3H HeJ' => 'C3H/HeJ',
  'BALBc' => 'BALB/c',
  'Lamancha' => 'Lamancha',
  'Freisen' => 'Freisen',
  'Myotonic' => 'Myotonic',
  'LDLR' => 'LDLR Yucatan',
  'Dutch Belted' => 'Dutch Belted',
  'Alpine' => 'Alpine',
  'Saanen' => 'Saanen',
  'Toggenburg' => 'Toggenburg',
  'Ossabaw' => 'Ossabaw',
  'Jersey' => 'Jersey',
  '' => '',
  'Dorset' => 'Dorset',
  'Dorset X' => 'Dorset X',
  'Hampshire' => 'Hampshire',
  'Hampshire X' => 'Hampshire X',
  'Hanford' => 'Hanford',
  'Other' => 'Other',
  'Polypay X' => 'Polypay X',
  'Suffolk' => 'Suffolk',
  'Landrace Cross' => 'Landrace Cross',
  'Lewis' => 'Lewis',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_ii_test_type_list.php

 // created: 2022-04-12 09:03:57

$app_list_strings['ii_test_type_list']=array (
  '' => '',
  'Aerobic and Anaerobic Culture ID Bacteria and Fungi' => 'Aerobic and Anaerobic Culture, ID-Bacteria and Fungi',
  'Aerobic Culture and ID Bacteria only' => 'Aerobic Culture and ID-Bacteria only',
  'Aerobic Culture and ID Fungi only' => 'Aerobic Culture and ID-Fungi only',
  'Amylase andor Lipase' => 'Amylase and/or Lipase',
  'Anti Factor Xa' => 'Anti-Factor Xa',
  'Blood Culture' => 'Blood Culture',
  'CBC' => 'CBC',
  'CBC with Reticulocytes' => 'CBC with Reticulocytes',
  'Chemistry' => 'Chemistry',
  'Coagulation' => 'Coagulation',
  'Cryptosporidium ELISA' => 'Cryptosporidium ELISA',
  'Custom Chemistry' => 'Custom Chemistry',
  'D Dimer' => 'D-Dimer',
  'Fecal Ova and Parasite' => 'Fecal Ova and Parasite',
  'Fibrinogen' => 'Fibrinogen',
  'Free Hemoglobin' => 'Free Hemoglobin',
  'Giardia ELISA' => 'Giardia ELISA',
  'Haptoglobin' => 'Haptoglobin',
  'Ionized Calcium' => 'Ionized Calcium',
  'Other' => 'Other',
  'PK in Plasma' => 'Pharmacokinetic (PK) in Plasma',
  'PK in Serum' => 'Pharmacokinetic (PK) in Serum',
  'PK in Whole Blood' => 'Pharmacokinetic (PK) in Whole Blood',
  'PT aPTT' => 'PT, aPTT',
  'PT aPTT Fibrinogen' => 'PT, aPTT, Fibrinogen',
  'Troponin 1' => 'Troponin 1',
  'Urinalysis' => 'Urinalysis',
  'Urine ProteinCreatinine Ratio' => 'Urine Protein/Creatinine Ratio',
  'PK in Balloons' => 'Pharmacokinetic (PK) in Balloons',
  'PK in Tissue' => 'Pharmacokinetic (PK) in Tissue',
  'CBC plus Heartworm' => 'CBC plus Heartworm',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_task_procedure_list.php

 // created: 2022-04-21 08:33:24

$app_list_strings['task_procedure_list']=array (
  'Task' => 'Task',
  'Procedure' => 'Procedure',
  '' => '',
  'Unscheduled Task' => 'Unscheduled Task',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_bid_status_list.php

 // created: 2022-04-26 07:09:10

$app_list_strings['bid_status_list']=array (
  '' => '',
  'Open' => 'Open',
  'Testing' => 'Testing',
  'Closed' => 'Closed',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_biod_dose_route_list.php

 // created: 2022-04-26 07:17:36

$app_list_strings['biod_dose_route_list']=array (
  '' => '',
  'IV' => 'IV',
  'IP' => 'IP',
  'IV IP' => 'IV & IP',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_inventory_management_type_list.php

 // created: 2022-05-19 07:21:44

$app_list_strings['inventory_management_type_list']=array (
  '' => '',
  'Archive Offsite' => 'Archive Offsite',
  'Archive Onsite' => 'Archive Onsite',
  'Create Inventory Collection' => 'Create Inventory Collection',
  'Discard' => 'Discard',
  'Exhausted' => 'Exhausted',
  'External Transfer' => 'External Transfer',
  'Internal Transfer Analysis' => 'Internal Transfer - Analysis',
  'Internal Transfer' => 'Internal Transfer',
  'Missing' => 'Missing',
  'Obsolete' => 'Obsolete',
  'Separate Inventory Items' => 'Separate Inventory Collection Item(s)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_wpc_type_list.php

 // created: 2022-05-24 07:05:18

$app_list_strings['wpc_type_list']=array (
  '' => '',
  'IR0' => 'IR0',
  'IR1' => 'IR1',
  'IR2' => 'IR2',
  'IR3' => 'IR3',
  'SE0' => 'SE0',
  'SE1' => 'SE1',
  'ST0' => 'ST0',
  'ST1' => 'ST1',
  'ST2' => 'ST2',
  'ST3' => 'ST3',
  'ST4' => 'ST4',
  'IM0' => 'IM0',
  'CY0' => 'CY0',
  'CY1' => 'CY1',
  'CY2' => 'CY2',
  'CY3' => 'CY3',
  'CY4' => 'CY4',
  'CY5' => 'CY5',
  'CY6' => 'CY6',
  'CY7' => 'CY7',
  'HE0' => 'HE0',
  'HE1' => 'HE1',
  'HE2' => 'HE2',
  'HE3' => 'HE3',
  'HE4' => 'HE4',
  'HE5' => 'HE5',
  'HE7' => 'HE7',
  'HE8' => 'HE8',
  'HE9' => 'HE9',
  'GE0' => 'GE0',
  'GE1' => 'GE1',
  'GE2' => 'GE2',
  'GE3' => 'GE3',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_work_product_status_list.php

 // created: 2022-05-24 07:15:38

$app_list_strings['work_product_status_list']=array (
  'Archived' => 'Archived',
  'Testing' => 'Testing',
  'Withdrawn' => 'Withdrawn',
  'In Development' => 'In Development',
  'Pending' => 'Pending',
  'Reporting' => 'Reporting',
  'Transferred to NW' => 'Transferred to NW',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_aaalac_requirements_list.php

 // created: 2022-06-07 05:25:35

$app_list_strings['aaalac_requirements_list']=array (
  '' => '',
  'Neuromuscular BlockerParalytics' => 'Neuromuscular Blocker/Paralytics',
  'Personnel Training' => 'Personnel Training',
  'Survival Surgery' => 'Survival Surgery',
  'Toxicity Study' => 'Toxicity Study',
  'None' => 'None',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_usda_exemptions_list.php

 // created: 2022-06-07 05:29:34

$app_list_strings['usda_exemptions_list']=array (
  '' => '',
  'Conscious Restraint' => 'Conscious Restraint',
  'Canine Exercise Restrictions' => 'Canine Exercise Restrictions',
  'FoodWater Restriction' => 'Food/Water Restriction (aside from approved fasting)',
  'Multiple Major Survival Surgeries on a Single Protocol' => 'Multiple Major Survival Surgeries on a Single Protocol',
  'Survival Surgeries on Multiple Protocols' => 'Survival Surgeries on Multiple Protocols',
  'None' => 'None',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_department_list.php

 // created: 2022-06-16 05:44:23

$app_list_strings['department_list']=array (
  '' => '',
  'Analytical Services' => 'Analytical',
  'Business Development' => 'Business Development',
  'Clinical Pathology' => 'Clinical Pathology',
  'Facilities' => 'Facilities',
  'Finance' => 'Finance',
  'Histology Services' => 'Histology Services',
  'Human_Resources' => 'Human Resources',
  'In Vitro Testing' => 'In Vitro Testing',
  'In life Large Animal Care' => 'In-life Large Animal Care',
  'Inlife Large Animal prepRecovery' => 'In-life Large Animal Prep/Recovery',
  'In life Large Animal Research' => 'In-life Large Animal Research',
  'In life Small Animal Care' => 'In-life Small Animal Care',
  'In life Small Animal Research' => 'In-life Small Animal Research',
  'In life Small Animal Surgical Research' => 'In-life Small Animal Surgical Research',
  'Information_Technology' => 'Information Technology',
  'Interventional Surgical Research' => 'Interventional Surgical Research',
  'Interventional Surgical Research Operators' => 'Interventional Surgical Research Operators',
  'Interventional Surgical Research Technicians' => 'Interventional Surgical Research Technicians',
  'Operations' => 'Operations',
  'Operations Support' => 'Operations Support',
  'Pathology Services' => 'Pathology Services',
  'Pharmacology' => 'Pharmacology Services',
  'Process Improvement' => 'Process Improvement',
  'Quality Assurance Unit' => 'Quality Assurance Unit',
  'Regulatory Services' => 'Regulatory Services',
  'Sample Preparation' => 'Sample Preparation',
  'Scientific' => 'Scientific',
  'Software Development' => 'Software Development',
  'Toxicology' => 'Toxicology Services',
  'Veterinary Services' => 'Veterinary Services',
  'Employee' => 'Associate',
  'Lab Services' => 'Lab Services (Obsolete)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_location_list.php

 // created: 2022-06-21 06:26:40

$app_list_strings['location_list']=array (
  8945 => '8945',
  8960 => '8960',
  780 => '780',
  '' => '',
  9055 => '9055 Evergreen Blvd.',
  'Remote' => 'Remote',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_extraction_conditions_list.php

 // created: 2022-07-12 09:29:21

$app_list_strings['extraction_conditions_list']=array (
  '' => '',
  '37C' => '37C',
  '50C' => '50C',
  '70C' => '70C',
  'Other' => 'Other',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_payment_terms_list.php

 // created: 2022-07-28 06:41:23

$app_list_strings['payment_terms_list']=array (
  'aps account' => 'APS Account - Net 30',
  'new client' => 'New Client - per bid',
  'pre_pay' => 'Pre-pay',
  'check_with_finance' => 'Check with Finance',
  '' => '',
  'Net 30' => 'Net 30',
  'Net 45' => 'Net 45',
  'Net 60' => 'Net 60',
  'Net 77' => 'Net 77',
  'Per MSA' => 'Per MSA',
  'Net 90' => 'Net 90',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_error_category_list.php

 // created: 2022-10-04 05:47:53

$app_list_strings['error_category_list']=array (
  'Sttudy' => 'Study',
  'Facility' => 'Facility',
  'Early Term' => 'Early Term',
  'Found Dead' => 'Found Dead',
  'Complaint' => 'Complaint',
  'Kudos' => 'Kudos',
  'Major' => 'Major',
  'Minor' => 'Minor',
  'OFI' => 'OFI',
  'Program' => 'Program',
  'Study Article Administered' => 'Study Article Administered',
  'Study Article NOT Administered Surgical Access NOT Performed' => 'Study Article NOT Administered, Surgical Access NOT Performed',
  'Study Article NOT Administered Surgical Access Performed' => 'Study Article NOT Administered, Surgical Access Performed',
  'Trainee' => 'Trainee',
  'Trainer' => 'Trainer',
  'Not Sedated Study Article NOT Administered Surgical Access NOT Performed' => 'Not Sedated, Study Article NOT Administered, Surgical Access NOT Performed',
  '' => '',
  'Expanded Study' => 'Expanded Study',
  'Failed Study' => 'Failed Study',
  'Aborted Study' => 'Aborted Study',
  'CAB' => 'CAB',
  'Invalid Study' => 'Invalid Study',
  'Scientific' => 'Scientific',
  'Operations' => 'Operations',
  'Unused_Medication Administered' => 'Unused/Medication Administered',
  'Unused_No Medication' => 'Unused/No Medication',
  'Initial Issue' => 'Initial Issue',
  'Repeat Issue' => 'Repeat Issue',
  'Resolution Notification' => 'Resolution Notification',
  'Observation at Transfer' => 'Observation at Transfer',
  'Clinical Pathology Review' => 'Clinical Pathology Review',
  'Remaining Departments' => 'Remaining Departments',
  'Re_identification' => 'Re-identification',
  'Herd Health' => 'Herd Health',
  'Deceased Animal' => 'Deceased Animal',
  'SPA Bug' => 'SPA Bug',
  'Performed Per Protocol' => 'Performed Per Protocol',
  'Rejected' => 'Rejected',
  'Unsuccessful Procedure and Survived' => 'Unsuccessful Procedure and Survived',
  'Procedure Death' => 'Procedure Death',
  'Early Term Outcome' => 'Early Term',
  'Found Deceased' => 'Found Deceased',
  'Not Used' => 'Not Used',
  'Early Death' => 'Early Death',
  'Failed Sham' => 'Failed Sham',
  'Passed Sham' => 'Passed Sham',
  'Failed Pyrogen' => 'Failed Pyrogen',
  'Passed Pyrogen' => 'Passed Pyrogen',
  'Trainee Initial Training' => 'Trainee: Initial Training',
  'Trainee Refresher Training' => 'Trainee: Refresher Training',
  'Trainer Initial Training' => 'Trainer: Initial Training',
  'Trainer Refresher Training' => 'Trainer: Refresher Training',
  'Necropsy Findings Notification' => 'Necropsy Findings Notification',
  'Not used shared BU' => 'Not used – shared BU',
  'Sedation for Animal Health' => 'Sedation for Animal Health',
  'Laboratory Operations' => 'Laboratory Operations',
  'Operations Support' => 'Operations Support',
  'Large Animal Operations' => 'Large Animal Operations',
  'Small Animal Operations' => 'Small Animal Operations',
  'Analytical Operations' => 'Analytical Operations',
  'Inlife Operations' => 'In-life Operations',
  'Pathology Operations' => 'Pathology Operations',
  'Standard Biocomp TS Selection' => 'Standard Biocomp TS Selection',
  'Re Challenge' => 'Re-Challenge',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_deliverable_list.php

 // created: 2022-10-25 12:42:02

$app_list_strings['deliverable_list']=array (
  'Final Report' => 'Final Report',
  'Gross Pathology Report' => 'Gross Pathology Report',
  'Histopathology Report' => 'Histopathology Report',
  'Animal Health Report' => 'Animal Health Report',
  'In Life Report' => 'In Life Report',
  'Tissue Shipping' => 'Tissue Shipping',
  'Tissue Trimming' => 'Tissue Trimming',
  'SEM Prep' => 'SEM Prep',
  'SEM' => 'SEM',
  'Slide Completion' => 'Slide Completion',
  'Recuts' => 'Recuts',
  'Slide Shipping' => 'Slide Shipping',
  'Histomorphometry' => 'Histomorphometry',
  'Tissue Receipt' => 'Tissue Receipt',
  'Faxitron' => 'Faxitron',
  'Interim Report' => 'Interim Report',
  'Interim Gross Pathology Report' => 'Interim Gross Pathology Report',
  'Interim Histopathology Report' => 'Interim Histopathology Report',
  'Interim Animal Health Report' => 'Interim Animal Health Report',
  'Interim In Life Report' => 'Interim In Life Report',
  'SEM Report' => 'SEM Report',
  'Analytical Report' => 'Analytical Report',
  'Bioanalytical Data Table' => 'Bioanalytical Data Tables',
  'Bioanalytical Specimen Analysis' => 'Bioanalytical Specimen Analysis',
  'Bioanalytical Validation Report' => 'Bioanalytical Validation Report',
  'Pharmacology Data Summary' => 'Pharmacology Data Summary',
  'Waiting on external test site' => 'Waiting on External Test Site',
  'Bioanalytical Method Development' => 'Bioanalytical Method Development',
  'Bioanalytical Stability Analysis' => 'Bioanalytical Stability Analysis',
  'Histopathology Notes' => 'Histopathology Notes',
  'Histopathology and Histomorphometry Report' => 'Histopathology and Histomorphometry Report',
  'Protocol Development' => 'Protocol Development',
  'QA Data Review' => 'QA Data Review',
  'Critical Phase Audit' => 'Critical Phase Audit',
  'Final Report Amendment' => 'Final Report Amendment',
  'Bioanalytical Sample Prep' => 'Bioanalytical Sample Prep',
  'Bioanalytical Protocol Dvelopment' => 'Analytical Protocol Dvelopment',
  'Statistics Report' => 'Statistics Report',
  'Risk Assessment Report Review' => 'Risk Assessment Report Review',
  'GLP Discontinuation Report' => 'GLP Discontinuation Report',
  'Regulatory Response' => 'Regulatory Response',
  'Analytical ExhaustiveExaggerated Extraction' => 'Analytical Exhaustive/Exaggerated Extraction',
  'Analytical Simulated Use Extraction' => 'Analytical Simulated Use Extraction',
  'Statistical Summary' => 'Statistical Summary',
  'Paraffin Slide Completion' => 'Paraffin Slide Completion',
  'EXAKT Slide Completion' => 'EXAKT Slide Completion',
  'Frozen Slide Completion' => 'Frozen Slide Completion',
  'Plastic Microtome Slide Completion' => 'Plastic Microtome Slide Completion',
  'Slide Scanning' => 'Slide Scanning',
  'Pathology Unknown' => 'Pathology - Unknown',
  'Analytical Nickel Ion Release Extraction' => 'Analytical Nickel Ion Release Extraction',
  'Analytical Headspace Analysis' => 'Analytical Headspace Analysis',
  'Biological Evaluation Plan' => 'Biological Evaluation Plan',
  'Risk Assessment Report' => 'Risk Assessment Report',
  'Analytical Interim Validation Report' => 'Analytical Interim Validation Report',
  'Gross Morphometry' => 'Gross Morphometry',
  'Histopathology Methods Report' => 'Histopathology Methods Report',
  'Analytical Pre testing' => 'Analytical Pre-testing',
  'Abnormal Extract' => 'Abnormal Extract',
  'SpecimenSample Disposition' => 'Specimen/Sample Disposition',
  'BC Abnormal Study Article' => 'BC Abnormal Study Article',
  'Chemical and Physical Characterization of Particulates' => 'Chemical and Physical Characterization of Particulates',
  'Histology Controls' => 'Histology Control(s)',
  'Shipping Request' => 'Shipping Request',
  'Contributing Scientist Report Amendment' => 'Contributing Scientist Report Amendment',
  'Decalcification' => 'Decalcification',
  '' => '',
  'Analytical Method' => 'Analytical Method',
  'Animal Selection' => 'Animal Selection',
  'Early DeathTermination Investigation Report' => 'Early Death/Termination Investigation Report',
  'Histopathology Report2' => 'Histopathology Report (histology only)',
  'IACUC Submission' => 'IACUC Submission',
  'Interim Histopathology Report2' => 'Interim Histopathology Report (histology only)',
  'Pathology Misc' => 'Pathology Misc.',
  'Pathology Protocol Review' => 'Pathology Protocol Review',
  'Pharmacokinetic Report' => 'Pharmacokinetic Report',
  'SEND Report' => 'SEND Report',
  'Sponsor Contracted Contributing Scientist Report' => 'Sponsor Contracted Contributing Scientist Report',
  'Statistical Review' => 'Statistical Review',
  'TWT Protocol Review' => 'TWT Protocol Review',
  'TWT Report Review' => 'TWT Report Review',
  'Animal Selection Populate TSD' => 'Animal Selection – Populate TSD',
  'Animal Selection Procedure Use' => 'Animal Selection – Procedure Use',
  'QC Data Review' => 'QC Data Review',
  'Summary Certificate of ISO 10993' => 'Summary Certificate of ISO 10993',
  'Summary Certificate of USP Class VI Testing' => 'Summary Certificate of USP Class VI Testing',
  'Histomorphometry Report' => 'Histomorphometry Report',
  'Sample Prep Design' => 'Sample Prep Design',
  'Slide Imaging' => 'Slide Imaging',
  'Analytical Exhaustive Extraction' => 'Analytical Exhaustive Extraction',
  'Analytical Exaggerated Extraction' => 'Analytical Exaggerated Extraction',
  'Analytical Headspace GCMS Analysis' => 'Analytical Headspace GCMS Analysis',
  'Analytical Direct Inject GCMS Analysis' => 'Analytical Direct Inject GCMS Analysis',
  'Analytical LCMS Analysis' => 'Analytical LCMS Analysis',
  'Analytical ICPMS Analysis' => 'Analytical ICPMS Analysis',
  'Analytical Headspace GCMS Interpretation' => 'Analytical Headspace GCMS Interpretation',
  'Analytical Direct Inject GCMS Interpretation' => 'Analytical Direct Inject GCMS Interpretation',
  'Analytical LCMS Interpretation' => 'Analytical LCMS Interpretation',
  'Analytical ICPMS Interpretation' => 'Analytical ICPMS Interpretation',
  'Analytical GCMS Sample Preparation' => 'Analytical GCMS Sample Preparation',
  'Analytical LCMS Sample Preparation' => 'Analytical LCMS Sample Preparation',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_equipment_required_list.php

 // created: 2022-10-27 05:52:42

$app_list_strings['equipment_required_list']=array (
  '12 lead' => '12-lead',
  'Bard' => 'Bard',
  'CT' => 'CT',
  'Fluroscopy' => 'Fluroscopy',
  'PowerLab' => 'PowerLab',
  'Ultrasound APS' => 'Ultrasound (APS)',
  'Ultrasound Jim B' => 'Ultrasound (Jim B.)',
  '' => '',
  'Angio' => 'Angio',
  'Any special Gas Requirements' => 'Any special Gas Requirements',
  'Automated Nociception Analyzer' => 'Automated Nociception Analyzer',
  'Bypass' => 'Bypass',
  'C Arm' => 'C-Arm (if in an OR)',
  'Cardiac Output' => 'Cardiac Output',
  'Digital Randall Selitto Device dRS' => 'Digital Randall-Selitto Device (dRS)',
  'Electronic Von Frey eVF' => 'Electronic Von Frey (eVF)',
  'Endoscopic Tower' => 'Endoscopic Tower',
  'Flow Probe' => 'Flow Probe',
  'Hargreaves Hg' => 'Hargreaves (Hg)',
  'Hot Cold Plate' => 'Hot-Cold Plate',
  'IABP' => 'IABP',
  'IVUS' => 'IVUS',
  'OCT' => 'OCT',
  'Other' => 'Other',
  'Pacemaker handheld' => 'Pacemaker - handheld',
  'Pacemaker Micropacer' => 'Pacemaker - Micropacer',
  'Rotarod' => 'Rotarod',
  'UVB' => 'UVB',
  'Velocity' => 'Velocity',
  'Von Frey vF' => 'Von Frey (vF)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_completion_status_list.php

 // created: 2022-11-08 05:44:12

$app_list_strings['completion_status_list']=array (
  'Complete' => 'Complete',
  'Incomplete' => 'Incomplete',
  'Confirmation Needed' => 'Confirmation(s) Needed',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_standard_task_list.php

 // created: 2022-11-08 05:54:03

$app_list_strings['standard_task_list']=array (
  '' => '',
  'Anesthetized Blood Draw' => 'Anesthetized Blood Draw',
  'Awake Blood Draw' => 'Awake Blood Draw',
  'Bandage Change' => 'Bandage Change',
  'Battery ChangeCharge' => 'Battery Change/Charge',
  'Body Weight' => 'Body Weight',
  'CT' => 'CT',
  'EKGECG' => 'EKG/ECG',
  'Extract In' => 'Extract In',
  'Extract Out' => 'Extract Out',
  'Gait Exam' => 'Gait Exam',
  'Neruo Exam' => 'Neruo Exam',
  'Physical Exam' => 'Physical Exam',
  'Ultrasound' => 'Ultrasound',
  'Urinalysis' => 'Urinalysis',
  'Provide Material' => 'Provide Material',
  'Acute' => 'Acute',
  'Behavioral Assessment' => 'Behavioral Assessment',
  'Chronic' => 'Chronic',
  'Device Checks' => 'Device Checks',
  'Dosing' => 'Dosing',
  'Follow up' => 'Follow-up',
  'Implant' => 'Implant',
  'Model Creation' => 'Model Creation',
  'Protocol Specific Acclimation' => 'Protocol Specific Acclimation',
  'Protocol Specific Exercise' => 'Protocol Specific Exercise',
  'Protocol Specific Screening' => 'Protocol Specific Screening',
  'Stimulation' => 'Stimulation',
  'Termination' => 'Termination',
  'Treatment' => 'Treatment',
  'Sample Preparation' => 'Sample Preparation',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_diagnosis_list.php

 // created: 2022-11-08 09:12:07

$app_list_strings['diagnosis_list']=array (
  '' => '',
  'VSD' => 'VSD',
  'Valve Regurg' => 'Valve Regurg',
  'Unknown' => 'Unknown',
  'Congenital' => 'Congenital',
  'Incidental' => 'Incidental',
  'Heart block' => 'Heart block',
  'A Fib' => 'A Fib',
  'Tachycardia' => 'Tachycardia',
  'Bradycardia' => 'Bradycardia',
  'Muffled heart sounds' => 'Muffled heart sounds',
  'Post Op' => 'Post-Op',
  'Undefined' => 'Undefined',
  'Hemorrhage' => 'Hemorrhage',
  'Cardiac disease' => 'Cardiac disease',
  'Cardiac' => 'Cardiac',
  'Pulmonary' => 'Pulmonary',
  'Brachyspira' => 'Brachyspira',
  'Lawsonia' => 'Lawsonia',
  'Salmonella' => 'Salmonella',
  'E coli' => 'E coli',
  'Porcine Parvovirus' => 'Porcine Parvovirus',
  'Giardia' => 'Giardia',
  'Coccidia' => 'Coccidia',
  'Monesia' => 'Monesia',
  'Stress' => 'Stress',
  'Cryptosporidium' => 'Cryptosporidium',
  'Dietary intolerance' => 'Dietary intolerance',
  'Hemorrhagic gastroenteritis' => 'Hemorrhagic gastroenteritis',
  'Dental Disease' => 'Dental Disease',
  'Tartar' => 'Tartar',
  'Gingivitis' => 'Gingivitis',
  'Broken tooth' => 'Broken tooth',
  'Foreign Body' => 'Foreign Body',
  'Volvulus' => 'Volvulus',
  'Primary GI' => 'Primary GI',
  'Secondary GI' => 'Secondary GI',
  'Dietary indiscretion' => 'Dietary indiscretion',
  'Pancreatitis' => 'Pancreatitis',
  'Hepatic disease' => 'Hepatic disease',
  'Choke' => 'Choke',
  'Peritoneal effusion' => 'Peritoneal effusion',
  'Bloat' => 'Bloat',
  'Organomegaly' => 'Organomegaly',
  'Ulcer' => 'Ulcer',
  'Parasitism' => 'Parasitism',
  'Roundworm' => 'Roundworm',
  'Strongile' => 'Strongile',
  'NSAID' => 'NSAID',
  'Rumen' => 'Rumen',
  'Monogastric' => 'Monogastric',
  'GI disease' => 'GI disease',
  'Urinary disease' => 'Urinary disease',
  'Colitis' => 'Colitis',
  'Oral disease' => 'Oral disease',
  'Oral injury' => 'Oral injury',
  'Nausea' => 'Nausea',
  'Neurologic' => 'Neurologic',
  'Abscess' => 'Abscess',
  'Acquired' => 'Acquired',
  'Allergic' => 'Allergic',
  'Anemia' => 'Anemia',
  'Anesthesia recovery' => 'Anesthesia recovery',
  'Animal Altercation' => 'Animal Altercation',
  'Aspiration' => 'Aspiration',
  'Barbaring' => 'Barbaring',
  'Cardiogenic' => 'Cardiogenic',
  'Carpal Valgus' => 'Carpal Valgus',
  'Carpal Varus' => 'Carpal Varus',
  'Cataract' => 'Cataract',
  'Cellulitis' => 'Cellulitis',
  'Central nervous' => 'Central nervous',
  'Cherry Eye' => 'Cherry Eye',
  'Chronic inflammation' => 'Chronic inflammation',
  'CLA' => 'CLA',
  'Coagulopathy' => 'Coagulopathy',
  'Congestion' => 'Congestion',
  'Conjunctivitis' => 'Conjunctivitis',
  'Corneal Edema' => 'Corneal Edema',
  'Corneal Ulcer' => 'Corneal Ulcer',
  'Cystitis' => 'Cystitis',
  'Dermatitis' => 'Dermatitis',
  'Disuse' => 'Disuse',
  'Dystrophy' => 'Dystrophy',
  'Edema' => 'Edema',
  'Environment' => 'Environment',
  'Epistaxis' => 'Epistaxis',
  'Estrus' => 'Estrus',
  'Extracranial' => 'Extracranial',
  'Fleas' => 'Fleas',
  'Fracture' => 'Fracture',
  'Full thickness dehiscence' => 'Full thickness dehiscence',
  'Hematoma' => 'Hematoma',
  'Hematomaseroma' => 'Hematoma/seroma',
  'Hematuria' => 'Hematuria',
  'Hemoglobinuria' => 'Hemoglobinuria',
  'Hoof Crack' => 'Hoof Crack',
  'Hyphema' => 'Hyphema',
  'Hypoglycemia' => 'Hypoglycemia',
  'Hypotension' => 'Hypotension',
  'Idiopathic' => 'Idiopathic',
  'Incisional Swelling' => 'Incisional Swelling',
  'Infection' => 'Infection',
  'Inflammation' => 'Inflammation',
  'Injection site' => 'Injection site',
  'Injection Site Abscess' => 'Injection Site Abscess',
  'Injury' => 'Injury',
  'Interdigital Cyst' => 'Interdigital Cyst',
  'Intracranial' => 'Intracranial',
  'Intubation' => 'Intubation',
  'Joint disease' => 'Joint disease',
  'Joint injury' => 'Joint injury',
  'Laminitis' => 'Laminitis',
  'Lice' => 'Lice',
  'Malignant hyperthermia' => 'Malignant hyperthermia',
  'Mange' => 'Mange',
  'Mastitis' => 'Mastitis',
  'Medication' => 'Medication',
  'Melenoma' => 'Melenoma',
  'Mites' => 'Mites',
  'Mycoplasma' => 'Mycoplasma',
  'Neoplasia' => 'Neoplasia',
  'Neoplasia undefined' => 'Neoplasia-undefined',
  'Neovascularization Pannus' => 'Neovascularization/ Pannus',
  'Non cardiogenic' => 'Non-cardiogenic',
  'Orf' => 'Orf',
  'Orthopedic injury' => 'Orthopedic injury',
  'Otitis externa' => 'Otitis externa',
  'Otitis interna' => 'Otitis interna',
  'Pain' => 'Pain',
  'Parasitic' => 'Parasitic',
  'ParesisParalysis' => 'Paresis/Paralysis',
  'Peripheral nervous' => 'Peripheral nervous',
  'Physiologic' => 'Physiologic',
  'Physiological' => 'Physiological',
  'Pneumonia' => 'Pneumonia',
  'Porphyrinuria' => 'Porphyrinuria',
  'Post clipping' => 'Post-clipping',
  'Post renal' => 'Post-renal',
  'Pre renal' => 'Pre-renal',
  'Pregnancy' => 'Pregnancy',
  'Pseudomonas' => 'Pseudomonas',
  'Pulmonary disease' => 'Pulmonary disease',
  'Purpura' => 'Purpura',
  'Pyelonephritis' => 'Pyelonephritis',
  'Pyoderma' => 'Pyoderma',
  'Pyuria' => 'Pyuria',
  'Recent Weaning' => 'Recent Weaning',
  'Renal' => 'Renal',
  'Reverse sneezing' => 'Reverse sneezing',
  'Ring worm' => 'Ring worm',
  'Rubbing' => 'Rubbing',
  'Sarcoma' => 'Sarcoma',
  'Self Mutilation' => 'Self Mutilation',
  'Seroma' => 'Seroma',
  'Skin only' => 'Skin only',
  'Soft Tissue Injury' => 'Soft Tissue Injury',
  'Staph other' => 'Staph - other',
  'Staph aureus' => 'Staph aureus',
  'Strep' => 'Strep',
  'Strep suis' => 'Strep suis',
  'Study Related' => 'Study Related',
  'Suture Reaction' => 'Suture Reaction',
  'Tender Foot' => 'Tender Foot',
  'Ticks' => 'Ticks',
  'Transfusion' => 'Transfusion',
  'Trauma' => 'Trauma',
  'Trueperella' => 'Trueperella',
  'Upper Respiratory Infection' => 'Upper Respiratory Infection',
  'Urinary obstruction' => 'Urinary obstruction',
  'Urolithasis' => 'Urolithasis',
  'Urticaria' => 'Urticaria',
  'Vaccine' => 'Vaccine',
  'Vaccine site' => 'Vaccine site',
  'Vaginitis' => 'Vaginitis',
  'Vasculitis' => 'Vasculitis',
  'Yucatan' => 'Yucatan',
  'Cyniclomyces guttulatus' => 'Cyniclomyces guttulatus',
  'Normal' => 'Normal',
  'Actinomyces hyovaginalis' => 'Actinomyces hyovaginalis',
  'Adenovirus' => 'Adenovirus',
  'Anticoagulation therapy' => 'Anticoagulation therapy',
  'Arthritis' => 'Arthritis',
  'BehaviorTemperament' => 'Behavior/Temperament',
  'Bordetella bronchiseptica' => 'Bordetella bronchiseptica',
  'Burn' => 'Burn',
  'Clawhoof avulsion' => 'Claw/hoof avulsion',
  'Device related' => 'Device related',
  'Diabetes' => 'Diabetes',
  'Ear Tag infections' => 'Ear Tag infections',
  'Endocarditis' => 'Endocarditis',
  'Erysipelas' => 'Erysipelas',
  'Fusobacterium' => 'Fusobacterium',
  'Gastric reflux' => 'Gastric reflux',
  'GI prep' => 'GI prep',
  'Glaesserella parasuis' => 'Glaesserella parasuis',
  'Hygroma' => 'Hygroma',
  'Incisional hernia' => 'Incisional hernia',
  'Influenza' => 'Influenza',
  'Inguinal hernia' => 'Inguinal hernia',
  'Keratoconjunctivitis Sicca KCS' => 'Keratoconjunctivitis Sicca (KCS)',
  'Laryngitis' => 'Laryngitis',
  'Medication Overdose' => 'Medication Overdose',
  'Microphthalmia' => 'Microphthalmia',
  'Myodegeneration' => 'Myodegeneration',
  'OPPV' => 'OPPV',
  'Pasteurella multocida' => 'Pasteurella multocida',
  'Patellar Luxation' => 'Patellar Luxation',
  'Peritonitis' => 'Peritonitis',
  'Perivascular Administration' => 'Perivascular Administration',
  'Pleural Effusion' => 'Pleural Effusion',
  'Pododermatitis' => 'Pododermatitis',
  'Porcine circovirus PCV' => 'Porcine circovirus (PCV)',
  'Rodent Urological Syndrome' => 'Rodent Urological Syndrome',
  'Saccharomycosis' => 'Saccharomycosis',
  'Septic Arthritis' => 'Septic Arthritis',
  'Shearing Injury' => 'Shearing Injury',
  'T pyogenes' => 'T. pyogenes',
  'Umbilical hernia' => 'Umbilical hernia',
  'Enterococcus gallinarum' => 'Enterococcus gallinarum',
  'Staphylococcus chromogenes' => 'Staphylococcus chromogenes',
  'Aortic valve regurgitation' => 'Aortic valve regurgitation',
  'Atrial Fibrillation' => 'Atrial Fibrillation',
  'BALT hyperplasia' => 'BALT hyperplasia',
  'Central Venous Line Infection' => 'Central Venous Line Infection',
  'Clostridium perfringens' => 'Clostridium perfringens',
  'Coccidiosis' => 'Coccidiosis',
  'Endometritis' => 'Endometritis',
  'GI procedure prep' => 'GI procedure prep',
  'Horners' => 'Horner\'s Syndrome',
  'Ileus' => 'Ileus',
  'Klebsiella pneumoniae' => 'Klebsiella pneumoniae',
  'Mitral valve regurgitation' => 'Mitral valve regurgitation',
  'Musculoskeletal' => 'Musculoskeletal',
  'Normal for PostOperative Recovery' => 'Normal for Post-Operative Recovery',
  'Overgrown Hooves' => 'Overgrown Hooves',
  'Pericarditis' => 'Pericarditis',
  'Pulmonic valve regurgitation' => 'Pulmonic valve regurgitation',
  'Rotavirus' => 'Rotavirus',
  'Toxicity' => 'Toxicity',
  'Feed transition' => 'Feed transition',
  'Mannheimia haemolytica' => 'Mannheimia haemolytica',
  'Sialocele' => 'Sialocele',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_condition_list.php

 // created: 2022-11-08 09:13:27

$app_list_strings['condition_list']=array (
  '' => '',
  'ADR' => 'ADR',
  'Anesthesia' => 'Anesthesia',
  'Ataxia' => 'Ataxia',
  'Blepharospasm' => 'Blepharospasm',
  'Bloat' => 'Bloat',
  'Choke' => 'Choke',
  'Conjunctivits' => 'Conjunctivits',
  'Corneal Edema' => 'Corneal Edema',
  'Coughing' => 'Coughing',
  'Dehiscence' => 'Dehiscence',
  'Dental disease' => 'Dental disease',
  'Diarrhea' => 'Diarrhea',
  'Ectoparasite' => 'Ectoparasite',
  'Head Tilt' => 'Head Tilt',
  'Heart murmur' => 'Heart murmur',
  'Hematuria' => 'Hematuria',
  'Infection' => 'Infection',
  'Intubation' => 'Intubation',
  'Lame' => 'Lame',
  'Nasal discharge' => 'Nasal discharge',
  'Obesity' => 'Obesity',
  'Other' => 'Other',
  'Otitis Externa' => 'Otitis Externa',
  'Paraphimosis' => 'Paraphimosis',
  'Poor Confirmation' => 'Poor Confirmation',
  'Post op Swelling' => 'Post-op Swelling',
  'Pyrexia' => 'Pyrexia',
  'Rash' => 'Rash',
  'Respiratory distress' => 'Respiratory distress',
  'Seizure' => 'Seizure',
  'Self mutilation' => 'Self-mutilation',
  'Swelling' => 'Swelling',
  'Trauma' => 'Trauma',
  'Urinary obstruction' => 'Urinary obstruction',
  'Vomiting' => 'Vomiting',
  'Vulvar discharge' => 'Vulvar discharge',
  'Weight loss' => 'Weight loss',
  'Wound' => 'Wound',
  'Arrhythmia' => 'Arrhythmia',
  'Hypoxia' => 'Hypoxia',
  'Pericardial effusion' => 'Pericardial effusion',
  'Prolonged CRT Cyanosis' => 'Prolonged CRT / Cyanosis',
  'Abdominal distension' => 'Abdominal distension',
  'Melena' => 'Melena',
  'Internal Parasites' => 'Internal Parasites',
  'Inappetence' => 'Inappetence',
  'Rectal Prolapse' => 'Rectal Prolapse',
  'Ulcer' => 'Ulcer',
  'Ileus' => 'Ileus',
  'Peritoneal effusionAscites' => 'Peritoneal effusion/Ascites',
  'Hematochezia' => 'Hematochezia',
  'Constipation' => 'Constipation',
  'Ptyalism' => 'Ptyalism',
  'Blisters' => 'Blisters',
  'PapulesPustules' => 'Papules/Pustules',
  'Mass' => 'Mass',
  'Erythema' => 'Erythema',
  'Pruritis' => 'Pruritis',
  'Alopecia' => 'Alopecia',
  'Hyperpigmentation' => 'Hyperpigmentation',
  'Conformation' => 'Conformation',
  'Atrophy' => 'Atrophy',
  'Joint Swelling' => 'Joint Swelling',
  'Paresis' => 'Paresis',
  'Paralysis' => 'Paralysis',
  'Deafness' => 'Deafness',
  'Nystagmus' => 'Nystagmus',
  'Syncope' => 'Syncope',
  'Corneal Opacity' => 'Corneal Opacity',
  'Chemosis' => 'Chemosis',
  'Blindness' => 'Blindness',
  'Scleral Injection' => 'Scleral Injection',
  'Horners' => 'Horner\'s',
  'Respiratory Stridor' => 'Respiratory Stridor',
  'Lung lesions' => 'Lung lesions',
  'Tachypnea' => 'Tachypnea',
  'Pleural effusion' => 'Pleural effusion',
  'Malaise' => 'Malaise',
  'Hemorrhage' => 'Hemorrhage',
  'Lymphadenopathy' => 'Lymphadenopathy',
  'Dehydration' => 'Dehydration',
  'Anaphylaxis' => 'Anaphylaxis',
  'PetechiaEcchyomoses' => 'Petechia/Ecchyomoses',
  'Pallor' => 'Pallor',
  'Shock' => 'Shock',
  'Recumbency' => 'Recumbency',
  'Bandage abnormality' => 'Bandage abnormality',
  'StrainingDysuria' => 'Straining/Dysuria',
  'Vulvar Swelling' => 'Vulvar Swelling',
  'Udder Enlargement' => 'Udder Enlargement',
  'Preputial Swelling' => 'Preputial Swelling',
  'Azotemia' => 'Azotemia',
  'Discolored urine' => 'Discolored urine',
  'Underweight' => 'Underweight',
  'Post Op Exam' => 'Post-Op Exam',
  'QAcclimation Activity' => 'Q/Acclimation Activity',
  'Routine Exam' => 'Routine Exam',
  'Abrasion' => 'Abrasion',
  'Abscess' => 'Abscess',
  'Apnea' => 'Apnea',
  'Bradycardia' => 'Bradycardia',
  'Bruising' => 'Bruising',
  'Discharge' => 'Discharge',
  'Enophthalmos' => 'Enophthalmos',
  'Hematemesis' => 'Hematemesis',
  'Hypothermia' => 'Hypothermia',
  'Icteric' => 'Icterus',
  'Laceration' => 'Laceration',
  'Necropsy Finding' => 'Necropsy Finding (Obsolete)',
  'Nodule' => 'Nodule',
  'Normal Exam' => 'Normal Exam',
  'Rectal Bleeding' => 'Rectal Bleeding',
  'Scabbing' => 'Scabbing',
  'Tachycardia' => 'Tachycardia',
  'Ulceration' => 'Ulceration',
  'Entropion' => 'Entropion',
  'Ectropion' => 'Ectropion',
  'Hypotension' => 'Hypotension',
  'Hypertension' => 'Hypertension',
  'Gingivitis' => 'Gingivitis',
  'Nausea' => 'Nausea',
  'Regurgitation' => 'Regurgitation',
  'Edema' => 'Edema',
  'Hoof crack' => 'Hoof crack',
  'Pustules' => 'Pustules',
  'Urticaria' => 'Urticaria',
  'Hyphema' => 'Hyphema',
  'Microphthalmia' => 'Microphthalmia',
  'NeovascularizationPannus' => 'Neovascularization / Pannus',
  'Dyspnea' => 'Dyspnea',
  'Hemoptysis' => 'Hemoptysis',
  'Hypoglycemia' => 'Hypoglycemia',
  'Bilirubinuria' => 'Bilirubinuria',
  'Hemoglobinuria' => 'Hemoglobinuria',
  'Porphyrinuria' => 'Porphyrinuria',
  'Pyuria' => 'Pyuria',
  'Heart block' => 'Heart block',
  'Muffled heart sounds' => 'Muffled heart sounds',
  'Catheter Site Swelling' => 'Catheter Site Swelling',
  'Incisional swelling' => 'Incisional swelling',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_amendment_request_list.php

 // created: 2022-12-01 05:37:11

$app_list_strings['amendment_request_list']=array (
  '' => '',
  'APS Error' => 'APS Error',
  'Sponsor Error' => 'Sponsor Error',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_error_type_list.php

 // created: 2022-12-01 09:46:37

$app_list_strings['error_type_list']=array (
  'Documentation Error' => 'Documentation (Initials/Date)',
  'Documentation Error Other' => 'Documentation Error (Other)',
  'Equipment' => 'Equipment',
  'Medication or Dosing' => 'Medication/Dosing',
  'Missed Task General Protocol' => 'Missed Task - General (Protocol Only)',
  'Missed Task General SOP' => 'Missed Task - General (SOP Only)',
  'Missed Task General Protocol or Sop' => 'Missed Task - General (Protocol and SOP)',
  'Missed Task Vet Order' => 'Missed Task - Vet Order',
  'Missed Task Water' => 'Missed Task - Water',
  'Missed Task Food' => 'Missed Task - Food',
  'Utilized noncurrent controlled doc' => 'Utilized Non-current Controlled Doc',
  'GLP Noncompliance' => 'GLP Noncompliance',
  'Standards Noncompliance' => 'Standard(s) Noncompliance',
  'Test Article' => 'Test Article',
  'Control Article' => 'Control Article',
  'Ancillary Article' => 'Ancillary Article',
  'Study Article' => 'Study Article',
  'Test System Model' => 'Test System Model',
  'Materials andor Methods' => 'Materials and/or Methods',
  'Undetermined' => 'Undetermined',
  'Animal Health' => 'Animal Health',
  'Photo Nonconformance' => 'Photo Nonconformance',
  'Missed Deliverable Generation' => 'Missed Deliverable Generation',
  'ACT out of range' => 'ACT out of range',
  'Start up' => 'Start up',
  'Chemical Reagent Expired' => 'Chemical / Reagent Expired',
  'Chemical Reagent Label Error' => 'Chemical / Reagent Label Error',
  'Cleaning Supply Expired' => 'Cleaning Supply Expired',
  'Cleaning Supply Label Error' => 'Cleaning Supply Label Error',
  'Drug Expired' => 'Drug Expired',
  'Drug Label Error' => 'Drug Label Error',
  'Equipment PMCal Overdue' => 'Equipment PM/Cal Overdue',
  'Equipment Tag Missing' => 'Equipment Tag Missing',
  'GLP Study Article Expired' => 'GLP Study Article Expired',
  'GLP Study Article Label Error' => 'GLP Study Article Label Error',
  'Lab Supply Expired' => 'Lab Supply Expired',
  'Controlled Documents Not Archived' => 'Controlled Documents Not Archived',
  'Controlled Documents Not QCed' => 'Controlled Documents Not QCed',
  'Other' => 'Other',
  'Specimen Label Error' => 'Specimen Label Error',
  'Report Compilation Error' => 'Report Compilation Error',
  'Extract Abnormality' => 'Extract Abnormality',
  'Study Article Abnormality' => 'Study Article Abnormality',
  'Animal Weight' => 'Animal Weight',
  'Analytical Method Criteria Failure' => 'Analytical Method Criteria Failure',
  'Incorrect Disregard' => 'Incorrect – Disregard',
  'Sample Prep Error' => 'Sample Prep Error',
  'ACT iSTAT error code' => 'ACT (i-STAT) error code',
  'ACT iSTAT starout' => 'ACT (i-STAT) star-out',
  'ACT time interval missed' => 'ACT time interval missed',
  'Not Applicable' => 'Not Applicable',
  '' => '',
  'Fluid Administration' => 'Fluid Administration',
  'Rejected Animal' => 'Rejected Animal',
  'Success Criteria Failure' => 'Success Criteria Failure',
  'Unused Animal' => 'Unused Animal',
  'Validity Criteria Failure' => 'Validity Criteria Failure',
  'Random FT glitch' => 'Random FT glitch',
  'ACT value out of range 1000 sec' => 'ACT value out of range: >1000 sec',
  'ACT value out of range high' => 'ACT value out of range: high',
  'ACT value out of range low' => 'ACT value out of range: low',
  'Additional activity performed' => 'Additional activity performed',
  'Analytical standards noncompliance' => 'Analytical: standard(s) noncompliance',
  'Anesthesia Change Without Comment' => 'Anesthesia: change without comment',
  'Anesthesia Incorrect Change' => 'Anesthesia: incorrect change',
  'Anesthesia Monitor Interval Missed' => 'Anesthesia: monitor interval missed',
  'Animal Escape' => 'Animal Escape',
  'Animal Identification incorrect documentation' => 'Animal Identification: incorrect documentation',
  'Animal Identification incorrect identification' => 'Animal Identification: incorrect identification',
  'Animal Procurement Incorrect' => 'Animal Procurement Incorrect',
  'Animal Re identification' => 'Animal Re-identification',
  'Animal Weight out of range' => 'Animal Weight: out of range',
  'Animal Weight suspect data' => 'Animal Weight: suspect data',
  'Clinical Pathology missing analyte' => 'Clinical Pathology: missing analyte',
  'Clinical Pathology missing vet review' => 'Clinical Pathology: missing vet review',
  'Clinical Pathology sample quality issue' => 'Clinical Pathology: sample quality issue',
  'Data Transfer not performed' => 'Data Transfer: not performed',
  'Disregard issue correctable' => 'Disregard: issue correctable',
  'Disregard unnecessary COM' => 'Disregard: unnecessary COM',
  'Documentation illegiblewriteover' => 'Documentation: illegible/writeover',
  'Documentation invalid entry' => 'Documentation: invalid entry',
  'Documentation missing data' => 'Documentation: missing data',
  'Documentation missing footnote' => 'Documentation: missing footnote',
  'Documentation missingincomplete verification' => 'Documentation: missing/incomplete verification',
  'Duplicate' => 'Duplicate',
  'Equipment Malfunction' => 'Equipment: Malfunction',
  'Equipment OOS' => 'Equipment: OOS',
  'FastTrack Glitch' => 'FastTrack Glitch',
  'FastTrack Incorrect' => 'FastTrack Incorrect',
  'Incorrect labels' => 'Incorrect label',
  'MedicationDosing Wrong Dose' => 'Medication/Dosing: Wrong Dose',
  'MedicationDosing Wrong route' => 'Medication/Dosing: Wrong route',
  'Missed Exam scheduling' => 'Missed Exam scheduling',
  'Missed Task Acclimation' => 'Missed Task: Acclimation',
  'Missed Task Bandaging' => 'Missed Task: Bandaging',
  'Missed Task Cage Cleaning' => 'Missed Task: Cage Cleaning',
  'Missed Task Clinical Pathology' => 'Missed Task: Clinical Pathology',
  'Missed Task Exam' => 'Missed Task: Exam',
  'Missed Task Exercise' => 'Missed Task: Exercise',
  'Missed Task Histology' => 'Missed Task: Histology',
  'Missed Task Imaging' => 'Missed Task: Imaging',
  'Missed Task Microbiology' => 'Missed Task: Microbiological Monitoring',
  'Missed Task Necropsy' => 'Missed Task: Necropsy',
  'Missed Task Observation' => 'Missed Task: Observation',
  'Missed Task Prep' => 'Missed Task: Prep',
  'Missed Task Preventive Health' => 'Missed Task: Preventive Health',
  'Missed Task Protocol Activity' => 'Missed Task: Protocol Activity',
  'Missed task scheduling' => 'Missed task scheduling',
  'Missed Task Specimen Collection' => 'Missed Task: Specimen Collection',
  'Missed Task Specimen Processing' => 'Missed Task: Specimen Processing',
  'Missed Task Substance Administration' => 'Missed Task: Substance Administration',
  'Missed Task Urinary Management' => 'Missed Task: Urinary Management',
  'Missed Task Vitals Monitor' => 'Missed Task: Vitals Monitor',
  'Missed Task Weight' => 'Missed Task: Weight',
  'Missed Task Wound Care' => 'Missed Task: Wound Care',
  'Missing forms' => 'Missing form',
  'Photo Nonconformance Image Quality' => 'Photo Nonconformance: Image Quality',
  'Photo Nonconformance Labeling Issue' => 'Photo Nonconformance: Labeling Issue',
  'Post op Form incorrect' => 'Post-op Form incorrect',
  'Pre op Form incorrect' => 'Pre-op Form incorrect',
  'Sample Prep Error Analytical' => 'Sample Prep Error: Analytical',
  'Sample Prep Error Extraction Duration' => 'Sample Prep Error: Extraction Duration',
  'Sample Prep Error Extraction Ratio' => 'Sample Prep Error: Extraction Ratio',
  'Sample Prep Error Extraction Temperature' => 'Sample Prep Error: Extraction Temperature',
  'Sample Prep Error Wrong components' => 'Sample Prep Error: Wrong components',
  'Sample Prep Error Wrong storage' => 'Sample Prep Error: Wrong storage',
  'Sample Prep Error Wrong TACA concentration' => 'Sample Prep Error: Wrong TA/CA concentration',
  'Sample Prep Error Wrong vehicle' => 'Sample Prep Error: Wrong vehicle',
  'SPA Bug' => 'SPA Bug',
  'Specimen Collection form incorrect' => 'Specimen Collection form incorrect',
  'Startups worksheet incorrect' => 'Startups worksheet incorrect',
  'Storage Conditions chemicalreagent' => 'Storage Conditions: chemical/reagent',
  'Storage Conditions cleaning supply' => 'Storage Conditions: cleaning supply',
  'Storage Conditions specimen' => 'Storage Conditions: specimen',
  'Storage Conditions study article' => 'Storage Conditions: study article',
  'Training task performed with inadequate training' => 'Training: task performed with inadequate training',
  'Training task performed without training' => 'Training: task performed without training',
  'Controlled Documents Incorrect Document Used' => 'Controlled Documents: Incorrect Document Used',
  'Animal Incorrect Assignment' => 'Animal: Incorrect Assignment',
  'Task Incorrect Scheduling' => 'Task: Incorrect Scheduling',
  'Label Missing' => 'Label Error: Missing Label',
  'Animal Inadequate Enrichment' => 'Animal: Inadequate Enrichment',
  'Clinical Pathology Insufficient Volume' => 'Clinical Pathology: Insufficient Volume',
  'Unsatisfactory Study Article Receipt' => 'Unsatisfactory Study Article Receipt',
  'Missed Task QuarantineIsolation Release' => 'Missed Task: Quarantine/Isolation Release',
  'Procedure Time Interval Missed' => 'Procedure Time Interval Missed',
  'Sterilization Failed Indicator' => 'Sterilization: Failed Indicator',
  'Scheduling Matrix not updated' => 'Scheduling Matrix not updated',
  'Temp OOR not OOS' => 'Temp OOR, not OOS',
  'Humidity OOR not OOS' => 'Humidity OOR, not OOS',
  'Water testing missed retest' => 'Water testing: missed retest',
  'WPO Not Submitted' => 'WPO Not Submitted',
  'Sample Prep Near Miss Prep' => 'Sample Prep Near Miss: Prep',
  'Sample Prep Near Miss Extraction' => 'Sample Prep Near Miss: Extraction',
  'Animal Room Incorrect' => 'Animal Room Incorrect',
  'Sample Prep Error Extract Manipulation' => 'Sample Prep Error: Extract Manipulation',
  'Incomplete or missed prep task' => 'Incomplete or missed prep task',
  'Catheter Concerns' => 'Catheter Concerns',
  'Intubation Concerns' => 'Intubation Concerns',
  'Surgical Site Concerns' => 'Surgical Site Concerns',
  'Animal Housing Inappropriately Group Housed' => 'Animal Housing: Inappropriately Group Housed',
  'Sample Prep Error Loss of Vehicle Glassware' => 'Sample Prep Error: Loss of Vehicle (Glassware)',
  'Sample Prep Error Loss of Vehicle WhirlPak' => 'Sample Prep Error: Loss of Vehicle (WhirlPak)',
  'Associate did not perform before due date' => 'Associate did not perform before due date',
  'Associate Out of Office' => 'Associate Out of Office',
  'Wrong Training assignment to associate' => 'Wrong Training assignment to associate',
  'Missed Task Re Marking' => 'Missed Task: Re-Marking',
  'Sample Prep Error Study article became unsuitable for use' => 'Sample Prep Error: Study article(s) became unsuitable for use',
);
?>
