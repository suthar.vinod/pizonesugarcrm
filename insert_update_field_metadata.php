<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$queryInsert = "INSERT INTO `fields_meta_data` (`id`, `name`, `vname`, `comments`, `help`, `custom_module`, `type`, `len`, `required`, `default_value`, `date_modified`, `deleted`, `audited`, `massupdate`, `duplicate_merge`, `reportable`, `importable`, `ext1`, `ext2`, `ext3`, `ext4`) VALUES
('Accountsiso_certified_c', 'iso_certified_c', 'LBL_ISO_CERTIFIED', NULL, NULL, 'Accounts', 'bool', NULL, 0, '0', '2019-03-13 12:51:01', 0, 1, 0, 0, 1, 'false', NULL, NULL, NULL, NULL),
('EFR_Equipment_Facility_Recorstatus_c', 'status_c', 'LBL_STATUS', NULL, NULL, 'EFR_Equipment_Facility_Recor', 'enum', 100, 1, NULL, '2019-03-12 14:40:19', 0, 1, 1, 1, 1, 'true', 'equipment_and_facilities_status_list', NULL, NULL, NULL),
('RT_Room_Transferroom_c', 'room_c', 'LBL_ROOM', NULL, NULL, 'RT_Room_Transfer', 'relate', 255, 1, NULL, '2019-03-12 13:55:27', 0, 1, 0, 1, 1, 'true', NULL, 'RMS_Room', 'rms_room_id_c', NULL),
('RT_Room_Transferrms_room_id_c', 'rms_room_id_c', 'LBL_ROOM_RMS_ROOM_ID', NULL, NULL, 'RT_Room_Transfer', 'id', 36, 0, NULL, '2019-03-12 13:55:27', 0, 0, 0, 1, 0, 'true', NULL, NULL, NULL, NULL),
('EFS_Equipment_Facility_Servilast_service_date_c', 'last_service_date_c', 'LBL_LAST_SERVICE_DATE', NULL, NULL, 'EFS_Equipment_Facility_Servi', 'date', NULL, 0, NULL, '2019-02-28 12:56:43', 0, 1, 1, 1, 1, 'true', NULL, NULL, NULL, NULL),
('Equip_Equipmentlast_service_date_c', 'last_service_date_c', 'LBL_LAST_SERVICE_DATE', NULL, NULL, 'Equip_Equipment', 'date', NULL, 0, NULL, '2019-02-28 12:54:28', 0, 1, 1, 1, 1, 'true', NULL, NULL, NULL, NULL),
('Equip_Equipmentnext_service_date_c', 'next_service_date_c', 'LBL_NEXT_SERVICE_DATE', NULL, NULL, 'Equip_Equipment', 'date', NULL, 0, NULL, '2019-02-28 12:54:11', 0, 1, 1, 0, 1, 'false', NULL, NULL, NULL, NULL),
('Accountscurrent_vendor_status_c', 'current_vendor_status_c', 'LBL_CURRENT_VENDOR_STATUS', NULL, NULL, 'Accounts', 'enum', 100, 1, NULL, '2019-02-26 21:07:27', 0, 1, 1, 1, 1, 'true', 'current_vendor_status_list', NULL, NULL, NULL),
('Accountsmethod_of_qualification_c', 'method_of_qualification_c', 'LBL_METHOD_OF_QUALIFICATION', NULL, NULL, 'Accounts', 'enum', 100, 1, NULL, '2019-02-26 21:06:38', 0, 1, 1, 1, 1, 'true', 'method_of_qualification_list', NULL, NULL, NULL),
('Accountsvendor_level_c', 'vendor_level_c', 'LBL_VENDOR_LEVEL', NULL, NULL, 'Accounts', 'enum', 100, 1, NULL, '2019-02-26 21:05:05', 0, 1, 1, 1, 1, 'true', 'vendor_level_list', NULL, NULL, NULL),
('Accountsvendor_type_c', 'vendor_type_c', 'LBL_VENDOR_TYPE', NULL, NULL, 'Accounts', 'multienum', 100, 1, '^^', '2019-02-26 21:04:16', 0, 1, 1, 1, 1, 'true', 'vendor_type_list', NULL, NULL, NULL),
('Accountspayment_terms_2_c', 'payment_terms_2_c', 'LBL_PAYMENT_TERMS_2', NULL, NULL, 'Accounts', 'enum', 100, 1, NULL, '2019-02-26 21:03:05', 0, 1, 1, 1, 1, 'true', 'payment_terms_list', NULL, NULL, NULL),
('Accountscategory_c', 'category_c', 'LBL_CATEGORY', NULL, NULL, 'Accounts', 'enum', 100, 1, NULL, '2019-02-26 20:59:02', 0, 1, 1, 1, 1, 'required', 'company_category_list', NULL, NULL, NULL),
('EFR_Equipment_Facility_Recortype_2_c', 'type_2_c', 'LBL_TYPE_2', NULL, NULL, 'EFR_Equipment_Facility_Recor', 'multienum', 100, 1, '^^', '2019-02-25 15:38:43', 0, 1, 1, 1, 1, 'true', 'equipment_and_facility_records_type_list', NULL, NULL, 'a:2:{s\:7:\"default\"\;s:2:\"^^\"\;s:10:\"dependency\"\;s:0:\"\"\;}'),
('EFS_Equipment_Facility_Servinext_service_date_c', 'next_service_date_c', 'LBL_NEXT_SERVICE_DATE', NULL, NULL, 'EFS_Equipment_Facility_Servi', 'date', NULL, 1, NULL, '2019-02-25 15:15:16', 0, 1, 0, 0, 1, 'false', NULL, NULL, NULL, NULL),
('EFS_Equipment_Facility_Serviservice_interval_days_c', 'service_interval_days_c', 'LBL_SERVICE_INTERVAL_DAYS', NULL, NULL, 'EFS_Equipment_Facility_Servi', 'int', 4, 1, NULL, '2019-02-25 15:13:13', 0, 1, 0, 1, 1, 'true', NULL, NULL, NULL, NULL),
('EFS_Equipment_Facility_Servicategory_c', 'category_c', 'LBL_CATEGORY', NULL, NULL, 'EFS_Equipment_Facility_Servi', 'enum', 100, 1, NULL, '2019-02-25 15:11:31', 0, 1, 1, 1, 1, 'true', 'equipment_and_facility_records_type_list', NULL, NULL, NULL),
('Equip_Equipmentcurrent_status_c', 'current_status_c', 'LBL_CURRENT_STATUS', NULL, NULL, 'Equip_Equipment', 'enum', 100, 1, NULL, '2019-02-25 14:07:25', 0, 1, 1, 1, 1, 'true', 'equipment_and_facilities_status_list', NULL, NULL, NULL),
('Equip_Equipmentroom_c', 'room_c', 'LBL_ROOM', NULL, NULL, 'Equip_Equipment', 'relate', 255, 0, NULL, '2019-02-22 15:56:23', 0, 1, 0, 1, 1, 'true', NULL, 'RMS_Room', 'rms_room_id_c', NULL),
('Equip_Equipmentrms_room_id_c', 'rms_room_id_c', 'LBL_ROOM_RMS_ROOM_ID', NULL, NULL, 'Equip_Equipment', 'id', 36, 0, NULL, '2019-02-22 15:56:23', 0, 0, 0, 1, 0, 'true', NULL, NULL, NULL, NULL),
('M06_Errorsubtype_c', 'subtype_c', 'LBL_SUBTYPE', NULL, NULL, 'M06_Error', 'multienum', 100, 0, NULL, '2019-02-21 20:16:30', 0, 0, 1, 1, 1, 'true', 'error_type_list', NULL, NULL, NULL),
('RMS_Roomroom_type_c', 'room_type_c', 'LBL_ROOM_TYPE', NULL, NULL, 'RMS_Room', 'enum', 100, 0, NULL, '2019-02-21 19:36:02', 0, 1, 1, 1, 1, 'true', 'room_type_list', NULL, NULL, NULL),
('RMS_Roomroom_number_c', 'room_number_c', 'LBL_ROOM_NUMBER', NULL, NULL, 'RMS_Room', 'int', 2, 1, NULL, '2019-02-21 19:33:43', 0, 1, 0, 1, 1, 'true', NULL, NULL, NULL, NULL),
('RMS_Roomcapacity_c', 'capacity_c', 'LBL_CAPACITY', NULL, NULL, 'RMS_Room', 'varchar', 255, 0, NULL, '2019-02-21 19:33:23', 0, 1, 0, 1, 1, 'true', NULL, NULL, NULL, NULL),
('RMS_Roombuilding_c', 'building_c', 'LBL_BUILDING', NULL, NULL, 'RMS_Room', 'enum', 100, 1, NULL, '2019-02-21 19:32:42', 0, 1, 1, 1, 1, 'true', 'location_list', NULL, NULL, NULL),
('M06_Errorresolution_c', 'resolution_c', 'LBL_RESOLUTION', NULL, NULL, 'M06_Error', 'text', NULL, 1, NULL, '2019-02-21 18:41:22', 0, 1, 0, 1, 1, 'true', NULL, '4', '20', NULL),
('M06_Errorreinspection_date_c', 'reinspection_date_c', 'LBL_REINSPECTION_DATE', NULL, NULL, 'M06_Error', 'date', NULL, 0, NULL, '2019-02-21 18:35:28', 0, 0, 1, 1, 1, 'true', NULL, NULL, NULL, NULL),
('M06_Errortarget_reinspection_date_c', 'target_reinspection_date_c', 'LBL_TARGET_REINSPECTION_DATE', NULL, NULL, 'M06_Error', 'date', NULL, 0, NULL, '2019-02-21 18:34:27', 0, 0, 0, 0, 1, 'false', NULL, NULL, NULL, NULL),
('M06_Errornotes_c', 'notes_c', 'LBL_NOTES', NULL, NULL, 'M06_Error', 'text', NULL, 0, NULL, '2019-02-21 18:30:03', 0, 1, 0, 1, 1, 'true', NULL, '4', '20', NULL),
('M06_Errorrecommendation_c', 'recommendation_c', 'LBL_RECOMMENDATION', NULL, NULL, 'M06_Error', 'text', NULL, 0, NULL, '2019-02-21 13:25:37', 0, 1, 0, 1, 1, 'true', NULL, '4', '20', NULL),
('M06_Errordepartment_c', 'department_c', 'LBL_DEPARTMENT', NULL, NULL, 'M06_Error', 'multienum', 100, 0, NULL, '2019-02-21 13:23:25', 0, 1, 1, 1, 1, 'true', 'department_list', NULL, NULL, 'a:1:{s:10:\"dependency\"\;s:0:\"\"\;}'),
('M06_Errordate_time_discovered_c', 'date_time_discovered_c', 'LBL_DATE_TIME_DISCOVERED', NULL, NULL, 'M06_Error', 'datetimecombo', NULL, 0, NULL, '2019-02-21 12:49:29', 0, 0, 1, 1, 1, 'true', NULL, NULL, NULL, NULL),
('Contactsmanager_c', 'manager_c', 'LBL_MANAGER', NULL, NULL, 'Contacts', 'relate', 255, 0, NULL, '2019-02-20 19:40:44', 0, 1, 0, 1, 1, 'true', NULL, 'Contacts', 'contact_id_c', NULL),
('Contactscontact_id_c', 'contact_id_c', 'LBL_MANAGER_CONTACT_ID', NULL, NULL, 'Contacts', 'id', 36, 0, NULL, '2019-02-20 19:40:44', 0, 0, 0, 1, 0, 'true', NULL, NULL, NULL, NULL),
('Contactsinitials_c', 'initials_c', 'LBL_INITIALS', NULL, NULL, 'Contacts', 'varchar', 4, 0, NULL, '2019-02-20 19:40:02', 0, 1, 0, 1, 1, 'true', NULL, NULL, NULL, NULL),
('Contactsdepartment_id_c', 'department_id_c', 'LBL_DEPARTMENT_ID', NULL, NULL, 'Contacts', 'enum', 100, 0, NULL, '2019-02-20 19:38:43', 0, 0, 1, 1, 1, 'true', 'department_list', NULL, NULL, NULL),
('Contactscurrent_title_c', 'current_title_c', 'LBL_CURRENT_TITLE', NULL, NULL, 'Contacts', 'varchar', 255, 0, NULL, '2019-02-20 19:25:05', 0, 0, 0, 1, 1, 'true', NULL, NULL, NULL, NULL),
('Accountsnext_approval_due_date_c', 'next_approval_due_date_c', 'LBL_NEXT_APPROVAL_DUE_DATE', NULL, NULL, 'Accounts', 'date', NULL, 1, NULL, '2019-02-20 18:23:13', 0, 1, 0, 0, 1, 'false', NULL, NULL, NULL, NULL),
('Accountsapproval_interval_days_c', 'approval_interval_days_c', 'LBL_APPROVAL_INTERVAL_DAYS', NULL, NULL, 'Accounts', 'int', 11, 1, NULL, '2019-02-20 18:20:51', 0, 1, 0, 1, 1, 'true', NULL, NULL, NULL, NULL),
('Accountsapproval_date_c', 'approval_date_c', 'LBL_APPROVAL_DATE', NULL, NULL, 'Accounts', 'date', NULL, 1, NULL, '2019-02-20 18:19:26', 0, 1, 1, 1, 1, 'true', NULL, NULL, NULL, NULL),
('Equip_Equipmentnotes_c', 'notes_c', 'LBL_NOTES', NULL, NULL, 'Equip_Equipment', 'text', NULL, 0, NULL, '2019-02-20 15:00:33', 0, 1, 0, 1, 1, 'true', NULL, '4', '20', NULL),
('Equip_Equipmentiso_17025_units_of_measure_c', 'iso_17025_units_of_measure_c', 'LBL_ISO_17025_UNITS_OF_MEASURE', NULL, NULL, 'Equip_Equipment', 'multienum', 100, 0, '^^', '2019-02-20 14:59:33', 0, 1, 1, 1, 1, 'true', 'iso_17025_units_of_measure_list', NULL, NULL, 'a:2:{s:7:\"default\"\;s:2:\"^^\"\;s:10:\"dependency\"\;s:0:\"\"\;}'),
('Equip_Equipmentiso_17025_certification_c', 'iso_17025_certification_c', 'LBL_ISO_17025_CERTIFICATION_REQUIRED', NULL, NULL, 'Equip_Equipment', 'enum', 100, 1, 'Choose one...', '2019-02-20 14:55:37', 0, 1, 1, 1, 1, 'true', 'Yes_No', NULL, NULL, NULL),
('Equip_Equipmentbuilding_c', 'building_c', 'LBL_BUILDING', NULL, NULL, 'Equip_Equipment', 'enum', 100, 1, NULL, '2019-02-20 14:47:38', 0, 1, 1, 1, 1, 'true', 'location_list', NULL, NULL, NULL),
('Equip_Equipmentapplicable_form_c', 'applicable_form_c', 'LBL_APPLICABLE_FORM', NULL, NULL, 'Equip_Equipment', 'relate', 255, 0, NULL, '2019-02-20 14:46:19', 0, 1, 0, 1, 1, 'true', NULL, 'Erd_Error_Documents', 'erd_error_documents_id1_c', NULL),
('Equip_Equipmenterd_error_documents_id1_c', 'erd_error_documents_id1_c', 'LBL_APPLICABLE_FORM_ERD_ERROR_DOCUMENTS_ID', NULL, NULL, 'Equip_Equipment', 'id', 36, 0, NULL, '2019-02-20 14:46:19', 0, 0, 0, 1, 0, 'true', NULL, NULL, NULL, NULL),
('Equip_Equipmentapplicable_sop_c', 'applicable_sop_c', 'LBL_APPLICABLE_SOP', NULL, NULL, 'Equip_Equipment', 'relate', 255, 0, NULL, '2019-02-20 14:41:55', 0, 1, 0, 1, 1, 'true', NULL, 'Erd_Error_Documents', 'erd_error_documents_id_c', NULL),
('Equip_Equipmenterd_error_documents_id_c', 'erd_error_documents_id_c', 'LBL_APPLICABLE_SOP_ERD_ERROR_DOCUMENTS_ID', NULL, NULL, 'Equip_Equipment', 'id', 36, 0, NULL, '2019-02-20 14:41:55', 0, 0, 0, 1, 0, 'true', NULL, NULL, NULL, NULL),
('Equip_Equipmentbinder_location_c', 'binder_location_c', 'LBL_BINDER_LOCATION', NULL, NULL, 'Equip_Equipment', 'enum', 100, 1, NULL, '2019-02-20 14:41:03', 0, 1, 1, 1, 1, 'true', 'binder_location_list', NULL, NULL, NULL),
('Equip_Equipmentdepartment_c', 'department_c', 'LBL_DEPARTMENT', NULL, NULL, 'Equip_Equipment', 'enum', 100, 1, 'Blank', '2019-02-20 14:35:19', 0, 1, 1, 1, 1, 'true', 'department_list', NULL, NULL, NULL),
('Equip_Equipmentcurrent_software_firmware_c', 'current_software_firmware_c', 'LBL_CURRENT_SOFTWARE_FIRMWARE', NULL, NULL, 'Equip_Equipment', 'varchar', 255, 0, NULL, '2019-02-20 14:23:10', 0, 1, 0, 1, 1, 'true', NULL, NULL, NULL, NULL),
('Equip_Equipmentmodel_c', 'model_c', 'LBL_MODEL', NULL, NULL, 'Equip_Equipment', 'varchar', 255, 0, NULL, '2019-02-20 14:21:43', 0, 1, 0, 1, 1, 'true', NULL, NULL, NULL, NULL),
('Equip_Equipmentaccount_id1_c', 'account_id1_c', 'LBL_MANUFACTURER_ACCOUNT_ID', NULL, NULL, 'Equip_Equipment', 'id', 36, 0, NULL, '2019-02-20 14:20:57', 0, 0, 0, 1, 0, 'true', NULL, NULL, NULL, NULL),
('Equip_Equipmentmanufacturer_c', 'manufacturer_c', 'LBL_MANUFACTURER', NULL, NULL, 'Equip_Equipment', 'relate', 255, 0, NULL, '2019-02-20 14:20:57', 0, 1, 0, 1, 1, 'true', NULL, 'Accounts', 'account_id1_c', NULL),
('Equip_Equipmentclassification_c', 'classification_c', 'LBL_CLASSIFICATION', NULL, NULL, 'Equip_Equipment', 'enum', 100, 1, NULL, '2019-02-20 14:16:09', 0, 1, 1, 1, 1, 'true', 'classification_list', NULL, NULL, NULL),
('Equip_Equipmentowner_c', 'owner_c', 'LBL_OWNER', NULL, NULL, 'Equip_Equipment', 'relate', 255, 1, NULL, '2019-02-20 14:14:06', 0, 1, 0, 1, 1, 'true', NULL, 'Accounts', 'account_id_c', NULL),
('Equip_Equipmentaccount_id_c', 'account_id_c', 'LBL_OWNER_ACCOUNT_ID', NULL, NULL, 'Equip_Equipment', 'id', 36, 0, NULL, '2019-02-20 14:14:06', 0, 0, 0, 1, 0, 'true', NULL, NULL, NULL, NULL),
('CD_Company_Documentsdate_of_qualification_c', 'date_of_qualification_c', 'LBL_DATE_OF_QUALIFICATION', NULL, NULL, 'CD_Company_Documents', 'date', NULL, 1, NULL, '2019-02-20 13:57:51', 0, 1, 1, 1, 1, 'true', NULL, NULL, NULL, NULL),
('CD_Company_Documentsmethod_of_qualification_c', 'method_of_qualification_c', 'LBL_METHOD_OF_QUALIFICATION', NULL, NULL, 'CD_Company_Documents', 'enum', 100, 1, NULL, '2019-02-20 13:50:00', 0, 1, 1, 1, 1, 'true', 'method_of_qualification_list', NULL, NULL, NULL),
('CD_Company_Documentsvendor_status_c', 'vendor_status_c', 'LBL_VENDOR_STATUS', NULL, NULL, 'CD_Company_Documents', 'enum', 100, 1, NULL, '2019-02-20 13:43:27', 0, 1, 1, 1, 1, 'true', 'vendor_status_list', NULL, NULL, NULL),
('RT_Room_Transferreason_for_transfer_c', 'reason_for_transfer_c', 'LBL_REASON_FOR_TRANSFER', NULL, NULL, 'RT_Room_Transfer', 'enum', 100, 0, 'Consolidation', '2019-02-20 13:16:02', 0, 0, 1, 1, 1, 'true', 'reason_for_transfer_list', NULL, NULL, NULL),
('RT_Room_Transfertransfer_date_c', 'transfer_date_c', 'LBL_TRANSFER_DATE', NULL, NULL, 'RT_Room_Transfer', 'date', NULL, 1, 'now', '2019-02-20 13:12:59', 0, 1, 1, 1, 1, 'true', NULL, NULL, NULL, NULL),
('EFD_Equipment_Facility_Doctype_2_c', 'type_2_c', 'LBL_TYPE_2', NULL, NULL, 'EFD_Equipment_Facility_Doc', 'enum', 100, 1, NULL, '2019-02-19 17:42:13', 0, 0, 1, 1, 1, 'true', 'equipment_and_facility_document_type_list', NULL, NULL, NULL),
('EFR_Equipment_Facility_Recorout_of_service_date_c', 'out_of_service_date_c', 'LBL_OUT_OF_SERVICE_DATE', NULL, NULL, 'EFR_Equipment_Facility_Recor', 'date', NULL, 0, NULL, '2019-02-19 17:30:14', 0, 1, 1, 1, 1, 'true', NULL, NULL, NULL, NULL),
('EFR_Equipment_Facility_Recorsoftware_firmware_c', 'software_firmware_c', 'LBL_SOFTWARE_FIRMWARE', NULL, NULL, 'EFR_Equipment_Facility_Recor', 'varchar', 255, 0, NULL, '2019-02-19 17:28:21', 0, 1, 0, 1, 1, 'true', NULL, NULL, NULL, NULL),
('EFR_Equipment_Facility_Recorservice_date_c', 'service_date_c', 'LBL_SERVICE_DATE', NULL, NULL, 'EFR_Equipment_Facility_Recor', 'date', NULL, 1, NULL, '2019-02-19 17:15:22', 0, 1, 1, 1, 1, 'true', NULL, NULL, NULL, NULL),
('EFR_EquipmentFacilityRecordnotes_c', 'notes_c', 'LBL_NOTES', NULL, NULL, 'EFR_EquipmentFacilityRecord', 'text', NULL, 0, NULL, '2019-02-19 15:08:21', 0, 1, 0, 1, 1, 'true', NULL, '4', '20', NULL),
('EFR_EquipmentFacilityRecordout_of_service_date_c', 'out_of_service_date_c', 'LBL_OUT_OF_SERVICE_DATE', NULL, NULL, 'EFR_EquipmentFacilityRecord', 'date', NULL, 0, NULL, '2019-02-19 15:07:19', 0, 1, 1, 1, 1, 'true', NULL, NULL, NULL, NULL),
('EFR_EquipmentFacilityRecordsoftware_firmware_c', 'software_firmware_c', 'LBL_SOFTWARE_FIRMWARE', NULL, NULL, 'EFR_EquipmentFacilityRecord', 'varchar', 255, 0, NULL, '2019-02-19 15:06:30', 0, 1, 0, 1, 1, 'true', NULL, NULL, NULL, NULL),
('EFR_EquipmentFacilityRecordtype_2_c', 'type_2_c', 'LBL_TYPE_2', NULL, NULL, 'EFR_EquipmentFacilityRecord', 'multienum', 100, 1, '^^', '2019-02-19 15:05:38', 0, 1, 1, 1, 1, 'true', 'equipment_and_facility_records_type_list', NULL, NULL, 'a:2:{s:7:\"default\"\;s:2:\"^^\"\;s:10:\"dependency\"\;s:0:\"\"\;}'),
('EFR_EquipmentFacilityRecordstatus_c', 'status_c', 'LBL_STATUS', NULL, NULL, 'EFR_EquipmentFacilityRecord', 'enum', 100, 1, NULL, '2019-02-19 14:46:33', 0, 1, 1, 1, 1, 'true', 'equipment_and_facilities_status_list', NULL, NULL, NULL),
('EFR_EquipmentFacilityRecordservice_date_c', 'service_date_c', 'LBL_SERVICE_DATE', NULL, NULL, 'EFR_EquipmentFacilityRecord', 'date', NULL, 1, NULL, '2019-02-19 14:43:28', 0, 1, 1, 1, 1, 'true', NULL, NULL, NULL, NULL);
";

if (!$db->query($queryInsert)) {
    $message1 = "Insert failed at some point";
    echo "<script type='text/javascript'>alert('$message1');</script>";
    exit(1);
}
$query_array = array();

$queryUpdate1 = "UPDATE fields_meta_data f1 JOIN fields_meta_data f2 "
        . "ON f1.id = 'Accountscompany_status_c' AND f2.id = 'Accountsconnection_source_c' "
        . "SET f1.required = 1 , f1.default_value = NULL , f1.audited = 1, "
        . "f2.audited = 1 ";
$query_array[1] = $queryUpdate1;

$queryUpdate2 = "UPDATE fields_meta_data SET ext1 = 'category_list' "
        . "WHERE id = 'M06_Errorerror_type_c' ";
$query_array[2] = $queryUpdate2;

$queryUpdate3 = "UPDATE fields_meta_data SET default_value = 'Choose One' "
        . "WHERE id = 'M06_Errorerror_classification_c' ";
$query_array[3] = $queryUpdate3;

$queryUpdate4 = "UPDATE fields_meta_data SET ext1 = 'error_category_list' "
        . "WHERE id = 'M06_Errorerror_category_c' ";
$query_array[4] = $queryUpdate4;

$queryUpdate5 = "UPDATE fields_meta_data SET audited =1 "
        . "WHERE id = 'M06_Errorerror_category_c' ";
$query_array[5] = $queryUpdate5;

$queryUpdate6 = "UPDATE fields_meta_data SET ext1 = 'Yes_No' "
        . "WHERE id = 'Contactsportal_account_activated_c' ";
$query_array[6] = $queryUpdate6;

$queryUpdate7 = "UPDATE fields_meta_data SET audited = 1 "
        . "WHERE id = 'Accountscompany_notes_c' ";
$query_array[7] = $queryUpdate7;

$queryUpdate8 = "UPDATE fields_meta_data SET required = 1, "
        . "audited = 1 "
        . "WHERE id = 'Accountsbiocomp_pricelist_c' ";
$query_array[8] = $queryUpdate8;

$queryUpdate9 = "UPDATE fields_meta_data SET required = 1, "
        . "audited = 1 "
        . "WHERE id = 'Accountstradeshow_name_c' ";
$query_array[9] = $queryUpdate9;

$queryUpdate10 = "UPDATE fields_meta_data SET required = 1, "
        . "audited = 1 , duplicate_merge = 1 "
        . "WHERE id = 'Accountsreferred_by_c' ";
$query_array[10] = $queryUpdate10;

$queryUpdate11 = "UPDATE fields_meta_data SET required = 1, "
        . "audited = 1 , duplicate_merge = 1 "
        . "WHERE id = 'Accountsreferred_by_c' ";
$query_array[11] = $queryUpdate11;

$queryUpdate12 = "UPDATE fields_meta_data SET audited = 1 "
        . "WHERE id = 'Equip_Equipmentserial_number_c' ";
$query_array[12] = $queryUpdate12;

foreach ($query_array as $value) {
   
    if (!$db->query($value)) {
        $message2 = "Updation failed for query" . $value;
        echo "<script type='text/javascript'>alert('$message2');</script>";
        exit(1);
    }
}

$message3 = "Script executed successfully";
echo "<script type='text/javascript'>alert('$message3');</script>";
