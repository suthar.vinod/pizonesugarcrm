<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$result = $db->query("SELECT id_c FROM equip_equipment_cstm");
if ($result) {
    while ($row = $db->fetchByAssoc($result)) {
        $id = $row['id_c'];
         $queryForNextServiceDate = "SELECT min(efs_cstm.next_service_date_c) AS next_service_date_c FROM equip_equipment ee "
                    . "INNER JOIN equip_equipment_efs_equipment_facility_servi_1_c eefs ON ee.id = eefs.equip_equia9d9uipment_ida "
                    . "INNER JOIN efs_equipment_facility_servi efs ON eefs.equip_equi3f6dy_servi_idb = efs.id "
                    . "INNER JOIN  efs_equipment_facility_servi_cstm efs_cstm ON efs.id = efs_cstm.id_c "
                    . "WHERE ee.deleted = 0 AND eefs.deleted = 0 AND efs.deleted = 0 AND ee.id = '" . $id . "'";

          $result1 = $db->query($queryForNextServiceDate);
          if ($row = $db->fetchByAssoc($result1)) {
               
                $next_service_date = $row['next_service_date_c'];  
                $query = "UPDATE equip_equipment_cstm SET next_service_date_c = '" . $next_service_date . "' WHERE id_c='" . $id . "'";
               
                $db->query($query);
            }
    }
    echo "Next service date in Equip & Facility module updated successfully!";
  
} else {
    echo "Table not found";
}

