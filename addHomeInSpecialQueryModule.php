<?php

require_once 'modules/Configurator/Configurator.php';
$configuratorObj = new Configurator();
// //Load config
$configuratorObj->loadConfig();

if (isset($configuratorObj->config['resource_management']['special_query_modules'])) {
    $configuratorObj->config['resource_management']['special_query_modules'][] = 'Home';
    $configuratorObj->saveConfig();
    echo "Home added in resource management successfully";
} else {
    echo "Home module not added in resource management";
}
