<?php
$viewdefs['M03_Work_Product'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'M03_Work_Product',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'functional_area_c',
                'label' => 'LBL_FUNCTIONAL_AREA',
              ),
              1 => 
              array (
                'name' => 'work_product_compliance_c',
                'label' => 'LBL_WORK_PRODUCT_COMPLIANCE',
              ),
              2 => 
              array (
                'name' => 'title_description_c',
                'label' => 'LBL_TITLE_DESCRIPTION',
              ),
              3 => 
              array (
                'name' => 'work_product_code_2_c',
                'studio' => 'visible',
                'label' => 'LBL_WORK_PRODUCT_CODE_2',
              ),
              4 => 
              array (
                'name' => 'analytical_deliverables_c',
                'label' => 'LBL_ANALYTICAL_DELIVERABLES',
              ),
              5 => 
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
              ),
              6 => 
              array (
                'name' => 'test_system_c',
                'label' => 'LBL_TEST_SYSTEM',
              ),
              7 => 
              array (
                'name' => 'total_animal_numbers_c',
                'label' => 'LBL_TOTAL_ANIMAL_NUMBERS',
              ),
              8 => 
              array (
                'name' => 'test_system_description_c',
                'label' => 'LBL_TEST_SYSTEM_DESCRIPTION',
                'span' => 12,
              ),
              9 => 
              array (
                'name' => 'test_article_description_c',
                'label' => 'LBL_TEST_ARTICLE_DESCRIPTION',
                'span' => 12,
              ),
              10 => 
              array (
                'name' => 'work_product_id_sponsor_c',
                'label' => 'LBL_WORK_PRODUCT_ID_SPONSOR',
              ),
              11 => 'assigned_user_name',
              12 => 
              array (
                'name' => 'm01_sales_m03_work_product_1_name',
                'label' => 'LBL_M01_SALES_M03_WORK_PRODUCT_1_FROM_M01_SALES_TITLE',
              ),
              13 => 
              array (
                'name' => 'blanket_protocol_c',
                'studio' => 'visible',
                'label' => 'LBL_BLANKET_PROTOCOL',
              ),
              14 => 
              array (
                'name' => 'master_schedule_company_name_c',
                'label' => 'LBL_MASTER_SCHEDULE_COMPANY_NAME',
              ),
              15 => 
              array (
                'name' => 'master_schedule_sd_c',
                'label' => 'LBL_MASTER_SCHEDULE_SD',
              ),
              16 => 
              array (
                'name' => 'study_director_initiation_c',
                'label' => 'LBL_STUDY_DIRECTOR_INITIATION',
              ),
              17 => 
              array (
                'name' => 'work_product_status_c',
                'label' => 'LBL_WORK_PRODUCT_STATUS',
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'accounts_m03_work_product_1_name',
                'label' => 'LBL_ACCOUNTS_M03_WORK_PRODUCT_1_FROM_ACCOUNTS_TITLE',
              ),
              1 => 
              array (
                'name' => 'contacts_m03_work_product_1_name',
                'label' => 'LBL_CONTACTS_M03_WORK_PRODUCT_1_FROM_CONTACTS_TITLE',
              ),
              2 => 
              array (
                'name' => 'company_status_c',
                'label' => 'LBL_COMPANY_STATUS',
              ),
              3 => 
              array (
                'name' => 'study_director_script_c',
                'studio' => 'visible',
                'label' => 'LBL_STUDY_DIRECTOR_SCRIPT',
              ),
              4 => 
              array (
                'name' => 'analytical_pi_c',
                'studio' => 'visible',
                'label' => 'LBL_ANALYTICAL_PI',
              ),
              5 => 
              array (
              ),
              6 => 
              array (
                'name' => 'aps_principal_investigator_r_c',
                'studio' => 'visible',
                'label' => 'LBL_APS_PRINCIPAL_INVESTIGATOR_R',
              ),
              7 => 
              array (
                'name' => 'lead_auditor_relate_c',
                'studio' => 'visible',
                'label' => 'LBL_LEAD_AUDITOR_RELATE',
              ),
              8 => 
              array (
                'name' => 'sc_requirement_c',
                'label' => 'LBL_SC_REQUIREMENT',
              ),
              9 => 
              array (
                'name' => 'study_coordinator_relate_c',
                'studio' => 'visible',
                'label' => 'LBL_STUDY_COORDINATOR_RELATE',
              ),
              10 => 
              array (
                'name' => 'primary_aps_operator_relate_c',
                'studio' => 'visible',
                'label' => 'LBL_PRIMARY_APS_OPERATOR_RELATE',
                'span' => 12,
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'first_procedure_c',
                'label' => 'LBL_FIRST_PROCEDURE',
              ),
              1 => 
              array (
                'name' => 'last_procedure_c',
                'label' => 'LBL_LAST_PROCEDURE',
              ),
              2 => 
              array (
                'readonly' => true,
                'name' => 'study_duration_c',
                'label' => 'LBL_STUDY_DURATION',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'name' => 'bc_spa_reconciled_date_c',
                'label' => 'LBL_BC_SPA_RECONCILED_DATE',
              ),
              5 => 
              array (
              ),
              6 => 
              array (
                'name' => 'final_report_timeline_c',
                'label' => 'LBL_FINAL_REPORT_TIMELINE',
              ),
              7 => 
              array (
                'name' => 'final_report_timeline_type_c',
                'label' => 'LBL_FINAL_REPORT_TIMELINE_TYPE',
              ),
              8 => 
              array (
                'name' => 'study_outcome_type_c',
                'label' => 'LBL_STUDY_OUTCOME_TYPE',
              ),
              9 => 
              array (
                'name' => 'expanded_study_result_c',
                'label' => 'LBL_EXPANDED_STUDY_RESULT',
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'iacuc_protocol_status_c',
                'label' => 'LBL_IACUC_PROTOCOL_STATUS',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'iacuc_approval_date_c',
                'label' => 'LBL_IACUC_APPROVAL_DATE',
              ),
              2 => 
              array (
                'name' => 'iacuc_closure_date_c',
                'label' => 'LBL_IACUC_CLOSURE_DATE',
              ),
            ),
          ),
          5 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL8',
            'label' => 'LBL_RECORDVIEW_PANEL8',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'iacuc_first_annual_review_c',
                'label' => 'LBL_IACUC_FIRST_ANNUAL_REVIEW',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'first_annual_review_c',
                'label' => 'LBL_FIRST_ANNUAL_REVIEW',
              ),
              2 => 
              array (
                'name' => 'first_annual_review_complete_c',
                'label' => 'LBL_FIRST_ANNUAL_REVIEW_COMPLETE',
              ),
              3 => 
              array (
                'name' => 'iacuc_second_annual_review_c',
                'label' => 'LBL_IACUC_SECOND_ANNUAL_REVIEW',
                'span' => 12,
              ),
              4 => 
              array (
                'name' => 'second_annual_review_c',
                'label' => 'LBL_SECOND_ANNUAL_REVIEW',
              ),
              5 => 
              array (
                'name' => 'second_annual_review_complet_c',
                'label' => 'LBL_SECOND_ANNUAL_REVIEW_COMPLET',
              ),
              6 => 
              array (
                'name' => 'iacuc_triennial_review_c',
                'label' => 'LBL_IACUC_TRIENNIAL_REVIEW',
                'span' => 12,
              ),
              7 => 
              array (
                'name' => 'triennial_review_c',
                'label' => 'LBL_TRIENNIAL_REVIEW',
              ),
              8 => 
              array (
                'name' => 'triennial_review_completed_c',
                'label' => 'LBL_TRIENNIAL_REVIEW_COMPLETED',
              ),
            ),
          ),
          6 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL9',
            'label' => 'LBL_RECORDVIEW_PANEL9',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'usda_category_multiselect_c',
                'label' => 'LBL_USDA_CATEGORY_MULTISELECT',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'initial_animals_approved_pri_c',
                'label' => 'LBL_INITIAL_ANIMALS_APPROVED_PRI',
              ),
              2 => 
              array (
                'name' => 'initial_animals_approved_bac_c',
                'label' => 'LBL_INITIAL_ANIMALS_APPROVED_BAC',
              ),
              3 => 
              array (
                'name' => 'animals_added_primary_c',
                'label' => 'LBL_ANIMALS_ADDED_PRIMARY',
              ),
              4 => 
              array (
                'name' => 'animals_added_backup_c',
                'label' => 'LBL_ANIMALS_ADDED_BACKUP',
              ),
              5 => 
              array (
                'name' => 'total_animals_used_primary_c',
                'label' => 'LBL_TOTAL_ANIMALS_USED_PRIMARY',
              ),
              6 => 
              array (
                'name' => 'total_animals_used_backup_c',
                'label' => 'LBL_TOTAL_ANIMALS_USED_BACKUP',
              ),
              7 => 
              array (
                'name' => 'primary_animals_countdown_c',
                'label' => 'LBL_PRIMARY_ANIMALS_COUNTDOWN_C',
              ),
              8 => 
              array (
                'name' => 'back_up_animals_countdown_c',
                'label' => 'LBL_BACK_UP_ANIMALS_COUNTDOWN_C',
              ),
              9 => 
              array (
                'name' => 'unspecified_sex_c',
                'label' => 'LBL_UNSPECIFIED_SEX',
                'span' => 12,
              ),
              10 => 
              array (
                'name' => 'initial_males_c',
                'label' => 'LBL_INITIAL_MALES',
              ),
              11 => 
              array (
                'name' => 'initial_females_c',
                'label' => 'LBL_INITIAL_FEMALES',
              ),
              12 => 
              array (
                'name' => 'added_males_c',
                'label' => 'LBL_ADDED_MALES',
              ),
              13 => 
              array (
                'name' => 'added_females_c',
                'label' => 'LBL_ADDED_FEMALES',
              ),
              14 => 
              array (
                'name' => 'total_males_c',
                'label' => 'LBL_TOTAL_MALES',
              ),
              15 => 
              array (
                'name' => 'total_females_c',
                'label' => 'LBL_TOTAL_FEMALES',
              ),
              16 => 
              array (
                'name' => 'initial_weight_range_c',
                'label' => 'LBL_INITIAL_WEIGHT_RANGE',
              ),
              17 => 
              array (
                'name' => 'weight_range_changed_to_c',
                'label' => 'LBL_WEIGHT_RANGE_CHANGED_TO',
              ),
              18 => 
              array (
                'name' => 'target_animals_c',
                'label' => 'LBL_TARGET_ANIMALS',
              ),
              19 => 
              array (
              ),
              20 => 
              array (
                'name' => 'see_amendment_numbers_c',
                'studio' => 'visible',
                'label' => 'LBL_SEE_AMENDMENT_NUMBERS',
                'span' => 12,
              ),
            ),
          ),
          7 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL10',
            'label' => 'LBL_RECORDVIEW_PANEL10',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'no_exemptions_c',
                'label' => 'LBL_NO_EXEMPTIONS',
              ),
              1 => 
              array (
                'name' => 'aaalac_and_usda_exemptions_c',
                'label' => 'LBL_AAALAC_AND_USDA_EXEMPTIONS',
              ),
              2 => 
              array (
                'name' => 'hazardous_agents_c',
                'label' => 'LBL_HAZARDOUS_AGENTS',
                'span' => 12,
              ),
              3 => 
              array (
                'name' => 'hazardous_substance_c',
                'label' => 'LBL_HAZARDOUS_SUBSTANCE',
                'span' => 12,
              ),
              4 => 
              array (
                'name' => 'exemption_notes_c',
                'studio' => 'visible',
                'label' => 'LBL_EXEMPTION_NOTES',
                'span' => 12,
              ),
            ),
          ),
          8 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL11',
            'label' => 'LBL_RECORDVIEW_PANEL11',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'iacuc_notes_c',
                'studio' => 'visible',
                'label' => 'LBL_IACUC_NOTES',
                'span' => 12,
              ),
            ),
          ),
          9 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL7',
            'label' => 'LBL_RECORDVIEW_PANEL7',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'aps_study_id_c',
                'label' => 'LBL_APS_STUDY_ID',
              ),
              1 => 
              array (
                'name' => 'analyte_c',
                'label' => 'LBL_ANALYTE',
              ),
              2 => 
              array (
                'name' => 'matrix_c',
                'label' => 'LBL_MATRIX',
              ),
              3 => 
              array (
                'name' => 'instrument_c',
                'label' => 'LBL_INSTRUMENT',
              ),
              4 => 
              array (
                'name' => 'analyst_c',
                'studio' => 'visible',
                'label' => 'LBL_ANALYST',
                'span' => 12,
              ),
            ),
          ),
          10 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL5',
            'label' => 'LBL_RECORDVIEW_PANEL5',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'chronic_study_c',
                'label' => 'LBL_CHRONIC_STUDY',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'study_data_location_c',
                'label' => 'LBL_STUDY_DATA_LOCATION',
              ),
              2 => 
              array (
                'name' => 'data_storage_phase_c',
                'label' => 'LBL_DATA_STORAGE_PHASE',
              ),
              3 => 
              array (
                'name' => 'specimens_c',
                'label' => 'LBL_SPECIMENS',
              ),
              4 => 
              array (
                'name' => 'specimen_location_c',
                'label' => 'LBL_SPECIMEN_LOCATION',
              ),
              5 => 
              array (
                'name' => 'study_article_c',
                'label' => 'LBL_STUDY_ARTICLE',
              ),
              6 => 
              array (
                'name' => 'control_article_c',
                'label' => 'LBL_CONTROL_ARTICLE',
              ),
              7 => 
              array (
                'name' => 'study_articles_location_c',
                'label' => 'LBL_STUDY_ARTICLES_LOCATION',
              ),
              8 => 
              array (
                'name' => 'control_articles_location_c',
                'label' => 'LBL_CONTROL_ARTICLES_LOCATION',
              ),
              9 => 
              array (
                'name' => 'ta_unique_ids_c',
                'label' => 'LBL_TA_UNIQUE_IDS',
              ),
              10 => 
              array (
                'name' => 'control_article_unique_ids_c',
                'label' => 'LBL_CONTROL_ARTICLE_UNIQUE_IDS',
              ),
              11 => 
              array (
                'name' => 'data_retention_start_date_c',
                'label' => 'LBL_DATA_RETENTION_START_DATE',
                'span' => 12,
              ),
              12 => 
              array (
                'name' => 'data_retention_end_date_glp_c',
                'label' => 'LBL_DATA_RETENTION_END_DATE_GLP',
                'span' => 12,
              ),
              13 => 
              array (
                'name' => 'data_retention_end_date_nong_c',
                'label' => 'LBL_DATA_RETENTION_END_DATE_NONG',
                'span' => 12,
              ),
              14 => 
              array (
                'name' => 'specimen_retention_start_c',
                'label' => 'LBL_SPECIMEN_RETENTION_START',
                'span' => 12,
              ),
              15 => 
              array (
                'name' => 'specimen_retention_end_c',
                'label' => 'LBL_SPECIMEN_RETENTION_END',
                'span' => 12,
              ),
              16 => 
              array (
                'name' => 'specimen_retention_end_date2_c',
                'label' => 'LBL_SPECIMEN_RETENTION_END_DATE2',
                'span' => 12,
              ),
              17 => 
              array (
                'name' => 'data_scanned_date_c',
                'label' => 'LBL_DATA_SCANNED_DATE',
              ),
              18 => 
              array (
                'name' => 'archived_date_c',
                'label' => 'LBL_ARCHIVED_DATE',
              ),
              19 => 
              array (
                'name' => 'shipped_destroyed_date_text_c',
                'label' => 'LBL_SHIPPED_DESTROYED_DATE_TEXT',
              ),
              20 => 
              array (
                'name' => 'culprits_list_c',
                'label' => 'LBL_CULPRITS_LIST',
              ),
              21 => 
              array (
                'name' => 'two_week_notice_issued_c',
                'label' => 'LBL_TWO_WEEK_NOTICE_ISSUED',
              ),
              22 => 
              array (
                'name' => 'type_of_contact_c',
                'label' => 'LBL_TYPE_OF_CONTACT',
              ),
              23 => 
              array (
                'name' => 'date_of_last_contact_c',
                'label' => 'LBL_DATE_OF_LAST_CONTACT',
              ),
              24 => 
              array (
                'name' => 'client_contact_notes_c',
                'label' => 'LBL_CLIENT_CONTACT_NOTES',
              ),
              25 => 
              array (
                'name' => 'data_storage_comments_c',
                'label' => 'LBL_DATA_STORAGE_COMMENTS',
                'span' => 12,
              ),
            ),
          ),
          11 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL6',
            'label' => 'LBL_RECORDVIEW_PANEL6',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'extract_abnormality_c',
                'label' => 'LBL_EXTRACT_ABNORMALITY',
              ),
              2 => 
              array (
                'name' => 'describe_abnormality_c',
                'studio' => 'visible',
                'label' => 'LBL_DESCRIBE_ABNORMALITY',
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
