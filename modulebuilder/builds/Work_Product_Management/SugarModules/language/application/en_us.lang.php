<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['M03_Work_Product'] = 'Work Products';
$app_list_strings['moduleList']['M03_Work_Product_Deliverable'] = 'Work Product Deliverables';
$app_list_strings['moduleList']['M03_Work_Product_Personnel'] = 'Additional WP Personnel';
$app_list_strings['moduleList']['M03_Work_Product_Facility'] = 'Work Product Facilities';
$app_list_strings['moduleList']['M03_Work_Product_IDs'] = 'Additional WP IDs';
$app_list_strings['moduleList']['M03_work_product_code'] = 'Work Product Codes';
$app_list_strings['moduleList']['M03_Additional_WP_Personnel'] = 'Additional WP Personnel';
$app_list_strings['moduleList']['M03_Additional_WP_IDs'] = 'Additional WP IDs';
$app_list_strings['moduleList']['M03_Work_Product_Code'] = 'Work Product Codes';
$app_list_strings['moduleListSingular']['M03_Work_Product'] = 'Work Product';
$app_list_strings['moduleListSingular']['M03_Work_Product_Deliverable'] = 'Work Product Deliverables';
$app_list_strings['moduleListSingular']['M03_Work_Product_Personnel'] = 'Additional WP Person';
$app_list_strings['moduleListSingular']['M03_Work_Product_Facility'] = 'Work Product Facility';
$app_list_strings['moduleListSingular']['M03_Work_Product_IDs'] = 'Additional WP ID';
$app_list_strings['moduleListSingular']['M03_work_product_code'] = 'Work Product Code';
$app_list_strings['moduleListSingular']['M03_Additional_WP_Personnel'] = 'Additional WP Person';
$app_list_strings['moduleListSingular']['M03_Additional_WP_IDs'] = 'Additional WP ID';
$app_list_strings['moduleListSingular']['M03_Work_Product_Code'] = 'Work Product Code';
