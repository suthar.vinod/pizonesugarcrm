<?php
// created: 2022-12-13 04:47:18
$mod_strings = array (
  'LBL_TEAM' => 'Equipos',
  'LBL_TEAMS' => 'Equipos',
  'LBL_TEAM_ID' => 'Id de Equipo',
  'LBL_ASSIGNED_TO_ID' => 'Id Usuario Asignado',
  'LBL_ASSIGNED_TO_NAME' => 'Asignado a',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Fecha de Creación',
  'LBL_DATE_MODIFIED' => 'Fecha de Modificación',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nombre',
  'LBL_CREATED' => 'Creado por',
  'LBL_CREATED_ID' => 'Creado por Id',
  'LBL_DOC_OWNER' => 'Propietario del documento',
  'LBL_USER_FAVORITES' => 'Usuarios Favoritos',
  'LBL_DESCRIPTION' => 'Descripción',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nombre',
  'LBL_CREATED_USER' => 'Creado por Usuario',
  'LBL_MODIFIED_USER' => 'Modificado por Usuario',
  'LBL_LIST_NAME' => 'Nombre',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Quitar',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado Por Nombre',
  'LBL_LIST_FORM_TITLE' => 'Work Product Personnel Lista',
  'LBL_MODULE_NAME' => 'Work Product Personnel',
  'LBL_MODULE_TITLE' => 'Work Product Personnel',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Person',
  'LBL_HOMEPAGE_TITLE' => 'Mi Work Product Personnel',
  'LNK_NEW_RECORD' => 'Crear Work Product Person',
  'LNK_LIST' => 'Ver Work Product Personnel',
  'LNK_IMPORT_M03_WORK_PRODUCT_PERSONNEL' => 'Import Work Product Person',
  'LBL_SEARCH_FORM_TITLE' => 'Buscar Work Product Person',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Actividades',
  'LBL_M03_WORK_PRODUCT_PERSONNEL_SUBPANEL_TITLE' => 'Work Product Personnel',
  'LBL_NEW_FORM_TITLE' => 'Nuevo Work Product Person',
  'LNK_IMPORT_VCARD' => 'Import Work Product Person vCard',
  'LBL_IMPORT' => 'Import Work Product Personnel',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Person record by importing a vCard from your file system.',
);