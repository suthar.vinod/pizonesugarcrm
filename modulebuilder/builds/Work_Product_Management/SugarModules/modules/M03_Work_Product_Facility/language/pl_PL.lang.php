<?php
// created: 2022-12-13 04:47:17
$mod_strings = array (
  'LBL_TEAM' => 'Zespoły',
  'LBL_TEAMS' => 'Zespoły',
  'LBL_TEAM_ID' => 'ID zespołu',
  'LBL_ASSIGNED_TO_ID' => 'Przydzielono do (ID):',
  'LBL_ASSIGNED_TO_NAME' => 'Przydzielono do',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data utworzenia',
  'LBL_DATE_MODIFIED' => 'Data modyfikacji',
  'LBL_MODIFIED' => 'Zmodyfikowane przez',
  'LBL_MODIFIED_ID' => 'Zmodyfikowane przez (ID)',
  'LBL_MODIFIED_NAME' => 'Zmodyfikowany przez (nazwa)',
  'LBL_CREATED' => 'Utworzone przez',
  'LBL_CREATED_ID' => 'Utworzone przez (ID)',
  'LBL_DOC_OWNER' => 'Właściciel dokumentu',
  'LBL_USER_FAVORITES' => 'Użytkownicy, którzy dodali rekord do ulubionych',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Usunięto',
  'LBL_NAME' => 'Nazwa',
  'LBL_CREATED_USER' => 'Utworzone przez',
  'LBL_MODIFIED_USER' => 'Zmodyfikowane przez',
  'LBL_LIST_NAME' => 'Nazwa',
  'LBL_EDIT_BUTTON' => 'Edytuj',
  'LBL_REMOVE' => 'Usuń',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmodyfikowany przez (nazwa)',
  'LBL_LIST_FORM_TITLE' => 'Work Product Facilities Cena katalogowa',
  'LBL_MODULE_NAME' => 'Work Product Facilities',
  'LBL_MODULE_TITLE' => 'Work Product Facilities',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Facility',
  'LBL_HOMEPAGE_TITLE' => 'Moje Work Product Facilities',
  'LNK_NEW_RECORD' => 'Utwórz Work Product Facility',
  'LNK_LIST' => 'Widok Work Product Facilities',
  'LNK_IMPORT_M03_WORK_PRODUCT_FACILITY' => 'Import Work Product Facility',
  'LBL_SEARCH_FORM_TITLE' => 'Wyszukiwanie Work Product Facility',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Wydarzenia',
  'LBL_M03_WORK_PRODUCT_FACILITY_SUBPANEL_TITLE' => 'Work Product Facilities',
  'LBL_NEW_FORM_TITLE' => 'Nowy Work Product Facility',
  'LNK_IMPORT_VCARD' => 'Import Work Product Facility vCard',
  'LBL_IMPORT' => 'Import Work Product Facilities',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Facility record by importing a vCard from your file system.',
);