<?php
// created: 2022-12-13 04:47:14
$mod_strings = array (
  'LBL_TEAM' => 'チーム',
  'LBL_TEAMS' => 'チーム',
  'LBL_TEAM_ID' => 'チームID',
  'LBL_ASSIGNED_TO_ID' => 'アサイン先ID',
  'LBL_ASSIGNED_TO_NAME' => 'アサイン先',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '作成日',
  'LBL_DATE_MODIFIED' => '更新日',
  'LBL_MODIFIED' => '更新者',
  'LBL_MODIFIED_ID' => '更新者ID',
  'LBL_MODIFIED_NAME' => '更新者',
  'LBL_CREATED' => '作成者',
  'LBL_CREATED_ID' => '作成者ID',
  'LBL_DOC_OWNER' => 'ドキュメントの所有者',
  'LBL_USER_FAVORITES' => 'お気に入りのユーザー',
  'LBL_DESCRIPTION' => '詳細',
  'LBL_DELETED' => '削除済み',
  'LBL_NAME' => '名前',
  'LBL_CREATED_USER' => '作成者',
  'LBL_MODIFIED_USER' => '更新者',
  'LBL_LIST_NAME' => '名前',
  'LBL_EDIT_BUTTON' => '編集',
  'LBL_REMOVE' => 'はずす',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '更新者',
  'LBL_LIST_FORM_TITLE' => 'Additional WP IDs リスト',
  'LBL_MODULE_NAME' => 'Additional WP IDs',
  'LBL_MODULE_TITLE' => 'Additional WP IDs',
  'LBL_MODULE_NAME_SINGULAR' => 'Additional WP ID',
  'LBL_HOMEPAGE_TITLE' => '私の Additional WP IDs',
  'LNK_NEW_RECORD' => '作成 Additional WP ID',
  'LNK_LIST' => '閲覧 Additional WP IDs',
  'LNK_IMPORT_M03_WORK_PRODUCT_IDS' => 'Import Additional WP ID',
  'LBL_SEARCH_FORM_TITLE' => '検索 Additional WP ID',
  'LBL_HISTORY_SUBPANEL_TITLE' => '履歴の表示',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活動',
  'LBL_M03_WORK_PRODUCT_IDS_SUBPANEL_TITLE' => 'Additional WP IDs',
  'LBL_NEW_FORM_TITLE' => '新規 Additional WP ID',
  'LNK_IMPORT_VCARD' => 'Import Additional WP ID vCard',
  'LBL_IMPORT' => 'Import Additional WP IDs',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Additional WP ID record by importing a vCard from your file system.',
  'LNK_IMPORT_M03_ADDITIONAL_WP_IDS' => 'Import Additional WP ID',
  'LBL_M03_ADDITIONAL_WP_IDS_SUBPANEL_TITLE' => 'Additional WP IDs',
);