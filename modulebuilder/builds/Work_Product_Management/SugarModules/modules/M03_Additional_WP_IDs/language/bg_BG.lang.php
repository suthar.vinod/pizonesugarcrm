<?php
// created: 2022-12-13 04:47:14
$mod_strings = array (
  'LBL_TEAM' => 'Екип',
  'LBL_TEAMS' => 'Екип',
  'LBL_TEAM_ID' => 'Идентификатор на екип',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Отговорник',
  'LBL_ID' => 'Идентификатор',
  'LBL_DATE_ENTERED' => 'Въведено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_DOC_OWNER' => 'Собственик на документа',
  'LBL_USER_FAVORITES' => 'Потребители които харесват',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Изтрити',
  'LBL_NAME' => 'Име',
  'LBL_CREATED_USER' => 'Създадено от потребител',
  'LBL_MODIFIED_USER' => 'Модифицирано от потребител',
  'LBL_LIST_NAME' => 'Име',
  'LBL_EDIT_BUTTON' => 'Редактиране',
  'LBL_REMOVE' => 'Изтрий',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Модифицирано от',
  'LBL_LIST_FORM_TITLE' => 'Additional WP IDs Списък',
  'LBL_MODULE_NAME' => 'Additional WP IDs',
  'LBL_MODULE_TITLE' => 'Additional WP IDs',
  'LBL_MODULE_NAME_SINGULAR' => 'Additional WP ID',
  'LBL_HOMEPAGE_TITLE' => 'Мои Additional WP IDs',
  'LNK_NEW_RECORD' => 'Създай Additional WP ID',
  'LNK_LIST' => 'Разгледай Additional WP IDs',
  'LNK_IMPORT_M03_WORK_PRODUCT_IDS' => 'Import Additional WP ID',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Additional WP ID',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_M03_WORK_PRODUCT_IDS_SUBPANEL_TITLE' => 'Additional WP IDs',
  'LBL_NEW_FORM_TITLE' => 'Нов Additional WP ID',
  'LNK_IMPORT_VCARD' => 'Import Additional WP ID vCard',
  'LBL_IMPORT' => 'Import Additional WP IDs',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Additional WP ID record by importing a vCard from your file system.',
  'LNK_IMPORT_M03_ADDITIONAL_WP_IDS' => 'Import Additional WP ID',
  'LBL_M03_ADDITIONAL_WP_IDS_SUBPANEL_TITLE' => 'Additional WP IDs',
);