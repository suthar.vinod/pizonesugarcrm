<?php
// created: 2022-12-13 04:47:18
$mod_strings = array (
  'LBL_TEAM' => 'Команды',
  'LBL_TEAMS' => 'Команды',
  'LBL_TEAM_ID' => 'ID команды',
  'LBL_ASSIGNED_TO_ID' => 'Ответственный (-ая)',
  'LBL_ASSIGNED_TO_NAME' => 'За кем ответственный (-ая)',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено пользователем:',
  'LBL_MODIFIED_ID' => 'Изменено (Id)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано (Id)',
  'LBL_DOC_OWNER' => 'Владелец Документа',
  'LBL_USER_FAVORITES' => 'Пользователи, которые добавили в Избранное',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано пользователем',
  'LBL_MODIFIED_USER' => 'Изменено пользователем',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Правка',
  'LBL_REMOVE' => 'Удалить',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Изменено',
  'LBL_LIST_FORM_TITLE' => 'Work Product IDs Прайслист',
  'LBL_MODULE_NAME' => 'Work Product IDs',
  'LBL_MODULE_TITLE' => 'Work Product IDs',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product ID',
  'LBL_HOMEPAGE_TITLE' => 'Моя Work Product IDs',
  'LNK_NEW_RECORD' => 'Создать Work Product ID',
  'LNK_LIST' => 'Просмотр Work Product IDs',
  'LNK_IMPORT_M03_WORK_PRODUCT_IDS' => 'Import Work Product ID',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск Work Product ID',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_M03_WORK_PRODUCT_IDS_SUBPANEL_TITLE' => 'Work Product IDs',
  'LBL_NEW_FORM_TITLE' => 'Новая Work Product ID',
  'LNK_IMPORT_VCARD' => 'Import Work Product ID vCard',
  'LBL_IMPORT' => 'Import Work Product IDs',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product ID record by importing a vCard from your file system.',
);