<?php
// created: 2022-12-13 04:47:17
$mod_strings = array (
  'LBL_TEAM' => 'Equipas',
  'LBL_TEAMS' => 'Equipas',
  'LBL_TEAM_ID' => 'ID da Equipa',
  'LBL_ASSIGNED_TO_ID' => 'Atribuído a',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Introdução',
  'LBL_DATE_MODIFIED' => 'Data de Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Id de Modificado Por',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado por',
  'LBL_CREATED_ID' => 'ID Criado por',
  'LBL_DOC_OWNER' => 'Proprietário do Documento',
  'LBL_USER_FAVORITES' => 'Utilizadores que adicionaram aos Favoritos',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado pelo Utilizador',
  'LBL_MODIFIED_USER' => 'Modificado pelo Utilizador',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado Por Nome',
  'LBL_LIST_FORM_TITLE' => 'Work Product Deliverables Lista',
  'LBL_MODULE_NAME' => 'Work Product Deliverables',
  'LBL_MODULE_TITLE' => 'Work Product Deliverables',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Deliverables',
  'LBL_HOMEPAGE_TITLE' => 'Minha Work Product Deliverables',
  'LNK_NEW_RECORD' => 'Criar Work Product Deliverables',
  'LNK_LIST' => 'Ver Work Product Deliverables',
  'LNK_IMPORT_M03_WORK_PRODUCT_DELIVERABLE' => 'Import Work Product Deliverables',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Work Product Deliverables',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Actividades',
  'LBL_M03_WORK_PRODUCT_DELIVERABLE_SUBPANEL_TITLE' => 'Work Product Deliverables',
  'LBL_NEW_FORM_TITLE' => 'Novo Work Product Deliverables',
  'LNK_IMPORT_VCARD' => 'Import Work Product Deliverables vCard',
  'LBL_IMPORT' => 'Import Work Product Deliverables',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Deliverables record by importing a vCard from your file system.',
);