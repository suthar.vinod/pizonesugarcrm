<?php
// created: 2022-12-13 04:47:12
$mod_strings = array (
  'LBL_TEAM' => 'Meeskonnad',
  'LBL_TEAMS' => 'Meeskonnad',
  'LBL_TEAM_ID' => 'Meeskonna ID',
  'LBL_ASSIGNED_TO_ID' => 'Määratud kasutaja ID',
  'LBL_ASSIGNED_TO_NAME' => 'Määratud kasutajale',
  'LBL_TAGS_LINK' => 'Sildid',
  'LBL_TAGS' => 'Sildid',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Loomiskuupäev',
  'LBL_DATE_MODIFIED' => 'Muutmiskuupäev',
  'LBL_MODIFIED' => 'Muutja',
  'LBL_MODIFIED_ID' => 'Muutja ID',
  'LBL_MODIFIED_NAME' => 'Muutja nimi',
  'LBL_CREATED' => 'Loodud',
  'LBL_CREATED_ID' => 'Looja ID',
  'LBL_DOC_OWNER' => 'Dokumendi omanik',
  'LBL_USER_FAVORITES' => 'Lemmikkasutajad',
  'LBL_DESCRIPTION' => 'Kirjeldus',
  'LBL_DELETED' => 'Kustutatud',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Loonud kasutaja',
  'LBL_MODIFIED_USER' => 'Muutnud kasutaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Redigeeri',
  'LBL_REMOVE' => 'Eemalda',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muutja nimi',
  'LBL_LIST_FORM_TITLE' => 'Tradeshow_Management Loend',
  'LBL_MODULE_NAME' => 'Tradeshow_Management',
  'LBL_MODULE_TITLE' => 'Tradeshow_Management',
  'LBL_MODULE_NAME_SINGULAR' => 'Tradeshow_Management',
  'LBL_HOMEPAGE_TITLE' => 'Minu Tradeshow_Management',
  'LNK_NEW_RECORD' => 'Loo Tradeshow_Management',
  'LNK_LIST' => 'Vaade Tradeshow_Management',
  'LNK_IMPORT_TM_TRADESHOW_MANAGEMENT' => 'Import Tradeshow_Management',
  'LBL_SEARCH_FORM_TITLE' => 'Otsi Tradeshow_Management',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vaata ajalugu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tegevuste voog',
  'LBL_TM_TRADESHOW_MANAGEMENT_SUBPANEL_TITLE' => 'Tradeshow_Management',
  'LBL_NEW_FORM_TITLE' => 'Uus Tradeshow_Management',
  'LNK_IMPORT_VCARD' => 'Import Tradeshow_Management vCard',
  'LBL_IMPORT' => 'Import Tradeshow_Management',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Tradeshow_Management record by importing a vCard from your file system.',
);