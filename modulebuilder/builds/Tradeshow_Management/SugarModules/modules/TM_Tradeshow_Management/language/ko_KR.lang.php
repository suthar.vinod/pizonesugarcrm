<?php
// created: 2022-12-13 04:47:12
$mod_strings = array (
  'LBL_TEAM' => '팀',
  'LBL_TEAMS' => '팀',
  'LBL_TEAM_ID' => '팀 ID',
  'LBL_ASSIGNED_TO_ID' => '지정 사용자 ID',
  'LBL_ASSIGNED_TO_NAME' => '지정자',
  'LBL_TAGS_LINK' => '태그',
  'LBL_TAGS' => '태그',
  'LBL_ID' => 'ID:',
  'LBL_DATE_ENTERED' => '생성일자:',
  'LBL_DATE_MODIFIED' => '수정일자:',
  'LBL_MODIFIED' => '수정자:',
  'LBL_MODIFIED_ID' => '수정자 ID',
  'LBL_MODIFIED_NAME' => '사용자명에 의해 수정',
  'LBL_CREATED' => '생성자',
  'LBL_CREATED_ID' => '생성자 ID',
  'LBL_DOC_OWNER' => '문서소유자',
  'LBL_USER_FAVORITES' => '사용자 즐겨 찾기',
  'LBL_DESCRIPTION' => '설명',
  'LBL_DELETED' => '삭제',
  'LBL_NAME' => '이름',
  'LBL_CREATED_USER' => '사용자에 의해 생성',
  'LBL_MODIFIED_USER' => '사용자에 의해 수정',
  'LBL_LIST_NAME' => '성명',
  'LBL_EDIT_BUTTON' => '수정하기',
  'LBL_REMOVE' => '제거하기',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '이름으로 수정',
  'LBL_LIST_FORM_TITLE' => 'Tradeshow_Management 목록',
  'LBL_MODULE_NAME' => 'Tradeshow_Management',
  'LBL_MODULE_TITLE' => 'Tradeshow_Management',
  'LBL_MODULE_NAME_SINGULAR' => 'Tradeshow_Management',
  'LBL_HOMEPAGE_TITLE' => '나의 Tradeshow_Management',
  'LNK_NEW_RECORD' => '새로 만들기 Tradeshow_Management',
  'LNK_LIST' => '보기 Tradeshow_Management',
  'LNK_IMPORT_TM_TRADESHOW_MANAGEMENT' => 'Import Tradeshow_Management',
  'LBL_SEARCH_FORM_TITLE' => '검색 Tradeshow_Management',
  'LBL_HISTORY_SUBPANEL_TITLE' => '연혁보기',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '활동내역',
  'LBL_TM_TRADESHOW_MANAGEMENT_SUBPANEL_TITLE' => 'Tradeshow_Management',
  'LBL_NEW_FORM_TITLE' => '신규 Tradeshow_Management',
  'LNK_IMPORT_VCARD' => 'Import Tradeshow_Management vCard',
  'LBL_IMPORT' => 'Import Tradeshow_Management',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Tradeshow_Management record by importing a vCard from your file system.',
);