<?php
// created: 2022-12-13 04:47:08
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Lag-ID',
  'LBL_ASSIGNED_TO_ID' => 'Tilldelat användar-ID',
  'LBL_ASSIGNED_TO_NAME' => 'Tilldelad användare',
  'LBL_TAGS_LINK' => 'Taggar',
  'LBL_TAGS' => 'Taggar',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum skapat',
  'LBL_DATE_MODIFIED' => 'Modifierat Datum',
  'LBL_MODIFIED' => 'Modifierat Av',
  'LBL_MODIFIED_ID' => 'Modifierat Av Id',
  'LBL_MODIFIED_NAME' => 'Modifierat Av Namn',
  'LBL_CREATED' => 'Skapat Av',
  'LBL_CREATED_ID' => 'Skapat Av',
  'LBL_DOC_OWNER' => 'Ägare av dokument',
  'LBL_USER_FAVORITES' => 'Användare som favorite',
  'LBL_DESCRIPTION' => 'Beskrivning',
  'LBL_DELETED' => 'Raderad',
  'LBL_NAME' => 'Namn',
  'LBL_CREATED_USER' => 'Skapat Av Användare',
  'LBL_MODIFIED_USER' => 'Modifierat Av Användare',
  'LBL_LIST_NAME' => 'Namn',
  'LBL_EDIT_BUTTON' => 'Redigera',
  'LBL_REMOVE' => 'Ta bort',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ändrad av namn',
  'LBL_LIST_FORM_TITLE' => 'Study Workflow List',
  'LBL_MODULE_NAME' => 'Study Workflow',
  'LBL_MODULE_TITLE' => 'Study Workflow',
  'LBL_MODULE_NAME_SINGULAR' => 'Study Workflow',
  'LBL_HOMEPAGE_TITLE' => 'Min Study Workflow',
  'LNK_NEW_RECORD' => 'Create Study Workflow',
  'LNK_LIST' => 'Visa Study Workflow',
  'LNK_IMPORT_SW_STUDY_WORKFLOW' => 'Import Study Workflow',
  'LBL_SEARCH_FORM_TITLE' => 'Search Study Workflow',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetsström',
  'LBL_SW_STUDY_WORKFLOW_SUBPANEL_TITLE' => 'Study Workflow',
  'LBL_NEW_FORM_TITLE' => 'Ny Study Workflow',
  'LNK_IMPORT_VCARD' => 'Import Study Workflow vCard',
  'LBL_IMPORT' => 'Import Study Workflow',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Study Workflow record by importing a vCard from your file system.',
);