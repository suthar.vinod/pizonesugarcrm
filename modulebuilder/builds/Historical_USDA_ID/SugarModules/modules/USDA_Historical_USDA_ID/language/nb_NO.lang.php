<?php
// created: 2022-12-13 04:46:54
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Historical USDA IDs Liste',
  'LBL_MODULE_NAME' => 'Historical USDA IDs',
  'LBL_MODULE_TITLE' => 'Historical USDA IDs',
  'LBL_MODULE_NAME_SINGULAR' => 'Historical USDA ID',
  'LBL_HOMEPAGE_TITLE' => 'Min Historical USDA IDs',
  'LNK_NEW_RECORD' => 'Opprett Historical USDA ID',
  'LNK_LIST' => 'Vis Historical USDA IDs',
  'LNK_IMPORT_USDA_HISTORICAL_USDA_ID' => 'Import Historical USDA IDs',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Historical USDA ID',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_USDA_HISTORICAL_USDA_ID_SUBPANEL_TITLE' => 'Historical USDA IDs',
  'LBL_NEW_FORM_TITLE' => 'Ny Historical USDA ID',
  'LNK_IMPORT_VCARD' => 'Import Historical USDA ID vCard',
  'LBL_IMPORT' => 'Import Historical USDA IDs',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Historical USDA ID record by importing a vCard from your file system.',
  'LBL_USDA_HISTORICAL_USDA_ID_FOCUS_DRAWER_DASHBOARD' => 'Historical USDA IDs Focus Drawer',
  'LBL_DATE_OF_REIDENTIFICATION' => 'Date of Reidentification',
);