<?php
// created: 2022-12-13 04:46:54
$mod_strings = array (
  'LBL_TEAM' => 'Grupet',
  'LBL_TEAMS' => 'Grupet',
  'LBL_TEAM_ID' => 'Id e grupit',
  'LBL_ASSIGNED_TO_ID' => 'Id e përdoruesit të caktuar',
  'LBL_ASSIGNED_TO_NAME' => 'Përdorues',
  'LBL_TAGS_LINK' => 'Etiketat',
  'LBL_TAGS' => 'Etiketat',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Të dhënat e krijuara',
  'LBL_DATE_MODIFIED' => 'Të dhënat e modifikuara',
  'LBL_MODIFIED' => 'Modifikuar nga',
  'LBL_MODIFIED_ID' => 'Modifikuar nga Id',
  'LBL_MODIFIED_NAME' => 'Modifikuar nga emri',
  'LBL_CREATED' => 'Krijuar nga',
  'LBL_CREATED_ID' => 'Krijuar nga Id',
  'LBL_DOC_OWNER' => 'Zotëruesi i dokumentit',
  'LBL_USER_FAVORITES' => 'Përdoruesit të cilët preferojnë',
  'LBL_DESCRIPTION' => 'Përshkrim',
  'LBL_DELETED' => 'E fshirë',
  'LBL_NAME' => 'Emri',
  'LBL_CREATED_USER' => 'Krijuar nga përdoruesi',
  'LBL_MODIFIED_USER' => 'Modifikuar nga përdoruesi',
  'LBL_LIST_NAME' => 'Emri',
  'LBL_EDIT_BUTTON' => 'Ndrysho',
  'LBL_REMOVE' => 'Hiqe',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikuar nga emri',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Historical USDA IDs lista',
  'LBL_MODULE_NAME' => 'Historical USDA IDs',
  'LBL_MODULE_TITLE' => 'Historical USDA IDs',
  'LBL_MODULE_NAME_SINGULAR' => 'Historical USDA ID',
  'LBL_HOMEPAGE_TITLE' => 'Mia Historical USDA IDs',
  'LNK_NEW_RECORD' => 'Krijo Historical USDA ID',
  'LNK_LIST' => 'Pamje Historical USDA IDs',
  'LNK_IMPORT_USDA_HISTORICAL_USDA_ID' => 'Import Historical USDA IDs',
  'LBL_SEARCH_FORM_TITLE' => 'Kërkim Historical USDA ID',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'shiko historinë',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetet',
  'LBL_USDA_HISTORICAL_USDA_ID_SUBPANEL_TITLE' => 'Historical USDA IDs',
  'LBL_NEW_FORM_TITLE' => 'E re Historical USDA ID',
  'LNK_IMPORT_VCARD' => 'Import Historical USDA ID vCard',
  'LBL_IMPORT' => 'Import Historical USDA IDs',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Historical USDA ID record by importing a vCard from your file system.',
  'LBL_USDA_HISTORICAL_USDA_ID_FOCUS_DRAWER_DASHBOARD' => 'Historical USDA IDs Focus Drawer',
  'LBL_DATE_OF_REIDENTIFICATION' => 'Date of Reidentification',
);