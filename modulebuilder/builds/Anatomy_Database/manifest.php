<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

// THIS CONTENT IS GENERATED BY MBPackage.php
$manifest = array (
  'built_in_version' => '7.11.1.0',
  'acceptable_sugar_versions' => 
  array (
    0 => '',
  ),
  'acceptable_sugar_flavors' => 
  array (
    0 => 'ENT',
    1 => 'ULT',
  ),
  'readme' => '',
  'key' => 'AD',
  'author' => 'MF',
  'description' => '',
  'icon' => '',
  'is_uninstallable' => true,
  'name' => 'Anatomy_Database',
  'published_date' => '2018-05-21 16:26:15',
  'type' => 'module',
  'version' => 1526919976,
  'remove_tables' => 'prompt',
);


$installdefs = array (
  'id' => 'Anatomy_Database',
  'beans' => 
  array (
    0 => 
    array (
      'module' => 'AD_Anatomy_Database',
      'class' => 'AD_Anatomy_Database',
      'path' => 'modules/AD_Anatomy_Database/AD_Anatomy_Database.php',
      'tab' => true,
    ),
  ),
  'layoutdefs' => 
  array (
  ),
  'relationships' => 
  array (
  ),
  'copy' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/SugarModules/modules/AD_Anatomy_Database',
      'to' => 'modules/AD_Anatomy_Database',
    ),
  ),
  'language' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/SugarModules/language/application/en_us.lang.php',
      'to_module' => 'application',
      'language' => 'en_us',
    ),
  ),
  'image_dir' => '<basepath>/icons',
);