<?php
// created: 2022-12-13 04:46:50
$mod_strings = array (
  'LBL_TEAM' => 'Команди',
  'LBL_TEAMS' => 'Команди',
  'LBL_TEAM_ID' => 'Команда (Id)',
  'LBL_ASSIGNED_TO_ID' => 'Відповідальний (-а) (Id)',
  'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (-а)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_DATE_MODIFIED' => 'Дата змінення',
  'LBL_MODIFIED' => 'Змінено:',
  'LBL_MODIFIED_ID' => 'Змінено (Id)',
  'LBL_MODIFIED_NAME' => 'Змінено користувачем',
  'LBL_CREATED' => 'Створено',
  'LBL_CREATED_ID' => 'Створено (Id)',
  'LBL_DOC_OWNER' => 'Власник документа',
  'LBL_USER_FAVORITES' => 'Користувачі, які додали в Обране',
  'LBL_DESCRIPTION' => 'Опис',
  'LBL_DELETED' => 'Видалено',
  'LBL_NAME' => 'Назва',
  'LBL_CREATED_USER' => 'Створенено користувачем',
  'LBL_MODIFIED_USER' => 'Змінено користувачем',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_EDIT_BUTTON' => 'Редагувати',
  'LBL_REMOVE' => 'Видалити',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Змінено користувачем',
  'LBL_LIST_FORM_TITLE' => 'Errors Departments Список',
  'LBL_MODULE_NAME' => 'Errors Departments',
  'LBL_MODULE_TITLE' => 'Errors Departments',
  'LBL_MODULE_NAME_SINGULAR' => 'Errors Department',
  'LBL_HOMEPAGE_TITLE' => 'Мій Errors Departments',
  'LNK_NEW_RECORD' => 'Створити Errors Department',
  'LNK_LIST' => 'Переглянути Errors Departments',
  'LNK_IMPORT_ED_ERRORS_DEPARTMENT' => 'Import Errors Departments',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук Errors Department',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Переглянути історію',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Стрічка активностей',
  'LBL_ED_ERRORS_DEPARTMENT_SUBPANEL_TITLE' => 'Errors Departments',
  'LBL_NEW_FORM_TITLE' => 'Новий Errors Department',
  'LNK_IMPORT_VCARD' => 'Import Errors Department vCard',
  'LBL_IMPORT' => 'Import Errors Departments',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Errors Department record by importing a vCard from your file system.',
);