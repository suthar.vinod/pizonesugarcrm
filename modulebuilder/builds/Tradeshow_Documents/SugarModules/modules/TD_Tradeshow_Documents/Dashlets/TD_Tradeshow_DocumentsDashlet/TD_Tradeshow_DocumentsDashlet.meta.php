<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['TD_Tradeshow_DocumentsDashlet'] = array('module'		=> 'TD_Tradeshow_Documents',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'TD_Tradeshow_Documents'), 
                                          'description' => 'A customizable view into TD_Tradeshow_Documents',
                                          'icon'        => 'icon_TD_Tradeshow_Documents_32.gif',
                                          'category'    => 'Module Views');