<?php
// created: 2022-12-13 04:46:46
$mod_strings = array (
  'LBL_TEAM' => '小組',
  'LBL_TEAMS' => '小組',
  'LBL_TEAM_ID' => '小組 ID',
  'LBL_ASSIGNED_TO_ID' => '指派的使用者 ID',
  'LBL_ASSIGNED_TO_NAME' => '已指派至',
  'LBL_TAGS_LINK' => '標籤',
  'LBL_TAGS' => '標籤',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '建立日期',
  'LBL_DATE_MODIFIED' => '修改日期',
  'LBL_MODIFIED' => '修改人',
  'LBL_MODIFIED_ID' => '按 ID 修改',
  'LBL_MODIFIED_NAME' => '按名稱修改',
  'LBL_CREATED' => '建立人',
  'LBL_CREATED_ID' => '按 ID 建立',
  'LBL_DOC_OWNER' => '文件擁有者',
  'LBL_USER_FAVORITES' => '最愛的使用者',
  'LBL_DESCRIPTION' => '描述',
  'LBL_DELETED' => '已刪除',
  'LBL_NAME' => '名稱',
  'LBL_CREATED_USER' => '由使用者建立',
  'LBL_MODIFIED_USER' => '由使用者修改',
  'LBL_LIST_NAME' => '名稱',
  'LBL_EDIT_BUTTON' => '編輯',
  'LBL_REMOVE' => '移除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '按名稱修改',
  'LBL_EXPORT_CREATED_BY_NAME' => '由名稱建立',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Controlled Document Utilizations 清單',
  'LBL_MODULE_NAME' => 'Controlled Document Utilizations',
  'LBL_MODULE_TITLE' => 'Controlled Document Utilizations',
  'LBL_MODULE_NAME_SINGULAR' => 'Controlled Document Utilizations',
  'LBL_HOMEPAGE_TITLE' => '我的 Controlled Document Utilizations',
  'LNK_NEW_RECORD' => '建立 Controlled Document Utilizations',
  'LNK_LIST' => '檢視 Controlled Document Utilizations',
  'LNK_IMPORT_CDU_CD_UTILIZATION' => 'Import Controlled Document Utilizations',
  'LBL_SEARCH_FORM_TITLE' => '搜尋 Controlled Document Utilizations',
  'LBL_HISTORY_SUBPANEL_TITLE' => '檢視歷史',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活動流',
  'LBL_CDU_CD_UTILIZATION_SUBPANEL_TITLE' => 'Controlled Document Utilizations',
  'LBL_NEW_FORM_TITLE' => '新 Controlled Document Utilizations',
  'LNK_IMPORT_VCARD' => 'Import Controlled Document Utilizations vCard',
  'LBL_IMPORT' => 'Import Controlled Document Utilizations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Controlled Document Utilizations record by importing a vCard from your file system.',
  'LBL_CDU_CD_UTILIZATION_FOCUS_DRAWER_DASHBOARD' => 'Controlled Document Utilizations Focus Drawer',
  'LBL_MONTH_END' => 'Month End',
  'LBL_NUMBER_OF_PROCEDURES' => 'Number of Procedures',
  'LBL_NUMBER_OF_TESTS' => 'Number of Tests',
  'LBL_NUMBER_OF_ANIMALS' => 'Number of Animals',
  'LBL_CONTROLLED_DOCUMENT_UTILIZATIO' => 'Controlled Document Utilization',
  'LBL_NUMBER_OF_PROCEDURES_2' => 'Number of Procedures',
  'LBL_NUMBER_OF_TESTS_2' => 'Number of Tests',
  'LBL_NUMBER_OF_ANIMALS_2' => 'Number of Animals',
  'LBL_CONTROLLED_DOCUMENT_UTI' => 'Controlled Document Utilization',
  'LBL_CDU_CD_UTILIZATION_RECORD_DASHBOARD' => 'Controlled Document Utilizations Record Dashboard',
);