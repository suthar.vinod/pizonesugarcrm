<?php
// created: 2022-12-13 04:46:46
$mod_strings = array (
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Id Echipă',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuit lui',
  'LBL_TAGS_LINK' => 'Etichete',
  'LBL_TAGS' => 'Etichete',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data creării',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificat după Id',
  'LBL_MODIFIED_NAME' => 'Modificat după Nume',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creat de Id',
  'LBL_DOC_OWNER' => 'Proprietar document',
  'LBL_USER_FAVORITES' => 'Utilizatori cu setări favorite',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_DELETED' => 'Şters',
  'LBL_NAME' => 'Nume',
  'LBL_CREATED_USER' => 'Creat de Utilizator',
  'LBL_MODIFIED_USER' => 'Modificat după Utilizator',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_REMOVE' => 'Eliminare',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat după Nume',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Creat după nume',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Controlled Document Utilizations Lista',
  'LBL_MODULE_NAME' => 'Controlled Document Utilizations',
  'LBL_MODULE_TITLE' => 'Controlled Document Utilizations',
  'LBL_MODULE_NAME_SINGULAR' => 'Controlled Document Utilizations',
  'LBL_HOMEPAGE_TITLE' => 'Al meu Controlled Document Utilizations',
  'LNK_NEW_RECORD' => 'Creează Controlled Document Utilizations',
  'LNK_LIST' => 'Vizualizare Controlled Document Utilizations',
  'LNK_IMPORT_CDU_CD_UTILIZATION' => 'Import Controlled Document Utilizations',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta Controlled Document Utilizations',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_CDU_CD_UTILIZATION_SUBPANEL_TITLE' => 'Controlled Document Utilizations',
  'LBL_NEW_FORM_TITLE' => 'Nou Controlled Document Utilizations',
  'LNK_IMPORT_VCARD' => 'Import Controlled Document Utilizations vCard',
  'LBL_IMPORT' => 'Import Controlled Document Utilizations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Controlled Document Utilizations record by importing a vCard from your file system.',
  'LBL_CDU_CD_UTILIZATION_FOCUS_DRAWER_DASHBOARD' => 'Controlled Document Utilizations Focus Drawer',
  'LBL_MONTH_END' => 'Month End',
  'LBL_NUMBER_OF_PROCEDURES' => 'Number of Procedures',
  'LBL_NUMBER_OF_TESTS' => 'Number of Tests',
  'LBL_NUMBER_OF_ANIMALS' => 'Number of Animals',
  'LBL_CONTROLLED_DOCUMENT_UTILIZATIO' => 'Controlled Document Utilization',
  'LBL_NUMBER_OF_PROCEDURES_2' => 'Number of Procedures',
  'LBL_NUMBER_OF_TESTS_2' => 'Number of Tests',
  'LBL_NUMBER_OF_ANIMALS_2' => 'Number of Animals',
  'LBL_CONTROLLED_DOCUMENT_UTI' => 'Controlled Document Utilization',
  'LBL_CDU_CD_UTILIZATION_RECORD_DASHBOARD' => 'Controlled Document Utilizations Record Dashboard',
);