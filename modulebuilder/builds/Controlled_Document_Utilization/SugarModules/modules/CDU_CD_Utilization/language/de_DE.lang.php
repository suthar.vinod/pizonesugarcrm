<?php
// created: 2022-12-13 04:46:45
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team-ID',
  'LBL_ASSIGNED_TO_ID' => 'Zugewiesene Benutzer-Id',
  'LBL_ASSIGNED_TO_NAME' => 'Zugewiesen an',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Erstellungsdatum',
  'LBL_DATE_MODIFIED' => 'Änderungsdatum',
  'LBL_MODIFIED' => 'Geändert von',
  'LBL_MODIFIED_ID' => 'Geändert von ID',
  'LBL_MODIFIED_NAME' => 'Geändert von Name',
  'LBL_CREATED' => 'Erstellt von:',
  'LBL_CREATED_ID' => 'Ersteller',
  'LBL_DOC_OWNER' => 'Dokument-Eigentümer',
  'LBL_USER_FAVORITES' => 'Benutzer mit Favoriten',
  'LBL_DESCRIPTION' => 'Beschreibung',
  'LBL_DELETED' => 'Gelöscht',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Erstellt von',
  'LBL_MODIFIED_USER' => 'Geändert von',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Bearbeiten',
  'LBL_REMOVE' => 'Entfernen',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Geändert von Name',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Erstellt von Name',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Controlled Document Utilizations Liste',
  'LBL_MODULE_NAME' => 'Controlled Document Utilizations',
  'LBL_MODULE_TITLE' => 'Controlled Document Utilizations',
  'LBL_MODULE_NAME_SINGULAR' => 'Controlled Document Utilizations',
  'LBL_HOMEPAGE_TITLE' => 'Mein Controlled Document Utilizations',
  'LNK_NEW_RECORD' => 'Erstellen Controlled Document Utilizations',
  'LNK_LIST' => 'Ansicht Controlled Document Utilizations',
  'LNK_IMPORT_CDU_CD_UTILIZATION' => 'Import Controlled Document Utilizations',
  'LBL_SEARCH_FORM_TITLE' => 'Suchen Controlled Document Utilizations',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Verlauf ansehen',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitäten-Stream',
  'LBL_CDU_CD_UTILIZATION_SUBPANEL_TITLE' => 'Controlled Document Utilizations',
  'LBL_NEW_FORM_TITLE' => 'Neu Controlled Document Utilizations',
  'LNK_IMPORT_VCARD' => 'Import Controlled Document Utilizations vCard',
  'LBL_IMPORT' => 'Import Controlled Document Utilizations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Controlled Document Utilizations record by importing a vCard from your file system.',
  'LBL_CDU_CD_UTILIZATION_FOCUS_DRAWER_DASHBOARD' => 'Controlled Document Utilizations Focus Drawer',
  'LBL_MONTH_END' => 'Month End',
  'LBL_NUMBER_OF_PROCEDURES' => 'Number of Procedures',
  'LBL_NUMBER_OF_TESTS' => 'Number of Tests',
  'LBL_NUMBER_OF_ANIMALS' => 'Number of Animals',
  'LBL_CONTROLLED_DOCUMENT_UTILIZATIO' => 'Controlled Document Utilization',
  'LBL_NUMBER_OF_PROCEDURES_2' => 'Number of Procedures',
  'LBL_NUMBER_OF_TESTS_2' => 'Number of Tests',
  'LBL_NUMBER_OF_ANIMALS_2' => 'Number of Animals',
  'LBL_CONTROLLED_DOCUMENT_UTI' => 'Controlled Document Utilization',
  'LBL_CDU_CD_UTILIZATION_RECORD_DASHBOARD' => 'Controlled Document Utilizations Record Dashboard',
);