<?php
// created: 2022-12-13 04:46:46
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Opprett av navn',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Controlled Document Utilizations Liste',
  'LBL_MODULE_NAME' => 'Controlled Document Utilizations',
  'LBL_MODULE_TITLE' => 'Controlled Document Utilizations',
  'LBL_MODULE_NAME_SINGULAR' => 'Controlled Document Utilizations',
  'LBL_HOMEPAGE_TITLE' => 'Min Controlled Document Utilizations',
  'LNK_NEW_RECORD' => 'Opprett Controlled Document Utilizations',
  'LNK_LIST' => 'Vis Controlled Document Utilizations',
  'LNK_IMPORT_CDU_CD_UTILIZATION' => 'Import Controlled Document Utilizations',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Controlled Document Utilizations',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_CDU_CD_UTILIZATION_SUBPANEL_TITLE' => 'Controlled Document Utilizations',
  'LBL_NEW_FORM_TITLE' => 'Ny Controlled Document Utilizations',
  'LNK_IMPORT_VCARD' => 'Import Controlled Document Utilizations vCard',
  'LBL_IMPORT' => 'Import Controlled Document Utilizations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Controlled Document Utilizations record by importing a vCard from your file system.',
  'LBL_CDU_CD_UTILIZATION_FOCUS_DRAWER_DASHBOARD' => 'Controlled Document Utilizations Focus Drawer',
  'LBL_MONTH_END' => 'Month End',
  'LBL_NUMBER_OF_PROCEDURES' => 'Number of Procedures',
  'LBL_NUMBER_OF_TESTS' => 'Number of Tests',
  'LBL_NUMBER_OF_ANIMALS' => 'Number of Animals',
  'LBL_CONTROLLED_DOCUMENT_UTILIZATIO' => 'Controlled Document Utilization',
  'LBL_NUMBER_OF_PROCEDURES_2' => 'Number of Procedures',
  'LBL_NUMBER_OF_TESTS_2' => 'Number of Tests',
  'LBL_NUMBER_OF_ANIMALS_2' => 'Number of Animals',
  'LBL_CONTROLLED_DOCUMENT_UTI' => 'Controlled Document Utilization',
  'LBL_CDU_CD_UTILIZATION_RECORD_DASHBOARD' => 'Controlled Document Utilizations Record Dashboard',
);