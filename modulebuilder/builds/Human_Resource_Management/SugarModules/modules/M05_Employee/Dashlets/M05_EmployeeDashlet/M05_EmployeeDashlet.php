<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/*********************************************************************************
 * $Id$
 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('include/Dashlets/DashletGeneric.php');
require_once('modules/M05_Employee/M05_Employee.php');

class M05_EmployeeDashlet extends DashletGeneric { 
    function M05_EmployeeDashlet($id, $def = null) {
		global $current_user, $app_strings;
		require('modules/M05_Employee/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'M05_Employee');

        $this->searchFields = $dashletData['M05_EmployeeDashlet']['searchFields'];
        $this->columns = $dashletData['M05_EmployeeDashlet']['columns'];

        $this->seedBean = new M05_Employee();        
    }
}