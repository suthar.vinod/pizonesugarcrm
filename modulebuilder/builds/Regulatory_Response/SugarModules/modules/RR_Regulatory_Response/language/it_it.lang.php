<?php
// created: 2022-12-13 04:46:59
$mod_strings = array (
  'LBL_TEAM' => 'Gruppi',
  'LBL_TEAMS' => 'Gruppi',
  'LBL_TEAM_ID' => 'Id Gruppo',
  'LBL_ASSIGNED_TO_ID' => 'Assegnato a ID:',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LBL_TAGS_LINK' => 'Tag',
  'LBL_TAGS' => 'Tag',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Inserimento',
  'LBL_DATE_MODIFIED' => 'Data Modifica',
  'LBL_MODIFIED' => 'Modificato da',
  'LBL_MODIFIED_ID' => 'Modificato da Id',
  'LBL_MODIFIED_NAME' => 'Modificato da Nome',
  'LBL_CREATED' => 'Creato da',
  'LBL_CREATED_ID' => 'Creato da Id',
  'LBL_DOC_OWNER' => 'Proprietario del documento',
  'LBL_USER_FAVORITES' => 'Preferenze Utenti',
  'LBL_DESCRIPTION' => 'Descrizione',
  'LBL_DELETED' => 'Eliminato',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Creato da utente',
  'LBL_MODIFIED_USER' => 'Modificato da utente',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Modifica',
  'LBL_REMOVE' => 'Rimuovi',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificato dal Nome',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Regulatory Responses Lista',
  'LBL_MODULE_NAME' => 'Regulatory Responses',
  'LBL_MODULE_TITLE' => 'Regulatory Responses',
  'LBL_MODULE_NAME_SINGULAR' => 'Regulatory Response',
  'LBL_HOMEPAGE_TITLE' => 'Mio Regulatory Responses',
  'LNK_NEW_RECORD' => 'Crea Regulatory Response',
  'LNK_LIST' => 'Visualizza Regulatory Responses',
  'LNK_IMPORT_RR_REGULATORY_RESPONSE' => 'Import Regulatory Responses',
  'LBL_SEARCH_FORM_TITLE' => 'Ricerca Regulatory Response',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Cronologia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Attività',
  'LBL_RR_REGULATORY_RESPONSE_SUBPANEL_TITLE' => 'Regulatory Responses',
  'LBL_NEW_FORM_TITLE' => 'Nuovo Regulatory Response',
  'LNK_IMPORT_VCARD' => 'Import Regulatory Response vCard',
  'LBL_IMPORT' => 'Import Regulatory Responses',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Regulatory Response record by importing a vCard from your file system.',
);