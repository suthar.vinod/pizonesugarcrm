<?php
// created: 2022-12-13 04:46:59
$mod_strings = array (
  'LBL_TEAM' => 'Komandos',
  'LBL_TEAMS' => 'Komandos',
  'LBL_TEAM_ID' => 'Komandos Id',
  'LBL_ASSIGNED_TO_ID' => 'Priskirto vartotojo ID',
  'LBL_ASSIGNED_TO_NAME' => 'Kam priskirta',
  'LBL_TAGS_LINK' => 'Žymės',
  'LBL_TAGS' => 'Žymės',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Sukūrimo data',
  'LBL_DATE_MODIFIED' => 'Modifikavimo data',
  'LBL_MODIFIED' => 'Redagavo',
  'LBL_MODIFIED_ID' => 'Redaguotojo Id',
  'LBL_MODIFIED_NAME' => 'Redaguotojo vardas',
  'LBL_CREATED' => 'Sukūrė',
  'LBL_CREATED_ID' => 'Kūrėjo ID',
  'LBL_DOC_OWNER' => 'Dokumento savininkas',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Aprašas',
  'LBL_DELETED' => 'Panaikintas',
  'LBL_NAME' => 'Pavadinimas',
  'LBL_CREATED_USER' => 'Sukūręs vartotojas',
  'LBL_MODIFIED_USER' => 'Modifikavęs vartotojas.',
  'LBL_LIST_NAME' => 'Pavadinimas',
  'LBL_EDIT_BUTTON' => 'Redaguoti',
  'LBL_REMOVE' => 'Pašalinti',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikuotojo vardas',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Regulatory Responses Sąrašas',
  'LBL_MODULE_NAME' => 'Regulatory Responses',
  'LBL_MODULE_TITLE' => 'Regulatory Responses',
  'LBL_MODULE_NAME_SINGULAR' => 'Regulatory Response',
  'LBL_HOMEPAGE_TITLE' => 'Mano Regulatory Responses',
  'LNK_NEW_RECORD' => 'Sukurti Regulatory Response',
  'LNK_LIST' => 'View Regulatory Responses',
  'LNK_IMPORT_RR_REGULATORY_RESPONSE' => 'Import Regulatory Responses',
  'LBL_SEARCH_FORM_TITLE' => 'Paieška Regulatory Response',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Rodyti istoriją',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Priminimai',
  'LBL_RR_REGULATORY_RESPONSE_SUBPANEL_TITLE' => 'Regulatory Responses',
  'LBL_NEW_FORM_TITLE' => 'Naujas Regulatory Response',
  'LNK_IMPORT_VCARD' => 'Import Regulatory Response vCard',
  'LBL_IMPORT' => 'Import Regulatory Responses',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Regulatory Response record by importing a vCard from your file system.',
);