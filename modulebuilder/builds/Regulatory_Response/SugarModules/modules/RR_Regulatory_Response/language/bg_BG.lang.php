<?php
// created: 2022-12-13 04:46:59
$mod_strings = array (
  'LBL_TEAM' => 'Екип',
  'LBL_TEAMS' => 'Екип',
  'LBL_TEAM_ID' => 'Екип',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Потребител',
  'LBL_TAGS_LINK' => 'Етикети',
  'LBL_TAGS' => 'Етикети',
  'LBL_ID' => 'Идентификатор',
  'LBL_DATE_ENTERED' => 'Създадено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_DOC_OWNER' => 'Собственик на документа',
  'LBL_USER_FAVORITES' => 'Потребители които харесват',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Изтрити',
  'LBL_NAME' => 'Име',
  'LBL_CREATED_USER' => 'Създадено от',
  'LBL_MODIFIED_USER' => 'Модифицирано от',
  'LBL_LIST_NAME' => 'Име',
  'LBL_EDIT_BUTTON' => 'Редактирай',
  'LBL_REMOVE' => 'Премахни',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Модифицирано от',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Regulatory Responses Списък',
  'LBL_MODULE_NAME' => 'Regulatory Responses',
  'LBL_MODULE_TITLE' => 'Regulatory Responses',
  'LBL_MODULE_NAME_SINGULAR' => 'Regulatory Response',
  'LBL_HOMEPAGE_TITLE' => 'Мои Regulatory Responses',
  'LNK_NEW_RECORD' => 'Създай Regulatory Response',
  'LNK_LIST' => 'Изглед Regulatory Responses',
  'LNK_IMPORT_RR_REGULATORY_RESPONSE' => 'Import Regulatory Responses',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Regulatory Response',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_RR_REGULATORY_RESPONSE_SUBPANEL_TITLE' => 'Regulatory Responses',
  'LBL_NEW_FORM_TITLE' => 'Нов Regulatory Response',
  'LNK_IMPORT_VCARD' => 'Import Regulatory Response vCard',
  'LBL_IMPORT' => 'Import Regulatory Responses',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Regulatory Response record by importing a vCard from your file system.',
);