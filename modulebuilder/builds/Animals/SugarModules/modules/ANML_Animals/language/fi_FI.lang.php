<?php
// created: 2022-12-13 04:46:39
$mod_strings = array (
  'LBL_TEAM' => 'Tiimit',
  'LBL_TEAMS' => 'Tiimit',
  'LBL_TEAM_ID' => 'Tiimin tunnus',
  'LBL_ASSIGNED_TO_ID' => 'Vastuukäyttäjän ID',
  'LBL_ASSIGNED_TO_NAME' => 'Vastuuhenkilö',
  'LBL_TAGS_LINK' => 'Tagit',
  'LBL_TAGS' => 'Tagit',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Luontipäivä',
  'LBL_DATE_MODIFIED' => 'Muokkattu viimeksi',
  'LBL_MODIFIED' => 'Muokannut',
  'LBL_MODIFIED_ID' => 'Muokkaajan ID',
  'LBL_MODIFIED_NAME' => 'Muokkaajan nimi',
  'LBL_CREATED' => 'Luonut',
  'LBL_CREATED_ID' => 'Luojan ID',
  'LBL_DOC_OWNER' => 'Tiedoston omistaja',
  'LBL_USER_FAVORITES' => 'Käyttäjät, jotka ottivat suosikikseen',
  'LBL_DESCRIPTION' => 'Kuvaus',
  'LBL_DELETED' => 'Poistettu',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Luoja',
  'LBL_MODIFIED_USER' => 'Muokkaaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Muokkaa',
  'LBL_REMOVE' => 'Poista',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muokkaajan nimi',
  'LBL_LIST_FORM_TITLE' => 'Animals Lista',
  'LBL_MODULE_NAME' => 'Animals',
  'LBL_MODULE_TITLE' => 'Animals',
  'LBL_MODULE_NAME_SINGULAR' => 'Animal',
  'LBL_HOMEPAGE_TITLE' => 'Oma Animals',
  'LNK_NEW_RECORD' => 'Luo Animal',
  'LNK_LIST' => 'Näkymä Animals',
  'LNK_IMPORT_ANML_ANIMALS' => 'Import Animals',
  'LBL_SEARCH_FORM_TITLE' => 'Etsi Animal',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Näytä historia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteetit',
  'LBL_ANML_ANIMALS_SUBPANEL_TITLE' => 'Animals',
  'LBL_NEW_FORM_TITLE' => 'Uusi Animal',
  'LNK_IMPORT_VCARD' => 'Import Animal vCard',
  'LBL_IMPORT' => 'Import Animals',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Animal record by importing a vCard from your file system.',
);