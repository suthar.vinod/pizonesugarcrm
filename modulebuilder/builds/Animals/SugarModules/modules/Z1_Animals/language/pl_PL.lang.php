<?php
// created: 2022-12-13 04:46:39
$mod_strings = array (
  'LBL_TEAM' => 'Zespoły',
  'LBL_TEAMS' => 'Zespoły',
  'LBL_TEAM_ID' => 'ID zespołu',
  'LBL_ASSIGNED_TO_ID' => 'Przydzielono do użytkownika (ID)',
  'LBL_ASSIGNED_TO_NAME' => 'Przydzielono do',
  'LBL_TAGS_LINK' => 'Tagi',
  'LBL_TAGS' => 'Tagi',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data utworzenia',
  'LBL_DATE_MODIFIED' => 'Data modyfikacji',
  'LBL_MODIFIED' => 'Zmodyfikowane przez',
  'LBL_MODIFIED_ID' => 'ID modyfikującego',
  'LBL_MODIFIED_NAME' => 'Zmodyfikowane przez (nazwa)',
  'LBL_CREATED' => 'Utworzone przez',
  'LBL_CREATED_ID' => 'ID tworzącego',
  'LBL_DOC_OWNER' => 'Właściciel dokumentu',
  'LBL_USER_FAVORITES' => 'Użytkownicy, którzy dodali rekord do ulubionych',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Usunięto',
  'LBL_NAME' => 'Nazwa',
  'LBL_CREATED_USER' => 'Utworzone przez użytkownika',
  'LBL_MODIFIED_USER' => 'Zmodyfikowane przez użytkownika',
  'LBL_LIST_NAME' => 'Nazwa',
  'LBL_EDIT_BUTTON' => 'Edytuj',
  'LBL_REMOVE' => 'Usuń',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmodyfikowane przez (nazwa)',
  'LBL_LIST_FORM_TITLE' => 'Animals Lista',
  'LBL_MODULE_NAME' => 'Animals',
  'LBL_MODULE_TITLE' => 'Animals',
  'LBL_MODULE_NAME_SINGULAR' => 'Animal',
  'LBL_HOMEPAGE_TITLE' => 'Moje Animals',
  'LNK_NEW_RECORD' => 'Utwórz Animal',
  'LNK_LIST' => 'Widok Animals',
  'LNK_IMPORT_Z1_VIVARIUM' => 'Import Animal',
  'LBL_SEARCH_FORM_TITLE' => 'Wyszukiwanie Animal',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historia zmian',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Wydarzenia',
  'LBL_Z1_VIVARIUM_SUBPANEL_TITLE' => 'Animals',
  'LBL_NEW_FORM_TITLE' => 'Nowy Animal',
  'LNK_IMPORT_VCARD' => 'Import Animal vCard',
  'LBL_IMPORT' => 'Import Animals',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Animal record by importing a vCard from your file system.',
  'LBL_ANIMAL_ID' => 'Animal ID',
  'LBL_ROOM_ID' => 'Room ID',
  'LBL_BODYWEIGHT' => 'Bodyweight',
  'LBL_LAST_DATE_WEIGHED' => 'Last Date Weighed',
  'LBL_HUSBANDRY_STATUS' => 'Husbandry Status',
  'LBL_ABNORMALITY' => 'Abnormality',
  'LBL_VENDOR' => 'Vendor',
  'LBL_SEX' => 'Sex',
  'LBL_BREED' => 'Breed',
  'LBL_DATE_RECEIVED' => 'Date Received',
  'LBL_USDA_CLASS' => 'USDA Class',
  'LBL_USDA_ID' => 'USDA ID',
  'LBL_SPECIES' => 'Species',
  'LBL_INITIAL_LAWSONIA_VACCINATION' => 'Initial Lawsonia Vaccination',
  'LBL_LAST_HOOF_TRIM' => 'Last Hoof Trim',
  'LBL_NEXT_HOOF_TRIM' => 'Next Hoof Trim',
  'LBL_BOOSTER_1' => 'Booster 1',
  'LBL_BOOSTER_2' => 'Booster 2',
  'LBL_BOOSTER_3' => 'Booster 3',
  'LBL_BOOSTER_4' => 'Booster 4',
  'LBL_BOOSTER_5' => 'Booster 5',
  'LBL_BOOSTER_6' => 'Booster 6',
  'LBL_DECEASED' => 'Deceased',
  'LNK_IMPORT_Z1_ANIMALS' => 'Import Animal',
  'LBL_Z1_ANIMALS_SUBPANEL_TITLE' => 'Animals',
);