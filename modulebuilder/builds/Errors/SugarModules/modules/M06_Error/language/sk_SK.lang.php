<?php
// created: 2022-12-13 04:46:52
$mod_strings = array (
  'LBL_TEAM' => 'Tímy',
  'LBL_TEAMS' => 'Tímy',
  'LBL_TEAM_ID' => 'ID tímu',
  'LBL_ASSIGNED_TO_ID' => 'Pridelené užívateľské ID',
  'LBL_ASSIGNED_TO_NAME' => 'Pridelený k',
  'LBL_CREATED' => 'Vytvoril podľa',
  'LBL_CREATED_ID' => 'Vytvoril podľa ID',
  'LBL_CREATED_USER' => 'Vytvorené používateľom',
  'LBL_DATE_ENTERED' => 'Dátum vytvorenia',
  'LBL_DATE_MODIFIED' => 'Dátum úpravy',
  'LBL_DELETED' => 'Vymazaný',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_EDIT_BUTTON' => 'Upraviť',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Názov',
  'LBL_MODIFIED' => 'Zmenil',
  'LBL_MODIFIED_ID' => 'Zmenil podľa ID',
  'LBL_MODIFIED_NAME' => 'Zmenil podľa mena',
  'LBL_MODIFIED_USER' => 'Zmenené používateľom',
  'LBL_NAME' => 'Názov dokumentu',
  'LBL_REMOVE' => 'Odstrániť',
  'LBL_LIST_FORM_TITLE' => 'Errors Zoznam',
  'LBL_MODULE_NAME' => 'Errors',
  'LBL_MODULE_TITLE' => 'Errors',
  'LBL_MODULE_NAME_SINGULAR' => 'Error',
  'LBL_HOMEPAGE_TITLE' => 'Moje Errors',
  'LNK_NEW_RECORD' => 'Vytvoriť Error',
  'LNK_LIST' => 'zobrazenie Errors',
  'LNK_IMPORT_M06_ERROR' => 'Import Error',
  'LBL_SEARCH_FORM_TITLE' => 'Vyhľadávanie Error',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobraziť Históriu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_M06_ERROR_SUBPANEL_TITLE' => 'Errors',
  'LBL_NEW_FORM_TITLE' => 'Nový Error',
  'LNK_IMPORT_VCARD' => 'Import Error vCard',
  'LBL_IMPORT' => 'Import Errors',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Error record by importing a vCard from your file system.',
);