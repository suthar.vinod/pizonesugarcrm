<?php
$module_name = 'CAPA_CAPA';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'CAPA_CAPA',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
                'size' => 'large',
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'date_initiated',
                'label' => 'LBL_DATE_INITIATED',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'ca_andor_pa',
                'label' => 'LBL_CA_ANDOR_PA',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'issue',
                'studio' => 'visible',
                'label' => 'LBL_ISSUE',
                'span' => 12,
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'capa_owner',
                'studio' => 'visible',
                'label' => 'LBL_CAPA_OWNER',
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'study_director',
                'studio' => 'visible',
                'label' => 'LBL_STUDY_DIRECTOR',
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'non_conforming_event',
                'studio' => 'visible',
                'label' => 'LBL_NON_CONFORMING_EVENT',
                'span' => 12,
              ),
              6 => 
              array (
                'readonly' => false,
                'name' => 'initial_scope',
                'studio' => 'visible',
                'label' => 'LBL_INITIAL_SCOPE',
                'span' => 12,
              ),
              7 => 
              array (
                'readonly' => false,
                'name' => 'correction',
                'studio' => 'visible',
                'label' => 'LBL_CORRECTION',
                'span' => 12,
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'frequency_score',
                'label' => 'LBL_FREQUENCY_SCORE',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'severity_score',
                'label' => 'LBL_SEVERITY_SCORE',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'risk_assessment_score',
                'label' => 'LBL_RISK_ASSESSMENT_SCORE',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'risk_assessment',
                'label' => 'LBL_RISK_ASSESSMENT',
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'root_cause_investigation',
                'studio' => 'visible',
                'label' => 'LBL_ROOT_CAUSE_INVESTIGATION',
                'span' => 12,
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'corrective_actions_proposed',
                'studio' => 'visible',
                'label' => 'LBL_CORRECTIVE_ACTIONS_PROPOSED',
                'span' => 12,
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'corrective_action_target_due',
                'label' => 'LBL_CORRECTIVE_ACTION_TARGET_DUE',
              ),
              2 => 
              array (
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'proposed_verification_effectiv',
                'studio' => 'visible',
                'label' => 'LBL_PROPOSED_VERIFICATION_EFFECTIV',
                'span' => 12,
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'effectiveness_verification_due',
                'label' => 'LBL_EFFECTIVENESS_VERIFICATION_DUE',
              ),
              5 => 
              array (
              ),
            ),
          ),
          5 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL4',
            'label' => 'LBL_RECORDVIEW_PANEL4',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'preventative_actions_proposed',
                'studio' => 'visible',
                'label' => 'LBL_PREVENTATIVE_ACTIONS_PROPOSED',
                'span' => 12,
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'preventative_action_target_due',
                'label' => 'LBL_PREVENTATIVE_ACTION_TARGET_DUE',
              ),
              2 => 
              array (
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'rationale_or_discussion_of_pa',
                'studio' => 'visible',
                'label' => 'LBL_RATIONALE_OR_DISCUSSION_OF_PA',
                'span' => 12,
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'proposed_verification_pa',
                'studio' => 'visible',
                'label' => 'LBL_PROPOSED_VERIFICATION_PA',
                'span' => 12,
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'effectiveness_verification_pa',
                'label' => 'LBL_EFFECTIVENESS_VERIFICATION_PA',
              ),
              6 => 
              array (
              ),
            ),
          ),
          6 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL5',
            'label' => 'LBL_RECORDVIEW_PANEL5',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'corrective_action_effective',
                'studio' => 'visible',
                'label' => 'LBL_CORRECTIVE_ACTION_EFFECTIVE',
                'span' => 12,
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'progress_update_ca',
                'label' => 'LBL_PROGRESS_UPDATE_CA',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'effectiveness_review_ca',
                'label' => 'LBL_EFFECTIVENESS_REVIEW_CA',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'further_corrective_action_requ',
                'label' => 'LBL_FURTHER_CORRECTIVE_ACTION_REQU',
              ),
              4 => 
              array (
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'preventative_action_effective',
                'studio' => 'visible',
                'label' => 'LBL_PREVENTATIVE_ACTION_EFFECTIVE',
                'span' => 12,
              ),
              6 => 
              array (
                'readonly' => false,
                'name' => 'progress_update_pa',
                'label' => 'LBL_PROGRESS_UPDATE_PA',
              ),
              7 => 
              array (
                'readonly' => false,
                'name' => 'effectiveness_review_pa',
                'label' => 'LBL_EFFECTIVENESS_REVIEW_PA',
              ),
              8 => 
              array (
                'readonly' => false,
                'name' => 'further_preventative_action_re',
                'label' => 'LBL_FURTHER_PREVENTATIVE_ACTION_RE',
              ),
              9 => 
              array (
              ),
              10 => 
              array (
                'readonly' => false,
                'name' => 'capa_cancellation_justificatio',
                'studio' => 'visible',
                'label' => 'LBL_CAPA_CANCELLATION_JUSTIFICATIO',
                'span' => 12,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
