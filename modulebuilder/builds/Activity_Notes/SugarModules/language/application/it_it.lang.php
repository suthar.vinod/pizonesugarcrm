<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['an01_activity_notes_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['an01_activity_notes_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['an01_activity_notes_category_dom']['Sales'] = 'Vendite';
$app_list_strings['an01_activity_notes_category_dom'][''] = '';
$app_list_strings['an01_activity_notes_subcategory_dom']['Marketing Collateral'] = 'Materiale di Marketing';
$app_list_strings['an01_activity_notes_subcategory_dom']['Product Brochures'] = 'Brochure Prodotto';
$app_list_strings['an01_activity_notes_subcategory_dom']['FAQ'] = 'Domande Più Frequenti';
$app_list_strings['an01_activity_notes_subcategory_dom'][''] = '';
$app_list_strings['an01_activity_notes_status_dom']['Active'] = 'Attivo';
$app_list_strings['an01_activity_notes_status_dom']['Draft'] = 'Bozza';
$app_list_strings['an01_activity_notes_status_dom']['FAQ'] = 'Domande Più Frequenti';
$app_list_strings['an01_activity_notes_status_dom']['Expired'] = 'Scaduto';
$app_list_strings['an01_activity_notes_status_dom']['Under Review'] = 'In Revisione';
$app_list_strings['an01_activity_notes_status_dom']['Pending'] = 'In attesa';
$app_list_strings['moduleList']['AN01_Activity_Notes'] = 'Activity Notes';
$app_list_strings['moduleListSingular']['AN01_Activity_Notes'] = 'Activity Note';
