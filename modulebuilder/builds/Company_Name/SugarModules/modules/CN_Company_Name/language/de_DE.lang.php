<?php
// created: 2022-12-13 04:46:44
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team-ID',
  'LBL_ASSIGNED_TO_ID' => 'Zugewiesene Benutzer-Id',
  'LBL_ASSIGNED_TO_NAME' => 'Zugewiesen an',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Erstellungsdatum',
  'LBL_DATE_MODIFIED' => 'Änderungsdatum',
  'LBL_MODIFIED' => 'Geändert von',
  'LBL_MODIFIED_ID' => 'Geändert von ID',
  'LBL_MODIFIED_NAME' => 'Geändert von Name',
  'LBL_CREATED' => 'Erstellt von:',
  'LBL_CREATED_ID' => 'Ersteller',
  'LBL_DOC_OWNER' => 'Dokument-Eigentümer',
  'LBL_USER_FAVORITES' => 'Benutzer mit Favoriten',
  'LBL_DESCRIPTION' => 'Beschreibung',
  'LBL_DELETED' => 'Gelöscht',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Erstellt von',
  'LBL_MODIFIED_USER' => 'Geändert von',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Bearbeiten',
  'LBL_REMOVE' => 'Entfernen',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Geändert von Name',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Company Names Liste',
  'LBL_MODULE_NAME' => 'Company Names',
  'LBL_MODULE_TITLE' => 'Company Names',
  'LBL_MODULE_NAME_SINGULAR' => 'Company Name',
  'LBL_HOMEPAGE_TITLE' => 'Mein Company Names',
  'LNK_NEW_RECORD' => 'Erstellen Company Name',
  'LNK_LIST' => 'Ansicht Company Names',
  'LNK_IMPORT_CN_COMPANY_NAME' => 'Import Company Names',
  'LBL_SEARCH_FORM_TITLE' => 'Suchen Company Name',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Verlauf ansehen',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitäten-Stream',
  'LBL_CN_COMPANY_NAME_SUBPANEL_TITLE' => 'Company Names',
  'LBL_NEW_FORM_TITLE' => 'Neu Company Name',
  'LNK_IMPORT_VCARD' => 'Import Company Name vCard',
  'LBL_IMPORT' => 'Import Company Names',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Company Name record by importing a vCard from your file system.',
);