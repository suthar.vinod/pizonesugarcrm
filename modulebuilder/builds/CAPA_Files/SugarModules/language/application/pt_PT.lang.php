<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['cf_capa_files_category_dom']['Marketing'] = 'Página de Marketing';
$app_list_strings['cf_capa_files_category_dom']['Knowledege Base'] = 'Base de Conhecimento';
$app_list_strings['cf_capa_files_category_dom']['Sales'] = 'Página de Vendas';
$app_list_strings['cf_capa_files_category_dom'][''] = '';
$app_list_strings['cf_capa_files_subcategory_dom']['Marketing Collateral'] = 'Materiais Colaterais de Marketing';
$app_list_strings['cf_capa_files_subcategory_dom']['Product Brochures'] = 'Brochuras de Produtos';
$app_list_strings['cf_capa_files_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['cf_capa_files_subcategory_dom'][''] = '';
$app_list_strings['cf_capa_files_status_dom']['Active'] = 'Ativo';
$app_list_strings['cf_capa_files_status_dom']['Draft'] = 'Rascunho';
$app_list_strings['cf_capa_files_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['cf_capa_files_status_dom']['Expired'] = 'Expirou';
$app_list_strings['cf_capa_files_status_dom']['Under Review'] = 'Em Revisão';
$app_list_strings['cf_capa_files_status_dom']['Pending'] = 'Pendente';
$app_list_strings['moduleList']['CF_CAPA_Files'] = 'CAPA Files';
$app_list_strings['moduleListSingular']['CF_CAPA_Files'] = 'CAPA File';
