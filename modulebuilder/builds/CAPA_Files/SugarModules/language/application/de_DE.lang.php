<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['cf_capa_files_category_dom']['Marketing'] = 'Marketingseite';
$app_list_strings['cf_capa_files_category_dom']['Knowledege Base'] = 'Wissensdatenbank';
$app_list_strings['cf_capa_files_category_dom']['Sales'] = 'Verkaufsseite';
$app_list_strings['cf_capa_files_category_dom'][''] = '';
$app_list_strings['cf_capa_files_subcategory_dom']['Marketing Collateral'] = 'Werbematerial';
$app_list_strings['cf_capa_files_subcategory_dom']['Product Brochures'] = 'Produktbroschüren';
$app_list_strings['cf_capa_files_subcategory_dom']['FAQ'] = 'Häufig gestellte Fragen';
$app_list_strings['cf_capa_files_subcategory_dom'][''] = '';
$app_list_strings['cf_capa_files_status_dom']['Active'] = 'Aktiv';
$app_list_strings['cf_capa_files_status_dom']['Draft'] = 'Entwurf';
$app_list_strings['cf_capa_files_status_dom']['FAQ'] = 'Häufig gestellte Fragen';
$app_list_strings['cf_capa_files_status_dom']['Expired'] = 'Abgelaufen';
$app_list_strings['cf_capa_files_status_dom']['Under Review'] = 'Wird überprüft';
$app_list_strings['cf_capa_files_status_dom']['Pending'] = 'Ausstehend';
$app_list_strings['moduleList']['CF_CAPA_Files'] = 'CAPA Files';
$app_list_strings['moduleListSingular']['CF_CAPA_Files'] = 'CAPA File';
