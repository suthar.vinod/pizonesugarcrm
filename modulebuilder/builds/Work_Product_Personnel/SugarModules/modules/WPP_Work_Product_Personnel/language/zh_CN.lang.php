<?php
// created: 2022-12-13 04:47:20
$mod_strings = array (
  'LBL_TEAM' => '团队',
  'LBL_TEAMS' => '团队',
  'LBL_TEAM_ID' => '团队ID',
  'LBL_ASSIGNED_TO_ID' => '负责人ID',
  'LBL_ASSIGNED_TO_NAME' => '负责人',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '创建日期',
  'LBL_DATE_MODIFIED' => '修改日期',
  'LBL_MODIFIED' => '修改人',
  'LBL_MODIFIED_ID' => '有Id修改',
  'LBL_MODIFIED_NAME' => '修改人姓名',
  'LBL_CREATED' => '由创建',
  'LBL_CREATED_ID' => '创建人ID',
  'LBL_DOC_OWNER' => '文档拥有者',
  'LBL_USER_FAVORITES' => '用户最喜欢的',
  'LBL_DESCRIPTION' => '说明',
  'LBL_DELETED' => '删除',
  'LBL_NAME' => '名称',
  'LBL_CREATED_USER' => '创建人',
  'LBL_MODIFIED_USER' => '修改人',
  'LBL_LIST_NAME' => '名称',
  'LBL_EDIT_BUTTON' => '编辑',
  'LBL_REMOVE' => '移除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '修改人姓名',
  'LBL_LIST_FORM_TITLE' => 'Work Product Personnel  列表',
  'LBL_MODULE_NAME' => 'Work Product Personnel ',
  'LBL_MODULE_TITLE' => 'Work Product Personnel ',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Person',
  'LBL_HOMEPAGE_TITLE' => '我的 Work Product Personnel ',
  'LNK_NEW_RECORD' => '新增 Work Product Person',
  'LNK_LIST' => '显示 Work Product Personnel ',
  'LNK_IMPORT_WPP_WPP2' => 'Import Work_Product_Personnel',
  'LBL_SEARCH_FORM_TITLE' => '搜索 Work Product Person',
  'LBL_HISTORY_SUBPANEL_TITLE' => '查看历史记录',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活动',
  'LBL_WPP_WPP2_SUBPANEL_TITLE' => 'Work_Product_Personnel',
  'LBL_NEW_FORM_TITLE' => '新建 Work Product Person',
  'LNK_IMPORT_VCARD' => 'Import Work Product Person vCard',
  'LBL_IMPORT' => 'Import Work Product Personnel ',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Person record by importing a vCard from your file system.',
  'LNK_IMPORT_WPP_WORK_PRODUCT_PERSONNEL' => 'Import Work Product Person',
  'LBL_WPP_WORK_PRODUCT_PERSONNEL_SUBPANEL_TITLE' => 'Work Product Personnel ',
);