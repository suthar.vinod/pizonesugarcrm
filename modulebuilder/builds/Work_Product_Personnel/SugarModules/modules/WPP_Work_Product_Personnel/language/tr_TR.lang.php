<?php
// created: 2022-12-13 04:47:20
$mod_strings = array (
  'LBL_TEAM' => 'Takımlar',
  'LBL_TEAMS' => 'Takımlar',
  'LBL_TEAM_ID' => 'Takım Id',
  'LBL_ASSIGNED_TO_ID' => 'Atanan Kullanıcı ID',
  'LBL_ASSIGNED_TO_NAME' => 'Atanan Kişi',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Oluşturulma Tarihi',
  'LBL_DATE_MODIFIED' => 'Değiştirilme Tarihi',
  'LBL_MODIFIED' => 'Değiştiren Kişi',
  'LBL_MODIFIED_ID' => 'Değiştiren ID',
  'LBL_MODIFIED_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_CREATED' => 'Oluşturan',
  'LBL_CREATED_ID' => 'Oluşturan ID',
  'LBL_DOC_OWNER' => 'Döküman Sahibi',
  'LBL_USER_FAVORITES' => 'Favorileri Gösteren Kullanıcılar',
  'LBL_DESCRIPTION' => 'Tanım',
  'LBL_DELETED' => 'Silindi',
  'LBL_NAME' => 'İsim',
  'LBL_CREATED_USER' => 'Oluşturan Kullanıcı',
  'LBL_MODIFIED_USER' => 'Değiştiren Kullanıcı',
  'LBL_LIST_NAME' => 'İsim',
  'LBL_EDIT_BUTTON' => 'Değiştir',
  'LBL_REMOVE' => 'Sil',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_LIST_FORM_TITLE' => 'Work Product Personnel  Liste',
  'LBL_MODULE_NAME' => 'Work Product Personnel ',
  'LBL_MODULE_TITLE' => 'Work Product Personnel ',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Person',
  'LBL_HOMEPAGE_TITLE' => 'Benim Work Product Personnel ',
  'LNK_NEW_RECORD' => 'Oluştur Work Product Person',
  'LNK_LIST' => 'Göster Work Product Personnel ',
  'LNK_IMPORT_WPP_WPP2' => 'Import Work_Product_Personnel',
  'LBL_SEARCH_FORM_TITLE' => 'Ara Work Product Person',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Tarihçeyi Gör',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteler',
  'LBL_WPP_WPP2_SUBPANEL_TITLE' => 'Work_Product_Personnel',
  'LBL_NEW_FORM_TITLE' => 'Yeni Work Product Person',
  'LNK_IMPORT_VCARD' => 'Import Work Product Person vCard',
  'LBL_IMPORT' => 'Import Work Product Personnel ',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Person record by importing a vCard from your file system.',
  'LNK_IMPORT_WPP_WORK_PRODUCT_PERSONNEL' => 'Import Work Product Person',
  'LBL_WPP_WORK_PRODUCT_PERSONNEL_SUBPANEL_TITLE' => 'Work Product Personnel ',
);