<?php
// created: 2022-12-13 04:47:19
$mod_strings = array (
  'LBL_TEAM' => 'الفرق',
  'LBL_TEAMS' => 'الفرق',
  'LBL_TEAM_ID' => 'معرّف الفريق',
  'LBL_ASSIGNED_TO_ID' => 'معرّف المستخدم المعين',
  'LBL_ASSIGNED_TO_NAME' => 'تعيين إلى',
  'LBL_ID' => 'المعرّف',
  'LBL_DATE_ENTERED' => 'تاريخ الإنشاء',
  'LBL_DATE_MODIFIED' => 'تاريخ التعديل',
  'LBL_MODIFIED' => 'تم التعديل بواسطة',
  'LBL_MODIFIED_ID' => 'تم التعديل بواسطة المعرّف',
  'LBL_MODIFIED_NAME' => 'تم التعديل بواسطة الاسم',
  'LBL_CREATED' => 'تم الإنشاء بواسطة',
  'LBL_CREATED_ID' => 'تم الإنشاء بواسطة المعرّف',
  'LBL_DOC_OWNER' => 'مالك المستند',
  'LBL_USER_FAVORITES' => 'المستخدمون الذي يفضلون',
  'LBL_DESCRIPTION' => 'الوصف',
  'LBL_DELETED' => 'تم الحذف',
  'LBL_NAME' => 'الاسم',
  'LBL_CREATED_USER' => 'تم الإنشاء بواسطة مستخدم',
  'LBL_MODIFIED_USER' => 'تم التعديل بواسطة مستخدم',
  'LBL_LIST_NAME' => 'الاسم',
  'LBL_EDIT_BUTTON' => 'تحرير',
  'LBL_REMOVE' => 'إزالة',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'تم التعديل بواسطة الاسم',
  'LBL_LIST_FORM_TITLE' => 'Work Product Personnel  القائمة',
  'LBL_MODULE_NAME' => 'Work Product Personnel ',
  'LBL_MODULE_TITLE' => 'Work Product Personnel ',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Person',
  'LBL_HOMEPAGE_TITLE' => 'الخاص بي Work Product Personnel ',
  'LNK_NEW_RECORD' => 'إنشاء Work Product Person',
  'LNK_LIST' => 'عرض Work Product Personnel ',
  'LNK_IMPORT_WPP_WPP2' => 'Import Work_Product_Personnel',
  'LBL_SEARCH_FORM_TITLE' => 'بحث Work Product Person',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'عرض السجل',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'سير النشاط',
  'LBL_WPP_WPP2_SUBPANEL_TITLE' => 'Work_Product_Personnel',
  'LBL_NEW_FORM_TITLE' => 'جديد Work Product Person',
  'LNK_IMPORT_VCARD' => 'Import Work Product Person vCard',
  'LBL_IMPORT' => 'Import Work Product Personnel ',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Person record by importing a vCard from your file system.',
  'LNK_IMPORT_WPP_WORK_PRODUCT_PERSONNEL' => 'Import Work Product Person',
  'LBL_WPP_WORK_PRODUCT_PERSONNEL_SUBPANEL_TITLE' => 'Work Product Personnel ',
);