<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['prodo_product_document_category_dom']['Marketing'] = '行銷';
$app_list_strings['prodo_product_document_category_dom']['Knowledege Base'] = '知識庫';
$app_list_strings['prodo_product_document_category_dom']['Sales'] = '銷售';
$app_list_strings['prodo_product_document_category_dom'][''] = '';
$app_list_strings['prodo_product_document_subcategory_dom']['Marketing Collateral'] = '行銷材料';
$app_list_strings['prodo_product_document_subcategory_dom']['Product Brochures'] = '產品手冊';
$app_list_strings['prodo_product_document_subcategory_dom']['FAQ'] = '常見問題集';
$app_list_strings['prodo_product_document_subcategory_dom'][''] = '';
$app_list_strings['prodo_product_document_status_dom']['Active'] = '使用中';
$app_list_strings['prodo_product_document_status_dom']['Draft'] = '草稿';
$app_list_strings['prodo_product_document_status_dom']['FAQ'] = '常見問題集';
$app_list_strings['prodo_product_document_status_dom']['Expired'] = '已過期';
$app_list_strings['prodo_product_document_status_dom']['Under Review'] = '正在檢閱';
$app_list_strings['prodo_product_document_status_dom']['Pending'] = '擱置中';
$app_list_strings['moduleList']['ProDo_Product_Document'] = 'Product Documents';
$app_list_strings['moduleListSingular']['ProDo_Product_Document'] = 'Product Document';
$app_list_strings['quality_requirement_list']['Certificate'] = 'Certificate';
$app_list_strings['quality_requirement_list']['Certificate of Analysis'] = 'Certificate of Analysis';
$app_list_strings['quality_requirement_list']['Certificate of Compliance'] = 'Certificate of Compliance';
$app_list_strings['quality_requirement_list']['Certificate of Conformity'] = 'Certificate of Conformity';
$app_list_strings['quality_requirement_list']['Certificate of Quality'] = 'Certificate of Quality';
$app_list_strings['quality_requirement_list']['Certificate of Sterility'] = 'Certificate of Sterility';
$app_list_strings['quality_requirement_list']['None'] = 'None';
$app_list_strings['quality_requirement_list']['Performance Report'] = 'Performance Report';
$app_list_strings['quality_requirement_list']['Quality Control Production Certificate'] = 'Quality Control & Production Certificate';
$app_list_strings['quality_requirement_list'][''] = '';
