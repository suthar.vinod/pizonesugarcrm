<?php
// created: 2022-12-13 04:47:13
$mod_strings = array (
  'LBL_TEAM' => 'Екип',
  'LBL_TEAMS' => 'Екип',
  'LBL_TEAM_ID' => 'Екип',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Потребител',
  'LBL_TAGS_LINK' => 'Етикети',
  'LBL_TAGS' => 'Етикети',
  'LBL_ID' => 'Идентификатор',
  'LBL_DATE_ENTERED' => 'Създадено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_DOC_OWNER' => 'Собственик на документа',
  'LBL_USER_FAVORITES' => 'Потребители които харесват',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Изтрити',
  'LBL_NAME' => 'Име',
  'LBL_CREATED_USER' => 'Създадено от',
  'LBL_MODIFIED_USER' => 'Модифицирано от',
  'LBL_LIST_NAME' => 'Име',
  'LBL_EDIT_BUTTON' => 'Редактирай',
  'LBL_REMOVE' => 'Премахни',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Модифицирано от',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Work Product Groups Списък',
  'LBL_MODULE_NAME' => 'Work Product Groups',
  'LBL_MODULE_TITLE' => 'Work Product Groups',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Group',
  'LBL_HOMEPAGE_TITLE' => 'Мои Work Product Groups',
  'LNK_NEW_RECORD' => 'Създай Work Product Group',
  'LNK_LIST' => 'Изглед Work Product Groups',
  'LNK_IMPORT_WPG_WORK_PRODUCT_GROUP' => 'Import Work Product Groups',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Work Product Group',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_WPG_WORK_PRODUCT_GROUP_SUBPANEL_TITLE' => 'Work Product Groups',
  'LBL_NEW_FORM_TITLE' => 'Нов Work Product Group',
  'LNK_IMPORT_VCARD' => 'Import Work Product Group vCard',
  'LBL_IMPORT' => 'Import Work Product Groups',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Group record by importing a vCard from your file system.',
  'LBL_WPG_WORK_PRODUCT_GROUP_FOCUS_DRAWER_DASHBOARD' => 'Work Product Groups Focus Drawer',
  'LBL_TOTAL_CAPACITY' => 'Total Capacity (by week)',
);