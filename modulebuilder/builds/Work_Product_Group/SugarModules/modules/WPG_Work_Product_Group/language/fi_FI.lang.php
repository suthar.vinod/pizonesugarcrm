<?php
// created: 2022-12-13 04:47:13
$mod_strings = array (
  'LBL_TEAM' => 'Tiimit',
  'LBL_TEAMS' => 'Tiimit',
  'LBL_TEAM_ID' => 'Tiimin tunnus',
  'LBL_ASSIGNED_TO_ID' => 'Vastuukäyttäjän ID',
  'LBL_ASSIGNED_TO_NAME' => 'Vastuuhenkilö',
  'LBL_TAGS_LINK' => 'Tagit',
  'LBL_TAGS' => 'Tagit',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Luontipäivä',
  'LBL_DATE_MODIFIED' => 'Muokkattu viimeksi',
  'LBL_MODIFIED' => 'Muokannut',
  'LBL_MODIFIED_ID' => 'Muokkaajan ID',
  'LBL_MODIFIED_NAME' => 'Muokkaajan nimi',
  'LBL_CREATED' => 'Luonut',
  'LBL_CREATED_ID' => 'Luojan ID',
  'LBL_DOC_OWNER' => 'Tiedoston omistaja',
  'LBL_USER_FAVORITES' => 'Käyttäjät, jotka ottivat suosikikseen',
  'LBL_DESCRIPTION' => 'Kuvaus',
  'LBL_DELETED' => 'Poistettu',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Luoja',
  'LBL_MODIFIED_USER' => 'Muokkaaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Muokkaa',
  'LBL_REMOVE' => 'Poista',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muokkaajan nimi',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Work Product Groups Lista',
  'LBL_MODULE_NAME' => 'Work Product Groups',
  'LBL_MODULE_TITLE' => 'Work Product Groups',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Group',
  'LBL_HOMEPAGE_TITLE' => 'Oma Work Product Groups',
  'LNK_NEW_RECORD' => 'Luo Work Product Group',
  'LNK_LIST' => 'Näkymä Work Product Groups',
  'LNK_IMPORT_WPG_WORK_PRODUCT_GROUP' => 'Import Work Product Groups',
  'LBL_SEARCH_FORM_TITLE' => 'Etsi Work Product Group',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Näytä historia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteetit',
  'LBL_WPG_WORK_PRODUCT_GROUP_SUBPANEL_TITLE' => 'Work Product Groups',
  'LBL_NEW_FORM_TITLE' => 'Uusi Work Product Group',
  'LNK_IMPORT_VCARD' => 'Import Work Product Group vCard',
  'LBL_IMPORT' => 'Import Work Product Groups',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Group record by importing a vCard from your file system.',
  'LBL_WPG_WORK_PRODUCT_GROUP_FOCUS_DRAWER_DASHBOARD' => 'Work Product Groups Focus Drawer',
  'LBL_TOTAL_CAPACITY' => 'Total Capacity (by week)',
);