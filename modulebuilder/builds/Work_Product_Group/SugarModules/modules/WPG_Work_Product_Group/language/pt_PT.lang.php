<?php
// created: 2022-12-13 04:47:14
$mod_strings = array (
  'LBL_TEAM' => 'Equipas',
  'LBL_TEAMS' => 'Equipas',
  'LBL_TEAM_ID' => 'Id Equipa',
  'LBL_ASSIGNED_TO_ID' => 'ID de Utilizador Atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_TAGS_LINK' => 'Etiquetas',
  'LBL_TAGS' => 'Etiquetas',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data da Criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'ID do Utilizador que Criou',
  'LBL_DOC_OWNER' => 'Proprietário do Documento',
  'LBL_USER_FAVORITES' => 'Utilizadores que adicionaram aos Favoritos',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado por Utilizador',
  'LBL_MODIFIED_USER' => 'Modificado por Utilizador',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado Por Nome',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Work Product Groups Lista',
  'LBL_MODULE_NAME' => 'Work Product Groups',
  'LBL_MODULE_TITLE' => 'Work Product Groups',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Group',
  'LBL_HOMEPAGE_TITLE' => 'Minha Work Product Groups',
  'LNK_NEW_RECORD' => 'Criar Work Product Group',
  'LNK_LIST' => 'Vista Work Product Groups',
  'LNK_IMPORT_WPG_WORK_PRODUCT_GROUP' => 'Import Work Product Groups',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Work Product Group',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Fluxo de Atividades',
  'LBL_WPG_WORK_PRODUCT_GROUP_SUBPANEL_TITLE' => 'Work Product Groups',
  'LBL_NEW_FORM_TITLE' => 'Novo Work Product Group',
  'LNK_IMPORT_VCARD' => 'Import Work Product Group vCard',
  'LBL_IMPORT' => 'Import Work Product Groups',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Group record by importing a vCard from your file system.',
  'LBL_WPG_WORK_PRODUCT_GROUP_FOCUS_DRAWER_DASHBOARD' => 'Work Product Groups Focus Drawer',
  'LBL_TOTAL_CAPACITY' => 'Total Capacity (by week)',
);