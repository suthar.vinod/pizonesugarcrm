<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['SP_Service_Pricing'] = 'Service Pricing';
$app_list_strings['moduleListSingular']['SP_Service_Pricing'] = 'Service Pricing';
$app_list_strings['service_type_list']['Analytical'] = 'Analytical';
$app_list_strings['service_type_list']['Bioskills'] = 'Bioskills';
$app_list_strings['service_type_list']['Clinical Pathology'] = 'Clinical Pathology';
$app_list_strings['service_type_list']['Histology'] = 'Histology';
$app_list_strings['service_type_list']['ISR'] = 'ISR';
$app_list_strings['service_type_list']['IVT'] = 'IVT';
$app_list_strings['service_type_list']['LAIL'] = 'LAIL';
$app_list_strings['service_type_list']['Pathology'] = 'Pathology';
$app_list_strings['service_type_list']['Phamacology'] = 'Phamacology';
$app_list_strings['service_type_list']['QA'] = 'QA';
$app_list_strings['service_type_list']['SAIL'] = 'SAIL';
$app_list_strings['service_type_list']['Sample Prep'] = 'Sample Prep';
$app_list_strings['service_type_list']['Scientific'] = 'Scientific';
$app_list_strings['service_type_list']['Toxicology'] = 'Toxicology';
$app_list_strings['service_type_list'][''] = '';
$app_list_strings['service_subtype_list']['ACT'] = 'ACT';
$app_list_strings['service_subtype_list']['AnesthesiaFluids'] = 'Anesthesia/Fluids';
$app_list_strings['service_subtype_list']['Decalcification'] = 'Decalcification';
$app_list_strings['service_subtype_list']['Deliverable'] = 'Deliverable';
$app_list_strings['service_subtype_list']['Dosing'] = 'Dosing';
$app_list_strings['service_subtype_list']['Equipment'] = 'Equipment';
$app_list_strings['service_subtype_list']['Formulation'] = 'Formulation';
$app_list_strings['service_subtype_list']['Gross Morphometry'] = 'Gross Morphometry';
$app_list_strings['service_subtype_list']['Histology Report'] = 'Histology Report';
$app_list_strings['service_subtype_list']['Histomorphometry'] = 'Histomorphometry';
$app_list_strings['service_subtype_list']['Husbandry'] = 'Husbandry';
$app_list_strings['service_subtype_list']['Induction'] = 'Induction';
$app_list_strings['service_subtype_list']['LM Interpretation'] = 'LM Interpretation';
$app_list_strings['service_subtype_list']['Method Development'] = 'Method Development';
$app_list_strings['service_subtype_list']['Method Validation'] = 'Method Validation';
$app_list_strings['service_subtype_list']['MicroCT'] = 'MicroCT';
$app_list_strings['service_subtype_list']['Miscellaneous'] = 'Miscellaneous';
$app_list_strings['service_subtype_list']['Paraffin Embedding'] = 'Paraffin Embedding';
$app_list_strings['service_subtype_list']['Per Diem'] = 'Per Diem';
$app_list_strings['service_subtype_list']['Plastic Embedding'] = 'Plastic Embedding';
$app_list_strings['service_subtype_list']['Prep'] = 'Prep';
$app_list_strings['service_subtype_list']['Procedure Room'] = 'Procedure Room';
$app_list_strings['service_subtype_list']['Procedure Room Overhead'] = 'Procedure Room Overhead';
$app_list_strings['service_subtype_list']['Recovery'] = 'Recovery';
$app_list_strings['service_subtype_list']['Room Setup'] = 'Room Setup';
$app_list_strings['service_subtype_list']['Sample Analysis'] = 'Sample Analysis';
$app_list_strings['service_subtype_list']['Sectioning and Staining'] = 'Sectioning and Staining';
$app_list_strings['service_subtype_list']['SEM'] = 'SEM';
$app_list_strings['service_subtype_list']['SEM Interpretation'] = 'SEM Interpretation';
$app_list_strings['service_subtype_list']['Shipping'] = 'Shipping';
$app_list_strings['service_subtype_list']['Slide Scan'] = 'Slide Scan';
$app_list_strings['service_subtype_list']['Specimen Collection'] = 'Specimen Collection';
$app_list_strings['service_subtype_list']['Tissue Processing'] = 'Tissue Processing';
$app_list_strings['service_subtype_list']['Tissue Trimming'] = 'Tissue Trimming';
$app_list_strings['service_subtype_list']['Other'] = 'Other';
$app_list_strings['service_subtype_list'][''] = '';
$app_list_strings['service_pricing_roles_list']['Assistant'] = 'Assistant';
$app_list_strings['service_pricing_roles_list']['Cadaver Technician'] = 'Cadaver Technician';
$app_list_strings['service_pricing_roles_list']['Cardiologist'] = 'Cardiologist';
$app_list_strings['service_pricing_roles_list']['Chemist'] = 'Chemist';
$app_list_strings['service_pricing_roles_list']['Consulting Surgeon'] = 'Consulting Surgeon';
$app_list_strings['service_pricing_roles_list']['CT Technician'] = 'CT Technician';
$app_list_strings['service_pricing_roles_list']['Data Recorder'] = 'Data Recorder';
$app_list_strings['service_pricing_roles_list']['Equipment Technician'] = 'Equipment Technician';
$app_list_strings['service_pricing_roles_list']['Float Technician'] = 'Float Technician';
$app_list_strings['service_pricing_roles_list']['Interventionalist'] = 'Interventionalist';
$app_list_strings['service_pricing_roles_list']['Ophthalmologist'] = 'Ophthalmologist';
$app_list_strings['service_pricing_roles_list']['Pathologist'] = 'Pathologist';
$app_list_strings['service_pricing_roles_list']['Perfusionist'] = 'Perfusionist';
$app_list_strings['service_pricing_roles_list']['Prosector'] = 'Prosector';
$app_list_strings['service_pricing_roles_list']['QA Auditor'] = 'QA Auditor';
$app_list_strings['service_pricing_roles_list']['QC Specialist'] = 'QC Specialist';
$app_list_strings['service_pricing_roles_list']['Research Technician'] = 'Research Technician';
$app_list_strings['service_pricing_roles_list']['Study Director'] = 'Study Director';
$app_list_strings['service_pricing_roles_list']['Surgeon'] = 'Surgeon';
$app_list_strings['service_pricing_roles_list']['Surgical Technician'] = 'Surgical Technician';
$app_list_strings['service_pricing_roles_list']['Ultrasonographer'] = 'Ultrasonographer';
$app_list_strings['service_pricing_roles_list']['Veterinarian'] = 'Veterinarian';
$app_list_strings['service_pricing_roles_list'][''] = '';
$app_list_strings['service_format_list']['Combination'] = 'Combination';
$app_list_strings['service_format_list']['Hourly'] = 'Hourly';
$app_list_strings['service_format_list']['Task'] = 'Task';
$app_list_strings['service_format_list'][''] = '';
