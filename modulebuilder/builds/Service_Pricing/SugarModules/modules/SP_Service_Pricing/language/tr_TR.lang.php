<?php
// created: 2022-12-13 04:47:06
$mod_strings = array (
  'LBL_TEAM' => 'Takımlar',
  'LBL_TEAMS' => 'Takımlar',
  'LBL_TEAM_ID' => 'Takım ID',
  'LBL_ASSIGNED_TO_ID' => 'Atanan Kullanıcı ID',
  'LBL_ASSIGNED_TO_NAME' => 'Atanan Kişi',
  'LBL_TAGS_LINK' => 'Etiketler',
  'LBL_TAGS' => 'Etiketler',
  'LBL_ID' => 'KİMLİK',
  'LBL_DATE_ENTERED' => 'Oluşturulma Tarihi',
  'LBL_DATE_MODIFIED' => 'Değiştirilme Tarihi',
  'LBL_MODIFIED' => 'Değiştiren',
  'LBL_MODIFIED_ID' => 'Değiştiren ID',
  'LBL_MODIFIED_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_CREATED' => 'Oluşturan',
  'LBL_CREATED_ID' => 'Oluşturan ID',
  'LBL_DOC_OWNER' => 'Doküman Sahibi',
  'LBL_USER_FAVORITES' => 'Favori Olan Kullanıcılar',
  'LBL_DESCRIPTION' => 'Tanım',
  'LBL_DELETED' => 'Silindi',
  'LBL_NAME' => 'İsim',
  'LBL_CREATED_USER' => 'Oluşturan Kullanıcı',
  'LBL_MODIFIED_USER' => 'Değiştiren Kullanıcı',
  'LBL_LIST_NAME' => 'İsim',
  'LBL_EDIT_BUTTON' => 'Değiştir',
  'LBL_REMOVE' => 'Sil',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Service Pricing Liste',
  'LBL_MODULE_NAME' => 'Service Pricing',
  'LBL_MODULE_TITLE' => 'Service Pricing',
  'LBL_MODULE_NAME_SINGULAR' => 'Service Pricing',
  'LBL_HOMEPAGE_TITLE' => 'Benim Service Pricing',
  'LNK_NEW_RECORD' => 'Oluştur Service Pricing',
  'LNK_LIST' => 'Göster Service Pricing',
  'LNK_IMPORT_SP_SERVICE_PRICING' => 'Import Service Pricing',
  'LBL_SEARCH_FORM_TITLE' => 'Ara Service Pricing',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Tarihçeyi Gör',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteler',
  'LBL_SP_SERVICE_PRICING_SUBPANEL_TITLE' => 'Service Pricing',
  'LBL_NEW_FORM_TITLE' => 'Yeni Service Pricing',
  'LNK_IMPORT_VCARD' => 'Import Service Pricing vCard',
  'LBL_IMPORT' => 'Import Service Pricing',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Service Pricing record by importing a vCard from your file system.',
  'LBL_TYPE_2' => 'Type',
  'LBL_SUBTYPE' => 'Subtype',
  'LBL_ROLES' => 'Role(s)',
  'LBL_ROOM_RMS_ROOM_ID' => 'Room (related Room ID)',
  'LBL_ROOM' => 'Room',
  'LBL_SPECIES_S_SPECIES_ID' => 'Species (related Species ID)',
  'LBL_SPECIES' => 'Species',
  'LBL_FORMAT' => 'Format',
  'LBL_CURRENCY' => 'Currency',
  'LBL_PRICE' => 'Price',
  'LBL_INTERNAL_BARCODE' => 'Internal Barcode',
);