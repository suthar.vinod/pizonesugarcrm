<?php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$dictionary['GE_GDP_Examples'] = array(
    'table' => 'ge_gdp_examples',
    'audited' => true,
    'activity_enabled' => false,
    'fields' => array (
  'area' => 
  array (
    'required' => true,
    'name' => 'area',
    'vname' => 'LBL_AREA',
    'type' => 'enum',
    'massupdate' => true,
    'hidemassupdate' => false,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'enabled',
    'duplicate_merge_dom_value' => '1',
    'audited' => true,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'pii' => false,
    'default' => '',
    'calculated' => false,
    'len' => 100,
    'size' => '20',
    'options' => 'gdp_ex_area_list',
    'dependency' => false,
  ),
  'date_2' => 
  array (
    'required' => true,
    'name' => 'date_2',
    'vname' => 'LBL_DATE_2',
    'type' => 'date',
    'massupdate' => true,
    'hidemassupdate' => false,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'enabled',
    'duplicate_merge_dom_value' => '1',
    'audited' => true,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'pii' => false,
    'calculated' => false,
    'size' => '20',
    'enable_range_search' => false,
  ),
  'document_name' => 
  array (
    'name' => 'document_name',
    'vname' => 'LBL_NAME',
    'type' => 'name',
    'dbType' => 'varchar',
    'len' => '255',
    'required' => true,
    'unified_search' => true,
    'duplicate_on_record_copy' => 'always',
    'full_text_search' => 
    array (
      'enabled' => true,
      'boost' => '0.82',
      'searchable' => true,
    ),
    'massupdate' => false,
    'hidemassupdate' => false,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'false',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => 0,
    'audited' => true,
    'reportable' => true,
    'merge_filter' => 'disabled',
    'pii' => false,
    'default' => '',
    'calculated' => '1',
    'size' => '20',
    'formula' => 'concat(getDropdownValue("gdp_ex_area_list",$area)," ",toString($date_2))',
    'enforced' => true,
  ),
  'type_2' => 
  array (
    'required' => true,
    'name' => 'type_2',
    'vname' => 'LBL_TYPE_2',
    'type' => 'enum',
    'massupdate' => true,
    'hidemassupdate' => false,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'enabled',
    'duplicate_merge_dom_value' => '1',
    'audited' => true,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'pii' => false,
    'default' => '',
    'calculated' => false,
    'len' => 100,
    'size' => '20',
    'options' => 'pos_neg_list',
    'dependency' => false,
  ),
),
    'relationships' => array (
),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('GE_GDP_Examples','GE_GDP_Examples', array('basic','team_security','assignable','taggable','file'));