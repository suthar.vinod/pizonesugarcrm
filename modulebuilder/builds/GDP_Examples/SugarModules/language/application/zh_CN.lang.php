<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['ge_gdp_examples_category_dom']['Marketing'] = '市场营销';
$app_list_strings['ge_gdp_examples_category_dom']['Knowledege Base'] = '知识库';
$app_list_strings['ge_gdp_examples_category_dom']['Sales'] = '销售';
$app_list_strings['ge_gdp_examples_category_dom'][''] = '';
$app_list_strings['ge_gdp_examples_subcategory_dom']['Marketing Collateral'] = '营销资料';
$app_list_strings['ge_gdp_examples_subcategory_dom']['Product Brochures'] = '产品手册';
$app_list_strings['ge_gdp_examples_subcategory_dom']['FAQ'] = '常见问题';
$app_list_strings['ge_gdp_examples_subcategory_dom'][''] = '';
$app_list_strings['ge_gdp_examples_status_dom']['Active'] = '启用';
$app_list_strings['ge_gdp_examples_status_dom']['Draft'] = '草稿';
$app_list_strings['ge_gdp_examples_status_dom']['FAQ'] = '常见问题';
$app_list_strings['ge_gdp_examples_status_dom']['Expired'] = '失效';
$app_list_strings['ge_gdp_examples_status_dom']['Under Review'] = '审查中';
$app_list_strings['ge_gdp_examples_status_dom']['Pending'] = '未决定';
$app_list_strings['moduleList']['GE_GDP_Examples'] = 'GDP Examples';
$app_list_strings['moduleListSingular']['GE_GDP_Examples'] = 'GDP Example';
$app_list_strings['gdp_ex_area_list']['Analytical'] = 'Analytical';
$app_list_strings['gdp_ex_area_list']['Histo'] = 'Histo';
$app_list_strings['gdp_ex_area_list']['IVTClin PathSterilization'] = 'IVT/Clin Path/Sterilization';
$app_list_strings['gdp_ex_area_list']['LA ATVets'] = 'LA AT/Vets';
$app_list_strings['gdp_ex_area_list']['LA ISR OpsVets'] = 'LA ISR Ops/Vets';
$app_list_strings['gdp_ex_area_list']['LA PRTVets'] = 'LA PRT/Vets';
$app_list_strings['gdp_ex_area_list']['LA RTVets'] = 'LA RT/Vets';
$app_list_strings['gdp_ex_area_list']['Pathology'] = 'Pathology';
$app_list_strings['gdp_ex_area_list']['PIFacilities'] = 'PI/Facilities';
$app_list_strings['gdp_ex_area_list']['SA ATVets'] = 'SA AT/Vets';
$app_list_strings['gdp_ex_area_list']['SA RTVets'] = 'SA RT/Vets';
$app_list_strings['gdp_ex_area_list']['Sample Prep'] = 'Sample Prep';
$app_list_strings['gdp_ex_area_list']['Scientific'] = 'Scientific';
$app_list_strings['gdp_ex_area_list'][''] = '';
$app_list_strings['pos_neg_list']['Negative'] = 'Negative';
$app_list_strings['pos_neg_list']['Positive'] = 'Positive';
$app_list_strings['pos_neg_list'][''] = '';
