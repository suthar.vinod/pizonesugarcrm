<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['Prod_Product'] = 'Products';
$app_list_strings['moduleListSingular']['Prod_Product'] = 'Product';
$app_list_strings['product_category_list']['Balloon Catheter'] = 'Balloon Catheter';
$app_list_strings['product_category_list']['Cannula'] = 'Cannula';
$app_list_strings['product_category_list']['Catheter'] = 'Catheter';
$app_list_strings['product_category_list']['Chemical'] = 'Chemical';
$app_list_strings['product_category_list']['Cleaning Agent'] = 'Cleaning Agent';
$app_list_strings['product_category_list']['Contrast'] = 'Contrast';
$app_list_strings['product_category_list']['Drug'] = 'Drug';
$app_list_strings['product_category_list']['Equipment'] = 'Equipment';
$app_list_strings['product_category_list']['Graft'] = 'Graft';
$app_list_strings['product_category_list']['Inflation Device'] = 'Inflation Device';
$app_list_strings['product_category_list']['Reagent'] = 'Reagent';
$app_list_strings['product_category_list']['Sheath'] = 'Sheath';
$app_list_strings['product_category_list']['Solution'] = 'Solution';
$app_list_strings['product_category_list']['Stent'] = 'Stent';
$app_list_strings['product_category_list']['Suture'] = 'Suture';
$app_list_strings['product_category_list']['Wire'] = 'Wire';
$app_list_strings['product_category_list'][''] = '';
$app_list_strings['pro_type_list']['Non stock'] = 'Non-stock';
$app_list_strings['pro_type_list']['Stock'] = 'Stock';
$app_list_strings['pro_type_list'][''] = '';
$app_list_strings['purchase_unit_list']['Each'] = 'Each';
$app_list_strings['purchase_unit_list']['Bottle'] = 'Bottle';
$app_list_strings['purchase_unit_list']['Box'] = 'Box';
$app_list_strings['purchase_unit_list']['Case'] = 'Case';
$app_list_strings['purchase_unit_list'][''] = '';
$app_list_strings['quality_requirement_list']['Certificate'] = 'Certificate';
$app_list_strings['quality_requirement_list']['Certificate of Analysis'] = 'Certificate of Analysis';
$app_list_strings['quality_requirement_list']['Certificate of Compliance'] = 'Certificate of Compliance';
$app_list_strings['quality_requirement_list']['Certificate of Conformity'] = 'Certificate of Conformity';
$app_list_strings['quality_requirement_list']['Certificate of Quality'] = 'Certificate of Quality';
$app_list_strings['quality_requirement_list']['Certificate of Sterility'] = 'Certificate of Sterility';
$app_list_strings['quality_requirement_list']['None'] = 'None';
$app_list_strings['quality_requirement_list']['Performance Report'] = 'Performance Report';
$app_list_strings['quality_requirement_list']['Quality Control Production Certificate'] = 'Quality Control & Production Certificate';
$app_list_strings['quality_requirement_list'][''] = '';
$app_list_strings['pro_subtype_list']['Access Catheter'] = 'Access Catheter';
$app_list_strings['pro_subtype_list']['Angioplasty Balloon'] = 'Angioplasty Balloon';
$app_list_strings['pro_subtype_list']['Aortic Root Cannula'] = 'Aortic Root Cannula';
$app_list_strings['pro_subtype_list']['Arterial Cannula'] = 'Arterial Cannula';
$app_list_strings['pro_subtype_list']['Bare Metal Stent'] = 'Bare Metal Stent';
$app_list_strings['pro_subtype_list']['Barium Contrast'] = 'Barium Contrast';
$app_list_strings['pro_subtype_list']['Central Venous Catheter'] = 'Central Venous Catheter';
$app_list_strings['pro_subtype_list']['Coated'] = 'Coated';
$app_list_strings['pro_subtype_list']['Cutting Balloon'] = 'Cutting Balloon';
$app_list_strings['pro_subtype_list']['Diagnostic Catheter'] = 'Diagnostic Catheter';
$app_list_strings['pro_subtype_list']['Drug Coated Balloon'] = 'Drug Coated Balloon';
$app_list_strings['pro_subtype_list']['Drug Coated Stent'] = 'Drug Coated Stent';
$app_list_strings['pro_subtype_list']['EP Catheter'] = 'EP Catheter';
$app_list_strings['pro_subtype_list']['ePTFE'] = 'ePTFE';
$app_list_strings['pro_subtype_list']['Ethibond'] = 'Ethibond';
$app_list_strings['pro_subtype_list']['Ethilon'] = 'Ethilon';
$app_list_strings['pro_subtype_list']['Exchange Wire'] = 'Exchange Wire';
$app_list_strings['pro_subtype_list']['Extension Catheter'] = 'Extension Catheter';
$app_list_strings['pro_subtype_list']['Foley Catheter'] = 'Foley Catheter';
$app_list_strings['pro_subtype_list']['Guide Catheter'] = 'Guide Catheter';
$app_list_strings['pro_subtype_list']['Guide Wire'] = 'Guide Wire';
$app_list_strings['pro_subtype_list']['High Pressure'] = 'High Pressure';
$app_list_strings['pro_subtype_list']['Injectable'] = 'Injectable';
$app_list_strings['pro_subtype_list']['Introducer Sheath'] = 'Introducer Sheath';
$app_list_strings['pro_subtype_list']['Iodinated Contrast'] = 'Iodinated Contrast';
$app_list_strings['pro_subtype_list']['IV Catheter'] = 'IV Catheter';
$app_list_strings['pro_subtype_list']['Knitted'] = 'Knitted';
$app_list_strings['pro_subtype_list']['Mapping Catheter'] = 'Mapping Catheter';
$app_list_strings['pro_subtype_list']['Marking Wire'] = 'Marking Wire';
$app_list_strings['pro_subtype_list']['Micro Catheter'] = 'Micro Catheter';
$app_list_strings['pro_subtype_list']['Mila Catheter'] = 'Mila Catheter';
$app_list_strings['pro_subtype_list']['Monocryl'] = 'Monocryl';
$app_list_strings['pro_subtype_list']['Oral'] = 'Oral';
$app_list_strings['pro_subtype_list']['OTW over the wire'] = 'OTW (over the wire)';
$app_list_strings['pro_subtype_list']['Patches'] = 'Patches';
$app_list_strings['pro_subtype_list']['PDS'] = 'PDS';
$app_list_strings['pro_subtype_list']['Pediatric Cannula'] = 'Pediatric Cannula';
$app_list_strings['pro_subtype_list']['Pledget'] = 'Pledget';
$app_list_strings['pro_subtype_list']['POBA Balloon'] = 'POBA Balloon';
$app_list_strings['pro_subtype_list']['Pressure Catheter'] = 'Pressure Catheter';
$app_list_strings['pro_subtype_list']['Prolene'] = 'Prolene';
$app_list_strings['pro_subtype_list']['PTA Balloon'] = 'PTA Balloon';
$app_list_strings['pro_subtype_list']['Silk'] = 'Silk';
$app_list_strings['pro_subtype_list']['Steerable Sheath'] = 'Steerable Sheath';
$app_list_strings['pro_subtype_list']['Support Catheter'] = 'Support Catheter';
$app_list_strings['pro_subtype_list']['Support Wire'] = 'Support Wire';
$app_list_strings['pro_subtype_list']['Ti Cron'] = 'Ti-Cron';
$app_list_strings['pro_subtype_list']['Transseptal Catheter'] = 'Transseptal Catheter';
$app_list_strings['pro_subtype_list']['Venous Cannula'] = 'Venous Cannula';
$app_list_strings['pro_subtype_list']['Vessel Cannula'] = 'Vessel Cannula';
$app_list_strings['pro_subtype_list']['Vicryl'] = 'Vicryl';
$app_list_strings['pro_subtype_list']['Woven'] = 'Woven';
$app_list_strings['pro_subtype_list'][''] = '';
