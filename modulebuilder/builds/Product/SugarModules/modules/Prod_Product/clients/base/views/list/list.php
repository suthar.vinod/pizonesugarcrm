<?php
$module_name = 'Prod_Product';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'subtype',
                'label' => 'LBL_SUBTYPE',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'manufacturer',
                'label' => 'LBL_MANUFACTURER',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'vendor',
                'label' => 'LBL_VENDOR',
                'enabled' => true,
                'id' => 'ACCOUNT_ID1_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'cost_per_each',
                'label' => 'LBL_COST_PER_EACH',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'cost_per_each_2',
                'label' => 'LBL_COST_PER_EACH_2',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
              8 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              9 => 
              array (
                'name' => 'quality_requirement',
                'label' => 'LBL_QUALITY_REQUIREMENT',
                'enabled' => true,
                'default' => false,
              ),
              10 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => false,
              ),
              11 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => false,
              ),
              12 => 
              array (
                'name' => 'cost_per_unit',
                'label' => 'LBL_COST_PER_UNIT',
                'enabled' => true,
                'default' => false,
              ),
              13 => 
              array (
                'name' => 'concentration',
                'label' => 'LBL_CONCENTRATION',
                'enabled' => true,
                'default' => false,
              ),
              14 => 
              array (
                'name' => 'concentration_unit',
                'label' => 'LBL_CONCENTRATION_UNIT',
                'enabled' => true,
                'id' => 'U_UNITS_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              15 => 
              array (
                'name' => 'stock',
                'label' => 'LBL_STOCK',
                'enabled' => true,
                'default' => false,
              ),
              16 => 
              array (
                'name' => 'purchase_unit',
                'label' => 'LBL_PURCHASE_UNIT',
                'enabled' => true,
                'default' => false,
              ),
              17 => 
              array (
                'name' => 'allowed_storage',
                'label' => 'LBL_ALLOWED_STORAGE',
                'enabled' => true,
                'default' => false,
              ),
              18 => 
              array (
                'name' => 'stock_quantity',
                'label' => 'LBL_STOCK_QUANTITY',
                'enabled' => true,
                'default' => false,
              ),
              19 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              20 => 
              array (
                'name' => 'product_id_2',
                'label' => 'LBL_PRODUCT_ID_2',
                'enabled' => true,
                'default' => false,
              ),
              21 => 
              array (
                'name' => 'external_barcode',
                'label' => 'LBL_EXTERNAL_BARCODE',
                'enabled' => true,
                'default' => false,
              ),
              22 => 
              array (
                'name' => 'product_name',
                'label' => 'LBL_PRODUCT_NAME',
                'enabled' => true,
                'default' => false,
              ),
              23 => 
              array (
                'name' => 'department',
                'label' => 'LBL_DEPARTMENT',
                'enabled' => true,
                'default' => false,
              ),
              24 => 
              array (
                'name' => 'cost_per_unit_2',
                'label' => 'LBL_COST_PER_UNIT_2',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
              ),
              25 => 
              array (
                'name' => 'unit_quantity',
                'label' => 'LBL_UNIT_QUANTITY',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
