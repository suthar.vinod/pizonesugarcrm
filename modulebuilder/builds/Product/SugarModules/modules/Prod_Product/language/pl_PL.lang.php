<?php
// created: 2022-12-13 04:46:56
$mod_strings = array (
  'LBL_TEAM' => 'Zespoły',
  'LBL_TEAMS' => 'Zespoły',
  'LBL_TEAM_ID' => 'ID zespołu',
  'LBL_ASSIGNED_TO_ID' => 'Przypisano do (ID użytkownika)',
  'LBL_ASSIGNED_TO_NAME' => 'Przydzielono do',
  'LBL_TAGS_LINK' => 'Tagi',
  'LBL_TAGS' => 'Tagi',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data utworzenia',
  'LBL_DATE_MODIFIED' => 'Data modyfikacji',
  'LBL_MODIFIED' => 'Zmodyfikowano przez',
  'LBL_MODIFIED_ID' => 'Zmodyfikowano przez (ID)',
  'LBL_MODIFIED_NAME' => 'Zmodyfikowano przez (nazwa)',
  'LBL_CREATED' => 'Utworzono przez',
  'LBL_CREATED_ID' => 'Utworzone przez (ID)',
  'LBL_DOC_OWNER' => 'Właściciel dokumentu',
  'LBL_USER_FAVORITES' => 'Użytkownicy, którzy dodali rekord do ulubionych',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Usunięto',
  'LBL_NAME' => 'Nazwa',
  'LBL_CREATED_USER' => 'Utworzono przez',
  'LBL_MODIFIED_USER' => 'Zmodyfikowane przez',
  'LBL_LIST_NAME' => 'Nazwa',
  'LBL_EDIT_BUTTON' => 'Edytuj',
  'LBL_REMOVE' => 'Usuń',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmodyfikowano przez (nazwa)',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Products Lista',
  'LBL_MODULE_NAME' => 'Products',
  'LBL_MODULE_TITLE' => 'Products',
  'LBL_MODULE_NAME_SINGULAR' => 'Product',
  'LBL_HOMEPAGE_TITLE' => 'Moje Products',
  'LNK_NEW_RECORD' => 'Utwórz Product',
  'LNK_LIST' => 'Widok Products',
  'LNK_IMPORT_PROD_PRODUCT' => 'Import Products',
  'LBL_SEARCH_FORM_TITLE' => 'Wyszukiwanie Product',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Wyświetl historię',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Panel aktywności',
  'LBL_PROD_PRODUCT_SUBPANEL_TITLE' => 'Products',
  'LBL_NEW_FORM_TITLE' => 'Nowy Product',
  'LNK_IMPORT_VCARD' => 'Import Product vCard',
  'LBL_IMPORT' => 'Import Products',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Product record by importing a vCard from your file system.',
  'LBL_CATEGORY' => 'Category',
  'LBL_TYPE_2' => 'Type',
  'LBL_STOCK_QUANTITY' => 'Stock Quantity',
  'LBL_MANUFACTURER' => 'Manufacturer',
  'LBL_PRODUCT_ID_2' => 'Product ID',
  'LBL_MANUFACTURER_ID' => 'Manufacturer ID',
  'LBL_EXTERNAL_BARCODE' => 'External Barcode',
  'LBL_VENDOR_ACCOUNT_ID' => 'Vendor (related Company ID)',
  'LBL_VENDOR' => 'Vendor',
  'LBL_PURCHASE_UNIT' => 'Purchase Unit',
  'LBL_QUANTITY_IN_UNIT' => 'Quantity in Unit',
  'LBL_CURRENCY' => 'Currency',
  'LBL_COST' => 'Cost',
  'LBL_COST_PER_UNIT' => 'Cost Per Unit',
  'LBL_PURCHASE_UNIT_TEXT' => 'Purchase Unit Text',
  'LBL_PRODUCT_NAME' => 'Product Name',
  'LBL_DEPARTMENT' => 'Department',
  'LBL_PRODUCT_UNIT' => 'Product Unit',
  'LBL_COST_PER_UNIT_2' => 'Cost Per Unit',
  'LBL_UNIT_QUANTITY' => 'Unit Quantity',
  'LBL_COST_PER_EACH' => 'Cost Per Each',
  'LBL_COST_PER_EACH_2' => 'Cost Per Each',
  'LBL_QUALITY_REQUIREMENT' => 'Quality Requirement',
  'LBL_SUBTYPE' => 'Subtype',
  'LBL_CONCENTRATION' => 'Concentration',
  'LBL_CONCENTRATION_UNIT_U_UNITS_ID' => 'Concentration Unit (related Unit ID)',
  'LBL_CONCENTRATION_UNIT' => 'Concentration Unit',
  'LBL_ALLOWED_STORAGE' => 'Allowed Storage',
  'LBL_STOCK' => 'Stock',
);