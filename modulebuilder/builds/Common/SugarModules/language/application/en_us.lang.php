<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['m06_Communication'] = 'APS Communications';
$app_list_strings['moduleList']['m06_Note_To_File'] = 'Notes_To_File';
$app_list_strings['moduleList']['m06_APS_Communication'] = 'APS Communications';
$app_list_strings['moduleListSingular']['m06_Communication'] = 'APS Communication';
$app_list_strings['moduleListSingular']['m06_Note_To_File'] = 'Note_To_File';
$app_list_strings['moduleListSingular']['m06_APS_Communication'] = 'APS Communication';
