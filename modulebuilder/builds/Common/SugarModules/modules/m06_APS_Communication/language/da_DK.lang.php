<?php
// created: 2022-12-13 04:46:41
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team-id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruger-id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_ID' => 'Id',
  'LBL_DATE_ENTERED' => 'Oprettet den',
  'LBL_DATE_MODIFIED' => 'Ændret den',
  'LBL_MODIFIED' => 'Ændret af',
  'LBL_MODIFIED_ID' => 'Ændret af id',
  'LBL_MODIFIED_NAME' => 'Ændret af navn:',
  'LBL_CREATED' => 'Oprettet af',
  'LBL_CREATED_ID' => 'Oprettet af id',
  'LBL_DOC_OWNER' => 'Dokument ejer',
  'LBL_USER_FAVORITES' => 'Brugernes favorit',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Oprettet af bruger:',
  'LBL_MODIFIED_USER' => 'Ændret af bruger:',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ændret af navn:',
  'LBL_LIST_FORM_TITLE' => 'APS Communications Liste',
  'LBL_MODULE_NAME' => 'APS Communications',
  'LBL_MODULE_TITLE' => 'APS Communications',
  'LBL_MODULE_NAME_SINGULAR' => 'APS Communication',
  'LBL_HOMEPAGE_TITLE' => 'Min APS Communications',
  'LNK_NEW_RECORD' => 'Opret APS Communication',
  'LNK_LIST' => 'Vis APS Communications',
  'LNK_IMPORT_M06_COMMUNICATION' => 'Import APS Communication',
  'LBL_SEARCH_FORM_TITLE' => 'Søg APS Communication',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Se historik',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteter',
  'LBL_M06_COMMUNICATION_SUBPANEL_TITLE' => 'APS Communications',
  'LBL_NEW_FORM_TITLE' => 'Ny APS Communication',
  'LNK_IMPORT_VCARD' => 'Import APS Communication vCard',
  'LBL_IMPORT' => 'Import APS Communications',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new APS Communication record by importing a vCard from your file system.',
  'LNK_IMPORT_M06_APS_COMMUNICATION' => 'Import APS Communication',
  'LBL_M06_APS_COMMUNICATION_SUBPANEL_TITLE' => 'APS Communications',
);