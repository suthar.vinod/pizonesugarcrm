<?php
// created: 2022-12-13 04:46:41
$mod_strings = array (
  'LBL_TEAM' => 'Команды',
  'LBL_TEAMS' => 'Команды',
  'LBL_TEAM_ID' => 'ID команды',
  'LBL_ASSIGNED_TO_ID' => 'Ответственный (-ая)',
  'LBL_ASSIGNED_TO_NAME' => 'За кем ответственный (-ая)',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено пользователем:',
  'LBL_MODIFIED_ID' => 'Изменено (Id)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано (Id)',
  'LBL_DOC_OWNER' => 'Владелец Документа',
  'LBL_USER_FAVORITES' => 'Пользователи, которые добавили в Избранное',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано пользователем',
  'LBL_MODIFIED_USER' => 'Изменено пользователем',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Правка',
  'LBL_REMOVE' => 'Удалить',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Изменено',
  'LBL_LIST_FORM_TITLE' => 'APS Communications Прайслист',
  'LBL_MODULE_NAME' => 'APS Communications',
  'LBL_MODULE_TITLE' => 'APS Communications',
  'LBL_MODULE_NAME_SINGULAR' => 'APS Communication',
  'LBL_HOMEPAGE_TITLE' => 'Моя APS Communications',
  'LNK_NEW_RECORD' => 'Создать APS Communication',
  'LNK_LIST' => 'Просмотр APS Communications',
  'LNK_IMPORT_M06_COMMUNICATION' => 'Import APS Communication',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск APS Communication',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_M06_COMMUNICATION_SUBPANEL_TITLE' => 'APS Communications',
  'LBL_NEW_FORM_TITLE' => 'Новая APS Communication',
  'LNK_IMPORT_VCARD' => 'Import APS Communication vCard',
  'LBL_IMPORT' => 'Import APS Communications',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new APS Communication record by importing a vCard from your file system.',
  'LNK_IMPORT_M06_APS_COMMUNICATION' => 'Import APS Communication',
  'LBL_M06_APS_COMMUNICATION_SUBPANEL_TITLE' => 'APS Communications',
);