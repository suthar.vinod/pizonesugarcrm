<?php
// created: 2022-12-13 04:46:41
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen aan ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum aangemaakt',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door Naam',
  'LBL_CREATED' => 'Aangemaakt Door',
  'LBL_CREATED_ID' => 'Gemaakt Door ID',
  'LBL_DOC_OWNER' => 'Eigenaar van document',
  'LBL_USER_FAVORITES' => 'Gebruikers die favoriet gemaakt hebben',
  'LBL_DESCRIPTION' => 'Beschrijving',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_NAME' => 'Naam',
  'LBL_CREATED_USER' => 'Gemaakt door Gebruiker',
  'LBL_MODIFIED_USER' => 'Gewijzigd door Gebruiker',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_EDIT_BUTTON' => 'Wijzigen',
  'LBL_REMOVE' => 'Verwijder',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Gewijzigd door Naam',
  'LBL_LIST_FORM_TITLE' => 'APS Communications Bruto verkoopprijs',
  'LBL_MODULE_NAME' => 'APS Communications',
  'LBL_MODULE_TITLE' => 'APS Communications',
  'LBL_MODULE_NAME_SINGULAR' => 'APS Communication',
  'LBL_HOMEPAGE_TITLE' => 'My APS Communications',
  'LNK_NEW_RECORD' => 'Nieuw(e) APS Communication',
  'LNK_LIST' => 'Bekijken APS Communications',
  'LNK_IMPORT_M06_COMMUNICATION' => 'Import APS Communication',
  'LBL_SEARCH_FORM_TITLE' => 'Search APS Communication',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Bekijk Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_M06_COMMUNICATION_SUBPANEL_TITLE' => 'APS Communications',
  'LBL_NEW_FORM_TITLE' => 'New APS Communication',
  'LNK_IMPORT_VCARD' => 'Import APS Communication vCard',
  'LBL_IMPORT' => 'Import APS Communications',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new APS Communication record by importing a vCard from your file system.',
  'LNK_IMPORT_M06_APS_COMMUNICATION' => 'Import APS Communication',
  'LBL_M06_APS_COMMUNICATION_SUBPANEL_TITLE' => 'APS Communications',
);