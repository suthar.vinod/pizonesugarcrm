<?php
// created: 2022-12-13 04:46:41
$mod_strings = array (
  'LBL_TEAM' => 'Equipas',
  'LBL_TEAMS' => 'Equipas',
  'LBL_TEAM_ID' => 'ID da Equipa',
  'LBL_ASSIGNED_TO_ID' => 'Atribuído a',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Introdução',
  'LBL_DATE_MODIFIED' => 'Data de Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Id de Modificado Por',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado por',
  'LBL_CREATED_ID' => 'ID Criado por',
  'LBL_DOC_OWNER' => 'Proprietário do Documento',
  'LBL_USER_FAVORITES' => 'Utilizadores que adicionaram aos Favoritos',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado pelo Utilizador',
  'LBL_MODIFIED_USER' => 'Modificado pelo Utilizador',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado Por Nome',
  'LBL_LIST_FORM_TITLE' => 'APS Communications Lista',
  'LBL_MODULE_NAME' => 'APS Communications',
  'LBL_MODULE_TITLE' => 'APS Communications',
  'LBL_MODULE_NAME_SINGULAR' => 'APS Communication',
  'LBL_HOMEPAGE_TITLE' => 'Minha APS Communications',
  'LNK_NEW_RECORD' => 'Criar APS Communication',
  'LNK_LIST' => 'Ver APS Communications',
  'LNK_IMPORT_M06_COMMUNICATION' => 'Import APS Communication',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar APS Communication',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Actividades',
  'LBL_M06_COMMUNICATION_SUBPANEL_TITLE' => 'APS Communications',
  'LBL_NEW_FORM_TITLE' => 'Novo APS Communication',
  'LNK_IMPORT_VCARD' => 'Import APS Communication vCard',
  'LBL_IMPORT' => 'Import APS Communications',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new APS Communication record by importing a vCard from your file system.',
  'LNK_IMPORT_M06_APS_COMMUNICATION' => 'Import APS Communication',
  'LBL_M06_APS_COMMUNICATION_SUBPANEL_TITLE' => 'APS Communications',
);