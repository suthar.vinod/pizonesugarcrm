<?php
// created: 2022-12-13 04:46:42
$mod_strings = array (
  'LBL_TEAM' => 'Екип',
  'LBL_TEAMS' => 'Екип',
  'LBL_TEAM_ID' => 'Идентификатор на екип',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Отговорник',
  'LBL_ID' => 'Идентификатор',
  'LBL_DATE_ENTERED' => 'Въведено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_DOC_OWNER' => 'Собственик на документа',
  'LBL_USER_FAVORITES' => 'Потребители които харесват',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Изтрити',
  'LBL_NAME' => 'Име',
  'LBL_CREATED_USER' => 'Създадено от потребител',
  'LBL_MODIFIED_USER' => 'Модифицирано от потребител',
  'LBL_LIST_NAME' => 'Име',
  'LBL_EDIT_BUTTON' => 'Редактиране',
  'LBL_REMOVE' => 'Изтрий',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Модифицирано от',
  'LBL_LIST_FORM_TITLE' => 'Notes_To_File Списък',
  'LBL_MODULE_NAME' => 'Notes_To_File',
  'LBL_MODULE_TITLE' => 'Notes_To_File',
  'LBL_MODULE_NAME_SINGULAR' => 'Note_To_File',
  'LBL_HOMEPAGE_TITLE' => 'Мои Notes_To_File',
  'LNK_NEW_RECORD' => 'Създай Note_To_File',
  'LNK_LIST' => 'Разгледай Notes_To_File',
  'LNK_IMPORT_M06_NOTE_TO_FILE' => 'Import Note_To_File',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Note_To_File',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_M06_NOTE_TO_FILE_SUBPANEL_TITLE' => 'Notes_To_File',
  'LBL_NEW_FORM_TITLE' => 'Нов Note_To_File',
  'LNK_IMPORT_VCARD' => 'Import Note_To_File vCard',
  'LBL_IMPORT' => 'Import Notes_To_File',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Note_To_File record by importing a vCard from your file system.',
);