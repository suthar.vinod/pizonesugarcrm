<?php
// created: 2022-12-13 04:46:42
$mod_strings = array (
  'LBL_TEAM' => 'الفرق',
  'LBL_TEAMS' => 'الفرق',
  'LBL_TEAM_ID' => 'معرّف الفريق',
  'LBL_ASSIGNED_TO_ID' => 'معرّف المستخدم المعين',
  'LBL_ASSIGNED_TO_NAME' => 'تعيين إلى',
  'LBL_ID' => 'المعرّف',
  'LBL_DATE_ENTERED' => 'تاريخ الإنشاء',
  'LBL_DATE_MODIFIED' => 'تاريخ التعديل',
  'LBL_MODIFIED' => 'تم التعديل بواسطة',
  'LBL_MODIFIED_ID' => 'تم التعديل بواسطة المعرّف',
  'LBL_MODIFIED_NAME' => 'تم التعديل بواسطة الاسم',
  'LBL_CREATED' => 'تم الإنشاء بواسطة',
  'LBL_CREATED_ID' => 'تم الإنشاء بواسطة المعرّف',
  'LBL_DOC_OWNER' => 'مالك المستند',
  'LBL_USER_FAVORITES' => 'المستخدمون الذي يفضلون',
  'LBL_DESCRIPTION' => 'الوصف',
  'LBL_DELETED' => 'تم الحذف',
  'LBL_NAME' => 'الاسم',
  'LBL_CREATED_USER' => 'تم الإنشاء بواسطة مستخدم',
  'LBL_MODIFIED_USER' => 'تم التعديل بواسطة مستخدم',
  'LBL_LIST_NAME' => 'الاسم',
  'LBL_EDIT_BUTTON' => 'تحرير',
  'LBL_REMOVE' => 'إزالة',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'تم التعديل بواسطة الاسم',
  'LBL_LIST_FORM_TITLE' => 'Communications القائمة',
  'LBL_MODULE_NAME' => 'Communications',
  'LBL_MODULE_TITLE' => 'Communications',
  'LBL_MODULE_NAME_SINGULAR' => 'Communication',
  'LBL_HOMEPAGE_TITLE' => 'الخاص بي Communications',
  'LNK_NEW_RECORD' => 'إنشاء Communication',
  'LNK_LIST' => 'عرض Communications',
  'LNK_IMPORT_M06_COMMUNICATION' => 'Import Communication',
  'LBL_SEARCH_FORM_TITLE' => 'بحث Communication',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'عرض السجل',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'سير النشاط',
  'LBL_M06_COMMUNICATION_SUBPANEL_TITLE' => 'Communications',
  'LBL_NEW_FORM_TITLE' => 'جديد Communication',
  'LNK_IMPORT_VCARD' => 'Import Communication vCard',
  'LBL_IMPORT' => 'Import Communications',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Communication record by importing a vCard from your file system.',
);