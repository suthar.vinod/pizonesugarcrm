<?php
// created: 2022-12-13 04:46:42
$mod_strings = array (
  'LBL_TEAM' => 'Tiimit',
  'LBL_TEAMS' => 'Tiimit',
  'LBL_TEAM_ID' => 'Tiimi-ID',
  'LBL_ASSIGNED_TO_ID' => 'Vastuuhenkilön ID',
  'LBL_ASSIGNED_TO_NAME' => 'Vastuuhenkilö',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Luontipäivä',
  'LBL_DATE_MODIFIED' => 'Muokattu viimeksi',
  'LBL_MODIFIED' => 'Muuttanut',
  'LBL_MODIFIED_ID' => 'Muokkaajan ID',
  'LBL_MODIFIED_NAME' => 'Muokkaajan nimi',
  'LBL_CREATED' => 'Luonut',
  'LBL_CREATED_ID' => 'Luojan ID',
  'LBL_DOC_OWNER' => 'Tiedoston omistaja',
  'LBL_USER_FAVORITES' => 'Käyttäjät, jotka ottivat suosikikseen',
  'LBL_DESCRIPTION' => 'Kuvaus',
  'LBL_DELETED' => 'Poistettu',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Luonut käyttäjä',
  'LBL_MODIFIED_USER' => 'Muokannut käyttäjä',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Muokkaa',
  'LBL_REMOVE' => 'Poista',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muokkaajan nimi',
  'LBL_LIST_FORM_TITLE' => 'Communications Lista',
  'LBL_MODULE_NAME' => 'Communications',
  'LBL_MODULE_TITLE' => 'Communications',
  'LBL_MODULE_NAME_SINGULAR' => 'Communication',
  'LBL_HOMEPAGE_TITLE' => 'Oma Communications',
  'LNK_NEW_RECORD' => 'Luo Communication',
  'LNK_LIST' => 'Näytä Communications',
  'LNK_IMPORT_M06_COMMUNICATION' => 'Import Communication',
  'LBL_SEARCH_FORM_TITLE' => 'Haku Communication',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Näytä historia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteetit',
  'LBL_M06_COMMUNICATION_SUBPANEL_TITLE' => 'Communications',
  'LBL_NEW_FORM_TITLE' => 'Uusi Communication',
  'LNK_IMPORT_VCARD' => 'Import Communication vCard',
  'LBL_IMPORT' => 'Import Communications',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Communication record by importing a vCard from your file system.',
);