<?php
// created: 2022-12-13 04:46:49
$mod_strings = array (
  'LBL_TEAM' => 'Darba grupa',
  'LBL_TEAMS' => 'Darba grupas',
  'LBL_TEAM_ID' => 'Darba grupas ID',
  'LBL_ASSIGNED_TO_ID' => 'Piešķirts lietotāja Id',
  'LBL_ASSIGNED_TO_NAME' => 'Piešķirts lietotājam',
  'LBL_TAGS_LINK' => 'Birkas',
  'LBL_TAGS' => 'Birkas',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Izveides datums',
  'LBL_DATE_MODIFIED' => 'Modificēšanas datums',
  'LBL_MODIFIED' => 'Modificēja',
  'LBL_MODIFIED_ID' => 'Modificētāja ID',
  'LBL_MODIFIED_NAME' => 'Modificēts pēc vārda',
  'LBL_CREATED' => 'Izveidoja',
  'LBL_CREATED_ID' => 'Izveidotāja ID',
  'LBL_DOC_OWNER' => 'Dokumenta īpašnieks',
  'LBL_USER_FAVORITES' => 'Lietotāji atzīmēja',
  'LBL_DESCRIPTION' => 'Apraksts',
  'LBL_DELETED' => 'Dzēsts',
  'LBL_NAME' => 'Nosaukums',
  'LBL_CREATED_USER' => 'Izveidoja',
  'LBL_MODIFIED_USER' => 'Modificēja lietotājs',
  'LBL_LIST_NAME' => 'Nosaukums',
  'LBL_EDIT_BUTTON' => 'Rediģēt',
  'LBL_REMOVE' => 'Noņemt',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificēts pēc vārda',
  'LBL_LIST_FORM_TITLE' => 'Equipment Saraksts',
  'LBL_MODULE_NAME' => 'Equipment',
  'LBL_MODULE_TITLE' => 'Equipment',
  'LBL_MODULE_NAME_SINGULAR' => 'Equipment',
  'LBL_HOMEPAGE_TITLE' => 'Mans Equipment',
  'LNK_NEW_RECORD' => 'Izveidot Equipment',
  'LNK_LIST' => 'Skats Equipment',
  'LNK_IMPORT_EQUIP_EQUIPMENT' => 'Import Equipment',
  'LBL_SEARCH_FORM_TITLE' => 'Meklēt Equipment',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Aplūkot vēsturi',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Darbības',
  'LBL_EQUIP_EQUIPMENT_SUBPANEL_TITLE' => 'Equipment',
  'LBL_NEW_FORM_TITLE' => 'Jauns Equipment',
  'LNK_IMPORT_VCARD' => 'Import Equipment vCard',
  'LBL_IMPORT' => 'Import Equipment',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Equipment record by importing a vCard from your file system.',
);