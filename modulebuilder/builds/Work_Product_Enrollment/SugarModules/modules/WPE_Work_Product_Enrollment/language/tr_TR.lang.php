<?php
// created: 2022-12-13 04:47:13
$mod_strings = array (
  'LBL_TEAM' => 'Takımlar',
  'LBL_TEAMS' => 'Takımlar',
  'LBL_TEAM_ID' => 'Takım ID',
  'LBL_ASSIGNED_TO_ID' => 'Atanan Kullanıcı ID',
  'LBL_ASSIGNED_TO_NAME' => 'Atanan Kişi',
  'LBL_TAGS_LINK' => 'Etiketler',
  'LBL_TAGS' => 'Etiketler',
  'LBL_ID' => 'KİMLİK',
  'LBL_DATE_ENTERED' => 'Oluşturulma Tarihi',
  'LBL_DATE_MODIFIED' => 'Değiştirilme Tarihi',
  'LBL_MODIFIED' => 'Değiştiren',
  'LBL_MODIFIED_ID' => 'Değiştiren ID',
  'LBL_MODIFIED_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_CREATED' => 'Oluşturan',
  'LBL_CREATED_ID' => 'Oluşturan ID',
  'LBL_DOC_OWNER' => 'Doküman Sahibi',
  'LBL_USER_FAVORITES' => 'Favori Olan Kullanıcılar',
  'LBL_DESCRIPTION' => 'Tanım',
  'LBL_DELETED' => 'Silindi',
  'LBL_NAME' => 'İsim',
  'LBL_CREATED_USER' => 'Oluşturan Kullanıcı',
  'LBL_MODIFIED_USER' => 'Değiştiren Kullanıcı',
  'LBL_LIST_NAME' => 'İsim',
  'LBL_EDIT_BUTTON' => 'Değiştir',
  'LBL_REMOVE' => 'Sil',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_LIST_FORM_TITLE' => 'Work Product Enrollment Liste',
  'LBL_MODULE_NAME' => 'Work Product Enrollment',
  'LBL_MODULE_TITLE' => 'Work Product Enrollment',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Enrollment',
  'LBL_HOMEPAGE_TITLE' => 'Benim Work Product Enrollment',
  'LNK_NEW_RECORD' => 'Oluştur Work Product Enrollment',
  'LNK_LIST' => 'Göster Work Product Enrollment',
  'LNK_IMPORT_WPE_WORK_PRODUCT_ENROLLMENT' => 'Import Work Product Enrollment',
  'LBL_SEARCH_FORM_TITLE' => 'Ara Work Product Enrollment',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Tarihçeyi Gör',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteler',
  'LBL_WPE_WORK_PRODUCT_ENROLLMENT_SUBPANEL_TITLE' => 'Work Product Enrollment',
  'LBL_NEW_FORM_TITLE' => 'Yeni Work Product Enrollment',
  'LNK_IMPORT_VCARD' => 'Import Work Product Enrollment vCard',
  'LBL_IMPORT' => 'Import Work Product Enrollment',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Enrollment record by importing a vCard from your file system.',
);