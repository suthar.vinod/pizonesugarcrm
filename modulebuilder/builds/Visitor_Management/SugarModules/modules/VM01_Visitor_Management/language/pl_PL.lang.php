<?php
// created: 2022-12-13 04:47:12
$mod_strings = array (
  'LBL_TEAM' => 'Zespoły',
  'LBL_TEAMS' => 'Zespoły',
  'LBL_TEAM_ID' => 'ID zespołu',
  'LBL_ASSIGNED_TO_ID' => 'Przypisano do (ID użytkownika)',
  'LBL_ASSIGNED_TO_NAME' => 'Przydzielono do',
  'LBL_TAGS_LINK' => 'Tagi',
  'LBL_TAGS' => 'Tagi',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data utworzenia',
  'LBL_DATE_MODIFIED' => 'Data modyfikacji',
  'LBL_MODIFIED' => 'Zmodyfikowano przez',
  'LBL_MODIFIED_ID' => 'Zmodyfikowano przez (ID)',
  'LBL_MODIFIED_NAME' => 'Zmodyfikowano przez (nazwa)',
  'LBL_CREATED' => 'Utworzono przez',
  'LBL_CREATED_ID' => 'Utworzone przez (ID)',
  'LBL_DOC_OWNER' => 'Właściciel dokumentu',
  'LBL_USER_FAVORITES' => 'Użytkownicy, którzy dodali rekord do ulubionych',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Usunięto',
  'LBL_NAME' => 'Nazwa',
  'LBL_CREATED_USER' => 'Utworzono przez',
  'LBL_MODIFIED_USER' => 'Zmodyfikowane przez',
  'LBL_LIST_NAME' => 'Nazwa',
  'LBL_EDIT_BUTTON' => 'Edytuj',
  'LBL_REMOVE' => 'Usuń',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmodyfikowano przez (nazwa)',
  'LBL_LIST_FORM_TITLE' => 'Visitor Management Lista',
  'LBL_MODULE_NAME' => 'Visitor Management',
  'LBL_MODULE_TITLE' => 'Visitor Management',
  'LBL_MODULE_NAME_SINGULAR' => 'Visitor Management',
  'LBL_HOMEPAGE_TITLE' => 'Moje Visitor Management',
  'LNK_NEW_RECORD' => 'Utwórz Visitor Management',
  'LNK_LIST' => 'Widok Visitor Management',
  'LNK_IMPORT_VM01_VISITOR_MANAGEMENT' => 'Import Visitor Management',
  'LBL_SEARCH_FORM_TITLE' => 'Wyszukiwanie Visitor Management',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Wyświetl historię',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Panel aktywności',
  'LBL_VM01_VISITOR_MANAGEMENT_SUBPANEL_TITLE' => 'Visitor Management',
  'LBL_NEW_FORM_TITLE' => 'Nowy Visitor Management',
  'LNK_IMPORT_VCARD' => 'Import Visitor Management vCard',
  'LBL_IMPORT' => 'Import Visitor Management',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Visitor Management record by importing a vCard from your file system.',
);