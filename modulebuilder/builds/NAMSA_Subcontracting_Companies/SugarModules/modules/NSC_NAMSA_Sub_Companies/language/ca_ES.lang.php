<?php
// created: 2022-12-13 04:46:55
$mod_strings = array (
  'LBL_TEAM' => 'Equip',
  'LBL_TEAMS' => 'Equips',
  'LBL_TEAM_ID' => 'ID de l&#39;equip',
  'LBL_ASSIGNED_TO_ID' => 'ID d&#39;usuari assignat',
  'LBL_ASSIGNED_TO_NAME' => 'Assignat a',
  'LBL_TAGS_LINK' => 'Etiquetes',
  'LBL_TAGS' => 'Etiquetes',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de creació',
  'LBL_DATE_MODIFIED' => 'Última Modificació',
  'LBL_MODIFIED' => 'Modificat per',
  'LBL_MODIFIED_ID' => 'Modificat per ID',
  'LBL_MODIFIED_NAME' => 'Modificat Per Nom',
  'LBL_CREATED' => 'Creat Per',
  'LBL_CREATED_ID' => 'Creat per ID',
  'LBL_DOC_OWNER' => 'Propietari del document',
  'LBL_USER_FAVORITES' => 'Usuaris que son favorits',
  'LBL_DESCRIPTION' => 'Descripció',
  'LBL_DELETED' => 'Suprimit',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Creat Per Usuari',
  'LBL_MODIFIED_USER' => 'Modificat Per Usuari',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Edita',
  'LBL_REMOVE' => 'Suprimir',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat per nom',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Creat pel nom',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'NAMSA Subcontracting Companies Llista',
  'LBL_MODULE_NAME' => 'NAMSA Subcontracting Companies',
  'LBL_MODULE_TITLE' => 'NAMSA Subcontracting Companies',
  'LBL_MODULE_NAME_SINGULAR' => 'NAMSA Subcontracting Company',
  'LBL_HOMEPAGE_TITLE' => 'Meu NAMSA Subcontracting Companies',
  'LNK_NEW_RECORD' => 'Crea NAMSA Subcontracting Company',
  'LNK_LIST' => 'Vista NAMSA Subcontracting Companies',
  'LNK_IMPORT_NSC_NAMSA_SUB_COMPANIES' => 'Import NAMSA Subcontracting Companies',
  'LBL_SEARCH_FORM_TITLE' => 'Cerca NAMSA Subcontracting Company',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Veure historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Seqüència d&#39;activitats',
  'LBL_NSC_NAMSA_SUB_COMPANIES_SUBPANEL_TITLE' => 'NAMSA Subcontracting Companies',
  'LBL_NEW_FORM_TITLE' => 'Nou NAMSA Subcontracting Company',
  'LNK_IMPORT_VCARD' => 'Import NAMSA Subcontracting Company vCard',
  'LBL_IMPORT' => 'Import NAMSA Subcontracting Companies',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new NAMSA Subcontracting Company record by importing a vCard from your file system.',
  'LBL_NSC_NAMSA_SUB_COMPANIES_FOCUS_DRAWER_DASHBOARD' => 'NAMSA Subcontracting Companies Focus Drawer',
  'LBL_NSC_NAMSA_SUB_COMPANIES_RECORD_DASHBOARD' => 'NAMSA Subcontracting Companies Record Dashboard',
  'LBL_COMPANY_NAME' => 'Company Name',
);