<?php
 // created: 2018-02-05 14:21:02
$layout_defs["M99_Protocol_Amendments"]["subpanel_setup"]['m99_protocol_amendments_m03_work_product'] = array (
  'order' => 100,
  'module' => 'M03_Work_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_M99_PROTOCOL_AMENDMENTS_M03_WORK_PRODUCT_FROM_M03_WORK_PRODUCT_TITLE',
  'get_subpanel_data' => 'm99_protocol_amendments_m03_work_product',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
