<?php
 // created: 2018-02-05 14:21:02
$layout_defs["M03_Work_Product"]["subpanel_setup"]['m99_protocol_amendments_m03_work_product'] = array (
  'order' => 100,
  'module' => 'M99_Protocol_Amendments',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_M99_PROTOCOL_AMENDMENTS_M03_WORK_PRODUCT_FROM_M99_PROTOCOL_AMENDMENTS_TITLE',
  'get_subpanel_data' => 'm99_protocol_amendments_m03_work_product',
);
