<?php
// created: 2018-02-05 14:21:02
$dictionary["M99_Protocol_Amendments"]["fields"]["m99_protocol_amendments_m03_work_product"] = array (
  'name' => 'm99_protocol_amendments_m03_work_product',
  'type' => 'link',
  'relationship' => 'm99_protocol_amendments_m03_work_product',
  'source' => 'non-db',
  'module' => 'M03_Work_Product',
  'bean_name' => 'M03_Work_Product',
  'vname' => 'LBL_M99_PROTOCOL_AMENDMENTS_M03_WORK_PRODUCT_FROM_M03_WORK_PRODUCT_TITLE',
  'id_name' => 'm99_protocol_amendments_m03_work_productm03_work_product_idb',
);
