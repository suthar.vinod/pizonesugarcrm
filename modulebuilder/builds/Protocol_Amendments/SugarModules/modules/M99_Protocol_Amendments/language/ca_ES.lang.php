<?php
// created: 2022-12-13 04:46:57
$mod_strings = array (
  'LBL_TEAM' => 'Equip',
  'LBL_TEAMS' => 'Equips',
  'LBL_TEAM_ID' => 'Id Equip',
  'LBL_ASSIGNED_TO_ID' => 'Usuari Assignat',
  'LBL_ASSIGNED_TO_NAME' => 'Assignat a',
  'LBL_TAGS_LINK' => 'Etiquetes',
  'LBL_TAGS' => 'Etiquetes',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Creació',
  'LBL_DATE_MODIFIED' => 'Última Modificació',
  'LBL_MODIFIED' => 'Modificat Per',
  'LBL_MODIFIED_ID' => 'Modificat Per Id',
  'LBL_MODIFIED_NAME' => 'Modificat Per Nom',
  'LBL_CREATED' => 'Creat Per',
  'LBL_CREATED_ID' => 'Creat Per Id',
  'LBL_DOC_OWNER' => 'Propietari del document',
  'LBL_USER_FAVORITES' => 'Usuaris que son favorits',
  'LBL_DESCRIPTION' => 'Descripció',
  'LBL_DELETED' => 'Eliminat',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Creat Per Usuari',
  'LBL_MODIFIED_USER' => 'Modificat Per Usuari',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Treure',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat per nom',
  'LBL_LIST_FORM_TITLE' => 'Protocol Amendments Llista',
  'LBL_MODULE_NAME' => 'Protocol Amendments',
  'LBL_MODULE_TITLE' => 'Protocol Amendments',
  'LBL_MODULE_NAME_SINGULAR' => 'Protocol Amendment',
  'LBL_HOMEPAGE_TITLE' => 'Meu Protocol Amendments',
  'LNK_NEW_RECORD' => 'Crea Protocol Amendment',
  'LNK_LIST' => 'Vista Protocol Amendments',
  'LNK_IMPORT_M99_PROTOCOL_AMENDMENTS' => 'Import Protocol Amendments',
  'LBL_SEARCH_FORM_TITLE' => 'Cerca Protocol Amendment',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Veure historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Seqüència d&#39;activitats',
  'LBL_M99_PROTOCOL_AMENDMENTS_SUBPANEL_TITLE' => 'Protocol Amendments',
  'LBL_NEW_FORM_TITLE' => 'Nou Protocol Amendment',
  'LNK_IMPORT_VCARD' => 'Import Protocol Amendment vCard',
  'LBL_IMPORT' => 'Import Protocol Amendments',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Protocol Amendment record by importing a vCard from your file system.',
);