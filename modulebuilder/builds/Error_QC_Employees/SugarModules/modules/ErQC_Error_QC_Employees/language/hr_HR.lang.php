<?php
// created: 2022-12-13 04:46:51
$mod_strings = array (
  'LBL_TEAM' => 'Timovi',
  'LBL_TEAMS' => 'Timovi',
  'LBL_TEAM_ID' => 'ID tima',
  'LBL_ASSIGNED_TO_ID' => 'ID dodijeljenog korisnika',
  'LBL_ASSIGNED_TO_NAME' => 'Dodijeljeno',
  'LBL_TAGS_LINK' => 'Oznake',
  'LBL_TAGS' => 'Oznake',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum stvaranja',
  'LBL_DATE_MODIFIED' => 'Datum izmjene',
  'LBL_MODIFIED' => 'Izmijenio/la',
  'LBL_MODIFIED_ID' => 'Izmijenio ID',
  'LBL_MODIFIED_NAME' => 'Izmijenilo ime',
  'LBL_CREATED' => 'Stvorio/la',
  'LBL_CREATED_ID' => 'Stvorio ID',
  'LBL_DOC_OWNER' => 'Vlasnik dokumenta',
  'LBL_USER_FAVORITES' => 'Korisnici koji su dodali u Favorite',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Izbrisano',
  'LBL_NAME' => 'Ime',
  'LBL_CREATED_USER' => 'Stvorio korisnik',
  'LBL_MODIFIED_USER' => 'Izmijenio korisnik',
  'LBL_LIST_NAME' => 'Ime',
  'LBL_EDIT_BUTTON' => 'Uredi',
  'LBL_REMOVE' => 'Ukloni',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Izmijenilo ime',
  'LBL_LIST_FORM_TITLE' => 'Errors QC Employees Popis',
  'LBL_MODULE_NAME' => 'Errors QC Employees',
  'LBL_MODULE_TITLE' => 'Errors QC Employees',
  'LBL_MODULE_NAME_SINGULAR' => 'Error QC Employee',
  'LBL_HOMEPAGE_TITLE' => 'Moja Errors QC Employees',
  'LNK_NEW_RECORD' => 'Stvori Error QC Employee',
  'LNK_LIST' => 'Prikaži Errors QC Employees',
  'LNK_IMPORT_ERQC_ERROR_QC_EMPLOYEES' => 'Import Errors QC Employees',
  'LBL_SEARCH_FORM_TITLE' => 'Pretraži Error QC Employee',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Prikaži povijest',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Pregled aktivnosti',
  'LBL_ERQC_ERROR_QC_EMPLOYEES_SUBPANEL_TITLE' => 'Errors QC Employees',
  'LBL_NEW_FORM_TITLE' => 'Novo Error QC Employee',
  'LNK_IMPORT_VCARD' => 'Import Error QC Employee vCard',
  'LBL_IMPORT' => 'Import Errors QC Employees',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Error QC Employee record by importing a vCard from your file system.',
);