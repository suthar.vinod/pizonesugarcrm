<?php
// created: 2022-12-13 04:47:02
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'צוות Id',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'הוקצה עבור',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'נוצר בתאריך',
  'LBL_DATE_MODIFIED' => 'שונה בתאריך',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'שונה על ידי Id',
  'LBL_MODIFIED_NAME' => 'שונה על ידי ששמו',
  'LBL_CREATED' => 'נוצר על ידי',
  'LBL_CREATED_ID' => 'נוצר על ידי Id',
  'LBL_DOC_OWNER' => 'בעלים של המסמך',
  'LBL_USER_FAVORITES' => 'משתמשים שמעדיפים',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'נמחק',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'נוצר על ידי משתמש',
  'LBL_MODIFIED_USER' => 'שונה על ידי משתמש',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'הסר',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'שונה על ידי ששמו',
  'LBL_LIST_FORM_TITLE' => 'Sales רשימה',
  'LBL_MODULE_NAME' => 'Sales',
  'LBL_MODULE_TITLE' => 'Sales',
  'LBL_MODULE_NAME_SINGULAR' => 'Sale',
  'LBL_HOMEPAGE_TITLE' => 'My Sales',
  'LNK_NEW_RECORD' => 'Create Sale',
  'LNK_LIST' => 'צפה Sales',
  'LNK_IMPORT_M01_SALES' => 'Import Sale',
  'LBL_SEARCH_FORM_TITLE' => 'חיפוש Sale',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'צפה בהסטוריה',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_M01_SALES_SUBPANEL_TITLE' => 'Sales',
  'LBL_NEW_FORM_TITLE' => 'New Sale',
  'LNK_IMPORT_VCARD' => 'Import Sale vCard',
  'LBL_IMPORT' => 'Import Sales',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Sale record by importing a vCard from your file system.',
);