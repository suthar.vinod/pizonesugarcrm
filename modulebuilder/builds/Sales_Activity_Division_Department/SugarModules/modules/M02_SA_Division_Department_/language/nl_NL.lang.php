<?php
// created: 2022-12-13 04:47:03
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen aan ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum aangemaakt',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door Naam',
  'LBL_CREATED' => 'Aangemaakt Door',
  'LBL_CREATED_ID' => 'Gemaakt Door ID',
  'LBL_DOC_OWNER' => 'Eigenaar van document',
  'LBL_USER_FAVORITES' => 'Gebruikers die favoriet gemaakt hebben',
  'LBL_DESCRIPTION' => 'Beschrijving',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_NAME' => 'Naam',
  'LBL_CREATED_USER' => 'Gemaakt door Gebruiker',
  'LBL_MODIFIED_USER' => 'Gewijzigd door Gebruiker',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_EDIT_BUTTON' => 'Wijzigen',
  'LBL_REMOVE' => 'Verwijder',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Gewijzigd door Naam',
  'LBL_LIST_FORM_TITLE' => 'SA Divisions Departments  Bruto verkoopprijs',
  'LBL_MODULE_NAME' => 'SA Divisions Departments ',
  'LBL_MODULE_TITLE' => 'SA Divisions Departments ',
  'LBL_MODULE_NAME_SINGULAR' => 'SA Division Department ',
  'LBL_HOMEPAGE_TITLE' => 'My SA Divisions Departments ',
  'LNK_NEW_RECORD' => 'Nieuw(e) SA Division Department ',
  'LNK_LIST' => 'Bekijken SA Divisions Departments ',
  'LNK_IMPORT_M02_SA_DIVISION_DEPARTMENT ' => 'Import SA Division Department ',
  'LBL_SEARCH_FORM_TITLE' => 'Search SA Division Department ',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Bekijk Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_M02_SA_DIVISION_DEPARTMENT _SUBPANEL_TITLE' => 'SA Divisions Departments ',
  'LBL_NEW_FORM_TITLE' => 'New SA Division Department ',
  'LNK_IMPORT_VCARD' => 'Import SA Division Department  vCard',
  'LBL_IMPORT' => 'Import SA Divisions Departments ',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new SA Division Department  record by importing a vCard from your file system.',
  'LNK_IMPORT_M02_SA_DIVISION_DEPARTMENT_' => 'Import SA Division Department ',
  'LBL_M02_SA_DIVISION_DEPARTMENT__SUBPANEL_TITLE' => 'SA Divisions Departments ',
);