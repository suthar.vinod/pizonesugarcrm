<?php
// created: 2022-12-13 04:47:03
$mod_strings = array (
  'LBL_TEAM' => 'Команди',
  'LBL_TEAMS' => 'Команди',
  'LBL_TEAM_ID' => 'Команда (Id)',
  'LBL_ASSIGNED_TO_ID' => 'Відповідальний (-а) (Id)',
  'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (-а)',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_DATE_MODIFIED' => 'Дата змінення',
  'LBL_MODIFIED' => 'Змінено:',
  'LBL_MODIFIED_ID' => 'Змінено (Id)',
  'LBL_MODIFIED_NAME' => 'Змінено користувачем',
  'LBL_CREATED' => 'Створено',
  'LBL_CREATED_ID' => 'Створено (Id)',
  'LBL_DOC_OWNER' => 'Власник документа',
  'LBL_USER_FAVORITES' => 'Користувачі, які додали в Обране',
  'LBL_DESCRIPTION' => 'Опис',
  'LBL_DELETED' => 'Видалено',
  'LBL_NAME' => 'Назва',
  'LBL_CREATED_USER' => 'Створенено користувачем',
  'LBL_MODIFIED_USER' => 'Змінено користувачем',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_EDIT_BUTTON' => 'Редагувати',
  'LBL_REMOVE' => 'Видалити',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Змінено користувачем',
  'LBL_LIST_FORM_TITLE' => 'SA Divisions Departments  Список',
  'LBL_MODULE_NAME' => 'SA Divisions Departments ',
  'LBL_MODULE_TITLE' => 'SA Divisions Departments ',
  'LBL_MODULE_NAME_SINGULAR' => 'SA Division Department ',
  'LBL_HOMEPAGE_TITLE' => 'Мій SA Divisions Departments ',
  'LNK_NEW_RECORD' => 'Створити SA Division Department ',
  'LNK_LIST' => 'Переглянути SA Divisions Departments ',
  'LNK_IMPORT_M02_SA_DIVISION_DEPARTMENT ' => 'Import SA Division Department ',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук SA Division Department ',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Переглянути історію',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Стрічка активностей',
  'LBL_M02_SA_DIVISION_DEPARTMENT _SUBPANEL_TITLE' => 'SA Divisions Departments ',
  'LBL_NEW_FORM_TITLE' => 'Новий SA Division Department ',
  'LNK_IMPORT_VCARD' => 'Import SA Division Department  vCard',
  'LBL_IMPORT' => 'Import SA Divisions Departments ',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new SA Division Department  record by importing a vCard from your file system.',
  'LNK_IMPORT_M02_SA_DIVISION_DEPARTMENT_' => 'Import SA Division Department ',
  'LBL_M02_SA_DIVISION_DEPARTMENT__SUBPANEL_TITLE' => 'SA Divisions Departments ',
);