<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

// THIS CONTENT IS GENERATED BY MBPackage.php
$manifest = array (
  'built_in_version' => '7.6.1.0',
  'acceptable_sugar_versions' => 
  array (
    0 => '',
  ),
  'acceptable_sugar_flavors' => 
  array (
    0 => 'ENT',
    1 => 'ULT',
  ),
  'readme' => '',
  'key' => 'M02',
  'author' => 'MLC',
  'description' => 'Child of Sales Activity',
  'icon' => '',
  'is_uninstallable' => true,
  'name' => 'Sales_Activity_Division_Department',
  'published_date' => '2016-01-21 03:25:42',
  'type' => 'module',
  'version' => 1453346742,
  'remove_tables' => 'prompt',
);


$installdefs = array (
  'id' => 'Sales_Activity_Division_Department',
  'beans' => 
  array (
    0 => 
    array (
      'module' => 'M02_SA_Division_Department_',
      'class' => 'M02_SA_Division_Department_',
      'path' => 'modules/M02_SA_Division_Department_/M02_SA_Division_Department_.php',
      'tab' => true,
    ),
  ),
  'layoutdefs' => 
  array (
  ),
  'relationships' => 
  array (
  ),
  'image_dir' => '<basepath>/icons',
  'copy' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/SugarModules/modules/M02_SA_Division_Department_',
      'to' => 'modules/M02_SA_Division_Department_',
    ),
  ),
  'language' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/SugarModules/language/application/en_us.lang.php',
      'to_module' => 'application',
      'language' => 'en_us',
    ),
  ),
);