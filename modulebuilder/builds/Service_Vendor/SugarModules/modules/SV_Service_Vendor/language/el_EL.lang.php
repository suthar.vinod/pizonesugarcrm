<?php
// created: 2022-12-13 04:47:06
$mod_strings = array (
  'LBL_TEAM' => 'Ομάδες',
  'LBL_TEAMS' => 'Ομάδες',
  'LBL_TEAM_ID' => 'Ταυτότητα Ομάδας',
  'LBL_ASSIGNED_TO_ID' => 'Ταυτότητα Ανατεθειμένου Χρήστη',
  'LBL_ASSIGNED_TO_NAME' => 'Ανατέθηκε σε',
  'LBL_TAGS_LINK' => 'Ετικέτες',
  'LBL_TAGS' => 'Ετικέτες',
  'LBL_ID' => 'Ταυτότητα',
  'LBL_DATE_ENTERED' => 'Ημερομηνία Δημιουργίας',
  'LBL_DATE_MODIFIED' => 'Ημερομηνία Τροποποίησης',
  'LBL_MODIFIED' => 'Τροποποιήθηκε Από',
  'LBL_MODIFIED_ID' => 'Τροποποιήθηκε Από Ταυτότητα',
  'LBL_MODIFIED_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_CREATED' => 'Δημιουργήθηκε Από',
  'LBL_CREATED_ID' => 'Δημιουργήθηκε Από Ταυτότητα',
  'LBL_DOC_OWNER' => 'Κάτοχος του εγγράφου',
  'LBL_USER_FAVORITES' => 'Αγαπημένα Χρηστών',
  'LBL_DESCRIPTION' => 'Περιγραφή',
  'LBL_DELETED' => 'Διαγράφηκε',
  'LBL_NAME' => 'Όνομα',
  'LBL_CREATED_USER' => 'Δημιουργήθηκε Από Χρήστη',
  'LBL_MODIFIED_USER' => 'Τροποποιήθηκε από Χρήστη',
  'LBL_LIST_NAME' => 'Όνομα',
  'LBL_EDIT_BUTTON' => 'Επεξεργασία',
  'LBL_REMOVE' => 'Αφαίρεση',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Service Vendors Λίστα',
  'LBL_MODULE_NAME' => 'Service Vendors',
  'LBL_MODULE_TITLE' => 'Service Vendors',
  'LBL_MODULE_NAME_SINGULAR' => 'Service Vendor',
  'LBL_HOMEPAGE_TITLE' => 'Δική Μου Service Vendors',
  'LNK_NEW_RECORD' => 'Δημιουργία Service Vendor',
  'LNK_LIST' => 'Προβολή Service Vendors',
  'LNK_IMPORT_SV_SERVICE_VENDOR' => 'Import Service Vendors',
  'LBL_SEARCH_FORM_TITLE' => 'Αναζήτηση Service Vendor',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Προβολή Ιστορικού',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Δραστηριότητες',
  'LBL_SV_SERVICE_VENDOR_SUBPANEL_TITLE' => 'Service Vendors',
  'LBL_NEW_FORM_TITLE' => 'Νέα Service Vendor',
  'LNK_IMPORT_VCARD' => 'Import Service Vendor vCard',
  'LBL_IMPORT' => 'Import Service Vendors',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Service Vendor record by importing a vCard from your file system.',
  'LBL_SERVICE_VENDOR_ACCOUNT_ID' => 'Service Vendor (related Company ID)',
  'LBL_SERVICE_VENDOR' => 'Service Vendor',
);