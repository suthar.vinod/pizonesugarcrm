<?php
// created: 2022-12-13 04:47:07
$mod_strings = array (
  'LBL_TEAM' => 'Tiimit',
  'LBL_TEAMS' => 'Tiimit',
  'LBL_TEAM_ID' => 'Tiimin tunnus',
  'LBL_ASSIGNED_TO_ID' => 'Vastuukäyttäjän ID',
  'LBL_ASSIGNED_TO_NAME' => 'Vastuuhenkilö',
  'LBL_TAGS_LINK' => 'Tagit',
  'LBL_TAGS' => 'Tagit',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Luontipäivä',
  'LBL_DATE_MODIFIED' => 'Muokkattu viimeksi',
  'LBL_MODIFIED' => 'Muokannut',
  'LBL_MODIFIED_ID' => 'Muokkaajan ID',
  'LBL_MODIFIED_NAME' => 'Muokkaajan nimi',
  'LBL_CREATED' => 'Luonut',
  'LBL_CREATED_ID' => 'Luojan ID',
  'LBL_DOC_OWNER' => 'Tiedoston omistaja',
  'LBL_USER_FAVORITES' => 'Käyttäjät, jotka ottivat suosikikseen',
  'LBL_DESCRIPTION' => 'Kuvaus',
  'LBL_DELETED' => 'Poistettu',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Luoja',
  'LBL_MODIFIED_USER' => 'Muokkaaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Muokkaa',
  'LBL_REMOVE' => 'Poista',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muokkaajan nimi',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Species Census Lista',
  'LBL_MODULE_NAME' => 'Species Census',
  'LBL_MODULE_TITLE' => 'Species Census',
  'LBL_MODULE_NAME_SINGULAR' => 'Species Census',
  'LBL_HOMEPAGE_TITLE' => 'Oma Species Census',
  'LNK_NEW_RECORD' => 'Luo Species Census',
  'LNK_LIST' => 'Näkymä Species Census',
  'LNK_IMPORT_SC_SPECIES_CENSUS' => 'Import Species Census',
  'LBL_SEARCH_FORM_TITLE' => 'Etsi Species Census',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Näytä historia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteetit',
  'LBL_SC_SPECIES_CENSUS_SUBPANEL_TITLE' => 'Species Census',
  'LBL_NEW_FORM_TITLE' => 'Uusi Species Census',
  'LNK_IMPORT_VCARD' => 'Import Species Census vCard',
  'LBL_IMPORT' => 'Import Species Census',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Species Census record by importing a vCard from your file system.',
  'LBL_SC_SPECIES_CENSUS_FOCUS_DRAWER_DASHBOARD' => 'Species Census Focus Drawer',
  'LBL_DATE_2' => 'Date',
  'LBL_CENSUS' => 'Census',
  'LBL_ALLOCATED' => 'Allocated',
  'LBL_ON_STUDY' => 'On Study',
  'LBL_STOCK' => 'Stock',
  'LBL_TOTAL' => 'Total',
);