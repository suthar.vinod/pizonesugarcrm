<?php
// created: 2022-12-13 04:47:05
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen aan ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum aangemaakt',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door Naam',
  'LBL_CREATED' => 'Aangemaakt Door',
  'LBL_CREATED_ID' => 'Gemaakt Door ID',
  'LBL_DOC_OWNER' => 'Eigenaar van document',
  'LBL_USER_FAVORITES' => 'Gebruikers die favoriet gemaakt hebben',
  'LBL_DESCRIPTION' => 'Beschrijving',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_NAME' => 'Naam',
  'LBL_CREATED_USER' => 'Gemaakt door Gebruiker',
  'LBL_MODIFIED_USER' => 'Gewijzigd door Gebruiker',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_EDIT_BUTTON' => 'Wijzigen',
  'LBL_REMOVE' => 'Verwijder',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Gewijzigd door Naam',
  'LBL_LIST_FORM_TITLE' => 'Sales Activity Quotes Bruto verkoopprijs',
  'LBL_MODULE_NAME' => 'Sales Activity Quotes',
  'LBL_MODULE_TITLE' => 'Sales Activity Quotes',
  'LBL_MODULE_NAME_SINGULAR' => 'Sales Activity Quote',
  'LBL_HOMEPAGE_TITLE' => 'My Sales Activity Quotes',
  'LNK_NEW_RECORD' => 'Nieuw(e) Sales Activity Quote',
  'LNK_LIST' => 'Bekijken Sales Activity Quotes',
  'LNK_IMPORT_M01_QUOTE' => 'Import Sales Activity Quote',
  'LBL_SEARCH_FORM_TITLE' => 'Search Sales Activity Quote',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Bekijk Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_M01_QUOTE_SUBPANEL_TITLE' => 'Sales Activity Quotes',
  'LBL_NEW_FORM_TITLE' => 'New Sales Activity Quote',
  'LNK_IMPORT_VCARD' => 'Import Sales Activity Quote vCard',
  'LBL_IMPORT' => 'Import Sales Activity Quotes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Sales Activity Quote record by importing a vCard from your file system.',
  'LNK_IMPORT_M01_SALES_ACTIVITY_QUOTE' => 'Import Sales Activity Quote',
  'LBL_M01_SALES_ACTIVITY_QUOTE_SUBPANEL_TITLE' => 'Sales Activity Quotes',
);