<?php
// created: 2022-12-13 04:47:03
$mod_strings = array (
  'LBL_TEAM' => 'Takımlar',
  'LBL_TEAMS' => 'Takımlar',
  'LBL_TEAM_ID' => 'Takım Id',
  'LBL_ASSIGNED_TO_ID' => 'Atanan Kullanıcı ID',
  'LBL_ASSIGNED_TO_NAME' => 'Atanan Kişi',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Oluşturulma Tarihi',
  'LBL_DATE_MODIFIED' => 'Değiştirilme Tarihi',
  'LBL_MODIFIED' => 'Değiştiren Kişi',
  'LBL_MODIFIED_ID' => 'Değiştiren ID',
  'LBL_MODIFIED_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_CREATED' => 'Oluşturan',
  'LBL_CREATED_ID' => 'Oluşturan ID',
  'LBL_DOC_OWNER' => 'Döküman Sahibi',
  'LBL_USER_FAVORITES' => 'Favorileri Gösteren Kullanıcılar',
  'LBL_DESCRIPTION' => 'Tanım',
  'LBL_DELETED' => 'Silindi',
  'LBL_NAME' => 'İsim',
  'LBL_CREATED_USER' => 'Oluşturan Kullanıcı',
  'LBL_MODIFIED_USER' => 'Değiştiren Kullanıcı',
  'LBL_LIST_NAME' => 'İsim',
  'LBL_EDIT_BUTTON' => 'Değiştir',
  'LBL_REMOVE' => 'Sil',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_LIST_FORM_TITLE' => 'Quotes Liste',
  'LBL_MODULE_NAME' => 'Quotes',
  'LBL_MODULE_TITLE' => 'Quotes',
  'LBL_MODULE_NAME_SINGULAR' => 'Quote',
  'LBL_HOMEPAGE_TITLE' => 'Benim Quotes',
  'LNK_NEW_RECORD' => 'Oluştur Quote',
  'LNK_LIST' => 'Göster Quotes',
  'LNK_IMPORT_M01_QUOTE' => 'Import Quote',
  'LBL_SEARCH_FORM_TITLE' => 'Ara Quote',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Tarihçeyi Gör',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteler',
  'LBL_M01_QUOTE_SUBPANEL_TITLE' => 'Quotes',
  'LBL_NEW_FORM_TITLE' => 'Yeni Quote',
  'LNK_IMPORT_VCARD' => 'Import Quote vCard',
  'LBL_IMPORT' => 'Import Quotes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Quote record by importing a vCard from your file system.',
);