<?php
// created: 2022-12-13 04:47:05
$mod_strings = array (
  'LBL_TEAM' => 'Darba grupas',
  'LBL_TEAMS' => 'Darba grupas',
  'LBL_TEAM_ID' => 'Darba grupas ID',
  'LBL_ASSIGNED_TO_ID' => 'Piešķirts lietotājam ar ID',
  'LBL_ASSIGNED_TO_NAME' => 'Piešķirts lietotājam',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Izveidots',
  'LBL_DATE_MODIFIED' => 'Modificēts',
  'LBL_MODIFIED' => 'Modificēja',
  'LBL_MODIFIED_ID' => 'Modificētāja ID',
  'LBL_MODIFIED_NAME' => 'Modificētāja vārds',
  'LBL_CREATED' => 'Izveidoja',
  'LBL_CREATED_ID' => 'Izveidotāja ID',
  'LBL_DOC_OWNER' => 'Dokumenta īpašnieks',
  'LBL_USER_FAVORITES' => 'Lietotāji atzīmēja',
  'LBL_DESCRIPTION' => 'Apraksts',
  'LBL_DELETED' => 'Dzēsts',
  'LBL_NAME' => 'Nosaukums',
  'LBL_CREATED_USER' => 'Izveidoja lietotājs',
  'LBL_MODIFIED_USER' => 'Modificēja lietotājs',
  'LBL_LIST_NAME' => 'Nosaukums',
  'LBL_EDIT_BUTTON' => 'Rediģēt',
  'LBL_REMOVE' => 'Noņemt',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificētāja vārds',
  'LBL_LIST_FORM_TITLE' => 'Sales Activities Kataloga cena',
  'LBL_MODULE_NAME' => 'Sales Activities',
  'LBL_MODULE_TITLE' => 'Sales Activities',
  'LBL_MODULE_NAME_SINGULAR' => 'Sales Activity',
  'LBL_HOMEPAGE_TITLE' => 'Mans Sales Activities',
  'LNK_NEW_RECORD' => 'Izveidot Sales Activity',
  'LNK_LIST' => 'Skats Sales Activities',
  'LNK_IMPORT_M01_SALES' => 'Import Sale',
  'LBL_SEARCH_FORM_TITLE' => 'Meklēšana Sales Activity',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Aplūkot vēsturi',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Darbības',
  'LBL_M01_SALES_SUBPANEL_TITLE' => 'Sales',
  'LBL_NEW_FORM_TITLE' => 'Jauns Sales Activity',
  'LNK_IMPORT_VCARD' => 'Import Sales Activity vCard',
  'LBL_IMPORT' => 'Import Sales Activities',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Sales Activity record by importing a vCard from your file system.',
  'LNK_IMPORT_M01_SALE' => 'Import Sales Activity',
  'LBL_M01_SALE_SUBPANEL_TITLE' => 'Sales Activities',
  'LNK_IMPORT_M01_SALES_ACTIVITY' => 'Import Sales Activity',
  'LBL_M01_SALES_ACTIVITY_SUBPANEL_TITLE' => 'Sales Activities',
);