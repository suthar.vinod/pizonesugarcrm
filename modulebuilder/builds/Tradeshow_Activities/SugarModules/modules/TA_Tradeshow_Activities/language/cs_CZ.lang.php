<?php
// created: 2022-12-13 04:47:10
$mod_strings = array (
  'LBL_TEAM' => 'Týmy',
  'LBL_TEAMS' => 'Týmy',
  'LBL_TEAM_ID' => 'ID týmu',
  'LBL_ASSIGNED_TO_ID' => 'Přiřazené uživateli s ID',
  'LBL_ASSIGNED_TO_NAME' => 'Přiřazeno komu',
  'LBL_TAGS_LINK' => 'Tagy',
  'LBL_TAGS' => 'Tagy',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum zahájení',
  'LBL_DATE_MODIFIED' => 'Datum poslední úpravy',
  'LBL_MODIFIED' => 'Modifikováno kym',
  'LBL_MODIFIED_ID' => 'Upraveno podle ID',
  'LBL_MODIFIED_NAME' => 'Upraveno uživatelem',
  'LBL_CREATED' => 'Vytvořil:',
  'LBL_CREATED_ID' => 'Vytvořeno podle ID',
  'LBL_DOC_OWNER' => 'Vlastník dokumentu',
  'LBL_USER_FAVORITES' => 'Uživatelé, jejichž je oblíbeným',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Odstranit',
  'LBL_NAME' => 'Název',
  'LBL_CREATED_USER' => 'Vytvořeno uživatelem',
  'LBL_MODIFIED_USER' => 'Změněno uživatelem',
  'LBL_LIST_NAME' => 'Název',
  'LBL_EDIT_BUTTON' => 'Editace',
  'LBL_REMOVE' => 'Odstranit',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Upraveno uživatelem',
  'LBL_LIST_FORM_TITLE' => 'Tradeshow Activities Celk. cena',
  'LBL_MODULE_NAME' => 'Tradeshow Activities',
  'LBL_MODULE_TITLE' => 'Tradeshow Activities',
  'LBL_MODULE_NAME_SINGULAR' => 'Tradeshow Activity',
  'LBL_HOMEPAGE_TITLE' => 'Moje Tradeshow Activities',
  'LNK_NEW_RECORD' => 'Přidat Tradeshow Activity',
  'LNK_LIST' => 'Zobrazit Tradeshow Activities',
  'LNK_IMPORT_TA_TRADESHOW_ACTIVITIES' => 'Import Tradeshow Activities',
  'LBL_SEARCH_FORM_TITLE' => 'Hledat Tradeshow Activity',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobrazit historii',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_TA_TRADESHOW_ACTIVITIES_SUBPANEL_TITLE' => 'Tradeshow Activities',
  'LBL_NEW_FORM_TITLE' => 'Nový Tradeshow Activity',
  'LNK_IMPORT_VCARD' => 'Import Tradeshow Activity vCard',
  'LBL_IMPORT' => 'Import Tradeshow Activities',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Tradeshow Activity record by importing a vCard from your file system.',
);