<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/*********************************************************************************
 * $Id$
 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/TA_Tradeshow_Activities/TA_Tradeshow_Activities.php');

class TA_Tradeshow_ActivitiesDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/TA_Tradeshow_Activities/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'TA_Tradeshow_Activities');

        $this->searchFields = $dashletData['TA_Tradeshow_ActivitiesDashlet']['searchFields'];
        $this->columns = $dashletData['TA_Tradeshow_ActivitiesDashlet']['columns'];

        $this->seedBean = new TA_Tradeshow_Activities();        
    }
}