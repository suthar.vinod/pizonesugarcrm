<?php
// created: 2022-12-13 04:47:10
$mod_strings = array (
  'LBL_TEAM' => 'Команди',
  'LBL_TEAMS' => 'Команди',
  'LBL_TEAM_ID' => 'Команда (Id)',
  'LBL_ASSIGNED_TO_ID' => 'Відповідальний (-а) (Id):',
  'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (-а)',
  'LBL_CREATED' => 'Створено',
  'LBL_CREATED_ID' => 'Створено (Id)',
  'LBL_CREATED_USER' => 'Створенено користувачем',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_DATE_MODIFIED' => 'Дата змінення',
  'LBL_DELETED' => 'Видалено',
  'LBL_DESCRIPTION' => 'Опис',
  'LBL_DOC_OWNER' => 'Власник документа',
  'LBL_EDIT_BUTTON' => 'Редагувати',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Змінено користувачем',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_MODIFIED' => 'Змінено',
  'LBL_MODIFIED_ID' => 'Змінено (Id)',
  'LBL_MODIFIED_NAME' => 'Змінено користувачем',
  'LBL_MODIFIED_USER' => 'Змінено користувачем',
  'LBL_NAME' => 'Назва',
  'LBL_REMOVE' => 'Видалити',
  'LBL_USER_FAVORITES' => 'Користувачі, які додали в Обране',
  'LBL_LIST_FORM_TITLE' => 'Timesheet Список',
  'LBL_MODULE_NAME' => 'Timesheet',
  'LBL_MODULE_TITLE' => 'Timesheet',
  'LBL_MODULE_NAME_SINGULAR' => 'Timesheet',
  'LBL_HOMEPAGE_TITLE' => 'Мій Timesheet',
  'LNK_NEW_RECORD' => 'Створити Timesheet',
  'LNK_LIST' => 'Переглянути Timesheet',
  'LNK_IMPORT_M07_TIMESHEET' => 'Import Timesheet',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук Timesheet',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Переглянути історію',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Стрічка активностей',
  'LBL_M07_TIMESHEET_SUBPANEL_TITLE' => 'Timesheet',
  'LBL_NEW_FORM_TITLE' => 'Новий Timesheet',
  'LNK_IMPORT_VCARD' => 'Import Timesheet vCard',
  'LBL_IMPORT' => 'Import Timesheet',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Timesheet record by importing a vCard from your file system.',
);