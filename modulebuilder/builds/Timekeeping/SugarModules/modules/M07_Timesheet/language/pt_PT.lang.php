<?php
// created: 2022-12-13 04:47:10
$mod_strings = array (
  'LBL_TEAM' => 'Equipas',
  'LBL_TEAMS' => 'Equipas',
  'LBL_TEAM_ID' => 'Id Equipa',
  'LBL_ASSIGNED_TO_ID' => 'Atribuído a',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_CREATED_USER' => 'Criado por Utilizador',
  'LBL_DATE_ENTERED' => 'Data da Criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_DELETED' => 'Eliminado',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DOC_OWNER' => 'Proprietário do Documento',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado Por Nome',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_MODIFIED_USER' => 'Modificado por Utilizador',
  'LBL_NAME' => 'Nome',
  'LBL_REMOVE' => 'Remover',
  'LBL_USER_FAVORITES' => 'Utilizadores que adicionaram aos Favoritos',
  'LBL_LIST_FORM_TITLE' => 'Timesheet Lista',
  'LBL_MODULE_NAME' => 'Timesheet',
  'LBL_MODULE_TITLE' => 'Timesheet',
  'LBL_MODULE_NAME_SINGULAR' => 'Timesheet',
  'LBL_HOMEPAGE_TITLE' => 'Minha Timesheet',
  'LNK_NEW_RECORD' => 'Criar Timesheet',
  'LNK_LIST' => 'Vista Timesheet',
  'LNK_IMPORT_M07_TIMESHEET' => 'Import Timesheet',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Timesheet',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Actividades',
  'LBL_M07_TIMESHEET_SUBPANEL_TITLE' => 'Timesheet',
  'LBL_NEW_FORM_TITLE' => 'Novo Timesheet',
  'LNK_IMPORT_VCARD' => 'Import Timesheet vCard',
  'LBL_IMPORT' => 'Import Timesheet',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Timesheet record by importing a vCard from your file system.',
);