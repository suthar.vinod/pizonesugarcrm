<?php
// created: 2022-12-13 04:47:10
$mod_strings = array (
  'LBL_TEAM' => 'Grupet',
  'LBL_TEAMS' => 'Grupet',
  'LBL_TEAM_ID' => 'Id e grupit',
  'LBL_ASSIGNED_TO_ID' => 'Id e përdoruesit të caktuar',
  'LBL_ASSIGNED_TO_NAME' => 'Përdorues',
  'LBL_CREATED' => 'Krijuar nga',
  'LBL_CREATED_ID' => 'Krijuar nga Id',
  'LBL_CREATED_USER' => 'Krijuar nga përdoruesi',
  'LBL_DATE_ENTERED' => 'Të dhënat e krijuara',
  'LBL_DATE_MODIFIED' => 'Të dhënat e modifikuara',
  'LBL_DELETED' => 'E fshirë',
  'LBL_DESCRIPTION' => 'Përshkrim',
  'LBL_DOC_OWNER' => 'Pronari i Dokumentit',
  'LBL_EDIT_BUTTON' => 'Ndrysho',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikuar nga emri',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Emri',
  'LBL_MODIFIED' => 'Modifikuar nga',
  'LBL_MODIFIED_ID' => 'Modifikuar nga Id',
  'LBL_MODIFIED_NAME' => 'Modifikuar nga emri',
  'LBL_MODIFIED_USER' => 'Modifikuar nga përdoruesi',
  'LBL_NAME' => 'Emri',
  'LBL_REMOVE' => 'Fshijë',
  'LBL_USER_FAVORITES' => 'Përdoruesit të cilët preferojnë',
  'LBL_LIST_FORM_TITLE' => 'Timesheet lista',
  'LBL_MODULE_NAME' => 'Timesheet',
  'LBL_MODULE_TITLE' => 'Timesheet',
  'LBL_MODULE_NAME_SINGULAR' => 'Timesheet',
  'LBL_HOMEPAGE_TITLE' => 'Mia Timesheet',
  'LNK_NEW_RECORD' => 'Krijo Timesheet',
  'LNK_LIST' => 'Pamje Timesheet',
  'LNK_IMPORT_M07_TIMESHEET' => 'Import Timesheet',
  'LBL_SEARCH_FORM_TITLE' => 'Kërkim Timesheet',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'shiko historinë',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetet',
  'LBL_M07_TIMESHEET_SUBPANEL_TITLE' => 'Timesheet',
  'LBL_NEW_FORM_TITLE' => 'E re Timesheet',
  'LNK_IMPORT_VCARD' => 'Import Timesheet vCard',
  'LBL_IMPORT' => 'Import Timesheet',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Timesheet record by importing a vCard from your file system.',
);