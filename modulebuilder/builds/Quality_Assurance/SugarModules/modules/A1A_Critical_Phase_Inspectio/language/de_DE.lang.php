<?php
// created: 2022-12-13 04:46:58
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team-ID',
  'LBL_ASSIGNED_TO_ID' => 'Zugewiesene Benutzer-Id',
  'LBL_ASSIGNED_TO_NAME' => 'Zugewiesen an',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Erstellungsdatum',
  'LBL_DATE_MODIFIED' => 'Änderungsdatum',
  'LBL_MODIFIED' => 'Geändert von',
  'LBL_MODIFIED_ID' => 'Geändert von ID',
  'LBL_MODIFIED_NAME' => 'Geändert von Name',
  'LBL_CREATED' => 'Erstellt von:',
  'LBL_CREATED_ID' => 'Ersteller',
  'LBL_DOC_OWNER' => 'Dokument-Eigentümer',
  'LBL_USER_FAVORITES' => 'Benutzer mit Favoriten',
  'LBL_DESCRIPTION' => 'Beschreibung',
  'LBL_DELETED' => 'Gelöscht',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Erstellt von',
  'LBL_MODIFIED_USER' => 'Geändert von',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Bearbeiten',
  'LBL_REMOVE' => 'Entfernen',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Geändert von Name',
  'LBL_LIST_FORM_TITLE' => 'Critical Phase Inspections Liste',
  'LBL_MODULE_NAME' => 'Critical Phase Inspections',
  'LBL_MODULE_TITLE' => 'Critical Phase Inspections',
  'LBL_MODULE_NAME_SINGULAR' => 'Critical Phase Inspections',
  'LBL_HOMEPAGE_TITLE' => 'Mein Critical Phase Inspections',
  'LNK_NEW_RECORD' => 'Erstellen Critical Phase Inspections',
  'LNK_LIST' => 'Ansicht Critical Phase Inspections',
  'LNK_IMPORT_A1A_CRITICAL_PHASE_INSPECTIO' => 'Import Critical Phase Inspections',
  'LBL_SEARCH_FORM_TITLE' => 'Suchen Critical Phase Inspections',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Verlauf ansehen',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitäten-Stream',
  'LBL_A1A_CRITICAL_PHASE_INSPECTIO_SUBPANEL_TITLE' => 'Critical Phase Inspections',
  'LBL_NEW_FORM_TITLE' => 'Neu Critical Phase Inspections',
  'LNK_IMPORT_VCARD' => 'Import Critical Phase Inspections vCard',
  'LBL_IMPORT' => 'Import Critical Phase Inspections',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Critical Phase Inspections record by importing a vCard from your file system.',
  'LBL_ACTUAL_TIME_QUARTER' => 'Actual Time (By Quarter Hour)',
  'LBL_DATE_OF_INSPECTION' => 'Date of Inspection',
  'LBL_PHASE_OF_INSPECTION' => 'Phase of Inspection',
);