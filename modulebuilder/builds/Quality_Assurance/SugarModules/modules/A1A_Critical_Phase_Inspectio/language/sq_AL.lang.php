<?php
// created: 2022-12-13 04:46:59
$mod_strings = array (
  'LBL_TEAM' => 'Grupet',
  'LBL_TEAMS' => 'Grupet',
  'LBL_TEAM_ID' => 'Id e grupit',
  'LBL_ASSIGNED_TO_ID' => 'Id e përdoruesit të caktuar',
  'LBL_ASSIGNED_TO_NAME' => 'Përdorues',
  'LBL_TAGS_LINK' => 'Etiketat',
  'LBL_TAGS' => 'Etiketat',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Të dhënat e krijuara',
  'LBL_DATE_MODIFIED' => 'Të dhënat e modifikuara',
  'LBL_MODIFIED' => 'Modifikuar nga',
  'LBL_MODIFIED_ID' => 'Modifikuar nga Id',
  'LBL_MODIFIED_NAME' => 'Modifikuar nga emri',
  'LBL_CREATED' => 'Krijuar nga',
  'LBL_CREATED_ID' => 'Krijuar nga Id',
  'LBL_DOC_OWNER' => 'Zotëruesi i dokumentit',
  'LBL_USER_FAVORITES' => 'Përdoruesit të cilët preferojnë',
  'LBL_DESCRIPTION' => 'Përshkrim',
  'LBL_DELETED' => 'E fshirë',
  'LBL_NAME' => 'Emri',
  'LBL_CREATED_USER' => 'Krijuar nga përdoruesi',
  'LBL_MODIFIED_USER' => 'Modifikuar nga përdoruesi',
  'LBL_LIST_NAME' => 'Emri',
  'LBL_EDIT_BUTTON' => 'Ndrysho',
  'LBL_REMOVE' => 'Hiqe',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikuar nga emri',
  'LBL_LIST_FORM_TITLE' => 'Critical Phase Inspections lista',
  'LBL_MODULE_NAME' => 'Critical Phase Inspections',
  'LBL_MODULE_TITLE' => 'Critical Phase Inspections',
  'LBL_MODULE_NAME_SINGULAR' => 'Critical Phase Inspections',
  'LBL_HOMEPAGE_TITLE' => 'Mia Critical Phase Inspections',
  'LNK_NEW_RECORD' => 'Krijo Critical Phase Inspections',
  'LNK_LIST' => 'Pamje Critical Phase Inspections',
  'LNK_IMPORT_A1A_CRITICAL_PHASE_INSPECTIO' => 'Import Critical Phase Inspections',
  'LBL_SEARCH_FORM_TITLE' => 'Kërkim Critical Phase Inspections',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'shiko historinë',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetet',
  'LBL_A1A_CRITICAL_PHASE_INSPECTIO_SUBPANEL_TITLE' => 'Critical Phase Inspections',
  'LBL_NEW_FORM_TITLE' => 'E re Critical Phase Inspections',
  'LNK_IMPORT_VCARD' => 'Import Critical Phase Inspections vCard',
  'LBL_IMPORT' => 'Import Critical Phase Inspections',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Critical Phase Inspections record by importing a vCard from your file system.',
  'LBL_ACTUAL_TIME_QUARTER' => 'Actual Time (By Quarter Hour)',
  'LBL_DATE_OF_INSPECTION' => 'Date of Inspection',
  'LBL_PHASE_OF_INSPECTION' => 'Phase of Inspection',
);