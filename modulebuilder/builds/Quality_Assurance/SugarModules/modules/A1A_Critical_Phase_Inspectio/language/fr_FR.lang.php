<?php
// created: 2022-12-13 04:46:59
$mod_strings = array (
  'LBL_TEAM' => 'Équipes',
  'LBL_TEAMS' => 'Équipes',
  'LBL_TEAM_ID' => 'Équipe (ID)',
  'LBL_ASSIGNED_TO_ID' => 'ID utilisateur assigné',
  'LBL_ASSIGNED_TO_NAME' => 'Assigné à',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date de création',
  'LBL_DATE_MODIFIED' => 'Date de modification',
  'LBL_MODIFIED' => 'Modifié par',
  'LBL_MODIFIED_ID' => 'Modifié par (ID)',
  'LBL_MODIFIED_NAME' => 'Modifié par Nom',
  'LBL_CREATED' => 'Créé par',
  'LBL_CREATED_ID' => 'Créé par (ID)',
  'LBL_DOC_OWNER' => 'Propriétaire du document',
  'LBL_USER_FAVORITES' => 'Favoris pour les utilisateurs suivants',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Supprimé',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Créé par',
  'LBL_MODIFIED_USER' => 'Modifié par',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Éditer',
  'LBL_REMOVE' => 'Supprimer',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifié par Nom',
  'LBL_LIST_FORM_TITLE' => 'Critical Phase Inspections Catalogue',
  'LBL_MODULE_NAME' => 'Critical Phase Inspections',
  'LBL_MODULE_TITLE' => 'Critical Phase Inspections',
  'LBL_MODULE_NAME_SINGULAR' => 'Critical Phase Inspections',
  'LBL_HOMEPAGE_TITLE' => 'Mes Critical Phase Inspections',
  'LNK_NEW_RECORD' => 'Créer Critical Phase Inspections',
  'LNK_LIST' => 'Afficher Critical Phase Inspections',
  'LNK_IMPORT_A1A_CRITICAL_PHASE_INSPECTIO' => 'Import Critical Phase Inspections',
  'LBL_SEARCH_FORM_TITLE' => 'Recherche Critical Phase Inspections',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historique',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activités',
  'LBL_A1A_CRITICAL_PHASE_INSPECTIO_SUBPANEL_TITLE' => 'Critical Phase Inspections',
  'LBL_NEW_FORM_TITLE' => 'Nouveau Critical Phase Inspections',
  'LNK_IMPORT_VCARD' => 'Import Critical Phase Inspections vCard',
  'LBL_IMPORT' => 'Import Critical Phase Inspections',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Critical Phase Inspections record by importing a vCard from your file system.',
  'LBL_ACTUAL_TIME_QUARTER' => 'Actual Time (By Quarter Hour)',
  'LBL_DATE_OF_INSPECTION' => 'Date of Inspection',
  'LBL_PHASE_OF_INSPECTION' => 'Phase of Inspection',
);