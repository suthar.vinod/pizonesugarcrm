<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['m01_quote_document_category_dom']['Knowledege Base'] = 'Tietovarasto';
$app_list_strings['m01_quote_document_category_dom']['Marketing'] = 'Markkinointi';
$app_list_strings['m01_quote_document_category_dom']['Sales'] = 'Uusi liidi';
$app_list_strings['m01_quote_document_category_dom'][''] = '';
$app_list_strings['m01_quote_document_subcategory_dom']['FAQ'] = 'Kysymykset';
$app_list_strings['m01_quote_document_subcategory_dom']['Marketing Collateral'] = 'Markkinointimateriaali';
$app_list_strings['m01_quote_document_subcategory_dom']['Product Brochures'] = 'Tuote-esitteet';
$app_list_strings['m01_quote_document_subcategory_dom'][''] = '';
$app_list_strings['m01_quote_document_status_dom']['Active'] = 'Aktiivinen';
$app_list_strings['m01_quote_document_status_dom']['Draft'] = 'Luonnos';
$app_list_strings['m01_quote_document_status_dom']['Expired'] = 'Vanhentunut';
$app_list_strings['m01_quote_document_status_dom']['FAQ'] = 'Kysymykset';
$app_list_strings['m01_quote_document_status_dom']['Pending'] = 'Odottaa';
$app_list_strings['m01_quote_document_status_dom']['Under Review'] = 'Arvioidaan';
$app_list_strings['m01_activity_notes_category_dom']['Marketing'] = 'Markkinointi';
$app_list_strings['m01_activity_notes_category_dom']['Knowledege Base'] = 'Tietämyskanta';
$app_list_strings['m01_activity_notes_category_dom']['Sales'] = 'Myynti';
$app_list_strings['m01_activity_notes_category_dom'][''] = '';
$app_list_strings['m01_activity_notes_subcategory_dom']['Marketing Collateral'] = 'Markkinointimateriaali';
$app_list_strings['m01_activity_notes_subcategory_dom']['Product Brochures'] = 'Tuote-esitteet';
$app_list_strings['m01_activity_notes_subcategory_dom']['FAQ'] = 'UKK';
$app_list_strings['m01_activity_notes_subcategory_dom'][''] = '';
$app_list_strings['m01_activity_notes_status_dom']['Active'] = 'Aktiivinen';
$app_list_strings['m01_activity_notes_status_dom']['Draft'] = 'Luonnos';
$app_list_strings['m01_activity_notes_status_dom']['FAQ'] = 'UKK';
$app_list_strings['m01_activity_notes_status_dom']['Expired'] = 'Vanhentunut';
$app_list_strings['m01_activity_notes_status_dom']['Under Review'] = 'Arvioidaan';
$app_list_strings['m01_activity_notes_status_dom']['Pending'] = 'Odottaa';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Sales'] = 'Sales';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_sales_activity_quote_document_category_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_category_dom'][''] = '';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Active'] = 'Active';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Draft'] = 'Draft';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Expired'] = 'Expired';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Pending'] = 'Pending';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_sales_activity_quote_document_status_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_sales_activity_quote_document_status_dom'][''] = '';
$app_list_strings['moduleList']['M01_Quote_Document'] = 'Quote Documents';
$app_list_strings['moduleList']['M01_Quote'] = 'Sales Activity Quotes';
$app_list_strings['moduleList']['M01_SA_Division_Department'] = 'SA_Division_Departments';
$app_list_strings['moduleList']['M01_Sales_Activity'] = 'Sales Activities';
$app_list_strings['moduleList']['M01_Sales_Activity_Quote'] = 'Sales Activity Quotes';
$app_list_strings['moduleList']['M01_Timekeeping'] = 'Timekeeping';
$app_list_strings['moduleList']['M01_Sales'] = 'Sales';
$app_list_strings['moduleList']['M01_Sale'] = 'Sales Activities';
$app_list_strings['moduleList']['M01_SA_Division_Department_'] = 'SA_Division_Departments';
$app_list_strings['moduleListSingular']['M01_Quote_Document'] = 'Quote Document';
$app_list_strings['moduleListSingular']['M01_Quote'] = 'Sales Activity Quote';
$app_list_strings['moduleListSingular']['M01_SA_Division_Department'] = 'SA_Division_Department';
$app_list_strings['moduleListSingular']['M01_Sales_Activity'] = 'Sales Activity';
$app_list_strings['moduleListSingular']['M01_Sales_Activity_Quote'] = 'Sales Activity Quote';
$app_list_strings['moduleListSingular']['M01_Timekeeping'] = 'Timekeeping';
$app_list_strings['moduleListSingular']['M01_Sales'] = 'Sale';
$app_list_strings['moduleListSingular']['M01_Sale'] = 'Sales Activity';
$app_list_strings['moduleListSingular']['M01_SA_Division_Department_'] = 'SA_Division_Department';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Marketing'] = 'Marketing';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom']['Sales'] = 'Sales';
$app_list_strings['m01_sales_activity_quote_document_subcategory_dom'][''] = '';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Active'] = 'Active';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Draft'] = 'Draft';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Expired'] = 'Expired';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_sales_activity_quote_document_priority_dom']['Pending'] = 'Pending';
$app_list_strings['m01_quote_document_priority_dom']['Active'] = 'Active';
$app_list_strings['m01_quote_document_priority_dom']['Draft'] = 'Draft';
$app_list_strings['m01_quote_document_priority_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_priority_dom']['Expired'] = 'Expired';
$app_list_strings['m01_quote_document_priority_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_quote_document_priority_dom']['Pending'] = 'Pending';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Active'] = 'Active';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Draft'] = 'Draft';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Expired'] = 'Expired';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_sales_activity_quote_document_resolution_dom']['Pending'] = 'Pending';
$app_list_strings['m01_quote_document_resolution_dom']['Active'] = 'Active';
$app_list_strings['m01_quote_document_resolution_dom']['Draft'] = 'Draft';
$app_list_strings['m01_quote_document_resolution_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_resolution_dom']['Expired'] = 'Expired';
$app_list_strings['m01_quote_document_resolution_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_quote_document_resolution_dom']['Pending'] = 'Pending';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Active'] = 'Active';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Draft'] = 'Draft';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Expired'] = 'Expired';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_sales_activity_quote_document_type_dom']['Pending'] = 'Pending';
$app_list_strings['m01_quote_document_type_dom']['Active'] = 'Active';
$app_list_strings['m01_quote_document_type_dom']['Draft'] = 'Draft';
$app_list_strings['m01_quote_document_type_dom']['FAQ'] = 'FAQ';
$app_list_strings['m01_quote_document_type_dom']['Expired'] = 'Expired';
$app_list_strings['m01_quote_document_type_dom']['Under Review'] = 'Under Review';
$app_list_strings['m01_quote_document_type_dom']['Pending'] = 'Pending';
