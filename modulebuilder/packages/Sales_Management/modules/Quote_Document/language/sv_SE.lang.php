<?php
// created: 2022-12-13 04:46:22
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_ASSIGNED_TO_ID' => 'Tilldelat användarid',
  'LBL_ASSIGNED_TO_NAME' => 'Tilldelad till',
  'LBL_TAGS_LINK' => 'Taggar',
  'LBL_TAGS' => 'Taggar',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Skapat datum',
  'LBL_DATE_MODIFIED' => 'Senast ändrad',
  'LBL_MODIFIED' => 'Ändrad av',
  'LBL_MODIFIED_ID' => 'Redigerad av ID',
  'LBL_MODIFIED_NAME' => 'Ändrad av namn',
  'LBL_CREATED' => 'Skapad av',
  'LBL_CREATED_ID' => 'Skapat av ID',
  'LBL_DOC_OWNER' => 'Dokument ägare',
  'LBL_USER_FAVORITES' => 'Användare som favorite',
  'LBL_DESCRIPTION' => 'Beskrivning',
  'LBL_DELETED' => 'Raderad',
  'LBL_NAME' => 'Namn',
  'LBL_CREATED_USER' => 'Skapad av användare',
  'LBL_MODIFIED_USER' => 'Ändrad av användare',
  'LBL_LIST_NAME' => 'Namn',
  'LBL_EDIT_BUTTON' => 'Redigera',
  'LBL_REMOVE' => 'Radera',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ändrad av namn',
  'LBL_MODULE_NAME' => 'Quote Documents',
  'LBL_MODULE_TITLE' => 'Quote Documents',
  'LNK_NEW_DOCUMENT' => 'Skapa dokument',
  'LNK_DOCUMENT_LIST' => 'Dokumentslista',
  'LBL_SEARCH_FORM_TITLE' => 'Search Quote Document',
  'LBL_DOCUMENT_ID' => 'Dokument Id',
  'LBL_ASSIGNED_TO' => 'Tilldelad till:',
  'LBL_CATEGORY' => 'Kategori',
  'LBL_SUBCATEGORY' => 'Underkategori',
  'LBL_STATUS' => 'Status',
  'LBL_IS_TEMPLATE' => 'Är en mall',
  'LBL_TEMPLATE_TYPE' => 'Dokumenttyp',
  'LBL_REVISION_NAME' => 'Revisionsnummer',
  'LBL_MIME' => 'Mime typ',
  'LBL_REVISION' => 'Revision',
  'LBL_DOCUMENT' => 'Relaterat dokument',
  'LBL_LATEST_REVISION' => 'Senaste revision',
  'LBL_CHANGE_LOG' => 'Förändringslogg',
  'LBL_ACTIVE_DATE' => 'Publiceringsdatum',
  'LBL_EXPIRATION_DATE' => 'Förfallodatum',
  'LBL_FILE_EXTENSION' => 'Filändelse',
  'LBL_CAT_OR_SUBCAT_UNSPEC' => 'Ospecificerad',
  'LBL_NEW_FORM_TITLE' => 'Ny Quote Document',
  'LBL_DOC_NAME' => 'Dokument namn:',
  'LBL_FILENAME' => 'Filnamn:',
  'LBL_FILE_UPLOAD' => 'Fil:',
  'LBL_DOC_VERSION' => 'Revision:',
  'LBL_CATEGORY_VALUE' => 'Kategori',
  'LBL_SUBCATEGORY_VALUE' => 'Underkategori:',
  'LBL_DOC_STATUS' => 'Status:',
  'LBL_DET_TEMPLATE_TYPE' => 'Dokumenttyp',
  'LBL_DOC_DESCRIPTION' => 'Beskrivning:',
  'LBL_DOC_ACTIVE_DATE' => 'Publiceringsdatum:',
  'LBL_DOC_EXP_DATE' => 'Förfallodatum:',
  'LBL_LIST_FORM_TITLE' => 'Quote Documents List',
  'LBL_LIST_DOCUMENT' => 'Dokument',
  'LBL_LIST_CATEGORY' => 'Kategori',
  'LBL_LIST_SUBCATEGORY' => 'Underkategori',
  'LBL_LIST_REVISION' => 'Revision',
  'LBL_LIST_LAST_REV_CREATOR' => 'Publicerad av',
  'LBL_LIST_LAST_REV_DATE' => 'Revisions datum',
  'LBL_LIST_VIEW_DOCUMENT' => 'Visa',
  'LBL_LIST_DOWNLOAD' => 'Ladda ner',
  'LBL_LIST_ACTIVE_DATE' => 'Publiceringsdatum',
  'LBL_LIST_EXP_DATE' => 'Förfallodatum',
  'LBL_LIST_STATUS' => 'Status',
  'LBL_LIST_DOC_TYPE' => 'Dokumenttyp',
  'LBL_LIST_FILENAME' => 'Filnamn',
  'LBL_SF_DOCUMENT' => 'Dokument namn:',
  'LBL_SF_CATEGORY' => 'Kategori',
  'LBL_SF_SUBCATEGORY' => 'Underkategori:',
  'LBL_SF_ACTIVE_DATE' => 'Publiceringsdatum:',
  'LBL_SF_EXP_DATE' => 'Förfallodatum:',
  'DEF_CREATE_LOG' => 'Dokument skapat',
  'ERR_DOC_NAME' => 'Dokumentnamn',
  'ERR_DOC_ACTIVE_DATE' => 'Publiceringsdatum',
  'ERR_DOC_EXP_DATE' => 'Förfallodatum',
  'ERR_FILENAME' => 'Filnamn',
  'LBL_TREE_TITLE' => 'Dokumenter',
  'LBL_LIST_DOCUMENT_NAME' => 'Dokumentnamn',
  'LBL_MODULE_NAME_SINGULAR' => 'Quote Document',
  'LBL_HOMEPAGE_TITLE' => 'Min Quote Documents',
  'LNK_NEW_RECORD' => 'Create Quote Document',
  'LNK_LIST' => 'Visa Quote Documents',
  'LNK_IMPORT_M01_QUOTE_DOCUMENT' => 'Import Quote Documents',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetsström',
  'LBL_M01_QUOTE_DOCUMENT_SUBPANEL_TITLE' => 'Quote Documents',
  'LNK_IMPORT_VCARD' => 'Import Quote Document vCard',
  'LBL_IMPORT' => 'Import Quote Documents',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Quote Document record by importing a vCard from your file system.',
);