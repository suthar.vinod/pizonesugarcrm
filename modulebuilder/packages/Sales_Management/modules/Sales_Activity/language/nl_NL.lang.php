<?php
// created: 2022-12-13 04:46:23
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen aan ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum aangemaakt',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door Naam',
  'LBL_CREATED' => 'Aangemaakt Door',
  'LBL_CREATED_ID' => 'Gemaakt Door ID',
  'LBL_DOC_OWNER' => 'Eigenaar van document',
  'LBL_USER_FAVORITES' => 'Gebruikers die favoriet gemaakt hebben',
  'LBL_DESCRIPTION' => 'Beschrijving',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_NAME' => 'Naam',
  'LBL_CREATED_USER' => 'Gemaakt door Gebruiker',
  'LBL_MODIFIED_USER' => 'Gewijzigd door Gebruiker',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_EDIT_BUTTON' => 'Wijzigen',
  'LBL_REMOVE' => 'Verwijder',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Gewijzigd door Naam',
  'LBL_LIST_FORM_TITLE' => 'Sales Activities List',
  'LBL_MODULE_NAME' => 'Sales Activities',
  'LBL_MODULE_TITLE' => 'Sales Activities',
  'LBL_MODULE_NAME_SINGULAR' => 'Sales Activity',
  'LBL_HOMEPAGE_TITLE' => 'Mijn Sales Activities',
  'LNK_NEW_RECORD' => 'Create Sales Activity',
  'LNK_LIST' => 'View Sales Activities',
  'LNK_IMPORT_M01_SALES' => 'Import Sale',
  'LBL_SEARCH_FORM_TITLE' => 'Zoeken Sales Activity',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_M01_SALES_SUBPANEL_TITLE' => 'Sales',
  'LBL_NEW_FORM_TITLE' => 'Nieuw Sales Activity',
  'LNK_IMPORT_VCARD' => 'Import Sales Activity vCard',
  'LBL_IMPORT' => 'Import Sales Activities',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Sales Activity record by importing a vCard from your file system.',
  'LNK_IMPORT_M01_SALE' => 'Import Sales Activity',
  'LBL_M01_SALE_SUBPANEL_TITLE' => 'Sales Activities',
  'LNK_IMPORT_M01_SALES_ACTIVITY' => 'Import Sales Activities',
  'LBL_M01_SALES_ACTIVITY_SUBPANEL_TITLE' => 'Sales Activities',
);