<?php
// created: 2022-12-13 04:45:57
$config = array (
  'team_security' => true,
  'assignable' => true,
  'acl' => true,
  'has_tab' => true,
  'studio' => true,
  'audit' => true,
  'activity_enabled' => 0,
  'templates' => 
  array (
    'basic' => 1,
  ),
  'label' => 'Sales Activity Quotes',
  'label_singular' => 'Sales Activity Quote',
  'importable' => true,
  'taggable' => 1,
);