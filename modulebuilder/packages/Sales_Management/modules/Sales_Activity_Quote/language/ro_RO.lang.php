<?php
// created: 2022-12-13 04:46:23
$mod_strings = array (
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'ID Echipa:',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Atrbuit lui',
  'LBL_TAGS_LINK' => 'Etichete',
  'LBL_TAGS' => 'Etichete',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data intrarii',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_MODIFIED' => 'Modificata de',
  'LBL_MODIFIED_ID' => 'Modificata de ID',
  'LBL_MODIFIED_NAME' => 'Modificata de Nume',
  'LBL_CREATED' => 'Creeata de',
  'LBL_CREATED_ID' => 'Creata de ID',
  'LBL_DOC_OWNER' => 'Deţinător document',
  'LBL_USER_FAVORITES' => 'Utilizatori cu setări favorite',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_DELETED' => 'Stearsa',
  'LBL_NAME' => 'Nume',
  'LBL_CREATED_USER' => 'Creeata de Utilizator',
  'LBL_MODIFIED_USER' => 'Modificata de Utilizator',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_REMOVE' => 'Sterge',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificata de Nume',
  'LBL_LIST_FORM_TITLE' => 'Sales Activity Quotes Lista',
  'LBL_MODULE_NAME' => 'Sales Activity Quotes',
  'LBL_MODULE_TITLE' => 'Sales Activity Quotes',
  'LBL_MODULE_NAME_SINGULAR' => 'Sales Activity Quote',
  'LBL_HOMEPAGE_TITLE' => 'Al meu Sales Activity Quotes',
  'LNK_NEW_RECORD' => 'Creează Sales Activity Quote',
  'LNK_LIST' => 'Vizualizare Sales Activity Quotes',
  'LNK_IMPORT_M01_QUOTE' => 'Import Sales Activity Quote',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta Sales Activity Quote',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_M01_QUOTE_SUBPANEL_TITLE' => 'Sales Activity Quotes',
  'LBL_NEW_FORM_TITLE' => 'Nou Sales Activity Quote',
  'LNK_IMPORT_VCARD' => 'Import Sales Activity Quote vCard',
  'LBL_IMPORT' => 'Import Sales Activity Quotes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Sales Activity Quote record by importing a vCard from your file system.',
  'LNK_IMPORT_M01_SALES_ACTIVITY_QUOTE' => 'Import Sales Activity Quotes',
  'LBL_M01_SALES_ACTIVITY_QUOTE_SUBPANEL_TITLE' => 'Sales Activity Quotes',
);