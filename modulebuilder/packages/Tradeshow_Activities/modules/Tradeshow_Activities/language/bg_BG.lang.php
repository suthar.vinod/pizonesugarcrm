<?php
// created: 2022-12-13 04:46:27
$mod_strings = array (
  'LBL_TEAM' => 'Екип',
  'LBL_TEAMS' => 'Екип',
  'LBL_TEAM_ID' => 'Екип',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Потребител',
  'LBL_TAGS_LINK' => 'Етикети',
  'LBL_TAGS' => 'Етикети',
  'LBL_ID' => 'Идентификатор',
  'LBL_DATE_ENTERED' => 'Създадено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_DOC_OWNER' => 'Собственик на документа',
  'LBL_USER_FAVORITES' => 'Потребители които харесват',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Изтрити',
  'LBL_NAME' => 'Име',
  'LBL_CREATED_USER' => 'Създадено от',
  'LBL_MODIFIED_USER' => 'Модифицирано от',
  'LBL_LIST_NAME' => 'Име',
  'LBL_EDIT_BUTTON' => 'Редактирай',
  'LBL_REMOVE' => 'Премахни',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Модифицирано от',
  'LBL_LIST_FORM_TITLE' => 'Tradeshow Activities Списък',
  'LBL_MODULE_NAME' => 'Tradeshow Activities',
  'LBL_MODULE_TITLE' => 'Tradeshow Activities',
  'LBL_MODULE_NAME_SINGULAR' => 'Tradeshow Activity',
  'LBL_HOMEPAGE_TITLE' => 'Мои Tradeshow Activities',
  'LNK_NEW_RECORD' => 'Създай Tradeshow Activity',
  'LNK_LIST' => 'Изглед Tradeshow Activities',
  'LNK_IMPORT_TA_TRADESHOW_ACTIVITIES' => 'Import Tradeshow Activities',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Tradeshow Activity',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_TA_TRADESHOW_ACTIVITIES_SUBPANEL_TITLE' => 'Tradeshow Activities',
  'LBL_NEW_FORM_TITLE' => 'Нов Tradeshow Activity',
  'LNK_IMPORT_VCARD' => 'Import Tradeshow Activity vCard',
  'LBL_IMPORT' => 'Import Tradeshow Activities',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Tradeshow Activity record by importing a vCard from your file system.',
);