<?php
// created: 2022-12-13 04:46:27
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruger-id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'Id',
  'LBL_DATE_ENTERED' => 'Oprettet den',
  'LBL_DATE_MODIFIED' => 'Ændret den',
  'LBL_MODIFIED' => 'Ændret af',
  'LBL_MODIFIED_ID' => 'Ændret af id',
  'LBL_MODIFIED_NAME' => 'Ændret af navn',
  'LBL_CREATED' => 'Oprettet af',
  'LBL_CREATED_ID' => 'Oprettet af id',
  'LBL_DOC_OWNER' => 'Dokument ejer',
  'LBL_USER_FAVORITES' => 'Brugernes favorit',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Oprettet af bruger',
  'LBL_MODIFIED_USER' => 'Ændret af bruger',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ændret af navn:',
  'LBL_LIST_FORM_TITLE' => 'Tradeshow Activities Liste',
  'LBL_MODULE_NAME' => 'Tradeshow Activities',
  'LBL_MODULE_TITLE' => 'Tradeshow Activities',
  'LBL_MODULE_NAME_SINGULAR' => 'Tradeshow Activity',
  'LBL_HOMEPAGE_TITLE' => 'Min Tradeshow Activities',
  'LNK_NEW_RECORD' => 'Opret Tradeshow Activity',
  'LNK_LIST' => 'Vis Tradeshow Activities',
  'LNK_IMPORT_TA_TRADESHOW_ACTIVITIES' => 'Import Tradeshow Activities',
  'LBL_SEARCH_FORM_TITLE' => 'Søg Tradeshow Activity',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vis historik',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteter',
  'LBL_TA_TRADESHOW_ACTIVITIES_SUBPANEL_TITLE' => 'Tradeshow Activities',
  'LBL_NEW_FORM_TITLE' => 'Ny Tradeshow Activity',
  'LNK_IMPORT_VCARD' => 'Import Tradeshow Activity vCard',
  'LBL_IMPORT' => 'Import Tradeshow Activities',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Tradeshow Activity record by importing a vCard from your file system.',
);