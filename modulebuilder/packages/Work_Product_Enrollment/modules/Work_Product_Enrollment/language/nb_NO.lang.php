<?php
// created: 2022-12-13 04:46:30
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_LIST_FORM_TITLE' => 'Work Product Enrollment Liste',
  'LBL_MODULE_NAME' => 'Work Product Enrollment',
  'LBL_MODULE_TITLE' => 'Work Product Enrollment',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Enrollment',
  'LBL_HOMEPAGE_TITLE' => 'Min Work Product Enrollment',
  'LNK_NEW_RECORD' => 'Opprett Work Product Enrollment',
  'LNK_LIST' => 'Vis Work Product Enrollment',
  'LNK_IMPORT_WPE_WORK_PRODUCT_ENROLLMENT' => 'Import Work Product Enrollment',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Work Product Enrollment',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_WPE_WORK_PRODUCT_ENROLLMENT_SUBPANEL_TITLE' => 'Work Product Enrollment',
  'LBL_NEW_FORM_TITLE' => 'Ny Work Product Enrollment',
  'LNK_IMPORT_VCARD' => 'Import Work Product Enrollment vCard',
  'LBL_IMPORT' => 'Import Work Product Enrollment',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Enrollment record by importing a vCard from your file system.',
);