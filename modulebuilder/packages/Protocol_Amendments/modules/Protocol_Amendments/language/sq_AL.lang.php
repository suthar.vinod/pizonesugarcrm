<?php
// created: 2022-12-13 04:46:16
$mod_strings = array (
  'LBL_TEAM' => 'Grupet',
  'LBL_TEAMS' => 'Grupet',
  'LBL_TEAM_ID' => 'Id e grupit',
  'LBL_ASSIGNED_TO_ID' => 'Id e përdoruesit të caktuar',
  'LBL_ASSIGNED_TO_NAME' => 'Përdorues',
  'LBL_TAGS_LINK' => 'Etiketat',
  'LBL_TAGS' => 'Etiketat',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Të dhënat e krijuara',
  'LBL_DATE_MODIFIED' => 'Të dhënat e modifikuara',
  'LBL_MODIFIED' => 'Modifikuar nga',
  'LBL_MODIFIED_ID' => 'Modifikuar nga Id',
  'LBL_MODIFIED_NAME' => 'Modifikuar nga emri',
  'LBL_CREATED' => 'Krijuar nga',
  'LBL_CREATED_ID' => 'Krijuar nga Id',
  'LBL_DOC_OWNER' => 'Pronari i Dokumentit',
  'LBL_USER_FAVORITES' => 'Përdoruesit të cilët preferojnë',
  'LBL_DESCRIPTION' => 'Përshkrim',
  'LBL_DELETED' => 'E fshirë',
  'LBL_NAME' => 'Emri',
  'LBL_CREATED_USER' => 'Krijuar nga përdoruesi',
  'LBL_MODIFIED_USER' => 'Modifikuar nga përdoruesi',
  'LBL_LIST_NAME' => 'Emri',
  'LBL_EDIT_BUTTON' => 'Ndrysho',
  'LBL_REMOVE' => 'Fshijë',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikuar nga emri',
  'LBL_LIST_FORM_TITLE' => 'Protocol Amendments lista',
  'LBL_MODULE_NAME' => 'Protocol Amendments',
  'LBL_MODULE_TITLE' => 'Protocol Amendments',
  'LBL_MODULE_NAME_SINGULAR' => 'Protocol Amendment',
  'LBL_HOMEPAGE_TITLE' => 'Mia Protocol Amendments',
  'LNK_NEW_RECORD' => 'Krijo Protocol Amendment',
  'LNK_LIST' => 'Pamje Protocol Amendments',
  'LNK_IMPORT_M99_PROTOCOL_AMENDMENTS' => 'Import Protocol Amendments',
  'LBL_SEARCH_FORM_TITLE' => 'Kërkim Protocol Amendment',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'shiko historinë',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetet',
  'LBL_M99_PROTOCOL_AMENDMENTS_SUBPANEL_TITLE' => 'Protocol Amendments',
  'LBL_NEW_FORM_TITLE' => 'E re Protocol Amendment',
  'LNK_IMPORT_VCARD' => 'Import Protocol Amendment vCard',
  'LBL_IMPORT' => 'Import Protocol Amendments',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Protocol Amendment record by importing a vCard from your file system.',
);