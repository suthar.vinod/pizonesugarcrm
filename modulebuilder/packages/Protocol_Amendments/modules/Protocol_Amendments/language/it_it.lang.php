<?php
// created: 2022-12-13 04:46:16
$mod_strings = array (
  'LBL_TEAM' => 'Gruppi',
  'LBL_TEAMS' => 'Gruppi',
  'LBL_TEAM_ID' => 'Id Gruppo',
  'LBL_ASSIGNED_TO_ID' => 'Assegnato a ID:',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LBL_TAGS_LINK' => 'Tag',
  'LBL_TAGS' => 'Tag',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Inserimento',
  'LBL_DATE_MODIFIED' => 'Data Modifica',
  'LBL_MODIFIED' => 'Modificato da',
  'LBL_MODIFIED_ID' => 'Modificato da Id',
  'LBL_MODIFIED_NAME' => 'Modificato da Nome',
  'LBL_CREATED' => 'Creato da',
  'LBL_CREATED_ID' => 'Creato da Id',
  'LBL_DOC_OWNER' => 'Possessore del Documento',
  'LBL_USER_FAVORITES' => 'Preferenze Utenti',
  'LBL_DESCRIPTION' => 'Descrizione',
  'LBL_DELETED' => 'Cancellato',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Creato da utente',
  'LBL_MODIFIED_USER' => 'Modificato da utente',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Modifica',
  'LBL_REMOVE' => 'Rimuovi',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificato dal Nome',
  'LBL_LIST_FORM_TITLE' => 'Protocol Amendments Lista',
  'LBL_MODULE_NAME' => 'Protocol Amendments',
  'LBL_MODULE_TITLE' => 'Protocol Amendments',
  'LBL_MODULE_NAME_SINGULAR' => 'Protocol Amendment',
  'LBL_HOMEPAGE_TITLE' => 'Mio Protocol Amendments',
  'LNK_NEW_RECORD' => 'Crea Protocol Amendment',
  'LNK_LIST' => 'Visualizza Protocol Amendments',
  'LNK_IMPORT_M99_PROTOCOL_AMENDMENTS' => 'Import Protocol Amendments',
  'LBL_SEARCH_FORM_TITLE' => 'Ricerca Protocol Amendment',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Cronologia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Attività',
  'LBL_M99_PROTOCOL_AMENDMENTS_SUBPANEL_TITLE' => 'Protocol Amendments',
  'LBL_NEW_FORM_TITLE' => 'Nuovo Protocol Amendment',
  'LNK_IMPORT_VCARD' => 'Import Protocol Amendment vCard',
  'LBL_IMPORT' => 'Import Protocol Amendments',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Protocol Amendment record by importing a vCard from your file system.',
);