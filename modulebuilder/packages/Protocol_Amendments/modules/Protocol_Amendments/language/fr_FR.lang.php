<?php
// created: 2022-12-13 04:46:16
$mod_strings = array (
  'LBL_TEAM' => 'Equipe',
  'LBL_TEAMS' => 'Equipes',
  'LBL_TEAM_ID' => 'Equipe (ID)',
  'LBL_ASSIGNED_TO_ID' => 'Assigné à (ID)',
  'LBL_ASSIGNED_TO_NAME' => 'Assigné à',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date de création',
  'LBL_DATE_MODIFIED' => 'Date de modification',
  'LBL_MODIFIED' => 'Modifié par',
  'LBL_MODIFIED_ID' => 'Modifié par (ID)',
  'LBL_MODIFIED_NAME' => 'Modifié par',
  'LBL_CREATED' => 'Créé par',
  'LBL_CREATED_ID' => 'Créé par (ID)',
  'LBL_DOC_OWNER' => 'Propriétaire du document',
  'LBL_USER_FAVORITES' => 'Favoris pour les utilisateurs suivants',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Supprimé',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Créé par',
  'LBL_MODIFIED_USER' => 'Modifié par',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Editer',
  'LBL_REMOVE' => 'Supprimer',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifié par',
  'LBL_LIST_FORM_TITLE' => 'Protocol Amendments Catalogue',
  'LBL_MODULE_NAME' => 'Protocol Amendments',
  'LBL_MODULE_TITLE' => 'Protocol Amendments',
  'LBL_MODULE_NAME_SINGULAR' => 'Protocol Amendment',
  'LBL_HOMEPAGE_TITLE' => 'Mes Protocol Amendments',
  'LNK_NEW_RECORD' => 'Créer Protocol Amendment',
  'LNK_LIST' => 'Afficher Protocol Amendments',
  'LNK_IMPORT_M99_PROTOCOL_AMENDMENTS' => 'Import Protocol Amendments',
  'LBL_SEARCH_FORM_TITLE' => 'Recherche Protocol Amendment',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historique',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activités',
  'LBL_M99_PROTOCOL_AMENDMENTS_SUBPANEL_TITLE' => 'Protocol Amendments',
  'LBL_NEW_FORM_TITLE' => 'Nouveau Protocol Amendment',
  'LNK_IMPORT_VCARD' => 'Import Protocol Amendment vCard',
  'LBL_IMPORT' => 'Import Protocol Amendments',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Protocol Amendment record by importing a vCard from your file system.',
);