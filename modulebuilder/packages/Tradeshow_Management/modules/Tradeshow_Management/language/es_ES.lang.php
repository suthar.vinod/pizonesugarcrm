<?php
// created: 2022-12-13 04:46:29
$mod_strings = array (
  'LBL_TEAM' => 'Equipos',
  'LBL_TEAMS' => 'Equipos',
  'LBL_TEAM_ID' => 'Id Equipo',
  'LBL_ASSIGNED_TO_ID' => 'Id del Usuario Asignado',
  'LBL_ASSIGNED_TO_NAME' => 'Usuario',
  'LBL_TAGS_LINK' => 'Etiquetas',
  'LBL_TAGS' => 'Etiquetas',
  'LBL_ID' => 'Id.',
  'LBL_DATE_ENTERED' => 'Fecha de Creación',
  'LBL_DATE_MODIFIED' => 'Última Modificación',
  'LBL_MODIFIED' => 'Modificado por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado por nombre',
  'LBL_CREATED' => 'Creado Por',
  'LBL_CREATED_ID' => 'Creado Por Id',
  'LBL_DOC_OWNER' => 'Propietario del documento',
  'LBL_USER_FAVORITES' => 'Usuarios Favoritos',
  'LBL_DESCRIPTION' => 'Descripción',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nombre',
  'LBL_CREATED_USER' => 'Creado Por Usuario',
  'LBL_MODIFIED_USER' => 'Modificado Por Usuario',
  'LBL_LIST_NAME' => 'Nombre',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Quitar',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado por nombre',
  'LBL_LIST_FORM_TITLE' => 'Tradeshow Management Lista',
  'LBL_MODULE_NAME' => 'Tradeshow Management',
  'LBL_MODULE_TITLE' => 'Tradeshow Management',
  'LBL_MODULE_NAME_SINGULAR' => 'Tradeshow Management',
  'LBL_HOMEPAGE_TITLE' => 'Mi Tradeshow Management',
  'LNK_NEW_RECORD' => 'Crear Tradeshow Management',
  'LNK_LIST' => 'Vista Tradeshow Management',
  'LNK_IMPORT_TM_TRADESHOW_MANAGEMENT' => 'Import Tradeshow Management',
  'LBL_SEARCH_FORM_TITLE' => 'Buscar Tradeshow Management',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Flujo de actividad',
  'LBL_TM_TRADESHOW_MANAGEMENT_SUBPANEL_TITLE' => 'Tradeshow Management',
  'LBL_NEW_FORM_TITLE' => 'Nuevo Tradeshow Management',
  'LNK_IMPORT_VCARD' => 'Import Tradeshow Management vCard',
  'LBL_IMPORT' => 'Import Tradeshow Management',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Tradeshow Management record by importing a vCard from your file system.',
);