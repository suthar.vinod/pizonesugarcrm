<?php
// created: 2022-12-13 04:46:29
$mod_strings = array (
  'LBL_TEAM' => 'Equipes',
  'LBL_TEAMS' => 'Equipes',
  'LBL_TEAM_ID' => 'Id da Equipe',
  'LBL_ASSIGNED_TO_ID' => 'ID do usuário atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_TAGS_LINK' => 'Marcações',
  'LBL_TAGS' => 'Marcações',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado por nome',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_DOC_OWNER' => 'Proprietário do documento',
  'LBL_USER_FAVORITES' => 'Usuários Favoritos',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Excluído',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado pelo usuário',
  'LBL_MODIFIED_USER' => 'Modificado pelo usuário',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado por nome',
  'LBL_LIST_FORM_TITLE' => 'Tradeshow Management Lista',
  'LBL_MODULE_NAME' => 'Tradeshow Management',
  'LBL_MODULE_TITLE' => 'Tradeshow Management',
  'LBL_MODULE_NAME_SINGULAR' => 'Tradeshow Management',
  'LBL_HOMEPAGE_TITLE' => 'Minha Tradeshow Management',
  'LNK_NEW_RECORD' => 'Criar Tradeshow Management',
  'LNK_LIST' => 'Visualização Tradeshow Management',
  'LNK_IMPORT_TM_TRADESHOW_MANAGEMENT' => 'Import Tradeshow Management',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Tradeshow Management',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Cadeia de atividades',
  'LBL_TM_TRADESHOW_MANAGEMENT_SUBPANEL_TITLE' => 'Tradeshow Management',
  'LBL_NEW_FORM_TITLE' => 'Novo Tradeshow Management',
  'LNK_IMPORT_VCARD' => 'Import Tradeshow Management vCard',
  'LBL_IMPORT' => 'Import Tradeshow Management',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Tradeshow Management record by importing a vCard from your file system.',
);