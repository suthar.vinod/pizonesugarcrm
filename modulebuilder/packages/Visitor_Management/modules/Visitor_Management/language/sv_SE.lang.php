<?php
// created: 2022-12-13 04:46:30
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Lag-ID',
  'LBL_ASSIGNED_TO_ID' => 'Tilldelat användar-ID',
  'LBL_ASSIGNED_TO_NAME' => 'Tilldelad användare',
  'LBL_TAGS_LINK' => 'Taggar',
  'LBL_TAGS' => 'Taggar',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum skapat',
  'LBL_DATE_MODIFIED' => 'Modifierat Datum',
  'LBL_MODIFIED' => 'Modifierat Av',
  'LBL_MODIFIED_ID' => 'Modifierat Av Id',
  'LBL_MODIFIED_NAME' => 'Modifierat Av Namn',
  'LBL_CREATED' => 'Skapat Av',
  'LBL_CREATED_ID' => 'Skapat Av',
  'LBL_DOC_OWNER' => 'Ägare av dokument',
  'LBL_USER_FAVORITES' => 'Användare som favorite',
  'LBL_DESCRIPTION' => 'Beskrivning',
  'LBL_DELETED' => 'Raderad',
  'LBL_NAME' => 'Namn',
  'LBL_CREATED_USER' => 'Skapat Av Användare',
  'LBL_MODIFIED_USER' => 'Modifierat Av Användare',
  'LBL_LIST_NAME' => 'Namn',
  'LBL_EDIT_BUTTON' => 'Redigera',
  'LBL_REMOVE' => 'Ta bort',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ändrad av namn',
  'LBL_LIST_FORM_TITLE' => 'Visitor Management List',
  'LBL_MODULE_NAME' => 'Visitor Management',
  'LBL_MODULE_TITLE' => 'Visitor Management',
  'LBL_MODULE_NAME_SINGULAR' => 'Visitor Management',
  'LBL_HOMEPAGE_TITLE' => 'Min Visitor Management',
  'LNK_NEW_RECORD' => 'Create Visitor Management',
  'LNK_LIST' => 'Visa Visitor Management',
  'LNK_IMPORT_VM01_VISITOR_MANAGEMENT' => 'Import Visitor Management',
  'LBL_SEARCH_FORM_TITLE' => 'Search Visitor Management',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetsström',
  'LBL_VM01_VISITOR_MANAGEMENT_SUBPANEL_TITLE' => 'Visitor Management',
  'LBL_NEW_FORM_TITLE' => 'Ny Visitor Management',
  'LNK_IMPORT_VCARD' => 'Import Visitor Management vCard',
  'LBL_IMPORT' => 'Import Visitor Management',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Visitor Management record by importing a vCard from your file system.',
);