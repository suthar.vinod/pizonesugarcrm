<?php
// created: 2022-12-13 04:46:29
$mod_strings = array (
  'LBL_TEAM' => 'Tiimit',
  'LBL_TEAMS' => 'Tiimit',
  'LBL_TEAM_ID' => 'Tiimin tunnus',
  'LBL_ASSIGNED_TO_ID' => 'Vastuukäyttäjän ID',
  'LBL_ASSIGNED_TO_NAME' => 'Vastuuhenkilö',
  'LBL_TAGS_LINK' => 'Tagit',
  'LBL_TAGS' => 'Tagit',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Luontipäivä',
  'LBL_DATE_MODIFIED' => 'Muokkattu viimeksi',
  'LBL_MODIFIED' => 'Muokannut',
  'LBL_MODIFIED_ID' => 'Muokkaajan ID',
  'LBL_MODIFIED_NAME' => 'Muokkaajan nimi',
  'LBL_CREATED' => 'Luonut',
  'LBL_CREATED_ID' => 'Luojan ID',
  'LBL_DOC_OWNER' => 'Tiedoston omistaja',
  'LBL_USER_FAVORITES' => 'Käyttäjät, jotka ottivat suosikikseen',
  'LBL_DESCRIPTION' => 'Kuvaus',
  'LBL_DELETED' => 'Poistettu',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Luoja',
  'LBL_MODIFIED_USER' => 'Muokkaaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Muokkaa',
  'LBL_REMOVE' => 'Poista',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muokkaajan nimi',
  'LBL_LIST_FORM_TITLE' => 'Visitor Management Lista',
  'LBL_MODULE_NAME' => 'Visitor Management',
  'LBL_MODULE_TITLE' => 'Visitor Management',
  'LBL_MODULE_NAME_SINGULAR' => 'Visitor Management',
  'LBL_HOMEPAGE_TITLE' => 'Oma Visitor Management',
  'LNK_NEW_RECORD' => 'Luo Visitor Management',
  'LNK_LIST' => 'Näkymä Visitor Management',
  'LNK_IMPORT_VM01_VISITOR_MANAGEMENT' => 'Import Visitor Management',
  'LBL_SEARCH_FORM_TITLE' => 'Etsi Visitor Management',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Näytä historia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteetit',
  'LBL_VM01_VISITOR_MANAGEMENT_SUBPANEL_TITLE' => 'Visitor Management',
  'LBL_NEW_FORM_TITLE' => 'Uusi Visitor Management',
  'LNK_IMPORT_VCARD' => 'Import Visitor Management vCard',
  'LBL_IMPORT' => 'Import Visitor Management',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Visitor Management record by importing a vCard from your file system.',
);