<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['maj_contact_documents_category_dom']['Marketing'] = 'Rinkodara';
$app_list_strings['maj_contact_documents_category_dom']['Knowledege Base'] = 'Žinių bazė';
$app_list_strings['maj_contact_documents_category_dom']['Sales'] = 'Pardavimas';
$app_list_strings['maj_contact_documents_category_dom'][''] = '';
$app_list_strings['maj_contact_documents_subcategory_dom']['Marketing Collateral'] = 'Rinkodaros dokumentacija';
$app_list_strings['maj_contact_documents_subcategory_dom']['Product Brochures'] = 'Produktų brošiūros';
$app_list_strings['maj_contact_documents_subcategory_dom']['FAQ'] = 'DUK';
$app_list_strings['maj_contact_documents_subcategory_dom'][''] = '';
$app_list_strings['maj_contact_documents_status_dom']['Active'] = 'Aktyvus';
$app_list_strings['maj_contact_documents_status_dom']['Draft'] = 'Juodraštis';
$app_list_strings['maj_contact_documents_status_dom']['FAQ'] = 'DUK';
$app_list_strings['maj_contact_documents_status_dom']['Expired'] = 'Nebegalioja';
$app_list_strings['maj_contact_documents_status_dom']['Under Review'] = 'Peržiūrimas';
$app_list_strings['maj_contact_documents_status_dom']['Pending'] = 'Laukiantis';
$app_list_strings['moduleList']['MAJ_Contact_Documents'] = 'Contact Documents';
$app_list_strings['moduleListSingular']['MAJ_Contact_Documents'] = 'Contact Document';
