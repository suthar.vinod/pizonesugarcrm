<?php
// created: 2022-12-13 04:46:15
$mod_strings = array (
  'LBL_TEAM' => 'Команди',
  'LBL_TEAMS' => 'Команди',
  'LBL_TEAM_ID' => 'Команда (Id)',
  'LBL_ASSIGNED_TO_ID' => 'Відповідальний (-а) (Id)',
  'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (-а)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_DATE_MODIFIED' => 'Дата змінення',
  'LBL_MODIFIED' => 'Змінено:',
  'LBL_MODIFIED_ID' => 'Змінено (Id)',
  'LBL_MODIFIED_NAME' => 'Змінено користувачем',
  'LBL_CREATED' => 'Створено',
  'LBL_CREATED_ID' => 'Створено (Id)',
  'LBL_DOC_OWNER' => 'Власник документа',
  'LBL_USER_FAVORITES' => 'Користувачі, які додали в Обране',
  'LBL_DESCRIPTION' => 'Опис',
  'LBL_DELETED' => 'Видалено',
  'LBL_NAME' => 'Назва',
  'LBL_CREATED_USER' => 'Створенено користувачем',
  'LBL_MODIFIED_USER' => 'Змінено користувачем',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_EDIT_BUTTON' => 'Редагувати',
  'LBL_REMOVE' => 'Видалити',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Змінено користувачем',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Products Список',
  'LBL_MODULE_NAME' => 'Products',
  'LBL_MODULE_TITLE' => 'Products',
  'LBL_MODULE_NAME_SINGULAR' => 'Product',
  'LBL_HOMEPAGE_TITLE' => 'Мій Products',
  'LNK_NEW_RECORD' => 'Створити Product',
  'LNK_LIST' => 'Переглянути Products',
  'LNK_IMPORT_PROD_PRODUCT' => 'Import Products',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук Product',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Переглянути історію',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Стрічка активностей',
  'LBL_PROD_PRODUCT_SUBPANEL_TITLE' => 'Products',
  'LBL_NEW_FORM_TITLE' => 'Новий Product',
  'LNK_IMPORT_VCARD' => 'Import Product vCard',
  'LBL_IMPORT' => 'Import Products',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Product record by importing a vCard from your file system.',
  'LBL_CATEGORY' => 'Category',
  'LBL_TYPE_2' => 'Type',
  'LBL_STOCK_QUANTITY' => 'Stock Quantity',
  'LBL_MANUFACTURER' => 'Manufacturer',
  'LBL_PRODUCT_ID_2' => 'Product ID',
  'LBL_MANUFACTURER_ID' => 'Manufacturer ID',
  'LBL_EXTERNAL_BARCODE' => 'External Barcode',
  'LBL_VENDOR_ACCOUNT_ID' => 'Vendor (related Company ID)',
  'LBL_VENDOR' => 'Vendor',
  'LBL_PURCHASE_UNIT' => 'Purchase Unit',
  'LBL_QUANTITY_IN_UNIT' => 'Quantity in Unit',
  'LBL_CURRENCY' => 'Currency',
  'LBL_COST' => 'Cost',
  'LBL_COST_PER_UNIT' => 'Cost Per Unit',
  'LBL_PURCHASE_UNIT_TEXT' => 'Purchase Unit Text',
  'LBL_PRODUCT_NAME' => 'Product Name',
  'LBL_DEPARTMENT' => 'Department',
  'LBL_PRODUCT_UNIT' => 'Product Unit',
  'LBL_COST_PER_UNIT_2' => 'Cost Per Unit',
  'LBL_UNIT_QUANTITY' => 'Unit Quantity',
  'LBL_COST_PER_EACH' => 'Cost Per Each',
  'LBL_COST_PER_EACH_2' => 'Cost Per Each',
  'LBL_QUALITY_REQUIREMENT' => 'Quality Requirement',
  'LBL_SUBTYPE' => 'Subtype',
  'LBL_CONCENTRATION' => 'Concentration',
  'LBL_CONCENTRATION_UNIT_U_UNITS_ID' => 'Concentration Unit (related Unit ID)',
  'LBL_CONCENTRATION_UNIT' => 'Concentration Unit',
  'LBL_ALLOWED_STORAGE' => 'Allowed Storage',
  'LBL_STOCK' => 'Stock',
);