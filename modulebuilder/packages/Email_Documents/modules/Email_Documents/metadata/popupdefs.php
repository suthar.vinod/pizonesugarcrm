<?php
$popupMeta = array (
    'moduleMain' => 'EDoc_Email_Documents',
    'varName' => 'EDoc_Email_Documents',
    'orderBy' => 'edoc_email_documents.name',
    'whereClauses' => array (
  'name' => 'edoc_email_documents.name',
),
    'searchInputs' => array (
  0 => 'edoc_email_documents_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'DOCUMENT_NAME' => 
  array (
    'type' => 'name',
    'default' => true,
    'label' => 'LBL_NAME',
    'width' => 10,
  ),
  'TYPE_2' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_TYPE_2',
    'width' => 10,
  ),
  'DEPARTMENT' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_DEPARTMENT',
    'width' => 10,
  ),
),
);
