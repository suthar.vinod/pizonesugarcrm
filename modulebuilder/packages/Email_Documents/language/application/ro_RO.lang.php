<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['edoc_email_documents_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['edoc_email_documents_category_dom']['Knowledege Base'] = 'Baza de cunostinte';
$app_list_strings['edoc_email_documents_category_dom']['Sales'] = 'Vanzari';
$app_list_strings['edoc_email_documents_category_dom'][''] = '';
$app_list_strings['edoc_email_documents_subcategory_dom']['Marketing Collateral'] = 'Colaterala de Marketing';
$app_list_strings['edoc_email_documents_subcategory_dom']['Product Brochures'] = 'Brosuri ale produsului';
$app_list_strings['edoc_email_documents_subcategory_dom']['FAQ'] = 'Întrebări frecvente';
$app_list_strings['edoc_email_documents_subcategory_dom'][''] = '';
$app_list_strings['edoc_email_documents_status_dom']['Active'] = 'Activ';
$app_list_strings['edoc_email_documents_status_dom']['Draft'] = 'Schita';
$app_list_strings['edoc_email_documents_status_dom']['FAQ'] = 'Întrebări frecvente';
$app_list_strings['edoc_email_documents_status_dom']['Expired'] = 'Expirat';
$app_list_strings['edoc_email_documents_status_dom']['Under Review'] = 'În curs de revizuire';
$app_list_strings['edoc_email_documents_status_dom']['Pending'] = 'In asteptare';
$app_list_strings['moduleList']['EDoc_Email_Documents'] = 'Email Documents';
$app_list_strings['moduleListSingular']['EDoc_Email_Documents'] = 'Email Document';
$app_list_strings['department_list']['Analytical Services'] = 'Analytical';
$app_list_strings['department_list']['Business Development'] = 'Business Development';
$app_list_strings['department_list']['Facilities'] = 'Facilities';
$app_list_strings['department_list']['Finance'] = 'Finance';
$app_list_strings['department_list']['Histology Services'] = 'Histology Services';
$app_list_strings['department_list']['Human_Resources'] = 'Human Resources';
$app_list_strings['department_list']['In life Large Animal Care'] = 'In-life Large Animal Care';
$app_list_strings['department_list']['In life Large Animal Research'] = 'In-life Large Animal Research';
$app_list_strings['department_list']['In life Small Animal Care'] = 'In-life Small Animal Care';
$app_list_strings['department_list']['In life Small Animal Research'] = 'In-life Small Animal Research';
$app_list_strings['department_list']['Information_Technology'] = 'Information Technology';
$app_list_strings['department_list']['Interventional Surgical Research'] = 'Interventional Surgical Research';
$app_list_strings['department_list']['Lab Services'] = 'Lab Services';
$app_list_strings['department_list']['Operations Support'] = 'Operations Support';
$app_list_strings['department_list']['Pathology Services'] = 'Pathology Services';
$app_list_strings['department_list']['Pharmacology'] = 'Pharmacology Services';
$app_list_strings['department_list']['Process Improvement'] = 'Process Improvement';
$app_list_strings['department_list']['Quality Assurance Unit'] = 'Quality Assurance Unit';
$app_list_strings['department_list']['Regulatory Services'] = 'Regulatory Services';
$app_list_strings['department_list']['Scientific'] = 'Scientific';
$app_list_strings['department_list']['Software Development'] = 'Software Development';
$app_list_strings['department_list']['Toxicology'] = 'Toxicology Services';
$app_list_strings['department_list']['Veterinary Services'] = 'Veterinary Services';
$app_list_strings['department_list'][''] = '';
$app_list_strings['ed_type_list']['Animal Enrollment'] = 'Animal Enrollment';
$app_list_strings['ed_type_list']['Animal Health Status to SD'] = 'Animal Health Status to SD';
$app_list_strings['ed_type_list']['Early Term Early Death'] = 'Early Term/Early Death';
$app_list_strings['ed_type_list']['Test Article Failures'] = 'Test Article Failures';
$app_list_strings['ed_type_list']['Unblinding of Study Personnel'] = 'Unblinding of Study Personnel';
$app_list_strings['ed_type_list'][''] = '';
