<?php
// created: 2022-12-13 04:45:57
$config = array (
  'team_security' => true,
  'assignable' => true,
  'taggable' => 1,
  'acl' => true,
  'has_tab' => true,
  'studio' => true,
  'audit' => true,
  'activity_enabled' => 0,
  'templates' => 
  array (
    'basic' => 1,
  ),
  'label' => 'Work Product Groups',
  'label_singular' => 'Work Product Group',
  'importable' => true,
);