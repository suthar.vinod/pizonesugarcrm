<?php
// created: 2022-12-13 04:46:32
$mod_strings = array (
  'LBL_TEAM' => 'チーム',
  'LBL_TEAMS' => 'チーム',
  'LBL_TEAM_ID' => 'チームID',
  'LBL_ASSIGNED_TO_ID' => 'アサイン先ID',
  'LBL_ASSIGNED_TO_NAME' => 'アサイン先',
  'LBL_TAGS_LINK' => 'タグ',
  'LBL_TAGS' => 'タグ',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '作成日',
  'LBL_DATE_MODIFIED' => '更新日',
  'LBL_MODIFIED' => '更新者',
  'LBL_MODIFIED_ID' => '更新者ID',
  'LBL_MODIFIED_NAME' => '更新者',
  'LBL_CREATED' => '作成者',
  'LBL_CREATED_ID' => '作成者ID',
  'LBL_DOC_OWNER' => 'ドキュメントの所有者',
  'LBL_USER_FAVORITES' => 'お気に入りのユーザー',
  'LBL_DESCRIPTION' => '詳細',
  'LBL_DELETED' => '削除済み',
  'LBL_NAME' => '名前',
  'LBL_CREATED_USER' => '作成者',
  'LBL_MODIFIED_USER' => '更新者',
  'LBL_LIST_NAME' => '名前',
  'LBL_EDIT_BUTTON' => '編集',
  'LBL_REMOVE' => '削除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '更新者',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Work Product Groups 一覧',
  'LBL_MODULE_NAME' => 'Work Product Groups',
  'LBL_MODULE_TITLE' => 'Work Product Groups',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Group',
  'LBL_HOMEPAGE_TITLE' => '私の Work Product Groups',
  'LNK_NEW_RECORD' => '作成 Work Product Group',
  'LNK_LIST' => '画面 Work Product Groups',
  'LNK_IMPORT_WPG_WORK_PRODUCT_GROUP' => 'Import Work Product Groups',
  'LBL_SEARCH_FORM_TITLE' => '検索 Work Product Group',
  'LBL_HISTORY_SUBPANEL_TITLE' => '履歴',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'アクティビティストリーム',
  'LBL_WPG_WORK_PRODUCT_GROUP_SUBPANEL_TITLE' => 'Work Product Groups',
  'LBL_NEW_FORM_TITLE' => '新規 Work Product Group',
  'LNK_IMPORT_VCARD' => 'Import Work Product Group vCard',
  'LBL_IMPORT' => 'Import Work Product Groups',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Group record by importing a vCard from your file system.',
  'LBL_WPG_WORK_PRODUCT_GROUP_FOCUS_DRAWER_DASHBOARD' => 'Work Product Groups Focus Drawer',
  'LBL_TOTAL_CAPACITY' => 'Total Capacity (by week)',
);