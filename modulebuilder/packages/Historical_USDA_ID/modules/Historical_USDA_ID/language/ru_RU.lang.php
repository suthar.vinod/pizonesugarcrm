<?php
// created: 2022-12-13 04:46:14
$mod_strings = array (
  'LBL_TEAM' => 'Команды',
  'LBL_TEAMS' => 'Команды',
  'LBL_TEAM_ID' => 'Id Команды',
  'LBL_ASSIGNED_TO_ID' => 'Ответственный (-ая)',
  'LBL_ASSIGNED_TO_NAME' => 'Ответственный (-ая)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено',
  'LBL_MODIFIED_ID' => 'Изменено (Id)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано (Id)',
  'LBL_DOC_OWNER' => 'Владелец документа',
  'LBL_USER_FAVORITES' => 'Пользователи, которые добавили в Избранное',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано',
  'LBL_MODIFIED_USER' => 'Изменено',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Правка',
  'LBL_REMOVE' => 'Удалить',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Изменено (по названию)',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Historical USDA IDs Список',
  'LBL_MODULE_NAME' => 'Historical USDA IDs',
  'LBL_MODULE_TITLE' => 'Historical USDA IDs',
  'LBL_MODULE_NAME_SINGULAR' => 'Historical USDA ID',
  'LBL_HOMEPAGE_TITLE' => 'Моя Historical USDA IDs',
  'LNK_NEW_RECORD' => 'Создать Historical USDA ID',
  'LNK_LIST' => 'Просмотр Historical USDA IDs',
  'LNK_IMPORT_USDA_HISTORICAL_USDA_ID' => 'Import Historical USDA IDs',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск Historical USDA ID',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_USDA_HISTORICAL_USDA_ID_SUBPANEL_TITLE' => 'Historical USDA IDs',
  'LBL_NEW_FORM_TITLE' => 'Новый Historical USDA ID',
  'LNK_IMPORT_VCARD' => 'Import Historical USDA ID vCard',
  'LBL_IMPORT' => 'Import Historical USDA IDs',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Historical USDA ID record by importing a vCard from your file system.',
  'LBL_USDA_HISTORICAL_USDA_ID_FOCUS_DRAWER_DASHBOARD' => 'Historical USDA IDs Focus Drawer',
  'LBL_DATE_OF_REIDENTIFICATION' => 'Date of Reidentification',
);