<?php
// created: 2022-12-13 04:46:00
$mod_strings = array (
  'LBL_TEAM' => 'Csoportok',
  'LBL_TEAMS' => 'Csoportok',
  'LBL_TEAM_ID' => 'Csoport azonosító',
  'LBL_ASSIGNED_TO_ID' => 'Hozzárendelt felhasználó azonosító',
  'LBL_ASSIGNED_TO_NAME' => 'Felelős felhasználó',
  'LBL_TAGS_LINK' => 'Címkék',
  'LBL_TAGS' => 'Címkék',
  'LBL_ID' => 'Azonosító',
  'LBL_DATE_ENTERED' => 'Létrehozás dátuma',
  'LBL_DATE_MODIFIED' => 'Módosítás dátuma',
  'LBL_MODIFIED' => 'Módosította',
  'LBL_MODIFIED_ID' => 'Módosította (azonosító szerint)',
  'LBL_MODIFIED_NAME' => 'Módosította (név szerint)',
  'LBL_CREATED' => 'Létrehozta',
  'LBL_CREATED_ID' => 'Létrehozó azonosítója',
  'LBL_DOC_OWNER' => 'Dokumentum tulajdonosa',
  'LBL_USER_FAVORITES' => 'Felhasználók, akik kedvelték',
  'LBL_DESCRIPTION' => 'Leírás',
  'LBL_DELETED' => 'Törölve',
  'LBL_NAME' => 'Név',
  'LBL_CREATED_USER' => 'Felhasználó által létrehozva',
  'LBL_MODIFIED_USER' => 'Felhasználó által módosítva',
  'LBL_LIST_NAME' => 'Név',
  'LBL_EDIT_BUTTON' => 'Szerkesztés',
  'LBL_REMOVE' => 'Eltávolítás',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Módosítva név szerint',
  'LBL_LIST_FORM_TITLE' => 'Acquired Companies Lista',
  'LBL_MODULE_NAME' => 'Acquired Companies',
  'LBL_MODULE_TITLE' => 'Acquired Companies',
  'LBL_MODULE_NAME_SINGULAR' => 'Acquired Company',
  'LBL_HOMEPAGE_TITLE' => 'Saját Acquired Companies',
  'LNK_NEW_RECORD' => 'Új létrehozása Acquired Company',
  'LNK_LIST' => 'Megtekintés Acquired Companies',
  'LNK_IMPORT_AC01_ACQUIRED_COMPANIES' => 'Import Acquired Companies',
  'LBL_SEARCH_FORM_TITLE' => 'Keresés Acquired Company',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Előzmények megtekintése',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tevékenységek',
  'LBL_AC01_ACQUIRED_COMPANIES_SUBPANEL_TITLE' => 'Acquired Companies',
  'LBL_NEW_FORM_TITLE' => 'Új Acquired Company',
  'LNK_IMPORT_VCARD' => 'Import Acquired Company vCard',
  'LBL_IMPORT' => 'Import Acquired Companies',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Acquired Company record by importing a vCard from your file system.',
);