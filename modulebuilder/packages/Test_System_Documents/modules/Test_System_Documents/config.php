<?php
// created: 2022-12-13 04:45:57
$config = array (
  'team_security' => true,
  'assignable' => true,
  'taggable' => 1,
  'acl' => true,
  'has_tab' => true,
  'studio' => true,
  'audit' => true,
  'activity_enabled' => 0,
  'templates' => 
  array (
    'basic' => 1,
    'file' => 1,
  ),
  'label' => 'Test System Documents',
  'label_singular' => 'Test System Document',
  'importable' => true,
);