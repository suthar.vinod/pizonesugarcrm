<?php
// created: 2022-12-13 04:46:17
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruger-id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'Id',
  'LBL_DATE_ENTERED' => 'Oprettet den',
  'LBL_DATE_MODIFIED' => 'Ændret den',
  'LBL_MODIFIED' => 'Ændret af',
  'LBL_MODIFIED_ID' => 'Ændret af id',
  'LBL_MODIFIED_NAME' => 'Ændret af navn',
  'LBL_CREATED' => 'Oprettet af',
  'LBL_CREATED_ID' => 'Oprettet af id',
  'LBL_DOC_OWNER' => 'Dokument ejer',
  'LBL_USER_FAVORITES' => 'Brugernes favorit',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Oprettet af bruger',
  'LBL_MODIFIED_USER' => 'Ændret af bruger',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ændret af navn:',
  'LBL_LIST_FORM_TITLE' => 'CPI Findings Liste',
  'LBL_MODULE_NAME' => 'CPI Findings',
  'LBL_MODULE_TITLE' => 'CPI Findings',
  'LBL_MODULE_NAME_SINGULAR' => 'CPI Findings',
  'LBL_HOMEPAGE_TITLE' => 'Min CPI Findings',
  'LNK_NEW_RECORD' => 'Opret CPI Findings',
  'LNK_LIST' => 'Vis CPI Findings',
  'LNK_IMPORT_A1A_CPI_FINDINGS' => 'Import CPI Findings',
  'LBL_SEARCH_FORM_TITLE' => 'Søg CPI Findings',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vis historik',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteter',
  'LBL_A1A_CPI_FINDINGS_SUBPANEL_TITLE' => 'CPI Findings',
  'LBL_NEW_FORM_TITLE' => 'Ny CPI Findings',
  'LNK_IMPORT_VCARD' => 'Import CPI Findings vCard',
  'LBL_IMPORT' => 'Import CPI Findings',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new CPI Findings record by importing a vCard from your file system.',
  'LBL_CPI_FINDING_CATEGORY' => 'CPI Finding Category',
  'LBL_CPI_FINDING_DESCRIPTION' => 'CPI Finding Description',
  'LBL_CPI_RECOMMENDED_ACTION' => 'CPI Recommended Action',
);