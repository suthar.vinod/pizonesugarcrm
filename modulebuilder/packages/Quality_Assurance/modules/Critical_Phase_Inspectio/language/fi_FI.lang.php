<?php
// created: 2022-12-13 04:46:18
$mod_strings = array (
  'LBL_TEAM' => 'Tiimit',
  'LBL_TEAMS' => 'Tiimit',
  'LBL_TEAM_ID' => 'Tiimin tunnus',
  'LBL_ASSIGNED_TO_ID' => 'Vastuukäyttäjän ID',
  'LBL_ASSIGNED_TO_NAME' => 'Vastuuhenkilö',
  'LBL_TAGS_LINK' => 'Tagit',
  'LBL_TAGS' => 'Tagit',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Luontipäivä',
  'LBL_DATE_MODIFIED' => 'Muokkattu viimeksi',
  'LBL_MODIFIED' => 'Muokannut',
  'LBL_MODIFIED_ID' => 'Muokkaajan ID',
  'LBL_MODIFIED_NAME' => 'Muokkaajan nimi',
  'LBL_CREATED' => 'Luonut',
  'LBL_CREATED_ID' => 'Luojan ID',
  'LBL_DOC_OWNER' => 'Tiedoston omistaja',
  'LBL_USER_FAVORITES' => 'Käyttäjät, jotka ottivat suosikikseen',
  'LBL_DESCRIPTION' => 'Kuvaus',
  'LBL_DELETED' => 'Poistettu',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Luoja',
  'LBL_MODIFIED_USER' => 'Muokkaaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Muokkaa',
  'LBL_REMOVE' => 'Poista',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muokkaajan nimi',
  'LBL_LIST_FORM_TITLE' => 'Critical Phase Inspections Lista',
  'LBL_MODULE_NAME' => 'Critical Phase Inspections',
  'LBL_MODULE_TITLE' => 'Critical Phase Inspections',
  'LBL_MODULE_NAME_SINGULAR' => 'Critical Phase Inspections',
  'LBL_HOMEPAGE_TITLE' => 'Oma Critical Phase Inspections',
  'LNK_NEW_RECORD' => 'Luo Critical Phase Inspections',
  'LNK_LIST' => 'Näkymä Critical Phase Inspections',
  'LNK_IMPORT_A1A_CRITICAL_PHASE_INSPECTIO' => 'Import Critical Phase Inspections',
  'LBL_SEARCH_FORM_TITLE' => 'Etsi Critical Phase Inspections',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Näytä historia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteetit',
  'LBL_A1A_CRITICAL_PHASE_INSPECTIO_SUBPANEL_TITLE' => 'Critical Phase Inspections',
  'LBL_NEW_FORM_TITLE' => 'Uusi Critical Phase Inspections',
  'LNK_IMPORT_VCARD' => 'Import Critical Phase Inspections vCard',
  'LBL_IMPORT' => 'Import Critical Phase Inspections',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Critical Phase Inspections record by importing a vCard from your file system.',
  'LBL_ACTUAL_TIME_QUARTER' => 'Actual Time (By Quarter Hour)',
  'LBL_DATE_OF_INSPECTION' => 'Date of Inspection',
  'LBL_PHASE_OF_INSPECTION' => 'Phase of Inspection',
);