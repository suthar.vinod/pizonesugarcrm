<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$vardefs = array (
  'fields' => 
  array (
    'actual_time_quarter' => 
    array (
      'required' => false,
      'name' => 'actual_time_quarter',
      'vname' => 'LBL_ACTUAL_TIME_QUARTER',
      'type' => 'decimal',
      'massupdate' => false,
      'default' => '',
      'no_default' => false,
      'comments' => '',
      'help' => '',
      'importable' => 'true',
      'duplicate_merge' => 'enabled',
      'duplicate_merge_dom_value' => '1',
      'audited' => false,
      'reportable' => true,
      'unified_search' => false,
      'merge_filter' => 'disabled',
      'calculated' => false,
      'len' => '18',
      'size' => '20',
      'enable_range_search' => false,
      'precision' => 2,
    ),
    'date_of_inspection' => 
    array (
      'required' => false,
      'name' => 'date_of_inspection',
      'vname' => 'LBL_DATE_OF_INSPECTION',
      'type' => 'date',
      'massupdate' => true,
      'no_default' => false,
      'comments' => '',
      'help' => '',
      'importable' => 'true',
      'duplicate_merge' => 'enabled',
      'duplicate_merge_dom_value' => '1',
      'audited' => false,
      'reportable' => true,
      'unified_search' => false,
      'merge_filter' => 'disabled',
      'calculated' => false,
      'size' => '20',
      'enable_range_search' => false,
    ),
    'phase_of_inspection' => 
    array (
      'required' => false,
      'name' => 'phase_of_inspection',
      'vname' => 'LBL_PHASE_OF_INSPECTION',
      'type' => 'enum',
      'massupdate' => true,
      'default' => 'Choose One',
      'no_default' => false,
      'comments' => '',
      'help' => '',
      'importable' => 'true',
      'duplicate_merge' => 'enabled',
      'duplicate_merge_dom_value' => '1',
      'audited' => false,
      'reportable' => true,
      'unified_search' => false,
      'merge_filter' => 'disabled',
      'calculated' => false,
      'len' => 100,
      'size' => '20',
      'options' => 'phase_of_inspection_list',
      'dependency' => false,
    ),
  ),
  'relationships' => 
  array (
  ),
);