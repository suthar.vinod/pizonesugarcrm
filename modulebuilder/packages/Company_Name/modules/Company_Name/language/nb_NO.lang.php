<?php
// created: 2022-12-13 04:46:06
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Company Names Liste',
  'LBL_MODULE_NAME' => 'Company Names',
  'LBL_MODULE_TITLE' => 'Company Names',
  'LBL_MODULE_NAME_SINGULAR' => 'Company Name',
  'LBL_HOMEPAGE_TITLE' => 'Min Company Names',
  'LNK_NEW_RECORD' => 'Opprett Company Name',
  'LNK_LIST' => 'Vis Company Names',
  'LNK_IMPORT_CN_COMPANY_NAME' => 'Import Company Names',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Company Name',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_CN_COMPANY_NAME_SUBPANEL_TITLE' => 'Company Names',
  'LBL_NEW_FORM_TITLE' => 'Ny Company Name',
  'LNK_IMPORT_VCARD' => 'Import Company Name vCard',
  'LBL_IMPORT' => 'Import Company Names',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Company Name record by importing a vCard from your file system.',
);