<?php
// created: 2022-12-13 04:46:19
$mod_strings = array (
  'LBL_TEAM' => 'Darba grupas',
  'LBL_TEAMS' => 'Darba grupas',
  'LBL_TEAM_ID' => 'Darba grupas ID',
  'LBL_ASSIGNED_TO_ID' => 'Piešķirts lietotājam ar ID',
  'LBL_ASSIGNED_TO_NAME' => 'Piešķirts lietotājam',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Izveidots',
  'LBL_DATE_MODIFIED' => 'Modificēts',
  'LBL_MODIFIED' => 'Modificēja',
  'LBL_MODIFIED_ID' => 'Modificētāja ID',
  'LBL_MODIFIED_NAME' => 'Modificētāja vārds',
  'LBL_CREATED' => 'Izveidoja',
  'LBL_CREATED_ID' => 'Izveidotāja ID',
  'LBL_DOC_OWNER' => 'Dokumenta īpašnieks',
  'LBL_USER_FAVORITES' => 'Lietotāji atzīmēja',
  'LBL_DESCRIPTION' => 'Apraksts',
  'LBL_DELETED' => 'Dzēsts',
  'LBL_NAME' => 'Nosaukums',
  'LBL_CREATED_USER' => 'Izveidoja lietotājs',
  'LBL_MODIFIED_USER' => 'Modificēja lietotājs',
  'LBL_LIST_NAME' => 'Nosaukums',
  'LBL_EDIT_BUTTON' => 'Rediģēt',
  'LBL_REMOVE' => 'Noņemt',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificētāja vārds',
  'LBL_LIST_FORM_TITLE' => 'Equipment Saraksts',
  'LBL_MODULE_NAME' => 'Equipment',
  'LBL_MODULE_TITLE' => 'Equipment',
  'LBL_MODULE_NAME_SINGULAR' => 'Equipment',
  'LBL_HOMEPAGE_TITLE' => 'Mans Equipment',
  'LNK_NEW_RECORD' => 'Izveidot Equipment',
  'LNK_LIST' => 'Skats Equipment',
  'LNK_IMPORT_M04_EQUIPMENT' => 'Import Equipment',
  'LBL_SEARCH_FORM_TITLE' => 'Meklēt Equipment',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Aplūkot vēsturi',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Darbības',
  'LBL_M04_EQUIPMENT_SUBPANEL_TITLE' => 'Equipment',
  'LBL_NEW_FORM_TITLE' => 'Jauns Equipment',
  'LNK_IMPORT_VCARD' => 'Import Equipment vCard',
  'LBL_IMPORT' => 'Import Equipment',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Equipment record by importing a vCard from your file system.',
);