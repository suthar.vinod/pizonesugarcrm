<?php
// created: 2022-12-13 04:46:19
$mod_strings = array (
  'LBL_TEAM' => 'Gruppi',
  'LBL_TEAMS' => 'Gruppi',
  'LBL_TEAM_ID' => 'Id Gruppo',
  'LBL_ASSIGNED_TO_ID' => 'Id Utente Assegnato',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Inserimento',
  'LBL_DATE_MODIFIED' => 'Data Modifica',
  'LBL_MODIFIED' => 'Modificato da',
  'LBL_MODIFIED_ID' => 'Modificato Da Id',
  'LBL_MODIFIED_NAME' => 'Modificato Da Nome',
  'LBL_CREATED' => 'Creato Da',
  'LBL_CREATED_ID' => 'Creato da Id',
  'LBL_DOC_OWNER' => 'Possessore del Documento',
  'LBL_USER_FAVORITES' => 'Preferenze Utenti',
  'LBL_DESCRIPTION' => 'Descrizione',
  'LBL_DELETED' => 'Eliminato',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Creato da Utente',
  'LBL_MODIFIED_USER' => 'Modificato da Utente',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Modifica',
  'LBL_REMOVE' => 'Rimuovi',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificato Da Nome',
  'LBL_LIST_FORM_TITLE' => 'Equipment Lista',
  'LBL_MODULE_NAME' => 'Equipment',
  'LBL_MODULE_TITLE' => 'Equipment',
  'LBL_MODULE_NAME_SINGULAR' => 'Equipment',
  'LBL_HOMEPAGE_TITLE' => 'Mio Equipment',
  'LNK_NEW_RECORD' => 'Crea Equipment',
  'LNK_LIST' => 'Visualizza Equipment',
  'LNK_IMPORT_M04_EQUIPMENT' => 'Import Equipment',
  'LBL_SEARCH_FORM_TITLE' => 'Ricerca Equipment',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Cronologia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Attività',
  'LBL_M04_EQUIPMENT_SUBPANEL_TITLE' => 'Equipment',
  'LBL_NEW_FORM_TITLE' => 'Nuovo Equipment',
  'LNK_IMPORT_VCARD' => 'Import Equipment vCard',
  'LBL_IMPORT' => 'Import Equipment',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Equipment record by importing a vCard from your file system.',
);