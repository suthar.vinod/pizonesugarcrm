<?php
// created: 2022-12-13 04:46:19
$mod_strings = array (
  'LBL_TEAM' => 'Команды',
  'LBL_TEAMS' => 'Команды',
  'LBL_TEAM_ID' => 'ID команды',
  'LBL_ASSIGNED_TO_ID' => 'Ответственный (-ая)',
  'LBL_ASSIGNED_TO_NAME' => 'За кем ответственный (-ая)',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено пользователем:',
  'LBL_MODIFIED_ID' => 'Изменено (Id)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано (Id)',
  'LBL_DOC_OWNER' => 'Владелец Документа',
  'LBL_USER_FAVORITES' => 'Пользователи, которые добавили в Избранное',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано пользователем',
  'LBL_MODIFIED_USER' => 'Изменено пользователем',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Правка',
  'LBL_REMOVE' => 'Удалить',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Изменено',
  'LBL_LIST_FORM_TITLE' => 'Equipment Список',
  'LBL_MODULE_NAME' => 'Equipment',
  'LBL_MODULE_TITLE' => 'Equipment',
  'LBL_MODULE_NAME_SINGULAR' => 'Equipment',
  'LBL_HOMEPAGE_TITLE' => 'Моя Equipment',
  'LNK_NEW_RECORD' => 'Создать Equipment',
  'LNK_LIST' => 'Просмотр Equipment',
  'LNK_IMPORT_M04_EQUIPMENT' => 'Import Equipment',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск Equipment',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_M04_EQUIPMENT_SUBPANEL_TITLE' => 'Equipment',
  'LBL_NEW_FORM_TITLE' => 'Новый Equipment',
  'LNK_IMPORT_VCARD' => 'Import Equipment vCard',
  'LBL_IMPORT' => 'Import Equipment',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Equipment record by importing a vCard from your file system.',
);