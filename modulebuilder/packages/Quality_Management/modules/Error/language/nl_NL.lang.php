<?php
// created: 2022-12-13 04:46:20
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen aan ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum aangemaakt',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door Naam',
  'LBL_CREATED' => 'Aangemaakt Door',
  'LBL_CREATED_ID' => 'Gemaakt Door ID',
  'LBL_DOC_OWNER' => 'Eigenaar van document',
  'LBL_USER_FAVORITES' => 'Gebruikers die favoriet gemaakt hebben',
  'LBL_DESCRIPTION' => 'Beschrijving',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_NAME' => 'Naam',
  'LBL_CREATED_USER' => 'Gemaakt door Gebruiker',
  'LBL_MODIFIED_USER' => 'Gewijzigd door Gebruiker',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_EDIT_BUTTON' => 'Wijzigen',
  'LBL_REMOVE' => 'Verwijder',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Gewijzigd door Naam',
  'LBL_LIST_FORM_TITLE' => 'Errors List',
  'LBL_MODULE_NAME' => 'Errors',
  'LBL_MODULE_TITLE' => 'Errors',
  'LBL_MODULE_NAME_SINGULAR' => 'Error',
  'LBL_HOMEPAGE_TITLE' => 'Mijn Errors',
  'LNK_NEW_RECORD' => 'Create Error',
  'LNK_LIST' => 'View Errors',
  'LNK_IMPORT_M04_ERROR' => 'Import Error',
  'LBL_SEARCH_FORM_TITLE' => 'Zoeken Error',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_M04_ERROR_SUBPANEL_TITLE' => 'Errors',
  'LBL_NEW_FORM_TITLE' => 'Nieuw Error',
  'LNK_IMPORT_VCARD' => 'Import Error vCard',
  'LBL_IMPORT' => 'Import Errors',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Error record by importing a vCard from your file system.',
);