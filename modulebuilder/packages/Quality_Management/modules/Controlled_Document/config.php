<?php
// created: 2022-12-13 04:45:56
$config = array (
  'team_security' => true,
  'assignable' => true,
  'acl' => true,
  'has_tab' => true,
  'studio' => true,
  'audit' => true,
  'activity_enabled' => 0,
  'templates' => 
  array (
    'basic' => 1,
  ),
  'label' => 'Controlled_Documents',
  'label_singular' => 'Controlled_Document',
  'importable' => true,
  'taggable' => 1,
);