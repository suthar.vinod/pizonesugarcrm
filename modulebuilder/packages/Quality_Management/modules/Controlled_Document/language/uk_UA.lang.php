<?php
// created: 2022-12-13 04:46:19
$mod_strings = array (
  'LBL_TEAM' => 'Команди',
  'LBL_TEAMS' => 'Команди',
  'LBL_TEAM_ID' => 'Команда (Id)',
  'LBL_ASSIGNED_TO_ID' => 'Відповідальний (-а) (Id)',
  'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (-а)',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_DATE_MODIFIED' => 'Дата змінення',
  'LBL_MODIFIED' => 'Змінено:',
  'LBL_MODIFIED_ID' => 'Змінено (Id)',
  'LBL_MODIFIED_NAME' => 'Змінено користувачем',
  'LBL_CREATED' => 'Створено',
  'LBL_CREATED_ID' => 'Створено (Id)',
  'LBL_DOC_OWNER' => 'Власник документа',
  'LBL_USER_FAVORITES' => 'Користувачі, які додали в Обране',
  'LBL_DESCRIPTION' => 'Опис',
  'LBL_DELETED' => 'Видалено',
  'LBL_NAME' => 'Назва',
  'LBL_CREATED_USER' => 'Створенено користувачем',
  'LBL_MODIFIED_USER' => 'Змінено користувачем',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_EDIT_BUTTON' => 'Редагувати',
  'LBL_REMOVE' => 'Видалити',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Змінено користувачем',
  'LBL_LIST_FORM_TITLE' => 'Controlled_Documents Список',
  'LBL_MODULE_NAME' => 'Controlled_Documents',
  'LBL_MODULE_TITLE' => 'Controlled_Documents',
  'LBL_MODULE_NAME_SINGULAR' => 'Controlled_Document',
  'LBL_HOMEPAGE_TITLE' => 'Мій Controlled_Documents',
  'LNK_NEW_RECORD' => 'Створити Controlled_Document',
  'LNK_LIST' => 'Переглянути Controlled_Documents',
  'LNK_IMPORT_M04_CONTROLLED_DOCUMENT' => 'Import Controlled_Document',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук Controlled_Document',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Переглянути історію',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Стрічка активностей',
  'LBL_M04_CONTROLLED_DOCUMENT_SUBPANEL_TITLE' => 'Controlled_Documents',
  'LBL_NEW_FORM_TITLE' => 'Новий Controlled_Document',
  'LNK_IMPORT_VCARD' => 'Import Controlled_Document vCard',
  'LBL_IMPORT' => 'Import Controlled_Documents',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Controlled_Document record by importing a vCard from your file system.',
);