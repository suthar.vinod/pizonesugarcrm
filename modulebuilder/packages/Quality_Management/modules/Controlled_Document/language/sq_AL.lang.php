<?php
// created: 2022-12-13 04:46:19
$mod_strings = array (
  'LBL_TEAM' => 'Grupet',
  'LBL_TEAMS' => 'Grupet',
  'LBL_TEAM_ID' => 'ID e grupit',
  'LBL_ASSIGNED_TO_ID' => 'Id e përdoruesve të caktuar',
  'LBL_ASSIGNED_TO_NAME' => 'Drejtuar për',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'krijim i të dhënës',
  'LBL_DATE_MODIFIED' => 'Të dhënat e modifikuara',
  'LBL_MODIFIED' => 'Modifikuar nga',
  'LBL_MODIFIED_ID' => 'Modifikuar nga Id',
  'LBL_MODIFIED_NAME' => 'Modifikuar nga emri',
  'LBL_CREATED' => 'Krijuar nga',
  'LBL_CREATED_ID' => 'Krijuar nga Id',
  'LBL_DOC_OWNER' => 'Pronari i Dokumentit',
  'LBL_USER_FAVORITES' => 'Përdoruesit të cilët preferojnë',
  'LBL_DESCRIPTION' => 'Përshkrim',
  'LBL_DELETED' => 'E fshirë',
  'LBL_NAME' => 'Emri',
  'LBL_CREATED_USER' => 'Krijuar nga përdoruesi',
  'LBL_MODIFIED_USER' => 'Modifikuar nga përdoruesi',
  'LBL_LIST_NAME' => 'Emri',
  'LBL_EDIT_BUTTON' => 'Ndrysho',
  'LBL_REMOVE' => 'Heq',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikuar nga emri',
  'LBL_LIST_FORM_TITLE' => 'Controlled_Documents lista',
  'LBL_MODULE_NAME' => 'Controlled_Documents',
  'LBL_MODULE_TITLE' => 'Controlled_Documents',
  'LBL_MODULE_NAME_SINGULAR' => 'Controlled_Document',
  'LBL_HOMEPAGE_TITLE' => 'Mia Controlled_Documents',
  'LNK_NEW_RECORD' => 'Krijo Controlled_Document',
  'LNK_LIST' => 'Pamje Controlled_Documents',
  'LNK_IMPORT_M04_CONTROLLED_DOCUMENT' => 'Import Controlled_Document',
  'LBL_SEARCH_FORM_TITLE' => 'Kërkim Controlled_Document',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'shiko historinë',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetet',
  'LBL_M04_CONTROLLED_DOCUMENT_SUBPANEL_TITLE' => 'Controlled_Documents',
  'LBL_NEW_FORM_TITLE' => 'E re Controlled_Document',
  'LNK_IMPORT_VCARD' => 'Import Controlled_Document vCard',
  'LBL_IMPORT' => 'Import Controlled_Documents',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Controlled_Document record by importing a vCard from your file system.',
);