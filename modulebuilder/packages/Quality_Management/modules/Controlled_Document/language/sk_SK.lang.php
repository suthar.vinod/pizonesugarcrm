<?php
// created: 2022-12-13 04:46:19
$mod_strings = array (
  'LBL_TEAM' => 'Tímy',
  'LBL_TEAMS' => 'Tímy',
  'LBL_TEAM_ID' => 'ID tímu',
  'LBL_ASSIGNED_TO_ID' => 'Pridelené užívateľské ID',
  'LBL_ASSIGNED_TO_NAME' => 'Pridelený k',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Dátum vytvorenia',
  'LBL_DATE_MODIFIED' => 'Dátum úpravy',
  'LBL_MODIFIED' => 'Zmenil',
  'LBL_MODIFIED_ID' => 'Zmenil ID',
  'LBL_MODIFIED_NAME' => 'Zmenil Meno',
  'LBL_CREATED' => 'Vytvoril',
  'LBL_CREATED_ID' => 'Vytvoril ID',
  'LBL_DOC_OWNER' => 'Document Onwer',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Vymazaný',
  'LBL_NAME' => 'Meno',
  'LBL_CREATED_USER' => 'Vytvorené užívateľom',
  'LBL_MODIFIED_USER' => 'Zmenil užívateľ',
  'LBL_LIST_NAME' => 'Meno',
  'LBL_EDIT_BUTTON' => 'Upraviť',
  'LBL_REMOVE' => 'Odstrániť',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmenil Meno',
  'LBL_LIST_FORM_TITLE' => 'Controlled_Documents Zoznam',
  'LBL_MODULE_NAME' => 'Controlled_Documents',
  'LBL_MODULE_TITLE' => 'Controlled_Documents',
  'LBL_MODULE_NAME_SINGULAR' => 'Controlled_Document',
  'LBL_HOMEPAGE_TITLE' => 'Moje Controlled_Documents',
  'LNK_NEW_RECORD' => 'Vytvoriť Controlled_Document',
  'LNK_LIST' => 'zobrazenie Controlled_Documents',
  'LNK_IMPORT_M04_CONTROLLED_DOCUMENT' => 'Import Controlled_Document',
  'LBL_SEARCH_FORM_TITLE' => 'Vyhľadávanie Controlled_Document',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobraziť Históriu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_M04_CONTROLLED_DOCUMENT_SUBPANEL_TITLE' => 'Controlled_Documents',
  'LBL_NEW_FORM_TITLE' => 'Nový Controlled_Document',
  'LNK_IMPORT_VCARD' => 'Import Controlled_Document vCard',
  'LBL_IMPORT' => 'Import Controlled_Documents',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Controlled_Document record by importing a vCard from your file system.',
);