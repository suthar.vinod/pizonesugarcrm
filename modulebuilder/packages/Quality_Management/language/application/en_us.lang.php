<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['M04_Error'] = 'Errors';
$app_list_strings['moduleList']['M04_Equipment'] = 'Equipment';
$app_list_strings['moduleList']['M04_Controlled_Document'] = 'Controlled_Documents';
$app_list_strings['moduleListSingular']['M04_Error'] = 'Error';
$app_list_strings['moduleListSingular']['M04_Equipment'] = 'Equipment';
$app_list_strings['moduleListSingular']['M04_Controlled_Document'] = 'Controlled_Document';
