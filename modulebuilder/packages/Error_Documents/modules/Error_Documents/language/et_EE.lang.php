<?php
// created: 2022-12-13 04:46:10
$mod_strings = array (
  'LBL_TEAM' => 'Meeskonnad',
  'LBL_TEAMS' => 'Meeskonnad',
  'LBL_TEAM_ID' => 'Meeskonna ID',
  'LBL_ASSIGNED_TO_ID' => 'Määratud kasutaja ID',
  'LBL_ASSIGNED_TO_NAME' => 'Määratud kasutajale',
  'LBL_TAGS_LINK' => 'Sildid',
  'LBL_TAGS' => 'Sildid',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Loomiskuupäev',
  'LBL_DATE_MODIFIED' => 'Muutmiskuupäev',
  'LBL_MODIFIED' => 'Muutja',
  'LBL_MODIFIED_ID' => 'Muutja ID',
  'LBL_MODIFIED_NAME' => 'Muutja nimi',
  'LBL_CREATED' => 'Loodud',
  'LBL_CREATED_ID' => 'Looja ID',
  'LBL_DOC_OWNER' => 'Dokumendi omanik',
  'LBL_USER_FAVORITES' => 'Lemmikkasutajad',
  'LBL_DESCRIPTION' => 'Kirjeldus',
  'LBL_DELETED' => 'Kustutatud',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Loonud kasutaja',
  'LBL_MODIFIED_USER' => 'Muutnud kasutaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Redigeeri',
  'LBL_REMOVE' => 'Eemalda',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muutja nimi',
  'LBL_LIST_FORM_TITLE' => 'Errors Documents Loend',
  'LBL_MODULE_NAME' => 'Errors Documents',
  'LBL_MODULE_TITLE' => 'Errors Documents',
  'LBL_MODULE_NAME_SINGULAR' => 'Error Documents',
  'LBL_HOMEPAGE_TITLE' => 'Minu Errors Documents',
  'LNK_NEW_RECORD' => 'Loo Error Documents',
  'LNK_LIST' => 'Vaade Errors Documents',
  'LNK_IMPORT_ERD_ERROR_DOCUMENTS' => 'Import Errors Documents',
  'LBL_SEARCH_FORM_TITLE' => 'Otsi Error Documents',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vaata ajalugu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tegevuste voog',
  'LBL_ERD_ERROR_DOCUMENTS_SUBPANEL_TITLE' => 'Errors Documents',
  'LBL_NEW_FORM_TITLE' => 'Uus Error Documents',
  'LNK_IMPORT_VCARD' => 'Import Error Documents vCard',
  'LBL_IMPORT' => 'Import Errors Documents',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Error Documents record by importing a vCard from your file system.',
);