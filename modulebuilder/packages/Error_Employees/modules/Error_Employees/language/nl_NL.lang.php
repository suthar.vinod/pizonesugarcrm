<?php
// created: 2022-12-13 04:46:11
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen aan ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum ingevoerd',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door Naam',
  'LBL_CREATED' => 'Gemaakt door',
  'LBL_CREATED_ID' => 'Gemaakt door ID',
  'LBL_DOC_OWNER' => 'Documenteigenaar',
  'LBL_USER_FAVORITES' => 'Gebruikers die favoriet gemaakt hebben',
  'LBL_DESCRIPTION' => 'Beschrijving',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_NAME' => 'Naam',
  'LBL_CREATED_USER' => 'Gemaakt door Gebruiker',
  'LBL_MODIFIED_USER' => 'Gewijzigd door Gebruiker',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_EDIT_BUTTON' => 'Wijzig',
  'LBL_REMOVE' => 'Verwijder',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Gewijzigd door Naam',
  'LBL_LIST_FORM_TITLE' => 'Errors Employees List',
  'LBL_MODULE_NAME' => 'Errors Employees',
  'LBL_MODULE_TITLE' => 'Errors Employees',
  'LBL_MODULE_NAME_SINGULAR' => 'Error Employee',
  'LBL_HOMEPAGE_TITLE' => 'Mijn Errors Employees',
  'LNK_NEW_RECORD' => 'Create Error Employee',
  'LNK_LIST' => 'View Errors Employees',
  'LNK_IMPORT_ERE_ERROR_EMPLOYEES' => 'Import Errors Employees',
  'LBL_SEARCH_FORM_TITLE' => 'Zoeken Error Employee',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_ERE_ERROR_EMPLOYEES_SUBPANEL_TITLE' => 'Errors Employees',
  'LBL_NEW_FORM_TITLE' => 'Nieuw Error Employee',
  'LNK_IMPORT_VCARD' => 'Import Error Employee vCard',
  'LBL_IMPORT' => 'Import Errors Employees',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Error Employee record by importing a vCard from your file system.',
);