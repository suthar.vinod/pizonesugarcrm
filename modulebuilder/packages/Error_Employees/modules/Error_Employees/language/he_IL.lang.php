<?php
// created: 2022-12-13 04:46:11
$mod_strings = array (
  'LBL_TEAM' => 'קבוצות',
  'LBL_TEAMS' => 'קבוצות',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'מזהה משתמש מוקצה',
  'LBL_ASSIGNED_TO_NAME' => 'משתמש',
  'LBL_TAGS_LINK' => 'תגיות',
  'LBL_TAGS' => 'תגיות',
  'LBL_ID' => 'מזהה',
  'LBL_DATE_ENTERED' => 'נוצר בתאריך',
  'LBL_DATE_MODIFIED' => 'שונה בתאריך',
  'LBL_MODIFIED' => 'נערך על ידי',
  'LBL_MODIFIED_ID' => 'שונה על ידי Id',
  'LBL_MODIFIED_NAME' => 'שונה על ידי ששמו',
  'LBL_CREATED' => 'נוצר על ידי',
  'LBL_CREATED_ID' => 'נוצר על ידי Id',
  'LBL_DOC_OWNER' => 'בעל המסמך',
  'LBL_USER_FAVORITES' => 'משתמשים שמעדיפים',
  'LBL_DESCRIPTION' => 'תיאור',
  'LBL_DELETED' => 'נמחק',
  'LBL_NAME' => 'שם',
  'LBL_CREATED_USER' => 'נוצר על ידי משתמש',
  'LBL_MODIFIED_USER' => 'שונה על ידי משתמש',
  'LBL_LIST_NAME' => 'שם',
  'LBL_EDIT_BUTTON' => 'ערוך',
  'LBL_REMOVE' => 'הסר',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'שונה על ידי משתמש',
  'LBL_LIST_FORM_TITLE' => 'Errors Employees List',
  'LBL_MODULE_NAME' => 'Errors Employees',
  'LBL_MODULE_TITLE' => 'Errors Employees',
  'LBL_MODULE_NAME_SINGULAR' => 'Error Employee',
  'LBL_HOMEPAGE_TITLE' => 'שלי Errors Employees',
  'LNK_NEW_RECORD' => 'צור Error Employee',
  'LNK_LIST' => 'View Errors Employees',
  'LNK_IMPORT_ERE_ERROR_EMPLOYEES' => 'Import Errors Employees',
  'LBL_SEARCH_FORM_TITLE' => 'Search Error Employee',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_ERE_ERROR_EMPLOYEES_SUBPANEL_TITLE' => 'Errors Employees',
  'LBL_NEW_FORM_TITLE' => 'חדש Error Employee',
  'LNK_IMPORT_VCARD' => 'Import Error Employee vCard',
  'LBL_IMPORT' => 'Import Errors Employees',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Error Employee record by importing a vCard from your file system.',
);