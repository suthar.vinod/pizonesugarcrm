<?php
// created: 2022-12-13 04:46:11
$mod_strings = array (
  'LBL_TEAM' => 'Meeskonnad',
  'LBL_TEAMS' => 'Meeskonnad',
  'LBL_TEAM_ID' => 'Meeskonna ID',
  'LBL_ASSIGNED_TO_ID' => 'Määratud kasutaja ID',
  'LBL_ASSIGNED_TO_NAME' => 'Määratud kasutajale',
  'LBL_TAGS_LINK' => 'Sildid',
  'LBL_TAGS' => 'Sildid',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Loomiskuupäev',
  'LBL_DATE_MODIFIED' => 'Muutmiskuupäev',
  'LBL_MODIFIED' => 'Muutja',
  'LBL_MODIFIED_ID' => 'Muutja ID',
  'LBL_MODIFIED_NAME' => 'Muutja nimi',
  'LBL_CREATED' => 'Loodud',
  'LBL_CREATED_ID' => 'Looja ID',
  'LBL_DOC_OWNER' => 'Dokumendi omanik',
  'LBL_USER_FAVORITES' => 'Lemmikkasutajad',
  'LBL_DESCRIPTION' => 'Kirjeldus',
  'LBL_DELETED' => 'Kustutatud',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Loonud kasutaja',
  'LBL_MODIFIED_USER' => 'Muutnud kasutaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Redigeeri',
  'LBL_REMOVE' => 'Eemalda',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muutja nimi',
  'LBL_LIST_FORM_TITLE' => 'Errors Employees Loend',
  'LBL_MODULE_NAME' => 'Errors Employees',
  'LBL_MODULE_TITLE' => 'Errors Employees',
  'LBL_MODULE_NAME_SINGULAR' => 'Error Employee',
  'LBL_HOMEPAGE_TITLE' => 'Minu Errors Employees',
  'LNK_NEW_RECORD' => 'Loo Error Employee',
  'LNK_LIST' => 'Vaade Errors Employees',
  'LNK_IMPORT_ERE_ERROR_EMPLOYEES' => 'Import Errors Employees',
  'LBL_SEARCH_FORM_TITLE' => 'Otsi Error Employee',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vaata ajalugu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tegevuste voog',
  'LBL_ERE_ERROR_EMPLOYEES_SUBPANEL_TITLE' => 'Errors Employees',
  'LBL_NEW_FORM_TITLE' => 'Uus Error Employee',
  'LNK_IMPORT_VCARD' => 'Import Error Employee vCard',
  'LBL_IMPORT' => 'Import Errors Employees',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Error Employee record by importing a vCard from your file system.',
);