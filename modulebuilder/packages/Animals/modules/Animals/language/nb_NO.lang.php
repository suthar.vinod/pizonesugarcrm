<?php
// created: 2022-12-13 04:46:02
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_LIST_FORM_TITLE' => 'Animals Liste',
  'LBL_MODULE_NAME' => 'Animals',
  'LBL_MODULE_TITLE' => 'Animals',
  'LBL_MODULE_NAME_SINGULAR' => 'Animal',
  'LBL_HOMEPAGE_TITLE' => 'Min Animals',
  'LNK_NEW_RECORD' => 'Opprett Animal',
  'LNK_LIST' => 'Vis Animals',
  'LNK_IMPORT_ANML_ANIMALS' => 'Import Animals',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Animal',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_ANML_ANIMALS_SUBPANEL_TITLE' => 'Animals',
  'LBL_NEW_FORM_TITLE' => 'Ny Animal',
  'LNK_IMPORT_VCARD' => 'Import Animal vCard',
  'LBL_IMPORT' => 'Import Animals',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Animal record by importing a vCard from your file system.',
);