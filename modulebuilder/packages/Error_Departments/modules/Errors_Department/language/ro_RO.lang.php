<?php
// created: 2022-12-13 04:46:10
$mod_strings = array (
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Id Echipă',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuit lui',
  'LBL_TAGS_LINK' => 'Etichete',
  'LBL_TAGS' => 'Etichete',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data creării',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificat după Id',
  'LBL_MODIFIED_NAME' => 'Modificat după Nume',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creat de Id',
  'LBL_DOC_OWNER' => 'Proprietar document',
  'LBL_USER_FAVORITES' => 'Utilizatori cu setări favorite',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_DELETED' => 'Şters',
  'LBL_NAME' => 'Nume',
  'LBL_CREATED_USER' => 'Creat de Utilizator',
  'LBL_MODIFIED_USER' => 'Modificat după Utilizator',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_REMOVE' => 'Eliminare',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat după Nume',
  'LBL_LIST_FORM_TITLE' => 'Errors Departments Lista',
  'LBL_MODULE_NAME' => 'Errors Departments',
  'LBL_MODULE_TITLE' => 'Errors Departments',
  'LBL_MODULE_NAME_SINGULAR' => 'Errors Department',
  'LBL_HOMEPAGE_TITLE' => 'Al meu Errors Departments',
  'LNK_NEW_RECORD' => 'Creează Errors Department',
  'LNK_LIST' => 'Vizualizare Errors Departments',
  'LNK_IMPORT_ED_ERRORS_DEPARTMENT' => 'Import Errors Departments',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta Errors Department',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_ED_ERRORS_DEPARTMENT_SUBPANEL_TITLE' => 'Errors Departments',
  'LBL_NEW_FORM_TITLE' => 'Nou Errors Department',
  'LNK_IMPORT_VCARD' => 'Import Errors Department vCard',
  'LBL_IMPORT' => 'Import Errors Departments',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Errors Department record by importing a vCard from your file system.',
);