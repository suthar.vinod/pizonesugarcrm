<?php
// created: 2022-12-13 04:46:10
$mod_strings = array (
  'LBL_TEAM' => 'Команды',
  'LBL_TEAMS' => 'Команды',
  'LBL_TEAM_ID' => 'Id Команды',
  'LBL_ASSIGNED_TO_ID' => 'Ответственный (-ая)',
  'LBL_ASSIGNED_TO_NAME' => 'Ответственный (-ая)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено',
  'LBL_MODIFIED_ID' => 'Изменено (Id)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано (Id)',
  'LBL_DOC_OWNER' => 'Владелец документа',
  'LBL_USER_FAVORITES' => 'Пользователи, которые добавили в Избранное',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано',
  'LBL_MODIFIED_USER' => 'Изменено',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Правка',
  'LBL_REMOVE' => 'Удалить',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Изменено (по названию)',
  'LBL_LIST_FORM_TITLE' => 'Errors Departments Список',
  'LBL_MODULE_NAME' => 'Errors Departments',
  'LBL_MODULE_TITLE' => 'Errors Departments',
  'LBL_MODULE_NAME_SINGULAR' => 'Errors Department',
  'LBL_HOMEPAGE_TITLE' => 'Моя Errors Departments',
  'LNK_NEW_RECORD' => 'Создать Errors Department',
  'LNK_LIST' => 'Просмотр Errors Departments',
  'LNK_IMPORT_ED_ERRORS_DEPARTMENT' => 'Import Errors Departments',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск Errors Department',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_ED_ERRORS_DEPARTMENT_SUBPANEL_TITLE' => 'Errors Departments',
  'LBL_NEW_FORM_TITLE' => 'Новый Errors Department',
  'LNK_IMPORT_VCARD' => 'Import Errors Department vCard',
  'LBL_IMPORT' => 'Import Errors Departments',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Errors Department record by importing a vCard from your file system.',
);