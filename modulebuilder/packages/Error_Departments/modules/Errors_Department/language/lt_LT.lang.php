<?php
// created: 2022-12-13 04:46:10
$mod_strings = array (
  'LBL_TEAM' => 'Komandos',
  'LBL_TEAMS' => 'Komandos',
  'LBL_TEAM_ID' => 'Komandos Id',
  'LBL_ASSIGNED_TO_ID' => 'Priskirto vartotojo ID',
  'LBL_ASSIGNED_TO_NAME' => 'Kam priskirta',
  'LBL_TAGS_LINK' => 'Žymės',
  'LBL_TAGS' => 'Žymės',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Sukūrimo data',
  'LBL_DATE_MODIFIED' => 'Modifikavimo data',
  'LBL_MODIFIED' => 'Redagavo',
  'LBL_MODIFIED_ID' => 'Redaguotojo Id',
  'LBL_MODIFIED_NAME' => 'Redaguotojo vardas',
  'LBL_CREATED' => 'Sukūrė',
  'LBL_CREATED_ID' => 'Kūrėjo ID',
  'LBL_DOC_OWNER' => 'Dokumento savininkas',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Aprašas',
  'LBL_DELETED' => 'Panaikintas',
  'LBL_NAME' => 'Pavadinimas',
  'LBL_CREATED_USER' => 'Sukūręs vartotojas',
  'LBL_MODIFIED_USER' => 'Modifikavęs vartotojas.',
  'LBL_LIST_NAME' => 'Pavadinimas',
  'LBL_EDIT_BUTTON' => 'Redaguoti',
  'LBL_REMOVE' => 'Pašalinti',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikuotojo vardas',
  'LBL_LIST_FORM_TITLE' => 'Errors Departments Sąrašas',
  'LBL_MODULE_NAME' => 'Errors Departments',
  'LBL_MODULE_TITLE' => 'Errors Departments',
  'LBL_MODULE_NAME_SINGULAR' => 'Errors Department',
  'LBL_HOMEPAGE_TITLE' => 'Mano Errors Departments',
  'LNK_NEW_RECORD' => 'Sukurti Errors Department',
  'LNK_LIST' => 'View Errors Departments',
  'LNK_IMPORT_ED_ERRORS_DEPARTMENT' => 'Import Errors Departments',
  'LBL_SEARCH_FORM_TITLE' => 'Paieška Errors Department',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Rodyti istoriją',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Priminimai',
  'LBL_ED_ERRORS_DEPARTMENT_SUBPANEL_TITLE' => 'Errors Departments',
  'LBL_NEW_FORM_TITLE' => 'Naujas Errors Department',
  'LNK_IMPORT_VCARD' => 'Import Errors Department vCard',
  'LBL_IMPORT' => 'Import Errors Departments',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Errors Department record by importing a vCard from your file system.',
);