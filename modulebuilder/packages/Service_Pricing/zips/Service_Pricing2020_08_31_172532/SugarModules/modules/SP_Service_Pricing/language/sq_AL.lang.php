<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Grupet',
  'LBL_TEAMS' => 'Grupet',
  'LBL_TEAM_ID' => 'Id e grupit',
  'LBL_ASSIGNED_TO_ID' => 'Id e përdoruesit të caktuar',
  'LBL_ASSIGNED_TO_NAME' => 'Përdorues',
  'LBL_TAGS_LINK' => 'Etiketat',
  'LBL_TAGS' => 'Etiketat',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Të dhënat e krijuara',
  'LBL_DATE_MODIFIED' => 'Të dhënat e modifikuara',
  'LBL_MODIFIED' => 'Modifikuar nga',
  'LBL_MODIFIED_ID' => 'Modifikuar nga Id',
  'LBL_MODIFIED_NAME' => 'Modifikuar nga emri',
  'LBL_CREATED' => 'Krijuar nga',
  'LBL_CREATED_ID' => 'Krijuar nga Id',
  'LBL_DOC_OWNER' => 'Zotëruesi i dokumentit',
  'LBL_USER_FAVORITES' => 'Përdoruesit të cilët preferojnë',
  'LBL_DESCRIPTION' => 'Përshkrim',
  'LBL_DELETED' => 'E fshirë',
  'LBL_NAME' => 'Emri',
  'LBL_CREATED_USER' => 'Krijuar nga përdoruesi',
  'LBL_MODIFIED_USER' => 'Modifikuar nga përdoruesi',
  'LBL_LIST_NAME' => 'Emri',
  'LBL_EDIT_BUTTON' => 'Ndrysho',
  'LBL_REMOVE' => 'Hiqe',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikuar nga emri',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Service Pricing lista',
  'LBL_MODULE_NAME' => 'Service Pricing',
  'LBL_MODULE_TITLE' => 'Service Pricing',
  'LBL_MODULE_NAME_SINGULAR' => 'Service Pricing',
  'LBL_HOMEPAGE_TITLE' => 'Mia Service Pricing',
  'LNK_NEW_RECORD' => 'Krijo Service Pricing',
  'LNK_LIST' => 'Pamje Service Pricing',
  'LNK_IMPORT_SP_SERVICE_PRICING' => 'Import Service Pricing',
  'LBL_SEARCH_FORM_TITLE' => 'Kërkim Service Pricing',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'shiko historinë',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetet',
  'LBL_SP_SERVICE_PRICING_SUBPANEL_TITLE' => 'Service Pricing',
  'LBL_NEW_FORM_TITLE' => 'E re Service Pricing',
  'LNK_IMPORT_VCARD' => 'Import Service Pricing vCard',
  'LBL_IMPORT' => 'Import Service Pricing',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Service Pricing record by importing a vCard from your file system.',
  'LBL_TYPE_2' => 'Type',
  'LBL_SUBTYPE' => 'Subtype',
  'LBL_ROLES' => 'Role(s)',
  'LBL_ROOM_RMS_ROOM_ID' => 'Room (related Room ID)',
  'LBL_ROOM' => 'Room',
  'LBL_SPECIES_S_SPECIES_ID' => 'Species (related Species ID)',
  'LBL_SPECIES' => 'Species',
  'LBL_FORMAT' => 'Format',
  'LBL_CURRENCY' => 'Currency',
  'LBL_PRICE' => 'Price',
  'LBL_INTERNAL_BARCODE' => 'Internal Barcode',
);