<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Zespoły',
  'LBL_TEAMS' => 'Zespoły',
  'LBL_TEAM_ID' => 'ID zespołu',
  'LBL_ASSIGNED_TO_ID' => 'Przypisano do (ID użytkownika)',
  'LBL_ASSIGNED_TO_NAME' => 'Przydzielono do',
  'LBL_TAGS_LINK' => 'Tagi',
  'LBL_TAGS' => 'Tagi',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data utworzenia',
  'LBL_DATE_MODIFIED' => 'Data modyfikacji',
  'LBL_MODIFIED' => 'Zmodyfikowano przez',
  'LBL_MODIFIED_ID' => 'Zmodyfikowano przez (ID)',
  'LBL_MODIFIED_NAME' => 'Zmodyfikowano przez (nazwa)',
  'LBL_CREATED' => 'Utworzono przez',
  'LBL_CREATED_ID' => 'Utworzone przez (ID)',
  'LBL_DOC_OWNER' => 'Właściciel dokumentu',
  'LBL_USER_FAVORITES' => 'Użytkownicy, którzy dodali rekord do ulubionych',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Usunięto',
  'LBL_NAME' => 'Nazwa',
  'LBL_CREATED_USER' => 'Utworzono przez',
  'LBL_MODIFIED_USER' => 'Zmodyfikowane przez',
  'LBL_LIST_NAME' => 'Nazwa',
  'LBL_EDIT_BUTTON' => 'Edytuj',
  'LBL_REMOVE' => 'Usuń',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmodyfikowano przez (nazwa)',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Service Pricing Lista',
  'LBL_MODULE_NAME' => 'Service Pricing',
  'LBL_MODULE_TITLE' => 'Service Pricing',
  'LBL_MODULE_NAME_SINGULAR' => 'Service Pricing',
  'LBL_HOMEPAGE_TITLE' => 'Moje Service Pricing',
  'LNK_NEW_RECORD' => 'Utwórz Service Pricing',
  'LNK_LIST' => 'Widok Service Pricing',
  'LNK_IMPORT_SP_SERVICE_PRICING' => 'Import Service Pricing',
  'LBL_SEARCH_FORM_TITLE' => 'Wyszukiwanie Service Pricing',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Wyświetl historię',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Panel aktywności',
  'LBL_SP_SERVICE_PRICING_SUBPANEL_TITLE' => 'Service Pricing',
  'LBL_NEW_FORM_TITLE' => 'Nowy Service Pricing',
  'LNK_IMPORT_VCARD' => 'Import Service Pricing vCard',
  'LBL_IMPORT' => 'Import Service Pricing',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Service Pricing record by importing a vCard from your file system.',
  'LBL_TYPE_2' => 'Type',
  'LBL_SUBTYPE' => 'Subtype',
  'LBL_ROLES' => 'Role(s)',
  'LBL_ROOM_RMS_ROOM_ID' => 'Room (related Room ID)',
  'LBL_ROOM' => 'Room',
  'LBL_SPECIES_S_SPECIES_ID' => 'Species (related Species ID)',
  'LBL_SPECIES' => 'Species',
  'LBL_FORMAT' => 'Format',
  'LBL_CURRENCY' => 'Currency',
  'LBL_PRICE' => 'Price',
  'LBL_INTERNAL_BARCODE' => 'Internal Barcode',
);