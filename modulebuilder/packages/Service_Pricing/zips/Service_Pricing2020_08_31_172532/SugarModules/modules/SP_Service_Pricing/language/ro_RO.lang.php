<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Id Echipă',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuit lui',
  'LBL_TAGS_LINK' => 'Etichete',
  'LBL_TAGS' => 'Etichete',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data creării',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificat după Id',
  'LBL_MODIFIED_NAME' => 'Modificat după Nume',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creat de Id',
  'LBL_DOC_OWNER' => 'Proprietar document',
  'LBL_USER_FAVORITES' => 'Utilizatori cu setări favorite',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_DELETED' => 'Şters',
  'LBL_NAME' => 'Nume',
  'LBL_CREATED_USER' => 'Creat de Utilizator',
  'LBL_MODIFIED_USER' => 'Modificat după Utilizator',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_REMOVE' => 'Eliminare',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat după Nume',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Service Pricing Lista',
  'LBL_MODULE_NAME' => 'Service Pricing',
  'LBL_MODULE_TITLE' => 'Service Pricing',
  'LBL_MODULE_NAME_SINGULAR' => 'Service Pricing',
  'LBL_HOMEPAGE_TITLE' => 'Al meu Service Pricing',
  'LNK_NEW_RECORD' => 'Creează Service Pricing',
  'LNK_LIST' => 'Vizualizare Service Pricing',
  'LNK_IMPORT_SP_SERVICE_PRICING' => 'Import Service Pricing',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta Service Pricing',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_SP_SERVICE_PRICING_SUBPANEL_TITLE' => 'Service Pricing',
  'LBL_NEW_FORM_TITLE' => 'Nou Service Pricing',
  'LNK_IMPORT_VCARD' => 'Import Service Pricing vCard',
  'LBL_IMPORT' => 'Import Service Pricing',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Service Pricing record by importing a vCard from your file system.',
  'LBL_TYPE_2' => 'Type',
  'LBL_SUBTYPE' => 'Subtype',
  'LBL_ROLES' => 'Role(s)',
  'LBL_ROOM_RMS_ROOM_ID' => 'Room (related Room ID)',
  'LBL_ROOM' => 'Room',
  'LBL_SPECIES_S_SPECIES_ID' => 'Species (related Species ID)',
  'LBL_SPECIES' => 'Species',
  'LBL_FORMAT' => 'Format',
  'LBL_CURRENCY' => 'Currency',
  'LBL_PRICE' => 'Price',
  'LBL_INTERNAL_BARCODE' => 'Internal Barcode',
);