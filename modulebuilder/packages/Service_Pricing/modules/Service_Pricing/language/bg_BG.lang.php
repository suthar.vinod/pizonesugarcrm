<?php
// created: 2022-12-13 04:46:24
$mod_strings = array (
  'LBL_TEAM' => 'Екип',
  'LBL_TEAMS' => 'Екип',
  'LBL_TEAM_ID' => 'Екип',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Потребител',
  'LBL_TAGS_LINK' => 'Етикети',
  'LBL_TAGS' => 'Етикети',
  'LBL_ID' => 'Идентификатор',
  'LBL_DATE_ENTERED' => 'Създадено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_DOC_OWNER' => 'Собственик на документа',
  'LBL_USER_FAVORITES' => 'Потребители които харесват',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Изтрити',
  'LBL_NAME' => 'Име',
  'LBL_CREATED_USER' => 'Създадено от',
  'LBL_MODIFIED_USER' => 'Модифицирано от',
  'LBL_LIST_NAME' => 'Име',
  'LBL_EDIT_BUTTON' => 'Редактирай',
  'LBL_REMOVE' => 'Премахни',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Модифицирано от',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Service Pricing Списък',
  'LBL_MODULE_NAME' => 'Service Pricing',
  'LBL_MODULE_TITLE' => 'Service Pricing',
  'LBL_MODULE_NAME_SINGULAR' => 'Service Pricing',
  'LBL_HOMEPAGE_TITLE' => 'Мои Service Pricing',
  'LNK_NEW_RECORD' => 'Създай Service Pricing',
  'LNK_LIST' => 'Изглед Service Pricing',
  'LNK_IMPORT_SP_SERVICE_PRICING' => 'Import Service Pricing',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Service Pricing',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_SP_SERVICE_PRICING_SUBPANEL_TITLE' => 'Service Pricing',
  'LBL_NEW_FORM_TITLE' => 'Нов Service Pricing',
  'LNK_IMPORT_VCARD' => 'Import Service Pricing vCard',
  'LBL_IMPORT' => 'Import Service Pricing',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Service Pricing record by importing a vCard from your file system.',
  'LBL_TYPE_2' => 'Type',
  'LBL_SUBTYPE' => 'Subtype',
  'LBL_ROLES' => 'Role(s)',
  'LBL_ROOM_RMS_ROOM_ID' => 'Room (related Room ID)',
  'LBL_ROOM' => 'Room',
  'LBL_SPECIES_S_SPECIES_ID' => 'Species (related Species ID)',
  'LBL_SPECIES' => 'Species',
  'LBL_FORMAT' => 'Format',
  'LBL_CURRENCY' => 'Currency',
  'LBL_PRICE' => 'Price',
  'LBL_INTERNAL_BARCODE' => 'Internal Barcode',
);