<?php
$module_name = 'SP_Service_Pricing';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'type_2',
                'label' => 'LBL_TYPE_2',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'subtype',
                'label' => 'LBL_SUBTYPE',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'roles',
                'label' => 'LBL_ROLES',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'room',
                'label' => 'LBL_ROOM',
                'enabled' => true,
                'id' => 'RMS_ROOM_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'species',
                'label' => 'LBL_SPECIES',
                'enabled' => true,
                'id' => 'S_SPECIES_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'format',
                'label' => 'LBL_FORMAT',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'price',
                'label' => 'LBL_PRICE',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
              9 => 
              array (
                'name' => 'internal_barcode',
                'label' => 'LBL_INTERNAL_BARCODE',
                'enabled' => true,
                'default' => false,
              ),
              10 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
