<?php
// created: 2022-12-13 04:46:34
$mod_strings = array (
  'LBL_TEAM' => 'Equips',
  'LBL_TEAMS' => 'Equips',
  'LBL_TEAM_ID' => 'Id d´Equip',
  'LBL_ASSIGNED_TO_ID' => 'ID usuari assignat',
  'LBL_ASSIGNED_TO_NAME' => 'Assignat a',
  'LBL_TAGS_LINK' => 'Etiquetes',
  'LBL_TAGS' => 'Etiquetes',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Creació',
  'LBL_DATE_MODIFIED' => 'Data de modificació',
  'LBL_MODIFIED' => 'Modificat Per',
  'LBL_MODIFIED_ID' => 'Modificat per Id',
  'LBL_MODIFIED_NAME' => 'Modificat per nom',
  'LBL_CREATED' => 'Creat per',
  'LBL_CREATED_ID' => 'Creat per Id',
  'LBL_DOC_OWNER' => 'Propietari del document',
  'LBL_USER_FAVORITES' => 'Usuaris que son favorits',
  'LBL_DESCRIPTION' => 'Descripció',
  'LBL_DELETED' => 'Eliminat',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Creat per usuari',
  'LBL_MODIFIED_USER' => 'Modificat per usuari',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Treure',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat per nom',
  'LBL_LIST_FORM_TITLE' => 'Work Products Llista',
  'LBL_MODULE_NAME' => 'Work Products',
  'LBL_MODULE_TITLE' => 'Work Products',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product',
  'LBL_HOMEPAGE_TITLE' => 'Meu Work Products',
  'LNK_NEW_RECORD' => 'Crea Work Product',
  'LNK_LIST' => 'Vista Work Products',
  'LNK_IMPORT_M03_WORK_PRODUCT' => 'Import Work Products',
  'LBL_SEARCH_FORM_TITLE' => 'Cerca Work Product',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Veure historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Seqüència d&#39;activitats',
  'LBL_M03_WORK_PRODUCT_SUBPANEL_TITLE' => 'Work Products',
  'LBL_NEW_FORM_TITLE' => 'Nou Work Product',
  'LNK_IMPORT_VCARD' => 'Import Work Product vCard',
  'LBL_IMPORT' => 'Import Work Products',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product record by importing a vCard from your file system.',
);