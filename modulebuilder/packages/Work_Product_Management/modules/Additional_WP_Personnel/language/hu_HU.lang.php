<?php
// created: 2022-12-13 04:46:34
$mod_strings = array (
  'LBL_TEAM' => 'Csoportok',
  'LBL_TEAMS' => 'Csoportok',
  'LBL_TEAM_ID' => 'Csoport azonosító',
  'LBL_ASSIGNED_TO_ID' => 'Felelős felhasználói azonosító',
  'LBL_ASSIGNED_TO_NAME' => 'Felelős',
  'LBL_TAGS_LINK' => 'Címkék',
  'LBL_TAGS' => 'Címkék',
  'LBL_ID' => 'Azonosító',
  'LBL_DATE_ENTERED' => 'Kezdés dátuma',
  'LBL_DATE_MODIFIED' => 'Módosítás dátuma',
  'LBL_MODIFIED' => 'Módosította',
  'LBL_MODIFIED_ID' => 'Módosító azonosítója',
  'LBL_MODIFIED_NAME' => 'Módosító neve',
  'LBL_CREATED' => 'Létrehozta',
  'LBL_CREATED_ID' => 'Létrehozó azonosítója',
  'LBL_DOC_OWNER' => 'Dokumentum tulajdonosa',
  'LBL_USER_FAVORITES' => 'Felhasználók, akik kedvelték',
  'LBL_DESCRIPTION' => 'Leírás',
  'LBL_DELETED' => 'Törölve',
  'LBL_NAME' => 'Név',
  'LBL_CREATED_USER' => 'Létrehozó felhasználó',
  'LBL_MODIFIED_USER' => 'Módosító felhasználó',
  'LBL_LIST_NAME' => 'Név',
  'LBL_EDIT_BUTTON' => 'Szerkeszt',
  'LBL_REMOVE' => 'Eltávolítás',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Módosító neve',
  'LBL_LIST_FORM_TITLE' => 'Additional WP Personnel Lista',
  'LBL_MODULE_NAME' => 'Additional WP Personnel',
  'LBL_MODULE_TITLE' => 'Additional WP Personnel',
  'LBL_MODULE_NAME_SINGULAR' => 'Additional WP Person',
  'LBL_HOMEPAGE_TITLE' => 'Saját Additional WP Personnel',
  'LNK_NEW_RECORD' => 'Új létrehozása Additional WP Person',
  'LNK_LIST' => 'Megtekintés Additional WP Personnel',
  'LNK_IMPORT_M03_WORK_PRODUCT_PERSONNEL' => 'Import Additional WP Person',
  'LBL_SEARCH_FORM_TITLE' => 'Keresés Additional WP Person',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Előzmények megtekintése',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tevékenységek',
  'LBL_M03_WORK_PRODUCT_PERSONNEL_SUBPANEL_TITLE' => 'Additional WP Personnel',
  'LBL_NEW_FORM_TITLE' => 'Új Additional WP Person',
  'LNK_IMPORT_VCARD' => 'Import Additional WP Person vCard',
  'LBL_IMPORT' => 'Import Additional WP Personnel',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Additional WP Person record by importing a vCard from your file system.',
  'LNK_IMPORT_M03_ADDITIONAL_WP_PERSONNEL' => 'Import Additional WP Personnel',
  'LBL_M03_ADDITIONAL_WP_PERSONNEL_SUBPANEL_TITLE' => 'Additional WP Personnel',
);