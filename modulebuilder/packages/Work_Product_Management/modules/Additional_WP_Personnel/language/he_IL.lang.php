<?php
// created: 2022-12-13 04:46:34
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'צוות Id',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'הוקצה עבור',
  'LBL_TAGS_LINK' => 'תגיות',
  'LBL_TAGS' => 'תגיות',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'נוצר בתאריך',
  'LBL_DATE_MODIFIED' => 'שונה בתאריך',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'שונה על ידי Id',
  'LBL_MODIFIED_NAME' => 'שונה על ידי ששמו',
  'LBL_CREATED' => 'נוצר על ידי',
  'LBL_CREATED_ID' => 'נוצר על ידי Id',
  'LBL_DOC_OWNER' => 'בעלים של המסמך',
  'LBL_USER_FAVORITES' => 'משתמשים שמעדיפים',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'נמחק',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'נוצר על ידי משתמש',
  'LBL_MODIFIED_USER' => 'שונה על ידי משתמש',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'הסר',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'שונה על ידי ששמו',
  'LBL_LIST_FORM_TITLE' => 'Additional WP Personnel List',
  'LBL_MODULE_NAME' => 'Additional WP Personnel',
  'LBL_MODULE_TITLE' => 'Additional WP Personnel',
  'LBL_MODULE_NAME_SINGULAR' => 'Additional WP Person',
  'LBL_HOMEPAGE_TITLE' => 'שלי Additional WP Personnel',
  'LNK_NEW_RECORD' => 'צור Additional WP Person',
  'LNK_LIST' => 'View Additional WP Personnel',
  'LNK_IMPORT_M03_WORK_PRODUCT_PERSONNEL' => 'Import Additional WP Person',
  'LBL_SEARCH_FORM_TITLE' => 'Search Additional WP Person',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_M03_WORK_PRODUCT_PERSONNEL_SUBPANEL_TITLE' => 'Additional WP Personnel',
  'LBL_NEW_FORM_TITLE' => 'חדש Additional WP Person',
  'LNK_IMPORT_VCARD' => 'Import Additional WP Person vCard',
  'LBL_IMPORT' => 'Import Additional WP Personnel',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Additional WP Person record by importing a vCard from your file system.',
  'LNK_IMPORT_M03_ADDITIONAL_WP_PERSONNEL' => 'Import Additional WP Personnel',
  'LBL_M03_ADDITIONAL_WP_PERSONNEL_SUBPANEL_TITLE' => 'Additional WP Personnel',
);