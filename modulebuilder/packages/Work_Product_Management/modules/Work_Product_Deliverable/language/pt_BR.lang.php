<?php
// created: 2022-12-13 04:46:36
$mod_strings = array (
  'LBL_TEAM' => 'Equipes',
  'LBL_TEAMS' => 'Equipes',
  'LBL_TEAM_ID' => 'Id Equipe',
  'LBL_ASSIGNED_TO_ID' => 'Atribuído a',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a:',
  'LBL_TAGS_LINK' => 'Marcações',
  'LBL_TAGS' => 'Marcações',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Introdução',
  'LBL_DATE_MODIFIED' => 'Data da modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado por',
  'LBL_CREATED_ID' => 'Criado por Id',
  'LBL_DOC_OWNER' => 'Dono do Documento',
  'LBL_USER_FAVORITES' => 'Usuários Favoritos',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Deletado',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado pelo Usuário',
  'LBL_MODIFIED_USER' => 'Modificado pelo Usuário',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado Por Nome',
  'LBL_LIST_FORM_TITLE' => 'Work Product Deliverables Lista',
  'LBL_MODULE_NAME' => 'Work Product Deliverables',
  'LBL_MODULE_TITLE' => 'Work Product Deliverables',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Deliverables',
  'LBL_HOMEPAGE_TITLE' => 'Minha Work Product Deliverables',
  'LNK_NEW_RECORD' => 'Criar Work Product Deliverables',
  'LNK_LIST' => 'Visualização Work Product Deliverables',
  'LNK_IMPORT_M03_WORK_PRODUCT_DELIVERABLE' => 'Import Work Product Deliverables',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Work Product Deliverables',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Cadeia de atividades',
  'LBL_M03_WORK_PRODUCT_DELIVERABLE_SUBPANEL_TITLE' => 'Work Product Deliverables',
  'LBL_NEW_FORM_TITLE' => 'Novo Work Product Deliverables',
  'LNK_IMPORT_VCARD' => 'Import Work Product Deliverables vCard',
  'LBL_IMPORT' => 'Import Work Product Deliverables',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Deliverables record by importing a vCard from your file system.',
);