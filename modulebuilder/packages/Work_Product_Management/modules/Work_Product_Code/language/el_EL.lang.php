<?php
// created: 2022-12-13 04:46:35
$mod_strings = array (
  'LBL_TEAM' => 'Ομάδες',
  'LBL_TEAMS' => 'Ομάδες',
  'LBL_TEAM_ID' => 'Ταυτότητα Ομάδας',
  'LBL_ASSIGNED_TO_ID' => 'Ταυτότητα Ανατεθειμένου Χειριστή',
  'LBL_ASSIGNED_TO_NAME' => 'Ανατέθηκε σε',
  'LBL_TAGS_LINK' => 'Ετικέτες',
  'LBL_TAGS' => 'Ετικέτες',
  'LBL_ID' => 'Ταυτότητα',
  'LBL_DATE_ENTERED' => 'Ημερομηνία Δημιουργίας',
  'LBL_DATE_MODIFIED' => 'Ημερομηνία Τροποποίησης',
  'LBL_MODIFIED' => 'Τροποποιήθηκε Από',
  'LBL_MODIFIED_ID' => 'Τροποποιήθηκε Από Ταυτότητα',
  'LBL_MODIFIED_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_CREATED' => 'Δημιουργήθηκε Από',
  'LBL_CREATED_ID' => 'Δημιουργήθηκε Από Ταυτότητα',
  'LBL_DOC_OWNER' => 'Έγγραφο Ιδιοκτήτη',
  'LBL_USER_FAVORITES' => 'Αγαπημένα Χρηστών',
  'LBL_DESCRIPTION' => 'Περιγραφή',
  'LBL_DELETED' => 'Διαγράφηκε',
  'LBL_NAME' => 'Όνομα',
  'LBL_CREATED_USER' => 'Δημιουργήθηκε από Χειριστή',
  'LBL_MODIFIED_USER' => 'Τροποποιήθηκε από Χειριστή',
  'LBL_LIST_NAME' => 'Όνομα',
  'LBL_EDIT_BUTTON' => 'Επεξεργασία',
  'LBL_REMOVE' => 'Αφαίρεση',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_LIST_FORM_TITLE' => 'Work Product Codes Λίστα',
  'LBL_MODULE_NAME' => 'Work Product Codes',
  'LBL_MODULE_TITLE' => 'Work Product Codes',
  'LBL_MODULE_NAME_SINGULAR' => 'Work Product Code',
  'LBL_HOMEPAGE_TITLE' => 'Δική Μου Work Product Codes',
  'LNK_NEW_RECORD' => 'Δημιουργία Work Product Code',
  'LNK_LIST' => 'Προβολή Work Product Codes',
  'LNK_IMPORT_M03_WORK_PRODUCT_CODE' => 'Import Work Product Codes',
  'LBL_SEARCH_FORM_TITLE' => 'Αναζήτηση Work Product Code',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Προβολή Ιστορικού',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Δραστηριότητες',
  'LBL_M03_WORK_PRODUCT_CODE_SUBPANEL_TITLE' => 'Work Product Codes',
  'LBL_NEW_FORM_TITLE' => 'Νέα Work Product Code',
  'LNK_IMPORT_VCARD' => 'Import Work Product Code vCard',
  'LBL_IMPORT' => 'Import Work Product Codes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Work Product Code record by importing a vCard from your file system.',
);