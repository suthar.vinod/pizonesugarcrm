<?php
// created: 2022-12-13 04:46:04
$mod_strings = array (
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Id Echipă',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuit lui',
  'LBL_TAGS_LINK' => 'Etichete',
  'LBL_TAGS' => 'Etichete',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data creării',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificat după Id',
  'LBL_MODIFIED_NAME' => 'Modificat după Nume',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creat de Id',
  'LBL_DOC_OWNER' => 'Proprietar document',
  'LBL_USER_FAVORITES' => 'Utilizatori cu setări favorite',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_DELETED' => 'Şters',
  'LBL_NAME' => 'Nume',
  'LBL_CREATED_USER' => 'Creat de Utilizator',
  'LBL_MODIFIED_USER' => 'Modificat după Utilizator',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_REMOVE' => 'Eliminare',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat după Nume',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Company Addresses Lista',
  'LBL_MODULE_NAME' => 'Company Addresses',
  'LBL_MODULE_TITLE' => 'Company Addresses',
  'LBL_MODULE_NAME_SINGULAR' => 'Company Address',
  'LBL_HOMEPAGE_TITLE' => 'Al meu Company Addresses',
  'LNK_NEW_RECORD' => 'Creează Company Address',
  'LNK_LIST' => 'Vizualizare Company Addresses',
  'LNK_IMPORT_CA_COMPANY_ADDRESS' => 'Import Company Addresses',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta Company Address',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_CA_COMPANY_ADDRESS_SUBPANEL_TITLE' => 'Company Addresses',
  'LBL_NEW_FORM_TITLE' => 'Nou Company Address',
  'LNK_IMPORT_VCARD' => 'Import Company Address vCard',
  'LBL_IMPORT' => 'Import Company Addresses',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Company Address record by importing a vCard from your file system.',
);