<?php
// created: 2022-12-13 04:46:04
$mod_strings = array (
  'LBL_TEAM' => 'Equipas',
  'LBL_TEAMS' => 'Equipas',
  'LBL_TEAM_ID' => 'Id Equipa',
  'LBL_ASSIGNED_TO_ID' => 'ID de Utilizador Atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_TAGS_LINK' => 'Etiquetas',
  'LBL_TAGS' => 'Etiquetas',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data da Criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'ID do Utilizador que Criou',
  'LBL_DOC_OWNER' => 'Proprietário do Documento',
  'LBL_USER_FAVORITES' => 'Utilizadores que adicionaram aos Favoritos',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado por Utilizador',
  'LBL_MODIFIED_USER' => 'Modificado por Utilizador',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado Por Nome',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Company Addresses Lista',
  'LBL_MODULE_NAME' => 'Company Addresses',
  'LBL_MODULE_TITLE' => 'Company Addresses',
  'LBL_MODULE_NAME_SINGULAR' => 'Company Address',
  'LBL_HOMEPAGE_TITLE' => 'Minha Company Addresses',
  'LNK_NEW_RECORD' => 'Criar Company Address',
  'LNK_LIST' => 'Vista Company Addresses',
  'LNK_IMPORT_CA_COMPANY_ADDRESS' => 'Import Company Addresses',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Company Address',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Fluxo de Atividades',
  'LBL_CA_COMPANY_ADDRESS_SUBPANEL_TITLE' => 'Company Addresses',
  'LBL_NEW_FORM_TITLE' => 'Novo Company Address',
  'LNK_IMPORT_VCARD' => 'Import Company Address vCard',
  'LBL_IMPORT' => 'Import Company Addresses',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Company Address record by importing a vCard from your file system.',
);