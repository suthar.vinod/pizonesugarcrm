<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['rrd_regulatory_response_doc_category_dom']['Marketing'] = 'マーケティング';
$app_list_strings['rrd_regulatory_response_doc_category_dom']['Knowledege Base'] = 'ナレッジベース';
$app_list_strings['rrd_regulatory_response_doc_category_dom']['Sales'] = '営業';
$app_list_strings['rrd_regulatory_response_doc_category_dom'][''] = '';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom']['Marketing Collateral'] = 'マーケティング資料';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom']['Product Brochures'] = '製品パンフレット';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['rrd_regulatory_response_doc_subcategory_dom'][''] = '';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Active'] = 'アクティブ';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Draft'] = 'ドラフト';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Expired'] = '期限切れ';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Under Review'] = 'レビュー中';
$app_list_strings['rrd_regulatory_response_doc_status_dom']['Pending'] = '保留';
$app_list_strings['moduleList']['RRD_Regulatory_Response_Doc'] = 'Regulatory Response Documents';
$app_list_strings['moduleListSingular']['RRD_Regulatory_Response_Doc'] = 'Regulatory Response Document';
