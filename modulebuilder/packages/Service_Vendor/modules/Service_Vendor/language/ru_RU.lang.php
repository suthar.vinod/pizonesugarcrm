<?php
// created: 2022-12-13 04:46:25
$mod_strings = array (
  'LBL_TEAM' => 'Команды',
  'LBL_TEAMS' => 'Команды',
  'LBL_TEAM_ID' => 'Id Команды',
  'LBL_ASSIGNED_TO_ID' => 'Ответственный (-ая)',
  'LBL_ASSIGNED_TO_NAME' => 'Ответственный (-ая)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено',
  'LBL_MODIFIED_ID' => 'Изменено (Id)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано (Id)',
  'LBL_DOC_OWNER' => 'Владелец документа',
  'LBL_USER_FAVORITES' => 'Пользователи, которые добавили в Избранное',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано',
  'LBL_MODIFIED_USER' => 'Изменено',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Правка',
  'LBL_REMOVE' => 'Удалить',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Изменено (по названию)',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Service Vendors Список',
  'LBL_MODULE_NAME' => 'Service Vendors',
  'LBL_MODULE_TITLE' => 'Service Vendors',
  'LBL_MODULE_NAME_SINGULAR' => 'Service Vendor',
  'LBL_HOMEPAGE_TITLE' => 'Моя Service Vendors',
  'LNK_NEW_RECORD' => 'Создать Service Vendor',
  'LNK_LIST' => 'Просмотр Service Vendors',
  'LNK_IMPORT_SV_SERVICE_VENDOR' => 'Import Service Vendors',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск Service Vendor',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_SV_SERVICE_VENDOR_SUBPANEL_TITLE' => 'Service Vendors',
  'LBL_NEW_FORM_TITLE' => 'Новый Service Vendor',
  'LNK_IMPORT_VCARD' => 'Import Service Vendor vCard',
  'LBL_IMPORT' => 'Import Service Vendors',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Service Vendor record by importing a vCard from your file system.',
  'LBL_SERVICE_VENDOR_ACCOUNT_ID' => 'Service Vendor (related Company ID)',
  'LBL_SERVICE_VENDOR' => 'Service Vendor',
);