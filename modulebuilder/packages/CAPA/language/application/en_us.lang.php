<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['CAPA_CAPA'] = 'CAPAs';
$app_list_strings['moduleListSingular']['CAPA_CAPA'] = 'CAPA';
$app_list_strings['ca_andor_pa_list']['CA'] = 'CA';
$app_list_strings['ca_andor_pa_list']['PA'] = 'PA';
$app_list_strings['ca_andor_pa_list'][''] = '';
$app_list_strings['risk_assessment_list']['Minor'] = 'Minor – No further investigation required. Complete CAPA Review.';
$app_list_strings['risk_assessment_list']['Significant'] = 'Significant – Investigation Report and Corrective Action Plan required.';
$app_list_strings['risk_assessment_list']['Not applicable'] = 'Not applicable – Preventative Action Plan required.';
$app_list_strings['risk_assessment_list'][''] = '';
$app_list_strings['yes_no_list']['Yes'] = 'Yes';
$app_list_strings['yes_no_list']['No'] = 'No';
$app_list_strings['yes_no_list'][''] = '';
