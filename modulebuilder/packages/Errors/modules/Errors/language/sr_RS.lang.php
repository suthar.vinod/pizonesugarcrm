<?php
// created: 2022-12-13 04:46:12
$mod_strings = array (
  'LBL_TEAM' => 'Timovi',
  'LBL_TEAMS' => 'Timovi',
  'LBL_TEAM_ID' => 'ID broj tima',
  'LBL_ASSIGNED_TO_ID' => 'ID broj dodeljenog korisnika',
  'LBL_ASSIGNED_TO_NAME' => 'Korisnik',
  'LBL_TAGS_LINK' => 'Oznake',
  'LBL_TAGS' => 'Oznake',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum kreiranja',
  'LBL_DATE_MODIFIED' => 'Datum izmene',
  'LBL_MODIFIED' => 'Promenio',
  'LBL_MODIFIED_ID' => 'ID broj korisnika koji je promenio',
  'LBL_MODIFIED_NAME' => 'Ime korisnika koji je promenio',
  'LBL_CREATED' => 'Autor',
  'LBL_CREATED_ID' => 'ID broj korisnika koji je kreirao',
  'LBL_DOC_OWNER' => 'Vlasnik dokumenta',
  'LBL_USER_FAVORITES' => 'Korisnici koji su označili omiljene',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Obrisan',
  'LBL_NAME' => 'Ime',
  'LBL_CREATED_USER' => 'Kreirao korisnik',
  'LBL_MODIFIED_USER' => 'Promenio korisnik',
  'LBL_LIST_NAME' => 'Ime',
  'LBL_EDIT_BUTTON' => 'Izmeni',
  'LBL_REMOVE' => 'Ukloni',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ime porisnika koji je promenio',
  'LBL_LIST_FORM_TITLE' => 'Errors Lista',
  'LBL_MODULE_NAME' => 'Errors',
  'LBL_MODULE_TITLE' => 'Errors',
  'LBL_MODULE_NAME_SINGULAR' => 'Error',
  'LBL_HOMEPAGE_TITLE' => 'Moja Errors',
  'LNK_NEW_RECORD' => 'Kreiraj Error',
  'LNK_LIST' => 'Pregled Errors',
  'LNK_IMPORT_M06_ERROR' => 'Import Error',
  'LBL_SEARCH_FORM_TITLE' => 'Pretraga Error',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Pregled istorije',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivnosti',
  'LBL_M06_ERROR_SUBPANEL_TITLE' => 'Errors',
  'LBL_NEW_FORM_TITLE' => 'Novo Error',
  'LNK_IMPORT_VCARD' => 'Import Error vCard',
  'LBL_IMPORT' => 'Import Errors',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Error record by importing a vCard from your file system.',
  'LNK_IMPORT_M06_ERRORS' => 'Import Errors',
  'LBL_M06_ERRORS_SUBPANEL_TITLE' => 'Errors',
  'LBL_OCCURRENCE_DATE' => 'Occurrence Date',
  'LBL_DISCOVERY_DATE' => 'Discovery Date',
  'LBL_CATEGORY' => 'Category',
  'LBL_RESPONSIBLE_DEPARTMENT' => 'Responsible Department',
  'LBL_WEEKEND_OCCURRENCE' => 'Weekend Occurrence',
  'LBL_SOP_ID' => 'SOP ID',
  'LBL_FORM_ID' => 'Form ID',
  'LBL_TECHNICIAN' => 'Technician',
  'LBL_ERROR_TYPE' => 'Error Type',
  'LBL_IMPACTFUL' => 'Impactful',
  'LBL_OCCURRENCE' => 'Occurrence',
  'LBL_ERROR_CLASSIFICATION' => 'Error Classification',
);