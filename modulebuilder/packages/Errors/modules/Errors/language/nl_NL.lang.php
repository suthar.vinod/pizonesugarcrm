<?php
// created: 2022-12-13 04:46:12
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen aan ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum ingevoerd',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door Naam',
  'LBL_CREATED' => 'Gemaakt door',
  'LBL_CREATED_ID' => 'Gemaakt door ID',
  'LBL_DOC_OWNER' => 'Eigenaar van document',
  'LBL_USER_FAVORITES' => 'Gebruikers die favoriet gemaakt hebben',
  'LBL_DESCRIPTION' => 'Beschrijving',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_NAME' => 'Naam',
  'LBL_CREATED_USER' => 'Gemaakt door Gebruiker',
  'LBL_MODIFIED_USER' => 'Gewijzigd door Gebruiker',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_EDIT_BUTTON' => 'Wijzig',
  'LBL_REMOVE' => 'Verwijder',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Gewijzigd door Naam',
  'LBL_LIST_FORM_TITLE' => 'Errors List',
  'LBL_MODULE_NAME' => 'Errors',
  'LBL_MODULE_TITLE' => 'Errors',
  'LBL_MODULE_NAME_SINGULAR' => 'Error',
  'LBL_HOMEPAGE_TITLE' => 'Mijn Errors',
  'LNK_NEW_RECORD' => 'Create Error',
  'LNK_LIST' => 'View Errors',
  'LNK_IMPORT_M06_ERROR' => 'Import Error',
  'LBL_SEARCH_FORM_TITLE' => 'Zoeken Error',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_M06_ERROR_SUBPANEL_TITLE' => 'Errors',
  'LBL_NEW_FORM_TITLE' => 'Nieuw Error',
  'LNK_IMPORT_VCARD' => 'Import Error vCard',
  'LBL_IMPORT' => 'Import Errors',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Error record by importing a vCard from your file system.',
  'LNK_IMPORT_M06_ERRORS' => 'Import Errors',
  'LBL_M06_ERRORS_SUBPANEL_TITLE' => 'Errors',
  'LBL_OCCURRENCE_DATE' => 'Occurrence Date',
  'LBL_DISCOVERY_DATE' => 'Discovery Date',
  'LBL_CATEGORY' => 'Category',
  'LBL_RESPONSIBLE_DEPARTMENT' => 'Responsible Department',
  'LBL_WEEKEND_OCCURRENCE' => 'Weekend Occurrence',
  'LBL_SOP_ID' => 'SOP ID',
  'LBL_FORM_ID' => 'Form ID',
  'LBL_TECHNICIAN' => 'Technician',
  'LBL_ERROR_TYPE' => 'Error Type',
  'LBL_IMPACTFUL' => 'Impactful',
  'LBL_OCCURRENCE' => 'Occurrence',
  'LBL_ERROR_CLASSIFICATION' => 'Error Classification',
);