<?php
// created: 2022-12-13 04:46:12
$mod_strings = array (
  'LBL_TEAM' => 'Equip',
  'LBL_TEAMS' => 'Equips',
  'LBL_TEAM_ID' => 'Id Equip',
  'LBL_ASSIGNED_TO_ID' => 'Usuari Assignat',
  'LBL_ASSIGNED_TO_NAME' => 'Assignat a',
  'LBL_TAGS_LINK' => 'Etiquetes',
  'LBL_TAGS' => 'Etiquetes',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Creació',
  'LBL_DATE_MODIFIED' => 'Última Modificació',
  'LBL_MODIFIED' => 'Modificat Per',
  'LBL_MODIFIED_ID' => 'Modificat Per Id',
  'LBL_MODIFIED_NAME' => 'Modificat Per Nom',
  'LBL_CREATED' => 'Creat Per',
  'LBL_CREATED_ID' => 'Creat Per Id',
  'LBL_DOC_OWNER' => 'Propietari del document',
  'LBL_USER_FAVORITES' => 'Usuaris que son favorits',
  'LBL_DESCRIPTION' => 'Descripció',
  'LBL_DELETED' => 'Eliminat',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Creat Per Usuari',
  'LBL_MODIFIED_USER' => 'Modificat Per Usuari',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Treure',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat per nom',
  'LBL_LIST_FORM_TITLE' => 'Errors Llista',
  'LBL_MODULE_NAME' => 'Errors',
  'LBL_MODULE_TITLE' => 'Errors',
  'LBL_MODULE_NAME_SINGULAR' => 'Error',
  'LBL_HOMEPAGE_TITLE' => 'Meu Errors',
  'LNK_NEW_RECORD' => 'Crea Error',
  'LNK_LIST' => 'Vista Errors',
  'LNK_IMPORT_M06_ERROR' => 'Import Error',
  'LBL_SEARCH_FORM_TITLE' => 'Cerca Error',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Veure historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Seqüència d&#39;activitats',
  'LBL_M06_ERROR_SUBPANEL_TITLE' => 'Errors',
  'LBL_NEW_FORM_TITLE' => 'Nou Error',
  'LNK_IMPORT_VCARD' => 'Import Error vCard',
  'LBL_IMPORT' => 'Import Errors',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Error record by importing a vCard from your file system.',
  'LNK_IMPORT_M06_ERRORS' => 'Import Errors',
  'LBL_M06_ERRORS_SUBPANEL_TITLE' => 'Errors',
  'LBL_OCCURRENCE_DATE' => 'Occurrence Date',
  'LBL_DISCOVERY_DATE' => 'Discovery Date',
  'LBL_CATEGORY' => 'Category',
  'LBL_RESPONSIBLE_DEPARTMENT' => 'Responsible Department',
  'LBL_WEEKEND_OCCURRENCE' => 'Weekend Occurrence',
  'LBL_SOP_ID' => 'SOP ID',
  'LBL_FORM_ID' => 'Form ID',
  'LBL_TECHNICIAN' => 'Technician',
  'LBL_ERROR_TYPE' => 'Error Type',
  'LBL_IMPACTFUL' => 'Impactful',
  'LBL_OCCURRENCE' => 'Occurrence',
  'LBL_ERROR_CLASSIFICATION' => 'Error Classification',
);