<?php
// created: 2022-12-13 04:46:04
$mod_strings = array (
  'LBL_TEAM' => 'Csoportok',
  'LBL_TEAMS' => 'Csoportok',
  'LBL_TEAM_ID' => 'Csoport azonosító',
  'LBL_ASSIGNED_TO_ID' => 'Hozzárendelt felhasználó azonosító',
  'LBL_ASSIGNED_TO_NAME' => 'Felelős felhasználó',
  'LBL_TAGS_LINK' => 'Címkék',
  'LBL_TAGS' => 'Címkék',
  'LBL_ID' => 'Azonosító',
  'LBL_DATE_ENTERED' => 'Létrehozás dátuma',
  'LBL_DATE_MODIFIED' => 'Módosítás dátuma',
  'LBL_MODIFIED' => 'Módosította',
  'LBL_MODIFIED_ID' => 'Módosította (azonosító szerint)',
  'LBL_MODIFIED_NAME' => 'Módosította (név szerint)',
  'LBL_CREATED' => 'Létrehozta',
  'LBL_CREATED_ID' => 'Létrehozó azonosítója',
  'LBL_DOC_OWNER' => 'Dokumentum tulajdonosa',
  'LBL_USER_FAVORITES' => 'Felhasználók, akik kedvelték',
  'LBL_DESCRIPTION' => 'Leírás',
  'LBL_DELETED' => 'Törölve',
  'LBL_NAME' => 'Név',
  'LBL_CREATED_USER' => 'Felhasználó által létrehozva',
  'LBL_MODIFIED_USER' => 'Felhasználó által módosítva',
  'LBL_LIST_NAME' => 'Név',
  'LBL_EDIT_BUTTON' => 'Szerkesztés',
  'LBL_REMOVE' => 'Eltávolítás',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Módosítva név szerint',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Név által létrehozva',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_MODULE_NAME' => 'CAPA Files',
  'LBL_MODULE_TITLE' => 'CAPA Files',
  'LNK_NEW_DOCUMENT' => 'Dokumentum létrehozása',
  'LNK_DOCUMENT_LIST' => 'Dokumentumok lista',
  'LBL_SEARCH_FORM_TITLE' => 'Keresés CAPA File',
  'LBL_DOCUMENT_ID' => 'Dokumentum azonosító',
  'LBL_ASSIGNED_TO' => 'Felelős:',
  'LBL_CATEGORY' => 'Kategória',
  'LBL_SUBCATEGORY' => 'Alkategória',
  'LBL_STATUS' => 'Állapot',
  'LBL_IS_TEMPLATE' => 'egy sablon',
  'LBL_TEMPLATE_TYPE' => 'Dokumentum típusa',
  'LBL_REVISION_NAME' => 'Felülvizsgálat száma',
  'LBL_MIME' => 'Mime típus',
  'LBL_REVISION' => 'Felülvizsgálat',
  'LBL_DOCUMENT' => 'Kapcsolódó dokumentum',
  'LBL_LATEST_REVISION' => 'Utolsó felülvizsgálat',
  'LBL_CHANGE_LOG' => 'Változásnapló',
  'LBL_ACTIVE_DATE' => 'Közzététel dátuma',
  'LBL_EXPIRATION_DATE' => 'Lejárat dátuma',
  'LBL_FILE_EXTENSION' => 'Fájl kiterjesztése',
  'LBL_CAT_OR_SUBCAT_UNSPEC' => 'Nincs meghatározva',
  'LBL_NEW_FORM_TITLE' => 'Új CAPA File',
  'LBL_DOC_NAME' => 'Dokumentum neve:',
  'LBL_FILENAME' => 'Fájlnév:',
  'LBL_FILE_UPLOAD' => 'Fájl:',
  'LBL_DOC_VERSION' => 'Felülvizsgálat:',
  'LBL_CATEGORY_VALUE' => 'Kategória:',
  'LBL_SUBCATEGORY_VALUE' => 'Alkategória:',
  'LBL_DOC_STATUS' => 'Állapot:',
  'LBL_DET_TEMPLATE_TYPE' => 'Dokumentum típusa:',
  'LBL_DOC_DESCRIPTION' => 'Leírás:',
  'LBL_DOC_ACTIVE_DATE' => 'Közzététel dátuma:',
  'LBL_DOC_EXP_DATE' => 'Lejárat dátuma:',
  'LBL_LIST_FORM_TITLE' => 'CAPA Files Lista',
  'LBL_LIST_DOCUMENT' => 'Dokumentum',
  'LBL_LIST_CATEGORY' => 'Kategória',
  'LBL_LIST_SUBCATEGORY' => 'Alkategória',
  'LBL_LIST_REVISION' => 'Felülvizsgálat',
  'LBL_LIST_LAST_REV_CREATOR' => 'Közzétette',
  'LBL_LIST_LAST_REV_DATE' => 'Felülvizsgálat dátuma',
  'LBL_LIST_VIEW_DOCUMENT' => 'Nézet',
  'LBL_LIST_DOWNLOAD' => 'Letöltés',
  'LBL_LIST_ACTIVE_DATE' => 'Közzététel dátuma',
  'LBL_LIST_EXP_DATE' => 'Lejárat dátuma',
  'LBL_LIST_STATUS' => 'Állapot',
  'LBL_LIST_DOC_TYPE' => 'Dokumentum Típus',
  'LBL_LIST_FILENAME' => 'Fájlnév',
  'LBL_SF_DOCUMENT' => 'Dokumentum neve:',
  'LBL_SF_CATEGORY' => 'Kategória:',
  'LBL_SF_SUBCATEGORY' => 'Alkategória:',
  'LBL_SF_ACTIVE_DATE' => 'Közzététel dátuma:',
  'LBL_SF_EXP_DATE' => 'Lejárat dátuma:',
  'DEF_CREATE_LOG' => 'Dokumentum létrehozva',
  'ERR_DOC_NAME' => 'Dokumentum neve',
  'ERR_DOC_ACTIVE_DATE' => 'Közzététel dátuma',
  'ERR_DOC_EXP_DATE' => 'Lejárat dátuma',
  'ERR_FILENAME' => 'Fájlnév',
  'LBL_TREE_TITLE' => 'Dokumentumok',
  'LBL_LIST_DOCUMENT_NAME' => 'Dokumentum neve',
  'LBL_MODULE_NAME_SINGULAR' => 'CAPA File',
  'LBL_HOMEPAGE_TITLE' => 'Saját CAPA Files',
  'LNK_NEW_RECORD' => 'Új létrehozása CAPA File',
  'LNK_LIST' => 'Megtekintés CAPA Files',
  'LNK_IMPORT_CF_CAPA_FILES' => 'Import CAPA Files',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Előzmények megtekintése',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tevékenységek',
  'LBL_CF_CAPA_FILES_SUBPANEL_TITLE' => 'CAPA Files',
  'LNK_IMPORT_VCARD' => 'Import CAPA File vCard',
  'LBL_IMPORT' => 'Import CAPA Files',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new CAPA File record by importing a vCard from your file system.',
  'LBL_CF_CAPA_FILES_FOCUS_DRAWER_DASHBOARD' => 'CAPA Files Focus Drawer',
  'LBL_CF_CAPA_FILES_RECORD_DASHBOARD' => 'CAPA Files Record Dashboard',
);