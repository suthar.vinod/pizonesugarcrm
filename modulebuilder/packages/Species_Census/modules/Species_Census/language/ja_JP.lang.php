<?php
// created: 2022-12-13 04:46:25
$mod_strings = array (
  'LBL_TEAM' => 'チーム',
  'LBL_TEAMS' => 'チーム',
  'LBL_TEAM_ID' => 'チームID',
  'LBL_ASSIGNED_TO_ID' => 'アサイン先ID',
  'LBL_ASSIGNED_TO_NAME' => 'アサイン先',
  'LBL_TAGS_LINK' => 'タグ',
  'LBL_TAGS' => 'タグ',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '作成日',
  'LBL_DATE_MODIFIED' => '更新日',
  'LBL_MODIFIED' => '更新者',
  'LBL_MODIFIED_ID' => '更新者ID',
  'LBL_MODIFIED_NAME' => '更新者',
  'LBL_CREATED' => '作成者',
  'LBL_CREATED_ID' => '作成者ID',
  'LBL_DOC_OWNER' => 'ドキュメントの所有者',
  'LBL_USER_FAVORITES' => 'お気に入りのユーザー',
  'LBL_DESCRIPTION' => '詳細',
  'LBL_DELETED' => '削除済み',
  'LBL_NAME' => '名前',
  'LBL_CREATED_USER' => '作成者',
  'LBL_MODIFIED_USER' => '更新者',
  'LBL_LIST_NAME' => '名前',
  'LBL_EDIT_BUTTON' => '編集',
  'LBL_REMOVE' => '削除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '更新者',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Species Census 一覧',
  'LBL_MODULE_NAME' => 'Species Census',
  'LBL_MODULE_TITLE' => 'Species Census',
  'LBL_MODULE_NAME_SINGULAR' => 'Species Census',
  'LBL_HOMEPAGE_TITLE' => '私の Species Census',
  'LNK_NEW_RECORD' => '作成 Species Census',
  'LNK_LIST' => '画面 Species Census',
  'LNK_IMPORT_SC_SPECIES_CENSUS' => 'Import Species Census',
  'LBL_SEARCH_FORM_TITLE' => '検索 Species Census',
  'LBL_HISTORY_SUBPANEL_TITLE' => '履歴',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'アクティビティストリーム',
  'LBL_SC_SPECIES_CENSUS_SUBPANEL_TITLE' => 'Species Census',
  'LBL_NEW_FORM_TITLE' => '新規 Species Census',
  'LNK_IMPORT_VCARD' => 'Import Species Census vCard',
  'LBL_IMPORT' => 'Import Species Census',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Species Census record by importing a vCard from your file system.',
  'LBL_SC_SPECIES_CENSUS_FOCUS_DRAWER_DASHBOARD' => 'Species Census Focus Drawer',
  'LBL_DATE_2' => 'Date',
  'LBL_CENSUS' => 'Census',
  'LBL_ALLOCATED' => 'Allocated',
  'LBL_ON_STUDY' => 'On Study',
  'LBL_STOCK' => 'Stock',
  'LBL_TOTAL' => 'Total',
);