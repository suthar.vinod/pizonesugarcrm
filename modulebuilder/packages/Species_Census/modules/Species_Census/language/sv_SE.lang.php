<?php
// created: 2022-12-13 04:46:26
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Lag-ID',
  'LBL_ASSIGNED_TO_ID' => 'Tilldelat användar-ID',
  'LBL_ASSIGNED_TO_NAME' => 'Tilldelad användare',
  'LBL_TAGS_LINK' => 'Taggar',
  'LBL_TAGS' => 'Taggar',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum skapat',
  'LBL_DATE_MODIFIED' => 'Modifierat Datum',
  'LBL_MODIFIED' => 'Modifierat Av',
  'LBL_MODIFIED_ID' => 'Modifierat Av Id',
  'LBL_MODIFIED_NAME' => 'Modifierat Av Namn',
  'LBL_CREATED' => 'Skapat Av',
  'LBL_CREATED_ID' => 'Skapat Av',
  'LBL_DOC_OWNER' => 'Ägare av dokument',
  'LBL_USER_FAVORITES' => 'Användare som favorite',
  'LBL_DESCRIPTION' => 'Beskrivning',
  'LBL_DELETED' => 'Raderad',
  'LBL_NAME' => 'Namn',
  'LBL_CREATED_USER' => 'Skapat Av Användare',
  'LBL_MODIFIED_USER' => 'Modifierat Av Användare',
  'LBL_LIST_NAME' => 'Namn',
  'LBL_EDIT_BUTTON' => 'Redigera',
  'LBL_REMOVE' => 'Ta bort',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ändrad av namn',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Species Census List',
  'LBL_MODULE_NAME' => 'Species Census',
  'LBL_MODULE_TITLE' => 'Species Census',
  'LBL_MODULE_NAME_SINGULAR' => 'Species Census',
  'LBL_HOMEPAGE_TITLE' => 'Min Species Census',
  'LNK_NEW_RECORD' => 'Create Species Census',
  'LNK_LIST' => 'Visa Species Census',
  'LNK_IMPORT_SC_SPECIES_CENSUS' => 'Import Species Census',
  'LBL_SEARCH_FORM_TITLE' => 'Search Species Census',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetsström',
  'LBL_SC_SPECIES_CENSUS_SUBPANEL_TITLE' => 'Species Census',
  'LBL_NEW_FORM_TITLE' => 'Ny Species Census',
  'LNK_IMPORT_VCARD' => 'Import Species Census vCard',
  'LBL_IMPORT' => 'Import Species Census',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Species Census record by importing a vCard from your file system.',
  'LBL_SC_SPECIES_CENSUS_FOCUS_DRAWER_DASHBOARD' => 'Species Census Focus Drawer',
  'LBL_DATE_2' => 'Date',
  'LBL_CENSUS' => 'Census',
  'LBL_ALLOCATED' => 'Allocated',
  'LBL_ON_STUDY' => 'On Study',
  'LBL_STOCK' => 'Stock',
  'LBL_TOTAL' => 'Total',
);