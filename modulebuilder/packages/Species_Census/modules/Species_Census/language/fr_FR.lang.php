<?php
// created: 2022-12-13 04:46:25
$mod_strings = array (
  'LBL_TEAM' => 'Équipes',
  'LBL_TEAMS' => 'Équipes',
  'LBL_TEAM_ID' => 'Équipe (ID)',
  'LBL_ASSIGNED_TO_ID' => 'ID utilisateur assigné',
  'LBL_ASSIGNED_TO_NAME' => 'Assigné à',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date de création',
  'LBL_DATE_MODIFIED' => 'Date de modification',
  'LBL_MODIFIED' => 'Modifié par',
  'LBL_MODIFIED_ID' => 'Modifié par (ID)',
  'LBL_MODIFIED_NAME' => 'Modifié par Nom',
  'LBL_CREATED' => 'Créé par',
  'LBL_CREATED_ID' => 'Créé par (ID)',
  'LBL_DOC_OWNER' => 'Propriétaire du document',
  'LBL_USER_FAVORITES' => 'Favoris pour les utilisateurs suivants',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Supprimé',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Créé par',
  'LBL_MODIFIED_USER' => 'Modifié par',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Éditer',
  'LBL_REMOVE' => 'Supprimer',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifié par Nom',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Species Census Catalogue',
  'LBL_MODULE_NAME' => 'Species Census',
  'LBL_MODULE_TITLE' => 'Species Census',
  'LBL_MODULE_NAME_SINGULAR' => 'Species Census',
  'LBL_HOMEPAGE_TITLE' => 'Mes Species Census',
  'LNK_NEW_RECORD' => 'Créer Species Census',
  'LNK_LIST' => 'Afficher Species Census',
  'LNK_IMPORT_SC_SPECIES_CENSUS' => 'Import Species Census',
  'LBL_SEARCH_FORM_TITLE' => 'Recherche Species Census',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historique',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activités',
  'LBL_SC_SPECIES_CENSUS_SUBPANEL_TITLE' => 'Species Census',
  'LBL_NEW_FORM_TITLE' => 'Nouveau Species Census',
  'LNK_IMPORT_VCARD' => 'Import Species Census vCard',
  'LBL_IMPORT' => 'Import Species Census',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Species Census record by importing a vCard from your file system.',
  'LBL_SC_SPECIES_CENSUS_FOCUS_DRAWER_DASHBOARD' => 'Species Census Focus Drawer',
  'LBL_DATE_2' => 'Date',
  'LBL_CENSUS' => 'Census',
  'LBL_ALLOCATED' => 'Allocated',
  'LBL_ON_STUDY' => 'On Study',
  'LBL_STOCK' => 'Stock',
  'LBL_TOTAL' => 'Total',
);