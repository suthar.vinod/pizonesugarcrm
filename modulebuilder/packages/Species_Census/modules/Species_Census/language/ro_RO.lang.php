<?php
// created: 2022-12-13 04:46:26
$mod_strings = array (
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Id Echipă',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuit lui',
  'LBL_TAGS_LINK' => 'Etichete',
  'LBL_TAGS' => 'Etichete',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data creării',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificat după Id',
  'LBL_MODIFIED_NAME' => 'Modificat după Nume',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creat de Id',
  'LBL_DOC_OWNER' => 'Proprietar document',
  'LBL_USER_FAVORITES' => 'Utilizatori cu setări favorite',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_DELETED' => 'Şters',
  'LBL_NAME' => 'Nume',
  'LBL_CREATED_USER' => 'Creat de Utilizator',
  'LBL_MODIFIED_USER' => 'Modificat după Utilizator',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_REMOVE' => 'Eliminare',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat după Nume',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Species Census Lista',
  'LBL_MODULE_NAME' => 'Species Census',
  'LBL_MODULE_TITLE' => 'Species Census',
  'LBL_MODULE_NAME_SINGULAR' => 'Species Census',
  'LBL_HOMEPAGE_TITLE' => 'Al meu Species Census',
  'LNK_NEW_RECORD' => 'Creează Species Census',
  'LNK_LIST' => 'Vizualizare Species Census',
  'LNK_IMPORT_SC_SPECIES_CENSUS' => 'Import Species Census',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta Species Census',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_SC_SPECIES_CENSUS_SUBPANEL_TITLE' => 'Species Census',
  'LBL_NEW_FORM_TITLE' => 'Nou Species Census',
  'LNK_IMPORT_VCARD' => 'Import Species Census vCard',
  'LBL_IMPORT' => 'Import Species Census',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Species Census record by importing a vCard from your file system.',
  'LBL_SC_SPECIES_CENSUS_FOCUS_DRAWER_DASHBOARD' => 'Species Census Focus Drawer',
  'LBL_DATE_2' => 'Date',
  'LBL_CENSUS' => 'Census',
  'LBL_ALLOCATED' => 'Allocated',
  'LBL_ON_STUDY' => 'On Study',
  'LBL_STOCK' => 'Stock',
  'LBL_TOTAL' => 'Total',
);