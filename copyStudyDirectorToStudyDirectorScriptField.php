<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$counter = 1;
global $db;
$msg = [];
$result = $db->query("select id_c, m03_work_product.name , study_director_c from m03_work_product_cstm inner join m03_work_product on m03_work_product.id = m03_work_product_cstm.id_c where m03_work_product.deleted = 0");
if ($result) {
    while ($row = $db->fetchByAssoc($result)) {
        $id = $row['id_c'];
        $name = $row['name'];
        $study_dir_key = $row['study_director_c'];
        $study_dir_value = $GLOBALS['app_list_strings']['study_director_list'][$study_dir_key];
   
        if ($study_dir_key) {
            if($study_dir_value == "Megan O'Brien"){
                $query = "select id from contacts where concat(first_name,' ',last_name) like '%Megan%Brien' and deleted = 0";
            } else if($study_dir_value == "Liz Roby") {
                $query = "select id from contacts where concat(first_name,' ',last_name) = 'Liz Clark' and deleted = 0";
            } else if($study_dir_value == "Thomas van Valkenburg") {
                $query = "select id from contacts where concat(first_name,' ',last_name) = 'Thomas Van Valkenburg' and deleted = 0";
            } else if($study_dir_value == "Melissa Carlson") {
                $query = "select id from contacts where concat(first_name,' ',last_name) = 'Melissa Bruns' and deleted = 0";
            } else if($study_dir_value == "Rhonda Swenson") {
                $query = "select id from contacts where concat(first_name,' ',last_name) = 'Ronda Swenson' and deleted = 0";
            } 
            else {
                $query = "select id from contacts where concat(first_name,' ',last_name) = '" . $study_dir_value. "' and deleted = 0";
            }
            $result2 = $db->query($query);
            if ($row2 = $db->fetchByAssoc($result2)) {
                $query2 = "update m03_work_product_cstm set contact_id_c = '".$row2['id']."' where id_c = '".$id."'";
                if(!$db->query($query2)) {
                    $message1 = "Script Failed";
                    echo "<script type='text/javascript'>alert('$message1');</script>";
                }
            } else {
                $msg[] = "Fail to populate the contact '" . $study_dir_value . "' for Work Product Id : " . $id . " and Work Product name : " . $name . "<br>";
            }
        }
    }
    print_r($msg);
    $message2 = "Script executed successfully";
    echo "<script type='text/javascript'>alert('$message2');</script>";
} else {
    $message3 = "Table not found";
    echo "<script type='text/javascript'>alert('$message3');</script>";
}

