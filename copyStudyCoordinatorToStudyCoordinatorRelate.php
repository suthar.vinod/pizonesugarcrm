<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $db;

$result = $db->query("SELECT m03_work_product_cstm.id_c, m03_work_product_cstm.study_coordinator_c FROM m03_work_product_cstm INNER JOIN m03_work_product ON m03_work_product.id = m03_work_product_cstm.id_c WHERE m03_work_product.deleted = 0");
if ($result) {
    while ($row = $db->fetchByAssoc($result)) {
        $id = $row['id_c'];
        $study_coordinator_key = $row['study_coordinator_c'];
        $study_coordinator_value = $GLOBALS['app_list_strings']['study_coordinator_list'][$study_coordinator_key];
   
        if ($study_coordinator_key) {
            if($study_coordinator_value == "Tom Van Valkenburg"){
                $query = "select id from contacts where concat(first_name,' ',last_name) = 'Thomas Van Valkenburg' and deleted = 0";
            }else if($study_coordinator_value == "Erica VanReeth"){
                $query = "select id from contacts where concat(first_name,' ',last_name) = 'Erica Van Reeth' and deleted = 0";
            } else {
                $query = "select id from contacts where concat(first_name,' ',last_name) = '" . $study_coordinator_value. "' and deleted = 0";
            }
            $result2 = $db->query($query);
            if ($row2 = $db->fetchByAssoc($result2)) {
                $query2 = "update m03_work_product_cstm set contact_id2_c = '".$row2['id']."' where id_c = '".$id."'";
                if(!$db->query($query2)) {
                    echo "Updation Failed";
                }
            } else {
                $msg[] = "Fail to populate the contact '" . $study_coordinator_value . "' for Work Product Id : '".$id ."' <br>";
            }
        }
    }
    print_r($msg);
    echo "Script executed successfully";
} else {
    echo "Table not found";
}

