(function appFUnc(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseII_Inventory_ItemPanelTopView &&
            !app.view.views.BaseII_Inventory_ItemCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "II_Inventory_Item", undefined, false, "base");
        }

        var iiPanelTop = "BaseII_Inventory_ItemPanelTopView";

        if (app.view.views.BaseII_Inventory_ItemCustomPanelTopView) {
            iiPanelTop = "BaseII_Inventory_ItemCustomPanelTopView";
        }

        if (App.view.views[iiPanelTop].iiRmoveCreateButton === true) {
            return;
        }

        App.view.views[iiPanelTop] = App.view.views[iiPanelTop].extend({
            iiRmoveCreateButton: true,

            render: function() {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "POI_Purchase_Order_Item" || this.parentModule === "ORI_Order_Request_Item") {
                    this._removeIISubpanelActionButtons();
                }       

                return initializeOptions;
            },

            _removeIISubpanelActionButtons: function () {
                if (this.$el.find("[name=create_button]").length > 0 ){
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });
})(SUGAR.App);