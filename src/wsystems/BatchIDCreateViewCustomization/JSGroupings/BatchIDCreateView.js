var offsetBID = 0;
var offsetBIDCon = 0;
var appendQueryBID = "&order_by=name%3Aasc";
(function appFunct(app) {
    var self = this;
    //fetching next 20 records after click on show more Work Product
    $(document).on('click', 'button[data-action="show-more-wpCon"]', function () {
        var wpNameSearch = $('.wp-selection input.search-name').val();
        // console.log("More btn log", wpNameSearch);
        // console.log("<=====show more Clicked=====>");
        $('button[data-action="show-more-wpCon"]').hide();
        $('div.loading-ori').show();
       
        offsetBID += 20;
        var url = app.api.buildURL("getWPSidecarData");
        var method = "create";
        var data = {
            module: 'M03_Work_Product',
            offsetBID: offsetBID,
            wpNameSearch: wpNameSearch
        };

        var callback = {
            success: _.bind(function successCB(wpCon) {
                // console.log("wpCon after getdata======>", wpCon);
                // console.log('API response', wpCon);
                
                $('div.loading-ori').remove();
                // $(".show-more-wpCon-main").remove();
                $("div.show-more-wpCon").remove();
                $("div.show-more-wpCon").hide();
                $('button[data-action="show-more-wpCon"]').remove();
                
                var wpConitems_ids1 = [];
                if (wpCon.records && wpCon.records.length > 0) {
                    wpConitems_ids1 = wpCon.records;
                }
                wpConView.collection.add(wpConitems_ids1);
                wpConView.render();
                $('div.main-pane.wp-selection .dataTable td:first-child>span').css("border-top", "1px solid #ebedef");
                $('div.main-pane.wp-selection .dataTable td:first-child>span').css("padding", "0px 4px 0px 0px");
                $('div.main-pane.wp-selection .dataTable td:first-child>span').css("margin", "-14px 0px 0px 12px");
                $('div.main-pane.wp-selection .dataTable td:first-child>span').css("border-right", "1px solid #ebedef");
                setTimeout(function () {
                    if ($('button[data-action="show-more-wpCon"]').length == 0) {
                        if (wpCon.records.length > 19) {

                            $("div.main-pane.wp-selection div.main-content").append(`
                            <div class="show-more-wpCon-main">
                                <div class="show-more-wpCon">
                                    <button data-action="show-more-wpCon" class="btn btn-link btn-invisible more padded">More Work Products...</button>
                                </div>
                                <div class="block-footer loading-ori hide">
                                    <div class="loading">
                                        Loading...
                                    </div>
                                </div>
                            </div>
                        `);
                        }
                        }
                }, 500);
            }, this)
        };
        app.api.call(method, url, data, callback);
    });

    //Filter for WP Name Search
    $(document).on("keyup", ".wp-selection input.search-name", function () {
        wpNameSearch = $('.wp-selection input.search-name').val();
        offsetBID = 0;
        // console.log("searching wpNameSearch====>", wpNameSearch);
        $('button[data-action="show-more-wpCon"]').remove();
        $(".wp-selection .table").html("");
        $("div.loading-ori").remove();
        // $(".show-more-wpCon-main").remove();
        $("div.show-more-wpCon").remove();
        $("div.show-more-wpCon").hide();
        wpView.collection.reset();
        wpView.render();
        var url = app.api.buildURL("getWPSidecarData");
        var method = "create";
        var data = {
            module: 'M03_Work_Product',
            offsetBID: 0,
            wpNameSearch: wpNameSearch
        };

        var callback = {
            success: _.bind(function successCB(wpData) {
            // console.log('wpData',wpData);
            
                $('div.loading-ori').remove();
                // $(".show-more-wpCon-main").remove();
                $("div.show-more-wpCon").remove();
                $("div.show-more-wpCon").hide();
                $('button[data-action="show-more-wpCon"]').remove();

                 
                wpDataModels = [];
                // var selectediiwpDataViewModels = wpDataView.massCollection;
                if (wpData.records && wpData.records.length > 0) {
                    wpDataModels = wpData.records;
                    wpConView.collection.add(wpDataModels);
                    wpConView.render();
                    $('div.main-pane.wp-selection .dataTable td:first-child>span').css("border-top", "1px solid #ebedef");
                            $('div.main-pane.wp-selection .dataTable td:first-child>span').css("padding", "0px 4px 0px 0px");
                            $('div.main-pane.wp-selection .dataTable td:first-child>span').css("margin", "-14px 0px 0px 12px");
                            $('div.main-pane.wp-selection .dataTable td:first-child>span').css("border-right", "1px solid #ebedef");
                    setTimeout(function () {
                        if ($('button[data-action="show-more-wpCon"]').length == 0) {
                            // console.log("length==>",wpData.records.length);
                            if (wpData.records.length > 19) {
                                $('button[data-action="show-more-wpCon"]').hide();
                                $("div.main-pane.wp-selection div.main-content").append(`
                                <div class="show-more-wpCon-main">
                                <div class="show-more-wpCon">
                                    <button data-action="show-more-wpCon" class="btn btn-link btn-invisible more padded">More Work Products...</button>
                                </div>
                                <div class="block-footer loading-ori hide">
                                    <div class="loading">
                                        Loading...
                                    </div>
                                </div>
                            </div>
                                    `);
                            }
                        }
                    }, 500);
                }
                $(".wp-selection table th").removeClass("orderByname");
                // $(".wp_selection table th").removeClass("orderByusda_id_c");
                $(".wp-selection table th.sorting").css("cursor", "unset");
                $('.wp-selection input[name="check"]').prop('checked', false);
            }, this)
        };
        app.api.call(method, url, data, callback);
        $('.wp-selection input[name="check"]').prop('checked', false);
        //}      
    });

    //Reload default Sidecar when clicking on x btn
    $(document).on("click", ".sicon.sicon-close.add-on", function () {
        offsetBID = 0;
        var wpNameSearch = $('.wp-selection input.search-name').val();
        $(".wp-selection").css('width', "100%");
        $(".wp-selection .table").html('');
        $('div.loading-ori').remove();
        $("div.show-more-wpCon").hide();
        // $(".show-more-wpCon-main").remove();
        $("div.show-more-wpCon").remove();
        $('button[data-action="show-more-wpCon"]').remove();
        wpView.collection.reset();
        wpView.render();
        var url = app.api.buildURL("getWPSidecarData");
        var method = "create";
        var data = {
            module: 'M03_Work_Product',
            offsetBID: offsetBID,
            wpNameSearch: wpNameSearch
        };

        var callback = {
            success: _.bind(function successCB(wpCon) {
                $('button[data-action="show-more-wpCon"]').remove();
                $("loading-ori").remove();
                // $(".show-more-wpCon-main").remove();
                $("div.show-more-wpCon").remove();
                $("div.show-more-wpCon").hide();
                // console.log("wpCon after getdata======>", wpCon);
                // console.log('API response', wpCon);
                // wpConLayout = self.layout.layout.getComponent("dashboard-pane").getComponent("wp-selection");
                // wpConView = wpConLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("wp-selection");
                var wpConModels = [];
                var selectediiwpConViewModels = wpConView.massCollection;
                if (wpCon.records && wpCon.records.length > 0) {
                    wpConModels = wpCon.records;
                }
                // wpConView.collection.reset();
                wpConView.collection.add(wpConModels);
                wpConView.render();
                
                $('div.main-pane.wp-selection div.flex-list-view-content').css("overflow", "visible");
                // this.layout.layout.$el.find(".wp-selection table th").removeClass("orderByname");
                
                // this.layout.layout.$el.find(".wp-selection table th").removeClass("orderByemail");
                var wpConitems_ids1 = [];
                wpConitems_ids1 = _.map(selectediiwpConViewModels.models, function (model) {
                    return model.set("id", "");
                });
                $('div.main-pane.wp-selection .dataTable td:first-child>span').css("border-top", "1px solid #ebedef");
                $('div.main-pane.wp-selection .dataTable td:first-child>span').css("padding", "0px 4px 0px 0px");
                $('div.main-pane.wp-selection .dataTable td:first-child>span').css("margin", "-14px 0px 0px 12px");
                $('div.main-pane.wp-selection .dataTable td:first-child>span').css("border-right", "1px solid #ebedef");
                setTimeout(function () {
                    if ($('button[data-action="show-more-wpCon"]').length == 0) {
                        if (wpCon.records.length > 19) {
                            $("div.main-pane.wp-selection div.main-content").append(`
                            <div class="show-more-wpCon-main">
                                <div class="show-more-wpCon">
                                    <button data-action="show-more-wpCon" class="btn btn-link btn-invisible more padded">More Work Products...</button>
                                </div>
                                <div class="block-footer loading-ori hide">
                                    <div class="loading">
                                        Loading...
                                    </div>
                                </div>
                            </div>
                            `);
                        }
                    }
                }, 500);
                
                $(".wp-selection table th").removeClass("orderByname");
                $(".wp-selection table th.sorting").css("cursor", "unset");
                $('.wp-selection input[name="check"]').prop('checked', false);
            }, this)
        };
        app.api.call(method, url, data, callback);
        $('.wp-selection input[name="check"]').prop('checked', false);
        //}      
    });


    app.events.on("app:sync:complete", function appSyncComplete() {
        if (!app.view.views.BaseBID_Batch_IDCreateView && !app.view.views.BaseBID_Batch_IDCustomCreateView) {
            app.view.declareComponent("view", "create", "BID_Batch_ID", undefined, false, "base");
        }
        var createView = "BaseBID_Batch_IDCreateView";
        if (app.view.views.BaseBID_Batch_IDCustomCreateView) {
            createView = "BaseBID_Batch_IDCustomCreateView";
        }
        if (App.view.views[createView].createExtended === true) {
            return;
        }

        App.view.views[createView] = App.view.views[createView].extend({
            createExtended: true,
            initialize: function () {
                this._super("initialize", arguments);
                this.on('render', _.bind(this.onload_function, this));
                this.model.on("change:status_c", this._hideFilter, this);
                this.context.on('button:cancel_button:click', this.cancel_button, this);
            },
            
            cancel_button:function(){
                $('div.show-more-wpCon').remove();
                $('div.loading-ori').remove();
                $("div.main-pane.wp-selection div.main-content").remove();
            },

            _hideFilter: function () {
                $('.wp-selection').find('.table-cell.full-width').css('width', '426px');
                $('.wp-selection').find('.table-cell .search-filter').css('display', 'none');
                $('.wp-selection').find('.table-cell .choice-filter').css('display', 'none');
            },

            onload_function: function () {
                var wpNameSearch = $('.wp-selection input.search-name').val();
                offsetBID = 0;
                var self = this;
                if ($(".drawer.active").find(".sidebar-content").hasClass("side-collapsed") === true) {
                    this.layout.layout.toggleSidePane();
                }
                // $(".show-more-wpCon-main").remove();
                $("div.show-more-wpCon").remove();
                $("div.show-more-wpCon").hide();

                // for Work Product Sidecar 
                wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("wp-selection");
                wpView = wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("wp-selection");
                wpView.collection.reset();
                wpView.render();

                // this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").show();
                this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span14").show();

                //using custom API to fix the empty sidecar issue for the normal user
                var url = app.api.buildURL("getWPSidecarData");
                    var method = "create";
                    var data = {
                        module: 'M03_Work_Product',
                        offsetBID: offsetBID,
                        wpNameSearch: wpNameSearch
                    };

                    var callback = {
                        success: _.bind(function successCB(wpCon) {
                            // console.log("wpCon after getdata======>", wpCon);
                            // console.log('API response', wpCon);
                            wpConLayout = self.layout.layout.getComponent("dashboard-pane").getComponent("wp-selection");
                            wpConView = wpConLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("wp-selection");
                            var wpConModels = [];
                            var selectediiwpConViewModels = wpConView.massCollection;
                            if (wpCon.records && wpCon.records.length > 0) {
                                wpConModels = wpCon.records;
                            }
                            wpConView.collection.reset();
                            wpConView.collection.add(wpConModels);
                            wpConView.render();
                            
                            $('div.main-pane.wp-selection div.flex-list-view-content').css("overflow", "visible");
                            this.layout.layout.$el.find(".wp-selection table th").removeClass("orderByname");
                            
                            // this.layout.layout.$el.find(".wp-selection table th").removeClass("orderByemail");
                            var wpConitems_ids1 = [];
                            wpConitems_ids1 = _.map(selectediiwpConViewModels.models, function (model) {
                                return model.set("id", "");
                            });
                            $('div.main-pane.wp-selection .dataTable td:first-child>span').css("border-top", "1px solid #ebedef");
                            $('div.main-pane.wp-selection .dataTable td:first-child>span').css("padding", "0px 4px 0px 0px");
                            $('div.main-pane.wp-selection .dataTable td:first-child>span').css("margin", "-14px 0px 0px 12px");
                            $('div.main-pane.wp-selection .dataTable td:first-child>span').css("border-right", "1px solid #ebedef");
                             
                            setTimeout(function () {
                                if ($('button[data-action="show-more-wpCon"]').length == 0) {
                                    if (wpCon.records.length > 19) {
                                        $("div.main-pane.wp-selection div.main-content").append(`
                                        <div class="show-more-wpCon-main">
                                            <div class="show-more-wpCon">
                                                <button data-action="show-more-wpCon" class="btn btn-link btn-invisible more padded">More Work Products...</button>
                                            </div>
                                            <div class="block-footer loading-ori hide">
                                                <div class="loading">
                                                    Loading...
                                                </div>
                                            </div>
                                        </div>
                                      `);
                                    }
                                }
                            }, 500);
                        }, this)
                    };
                    app.api.call(method, url, data, callback);

            },
            /*
            For linking the Work Product record in Batch Id
            Ticket 2268: Added By Harshit Shreshthi
            */
            saveAndClose: function () {
                var self = this;
                setTimeout(function () {
                    self.initiateSave(_.bind(function () {
                        if (self.closestComponent("drawer")) {
                            self._workProductlinking().then(function thenFunc(result) {
                                if (result === "success") {
                                    // console.log("Linking Done");
                                } else {
                                    // app.alert.show("wp_linking_error", {
                                    //     level: "error",
                                    //     messages: "Please select atleast one Work Product..."
                                    // });
                                    // return false;
                                }
                                //app.drawer.close(this.context, this.model);
                            }.bind(self));
                            app.drawer.close(self.context, self.model);
                        }
                    }, self));
                }, 700);
            },

            _workProductlinking: function () {
                return new Promise(function promiseFunc(resolve, reject) {
                    var modelId = this.model.get("id");
                    var scheduleDate = this.model.get("scheduled_date_c");
                    // console.log("scheduleDate===>",scheduleDate);
                    //Other sidecar comes here(deliverable Template Sidecar)
                    var ii_wpConLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("wp-selection");
                    var ii_wpConView = ii_wpConLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("wp-selection");
                    var selectedii_wpCon = ii_wpConView.massCollection;
                    if (selectedii_wpCon.length > 0 || selectedii_wpCon > 0) {
                        var wpCon_items_ids = [];
                        wpCon_items_ids = _.map(selectedii_wpCon.models, function (model) {
                            return model.get("id");
                        });

                        /*
                        Rollback this feature from the ticket @2268
                        API for check the selected first procedure field in WP are Same or not
                        var url = app.api.buildURL("checkFirstProcedureSameInWP");
                        var method = "create";
                        var data = {
                            module: 'M03_Work_Product',
                            WPIds: wpCon_items_ids
                        };
                        var callback = {
                            success: _.bind(function successCB(wpCon) {
                                if (wpCon.Data !== '1') {
                                    setTimeout(() => {
                                        app.alert.show("wpCon_linking_warning", {
                                            level: "warning",
                                            messages: "First Procedure on selected Work Products varies.Please review and edit First Procedures to the same date.",
                                            autoClose: false
                                        });
                                        resolve("success");
                                    }, 500);

                                }

                            }, this)
                        };
                        app.api.call(method, url, data, callback);

                        */
                        if (selectedii_wpCon.length > 0) {
                            setTimeout(() => {
                                app.alert.show("wpCon_linking_info", {
                                    level: "info",
                                    messages: "Linking Work Product...",
                                    autoClose: true
                                });
                                resolve("success");
                            }, 500);
                        }
                        app.api.call("create", "rest/v10/BID_Batch_ID/" + modelId + "/link", {
                            link_name: "bid_batch_id_m03_work_product_1",
                            ids: wpCon_items_ids
                        }, {
                            success: function () {
                                // console.log("after linking schedule date");
                                app.alert.dismiss('wpCon_linking_info');
                                resolve("success");
                                 //Added By: Harshit Shreshthi for Ticket @2377
                                // call custom API to set Scheduled date to the WP First Procedure field
                                var urlScheduleDate = app.api.buildURL("insertSheduledDateToFirstProcedureInWPD");
                                var methodScheduleDate = "create";
                                var dataScheduleDate = {
                                    module: 'M03_Work_Product',
                                    WPIds: wpCon_items_ids,
                                    scheduleDate: scheduleDate
                                };
                                var callbackScheduleDate = {
                                    success: _.bind(function successCB(res) {
                                        // console.log("API Run!",res);
                                    }, this)
                                };
                                app.api.call(methodScheduleDate, urlScheduleDate, dataScheduleDate, callbackScheduleDate);
                            },
                            error: function (error) {
                                resolve(error);
                            }
                        });
                    } else {
                        resolve("success");
                    }

                }.bind(this));

            },
        });
    });
})(SUGAR.App);