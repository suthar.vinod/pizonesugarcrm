(function appFUnc(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseIM_Inventory_ManagementPanelTopView &&
            !app.view.views.BaseIM_Inventory_ManagementCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "IM_Inventory_Management", undefined, false, "base");
        }

        var imPanelTop = "BaseIM_Inventory_ManagementPanelTopView";

        if (app.view.views.BaseIM_Inventory_ManagementCustomPanelTopView) {
            imPanelTop = "BaseIM_Inventory_ManagementCustomPanelTopView";
        }

        if (App.view.views[imPanelTop].removeCreateButton === true) {
            return;
        }

        App.view.views[imPanelTop] = App.view.views[imPanelTop].extend({
            removeCreateButton: true,

            render: function() {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "II_Inventory_Item") {
                    this._removeActionButtons();
                }       

                return initializeOptions;
            },

            _removeActionButtons: function () {
                if (this.$("[name=create_button]").length > 0 ){
                    this.$("[name=create_button]").remove();
                }
            }
        });
    });
})(SUGAR.App);
