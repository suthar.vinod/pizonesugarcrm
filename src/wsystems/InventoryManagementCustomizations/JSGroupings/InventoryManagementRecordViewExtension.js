/**
 * Extend the record view in order to populat the unique id based on the Unique Id Type
 */
//  popupAlert = 0;
//  popupAlert2 = 0;
//  popupAlert3 = 0;
popupAlert = 1;
    $(document).on('click', '.closeCustomIconRV', function () {
        selfIMRecord.saveInventoryRecordView();
    });
(function (app) {  

    app.events.on("app:sync:complete", function () {
        if (!app.view.views.BaseIM_Inventory_ManagementRecordView && !app.view.views.BaseIM_Inventory_ManagementCustomRecordView) {
            app.view.declareComponent("view", "record", "IM_Inventory_Management", undefined, false, "base");
        }

        var recordView = "BaseIM_Inventory_ManagementRecordView";

        if (app.view.views.BaseIM_Inventory_ManagementCustomRecordView) {
            recordView = "BaseIM_Inventory_ManagementCustomRecordView";
        }

        if (App.view.views[recordView].recordExtended === true) {
            return;
        }

        App.view.views[recordView] = App.view.views[recordView].extend({
            recordExtended: true,

            initialize: function (options) {
                this._super("initialize", arguments);
                this._bindEvents();
            },

            /**
             * Register events
             */
            _bindEvents: function () {
                this.model.once("sync",function () {this.model.on("change:reviewed_record_c",this.functionreviewedrecord,this);},this);
                this.model.once("sync",function () {this.model.on("change:processed_per_protocol_c",this.functionprocessedperprotocol,this);},this);
                this.model.on('data:sync:complete', this.onload_function, this);
            },
            functionreviewedrecord: function() {
                if (this.model.get("reviewed_record_c") == "No") { 
                    popupAlert = 0;   
                    console.log('functionreviewedrecord ', popupAlert); 
                }  else {
                    popupAlert = 1;
                }         
            },
            functionprocessedperprotocol: function() {
                if (this.model.get("processed_per_protocol_c") == "No") { 
                    popupAlert = 0;   
                    console.log('functionreviewedrecord ', popupAlert); 
                }  else {
                    popupAlert = 1;
                } 
            },
            onload_function: function() {                
                popupAlert = 1;                    
                this._super("render");
                var related_to = this.model.get('related_to');
                if (related_to == 'Sales') {
                    $('[data-name="m01_sales_im_inventory_management_1_name"]').css("visibility", "visible"); 
                    $('[data-name="m01_sales_im_inventory_management_1_name"]').css("height", "auto"); 

                } else {
                    $('[data-name="m01_sales_im_inventory_management_1_name"]').css("visibility", "hidden"); 
                    $('[data-name="m01_sales_im_inventory_management_1_name"]').css("height", "0"); 
                }
                if (related_to == 'Work Product') {
                    $('[data-name="m03_work_product_im_inventory_management_1_name"]').css("visibility", "visible"); 
                    $('[data-name="m03_work_product_im_inventory_management_1_name"]').css("height", "auto"); 

                } else {
                    $('[data-name="m03_work_product_im_inventory_management_1_name"]').css("visibility", "hidden"); 
                    $('[data-name="m03_work_product_im_inventory_management_1_name"]').css("height", "0"); 
                }
               
                var category = this.model.get('category');                
                if (category != 'Specimen') {
                    $('[data-name="type_2"]').css("visibility", "visible"); 
                    $('[data-name="type_2"]').css("height", "auto"); 

                } else {
                    $('[data-name="type_2"]').css("visibility", "hidden"); 
                    $('[data-name="type_2"]').css("height", "0"); 
                }
                if (category == 'Specimen') {
                    $('[data-name="type_specimen_c"]').css("visibility", "visible"); 
                    $('[data-name="type_specimen_c"]').css("height", "auto"); 

                } else {
                    $('[data-name="type_specimen_c"]').css("visibility", "hidden"); 
                    $('[data-name="type_specimen_c"]').css("height", "0"); 
                }
               
                var type_specimen = this.model.get('type_specimen_c');                
                if (_.contains(type_specimen, "Tissue") || _.contains(this.model.get('type_specimen_c'), "Tissue")) {
                    $('[data-name="ending_storage_media"]').css("visibility", "visible"); 
                    $('[data-name="ending_storage_media"]').css("height", "auto"); 
                } else {
                    $('[data-name="ending_storage_media"]').css("visibility", "hidden"); 
                    $('[data-name="ending_storage_media"]').css("height", "0"); 
                }
                
                var action = this.model.get('action');
                if (action == 'Archive Offsite' || action == 'External Transfer') {
                    $('[data-name="external_receipt_date"]').css("visibility", "visible"); 
                    $('[data-name="ship_to_contact"]').css("visibility", "visible");
                    $('[data-name="ship_to_address"]').css("visibility", "visible");
                    $('[data-name="ship_to_company"]').css("visibility", "visible");

                    $('[data-name="external_receipt_date"]').css("height", "auto");
                    $('[data-name="ship_to_contact"]').css("height", "auto");
                    $('[data-name="ship_to_address"]').css("height", "auto");
                    $('[data-name="ship_to_company"]').css("height", "auto");
                } else {
                    $('[data-name="external_receipt_date"]').css("visibility", "hidden");
                    $('[data-name="ship_to_contact"]').css("visibility", "hidden"); 
                    $('[data-name="ship_to_address"]').css("visibility", "hidden"); 
                    $('[data-name="ship_to_company"]').css("visibility", "hidden"); 
                    
                    $('[data-name="external_receipt_date"]').css("height", "0");
                    $('[data-name="ship_to_contact"]').css("height", "0");
                    $('[data-name="ship_to_address"]').css("height", "0");
                    $('[data-name="ship_to_company"]').css("height", "0");
                }
                if (action == 'Archive Onsite' || action == 'Separate Inventory Items') {
                    $('[data-name="location_building"]').css("visibility", "visible"); 
                    $('[data-name="location_building"]').css("height", "auto");
                } else {
                    $('[data-name="location_building"]').css("visibility", "hidden");            
                    $('[data-name="location_building"]').css("height", "0");
                }
                if (action == 'Processing' || action == 'Internal Transfer') {
                    $('[data-name="start_datetime"]').css("visibility", "visible"); 
                    $('[data-name="start_datetime"]').css("height", "auto");
                    $('[data-name="end_datetime"]').css("visibility", "visible"); 
                    $('[data-name="end_datetime"]').css("height", "auto");
                    $('[data-name="duration_hours"]').css("visibility", "visible"); 
                    $('[data-name="duration_hours"]').css("height", "auto"); 
                } else {
                    $('[data-name="start_datetime"]').css("visibility", "hidden");            
                    $('[data-name="start_datetime"]').css("height", "0");
                    $('[data-name="end_datetime"]').css("visibility", "hidden");            
                    $('[data-name="end_datetime"]').css("height", "0");
                    $('[data-name="duration_hours"]').css("visibility", "hidden");            
                    $('[data-name="duration_hours"]').css("height", "0");    
                }
                if (action == 'Archive Onsite' || action == 'External Transfer' || action == 'Internal Transfer' || action == 'Processing') {
                    $('[data-name="processing_transfer_condition"]').css("visibility", "visible"); 
                    $('[data-name="processing_transfer_condition"]').css("height", "auto");
                } else {
                    $('[data-name="processing_transfer_condition"]').css("visibility", "hidden");            
                    $('[data-name="processing_transfer_condition"]').css("height", "0");
                }
                if (action == 'Archive Onsite' || action == 'Create Inventory Collection' || action == 'Internal Transfer' || action == 'Processing' || action == 'Separate Inventory Items') {
                    $('[data-name="end_condition"]').css("visibility", "visible"); 
                    $('[data-name="end_condition"]').css("height", "auto");
                } else {
                    $('[data-name="end_condition"]').css("visibility", "hidden");            
                    $('[data-name="end_condition"]').css("height", "0");
                }
                if (action == 'Archive Onsite' || action == 'Internal Transfer Storage' || action == 'Separate Inventory Items') {              
                    $('[data-name="location_type"]').css("visibility", "visible"); 
                    $('[data-name="location_type"]').css("height", "auto");
                } else {
                    $('[data-name="location_type"]').css("visibility", "hidden");            
                    $('[data-name="location_type"]').css("height", "0");
                }

                var location_type = this.model.get('location_type');
                if (location_type == 'Room' || location_type == 'RoomCabinet' || location_type == 'RoomShelf') {
                    $('[data-name="location_room"]').css("visibility", "visible"); 
                    $('[data-name="location_room"]').css("height", "auto"); 

                } else {
                    $('[data-name="location_room"]').css("visibility", "hidden"); 
                    $('[data-name="location_room"]').css("height", "0"); 
                }                
                if (location_type == 'Shelf' || location_type == 'RoomShelf') {
                    $('[data-name="location_shelf"]').css("visibility", "visible"); 
                    $('[data-name="location_shelf"]').css("height", "auto"); 

                } else {
                    $('[data-name="location_shelf"]').css("visibility", "hidden"); 
                    $('[data-name="location_shelf"]').css("height", "0"); 
                }
                if (location_type == 'RoomCabinet' || location_type == 'Cabinet') {
                    $('[data-name="location_cabinet"]').css("visibility", "visible"); 
                    $('[data-name="location_cabinet"]').css("height", "auto"); 

                } else {
                    $('[data-name="location_cabinet"]').css("visibility", "hidden"); 
                    $('[data-name="location_cabinet"]').css("height", "0"); 
                }
                if (location_type == 'Equipment') {
                    $('[data-name="location_equipment"]').css("visibility", "visible"); 
                    $('[data-name="location_equipment"]').css("height", "auto"); 

                } else {
                    $('[data-name="location_equipment"]').css("visibility", "hidden"); 
                    $('[data-name="location_equipment"]').css("height", "0"); 
                }
                
                var location_equi_serial_no_hide_c = this.model.get('location_equi_serial_no_hide_c');
                if (location_equi_serial_no_hide_c == '51014222') {
                    $('[data-name="location_rack_c"]').css("visibility", "visible"); 
                    $('[data-name="location_rack_c"]').css("height", "auto");
                    $('[data-name="location_bin_c"]').css("visibility", "visible"); 
                    $('[data-name="location_bin_c"]').css("height", "auto");  
                    $('[data-name="location_cubby_c"]').css("visibility", "visible"); 
                    $('[data-name="location_cubby_c"]').css("height", "auto");
                } else {
                    $('[data-name="location_rack_c"]').css("visibility", "hidden"); 
                    $('[data-name="location_rack_c"]').css("height", "0"); 
                    $('[data-name="location_bin_c"]').css("visibility", "hidden"); 
                    $('[data-name="location_bin_c"]').css("height", "0"); 
                    $('[data-name="location_cubby_c"]').css("visibility", "hidden"); 
                    $('[data-name="location_cubby_c"]').css("height", "0"); 
                }
                console.log('onload_function ', popupAlert);  
            },            
            handleSave: function () {
                $("#alertsCustom").remove();
                selfIMRecord = this;
                if (selfIMRecord.model.get("reviewed_record_c") == "No" && selfIMRecord.model.get("processed_per_protocol_c") == "No"  && popupAlert == 0 ) {
                    $("#sidecar").prepend(`    
                        <div id="alertsCustom" class="alert-top">
                        <div class="alert-wrapper">
                            <div class="alert alert-warning alert-block closeable">
                                <div class="indicator">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </div>
                                <button class="close btn btn-link btn-invisible closeCustomIconRV" data-action="close"><i class="fa fa-times"></i></button>
                                <span class="text">
                            <strong>Warning: </strong>
                            Inventory Management not processed per protocol and record not reviewed.  Remember to submit a Communication.
                        </span>
                            </div>
                        </div>
                        </div>
                    `);
                } else if (selfIMRecord.model.get("reviewed_record_c") == "No" && popupAlert == 0) {
                    $("#sidecar").prepend(`    
                        <div id="alertsCustom" class="alert-top">
                        <div class="alert-wrapper">
                            <div class="alert alert-warning alert-block closeable">
                                <div class="indicator">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </div>
                                <button class="close btn btn-link btn-invisible closeCustomIconRV" data-action="close"><i class="fa fa-times"></i></button>
                                <span class="text">
                            <strong>Warning: </strong>
                            Inventory Management record not reviewed. Remember to submit a Communication.
                        </span>
                            </div>
                        </div>
                        </div>
                    `);
                } else if (selfIMRecord.model.get("processed_per_protocol_c") == "No"  && popupAlert == 0) {
                    $("#sidecar").prepend(`    
                        <div id="alertsCustom" class="alert-top">
                        <div class="alert-wrapper">
                            <div class="alert alert-warning alert-block closeable">
                                <div class="indicator">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </div>
                                <button class="close btn btn-link btn-invisible closeCustomIconRV" data-action="close"><i class="fa fa-times"></i></button>
                                <span class="text">
                            <strong>Warning: </strong>
                            Inventory Management not processed per protocol. Remember to submit a Communication.
                        </span>
                            </div>
                        </div>
                        </div>
                    `);
                } else {
                    selfIMRecord.saveInventoryRecordView();
                }

            },
            saveInventoryRecordView: function () {
                $("#alertsCustom").remove();                
                selfIMRecord._super('handleSave');
            }
        })
    });
})(SUGAR.App);