(function appFunc(app) {
    app.events.on("app:start", function appStartFunc() {
        if (!app.view.layouts.BaseSubpanelsLayout && !app.view.layouts.BaseCustomSubpanelsLayout) {
            app.view.declareComponent("layout", "subpanels", undefined, undefined, false, "base");
        }

        var subpanelsLayout = "BaseSubpanelsLayout";

        if (app.view.layouts.BaseCustomSubpanelsLayout) { 
            subpanelsLayout = "BaseCustomSubpanelsLayout";
        }

        if (App.view.layouts[subpanelsLayout].hideIMSubpanel === true) {
            return;
        }

        App.view.layouts[subpanelsLayout] = App.view.layouts[subpanelsLayout].extend({
            hideIMSubpanel:   true,
            _hiddenSubpanels: [],
            
            initialize: function () {
                var result = this._super("initialize", arguments);

                if (this.model.module === "IM_Inventory_Management") {
                    app.api.call("read", "rest/v10/IM_Inventory_Management/" + this.model.id + "?fields=related_to", null, {
                        success: function apiCallSuccess(model) {
                            if (model && model.related_to && model.related_to === "Work Product") {
                                this._hideUnnecessarySubpanel("m03_work_product_im_inventory_management_1");
                            }
                        }.bind(this)
                    });
                }

                return result;
            },

            /**
             * Add to the _hiddenSubpanels array, and hide the subpanel
             */
            _hideUnnecessarySubpanel: function (link) {
                this._hiddenSubpanels.push(link);
                var component = this._getSubpanelByLink(link);

                if (!_.isUndefined(component)) {
                    component.hide();
                }
                this._hiddenSubpanels = _.unique(this._hiddenSubpanels);
            },

            /**
             * Helper for getting the Subpanel View for a specific link
             */
            _getSubpanelByLink: function (link) {
                return this._components.find(function findComponent(component) {
                    return component.context.get("link") === link;
                });
            },

        });
    });
})(SUGAR.App);