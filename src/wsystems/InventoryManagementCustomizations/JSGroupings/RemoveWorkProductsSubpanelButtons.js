(function appFUnc(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {

        if (
            !app.view.views.BaseM03_Work_ProductSubpanelListView &&
            !app.view.views.BaseM03_Work_ProductCustomSubpanelListView
        ) {
            app.view.declareComponent("view", "subpanel-list", "M03_Work_Product", undefined, false, "base");
        }
        var subpanelListView = "BaseM03_Work_ProductSubpanelListView";
        if (app.view.views.BaseM03_Work_ProductCustomSubpanelListView) {
            subpanelListView = "BaseM03_Work_ProductCustomSubpanelListView";
        }
        if (App.view.views[subpanelListView].workProductsSubpanelList === true) {
            return;
        }
        App.view.views[subpanelListView] = App.view.views[subpanelListView].extend({
            workProductsSubpanelList: true,

            initialize: function () {
                var returnValue = this._super("initialize", arguments);

                if (this.context.get("parentModule") === "IM_Inventory_Management") {
                    this._removeActionButtons();
                }

                return returnValue;
            },

            _removeActionButtons: function () {
                var counter = 0;

                while (this.meta.rowactions.actions.length > 1) {
                    if (this.meta.rowactions.actions[counter].event === "list:preview:fire") {
                        counter++;
                    } else {
                        this.meta.rowactions.actions.splice(counter, 1);
                    }
                }
            },

        });

        if (
            !app.view.views.BaseM03_Work_ProductPanelTopView &&
            !app.view.views.BaseM03_Work_ProductCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "M03_Work_Product", undefined, false, "base");
        }

        var workProductPanelTop = "BaseM03_Work_ProductPanelTopView";

        if (app.view.views.BaseM03_Work_ProductCustomPanelTopView) {
            workProductPanelTop = "BaseM03_Work_ProductCustomPanelTopView";
        }

        if (App.view.views[workProductPanelTop].removePanelTopButtons === true) {
            return;
        }

        App.view.views[workProductPanelTop] = App.view.views[workProductPanelTop].extend({
            removePanelTopButtons: true,

            initialize: function () {
                var initializeOptions = this._super("initialize", arguments);

                if (this.parentModule === "IM_Inventory_Management") {
                    this._removeActionButtons();
                }

                return initializeOptions;
            },

            _removeActionButtons: function () {
                var index = _.findIndex(this.meta.buttons, {
                    type: "actiondropdown"
                });

                if (index === -1) {
                    return;
                }

                while (this.meta.buttons[index].buttons.length > 0) {
                    this.meta.buttons[index].buttons.splice(0, 1);
                }
            }
        });
    });
})(SUGAR.App);
