(function appFunc(app) {
    app.events.on(
        "data:sync:start",
        function dataSyncCompleteFunct(method, collection, options) {
            if (method !== "read") return;

            var fieldObj;

            if (options) {
                fieldObj = options.context;
            }

            if (_.isUndefined(fieldObj)) {
                return;
            }

            if (
                options.view === "selection-list" &&
                fieldObj.get("module") === "IM_Inventory_Management" &&
                App.router._currentFragment.indexOf("II_Inventory_Item") > -1
            ) {
                options.params._filterIMRecords = true;
            }
        }.bind(this)
    );
})(SUGAR.App);
