$(document).on('click', '.closeCustomIconCV', function () {
    selfI.saveInventoryManagementCRecord();
});

function onChangeSearch(subtype) {
    var count = 0;
    var checked = $(subtype).prop('checked');
    $('.checkchk').prop('checked', checked);
    if ($(".checkAllst:checked")) {

        count = $(".checkchk:checked").length;
        if (count > 0) {
            $('.showalert').html('');
            var str = 'You have selected ' + count + ' records in the result set.<button type="button" class="btn btn-link btn-inline removeselectedchk" data-action="clear" onclick="removeselectedchk();">Clear selections.</button>';
            $('.showalert').show();
            $('.showalert').html(str);
            $('.showalertcls').show();
        } else {
            $('.showalert').hide();
            $('.showalert').html('');
            $('.showalertcls').hide();
        }

    } else {
        $('.showalert').hide();
        $('.showalert').html('');
        $('.showalertcls').hide();
    }
}



$(document).on("change", ".checkchk", function () {

    if ($('.checkchk:checked').length == $('.checkchk').length) {
        $('.checkAllst').prop('checked', true);
        $('.showalert').show();
    } else {
        $('.checkAllst').prop('checked', false);
        $('.showalert').hide();
    }
});


function removeselectedchk() {
    $('.showalert').html('');
    $('.showalertcls').hide();
    $('input[name="check"]').prop('checked', false);
    $('input[name="checkAllst"]').prop('checked', false);
}
/* Prakash Jangir 950 On 02-March-2021 */

//Settings for load more buttom
var offsetIM = 0;
var orderTypeIM = "desc";
var lastSortedColumnNameIM = "name";
var appendQueryIM = "&order_by=name%3Adesc";
var timer = null;

(function appFunct(app) {
    var self = this;

    /* Prakash Jangir 950 --Start */
    //Internal Selection Sidecar
    $(document).on('click', 'div.main-pane.ii_internal_selection th', function (event) {
        if (typeof moduleNameIM !== 'undefined' && moduleNameIM == "II_Inventory_Item_IM") {
            currentClickedColumnName = $(this).attr('data-fieldname');
            if (currentClickedColumnName == undefined)
                return;
            //console.log("orderTypeIM ", orderTypeIM);
            offsetIM = 0;
            if (lastSortedColumnNameIM == currentClickedColumnName) {
                if (orderTypeIM == "asc")
                    orderTypeIM = "desc";
                else
                    orderTypeIM = "asc";
            } else {
                orderTypeIM = "desc";
            }
            var category = $('.ii_internal_selection input.search-name').attr('category');
            var type_2 = $('.ii_internal_selection input.search-name').attr('type_2');
            var type_specimen_c = $('.ii_internal_selection input.search-name').attr('type_specimen_c');
            //console.log('1',type_specimen_c);
            var role_Type = $('.ii_internal_selection input.search-name').attr('roleType');
            if (role_Type == 'admin') {
                var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
            } else {
                var ICByRole = '';
            }

            var typespecimen = type_specimen_c.split(',');
            //console.log('11',typespecimen);
            var typeString = "";
            if (type_specimen_c != "") {
                //var type_specimen_Arr_String = type_specimen_c.split(',');
                var type_specimen_Arr = [];
                $ii = 0;
                $.each(typespecimen, function (index, value) {
                    var strVal = value.replace(/,\s*$/, "");
                    type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                    $ii++;
                });
                typeString = type_specimen_Arr.join('')
                //console.log('typeString',typeString);
            }
            //console.log("79 Type: " + type_2 + " Category: " + category);
            lastSortedColumnNameIM = currentClickedColumnName;
            appendQueryIM = "&order_by=" + currentClickedColumnName + "%3A" + orderTypeIM;
            $('.checkall').trigger('click');
            $('.checkall input[name="check"]').prop('checked', false);
            $('input[name="check"]').trigger('click');
            $('input[name="check"]').prop('checked', false);
            var self = this;

            setTimeout(function () {
                if (type_specimen_c != "" && category == "Specimen") {
                    var url = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&offset=0" + appendQueryIM + "&filter[0][related_to_c][$in][]=Internal Use&filter[0][related_to_c][$in][]=Order Request Item&filter[0][related_to_c][$in][]=Purchase Order Item&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + typeString + ICByRole;
                } else {
                    var url = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&offset=0" + appendQueryIM + "&filter[0][related_to_c][$in][]=Internal Use&filter[0][related_to_c][$in][]=Order Request Item&filter[0][related_to_c][$in][]=Purchase Order Item&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole;
                }

                app.api.call("read", url, null, {
                    success: function (ii_internal) {
                        offsetIM = 20;
                        if (ii_internal.records.length == 0) {
                            hideShowMoreButton = 1;
                            // $('button[data-action="show-more-ii"]').hide();
                            // return;
                        }
                        var internalModels = [];

                        var internalModelsReset = [];
                        if (ii_internal.records && ii_internal.records.length > 0) {
                            internalModels = ii_internal.records;
                        }

                        internalView.collection.reset();
                        internalView.collection.add(internalModelsReset);
                        internalView.collection.add(internalModels);
                        internalView.render();
                        $('div.main-pane.ii_internal_selection div.flex-list-view-content').css("overflow", "visible");
                        $('button[data-action="show-more-ii"]').show();


                    }.bind(self),
                    error: function (error) {
                        app.alert.show("ori_error", {
                            level: "error",
                            messages: "Error retrieving Inventory Item",
                            autoClose: true
                        });
                    }
                });

            }, 200);
            $('button[data-action="show-more-ii"]').show();
        }
    });

    $(document).on('click', 'button[data-action="show-more-ii"]', function () {
        $('button[data-action="show-more-ii"]').hide();
        $('div.loading-ori').show();
        var category = $('.ii_internal_selection input.search-name').attr('category');
        var type_2 = $('.ii_internal_selection input.search-name').attr('type_2');
        var type_specimen_c = $('.ii_internal_selection input.search-name').attr('type_specimen_c');
        //console.log('2',type_specimen_c);
        var role_Type = $('.ii_internal_selection input.search-name').attr('roleType');
        if (role_Type == 'admin') {
            var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
        } else {
            var ICByRole = '';
        }

        var typespecimen = type_specimen_c.split(',');
        //console.log('22',typespecimen);
        var typeString = "";
        if (type_specimen_c != "") {
            //var type_specimen_Arr_String = type_specimen_c.split(',');
            var type_specimen_Arr = [];
            $ii = 0;
            $.each(typespecimen, function (index, value) {
                var strVal = value.replace(/,\s*$/, "");
                type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                $ii++;
            });
            typeString = type_specimen_Arr.join('')
            //console.log('typeString',typeString);
        }
        if (type_specimen_c != "" && category == "Specimen") {
            var url = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][related_to_c][$in][]=Internal Use&filter[0][related_to_c][$in][]=Order Request Item&filter[0][related_to_c][$in][]=Purchase Order Item&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + typeString + ICByRole;
        } else {
            var url = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][related_to_c][$in][]=Internal Use&filter[0][related_to_c][$in][]=Order Request Item&filter[0][related_to_c][$in][]=Purchase Order Item&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole;
        }

        app.api.call("read", url, null, {
            success: function (ii_internal) {
                $('div.loading-ori').hide();
                offsetIM += 20;
                if (ii_internal.records.length == 0) {
                    hideShowMoreButton = 1;
                    $('button[data-action="show-more-ii"]').hide();
                    return;
                }
                var internalModels = [];

                if (ii_internal.records && ii_internal.records.length > 0) {
                    internalModels = ii_internal.records;
                }
                internalView.collection.add(internalModels);
                internalView.render();
                $('div.main-pane.ii_internal_selection div.flex-list-view-content').css("overflow", "visible");

                $('button[data-action="show-more-ii"]').show();

            }.bind(this),
            error: function (error) {
                app.alert.show("ori_error", {
                    level: "error",
                    messages: "Error retrieving Inventory Item",
                    autoClose: true
                });
            }
        });
    });
    $(document).on('click', 'button[data-action="show-more-specimenic"]', function () {
        $('button[data-action="show-more-specimenic"]').hide();
        $('div.loading-ori').show();
        console.log("after clicxck the Specimenic button");
        var IIsearch = $('.ii_specimen_selection input.search-name').val();
        console.log("name value===>",IIsearch);
        var workProduct = $('.ii_specimen_selection input.search-name').attr('wpName');
        var category = $('.ii_specimen_selection input.search-name').attr('category');
        var type_specimen_c = $('.ii_specimen_selection input.search-name').attr('type_specimen_c');
        const Converttype_specimen_c = type_specimen_c.split(',');
        console.log('6',type_specimen_c);
        console.log('7',category);
        var roleType = $('.ii_specimen_selection input.search-name').attr('roleType');
        if (roleType == 'admin') {
            var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
        } else {
            var ICByRole = '';
        }
        var typeString = "";
        if (Converttype_specimen_c != "") {
            //var type_specimen_Arr_String = type_specimen_c.split(',');
            var type_specimen_Arr = [];
            $ii = 0;
            $.each(Converttype_specimen_c, function (index, value) {
                var strVal = value.replace(/,\s*$/, "");
                type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                $ii++;
            });
            typeString = type_specimen_Arr.join('')
            console.log('typeString', typeString);
        }
        // offsetIM = 0;
        moduleNameIM = "II_Inventory_Item_WP_IC";
        $('button[data-action="show-more-wpic"]').remove();
        /**25 Feb 2022 Changes : #2021 : Gsingh */
        if(IIsearch == "" || IIsearch == null || IIsearch == undefined){
            url = "rest/v10/II_Inventory_Item?fields=name,collection_date&offset=0&max_num=20&offset=" + offsetIM + appendQueryIM +"&order_by=name%3Adesc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + typeString + "&filter[0][status][$not_in][]=Exhausted" + ICByRole;
        }else{
            console.log("In else after click show moew");
            url = "rest/v10/II_Inventory_Item?fields=name,collection_date&offset=0&max_num=20&offset=" + offsetIM + appendQueryIM +"&order_by=name%3Adesc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + typeString + "&filter[0][status][$not_in][]=Exhausted&filter[0][$and][0][$or][1][$or][3][name][$contains]=" + IIsearch + ICByRole;
        }
        

        app.api.call("read", url, null, {
            success: function (ii_specimen_selection) {
                $('div.loading-ori').hide();
                offsetIM += 20;
                if (ii_specimen_selection.records.length == 0) {
                    hideShowMoreButton = 1;
                    $('button[data-action="show-more-specimenic"]').hide();
                    return;
                }
                var internalModels = [];

                if (ii_specimen_selection.records && ii_specimen_selection.records.length > 0) {
                    internalModels = ii_specimen_selection.records;
                }
                console.log("internal Models==>", internalModels);
                ii_specimenView.collection.add(internalModels);
                ii_specimenView.render();
                $('div.main-pane.ii_specimen_selection div.flex-list-view-content').css("overflow", "visible");

                if(internalModels.length > 19)
                    $('button[data-action="show-more-specimenic"]').show();
                else
                    $('button[data-action="show-more-specimenic"]').hide();

            }.bind(this),
            error: function (error) {
                app.alert.show("ori_error", {
                    level: "error",
                    messages: "Error retrieving Inventory Item",
                    autoClose: true
                });
            }
        });
    });

    $(document).on('click', 'div.main-pane.ii_sales_selection th', function (event) {
        if (typeof moduleNameIM !== 'undefined' && moduleNameIM == "II_Inventory_Item_IM") {
            currentClickedColumnName = $(this).attr('data-fieldname');
            if (currentClickedColumnName == undefined)
                return;
            offsetIM = 0;
            //console.log("!70")
            if (lastSortedColumnNameIM == currentClickedColumnName) {
                if (orderTypeIM == "asc")
                    orderTypeIM = "desc";
                else
                    orderTypeIM = "asc";
            } else {
                orderTypeIM = "desc";
            }
            var saleId = $('.ii_sales_selection input.search-name').attr('saleName');
            var category = $('.ii_sales_selection input.search-name').attr('category');
            var type_2 = $('.ii_sales_selection input.search-name').attr('type_2');
            var type_specimen_c = $('.ii_sales_selection input.search-name').attr('type_specimen_c');
            //console.log('3',type_specimen_c);
            var role_Type = $('.ii_sales_selection input.search-name').attr('roleType');
            if (role_Type == 'admin') {
                var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
            } else {
                var ICByRole = '';
            }

            var typespecimen = type_specimen_c.split(',');
            //console.log('33',typespecimen);
            var typeString = "";
            if (type_specimen_c != "") {
                //var type_specimen_Arr_String = type_specimen_c.split(',');
                var type_specimen_Arr = [];
                $ii = 0;
                $.each(typespecimen, function (index, value) {
                    var strVal = value.replace(/,\s*$/, "");
                    type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                    $ii++;
                });
                typeString = type_specimen_Arr.join('')
                //console.log('typeString',typeString);
            }

            lastSortedColumnNameIM = currentClickedColumnName;
            appendQueryIM = "&order_by=" + currentClickedColumnName + "%3A" + orderTypeIM;
            $('.checkall').trigger('click');
            $('.checkall input[name="check"]').prop('checked', false);
            $('input[name="check"]').trigger('click');
            $('input[name="check"]').prop('checked', false);
            var self = this;

            setTimeout(function () {
                if (type_specimen_c != "" && category == "Specimen") {
                    var url = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][related_to_c][$equals]=Sales&filter[0][m01_sales_ii_inventory_item_1_name][$equals]=" + saleId + "&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + typeString + ICByRole;
                } else {
                    var url = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][related_to_c][$equals]=Sales&filter[0][m01_sales_ii_inventory_item_1_name][$equals]=" + saleId + "&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole;
                }
                app.api.call("read", url, null, {
                    success: function (ii_internal) {
                        offsetIM = 20;
                        if (ii_internal.records.length == 0) {
                            hideShowMoreButton = 1;
                            // $('button[data-action="show-more-ii"]').hide();
                            // return;
                        }
                        var internalModels = [];

                        var internalModelsReset = [];
                        if (ii_internal.records && ii_internal.records.length > 0) {
                            internalModels = ii_internal.records;
                        }

                        ii_salesView.collection.reset();
                        ii_salesView.collection.add(internalModelsReset);
                        ii_salesView.collection.add(internalModels);
                        ii_salesView.render();
                        $('div.main-pane.ii_sales_selection div.flex-list-view-content').css("overflow", "visible");

                        $('button[data-action="show-more-iisale"]').show();


                    }.bind(self),
                    error: function (error) {
                        app.alert.show("ori_error", {
                            level: "error",
                            messages: "Error retrieving Inventory Item",
                            autoClose: true
                        });
                    }
                });

            }, 200);
        }
    });

    $(document).on('click', 'button[data-action="show-more-iisale"]', function () {
        $('button[data-action="show-more-iisale"]').hide();
        $('div.loading-ori').show();
        var saleId = $('.ii_sales_selection input.search-name').attr('saleName');
        var category = $('.ii_sales_selection input.search-name').attr('category');
        var type_2 = $('.ii_sales_selection input.search-name').attr('type_2');
        var type_specimen_c = $('.ii_sales_selection input.search-name').attr('type_specimen_c');
        //console.log('4',type_specimen_c);
        var role_Type = $('.ii_sales_selection input.search-name').attr('roleType');
        if (role_Type == 'admin') {
            var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
        } else {
            var ICByRole = '';
        }

        var typespecimen = type_specimen_c.split(',');
        //console.log('44',typespecimen);
        var typeString = "";
        if (type_specimen_c != "") {
            //var type_specimen_Arr_String = type_specimen_c.split(',');
            var type_specimen_Arr = [];
            $ii = 0;
            $.each(typespecimen, function (index, value) {
                var strVal = value.replace(/,\s*$/, "");
                type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                $ii++;
            });
            typeString = type_specimen_Arr.join('')
            //console.log('typeString',typeString);
        }
        if (type_specimen_c != "" && category == "Specimen") {
            var url = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][related_to_c][$equals]=Sales&filter[0][m01_sales_ii_inventory_item_1_name][$equals]=" + saleId + "&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + typeString + ICByRole;
        } else {
            var url = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][related_to_c][$equals]=Sales&filter[0][m01_sales_ii_inventory_item_1_name][$equals]=" + saleId + "&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole;
        }
        app.api.call("read", url, null, {
            success: function (ii_internal) {
                $('div.loading-ori').hide();
                offsetIM += 20;
                if (ii_internal.records.length == 0) {
                    hideShowMoreButton = 1;
                    $('button[data-action="show-more-iisale"]').hide();
                    return;
                }
                var internalModels = [];

                if (ii_internal.records && ii_internal.records.length > 0) {
                    internalModels = ii_internal.records;
                }
                ii_salesView.collection.add(internalModels);
                ii_salesView.render();
                $('div.main-pane.ii_sales_selection div.flex-list-view-content').css("overflow", "visible");

                $('button[data-action="show-more-iisale"]').show();

            }.bind(this),
            error: function (error) {
                app.alert.show("ori_error", {
                    level: "error",
                    messages: "Error retrieving Inventory Item",
                    autoClose: true
                });
            }
        });
    });

    $(document).on('click', 'div.main-pane.ii_wp_selection th', function (event) {
        if (typeof moduleNameIM !== 'undefined' && moduleNameIM == "II_Inventory_Item_WP_II") {
            currentClickedColumnName = $(this).attr('data-fieldname');
            if (currentClickedColumnName == undefined)
                return;
            offsetIM = 0;
            if (lastSortedColumnNameIM == currentClickedColumnName) {
                if (orderTypeIM == "asc")
                    orderTypeIM = "desc";
                else
                    orderTypeIM = "asc";
            } else {
                orderTypeIM = "desc";
            }

            var workProduct = $('.ii_wp_selection input.search-name').attr('wpName');
            var category = $('.ii_wp_selection input.search-name').attr('category');
            var type_2 = $('.ii_wp_selection input.search-name').attr('type_2');
            var type_specimen_c = $('.ii_wp_selection input.search-name').attr('type_specimen_c');
            //console.log('5',type_specimen_c);
            var role_Type = $('.ii_wp_selection input.search-name').attr('roleType');
            if (role_Type == 'admin') {
                var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
            } else {
                var ICByRole = '';
            }

            var typespecimen = type_specimen_c.split(',');
            //console.log('55',typespecimen);
            var typeString = "";
            if (type_specimen_c != "") {
                //var type_specimen_Arr_String = type_specimen_c.split(',');
                var type_specimen_Arr = [];
                $ii = 0;
                $.each(typespecimen, function (index, value) {
                    var strVal = value.replace(/,\s*$/, "");
                    type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                    $ii++;
                });
                typeString = type_specimen_Arr.join('')
                //console.log('typeString',typeString);
            }

            lastSortedColumnNameIM = currentClickedColumnName;
            appendQueryIM = "&order_by=" + currentClickedColumnName + "%3A" + orderTypeIM;
            $('.checkall').trigger('click');
            $('.checkall input[name="check"]').prop('checked', false);
            $('input[name="check"]').trigger('click');
            $('input[name="check"]').prop('checked', false);
            var self = this;

            setTimeout(function () {
                if (type_specimen_c != "" && category == "Specimen") {
                    var url = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + typeString + ICByRole;
                } else {
                    var url = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole;
                }
                app.api.call("read", url, null, {
                    success: function (ii_internal) {
                        offsetIM = 20;
                        if (ii_internal.records.length == 0) {
                            hideShowMoreButton = 1;
                            // $('button[data-action="show-more-ii"]').hide();
                            // return;
                        }
                        var internalModels = [];

                        var internalModelsReset = [];
                        if (ii_internal.records && ii_internal.records.length > 0) {
                            internalModels = ii_internal.records;
                        }

                        ii_wpView.collection.reset();
                        ii_wpView.collection.add(internalModelsReset);
                        ii_wpView.collection.add(internalModels);
                        ii_wpView.render();
                        $('div.main-pane.ii_wp_selection div.flex-list-view-content').css("overflow", "visible");

                        $('button[data-action="show-more-wpii"]').show();

                    }.bind(self),
                    error: function (error) {
                        app.alert.show("ori_error", {
                            level: "error",
                            messages: "Error retrieving Inventory Item",
                            autoClose: true
                        });
                    }
                });

            }, 200);
        }
    });

    /**#2101 changes : 28 feb 2022 : If category = specimen , serach by timepoint column only */
    $(document).on('click', 'div.main-pane.ii_specimen_selection th', function (event) {
        console.log('search by timepoint line 483');
        if (typeof moduleNameIM !== 'undefined' && moduleNameIM == "II_Inventory_Item_WP_II") {
            currentClickedColumnName = $(this).attr('data-fieldname');
            if (currentClickedColumnName == undefined)
                return;
            offsetIM = 0;
            if (lastSortedColumnNameIM == currentClickedColumnName) {
                if (orderTypeIM == "asc")
                    orderTypeIM = "desc";
                else
                    orderTypeIM = "asc";
            } else {
                orderTypeIM = "desc";
            }
            var workProduct = $('.ii_specimen_selection input.search-name').attr('wpName');
            var type_specimen_c = $('.ii_wp_selection input.search-name').attr('type_specimen_c');
            var role_Type = $('.ii_specimen_selection input.search-name').attr('roleType');
            if (role_Type == 'admin') {
                var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
            } else {
                var ICByRole = '';
            }

            var typespecimen = type_specimen_c.split(',');
            //console.log('55',typespecimen);
            var typeString = "";
            if (type_specimen_c != "") {
                //var type_specimen_Arr_String = type_specimen_c.split(',');
                var type_specimen_Arr = [];
                $ii = 0;
                $.each(typespecimen, function (index, value) {
                    var strVal = value.replace(/,\s*$/, "");
                    type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                    $ii++;
                });
                typeString = type_specimen_Arr.join('')
                //console.log('typeString',typeString);
            }

            lastSortedColumnNameIM = currentClickedColumnName;
            appendQueryIM = "&order_by=" + currentClickedColumnName + "%3A" + orderTypeIM;
            $('.checkall').trigger('click');
            $('.checkall input[name="check"]').prop('checked', false);
            $('input[name="check"]').trigger('click');
            $('input[name="check"]').prop('checked', false);
            var self = this;

            setTimeout(function () {
                if (type_specimen_c != "" && category == "Specimen") {
                    var url = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + typeString + ICByRole;
                }
                app.api.call("read", url, null, {
                    success: function (ii_specimen) {
                        offsetIM = 20;
                        if (ii_specimen.records.length == 0) {
                            hideShowMoreButton = 1;
                            // $('button[data-action="show-more-ii"]').hide();
                            // return;
                        }
                        var specimenModels = [];

                        var specimenModelsReset = [];
                        if (ii_specimen.records && ii_specimen.records.length > 0) {
                            specimenModels = ii_specimen.records;
                        }

                        ii_specimenView.collection.reset();
                        ii_specimenView.collection.add(specimenModelsReset);
                        ii_specimenView.collection.add(specimenModels);
                        ii_specimenView.render();
                        $('div.main-pane.ii_specimen_selection div.flex-list-view-content').css("overflow", "visible");

                        $('button[data-action="show-more-specimenii"]').show();

                    }.bind(self),
                    error: function (error) {
                        app.alert.show("ori_error", {
                            level: "error",
                            messages: "Error retrieving Inventory Item",
                            autoClose: true
                        });
                    }
                });

            }, 200);
        }
    });
    /***//////////////////////////////////////////////////////////////////////////////////// */

    $(document).on('click', 'button[data-action="show-more-wpii"]', function () {
        $('button[data-action="show-more-wpii"]').hide();
        $('div.loading-ori').show();
        var workProduct = $('.ii_wp_selection input.search-name').attr('wpName');
        var category = $('.ii_wp_selection input.search-name').attr('category');
        var type_2 = $('.ii_wp_selection input.search-name').attr('type_2');
        var type_specimen_c = $('.ii_wp_selection input.search-name').attr('type_specimen_c');
        //console.log('6',type_specimen_c);
        var role_Type = $('.ii_wp_selection input.search-name').attr('roleType');
        if (role_Type == 'admin') {
            var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
        } else {
            var ICByRole = '';
        }
        var typespecimen = type_specimen_c.split(',');
        //console.log('66',typespecimen);
        var typeString = "";
        if (type_specimen_c != "") {
            //var type_specimen_Arr_String = type_specimen_c.split(',');
            var type_specimen_Arr = [];
            $ii = 0;
            $.each(typespecimen, function (index, value) {
                var strVal = value.replace(/,\s*$/, "");
                type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                $ii++;
            });
            typeString = type_specimen_Arr.join('')
            //console.log('typeString',typeString);
        }

        if (type_specimen_c != "" && category == "Specimen") {
            var url = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + typeString + ICByRole;
        } else {
            var url = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole;
        }
        app.api.call("read", url, null, {
            success: function (ii_internal) {
                $('div.loading-ori').hide();
                offsetIM += 20;
                if (ii_internal.records.length == 0) {
                    hideShowMoreButton = 1;
                    $('button[data-action="show-more-wpii"]').hide();
                    return;
                }
                var internalModels = [];

                if (ii_internal.records && ii_internal.records.length > 0) {
                    internalModels = ii_internal.records;
                }
                ii_wpView.collection.add(internalModels);
                ii_wpView.render();
                $('div.main-pane.ii_wp_selection div.flex-list-view-content').css("overflow", "visible");

                if (ii_internal.records && ii_internal.records.length > 19)
                    $('button[data-action="show-more-wpii"]').show();

            }.bind(this),
            error: function (error) {
                app.alert.show("ori_error", {
                    level: "error",
                    messages: "Error retrieving Inventory Item",
                    autoClose: true
                });
            }
        });
    });

    /**#2101 changes : 28 feb 2022 : If category = specimen , load more */
    $(document).on('click', 'button[data-action="show-more-specimenii"]', function () {
        console.log('specimen load more line 642');
        $('button[data-action="show-more-specimenii"]').hide();
        $('div.loading-ori').show();
        var workProduct = $('.ii_specimen_selection input.search-name').attr('wpName');
        var category = $('.ii_specimen_selection input.search-name').attr('category');
        var type_2 = $('.ii_specimen_selection input.search-name').attr('type_2');
        var type_specimen_c = $('.ii_specimen_selection input.search-name').attr('type_specimen_c');
        console.log('category line 649', category);
        console.log('type_specimen_c line 650', type_specimen_c);
        //console.log('6',type_specimen_c);
        var role_Type = $('.ii_specimen_selection input.search-name').attr('roleType');
        if (role_Type == 'admin') {
            var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
        } else {
            var ICByRole = '';
        }
        var typespecimen = type_specimen_c.split(',');
        //console.log('66',typespecimen);
        var typeString = "";
        if (type_specimen_c != "") {
            //var type_specimen_Arr_String = type_specimen_c.split(',');
            var type_specimen_Arr = [];
            $ii = 0;
            $.each(typespecimen, function (index, value) {
                var strVal = value.replace(/,\s*$/, "");
                type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                $ii++;
            });
            typeString = type_specimen_Arr.join('')
            //console.log('typeString',typeString);
        }

        if (type_specimen_c != "" && category == "Specimen") {
            console.log('type_specimen_c line 673', type_specimen_c);
            var url = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + typeString + ICByRole;
        }
        app.api.call("read", url, null, {
            success: function (ii_specimen) {
                $('div.loading-ori').hide();
                offsetIM += 20;
                if (ii_specimen.records.length == 0) {
                    hideShowMoreButton = 1;
                    $('button[data-action="show-more-specimenii"]').hide();
                    return;
                }
                var specimenModels = [];

                if (ii_specimen.records && ii_specimen.records.length > 0) {
                    specimenModels = ii_specimen.records;
                }
                ii_specimenView.collection.add(specimenModels);
                ii_specimenView.render();
                $('div.main-pane.ii_specimen_selection div.flex-list-view-content').css("overflow", "visible");

                if (ii_specimen.records && ii_specimen.records.length > 19)
                    $('button[data-action="show-more-specimenii"]').show();

            }.bind(this),
            error: function (error) {
                app.alert.show("ori_error", {
                    level: "error",
                    messages: "Error retrieving Inventory Item",
                    autoClose: true
                });
            }
        });
    });
    /***///////////////////////////////////////////////////////////////////////////////////// */

    // =====================Working Area Start=============================
    $(document).on('click', 'div.main-pane.ii_wp_selection th', function (event) {
        if (typeof moduleNameIM !== 'undefined' && moduleNameIM == "II_Inventory_Item_WP_IC") {
            currentClickedColumnName = $(this).attr('data-fieldname');
            if (currentClickedColumnName == undefined)
                return;
            offsetIM = 0;
            if (lastSortedColumnNameIM == currentClickedColumnName) {
                if (orderTypeIM == "asc")
                    orderTypeIM = "desc";
                else
                    orderTypeIM = "asc";
            } else {
                orderTypeIM = "desc";
            }
            var workProduct = $('.ii_wp_selection input.search-name').attr('wpName');
            var category = $('.ii_wp_selection input.search-name').attr('category');
            var type_2 = $('.ii_wp_selection input.search-name').attr('type_2');
            var type_specimen_c = $('.ii_wp_selection input.search-name').attr('type_specimen_c');
            //console.log('7',typespecimen);
            var role_Type = $('.ii_wp_selection input.search-name').attr('roleType');
            if (role_Type == 'admin') {
                var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
            } else {
                var ICByRole = '';
            }

            var typespecimen = type_specimen_c.split(',');
            //console.log('77',typespecimen);
            var typeString = "";
            if (type_specimen_c != "") {
                //var type_specimen_Arr_String = type_specimen_c.split(',');
                var type_specimen_Arr = [];
                $ii = 0;
                $.each(typespecimen, function (index, value) {
                    var strVal = value.replace(/,\s*$/, "");
                    type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                    $ii++;
                });
                typeString = type_specimen_Arr.join('')
                //console.log('typeString',typeString);
            }

            lastSortedColumnNameIM = currentClickedColumnName;
            appendQueryIM = "&order_by=" + currentClickedColumnName + "%3A" + orderTypeIM;
            $('.checkall').trigger('click');
            $('.checkall input[name="check"]').prop('checked', false);
            $('input[name="check"]').trigger('click');
            $('input[name="check"]').prop('checked', false);
            var self = this;

            setTimeout(function () {

                if (type_specimen_c != "" && category == "Specimen") {
                    var url = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + typeString + ICByRole;
                } else {
                    var url = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole;
                }

                app.api.call("read", url, null, {
                    success: function (ii_internal) {
                        offsetIM = 20;
                        if (ii_internal.records.length == 0) {
                            hideShowMoreButton = 1;
                            // $('button[data-action="show-more-ii"]').hide();
                            // return;
                        }
                        var internalModels = [];

                        var internalModelsReset = [];
                        if (ii_internal.records && ii_internal.records.length > 0) {
                            internalModels = ii_internal.records;
                        }

                        ii_wpView.collection.reset();
                        ii_wpView.collection.add(internalModelsReset);
                        ii_wpView.collection.add(internalModels);
                        ii_wpView.render();
                        $('div.main-pane.ii_wp_selection div.flex-list-view-content').css("overflow", "visible");

                        $('button[data-action="show-more-wpic"]').show();


                    }.bind(self),
                    error: function (error) {
                        app.alert.show("ori_error", {
                            level: "error",
                            messages: "Error retrieving Inventory Item",
                            autoClose: true
                        });
                    }
                });

            }, 200);
        }
    });

    $(document).on('click', 'button[data-action="show-more-wpic"]', function () {
        $('button[data-action="show-more-wpic"]').hide();
        $('div.loading-ori').show();
        var workProduct = $('.ii_wp_selection input.search-name').attr('wpName');
        var category = $('.ii_wp_selection input.search-name').attr('category');
        var type_2 = $('.ii_wp_selection input.search-name').attr('type_2');
        var type_specimen_c = $('.ii_wp_selection input.search-name').attr('type_specimen_c');
        //console.log('8',typespecimen);
        var role_Type = $('.ii_wp_selection input.search-name').attr('roleType');
        if (role_Type == 'admin') {
            var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
        } else {
            var ICByRole = '';
        }

        var typespecimen = type_specimen_c.split(',');
        // console.log('88',typespecimen);
        var typeString = "";
        if (type_specimen_c != "") {
            //var type_specimen_Arr_String = type_specimen_c.split(',');
            var type_specimen_Arr = [];
            $ii = 0;
            $.each(typespecimen, function (index, value) {
                var strVal = value.replace(/,\s*$/, "");
                type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                $ii++;
            });
            typeString = type_specimen_Arr.join('')
            //console.log('typeString',typeString);
        }

        if (type_specimen_c != "" && category == "Specimen") {
            var url = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + typeString + ICByRole;
        } else {
            var url = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&offset=" + offsetIM + appendQueryIM + "&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole;
        }
        app.api.call("read", url, null, {
            success: function (ii_internal) {
                $('div.loading-ori').hide();
                offsetIM += 20;
                if (ii_internal.records.length == 0) {
                    hideShowMoreButton = 1;
                    $('button[data-action="show-more-wpic"]').hide();
                    return;
                }
                var internalModels = [];

                if (ii_internal.records && ii_internal.records.length > 0) {
                    internalModels = ii_internal.records;
                }
                ii_wpView.collection.add(internalModels);
                ii_wpView.render();
                $('div.main-pane.ii_wp_selection div.flex-list-view-content').css("overflow", "visible");

                if (ii_internal.records && ii_internal.records.length > 19)
                    $('button[data-action="show-more-wpic"]').show();

            }.bind(this),
            error: function (error) {
                app.alert.show("ori_error", {
                    level: "error",
                    messages: "Error retrieving Inventory Item",
                    autoClose: true
                });
            }
        });
    });
    // =====================Working Area End=============================


    /* End */
    /** Sidecar search code based on related to : sales, work product, internal search  */
    $(document).on("keyup", ".ii_sales_selection input.search-name", function () {

        var sale_search = $('.ii_sales_selection input.search-name').val();

        var saleId = $('.ii_sales_selection input.search-name').attr('saleName');
        var category = $('.ii_sales_selection input.search-name').attr('category');
        var type_2 = $('.ii_sales_selection input.search-name').attr('type_2');
        var type_specimen_c = $('.ii_sales_selection input.search-name').attr('type_specimen_c');
        var composition = $('.ii_sales_selection input.search-name').attr('composition');
        var role_Type = $('.ii_sales_selection input.search-name').attr('roleType');
        if (role_Type == 'admin') {
            var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
        } else {
            var ICByRole = '';
        }

        var typespecimen = type_specimen_c.split(',');
        // console.log('88',typespecimen);
        var typeString = "";
        if (type_specimen_c != "") {
            //var type_specimen_Arr_String = type_specimen_c.split(',');
            var type_specimen_Arr = [];
            $ii = 0;
            $.each(typespecimen, function (index, value) {
                var strVal = value.replace(/,\s*$/, "");
                type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                $ii++;
            });
            typeString = type_specimen_Arr.join('')
            //console.log('typeString',typeString);
        }

        $(".ii_sales_selection").css('width', "100%");
        $(".ii_sales_selection .table").html('');

        var APIURL = "";

        if (category != "" && category != null && composition === "Inventory Item" && saleId != "") {
            APIURL = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&filter[0][related_to_c][$equals]=Sales&filter[0][$and][0][$and][1][$or][0][name][$contains]=" + sale_search + "&filter[0][$and][0][$and][1][$or][1][internal_barcode][$contains]=" + sale_search + "&filter[0][$and][0][$and][0][m01_sales_ii_inventory_item_1_name][$starts]=" + saleId + "&filter[0][$and][0][$and][1][category][$starts]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
        }



        app.api.call("read", APIURL, null, {
            success: function successFunc(ii_sales) {
                //console.log("successFunc call", ii_sales);
                $(".ii_sales_selection .table").html('');
                var data = "";
                data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><th class="sorting" data-fieldname="internal_barcode" data-orderby="" tabindex="-1"><span>Internal Barcode</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                $.each(ii_sales.records, function (key, item) {

                    if (item.name != '') {
                        data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span sclass="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td><td data-type="text" data-column="internal_barcode"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.internal_barcode + '">' + item.internal_barcode + '</div></span></td></tr>';
                    }

                });
                data += '</tbody>';
                $('.ii_sales_selection .table').append(data);

            }.bind(this),
            error: function (error) {
                console.log("error call");
            }
        });
    });

    $(document).on("keyup", ".ii_internal_selection input.search-name", function () {

        var internal_search = $('.ii_internal_selection input.search-name').val();
        $(".ii_internal_selection").css('width', "100%");
        $(".ii_internal_selection .table").html('');
        var category = $('.ii_internal_selection input.search-name').attr('category');
        var type_2 = $('.ii_internal_selection input.search-name').attr('type_2');
        var type_specimen_c = $('.ii_internal_selection input.search-name').attr('type_specimen_c');
        var composition = $('.ii_internal_selection input.search-name').attr('composition');
        var role_Type = $('.ii_internal_selection input.search-name').attr('roleType');
        if (role_Type == 'admin') {
            var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
        } else {
            var ICByRole = '';
        }

        var typespecimen = type_specimen_c.split(',');
        // console.log('88',typespecimen);
        var typeString = "";
        if (type_specimen_c != "") {
            //var type_specimen_Arr_String = type_specimen_c.split(',');
            var type_specimen_Arr = [];
            $ii = 0;
            $.each(typespecimen, function (index, value) {
                var strVal = value.replace(/,\s*$/, "");
                type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                $ii++;
            });
            typeString = type_specimen_Arr.join('')
            //console.log('typeString',typeString);
        }

        var APIURL = "";

        if (category != "" && category != null && composition === "Inventory Item") {

            APIURL = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&filter[0][$and][0][$and][1][$or][0][name][$contains]=" + internal_search + "&filter[0][$and][0][$and][1][$or][1][internal_barcode][$contains]=" + internal_search + "&filter[0][related_to_c][$in][]=Internal Use&filter[0][related_to_c][$in][]=Order Request Item&filter[0][related_to_c][$in][]=Purchase Order Item&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
        }

        app.api.call("read", APIURL, null, {
            success: function successFunc(ii_internal) {
                console.log("successFunc call", ii_internal);
                $(".ii_internal_selection .table").html('');
                var data = "";
                data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><th class="sorting" data-fieldname="internal_barcode" data-orderby="" tabindex="-1"><span>Internal Barcode</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                $.each(ii_internal.records, function (key, item) {

                    if (item.name != '') {
                        data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td><td data-type="text" data-column="internal_barcode"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.internal_barcode + '">' + item.internal_barcode + '</div></span></td></tr>';
                    }

                });
                data += '</tbody>';
                $('.ii_internal_selection .table').append(data);

            }.bind(this),
            error: function (error) {
                console.log("error call");
            }
        });
    });

    $(document).on("keyup", ".ii_wp_selection input.search-name", function () {
        //console.log("In Keyup DOP");
        clearTimeout(timer)
        timer = setTimeout(() => {
        var wp_search = $('.ii_wp_selection input.search-name').val();
        var related = $('.input[name="related_to"]').val();
        $(".ii_wp_selection").css('width', "100%");
        $(".ii_wp_selection .table").html('');
        var related = $('.ii_wp_selection input.search-name').attr('related_to_c');
        console.log("related", related);
        var wpName = $('.ii_wp_selection input.search-name').attr('wpName');
        var category = $('.ii_wp_selection input.search-name').attr('category');
        var type_2 = $('.ii_wp_selection input.search-name').attr('type_2');
        var type_specimen_c = $('.ii_wp_selection input.search-name').attr('type_specimen_c');
        var composition = $('.ii_wp_selection input.search-name').attr('composition');
        var action = $('.ii_wp_selection input.search-name').attr('action');
        var ICName = $('.ii_wp_selection input.search-name').attr('ic_name');
        var role_Type = $('.ii_wp_selection input.search-name').attr('roleType');
        if (role_Type == 'admin') {
            var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
        } else {
            var ICByRole = '';
        }

        var typespecimen = type_specimen_c.split(',');
        // console.log('88',typespecimen);
        var typeString = "";
        if (type_specimen_c != "") {
            //var type_specimen_Arr_String = type_specimen_c.split(',');
            var type_specimen_Arr = [];
            $ii = 0;
            $.each(typespecimen, function (index, value) {
                var strVal = value.replace(/,\s*$/, "");
                type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                $ii++;
            });
            typeString = type_specimen_Arr.join('')
            //console.log('typeString',typeString);
        }

        var APIURL = "";
        if (related == "Work Product") {
            if (composition == "Inventory Collection") {
                $('.ii_wp_selection thead .checkall input').prop('checked', false);
                if (category != "" && category != null && wpName != "") {

                    APIURL = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&order_by=name:asc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + wpName + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + wpName + "&filter[0][$and][0][$or][1][$or][2][name][$contains]=" + wp_search + "&filter[0][$and][0][$or][1][$or][3][internal_barcode][$contains]=" + wp_search + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
                }
                if (category != "" && category != null && wpName != "" && action == "Separate Inventory Items" && ICName != "") {

                    APIURL = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&order_by=name:asc&filter[0][$and][0][$or][1][$or][2][name][$contains]=" + wp_search + "&filter[0][$and][0][$or][1][$or][3][internal_barcode][$contains]=" + wp_search + "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$equals]=" + ICName, null;
                }

            } else {
                if (category != "" && category != null && composition === "Inventory Item" && wpName != "" && type_2 != "") {
                    APIURL = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&order_by=name:asc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + wpName + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + wpName + "&filter[0][$and][0][$or][1][$or][2][name][$contains]=" + wp_search + "&filter[0][$and][0][$or][1][$or][3][internal_barcode][$contains]=" + wp_search + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
                }

            }

        }

        app.api.call("read", APIURL, null, {
            success: function successFunc(ii_sales) {
                console.log("successFunc call", ii_sales);
                $(".ii_wp_selection .table").html('');
                var data = "";
                data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><th class="sorting" data-fieldname="internal_barcode" data-orderby="" tabindex="-1"><span>Internal Barcode</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                $.each(ii_sales.records, function (key, item) {

                    if (item.name != '') {
                        data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td><td data-type="text" data-column="internal_barcode"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.internal_barcode + '">' + item.internal_barcode + '</div></span></td></tr>';
                    }

                });
                data += '</tbody>';
                $('.ii_wp_selection .table').append(data);

            }.bind(this),
            error: function (error) {
                console.log("error call");
            }
        });
        }, 1000); 
    });
    /** Sidecar search code if category is specimen  */
    /** 28 Feb 2021 : #2101 changes : Search by timepoint when category = specimen **/
    $(document).on("keyup", ".ii_specimen_selection input.search-name", function () {
        console.log("In Keyup DOP1");
        clearTimeout(timer);
        timer = setTimeout(() => {
        var specimen_search = $('.ii_specimen_selection input.search-name').val();
        console.log('specimen search process by timepoint line 1132', specimen_search);
        var related = $('.input[name="related_to"]').val();

        $(".ii_specimen_selection").css('width', "100%");
        $(".ii_specimen_selection .table").html('');
        var related = $('.ii_specimen_selection input.search-name').attr('related_to_c');
        console.log("related", related);
        var wpName = $('.ii_specimen_selection input.search-name').attr('wpName');
        var saleId = $('.ii_specimen_selection input.search-name').attr('saleName');
        var category = $('.ii_specimen_selection input.search-name').attr('category');
        var type_2 = $('.ii_specimen_selection input.search-name').attr('type_2');
        var type_specimen_c = $('.ii_specimen_selection input.search-name').attr('type_specimen_c');
        var composition = $('.ii_specimen_selection input.search-name').attr('composition');
        var action = $('.ii_specimen_selection input.search-name').attr('action');
        var ICName = $('.ii_specimen_selection input.search-name').attr('ic_name');
        var role_Type = $('.ii_specimen_selection input.search-name').attr('roleType');
        console.log('type_specimen_c line 1140', type_specimen_c);
        var typespecimen = type_specimen_c.split(',');
        console.log('typespecimen line 1141', typespecimen);

        var typeString = "";
        if (type_specimen_c != "") {
            //var type_specimen_Arr_String = type_specimen_c.split(',');
            var type_specimen_Arr = [];
            $ii = 0;
            $.each(typespecimen, function (index, value) {
                var strVal = value.replace(/,\s*$/, "");
                type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                $ii++;
            });
            typeString = type_specimen_Arr.join('')
            //console.log('typeString',typeString);
        }
        if (role_Type == 'admin') {
            var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
        } else {
            var ICByRole = '';
        }
        console.log('specimen search process by wpName line 1153', wpName);
        console.log('specimen search process by type_specimen_c line 1154', type_specimen_c);
        console.log('specimen search process by category line 1155', category);
        var APIURL = "";
        if (related == "Work Product") {
            if (composition == "Inventory Collection") {
                $('.ii_specimen_selection thead .checkall input').prop('checked', false);

                if (category != "" && category != null && wpName != "") {

                    APIURL = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&order_by=name:asc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + wpName + "&filter[0][$and][0][$or][1][$or][3][name][$contains]=" + specimen_search + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + typeString + ICByRole, null;
                }

                if (category != "" && category != null && wpName != "" && action == "Separate Inventory Items" && ICName != "") {

                    APIURL = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&order_by=name:asc&filter[0][$and][0][$or][1][$or][2][name][$contains]=" + specimen_search + "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$equals]=" + ICName + typeString, null;
                }

            } else {

                if (category != "" && category != null && composition === "Inventory Item" && wpName != "") {
                    APIURL = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&order_by=name:asc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + wpName + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + wpName + "&filter[0][$and][0][$or][1][$or][2][name][$contains]=" + specimen_search + "&filter[0][$and][0][$or][1][$or][3][name][$contains]=" + specimen_search + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + typeString + ICByRole, null;
                }
                else {
                    if (category != "" && category != null && composition === "Inventory Item" && wpName != "" && type_2 != "") {
                        APIURL = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&order_by=name:asc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + wpName + "&filter[0][$and][0][$or][1][$or][3][name][$contains]=" + specimen_search + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
                    }

                }


            }

        }
        else if (related == "Sales") {

            if (category != "" && category != null && composition === "Inventory Item" && saleId != "") {

                APIURL = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&filter[0][related_to_c][$equals]=Sales&filter[0][$and][0][$and][1][$or][0][name][$contains]=" + specimen_search + "&filter[0][$and][0][$and][0][m01_sales_ii_inventory_item_1_name][$starts]=" + saleId + "&filter[0][$and][0][$and][1][category][$starts]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
            }


        } else if (related == "Internal Use") {

            if (category != "" && category != null && composition === "Inventory Item") {

                APIURL = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&filter[0][$and][0][$and][1][$or][0][name][$contains]=" + specimen_search + "&filter[0][related_to_c][$in][]=Internal Use&filter[0][related_to_c][$in][]=Order Request Item&filter[0][related_to_c][$in][]=Purchase Order Item&filter[0][category][$equals]=" + category + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
            }


        }

        app.api.call("read", APIURL, null, {
            success: function successFunc(ii_specimen) {
                console.log("successFunc call", ii_specimen);
                $(".ii_specimen_selection .table").html('');
                var data = "";
                data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><th class="sorting" data-fieldname="collection_date" data-orderby="" tabindex="-1"><span>Collection Date</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                $.each(ii_specimen.records, function (key, item) {

                    if (item.name != '') {
                        data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td><td data-type="text" data-column="collection_date"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.collection_date + '">' + item.collection_date + '</div></span></td></tr>';
                    }

                });
                data += '</tbody>';
                $('.ii_specimen_selection .table').append(data);

            }.bind(this),
            error: function (error) {
                console.log("error call");
            }
        });
            },1000);
    });
    /**///////////////////////////////////////////////////////////////////////////// */
    /** Sidecar search code on close button click */
    $(document).on("click", ".sicon.sicon-close.add-on", function () {
        $('.showalert').html('');
        var composition = $('div[data-name="composition"] .select2-chosen').text();
        var related_to_c = $('div[data-name="related_to"] .select2-chosen').text();
        var category_name = $('div[data-name="category"] .select2-chosen').text();

        if (related_to_c == 'Sales') {
            if (category_name == 'Specimen') {
                var selection_cls = 'ii_specimen_selection';
            }
            else {
                var selection_cls = 'ii_sales_selection';
            }
            $('.' + selection_cls + ' input.search-name').val('');

            var saleId = $('.' + selection_cls + ' input.search-name').attr('saleName');
            var category = $('.' + selection_cls + ' input.search-name').attr('category');
            var type_2 = $('.' + selection_cls + ' input.search-name').attr('type_2');
            var type_specimen_c = $('.' + selection_cls + ' input.search-name').attr('type_specimen_c');
            var composition = $('.' + selection_cls + ' input.search-name').attr('composition');
            var role_Type = $('.' + selection_cls + ' input.search-name').attr('roleType');
            if (role_Type == 'admin') {
                var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
            } else {
                var ICByRole = '';
            }

            var typespecimen = type_specimen_c.split(',');
            // console.log('88',typespecimen);
            var typeString = "";
            if (type_specimen_c != "") {
                //var type_specimen_Arr_String = type_specimen_c.split(',');
                var type_specimen_Arr = [];
                $ii = 0;
                $.each(typespecimen, function (index, value) {
                    var strVal = value.replace(/,\s*$/, "");
                    type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                    $ii++;
                });
                typeString = type_specimen_Arr.join('')
                //console.log('typeString',typeString);
            }

            $("." + selection_cls + "").css('width', "100%");
            $("." + selection_cls + " .table").html('');

            var APIURL = "";

            if (type_specimen_c != "" && category == "Specimen") {
                if (category != "" && category != null && composition === "Inventory Item" && saleId != "") {
                    APIURL = "rest/v10/II_Inventory_Item?fields=name,collection_date&offset=0&order_by=name%3Adesc&max_num=20&filter[0][related_to_c][$equals]=Sales&filter[0][m01_sales_ii_inventory_item_1_name][$equals]=" + saleId + "&filter[0][category][$equals]=" + category + typeString + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
                }
            } else {
                if (category != "" && category != null && composition === "Inventory Item" && saleId != "") {
                    APIURL = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&filter[0][related_to_c][$equals]=Sales&filter[0][$and][0][$and][0][m01_sales_ii_inventory_item_1_name][$starts]=" + saleId + "&filter[0][$and][0][$and][1][category][$starts]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
                }
            }

            if (category == "Specimen") {
                app.api.call("read", APIURL, null, {
                    success: function successFunc(ii_specimen) {
                        console.log("successFunc call", ii_specimen);
                        $(".ii_specimen_selection .table").html('');
                        var data = "";
                        data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><th class="sorting" data-fieldname="collection_date" data-orderby="" tabindex="-1"><span>Collection Date</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                        $.each(ii_specimen.records, function (key, item) {

                            if (item.name != '') {
                                data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td><td data-type="text" data-column="collection_date"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.collection_date + '">' + item.collection_date + '</div></span></td></tr>';
                            }

                        });
                        data += '</tbody>';
                        $('.ii_specimen_selection .table').append(data);

                    }.bind(this),
                    error: function (error) {
                        console.log("error call");
                    }
                });
            }
            else {
                app.api.call("read", APIURL, null, {
                    success: function successFunc(ii_sales) {
                        console.log("successFunc call", ii_sales);
                        $("." + selection_cls + " .table").html('');
                        var data = "";
                        data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><th class="sorting" data-fieldname="internal_barcode" data-orderby="" tabindex="-1"><span>Internal Barcode</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                        $.each(ii_sales.records, function (key, item) {

                            if (item.name != '') {
                                data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span sclass="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td><td data-type="text" data-column="internal_barcode"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.internal_barcode + '">' + item.internal_barcode + '</div></span></td></tr>';
                            }

                        });
                        data += '</tbody>';
                        $('.' + selection_cls + ' .table').append(data);

                    }.bind(this),
                    error: function (error) {
                        console.log("error call");
                    }
                });
            }




        } else if (related_to_c == 'Internal Use') {
            if (category_name == 'Specimen') {
                var selection_cls = 'ii_specimen_selection';
            }
            else {
                var selection_cls = 'ii_internal_selection';
            }
            $('.' + selection_cls + ' input.search-name').val('');

            $("." + selection_cls + "").css('width', "100%");
            $("." + selection_cls + " .table").html('');
            var category = $('.' + selection_cls + ' input.search-name').attr('category');
            var type_2 = $('.' + selection_cls + ' input.search-name').attr('type_2');
            var type_specimen_c = $('.' + selection_cls + ' input.search-name').attr('type_specimen_c');
            var composition = $('.' + selection_cls + ' input.search-name').attr('composition');
            var role_Type = $('.' + selection_cls + ' input.search-name').attr('roleType');
            if (role_Type == 'admin') {
                var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
            } else {
                var ICByRole = '';
            }

            var typespecimen = type_specimen_c.split(',');
            // console.log('88',typespecimen);
            var typeString = "";
            if (type_specimen_c != "") {
                //var type_specimen_Arr_String = type_specimen_c.split(',');
                var type_specimen_Arr = [];
                $ii = 0;
                $.each(typespecimen, function (index, value) {
                    var strVal = value.replace(/,\s*$/, "");
                    type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                    $ii++;
                });
                typeString = type_specimen_Arr.join('')
                //console.log('typeString',typeString);
            }

            var APIURL = "";
            if (type_specimen_c != "" && category == "Specimen") {
                if (category != "" && category != null && composition === "Inventory Item") {
                    APIURL = "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&offset=0&order_by=name%3Adesc&filter[0][related_to_c][$in][]=Internal Use&filter[0][related_to_c][$in][]=Order Request Item&filter[0][related_to_c][$in][]=Purchase Order Item&filter[0][category][$equals]=" + category + typeString + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
                }
            } else {
                if (category != "" && category != null && composition === "Inventory Item" && type_2 != "") {
                    APIURL = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&filter[0][related_to_c][$in][]=Internal Use&filter[0][related_to_c][$in][]=Order Request Item&filter[0][related_to_c][$in][]=Purchase Order Item&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
                }
            }

            if (category == "Specimen") {
                app.api.call("read", APIURL, null, {
                    success: function successFunc(ii_specimen) {
                        console.log("successFunc call", ii_specimen);
                        $(".ii_specimen_selection .table").html('');
                        var data = "";
                        data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><th class="sorting" data-fieldname="collection_date" data-orderby="" tabindex="-1"><span>Collection Date</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                        $.each(ii_specimen.records, function (key, item) {

                            if (item.name != '') {
                                data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td><td data-type="text" data-column="collection_date"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.collection_date + '">' + item.collection_date + '</div></span></td></tr>';
                            }

                        });
                        data += '</tbody>';
                        $('.ii_specimen_selection .table').append(data);

                    }.bind(this),
                    error: function (error) {
                        console.log("error call");
                    }
                });
            }
            else {
                app.api.call("read", APIURL, null, {
                    success: function successFunc(ii_internal) {
                        console.log("successFunc call", ii_internal);
                        $("." + selection_cls + " .table").html('');
                        var data = "";
                        data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><th class="sorting" data-fieldname="internal_barcode" data-orderby="" tabindex="-1"><span>Internal Barcode</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                        $.each(ii_internal.records, function (key, item) {

                            if (item.name != '') {
                                data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td><td data-type="text" data-column="internal_barcode"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.internal_barcode + '">' + item.internal_barcode + '</div></span></td></tr>';
                            }

                        });
                        data += '</tbody>';
                        $('.' + selection_cls + ' .table').append(data);

                    }.bind(this),
                    error: function (error) {
                        console.log("error call");
                    }
                });
            }


        } else if (related_to_c == 'Work Product') {
            if (category_name == 'Specimen') {
                var selection_cls = 'ii_specimen_selection';
            }
            else {
                var selection_cls = 'ii_wp_selection';
            }
            console.log('related to wp line 1378');
            console.log('category value line 1379', category_name);
            var composition = $('.' + selection_cls + ' input.search-name').attr('composition');
            if (composition == 'Inventory Collection') {
                $('.' + selection_cls + ' thead .checkall input').prop('checked', false);
                $('.' + selection_cls + ' input.search-name').val('');
                var wpName = $('.' + selection_cls + ' input.search-name').attr('wpName');

                $("." + selection_cls + "").css('width', "100%");
                $("." + selection_cls + " .table").html('');
                var specimen_search = $('' + selection_cls + ' input.search-name').val();
                var category = $('.' + selection_cls + ' input.search-name').attr('category');
                var action = $('.' + selection_cls + ' input.search-name').attr('action');
                var ICName = $('.' + selection_cls + ' input.search-name').attr('ic_name');
                var type_2 = $('.' + selection_cls + ' input.search-name').attr('type_2');
                var type_specimen_c = $('.' + selection_cls + ' input.search-name').attr('type_specimen_c');
                var role_Type = $('.' + selection_cls + ' input.search-name').attr('roleType');
                if (role_Type == 'admin') {
                    var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
                } else {
                    var ICByRole = '';
                }

                var typespecimen = type_specimen_c.split(',');
                // console.log('88',typespecimen);
                var typeString = "";
                if (type_specimen_c != "") {
                    //var type_specimen_Arr_String = type_specimen_c.split(',');
                    var type_specimen_Arr = [];
                    $ii = 0;
                    $.each(typespecimen, function (index, value) {
                        var strVal = value.replace(/,\s*$/, "");
                        type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                        $ii++;
                    });
                    typeString = type_specimen_Arr.join('')
                    //console.log('typeString',typeString);
                }
                var APIURL = "";

                if (type_specimen_c != "" && category == "Specimen") {
                    if (category != "" && category != null && wpName != "") {
                        console.log('x no IC12');
                        APIURL = "rest/v10/II_Inventory_Item?fields=name,collection_date&offset=0&max_num=20&order_by=name%3Adesc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + wpName + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + wpName + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + typeString + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
                    }


                } else {
                    if (category != "" && category != null && wpName != "") {
                        console.log('x no IC');
                        APIURL = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&order_by=name:asc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + wpName + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + wpName + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + role_Type, null;
                    }

                    if (action == "Separate Inventory Items" && ICName != "") {
                        console.log('x IC');
                        APIURL = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&order_by=name:asc&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$equals]=" + ICName, null;
                    }
                }



                if (category == "Specimen") {
                    app.api.call("read", APIURL, null, {
                        success: function successFunc(ii_specimen) {
                            console.log("successFunc call", ii_specimen);
                            $(".ii_specimen_selection .table").html('');
                            var data = "";
                            data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><th class="sorting" data-fieldname="collection_date" data-orderby="" tabindex="-1"><span>Collection Date</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                            $.each(ii_specimen.records, function (key, item) {

                                if (item.name != '') {
                                    data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td><td data-type="text" data-column="collection_date"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.collection_date + '">' + item.collection_date + '</div></span></td></tr>';
                                }

                            });
                            data += '</tbody>';
                            $('.ii_specimen_selection .table').append(data);

                        }.bind(this),
                        error: function (error) {
                            console.log("error call");
                        }
                    });
                }
                else {
                    app.api.call("read", APIURL, null, {
                        success: function successFunc(ii_sales) {
                            console.log("successFunc call", ii_sales);
                            $("." + selection_cls + " .table").html('');
                            var data = "";
                            data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><th class="sorting" data-fieldname="internal_barcode" data-orderby="" tabindex="-1"><span>Internal Barcode</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                            $.each(ii_sales.records, function (key, item) {

                                if (item.name != '') {
                                    data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td><td data-type="text" data-column="internal_barcode"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.internal_barcode + '">' + item.internal_barcode + '</div></span></td></tr>';
                                }

                            });
                            data += '</tbody>';
                            $('.' + selection_cls + ' .table').append(data);

                        }.bind(this),
                        error: function (error) {
                            console.log("error call");
                        }
                    });
                }
            } else {
                $('.' + selection_cls + ' input.search-name').val('');
                var wpName = $('.' + selection_cls + ' input.search-name').attr('wpName');

                $("." + selection_cls + "").css('width', "100%");
                $("." + selection_cls + " .table").html('');
                var category = $('.' + selection_cls + ' input.search-name').attr('category');
                var type_2 = $('.' + selection_cls + ' input.search-name').attr('type_2');
                var type_specimen_c = $('.' + selection_cls + ' input.search-name').attr('type_specimen_c');
                var role_Type = $('.' + selection_cls + ' input.search-name').attr('roleType');
                if (role_Type == 'admin') {
                    var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
                } else {
                    var ICByRole = '';
                }

                var typespecimen = type_specimen_c.split(',');
                // console.log('88',typespecimen);
                var typeString = "";
                if (type_specimen_c != "") {
                    //var type_specimen_Arr_String = type_specimen_c.split(',');
                    var type_specimen_Arr = [];
                    $ii = 0;
                    $.each(typespecimen, function (index, value) {
                        var strVal = value.replace(/,\s*$/, "");
                        type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                        $ii++;
                    });
                    typeString = type_specimen_Arr.join('')
                    //console.log('typeString',typeString);
                }

                var APIURL = "";

                if (type_specimen_c != "" && category == "Specimen") {
                    if (category != "" && category != null && composition === "Inventory Item" && wpName != "") {
                        APIURL = "rest/v10/II_Inventory_Item?fields=name,collection_date&offset=0&max_num=20&order_by=name%3Adesc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + wpName + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + wpName + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + typeString + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
                    }
                } else {
                    if (category != "" && category != null && composition === "Inventory Item" && wpName != "") {
                        APIURL = "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&order_by=name:asc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + wpName + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + wpName + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null;
                    }
                }

                if (category == "Specimen") {
                    app.api.call("read", APIURL, null, {
                        success: function successFunc(ii_specimen) {
                            console.log("successFunc call", ii_specimen);
                            $(".ii_specimen_selection .table").html('');
                            var data = "";
                            data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><th class="sorting" data-fieldname="collection_date" data-orderby="" tabindex="-1"><span>Collection Date</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                            $.each(ii_specimen.records, function (key, item) {

                                if (item.name != '') {
                                    data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td><td data-type="text" data-column="collection_date"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.collection_date + '">' + item.collection_date + '</div></span></td></tr>';
                                }

                            });
                            data += '</tbody>';
                            $('.ii_specimen_selection .table').append(data);

                        }.bind(this),
                        error: function (error) {
                            console.log("error call");
                        }
                    });
                }
                else {
                    app.api.call("read", APIURL, null, {
                        success: function successFunc(ii_sales) {
                            console.log("successFunc call", ii_sales);
                            $("." + selection_cls + " .table").html('');
                            var data = "";
                            data += '<thead><tr><th><span><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><div class="btn checkall" data-check="all" tabindex="-1" style="margin-left:0px;"><input type="checkbox" name="check" class="toggle-all checkAllst subtypecls" onchange="onChangeSearch(this);" id="checkallii"></div></span></div></span></span></th><th class="sorting" data-fieldname="name" data-orderby="" tabindex="-1"><span>Name</span></th><th class="sorting" data-fieldname="internal_barcode" data-orderby="" tabindex="-1"><span>Internal Barcode</span></th></tr><tr class="showalertcls" style="display:none;"><th data-target="alert-container" class="alert1 alert-warning" colspan="3"><div data-target="alert"><span class="showalert" style="display:none;"></span></div></th></tr></thead><tbody>';
                            $.each(ii_sales.records, function (key, item) {

                                if (item.name != '') {
                                    data += '<tr name="II_Inventory_Item_' + item.id + '" class="single" tabindex="-1"><td data-column=""><span class="fieldset list"><div class="fieldset-field"><span class="fieldset actions actionmenu list"><input data-check="one" type="checkbox" name="check" class="checkchk"></span></div></span></td><td data-type="name" data-column="name"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.name + '">' + item.name + '</div></span></td><td data-type="text" data-column="internal_barcode"><span class="list"><div class="ellipsis_inline" data-placement="bottom" title="" data-original-title="' + item.internal_barcode + '">' + item.internal_barcode + '</div></span></td></tr>';
                                }

                            });
                            data += '</tbody>';
                            $('.' + selection_cls + ' .table').append(data);

                        }.bind(this),
                        error: function (error) {
                            console.log("error call");
                        }
                    });
                }


            }
        }
    });

    app.events.on("app:sync:complete", function appSyncComplete() {
        if (!app.view.views.BaseIM_Inventory_ManagementCreateView && !app.view.views.BaseIM_Inventory_ManagementCustomCreateView) {
            app.view.declareComponent("view", "create", "IM_Inventory_Management", undefined, false, "base");
        }
        var createView = "BaseIM_Inventory_ManagementCreateView";
        if (app.view.views.BaseIM_Inventory_ManagementCustomCreateView) {
            createView = "BaseIM_Inventory_ManagementCustomCreateView";
        }
        if (App.view.views[createView].createExtended === true) {
            return;
        }

        App.view.views[createView] = App.view.views[createView].extend({
            createExtended: true,
            initialize: function () {
                this._super("initialize", arguments);

                this.model.on("change:related_to", this._hideSidebarMainPane, this);
                this.model.on("change:m01_sales_im_inventory_management_1_name", this.showWorkProductsOfSale, this);
                this.model.on("change:m03_work_product_im_inventory_management_1_name", this.resetWorkProducts, this);

                this.model.on("change:category", this._function_category, this);
                this.model.on("change:m03_work_product_im_inventory_management_1_name", this._function_work_product, this);
                this.model.on("change:type_2", this._function_type_2, this);
                this.model.on("change:type_specimen_c", this._function_type_specimen, this);

                //this.model.on("change:barcode_scan_c", this._function_barcode_scan_c, this);
                //this.events['keyup input[name=barcode_scan_c]'] = 'selectBarcode';
                this.model.on("change:ic_inventory_collection_im_inventory_management_1_name", this.function_inventory_collection, this);
                this.model.on("change:action", this.function_action, this);
                this.model.on("change:composition", this.function_composition, this);

                this.model.addValidationTask('validate_condition', _.bind(this._doValidateCheck, this));
            },
            function_composition: function () {

                if (this.model.get("composition") === "Inventory Collection") {
                    this.model.set("category", "Specimen");
                    //this.model.set("type_2", "Tissue");
                    //$('div[data-name="type_2"]').css('pointer-events', 'none');
                    $('div[data-name="category"]').css('pointer-events', 'none');

                    if (this.model.get("action") === "Create Inventory Collection") {
                        this.model.set("type_2", "");
                        //$('div[data-name="type_2"]').css('pointer-events', 'unset');
                        //$('input[name="type_2"]').prop('disabled', false);
                    }

                } else {
                    $('div[data-name="type_2"]').css('pointer-events', 'unset');
                    $('div[data-name="category"]').css('pointer-events', 'unset');
                    $('input[name="category"]').prop('disabled', false);
                    $('input[name="type_2"]').prop('disabled', false);
                    this.model.set("category", null);
                    this.model.set("type_2", null);
                }
            },
            _doValidateCheck: function (fields, errors, callback) {
                //validate type requirements
                if (this.model.get('account_type') == 'Customer' && _.isEmpty(this.model.get('phone_office'))) {
                    errors['phone_office'] = errors['phone_office'] || {};
                    errors['phone_office'].required = true;
                }

                callback(null, fields, errors);
            },
            function_action: function () {

                if (this.model.get("composition") === "Inventory Collection") {
                    this.model.set("type_2", "");
                    var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                    var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                    ii_wpView.collection.reset();
                    ii_wpView.render();
                    this.model.set("ic_inventoe24election_ida", null);
                    this.model.set("ic_inventory_collection_im_inventory_management_1_name", null);
                    this.model.set("m03_work_product_im_inventory_management_1m03_work_product_ida", null);
                    this.model.set("m03_work_product_im_inventory_management_1_name", null);

                    $('.checkall').trigger('click');
                    $('.checkall input[name="check"]').prop('checked', false);
                    $('input[name="check"]').trigger('click');
                    $('input[name="check"]').prop('checked', false);

                }

            },
            function_inventory_collection: function () {

                var related_to_c = this.model.get("related_to");

                var action = this.model.get("action");
                var ICId = this.model.get("ic_inventoe24election_ida");
                var ICName = this.model.get('ic_inventory_collection_im_inventory_management_1_name');

                var roleType = app.user.attributes.type;

                if (roleType == 'admin') {
                    var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
                } else {
                    var ICByRole = '';
                }

                if (ICId == "") {
                    $('.ii_wp_selection thead .checkall input').prop('checked', false);
                    this._function_type_2();
                }
                var category = this.model.get("category");
                var type_2 = this.model.get("type_2");
                var composition = this.model.get("composition");

                if (composition === "Inventory Collection" && action == "Separate Inventory Items") {

                    if ($(".drawer.active").find(".sidebar-content").hasClass("side-collapsed") === true) {
                        this.layout.layout.toggleSidePane();
                    }

                    var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                    var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                    ii_wpView.collection.reset();
                    ii_wpView.render();

                    this.layout.layout.$el.find('[data-component=sidebar] .ii_wp_selection input.search-name').val('');
                    this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").show();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").hide();
                    this.layout.layout.$el.find(".ii_wp_selection").css("width", "100%");

                    app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&order_by=name:asc&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$equals]=" + ICName, null, {
                        success: function successFunc(ii_wp) {
                            console.log("barcode ii_wp_IC", ii_wp);
                            var AllIIIdArr = [];
                            if (ii_wp.records.length > 0) {
                                $.each(ii_wp.records, function (key, II_ids_item) {
                                    console.log(II_ids_item);
                                    var itemIds = II_ids_item.id;
                                    AllIIIdArr.push(itemIds);
                                });
                                var IIres = AllIIIdArr.join('$$')
                                this.model.set("ii_ids_hidden_c", IIres);
                                console.log('IIid ', IIres);
                            }

                            var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                            var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                            var ii_wpModels = [];
                            var selectediiwpViewModels = ii_wpView.massCollection;
                            if (ii_wp.records && ii_wp.records.length > 0) {
                                ii_wpModels = ii_wp.records;
                            }
                            ii_wpView.collection.reset();
                            ii_wpView.collection.add(ii_wpModels);
                            ii_wpView.render();

                            $('.ii_wp_selection').find(".search-name").attr('name', 'wpsearch');
                            $('.ii_wp_selection').find(".search-name").attr('category', category);
                            $('.ii_wp_selection').find(".search-name").attr('related_to_c', related_to_c);
                            $('.ii_wp_selection').find(".search-name").attr('type_2', type_2);
                            $('.ii_wp_selection').find(".search-name").attr('composition', composition);
                            $('.ii_wp_selection').find(".search-name").attr('ic_name', ICName);
                            $('.ii_wp_selection').find(".search-name").attr('action', action);
                            $('.ii_wp_selection').find(".search-name").attr('roleType', roleType);

                            this.layout.layout.$el.find("input[name=check]").prop('chekced', false);
                            this.$('input[name="check"]').prop('checked', false);
                            $('.ii_wp_selection').find('input[name="check"]').prop('checked', false);
                            $('.ii_sales_selection').find('input[name="check"]').prop('checked', false);
                            $('.ii_internal_selection').find('input[name="check"]').prop('checked', false);

                            var inventory_items_ids = [];
                            inventory_items_ids = _.map(selectediiwpViewModels.models, function (model) {
                                return model.set("id", "");
                            });

                            if (ii_wp.records.length > 0) {
                                $('.ii_wp_selection thead .checkall input').prop('checked', true);
                                $('.ii_wp_selection thead .checkall input').trigger('click');
                                $('.ii_wp_selection thead .checkall').trigger('click');
                            } else {
                                $('.ii_wp_selection thead .checkall input').prop('checked', false);
                            }
                        }.bind(this),
                        error: function (error) {
                            app.alert.show("ii_wp_error", {
                                level: "error",
                                messages: "Record not available",
                                autoClose: true
                            });
                        }
                    });
                }
            },
            selectBarcode: function () {
                var self = this;
                setTimeout(function () {
                    var txt = "";
                    var value = $("input[name=barcode_scan_c]").val();
                    var length = value.length;
                    //console.log(length);
                    if (txt != $("input[name=barcode_scan_c]").val()) {
                        self._function_barcode_scan_c(value);
                    }
                }, 500);
            },
            _function_barcode_scan_c: function (barcode_scan_c) {

                var category = this.model.get("category");

                var related_to_c = this.model.get("related_to");
                var composition = this.model.get("composition");
                var type_2 = this.model.get("type_2");
                var composition = this.model.get("composition");
                var collections = this.model.get("ic_inventory_collection_im_inventory_management_1_name");
                var saleId = this.model.get("m01_sales_im_inventory_management_1_name");
                var workProduct = this.model.get('m03_work_product_im_inventory_management_1_name');

                if (related_to_c === "Work Product" && composition === "Inventory Item" && category != "" && workProduct != "" && type_2 != "") {

                    app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&order_by=name:asc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][internal_barcode][$equals]=" + barcode_scan_c + "&filter[0][status][$not_in][]=Exhausted&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''", null, {
                        success: function successFunc(ii_wp) {
                            //console.log("barcode ii_wp", ii_wp);
                            if (ii_wp.records == "") {
                                app.alert.show("ii_internal_error", {
                                    level: "error",
                                    messages: "Record not available.",
                                    autoClose: true
                                });

                                $('input[name="barcode_scan_c"]').val('');
                            } else {

                                var recordInList = "II_Inventory_Item_" + ii_wp.records[0].id;
                                var sidecarRecord = $('.ii_wp_selection table tbody tr[name="' + recordInList + '"]').attr('name');
                                if (recordInList == sidecarRecord) {
                                    //$('.ii_wp_selection table tbody tr[name="' + sidecarRecord + '"] .actionmenu input').addClass('myCheckbox');
                                    $('.ii_wp_selection table tbody tr[name="' + sidecarRecord + '"] .actionmenu input').attr('id', 'myCheckbox' + sidecarRecord);
                                    $('#myCheckbox' + sidecarRecord).prop('checked', true);
                                    $('#myCheckbox' + sidecarRecord).trigger('click');
                                    $('.ii_wp_selection table tbody tr[name="' + sidecarRecord + '"]').trigger('click');
                                    $('input[name="barcode_scan_c"]').val('');
                                }
                            }
                        }.bind(this),
                        error: function (error) {
                            app.alert.show("ii_wp_error", {
                                level: "error",
                                messages: "Record not available",
                                autoClose: true
                            });
                        }
                    });
                } else if (related_to_c === "Sales" && composition === "Inventory Item" && category != "" && saleId != "" && type_2 != "") {

                    app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&filter[0][related_to_c][$equals]=Sales&filter[0][m01_sales_ii_inventory_item_1_name][$equals]=" + saleId + "&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][internal_barcode][$equals]=" + barcode_scan_c + "&filter[0][status][$not_in][]=Exhausted&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''", null, {
                        success: function successFunc(ii_sales) {
                            //console.log("barcode ii_sales", ii_sales);
                            if (ii_sales.records == "") {
                                app.alert.show("ii_internal_error", {
                                    level: "error",
                                    messages: "Record not available.",
                                    autoClose: true
                                });
                                $('input[name="barcode_scan_c"]').val('');
                            } else {

                                var recordInList = "II_Inventory_Item_" + ii_sales.records[0].id;
                                var sidecarRecord = $('.ii_sales_selection table tbody tr[name="' + recordInList + '"]').attr('name');

                                if (recordInList == sidecarRecord) {
                                    //$('.ii_sales_selection table tbody tr[name="' + sidecarRecord + '"] .actionmenu input').addClass('myCheckbox');
                                    $('.ii_sales_selection table tbody tr[name="' + sidecarRecord + '"] .actionmenu input').attr('id', 'myCheckbox' + sidecarRecord);
                                    $('#myCheckbox' + sidecarRecord).prop('checked', true);
                                    $('#myCheckbox' + sidecarRecord).trigger('click');
                                    $('.ii_sales_selection table tbody tr[name="' + sidecarRecord + '"]').trigger('click');
                                    $('input[name="barcode_scan_c"]').val('');
                                }
                            }
                        }.bind(this),
                        error: function (error) {
                            app.alert.show("ii_sales_error", {
                                level: "error",
                                messages: "Record not available",
                                autoClose: true
                            });
                        }
                    });
                } else if (related_to_c === "Internal Use" && composition === "Inventory Item" && category != "" && type_2 != "") {

                    app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&filter[0][related_to_c][$in][]=Internal Use&filter[0][related_to_c][$in][]=Order Request Item&filter[0][related_to_c][$in][]=Purchase Order Item&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][internal_barcode][$equals]=" + barcode_scan_c + "&filter[0][status][$not_in][]=Exhausted&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''", null, {
                        success: function successFunc(ii_internal) {
                            //console.log("barcode ii_internal", ii_internal);
                            if (ii_internal.records == "") {
                                app.alert.show("ii_internal_error", {
                                    level: "error",
                                    messages: "Record not available.",
                                    autoClose: true
                                });
                                $('input[name="barcode_scan_c"]').val('');
                            } else {

                                var recordInList = "II_Inventory_Item_" + ii_internal.records[0].id;
                                var sidecarRecord = $('.ii_internal_selection table tbody tr[name="' + recordInList + '"]').attr('name');

                                if (recordInList == sidecarRecord) {
                                    //$('.ii_internal_selection table tbody tr[name="' + sidecarRecord + '"] .actionmenu input').addClass('myCheckbox');
                                    $('.ii_internal_selection table tbody tr[name="' + sidecarRecord + '"] .actionmenu input').attr('id', 'myCheckbox' + sidecarRecord);
                                    $('#myCheckbox' + sidecarRecord).prop('checked', true);
                                    $('#myCheckbox' + sidecarRecord).trigger('click');
                                    $('.ii_internal_selection table tbody tr[name="' + sidecarRecord + '"]').trigger('click');
                                    $('input[name="barcode_scan_c"]').val('');
                                }
                            }

                        }.bind(this),
                        error: function (error) {

                        }
                    });
                } else if (composition === "Inventory Collection" && category == "Specimen" && type_2 == "Tissue") {

                    app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&order_by=name:asc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][internal_barcode][$equals]=" + barcode_scan_c + "&filter[0][status][$not_in][]=Exhausted&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''", null, {
                        success: function successFunc(ii_wp) {
                            //console.log("barcode ii_wp_IC", ii_wp);
                            if (ii_wp.records == "") {
                                app.alert.show("ii_internal_error", {
                                    level: "error",
                                    messages: "Record not available.",
                                    autoClose: true
                                });
                                $('input[name="barcode_scan_c"]').val('');
                            } else {

                                var recordInList = "II_Inventory_Item_" + ii_wp.records[0].id;
                                var sidecarRecord = $('.ii_wp_selection table tbody tr[name="' + recordInList + '"]').attr('name');

                                if (recordInList == sidecarRecord) {
                                    //$('.ii_wp_selection table tbody tr[name="' + sidecarRecord + '"] .actionmenu input').addClass('myCheckbox');
                                    $('.ii_wp_selection table tbody tr[name="' + sidecarRecord + '"] .actionmenu input').attr('id', 'myCheckbox' + sidecarRecord);
                                    $('#myCheckbox' + sidecarRecord).prop('checked', true);
                                    $('#myCheckbox' + sidecarRecord).trigger('click');
                                    $('.ii_wp_selection table tbody tr[name="' + sidecarRecord + '"]').trigger('click');
                                    $('input[name="barcode_scan_c"]').val('');
                                }
                            }
                        }.bind(this),
                        error: function (error) {
                            app.alert.show("ii_wp_error", {
                                level: "error",
                                messages: "Record not available",
                                autoClose: true
                            });
                        }
                    });
                } else {
                    app.alert.show("ii_wp_error", {
                        level: "error",
                        messages: "Please fill all requirment before barcode scan.",
                        autoClose: true
                    });
                    $('input[name="barcode_scan_c"]').val('');
                }
                //this.model.set("barcode_scan_c",null); 
            },
            _function_type_2: function () {
                var self = this;
                $('button[data-action="show-more-ii"]').remove();
                $('button[data-action="show-more-iisale"]').remove();
                $('button[data-action="show-more-iiwp"]').remove();
                offsetIM = 0;
                orderTypeIM = "desc";
                lastSortedColumnNameIM = "name";
                appendQueryIM = "&order_by=name%3Adesc";
                if ($(".drawer.active").find(".sidebar-content").hasClass("side-collapsed") === true) {
                    this.layout.layout.toggleSidePane();
                }

                ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                ii_wpView.collection.reset();
                ii_wpView.render();


                ii_salesLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-sales-selection");
                ii_salesView = ii_salesLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-sales-selection");
                ii_salesView.collection.reset();
                ii_salesView.render();


                internalLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-internal-selection");
                internalView = internalLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-internal-selection");
                internalView.collection.reset();
                internalView.render();

                this.model.set("ic_inventoe24election_ida", null);
                this.model.set("ic_inventory_collection_im_inventory_management_1_name", null);

                var action = this.model.get("action");
                var category = this.model.get("category");
                var related_to_c = this.model.get("related_to");
                var type_2 = this.model.get("type_2");
                var type_specimen_c = this.model.get("type_specimen_c");
                var composition = this.model.get("composition");
                var collections = this.model.get("ic_inventory_collection_im_inventory_management_1_name");
                var saleId = this.model.get("m01_sales_im_inventory_management_1_name");
                var workProduct = this.model.get('m03_work_product_im_inventory_management_1_name');
                var roleType = app.user.attributes.type;

                if (roleType == 'admin') {
                    var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
                } else {
                    var ICByRole = '';
                }
                console.log('roleType', roleType);
                if (related_to_c === "Work Product") {
                    var ICName = this.model.get('ic_inventory_collection_im_inventory_management_1_name');
                    $('button[data-action="show-more-wpii"]').remove();
                    $('button[data-action="show-more-wpic"]').remove();
                    if (composition === "Inventory Item" && category != "" && category != "Specimen" && workProduct != "" && type_2 != "") {
                        offsetIM = 0;
                        console.log('not specimen line 1680');
                        moduleNameIM = "II_Inventory_Item_WP_II";
                        this.layout.layout.$el.find('[data-component=sidebar] .ii_wp_selection input.search-name').val('');
                        this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").show();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_specimen_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").hide();
                        this.layout.layout.$el.find(".ii_wp_selection").css("width", "100%");

                        app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&offset=0&max_num=20&order_by=name%3Adesc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null, {
                            success: function successFunc(ii_wp) {

                                console.log("ii_wp", ii_wp);
                                offsetIM = 20;
                                ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                                ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                                var ii_wpModels = [];
                                var selectediiwpViewModels = ii_wpView.massCollection;
                                if (ii_wp.records && ii_wp.records.length > 0) {
                                    ii_wpModels = ii_wp.records;
                                }
                                ii_wpView.collection.reset();
                                ii_wpView.collection.add(ii_wpModels);
                                ii_wpView.render();
                                $('div.main-pane.ii_wp_selection div.flex-list-view-content').css("overflow", "visible");

                                $('.ii_wp_selection').find(".search-name").attr('name', 'wpsearch');
                                $('.ii_wp_selection').find(".search-name").attr('wpName', workProduct);
                                $('.ii_wp_selection').find(".search-name").attr('category', category);
                                $('.ii_wp_selection').find(".search-name").attr('related_to_c', related_to_c);
                                $('.ii_wp_selection').find(".search-name").attr('type_2', type_2);
                                $('.ii_wp_selection').find(".search-name").attr('type_specimen_c', type_specimen_c);
                                $('.ii_wp_selection').find(".search-name").attr('composition', composition);
                                $('.ii_wp_selection').find(".search-name").attr('roleType', roleType);

                                this.layout.layout.$el.find("input[name=check]").prop('chekced', false);
                                this.$('input[name="check"]').prop('checked', false);
                                $('.ii_wp_selection').find('input[name="check"]').prop('checked', false);
                                $('.ii_sales_selection').find('input[name="check"]').prop('checked', false);
                                $('.ii_internal_selection').find('input[name="check"]').prop('checked', false);

                                var inventory_items_ids = [];
                                inventory_items_ids = _.map(selectediiwpViewModels.models, function (model) {
                                    return model.set("id", "");
                                });

                                setTimeout(function () {
                                    console.log($('button[data-action="show-more-wpii"]').length);
                                    if ($('button[data-action="show-more-wpii"]').length == 0) {
                                        if (ii_wp.records.length > 19) {
                                            $("div.main-pane.ii_wp_selection div.main-content").append(`
                                <div>
                                    <button data-action="show-more-wpii" class="btn btn-link btn-invisible more padded">More Inventory Items...</button>
                                </div>
                                <div class="block-footer loading-ori hide">
                                  <div class="loading">
                                      Loading...
                                  </div>
                                </div>
                                `);
                                        }
                                    }
                                }, 500);

                            }.bind(this),
                            error: function (error) {
                                app.alert.show("ii_wp_error", {
                                    level: "error",
                                    messages: "Error retrieving Inventory Item for this Work Product",
                                    autoClose: true
                                });
                            }
                        });
                    }
                    else if (composition === "Inventory Collection" && category == "Specimen" && action == "Create Inventory Collection") {
                        //&& type_2 == "Tissue" 
                        offsetIM = 0;
                        moduleNameIM = "II_Inventory_Item_WP_IC";
                        $('button[data-action="show-more-specimenic"]').remove();
                        this.layout.layout.$el.find('[data-component=sidebar] .ii_specimen_selection input.search-name').val('');
                        this.layout.layout.$el.find("[data-component=sidebar] .specimen_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_specimen_selection").show();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").hide();
                        this.layout.layout.$el.find(".ii_specimen_selection").css("width", "100%");


                        app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&offset=0&max_num=20&order_by=name%3Adesc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null, {
                            success: function successFunc(ii_specimen) {
                                offsetIM = 20;
                                console.log("ii_specimen_IC line 1847", ii_specimen);
                                ii_specimenLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-specimen-selection");
                                ii_specimenView = ii_specimenLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-specimen-selection");
                                var ii_specimenModels = [];
                                var selectediispecimenViewModels = ii_specimenView.massCollection;
                                if (ii_specimen.records && ii_specimen.records.length > 0) {
                                    ii_specimenModels = ii_specimen.records;
                                }
                                ii_specimenView.collection.reset();
                                ii_specimenView.collection.add(ii_specimenModels);
                                ii_specimenView.render();
                                $('div.main-pane.ii_specimen_selection div.flex-list-view-content').css("overflow", "visible");

                                $('.ii_specimen_selection .table').find(".sorting").removeClass('orderByname');
                                $('.ii_specimen_selection .table').find(".sorting").removeClass('orderByinternal_barcode');
                                $('.ii_specimen_selection').find(".search-name").attr('name', 'wpsearch');
                                $('.ii_specimen_selection').find(".search-name").attr('specimenName', workProduct);
                                $('.ii_specimen_selection').find(".search-name").attr('category', category);
                                $('.ii_specimen_selection').find(".search-name").attr('related_to_c', related_to_c);
                                $('.ii_specimen_selection').find(".search-name").attr('type_2', type_2);
                                $('.ii_specimen_selection').find(".search-name").attr('type_specimen_c', type_specimen_c);
                                $('.ii_specimen_selection').find(".search-name").attr('composition', composition);
                                $('.ii_specimen_selection').find(".search-name").attr('roleType', roleType);
                                $('.ii_specimen_selection').find(".search-name").attr('ic_name', ICName);

                                this.layout.layout.$el.find("input[name=check]").prop('chekced', false);
                                this.$('input[name="check"]').prop('checked', false);
                                $('.ii_specimen_selection').find('input[name="check"]').prop('checked', false);
                                $('.ii_sales_selection').find('input[name="check"]').prop('checked', false);
                                $('.ii_internal_selection').find('input[name="check"]').prop('checked', false);

                                var inventory_items_ids = [];
                                inventory_items_ids = _.map(selectediispecimenViewModels.models, function (model) {
                                    return model.set("id", "");
                                });

                                setTimeout(function () {
                                    console.log($('button[data-action="show-more-specimenic"]').length);
                                    if ($('button[data-action="show-more-specimenic"]').length == 0) {
                                        if (ii_specimen.records.length > 19) {
                                            $("div.main-pane.ii_specimen_selection div.main-content").append(`
                                <div>
                                    <button data-action="show-more-specimenic" class="btn btn-link btn-invisible more padded">More Inventory Items...</button>
                                </div>
                                <div class="block-footer loading-ori hide">
                                  <div class="loading">
                                      Loading...
                                  </div>
                                </div>
                                `);
                                        }
                                    }
                                }, 500);


                            }.bind(this),
                            error: function (error) {
                                app.alert.show("ii_specimen_error", {
                                    level: "error",
                                    messages: "Error retrieving Inventory Item for this Work Product",
                                    autoClose: true
                                });
                            }
                        });
                    }

                } else if (related_to_c === "Sales" && composition === "Inventory Item" && category != "" && category != "Specimen" && saleId != "" && type_2 != "") {
                    offsetIM = 0;
                    console.log("1077")
                    $('button[data-action="show-more-iisale"]').remove();
                    this.layout.layout.$el.find('[data-component=sidebar] .ii_sales_selection input.search-name').val('');
                    this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").show();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").hide();
                    this.layout.layout.$el.find(".ii_sales_selection").css("width", "100%");

                    app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&offset=0&order_by=name%3Adesc&max_num=20&filter[0][related_to_c][$equals]=Sales&filter[0][m01_sales_ii_inventory_item_1_name][$equals]=" + saleId + "&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null, {
                        success: function successFunc(ii_sales) {
                            offsetIM = 20;
                            console.log("ii_sales", ii_sales);
                            ii_salesLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-sales-selection");
                            ii_salesView = ii_salesLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-sales-selection");
                            var ii_salesModels = [];
                            var selectediisalesViewModels = ii_salesView.massCollection;
                            if (ii_sales.records && ii_sales.records.length > 0) {
                                ii_salesModels = ii_sales.records;
                            }
                            ii_salesView.collection.reset();
                            ii_salesView.collection.add(ii_salesModels);
                            ii_salesView.render();
                            $('div.main-pane.ii_sales_selection div.flex-list-view-content').css("overflow", "visible");

                            $('.ii_sales_selection .table').find(".sorting").removeClass('orderByname');
                            $('.ii_sales_selection .table').find(".sorting").removeClass('orderByinternal_barcode');
                            $('.ii_sales_selection').find(".search-name").attr('name', 'salesearch');
                            $('.ii_sales_selection').find(".search-name").attr('saleName', saleId);
                            $('.ii_sales_selection').find(".search-name").attr('category', category);
                            $('.ii_sales_selection').find(".search-name").attr('related_to_c', related_to_c);
                            $('.ii_sales_selection').find(".search-name").attr('type_2', type_2);
                            $('.ii_sales_selection').find(".search-name").attr('type_specimen_c', type_specimen_c);
                            $('.ii_sales_selection').find(".search-name").attr('composition', composition);
                            $('.ii_sales_selection').find(".search-name").attr('roleType', roleType);

                            this.layout.layout.$el.find("input[name=check]").prop('chekced', false);
                            this.$('input[name="check"]').prop('checked', false);
                            $('.ii_wp_selection').find('input[name="check"]').prop('checked', false);
                            $('.ii_sales_selection').find('input[name="check"]').prop('checked', false);
                            $('.ii_internal_selection').find('input[name="check"]').prop('checked', false);

                            var inventory_items_ids1 = [];
                            inventory_items_ids1 = _.map(selectediisalesViewModels.models, function (model) {
                                return model.set("id", "");
                            });

                            setTimeout(function () {
                                console.log($('button[data-action="show-more-iisale"]').length);
                                if ($('button[data-action="show-more-iisale"]').length == 0) {
                                    if (ii_sales.records.length > 19) {
                                        $("div.main-pane.ii_sales_selection div.main-content").append(`
                              <div>
                                  <button data-action="show-more-iisale" class="btn btn-link btn-invisible more padded">More Inventory Items...</button>
                              </div>
                              <div class="block-footer loading-ori hide">
                                <div class="loading">
                                    Loading...
                                </div>
                              </div>
                              `);
                                    }
                                }
                            }, 500);


                        }.bind(this),
                        error: function (error) {
                            app.alert.show("ii_sales_error", {
                                level: "error",
                                messages: "Error retrieving Inventory Item for this Work Product",
                                autoClose: true
                            });
                        }
                    });
                }
                else if (related_to_c === "Internal Use" && composition === "Inventory Item" && category != "" && category != "Specimen" && type_2 != "") {
                    offsetIM = 0;
                    this.layout.layout.$el.find('[data-component=sidebar] .ii_internal_selection input.search-name').val('');
                    this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").show();
                    this.layout.layout.$el.find(".ii_internal_selection").css("width", "100%");

                    app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,internal_barcode,storage_condition,allowed_storage_conditions,allowed_storage_medium,location_building,location_room,location_equipment,location_shelf,location_cabinet&max_num=20&offset=0&order_by=name%3Adesc&filter[0][related_to_c][$in][]=Internal Use&filter[0][related_to_c][$in][]=Order Request Item&filter[0][related_to_c][$in][]=Purchase Order Item&filter[0][category][$equals]=" + category + "&filter[0][type_2][$equals]=" + type_2 + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null, {
                        success: function successFunc(ii_internal) {
                            offsetIM = 20;
                            //console.log("ii_internal", ii_internal);
                            internalLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-internal-selection");
                            internalView = internalLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-internal-selection");
                            var internalModels = [];
                            var selectediiinternalModels = internalView.massCollection;
                            if (ii_internal.records && ii_internal.records.length > 0) {
                                internalModels = ii_internal.records;
                            }
                            internalView.collection.reset();
                            internalView.collection.add(internalModels);
                            internalView.render();

                            $('div.main-pane.ii_internal_selection div.flex-list-view-content').css("overflow", "visible");

                            $('.ii_internal_selection .table').find(".sorting").removeClass('orderByname');
                            $('.ii_internal_selection .table').find(".sorting").removeClass('orderByinternal_barcode');
                            $('.ii_internal_selection').find(".search-name").attr('name', 'internalsearch');
                            $('.ii_internal_selection').find(".search-name").attr('category', category);
                            $('.ii_internal_selection').find(".search-name").attr('related_to_c', related_to_c);
                            $('.ii_internal_selection').find(".search-name").attr('type_2', type_2);
                            $('.ii_internal_selection').find(".search-name").attr('type_specimen_c', type_specimen_c);
                            $('.ii_internal_selection').find(".search-name").attr('composition', composition);
                            $('.ii_internal_selection').find(".search-name").attr('roleType', roleType);

                            this.layout.layout.$el.find("input[name=check]").prop('chekced', false);
                            this.$('input[name="check"]').prop('checked', false);
                            $('.ii_wp_selection').find('input[name="check"]').prop('checked', false);
                            $('.ii_sales_selection').find('input[name="check"]').prop('checked', false);
                            $('.ii_internal_selection').find('input[name="check"]').prop('checked', false);


                            var inventory_items_ids2 = [];
                            inventory_items_ids2 = _.map(selectediiinternalModels.models, function (model) {
                                return model.set("id", "");
                            });

                            setTimeout(function () {
                                console.log($('button[data-action="show-more-ii"]').length);
                                if ($('button[data-action="show-more-ii"]').length == 0) {
                                    if (ii_internal.records.length > 19) {
                                        $("div.main-pane.ii_internal_selection div.main-content").append(`
                              <div>
                                  <button data-action="show-more-ii" class="btn btn-link btn-invisible more padded">More Inventory Items...</button>
                              </div>
                              <div class="block-footer loading-ori hide">
                                <div class="loading">
                                    Loading...
                                </div>
                              </div>
                              `);
                                    }
                                }
                            }, 500);


                        }.bind(this),
                        error: function (error) {
                            app.alert.show("ii_internal_error", {
                                level: "error",
                                messages: "Error retrieving Inventory Item for this Internal",
                                autoClose: true
                            });
                        }
                    });
                } else {
                    this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();

                }
            },
            _function_type_specimen: function () {
                var ICName = this.model.get('ic_inventory_collection_im_inventory_management_1_name');
                var category = this.model.get("category");
                $('.table thead th span').css('width', '170px !important');
                $('.table thead th span').css('width', '170px !important');
                $('.table thead th span').css('width', '170px !important');
                $('.table thead th span').css('width', '170px !important');
                $('.table thead th span').css('width', '170px !important');
                $('.table thead th span').css('width', '170px !important');
                $('.table thead th span').css('width', '170px !important');
                $('.table thead th span').css('width', '170px !important');
                if (category == 'Specimen') {
                    var self = this;
                    $('button[data-action="show-more-ii"]').remove();
                    $('button[data-action="show-more-iisale"]').remove();
                    $('button[data-action="show-more-iiwp"]').remove();
                    offsetIM = 0;
                    orderTypeIM = "desc";
                    lastSortedColumnNameIM = "name";
                    appendQueryIM = "&order_by=name%3Adesc";
                    if ($(".drawer.active").find(".sidebar-content").hasClass("side-collapsed") === true) {
                        this.layout.layout.toggleSidePane();
                    }

                    ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                    ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                    ii_wpView.collection.reset();
                    ii_wpView.render();


                    ii_salesLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-sales-selection");
                    ii_salesView = ii_salesLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-sales-selection");
                    ii_salesView.collection.reset();
                    ii_salesView.render();


                    internalLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-internal-selection");
                    internalView = internalLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-internal-selection");
                    internalView.collection.reset();
                    internalView.render();
                    /**25 Feb 2022 Changes : #2021 : Gsingh */
                    ii_specimenLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-specimen-selection");
                    ii_specimenView = ii_specimenLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-specimen-selection");
                    ii_specimenView.collection.reset();
                    ii_specimenView.render();
                    /**25 Feb 2022 Changes : #2021 : Gsingh */
                    this.model.set("ic_inventoe24election_ida", null);
                    this.model.set("ic_inventory_collection_im_inventory_management_1_name", null);

                    var action = this.model.get("action");
                    var category = this.model.get("category");
                    var related_to_c = this.model.get("related_to");
                    var type_2 = this.model.get("type_2");
                    console.log('type_2 value line 2047', type_2);
                    var type_specimen_c = this.model.get("type_specimen_c");
                    var composition = this.model.get("composition");
                    var collections = this.model.get("ic_inventory_collection_im_inventory_management_1_name");
                    var saleId = this.model.get("m01_sales_im_inventory_management_1_name");
                    var workProduct = this.model.get('m03_work_product_im_inventory_management_1_name');
                    var roleType = app.user.attributes.type;

                    if (roleType == 'admin') {
                        var ICByRole = "&filter[0][ic_inventory_collection_ii_inventory_item_1_name][$empty]=''";
                    } else {
                        var ICByRole = '';
                    }
                    var typeString = "";
                    if (type_specimen_c != "") {
                        //var type_specimen_Arr_String = type_specimen_c.split(',');
                        var type_specimen_Arr = [];
                        $ii = 0;
                        $.each(type_specimen_c, function (index, value) {
                            var strVal = value.replace(/,\s*$/, "");
                            type_specimen_Arr.push('&filter[1][type_2][$in][]=' + strVal);
                            $ii++;
                        });
                        typeString = type_specimen_Arr.join('')
                        console.log('typeString', typeString);
                    }



                    console.log('roleType', roleType);
                    if (related_to_c === "Work Product") {
                        console.log('type_2 value line 2075', type_2);
                        console.log('related_to_c line 2072', related_to_c);
                        $('button[data-action="show-more-wpii"]').remove();
                        $('button[data-action="show-more-wpic"]').remove();
                        if (composition === "Inventory Item" && category != "" && workProduct != "") {
                            console.log('new speciment category sidecar changes line 2076');
                            console.log('category line 2076', category);
                            offsetIM = 0;
                            /**25 Feb 2022 Changes : #2021 : Gsingh */
                            moduleNameIM = "II_Inventory_Item_WP_II";
                            this.layout.layout.$el.find('[data-component=sidebar] .ii_specimen_selection input.search-name').val('');
                            this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                            this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").hide();
                            this.layout.layout.$el.find("[data-component=sidebar] .ii_specimen_selection").show();
                            this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").hide();
                            this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").hide();
                            this.layout.layout.$el.find(".ii_specimen_selection").css("width", "100%");
                            console.log('typeString value line 2092', typeString);
                            app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,collection_date&offset=0&max_num=20&order_by=name%3Adesc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + typeString + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null, {
                                success: function successFunc(ii_specimen) {

                                    console.log("ii_specimen line 2092", ii_specimen);
                                    console.log('timepoint_type line 2097', ii_specimen.records.timepoint_type);
                                    offsetIM = 20;
                                    ii_specimenLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-specimen-selection");
                                    ii_specimenView = ii_specimenLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-specimen-selection");
                                    var ii_specimenModels = [];
                                    var selectediispecimenViewModels = ii_specimenView.massCollection;
                                    if (ii_specimen.records && ii_specimen.records.length > 0) {
                                        ii_specimenModels = ii_specimen.records;
                                    }
                                    ii_specimenView.collection.reset();
                                    ii_specimenView.collection.add(ii_specimenModels);
                                    ii_specimenView.render();
                                    $('div.main-pane.ii_specimen_selection div.flex-list-view-content').css("overflow", "visible");
                                    $('.ii_specimen_selection').find(".search-name").attr('name', 'wpsearch');
                                    $('.ii_specimen_selection').find(".search-name").attr('wpName', workProduct);
                                    $('.ii_specimen_selection').find(".search-name").attr('category', category);
                                    $('.ii_specimen_selection').find(".search-name").attr('related_to_c', related_to_c);
                                    $('.ii_specimen_selection').find(".search-name").attr('type_2', type_2);
                                    $('.ii_specimen_selection').find(".search-name").attr('type_specimen_c', type_specimen_c);
                                    $('.ii_specimen_selection').find(".search-name").attr('action', action);
                                    $('.ii_specimen_selection').find(".search-name").attr('composition', composition);

                                    $('.ii_specimen_selection').find(".search-name").attr('roleType', roleType);
                                    $('.ii_specimen_selection').find(".search-name").attr('ic_name', ICName);
                                    /**To set custom value based on type or other fields data */
                                    /*  $.each(ii_specimen.records, function (i, IISpecimen) {
                                         console.log('IISpecimen type_2 values line 2399', IISpecimen.type_2);
                                         console.log('IISpecimen test_type_c values line 2401', IISpecimen.test_type_c);
                                         console.log('IISpecimen timepoint_type values line 2402', IISpecimen.timepoint_type);
                                         //ICLibkedII.push(IIid.id);
                                     }); */
                                    this.layout.layout.$el.find("input[name=check]").prop('chekced', false);
                                    this.$('input[name="check"]').prop('checked', false);
                                    $('.ii_specimen_selection').find('input[name="check"]').prop('checked', false);
                                    $('.ii_sales_selection').find('input[name="check"]').prop('checked', false);
                                    $('.ii_internal_selection').find('input[name="check"]').prop('checked', false);

                                    var inventory_items_ids = [];
                                    inventory_items_ids = _.map(selectediispecimenViewModels.models, function (model) {
                                        return model.set("id", "");
                                    });

                                    setTimeout(function () {
                                        console.log($('button[data-action="show-more-specimenii"]').length);
                                        if ($('button[data-action="show-more-specimenii"]').length == 0) {
                                            if (ii_specimen.records.length > 19) {
                                                $("div.main-pane.ii_specimen_selection div.main-content").append(`
                                    <div>
                                        <button data-action="show-more-specimenii" class="btn btn-link btn-invisible more padded">More Inventory Items...</button>
                                    </div>
                                    <div class="block-footer loading-ori hide">
                                    <div class="loading">
                                        Loading...
                                    </div>
                                    </div>
                                    `);
                                            }
                                        }
                                    }, 500);

                                }.bind(this),
                                error: function (error) {
                                    app.alert.show("ii_specimen_error", {
                                        level: "error",
                                        messages: "Error retrieving Inventory Item for this Work Product",
                                        autoClose: true
                                    });
                                }
                            });
                        }
                        else if (composition === "Inventory Collection" && action == "Create Inventory Collection") {
                            //&& type_2 == "Tissue" 
                            offsetIM = 0;
                            moduleNameIM = "II_Inventory_Item_WP_IC";
                            $('button[data-action="show-more-wpic"]').remove();
                            $('button[data-action="show-more-specimenic"]').hide();
                            this.layout.layout.$el.find('[data-component=sidebar] .ii_specimen_selection input.search-name').val('');
                            this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                            this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").hide();
                            this.layout.layout.$el.find("[data-component=sidebar] .ii_specimen_selection").show();
                            this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").hide();
                            this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").hide();
                            this.layout.layout.$el.find(".ii_specimen_selection").css("width", "100%");
                            /**25 Feb 2022 Changes : #2021 : Gsingh */
                            app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,collection_date&offset=0&max_num=20&order_by=name%3Adesc&filter[0][$and][0][$and][1][$or][0][m03_work_product_ii_inventory_item_1_name][$equals]=" + workProduct + "&filter[0][$and][0][$and][1][$or][1][m03_work_product_ii_inventory_item_1.name][$equals]=" + workProduct + "&filter[0][related_to_c][$in][]=Sales&filter[0][related_to_c][$in][]=Work Product&filter[0][related_to_c][$in][]=Single Test System&filter[0][related_to_c][$in][]=Multiple Test Systems&filter[0][category][$equals]=" + category + typeString + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null, {
                                success: function successFunc(ii_specimen) {
                                    offsetIM = 20;
                                    console.log("ii_specimen_IC line 2176", ii_specimen);
                                    ii_specimenLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-specimen-selection");
                                    ii_specimenView = ii_specimenLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-specimen-selection");
                                    var ii_specimenModels = [];
                                    var selectediispecimenViewModels = ii_specimenView.massCollection;
                                    if (ii_specimen.records && ii_specimen.records.length > 0) {
                                        ii_specimenModels = ii_specimen.records;
                                    }
                                    ii_specimenView.collection.reset();
                                    ii_specimenView.collection.add(ii_specimenModels);
                                    ii_specimenView.render();
                                    $('div.main-pane.ii_specimen_selection div.flex-list-view-content').css("overflow", "visible");

                                    $('.ii_specimen_selection .table').find(".sorting").removeClass('orderByname');
                                    $('.ii_specimen_selection .table').find(".sorting").removeClass('orderByinternal_barcode');
                                    $('.ii_specimen_selection').find(".search-name").attr('name', 'wpsearch');
                                    $('.ii_specimen_selection').find(".search-name").attr('wpName', workProduct);
                                    $('.ii_specimen_selection').find(".search-name").attr('category', category);
                                    $('.ii_specimen_selection').find(".search-name").attr('related_to_c', related_to_c);
                                    $('.ii_specimen_selection').find(".search-name").attr('type_2', type_2);
                                    $('.ii_specimen_selection').find(".search-name").attr('type_specimen_c', type_specimen_c);
                                    $('.ii_specimen_selection').find(".search-name").attr('action', action);
                                    $('.ii_specimen_selection').find(".search-name").attr('composition', composition);

                                    $('.ii_specimen_selection').find(".search-name").attr('roleType', roleType);
                                    /* $.each(ii_specimen.records, function (i, IISpecimen) {
                                        console.log('IISpecimen type_2 values line 2485', IISpecimen.type_2);
                                        console.log('IISpecimen test_type_c values line 2486', IISpecimen.test_type_c);
                                        console.log('IISpecimen timepoint_type values line 2487', IISpecimen.timepoint_type);
                                        //ICLibkedII.push(IIid.id);
                                    }); */

                                    /**25 Feb 2022 Changes : #2021 : Gsingh */
                                    //  $('.ii_specimen_selection').find(".search-name").attr('timepoint_type', timepoint_type);                                   
                                    /**25 Feb 2022 Changes : #2021 : Gsingh */
                                    this.layout.layout.$el.find("input[name=check]").prop('chekced', false);
                                    this.$('input[name="check"]').prop('checked', false);
                                    $('.ii_specimen_selection').find('input[name="check"]').prop('checked', false);
                                    $('.ii_sales_selection').find('input[name="check"]').prop('checked', false);
                                    $('.ii_internal_selection').find('input[name="check"]').prop('checked', false);

                                    var inventory_items_ids = [];
                                    inventory_items_ids = _.map(selectediispecimenViewModels.models, function (model) {
                                        return model.set("id", "");
                                    });

                                    setTimeout(function () {
                                        console.log($('button[data-action="show-more-specimenic"]').length);
                                        if ($('button[data-action="show-more-specimenic"]').length == 0) {
                                            if (ii_specimen.records.length > 19) {
                                                $("div.main-pane.ii_specimen_selection div.main-content").append(`
                                    <div>
                                        <button data-action="show-more-specimenic" class="btn btn-link btn-invisible more padded">More Inventory Items...</button>
                                    </div>
                                    <div class="block-footer loading-ori hide">
                                    <div class="loading">
                                        Loading...
                                    </div>
                                    </div>
                                    `);
                                            }
                                        }
                                    }, 500);


                                }.bind(this),
                                error: function (error) {
                                    app.alert.show("ii_specimen_error", {
                                        level: "error",
                                        messages: "Error retrieving Inventory Item for this Work Product",
                                        autoClose: true
                                    });
                                }
                            });
                        }
                    } else if (related_to_c === "Sales" && composition === "Inventory Item" && category != "" && saleId != "") {
                        offsetIM = 0;
                        console.log("1077")
                        $('button[data-action="show-more-iisale"]').remove();
                        this.layout.layout.$el.find('[data-component=sidebar] .ii_sales_selection input.search-name').val('');
                        this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_specimen_selection").show();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").hide();
                        this.layout.layout.$el.find(".ii_specimen_selection").css("width", "100%");
                        /**25 Feb 2022 Changes : #2021 : Gsingh */
                        app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,collection_date&offset=0&order_by=name%3Adesc&max_num=20&filter[0][related_to_c][$equals]=Sales&filter[0][m01_sales_ii_inventory_item_1_name][$equals]=" + saleId + "&filter[0][category][$equals]=" + category + typeString + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null, {
                            success: function successFunc(ii_specimen) {
                                offsetIM = 20;
                                console.log("ii_specimen line 2259", ii_specimen);
                                ii_specimenLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-specimen-selection");
                                ii_specimenView = ii_specimenLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-specimen-selection");
                                var ii_specimenModels = [];
                                var selectediispecimenViewModels = ii_specimenView.massCollection;
                                if (ii_specimen.records && ii_specimen.records.length > 0) {
                                    ii_specimenModels = ii_specimen.records;
                                }
                                ii_specimenView.collection.reset();
                                ii_specimenView.collection.add(ii_specimenModels);
                                ii_specimenView.render();
                                $('div.main-pane.ii_specimen_selection div.flex-list-view-content').css("overflow", "visible");

                                $('.ii_specimen_selection .table').find(".sorting").removeClass('orderByname');
                                $('.ii_specimen_selection .table').find(".sorting").removeClass('orderByinternal_barcode');
                                $('.ii_specimen_selection').find(".search-name").attr('name', 'salesearch');
                                $('.ii_specimen_selection').find(".search-name").attr('saleName', saleId);
                                $('.ii_specimen_selection').find(".search-name").attr('category', category);
                                $('.ii_specimen_selection').find(".search-name").attr('related_to_c', related_to_c);
                                $('.ii_specimen_selection').find(".search-name").attr('type_2', type_2);
                                $('.ii_specimen_selection').find(".search-name").attr('type_specimen_c', type_specimen_c);
                                $('.ii_specimen_selection').find(".search-name").attr('action', action);
                                $('.ii_specimen_selection').find(".search-name").attr('composition', composition);

                                $('.ii_specimen_selection').find(".search-name").attr('roleType', roleType);
                                /* $.each(ii_specimen.records, function (i, IISpecimen) {
                                    console.log('IISpecimen type_2 values line 2574', IISpecimen.type_2);
                                    console.log('IISpecimen test_type_c values line 2575', IISpecimen.test_type_c);
                                    console.log('IISpecimen timepoint_type values line 2576', IISpecimen.timepoint_type);
                                    //ICLibkedII.push(IIid.id);
                                }); */
                                /**25 Feb 2022 Changes : #2021 : Gsingh */
                                // $('.ii_specimen_selection').find(".search-name").attr('timepoint_type', timepoint_type);
                                this.layout.layout.$el.find("input[name=check]").prop('chekced', false);
                                this.$('input[name="check"]').prop('checked', false);
                                $('.ii_wp_selection').find('input[name="check"]').prop('checked', false);
                                $('.ii_specimen_selection').find('input[name="check"]').prop('checked', false);
                                $('.ii_internal_selection').find('input[name="check"]').prop('checked', false);

                                var inventory_items_ids1 = [];
                                inventory_items_ids1 = _.map(selectediispecimenViewModels.models, function (model) {
                                    return model.set("id", "");
                                });

                                setTimeout(function () {
                                    console.log($('button[data-action="show-more-iisale"]').length);
                                    if ($('button[data-action="show-more-iisale"]').length == 0) {
                                        if (ii_specimen.records.length > 19) {
                                            $("div.main-pane.ii_specimen_selection div.main-content").append(`
                                <div>
                                    <button data-action="show-more-iispecimen" class="btn btn-link btn-invisible more padded">More Inventory Items...</button>
                                </div>
                                <div class="block-footer loading-ori hide">
                                    <div class="loading">
                                        Loading...
                                    </div>
                                </div>
                                `);
                                        }
                                    }
                                }, 500);


                            }.bind(this),
                            error: function (error) {
                                app.alert.show("ii_specimens_error", {
                                    level: "error",
                                    messages: "Error retrieving Inventory Item for this Sale",
                                    autoClose: true
                                });
                            }
                        });
                    } else if (related_to_c === "Internal Use" && composition === "Inventory Item" && category != "") {
                        console.log("related_to_c line 2329", related_to_c);
                        offsetIM = 0;
                        this.layout.layout.$el.find('[data-component=sidebar] .ii_internal_selection input.search-name').val('');
                        this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").hide();
                        this.layout.layout.$el.find("[data-component=sidebar] .ii_specimen_selection").show();

                        this.layout.layout.$el.find(".ii_specimen_selection").css("width", "100%");
                        /**25 Feb 2022 Changes : #2021 : Gsingh */
                        app.api.call("read", "rest/v10/II_Inventory_Item?fields=name,collection_date&max_num=20&offset=0&order_by=name%3Adesc&filter[0][related_to_c][$in][]=Internal Use&filter[0][related_to_c][$in][]=Order Request Item&filter[0][related_to_c][$in][]=Purchase Order Item&filter[0][category][$equals]=" + category + typeString + "&filter[0][status][$not_in][]=Exhausted" + ICByRole, null, {
                            success: function successFunc(ii_specimen) {
                                offsetIM = 20;
                                console.log("ii_specimen line 2341", ii_specimen);
                                specimenLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-specimen-selection");
                                specimenView = specimenLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-specimen-selection");
                                var specimenModels = [];
                                var selectediispecimenModels = specimenView.massCollection;
                                if (ii_specimen.records && ii_specimen.records.length > 0) {
                                    specimenModels = ii_specimen.records;
                                }
                                specimenView.collection.reset();
                                specimenView.collection.add(specimenModels);
                                specimenView.render();

                                $('div.main-pane.ii_specimen_selection div.flex-list-view-content').css("overflow", "visible");

                                $('.ii_specimen_selection .table').find(".sorting").removeClass('orderByname');
                                $('.ii_specimen_selection .table').find(".sorting").removeClass('orderByinternal_barcode');
                                $('.ii_specimen_selection').find(".search-name").attr('name', 'internalsearch');
                                $('.ii_specimen_selection').find(".search-name").attr('category', category);
                                $('.ii_specimen_selection').find(".search-name").attr('related_to_c', related_to_c);
                                $('.ii_specimen_selection').find(".search-name").attr('type_2', type_2);
                                $('.ii_specimen_selection').find(".search-name").attr('type_specimen_c', type_specimen_c);
                                $('.ii_specimen_selection').find(".search-name").attr('action', action);
                                $('.ii_specimen_selection').find(".search-name").attr('composition', composition);

                                /**25 Feb 2022 Changes : #2021 : Gsingh */
                                //  $('.ii_specimen_selection').find(".search-name").attr('timepoint_type', timepoint_type);                                 
                                this.layout.layout.$el.find("input[name=check]").prop('chekced', false);
                                this.$('input[name="check"]').prop('checked', false);
                                $('.ii_wp_selection').find('input[name="check"]').prop('checked', false);
                                $('.ii_sales_selection').find('input[name="check"]').prop('checked', false);
                                $('.ii_specimen_selection').find('input[name="check"]').prop('checked', false);


                                var inventory_items_ids2 = [];
                                inventory_items_ids2 = _.map(selectediispecimenModels.models, function (model) {
                                    return model.set("id", "");
                                });

                                setTimeout(function () {
                                    console.log($('button[data-action="show-more-ii"]').length);
                                    if ($('button[data-action="show-more-ii"]').length == 0) {
                                        if (ii_specimen.records.length > 19) {
                                            $("div.main-pane.ii_specimen_selection div.main-content").append(`
                                <div>
                                    <button data-action="show-more-ii" class="btn btn-link btn-invisible more padded">More Inventory Items...</button>
                                </div>
                                <div class="block-footer loading-ori hide">
                                    <div class="loading">
                                        Loading...
                                    </div>
                                </div>
                                `);
                                        }
                                    }
                                }, 500);


                            }.bind(this),
                            error: function (error) {
                                app.alert.show("ii_specimen_error", {
                                    level: "error",
                                    messages: "Error retrieving Inventory Item for this Internal",
                                    autoClose: true
                                });
                            }
                        });
                    } else {
                        this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();

                    }
                }
            },
            /**07 March 2021 : #2101 changes */
            _function_category: function () {
                var category = this.model.get("category");
                console.log('in fun _function_category  line 2849', category);
                var related_to = this.model.get("related_to");
                this.model.set('type_specimen_c', '');
                this.model.set('type_2', '');
                $('button[data-action="show-more-ii"]').remove();
                $('button[data-action="show-more-iisale"]').remove();
                $('button[data-action="show-more-iiwp"]').remove();
                offsetIM = 0;
                orderTypeIM = "desc";
                lastSortedColumnNameIM = "name";
                appendQueryIM = "&order_by=name%3Adesc";
                if ($(".drawer.active").find(".sidebar-content").hasClass("side-collapsed") === true) {
                    this.layout.layout.toggleSidePane();
                }

                ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                ii_wpView.collection.reset();
                ii_wpView.render();


                ii_salesLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-sales-selection");
                ii_salesView = ii_salesLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-sales-selection");
                ii_salesView.collection.reset();
                ii_salesView.render();


                internalLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-internal-selection");
                internalView = internalLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-internal-selection");
                internalView.collection.reset();
                internalView.render();

                specimenLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-specimen-selection");
                specimenView = specimenLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-specimen-selection");
                specimenView.collection.reset();
                specimenView.render();

                if (related_to === "Work Product") {
                    console.log('in fun _function_category related_to line 2882', related_to);
                    $('button[data-action="show-more-wpii"]').remove();
                    $('button[data-action="show-more-wpic"]').remove();
                    this.layout.layout.$el.find('[data-component=sidebar] .ii_specimen_selection input.search-name').val('');
                    this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").show();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_specimen_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").hide();
                    this.layout.layout.$el.find(".ii_specimen_selection").css("width", "100%");
                } else if (related_to === "Sales") {
                    offsetIM = 0;
                    $('button[data-action="show-more-iisale"]').remove();
                    this.layout.layout.$el.find('[data-component=sidebar] .ii_sales_selection input.search-name').val('');
                    this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_specimen_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").show();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").hide();
                    this.layout.layout.$el.find(".ii_specimen_selection").css("width", "100%");

                } else if (related_to === "Internal Use") {
                    offsetIM = 0;
                    this.layout.layout.$el.find('[data-component=sidebar] .ii_internal_selection input.search-name').val('');
                    this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").show();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_specimen_selection").hide();

                    this.layout.layout.$el.find(".ii_specimen_selection").css("width", "100%");
                    /**25 Feb 2022 Changes : #2021 : Gsingh */
                } else {
                    this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();

                }
            },
            /**////////////////////////////// */
            render: function () {
                this._super("render", arguments);

                this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();

                this.$el.find(".tab-pane.record-panel-content").css("min-height", "300px");
                this.$el.find(".sidebar-content .main-pane .checkall").css("margin-left", "0px");

            },
            resetWorkProducts: function () {

                if (this.model.get("composition") === "Inventory Collection") {
                    this.model.set("category", "Specimen");
                    //this.model.set("type_2", "Tissue");
                    //$('div[data-name="type_2"]').css('pointer-events', 'none');
                    $('div[data-name="category"]').css('pointer-events', 'none');
                    //$('input[name="type_2"]').prop('disabled', true);
                    $('input[name="category"]').prop('disabled', true);

                    if (this.model.get("action") === "Create Inventory Collection") {
                        //alert('11');
                        this.model.set("type_2", "");
                        //$('div[data-name="type_2"]').css('pointer-events', 'unset');
                        //$('input[name="type_2"]').prop('disabled', false);
                    } else {
                        //alert('111');
                        this.model.set("type_2", "Tissue");
                        this._function_type_2();
                    }
                    var self = this;
                    setTimeout(function () {
                        $('span[data-fieldname="type_2"] .select2-search-choice-close').remove();
                    }, 200);
                } else {
                    $('div[data-name="type_2"]').css('pointer-events', 'unset');
                    $('div[data-name="category"]').css('pointer-events', 'unset');
                    this.model.set("category", null);
                }

                var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                ii_wpView.collection.reset();
                ii_wpView.render();

                if ($(".drawer.active").find(".sidebar-content").hasClass("side-collapsed") === true) {
                    this.layout.layout.toggleSidePane();
                }

                this.model.set("ic_inventoe24election_ida", null);
                this.model.set("ic_inventory_collection_im_inventory_management_1_name", null);

                var workProduct = this.model.get('m03_work_product_im_inventory_management_1_name');
                var category = this.model.get('category');

                this.layout.layout.$el.find('[data-component=sidebar] .ii_wp_selection input.search-name').val('');
                var related_to_c = this.model.get("related_to");
                if (related_to_c === "Work Product") {

                    this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").show();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").hide();
                    this.layout.layout.$el.find(".ii_wp_selection").css("width", "100%");
                }
            },
            showWorkProductsOfSale: function (model) {
                console.log("In 1283")
                offsetIM = 0;
                orderTypeIM = "desc";
                lastSortedColumnNameIM = "name";
                appendQueryIM = "&order_by=name%3Adesc";
                this.model.set("category", null);
                if ($(".drawer.active").find(".sidebar-content").hasClass("side-collapsed") === true) {
                    this.layout.layout.toggleSidePane();
                }

                var saleId = this.model.get("m01_sales_im_inventory_management_1_name");
                this.layout.layout.$el.find('[data-component=sidebar] .ii_sales_selection input.search-name').val('');
                var related_to_c = this.model.get("related_to");
                var category = this.model.get("category");

                if (related_to_c === "Sales") {
                    console.log("1296")
                    this.layout.layout.$el.find("[data-component=sidebar] .wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_wp_selection").hide();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_sales_selection").show();
                    this.layout.layout.$el.find("[data-component=sidebar] .ii_internal_selection").hide();

                }
            },

            saveAndClose: function () {
                $("#alertsCustom").remove();

                selfI = this;
                console.log('1590 : ' + selfI.model.get("reviewed_record_c") + ' = ' + selfI.model.get("processed_per_protocol_c"))
                if (selfI.model.get("reviewed_record_c") == "No" && selfI.model.get("processed_per_protocol_c") == "No") {
                    $("#sidecar").prepend(`    
                        <div id="alertsCustom" class="alert-top">
                        <div class="alert-wrapper">
                            <div class="alert alert-warning alert-block closeable">
                                <div class="indicator">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </div>
                                <button class="close btn btn-link btn-invisible closeCustomIconCV" data-action="close"><i class="fa fa-times"></i></button>
                                <span class="text">
                            <strong>Warning: </strong>
                            Inventory Management not processed per protocol and record not reviewed.  Remember to submit a Communication.
                        </span>
                            </div>
                        </div>
                        </div>
                    `);
                } else if (selfI.model.get("reviewed_record_c") == "No") {
                    console.log('1592');
                    $("#sidecar").prepend(`    
                        <div id="alertsCustom" class="alert-top">
                        <div class="alert-wrapper">
                            <div class="alert alert-warning alert-block closeable">
                                <div class="indicator">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </div>
                                <button class="close btn btn-link btn-invisible closeCustomIconCV" data-action="close"><i class="fa fa-times"></i></button>
                                <span class="text">
                            <strong>Warning: </strong>
                            Inventory Management record not reviewed.  Remember to submit a Communication.
                        </span>
                            </div>
                        </div>
                        </div>
                    `);
                } else if (selfI.model.get("processed_per_protocol_c") == "No") {
                    console.log('1610');
                    $("#sidecar").prepend(`    
                        <div id="alertsCustom" class="alert-top">
                        <div class="alert-wrapper">
                            <div class="alert alert-warning alert-block closeable">
                                <div class="indicator">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </div>
                                <button class="close btn btn-link btn-invisible closeCustomIconCV" data-action="close"><i class="fa fa-times"></i></button>
                                <span class="text">
                            <strong>Warning: </strong>
                            Inventory Management not processed per protocol. Remember to submit a Communication.
                        </span>
                            </div>
                        </div>
                        </div>
                    `);
                } else {
                    console.log('1628');
                    selfI.saveInventoryManagementCRecord();
                }
            },

            saveInventoryManagementCRecord: function () {
                $("#alertsCustom").remove();
                //var isWPSelected = this._isWorkProductSelected();
                var related_to_c = this.model.get("related_to");
                var composition = this.model.get("composition");
                var action = this.model.get("action");
                var category = this.model.get("category");
                var ic_inventoe24election_ida = this.model.get('ic_inventoe24election_ida');


                if (related_to_c === "Internal Use") {

                    if (this.intenalUseSelected() == false) {
                        app.alert.show('no_ii_selected', {
                            level: 'error',
                            messages: 'No Inventory Item checked. Inventory Management cannot be saved.'
                        });
                        return;
                    }

                    if (this._isRelatedToInternalSelected() === true) {
                        this._validateIIcondition(related_to_c, composition);
                    } else {
                        app.alert.show('no_iiinternal_seletecd', {
                            level: 'error',
                            messages: 'A Inventory Item Internal Use must be linked!'
                        });
                        return;
                    }
                } else if (related_to_c === "Sales") {
                    if (this.salesSelected() == false) {
                        app.alert.show('no_ii_selected', {
                            level: 'error',
                            messages: 'No Inventory Item checked. Inventory Management cannot be saved.'
                        });
                        return;
                    }
                    if (this._isRelatedToSalesSelected() === true) {
                        this._validateIIcondition(related_to_c, composition);
                    } else {
                        app.alert.show('no_iisales_seletecd', {
                            level: 'error',
                            messages: 'A Inventory Item Sales must be linked!'
                        });
                        return;
                    }
                } else if (related_to_c === "Work Product") {
                    if (composition === "Inventory Collection") {
                        console.log('Inventory Collection');
                        if (this.wpIICollection() == false && (action == "Create Inventory Collection" || action == "Separate Inventory Items")) {
                            app.alert.show('no_ii_selected', {
                                level: 'error',
                                messages: 'No Inventory Item checked. Inventory Management cannot be saved.'
                            });
                            return;
                        } else {
                            var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                            var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                            var selectedii_wp = ii_wpView.massCollection;
                            if (selectedii_wp.length > 0) {
                                this._validateIIcondition(related_to_c, composition);
                            } else {
                                this._createIIItemforCollection();
                            }
                        }

                    } else {
                        console.log('Inventory Item');
                        if (this.wpSelected() == false) {
                            app.alert.show('no_ii_selected', {
                                level: 'error',
                                messages: 'No Inventory Item checked. Inventory Management cannot be saved.'
                            });
                            return;
                        }
                        if (this._isRelatedToWPSelected() === true) {
                            //alert("WP Jack1" + related_to_c);
                            this._validateIIcondition(related_to_c, composition);
                        } else {
                            app.alert.show('no_iiwp_seletecd', {
                                level: 'error',
                                messages: 'A Inventory Item Work Product must be linked!'
                            });
                            return;
                        }
                    }
                }
            },
            //-------------------Added By Sunil-----------------------
            _validateIIcondition: function (relatedItem, composition) {
                if (relatedItem == 'Work Product') {
                    if (composition == 'Inventory Collection') {
                        var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                        var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                    } else {
                        var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                        var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                    }
                } else if (relatedItem == 'Sales') {
                    var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-sales-selection");
                    var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-sales-selection");
                } else if (relatedItem == 'Internal Use') {
                    var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-internal-selection");
                    var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-internal-selection");
                }
                var selectedii_wp = ii_wpView.massCollection;
                if (selectedii_wp.length > 0) {
                    var inventory_items_ids = []
                    inventory_items_ids = _.map(selectedii_wp.models, function (model) {
                        if (model.get('id') != '') {
                            return model.get("id");
                        }
                    });
                    var url = app.api.buildURL("getItemValue");
                    var method = "create";
                    var data = {
                        ii_Id: inventory_items_ids
                    };
                    var mediumArr = [];
                    var endCondition = storageMediumArr = conditionArr = [];
                    var counter = counter1 = 0;
                    var callback = {
                        success: _.bind(function successCB(res) {
                            for (i = 0; i < res.length; i++) {
                                var storageMediumArr = res[i]['allowedStorageMedium'].split(",");
                                var conditionArr = res[i]['allowedStorageCondition'].split(",");

                                if (!(storageMediumArr.includes(this.model.get("ending_storage_media")))) {
                                    mediumArr[counter] = res[i]['name'];
                                    counter++;
                                }
                                if (!(conditionArr.includes(this.model.get("end_condition")))) {
                                    endCondition[counter1] = res[i]['name'];
                                    counter1++;
                                }
                            }
                            if (mediumArr.length > 0 && this.model.get("ending_storage_media") != "") {
                                var mediumStr = mediumArr.toString();
                                app.alert.show("medium_error", {
                                    level: "error",
                                    messages: "The End Storage Media for the Inventory Management is not an Allowed Storage Medium for below item, please review:  " + mediumStr,
                                    autoClose: false,
                                });
                            }
                            if (endCondition.length > 0 && this.model.get("end_condition") != "") {
                                var endStr = endCondition.toString();
                                app.alert.show("end_error", {
                                    level: "error",
                                    messages: "The End Condition for the Inventory Management is not an Allowed Storage Condition for below item, please review:  " + endStr,
                                    autoClose: false,
                                });
                            }
                            if (this.model.get("end_condition") == "" && this.model.get("ending_storage_media") == "") {
                                console.log('hello 1191');
                                this.initiateSave(_.bind(function initSave() {
                                    if (this.closestComponent("drawer")) {

                                        if (this.model.get('composition') == "Inventory Collection") {
                                            this._createICRecord(this.model.get('ic_inventoe24election_ida')).then(function thenFunc(result) {
                                                if (result === "success") {
                                                    //app.alert.dismiss("no_iionly");
                                                }
                                                //app.drawer.close(this.context, this.model);
                                            }
                                                .bind(this));
                                        } else {
                                            this._linkInventoryItem().then(function thenFunc(result) {
                                                if (result === "success") {
                                                    //app.alert.dismiss("no_iionly");
                                                }
                                                //app.drawer.close(this.context, this.model);
                                            }
                                                .bind(this));
                                        }


                                        app.drawer.close(this.context, this.model);
                                    }
                                }, this));
                            } else {
                                if (this.model.get("end_condition") != "" && this.model.get("ending_storage_media") == "" && endCondition.length <= 0) {
                                    console.log('hello 1206');
                                    this.initiateSave(_.bind(function initSave() {
                                        if (this.closestComponent("drawer")) {

                                            if (this.model.get('composition') == "Inventory Collection") {
                                                this._createICRecord(this.model.get('ic_inventoe24election_ida')).then(function thenFunc(result) {
                                                    if (result === "success") {
                                                        //app.alert.dismiss("no_iionly");
                                                    }
                                                    //app.drawer.close(this.context, this.model);
                                                }
                                                    .bind(this));
                                            } else {
                                                this._linkInventoryItem().then(function thenFunc(result) {
                                                    if (result === "success") {
                                                        //app.alert.dismiss("no_iionly");
                                                    }
                                                    //app.drawer.close(this.context, this.model);
                                                }
                                                    .bind(this));
                                            }

                                            app.drawer.close(this.context, this.model);
                                        }
                                    }, this));
                                } else if (this.model.get("end_condition") == "" && this.model.get("ending_storage_media") != "" && mediumArr.length <= 0) {
                                    console.log('hello 1220');
                                    this.initiateSave(_.bind(function initSave() {
                                        if (this.closestComponent("drawer")) {

                                            if (this.model.get('composition') == "Inventory Collection") {
                                                this._createICRecord(this.model.get('ic_inventoe24election_ida')).then(function thenFunc(result) {
                                                    if (result === "success") {
                                                        //app.alert.dismiss("no_iionly");
                                                    }
                                                    //app.drawer.close(this.context, this.model);
                                                }
                                                    .bind(this));
                                            } else {
                                                this._linkInventoryItem().then(function thenFunc(result) {
                                                    if (result === "success") {
                                                        //app.alert.dismiss("no_iionly");
                                                    }
                                                    //app.drawer.close(this.context, this.model);
                                                }
                                                    .bind(this));
                                            }

                                            app.drawer.close(this.context, this.model);
                                        }
                                    }, this));

                                } else if (mediumArr.length <= 0 && endCondition.length <= 0) {
                                    console.log('hello 1240');
                                    if (relatedItem == 'Internal Use') {
                                        this._createIIItemforIU();
                                    } else if (relatedItem == 'Sales') {
                                        this._createIIItemforSales();
                                    } else if (relatedItem == 'Work Product') {
                                        if (composition == 'Inventory Collection') {
                                            this._createIIItemforCollection();
                                        } else {
                                            this._createIIItemforWP();
                                        }
                                    }
                                }
                            }
                        }, this)
                    };
                    app.api.call(method, url, data, callback);
                }
            },
            _linkInventoryItem: function () {
                return new Promise(function promiseFunc(resolve, reject) {
                    var modelId = this.model.get("id");
                    var relatedItem = this.model.get("related_to");
                    if (relatedItem == 'Work Product') {
                        var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                        var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                    } else if (relatedItem == 'Sales') {
                        var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-sales-selection");
                        var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-sales-selection");
                    } else if (relatedItem == 'Internal Use') {
                        var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-internal-selection");
                        var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-internal-selection");
                    }
                    var selectedii_wp = ii_wpView.massCollection;
                    if (selectedii_wp.length > 0) {

                        var inventory_items_ids = [];
                        inventory_items_ids = _.map(selectedii_wp.models, function (model) {
                            return model.get("id");
                        });

                        app.api.call("create", "rest/v10/IM_Inventory_Management/" + modelId + "/link", {
                            link_name: "ii_inventory_item_im_inventory_management_1",
                            ids: inventory_items_ids
                        }, {
                            success: function () {
                                resolve("success");
                            },
                            error: function (error) {
                                resolve(error);
                            }
                        });
                    } else {
                        resolve("success");
                    }

                }.bind(this));

            },
            _createIIItemforWP: function () {
                this.initiateSave(_.bind(function initSave() {
                    if (this.closestComponent("drawer")) {
                        this._linkWPInventoryItem().then(function thenFunc(result) {
                            if (result === "success") {
                                app.alert.dismiss("no_iiwp");
                            } else {
                                app.alert.show("no_iiwp_error", {
                                    level: "error",
                                    messages: "Failed linking Inventory Item..."
                                });
                            }
                            app.drawer.close(this.context, this.model);
                        }
                            .bind(this));
                    }
                }, this));
            },
            _createIIItemforCollection: function () {

                var ic_inventoe24election_ida = this.model.get('ic_inventoe24election_ida');
                var self = this;

                var ICLibkedII = [];
                var action = this.model.get("action");
                var type2 = this.model.get("type_2");
                if ((action == "Processing" || action == "Internal Transfer" || action == "External Transfer" || action == "Archive Offsite" || action == "Archive Onsite" || action == "Discard" || action == "Obsolete") && type2 != "Tissue") {
                    app.api.call("get", "rest/v10/IC_Inventory_Collection/" + ic_inventoe24election_ida + "/link/ic_inventory_collection_ii_inventory_item_1", null, {
                        success: function (iiData) {
                            $.each(iiData.records, function (i, IIid) {
                                ICLibkedII.push(IIid.id);
                            });

                            var inventory_items_ids = ICLibkedII;
                            console.log('IC linked II', inventory_items_ids);

                            if (iiData.records.length > 0) {
                                var url = app.api.buildURL("getItemValue");
                                var method = "create";
                                var data = {
                                    ii_Id: inventory_items_ids
                                };
                                var mediumArr = [];
                                var endCondition = storageMediumArr = conditionArr = [];
                                var counter = counter1 = 0;
                                var callback = {
                                    success: _.bind(function successCB(res) {
                                        for (i = 0; i < res.length; i++) {
                                            var storageMediumArr = res[i]['allowedStorageMedium'].split(",");
                                            var conditionArr = res[i]['allowedStorageCondition'].split(",");

                                            if (!(storageMediumArr.includes(self.model.get("ending_storage_media")))) {
                                                mediumArr[counter] = res[i]['name'];
                                                counter++;
                                            }
                                            if (!(conditionArr.includes(self.model.get("end_condition")))) {
                                                endCondition[counter1] = res[i]['name'];
                                                counter1++;
                                            }
                                        }


                                        console.log('ending_storage_media', self.model.get("ending_storage_media"));
                                        console.log('end_condition', self.model.get("end_condition"));
                                        if (mediumArr.length > 0 && self.model.get("ending_storage_media") != "") {
                                            console.log('2008', endCondition.length);
                                            var mediumStr = mediumArr.toString();
                                            app.alert.show("medium_error", {
                                                level: "error",
                                                messages: "The End Storage Media for the Inventory Management is not an Allowed Storage Medium for below item, please review:  " + mediumStr,
                                                autoClose: false,
                                            });
                                        }
                                        if (endCondition.length > 0 && self.model.get("end_condition") != "") {
                                            console.log('2017', endCondition.length);
                                            var endStr = endCondition.toString();
                                            app.alert.show("end_error", {
                                                level: "error",
                                                messages: "The End Condition for the Inventory Management is not an Allowed Storage Condition for below item, please review:  " + endStr,
                                                autoClose: false,
                                            });
                                        }

                                        console.log('mediumArr', mediumArr.length);
                                        console.log('endConditionArr 123', endCondition.length);

                                        if (self.model.get("end_condition") == "" && self.model.get("ending_storage_media") == "") {
                                            console.log('2030');
                                            self.initiateSave(_.bind(function initSave() {
                                                if (self.closestComponent("drawer")) {
                                                    if (res.length > 0) {
                                                        self._createICRecord(ic_inventoe24election_ida).then(function thenFunc(result) {
                                                            if (result === "success") {
                                                                app.alert.dismiss("link_ic_error");
                                                            }
                                                            app.drawer.close(self.context, self.model);
                                                        }
                                                            .bind(self));
                                                    } else {
                                                        app.drawer.close(self.context, self.model);
                                                    }

                                                }
                                            }, self));
                                        } else {
                                            console.log('2048');
                                            if (self.model.get("end_condition") != "" && self.model.get("ending_storage_media") == "" && endCondition.length <= 0) {
                                                console.log('2050');
                                                self.initiateSave(_.bind(function initSave() {
                                                    if (self.closestComponent("drawer")) {
                                                        self._createICRecord(ic_inventoe24election_ida).then(function thenFunc(result) {
                                                            if (result === "success") {
                                                                app.alert.dismiss("link_ic_error");
                                                            }
                                                            app.drawer.close(self.context, self.model);
                                                        }
                                                            .bind(self));
                                                    }
                                                }, self));
                                            } else if (self.model.get("end_condition") == "" && self.model.get("ending_storage_media") != "" && mediumArr.length <= 0) {
                                                console.log('2063');
                                                self.initiateSave(_.bind(function initSave() {
                                                    if (self.closestComponent("drawer")) {
                                                        //console.log('ic1 : ', ic_inventoe24election_ida);
                                                        self._createICRecord(ic_inventoe24election_ida).then(function thenFunc(result) {
                                                            if (result === "success") {
                                                                app.alert.dismiss("link_ic_error");
                                                            }
                                                            app.drawer.close(self.context, self.model);
                                                        }
                                                            .bind(self));
                                                    }
                                                }, self));

                                            } else if (mediumArr.length <= 0 && endCondition.length <= 0) {
                                                console.log('2078');
                                                self.initiateSave(_.bind(function initSave() {
                                                    if (self.closestComponent("drawer")) {
                                                        self._createICRecord(ic_inventoe24election_ida).then(function thenFunc(result) {
                                                            if (result === "success") {
                                                                app.alert.dismiss("link_ic_error");
                                                            }
                                                            app.drawer.close(self.context, self.model);
                                                        }
                                                            .bind(self));
                                                    }
                                                }, self));
                                            }
                                        }
                                    }, self)
                                };
                                app.api.call(method, url, data, callback);

                            }
                        },
                        error: function (error) {
                            //resolve(error);
                        }
                    });
                } else {
                    this.initiateSave(_.bind(function initSave() {
                        if (this.closestComponent("drawer")) {
                            console.log('ic11 : ', ic_inventoe24election_ida);
                            this._createICRecord(ic_inventoe24election_ida).then(function thenFunc(result) {
                                if (result === "success") {
                                    app.alert.dismiss("link_ic_error");
                                }
                                app.drawer.close(this.context, this.model);
                            }
                                .bind(this));
                        }
                    }, this));
                }
            },
            _createIIItemforSales: function () {
                this.initiateSave(_.bind(function initSave() {
                    if (this.closestComponent("drawer")) {
                        this._linkSalesInventoryItem().then(function thenFunc(result) {
                            if (result === "success") {
                                app.alert.dismiss("no_iisales");
                            } else {
                                app.alert.show("no_iisales_error", {
                                    level: "error",
                                    messages: "Failed linking Inventory Item..."
                                });
                            }
                            app.drawer.close(this.context, this.model);
                        }.bind(this));
                    }
                }, this));
            },
            _createIIItemforIU: function () {
                this.initiateSave(_.bind(function initSave() {
                    if (this.closestComponent("drawer")) {
                        this._linkInternalInventoryItem().then(function thenFunc(result) {
                            if (result === "success") {
                                app.alert.dismiss("no_iiinternal");
                            } else {
                                app.alert.show("no_iiinternal_error", {
                                    level: "error",
                                    messages: "Failed linking Inventory Item..."
                                });
                            }
                            app.drawer.close(this.context, this.model);
                        }.bind(this));
                    }
                }, this));
            },
            //-------------------END Added By Sunil-----------------------

            _createICRecord: function (ic_inventoe24election_ida) {
                return new Promise(function promiseFunc(resolve, reject) {
                    var modelId = this.model.get("id");

                    var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                    var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                    var selectedii_wp = ii_wpView.massCollection;
                    var inventory_items_ids = [];
                    inventory_items_ids = _.map(selectedii_wp.models, function (model) {
                        return model.get("id");
                    });

                    app.api.call("create", "rest/v10/IM_Inventory_Management/" + modelId + "/link", {
                        link_name: "ii_inventory_item_im_inventory_management_1",
                        ids: inventory_items_ids
                    }, {
                        success: function () {
                            resolve("success");
                        },
                        error: function (error) {
                            resolve(error);
                        }
                    });

                    var action = this.model.get("action");
                    if (action == "Processing" || action == "Internal Transfer" || action == "External Transfer" || action == "Archive Offsite" || action == "Archive Onsite" || action == "Discard" || action == "Obsolete" || action == "Exhausted") {
                        app.api.call("get", "rest/v10/IC_Inventory_Collection/" + ic_inventoe24election_ida + "/link/ic_inventory_collection_ii_inventory_item_1", null, {
                            success: function (iiData) {
                                $.each(iiData.records, function (i, IIid) {
                                    app.api.call("create", "rest/v10/IM_Inventory_Management/" + modelId + "/link/ii_inventory_item_im_inventory_management_1/" + IIid.id, null, {
                                        success: function (dropdowndata) {
                                            resolve("success");
                                        },
                                        error: function (error) {
                                            resolve(error);
                                        }
                                    });
                                });
                            },
                            error: function (error) {
                                //resolve(error);
                            }
                        });
                    }

                    if (this.model.get("action") == "Create Inventory Collection") {
                        app.alert.show("ii_wp_error", {
                            level: "info",
                            messages: "Creating Inventory Collection Record.",
                            autoClose: true
                        });

                        var workProductId = this.model.get('m03_work_product_im_inventory_management_1m03_work_product_ida');

                        app.api.call("create", "rest/v10/create_ic_record", {
                            module: 'IC_Inventory_Collection',
                            wpId: workProductId,
                            II_ids: inventory_items_ids,
                            IM_id: modelId

                        }, {
                            success: function () {
                                resolve("success");
                            },
                            error: function (error) {
                                resolve(error);
                            }
                        })
                    }

                    if (this.model.get("action") == "Separate Inventory Items") {
                        app.alert.show("ii_wp_error", {
                            level: "info",
                            messages: "Deleting Inventory Collection Relationship to Inventory Item.",
                            autoClose: true
                        });

                        //console.log('inventory_items_ids : ',inventory_items_ids);

                        var ii_ids_hidden_c = this.model.get("ii_ids_hidden_c");
                        var ii_ids_hidden_array = ii_ids_hidden_c.split('$$');
                        console.log('ii_ids_hidden_array', ii_ids_hidden_array);
                        console.log('inventory_items_ids', inventory_items_ids);

                        var returnValue =
                            ii_ids_hidden_array.filter(function (el) {
                                return $.inArray(el, inventory_items_ids) === -1;
                            }).concat(
                                inventory_items_ids.filter(function (el) {
                                    return $.inArray(el, ii_ids_hidden_array) === -1;
                                })
                            );
                        console.log('returnValue', returnValue);
                        //var i = 0;
                        $.each(returnValue, function (key, IIid) {
                            //alert(IIid);
                            //if (IIid != inventory_items_ids[i]) {
                            app.api.call("delete", "rest/v10/IC_Inventory_Collection/" + ic_inventoe24election_ida + "/link/ic_inventory_collection_ii_inventory_item_1/" + IIid, null, {
                                success: function (dropdowndata) {
                                    resolve("success");
                                },
                                error: function (error) {
                                    resolve(error);
                                }
                            });
                            //}
                            //i++;
                        });

                        /*$.each(inventory_items_ids, function (i, IIid) {
                         app.api.call("delete", "rest/v10/IC_Inventory_Collection/" + ic_inventoe24election_ida + "/link/ic_inventory_collection_ii_inventory_item_1/" + IIid, null, {
                         success: function (dropdowndata) {
                         resolve("success");
                         },
                         error: function (error) {
                         resolve(error);
                         }
                         });
                         });*/

                    }

                }.bind(this));

            },
            intenalUseSelected: function () {
                if (this.model.get("related_to") !== "Internal Use") {
                    return true;
                }
                var internalLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-internal-selection");
                var internalView = internalLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-internal-selection");

                if (internalView.massCollection && internalView.massCollection.length === 0) {
                    return false;
                }

                return true;

            },
            salesSelected: function () {
                if (this.model.get("related_to") !== "Sales") {
                    return true;
                }
                var ii_salesLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-sales-selection");
                var ii_salesView = ii_salesLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-sales-selection");

                if (ii_salesView.massCollection && ii_salesView.massCollection.length === 0) {
                    return false;
                }

                return true;

            },
            wpSelected: function () {
                if (this.model.get("related_to") !== "Work Product") {
                    return true;
                }
                var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");

                if (ii_wpView.massCollection && ii_wpView.massCollection.length === 0) {
                    return false;
                }

                return true;

            },
            wpIICollection: function () {
                if (this.model.get("composition") !== "Inventory Collection") {
                    return true;
                }
                var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");

                if (ii_wpView.massCollection && ii_wpView.massCollection.length === 0) {
                    return false;
                }

                return true;

            },
            _hideSidebarMainPane: function (model, currentValue) {
                //$('div.search-filter').css('display', 'none');
                $('div.search-filter .choice-filter-clickable').css('display', 'none');
                $('div.search-filter .choice-filter-clickable a.select2-choice').css('display', 'none');
                $('div.search-filter a.select2-choice').css('display', 'none');
                //$('div.search-filter .search-name').css('placeholder', 'Search by name...');

                $('.dashboard-pane .main-content .table .orderByname span').css('width', '170px');
                $('.dashboard-pane .main-content .table .orderByinternal_barcode span').css('width', '170px');
                $('.dashboard-pane .main-content .table .orderBystorage_condition span').css('width', '170px');
                $('.dashboard-pane .main-content .table .orderByallowed_storage_conditions span').css('width', '170px');
                $('.dashboard-pane .main-content .table .orderByallowed_storage_medium span').css('width', '170px');
                $('.dashboard-pane .main-content .table .orderBylocation_building span').css('width', '170px');
                $('.dashboard-pane .main-content .table .orderBylocation_shelf span').css('width', '170px');
                $('.dashboard-pane .main-content .table .orderBylocation_cabinet span').css('width', '170px');

                moduleNameIM = "II_Inventory_Item_IM";
                offsetIM = 0;
                orderTypeIM = "desc";
                lastSortedColumnNameIM = "name";
                appendQueryIM = "&order_by=name%3Adesc";
                this.layout.layout.$el.find("input[name=check]").prop('chekced', false);
                this.$('input[name="check"]').prop('checked', false);
                $('.ii_wp_selection').find('input[name="check"]').prop('checked', false);
                $('.ii_sales_selection').find('input[name="check"]').prop('checked', false);
                $('.ii_internal_selection').find('input[name="check"]').prop('checked', false);

                this.model.set("action", null);
                this.model.set("type_2", null);
                this.model.set("ic_inventoe24election_ida", null);
                this.model.set("ic_inventory_collection_im_inventory_management_1_name", null);

                this.model.set("category", null);
                this.model.set("m01_sales_im_inventory_management_1m01_sales_ida", null);
                this.model.set("m01_sales_im_inventory_management_1_name", null);

                this.model.set("m03_work_product_im_inventory_management_1m03_work_product_ida", null);
                this.model.set("m03_work_product_im_inventory_management_1_name", null);

                $('.ii_sales_selection').find(".search-name").attr('placeholder', 'Search by name, internal barcode...');
                $('.ii_internal_selection').find(".search-name").attr('placeholder', 'Search by name, internal barcode...');
                $('.ii_wp_selection').find(".search-name").attr('placeholder', 'Search by name, internal barcode...');
                //$('.ii_sales_selection').find(".search-name").attr('placeholder', 'Search by name...');
                //$('.ii_internal_selection').find(".search-name").attr('placeholder', 'Search by name...');
                //$('.ii_wp_selection').find(".search-name").attr('placeholder', 'Search by name...');
                this.layout.layout.$el.find("[data-component=sidebar] .main-pane.span8").hide();

                if (this.model.get("composition") === "Inventory Collection") {
                    this.model.set("type_2", "Tissue");
                    $('div[data-name="type_2"]').css('pointer-events', 'none');
                    $('div[data-name="category"]').css('pointer-events', 'none');
                    var self = this;
                    setTimeout(function () {
                        $('span[data-fieldname="type_2"] .select2-search-choice-close').remove();
                    }, 200);

                } else {
                    $('div[data-name="type_2"]').css('pointer-events', 'unset');
                    $('div[data-name="category"]').css('pointer-events', 'unset');
                }
            },
            _isWorkProductSelected: function () {
                var wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("wp-selection");
                var wpView = wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("wp-selection");
                var selectedWPs = wpView.massCollection;

                if (this.model.get("related_to") === "Work Product" || selectedWPs.length > 0) {
                    return true;
                }

                return false;
            },
            _isRelatedToInternalSelected: function () {

                var internalLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-internal-selection");
                var internalView = internalLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-internal-selection");
                var selectedInternal = internalView.massCollection;
                if (this.model.get("related_to") === "Internal Use" || selectedInternal.length > 0) {
                    return true;
                }

                return false;
            },
            _isRelatedToSalesSelected: function () {

                var salesLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-sales-selection");
                var salesView = salesLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-sales-selection");
                var selectedsales = salesView.massCollection;
                if (this.model.get("related_to") === "Sales" || selectedsales.length > 0) {
                    return true;
                }

                return false;
            },
            _isRelatedToWPSelected: function () {

                var wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                var wpView = wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");
                var selectedwp = wpView.massCollection;
                if (this.model.get("related_to") === "Work Product" || selectedwp.length > 0) {
                    return true;
                }

                return false;
            },
            _linkWorkProducts: function () {
                return new Promise(function promiseFunc(resolve, reject) {
                    var modelId = this.model.get("id");
                    var wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("wp-selection");
                    var wpView = wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("wp-selection");
                    var selectedWorkProducts = wpView.massCollection;
                    if (selectedWorkProducts.length > 0) {
                        app.alert.show("link_wps", {
                            level: "info",
                            messages: "Linking Work Products..."
                        });
                        var wpIds = _.map(selectedWorkProducts.models, function mapModels(model) {
                            return model.get("id");
                        });
                        app.api.call("create", "rest/v10/IM_Inventory_Management/" + modelId + "/link", {
                            //eslint-disable-next-line
                            link_name: "m03_work_product_im_inventory_management_1",
                            ids: wpIds
                        }, {
                            success: function () {
                                resolve("success");
                            },
                            error: function (error) {
                                resolve(error);
                            }
                        });
                    } else {
                        resolve("success");
                    }
                }.bind(this));
            },
            _linkWPInventoryItem: function () {
                return new Promise(function promiseFunc(resolve, reject) {

                    var related_to_c = this.model.get("related_to");
                    if (related_to_c === "Work Product") {
                        var modelId = this.model.get("id");
                        var ii_wpLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-wp-selection");
                        var ii_wpView = ii_wpLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-wp-selection");

                        var selectedii_wp = ii_wpView.massCollection;
                        if (selectedii_wp.length > 0) {
                            app.alert.show("no_iiwp", {
                                level: "info",
                                messages: "Linking Inventory Item..."
                            });
                            var inventory_items_ids = []

                            inventory_items_ids = _.map(selectedii_wp.models, function (model) {
                                return model.get("id");
                            });
                            console.log("wpID", inventory_items_ids);
                            app.api.call("create", "rest/v10/IM_Inventory_Management/" + modelId + "/link", {
                                link_name: "ii_inventory_item_im_inventory_management_1",
                                ids: inventory_items_ids
                            }, {
                                success: function () {
                                    resolve("success");
                                },
                                error: function (error) {
                                    resolve(error);
                                }
                            });
                        } else {
                            resolve("success");
                        }
                    }
                }.bind(this));
            },
            _linkSalesInventoryItem: function () {
                return new Promise(function promiseFunc(resolve, reject) {

                    var related_to_c = this.model.get("related_to");
                    if (related_to_c === "Sales") {
                        var modelId = this.model.get("id");
                        var ii_salesLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-sales-selection");
                        var ii_salesView = ii_salesLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-sales-selection");

                        var selectedii_sales = ii_salesView.massCollection;
                        if (selectedii_sales.length > 0) {
                            app.alert.show("no_iisales", {
                                level: "info",
                                messages: "Linking Inventory Item..."
                            });

                            var inventory_items_ids2 = []
                            inventory_items_ids2 = _.map(selectedii_sales.models, function (model) {
                                return model.get("id");
                            });
                            console.log("sID", inventory_items_ids2);
                            app.api.call("create", "rest/v10/IM_Inventory_Management/" + modelId + "/link", {
                                link_name: "ii_inventory_item_im_inventory_management_1",
                                ids: inventory_items_ids2
                            }, {
                                success: function () {
                                    resolve("success");
                                },
                                error: function (error) {
                                    resolve(error);
                                }
                            });
                        } else {
                            resolve("success");
                        }
                    }
                }.bind(this));
            },
            _linkInternalInventoryItem: function () {
                return new Promise(function promiseFunc(resolve, reject) {

                    var related_to_c = this.model.get("related_to");
                    if (related_to_c === "Internal Use") {
                        var modelId = this.model.get("id");
                        var ii_internalLayout = this.layout.layout.getComponent("dashboard-pane").getComponent("ii-internal-selection");
                        var ii_internalView = ii_internalLayout.getComponent("main-pane").getComponent("filterpanel").getComponent("ii-internal-selection");

                        var selectedii_internal = ii_internalView.massCollection;
                        if (selectedii_internal.length > 0) {
                            app.alert.show("no_iiinternal", {
                                level: "info",
                                messages: "Linking Inventory Item..."
                            });

                            var inventory_items_ids3 = [];

                            inventory_items_ids3 = _.map(selectedii_internal.models, function (model) {
                                return model.get("id");
                            });
                            console.log("inID", inventory_items_ids3);
                            app.api.call("create", "rest/v10/IM_Inventory_Management/" + modelId + "/link", {
                                link_name: "ii_inventory_item_im_inventory_management_1",
                                ids: inventory_items_ids3
                            }, {
                                success: function () {
                                    resolve("success");
                                },
                                error: function (error) {
                                    resolve(error);
                                }
                            });
                        } else {
                            resolve("success");
                        }
                    }
                }.bind(this));
            },
        });
    });
})(SUGAR.App);