(function appFUnc(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        
        var _hideIIIMSubpanel = function(hide){
            if (hide === true) {
                $("[data-subpanel-link=ii_inventory_item_im_inventory_management_1]")
                    .find("[track='click:actiondropdown']")
                    .hide();
            } else {
                $("[data-subpanel-link=ii_inventory_item_im_inventory_management_1]")
                    .find("[track='click:actiondropdown']")
                    .show();
            }
        };

        if (
            !app.view.views.BaseII_Inventory_ItemRecordView &&
            !app.view.views.BaseII_Inventory_ItemCustomRecordView
        ) {
            app.view.declareComponent("view", "record", "II_Inventory_Item", undefined, false, "base");
        }

        var iiRecordView = "BaseII_Inventory_ItemRecordView";

        if (app.view.views.BaseII_Inventory_ItemCustomRecordView) {
            iiRecordView = "BaseII_Inventory_ItemCustomRecordView";
        }

        if (App.view.views[iiRecordView].hideLinkExistingButton === true) {
            return;
        }

        App.view.views[iiRecordView] = App.view.views[iiRecordView].extend({
            hideLinkExistingButton: true,

            initialize: function() {
                var initializeOptions = this._super("initialize", arguments);

                this.model.on("sync", this._modelSynced, this);     

                return initializeOptions;
            },           
            
            _modelSynced: function(model) {
                if (model.get("inactive_c") === true) {
                    _hideIIIMSubpanel.call(this, true);
                } else {
                    _hideIIIMSubpanel.call(this, false);
                }
            }
        });

        if (
            !app.view.views.BaseIM_Inventory_ManagementRecordView &&
            !app.view.views.BaseIM_Inventory_ManagementCustomRecordView
        ) {
            app.view.declareComponent("view", "record", "IM_Inventory_Management", undefined, false, "base");
        }

        var imRecordView = "BaseIM_Inventory_ManagementRecordView";

        if (app.view.views.BaseIM_Inventory_ManagementCustomRecordView) {
            imRecordView = "BaseIM_Inventory_ManagementCustomRecordView";
        }

        if (App.view.views[imRecordView].hideLinkExistingButton === true) {
            return;
        }

        App.view.views[imRecordView] = App.view.views[imRecordView].extend({
            hideLinkExistingButton: true,

            initialize: function() {
                var initializeOptions = this._super("initialize", arguments);

                this.model.on("sync", this._modelSynced, this);     

                return initializeOptions;
            },           
            
            _modelSynced: function(model) {
                if (model.get("im_inactive_c") === true) {
                    _hideIIIMSubpanel.call(this, true);
                } else {
                    _hideIIIMSubpanel.call(this, false);
                }
            }
        });
    });
})(SUGAR.App);
