<?php

namespace Sugarcrm\Sugarcrm\custom\src\wsystems\InventoryManagementCustomizations;

use Sugarcrm\Sugarcrm\custom\src\wsystems\InventoryManagementCustomizations\IMUtils;

class IMManager {

    private $utils;
    private $_observationWorkProductsLink = "m06_error_m03_work_product_1";
    private $_inventoryMangementWorkProductsLink = "m03_work_product_im_inventory_management_1";
    //private $_inventoryItemWorkProductsLink = "m03_work_product_ii_inventory_item_1";
    private $_observationTestSystemLink = "m06_error_anml_animals_1";

    //private $_inventoryItemTestSystemLink   = "anml_animals_ii_inventory_item_1";


    public function __construct() {
        $this->utils = new IMUtils();
        }

        function new () {
        $manager = new self();

        return $manager;
    }

    public function getFormattedRecordName() {
        $recordNumber = $this->utils->getFormattedIMNumber();
        $lastYearDigits = $this->utils->getYearLastDigits();
        $newRecordName = $this->utils->modulePrefix . $lastYearDigits . "-" . $recordNumber;

        return $newRecordName;
    }

    public function processScannedItems($bean, $arguments) {
        $inventoryItemBean = \BeanFactory::retrieveBean("II_Inventory_Item", $arguments["related_id"]);
        $observationMessages = $this->utils->getObservationMessages($bean, $inventoryItemBean);
        $wasObservationCreated = $this->utils->wasObservationCreated($bean->id, $inventoryItemBean->id);

        if ($wasObservationCreated === true) {
            return;
        }

        $workProducts = $bean->m03_work_product_im_inventory_management_1m03_work_product_ida;
       
        $anmlAnimals = $inventoryItemBean->anml_animals_ii_inventory_item_1anml_animals_ida;

        if ($observationMessages["mustCreateObservation"] === true) {
            $observationData = $this->utils->getInventoryObservationData();
            $observationData["actual_event_c"] = $observationMessages["actualEventMessage"];
            $observationData["expected_event_c"] = $observationMessages["expectedEventMessage"];
            $observationData["work_products"] = $workProducts;
            $observationData["anml_animals"] = $anmlAnimals;

            $observationBean = $this->utils->createObservationRecord($observationData);

            if (empty($observationBean->name) === true) {
                return;
            }

            $this->utils->registerObservation($observationBean->id, $bean->id, $inventoryItemBean->id);

            //Test system Linking in M06_error from Inventory Item test system
            if (($inventoryItemBean->related_to_c === "Single Test System" || $inventoryItemBean->related_to_c === "Multiple Test Systems") && $observationBean->load_relationship($this->_observationTestSystemLink)
            ) {
                $TsId = $inventoryItemBean->anml_animals_ii_inventory_item_1anml_animals_ida;
                $observationBean->{$this->_observationTestSystemLink}->add($TsId);
            }

            //Work Product Linking in M06_error from Inventory management work product.
            if ($bean->related_to_c === "Sales") {

                $this->utils->copyRelatedRecordsToAnotherRecord($observationBean, $this->_observationWorkProductsLink, $bean, $this->_inventoryMangementWorkProductsLink);
            } else if (($bean->related_to_c === "Work Product" || $bean->related_to_c === "Single Test System" || $bean->related_to_c === "Multiple Test Systems") && $observationBean->load_relationship($this->_observationWorkProductsLink)
            ) {
                //$wpId = $bean->m03_work_product_im_inventory_management_2m03_work_product_ida;
                $wpId = $bean->m03_work_product_im_inventory_management_1m03_work_product_ida;
                //$GLOBALS['log']->fatal('WPID IN'. $wpId);
                $observationBean->{$this->_observationWorkProductsLink}->add($wpId);
            }

            /* if ($inventoryItemBean->related_to_c === "Sales") {
              $this->utils->copyRelatedRecordsToAnotherRecord(
              $observationBean,
              $this->_observationTestSystemLink,
              $inventoryItemBean,
              $this->_inventoryItemTestSystemLink
              );

              $this->utils->copyRelatedRecordsToAnotherRecord(
              $observationBean,
              $this->_observationWorkProductsLink,
              $inventoryItemBean,
              $this->_inventoryItemWorkProductsLink
              //$this->_inventoryMangementWorkProductsLink
              );

              } else if (($inventoryItemBean->related_to_c === "Work Product" || $inventoryItemBean->related_to_c === "Single Test System" || $inventoryItemBean->related_to_c === "Multiple Test Systems") && $observationBean->load_relationship($this->_observationWorkProductsLink)
              ) {
              $wpId = $inventoryItemBean->m03_work_product_ii_inventory_item_2m03_work_product_ida;

              $observationBean->{$this->_observationWorkProductsLink}->add($wpId);
              } */
        }
    }

}
