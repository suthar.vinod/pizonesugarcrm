<?php

namespace Sugarcrm\Sugarcrm\custom\src\wsystems\InventoryManagementCustomizations\LogicHooks;

use Sugarcrm\Sugarcrm\custom\src\wsystems\InventoryManagementCustomizations\IMManager;
use Sugarcrm\Sugarcrm\custom\src\wsystems\InventoryManagementCustomizations\IMUtils;

class InventoryManagementCustomizationsHook
{
    public function beforeSaveMethod($bean, $event, $arguments)
    {
        if ($arguments["isUpdate"] === false) {
            $manager = new IMManager();
            $utils   = new IMUtils();
            /* 1901 : Add WP name in IM naming sequence 04 Jan 2022 */
            $wp_name = $utils->getWPName($bean->m03_work_product_im_inventory_management_1m03_work_product_ida);
            
            $name                       = $manager->getFormattedRecordName();
            $bean->inventory_management_num_c = $utils->getIMNumber();

            if(!empty($wp_name))
            {
                $bean->name = $wp_name . " " . $name;
            } else {
                $bean->name = $name;
            }
        }

        $bean->duration_hours = $this->getDifferenceInHours($bean->start_datetime, $bean->end_datetime);
    }

    public function afterRelationshipAddMethod($bean, $event, $arguments)
    {
        $manager       = new IMManager();
        $module        = $arguments["module"];
        $relatedModule = $arguments["related_module"];

        if ($module === "IM_Inventory_Management" && $relatedModule === "II_Inventory_Item") {
            $manager->processScannedItems($bean, $arguments);
        }
    }

    public function beforeFilterMethod($bean, $event, $arguments)
    {
        $utils = new IMUtils();

        if ($utils->shouldFilterInventoryManagementRecords()) {
            $arguments[0]->where()->equals('im_inactive_c', false);

            if (isset($arguments[1]['id_query'])) {
                $arguments[1]['id_query']->where()->equals('im_inactive_c', false);
            }
        }
    }

    /**
     * Calculate the difference number of hours between the dates
     *
     * @param string $startDate
     * @param string $endDate
     *
     * @return double
     */
    protected function getDifferenceInHours($startDate, $endDate)
    {
        $startTimestamp = strtotime($startDate);
        $endTimestamp   = strtotime($endDate);
        $difference     = abs($startTimestamp - $endTimestamp) / 3600;

        return $difference;
    }
}
