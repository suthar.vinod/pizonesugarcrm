<?php

namespace Sugarcrm\Sugarcrm\custom\src\wsystems\InventoryManagementCustomizations;

class IMUtils
{
    private $_db;
    private $_conn;
    private $_sugarQuery;
    private $_currentUser;
    private $appListStrings;
    private $_service;

    public $modulePrefix = "IM";

    public const OBSERVATION_AUDIT_TABLE = "wsys_observations_audit";

    public const OAT_IM_FIELD          = "inventory_management_id";
    public const OAT_II_FIELD          = "inventory_item_id";
    public const OAT_OBSERVATION_FIELD = "observation_id";
    public const OAT_DELETED_FIELD     = "deleted";

    public function __construct()
    {
        global $db, $current_user, $service, $app_list_strings, $dictionary;

        $this->_db   = $db;
        $this->_conn = $this->_db->getConnection();

        $this->_sugarQuery    = new \SugarQuery();
        $this->_timeDate      = new \TimeDate($this->_currentUser);
        $this->_currentUser   = $current_user;
        $this->_service       = $service;
        $this->appListStrings = $app_list_strings;
        $this->dictionary     = $dictionary;
    }

    /**
     * Return the last 2 digits of the current year
     */
    public function getYearLastDigits()
    {
        return date("y");
    }

    /**
     * Get the next number that should be set for the IM record
     *
     * @return integer $recordNumber
     */
    public function getIMNumber()
    {
        $recordNumber = 1;

        $nameFormat = $this->modulePrefix . $this->getYearLastDigits() . "-";

        $this->_sugarQuery->from(\BeanFactory::newBean("IM_Inventory_Management"));
        $this->_sugarQuery->select(array("id", "name", "inventory_management_num_c"));
        $this->_sugarQuery->where()->contains("name", $nameFormat);
        $this->_sugarQuery->orderBy('inventory_management_num_c');
        $this->_sugarQuery->limit(1);

        $resultSet = $this->_sugarQuery->execute();

        foreach ($resultSet as $row) {
            if (empty($row["inventory_management_num_c"]) === false) {
                $recordNumber = $row["inventory_management_num_c"] + 1;
            }
        }

        return $recordNumber;
    }

    /**
     * 1901 : Get WP name from IM Record 04 Jan 2022
     *
     * @return $WPRecordName
     */
    public function getWPName($wp_id)
    {
        $sqlWP = "SELECT name as WPName FROM m03_work_product WHERE id='" . $wp_id . "' AND deleted = 0  ORDER BY date_modified desc";
        $resultWP = $this->_db->query($sqlWP);
        $rowWP = $this->_db->fetchByAssoc($resultWP);
        $WPRecordName = $rowWP['WPName'];
        return $WPRecordName;
    }

    /**
     * Return the formatted IM number
     *
     * @return string $IMNumber
     */
    public function getFormattedIMNumber()
    {
        $IMNumber = $this->getIMNumber();

        if ($IMNumber <= 9) {
            return "0000" . $IMNumber;
        } else if ($IMNumber <= 99) {
            return "000" . $IMNumber;
        } else if ($IMNumber <= 999) {
            return "00" . $IMNumber;
        } else if ($IMNumber <= 9999) {
            return "0" . $IMNumber;
        } else {
            return $IMNumber;
        }
    }

    /**
     * Check if the storage condition is met
     *
     * @param string $managementValue   The value from the IM record
     * @param string $itemValue         The value from the II record
     *
     * @return boolean
     */
    public function isStorageConditionMet($managementValue, $itemValue)
    {
         
        if ($managementValue === "30 45 C" && $itemValue === "30 45") {
            return true;
        }
        
        if ($managementValue === "Ambient Temperature" && $itemValue === "Ambient") {
            return true;
        }

        if ($managementValue === "Room Temperature" && $itemValue === "Room Temperature") {
            return true;
        }

        if ($managementValue === "Refrigerated" && $itemValue === "Refrigerate") {
            return true;
        }

        if ($managementValue === "On Ice" && $itemValue === "Refrigerate") {
            return true;
        }

        if ($managementValue === "Frozen on Dry Ice" && $itemValue === "Ultra Frozen") {
            return true;
        }

        if ($managementValue === "Frozen in Dry IceAcetone" && $itemValue === "Ultra Frozen") {
            return true;
        }

        if ($managementValue === "Frozen in 15C Freezer" && $itemValue === "Frozen") {
            return true;
        }

        if ($managementValue === "Frozen in 70C Freezer" && $itemValue === "Ultra Frozen") {
            return true;
        }
        

        return false;
    }

    /**
     * Return an array with the fields and values that should be added on the new created Observation record
     *
     * @param SugarBean $inventoryItemBean  The Inventory bean for which should be created the Observation record
     *
     * @return array $response              Contains the Observation data
     */
    public function getInventoryObservationData()
    {
        $response = array();
	
        $currentDate = $this->_timeDate->nowDate();
        $contactId   = $this->getCorrespondingContactId($this->_currentUser);
	
        $contactBean = \BeanFactory::retrieveBean("Contacts", $contactId);
	
        $response["error_category_c"]        = "Real time study conduct";
        $response["employee_id"]             = $contactBean->id;
        $response["submitter_c"]             = $contactBean->full_name;
        $response["department_c"]            = "^" . $contactBean->department_id_c . "^";
        $response["date_error_occurred_c"]   = $currentDate;
        $response["date_error_documented_c"] = $currentDate;
        $response["error_type_c"]            = "Sttudy";
        
        return $response;
    }

    /**
     * Create an Observation record using the data from the given variable
     *
     * @param array $observationData        Contains the Observation data
     *
     * @return SugarBean $observationBean   Returns the Observation bean
     */
    public function createObservationRecord($observationData)
    {
        //$GLOBALS['log']->fatal('createObservationRecord WP IN'.$observationData["work_products"]);
        $observationBean                          = \BeanFactory::newBean("M06_Error");
        $observationBean->error_category_c        = $observationData["error_category_c"];
        $observationBean->submitter_c             = $observationData["submitter_c"];
        $observationBean->department_c            = $observationData["department_c"];
        $observationBean->date_error_occurred_c   = $observationData["date_error_occurred_c"];
        $observationBean->date_error_documented_c = $observationData["date_error_documented_c"];
        $observationBean->error_type_c            = $observationData["error_type_c"];
        $observationBean->actual_event_c          = $observationData["actual_event_c"];
        $observationBean->expected_event_c        = $observationData["expected_event_c"];
        $observationBean->employee_id             = $observationData["employee_id"];
        //$observationBean->wordpress_flag          = 1;
        $observationBean->rp_review               = '[\"'.$observationData["employee_id"].'\"]';
        $observationBean->rp_entry                = '[\"'.$observationData["employee_id"].'\"]';
        $observationBean->rp_entry_email_dept     = '[\"'.$observationData["submitter_c"].'--'.$observationData["employee_id"].'\"]';
        $observationBean->rp_review_email_dept    = '[\"'.$observationData["submitter_c"].'--'.$observationData["employee_id"].'\"]';
        $observationBean->related_data            = array(
                                                        "Erd_Error_Documents" => "",
                                                        "ANML_Animals" => '[\"'.$observationData["anml_animals"].'\"]',
                                                        "Equip_Equipment" => "",
                                                        "RMS_Room" => "",
                                                        "A1A_Critical_Phase_Inspectio" => "",
                                                        "Teams" => "",
                                                    ); 
		
        //$observationBean->work_products           = '[\"'.$observationData["work_products"].'\"]';
        
        //$observationBean->save();

        return $observationBean;
    }

    /**
     * Use the 'currentParentLink' relationship to get the related records to the 'currentParent' record
     * and link them to the 'newParent' record, using the 'newParentLink' relationship
     *
     * @param SugarBean     $newParent          New record that will contain the records in the subpanel
     * @param string        $newParentLink      Relationship of the new record that it's used to add the related records
     * @param SugarBean   $currentParent      Current records that has the records linked to them
     * @param string        $currentParentLink  The link used to retrieve the records from the subpanel.
     *
     */
   
    public function copyRelatedRecordsToAnotherRecord($newParent, $newParentLink, $currentParent, $currentParentLink)
    {
        if (empty($newParent) === true || empty($currentParent) === true) {
            return;
        }

        if ($currentParent->load_relationship($currentParentLink)) {
            $relatedBeans = $currentParent->$currentParentLink->get();

            if ($newParent->load_relationship($newParentLink)) {
                foreach ($relatedBeans as $relatedBeanId) {
                    $newParent->$newParentLink->add($relatedBeanId);
                }
            }
        }
    }

    /**
     * Get the Contact record id with the same first and last name
     *
     * @param SugarBean $user       The user bean that want to search for in the contacts table
     *
     * @return string   $response   The string with the contact id
     */
    public function getCorrespondingContactId($user)
    {
        $response = "";
	
        $this->_sugarQuery->from(\BeanFactory::newBean("Contacts"));
        $this->_sugarQuery->select(array("id"));
        
	if($user->email1 != ""){
            $this->_sugarQuery->where()->queryAnd()->equals("email1", $user->email1);
	}else{
            $this->_sugarQuery->where()->queryAnd()->equals("first_name", $user->first_name)->equals("last_name", $user->last_name);
	}
	
        $resultSet = $this->_sugarQuery->execute();
	
        foreach ($resultSet as $row) {
            $response = $row["id"];
        }
        return $response;
    }

    /**
     * Return if an Observation need to be created and the messages that should be displayed on that record
     *
     * @param SugarBean $managementBean
     * @param SugarBean $itemBean
     *
     * @return array $response
     */
    public function getObservationMessages($managementBean, $itemBean)
    {
        $mustCreateObservation = false;
        $expectedEvent         = "";
        $actualEvent           = "";
        $response              = array();
        $locationCondition     = array("Archive Offsite", "Archive Onsite", "Discard", "Processing",
            "External Transfer", "Internal Transfer");

        if (in_array($managementBean->type_2, $locationCondition)
            && $managementBean->current_location_verification === 0
        ) {
            $mustCreateObservation = true;
            $expectedEvent         = "Inventory Management record's Current Location was in the appropriate location";
            $actualEvent           = "Inventory Management record's Current Location was INACCURATE";
        }

        if ($managementBean->type_2 === "Processing"
            && $managementBean->processing_type !== $itemBean->processing_method
        ) {
            $mustCreateObservation = true;
            $newExpectedEvent      = "Inventory Management record's Processing Type = the Inventory Item record's Processing Type";
            $newActualEvent        = "Inventory Management record's Processing Type = '[IMValue]' and the Inventory Item record's Processing Type = '[IIValue]'";

            $newActualEvent = $this->replaceStringValues($newActualEvent, $managementBean->processing_type, $itemBean->processing_type);
            $actualEvent    = $this->getFormattedMessage($actualEvent, $newActualEvent);
            $expectedEvent  = $this->getFormattedMessage($expectedEvent, $newExpectedEvent);
        }

        $isStorageConditionMet = $this->isStorageConditionMet(
            $managementBean->start_condition,
            $itemBean->storage_condition
        );

        if ($isStorageConditionMet === false) {
            $mustCreateObservation = true;
            $newExpectedEvent      = "Inventory Management record's Start Condition = the Inventory Item record's Storage Condition";
            $newActualEvent        = "Inventory Management record's Start Condition = '[IMValue]' and the Inventory Item record's Storage Condition = '[IIValue]'";

            $imStartCondition   = $this->appListStrings['im_condition_list'][$managementBean->start_condition];
            $iiStorageCondition = $this->appListStrings['inventory_item_storage_condition_list'][$itemBean->storage_condition];

            $newActualEvent = $this->replaceStringValues($newActualEvent, $imStartCondition, $iiStorageCondition);
            $actualEvent    = $this->getFormattedMessage($actualEvent, $newActualEvent);
            $expectedEvent  = $this->getFormattedMessage($expectedEvent, $newExpectedEvent);
        }

        $isStorageConditionMet = $this->isStorageConditionMet(
            $managementBean->end_condition,
            $itemBean->storage_condition
        );

        if ($isStorageConditionMet === false) {
            $mustCreateObservation = true;
            $newExpectedEvent      = "Inventory Management record's End Condition = the Inventory Item record's Storage Condition";
            $newActualEvent        = "Inventory Management record's End Condition = '[IMValue]' and the Inventory Item record's Storage Condition = '[IIValue]'";

            $imEndCondition     = $this->appListStrings['im_condition_list'][$managementBean->end_condition];
            $iiStorageCondition = $this->appListStrings['inventory_item_storage_condition_list'][$itemBean->storage_condition];

            $newActualEvent = $this->replaceStringValues($newActualEvent, $imEndCondition, $iiStorageCondition);
            $actualEvent    = $this->getFormattedMessage($actualEvent, $newActualEvent);
            $expectedEvent  = $this->getFormattedMessage($expectedEvent, $newExpectedEvent);
        }

        $response["mustCreateObservation"] = $mustCreateObservation;
        $response["expectedEventMessage"]  = $expectedEvent;
        $response["actualEventMessage"]    = $actualEvent;

        return $response;
    }

    /**
     * Concatenate the new message with the existing message
     *
     * @param string $message
     * @param string $newMessage
     *
     * @return string $message Concatenated message
     */
    public function getFormattedMessage($message, $newMessage)
    {
        if (empty($message)) {
            $message = $newMessage;
        } else {
            $message .= ", " . $newMessage;
        }

        return $message;
    }

    /**
     * Replace the hard codded string with the data from the bean
     *
     * @param string $message
     * @param string $managementValue
     * @param string $itemvalue
     *
     * @return string $message String with the values from bean added in it
     */
    public function replaceStringValues($message, $managementValue, $itemvalue)
    {
        $message = str_replace('[IMValue]', $managementValue, $message);
        $message = str_replace('[IIValue]', $itemvalue, $message);

        return $message;
    }

    /**
     * Check if the IM record list should be filtered
     *
     * @return bool
     */
    public function shouldFilterInventoryManagementRecords()
    {
        $args = $this->_service->getRequest()->getArgs();

        if ($args["module"] === "IM_Inventory_Management" && $args["_filterIMRecords"] === "true") {
            return true;
        }

        return false;
    }

    /**
     * Add the newComponent to the array of components layout
     *
     * @param array $components
     * @param array $newComponent
     * @param string $cssClass
     *
     * @return array $components
     */
    public static function injectAfterSidebarMainpane(array $components, array $newComponent, string $cssClass = null)
    {
        foreach ($components as $i => $createComponent) {
            // skip wrong component
            if (empty($createComponent['layout']['name']) || $createComponent['layout']['name'] !== 'sidebar') {
                continue;
            }
            // add custom class
            if (!is_null($cssClass)) {
                $components[$i]['layout']['css_class'] = $cssClass;
            }
            $sidebarComponents = $createComponent['layout']['components'] ?? [];
            foreach ($sidebarComponents as $j => $sidebarComponent) {
                // skip wrong component
                if (empty($sidebarComponent['layout']['name']) || $sidebarComponent['layout']['name'] !== 'main-pane') {
                    continue;
                }
                // inject component right after the current one
                array_splice($sidebarComponents, $j + 1, 0, [$newComponent]);
                break;
            }
            $components[$i]['layout']['components'] = $sidebarComponents;
        }

        return $components;
    }

    /**
     * Check if Observation was created or should be created
     *
     * @param string $parentId
     * @param string $relatedId
     *
     * @return boolean
     */
    public function wasObservationCreated(string $managementId, string $itemId): bool
    {
        $response = false;

        if (empty($managementId) === true && empty($itemId) === true) {
            return $response;
        }

        $builder = $this->_db
                        ->getConnection()
                        ->createQueryBuilder();

        $and = $builder->expr()->andX();

        $and->add($builder->expr()->eq(self::OAT_IM_FIELD, $builder->createPositionalParameter($managementId)));
        $and->add($builder->expr()->eq(self::OAT_II_FIELD, $builder->createPositionalParameter($itemId)));

        $builder->select('id')
                ->from(self::OBSERVATION_AUDIT_TABLE)
                ->where($and);

        $builder->setMaxResults(1);

        $stmt = $builder->execute();

        while ($row = $stmt->fetch()) {
            if (empty($row["id"]) === false) {
                $response = true;
            }
        }

        return $response;
    }

    /**
     * Register the Observation in the custom audit table
     *
     * @param string $observationId
     * @param string $managementId
     * @param string $itemId
     *
     * @return void
     */
    public function registerObservation(string $observationId, string $managementId, string $itemId): void
    {
        $fieldDefs = $this->dictionary["wsys_observations_audit"]["fields"];

        $id   = create_guid();
        $time = $this->_timeDate->asDb($this->_timeDate->getNow());

        $fieldValues = array(
            "id"                        => $id,
            self::OAT_OBSERVATION_FIELD => $observationId,
            self::OAT_IM_FIELD          => $managementId,
            self::OAT_II_FIELD          => $itemId,
            "date_entered"              => $time,
            "date_modified"             => $time,
            "deleted"                   => 0,
        );

        $this->_db->insertParams(self::OBSERVATION_AUDIT_TABLE, $fieldDefs, $fieldValues);
    }
}

