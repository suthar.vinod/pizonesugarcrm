(function appFUnc(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (!app.view.views.BaseTaskD_Task_DesignPanelTopView &&
            !app.view.views.BaseTaskD_Task_DesignCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "TaskD_Task_Design", undefined, false, "base");
        }

        var tdPanelTop = "BaseTaskD_Task_DesignPanelTopView";

        if (app.view.views.BaseTaskD_Task_DesignCustomPanelTopView) {
            tdPanelTop = "BaseTaskD_Task_DesignCustomPanelTopView";
        }

        if (App.view.views[tdPanelTop].tdRmoveCreateButton === true) {
            return;
        }

        App.view.views[tdPanelTop] = App.view.views[tdPanelTop].extend({
            tdRmoveCreateButton: true,

            render: function() {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "TaskD_Task_Design") {
                    this._removetdSubpanelActionButtons();
                }

                return initializeOptions;
            },

            _removetdSubpanelActionButtons: function() {
                if (this.$el.find("[name=create_button]").length > 0) {
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });
})(SUGAR.App);