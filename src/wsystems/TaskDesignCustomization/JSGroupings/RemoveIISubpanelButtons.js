(function appFUnc(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (
            !app.view.views.BaseWPE_Work_Product_EnrollmentPanelTopView &&
            !app.view.views.BaseWPE_Work_Product_EnrollmentCustomPanelTopView
        ) {
            app.view.declareComponent("view", "panel-top", "WPE_Work_Product_Enrollment", undefined, false, "base");
        }

        var iiPanelTop = "BaseWPE_Work_Product_EnrollmentPanelTopView";

        if (app.view.views.BaseWPE_Work_Product_EnrollmentCustomPanelTopView) {
            iiPanelTop = "BaseWPE_Work_Product_EnrollmentCustomPanelTopView";
        }

        if (App.view.views[iiPanelTop].iiRmoveCreateButton === true) {
            return;
        }

        App.view.views[iiPanelTop] = App.view.views[iiPanelTop].extend({
            iiRmoveCreateButton: true,

            render: function() {
                var initializeOptions = this._super("render", arguments);

                if (this.parentModule === "GD_Group_Design") {
                    this._removeIISubpanelActionButtons();
                }       

                return initializeOptions;
            },

            _removeIISubpanelActionButtons: function () {
                if (this.$el.find("[name=create_button]").length > 0 ){
                    this.$el.find("[name=create_button]").remove();
                }
            }
        });
    });
})(SUGAR.App);