<?php

/**
 * When a record is saved, identify the next number sequence and append it to the name
 */
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class SetNameSequenceForORI {

    function setName($bean, $event, $arguments) {

        global $db;
        if ($arguments['isUpdate'] == false) {

            $or_id = $bean->or_order_request_ori_order_request_item_1or_order_request_ida;

            $sqlor = 'SELECT name FROM or_order_request WHERE id = "' . $or_id . '"';
            $resultor = $db->query($sqlor);
            if ($resultor->num_rows > 0) {
                $rowor = $db->fetchByAssoc($resultor);
                $orName = $rowor['name'];
            }

            $bean->name = $orName . '-';
            //remove extra spaces
            $bean->name = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $bean->name)));
            $existingSequence = null;
            if ($bean->fetched_row) {
                $previous_name = $bean->fetched_row['name'];
                $previous_nameExp = explode('-', $previous_name);
                $previous_nameVal = strlen($previous_nameExp[2]);
                if ($previous_nameVal == '2') {
                    $existingSequence = substr($previous_name, -2);
                } else {
                    $existingSequence = substr($previous_name, -3);
                }

                if (!is_numeric($existingSequence) === true) {
                    $existingSequence = null;
                }
            }
            if ($existingSequence >= 100) {
                $seq = $this->getSequence($bean, $existingSequence);
                $bean->name = trim($bean->name);
                if ($existingSequence === null) {
                    $bean->name .= $seq;
                } else if ($existingSequence !== $seq) {
                    $bean->name = substr($bean->name, 0, -3) . $seq;
                } else if ($existingSequence === $seq) {
                    $eseq = substr($bean->name, -3);
                    if (is_numeric($eseq) === true) {
                        $bean->name = substr($bean->name, 0, -3) . $seq;
                    } else {
                        $bean->name .= $seq;
                    }
                }
            } else {
                $seq = $this->getSequence($bean, $existingSequence);
                $bean->name = trim($bean->name);
                if ($existingSequence === null) {
                    $bean->name .= $seq;
                } else if ($existingSequence !== $seq) {
                    $bean->name = substr($bean->name, 0, -2) . $seq;
                } else if ($existingSequence === $seq) {
                    $eseq = substr($bean->name, -2);
                    if (is_numeric($eseq) === true) {
                        $bean->name = substr($bean->name, 0, -2) . $seq;
                    } else {
                        $bean->name .= $seq;
                    }
                }
            }

            $bean->name = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $bean->name)));
        }
    }

    private function getSequence($bean, $existingSequence) {
        $sugarQuery = new SugarQuery();
        $sugarQuery->from(BeanFactory::newBean($bean->module_dir));
        $sugarQuery->select(array("id", "name"));
        $sugarQuery->where()->contains("name", $bean->name);
        $resultSet = $sugarQuery->execute();

        $sequences = array();
        foreach ($resultSet as $row) {
            $row_name = $row["name"];

            $row_nameExp = explode('-', $row_name);
            $row_nameVal = strlen($row_nameExp[2]);
            if ($row_nameVal == '2') {
                $row_seq = substr($row_name, -2);
            } else {
                $row_seq = substr($row_name, -3);
            }

            if (is_numeric($row_seq)) {
                $sequences[] = intval($row_seq);
            }
        }

        if (count($sequences) > 0) {
            $max_seq = max($sequences);

            if ($existingSequence === null) {
                $max_seq += 1;
            }
            if ($max_seq > 9) {
                $seq = $max_seq;
            } else if ($max_seq <= 9) {
                $seq = "0" . $max_seq;
            }
        } else {
            $seq = "01";
        }
        return $seq;
    }

}
