<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class SetStatusForORI
{
    static $already_ran = false;
    public function setStatus($bean, $event, $arguments)
    {
        if (self::$already_ran == true) return; //So that hook will only trigger once
        self::$already_ran = true;
        $module = $arguments['module'];
        $modelId = $arguments['id'];

        $relatedModule = $arguments['related_module'];
        $relatedId = $arguments['related_id'];
        if ($module === 'ORI_Order_Request_Item' && $relatedModule === 'RI_Received_Items') {
            $this->calcStatus($modelId, $relatedId, $event, $arguments);
        } else if ($relatedModule === 'ORI_Order_Request_Item' && $module === 'RI_Received_Items') {
            $this->calcStatus($relatedId, $modelId, $event, $arguments);
        }
    }

    public function calcStatus($id, $reld, $event, $arguments)
    {
        global $db;

        $order_request_item = BeanFactory::retrieveBean('ORI_Order_Request_Item', $id);

        $order_request_item->load_relationship('po_purchase_order_ori_order_request_item_1');
        $purchase_orderID = $order_request_item->po_purchase_order_ori_order_request_item_1->get();

        $order_request_item->load_relationship('ori_order_request_item_ri_received_items_1');
        $relatedRI = $order_request_item->ori_order_request_item_ri_received_items_1->get();

        if ($event != 'after_relationship_delete') {
            array_push($relatedRI, $arguments['related_id']);
            $relatedRI = array_unique($relatedRI);
        }

        $total = 0;
        for ($a = 0; $a < count($relatedRI); $a++) {
            $RI_Received_Items = BeanFactory::retrieveBean('RI_Received_Items', $relatedRI[$a]);
            $quantity_received = $RI_Received_Items->quantity_received;
            $total = $total + $quantity_received;
        }

        $request_date = $order_request_item->request_date;
        $unit_quantity_requested = $order_request_item->unit_quantity_requested;
        if ($request_date != ""  && count($purchase_orderID) > 0 && count($relatedRI) <= 0) {
            $order_request_item->status = "Ordered";
            $order_request_item->save();
        } else if ($request_date != ""  && count($purchase_orderID) <= 0 && count($relatedRI) <= 0) {
            $order_request_item->status = "Requested";
            $order_request_item->save();
        } else  if (count($relatedRI) > 0 && $unit_quantity_requested <= $total) {
            $order_request_item->status = "Inventory";
            $order_request_item->save();
        } else if (count($relatedRI) > 0 && $unit_quantity_requested != "") {
            $order_request_item->status = "Partially Received";
            $order_request_item->save();
        }

        $this->checkORIStatusAndSetPoStatus($order_request_item);
    }

    function checkORIStatusAndSetPoStatus($order_request_item)
    {
        if ($order_request_item->po_purchase_order_ori_order_request_item_1po_purchase_order_ida != "") {

            $poid = $order_request_item->po_purchase_order_ori_order_request_item_1po_purchase_order_ida;

            $currentPOBean = BeanFactory::retrieveBean('PO_Purchase_Order', $poid);
            $currentPOBean->load_relationship('po_purchase_order_poi_purchase_order_item_1');
            $currentPOBean->load_relationship('po_purchase_order_ori_order_request_item_1');
            /** Load poi and ori bean to get all the linked record of poi and ori */
            $relatedPOI = $currentPOBean->po_purchase_order_poi_purchase_order_item_1->get();
            $relatedORI = $currentPOBean->po_purchase_order_ori_order_request_item_1->get();
            /**linkedrecstatus array : To store the status valye of all the linked poi and ori records  */
            $linkedrecstatus = array();

            /*find all the lined poi record and get the status value */
            foreach ($relatedPOI as $POIid) {
                $clonedBeanPOI = BeanFactory::getBean('POI_Purchase_Order_Item', $POIid);
                $poi_status = $clonedBeanPOI->status;
                if ($poi_status != 'Inventory') {
                    $linkedrecstatus[] = $poi_status;
                }
            }
            /*find all the lined ori record and get the status value */
            foreach ($relatedORI as $ORIid) {
                $clonedBeanORI = BeanFactory::getBean('ORI_Order_Request_Item', $ORIid);
                $ori_status = $clonedBeanORI->status;
                if ($ori_status != 'Inventory') {
                    $linkedrecstatus[] = $ori_status;
                }
            }
            /**25 nov 2021 : #1778 : Status field on Purchase Orders to automatically update to read only 
             * Complete if all of the ORIs and POIs linked to the PO have Status = Fully Received. */
            if ((count($linkedrecstatus) == 0) && (count($relatedPOI) != 0 || count($relatedORI) != 0)) {
                /**To store the old value of status field , we are using status_old_c field */
                $currentPOBean->status_old_c = $currentPOBean->fetched_row['status_c'];
                $currentPOBean->status_c = "Complete";
                $currentPOBean->save();
            } else {
                /**In case status valye changed from full recevied to other then it will pull the old value to status field */
                if ($currentPOBean->status_old_c != '') {
                    $currentPOBean->status_c = $currentPOBean->status_old_c;
                    $currentPOBean->save();
                }
            }
        }
    }
}
