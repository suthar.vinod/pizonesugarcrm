
(function appFunc(app) {
    app.events.on(
        "data:sync:start",
        function dataSyncCompleteFunct(method, collection, options) {
            if (method !== "read") return;

            var fieldObj,
                isCreateDrawer = false,
                drawerComponents = App.drawer._components;

            if (options) fieldObj = options.context;

            if (_.isUndefined(fieldObj)) return;

            _.each(
                drawerComponents,
                function eachDrawer(drawerComponent) {
                    if (drawerComponent.module === "ORI_Order_Request_Item" && drawerComponent.name === "create") {
                        isCreateDrawer = true;
                    }
                }.bind(this)
            );

            if (fieldObj.name === "prod_product_ori_order_request_item_1_name" && fieldObj.module === "ORI_Order_Request_Item") {
                _setFlagForFilterProd_Product.call(this, options);
            } else if (
                options.view === "selection-list" &&
                fieldObj.get("module") === "Prod_Product" &&
                (App.router._currentFragment.indexOf("ORI_Order_Request_Item" > -1) || isCreateDrawer)
            ) {
                _setFlagForFilterProd_Product.call(this, options);
            }
        }.bind(this)
    );

    var _setFlagForFilterProd_Product = function(options) {
        var department;
		var type;
		//var subtype;
        var isCreateDrawer = false;
        var drawerComponents = App.drawer._components;

        if (
            App.controller.layout.context.get("module") === "ORI_Order_Request_Item" &&
            App.controller.layout.context.get("layout") === "record"
        ) {
            if (drawerComponents.length > 0) {
                _.each(
                    drawerComponents,
                    function eachDrawer(drawerComponent) {
                        if (drawerComponent.module === "ORI_Order_Request_Item" && drawerComponent.name === "create") {
                            isCreateDrawer = true;
                            department = drawerComponent.model.get("department_new_c");
							type = drawerComponent.model.get("type_2");
							//subtype = drawerComponent.model.get("subtype");
                        }
                    }.bind(this)
                );
            }
            if (isCreateDrawer === false) {
                department = App.controller.layout.context.get("model").get("department_new_c");
				type = App.controller.layout.context.get("model").get("type_2");
				//subtype = App.controller.layout.context.get("model").get("subtype");
            }
        } else if (
            App.controller.layout.name === "records" &&
            options.context.module === "ORI_Order_Request_Item" &&
            options.context.name === "prod_product_ori_order_request_item_1_name"
        ) {
            
			department = options.context.model.get("department_new_c");
			type = options.context.model.get("type_2");
			//subtype = options.context.model.get("subtype");
        } else {
            var drawerComponent = _.where(App.drawer._components, { module: "ORI_Order_Request_Item" })[0];
			if(drawerComponent == undefined) {
				department = App.drawer.context.attributes.model.get('department_new_c');
				type = App.drawer.context.attributes.model.get("type_2");
				//subtype = App.drawer.context.attributes.model.get("subtype");
				
			}else if (drawerComponent.model) {
				department = drawerComponent.model.get("department_new_c");
				type = drawerComponent.model.get("type_2");
				//subtype = drawerComponent.model.get("subtype");
            }
        }

        if (_.isEmpty(department) && _.isEmpty(type)) {
            return;
        }

        console.log('mdl', App.controller.layout.context.get("module"))
        if (App.controller.layout.context.get("module") != "POI_Purchase_Order_Item") {
            options.params._filterProd_ProductRelatedToORI = true;
            options.params._ORIRelateddepartment = department;
            options.params._ORIRelatedtype = type;
            //options.params._ORIRelatedsubtype = subtype;
        }
    };
})(SUGAR.App);