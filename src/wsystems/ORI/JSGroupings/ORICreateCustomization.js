

(function appFunct(app) {
    app.events.on("app:sync:complete", function appSyncComplete() {
        if (!app.view.views.BaseORI_Order_Request_ItemCreateView && !app.view.views.BaseORI_Order_Request_ItemCustomCreateView) {
            app.view.declareComponent("view", "create", "ORI_Order_Request_Item", undefined, false, "base");
        }

        var createView = "BaseORI_Order_Request_ItemCreateView";

        if (app.view.views.BaseORI_Order_Request_ItemCustomCreateView) {
            createView = "BaseORI_Order_Request_ItemCustomCreateView";
        }

        if (App.view.views[createView].createExtended === true) {
            return;
        }

        App.view.views[createView] = App.view.views[createView].extend({
            createExtended: true,
            initialize: function () {
                this._super("initialize", arguments);

                this.model.on('change:po_purchase_order_ori_order_request_item_1_name', this.setArrivalDate, this);
                this.model.on('change:prod_product_ori_order_request_item_1_name', this.setCostUnit, this);
                this.model.on('change:product_selection change:type_2 change:department_new_c', this.setProductVisibility, this);
                this.model.on('change:request_date change:po_purchase_order_ori_order_request_item_1po_purchase_order_ida', this.setStatus, this);

                if (this.module == 'ORI_Order_Request_Item' && this.context.parent && this.context.parent.get('module') == 'PO_Purchase_Order' && this.context.parent.get('layout') == 'record') {
                    this.setArrivalDate();
                }

                $('span[data-fieldname="status"] .select2-container').removeClass('select2-container-disabled');
                $('input[name="status"]').prop('disabled', false);
            },
            render: function () {
                var superResult = this._super("render", arguments);
                this.$el.find('[data-name="prod_product_ori_order_request_item_1_name"]').css('visibility', 'hidden');
            },
            setStatus: function () {
                console.log("request_date Triggered", this.model.get('request_date'));
                console.log("po Triggered", this.model.get('po_purchase_order_ori_order_request_item_1_name'));                

                var PO_ID = this.model.get('po_purchase_order_ori_order_request_item_1po_purchase_order_ida');
                if (PO_ID != "" && PO_ID != null) {
                    
                    var self = this;
                    App.api.call("get", "rest/v10/PO_Purchase_Order/" + PO_ID + "?fields=status_c", null, {
                        success: function (Data) {
                            setTimeout(function () {
                                if (self.model.get('request_date') != '' && (self.model.get('po_purchase_order_ori_order_request_item_1_name') == '' || self.model.get('po_purchase_order_ori_order_request_item_1_name') == undefined) ) {
                                    self.model.set('status', 'Requested');
                                    $('span[data-fieldname="status"] .select2-container').addClass('select2-container-disabled');
                                    $('input[name="status"]').prop('disabled', true);
                                    console.log("setStatus Triggered 10");
                                } else if (self.model.get('request_date') != '' && self.model.get('po_purchase_order_ori_order_request_item_1_name') != '' && Data.status_c == "Pending") {
                                    self.model.set('status', 'Requested');
                                    $('span[data-fieldname="status"] .select2-container').addClass('select2-container-disabled');
                                    $('input[name="status"]').prop('disabled', true);
                                    console.log("setStatus Triggered 11");
                                } else if (self.model.get('request_date') != '' && self.model.get('po_purchase_order_ori_order_request_item_1_name') != '' && Data.status_c == "Submitted") {
                                    self.model.set('status', 'Ordered');
                                    console.log("setStatus Triggered 12");
                                } else if (self.model.get('request_date') != '' && self.model.get('po_purchase_order_ori_order_request_item_1_name') != '' && Data.status_c == "Submitted") {
                                    self.model.set('status', 'Inventory');
                                    console.log("setStatus Triggered 13");
                                }
                               
                            }, 200);
                        }
                    });
                } else if ((this.model.get('request_date') != '' || this.model.get('request_date') != undefined) && (this.model.get('po_purchase_order_ori_order_request_item_1_name') == '' || this.model.get('po_purchase_order_ori_order_request_item_1_name') == undefined) ) {
                    console.log("setStatus Triggered 2");
                    this.model.set('status', 'Requested');
                    setTimeout(function () {
                        $('span[data-fieldname="status"] .select2-container').addClass('select2-container-disabled');
                        $('input[name="status"]').prop('disabled', true);
                    }, 200);
                } else {
                    console.log("setStatus Triggered 3");
                    this.model.set('status', '');
                    setTimeout(function () {                        
                        $('span[data-fieldname="status"] .select2-container').removeClass('select2-container-disabled');
                        $('input[name="status"]').prop('disabled', false);
                    }, 200);
                }

                if((this.model.get('request_date') == '' || this.model.get('request_date') == undefined) && (this.model.get('po_purchase_order_ori_order_request_item_1_name') == '' || this.model.get('po_purchase_order_ori_order_request_item_1_name') == undefined) ){
                    console.log("setStatus Triggered 4");
                    this.model.set('status', '');
                    setTimeout(function () {                        
                        $('span[data-fieldname="status"] .select2-container').removeClass('select2-container-disabled');
                        $('input[name="status"]').prop('disabled', false);
                    }, 200);
                }

                /*if ((this.model.get('request_date') != '' && this.model.get('request_date') != undefined) && ((this.model.get('po_purchase_order_ori_order_request_item_1po_purchase_order_ida') == undefined) || this.model.get('po_purchase_order_ori_order_request_item_1po_purchase_order_ida') == '')) {

                    this.model.set('status', 'Requested');

                } else if ((this.model.get('request_date') != '' && this.model.get('request_date') != undefined) && ((this.model.get('po_purchase_order_ori_order_request_item_1po_purchase_order_ida') != undefined) || this.model.get('po_purchase_order_ori_order_request_item_1po_purchase_order_ida') != '')) {

                    this.model.set('status', 'Ordered');

                } else {
                    this.model.set('status', '');
                }*/
            },
            setArrivalDate: function (model) {
                var url = app.api.buildURL("getArrivalDateValue");
                var method = "create";
                var data = {
                    po_id: this.model.get('po_purchase_order_ori_order_request_item_1po_purchase_order_ida')
                };
                var callback = {
                    success: _.bind(function successCB(res) {
                        if (res != "") {
                            this.model.set('estimated_arrival_date', res);
                        }

                    }, this)
                };
                app.api.call(method, url, data, callback);
            },
            setCostUnit: function (model) {

                var url = app.api.buildURL("getCostPerUnit");
                var method = "create";
                var data = {
                    p_id: this.model.get('prod_product_ori_order_request_item_1prod_product_ida')
                };
                var callback = {
                    success: _.bind(function successCB(res) {
                        this.model.defaultCost = res;
                        this.model.set('cost_per_unit', res);

                    }, this)
                };
                app.api.call(method, url, data, callback);
            },
            setProductVisibility: function (model) {
                if (this.model._previousAttributes.type_2 != '' && this.model._previousAttributes.type_2 != undefined && this.model._previousAttributes.type_2 != this.model.get('type_2')) {
                    this.model.unset('prod_product_ori_order_request_item_1_name');
                    this.model.unset('prod_product_ori_order_request_item_1prod_product_ida');
                    fieldRef = this.getField('prod_product_ori_order_request_item_1_name');
                    fieldRef.render();
                }
                if (this.model._previousAttributes.department_new_c != '' && this.model._previousAttributes.department_new_c != undefined && this.model._previousAttributes.department_new_c != this.model.get('department_new_c')) {
                    this.model.unset('prod_product_ori_order_request_item_1_name');
                    this.model.unset('prod_product_ori_order_request_item_1prod_product_ida');
                    fieldRef = this.getField('prod_product_ori_order_request_item_1_name');
                    fieldRef.render();
                }
                /*if (this.model._previousAttributes.subtype != undefined && this.model._previousAttributes.subtype != this.model.get('subtype')) {
                    this.model.unset('prod_product_ori_order_request_item_1_name');
                    this.model.unset('prod_product_ori_order_request_item_1prod_product_ida');
                    fieldRef = this.getField('prod_product_ori_order_request_item_1_name');
                    fieldRef.render();
                }*/
                
                if ((this.model.get('product_selection') == 'In System Vendor Non Specific' || this.model.get('product_selection') == 'In System Vendor Specific') && this.model.get('type_2') != '' && this.model.get('department_new_c') != '') {
                    this.$el.find('[data-name="prod_product_ori_order_request_item_1_name"]').css('visibility', 'visible');
                }else {
                    this.$el.find('[data-name="prod_product_ori_order_request_item_1_name"]').css('visibility', 'hidden');
                }
            },
            saveAndClose: function () {
                console.log('1', this.model.get('request_date'));

                /*if ((this.model.get('request_date') != '' || this.model.get('request_date') != undefined) && (this.model.relatedAttributes.po_purchase_order_ori_order_request_item_1po_purchase_order_ida == "" || this.model.relatedAttributes.po_purchase_order_ori_order_request_item_1po_purchase_order_ida == undefined)) {
                    this.model.set('status', 'Requested');
                    console.log('Requested', this.model.get('status'));
                } else if ((this.model.get('request_date') != '' || this.model.get('request_date') != undefined) && (this.model.relatedAttributes.po_purchase_order_ori_order_request_item_1po_purchase_order_ida != "" || this.model.relatedAttributes.po_purchase_order_ori_order_request_item_1po_purchase_order_ida != undefined)) {
                    this.model.set('status', 'Ordered');
                    console.log('Ordered', this.model.get('status'));
                }*/

                console.log('on save ', this.model.get('status'));

                var self = this;
                if (this.model.defaultCost != undefined && this.model.defaultCost != this.model.get('cost_per_unit') && this.model.get('prod_product_ori_order_request_item_1_name') != '' && this.model.get('prod_product_ori_order_request_item_1_name') != undefined) {
                    console.log('Line 116');
                    var productID = this.model.get('prod_product_ori_order_request_item_1prod_product_ida');
                    var costPerUnit = this.model.get('cost_per_unit');

                    app.alert.show('message-id', {
                        level: 'confirmation',
                        messages: 'Cost Per Unit field was updated. Click Confirm to edit corresponding Product record.',
                        autoClose: false,
                        onConfirm: function () {
                            app.api.call("create", "rest/v10/update_product_cost_unit", {
                                module: 'Prod_Product',
                                product: productID,
                                cost_per_unit: costPerUnit,
                            }, {
                                success: function (result) {
                                    console.log('Line 131');
                                    self.initiateSave(_.bind(function () {
                                        app.navigate(self.context, self.model);
                                    }, self));                                   
                                },
                                error: function (error) {
                                    app.alert.show('link_wps_error', {
                                        level: 'error',
                                        messages: 'Failed Creating Duplicates!'
                                    });
                                }
                            });
                        },
                        onCancel: function () {
                            self.initiateSave(_.bind(function () {
                                app.navigate(self.context, self.model);
                            }, self));
                        }
                    });
                } else {
                    console.log('Line 203');
                    self.initiateSave(_.bind(function () {
                        app.navigate(self.context, self.model);
                    }, self));
                }
            },
        })
    });
})(SUGAR.App);

