/**
 * Add the scan button on the Inventory Items Subpanel in order to link multiple items by scanning.
 */

;
(function (app) {
    app.events.on("app:sync:complete", function () {
        if (!app.view.views.BaseII_Inventory_ItemPanelTopView && !app.view.views.BaseII_Inventory_ItemCustomPanelTopView) {
            app.view.declareComponent("view", "panel-top", "II_Inventory_Item", undefined, false, "base");
        }

        var panelTopView = "BaseII_Inventory_ItemPanelTopView";

        if (app.view.views.BaseII_Inventory_ItemCustomPanelTopView) {
            panelTopView = "BaseII_Inventory_ItemCustomPanelTopView";
        }

        if (App.view.views[panelTopView].scanButtonAdded === true) {
            return;
        }

        App.view.views[panelTopView] = App.view.views[panelTopView].extend({
            scanButtonAdded: true,

            /**
             * Override the initialize function in order to add the scan button at the top of the subpanel.
             * @param {Object} options 
             */
            initialize: function (options) {
                this._super("initialize", arguments);

                var scanButton = {
                    name: "scan",
                    type: "rowaction",
                    label: "Scan Item(s)",
                    events: {
                        click: "subpanellist:scan:fire"
                    }
                };

                /**
                 * Making sure the necessary structure for pushing our button, exists.
                 * Might need improvement.
                 */
                if (this.meta.buttons[0] && typeof this.meta.buttons[0].buttons !== "undefined" &&
                    typeof this.meta.buttons[0].buttons !== "undefined" && typeof this.meta.buttons[0].buttons.push == "function") {

                    if (!this.scanButtonExists(this.meta.buttons[0].buttons) === true) {
                        this.meta.buttons[0].buttons.push(scanButton);
                    }

                    this.context.on("subpanellist:scan:fire", _.bind(this.openScanDrawer, this));
                }

            },

            /**
             * Checks wheter the scan button already exists in the dropdown
             * @param {array} buttons 
             */
            scanButtonExists: function (buttons) {
                for (var index in buttons) {
                    var button = buttons[index];
                    if (button.name === "scan") {
                        return true;
                    }
                }

                return false;
            },

            /**
             * Open a drawer with a selection list
             */
            openScanDrawer: function () {
                app.drawer.open(
                    this.getDrawerOptions(),
                    _.bind(this.selectDrawerCallback, this)
                );
            },

            /**
             * retrieve the options for the current drawer
             */
            getDrawerOptions: function () {
                var parentModel = this.context.get('parentModel');
                var linkModule = this.context.get('module');
                var link = this.context.get('link');

                var filterOptions = new app.utils.FilterOptions().config(this.def);
                filterOptions.populateRelate(parentModel);

                return {
                    layout: 'inventory-management-selection-list-link',
                    context: {
                        module: linkModule,
                        recParentModel: parentModel,
                        recLink: link,
                        recContext: this.context,
                        recView: this.view,
                        independentMassCollection: true,
                        filterOptions: filterOptions.format()
                    }
                };
            },

            selectDrawerCallback: function () {

            }
        });
    });
})(SUGAR.App);