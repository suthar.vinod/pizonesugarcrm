/**
 * Override the _selectMultipleAndClose function in order to make sure all the conditions for linking are met
 */


;
(function (app) {
    app.events.on("app:sync:complete", function () {

        /**
         * Extend the multi-selection list view for Inventory Item
         */
        if (!app.view.views.BaseII_Inventory_ItemMultiSelectionListLinkView && !app.view.views.BaseII_Inventory_ItemCustomMultiSelectionListLinkView) {
            app.view.declareComponent("view", "multi-selection-list-link", "II_Inventory_Item", undefined, false, "base");
        }

        var multiSelectionListLinkView = "BaseII_Inventory_ItemMultiSelectionListLinkView";

        if (app.view.views.BaseII_Inventory_ItemCustomMultiSelectionListLinkView) {
            multiSelectionListLinkView = "BaseII_Inventory_ItemCustomMultiSelectionListLinkView";
        }

        if (App.view.views[multiSelectionListLinkView].linkFunctionOverriden === true) {
            return;
        }

        App.view.views[multiSelectionListLinkView] = App.view.views[multiSelectionListLinkView].extend({
            linkFunctionOverriden: true,

            /**
             * The initialize function
             * 
             * I override this function in order to override the 
             * collection.fetch function ofr this module
             * 
             * This is being used afterwards in a before_filter hook
             * @param {Object} options 
             */
            initialize: function (options) {
                this._super("initialize", arguments);

                var collection = this.collection;
                var protoFetch = this.collection.fetch;
                var parentModule = this.context.parent.attributes.module;

                this.collection.fetch = function (options_data) {
                    if (!options_data.params) {
                        options_data.params = {};
                    }

                    options_data.params.from_context = "ii_multi_selection";
                    options_data.params.parent_module = parentModule;
                    protoFetch.apply(collection, [options_data]);
                }
            },

            /**
             * When the search is completed and the user wants to link the items, then perform a compliance check
             */
            _selectMultipleAndClose: function () {
                if (this.context.get("recLink") === "ii_inventory_item_im_inventory_management_1") {
                    var selections = this.context.get('mass_collection');
                    var inventory_items_ids = _.map(selections.models, function (model) {
                        return model.get("id")
                    });
                    var inventory_managemet_id = this.context.get("recParentModel").id;

                    app.api.call("create", "rest/v10/checkInventoryItemsCompliance", {
                        inventory_items_ids: inventory_items_ids,
                        inventory_managemet_id: inventory_managemet_id
                    }, {
                        success: function (response) {
                            var items = response.items;
                            var notMatching = items["not_matching"];
                            var inactive = items["inactive_items"];

                            if (notMatching.length > 0) {
                                app.alert.show('message-id', {
                                    level: 'error',
                                    messages: "Work Products not a match!",
                                    autoClose: true
                                });
                                for (var i in notMatching) {
                                    var id = notMatching[i];
                                    this.layout.layout.getComponent("selection-list-context").$el.find(".select2-search-choice[data-id='" + id + "']").css("background", "red");
                                }
                            }
                            if (inactive.length > 0) {
                                app.alert.show('message-id', {
                                    level: 'error',
                                    messages: "Inactive Work Products cannot be linked!",
                                    autoClose: true
                                });
                                for (var i in inactive) {
                                    var id = inactive[i];
                                    this.layout.layout.getComponent("selection-list-context").$el.find(".select2-search-choice[data-id='" + id + "']").css("background", "orange");
                                }
                            }

                            if (notMatching.length === 0 && inactive.length === 0) {
                                this._super("_selectMultipleAndClose", arguments);
                            }
                        }.bind(this),
                        error: function (error) {
                            app.alert.show('message-id', {
                                level: 'error',
                                messages: error.message,
                                autoClose: true
                            });
                        }
                    })
                } else {
                    this._super("_selectMultipleAndClose", arguments);
                }
            }
        });


        /**
         * Extend the multi-selection list view for Inventory Management
         */
        if (!app.view.views.BaseIM_Inventory_ManagementMultiSelectionListLinkView && !app.view.views.BaseIM_Inventory_ManagementCustomMultiSelectionListLinkView) {
            app.view.declareComponent("view", "multi-selection-list-link", "IM_Inventory_Management", undefined, false, "base");
        }

        var imMultiSelectionListLinkView = "BaseIM_Inventory_ManagementMultiSelectionListLinkView";

        if (app.view.views.BaseIM_Inventory_ManagementCustomMultiSelectionListLinkView) {
            imMultiSelectionListLinkView = "BaseIM_Inventory_ManagementCustomMultiSelectionListLinkView";
        }

        if (App.view.views[imMultiSelectionListLinkView].linkFunctionOverriden === true) {
            return;
        }

        App.view.views[imMultiSelectionListLinkView] = App.view.views[imMultiSelectionListLinkView].extend({
            linkFunctionOverriden: true,

            /**
             * When the search is completed and the user wants to link the items, then perform a compliance check
             */
            _selectMultipleAndClose: function () {
                if (this.context.get("recLink") === "ii_inventory_item_im_inventory_management_1") {
                    var selections = this.context.get('mass_collection');
                    var inventoryManagementIds = _.map(selections.models, function (model) {
                        return model.get("id")
                    });

                    var inventoryItemId = this.context.get("recParentModel").id;
                    var imPromises = []
                    for (var index in inventoryManagementIds) {
                        var imId = inventoryManagementIds[index];
                        var imCheck = this.checkIMCompliance(imId, inventoryItemId);
                        imPromises.push(imCheck);
                    }

                    var errorsFound = false;
                    Promise.all(imPromises).then(function (results) {
                        for (var index in results) {
                            var result = results[index];
                            if (result.no_match) {
                                errorsFound = true;
                            }
                            if (result.no_match) {
                                this.layout.layout.getComponent("selection-list-context").$el.find(".select2-search-choice[data-id='" + result.no_match + "']").css("background", "red");
                            }
                        }

                        if (errorsFound === false) {
                            this._super("_selectMultipleAndClose", arguments);
                        } else {
                            app.alert.show('message-id', {
                                level: 'error',
                                messages: "Work Products not a match or inactive!",
                                autoClose: true
                            });
                        }
                    }.bind(this))

                } else {
                    this._super("_selectMultipleAndClose", arguments);
                }
            },

            /**
             * Check if the IM can be linked to II
             * @param {String} imId 
             * @param {String} iiId 
             */
            checkIMCompliance: function (imId, iiId) {
                return new Promise(function (resolve, reject) {
                    app.api.call("create", "rest/v10/checkInventoryItemsCompliance", {
                        inventory_items_ids: [iiId],
                        inventory_managemet_id: imId
                    }, {
                        success: function (imId, response) {
                            var items = response.items;

                            var notMatching = items["not_matching"];
                            var inactive = items["inactive_items"];

                            if (notMatching.length > 0 || inactive.length > 0) {
                                resolve({
                                    no_match: imId
                                })

                            } else if (notMatching.length === 0 && inactive.length === 0) {
                                resolve({
                                    match: imId
                                })
                            }
                        }.bind(this, imId),
                        error: function (error) {
                            resolve({
                                no_match: imId
                            })
                        }
                    });
                });
            }
        });

    })
})(SUGAR.App);