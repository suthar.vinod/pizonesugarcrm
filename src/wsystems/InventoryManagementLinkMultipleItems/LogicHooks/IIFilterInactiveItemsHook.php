<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * before_filter hook
 *
 * Do not show Inactive Items
 */
class IIFilterInactiveItemsHook
{
    public function filterItems($bean, $event, $arguments)
    {
        $context      = $_REQUEST['from_context'];
        $parentModule = $_REQUEST['parent_module'];

        if (isset($context) === true &&
            $context === 'ii_multi_selection' &&
            $parentModule === "IM_Inventory_Management") {
            $arguments[0]->where()->equals('inactive_c', 0);
        }
    }
}
