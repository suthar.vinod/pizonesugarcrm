<?php

namespace Sugarcrm\Sugarcrm\custom\wsystems\InventoryItemCreation\Jobs;

class InventoryItemCreateDuplicates implements \RunnableSchedulerJob
{
    public function setJob(\SchedulersJob $job)
    {
        $this->schedulerJob = $job;
    }

    public function run($data)
    {
        $jobData = unserialize($data);

        $module   = $jobData["module"];
        $recordId = $jobData["recordId"];
        $dup      = new \Sugarcrm\Sugarcrm\custom\wsystems\InventoryItemCreation\Utils\InventoryItemCreateDuplicatesUtils($module, $recordId);
        $dup->duplicate();
        return true;
    }
}
