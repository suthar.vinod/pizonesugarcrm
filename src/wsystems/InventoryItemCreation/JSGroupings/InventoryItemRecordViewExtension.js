/**
 * Extend the record view in order to populat the unique id based on the Unique Id Type
 */
/*$(document).on('click', '.closeCustomIconIIRecord', function() {
    selfIIR.saveInventoryItemRecordRecordView();
});*/

(function (app) {
    app.events.on("app:sync:complete", function () {
        if (!app.view.views.BaseII_Inventory_ItemRecordView && !app.view.views.BaseII_Inventory_ItemCustomRecordView) {
            app.view.declareComponent("view", "record", "II_Inventory_Item", undefined, false, "base");
        }

        var recordView = "BaseII_Inventory_ItemRecordView";

        if (app.view.views.BaseII_Inventory_ItemCustomRecordView) {
            recordView = "BaseII_Inventory_ItemCustomRecordView";
        }

        if (App.view.views[recordView].recordExtended === true) {
            return;
        }

        App.view.views[recordView] = App.view.views[recordView].extend({
            recordExtended: true,

            initialize: function (options) {
                this._super("initialize", arguments);
                this._bindEvents();
            },

            /**
             * Register events
             */
            _bindEvents: function () {
                this.model.on('change:unique_id_type', this.setUniqueId, this);
                this.model.on('change:collection_task', this.setTimepoint, this);
            },

            /**
             * calculate the timepoint field based on the task design relate
             * @param {SugarBean} model 
             */
            setTimepoint: function (model) {
                var taskId = model.get("tdes_task_design_id_c");
                if (taskId) {
                    app.api.call("read", "rest/v10/TDes_Task_Design/" + taskId + "?fields=timepoint,timepoint_integer,timepoint_unit", null, {
                        success: function (task) {
                            if (task && task.id) {
                                if (task.timepoint &&
                                    (task.timepoint == 'Day 0' || task.timepoint == 'Day 1' || task.timepoint == 'None')) {
                                    this.model.set('ii_timepoint_c', task.timepoint);
                                } else {
                                    this.model.set('ii_timepoint_c', task.timepoint_unit + " " + task.timepoint_integer);
                                }
                            }
                        }.bind(this),
                        error: function (error) {
                            app.alert.show('error_retreiving', {
                                level: 'error',
                                messages: 'Error retrieving Task'
                            })
                        }
                    })
                }
            },

            /**
             * generate a unieu id when type is internal
             * @param {SugarBean} model 
             */
            setUniqueId: function (model) {
                if (model.get("unique_id_type") == "Internal") {
                    this.model.set("unique_id_c", this.generateUniqueId());
                    this.hideUniqueId();
                    this.showQuantity();
                } else if (model.get("unique_id_type") == "External") {
                    //this.model.set("unique_id_c", "");
                    this.showUniqueId();
                    //this.hideQuantity();
                } else {
                    this.hideUniqueId();
                    //this.hideQuantity();
                }
            },

            showQuantity: function () {
                this.$el.find(".record-cell[data-name='quantity_c']").show();
            },

            hideQuantity: function () {
                this.$el.find(".record-cell[data-name='quantity_c']").hide();
            },

            hideUniqueId: function () {
                this.$el.find(".record-cell[data-name='unique_id_c']").hide();
            },

            showUniqueId: function () {
                this.$el.find(".record-cell[data-name='unique_id_c']").show();
            },

            generateUniqueId: function () {
                return "AP-" + Math.random().toString().substring(2, 10)
            },
            /*handleSave: function () {
                var other_type = this.model.get("other_type_c");
                var type = this.model.get("type_2");
                var category = this.model.get("category");
                var related = this.model.get("related_to_c");

                var moduleId = this.model.get('id');
                var self = this;

                app.api.call("read", "rest/v10/II_Inventory_Item/" + moduleId + "?fields=other_type_c", null, {
                    success: function (Data) {
                        console.log('Data', Data);
                        var nameII = self.model.get("name");
                        var lastThree = nameII.substr(nameII.length - 3);
                        if (Data.other_type_c != other_type) {
                            console.log('0');
                            
                            if (type == "Other" && category == "Specimen") {
                                console.log('1');
                                var IIname = '';
                                if (related == "Sales" || related == "Work Product") {
                                    var name = nameII.split(' ');
                                    console.log('2', name);
                                    var name0 = name[0];
                                    IIname = name0 + " " + other_type + " " + lastThree;
                                    self.model.set('name', IIname);
                                } else {
                                    var name1 = nameII.split(' ');
                                    console.log('3', name1);
                                    var name00 = name1[0];
                                    var name11 = name1[1];
                                    IIname = name00 + " " + name11 + " " + other_type + " " + lastThree;
                                    self.model.set('name', IIname);
                                }
                                
                                console.log('IIname',IIname);
                            }
                            setTimeout(function () {
                                self._super('handleSave');
                            }, 400);
                        } else {
                            //console.log('2');
                            self._super('handleSave');
                        }
                    }.bind(self),
                    error: function (error) {

                    }
                })


            },*/
        })
    });
})(SUGAR.App);